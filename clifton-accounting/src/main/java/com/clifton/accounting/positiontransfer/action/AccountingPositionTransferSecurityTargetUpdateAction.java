package com.clifton.accounting.positiontransfer.action;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.action.processor.AccountingJournalActionProcessor;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.util.InvestmentAccountSecurityTargetAdjustmentUtilHandler;
import com.clifton.system.schema.SystemTable;
import com.clifton.core.util.MathUtils;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;


/**
 * An AccountingJournalAction to update a client account's {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget} TargetUnderlyingQuantity, upon booking or unbooking
 * an AccountPositionTransfer increasing or decreasing the underlying quantity accordingly.
 *
 * @author davidi
 */
public class AccountingPositionTransferSecurityTargetUpdateAction implements AccountingJournalActionProcessor<AccountingPositionTransfer> {

	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	private InvestmentAccountSecurityTargetAdjustmentUtilHandler investmentAccountSecurityTargetAdjustmentUtilHandler;

	private AccountingPositionTransferService accountingPositionTransferService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processAction(AccountingPositionTransfer bookableEntity, AccountingJournal journal) {
		updateSecurityTargets(bookableEntity, true);
	}


	@Override
	public void rollbackAction(AccountingPositionTransfer bookableEntity, AccountingJournal journal) {
		updateSecurityTargets(bookableEntity, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Transactional
	protected void updateSecurityTargets(AccountingPositionTransfer positionTransfer, boolean booking) {

		SystemTable systemTable = positionTransfer.getSourceSystemTable();
		if (!"InvestmentAccountSecurityTarget".equals(systemTable.getName())) {
			return;
		}

		InvestmentAccountSecurityTarget securityTarget = getTransferSecurityTarget(positionTransfer);
		if (securityTarget != null) {
			InvestmentAccount fromClientAccount = positionTransfer.getFromClientInvestmentAccount();
			InvestmentAccount toClientAccount = positionTransfer.getToClientInvestmentAccount();

			Integer underlyingSecurityTargetId = securityTarget.getTargetUnderlyingSecurity().getId();
			List<AccountingPositionTransferDetail> detailList = CollectionUtils.getStream(positionTransfer.getDetailList())
					.filter(detail -> underlyingSecurityTargetId.equals(detail.getSecurity().getId()))
					.collect(Collectors.toList());

			InvestmentAccountSecurityTarget updatedSecurityTarget = securityTarget;
			for (AccountingPositionTransferDetail detail : detailList) {
				if (fromClientAccount != null) {
					updatedSecurityTarget = adjustSecurityTargetQuantitiesAndSave(securityTarget, detail, false, booking);
				}
				if (toClientAccount != null) {
					updatedSecurityTarget = adjustSecurityTargetQuantitiesAndSave(securityTarget, detail, true, booking);
				}
			}
			// Update AccountingPositionTransfer.SourceFkField with security target that actually got updated, to prevent it pointing to a historical target that is no longer active.
			if (!securityTarget.equals(updatedSecurityTarget)) {
				positionTransfer.setSourceFkFieldId(updatedSecurityTarget.getId());
				getAccountingPositionTransferService().saveAccountingPositionTransfer(positionTransfer);
			}
		}
	}


	/**
	 * Returns the security target defined as the source of the position transfer. Returns null if the security target is not found or is no longer active because there is nothing to adjust.
	 */
	private InvestmentAccountSecurityTarget getTransferSecurityTarget(AccountingPositionTransfer positionTransfer) {
		Integer investmentAccountSecurityTargetId = positionTransfer.getSourceFkFieldId();
		AssertUtils.assertNotNull(investmentAccountSecurityTargetId, "The InvestmentAccountSecurityTargetId cannot be null.");
		InvestmentAccountSecurityTarget securityTarget = getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTarget(investmentAccountSecurityTargetId);

		if (securityTarget == null) {
			// security target was probably deleted
			LogUtils.warn(LogCommand.ofMessage(getClass(), "Unable to find Investment Account Security Target to adjust with ID: ", investmentAccountSecurityTargetId));
		}
		return securityTarget;
	}


	private InvestmentAccountSecurityTarget adjustSecurityTargetQuantitiesAndSave(InvestmentAccountSecurityTarget securityTarget, AccountingPositionTransferDetail detail, boolean transferToAccount, boolean booking) {

		// adjust the quantity to + / - depending on whether we are booking or unbooking, and whether this is a transfer to or from the target
		BigDecimal adjustedDetailQuantity = transferToAccount ? detail.getQuantity() : MathUtils.negate(detail.getQuantity());
		adjustedDetailQuantity = booking ? adjustedDetailQuantity : MathUtils.negate(adjustedDetailQuantity);
		InvestmentAccountSecurityTarget updatedSecurityTarget = securityTarget;

		switch (securityTarget.getTargetType()) {
			case InvestmentAccountSecurityTarget.TARGET_TYPE_QUANTITY:
				updatedSecurityTarget = getInvestmentAccountSecurityTargetAdjustmentUtilHandler().adjustInvestmentAccountSecurityTargetQuantity(securityTarget, adjustedDetailQuantity);
				break;

			case InvestmentAccountSecurityTarget.TARGET_TYPE_NOTIONAL:
				updatedSecurityTarget = getInvestmentAccountSecurityTargetAdjustmentUtilHandler().adjustInvestmentAccountSecurityTargetNotionalFromQuantity(securityTarget, adjustedDetailQuantity, detail.getTransferPrice());
				break;
		}
		return updatedSecurityTarget;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public InvestmentAccountSecurityTargetAdjustmentUtilHandler getInvestmentAccountSecurityTargetAdjustmentUtilHandler() {
		return this.investmentAccountSecurityTargetAdjustmentUtilHandler;
	}


	public void setInvestmentAccountSecurityTargetAdjustmentUtilHandler(InvestmentAccountSecurityTargetAdjustmentUtilHandler investmentAccountSecurityTargetAdjustmentUtilHandler) {
		this.investmentAccountSecurityTargetAdjustmentUtilHandler = investmentAccountSecurityTargetAdjustmentUtilHandler;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}
}
