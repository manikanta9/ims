package com.clifton.accounting.positiontransfer.instruction;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instruction.selector.BaseInvestmentInstructionSelector;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;

import java.util.Date;
import java.util.List;


/**
 * The <code>BaseAccountingPositionTransferInstructionSelector</code> generates instruction items for position transfers based on selected criteria
 * <p>
 * NOTE: This is currently set up based on OTC Collateral Cash and Position Transfers (All entered into the AccountingPositionTransfer table)
 * So it required holding account type selection.  As other transfer types are supported, additional modifications will be required. For each transfer, we need to know if we
 * are instructing for the from or the to holding account.  Since OTC collateral transfers only have the OTC ISDA account on the from or to, this works for those cases.
 *
 * @author manderson
 */
public abstract class BaseAccountingPositionTransferInstructionSelector extends BaseInvestmentInstructionSelector<AccountingPositionTransfer> {

	private AccountingPositionTransferService accountingPositionTransferService;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	////////////////////////////////////////////////////////////////////////////////

	private boolean collateralTransfer;

	// Required in order to know which account in the transfer we are instructing on i.e. "counterparty" account
	private Short holdingAccountTypeId;

	private Integer holdingAccountIssuerId;

	private Integer holdingAccountId;

	private List<Integer> includeHoldingAccountGroupIds;

	private List<Integer> excludeHoldingAccountGroupIds;

	private List<Short> transferTypeList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getInstructionItemLabel(AccountingPositionTransfer entity) {
		return entity.getLabel();
	}


	@Override
	public boolean isInstructionItemBooked(InvestmentInstructionCategory category, AccountingPositionTransfer entity) {
		return entity.getBookingDate() != null;
	}


	@Override
	public void populateInstructionSelectionDetails(InvestmentInstructionSelection selection, AccountingPositionTransfer entity) {
		if (entity.getFromHoldingInvestmentAccount() != null && entity.getFromHoldingInvestmentAccount().getType().getId().equals(getHoldingAccountTypeId())) {
			selection.setHoldingAccount(entity.getFromHoldingInvestmentAccount());
			selection.setClientAccount(entity.getFromClientInvestmentAccount());
		}
		else {
			selection.setClientAccount(entity.getToClientInvestmentAccount());
			selection.setHoldingAccount(entity.getToHoldingInvestmentAccount());
		}
		selection.setBooked(isInstructionItemBooked(selection.getCategory(), entity));
	}


	@Override
	public List<AccountingPositionTransfer> getEntityList(InvestmentInstructionCategory category, InvestmentInstructionDefinition definition, Date date) {
		// Get the List Where "From" Account Matches
		List<AccountingPositionTransfer> transferList = getEntityListImpl(category, definition, date, true);
		// Append the List Where "To" Account Matches
		transferList.addAll(getEntityListImpl(category, definition, date, false));
		return transferList;
	}


	private List<AccountingPositionTransfer> getEntityListImpl(InvestmentInstructionCategory category, InvestmentInstructionDefinition definition, Date date, boolean from) {
		AccountingPositionTransferSearchForm searchForm = new AccountingPositionTransferSearchForm();
		searchForm.setTransactionDate(date);
		searchForm.setTransferTypeCash(category.isCash());
		if (!CollectionUtils.isEmpty(getTransferTypeList())) {
			if (getTransferTypeList().size() == 1) {
				searchForm.setTransferTypeId(getTransferTypeList().get(0));
			}
			else {
				searchForm.setTransferTypeIds(getTransferTypeList().toArray(new Short[getTransferTypeList().size()]));
			}
		}
		if (from) {
			if (getHoldingAccountId() != null) {
				searchForm.setFromHoldingInvestmentAccountId(getHoldingAccountId());
			}
			else {
				searchForm.setFromHoldingInvestmentAccountTypeId(getHoldingAccountTypeId());
				searchForm.setFromHoldingInvestmentAccountIssuerId(getHoldingAccountIssuerId());
				searchForm.setFromHoldingInvestmentAccountGroupIds(CollectionUtils.toArrayOrNull(getIncludeHoldingAccountGroupIds(), Integer.class));
				searchForm.setFromExcludeHoldingInvestmentAccountGroupIds(CollectionUtils.toArrayOrNull(getExcludeHoldingAccountGroupIds(), Integer.class));
			}
		}
		else {
			if (getHoldingAccountId() != null) {
				searchForm.setToHoldingInvestmentAccountId(getHoldingAccountId());
			}
			else {
				searchForm.setToHoldingInvestmentAccountTypeId(getHoldingAccountTypeId());
				searchForm.setToHoldingInvestmentAccountIssuerId(getHoldingAccountIssuerId());
				searchForm.setToHoldingInvestmentAccountGroupIds(CollectionUtils.toArrayOrNull(getIncludeHoldingAccountGroupIds(), Integer.class));
				searchForm.setToExcludeHoldingInvestmentAccountGroupIds(CollectionUtils.toArrayOrNull(getExcludeHoldingAccountGroupIds(), Integer.class));
			}
		}
		// Don't filter if definition is null - means that we are retrieving all to find transfers missing instructions
		if (definition != null) {
			searchForm.setTransferTypeCollateral(isCollateralTransfer());
		}
		return getAccountingPositionTransferService().getAccountingPositionTransferList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public boolean isCollateralTransfer() {
		return this.collateralTransfer;
	}


	public void setCollateralTransfer(boolean collateralTransfer) {
		this.collateralTransfer = collateralTransfer;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public List<Short> getTransferTypeList() {
		return this.transferTypeList;
	}


	public void setTransferTypeList(List<Short> transferTypeList) {
		this.transferTypeList = transferTypeList;
	}


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public List<Integer> getIncludeHoldingAccountGroupIds() {
		return this.includeHoldingAccountGroupIds;
	}


	public void setIncludeHoldingAccountGroupIds(List<Integer> includeHoldingAccountGroupIds) {
		this.includeHoldingAccountGroupIds = includeHoldingAccountGroupIds;
	}


	public List<Integer> getExcludeHoldingAccountGroupIds() {
		return this.excludeHoldingAccountGroupIds;
	}


	public void setExcludeHoldingAccountGroupIds(List<Integer> excludeHoldingAccountGroupIds) {
		this.excludeHoldingAccountGroupIds = excludeHoldingAccountGroupIds;
	}
}
