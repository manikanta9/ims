package com.clifton.accounting.positiontransfer.booking;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.beans.BeanUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;


/**
 * The AccountingInternalPositionTransferClosingRealizedBookingRule class is a special Gain/Loss booking rule that applies regular gain loss logic
 * to closing internal transfer positions and uses "Security Distribution" GL Account instead of "Cash" for offsetting entry.
 *
 * @author vgomelsky
 */
public class AccountingInternalPositionTransferClosingRealizedBookingRule extends AccountingRealizedGainLossBookingRule<AccountingPositionTransfer> {

	@Override
	protected AccountingJournalDetail createOffsettingEntry(AccountingJournalDetail realizedEntry, InvestmentSecurity settlementCurrency) {
		AccountingJournalDetail distribution = new AccountingJournalDetail();
		BeanUtils.copyProperties(realizedEntry, distribution);
		distribution.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.EQUITY_SECURITY_DISTRIBUTION));
		distribution.setLocalDebitCredit(MathUtils.negate(distribution.getLocalDebitCredit()));
		distribution.setBaseDebitCredit(MathUtils.negate(distribution.getBaseDebitCredit()));
		distribution.setDescription("Gain/Loss distribution from transfer from " + realizedEntry.getClientInvestmentAccount().getLabel());

		return distribution;
	}
}
