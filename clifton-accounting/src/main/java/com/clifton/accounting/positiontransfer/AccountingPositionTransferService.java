package com.clifton.accounting.positiontransfer;


import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferDetailSearchForm;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferTypeSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.List;


/**
 * The <code>AccountingPositionTransferService</code> interface defines methods for performing position transfers.
 *
 * @author vgomelsky
 */
public interface AccountingPositionTransferService {

	public static final String ACCOUNTING_POSITION_TRANSFER_DELETED_EVENT_NAME = "AccountingPositionTransferDeleted";
	public static final String ACCOUNTING_POSITION_TRANSFER_SAVED_EVENT_NAME = "AccountingPositionTransferSaved";


	/**
	 * Returns AccountingPositionTransfer with corresponding details populated.
	 */
	public AccountingPositionTransfer getAccountingPositionTransfer(int id);


	public AccountingPositionTransferDetail getAccountingPositionTransferDetail(int id);


	public List<AccountingPositionTransfer> getAccountingPositionTransferList(AccountingPositionTransferSearchForm searchForm);


	public List<AccountingPositionTransferDetail> getAccountingPositionTransferDetailList(AccountingPositionTransferDetailSearchForm searchForm);


	public void saveAccountingPositionTransfer(AccountingPositionTransfer transfer);


	public void deleteAccountingPositionTransfer(int id);


	////////////////////////////////////////////////////////////////////////////
	////////                Upload Save Methods                      ///////////
	////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public void saveAccountingPositionTransferFromUpload(final AccountingPositionTransfer bean);


	@DoNotAddRequestMapping
	public void saveAccountingPositionTransferDetailFromUpload(AccountingPositionTransferDetail bean);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferType getAccountingPositionTransferType(short id);


	@DoNotAddRequestMapping
	public List<AccountingPositionTransferType> getAccountingPositionTransferTypeList();


	public List<AccountingPositionTransferType> getAccountingPositionTransferTypeList(AccountingPositionTransferTypeSearchForm searchForm);


	public AccountingPositionTransferType getAccountingPositionTransferTypeByName(String name);
}
