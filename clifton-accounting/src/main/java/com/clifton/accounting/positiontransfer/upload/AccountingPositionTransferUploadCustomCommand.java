package com.clifton.accounting.positiontransfer.upload;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommand;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferContext;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadCommand;

import java.util.Date;


/**
 * The <code>AccountingPositionTransferUploadCustomCommand</code> is a custom System Upload that dynamically creates and groups associated
 * {@link AccountingPositionTransfer} and {@link com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail}
 * objects based on information provided in the upload file's rows or form data.
 * <p>
 * Note: Upload is really a list of details, grouped into new transfers based on matching criteria including transfer natural keys
 * and security instrument hierarchy and trading security.
 *
 * @author NickK
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class AccountingPositionTransferUploadCustomCommand extends FileUploadCommand<AccountingPositionTransferCommand> {

	public static final String[] REQUIRED_PROPERTIES = {"securitySymbol"};

	private final AccountingPositionTransferContext accountingPositionTransferContext = new AccountingPositionTransferContext();

	/**
	 * Optional transfer fields for use as defaults in an upload file's rows.
	 */
	private Date settlementDate;
	private Date transactionDate;
	private Date priceDate;
	private AccountingPositionTransferType type;
	private String note;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<AccountingPositionTransferCommand> getUploadBeanClass() {
		return AccountingPositionTransferCommand.class;
	}


	@Override
	public String[] getRequiredProperties() {
		return REQUIRED_PROPERTIES;
	}


	public void setupForUpload() {
		getAccountingPositionTransferContext().reset();
		setUploadResultsString(null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferContext getAccountingPositionTransferContext() {
		return this.accountingPositionTransferContext;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getPriceDate() {
		return this.priceDate;
	}


	public void setPriceDate(Date priceDate) {
		this.priceDate = priceDate;
	}


	public AccountingPositionTransferType getType() {
		return this.type;
	}


	public void setType(AccountingPositionTransferType type) {
		this.type = type;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}
}
