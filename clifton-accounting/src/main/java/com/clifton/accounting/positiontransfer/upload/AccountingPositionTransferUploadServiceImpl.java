package com.clifton.accounting.positiontransfer.upload;

import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommand;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommandHandler;
import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author NickK
 */
@Service
public class AccountingPositionTransferUploadServiceImpl implements AccountingPositionTransferUploadService {

	private AccountingPositionTransferCommandHandler accountingPositionTransferCommandHandler;
	private AccountingPositionTransferService accountingPositionTransferService;
	private FileUploadHandler fileUploadHandler;

	////////////////////////////////////////////////////////////////////////////
	////////      AccountingPositionTransferUploadService Methods       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadAccountingPositionTransferFile(AccountingPositionTransferUploadCustomCommand uploadCommand) {
		List<AccountingPositionTransferCommand> uploadCommandList = getFileUploadHandler().convertFileUploadFileToBeanList(uploadCommand);

		// Clears all data maps for fresh upload
		uploadCommand.setupForUpload();

		try {
			for (AccountingPositionTransferCommand transferCommand : CollectionUtils.getIterable(uploadCommandList)) {
				// populate the type, security, and transaction date first so they can be used for the transfer key
				populateAccountingPositionTransferCommandDefaultsFromCustomUpload(uploadCommand, transferCommand);
				if (transferCommand.getTransactionDate() == null) {
					ValidationUtils.assertNotNull(uploadCommand.getTransactionDate(), "The transfer import contained a position transfer command row without a Transaction Date defined. " +
							"The Transaction Date can also be specified on the position transfer upload window.");
					transferCommand.setTransactionDate(uploadCommand.getTransactionDate());
				}
				if (transferCommand.getSettlementDate() == null) {
					ValidationUtils.assertNotNull(uploadCommand.getSettlementDate(), "The transfer import contained a position transfer command row without a Settlement Date defined. " +
							"The Settlement Date can also be specified on the position transfer upload window.");
					transferCommand.setSettlementDate(uploadCommand.getSettlementDate());
				}
				getAccountingPositionTransferCommandHandler().populateAccountingPositionTransferCommandSecurity(transferCommand, uploadCommand.getAccountingPositionTransferContext());
				getAccountingPositionTransferCommandHandler().generateAccountingPositionTransfer(transferCommand, false, uploadCommand.getAccountingPositionTransferContext());
			}
		}
		catch (ValidationException e) {
			// prefix the validation exception
			throw new ValidationException("Cannot process position transfer upload: " + e.getMessage(), e);
		}

		uploadCommand.appendResult(getAccountingPositionTransferCommandHandler().saveGeneratedAccountingPositionTransferList(uploadCommand.getAccountingPositionTransferContext()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates the transfer type to that specified the provided row details on the upload file or the
	 * type specified from the window/UI. If neither is defined, a ValidationException is thrown.
	 * <p>
	 * Other items from the upload are populated on the transferCommand as well, including: the note and price date.
	 */
	private void populateAccountingPositionTransferCommandDefaultsFromCustomUpload(AccountingPositionTransferUploadCustomCommand uploadCommand, AccountingPositionTransferCommand transferCommand) {
		AccountingPositionTransferType transferType = uploadCommand.getType();
		String transferTypeName = transferCommand.getTypeName();
		ValidationUtils.assertTrue(transferType != null || transferTypeName != null, "Accounting Position Transfer import has a row without a Transfer type defined.");
		transferCommand.setType(transferTypeName != null ? getAccountingPositionTransferService().getAccountingPositionTransferTypeByName(transferTypeName) : transferType);

		transferCommand.setNote(uploadCommand.getNote() == null ? transferCommand.getNote()
				: new StringBuilder(uploadCommand.getNote()).append(" - ").append(transferCommand.getNote() == null ? "" : transferCommand.getNote()).toString());

		if (transferCommand.getPriceDate() == null) {
			transferCommand.setPriceDate(uploadCommand.getPriceDate());
		}
	}

////////////////////////////////////////////////////////////////////////////
////////                 Getter and Setter Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferCommandHandler getAccountingPositionTransferCommandHandler() {
		return this.accountingPositionTransferCommandHandler;
	}


	public void setAccountingPositionTransferCommandHandler(AccountingPositionTransferCommandHandler accountingPositionTransferCommandHandler) {
		this.accountingPositionTransferCommandHandler = accountingPositionTransferCommandHandler;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public FileUploadHandler getFileUploadHandler() {
		return this.fileUploadHandler;
	}


	public void setFileUploadHandler(FileUploadHandler fileUploadHandler) {
		this.fileUploadHandler = fileUploadHandler;
	}
}
