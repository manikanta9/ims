package com.clifton.accounting.positiontransfer.instruction;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;


/**
 * @author manderson
 */
public class AccountingPositionTransferCounterpartyInstructionSelector extends BaseAccountingPositionTransferInstructionSelector implements ValidationAware {

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getHoldingAccountTypeId(), "Holding Account Type Selection is Required");
		InvestmentAccountType accountType = getInvestmentAccountService().getInvestmentAccountType(getHoldingAccountTypeId());
		ValidationUtils.assertFalse(StringUtils.isEqual(accountType.getName(), InvestmentAccountType.COLLATERAL) || StringUtils.isEqual(accountType.getName(), InvestmentAccountType.CUSTODIAN), "Invalid holding account type selection: " + accountType.getName() + ".");
	}


	@Override
	public BusinessCompany getRecipient(AccountingPositionTransfer entity) {
		// Note: This assumes that the from or to account is the Custodian account type (or Collateral if collateral transfer) ONLY
		if (entity.getFromHoldingInvestmentAccount() != null) {
			if (isCollateralTransfer()) {
				if (StringUtils.isEqual(InvestmentAccountType.COLLATERAL, entity.getFromHoldingInvestmentAccount().getType().getName())) {
					return entity.getFromHoldingInvestmentAccount().getIssuingCompany();
				}
				if (StringUtils.isEqual(InvestmentAccountType.TRI_PARTY_COLLATERAL, entity.getFromHoldingInvestmentAccount().getType().getName())) {
					return entity.getFromHoldingInvestmentAccount().getIssuingCompany();
				}
			}
			if (StringUtils.isEqual(InvestmentAccountType.CUSTODIAN, entity.getFromHoldingInvestmentAccount().getType().getName())) {
				return entity.getFromHoldingInvestmentAccount().getIssuingCompany();
			}
		}
		if (entity.getToHoldingInvestmentAccount() != null) {
			if (isCollateralTransfer()) {
				if (StringUtils.isEqual(InvestmentAccountType.COLLATERAL, entity.getToHoldingInvestmentAccount().getType().getName())) {
					return entity.getToHoldingInvestmentAccount().getIssuingCompany();
				}
				if (StringUtils.isEqual(InvestmentAccountType.TRI_PARTY_COLLATERAL, entity.getToHoldingInvestmentAccount().getType().getName())) {
					return entity.getToHoldingInvestmentAccount().getIssuingCompany();
				}
			}
			if (StringUtils.isEqual(InvestmentAccountType.CUSTODIAN, entity.getToHoldingInvestmentAccount().getType().getName())) {
				return entity.getToHoldingInvestmentAccount().getIssuingCompany();
			}
		}
		throw new ValidationException("Unable to Determine Custodian " + (isCollateralTransfer() ? "or Collateral " : "") + "account for Position Transfer Record: [" + entity.getLabel()
				+ "].  Unable to generate instructions without a custodian issuer to use as the recipient.");
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////            Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
