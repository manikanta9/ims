package com.clifton.accounting.positiontransfer.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AccountingInternalPositionTransferRealizedBookingRule</code> generates the Realized G/L for the opening position
 * equal this of the closing position.  The net is 0.
 * <p/>
 * Opening position must preserve correct OTE in order to reconcile with brokers and properly contribute gain loss at full position close.
 * The broker is not aware of this transfer.
 * <p/>
 * It is a very special case rule and should only be used for internal Transfers.
 */
public class AccountingInternalPositionTransferRealizedBookingRule extends AccountingRealizedGainLossBookingRule<AccountingPositionTransfer> {

	@Override
	public void applyRule(BookingSession<AccountingPositionTransfer> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		List<AccountingJournalDetailDefinition> detailRecords = new ArrayList<>(journal.getJournalDetailList()); //Copy to prevent concurrent modification

		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailRecords)) {
			AccountingAccount accountingAccount = detail.getAccountingAccount();
			if (accountingAccount.isGainLoss() && !MathUtils.isNullOrZero(detail.getBaseDebitCredit())) {
				// copy and make opposite
				AccountingJournalDetailDefinition openingGainLoss = new AccountingJournalDetail();
				BeanUtils.copyProperties(detail, openingGainLoss);
				openingGainLoss.setQuantity(openingGainLoss.getQuantity().negate());
				openingGainLoss.setLocalDebitCredit(openingGainLoss.getLocalDebitCredit().negate());
				openingGainLoss.setBaseDebitCredit(openingGainLoss.getBaseDebitCredit().negate());

				// find the opening position that corresponds to this close
				AccountingJournalDetailDefinition openingPosition = null;
				AccountingJournalDetailDefinition closingPosition = openingGainLoss.getParentDefinition();
				for (AccountingJournalDetailDefinition d : journal.getJournalDetailList()) {
					if (!d.equals(closingPosition) && d.getAccountingAccount().equals(closingPosition.getAccountingAccount())
							&& d.getInvestmentSecurity().equals(closingPosition.getInvestmentSecurity())
							&& MathUtils.isEqual(openingGainLoss.getLocalDebitCredit(), MathUtils.add(d.getPositionCostBasis(), closingPosition.getPositionCostBasis()))) {
						// make sure there are no 2 lots with identical numbers
						if (CollectionUtils.isEmpty(journal.findChildEntries(d))) {
							openingPosition = d;
							openingGainLoss.setClientInvestmentAccount(d.getClientInvestmentAccount());
							break;
						}
					}
				}
				if (openingPosition == null) {
					throw new ValidationException("Cannot find opening lot for Internal Position Transfer with closing: " + closingPosition);
				}
				// adjust cost basis to preserve what's been transferred
				openingPosition.setLocalDebitCredit(MathUtils.negate(closingPosition.getLocalDebitCredit()));
				openingPosition.setBaseDebitCredit(MathUtils.negate(closingPosition.getBaseDebitCredit()));
				openingPosition.setPositionCostBasis(MathUtils.negate(closingPosition.getPositionCostBasis()));
				openingGainLoss.setParentDefinition(openingPosition);

				journal.addJournalDetail(openingGainLoss);

				if (InvestmentUtils.isNoPaymentOnOpen(openingPosition.getInvestmentSecurity())) {
					// need to offset realized gain loss debit/credit (position has those as 0's) with a Contribution
					AccountingJournalDetail contribution = new AccountingJournalDetail();
					BeanUtils.copyProperties(openingGainLoss, contribution);
					contribution.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.EQUITY_SECURITY_CONTRIBUTION));
					contribution.setLocalDebitCredit(MathUtils.negate(contribution.getLocalDebitCredit()));
					contribution.setBaseDebitCredit(MathUtils.negate(contribution.getBaseDebitCredit()));
					contribution.setDescription("Gain/Loss contribution from transfer from " + bookingSession.getBookableEntity().getFromClientInvestmentAccount().getLabel());
					journal.addJournalDetail(contribution);
				}
			}
		}
	}
}
