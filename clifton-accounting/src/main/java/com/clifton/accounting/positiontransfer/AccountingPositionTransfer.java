package com.clifton.accounting.positiontransfer;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.rule.violation.RuleViolationAware;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The <code>AccountingPositionTransfer</code> class represent a transfer of position(s)/security(ies) from one account to another.
 * <p>
 * Three types of transfers are possible:
 * 1. From our account to external account
 * 2. To our account from external account
 * 3. Between two different our account
 * <p>
 * External account means an account not managed by our system (we do not know anything about it: no position information).
 * When an external account is part of a transfer, its client and holding accounts will be null's and note field will usually have additional details.
 *
 * @author vgomelsky
 */
public class AccountingPositionTransfer extends BaseEntity<Integer> implements BookableEntity, RuleViolationAware, SystemEntityModifyConditionAware {

	public static final String ACCOUNTING_POSITION_TRANSFER_TABLE_NAME = "AccountingPositionTransfer";

	private AccountingPositionTransferType type;

	private InvestmentAccount toClientInvestmentAccount;
	private InvestmentAccount toHoldingInvestmentAccount;

	private InvestmentAccount fromClientInvestmentAccount;
	private InvestmentAccount fromHoldingInvestmentAccount;

	/**
	 * Specifies whether "TO" accounts receive collateral
	 */
	private boolean collateralTransfer;
	/**
	 * Typically you always want to use the OriginalTransactionDate
	 */
	private boolean useOriginalTransactionDate = true;

	private String note;

	private RuleViolationStatus violationStatus;

	/**
	 * Date to reflect transfers as exposure on PIOS reports.
	 * Most cases this will be NULL and just use transfer date, however
	 * in some cases (Boeing accounts) the transfers from today should be reflected in the PIOS report from yesterday.
	 */
	private Date exposureDate;
	private Date transactionDate;
	private Date settlementDate;
	private Date bookingDate;

	/**
	 * Additional security condition that when populated must evaluate to true in order for a non admin user to be able to edit this transfer
	 */
	private SystemCondition entityModifyCondition;

	/**
	 * Source information for the transfer.
	 */
	private SystemTable sourceSystemTable;

	@SoftLinkField(tableBeanPropertyName = "sourceSystemTable")
	private Integer sourceFkFieldId;

	private List<AccountingPositionTransferDetail> detailList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(100);
		if (this.fromHoldingInvestmentAccount != null) {
			result.append("From ");
			result.append(this.fromHoldingInvestmentAccount.getLabel());
		}
		if (this.toHoldingInvestmentAccount != null) {
			if (result.length() > 0) {
				result.append(' ');
			}
			result.append("To ");
			result.append(this.toHoldingInvestmentAccount.getLabel());
		}
		result.append(" on ");
		result.append(DateUtils.fromDateShort(this.transactionDate));
		result.append(" settling on ");
		result.append(DateUtils.fromDateShort(this.settlementDate));
		return result.toString();
	}


	@Override
	public Set<IdentityObject> getEntityLockSet() {
		return CollectionUtils.createHashSet(getFromClientInvestmentAccount(), getToClientInvestmentAccount());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	@Override
	public RuleViolationStatus getViolationStatus() {
		return this.violationStatus;
	}


	@Override
	public void setViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
	}


	public InvestmentAccount getToClientInvestmentAccount() {
		return this.toClientInvestmentAccount;
	}


	public void setToClientInvestmentAccount(InvestmentAccount toClientInvestmentAccount) {
		this.toClientInvestmentAccount = toClientInvestmentAccount;
	}


	public InvestmentAccount getToHoldingInvestmentAccount() {
		return this.toHoldingInvestmentAccount;
	}


	public void setToHoldingInvestmentAccount(InvestmentAccount toHoldingInvestmentAccount) {
		this.toHoldingInvestmentAccount = toHoldingInvestmentAccount;
	}


	public InvestmentAccount getFromClientInvestmentAccount() {
		return this.fromClientInvestmentAccount;
	}


	public void setFromClientInvestmentAccount(InvestmentAccount fromClientInvestmentAccount) {
		this.fromClientInvestmentAccount = fromClientInvestmentAccount;
	}


	public InvestmentAccount getFromHoldingInvestmentAccount() {
		return this.fromHoldingInvestmentAccount;
	}


	public void setFromHoldingInvestmentAccount(InvestmentAccount fromHoldingInvestmentAccount) {
		this.fromHoldingInvestmentAccount = fromHoldingInvestmentAccount;
	}


	public List<AccountingPositionTransferDetail> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<AccountingPositionTransferDetail> detailList) {
		this.detailList = detailList;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	@Override
	public Date getBookingDate() {
		return this.bookingDate;
	}


	@Override
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public AccountingPositionTransferType getType() {
		return this.type;
	}


	public void setType(AccountingPositionTransferType type) {
		this.type = type;
	}


	public Date getExposureDate() {
		return this.exposureDate;
	}


	public void setExposureDate(Date exposureDate) {
		this.exposureDate = exposureDate;
	}


	public boolean isUseOriginalTransactionDate() {
		return this.useOriginalTransactionDate;
	}


	public void setUseOriginalTransactionDate(boolean useOriginalTransactionDate) {
		this.useOriginalTransactionDate = useOriginalTransactionDate;
	}


	public SystemTable getSourceSystemTable() {
		return this.sourceSystemTable;
	}


	public void setSourceSystemTable(SystemTable sourceSystemTable) {
		this.sourceSystemTable = sourceSystemTable;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public boolean isCollateralTransfer() {
		return this.collateralTransfer;
	}


	public void setCollateralTransfer(boolean collateralTransfer) {
		this.collateralTransfer = collateralTransfer;
	}
}
