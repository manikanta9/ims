package com.clifton.accounting.positiontransfer.generator;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommand;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;


/**
 * <code>AccountingPositionTransferGeneratorService</code> generates {@link AccountingPositionTransfer}s.
 *
 * @author NickK
 */
public interface AccountingPositionTransferGeneratorService {


	/**
	 * Generates {@link AccountingPositionTransfer}(s) for the provided transferCommand.
	 */
	@SecureMethod(dtoClass = AccountingPositionTransfer.class, permissions = SecurityPermission.PERMISSION_CREATE)
	public void generateAccountingPositionTransferList(AccountingPositionTransferCommand transferCommand);
}
