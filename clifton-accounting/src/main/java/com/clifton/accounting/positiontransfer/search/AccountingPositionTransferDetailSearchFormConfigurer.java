package com.clifton.accounting.positiontransfer.search;

import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.setup.group.InvestmentGroupItemInstrument;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;


/**
 * Search configurer for {@link AccountingPositionTransferDetail} queries.
 *
 * @author MikeH
 */
public class AccountingPositionTransferDetailSearchFormConfigurer extends HibernateSearchFormConfigurer {

	public AccountingPositionTransferDetailSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		super(searchForm);
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		AccountingPositionTransferDetailSearchForm searchForm = (AccountingPositionTransferDetailSearchForm) getSortableSearchForm();
		if (searchForm.getInvestmentGroupId() != null) {
			// Filter on instrument group ID if specified
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentGroupItemInstrument.class, "igii");

			// Create link to specify instrument group ID
			sub.setProjection(Projections.id());
			sub.createAlias("referenceOne", "groupItem");
			sub.add(Restrictions.eq("groupItem.group.id", searchForm.getInvestmentGroupId()));

			// Create link from sub-query to outer-query
			sub.createAlias("referenceTwo", "instrument");
			sub.createAlias("instrument.securityList", "security");
			sub.add(Restrictions.eqProperty("security.id", criteria.getAlias() + ".security.id"));

			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getClientInvestmentAccountGroupId() != null) {
			// Filter on client account group ID if specified
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "iaga");

			// Create link to specify client account group ID
			sub.setProjection(Projections.id());
			sub.add(Restrictions.eq("referenceOne.id", searchForm.getClientInvestmentAccountGroupId()));

			// Create link from sub-query to outer-query
			sub.add(Restrictions.or(
					Restrictions.eqProperty("referenceTwo.id", getPathAlias("positionTransfer", criteria) + ".toClientInvestmentAccount"),
					Restrictions.eqProperty("referenceTwo.id", getPathAlias("positionTransfer", criteria) + ".fromClientInvestmentAccount")
			));

			criteria.add(Subqueries.exists(sub));
		}
	}
}
