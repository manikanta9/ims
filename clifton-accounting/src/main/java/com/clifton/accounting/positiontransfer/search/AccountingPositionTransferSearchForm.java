package com.clifton.accounting.positiontransfer.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>AccountingPositionTransferSearchForm</code> class defines search configuration for AccountingPositionTransfer objects.
 *
 * @author vgomelsky
 */
public class AccountingPositionTransferSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String transferTypeName;

	@SearchField(searchField = "type.id")
	private Short transferTypeId;

	@SearchField(searchField = "type.id")
	private Short[] transferTypeIds;

	@SearchField(searchFieldPath = "type", searchField = "cash")
	private Boolean transferTypeCash;

	@SearchField(searchFieldPath = "type", searchField = "collateral")
	private Boolean transferTypeCollateral;

	@SearchField(searchField = "toClientInvestmentAccount.id")
	private Integer toClientInvestmentAccountId;

	@SearchField(searchField = "toClientInvestmentAccount.id")
	private Integer[] toClientInvestmentAccountIds;

	@SearchField(searchField = "toHoldingInvestmentAccount.id")
	private Integer toHoldingInvestmentAccountId;

	@SearchField(searchField = "toHoldingInvestmentAccount.id")
	private Integer[] toHoldingInvestmentAccountIds;

	@SearchField(searchField = "fromClientInvestmentAccount.id")
	private Integer fromClientInvestmentAccountId;

	@SearchField(searchField = "fromClientInvestmentAccount.id")
	private Integer[] fromClientInvestmentAccountIds;

	@SearchField(searchField = "fromHoldingInvestmentAccount.id")
	private Integer fromHoldingInvestmentAccountId;

	@SearchField(searchField = "fromHoldingInvestmentAccount.id")
	private Integer[] fromHoldingInvestmentAccountIds;

	@SearchField(searchFieldPath = "fromHoldingInvestmentAccount", searchField = "type.id")
	private Short fromHoldingInvestmentAccountTypeId;

	@SearchField(searchFieldPath = "fromHoldingInvestmentAccount", searchField = "issuingCompany.id")
	private Integer fromHoldingInvestmentAccountIssuerId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "fromHoldingInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer fromHoldingInvestmentAccountGroupId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "fromHoldingInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer[] fromHoldingInvestmentAccountGroupIds;

	// Custom Search Field - Not Exists
	private Integer fromExcludeHoldingInvestmentAccountGroupId;

	// Custom Search Field - Not Exists
	private Integer[] fromExcludeHoldingInvestmentAccountGroupIds;

	@SearchField(searchFieldPath = "toHoldingInvestmentAccount", searchField = "type.id")
	private Short toHoldingInvestmentAccountTypeId;

	@SearchField(searchFieldPath = "toHoldingInvestmentAccount", searchField = "issuingCompany.id")
	private Integer toHoldingInvestmentAccountIssuerId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "toHoldingInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer toHoldingInvestmentAccountGroupId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "toHoldingInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer[] toHoldingInvestmentAccountGroupIds;

	// Custom Search Field - Not Exists
	private Integer toExcludeHoldingInvestmentAccountGroupId;

	// Custom Search Field - Not Exists
	private Integer[] toExcludeHoldingInvestmentAccountGroupIds;

	@SearchField(searchField = "toClientInvestmentAccount.id,fromClientInvestmentAccount.id", searchFieldCustomType = SearchFieldCustomTypes.OR, leftJoin = true)
	private Integer fromOrToClientInvestmentAccountId;

	@SearchField(searchField = "toHoldingInvestmentAccount.id,fromHoldingInvestmentAccount.id", searchFieldCustomType = SearchFieldCustomTypes.OR, leftJoin = true)
	private Integer fromOrToHoldingInvestmentAccountId;

	@SearchField
	private Boolean collateralTransfer;

	@SearchField
	private String note;

	@SearchField
	private Date exposureDate;

	@SearchField(searchField = "exposureDate", comparisonConditions = {ComparisonConditions.LESS_THAN_OR_EQUALS})
	private Date maxExposureDate;

	@SearchField(searchField = "transactionDate", comparisonConditions = {ComparisonConditions.GREATER_THAN_OR_EQUALS})
	private Date minTransactionDate;

	@SearchField
	private Date transactionDate;

	@SearchField
	private Date settlementDate;

	@SearchField
	private Date bookingDate;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean journalBooked;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean journalNotBooked;

	@SearchField(searchField = "sourceSystemTable.id")
	private Short sourceSystemTableId;

	@SearchField(searchField = "name", searchFieldPath = "sourceSystemTable")
	private String sourceSystemTableName;

	@SearchField(searchField = "name", searchFieldPath = "sourceSystemTable", comparisonConditions = ComparisonConditions.EQUALS)
	private String sourceSystemTableNameEquals;

	@SearchField
	private Integer sourceFkFieldId;

	@SearchField(searchField = "sourceFkFieldId", comparisonConditions = ComparisonConditions.IN)
	private Integer[] sourceFkFieldIdList;

	@SearchField(searchFieldPath = "violationStatus", searchField = "name")
	private String[] violationStatusNames;

	// Custom search filters
	private Short investmentGroupId;
	private Integer clientInvestmentAccountGroupId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getTransferTypeName() {
		return this.transferTypeName;
	}


	public void setTransferTypeName(String transferTypeName) {
		this.transferTypeName = transferTypeName;
	}


	public Short getTransferTypeId() {
		return this.transferTypeId;
	}


	public void setTransferTypeId(Short transferTypeId) {
		this.transferTypeId = transferTypeId;
	}


	public Short[] getTransferTypeIds() {
		return this.transferTypeIds;
	}


	public void setTransferTypeIds(Short[] transferTypeIds) {
		this.transferTypeIds = transferTypeIds;
	}


	public Boolean getTransferTypeCash() {
		return this.transferTypeCash;
	}


	public void setTransferTypeCash(Boolean transferTypeCash) {
		this.transferTypeCash = transferTypeCash;
	}


	public Boolean getTransferTypeCollateral() {
		return this.transferTypeCollateral;
	}


	public void setTransferTypeCollateral(Boolean transferTypeCollateral) {
		this.transferTypeCollateral = transferTypeCollateral;
	}


	public Integer getToClientInvestmentAccountId() {
		return this.toClientInvestmentAccountId;
	}


	public void setToClientInvestmentAccountId(Integer toClientInvestmentAccountId) {
		this.toClientInvestmentAccountId = toClientInvestmentAccountId;
	}


	public Integer[] getToClientInvestmentAccountIds() {
		return this.toClientInvestmentAccountIds;
	}


	public void setToClientInvestmentAccountIds(Integer[] toClientInvestmentAccountIds) {
		this.toClientInvestmentAccountIds = toClientInvestmentAccountIds;
	}


	public Integer getToHoldingInvestmentAccountId() {
		return this.toHoldingInvestmentAccountId;
	}


	public void setToHoldingInvestmentAccountId(Integer toHoldingInvestmentAccountId) {
		this.toHoldingInvestmentAccountId = toHoldingInvestmentAccountId;
	}


	public Integer[] getToHoldingInvestmentAccountIds() {
		return this.toHoldingInvestmentAccountIds;
	}


	public void setToHoldingInvestmentAccountIds(Integer[] toHoldingInvestmentAccountIds) {
		this.toHoldingInvestmentAccountIds = toHoldingInvestmentAccountIds;
	}


	public Integer getFromClientInvestmentAccountId() {
		return this.fromClientInvestmentAccountId;
	}


	public void setFromClientInvestmentAccountId(Integer fromClientInvestmentAccountId) {
		this.fromClientInvestmentAccountId = fromClientInvestmentAccountId;
	}


	public Integer[] getFromClientInvestmentAccountIds() {
		return this.fromClientInvestmentAccountIds;
	}


	public void setFromClientInvestmentAccountIds(Integer[] fromClientInvestmentAccountIds) {
		this.fromClientInvestmentAccountIds = fromClientInvestmentAccountIds;
	}


	public Integer getFromHoldingInvestmentAccountId() {
		return this.fromHoldingInvestmentAccountId;
	}


	public void setFromHoldingInvestmentAccountId(Integer fromHoldingInvestmentAccountId) {
		this.fromHoldingInvestmentAccountId = fromHoldingInvestmentAccountId;
	}


	public Integer[] getFromHoldingInvestmentAccountIds() {
		return this.fromHoldingInvestmentAccountIds;
	}


	public void setFromHoldingInvestmentAccountIds(Integer[] fromHoldingInvestmentAccountIds) {
		this.fromHoldingInvestmentAccountIds = fromHoldingInvestmentAccountIds;
	}


	public Short getFromHoldingInvestmentAccountTypeId() {
		return this.fromHoldingInvestmentAccountTypeId;
	}


	public void setFromHoldingInvestmentAccountTypeId(Short fromHoldingInvestmentAccountTypeId) {
		this.fromHoldingInvestmentAccountTypeId = fromHoldingInvestmentAccountTypeId;
	}


	public Integer getFromHoldingInvestmentAccountIssuerId() {
		return this.fromHoldingInvestmentAccountIssuerId;
	}


	public void setFromHoldingInvestmentAccountIssuerId(Integer fromHoldingInvestmentAccountIssuerId) {
		this.fromHoldingInvestmentAccountIssuerId = fromHoldingInvestmentAccountIssuerId;
	}


	public Integer getFromHoldingInvestmentAccountGroupId() {
		return this.fromHoldingInvestmentAccountGroupId;
	}


	public void setFromHoldingInvestmentAccountGroupId(Integer fromHoldingInvestmentAccountGroupId) {
		this.fromHoldingInvestmentAccountGroupId = fromHoldingInvestmentAccountGroupId;
	}


	public Integer[] getFromHoldingInvestmentAccountGroupIds() {
		return this.fromHoldingInvestmentAccountGroupIds;
	}


	public void setFromHoldingInvestmentAccountGroupIds(Integer[] fromHoldingInvestmentAccountGroupIds) {
		this.fromHoldingInvestmentAccountGroupIds = fromHoldingInvestmentAccountGroupIds;
	}


	public Integer getFromExcludeHoldingInvestmentAccountGroupId() {
		return this.fromExcludeHoldingInvestmentAccountGroupId;
	}


	public void setFromExcludeHoldingInvestmentAccountGroupId(Integer fromExcludeHoldingInvestmentAccountGroupId) {
		this.fromExcludeHoldingInvestmentAccountGroupId = fromExcludeHoldingInvestmentAccountGroupId;
	}


	public Integer[] getFromExcludeHoldingInvestmentAccountGroupIds() {
		return this.fromExcludeHoldingInvestmentAccountGroupIds;
	}


	public void setFromExcludeHoldingInvestmentAccountGroupIds(Integer[] fromExcludeHoldingInvestmentAccountGroupIds) {
		this.fromExcludeHoldingInvestmentAccountGroupIds = fromExcludeHoldingInvestmentAccountGroupIds;
	}


	public Short getToHoldingInvestmentAccountTypeId() {
		return this.toHoldingInvestmentAccountTypeId;
	}


	public void setToHoldingInvestmentAccountTypeId(Short toHoldingInvestmentAccountTypeId) {
		this.toHoldingInvestmentAccountTypeId = toHoldingInvestmentAccountTypeId;
	}


	public Integer getToHoldingInvestmentAccountIssuerId() {
		return this.toHoldingInvestmentAccountIssuerId;
	}


	public void setToHoldingInvestmentAccountIssuerId(Integer toHoldingInvestmentAccountIssuerId) {
		this.toHoldingInvestmentAccountIssuerId = toHoldingInvestmentAccountIssuerId;
	}


	public Integer getToHoldingInvestmentAccountGroupId() {
		return this.toHoldingInvestmentAccountGroupId;
	}


	public void setToHoldingInvestmentAccountGroupId(Integer toHoldingInvestmentAccountGroupId) {
		this.toHoldingInvestmentAccountGroupId = toHoldingInvestmentAccountGroupId;
	}


	public Integer[] getToHoldingInvestmentAccountGroupIds() {
		return this.toHoldingInvestmentAccountGroupIds;
	}


	public void setToHoldingInvestmentAccountGroupIds(Integer[] toHoldingInvestmentAccountGroupIds) {
		this.toHoldingInvestmentAccountGroupIds = toHoldingInvestmentAccountGroupIds;
	}


	public Integer getToExcludeHoldingInvestmentAccountGroupId() {
		return this.toExcludeHoldingInvestmentAccountGroupId;
	}


	public void setToExcludeHoldingInvestmentAccountGroupId(Integer toExcludeHoldingInvestmentAccountGroupId) {
		this.toExcludeHoldingInvestmentAccountGroupId = toExcludeHoldingInvestmentAccountGroupId;
	}


	public Integer[] getToExcludeHoldingInvestmentAccountGroupIds() {
		return this.toExcludeHoldingInvestmentAccountGroupIds;
	}


	public void setToExcludeHoldingInvestmentAccountGroupIds(Integer[] toExcludeHoldingInvestmentAccountGroupIds) {
		this.toExcludeHoldingInvestmentAccountGroupIds = toExcludeHoldingInvestmentAccountGroupIds;
	}


	public Integer getFromOrToClientInvestmentAccountId() {
		return this.fromOrToClientInvestmentAccountId;
	}


	public void setFromOrToClientInvestmentAccountId(Integer fromOrToClientInvestmentAccountId) {
		this.fromOrToClientInvestmentAccountId = fromOrToClientInvestmentAccountId;
	}


	public Integer getFromOrToHoldingInvestmentAccountId() {
		return this.fromOrToHoldingInvestmentAccountId;
	}


	public void setFromOrToHoldingInvestmentAccountId(Integer fromOrToHoldingInvestmentAccountId) {
		this.fromOrToHoldingInvestmentAccountId = fromOrToHoldingInvestmentAccountId;
	}


	public Boolean getCollateralTransfer() {
		return this.collateralTransfer;
	}


	public void setCollateralTransfer(Boolean collateralTransfer) {
		this.collateralTransfer = collateralTransfer;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Date getExposureDate() {
		return this.exposureDate;
	}


	public void setExposureDate(Date exposureDate) {
		this.exposureDate = exposureDate;
	}


	public Date getMaxExposureDate() {
		return this.maxExposureDate;
	}


	public void setMaxExposureDate(Date maxExposureDate) {
		this.maxExposureDate = maxExposureDate;
	}


	public Date getMinTransactionDate() {
		return this.minTransactionDate;
	}


	public void setMinTransactionDate(Date minTransactionDate) {
		this.minTransactionDate = minTransactionDate;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Boolean getJournalBooked() {
		return this.journalBooked;
	}


	public void setJournalBooked(Boolean journalBooked) {
		this.journalBooked = journalBooked;
	}


	public Boolean getJournalNotBooked() {
		return this.journalNotBooked;
	}


	public void setJournalNotBooked(Boolean journalNotBooked) {
		this.journalNotBooked = journalNotBooked;
	}


	public Short getSourceSystemTableId() {
		return this.sourceSystemTableId;
	}


	public void setSourceSystemTableId(Short sourceSystemTableId) {
		this.sourceSystemTableId = sourceSystemTableId;
	}


	public String getSourceSystemTableName() {
		return this.sourceSystemTableName;
	}


	public void setSourceSystemTableName(String sourceSystemTableName) {
		this.sourceSystemTableName = sourceSystemTableName;
	}


	public String getSourceSystemTableNameEquals() {
		return this.sourceSystemTableNameEquals;
	}


	public void setSourceSystemTableNameEquals(String sourceSystemTableNameEquals) {
		this.sourceSystemTableNameEquals = sourceSystemTableNameEquals;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public Integer[] getSourceFkFieldIdList() {
		return this.sourceFkFieldIdList;
	}


	public void setSourceFkFieldIdList(Integer[] sourceFkFieldIdList) {
		this.sourceFkFieldIdList = sourceFkFieldIdList;
	}


	public String[] getViolationStatusNames() {
		return this.violationStatusNames;
	}


	public void setViolationStatusNames(String[] violationStatusNames) {
		this.violationStatusNames = violationStatusNames;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}
}
