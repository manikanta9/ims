package com.clifton.accounting.positiontransfer;


import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferDetailSearchForm;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferDetailSearchFormConfigurer;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchFormConfigurer;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferTypeSearchForm;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.event.EventObject;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationAwareObserver;
import com.clifton.rule.violation.RuleViolationUtil;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingPositionTransferServiceImpl</code> class provides basic implementation of AccountingPositionTransferService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingPositionTransferServiceImpl implements AccountingPositionTransferService {

	public static final String RULE_CATEGORY_TRANSFER = "Transfer Rules";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private AdvancedUpdatableDAO<AccountingPositionTransfer, Criteria> accountingPositionTransferDAO;
	private AdvancedUpdatableDAO<AccountingPositionTransferDetail, Criteria> accountingPositionTransferDetailDAO;
	private AdvancedReadOnlyDAO<AccountingPositionTransferType, Criteria> accountingPositionTransferTypeDAO;

	private EventHandler eventHandler;

	private InvestmentCalculator investmentCalculator;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private RuleDefinitionService ruleDefinitionService;
	private RuleEvaluatorService ruleEvaluatorService;
	private RuleViolationStatusService ruleViolationStatusService;

	private DaoNamedEntityCache<AccountingPositionTransferType> accountingPositionTransferTypeCache;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true) // improve performance by avoiding second transaction creation and using 1st level cache
	public AccountingPositionTransfer getAccountingPositionTransfer(int id) {
		AccountingPositionTransfer transfer = getAccountingPositionTransferDAO().findByPrimaryKey(id);
		if (transfer != null) {
			transfer.setDetailList(getAccountingPositionTransferDetailDAO().findByField("positionTransfer.id", id));
		}
		return transfer;
	}


	@Override
	public AccountingPositionTransferDetail getAccountingPositionTransferDetail(int id) {
		return getAccountingPositionTransferDetailDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingPositionTransfer> getAccountingPositionTransferList(AccountingPositionTransferSearchForm searchForm) {
		return getAccountingPositionTransferDAO().findBySearchCriteria(new AccountingPositionTransferSearchFormConfigurer(searchForm));
	}


	@Override
	public List<AccountingPositionTransferDetail> getAccountingPositionTransferDetailList(AccountingPositionTransferDetailSearchForm searchForm) {
		return getAccountingPositionTransferDetailDAO().findBySearchCriteria(new AccountingPositionTransferDetailSearchFormConfigurer(searchForm));
	}


	@Override
	public void saveAccountingPositionTransfer(final AccountingPositionTransfer transfer) {
		saveAccountingPositionTransferImpl(transfer, true);
	}


	@Transactional
	protected void saveAccountingPositionTransferImpl(final AccountingPositionTransfer transfer, boolean updateDetails) {
		// 1. validate transfer (including comparison against previous values)
		boolean insertNewTransfer = transfer.isNewBean();
		AccountingPositionTransfer oldTransfer = null;
		if (!insertNewTransfer && updateDetails) {
			oldTransfer = DaoUtils.executeDetached(true, () -> getAccountingPositionTransfer(transfer.getId()));
		}
		validateAccountingPositionTransfer(transfer, updateDetails);

		// 2. If it's a new transfer, need to create first so that rule violations have linked FK field ID
		List<AccountingPositionTransferDetail> detailList = transfer.getDetailList();
		AccountingPositionTransfer updatedTransfer = transfer;
		if (insertNewTransfer) {
			// check collateral flag when transferring a position to collateral
			if (transfer.getType() != null && transfer.getType().isCollateralPosting() && !transfer.getType().isCash()) {
				// com.clifton.accounting.positiontransfer.booking.AccountingPositionTransferBookingRule.getJournalDetailAccountingAccount
				ValidationUtils.assertTrue(transfer.isCollateralTransfer(), "'\"To\" account receives collateral positions' should be selected when using 'Client Collateral Transfer - Post' or 'Counterparty Collateral Transfer - Post'.");
			}
			// do not run RuleViolationAwareObserver to avoid duplicate processing that is done in this method
			transfer.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS));
			updatedTransfer = DaoUtils.executeWithSpecificObserversDisabled(() -> getAccountingPositionTransferDAO().save(transfer), RuleViolationAwareObserver.class);
			updatedTransfer.setDetailList(detailList);
		}

		// 3. execute rules and update transfer violation status only if changed
		RuleCategory ruleCategory = getRuleDefinitionService().getRuleCategoryByName(RULE_CATEGORY_TRANSFER);
		List<RuleViolation> violationList = getRuleEvaluatorService().executeRules(ruleCategory, updatedTransfer, null);
		RuleViolationStatus.RuleViolationStatusNames newStatus = RuleViolationUtil.getRuleViolationStatus(violationList);
		if (!insertNewTransfer || !newStatus.name().equals(updatedTransfer.getViolationStatus().getName())) {
			// do not run RuleViolationAwareObserver to avoid duplicate processing: already have all results
			updatedTransfer.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(newStatus));
			final AccountingPositionTransfer latestTransfer = updatedTransfer;
			updatedTransfer = DaoUtils.executeWithSpecificObserversDisabled(() -> getAccountingPositionTransferDAO().save(latestTransfer), RuleViolationAwareObserver.class);
			updatedTransfer.setDetailList(detailList);
		}

		// 4. insert or update transfer details
		if (updateDetails) {
			for (AccountingPositionTransferDetail detail : CollectionUtils.getIterable(detailList)) {
				detail.setPositionTransfer(updatedTransfer);
			}
			getAccountingPositionTransferDetailDAO().saveList(detailList, oldTransfer == null ? null : oldTransfer.getDetailList());
		}

		// 5. notify any listeners that a position transfer has been saved.
		getEventHandler().raiseEvent(EventObject.ofEventTarget(ACCOUNTING_POSITION_TRANSFER_SAVED_EVENT_NAME, updatedTransfer));
	}


	@Override
	@Transactional
	public void deleteAccountingPositionTransfer(int id) {
		AccountingPositionTransfer transfer = getAccountingPositionTransfer(id);

		ValidationUtils.assertNotNull(transfer, "Cannot find position transfer with id = " + id);
		ValidationUtils.assertNull(transfer.getBookingDate(), "Cannot delete position transfer: " + transfer.getLabel() + " that has been booked.");

		getAccountingPositionTransferDetailDAO().deleteList(transfer.getDetailList());
		getAccountingPositionTransferDAO().delete(transfer);

		// notify any listeners that a position transfer has been deleted.
		getEventHandler().raiseEvent(EventObject.ofEventTarget(ACCOUNTING_POSITION_TRANSFER_DELETED_EVENT_NAME, transfer));
	}


	////////////////////////////////////////////////////////////////////////////
	////////                Upload Save Methods                      ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void saveAccountingPositionTransferFromUpload(final AccountingPositionTransfer bean) {
		// Each Detail is Validated and Saved separately: this method just validates the actual transfer bean
		saveAccountingPositionTransferImpl(bean, false);
	}


	@Override
	public void saveAccountingPositionTransferDetailFromUpload(AccountingPositionTransferDetail bean) {
		validateAccountingPositionTransferDetail(bean);
		getAccountingPositionTransferDetailDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingPositionTransferType getAccountingPositionTransferType(short id) {
		return getAccountingPositionTransferTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingPositionTransferType> getAccountingPositionTransferTypeList() {
		return getAccountingPositionTransferTypeDAO().findAll();
	}


	@Override
	public List<AccountingPositionTransferType> getAccountingPositionTransferTypeList(AccountingPositionTransferTypeSearchForm searchForm) {
		return getAccountingPositionTransferTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingPositionTransferType getAccountingPositionTransferTypeByName(String name) {
		return getAccountingPositionTransferTypeCache().getBeanForKeyValueStrict(getAccountingPositionTransferTypeDAO(), name);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateAccountingPositionTransfer(AccountingPositionTransfer transfer, boolean validateDetails) {
		// marking transfer as booked is executed without this validation
		if (transfer.getBookingDate() != null) {
			throw new ValidationException("Cannot modify a position transfer that has been booked.");
		}

		// Exposure Date can be less than transaction date - if the same - clear it, else throw an exception
		if (transfer.getExposureDate() != null && DateUtils.compare(transfer.getExposureDate(), transfer.getTransactionDate(), false) >= 0) {
			if (DateUtils.compare(transfer.getExposureDate(), transfer.getTransactionDate(), false) == 0) {
				transfer.setExposureDate(null);
			}
			else {
				throw new FieldValidationException("Price Date cannot be after Transaction Date.  Only set price date when before transaction date.");
			}
		}

		if (transfer.getType().isFromOurAccount() && transfer.isCollateralTransfer()) {
			throw new FieldValidationException("Collateral transfer can only be specified for the TO accounts", "collateralTransfer");
		}
		if (transfer.getType().isInternalTransfer()) {
			ValidationUtils.assertTrue((transfer.getFromHoldingInvestmentAccount() != null) && (transfer.getToHoldingInvestmentAccount() != null)
					&& transfer.getFromHoldingInvestmentAccount().equals(transfer.getToHoldingInvestmentAccount()), new StringBuilder().append("Invalid internal transfer.  The FROM holding account [").append(transfer.getFromHoldingInvestmentAccount().getLabel()).append("] is not equal to the TO [").append(transfer.getToHoldingInvestmentAccount().getLabel()).append("].").toString());
		}
		if (transfer.getSourceFkFieldId() != null) {
			ValidationUtils.assertNotNull(transfer.getSourceSystemTable(), "Source Table is required for transfers when Source FK Field is specified.");
		}
		if (transfer.getType().isCollateral() && !transfer.getType().isCash()) {
			ValidationUtils.assertTrue(transfer.isUseOriginalTransactionDate(), "Collateral position transfers require the use of original transaction date.", "useOriginalTransactionDate");
		}

		if (validateDetails) {
			ValidationUtils.assertNotEmpty(transfer.getDetailList(), "Cannot have position transfer: " + transfer.getLabel() + " that has no details.");

			// at this time we only support transfer of securities in the same hierarchy denominated in same currency (hierarchy specific booking rule sets)
			InvestmentInstrumentHierarchy transferHierarchy = null;
			InvestmentSecurity ccyDenomination = null;
			for (AccountingPositionTransferDetail detail : CollectionUtils.getIterable(transfer.getDetailList())) {
				detail.setPositionTransfer(transfer);
				validateAccountingPositionTransferDetail(detail);

				InvestmentSecurity transferSecurity = (detail.getExistingPosition() == null) ? detail.getSecurity() : detail.getExistingPosition().getInvestmentSecurity();
				if (transferHierarchy == null) {
					transferHierarchy = transferSecurity.getInstrument().getHierarchy();
				}
				else {
					ValidationUtils.assertTrue(transferHierarchy.equals(transferSecurity.getInstrument().getHierarchy()), "A transfer can only have securities from the same hierarchy: "
							+ transferSecurity.getSymbol() + " must be under " + transferHierarchy.getLabelExpanded());
				}
				if (ccyDenomination == null) {
					ccyDenomination = transferSecurity.getInstrument().getTradingCurrency();
				}
				else {
					ValidationUtils.assertTrue(ccyDenomination.equals(transferSecurity.getInstrument().getTradingCurrency()),
							"A transfer can only have securities with the same currency denomination: " + transferSecurity.getSymbol() + " must be denominated in " + ccyDenomination.getSymbol());
				}
			}
		}
	}


	private void validateAccountingPositionTransferDetail(AccountingPositionTransferDetail detail) {
		// make sure quantity is the same sign and no higher than transfered position
		if (detail.getExistingPosition() != null) {
			// currencies don't have quantity
			if (detail.getExistingPosition().getQuantity() != null && detail.getQuantity() != null) {
				if (detail.getExistingPosition().getQuantity().signum() != detail.getQuantity().signum()) {
					throw new FieldValidationException("Quantity transfered: " + detail.getQuantity() + " must have the same sign as this of position being transfered: "
							+ detail.getExistingPosition().getQuantity(), "quantity");
				}
				if (detail.getExistingPosition().getQuantity().abs().compareTo(detail.getQuantity().abs()) < 0) {
					throw new FieldValidationException("Quantity transfered: " + detail.getQuantity() + " cannot be greater than position quantity: " + detail.getExistingPosition().getQuantity(),
							"quantity");
				}
			}
		}
		else {
			ValidationUtils.assertNotNull(detail.getExchangeRateToBase(), "FX Rate is required for each transfer detail.", "exchangeRateToBase");
		}

		// require original face field populated for asset backed securities
		if (detail.getOriginalFace() == null) {
			if (getInvestmentSecurityEventService().isInvestmentSecurityEventTypeAllowedForSecurity(detail.getSecurity().getId(), InvestmentSecurityEventType.FACTOR_CHANGE)) {
				throw new FieldValidationException(InvestmentUtils.getUnadjustedQuantityFieldName(detail.getSecurity()) + " is required for securities in hierarchies that support Factor Change event: " + detail.getSecurity().getSymbol(), "originalFace");
			}
		}
		else {
			if (!getInvestmentSecurityEventService().isInvestmentSecurityEventTypeAllowedForSecurity(detail.getSecurity().getId(), InvestmentSecurityEventType.FACTOR_CHANGE)) {
				throw new FieldValidationException("Original Face CANNOT be set for securities in hierarchies that DO NOT support Factor Change event: " + detail.getSecurity().getSymbol(), "originalFace");
			}
		}

		if (detail.getPositionCostBasis() == null) {
			// update cost basis is null - UI currently calculates it and allows overrides. So shouldn't be null in these cases
			// however uploads don't set the field and rely on the system to calculate it
			Date date = ObjectUtils.coalesce(detail.getPositionTransfer().getExposureDate(), detail.getPositionTransfer().getTransactionDate());
			detail.setPositionCostBasis(getInvestmentCalculator().calculateNotional(detail.getSecurity(), detail.getTransferPrice(), detail.getQuantity(), date));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleEvaluatorService getRuleEvaluatorService() {
		return this.ruleEvaluatorService;
	}


	public void setRuleEvaluatorService(RuleEvaluatorService ruleEvaluatorService) {
		this.ruleEvaluatorService = ruleEvaluatorService;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}


	public AdvancedUpdatableDAO<AccountingPositionTransfer, Criteria> getAccountingPositionTransferDAO() {
		return this.accountingPositionTransferDAO;
	}


	public void setAccountingPositionTransferDAO(AdvancedUpdatableDAO<AccountingPositionTransfer, Criteria> accountingPositionTransferDAO) {
		this.accountingPositionTransferDAO = accountingPositionTransferDAO;
	}


	public AdvancedUpdatableDAO<AccountingPositionTransferDetail, Criteria> getAccountingPositionTransferDetailDAO() {
		return this.accountingPositionTransferDetailDAO;
	}


	public void setAccountingPositionTransferDetailDAO(AdvancedUpdatableDAO<AccountingPositionTransferDetail, Criteria> accountingPositionTransferDetailDAO) {
		this.accountingPositionTransferDetailDAO = accountingPositionTransferDetailDAO;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public DaoNamedEntityCache<AccountingPositionTransferType> getAccountingPositionTransferTypeCache() {
		return this.accountingPositionTransferTypeCache;
	}


	public void setAccountingPositionTransferTypeCache(DaoNamedEntityCache<AccountingPositionTransferType> accountingPositionTransferTypeCache) {
		this.accountingPositionTransferTypeCache = accountingPositionTransferTypeCache;
	}


	public AdvancedReadOnlyDAO<AccountingPositionTransferType, Criteria> getAccountingPositionTransferTypeDAO() {
		return this.accountingPositionTransferTypeDAO;
	}


	public void setAccountingPositionTransferTypeDAO(AdvancedReadOnlyDAO<AccountingPositionTransferType, Criteria> accountingPositionTransferTypeDAO) {
		this.accountingPositionTransferTypeDAO = accountingPositionTransferTypeDAO;
	}
}
