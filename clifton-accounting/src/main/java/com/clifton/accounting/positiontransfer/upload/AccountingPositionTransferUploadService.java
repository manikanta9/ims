package com.clifton.accounting.positiontransfer.upload;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.security.authorization.SecureMethod;


/**
 * <code>AccountingPositionTransferUploadService</code> facilitates the upload of position transfers with details.
 *
 * @author NickK
 */
public interface AccountingPositionTransferUploadService {

	/**
	 * Custom upload to populate {@link AccountingPositionTransferDetail} beans and then process
	 * those beans to create actual full {@link AccountingPositionTransfer} objects to save.
	 */
	@SecureMethod(dtoClass = AccountingPositionTransfer.class)
	public void uploadAccountingPositionTransferFile(AccountingPositionTransferUploadCustomCommand uploadCommand);
}
