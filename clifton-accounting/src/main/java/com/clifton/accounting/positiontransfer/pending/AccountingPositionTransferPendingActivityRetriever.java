package com.clifton.accounting.positiontransfer.pending;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookingPreviewCommand;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.pending.AccountingPendingActivityRetriever;
import com.clifton.accounting.gl.search.GeneralLedgerSearchForm;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferDetailSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;


/**
 * Retrieves a list of pending transactions for all unbooked position transfers.
 *
 * @author mwacker
 */
@Component
public class AccountingPositionTransferPendingActivityRetriever implements AccountingPendingActivityRetriever {

	private static final Map<String, Function<Boolean, String>> GL_ACCOUNT_RESTRICTION_KEYS = MapUtils.ofEntries(
			MapUtils.entry("clientInvestmentAccountId", from -> from ? "fromClientInvestmentAccountId" : "toClientInvestmentAccountId"),
			MapUtils.entry("holdingInvestmentAccountId", from -> from ? "fromHoldingInvestmentAccountId" : "toHoldingInvestmentAccountId")
	);

	private static final Map<String, Supplier<String>> GL_SECURITY_RESTRICTION_KEYS = MapUtils.ofEntries(
			MapUtils.entry("investmentSecurityId", () -> "securityId"),
			MapUtils.entry("investmentTypeId", () -> "investmentTypeId"),
			MapUtils.entry("underlyingSecurityId", () -> "underlyingSecurityId")
	);

	private AccountingAccountService accountingAccountService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private AccountingBookingService<AccountingPositionTransfer> accountingBookingService;
	private AccountingJournalService accountingJournalService;
	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingJournalDetailDefinition> getPendingTransactionList(final GeneralLedgerSearchForm searchForm) {
		List<AccountingJournalDetailDefinition> detailList = new ArrayList<>();

		// Only interested in transfers with applicable details.
		Set<AccountingPositionTransferDetail> pendingTransferDetailSet = new HashSet<>();
		pendingTransferDetailSet.addAll(getPendingTransferDetailListTo(searchForm));
		pendingTransferDetailSet.addAll(getPendingTransferDetailListFrom(searchForm));

		if (pendingTransferDetailSet.isEmpty()) {
			return detailList;
		}

		Map<AccountingPositionTransfer, List<AccountingPositionTransferDetail>> transferDetailListMap = BeanUtils.getBeansMap(pendingTransferDetailSet, AccountingPositionTransferDetail::getPositionTransfer);
		transferDetailListMap.forEach((transfer, transferDetailList) -> {
			transfer.setDetailList(transferDetailList);

			if (searchForm.getPendingActivityRequest().isGeneratedByBookingPreview()) {
				BookingRuleScopes scope = searchForm.getPendingActivityRequest().isPositionsOnly() ? BookingRuleScopes.POSITION_IMPACT : null;
				AccountingJournal journal = getAccountingBookingService().getAccountingJournalPreview(BookingPreviewCommand.ofScope(transfer, AccountingJournalType.TRANSFER_JOURNAL, scope));
				@SuppressWarnings("unchecked")
				List<AccountingJournalDetailDefinition> previewList = (List<AccountingJournalDetailDefinition>) journal.getJournalDetailList();
				if (searchForm.getPendingActivityRequest().isPositionsOnly()) {
					previewList = BeanUtils.filter(previewList, position -> position.getAccountingAccount().isPosition());
				}
				detailList.addAll(previewList);
			}
			else {
				detailList.addAll(createJournalDetailListFromTransfer(transfer));
			}
		});

		return detailList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<AccountingPositionTransferDetail> getPendingTransferDetailListTo(GeneralLedgerSearchForm generalLedgerSearchForm) {
		AccountingPositionTransferDetailSearchForm detailSearchForm = new AccountingPositionTransferDetailSearchForm();
		detailSearchForm.setToClientInvestmentAccountId(generalLedgerSearchForm.getClientInvestmentAccountId());
		detailSearchForm.setToClientInvestmentAccountIds(generalLedgerSearchForm.getClientInvestmentAccountIds());
		detailSearchForm.setToHoldingInvestmentAccountId(generalLedgerSearchForm.getHoldingInvestmentAccountId());
		detailSearchForm.setToHoldingInvestmentAccountIds(generalLedgerSearchForm.getHoldingInvestmentAccountIds());

		appendGeneralLedgerAccountSearchRestrictionsToTransferDetailSearchForm(generalLedgerSearchForm, detailSearchForm, Boolean.FALSE);
		return getPendingTransferDetailList(detailSearchForm, generalLedgerSearchForm);
	}


	private List<AccountingPositionTransferDetail> getPendingTransferDetailListFrom(GeneralLedgerSearchForm generalLedgerSearchForm) {
		AccountingPositionTransferDetailSearchForm detailSearchForm = new AccountingPositionTransferDetailSearchForm();
		detailSearchForm.setFromClientInvestmentAccountId(generalLedgerSearchForm.getClientInvestmentAccountId());
		detailSearchForm.setFromClientInvestmentAccountIds(generalLedgerSearchForm.getClientInvestmentAccountIds());
		detailSearchForm.setFromHoldingInvestmentAccountId(generalLedgerSearchForm.getHoldingInvestmentAccountId());
		detailSearchForm.setFromHoldingInvestmentAccountIds(generalLedgerSearchForm.getHoldingInvestmentAccountIds());

		appendGeneralLedgerAccountSearchRestrictionsToTransferDetailSearchForm(generalLedgerSearchForm, detailSearchForm, Boolean.TRUE);
		return getPendingTransferDetailList(detailSearchForm, generalLedgerSearchForm);
	}


	private void appendGeneralLedgerAccountSearchRestrictionsToTransferDetailSearchForm(GeneralLedgerSearchForm generalLedgerSearchForm, AccountingPositionTransferDetailSearchForm detailSearchForm, Boolean from) {
		CollectionUtils.getStream(generalLedgerSearchForm.getRestrictionList())
				.filter(searchRestriction -> GL_ACCOUNT_RESTRICTION_KEYS.containsKey(searchRestriction.getField()))
				.map(searchRestriction -> {
					String fieldName = GL_ACCOUNT_RESTRICTION_KEYS.get(searchRestriction.getField()).apply(from);
					return detailSearchForm.isSearchRestrictionSet(fieldName) ? null : searchRestriction.copyForField(fieldName);
				})
				.filter(Objects::nonNull)
				.forEach(detailSearchForm::addSearchRestriction);
	}


	private List<AccountingPositionTransferDetail> getPendingTransferDetailList(AccountingPositionTransferDetailSearchForm detailSearchForm, GeneralLedgerSearchForm generalLedgerSearchForm) {
		// add applicable common filters from GL
		detailSearchForm.setClientInvestmentAccountGroupId(generalLedgerSearchForm.getClientInvestmentAccountGroupId());
		if (isIncludeGeneralLedgerSecurityFilter(generalLedgerSearchForm)) {
			detailSearchForm.setSecurityId(generalLedgerSearchForm.getInvestmentSecurityId());
			detailSearchForm.setInvestmentGroupId(generalLedgerSearchForm.getInvestmentGroupId());
			detailSearchForm.setInvestmentInstrumentId(generalLedgerSearchForm.getInvestmentInstrumentId());
			detailSearchForm.setInvestmentTypeId(generalLedgerSearchForm.getInvestmentTypeId());
			detailSearchForm.setUnderlyingSecurityId(generalLedgerSearchForm.getUnderlyingSecurityId());

			CollectionUtils.getStream(generalLedgerSearchForm.getRestrictionList())
					.filter(searchRestriction -> GL_SECURITY_RESTRICTION_KEYS.containsKey(searchRestriction.getField()) && !detailSearchForm.isSearchRestrictionSet(searchRestriction.getField()))
					.map(searchRestriction -> {
						String fieldName = GL_SECURITY_RESTRICTION_KEYS.get(searchRestriction.getField()).get();
						return detailSearchForm.isSearchRestrictionSet(fieldName) ? null : searchRestriction.copyForField(fieldName);
					})
					.filter(Objects::nonNull)
					.forEach(detailSearchForm::addSearchRestriction);
		}
		detailSearchForm.setJournalNotBooked(true);
		detailSearchForm.setTransactionDateMax(generalLedgerSearchForm.getTransactionOrSettlementDate());
		return getAccountingPositionTransferService().getAccountingPositionTransferDetailList(detailSearchForm);
	}


	private List<? extends AccountingJournalDetailDefinition> createJournalDetailListFromTransfer(AccountingPositionTransfer transfer) {
		AccountingJournal journal = new AccountingJournal();
		journal.setJournalType(getAccountingJournalService().getAccountingJournalTypeByName(AccountingJournalType.TRANSFER_JOURNAL));
		journal.setFkFieldId(transfer.getId());
		journal.setJournalDetailList(new ArrayList<>());

		for (AccountingPositionTransferDetail transferDetail : CollectionUtils.getIterable(transfer.getDetailList())) {
			appendTransferDetailToAccountingJournal(journal, transferDetail);
		}
		return journal.getJournalDetailList();
	}


	private void appendTransferDetailToAccountingJournal(AccountingJournal journal, AccountingPositionTransferDetail detail) {
		boolean noPaymentOnOpen = detail.getExistingPosition() != null && InvestmentUtils.isNoPaymentOnOpen(detail.getExistingPosition().getInvestmentSecurity());

		// Create details for transfers that are to (1 detail), from (1 detail), or between (2 details) our account(s).
		AccountingPositionTransferType type = detail.getPositionTransfer().getType();
		if (type.isFromOurAccount() || type.isBetweenOurAccounts()) {
			AccountingJournalDetail fromDetail = new AccountingJournalDetail();
			fromDetail.setJournal(journal);
			fromDetail.setClientInvestmentAccount(detail.getPositionTransfer().getFromClientInvestmentAccount());
			fromDetail.setHoldingInvestmentAccount(detail.getPositionTransfer().getFromHoldingInvestmentAccount());
			fromDetail.setAccountingAccount(detail.getExistingPosition() != null ? detail.getExistingPosition().getAccountingAccount()
					: getAccountingAccountService().getAccountingAccountByName(type.isCollateral() ? AccountingAccount.ASSET_POSITION_COLLATERAL : AccountingAccount.ASSET_POSITION));
			fromDetail.setInvestmentSecurity(detail.getSecurity());
			fromDetail.setParentTransaction(detail.getExistingPosition());

			fromDetail.setQuantity(MathUtils.negate(detail.getQuantity()));
			if (noPaymentOnOpen) {
				fromDetail.setLocalDebitCredit(BigDecimal.ZERO);
			}
			else {
				fromDetail.setLocalDebitCredit(MathUtils.negate(detail.getPositionCostBasis()));
			}
			fromDetail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(fromDetail.getLocalDebitCredit(), detail.getExchangeRateToBase(), fromDetail.getClientInvestmentAccount()));
			fromDetail.setPositionCostBasis(MathUtils.negate(detail.getPositionCostBasis()));
			fromDetail.setExchangeRateToBase(detail.getExchangeRateToBase());

			fromDetail.setTransactionDate(detail.getPositionTransfer().getTransactionDate());
			fromDetail.setSettlementDate(detail.getPositionTransfer().getSettlementDate());
			fromDetail.setOriginalTransactionDate(detail.getPositionTransfer().isUseOriginalTransactionDate() ? detail.getOriginalPositionOpenDate() : detail.getPositionTransfer().getTransactionDate());

			fromDetail.setFkFieldId(detail.getId());
			journal.addJournalDetail(fromDetail);
		}
		if (type.isToOurAccount() || type.isBetweenOurAccounts()) {
			AccountingJournalDetail toDetail = new AccountingJournalDetail();
			toDetail.setJournal(journal);
			toDetail.setClientInvestmentAccount(detail.getPositionTransfer().getToClientInvestmentAccount());
			toDetail.setHoldingInvestmentAccount(detail.getPositionTransfer().getToHoldingInvestmentAccount());
			toDetail.setAccountingAccount(detail.getExistingPosition() != null ? detail.getExistingPosition().getAccountingAccount()
					: getAccountingAccountService().getAccountingAccountByName(type.isCollateral() ? AccountingAccount.ASSET_POSITION_COLLATERAL : AccountingAccount.ASSET_POSITION));
			toDetail.setInvestmentSecurity(detail.getSecurity());
			toDetail.setParentTransaction(detail.getExistingPosition());

			toDetail.setQuantity(detail.getQuantity());
			if (noPaymentOnOpen) {
				toDetail.setLocalDebitCredit(BigDecimal.ZERO);
			}
			else {
				toDetail.setLocalDebitCredit(detail.getPositionCostBasis());
			}
			toDetail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(toDetail.getLocalDebitCredit(), detail.getExchangeRateToBase(), toDetail.getClientInvestmentAccount()));
			toDetail.setPositionCostBasis(detail.getPositionCostBasis());
			toDetail.setExchangeRateToBase(detail.getExchangeRateToBase());

			toDetail.setTransactionDate(detail.getPositionTransfer().getTransactionDate());
			toDetail.setSettlementDate(detail.getPositionTransfer().getSettlementDate());
			toDetail.setOriginalTransactionDate(detail.getPositionTransfer().isUseOriginalTransactionDate() ? detail.getOriginalPositionOpenDate() : detail.getPositionTransfer().getTransactionDate());

			toDetail.setFkFieldId(detail.getId());
			journal.addJournalDetail(toDetail);
		}
	}


	/**
	 * Determines if the {@link GeneralLedgerSearchForm} is filtering for a currency.
	 * Currency securities are unique in they are not positions and can be affected by trades from different types of securities.
	 * Thus, we have to look up applicable trades more generically to get a good pending preview.
	 */
	private boolean isIncludeGeneralLedgerSecurityFilter(GeneralLedgerSearchForm searchForm) {
		if (!searchForm.getPendingActivityRequest().isPositionsOnly() && searchForm.getInvestmentSecurityId() != null) {
			return !getInvestmentInstrumentService().getInvestmentSecurity(searchForm.getInvestmentSecurityId()).isCurrency();
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public AccountingBookingService<AccountingPositionTransfer> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<AccountingPositionTransfer> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
