package com.clifton.accounting.positiontransfer;

import com.clifton.accounting.AccountingBean;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * This class is used to summarize AccountingPositionTransferDetail objects of the same security
 * before executing the 'Transfer Rules' against them, this reduces the number of rules that would
 * need to be unnecessarily evaluated (e.g. Client Approved Contracts is unnecessary to run twice
 * against the same unapproved security.)
 *
 * @author stevenf on 11/11/2016.
 */
public class AccountingPositionTransferRuleDetail implements IdentityObject, AccountingBean {

	private boolean buy;
	private boolean collateral;

	private Date settlementDate;
	private Date transactionDate;

	private Integer sourceEntityId;

	private BigDecimal quantity;
	private BigDecimal quantityNormalized;
	private BigDecimal accountingNotional;
	private BigDecimal exchangeRateToBase;

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	private InvestmentSecurity investmentSecurity;
	private InvestmentSecurity settlementCurrency;

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Serializable getIdentity() {
		return this.sourceEntityId;
	}


	@Override
	public boolean isNewBean() {
		return false;
	}


	@Override
	public Integer getSourceEntityId() {
		return this.sourceEntityId;
	}


	public void setSourceEntityId(Integer sourceEntityId) {
		this.sourceEntityId = sourceEntityId;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	@Override
	public InvestmentSecurity getSettlementCurrency() {
		return this.settlementCurrency;
	}


	public void setSettlementCurrency(InvestmentSecurity settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	@Override
	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	@Override
	public BigDecimal getQuantityNormalized() {
		return this.quantityNormalized;
	}


	public void setQuantityNormalized(BigDecimal quantityNormalized) {
		this.quantityNormalized = quantityNormalized;
	}


	@Override
	public BigDecimal getAccountingNotional() {
		return this.accountingNotional;
	}


	public void setAccountingNotional(BigDecimal accountingNotional) {
		this.accountingNotional = accountingNotional;
	}


	@Override
	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	@Override
	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	@Override
	public boolean isCollateral() {
		return this.collateral;
	}


	public void setCollateral(boolean collateral) {
		this.collateral = collateral;
	}


	@Override
	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	@Override
	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
}
