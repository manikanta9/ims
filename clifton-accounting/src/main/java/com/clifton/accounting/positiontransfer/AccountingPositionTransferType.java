package com.clifton.accounting.positiontransfer;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.schema.SystemTable;


/**
 * @author mwacker
 */
@CacheByName
public class AccountingPositionTransferType extends NamedEntity<Short> {

	public static final String FROM_TRANSFER = "From Our Account";
	public static final String TO_TRANSFER = "To Our Account";
	public static final String BETWEEN_TRANSFER = "Between Our Accounts";
	public static final String INTERNAL_TRANSFER = "Internal Accounts";

	public static final String COLLATERAL_TRANSFER_POST = "Client Collateral Transfer - Post";
	public static final String COLLATERAL_TRANSFER_RETURN = "Client Collateral Transfer - Return";
	public static final String COUNTERPARTY_COLLATERAL_TRANSFER_POST = "Counterparty Collateral Transfer - Post";
	public static final String COUNTERPARTY_COLLATERAL_TRANSFER_RETURN = "Counterparty Collateral Transfer - Return";

	public static final String CASH_COLLATERAL_TRANSFER_POST = "Client Cash Collateral Transfer - Post";
	public static final String CASH_COLLATERAL_TRANSFER_RETURN = "Client Cash Collateral Transfer - Return";
	public static final String COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST = "Counterparty Cash Collateral Transfer - Post";
	public static final String COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN = "Counterparty Cash Collateral Transfer - Return";

	public static final String SECURITY_TARGET_DISTRIBUTION = "Security Target Distribution";
	public static final String SECURITY_TARGET_CONTRIBUTION = "Security Target Contribution";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private AccountingAccount toAccountingAccount;
	private AccountingAccount toOffsettingAccountingAccount;

	private AccountingAccount fromAccountingAccount;
	private AccountingAccount fromOffsettingAccountingAccount;

	/**
	 * Optional {@link SystemTable} that {@link AccountingPositionTransfer} of this type should use for their source.
	 */
	private SystemTable sourceSystemTable;


	/**
	 * Our system is receiving the asset.
	 */
	private boolean receivingAsset;
	/**
	 * Our system is sending the asset.
	 */
	private boolean sendingAsset;
	/**
	 * Returns a boolean indicating whether this is an internal transfer (meaning a transfer of positions or securities between two
	 * internal accounts.) If false, then it is a transfer involving an external account.
	 */
	private boolean internalTransfer;

	private boolean cash;
	/**
	 * Indicates that the transfer is collateral.  Transfers of this type that are not cash, require the useOriginalTransactionDate field on the transfer to be true.
	 */
	private boolean collateral;
	/**
	 * True indicates that the transfer is posting collateral, and false indicates a collateral return.
	 */
	private boolean collateralPosting;
	/**
	 * The collateral being transfered belongs to the Counterparty.
	 */
	private boolean counterpartyCollateral;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public AccountingPositionTransferType() {
		super();
	}


	public AccountingPositionTransferType(String name, boolean receivingAsset, boolean sendingAsset, Short id) {
		this(name, receivingAsset, sendingAsset, false, false, false, id);
	}


	public AccountingPositionTransferType(String name, boolean receivingAsset, boolean sendingAsset, boolean collateralPosting, Short id) {
		this(name, receivingAsset, sendingAsset, collateralPosting, true, false, id);
	}


	public AccountingPositionTransferType(String name, boolean receivingAsset, boolean sendingAsset, Boolean collateralPosting, boolean cash, Short id) {
		this(name, receivingAsset, sendingAsset, collateralPosting, true, cash, id);
	}


	private AccountingPositionTransferType(String name, boolean receivingAsset, boolean sendingAsset, boolean collateralPosting, boolean collateral, boolean cash, Short id) {
		super();
		this.setId(id);
		this.setName(name);
		this.cash = cash;
		this.collateralPosting = collateralPosting;
		this.collateral = collateral;
		this.receivingAsset = receivingAsset;
		this.sendingAsset = sendingAsset;
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isFromOurAccount() {
		return !isReceivingAsset() && isSendingAsset();
	}


	public boolean isToOurAccount() {
		return isReceivingAsset() && !isSendingAsset();
	}


	public boolean isBetweenOurAccounts() {
		return isReceivingAsset() && isSendingAsset();
	}


	public boolean isCollateralReturn() {
		return isCollateral() && !isCollateralPosting();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccount getToAccountingAccount() {
		return this.toAccountingAccount;
	}


	public void setToAccountingAccount(AccountingAccount toAccountingAccount) {
		this.toAccountingAccount = toAccountingAccount;
	}


	public AccountingAccount getToOffsettingAccountingAccount() {
		return this.toOffsettingAccountingAccount;
	}


	public void setToOffsettingAccountingAccount(AccountingAccount toOffsettingAccountingAccount) {
		this.toOffsettingAccountingAccount = toOffsettingAccountingAccount;
	}


	public AccountingAccount getFromAccountingAccount() {
		return this.fromAccountingAccount;
	}


	public void setFromAccountingAccount(AccountingAccount fromAccountingAccount) {
		this.fromAccountingAccount = fromAccountingAccount;
	}


	public AccountingAccount getFromOffsettingAccountingAccount() {
		return this.fromOffsettingAccountingAccount;
	}


	public void setFromOffsettingAccountingAccount(AccountingAccount fromOffsettingAccountingAccount) {
		this.fromOffsettingAccountingAccount = fromOffsettingAccountingAccount;
	}


	public SystemTable getSourceSystemTable() {
		return this.sourceSystemTable;
	}


	public void setSourceSystemTable(SystemTable sourceSystemTable) {
		this.sourceSystemTable = sourceSystemTable;
	}


	public boolean isReceivingAsset() {
		return this.receivingAsset;
	}


	public void setReceivingAsset(boolean receivingAsset) {
		this.receivingAsset = receivingAsset;
	}


	public boolean isSendingAsset() {
		return this.sendingAsset;
	}


	public void setSendingAsset(boolean sendingAsset) {
		this.sendingAsset = sendingAsset;
	}


	public boolean isInternalTransfer() {
		return this.internalTransfer;
	}


	public void setInternalTransfer(boolean internalTransfer) {
		this.internalTransfer = internalTransfer;
	}


	public boolean isCash() {
		return this.cash;
	}


	public void setCash(boolean cash) {
		this.cash = cash;
	}


	public boolean isCollateralPosting() {
		return this.collateralPosting;
	}


	public void setCollateralPosting(boolean collateralPosting) {
		this.collateralPosting = collateralPosting;
	}


	public boolean isCounterpartyCollateral() {
		return this.counterpartyCollateral;
	}


	public void setCounterpartyCollateral(boolean counterpartyCollateral) {
		this.counterpartyCollateral = counterpartyCollateral;
	}


	public boolean isCollateral() {
		return this.collateral;
	}


	public void setCollateral(boolean collateral) {
		this.collateral = collateral;
	}
}
