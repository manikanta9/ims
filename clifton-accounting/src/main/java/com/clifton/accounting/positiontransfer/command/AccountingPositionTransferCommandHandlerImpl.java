package com.clifton.accounting.positiontransfer.command;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosing;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingCommand;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingSecurityQuantity;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingService;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * @author NickK
 */
@Component
public class AccountingPositionTransferCommandHandlerImpl implements AccountingPositionTransferCommandHandler {

	private AccountingValuationService accountingValuationService;
	private AccountingPositionClosingService accountingPositionClosingService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private AccountingTransactionService accountingTransactionService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataRetriever marketDataRetriever;


	////////////////////////////////////////////////////////////////////////////
	////////   AccountingPositionTransferCommandHandler Methods   //////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityList(AccountingPositionTransferCommand transferCommand) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setSymbolExact(transferCommand.getSecuritySymbol());
		searchForm.setActiveOnDate(transferCommand.getTransactionDate());
		if (transferCommand.getInvestmentType() != null) {
			searchForm.setInvestmentTypeId(transferCommand.getInvestmentType().getId());
		}
		if (transferCommand.getInvestmentTypeSubType() != null) {
			searchForm.setInvestmentTypeSubTypeId(transferCommand.getInvestmentTypeSubType().getId());
		}
		if (transferCommand.getInvestmentTypeSubType2() != null) {
			searchForm.setInvestmentTypeSubType2Id(transferCommand.getInvestmentTypeSubType2().getId());
		}
		if (transferCommand.getInvestmentGroup() != null) {
			searchForm.setInvestmentGroupId(transferCommand.getInvestmentGroup().getId());
		}
		if (transferCommand.getInstrumentHierarchy() != null) {
			searchForm.setHierarchyId(transferCommand.getInstrumentHierarchy().getId());
		}
		if (transferCommand.getInvestmentInstrument() != null) {
			searchForm.setInstrumentId(transferCommand.getInvestmentInstrument().getId());
		}
		if (transferCommand.getSecurityGroup() != null) {
			searchForm.setSecurityGroupId(transferCommand.getSecurityGroup().getId());
		}

		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
		ValidationUtils.assertNotEmpty(securityList, "Cannot find Active Security on " + DateUtils.fromDateShort(searchForm.getActiveOnDate()) + " with "
				+ (searchForm.getSymbolExact() == null ? "criteria specified" : ("Symbol [" + searchForm.getSymbolExact() + "]")) + ".");
		return securityList;
	}


	@Override
	public void populateAccountingPositionTransferCommandSecurity(AccountingPositionTransferCommand transferCommand, AccountingPositionTransferContext context) {
		ValidationUtils.assertNotNull(transferCommand.getSecuritySymbol(), "Position transfer command/row does not have a Security Symbol defined.");
		context.populateInvestmentSecurity(transferCommand, this::getInvestmentSecurity);
	}


	@Override
	public AccountingPositionTransfer generateAccountingPositionTransfer(AccountingPositionTransferCommand transferCommand, boolean suppressNoPositionValidation, AccountingPositionTransferContext context) {
		AccountingPositionTransfer transfer = context.getAccountingPositionTransfer(transferCommand, command -> createAccountingPositionTransfer(command, context));
		populateAccountingPositionTransferDetail(transfer, transferCommand, suppressNoPositionValidation, context);
		return transfer;
	}


	@Override
	@Transactional
	public String saveGeneratedAccountingPositionTransferList(AccountingPositionTransferContext context) {
		StringBuilder stringBuilder = new StringBuilder();
		for (AccountingPositionTransfer transfer : CollectionUtils.getIterable(context.getAccountingPositionTransferList())) {
			if (!CollectionUtils.isEmpty(transfer.getDetailList())) {
				getAccountingPositionTransferService().saveAccountingPositionTransfer(transfer);
				stringBuilder.append("Accounting Position Transfer of type [").append(transfer.getType().getName()).append("] with [")
						.append(CollectionUtils.getSize(transfer.getDetailList())).append("] Details added.\n");
			}
		}
		return stringBuilder.length() == 0 ? stringBuilder.append("No Accounting Position Transfers were generated from the provided transfer command(s).").toString() : stringBuilder.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	///////////                 Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	// This private method is used by method reference lambda in populateAccountingPositionTransferCommandSecurity(...)
	private InvestmentSecurity getInvestmentSecurity(AccountingPositionTransferCommand transferCommand) {
		List<InvestmentSecurity> securityList = getInvestmentSecurityList(transferCommand);
		InvestmentSecurity security = null;
		if (CollectionUtils.getSize(securityList) == 1) {
			security = CollectionUtils.getFirstElement(securityList);
		}
		else {
			// check for Yellow Key
			String yellowKey = transferCommand.getYellowKey();
			ValidationUtils.assertNotNull(yellowKey, "Multiple securities with the symbol [" + transferCommand.getSecuritySymbol() + "] were found active on "
					+ DateUtils.fromDateShort(transferCommand.getTransactionDate()) + ".  Please verify the security symbol or provide the Yellow Key.");
			for (InvestmentSecurity sec : CollectionUtils.getIterable(securityList)) {
				// Passing null in as the MarketDataSource name will default to "Bloomberg".
				MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(sec.getInstrument(), null);
				ValidationUtils.assertNotNull(mapping, "There is no Instrument Mapping setup for " + sec.getLabel() + " to get the market sector from.");
				if (mapping.getMarketSector() != null && yellowKey.equals(mapping.getMarketSector().getName())) {
					security = sec;
					break;
				}
			}
		}
		ValidationUtils.assertNotNull(security, "Cannot find Active Security on " + DateUtils.fromDateShort(transferCommand.getTransactionDate()) + " with Symbol ["
				+ transferCommand.getSecuritySymbol() + "] and Yellow Key [" + transferCommand.getYellowKey() + "].");
		return security;
	}


	private AccountingPositionTransfer createAccountingPositionTransfer(AccountingPositionTransferCommand transferCommand, AccountingPositionTransferContext context) {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		populateAccountingPositionTransferInvestmentAccounts(transfer, transferCommand, true, context);
		populateAccountingPositionTransferInvestmentAccounts(transfer, transferCommand, false, context);
		transfer.setType(transferCommand.getType());
		transfer.setNote(transferCommand.getNote());
		transfer.setTransactionDate(transferCommand.getTransactionDate());
		transfer.setSettlementDate(transferCommand.getSettlementDate());
		transfer.setExposureDate(transferCommand.getPriceDate());
		transfer.setCollateralTransfer(BooleanUtils.isTrue(transferCommand.isCollateralTransfer()));
		transfer.setUseOriginalTransactionDate(transferCommand.isUseOriginalTransactionDate());
		transfer.setDetailList(new ArrayList<>());
		return transfer;
	}


	/**
	 * Looks up and populates the client and holding {@link InvestmentAccount}s for the transfer by using the account number(s) specified on the upload. The fromAccounts flag
	 * defines whether to populate the source (from) or destination (to) accounts. If the client account number is null, neither account will be populated. If only the holding
	 * account number is null, the holding account will be looked up based on the client account and security investment type.
	 */
	private void populateAccountingPositionTransferInvestmentAccounts(AccountingPositionTransfer transfer, AccountingPositionTransferCommand transferCommand, boolean fromAccounts, AccountingPositionTransferContext context) {
		InvestmentAccount clientAccount = context.populateClientInvestmentAccount(transferCommand, fromAccounts, command -> {
			if (fromAccounts) {
				return getClientInvestmentAccountWithNumber(command.getFromClientInvestmentAccountNumber());
			}
			else {
				return getClientInvestmentAccountWithNumber(command.getToClientInvestmentAccountNumber());
			}
		});
		if (clientAccount == null) {
			return;
		}

		InvestmentAccount holdingAccount = context.populateHoldingInvestmentAccount(transferCommand, fromAccounts, command -> {
			if (fromAccounts) {
				return getHoldingInvestmentAccountForClientWithNumberAndActiveOnPurpose(clientAccount, command.getFromHoldingInvestmentAccountNumber(), transfer.getTransactionDate());
			}
			else {
				return getHoldingInvestmentAccountForClientWithNumberAndActiveOnPurpose(clientAccount, command.getToHoldingInvestmentAccountNumber(), transfer.getTransactionDate());
			}
		});

		if (fromAccounts) {
			transfer.setFromClientInvestmentAccount(clientAccount);
			transfer.setFromHoldingInvestmentAccount(holdingAccount);
		}
		else {
			transfer.setToClientInvestmentAccount(clientAccount);
			transfer.setToHoldingInvestmentAccount(holdingAccount);
		}
	}


	private InvestmentAccount getClientInvestmentAccountWithNumber(String clientAccountNumber) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setNumberEquals(clientAccountNumber);
		searchForm.setOurAccount(Boolean.TRUE);
		return getInvestmentAccountWithSearchForm(searchForm, true, "[Account Number: " + clientAccountNumber + ", IsOurAccount: TRUE]");
	}


	private InvestmentAccount getHoldingInvestmentAccountForClientWithNumberAndActiveOnPurpose(InvestmentAccount clientAccount, String holdingAccountNumber, Date activeOnDate) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setNumberEquals(holdingAccountNumber);
		searchForm.setMainAccountId(clientAccount.getId());
		searchForm.setMainPurposeActiveOnDate(activeOnDate);
		return getInvestmentAccountWithSearchForm(searchForm, false, "[Client Account Number: " + clientAccount.getNumber() + ", Holding Account Number: " + holdingAccountNumber + ", Purpose Active On Date: " + DateUtils.fromDate(activeOnDate) + "]");
	}


	private InvestmentAccount getInvestmentAccountWithSearchForm(InvestmentAccountSearchForm searchForm, boolean isClient, String errorDetails) {
		List<InvestmentAccount> investmentAccountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		ValidationUtils.assertNotEmpty(investmentAccountList, (isClient ? "Client" : "Holding") + " Investment Account was not found with criteria: " + errorDetails);
		try {
			return CollectionUtils.getOnlyElement(investmentAccountList);
		}
		catch (IllegalArgumentException e) {
			throw new ValidationException("Multiple " + (isClient ? "Client" : "Holding") + " Investment Accounts were found with criteria: " + errorDetails + ". Please verify the account number.");
		}
	}


	private void populateAccountingPositionTransferDetail(AccountingPositionTransfer transfer, AccountingPositionTransferCommand transferCommand, boolean suppressNoPositionValidation, AccountingPositionTransferContext context) {
		if (transfer.getType().isSendingAsset()) {
			populateTransferPropertiesOnCommand(transfer, transferCommand);
			List<AccountingPositionClosing> accountingPositionClosingList = getAccountingPositionClosingList(transferCommand, suppressNoPositionValidation, context);
			if (!suppressNoPositionValidation) {
				ValidationUtils.assertNotEmpty(accountingPositionClosingList, "Unable to find open positions from Client Investment Account [" + transfer.getFromClientInvestmentAccount().getLabel() + "] and Holding Investment Account ["
						+ transfer.getFromHoldingInvestmentAccount().getLabel() + "] for Security Symbol [" + transferCommand.getSecurity().getSymbol() + "]. Please verify the specified Accounts, Security, and Transfer Date.");
			}
			// create transfer details from identified lots
			for (AccountingPositionClosing positionClosing : CollectionUtils.getIterable(accountingPositionClosingList)) {
				transfer.getDetailList().add(createAccountingPositionTransferDetailFromLotPosition(transferCommand, positionClosing, context));
			}
		}
		else {
			/*
			 * We have no knowledge of the details being transferred to one of our accounts from an
			 * external source. Create the details from the information provided.
			 */
			transfer.getDetailList().add(createAccountingPositionTransferDetailFromImport(transferCommand));
		}
	}


	private List<AccountingPositionClosing> getAccountingPositionClosingList(AccountingPositionTransferCommand transferCommand, boolean suppressNoPositionValidation, AccountingPositionTransferContext context) {
		populateAccountingPositionTransferCommandQuantityAndOriginalFace(transferCommand, suppressNoPositionValidation, context);
		if (MathUtils.isNullOrZero(transferCommand.getQuantity())) {
			return Collections.emptyList();
		}

		/*
		 * Transfers from one of our accounts allows us to look up current lot positions and correctly
		 * populate the transfer details from the lots based on the quantity to transfer.
		 */
		AccountingPositionClosingCommand positionClosingCommand = AccountingPositionClosingCommand.onTransactionDate(transferCommand.getTransactionDate());
		positionClosingCommand.setClientInvestmentAccountId(transferCommand.getFromClientInvestmentAccount().getId());
		positionClosingCommand.setHoldingInvestmentAccountId(transferCommand.getFromHoldingInvestmentAccount().getId());
		positionClosingCommand.setInvestmentSecurityId(transferCommand.getSecurity().getId());

		// set lot selection order or specified lot on transferCommand
		String lotSelectionMethod = transferCommand.getLotSelectionMethod();
		if (!AccountingPositionTransferCommand.LOT_SPECIFIED_SELECTION_METHOD_NAME.equals(lotSelectionMethod)) {
			positionClosingCommand.setOrder(lotSelectionMethod != null ? AccountingPositionOrders.valueOf(lotSelectionMethod) : AccountingPositionOrders.FIFO);
		}
		else {
			positionClosingCommand.setExistingPositionId(transferCommand.getExistingPositionId());
		}

		// define position closing security quantity to transferCommand
		AccountingPositionClosingSecurityQuantity securityQuantity = new AccountingPositionClosingSecurityQuantity();
		securityQuantity.setInvestmentSecurityId(positionClosingCommand.getInvestmentSecurityId());
		securityQuantity.setQuantity(transferCommand.getQuantity());
		positionClosingCommand.setSecurityQuantityList(Collections.singletonList(securityQuantity));

		// create transfer details from identified lots
		return getAccountingPositionClosingService().getAccountingPositionClosingLotList(positionClosingCommand);
	}


	private void populateTransferPropertiesOnCommand(AccountingPositionTransfer transfer, AccountingPositionTransferCommand transferCommand) {
		transferCommand.setFromClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		transferCommand.setFromHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		transferCommand.setToClientInvestmentAccount(transfer.getToClientInvestmentAccount());
		transferCommand.setToHoldingInvestmentAccount(transfer.getToHoldingInvestmentAccount());
		if (transferCommand.getTransactionDate() == null) {
			transferCommand.setTransactionDate(transfer.getTransactionDate());
		}
		if (transferCommand.getSettlementDate() == null) {
			transferCommand.setSettlementDate(transfer.getSettlementDate());
		}
	}


	private AccountingPositionTransferDetail createAccountingPositionTransferDetailFromLotPosition(AccountingPositionTransferCommand transferCommand, AccountingPositionClosing positionClosing, AccountingPositionTransferContext context) {
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setSecurity(transferCommand.getSecurity());
		detail.setQuantity(positionClosing.getRemainingQuantity()); // Use the quantity from this lot
		populateAccountingPositionTransferDetailOriginalFace(detail, transferCommand, positionClosing.getQuantity(), context);
		detail.setOriginalPositionOpenDate(ObjectUtils.coalesce(transferCommand.getOriginalPositionOpenDate(), positionClosing.getOriginalTransactionDate()));
		detail.setCommissionPerUnitOverride(transferCommand.getCommissionPerUnitOverride()); // if null will be calculated by system
		detail.setExchangeRateToBase(ObjectUtils.coalesce(transferCommand.getExchangeRateToBase(), positionClosing.getExchangeRateToBase()));
		detail.setCostPrice(ObjectUtils.coalesce(transferCommand.getCostPrice(), positionClosing.getPrice()));
		detail.setTransferPrice(ObjectUtils.coalesce(getAccountingPositionTransferCommandTransferPrice(transferCommand), positionClosing.getClosingPrice(), positionClosing.getPrice()));
		detail.setPositionCostBasis(positionClosing.getRemainingCostBasis()); // Position Cost Basis will be calculated in PositionTransferService.
		detail.setExistingPosition(getAccountingTransactionService().getAccountingTransaction(positionClosing.getId()));
		return detail;
	}


	private BigDecimal getAccountingPositionTransferCommandTransferPrice(AccountingPositionTransferCommand transferCommand) {
		BigDecimal price = transferCommand.getTransferPrice();
		if (price == null && transferCommand.getPriceDate() != null) {
			price = getMarketDataRetriever().getPriceFlexible(transferCommand.getSecurity(), transferCommand.getPriceDate(), false);
		}
		return price;
	}


	private void populateAccountingPositionTransferDetailOriginalFace(AccountingPositionTransferDetail detail, AccountingPositionTransferCommand transferCommand, BigDecimal positionQuantity, AccountingPositionTransferContext context) {
		if (isAccountingPositionTransferCommandSecurityFactorChangeEventAllowed(transferCommand, context)) {
			detail.setOriginalFace(ObjectUtils.coalesce(transferCommand.getOriginalFace(), positionQuantity));
		}
	}


	private boolean isAccountingPositionTransferCommandSecurityFactorChangeEventAllowed(AccountingPositionTransferCommand transferCommand, AccountingPositionTransferContext context) {
		return context.isSecurityInstrumentHierarchyFactorChangeAllowed(transferCommand, command -> {
			Short hierarchyId = command.getSecurity().getInstrument().getHierarchy().getId();
			return getInvestmentSecurityEventService().isInvestmentSecurityEventTypeAllowedForHierarchy(hierarchyId, InvestmentSecurityEventType.FACTOR_CHANGE);
		});
	}


	private AccountingPositionTransferDetail createAccountingPositionTransferDetailFromImport(AccountingPositionTransferCommand transferCommand) {
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setSecurity(transferCommand.getSecurity());
		detail.setQuantity(transferCommand.getQuantity());
		detail.setOriginalFace(transferCommand.getOriginalFace());
		detail.setOriginalPositionOpenDate(transferCommand.getOriginalPositionOpenDate());
		detail.setCommissionPerUnitOverride(transferCommand.getCommissionPerUnitOverride());
		detail.setExchangeRateToBase(transferCommand.getExchangeRateToBase());
		detail.setCostPrice(transferCommand.getCostPrice());
		detail.setTransferPrice(transferCommand.getTransferPrice());
		return detail;
	}


	/**
	 * Populates the transferCommands's quantity and original face values based on what was specified on the transferCommand (quantity, quantity percent, original face, or original
	 * face percent). If a percent value is specified, the quantity and original face values to transfer are calculated based on a percentage of the net position. Original face
	 * values can only be used by securities that allow factor change events.
	 */
	private void populateAccountingPositionTransferCommandQuantityAndOriginalFace(AccountingPositionTransferCommand transferCommand, boolean suppressNoPositionValidation, AccountingPositionTransferContext context) {
		BigDecimal quantity = transferCommand.getQuantity();
		BigDecimal originalFace = transferCommand.getOriginalFace();

		if (quantity == null) {
			// check for percentage of net position
			if (transferCommand.getQuantityPercent() != null) {
				// convert quantity percent to a quantity
				quantity = getQuantityFromPercentNetPositionQuantity(transferCommand, transferCommand.getQuantityPercent(), suppressNoPositionValidation);
			}
		}

		if (originalFace != null) {
			ValidationUtils.assertNull(quantity, "Position transfer command/row has both (Quantity or Quantity Percent) and (Original Face or Original Face Percent) specified.");
			BigDecimal factor = getInvestmentSecurityFactor(transferCommand, transferCommand.getTransactionDate(), context);
			// calculate quantity from original face and factor
			quantity = MathUtils.multiply(originalFace, factor, 2);
		}
		else {
			if (transferCommand.getOriginalFacePercent() != null) {
				// convert original face percent to a quantity and then calculate the original face value
				ValidationUtils.assertNull(quantity, "Position transfer command/row has both (Quantity or Quantity Percent) and (Original Face or Original Face Percent) specified.");
				BigDecimal roundingValue = transferCommand.getOriginalFaceRounding();
				ValidationUtils.assertNotNull(roundingValue, "Cannot calculate an Original Face Percent amount without Original Face Rounding defined.");
				BigDecimal factor = getInvestmentSecurityFactor(transferCommand, transferCommand.getTransactionDate(), context);
				quantity = getQuantityFromPercentNetPositionQuantity(transferCommand, transferCommand.getOriginalFacePercent(), suppressNoPositionValidation);
				BigDecimal calculatedFaceFromQuantity = MathUtils.divide(quantity, factor);
				// calculate original face rounded by dividing by roundingValue to round and then multiplying by it rounding value again to get correct scale
				originalFace = MathUtils.multiply(MathUtils.divide(calculatedFaceFromQuantity, roundingValue, 0), roundingValue, 0);
				quantity = MathUtils.multiply(originalFace, factor, 2);
			}
		}

		if (!suppressNoPositionValidation) {
			ValidationUtils.assertFalse(MathUtils.isNullOrZero(quantity), "Position transfer command/row has no defined amount to transfer. " +
					"Expected one of Quantity, Quantity Percent, Original Face, or Original Face Percent to be defined with a value greater than 0.");
		}
		transferCommand.setQuantity(quantity);
		transferCommand.setOriginalFace(originalFace);
	}


	private BigDecimal getQuantityFromPercentNetPositionQuantity(AccountingPositionTransferCommand transferCommand, BigDecimal percentage, boolean suppressNoPositionValidation) {
		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(transferCommand.getTransactionDate());
		searchForm.setClientInvestmentAccountId(transferCommand.getFromClientInvestmentAccount().getId());
		searchForm.setHoldingInvestmentAccountId(transferCommand.getFromHoldingInvestmentAccount().getId());
		searchForm.setInvestmentSecurityId(transferCommand.getSecurity().getId());
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_NOT_OUR_ACCOUNTS_AND_RECEIVABLE_COLLATERAL);
		List<AccountingBalanceValue> clientSecurityBalanceList = getAccountingValuationService().getAccountingBalanceValueList(searchForm);
		try {
			AccountingBalanceValue value = CollectionUtils.getOnlyElement(clientSecurityBalanceList);
			if (suppressNoPositionValidation && value == null) {
				return null;
			}
			ValidationUtils.assertNotNull(value, "Unable to determine quantity to transfer for Quantity or Original Face Percent because there is no position available for Client Account with number ["
					+ transferCommand.getFromClientInvestmentAccount().getNumber() + "] and Security with symbol [" + transferCommand.getSecurity().getSymbol() + "].");
			RoundingMode roundingMode = transferCommand.getPercentRoundingMode();
			ValidationUtils.assertNotNull(roundingMode, "Unable to properly calculate quantity to transfer for Quantity or Original Face Percent because PercentRoundingMode is missing.");
			return MathUtils.round(MathUtils.getPercentageOf(percentage, value.getQuantity(), true), 0, roundingMode);
		}
		catch (IllegalArgumentException e) {
			throw new ValidationException("Unable to determine quantity to transfer for Quantity or Original Face Percent because there is more than one position available for Client Account with number ["
					+ transferCommand.getFromClientInvestmentAccount().getNumber() + "] and Security with symbol [" + transferCommand.getSecurity().getSymbol() + "].");
		}
	}


	private BigDecimal getInvestmentSecurityFactor(AccountingPositionTransferCommand transferCommand, Date transactionDate, AccountingPositionTransferContext context) {
		ValidationUtils.assertTrue(isAccountingPositionTransferCommandSecurityFactorChangeEventAllowed(transferCommand, context),
				"Position transfer command/row has a specified OriginalFace or Original Face Percent for a Security with Symbol ["
						+ transferCommand.getSecurity().getSymbol() + "], which does not allow Factor Change Events.");
		// The factor lookup logic is taken from the BondTradeWindow.
		InvestmentSecurityEvent securityEvent = getInvestmentSecurityEventService().getInvestmentSecurityEventPreviousForAccrualEndDate(transferCommand.getSecurity().getId(), InvestmentSecurityEventType.FACTOR_CHANGE, transactionDate);
		if (securityEvent != null) {
			return securityEvent.getAfterEventValue();
		}
		securityEvent = getInvestmentSecurityEventService().getInvestmentSecurityEventForAccrualEndDate(transferCommand.getSecurity().getId(), InvestmentSecurityEventType.FACTOR_CHANGE, transactionDate);
		return securityEvent.getBeforeEventValue();
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                   Getters and Setters                  //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public AccountingPositionClosingService getAccountingPositionClosingService() {
		return this.accountingPositionClosingService;
	}


	public void setAccountingPositionClosingService(AccountingPositionClosingService accountingPositionClosingService) {
		this.accountingPositionClosingService = accountingPositionClosingService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
