package com.clifton.accounting.positiontransfer.booking;


import com.clifton.accounting.gl.booking.processor.TypeBasedRulesBookingProcessor;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationAware;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.violation.RuleViolationUtil;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.system.schema.SystemTable;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingPositionTransferBookingProcessor</code> class books AccountingPositionTransfer objects.
 *
 * @author vgomelsky
 */
public class AccountingPositionTransferBookingProcessor extends TypeBasedRulesBookingProcessor<AccountingPositionTransfer> {


	public static final String JOURNAL_NAME = "Transfer Journal";

	private AccountingPositionTransferService accountingPositionTransferService;
	private RuleViolationService ruleViolationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJournalTypeName() {
		return JOURNAL_NAME;
	}


	@Override
	public AccountingPositionTransfer getBookableEntity(Serializable id) {
		return getAccountingPositionTransferService().getAccountingPositionTransfer((Integer) id);
	}


	@Override
	protected String getBookingEntityType(AccountingPositionTransfer transfer) {
		InvestmentInstrumentHierarchy hierarchy = null;
		InvestmentSecurity security = null;
		for (AccountingPositionTransferDetail detail : transfer.getDetailList()) {
			if (hierarchy == null) {
				security = detail.getSecurity();
				hierarchy = security.getInstrument().getHierarchy();
			}
			else {
				ValidationUtils.assertTrue(hierarchy.equals(detail.getSecurity().getInstrument().getHierarchy()),
						"A single transfer does not support securities in different investment hierarchies. Split it into multiple transfers.");
			}
		}
		if (hierarchy == null) {
			throw new ValidationException("Cannot find investment hierarchy for security for position transfer: " + transfer);
		}


		String transferTypeName = transfer.getType().getName();
		boolean collateralTransfer = transfer.getType().isCollateral();
		boolean internalTransfer = (transfer.getFromHoldingInvestmentAccount() != null && transfer.getToHoldingInvestmentAccount() != null
				&& transfer.getFromHoldingInvestmentAccount().equals(transfer.getToHoldingInvestmentAccount()));

		// collateral and internal transfers use slightly different sets of booking rules
		if (internalTransfer) {
			transferTypeName = collateralTransfer ? "Collateral Internal Transfer" : "Internal Transfer";
		}
		else if (collateralTransfer) {
			transferTypeName = "Collateral Transfer";
		}

		// check for investment hierarchy or investment type specific overrides
		String overrideKey = getBookingRulesKeyOverride(transferTypeName, security);
		if (overrideKey != null) {
			return overrideKey;
		}

		return transferTypeName;
	}


	@Override
	public SimpleBookingSession<AccountingPositionTransfer> newBookingSession(AccountingJournal journal, AccountingPositionTransfer transfer) {
		if (transfer == null) {
			transfer = getBookableEntity(journal.getFkFieldId());
			ValidationUtils.assertNotNull(transfer, "Cannot find position transfer with id = " + journal.getFkFieldId());
		}
		SystemTable systemTable = transfer.getSourceSystemTable();
		if (systemTable != null) {
			ReadOnlyDAO<?> systemTableDao = getDaoLocator().locate(systemTable.getName());
			IdentityObject sourceBean = systemTableDao.findByPrimaryKey(transfer.getSourceFkFieldId());
			RuleViolationAware ruleViolationAwareBean = RuleViolationUtil.getRuleViolationAwareBean(sourceBean);
			if (ruleViolationAwareBean != null && ruleViolationAwareBean.getViolationStatus().isUnresolvedViolationsExist()) {
				if (isViolationCauseEntityEqual((Integer) ruleViolationAwareBean.getIdentity(), transfer.getId())) {
					throw new ValidationException("Cannot book transfer for [" + systemTable.getName() + " : " + transfer.getSourceFkFieldId() + "] " +
							"because the source has unresolved violation(s).");
				}
			}
		}
		return new SimpleBookingSession<>(journal, transfer);
	}


	@Override
	public AccountingPositionTransfer markBooked(AccountingPositionTransfer transfer) {
		ValidationUtils.assertNull(transfer.getBookingDate(), "Cannot book Position Transfer with id = " + transfer.getId() + " because it already has bookingDate set to " + transfer.getBookingDate());
		transfer.setBookingDate(new Date());
		return saveTransfer(transfer);
	}


	@Override
	public AccountingPositionTransfer markUnbooked(int positionTransferId) {
		AccountingPositionTransfer transfer = getBookableEntity(positionTransferId);
		transfer.setBookingDate(null);
		return saveTransfer(transfer);
	}


	@Override
	public void deleteSourceEntity(int positionTransferId) {
		// cannot delete an entity that's marked as booked (clear booked status first)
		getAccountingPositionTransferService().deleteAccountingPositionTransfer(positionTransferId);
	}


	/**
	 * Execute save directly to avoid redundant validation and rule execution
	 */
	private AccountingPositionTransfer saveTransfer(AccountingPositionTransfer transfer) {
		UpdatableDAO<AccountingPositionTransfer> dao = (UpdatableDAO<AccountingPositionTransfer>) getDaoLocator().locate(AccountingPositionTransfer.class);
		return DaoUtils.executeWithAllObserversDisabled(() -> dao.save(transfer));
	}


	private boolean isViolationCauseEntityEqual(int sourceBeanId, int transferId) {
		RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
		ruleViolationSearchForm.setLinkedFkFieldId(MathUtils.getNumberAsLong(sourceBeanId));
		List<RuleViolation> violationList = getRuleViolationService().getRuleViolationList(ruleViolationSearchForm);
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(violationList)) {
			if (MathUtils.isEqual(ruleViolation.getCauseFkFieldId(), transferId)) {
				return false;
			}
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
