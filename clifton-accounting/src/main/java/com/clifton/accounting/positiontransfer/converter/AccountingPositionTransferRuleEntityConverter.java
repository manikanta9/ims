package com.clifton.accounting.positiontransfer.converter;


import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferRuleDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.rule.converter.RuleEntityConverter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingPositionTransferRuleEntityConverter</code> converts position transfer details into 'AccountingPositionTransferRuleDetail'
 * that implement the 'AccountingBean' interface and can be used properly for rule evaluation.
 *
 * @author stevenf
 */
public class AccountingPositionTransferRuleEntityConverter implements RuleEntityConverter<AccountingPositionTransfer, AccountingPositionTransferRuleDetail> {

	private AccountingPositionTransferService accountingPositionTransferService;


	/**
	 * Iterate the transfer details and combine positions with same securities while before processing rules.
	 */
	@Override
	public List<AccountingPositionTransferRuleDetail> convertEntity(AccountingPositionTransfer transfer) {
		AssertUtils.assertNotNull(transfer, "A null transfer cannot be converted.");
		Map<Integer, AccountingPositionTransferRuleDetail> transferRuleDetailMap = new HashMap<>();
		if (transfer.getDetailList() == null) {
			//Attempt to load - it's possible it wasn't populated because the entity was loaded via the DAO.
			transfer = getAccountingPositionTransferService().getAccountingPositionTransfer(transfer.getId());
		}
		for (AccountingPositionTransferDetail transferDetail : CollectionUtils.getIterable(transfer.getDetailList())) {
			AccountingPositionTransferRuleDetail existingTransferRuleDetail = transferRuleDetailMap.get(transferDetail.getSecurity().getId());
			if (existingTransferRuleDetail == null) {
				existingTransferRuleDetail = new AccountingPositionTransferRuleDetail();
				if (transfer.getToClientInvestmentAccount() != null && transfer.getToHoldingInvestmentAccount() != null) {
					existingTransferRuleDetail.setClientInvestmentAccount(transfer.getToClientInvestmentAccount());
					existingTransferRuleDetail.setHoldingInvestmentAccount(transfer.getToHoldingInvestmentAccount());
				}
				else {
					existingTransferRuleDetail.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
					existingTransferRuleDetail.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
				}
				existingTransferRuleDetail.setSourceEntityId(transfer.getId());
				existingTransferRuleDetail.setBuy(MathUtils.isGreaterThanOrEqual(transferDetail.getCostPrice(), BigDecimal.ZERO));
				existingTransferRuleDetail.setExchangeRateToBase(transferDetail.getExchangeRateToBase());
				existingTransferRuleDetail.setInvestmentSecurity(transferDetail.getSecurity());
				existingTransferRuleDetail.setSettlementCurrency(transferDetail.getSecurity().getInstrument().getTradingCurrency());
				existingTransferRuleDetail.setSettlementDate(transfer.getSettlementDate());
				existingTransferRuleDetail.setTransactionDate(transfer.getTransactionDate());
			}
			existingTransferRuleDetail.setAccountingNotional(MathUtils.add(existingTransferRuleDetail.getAccountingNotional(), transferDetail.getPositionCostBasis()));
			existingTransferRuleDetail.setQuantity(MathUtils.add(existingTransferRuleDetail.getQuantity(), transferDetail.getQuantity()));
			existingTransferRuleDetail.setQuantityNormalized(MathUtils.add(existingTransferRuleDetail.getQuantityNormalized(),
					// Quantity field is null for currency transfers which have Local Balance in Position Cost Basis field: same logic as "Quantity Normalized"
					transferDetail.getQuantity() != null ? transferDetail.getQuantity() : transferDetail.getPositionCostBasis()));
			transferRuleDetailMap.put(existingTransferRuleDetail.getInvestmentSecurity().getId(), existingTransferRuleDetail);
		}
		return new ArrayList<>(transferRuleDetailMap.values());
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}
}
