package com.clifton.accounting.positiontransfer.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.commission.AccountingCommission;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationProperties;
import com.clifton.accounting.gl.booking.rule.AccountingCommissionBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingPositionTransferCommissionBookingRule</code> calculates commissions and fees for position transfers and adds them to the journal.
 *
 * @author jgommels
 */
public class AccountingPositionTransferCommissionBookingRule extends AccountingCommissionBookingRule<AccountingPositionTransfer> {

	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<AccountingPositionTransfer> bookingSession) {
		AccountingPositionTransfer transfer = bookingSession.getBookableEntity();
		Map<Integer, AccountingPositionTransferDetail> transferDetails = BeanUtils.getBeanMap(transfer.getDetailList(), AccountingPositionTransferDetail::getId);
		SystemTable transferDetailTable = getSystemSchemaService().getSystemTableByName("AccountingPositionTransferDetail");
		AccountingAccount commissionAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.EXPENSE_COMMISSION);

		List<AccountingJournalDetailDefinition> journalDetails = new ArrayList<>(bookingSession.getJournal().getJournalDetailList()); //Copy to prevent concurrent modification
		for (AccountingJournalDetailDefinition journalDetail : CollectionUtils.getIterable(journalDetails)) {
			if (journalDetail.getAccountingAccount().isPosition() && journalDetail.isParentDefinitionEmpty()) {
				AccountingPositionTransferDetail transferDetail = transferDetails.get(journalDetail.getFkFieldId());
				BigDecimal exchangeRateToBase = transferDetail == null ? null : transferDetail.getExchangeRateToBase();

				if (journalDetail.getSystemTable().equals(transferDetailTable) && transferDetail != null && transferDetail.getCommissionPerUnitOverride() != null) {
					BigDecimal commissionAmount = journalDetail.getQuantity().abs().multiply(transferDetail.getCommissionPerUnitOverride()).setScale(2, BigDecimal.ROUND_HALF_UP);
					AccountingCommission commission = new AccountingCommission(commissionAccount, null, commissionAmount);
					createJournalDetails(transfer, journalDetail, commission, exchangeRateToBase);
				}
				else {
					applyAutomatedCommissions(new AccountingCommissionCalculationProperties<>(journalDetail, transfer), exchangeRateToBase, null);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////                   Getter & Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
