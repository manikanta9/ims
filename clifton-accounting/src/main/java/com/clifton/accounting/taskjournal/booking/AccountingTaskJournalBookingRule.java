package com.clifton.accounting.taskjournal.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.taskjournal.AccountingTaskJournal;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AccountingTaskJournalBookingRule</code> class identifies all unique GL account entries from
 * the journal associated with the task journal and its parent.  It makes sure that only one security per
 * unique GL Account is used.  It also aggregates all Local Debit/Credit amounts and eliminates those that
 * sum to 0 (accrual/reversal, etc.).
 * <p/>
 * Generated transactions will remove these GL Account balances from the main account and move/transfer
 * them into the TO account.
 *
 * @author vgomelsky
 */
public class AccountingTaskJournalBookingRule implements AccountingBookingRule<AccountingTaskJournal> {

	private AccountingJournalService accountingJournalService;


	@Override
	public void applyRule(BookingSession<AccountingTaskJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingTaskJournal taskJournal = bookingSession.getBookableEntity();

		// DetailConfig has unique entry for each GL Account in corresponding journal: aggregate and skip 0's
		List<DetailConfig> detailList = new ArrayList<>();
		populateDetailConfig(detailList, taskJournal, taskJournal.getAccountingJournal().getId());
		if (taskJournal.getAccountingJournal().getParent() != null) {
			// process both: accrual and reversal
			populateDetailConfig(detailList, taskJournal, taskJournal.getAccountingJournal().getParent().getId());
		}

		// generate journal description: references security
		for (DetailConfig detail : detailList) {
			if (!detail.accountingAccount.isCurrency()) {
				journal.setDescription(taskJournal.getJournalType().getName() + ": " + detail.investmentSecurity.getSymbol());
				break;
			}
		}

		// update position FX Rate from non-position (closing positions have corresponding opening FX Rate)
		BigDecimal fxRate = null;
		for (DetailConfig detail : detailList) {
			if (!detail.accountingAccount.isPosition() && !(detail.accountingAccount.isCurrency() && !detail.accountingAccount.isCash())) {
				if (fxRate == null) {
					fxRate = detail.fxRate;
				}
				else if (!MathUtils.isEqual(fxRate, detail.fxRate)) {
					// multiple rates found: don't override positions
					fxRate = null;
					break;
				}
			}
		}

		// generate journal details: from and to
		for (DetailConfig detail : detailList) {
			if (fxRate != null && (detail.accountingAccount.isPosition() || (detail.accountingAccount.isCurrency() && !detail.accountingAccount.isCash()))) {
				detail.fxRate = fxRate;
			}
			journal.addJournalDetail(generateJournalDetail(journal, taskJournal, detail, true));
			journal.addJournalDetail(generateJournalDetail(journal, taskJournal, detail, false));
		}
	}


	private AccountingJournalDetail generateJournalDetail(AccountingJournal journal, AccountingTaskJournal taskJournal, DetailConfig detailConfig, boolean transferOut) {
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(journal.getFkFieldId());
		detail.setSystemTable(journal.getSystemTable());
		detail.setClientInvestmentAccount(transferOut ? taskJournal.getClientInvestmentAccount() : taskJournal.getToClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(transferOut ? taskJournal.getHoldingInvestmentAccount() : taskJournal.getToHoldingInvestmentAccount());
		detail.setInvestmentSecurity(detailConfig.investmentSecurity);
		detail.setAccountingAccount(detailConfig.accountingAccount);
		detail.setDescription(journal.getDescription());

		BigDecimal quantity = taskJournal.getQuantity();
		detail.setQuantity((quantity == null || detail.getAccountingAccount().isCurrency()) ? null : (transferOut ? quantity.negate() : quantity));
		detail.setPrice(null);
		boolean debitPlus = detail.getAccountingAccount().getAccountType().isGrowingOnDebitSide();
		detail.setLocalDebitCredit((transferOut && debitPlus) || (!transferOut && !debitPlus) ? taskJournal.getAmount().negate() : taskJournal.getAmount());
		detail.setExchangeRateToBase(detailConfig.fxRate);
		detail.setBaseDebitCredit(MathUtils.multiply(detail.getLocalDebitCredit(), detail.getExchangeRateToBase(), 2));

		// adjust Base Debit/Credit for rounding if necessary
		BigDecimal baseDebitCredit = detailConfig.baseDebitCredit;
		if (!MathUtils.isSameSignum(baseDebitCredit, detail.getBaseDebitCredit())) {
			baseDebitCredit = baseDebitCredit.negate();
		}
		BigDecimal threshold = new BigDecimal("0.05");
		if (MathUtils.isLessThan(MathUtils.abs(MathUtils.subtract(baseDebitCredit, detail.getBaseDebitCredit())), threshold)) {
			detail.setBaseDebitCredit(baseDebitCredit);
		}

		if (detail.getAccountingAccount().isPosition()) {
			detail.setPositionCostBasis(detail.getLocalDebitCredit()); // currency
		}
		else {
			detail.setPositionCostBasis(BigDecimal.ZERO);
		}

		detail.setTransactionDate(taskJournal.getTransactionDate());
		detail.setOriginalTransactionDate(taskJournal.getTransactionDate());
		detail.setSettlementDate(taskJournal.getSettlementDate());

		// questionable fields other than possibly cost basis (may need to deleted later)
		detail.setOpening(true);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}


	/**
	 * Adds a new DetailConfig to the specified list if new GL Account is found (matches client/holding accounts to task journal).
	 * Aggregates Local Debit/Credit if one is already present and removes entries with 0 balances.
	 *
	 * @param detailList
	 * @param taskJournal
	 * @param accountingJournalId
	 */
	private void populateDetailConfig(List<DetailConfig> detailList, AccountingTaskJournal taskJournal, long accountingJournalId) {
		AccountingJournal journal = getAccountingJournalService().getAccountingJournal(accountingJournalId);
		for (AccountingJournalDetailDefinition journalDetail : journal.getJournalDetailList()) {
			// skip details for different client/holding account
			if (journalDetail.getClientInvestmentAccount().equals(taskJournal.getClientInvestmentAccount())
					&& journalDetail.getHoldingInvestmentAccount().equals(taskJournal.getHoldingInvestmentAccount())) {
				if (journalDetail.getAccountingAccount().isCurrencyTranslation()) {
					// skip currency translation GL accounts (uses proper FX Rate)
					continue;
				}
				// look for existing entry to see if aggregation is needed
				boolean present = false;
				for (DetailConfig detail : detailList) {
					if (journalDetail.getAccountingAccount().equals(detail.accountingAccount)) {
						if (!journalDetail.getInvestmentSecurity().equals(detail.investmentSecurity)) {
							throw new ValidationException("Journal with id = " + accountingJournalId + " cannot use different securities for same GL Account: "
									+ journalDetail.getAccountingAccount().getLabel());
						}
						detail.localDebitCredit = MathUtils.add(detail.localDebitCredit, journalDetail.getLocalDebitCredit());
						detail.baseDebitCredit = MathUtils.add(detail.baseDebitCredit, journalDetail.getBaseDebitCredit());
						if (MathUtils.isEqual(detail.localDebitCredit, BigDecimal.ZERO)) {
							detailList.remove(detail);
						}
						present = true;
						break;
					}
				}
				if (!present) {
					DetailConfig detail = new DetailConfig();
					detail.accountingAccount = journalDetail.getAccountingAccount();
					detail.investmentSecurity = journalDetail.getInvestmentSecurity();
					detail.fxRate = journalDetail.getExchangeRateToBase();
					detail.localDebitCredit = journalDetail.getLocalDebitCredit();
					detail.baseDebitCredit = journalDetail.getBaseDebitCredit();
					detailList.add(detail);
				}
			}
		}
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	class DetailConfig {

		AccountingAccount accountingAccount;
		InvestmentSecurity investmentSecurity;
		BigDecimal fxRate;
		BigDecimal localDebitCredit; // used for aggregation to eliminate accrual/reversals
		BigDecimal baseDebitCredit; // adjusts for penny rounding due to aggregation
	}
}
