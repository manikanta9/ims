package com.clifton.accounting.taskjournal.generator;


import com.clifton.accounting.taskjournal.AccountingTaskJournal;
import com.clifton.accounting.taskjournal.AccountingTaskJournalService;
import com.clifton.accounting.taskjournal.AccountingTaskJournalType;
import com.clifton.accounting.taskjournal.search.AccountingTaskJournalSearchForm;
import com.clifton.accounting.taskjournal.search.AccountingTaskJournalTypeSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.clifton.core.util.date.DateUtils.fromDate;
import static com.clifton.core.util.status.Status.ofMessage;


/**
 * The <code>AccountingTaskJournalGeneratorServiceImpl</code> class provides basic implementation
 * of the AccountingTaskJournalGeneratorService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingTaskJournalGeneratorServiceImpl implements AccountingTaskJournalGeneratorService {

	private AccountingTaskJournalService accountingTaskJournalService;
	private SystemBeanService systemBeanService;

	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status generateAccountingTaskJournalList(AccountingTaskJournalGeneratorCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Synchronously running for " + command);
			doGenerateAccountingTaskJournalList(command, status);
			return status;
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("TASK-JOURNALS", runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					doGenerateAccountingTaskJournalList(command, statusHolder.getStatus());
				}
				catch (Throwable e) {
					getStatus().setMessage("Error generating task journals for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error generating task journals for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);

		return Status.ofMessage("Started Task Journal generation: " + runId + ". Processing will be completed shortly.");
	}


	private void doGenerateAccountingTaskJournalList(AccountingTaskJournalGeneratorCommand command, Status status) {
		ValidationUtils.assertNotNull(command.getFromDate(), "[fromDate] is request to generate task journals.", "fromDate");
		ValidationUtils.assertNotNull(command.getToDate(), "[toDate] is request to generate task journals.", "toDate");

		Short[] journalTypeIds;
		if (command.getTaskJournalTypeId() != null) {
			journalTypeIds = new Short[]{command.getTaskJournalTypeId()};
		}
		else {
			List<AccountingTaskJournalType> typeList = getAccountingTaskJournalService().getAccountingTaskJournalTypeList(new AccountingTaskJournalTypeSearchForm());
			journalTypeIds = BeanUtils.getBeanIdentityArray(typeList, Short.class);
		}

		int totalCount = 0;
		for (Short journalTypeId : journalTypeIds) {
			command.setTaskJournalTypeId(journalTypeId);
			totalCount += generateAccountingTaskJournalListForType(command, status);
		}
		status.setMessage(totalCount + " generated; " + status.getSkippedCount() + " skipped; " + status.getErrorCount() + " errors.");
	}


	private int generateAccountingTaskJournalListForType(AccountingTaskJournalGeneratorCommand command, Status status) {
		int generateCount = 0;
		int skipCount = 0;
		AccountingTaskJournalType type = getAccountingTaskJournalService().getAccountingTaskJournalType(command.getTaskJournalTypeId());
		AccountingTaskJournalGenerator generator = (AccountingTaskJournalGenerator) getSystemBeanService().getBeanInstance(type.getJournalGeneratorBean());
		List<AccountingTaskJournal> journalList = generator.generateTaskJournals(type, command.getFromDate(), command.getToDate(), status);

		for (AccountingTaskJournal journal : CollectionUtils.getIterable(journalList)) {
			// make sure this is not a duplicate for same journal, type, holding and account
			AccountingTaskJournalSearchForm searchForm = new AccountingTaskJournalSearchForm();
			searchForm.setJournalTypeId(command.getTaskJournalTypeId());
			searchForm.setAccountingJournalId(journal.getAccountingJournal().getId());
			searchForm.setClientInvestmentAccountId(journal.getClientInvestmentAccount().getId());
			searchForm.setHoldingInvestmentAccountId(journal.getHoldingInvestmentAccount().getId());
			List<AccountingTaskJournal> existingList = getAccountingTaskJournalService().getAccountingTaskJournalList(searchForm);
			if (CollectionUtils.isEmpty(existingList)) {
				getAccountingTaskJournalService().saveAccountingTaskJournal(journal);
				generateCount++;
			}
			else {
				status.addSkipped("Already exists: " + existingList);
				skipCount++;
			}
			status.setMessage(CollectionUtils.getSize(journalList) + " total; " + generateCount + " generated; " + skipCount + " skipped; " + status.getErrorCount() + " errors; type: " + type);
		}

		status.addMessage(generateCount + " task journals generated and " + skipCount + " skipped for " + type);
		return generateCount;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTaskJournalService getAccountingTaskJournalService() {
		return this.accountingTaskJournalService;
	}


	public void setAccountingTaskJournalService(AccountingTaskJournalService accountingTaskJournalService) {
		this.accountingTaskJournalService = accountingTaskJournalService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
