package com.clifton.accounting.taskjournal.generator;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.taskjournal.AccountingTaskJournal;
import com.clifton.accounting.taskjournal.AccountingTaskJournalType;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingTaskJournalGeneratorForEventJournals</code> class generates a task journal
 * for the specified event types that have been booked.  It only generates journals for holding
 * account of the specified type.
 * <p>
 * For example, generate a task journal for each "REPO Account" entry of  booked "Cash Coupon Payment" journal.
 *
 * @author vgomelsky
 */
public class AccountingTaskJournalGeneratorForEventJournals implements AccountingTaskJournalGenerator {

	private AccountingJournalService accountingJournalService;
	private AccountingEventJournalService accountingEventJournalService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Restrict task journal generation to event journals of this type.
	 * For example, "Cash Coupon Payment".
	 */
	private String eventJournalTypeName;
	/**
	 * Generate task journals for transactions with holding accounts of the following type.
	 * For example, "REPO Account", "Collateral", etc.
	 */
	private String holdingAccountType;

	/**
	 * Use this purpose to find the TARGET Holding Investment Accounts for task journals.
	 * Note, you can specify multiple purposes separated by a comma. The first valid purpose will be used.
	 * For example, "Custodian", "Collateral,Custodian", etc.
	 */
	private String toAccountPurpose;

	/**
	 * Number of business days from transaction date to settlement date.
	 * Event Date (payment date) is used for transaction date.
	 */
	private int settlementCycle;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingTaskJournal> generateTaskJournals(AccountingTaskJournalType taskJournalType, Date fromDate, Date toDate, Status status) {
		// get booked event journals of the specified type for the date range
		AccountingEventJournalSearchForm searchForm = new AccountingEventJournalSearchForm();
		searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, fromDate));
		searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.LESS_THAN_OR_EQUALS, toDate));
		searchForm.setJournalTypeName(getEventJournalTypeName());
		searchForm.setJournalBooked(true);
		searchForm.setAccrualReversalBooked(true); // make sure the payment was booked
		List<AccountingEventJournal> eventJournalList = getAccountingEventJournalService().getAccountingEventJournalList(searchForm);

		List<AccountingTaskJournal> result = new ArrayList<>();
		for (AccountingEventJournal eventJournal : CollectionUtils.getIterable(eventJournalList)) {
			// get the last journal posted (payment): limit transactions to the specified holdingAccountType
			AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence(AccountingEventJournal.class.getSimpleName(), eventJournal.getId(),
					eventJournal.isAccrualReversalCombined() ? 1 : 2);
			// aggregate amounts/quantity grouped by holding account id
			Map<Integer, AccountingTaskJournal> resultMap = new HashMap<>();
			for (AccountingJournalDetailDefinition detail : journal.getJournalDetailList()) {
				if (detail.getHoldingInvestmentAccount().getType().getName().equals(getHoldingAccountType())) {
					// skip cash/currency and receivable transactions to avoid double counting
					AccountingAccount accountingAccount = detail.getAccountingAccount();
					if (!accountingAccount.isCurrency()) {
						try {
							AccountingTaskJournal taskJournal = resultMap.get(detail.getHoldingInvestmentAccount().getId());
							if (taskJournal == null) {
								taskJournal = new AccountingTaskJournal();
								taskJournal.setJournalType(taskJournalType);
								taskJournal.setAccountingJournal(journal);
								taskJournal.setTransactionDate(eventJournal.getSecurityEvent().getEventDate());
								taskJournal.setSettlementDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(taskJournal.getTransactionDate()), getSettlementCycle()));

								taskJournal.setInvestmentSecurity(eventJournal.getSecurityEvent().getSecurity());
								taskJournal.setClientInvestmentAccount(detail.getClientInvestmentAccount());
								taskJournal.setHoldingInvestmentAccount(detail.getHoldingInvestmentAccount());
								// assume same client account for now
								taskJournal.setToClientInvestmentAccount(taskJournal.getClientInvestmentAccount());
								taskJournal.setToHoldingInvestmentAccount(getToHoldingInvestmentAccount(taskJournal, eventJournal));

								resultMap.put(taskJournal.getHoldingInvestmentAccount().getId(), taskJournal);
							}

							if (accountingAccount.getAccountType().isGrowingOnDebitSide()) {
								// receivable
								taskJournal.setQuantity(MathUtils.subtract(taskJournal.getQuantity(), detail.getQuantity()));
							}
							else {
								// income statement
								taskJournal.setQuantity(MathUtils.add(taskJournal.getQuantity(), detail.getQuantity()));
							}
							taskJournal.setAmount(MathUtils.subtract(taskJournal.getAmount(), detail.getLocalDebitCredit()));
						}
						catch (Throwable e) {
							if (status == null) {
								throw e;
							}
							status.addError(ExceptionUtils.getDetailedMessage(e));
						}
					}
				}
			}
			result.addAll(resultMap.values());
		}

		return result;
	}


	private InvestmentAccount getToHoldingInvestmentAccount(AccountingTaskJournal taskJournal, AccountingEventJournal eventJournal) {
		String[] purposeList = getToAccountPurpose().split(",");
		List<ValidationException> exceptionList = null;

		for (String purpose : purposeList) {
			try {
				return getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurposeStrict(
						taskJournal.getClientInvestmentAccount().getId(), taskJournal.getHoldingInvestmentAccount().getId(), eventJournal.getSecurityEvent().getSecurity().getId(),
						StringUtils.trim(purpose), taskJournal.getTransactionDate());
			}
			catch (ValidationException e) {
				if (exceptionList == null) {
					exceptionList = new ArrayList<>();
				}
				exceptionList.add(e);
			}
		}

		if (exceptionList != null && exceptionList.size() == 1) {
			throw exceptionList.get(0);
		}
		throw new ValidationException("Error looking up related Holding Account for multiple purposes: " + ArrayUtils.toString(purposeList)
				+ ".\n\n" + BeanUtils.getPropertyValues(exceptionList, "message", ", ")
		);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}


	public String getEventJournalTypeName() {
		return this.eventJournalTypeName;
	}


	public void setEventJournalTypeName(String eventJournalTypeName) {
		this.eventJournalTypeName = eventJournalTypeName;
	}


	public String getHoldingAccountType() {
		return this.holdingAccountType;
	}


	public void setHoldingAccountType(String holdingAccountType) {
		this.holdingAccountType = holdingAccountType;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public String getToAccountPurpose() {
		return this.toAccountPurpose;
	}


	public void setToAccountPurpose(String toAccountPurpose) {
		this.toAccountPurpose = toAccountPurpose;
	}


	public int getSettlementCycle() {
		return this.settlementCycle;
	}


	public void setSettlementCycle(int settlementCycle) {
		this.settlementCycle = settlementCycle;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}
}
