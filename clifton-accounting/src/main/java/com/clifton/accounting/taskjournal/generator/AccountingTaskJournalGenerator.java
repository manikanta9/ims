package com.clifton.accounting.taskjournal.generator;


import com.clifton.accounting.taskjournal.AccountingTaskJournal;
import com.clifton.accounting.taskjournal.AccountingTaskJournalType;
import com.clifton.core.util.status.Status;

import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingTaskJournalGenerator</code> interface should be implemented by various AccountTaskJournalType objects.
 * For example: AccountingTaskJournalGeneratorForEventJournals.
 *
 * @author vgomelsky
 */
public interface AccountingTaskJournalGenerator {

	/**
	 * Returns a List of AccountingTaskJournal objects for the specified date range.
	 * Specific implementations will further limit task journals returned and will provide custom population logic.
	 *
	 * @param taskJournalType
	 * @param fromDate
	 * @param toDate
	 * @param status          if not null, adds errors for failed Task Journals to this object.  Otherwise, throws an exception.
	 */
	public List<AccountingTaskJournal> generateTaskJournals(AccountingTaskJournalType taskJournalType, Date fromDate, Date toDate, Status status);
}
