package com.clifton.accounting.taskjournal;


import com.clifton.accounting.taskjournal.search.AccountingTaskJournalSearchForm;
import com.clifton.accounting.taskjournal.search.AccountingTaskJournalTypeSearchForm;

import java.util.List;


/**
 * The <code>AccountingTaskJournalService</code> interface defines methods for working with Task Journals
 * and related entities.
 *
 * @author vgomelsky
 */
public interface AccountingTaskJournalService {

	////////////////////////////////////////////////////////////////////////////
	////////         AccountingTaskJournal Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTaskJournal getAccountingTaskJournal(int id);


	public List<AccountingTaskJournal> getAccountingTaskJournalList(AccountingTaskJournalSearchForm searchForm);


	public AccountingTaskJournal saveAccountingTaskJournal(AccountingTaskJournal bean);


	public void deleteAccountingTaskJournal(int id);

	////////////////////////////////////////////////////////////////////////////
	////////      AccountingTaskJournalType Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTaskJournalType getAccountingTaskJournalType(short id);


	public AccountingTaskJournalType getAccountingTaskJournalTypeByName(String name);


	public List<AccountingTaskJournalType> getAccountingTaskJournalTypeList(AccountingTaskJournalTypeSearchForm searchForm);


	public AccountingTaskJournalType saveAccountingTaskJournalType(AccountingTaskJournalType bean);


	public void deleteAccountingTaskJournalType(short id);
}
