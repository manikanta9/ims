package com.clifton.accounting.taskjournal;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>AccountingTaskJournalType</code> class defines a type and corresponding attributes
 * and behavior of AccountingTaskJournal.
 * <p/>
 * Examples: "REPO Coupon Transfer", "Collateral Coupon Transfer"
 *
 * @author vgomelsky
 */
public class AccountingTaskJournalType extends NamedEntity<Short> {

	/**
	 * A SystemBean of AccountingTaskJournalGenerator type responsible for AccountingTaskJournal
	 * generation for this type.
	 */
	private SystemBean journalGeneratorBean;

	private String amountFieldName;


	public SystemBean getJournalGeneratorBean() {
		return this.journalGeneratorBean;
	}


	public void setJournalGeneratorBean(SystemBean journalGeneratorBean) {
		this.journalGeneratorBean = journalGeneratorBean;
	}


	public String getAmountFieldName() {
		return this.amountFieldName;
	}


	public void setAmountFieldName(String amountFieldName) {
		this.amountFieldName = amountFieldName;
	}
}
