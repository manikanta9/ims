package com.clifton.accounting.taskjournal;


import com.clifton.accounting.taskjournal.search.AccountingTaskJournalSearchForm;
import com.clifton.accounting.taskjournal.search.AccountingTaskJournalTypeSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>AccountingTaskJournalServiceImpl</code> class provides basic implementation of the
 * AccountingTaskJournalService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingTaskJournalServiceImpl implements AccountingTaskJournalService {

	private AdvancedUpdatableDAO<AccountingTaskJournal, Criteria> accountingTaskJournalDAO;
	private AdvancedUpdatableDAO<AccountingTaskJournalType, Criteria> accountingTaskJournalTypeDAO;


	////////////////////////////////////////////////////////////////////////////
	////////         AccountingTaskJournal Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingTaskJournal getAccountingTaskJournal(int id) {
		return getAccountingTaskJournalDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingTaskJournal> getAccountingTaskJournalList(AccountingTaskJournalSearchForm searchForm) {
		return getAccountingTaskJournalDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	// see {@link AccountingTaskJournalValidator} for validation details
	@Override
	public AccountingTaskJournal saveAccountingTaskJournal(AccountingTaskJournal bean) {
		return getAccountingTaskJournalDAO().save(bean);
	}


	// see {@link AccountingTaskJournalValidator} for validation details
	@Override
	public void deleteAccountingTaskJournal(int id) {
		getAccountingTaskJournalDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////      AccountingTaskJournalType Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingTaskJournalType getAccountingTaskJournalType(short id) {
		return getAccountingTaskJournalTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingTaskJournalType getAccountingTaskJournalTypeByName(String name) {
		return getAccountingTaskJournalTypeDAO().findOneByField("name", name);
	}


	@Override
	public List<AccountingTaskJournalType> getAccountingTaskJournalTypeList(AccountingTaskJournalTypeSearchForm searchForm) {
		return getAccountingTaskJournalTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingTaskJournalType saveAccountingTaskJournalType(AccountingTaskJournalType bean) {
		return getAccountingTaskJournalTypeDAO().save(bean);
	}


	@Override
	public void deleteAccountingTaskJournalType(short id) {
		getAccountingTaskJournalTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingTaskJournalType, Criteria> getAccountingTaskJournalTypeDAO() {
		return this.accountingTaskJournalTypeDAO;
	}


	public void setAccountingTaskJournalTypeDAO(AdvancedUpdatableDAO<AccountingTaskJournalType, Criteria> accountingTaskJournalTypeDAO) {
		this.accountingTaskJournalTypeDAO = accountingTaskJournalTypeDAO;
	}


	public AdvancedUpdatableDAO<AccountingTaskJournal, Criteria> getAccountingTaskJournalDAO() {
		return this.accountingTaskJournalDAO;
	}


	public void setAccountingTaskJournalDAO(AdvancedUpdatableDAO<AccountingTaskJournal, Criteria> accountingTaskJournalDAO) {
		this.accountingTaskJournalDAO = accountingTaskJournalDAO;
	}
}
