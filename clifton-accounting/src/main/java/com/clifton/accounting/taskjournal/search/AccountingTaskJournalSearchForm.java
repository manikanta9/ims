package com.clifton.accounting.taskjournal.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class AccountingTaskJournalSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "journalType.id")
	private Short journalTypeId;

	@SearchField(searchField = "accountingJournal.id")
	private Long accountingJournalId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.id")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingInvestmentAccount")
	private Integer holdingInvestmentAccountIssuerId;

	@SearchField(searchField = "toClientInvestmentAccount.id")
	private Integer toClientInvestmentAccountId;

	@SearchField(searchField = "toHoldingInvestmentAccount.id")
	private Integer toHoldingInvestmentAccountId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "toHoldingInvestmentAccount")
	private Integer toHoldingInvestmentAccountIssuerId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal amount;

	@SearchField
	private Date transactionDate;

	@SearchField
	private Date settlementDate;

	@SearchField
	private Date bookingDate;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean journalBooked;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean journalNotBooked;


	public Short getJournalTypeId() {
		return this.journalTypeId;
	}


	public void setJournalTypeId(Short journalTypeId) {
		this.journalTypeId = journalTypeId;
	}


	public Long getAccountingJournalId() {
		return this.accountingJournalId;
	}


	public void setAccountingJournalId(Long accountingJournalId) {
		this.accountingJournalId = accountingJournalId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getToClientInvestmentAccountId() {
		return this.toClientInvestmentAccountId;
	}


	public void setToClientInvestmentAccountId(Integer toClientInvestmentAccountId) {
		this.toClientInvestmentAccountId = toClientInvestmentAccountId;
	}


	public Integer getToHoldingInvestmentAccountId() {
		return this.toHoldingInvestmentAccountId;
	}


	public void setToHoldingInvestmentAccountId(Integer toHoldingInvestmentAccountId) {
		this.toHoldingInvestmentAccountId = toHoldingInvestmentAccountId;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Boolean getJournalBooked() {
		return this.journalBooked;
	}


	public void setJournalBooked(Boolean journalBooked) {
		this.journalBooked = journalBooked;
	}


	public Boolean getJournalNotBooked() {
		return this.journalNotBooked;
	}


	public void setJournalNotBooked(Boolean journalNotBooked) {
		this.journalNotBooked = journalNotBooked;
	}


	public Integer getHoldingInvestmentAccountIssuerId() {
		return this.holdingInvestmentAccountIssuerId;
	}


	public void setHoldingInvestmentAccountIssuerId(Integer holdingInvestmentAccountIssuerId) {
		this.holdingInvestmentAccountIssuerId = holdingInvestmentAccountIssuerId;
	}


	public Integer getToHoldingInvestmentAccountIssuerId() {
		return this.toHoldingInvestmentAccountIssuerId;
	}


	public void setToHoldingInvestmentAccountIssuerId(Integer toHoldingInvestmentAccountIssuerId) {
		this.toHoldingInvestmentAccountIssuerId = toHoldingInvestmentAccountIssuerId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}
}
