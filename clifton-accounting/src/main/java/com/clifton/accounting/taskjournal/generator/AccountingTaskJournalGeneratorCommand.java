package com.clifton.accounting.taskjournal.generator;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;

import java.util.Date;


public class AccountingTaskJournalGeneratorCommand {

	private Short taskJournalTypeId;
	private Date fromDate;
	private Date toDate;

	private boolean synchronous;

	// allows to send "live" updates of rebuild status back to the caller (assume the caller sets this field)
	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return getRunId();
	}


	public String getRunId() {
		StringBuilder runId = new StringBuilder("");
		if (getTaskJournalTypeId() != null) {
			runId.append("JT_").append(getTaskJournalTypeId());
		}
		else {
			runId.append("ALL");
		}
		runId.append("-").append(DateUtils.fromDateShort(getFromDate()));
		runId.append("-").append(DateUtils.fromDateShort(getToDate()));
		return runId.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getTaskJournalTypeId() {
		return this.taskJournalTypeId;
	}


	public void setTaskJournalTypeId(Short taskJournalTypeId) {
		this.taskJournalTypeId = taskJournalTypeId;
	}


	public Date getFromDate() {
		return this.fromDate;
	}


	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	public Date getToDate() {
		return this.toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
