package com.clifton.accounting.taskjournal.validation;


import com.clifton.accounting.taskjournal.AccountingTaskJournal;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>AccountingTaskJournalValidator</code> class validates updates to AccountingTaskJournal according to business rules.
 *
 * @author vgomelsky
 */
@Component
public class AccountingTaskJournalValidator extends SelfRegisteringDaoValidator<AccountingTaskJournal> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(AccountingTaskJournal bean, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			if (bean.getBookingDate() != null) {
				throw new ValidationException("Cannot delete task journal: " + bean.getLabel() + " that has been booked.");
			}
		}
		else {
			if (config.isUpdate()) {
				AccountingTaskJournal originalBean = getOriginalBean(bean);
				if (bean.getBookingDate() != null && originalBean.getBookingDate() != null) {
					List<String> diffs = CoreCompareUtils.getNoEqualProperties(bean, originalBean, false, "workflowState", "workflowStatus");
					if (!CollectionUtils.isEmpty(diffs)) {
						throw new ValidationException("Cannot modify task journal: " + bean.getLabel() + " that has been booked. Fields: " + diffs);
					}
				}
			}
		}
	}
}
