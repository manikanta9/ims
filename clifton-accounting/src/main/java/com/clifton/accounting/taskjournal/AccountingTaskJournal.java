package com.clifton.accounting.taskjournal;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.workflow.BaseWorkflowAwareEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The <code>AccountingTaskJournal</code> class is a task journal which is automatically generated based on
 * the logic associated with its type.  The journal also has a workflow associated with it that it must go through.
 * <p/>
 * For example, every time we get a Coupon Payment in a REPO account, automatically create a task journal that
 * will transfer that payment into corresponding Custodian account.  Use the workflow to instruct and confirm
 * payment transfer and finally receipt.
 *
 * @author vgomelsky
 */
public class AccountingTaskJournal extends BaseWorkflowAwareEntity<Integer> implements BookableEntity {

	private AccountingTaskJournalType journalType;

	/**
	 * The journal that this task journal is based on (parent journal after posting).
	 * For example, Cash Coupon Payment AccountingJournal for a Cash Coupon Transfer task.
	 */
	private AccountingJournal accountingJournal;
	/**
	 * Limits the client account from accountingJournal to this account in case multiple were used.
	 */
	private InvestmentAccount clientInvestmentAccount;
	/**
	 * Limits the holding account from accountingJournal to this account in case multiple were used.
	 */
	private InvestmentAccount holdingInvestmentAccount;
	/**
	 * In case of transfers (coupon payment move from REPO to Custodian account, etc.), identifies the target account.
	 */
	private InvestmentAccount toClientInvestmentAccount;
	/**
	 * In case of transfers (coupon payment move from REPO to Custodian account, etc.), identifies the target account.
	 */
	private InvestmentAccount toHoldingInvestmentAccount;
	/**
	 * Limits task journal to the following security (event journal security, etc.).
	 * NOTE: if we ever start generating multi-security journals, will add IsSingleInvestmentSecurity to the type.
	 */
	private InvestmentSecurity investmentSecurity;

	/**
	 * Optional affected quantity (face on which coupon is transferred, etc.)
	 */
	private BigDecimal quantity;
	/**
	 * Affected amount (transfer amount, etc.). Uses FX Rate from accountingJournal.
	 */
	private BigDecimal amount;

	/**
	 * Transaction date for all details of this task journal.
	 */
	private Date transactionDate;
	/**
	 * Settlement date for all details of this task journal.
	 */
	private Date settlementDate;

	/**
	 * The date when this simple journal was moved into AccountingJournal. This field is set by the system
	 * after the event occurs and prevents journal from being modified after it was set.
	 */
	private Date bookingDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		result.append(CoreMathUtils.formatNumberMoney(this.amount));
		result.append(' ');
		if (this.journalType != null) {
			result.append(this.journalType.getLabel());
			result.append(": ");
		}
		result.append(getId());
		return result.toString();
	}


	@Override
	public Set<IdentityObject> getEntityLockSet() {
		return CollectionUtils.createHashSet(getClientInvestmentAccount(), getToClientInvestmentAccount());
	}


	@Override
	public Date getBookingDate() {
		return this.bookingDate;
	}


	@Override
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public AccountingTaskJournalType getJournalType() {
		return this.journalType;
	}


	public void setJournalType(AccountingTaskJournalType journalType) {
		this.journalType = journalType;
	}


	public AccountingJournal getAccountingJournal() {
		return this.accountingJournal;
	}


	public void setAccountingJournal(AccountingJournal accountingJournal) {
		this.accountingJournal = accountingJournal;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public InvestmentAccount getToClientInvestmentAccount() {
		return this.toClientInvestmentAccount;
	}


	public void setToClientInvestmentAccount(InvestmentAccount toClientInvestmentAccount) {
		this.toClientInvestmentAccount = toClientInvestmentAccount;
	}


	public InvestmentAccount getToHoldingInvestmentAccount() {
		return this.toHoldingInvestmentAccount;
	}


	public void setToHoldingInvestmentAccount(InvestmentAccount toHoldingInvestmentAccount) {
		this.toHoldingInvestmentAccount = toHoldingInvestmentAccount;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}
}
