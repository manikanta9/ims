package com.clifton.accounting.taskjournal.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class AccountingTaskJournalTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "journalGeneratorBean.id")
	private Integer journalGeneratorBeanId;

	@SearchField
	private String amountFieldName;


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getJournalGeneratorBeanId() {
		return this.journalGeneratorBeanId;
	}


	public void setJournalGeneratorBeanId(Integer journalGeneratorBeanId) {
		this.journalGeneratorBeanId = journalGeneratorBeanId;
	}


	public String getAmountFieldName() {
		return this.amountFieldName;
	}


	public void setAmountFieldName(String amountFieldName) {
		this.amountFieldName = amountFieldName;
	}
}
