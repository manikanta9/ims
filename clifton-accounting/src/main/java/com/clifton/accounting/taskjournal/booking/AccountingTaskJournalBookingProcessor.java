package com.clifton.accounting.taskjournal.booking;


import com.clifton.accounting.gl.booking.processor.SameRulesBookingProcessor;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.taskjournal.AccountingTaskJournal;
import com.clifton.accounting.taskjournal.AccountingTaskJournalService;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>AccountingTaskJournalBookingProcessor</code> class is a booking processor for accounting
 * journals of type "Task Journal".  Each task journal type has custom logic for task journal generation.
 *
 * @author vgomelsky
 */
public class AccountingTaskJournalBookingProcessor extends SameRulesBookingProcessor<AccountingTaskJournal> {

	public static final String JOURNAL_NAME = "Task Journal";

	private AccountingTaskJournalService accountingTaskJournalService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJournalTypeName() {
		return JOURNAL_NAME;
	}


	@Override
	public AccountingTaskJournal getBookableEntity(Serializable id) {
		return getAccountingTaskJournalService().getAccountingTaskJournal((Integer) id);
	}


	@Override
	public BookingSession<AccountingTaskJournal> newBookingSession(AccountingJournal journal, AccountingTaskJournal taskJournal) {
		if (taskJournal == null) {
			taskJournal = getBookableEntity(journal.getFkFieldId());
			ValidationUtils.assertNotNull(taskJournal, "Cannot find Accounting Task Journal with id = " + journal.getFkFieldId());
		}

		journal.setParent(taskJournal.getAccountingJournal());

		return new SimpleBookingSession<>(journal, taskJournal);
	}


	@Override
	public AccountingTaskJournal markBooked(AccountingTaskJournal taskJournal) {
		ValidationUtils.assertNull(taskJournal.getBookingDate(),
				"Cannot book Task Journal with id = " + taskJournal.getId() + " because it already has bookingDate set to " + taskJournal.getBookingDate());
		taskJournal.setBookingDate(new Date());
		return getAccountingTaskJournalService().saveAccountingTaskJournal(taskJournal);
	}


	@Override
	public AccountingTaskJournal markUnbooked(int taskJournalId) {
		AccountingTaskJournal taskJournal = getBookableEntity(taskJournalId);
		// TODO: workflow state validation?  If not draft, then can't edit?
		taskJournal.setBookingDate(null);
		return getAccountingTaskJournalService().saveAccountingTaskJournal(taskJournal);
	}


	@Override
	public void deleteSourceEntity(int taskJournalId) {
		throw new IllegalArgumentException("Cannot delete task journal entries from unbooking process.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTaskJournalService getAccountingTaskJournalService() {
		return this.accountingTaskJournalService;
	}


	public void setAccountingTaskJournalService(AccountingTaskJournalService accountingTaskJournalService) {
		this.accountingTaskJournalService = accountingTaskJournalService;
	}
}
