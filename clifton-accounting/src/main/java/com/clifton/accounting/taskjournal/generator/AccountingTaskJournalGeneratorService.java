package com.clifton.accounting.taskjournal.generator;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * The <code>AccountingTaskJournalGeneratorService</code> interface defines methods that generate AccountingTaskJournal
 * objects.
 *
 * @author vgomelsky
 */
public interface AccountingTaskJournalGeneratorService {

	/**
	 * Generate all AccountingTaskJournal objects for the specified date range and type if specified.
	 * <p/>
	 * Does not generate duplicate journals (if one already exists: same type and accountingJournal).
	 * Creates task journals in the database and returns the number of journals created.
	 */
	@ModelAttribute("result")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_CREATE)
	public Status generateAccountingTaskJournalList(AccountingTaskJournalGeneratorCommand command);
}
