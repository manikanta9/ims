package com.clifton.accounting.eventjournal.instruction;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;


/**
 * The <code>AccountingEventJournalCounterpartyInstructionSelector</code> defines the bean type
 * used to select corresponding {@link InvestmentInstructionDefinition} for each {@link AccountingEventJournalDetail} record
 * that defines the instructions for the "counterparty" i.e. Holding Account
 * that is sent to the custodian "Recipient" for a specific date "Event Date"
 *
 * @author manderson
 */
public class AccountingEventJournalCounterpartyInstructionSelector extends BaseAccountingEventJournalInstructionSelector {

	@Override
	public BusinessCompany getRecipient(AccountingEventJournalDetail detail) {

		InvestmentAccount custodianAccount = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurposeStrict(
				detail.getAccountingTransaction().getClientInvestmentAccount().getId(), detail.getAccountingTransaction().getHoldingInvestmentAccount().getId(),
				detail.getAccountingTransaction().getInvestmentSecurity().getId(), InvestmentAccountRelationshipPurpose.CUSTODIAN_ACCOUNT_PURPOSE_NAME,
				detail.getJournal().getSecurityEvent().getEventDate());
		// Strict Lookup will thrown an exception of none or more than one is found
		return custodianAccount.getIssuingCompany();
	}
}
