package com.clifton.accounting.eventjournal.validation;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import org.springframework.stereotype.Component;


/**
 * The <code>AccountingEventJournalValidator</code> class validates updates to AccountingEventJournal according to business rules.
 *
 * @author vgomelsky
 */
@Component
public class AccountingEventJournalValidator extends SelfRegisteringDaoValidator<AccountingEventJournal> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(AccountingEventJournal journal, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			if (journal.getBookingDate() != null) {
				throw new ValidationException("Cannot delete event journal: " + journal.getLabel() + " that has been booked.");
			}
		}
		else {
			if (config.isUpdate()) {
				AccountingEventJournal originalBean = getOriginalBean(journal);
				if (originalBean.getAccrualReversalBookingDate() != null && journal.getAccrualReversalBookingDate() != null) {
					throw new ValidationException("Cannot modify event journal: " + journal.getLabel() + " that has been booked.");
				}
			}

			ValidationUtils.assertNotEmpty(journal.getDetailList(), "Cannot create event journal: " + journal.getLabel() + " that has no details.");

			InvestmentSecurityEvent event = journal.getSecurityEvent();
			InvestmentSecurityEventType eventType = event.getType();
			for (AccountingEventJournalDetail detail : journal.getDetailList()) {
				InvestmentSecurityEventPayout payout = detail.getEventPayout();
				if (payout != null) {
					ValidationUtils.assertFalse(eventType.isSinglePayoutOnly(), "Payout is not allowed for Security Event of this Type.");
					ValidationUtils.assertEquals(payout.getSecurityEvent(), event, "Accounting Event Journal Detail payout must be from the same Security Event.");
				}
				else {
					// once we have event types that always require payouts, validate that payout is present
				}
			}
		}
	}
}
