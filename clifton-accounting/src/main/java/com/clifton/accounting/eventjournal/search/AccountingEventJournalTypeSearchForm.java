package com.clifton.accounting.eventjournal.search;

import com.clifton.accounting.eventjournal.EventJournalTypeScopes;
import com.clifton.accounting.eventjournal.generator.ExchangeRateDateSelectors;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class AccountingEventJournalTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "eventType.id")
	private Short eventTypeId;

	@SearchField
	private EventJournalTypeScopes eventJournalTypeScope;

	@SearchField(searchField = "debitAccount.id")
	private Short debitAccountId;
	@SearchField(searchField = "creditAccount.id")
	private Short creditAccountId;
	@SearchField(searchField = "accrualReversalAccount.id")
	private Short accrualReversalAccountId;

	@SearchField
	private Boolean affectedQuantityAdjustable;
	@SearchField
	private Boolean affectedCostAdjustable;
	@SearchField
	private Boolean transactionAmountAdjustable;
	@SearchField
	private Boolean transactionPriceAdjustable;
	@SearchField
	private Boolean additionalAmountAdjustable;
	@SearchField
	private Boolean additionalAmount2Adjustable;
	@SearchField
	private Boolean additionalAmount3Adjustable;

	@SearchField
	private Boolean settlementDateComparison;

	@SearchField
	private Short daysBeforeExDateForPositionLookup;

	@SearchField
	private Boolean autoPost;

	@SearchField
	private Boolean autoGenerate;

	@SearchField
	private String affectedQuantityLabel;
	@SearchField
	private String affectedCostLabel;
	@SearchField
	private String transactionAmountLabel;
	@SearchField
	private String transactionPriceLabel;
	@SearchField
	private String additionalAmountLabel;
	@SearchField
	private String additionalAmount2Label;
	@SearchField
	private String additionalAmount3Label;

	@SearchField
	private Integer bookingOrder;

	@SearchField
	private ExchangeRateDateSelectors exchangeRateDateSelector;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}


	public EventJournalTypeScopes getEventJournalTypeScope() {
		return this.eventJournalTypeScope;
	}


	public void setEventJournalTypeScope(EventJournalTypeScopes eventJournalTypeScope) {
		this.eventJournalTypeScope = eventJournalTypeScope;
	}


	public Short getDebitAccountId() {
		return this.debitAccountId;
	}


	public void setDebitAccountId(Short debitAccountId) {
		this.debitAccountId = debitAccountId;
	}


	public Short getCreditAccountId() {
		return this.creditAccountId;
	}


	public void setCreditAccountId(Short creditAccountId) {
		this.creditAccountId = creditAccountId;
	}


	public Short getAccrualReversalAccountId() {
		return this.accrualReversalAccountId;
	}


	public void setAccrualReversalAccountId(Short accrualReversalAccountId) {
		this.accrualReversalAccountId = accrualReversalAccountId;
	}


	public Boolean getAffectedQuantityAdjustable() {
		return this.affectedQuantityAdjustable;
	}


	public void setAffectedQuantityAdjustable(Boolean affectedQuantityAdjustable) {
		this.affectedQuantityAdjustable = affectedQuantityAdjustable;
	}


	public Boolean getAffectedCostAdjustable() {
		return this.affectedCostAdjustable;
	}


	public void setAffectedCostAdjustable(Boolean affectedCostAdjustable) {
		this.affectedCostAdjustable = affectedCostAdjustable;
	}


	public Boolean getTransactionAmountAdjustable() {
		return this.transactionAmountAdjustable;
	}


	public void setTransactionAmountAdjustable(Boolean transactionAmountAdjustable) {
		this.transactionAmountAdjustable = transactionAmountAdjustable;
	}


	public Boolean getTransactionPriceAdjustable() {
		return this.transactionPriceAdjustable;
	}


	public void setTransactionPriceAdjustable(Boolean transactionPriceAdjustable) {
		this.transactionPriceAdjustable = transactionPriceAdjustable;
	}


	public Boolean getAdditionalAmountAdjustable() {
		return this.additionalAmountAdjustable;
	}


	public void setAdditionalAmountAdjustable(Boolean additionalAmountAdjustable) {
		this.additionalAmountAdjustable = additionalAmountAdjustable;
	}


	public Boolean getAdditionalAmount2Adjustable() {
		return this.additionalAmount2Adjustable;
	}


	public void setAdditionalAmount2Adjustable(Boolean additionalAmount2Adjustable) {
		this.additionalAmount2Adjustable = additionalAmount2Adjustable;
	}


	public Boolean getAdditionalAmount3Adjustable() {
		return this.additionalAmount3Adjustable;
	}


	public void setAdditionalAmount3Adjustable(Boolean additionalAmount3Adjustable) {
		this.additionalAmount3Adjustable = additionalAmount3Adjustable;
	}


	public Boolean getSettlementDateComparison() {
		return this.settlementDateComparison;
	}


	public void setSettlementDateComparison(Boolean settlementDateComparison) {
		this.settlementDateComparison = settlementDateComparison;
	}


	public Short getDaysBeforeExDateForPositionLookup() {
		return this.daysBeforeExDateForPositionLookup;
	}


	public void setDaysBeforeExDateForPositionLookup(Short daysBeforeExDateForPositionLookup) {
		this.daysBeforeExDateForPositionLookup = daysBeforeExDateForPositionLookup;
	}


	public Boolean getAutoPost() {
		return this.autoPost;
	}


	public void setAutoPost(Boolean autoPost) {
		this.autoPost = autoPost;
	}


	public Boolean getAutoGenerate() {
		return this.autoGenerate;
	}


	public void setAutoGenerate(Boolean autoGenerate) {
		this.autoGenerate = autoGenerate;
	}


	public String getAffectedQuantityLabel() {
		return this.affectedQuantityLabel;
	}


	public void setAffectedQuantityLabel(String affectedQuantityLabel) {
		this.affectedQuantityLabel = affectedQuantityLabel;
	}


	public String getAffectedCostLabel() {
		return this.affectedCostLabel;
	}


	public void setAffectedCostLabel(String affectedCostLabel) {
		this.affectedCostLabel = affectedCostLabel;
	}


	public String getTransactionAmountLabel() {
		return this.transactionAmountLabel;
	}


	public void setTransactionAmountLabel(String transactionAmountLabel) {
		this.transactionAmountLabel = transactionAmountLabel;
	}


	public String getTransactionPriceLabel() {
		return this.transactionPriceLabel;
	}


	public void setTransactionPriceLabel(String transactionPriceLabel) {
		this.transactionPriceLabel = transactionPriceLabel;
	}


	public String getAdditionalAmountLabel() {
		return this.additionalAmountLabel;
	}


	public void setAdditionalAmountLabel(String additionalAmountLabel) {
		this.additionalAmountLabel = additionalAmountLabel;
	}


	public String getAdditionalAmount2Label() {
		return this.additionalAmount2Label;
	}


	public void setAdditionalAmount2Label(String additionalAmount2Label) {
		this.additionalAmount2Label = additionalAmount2Label;
	}


	public String getAdditionalAmount3Label() {
		return this.additionalAmount3Label;
	}


	public void setAdditionalAmount3Label(String additionalAmount3Label) {
		this.additionalAmount3Label = additionalAmount3Label;
	}


	public Integer getBookingOrder() {
		return this.bookingOrder;
	}


	public void setBookingOrder(Integer bookingOrder) {
		this.bookingOrder = bookingOrder;
	}


	public ExchangeRateDateSelectors getExchangeRateDateSelector() {
		return this.exchangeRateDateSelector;
	}


	public void setExchangeRateDateSelector(ExchangeRateDateSelectors exchangeRateDateSelector) {
		this.exchangeRateDateSelector = exchangeRateDateSelector;
	}
}
