package com.clifton.accounting.eventjournal.eventrollup;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>AccountingSecurityEventRollup</code> class is a generic class that combines two or more security
 * events into 1 object and includes payment information (from journals)
 * <p/>
 * Currently used for Swap reset events that need to combine 2 or 3 events into one in order to improve visibility
 * <p/>
 * Interest Rate Swaps events: Fixed & Floating Leg
 * Total Return Swap events: Equity & Interest Leg & Dividend Leg
 *
 * @author manderson
 */
public abstract class AccountingSecurityEventRollup extends BaseSimpleEntity<Integer> {

	/**
	 * "id" property: either Leg event id (the other event has the Event Date)
	 */

	private InvestmentSecurityEvent[] legEvents;

	/**
	 * For cases like resets, the report itself is driven by event journal details.
	 * For these, the report itself contains the same information regardless of which detail it uses and for which leg because
	 * the data in the report finds all relevant matching details for both legs.
	 */
	private Integer firstEventJournalDetailId;

	/**
	 * Set to true when the equity and/or interest payments were calculated via previewed {@link AccountingEventJournalDetail} - i.e. not real or saved/generated in the database yet
	 */
	private boolean paymentPreview;
	/**
	 * Populated for previews when details aren't generated because of missing information.
	 */
	private String previewSkipMessage;

	/**
	 * Sum of Payment Amounts for the corresponding event journal details.
	 */
	private BigDecimal[] legEventPayments;

	/**
	 * Sum of Payment Base Amounts for the corresponding event journal details.
	 */
	private BigDecimal[] legEventPaymentsBase;

	/**
	 * Need in order to create events.
	 */
	private InvestmentSecurity security;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingSecurityEventRollup() {
		this.legEvents = new InvestmentSecurityEvent[getEventLegCount()];
		this.legEventPayments = new BigDecimal[getEventLegCount()];
		this.legEventPaymentsBase = new BigDecimal[getEventLegCount()];
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns an array of investment security event type names for the events that are part of this rollup.
	 * Usually 2 or 3 events (swap legs) are grouped into a single rollup.
	 */
	public abstract String[] getEventLegTypes();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the name of the "event" that groups all of the rollup events.
	 * For swaps, they are usually called "Reset".  But a different name can be used.
	 */
	public String getEventType() {
		return "Reset";
	}


	public String getLabel() {
		StringBuilder result = new StringBuilder(32);
		result.append(getEventType());
		if (this.security != null) {
			result.append(" for ");
			result.append(this.security.getSymbol());
		}
		result.append(" on ");
		InvestmentSecurityEvent firstLegEvent = this.legEvents[0];
		result.append(firstLegEvent == null ? "???" : DateUtils.fromDateShort(firstLegEvent.getEventDate()));
		return result.toString();
	}


	/**
	 * Returns the total number of even legs for this rollup (usually 2 or 3).
	 */
	public int getEventLegCount() {
		String[] eventTypes = getEventLegTypes();
		return (eventTypes == null) ? 0 : eventTypes.length;
	}


	/**
	 * Returns the index of events of the specified type.
	 * Throws an exception if the specified event type is not supported by this rollup.
	 */
	public int getEventLegIndex(String typeName) {
		String[] eventTypes = getEventLegTypes();
		for (int i = 0; i < eventTypes.length; i++) {
			if (eventTypes[i].equals(typeName)) {
				return i;
			}
		}
		throw new IllegalArgumentException(typeName + " is not a valid event type.");
	}


	public BigDecimal getTotalPayment() {
		BigDecimal result = BigDecimal.ZERO;
		if (this.legEventPayments != null) {
			for (BigDecimal payment : this.legEventPayments) {
				result = MathUtils.add(result, payment);
			}
		}
		return result;
	}


	public BigDecimal getTotalPaymentBase() {
		BigDecimal result = BigDecimal.ZERO;
		if (this.legEventPaymentsBase != null) {
			for (BigDecimal payment : this.legEventPaymentsBase) {
				result = MathUtils.add(result, payment);
			}
		}
		return result;
	}


	public InvestmentSecurityEvent getEventLeg(String typeName) {
		return this.legEvents[getEventLegIndex(typeName)];
	}


	public void setEventLeg(String typeName, InvestmentSecurityEvent event) {
		this.legEvents[getEventLegIndex(typeName)] = event;
	}


	public BigDecimal getEventPayment(String typeName) {
		return this.legEventPayments[getEventLegIndex(typeName)];
	}


	public void setEventPayment(String typeName, BigDecimal payment) {
		this.legEventPayments[getEventLegIndex(typeName)] = payment;
	}


	public BigDecimal getEventPaymentBase(String typeName) {
		return this.legEventPaymentsBase[getEventLegIndex(typeName)];
	}


	public void setEventPaymentBase(String typeName, BigDecimal payment) {
		this.legEventPaymentsBase[getEventLegIndex(typeName)] = payment;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isPaymentPreview() {
		return this.paymentPreview;
	}


	public void setPaymentPreview(boolean paymentPreview) {
		this.paymentPreview = paymentPreview;
	}


	public String getPreviewSkipMessage() {
		return this.previewSkipMessage;
	}


	public void setPreviewSkipMessage(String previewSkipMessage) {
		this.previewSkipMessage = previewSkipMessage;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public Integer getFirstEventJournalDetailId() {
		return this.firstEventJournalDetailId;
	}


	public void setFirstEventJournalDetailId(Integer firstEventJournalDetailId) {
		this.firstEventJournalDetailId = firstEventJournalDetailId;
	}
}
