package com.clifton.accounting.eventjournal.generator.detailpopulator;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationService;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author vgomelsky
 */
public class AccountingEventJournalDetailCreditEventPopulator extends AccountingEventJournalDetailFactorChangePopulator {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityAllocationService investmentSecurityAllocationService;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private SystemColumnValueHandler systemColumnValueHandler;


	@Override
	public boolean populate(PopulatorCommand command) {
		// if position is currently fully closed, then do not process Credit Event
		AccountingEventJournalDetail detail = command.getJournalDetail();
		if (!isPositionStillOpen(detail.getAccountingTransaction())) {
			command.setSkipMessage("Position is currently fully closed.");
			return true;
		}

		// if previous and current factors are the same: skip the journal
		EventData eventData = command.getEventData();
		if (MathUtils.isEqual(eventData.getBeforeEventValue(), eventData.getAfterEventValue())) {
			command.setSkipMessage("Previous and Current Factors are the same.");
			return true;
		}

		// factor change to 0 fully closes existing position
		BigDecimal originalNotional = command.getOpeningQuantity();
		InvestmentNotionalCalculatorTypes notionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(eventData.getSecurity());
		if (eventData.getAfterEventValue().compareTo(BigDecimal.ZERO) == 0) {
			detail.setTransactionAmount(command.getRemainingQuantity());
			detail.setAffectedCost(command.getRemainingCostBasis());
		}
		else {
			BigDecimal expectedEndFace = MathUtils.multiply(originalNotional, eventData.getAfterEventValue());
			expectedEndFace = notionalCalculator.round(expectedEndFace);
			BigDecimal paymentAmount = detail.getAffectedQuantity().subtract(expectedEndFace);
			detail.setTransactionAmount(paymentAmount);
			detail.setAffectedCost(getInvestmentCalculator().calculateNotional(eventData.getSecurity(), detail.getTransactionPrice(), paymentAmount, BigDecimal.ONE));
		}

		BigDecimal principalPayment = calculatePrincipalPayment(eventData.getEvent(), originalNotional, notionalCalculator);

		// ignore rounding loss within a dollar
		BigDecimal lossAmount = MathUtils.subtract(detail.getTransactionAmount(), principalPayment);
		if (MathUtils.isGreaterThan(lossAmount.abs(), new BigDecimal("1.00"))) {
			detail.setAdditionalAmount(lossAmount);
		}
		detail.setTransactionAmount(principalPayment);

		BigDecimal accruedInterest = calculateAccruedInterestPayment(eventData.getEvent(), originalNotional);
		detail.setAdditionalAmount2(accruedInterest.negate()); // the sign indicates direction: plus receive and minus pay

		return false;
	}


	/**
	 * Principal Payment = Original Notional*(1-Recovery)*((Old Factor - New Factor)/((Detachment - Attachment)/100))
	 * <p>
	 * NOTE: Old and New Factor MUST come from the underlying index event.
	 * This formula supports all credit: equally and not equally weighted indices, tranches and single names.
	 * <p>
	 * We need to use different formula for Bespoke because their underlying will not have an underlying (it's custom and no point to create a duplicate).
	 * Principal Payment = Original Notional*(1-Recovery)*(Entity Weight/100)/((Detachment - Attachment)/100))
	 */
	private BigDecimal calculatePrincipalPayment(InvestmentSecurityEvent securityEvent, BigDecimal originalNotional, InvestmentNotionalCalculatorTypes notionalCalculator) {
		// get underlying security
		InvestmentSecurity security = securityEvent.getSecurity();
		InvestmentInstrument underlyingInstrument = security.getInstrument().getUnderlyingInstrument();
		ValidationUtils.assertNotNull(underlyingInstrument, "Cannot find underlying instrument for " + security);

		InvestmentSecurity underlyingSecurity = getInvestmentInstrumentService().getInvestmentSecurityByInstrument(underlyingInstrument.getId());
		ValidationUtils.assertNotNull(underlyingSecurity, "Cannot find security for instrument " + underlyingInstrument);

		BigDecimal attachmentPoint = getSecurityCustomFieldValue(underlyingSecurity, InvestmentSecurity.CUSTOM_FIELD_ATTACHMENT_POINT, BigDecimal.ZERO);
		BigDecimal detachmentPoint = getSecurityCustomFieldValue(underlyingSecurity, InvestmentSecurity.CUSTOM_FIELD_DETACHMENT_POINT, MathUtils.BIG_DECIMAL_ONE_HUNDRED);

		// TODO: change to use sub-type 2 to detect tranches and later bespoke?
		if (security.getInstrument().getHierarchy().getLabelExpanded().contains(InvestmentTypeSubType2.SWAPS_TRANCHE)) {
			// tranches have an underlying index: need it for old/new factors
			underlyingInstrument = underlyingInstrument.getUnderlyingInstrument();
			underlyingSecurity = getInvestmentInstrumentService().getInvestmentSecurityByInstrument(underlyingInstrument.getId());
			ValidationUtils.assertNotNull(underlyingSecurity, "Cannot find security for instrument " + underlyingInstrument);
		}

		// get underlying event
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(underlyingSecurity.getId());
		searchForm.setTypeId(securityEvent.getType().getId());
		searchForm.setEventDate(securityEvent.getEventDate());
		searchForm.setEventDescription(securityEvent.getEventDescription());
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		ValidationUtils.assertEquals(1, CollectionUtils.getSize(eventList), "Must have exactly one security event " + securityEvent + " for underlying security " + underlyingSecurity + " that matches on event type, event date and event description. Found: " + eventList);
		InvestmentSecurityEvent underlyingEvent = eventList.get(0);

		BigDecimal recoveryRate = securityEvent.getAdditionalEventValue();
		BigDecimal principalPayment = MathUtils.multiply(originalNotional, MathUtils.subtract(BigDecimal.ONE, MathUtils.divide(recoveryRate, MathUtils.BIG_DECIMAL_ONE_HUNDRED)));
		if (security.getInstrument().getHierarchy().getLabelExpanded().contains(InvestmentTypeSubType2.SWAPS_BESPOKE)) {
			Integer originalMembers = getSecurityCustomFieldValue(underlyingSecurity, InvestmentSecurity.CUSTOM_FIELD_ORIGINAL_MEMBERS, null);
			if (originalMembers == null) {
				throw new ValidationException(InvestmentSecurity.CUSTOM_FIELD_ORIGINAL_MEMBERS + " custom field must be populated for " + underlyingSecurity);
			}
			principalPayment = MathUtils.multiply(principalPayment, MathUtils.divide(getConstituentAllocationWeight(securityEvent), MathUtils.BIG_DECIMAL_ONE_HUNDRED));
		}
		else {
			principalPayment = MathUtils.multiply(principalPayment, MathUtils.subtract(underlyingEvent.getBeforeEventValue(), underlyingEvent.getAfterEventValue()));
		}
		principalPayment = MathUtils.divide(principalPayment, MathUtils.divide(MathUtils.subtract(detachmentPoint, attachmentPoint), MathUtils.BIG_DECIMAL_ONE_HUNDRED));
		return notionalCalculator.round(principalPayment);
	}


	private BigDecimal getConstituentAllocationWeight(InvestmentSecurityEvent securityEvent) {
		InvestmentSecurity underlyingSecurity = securityEvent.getSecurity().getUnderlyingSecurity();
		InvestmentSecurity constituent = securityEvent.getAdditionalSecurity();
		ValidationUtils.assertNotNull(constituent, "Underlying Reference Entity that has triggered a Credit Event must be specified for " + securityEvent);
		SecurityAllocationSearchForm allocationSearchForm = new SecurityAllocationSearchForm();
		allocationSearchForm.setParentInvestmentSecurityId(underlyingSecurity.getId());
		allocationSearchForm.setInvestmentSecurityId(constituent.getId());
		allocationSearchForm.setActiveOnDate(securityEvent.getAdditionalDate()); // Default Date
		List<InvestmentSecurityAllocation> allocationList = getInvestmentSecurityAllocationService().getInvestmentSecurityAllocationList(allocationSearchForm);
		if (CollectionUtils.isEmpty(allocationList)) {
			throw new ValidationException("Cannot find Constituent '" + constituent.getLabel() + "' for '" + underlyingSecurity.getLabel() + "' on " + DateUtils.fromDateShort(securityEvent.getAdditionalDate()));
		}
		else if (CollectionUtils.getSize(allocationList) > 1) {
			throw new ValidationException("Cannot have more than one Constituent '" + constituent.getLabel() + "' for '" + underlyingSecurity.getLabel() + "' on " + DateUtils.fromDateShort(securityEvent.getAdditionalDate()));
		}

		return allocationList.get(0).getAllocationWeight();
	}


	private BigDecimal calculateAccruedInterestPayment(InvestmentSecurityEvent securityEvent, BigDecimal originalNotional) {
		Date dateOfDefault = securityEvent.getAdditionalDate();
		ValidationUtils.assertNotNull(dateOfDefault, "Default Date [Additional Date] is required for Credit Event: " + securityEvent);
		Date dateAfterAuction = securityEvent.getExDate();
		// when auction date is in next period than the default, return accrued interest portion of next period vs receiving for current period
		Date dateToAccrueFrom = (dateAfterAuction.after(securityEvent.getAccrualEndDate())) ? securityEvent.getAccrualEndDate() : securityEvent.getAccrualStartDate();

		BigDecimal factorDifference = MathUtils.subtract(securityEvent.getAfterEventValue(), securityEvent.getBeforeEventValue());
		BigDecimal notional = MathUtils.multiply(originalNotional, factorDifference);
		return getInvestmentCalculator().calculateAccruedInterest(securityEvent.getSecurity(), AccrualBasis.of(notional), null, AccrualDates.ofSame1and2(dateToAccrueFrom, dateOfDefault));
	}


	private <T> T getSecurityCustomFieldValue(InvestmentSecurity security, String customFieldName, T defaultValue) {
		Object value = getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, customFieldName, false);
		if (value == null) {
			return defaultValue;
		}
		//noinspection unchecked
		return (T) value;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityAllocationService getInvestmentSecurityAllocationService() {
		return this.investmentSecurityAllocationService;
	}


	public void setInvestmentSecurityAllocationService(InvestmentSecurityAllocationService investmentSecurityAllocationService) {
		this.investmentSecurityAllocationService = investmentSecurityAllocationService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
