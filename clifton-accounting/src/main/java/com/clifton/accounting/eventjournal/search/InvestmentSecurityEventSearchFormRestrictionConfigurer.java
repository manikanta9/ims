package com.clifton.accounting.eventjournal.search;


import com.clifton.core.dataaccess.search.SearchFormRestrictionConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.BooleanUtils;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentSecurityEventSearchFormRestrictionConfigurer</code> class allows to define cross-project form restriction
 * for investment security event booking status.
 *
 * @author mwacker
 */
@Component
public class InvestmentSecurityEventSearchFormRestrictionConfigurer implements SearchFormRestrictionConfigurer {


	@Override
	public Class<?> getSearchFormClass() {
		return InvestmentSecurityEventSearchForm.class;
	}


	@Override
	public String getSearchFieldName() {
		return "booked";
	}


	@Override
	public void configureCriteria(Criteria criteria, SearchRestriction restriction) {
		if (restriction.getValue() != null) {
			// if there is a book journal entry then exclude it
			String sql = "EXISTS (SELECT * FROM AccountingEventJournal ej " + //
					"WHERE {alias}.InvestmentSecurityEventID = ej.InvestmentSecurityEventID " + //
					"AND BookingDate IS NOT NULL)";

			if (BooleanUtils.isTrueStrict(restriction.getValue())) {
				sql = "NOT " + sql;
			}
			criteria.add(Restrictions.sqlRestriction(sql));
		}
	}
}
