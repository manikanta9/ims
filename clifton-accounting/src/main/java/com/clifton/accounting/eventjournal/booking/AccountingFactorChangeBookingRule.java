package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>AccountingFactorChangeBookingRule</code> class generates appropriate GL entries to account for factor changes.
 * For bonds with delayed payment, two separate journals are generated: accrual of factor change and then then the reversal of accrual and payment.
 * <p>
 * Accrual Event: reduce Position and create equal Payment Receivable (at par) and the difference is Realized Gain / Loss (change in bond price from open to 100)
 * Payment Event: reverse Payment Receivable and get Cash.
 * <p>
 * Note:
 * If position was closed at Assumed Factor, we are still entitled to one principal payment.  Accrual part of event needs to be
 * different in this case since the position was already closed:
 * create Payment Receivable, adjust Interest Income and Realized Gain / Loss based on new factor, pay Cash.
 * Accrual reversal generates the same transaction set.
 */
public class AccountingFactorChangeBookingRule extends BaseEventJournalBookingRule {

	private AccountingPositionTransferService accountingPositionTransferService;
	private boolean accrualReversal = false;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();
		InvestmentSecurityEvent event = eventJournal.getSecurityEvent();

		// if payment is not delayed, no need to accrue
		boolean doNotAccrue = !event.isPaymentDelayed();

		// for each open position
		Date transactionDate = getTransactionDate(eventJournal, !(doNotAccrue || isAccrualReversal()));
		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			AccountingJournalDetailDefinition closingPosition = generatePositionClosingFromFactorChange(journal, eventDetail, transactionDate);
			AccountingJournalDetailDefinition realizedEntry = generateRealizedJournalDetail(journal, eventDetail, closingPosition, transactionDate);
			if (doNotAccrue) {
				if (!isAccrualReversal()) {
					// close from factor, realized and cash
					journal.addJournalDetail(closingPosition);
					addCollateralOffsetPositions(journal, closingPosition);
					journal.addJournalDetail(realizedEntry);
					addCurrencyTranslationDetail(journal, realizedEntry);
					AccountingJournalDetail accountingJournalDetail = createCashCurrencyRecord(closingPosition, eventDetail);
					journal.addJournalDetail(accountingJournalDetail);
					journal.addJournalDetail(createCashCurrencyRecord(realizedEntry, eventDetail));

					// account for losses, if any
					if (!MathUtils.isNullOrZero(eventDetail.getAdditionalAmount())) {
						AccountingJournalDetailDefinition loss = generateLossJournalDetail(journal, eventDetail, closingPosition, transactionDate);
						journal.addJournalDetail(loss);
						journal.addJournalDetail(createCashCurrencyRecord(loss, eventDetail));
					}

					// account for accrued interest, if any
					if (!MathUtils.isNullOrZero(eventDetail.getAdditionalAmount2())) {
						if (InvestmentSecurityEventType.CREDIT_EVENT.equals(event.getType().getName())) {
							AccountingJournalDetailDefinition accruedInterest = getAccruedInterestJournalDetail(journal, eventDetail, transactionDate);
							journal.addJournalDetail(accruedInterest);
							journal.addJournalDetail(createCashCurrencyRecord(accruedInterest, eventDetail));
						}
					}
				}
			}
			else {
				AccountingJournalDetailDefinition receivableDetail = generateReceivableFromFactorChange(closingPosition, eventDetail, transactionDate);
				AccountingJournalDetailDefinition lossDetail = null;
				if (isAccrualReversal()) {
					// reverse position receivable; realized gain/loss; receive cash
					journal.addJournalDetail(receivableDetail);
					journal.addJournalDetail(createCashCurrencyRecord(receivableDetail, eventDetail));
					// account for accrued interest, if any
					if (!MathUtils.isNullOrZero(eventDetail.getAdditionalAmount2())) {
						if (InvestmentSecurityEventType.CREDIT_EVENT.equals(event.getType().getName())) {
							AccountingJournalDetailDefinition accruedInterest = getAccruedInterestJournalDetail(journal, eventDetail, transactionDate);
							receivableDetail = generateReceivableFromFactorChange(accruedInterest, eventDetail, transactionDate);
							journal.addJournalDetail(receivableDetail);
							journal.addJournalDetail(createCashCurrencyRecord(receivableDetail, eventDetail));
						}
					}
				}
				else {
					// close position portion resulting from factor change and open corresponding position receivable
					journal.addJournalDetail(closingPosition);
					addCollateralOffsetPositions(journal, closingPosition);
					journal.addJournalDetail(realizedEntry);
					addCurrencyTranslationDetail(journal, realizedEntry);
					journal.addJournalDetail(receivableDetail);
					// account for losses, if any
					if (!MathUtils.isNullOrZero(eventDetail.getAdditionalAmount())) {
						lossDetail = generateLossJournalDetail(journal, eventDetail, closingPosition, transactionDate);
						journal.addJournalDetail(lossDetail);
					}
					// account for accrued interest, if any
					if (!MathUtils.isNullOrZero(eventDetail.getAdditionalAmount2())) {
						if (InvestmentSecurityEventType.CREDIT_EVENT.equals(event.getType().getName())) {
							AccountingJournalDetailDefinition accruedInterest = getAccruedInterestJournalDetail(journal, eventDetail, transactionDate);
							journal.addJournalDetail(accruedInterest);
							journal.addJournalDetail(generateReceivableFromFactorChange(accruedInterest, eventDetail, transactionDate));
						}
					}
				}
				// adjust the base amount on the loss detail to avoid rounding errors
				adjustPositionClosingBaseAmount(eventDetail, receivableDetail, lossDetail);
			}
		}
	}


	private void addCollateralOffsetPositions(AccountingJournal journal, AccountingJournalDetailDefinition closingPosition) {
		if (closingPosition.getAccountingAccount().isCollateral()) {
			List<AccountingPosition> offsetPositions = getOffsetPositions(closingPosition);
			for (AccountingPosition offsetPosition : offsetPositions) {
				AccountingJournalDetail detail = new AccountingJournalDetail();
				BeanUtils.copyProperties(closingPosition, detail);

				detail.setAccountingAccount(offsetPosition.getAccountingAccount());
				detail.setHoldingInvestmentAccount(offsetPosition.getHoldingInvestmentAccount());
				detail.setParentTransaction(getAccountingTransactionService().getAccountingTransaction(offsetPosition.getAccountingTransactionId()));
				detail.setOriginalTransactionDate(offsetPosition.getOriginalTransactionDate());
				if (isCollateralPayablePositionAccount(offsetPosition.getAccountingAccount())) {
					detail.setQuantity(detail.getQuantity().negate());
					detail.setLocalDebitCredit(detail.getLocalDebitCredit().negate());
					detail.setBaseDebitCredit(detail.getBaseDebitCredit().negate());
					detail.setPositionCostBasis(detail.getPositionCostBasis().negate());
				}
				journal.addJournalDetail(detail);
			}
		}
	}


	public List<AccountingPosition> getOffsetPositions(AccountingJournalDetailDefinition closingPosition) {
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(closingPosition.getParentTransaction().getTransactionDate())
				.forClientAccount(closingPosition.getClientInvestmentAccount().getId())
				.forInvestmentSecurity(closingPosition.getInvestmentSecurity().getId());
		List<Short> transferOffsetAccounts = getCollateralTransferOffsetAccountIds(closingPosition.getParentTransaction());
		List<AccountingPosition> offsetPositions = getAccountingPositionService().getAccountingPositionListUsingCommand(command);
		return offsetPositions.stream()
				.filter(AccountingPosition::isCollateral)
				.filter(p -> p.getAccountingAccount().isReceivable())
				.filter(p -> transferOffsetAccounts.contains(p.getAccountingAccount().getId()))
				.filter(p -> p.getOpeningTransaction() != null)
				.filter(p -> p.getOpeningTransaction().getParentTransaction() != null)
				.filter(p -> Objects.equals(p.getOpeningTransaction().getParentTransaction().getId(), closingPosition.getParentTransaction().getId()))
				.collect(Collectors.toList());
	}


	private List<Short> getCollateralTransferOffsetAccountIds(AccountingTransaction accountingTransaction) {
		if (AccountingJournalType.TRANSFER_JOURNAL.equals(accountingTransaction.getJournal().getJournalType().getName()) && accountingTransaction.getFkFieldId() != null) {
			AccountingPositionTransferDetail accountingPositionTransferDetail = getAccountingPositionTransferService().getAccountingPositionTransferDetail(accountingTransaction.getFkFieldId());
			AccountingPositionTransferType transferType = Optional.ofNullable(accountingPositionTransferDetail)
					.map(AccountingPositionTransferDetail::getPositionTransfer)
					.map(AccountingPositionTransfer::getType)
					.orElse(null);
			if (transferType != null) {
				return Stream.of(transferType.getToOffsettingAccountingAccount(), transferType.getFromOffsettingAccountingAccount())
						.filter(Objects::nonNull)
						.map(AccountingAccount::getId)
						.collect(Collectors.toList());
			}
		}
		return Collections.emptyList();
	}


	/**
	 * Will recalculate the base loss amount as the abs(round(detail.transactionAmount*fx_rate,2)) - abs(receivable.baseDebitCredit).  If that amount is with in $.01 of
	 * the lose detail, then adjust to loss detail to avoid rounding issues.
	 */
	private void adjustPositionClosingBaseAmount(AccountingEventJournalDetail eventDetail, AccountingJournalDetailDefinition
			receivableDetail, AccountingJournalDetailDefinition lossDetail) {
		if (lossDetail != null && !MathUtils.isEqual(BigDecimal.ONE, lossDetail.getExchangeRateToBase())) {
			BigDecimal basePositionAmount = InvestmentCalculatorUtils.calculateBaseAmount(MathUtils.add(eventDetail.getTransactionAmount(), eventDetail.getAdditionalAmount()), lossDetail.getExchangeRateToBase(), lossDetail.getClientInvestmentAccount());
			BigDecimal computedBaseLossAmount = receivableDetail.getBaseDebitCredit().abs().subtract(basePositionAmount.abs()).abs();

			if (MathUtils.isLessThanOrEqual(lossDetail.getBaseDebitCredit().abs().subtract(computedBaseLossAmount.abs()).abs(), new BigDecimal("0.01"))) {
				lossDetail.setBaseDebitCredit(MathUtils.isLessThan(lossDetail.getLocalDebitCredit(), BigDecimal.ZERO) ? computedBaseLossAmount.negate() : computedBaseLossAmount);
			}
		}
	}


	private boolean isCollateralPayablePositionAccount(AccountingAccount accountingAccount) {
		return accountingAccount.isCollateral() && accountingAccount.isPosition() && accountingAccount.isReceivable() && AccountingAccountType.LIABILITY.equals(accountingAccount.getAccountType().getName());
	}


	private AccountingJournalDetail generatePositionClosingFromFactorChange(AccountingJournal journal, AccountingEventJournalDetail eventDetail, Date transactionDate) {
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(eventDetail.getId());
		detail.setSystemTable(getAccountingEventJournalDetailTable());

		detail.setParentTransaction(affectedLot);
		detail.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		detail.setAccountingAccount(affectedLot.getAccountingAccount());
		detail.setExchangeRateToBase(eventDetail.getExchangeRateToBase());

		detail.setPrice(eventDetail.getTransactionPrice());
		// payment amount + loss amount (note that when manual override is used for one of these amounts, the other also needs to change so that total stays the same)
		detail.setQuantity(MathUtils.add(eventDetail.getTransactionAmount(), eventDetail.getAdditionalAmount()).negate());
		detail.setLocalDebitCredit(eventDetail.getAffectedCost().negate());
		detail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(detail.getLocalDebitCredit(), detail.getExchangeRateToBase(), affectedLot.getClientInvestmentAccount()));

		detail.setPositionCostBasis(detail.getLocalDebitCredit());

		detail.setDescription(eventDetail.getJournal().getLabel());

		detail.setOriginalTransactionDate(affectedLot.getOriginalTransactionDate());
		detail.setSettlementDate(event.getPaymentDate());
		detail.setTransactionDate(transactionDate);

		detail.setOpening(false);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}


	private AccountingJournalDetailDefinition generateRealizedJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventDetail, AccountingJournalDetailDefinition closingPosition, Date transactionDate) {
		AccountingEventJournalType eventJournalType = eventDetail.getJournal().getJournalType();
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(eventDetail.getId());
		detail.setSystemTable(getAccountingEventJournalDetailTable());

		detail.setParentDefinition(closingPosition);
		detail.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		detail.setAccountingAccount(eventJournalType.getCreditAccount());
		detail.setExchangeRateToBase(eventDetail.getExchangeRateToBase());

		detail.setPrice(eventDetail.getTransactionPrice());
		detail.setQuantity(null);

		BigDecimal transactionAmount = MathUtils.add(eventDetail.getTransactionAmount(), eventDetail.getAdditionalAmount());
		detail.setLocalDebitCredit(eventDetail.getAffectedCost().subtract(transactionAmount));

		// to avoid penny rounding issues, calculate the rounded base amounts first and then do the subtraction
		BigDecimal baseAffectedCost = InvestmentCalculatorUtils.calculateBaseAmount(eventDetail.getAffectedCost(), detail.getExchangeRateToBase(), affectedLot.getClientInvestmentAccount());
		BigDecimal baseTransactionAmount = InvestmentCalculatorUtils.calculateBaseAmount(transactionAmount, detail.getExchangeRateToBase(), affectedLot.getClientInvestmentAccount());
		detail.setBaseDebitCredit(baseAffectedCost.subtract(baseTransactionAmount));
		detail.setPositionCostBasis(BigDecimal.ZERO);

		detail.setDescription(detail.getAccountingAccount().getName() + " from " + event.getType().getName() + " for " + event.getSecurity().getSymbol() + " on " + DateUtils.fromDateShort(event.getEventDate()));

		detail.setOriginalTransactionDate(closingPosition.getOriginalTransactionDate());
		detail.setTransactionDate(transactionDate);
		detail.setSettlementDate(event.getPaymentDate());

		detail.setOpening(false);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}


	private AccountingJournalDetailDefinition getAccruedInterestJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventDetail, Date transactionDate) {
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();

		// the sign indicates direction: plus receive and minus pay
		BigDecimal interestAmount = eventDetail.getAdditionalAmount2().negate();

		AccountingAccount accountingAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.REVENUE_PREMIUM_LEG);

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(eventDetail.getId());
		detail.setSystemTable(getAccountingEventJournalDetailTable());

		detail.setParentTransaction(affectedLot);
		detail.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		detail.setAccountingAccount(accountingAccount);
		detail.setExchangeRateToBase(eventDetail.getExchangeRateToBase());

		detail.setPrice(null);
		detail.setQuantity(null);
		detail.setLocalDebitCredit(interestAmount);
		detail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(detail.getLocalDebitCredit(), detail.getExchangeRateToBase(), affectedLot.getClientInvestmentAccount()));
		detail.setPositionCostBasis(BigDecimal.ZERO);

		detail.setDescription(detail.getAccountingAccount().getName() + " from " + event.getType().getName() + " for " + event.getSecurity().getSymbol() + " on " + DateUtils.fromDateShort(event.getEventDate()));

		detail.setOriginalTransactionDate(affectedLot.getOriginalTransactionDate());
		detail.setTransactionDate(transactionDate);
		detail.setSettlementDate(event.getPaymentDate());

		detail.setOpening(false);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}


	private AccountingJournalDetailDefinition generateLossJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventDetail, AccountingJournalDetailDefinition
			closingPosition, Date transactionDate) {
		AccountingJournalDetailDefinition result = generateRealizedJournalDetail(journal, eventDetail, closingPosition, transactionDate);

		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();
		result.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.EXPENSE_PRINCIPAL_LOSSES));
		result.setLocalDebitCredit(eventDetail.getAdditionalAmount());
		result.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(result.getLocalDebitCredit(), result.getExchangeRateToBase(), result.getClientInvestmentAccount()));
		result.setDescription(result.getAccountingAccount().getName() + " from " + event.getType().getName() + " for " + event.getSecurity().getSymbol());
		return result;
	}


	private AccountingJournalDetail generateReceivableFromFactorChange(AccountingJournalDetailDefinition oppositeEntry, AccountingEventJournalDetail eventDetail, Date
			transactionDate) {
		AccountingEventJournalType eventJournalType = eventDetail.getJournal().getJournalType();
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail receivable = new AccountingJournalDetail();
		BeanUtils.copyProperties(oppositeEntry, receivable);
		receivable.setAccountingAccount(eventJournalType.getDebitAccount());
		receivable.setHoldingInvestmentAccount(eventDetail.getAccountingTransaction().getHoldingInvestmentAccount());
		// add realized to receivable
		if (oppositeEntry.getAccountingAccount().isPosition()) {
			receivable.setLocalDebitCredit(isAccrualReversal() ? eventDetail.getTransactionAmount().negate() : eventDetail.getTransactionAmount());
		}
		else {
			receivable.setLocalDebitCredit(isAccrualReversal() ? oppositeEntry.getLocalDebitCredit() : oppositeEntry.getLocalDebitCredit().negate());
		}
		receivable.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(receivable.getLocalDebitCredit(), receivable.getExchangeRateToBase(), receivable.getClientInvestmentAccount()));
		receivable.setPositionCostBasis(BigDecimal.ZERO);

		if (isAccrualReversal()) {
			receivable.setDescription("Reverse accrual of " + eventJournalType.getEventType().getName() + " for " + receivable.getInvestmentSecurity().getSymbol() + " from "
					+ DateUtils.fromDateShort(event.getDayBeforeExDate()));
		}
		else {
			receivable.setDescription("Accrual of " + eventJournalType.getEventType().getName() + " for " + receivable.getInvestmentSecurity().getSymbol() + " to be paid on "
					+ DateUtils.fromDateShort(event.getEventDate()));
		}

		// no quantity or price for cash/currency
		receivable.setInvestmentSecurity(receivable.getInvestmentSecurity().getInstrument().getTradingCurrency());
		receivable.setQuantity(null);
		receivable.setPrice(null);

		// Original transaction date is applied from the copy of oppositeEntry
		receivable.setTransactionDate(transactionDate);
		receivable.setSettlementDate(event.getPaymentDate());

		receivable.setPositionCommission(BigDecimal.ZERO);
		receivable.setOpening(false);

		return receivable;
	}


	/**
	 * For foreign transactions where closing position has different FX that corresponding opening and security has Payment On Open,
	 * update closing position FX and Base Debit/Credit to match these of the opening position and put the rest into Currency Translation Gain/Loss.
	 */
	private void addCurrencyTranslationDetail(AccountingJournal journal, AccountingJournalDetailDefinition realizedEntry) {
		AccountingJournalDetailDefinition closingPosition = realizedEntry.getParentDefinition();
		AccountingTransaction openingTransaction = closingPosition.getParentTransaction();
		InvestmentSecurity security = openingTransaction.getInvestmentSecurity();

		if (!MathUtils.isEqual(closingPosition.getExchangeRateToBase(), openingTransaction.getExchangeRateToBase())) {
			if (!InvestmentUtils.isNoPaymentOnOpen(security)) {
				// apply Currency Translation Gain/Loss for foreign physicals, if non zero
				BigDecimal closingBaseDebitCredit = InvestmentCalculatorUtils.calculateBaseAmount(closingPosition.getLocalDebitCredit(), openingTransaction.getExchangeRateToBase(), closingPosition.getClientInvestmentAccount());

				BigDecimal translationGainLoss = MathUtils.subtract(closingPosition.getBaseDebitCredit(), closingBaseDebitCredit);
				if (!MathUtils.isNullOrZero(translationGainLoss)) {
					AccountingJournalDetail currencyTranslation = new AccountingJournalDetail();
					BeanUtils.copyProperties(realizedEntry, currencyTranslation);
					currencyTranslation.setQuantity(null);
					currencyTranslation.setPrice(null);
					currencyTranslation.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS));
					currencyTranslation.setHoldingInvestmentAccount(openingTransaction.getHoldingInvestmentAccount());
					currencyTranslation.setDescription(AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS + " for " + security.getSymbol());
					currencyTranslation.setLocalDebitCredit(BigDecimal.ZERO);
					currencyTranslation.setBaseDebitCredit(translationGainLoss);
					currencyTranslation.setPositionCostBasis(BigDecimal.ZERO);
					journal.addJournalDetail(currencyTranslation);
				}

				// update closing position to match what's being closed (proportional using quantity to what was open)
				closingPosition.setExchangeRateToBase(openingTransaction.getExchangeRateToBase());
				closingPosition.setBaseDebitCredit(closingBaseDebitCredit);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public boolean isAccrualReversal() {
		return this.accrualReversal;
	}


	public void setAccrualReversal(boolean accrualReversal) {
		this.accrualReversal = accrualReversal;
	}
}
