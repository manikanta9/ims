package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.marketdata.MarketDataRetriever;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The AccountingStockDividendBookingRule class add corresponding {@link AccountingJournalDetail} entries for each event journal detail.
 * It supports delayed events (accrual and reversal) as well as events with no delay.
 * It also supports both taxable (new lot using Event Date price for cost basis) and non-taxable (close and reopen existing lot while keeping
 * original transaction date and combined cost basis) events.
 *
 * @author vgomelsky
 */
public class AccountingStockDividendBookingRule extends BaseEventJournalBookingRule {

	public static final BigDecimal ROUNDING_ERROR_THRESHOLD = new BigDecimal("0.02");

	/**
	 * Specifies whether this rule should be accruing stock dividend (or actually getting the dividend).
	 */
	private boolean accrual;
	/**
	 * Taxable and non-taxable (deferred taxes that may be taxed at lower rate) events generate different sets of GL entries.
	 */
	private boolean taxable;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private InvestmentCalculator investmentCalculator;
	private MarketDataRetriever marketDataRetriever;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return super.toString() + ", accrual = " + isAccrual() + ", taxable = " + isTaxable();
	}


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		// add debit and credit lines for each payment
		Date transactionDate = getTransactionDate(eventJournal, isAccrual());
		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			AccountingEventJournalDetail.StockDividend stockDividend = eventDetail.forStockDividend();
			BigDecimal additionalQuantity = MathUtils.subtract(stockDividend.getNewQuantity(), stockDividend.getOldQuantity());

			if (!MathUtils.isNullOrZero(additionalQuantity)) {
				// adjust stock dividend new price following rounding adjustments for fractional shares
				stockDividend.setOpeningPrice(InvestmentCalculatorUtils.getNotionalCalculator(stockDividend.getEventSecurity())
						.calculatePriceFromNotional(stockDividend.getOpeningQuantity(), stockDividend.getOpeningCostBasis(), stockDividend.getEventSecurity().getPriceMultiplier()));

				if (isAccrual()) {
					if (!isTaxable()) {
						// close existing position and reopen at same quantity but using latest price (flexible event date lookup)
						addOldPositionCloseAndReopen(journal, stockDividend, transactionDate, false);
					}
					// add receivable at same latest price
					addReceivableForNewPosition(journal, stockDividend, additionalQuantity, transactionDate);
				}
				else if (eventJournal.getSecurityEvent().isPaymentDelayed()) {
					// REVERSAL
					AccountingJournalDetailDefinition reversal = addReversalOfReceivableForNewPosition(journal, stockDividend, additionalQuantity, transactionDate);
					AccountingJournalDetail newPosition;
					AccountingPosition parentReopeningPosition = findParentReopeningPosition(bookingSession, journal, eventDetail, transactionDate);
					if (parentReopeningPosition == null) {
						// replace receivable with new position
						newPosition = addNewPosition(journal, stockDividend, additionalQuantity, transactionDate);
					}
					else {
						// Parent Reopening position is still open, close it
						AccountingJournalDetail closingDetail = getClosingDetail(journal, stockDividend, transactionDate, parentReopeningPosition);
						journal.addJournalDetail(closingDetail);
						// open combined position (existing position + receivable)
						BigDecimal combinedQuantity = MathUtils.add(parentReopeningPosition.getRemainingQuantity(), additionalQuantity);
						BigDecimal combinedCostBasis = MathUtils.add(parentReopeningPosition.getRemainingCostBasis(), reversal.getPositionCostBasis().abs());
						newPosition = getReopeningDetail(journal, stockDividend, transactionDate, combinedQuantity, combinedCostBasis);
						journal.addJournalDetail(newPosition);
					}
					addFractionalPositionClosing(journal, stockDividend, transactionDate);
					getCurrentOpenPositions(bookingSession, newPosition); // retrieve from GL in case there are existing positions
					bookingSession.addNewCurrentPosition(newPosition);
				}
				else {
					// NO DELAY: position + if Fractional Shares: close the fraction for using event date price (realized gain/loss + cash)
					if (isTaxable()) {
						addNewPosition(journal, stockDividend, additionalQuantity, transactionDate);
					}
					else {
						AccountingJournalDetail reopening = addOldPositionCloseAndReopen(journal, stockDividend, transactionDate, true);
						bookingSession.addNewCurrentPosition(reopening);
					}

					addFractionalPositionClosing(journal, stockDividend, transactionDate);
				}
			}
		}

		CoreMathUtils.applySumPropertyDifference(
				new ArrayList<>(CollectionUtils.asNonNullList(journal.getJournalDetailList())),
				AccountingJournalDetailDefinition::getBaseDebitCredit,
				AccountingJournalDetailDefinition::setBaseDebitCredit,
				BigDecimal.ZERO,
				() -> journal.getJournalDetailList().stream()
						.min((a, b) -> MathUtils.compare(a.getBaseDebitCredit().abs(), b.getBaseDebitCredit().abs())).orElseThrow(RuntimeException::new),
				ROUNDING_ERROR_THRESHOLD
		);

		bookingSession.sortPositions(AccountingPositionOrders.FIFO);
	}


	private AccountingJournalDetailDefinition findParentReopeningJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventDetail) {
		if (journal.getParent() == null) {
			return null;
		}
		return CollectionUtils.getStream(journal.getParent().getJournalDetailList())
				.filter(detail -> detail.getParentTransaction().getId().equals(eventDetail.getAccountingTransaction().getId()) && detail.isOpening() && detail.getAccountingAccount().getName().equals(AccountingAccount.ASSET_POSITION))
				.findFirst().orElse(null);
	}


	private AccountingPosition findParentReopeningPosition(BookingSession<?> bookingSession, AccountingJournal journal, AccountingEventJournalDetail eventDetail, Date transactionDate) {
		AccountingJournalDetailDefinition reopeningDetail = findParentReopeningJournalDetail(journal, eventDetail);
		if (reopeningDetail == null) {
			return null;
		}
		List<BookingPosition> bookingPositionList = getCurrentOpenPositions(bookingSession, reopeningDetail, transactionDate); // retrieve from GL in case there are existing positions
		return CollectionUtils.getStream(CollectionUtils.getConverted(bookingPositionList, BookingPosition::getPosition))
				.filter(accountingPosition -> accountingPosition.getOpeningTransaction().getParentTransaction().getId().equals(reopeningDetail.getParentTransaction().getId()))
				.findFirst().orElse(null);
	}


	private List<BookingPosition> getCurrentOpenPositions(BookingSession<?> bookingSession, AccountingJournalDetailDefinition journalDetail, Date paymentDate) {
		List<BookingPosition> currentOpenPositions = new ArrayList<>();
		Map<String, List<BookingPosition>> positionsByClient = bookingSession.getCurrentPositions();
		String key = bookingSession.getPositionKey(journalDetail);
		if (positionsByClient.containsKey(key)) {
			currentOpenPositions = positionsByClient.get(key);
		}
		else {
			AccountingPositionCommand command = AccountingPositionCommand.onPositionSettlementDate(paymentDate);
			command.setClientInvestmentAccountId(journalDetail.getClientInvestmentAccount().getId());
			command.setHoldingInvestmentAccountId(journalDetail.getHoldingInvestmentAccount().getId());
			command.setInvestmentSecurityId(journalDetail.getInvestmentSecurity().getId());
			command.setOrder(ObjectUtils.coalesce(getClosingOrder(), AccountingPositionOrders.FIFO)); // will sort in the end
			List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(command);
			// match only positions for the same accounting account (prevents Collateral positions from closing non-Collateral positions and visa versa)
			positionList = BeanUtils.filter(positionList, AccountingPosition::getAccountingAccount, journalDetail.getAccountingAccount());
			if (positionList == null) {
				positionList = Collections.emptyList();
			}
			Map<Boolean, List<AccountingPosition>> positionMap = BeanUtils.getBeansMap(positionList, position -> MathUtils.isPositive(position.getRemainingQuantity()));
			for (AccountingPosition openPosition : CollectionUtils.getIterable(positionMap.get(true))) {
				boolean fullyClosedPosition = false;
				List<AccountingPosition> closingPositionList = positionMap.get(false);
				while (!CollectionUtils.isEmpty(closingPositionList)) {
					AccountingPosition closingPosition = closingPositionList.remove(0);
					// adjust position lot by the closing lots: add because closing lot already has opposite sign
					openPosition.setRemainingQuantity(MathUtils.add(openPosition.getRemainingQuantity(), closingPosition.getRemainingQuantity()));
					openPosition.setRemainingBaseDebitCredit(MathUtils.add(openPosition.getRemainingBaseDebitCredit(), closingPosition.getRemainingBaseDebitCredit()));
					openPosition.setRemainingCostBasis(MathUtils.add(openPosition.getRemainingCostBasis(), closingPosition.getRemainingCostBasis()));
					fullyClosedPosition = MathUtils.isNullOrZero(openPosition.getRemainingQuantity()) && MathUtils.isNullOrZero(openPosition.getRemainingCostBasis()) && MathUtils.isNullOrZero(openPosition.getRemainingBaseDebitCredit());
				}
				if (!fullyClosedPosition) {
					currentOpenPositions.add(new BookingPosition(openPosition));
				}
			}
			positionsByClient.put(key, currentOpenPositions);
		}
		return currentOpenPositions;
	}


	private AccountingJournalDetail getClosingDetail(AccountingJournal journal, AccountingEventJournalDetail.StockDividend stockDividend, Date transactionDate) {
		return getClosingDetail(journal, stockDividend, transactionDate, null);
	}


	private AccountingJournalDetail getClosingDetail(AccountingJournal journal, AccountingEventJournalDetail.StockDividend stockDividend, Date transactionDate, AccountingPosition parentReopeningPosition) {
		AccountingJournalDetail closingDetail;
		if (parentReopeningPosition != null) {
			closingDetail = generateAccountingJournalDetail(journal, stockDividend.getDetail(), parentReopeningPosition.getRemainingQuantity(), parentReopeningPosition.getRemainingCostBasis(), transactionDate);
			closingDetail.setParentTransaction((AccountingTransaction) parentReopeningPosition.getOpeningTransaction());
			closingDetail.setOriginalTransactionDate(parentReopeningPosition.getOriginalTransactionDate());
			closingDetail.setPrice(stockDividend.getNewPrice());
		}
		else {
			closingDetail = generateAccountingJournalDetail(journal, stockDividend.getDetail(), stockDividend.getOldQuantity(), stockDividend.getOldCostBasis(), transactionDate);
			closingDetail.setOriginalTransactionDate(stockDividend.getOriginalTransactionDate());
		}
		closingDetail.setOpening(false);
		closingDetail.reverseSignForGLAmounts();
		closingDetail.setDescription("Closing from " + closingDetail.getDescription());
		return closingDetail;
	}


	private AccountingJournalDetail getReopeningDetail(AccountingJournal journal, AccountingEventJournalDetail.StockDividend stockDividend, Date transactionDate, BigDecimal quantity, BigDecimal costBasis) {
		AccountingJournalDetail reopeningDetail = generateAccountingJournalDetail(journal, stockDividend.getDetail(), quantity, costBasis, transactionDate);
		reopeningDetail.setOpening(true);
		reopeningDetail.setPrice(stockDividend.getNewPrice());
		reopeningDetail.setDescription("Reopening from " + reopeningDetail.getDescription());
		reopeningDetail.setOriginalTransactionDate(stockDividend.getOriginalTransactionDate());
		return reopeningDetail;
	}


	private AccountingJournalDetail addOldPositionCloseAndReopen(AccountingJournal journal, AccountingEventJournalDetail.StockDividend stockDividend, Date transactionDate, boolean reopenNewQuantity) {
		AccountingJournalDetail closingDetail = getClosingDetail(journal, stockDividend, transactionDate);
		journal.addJournalDetail(closingDetail);

		BigDecimal reopenQuantity = reopenNewQuantity ? stockDividend.getNewQuantity() : stockDividend.getOldQuantity();
		BigDecimal reopenCostBasis = reopenNewQuantity ? stockDividend.getNewCostBasis() : getInvestmentCalculator().calculateNotional(stockDividend.getEventSecurity(), stockDividend.getNewPrice(), reopenQuantity, BigDecimal.ONE);
		AccountingJournalDetail reopeningDetail = getReopeningDetail(journal, stockDividend, transactionDate, reopenQuantity, reopenCostBasis);
		journal.addJournalDetail(reopeningDetail);
		return reopeningDetail;
	}


	private void addFractionalPositionClosing(AccountingJournal journal, AccountingEventJournalDetail.StockDividend stockDividend, Date transactionDate) {
		if (!MathUtils.isNullOrZero(stockDividend.getFractionToClose())) {
			BigDecimal eventDatePrice = getMarketDataRetriever().getPrice(stockDividend.getEventSecurity(), stockDividend.getEventDate(), true, "Stock Dividend:");
			BigDecimal fractionalProceeds = InvestmentCalculatorUtils.getNotionalCalculator(stockDividend.getEventSecurity()).calculateNotional(eventDatePrice, stockDividend.getEventSecurity().getPriceMultiplier(), stockDividend.getFractionToClose());
			AccountingJournalDetail fractionalClosing = generateAccountingJournalDetail(journal, stockDividend.getDetail(), stockDividend.getFractionToClose(), fractionalProceeds, transactionDate);
			fractionalClosing.setParentTransaction(null);
			fractionalClosing.setPrice(eventDatePrice);
			fractionalClosing.reverseSignForGLAmounts();
			fractionalClosing.setDescription("Fractional Shares Closing from rounding from " + fractionalClosing.getDescription());

			AccountingJournalDetail cashProceeds = createCashCurrencyRecord(fractionalClosing);
			cashProceeds.setDescription(cashProceeds.getDescription() + " " + stockDividend.getJournalType().getName() + " fractional shares");

			journal.addJournalDetail(fractionalClosing);
			journal.addJournalDetail(cashProceeds);
			// NOTE: realized gain/loss will be calculated and added by the next rule
		}
	}


	private AccountingJournalDetail addReceivableForNewPosition(AccountingJournal journal, AccountingEventJournalDetail.StockDividend stockDividend, BigDecimal additionalQuantity, Date transactionDate) {
		AccountingJournalDetail positionReceivable = generateAccountingJournalDetail(journal, stockDividend.getDetail(), additionalQuantity, getNewLotPositionCostBasis(stockDividend, additionalQuantity), transactionDate);
		positionReceivable.setOpening(true);
		AccountingAccount positionAccount = stockDividend.getAccountingAccount();
		AccountingAccount receivableAccount = positionAccount.isReceivable() ? positionAccount : positionAccount.getReceivableAccount();
		ValidationUtils.assertNotNull(receivableAccount, "Receivable GL Account is not defined for: " + positionAccount.getLabel());
		positionReceivable.setAccountingAccount(receivableAccount);
		positionReceivable.setPrice(stockDividend.getNewPrice());
		positionReceivable.setDescription("Receivable from " + positionReceivable.getDescription());
		if (!isTaxable()) {
			positionReceivable.setOriginalTransactionDate(positionReceivable.getParentTransaction().getOriginalTransactionDate());
		}
		positionReceivable.setSettlementDate(stockDividend.getEventDate());
		journal.addJournalDetail(positionReceivable);

		return positionReceivable;
	}


	private AccountingJournalDetail addReversalOfReceivableForNewPosition(AccountingJournal journal, AccountingEventJournalDetail.StockDividend stockDividend, BigDecimal additionalQuantity, Date transactionDate) {
		AccountingJournalDetail reversal = addReceivableForNewPosition(journal, stockDividend, additionalQuantity, transactionDate);
		reversal.setOpening(false);
		reversal.reverseSignForGLAmounts();
		reversal.setDescription("Reversal of " + reversal.getDescription());

		// update reversal parent to reference transaction that it is reversing
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setParentId(reversal.getParentTransaction().getId());
		searchForm.setFkFieldId(stockDividend.getDetail().getId());
		searchForm.setJournalTypeId(journal.getJournalType().getId());
		searchForm.setAccountingAccountId(reversal.getAccountingAccount().getId());
		searchForm.setOpening(true);
		searchForm.setQuantity(MathUtils.negate(reversal.getQuantity()));
		List<AccountingTransaction> receivableList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		if (CollectionUtils.isEmpty(receivableList)) {
			// when reversing: there must always be a matching accrual
			throw new IllegalStateException("Cannot find accrual receivable position to reverse for " + stockDividend);
		}
		if (receivableList.size() > 1) {
			throw new IllegalStateException("Cannot have more than one accrual receivable to reverse for " + stockDividend + ": " + receivableList);
		}
		reversal.setParentTransaction(receivableList.get(0));
		return reversal;
	}


	private AccountingJournalDetail addNewPosition(AccountingJournal journal, AccountingEventJournalDetail.StockDividend stockDividend, BigDecimal additionalQuantity, Date transactionDate) {
		AccountingJournalDetail positionDetail = generateAccountingJournalDetail(journal, stockDividend.getDetail(), additionalQuantity, getNewLotPositionCostBasis(stockDividend, additionalQuantity), transactionDate);
		positionDetail.setOpening(true);
		positionDetail.setAccountingAccount(stockDividend.getAccountingAccount());
		positionDetail.setPrice(stockDividend.getNewPrice());
		if (!isTaxable()) {
			positionDetail.setOriginalTransactionDate(positionDetail.getParentTransaction().getOriginalTransactionDate());
		}
		positionDetail.setSettlementDate(stockDividend.getEventDate());
		journal.addJournalDetail(positionDetail);

		return positionDetail;
	}


	private BigDecimal getNewLotPositionCostBasis(AccountingEventJournalDetail.StockDividend stockDividend, BigDecimal additionalQuantity) {
		if (isTaxable()) {
			return getInvestmentCalculator().calculateNotional(stockDividend.getEventSecurity(), stockDividend.getNewPrice(), additionalQuantity, BigDecimal.ONE);
		}

		// combined cost basis for old lot re-open and new lot should stay the same as old cost basis: back into it
		BigDecimal oldLotReopeningCostBasis = getInvestmentCalculator().calculateNotional(stockDividend.getEventSecurity(), stockDividend.getNewPrice(), stockDividend.getOldQuantity(), BigDecimal.ONE);
		return MathUtils.subtract(stockDividend.getOldCostBasis(), oldLotReopeningCostBasis);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isAccrual() {
		return this.accrual;
	}


	public void setAccrual(boolean accrual) {
		this.accrual = accrual;
	}


	public boolean isTaxable() {
		return this.taxable;
	}


	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
