package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Set;


/**
 * The <code>AccountingSplitEventBookingRule</code> class handles booking of stock splits.
 * It fully closes existing stock positions and opens new positions with identical debit/credit/cost basis amounts
 * but different quantity and price.  New positions keeps Original Transaction Date of the closed positions.
 * <p/>
 * Note: Security market data values prior to the split need to be adjusted to reflect the change.
 *
 * @author vgomelsky
 */
public class AccountingSplitEventBookingRule extends BaseEventJournalBookingRule {

	private MarketDataRetriever marketDataRetriever;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		// close all open positions being split and open new positions that reflect the new price/quantity
		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			AccountingEventJournalDetail.StockSplit stockSplit = eventDetail.forStockSplit();
			// adjust stock dividend new price following rounding adjustments for fractional shares
			stockSplit.setOpeningPrice(InvestmentCalculatorUtils.getNotionalCalculator(stockSplit.getEventSecurity())
					.calculatePriceFromNotional(stockSplit.getOpeningQuantity(), stockSplit.getOpeningCostBasis(), stockSplit.getEventSecurity().getPriceMultiplier()));

			AccountingJournalDetail splitClosing = generateJournalDetail(journal, stockSplit, true);
			populateJournalDetail(splitClosing, stockSplit.getClosingQuantity().negate(), stockSplit.getClosingPrice(), stockSplit.getClosingCostBasis().negate());
			journal.addJournalDetail(splitClosing);

			AccountingJournalDetail splitOpening = generateJournalDetail(journal, stockSplit, false);
			populateJournalDetail(splitOpening, stockSplit.getOpeningQuantity(), stockSplit.getOpeningPrice(), stockSplit.getOpeningCostBasis());
			journal.addJournalDetail(splitOpening);

			bookingSession.addNewCurrentPosition(splitOpening);

			// if fractional shares: immediately partially "close" the fractional portion using Event Date price and realize gain/loss: Cash in Lieu
			if (!MathUtils.isNullOrZero(stockSplit.getFractionToClose())) {
				AccountingJournalDetail fractionalClosing = generateJournalDetail(journal, stockSplit, true);
				fractionalClosing.setParentTransaction(null);
				fractionalClosing.setParentDefinition(null);
				BigDecimal eventDatePrice = getMarketDataRetriever().getPrice(stockSplit.getEventSecurity(), stockSplit.getEventDate(), true, "Stock Split:");
				populateJournalDetail(fractionalClosing, stockSplit.getFractionToClose().negate(), eventDatePrice, null);
				fractionalClosing.setDescription("Fractional Shares Closing from rounding from " + fractionalClosing.getDescription());

				journal.addJournalDetail(fractionalClosing);
				journal.addJournalDetail(createCashCurrencyRecord(fractionalClosing));
				// NOTE: realized gain/loss will be calculated and added by the next rule
			}
		}

		bookingSession.sortPositions(AccountingPositionOrders.FIFO);
	}


	private AccountingJournalDetail generateJournalDetail(AccountingJournal journal, AccountingEventJournalDetail.StockSplit stockSplit, boolean close) {
		InvestmentSecurityEvent event = stockSplit.getDetail().getJournal().getSecurityEvent();
		AccountingTransaction affectedLot = stockSplit.getDetail().getAccountingTransaction();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(stockSplit.getDetail().getId());
		detail.setSystemTable(getAccountingEventJournalDetailTable());

		detail.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		detail.setAccountingAccount(affectedLot.getAccountingAccount());
		detail.setExchangeRateToBase(affectedLot.getExchangeRateToBase());
		if (detail.getAccountingAccount().isExecutingBrokerSpecific()) {
			detail.setExecutingCompany(affectedLot.getExecutingCompany());
		}
		populatePositionParentTransaction(detail, affectedLot, close);

		detail.setDescription(stockSplit.getDetail().getJournal().getLabel());

		detail.setSettlementDate(event.getEventDate());
		detail.setOriginalTransactionDate(affectedLot.getOriginalTransactionDate());
		detail.setTransactionDate(event.getEventDate());

		detail.setOpening(!close);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}


	private void populateJournalDetail(AccountingJournalDetail detail, BigDecimal quantity, BigDecimal price, BigDecimal positionCostBasis) {
		detail.setQuantity(quantity);
		detail.setPrice(price);
		if (positionCostBasis == null) {
			// calculate it if not specified
			positionCostBasis = InvestmentCalculatorUtils.getNotionalCalculator(detail.getInvestmentSecurity()).calculateNotional(price, detail.getInvestmentSecurity().getPriceMultiplier(), quantity);
		}
		if (detail.getInvestmentSecurity().getInstrument().getHierarchy().isNoPaymentOnOpen()) {
			detail.setLocalDebitCredit(BigDecimal.ZERO);
			detail.setBaseDebitCredit(BigDecimal.ZERO);
		}
		else {
			detail.setLocalDebitCredit(positionCostBasis);
			detail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(positionCostBasis, detail.getExchangeRateToBase(), detail.getClientInvestmentAccount()));
		}
		detail.setPositionCostBasis(positionCostBasis);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
