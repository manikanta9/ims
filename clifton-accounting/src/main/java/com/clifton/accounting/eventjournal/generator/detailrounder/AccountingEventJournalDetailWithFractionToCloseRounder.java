package com.clifton.accounting.eventjournal.generator.detailrounder;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;


/**
 * The AccountingEventJournalDetailWithFractionToCloseRounder calculates Fraction to Close summation across individual lots and compares it to the aggregate.
 * If they are different, aggregate amount is used and the difference is allocated proportionally across all details.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailWithFractionToCloseRounder implements AccountingEventJournalDetailRounder {

	private FractionToCloseAllocationType allocationType;

	private boolean adjustOpeningCostBasis;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void adjustForLotRounding(AccountingEventJournalDetail aggregateDetail, List<AccountingEventJournalDetail> groupedDetailList) {
		getAllocationType().allocate(aggregateDetail, groupedDetailList, isAdjustOpeningCostBasis());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FractionToCloseAllocationType getAllocationType() {
		return this.allocationType;
	}


	public void setAllocationType(FractionToCloseAllocationType allocationType) {
		this.allocationType = allocationType;
	}


	public boolean isAdjustOpeningCostBasis() {
		return this.adjustOpeningCostBasis;
	}


	public void setAdjustOpeningCostBasis(boolean adjustOpeningCostBasis) {
		this.adjustOpeningCostBasis = adjustOpeningCostBasis;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public enum FractionToCloseAllocationType {

		/**
		 * Allocate proportionally (proportional to each detail's fraction) across all details.
		 */
		PROPORTIONALLY {
			@Override
			public void allocate(AccountingEventJournalDetail aggregateDetail, List<AccountingEventJournalDetail> groupedDetailList, boolean adjustOpeningCostBasis) {
				adjustDetailsForRoundingLosses(aggregateDetail, groupedDetailList, adjustOpeningCostBasis);

				BigDecimal calculatedFractionToClose = CoreMathUtils.sumProperty(groupedDetailList, d -> d.forFractionToClose().getFractionToClose());
				BigDecimal actualFractionToClose = aggregateDetail.forFractionToClose().getFractionToClose();
				BigDecimal wholeSharesFromFractions = MathUtils.subtract(calculatedFractionToClose, actualFractionToClose);

				if (!MathUtils.isNullOrZero(wholeSharesFromFractions)) {
					for (AccountingEventJournalDetail detail : groupedDetailList) {
						AccountingEventJournalDetail.FractionToCloseEventJournalDetail<?> fractionToCloseDetail = detail.forFractionToClose();
						fractionToCloseDetail.setFractionToClose(MathUtils.subtract(fractionToCloseDetail.getFractionToClose(), MathUtils.divide(MathUtils.multiply(fractionToCloseDetail.getFractionToClose(), wholeSharesFromFractions), calculatedFractionToClose)));
					}
				}
			}
		},


		/**
		 * Lump all fractional shares to close into first detail with fraction. Sets all remaining to 0.  It does not matter which one: the closing position will run through the splitter logic.
		 */
		FIRST_DETAIL {
			@Override
			public void allocate(AccountingEventJournalDetail aggregateDetail, List<AccountingEventJournalDetail> groupedDetailList, boolean adjustOpeningCostBasis) {
				adjustDetailsForRoundingLosses(aggregateDetail, groupedDetailList, adjustOpeningCostBasis);

				boolean fractionPopulated = false;
				for (AccountingEventJournalDetail detail : groupedDetailList) {
					AccountingEventJournalDetail.FractionToCloseEventJournalDetail<?> fractionalDetail = detail.forFractionToClose();
					if (!fractionPopulated && !MathUtils.isNullOrZero(fractionalDetail.getFractionToClose())) {
						fractionalDetail.setFractionToClose(aggregateDetail.forFractionToClose().getFractionToClose());
						fractionPopulated = true;
					}
					else {
						fractionalDetail.setFractionToClose(BigDecimal.ZERO);
					}
				}
			}
		};

		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////


		public abstract void allocate(AccountingEventJournalDetail aggregateDetail, List<AccountingEventJournalDetail> groupedDetailList, boolean adjustOpeningCostBasis);


		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////


		/**
		 * Aggregate detail calculations for opening quantity and cost basis may be a little different from these for the sum of all details.
		 * If this is the case, allocate differences back so that nothing is lost from aggregate calculations.
		 */
		private static void adjustDetailsForRoundingLosses(AccountingEventJournalDetail aggregateDetail, List<AccountingEventJournalDetail> groupedDetailList, boolean adjustOpeningCostBasis) {
			AccountingEventJournalDetail.FractionToCloseEventJournalDetail<?> aggregatedFractionToCloseDetail = adjustAggregateDetailWithFractionalSharesRounding(aggregateDetail);

			BigDecimal calculatedFractionToClose = CoreMathUtils.sumProperty(groupedDetailList, d -> d.forFractionToClose().getFractionToClose());
			BigDecimal calculatedRemainingFractionToClose = MathUtils.subtract(calculatedFractionToClose, MathUtils.round(calculatedFractionToClose, 0, BigDecimal.ROUND_DOWN));
			BigDecimal extraFractionToClose = BigDecimal.ZERO;
			if (!MathUtils.isNullOrZero(aggregatedFractionToCloseDetail.getFractionToClose())) {
				extraFractionToClose = MathUtils.subtract(aggregatedFractionToCloseDetail.getFractionToClose(), calculatedRemainingFractionToClose);
			}
			else if (!MathUtils.isNullOrZero(calculatedFractionToClose)) {
				BigDecimal calculatedOpeningQuantity = CoreMathUtils.sumProperty(groupedDetailList, d -> d.forFractionToClose().getOpeningQuantity());
				extraFractionToClose = MathUtils.subtract(aggregatedFractionToCloseDetail.getOpeningQuantity(), calculatedOpeningQuantity);
			}

			// aggregate is most accurate: allocate difference to detail with the largest fractional share
			AccountingEventJournalDetail detailWithMaxFraction = CoreMathUtils.getBeanWithMaxProperty(groupedDetailList, AccountingEventJournalDetail::getAdditionalAmount3, true, d -> !MathUtils.isNullOrZero(d.forFractionToClose().getFractionToClose()));
			if (detailWithMaxFraction == null) {
				// no fractional share, select the largest detail
				detailWithMaxFraction = CoreMathUtils.getBeanWithMaxProperty(groupedDetailList, AccountingEventJournalDetail::getAffectedQuantity, true, d -> !MathUtils.isNullOrZero(d.getAffectedQuantity()));
			}
			AccountingEventJournalDetail.FractionToCloseEventJournalDetail<?> maxFraction = detailWithMaxFraction.forFractionToClose();

			if (!MathUtils.isNullOrZero(extraFractionToClose)) {
				maxFraction.setOpeningQuantity(MathUtils.round(MathUtils.add(maxFraction.getOpeningQuantity(), extraFractionToClose), DataTypes.QUANTITY.getPrecision()))
						.setFractionToClose(MathUtils.subtract(maxFraction.getOpeningQuantity(), MathUtils.round(maxFraction.getOpeningQuantity(), 0, RoundingMode.DOWN)));
			}

			if (adjustOpeningCostBasis) {
				BigDecimal calculatedOpeningCostBasis = CoreMathUtils.sumProperty(groupedDetailList, d -> d.forFractionToClose().getOpeningCostBasis());
				BigDecimal extraOpeningCostBasis = MathUtils.subtract(aggregatedFractionToCloseDetail.getOpeningCostBasis(), calculatedOpeningCostBasis);
				if ((!MathUtils.isNullOrZero(extraOpeningCostBasis))) {
					maxFraction.setOpeningCostBasis(MathUtils.add(maxFraction.getOpeningCostBasis(), extraOpeningCostBasis));
				}
			}
		}


		/**
		 * Adjusts the aggregate detail based on the defined security event {@link FractionalShares} rounder.
		 */
		private static AccountingEventJournalDetail.FractionToCloseEventJournalDetail<?> adjustAggregateDetailWithFractionalSharesRounding(AccountingEventJournalDetail aggregateDetail) {
			AccountingEventJournalDetail.FractionToCloseEventJournalDetail<?> aggregatedFractionToCloseDetail = aggregateDetail.forFractionToClose();

			BigDecimal fractionToClose = aggregatedFractionToCloseDetail.getFractionToClose();
			if (!MathUtils.isNullOrZero(fractionToClose)) {
				BigDecimal openingQuantity = aggregatedFractionToCloseDetail.getOpeningQuantity();
				FractionalShares fractionalSharesRounder = Optional.ofNullable(aggregateDetail.getEventPayout())
						.map(InvestmentSecurityEventPayout::getFractionalSharesMethod)
						.orElse(FractionalShares.DEFAULT);
				aggregatedFractionToCloseDetail.setOpeningQuantity(MathUtils.round(fractionalSharesRounder.round(openingQuantity), DataTypes.QUANTITY.getPrecision()))
						.setFractionToClose(MathUtils.subtract(aggregatedFractionToCloseDetail.getOpeningQuantity(), MathUtils.round(aggregatedFractionToCloseDetail.getOpeningQuantity(), 0, RoundingMode.DOWN)));
			}

			return aggregatedFractionToCloseDetail;
		}
	}
}
