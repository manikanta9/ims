package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.core.util.MathUtils;

import java.util.Date;
import java.util.Set;


/**
 * The {@link AccountingReturnOfCapitalBookingRule} class adds corresponding {@link AccountingJournalDetail} entries for each event journal detail.
 * It closes and reopens an existing lot with new a new cost basis while keeping original transaction date.
 *
 * @author michaelm
 */
public class AccountingReturnOfCapitalBookingRule extends AccountingPaymentAccrualEventBookingRule {

	/**
	 * Specifies whether this rule should be accruing cash amount to be received (Payment Receivable)
	 */
	private boolean accrual;

	/**
	 * Specifies whether this rule should be reversing the accrual of the cash amount to be received (Payment Receivable)
	 */
	private boolean accrualReversal;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return super.toString() + ", accrual = " + isAccrual() + ", accrualReversal = " + isAccrualReversal();
	}


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			AccountingEventJournalDetail.ReturnOfCapital returnOfCapital = eventDetail.forReturnOfCapital();
			Date exDate = eventJournal.getSecurityEvent().getExDate();
			Date paymentDate = EventData.forEventPayoutOrEvent(returnOfCapital.getDetail().getEventPayout(), eventJournal.getSecurityEvent()).getPaymentDate();
			Date originalTransactionDate = returnOfCapital.getOriginalTransactionDate();
			if (isAccrual()) {
				if (!MathUtils.isNullOrZero(returnOfCapital.getPrice())) {
					// 1) Close original position with original cost basis (Credit GL Account ‘Position’)
					// 2) Reopen position with cost basis of (original cost basis - cash payment) (Debit GL Account ‘Position’)
					closeOldPositionAndReopen(bookingSession, journal, returnOfCapital, exDate, originalTransactionDate);
					// 3) Accrue cash amount to be received (Debit GL Account ‘Payment Receivable’)
					AccountingJournalDetail journalDetail = generateJournalDetail(journal, returnOfCapital.getDetail(), true, exDate);
					journalDetail.setOriginalTransactionDate(originalTransactionDate);
					journal.addJournalDetail(journalDetail);
				}
			}
			else if (isAccrualReversal()) {
				// 4) Reverse accrual of Payment Receivable (Credit GL Account ‘Payment Receivable’)
				reverseAccrual(journal, returnOfCapital, paymentDate);
				// 5) Receive cash payment (Debit GL Account ‘Cash’ or 'Currency')
				receiveCashPayment(journal, returnOfCapital, paymentDate);
			}
			else {
				// Ex Date After Event Date (Same Day)
				// 1) Close original position with original cost basis (Credit GL Account ‘Position’)
				// 2) Reopen position with cost basis of (original cost basis - cash payment) (Debit GL Account ‘Position’)
				closeOldPositionAndReopen(bookingSession, journal, returnOfCapital, paymentDate, originalTransactionDate);
				// 3) Receive cash payment (Debit GL Account ‘Cash’ or 'Currency')
				receiveCashPayment(journal, returnOfCapital, paymentDate);
			}
		}

		bookingSession.sortPositions(AccountingPositionOrders.FIFO);
	}


	private AccountingJournalDetail closeOldPositionAndReopen(BookingSession<?> bookingSession, AccountingJournal journal, AccountingEventJournalDetail.ReturnOfCapital returnOfCapital, Date transactionDate, Date originalTransactionDate) {
		InvestmentSecurityEventPayout eventPayout = returnOfCapital.getDetail().getEventPayout();
		InvestmentSecurityEvent event = returnOfCapital.getDetail().getJournal().getSecurityEvent();
		Date paymentDate = EventData.forEventPayoutOrEvent(eventPayout, event).getPaymentDate();
		AssertUtils.assertNotNull(eventPayout, "InvestmentSecurityEventPayout is required in order to reduce cost basis.");

		AccountingTransaction affectedLot = returnOfCapital.getDetail().getAccountingTransaction();
		InvestmentSecurityEvent securityEvent = returnOfCapital.getDetail().getJournal().getSecurityEvent();
		Date settlementDate = EventData.forEventPayoutOrEvent(eventPayout, securityEvent).getPaymentDate();

		AccountingJournalDetail closingDetail = generateAccountingJournalDetail(journal, returnOfCapital.getDetail(), returnOfCapital.getDetail().getAffectedQuantity(), returnOfCapital.getOldCostBasis(), transactionDate);
		closingDetail.setDescription("Parent Closing from " + CoreMathUtils.formatNumberDecimal(returnOfCapital.getPrice()) + " " + event.getType().getName() + " for " + returnOfCapital.getEventSecurity().getSymbol() + " on " + DateUtils.fromDateShort(paymentDate));
		closingDetail.setPrice(affectedLot.getPrice());
		closingDetail.setOpening(false);
		closingDetail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(closingDetail.getLocalDebitCredit(), returnOfCapital.getDetail().getExchangeRateToBase(), affectedLot.getClientInvestmentAccount()));
		closingDetail.reverseSignForGLAmounts();
		closingDetail.setOriginalTransactionDate(originalTransactionDate);
		closingDetail.setSettlementDate(settlementDate);
		journal.addJournalDetail(closingDetail);

		AccountingJournalDetail reopeningDetail = generateAccountingJournalDetail(journal, returnOfCapital.getDetail(), returnOfCapital.getDetail().getAffectedQuantity(), returnOfCapital.getNewCostBasis(), transactionDate);
		reopeningDetail.setDescription("Parent Reopening from " + CoreMathUtils.formatNumberDecimal(returnOfCapital.getPrice()) + " " + event.getType().getName() + " for " + returnOfCapital.getEventSecurity().getSymbol() + " on " + DateUtils.fromDateShort(paymentDate));
		reopeningDetail.setOpening(true);
		reopeningDetail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(reopeningDetail.getLocalDebitCredit(), returnOfCapital.getDetail().getExchangeRateToBase(), affectedLot.getClientInvestmentAccount()));
		reopeningDetail.setPrice(InvestmentCalculatorUtils.getNotionalCalculator(returnOfCapital.getEventSecurity()).calculatePriceFromNotional(reopeningDetail.getQuantity(), reopeningDetail.getPositionCostBasis(), reopeningDetail.getInvestmentSecurity().getPriceMultiplier()));
		reopeningDetail.setOriginalTransactionDate(originalTransactionDate);
		reopeningDetail.setSettlementDate(settlementDate);
		journal.addJournalDetail(reopeningDetail);
		bookingSession.addNewCurrentPosition(reopeningDetail);
		return reopeningDetail;
	}


	private AccountingJournalDetail receiveCashPayment(AccountingJournal journal, AccountingEventJournalDetail.ReturnOfCapital returnOfCapital, Date transactionDate) {
		InvestmentSecurityEvent event = returnOfCapital.getDetail().getJournal().getSecurityEvent();
		Date paymentDate = EventData.forEventPayoutOrEvent(returnOfCapital.getDetail().getEventPayout(), event).getPaymentDate();
		AccountingJournalDetail journalDetail = generateJournalDetail(journal, returnOfCapital.getDetail(), true, transactionDate);
		journalDetail.setDescription(CoreMathUtils.formatNumberDecimal(returnOfCapital.getPrice()) + " " + event.getType().getName() + " for " + returnOfCapital.getEventSecurity().getSymbol() + " paid on " + DateUtils.fromDateShort(paymentDate));
		// If the security pays a foreign currency, use Accounting Account ‘Currency’ instead of ‘Cash’
		InvestmentSecurity localCurrency = returnOfCapital.getDetail().getAccountingTransaction().getInvestmentSecurity().getInstrument().getTradingCurrency();
		InvestmentSecurity baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(returnOfCapital.getDetail().getAccountingTransaction());
		if (localCurrency.equals(baseCurrency)) {
			journalDetail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		}
		else {
			journalDetail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
		}
		journalDetail.setTransactionDate(paymentDate);
		journalDetail.setOriginalTransactionDate(returnOfCapital.getOriginalTransactionDate());
		journal.addJournalDetail(journalDetail);
		return journalDetail;
	}


	private AccountingJournalDetail reverseAccrual(AccountingJournal journal, AccountingEventJournalDetail.ReturnOfCapital returnOfCapital, Date transactionDate) {
		AccountingJournalDetail journalDetail = generateJournalDetail(journal, returnOfCapital.getDetail(), true, transactionDate);
		journalDetail.setDescription("Reverse accrual of " + CoreMathUtils.formatNumberDecimal(returnOfCapital.getPrice()) + " " + returnOfCapital.getDetail().getJournal().getSecurityEvent().getType().getName()
				+ " for " + returnOfCapital.getEventSecurity().getSymbol() + " paid on " + DateUtils.fromDateShort(transactionDate));
		// reverse the accrual amounts on reversal
		journalDetail.setOpening(false);
		if (journalDetail.getQuantity() != null) {
			journalDetail.setQuantity(journalDetail.getQuantity().negate());
		}
		journalDetail.setLocalDebitCredit(journalDetail.getLocalDebitCredit().negate());
		journalDetail.setBaseDebitCredit(journalDetail.getBaseDebitCredit().negate());
		journalDetail.setTransactionDate(transactionDate);
		journalDetail.setOriginalTransactionDate(returnOfCapital.getOriginalTransactionDate());
		journal.addJournalDetail(journalDetail);
		return journalDetail;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isAccrual() {
		return this.accrual;
	}


	public void setAccrual(boolean accrual) {
		this.accrual = accrual;
	}


	public boolean isAccrualReversal() {
		return this.accrualReversal;
	}


	public void setAccrualReversal(boolean accrualReversal) {
		this.accrualReversal = accrualReversal;
	}
}
