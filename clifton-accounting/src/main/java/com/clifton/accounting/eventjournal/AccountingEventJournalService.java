package com.clifton.accounting.eventjournal;


import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalTypeSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>AccountingEventJournalService</code> interface defines methods for working with event journals.
 *
 * @author vgomelsky
 */
public interface AccountingEventJournalService {

	////////////////////////////////////////////////////////////////////////////
	////////         AccountingEventJournal Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournal getAccountingEventJournal(int id);


	public List<AccountingEventJournal> getAccountingEventJournalList(AccountingEventJournalSearchForm searchForm);


	/**
	 * If event journal is booked, unbooks it first.  Updates fields that are allowed to be adjusted with
	 * these of the specified bean and then rebooks the journal if event's Ex/Event dates are in past.
	 *
	 * @return returns updated journal: fields from the specified journal are used to update the actual journal
	 */
	@RequestMapping("accountingEventJournalAdjust")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public AccountingEventJournal adjustAccountingEventJournal(AccountingEventJournal bean);


	public AccountingEventJournal saveAccountingEventJournal(AccountingEventJournal journal);


	/**
	 * Deletes AccountingEventJournal with the specified id and corresponding details from the system.
	 * If the journal was booked, deletion will fail with corresponding ValidationException.
	 */
	public void deleteAccountingEventJournal(int id);


	/**
	 * Deletes AccountingEventJournal with the specified id and corresponding details from the system.
	 * If the journal was booked, unbooks corresponding journal(s) first.
	 */
	@SecureMethod(dtoClass = AccountingEventJournal.class, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void deleteAccountingEventJournalWithForcedUnposting(int id);


	////////////////////////////////////////////////////////////////////////////
	////////     AccountingEventJournalDetail Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalDetail getAccountingEventJournalDetail(int id);


	public List<AccountingEventJournalDetail> getAccountingEventJournalDetailList(AccountingEventJournalDetailSearchForm searchForm);


	@DoNotAddRequestMapping
	public void saveAccountingEventJournalDetailList(List<AccountingEventJournalDetail> beanList);


	////////////////////////////////////////////////////////////////////////////
	////////      AccountingEventJournalType Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalType getAccountingEventJournalType(short id);


	public AccountingEventJournalType getAccountingEventJournalTypeByName(String name);


	public List<AccountingEventJournalType> getAccountingEventJournalTypeListByEventType(short eventTypeId);


	public List<AccountingEventJournalType> getAccountingEventJournalTypeList(AccountingEventJournalTypeSearchForm searchForm);


	public List<AccountingEventJournalType> getAccountingEventJournalTypeList();


	public AccountingEventJournalType saveAccountingEventJournalType(AccountingEventJournalType bean);


	public void deleteAccountingEventJournalType(short id);
}
