package com.clifton.accounting.eventjournal.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>AccountingEventJournalSearchForm</code> class defines search configuration for AccountingEventJournal objects.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField
	private String description;

	@SearchField(searchField = "journalType.id")
	private Short journalTypeId;

	@SearchField(searchField = "name", searchFieldPath = "journalType", comparisonConditions = {ComparisonConditions.EQUALS})
	private String journalTypeName;

	@SearchField(searchField = "securityEvent.id")
	private Integer securityEventId;

	@SearchField(searchField = "security.id", searchFieldPath = "securityEvent")
	private Integer investmentSecurityId;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "securityEvent.security.instrument", sortField = "hierarchy.name")
	private Short securityInstrumentHierarchyId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "securityEvent.security.instrument.hierarchy", sortField = "investmentType.name")
	private Short securityInvestmentTypeId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "securityEvent.security.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "securityEvent.security.instrument")
	private Integer securityCurrencyDenominationId;

	// custom filter: exists for journal details
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "eventDescription", searchFieldPath = "securityEvent")
	private String eventDescription;

	@SearchField(searchField = "eventDate", searchFieldPath = "securityEvent")
	private Date eventDate;

	@SearchField
	private Date bookingDate;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean journalBooked;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean journalNotBooked;

	@SearchField
	private Date accrualReversalBookingDate;

	@SearchField(searchField = "accrualReversalBookingDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean accrualReversalBooked;

	@SearchField(searchField = "accrualReversalBookingDate", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean accrualReversalNotBooked;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getJournalTypeId() {
		return this.journalTypeId;
	}


	public void setJournalTypeId(Short journalTypeId) {
		this.journalTypeId = journalTypeId;
	}


	public Integer getSecurityEventId() {
		return this.securityEventId;
	}


	public void setSecurityEventId(Integer securityEventId) {
		this.securityEventId = securityEventId;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Boolean getJournalBooked() {
		return this.journalBooked;
	}


	public void setJournalBooked(Boolean journalBooked) {
		this.journalBooked = journalBooked;
	}


	public Boolean getJournalNotBooked() {
		return this.journalNotBooked;
	}


	public void setJournalNotBooked(Boolean journalNotBooked) {
		this.journalNotBooked = journalNotBooked;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Date getAccrualReversalBookingDate() {
		return this.accrualReversalBookingDate;
	}


	public void setAccrualReversalBookingDate(Date accrualReversalBookingDate) {
		this.accrualReversalBookingDate = accrualReversalBookingDate;
	}


	public Boolean getAccrualReversalBooked() {
		return this.accrualReversalBooked;
	}


	public void setAccrualReversalBooked(Boolean accrualReversalBooked) {
		this.accrualReversalBooked = accrualReversalBooked;
	}


	public Boolean getAccrualReversalNotBooked() {
		return this.accrualReversalNotBooked;
	}


	public void setAccrualReversalNotBooked(Boolean accrualReversalNotBooked) {
		this.accrualReversalNotBooked = accrualReversalNotBooked;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Short getSecurityInstrumentHierarchyId() {
		return this.securityInstrumentHierarchyId;
	}


	public void setSecurityInstrumentHierarchyId(Short securityInstrumentHierarchyId) {
		this.securityInstrumentHierarchyId = securityInstrumentHierarchyId;
	}


	public Short getSecurityInvestmentTypeId() {
		return this.securityInvestmentTypeId;
	}


	public void setSecurityInvestmentTypeId(Short securityInvestmentTypeId) {
		this.securityInvestmentTypeId = securityInvestmentTypeId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public String getJournalTypeName() {
		return this.journalTypeName;
	}


	public void setJournalTypeName(String journalTypeName) {
		this.journalTypeName = journalTypeName;
	}


	public String getEventDescription() {
		return this.eventDescription;
	}


	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}


	public Integer getSecurityCurrencyDenominationId() {
		return this.securityCurrencyDenominationId;
	}


	public void setSecurityCurrencyDenominationId(Integer securityCurrencyDenominationId) {
		this.securityCurrencyDenominationId = securityCurrencyDenominationId;
	}
}
