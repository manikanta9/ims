package com.clifton.accounting.eventjournal;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingEventJournalDetail</code> class represents a single open lot that will be affected by journal's event.
 * For elections with multiple payouts, there will be multiple journal details for each lot: one per payout.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetail extends BaseEntity<Integer> {

	public static final String TABLE_NAME = "AccountingEventJournalDetail";

	private AccountingEventJournal journal;

	/**
	 * The open lot that the event is affecting: stock position that's getting a dividend payment, etc.
	 */
	private AccountingTransaction accountingTransaction;

	/**
	 * For events that support payout(s), references the corresponding payout record (must be from the same event).
	 * For elections with multiple payouts, there will be multiple journal details for each lot: one per payout.
	 */
	private InvestmentSecurityEventPayout eventPayout;

	/**
	 * The quantity of the specified transaction that's been affected (transaction can be partially closed).
	 */
	private BigDecimal affectedQuantity;

	/**
	 * The cost basis remaining of the specified transaction that's been affected.
	 */
	private BigDecimal affectedCost;

	/**
	 * Examples: total dividend/coupon payment for the specified lot in local currency.
	 * Holds remaining Cost Basis when a new position is created (stock split)
	 */
	private BigDecimal transactionAmount;

	/**
	 * Exchange Rate from Settlement Currency of event to Base Currency of client account when {@link com.clifton.investment.instrument.event.InvestmentSecurityEventType#eventValueInEventCurrencyUnits} = false.
	 * When it's true, uses exchange rate from Addition Security to Settlement Currency.  Settlement Currency = COALESCE(Additional Security, Local Currency).
	 * Most events are in local currency so it's usually 1.
	 */
	private BigDecimal exchangeRateToBase = BigDecimal.ONE;

	/**
	 * When applicable, the price of event transactions: maturity price, dividend price per share, etc.
	 */
	private BigDecimal transactionPrice;

	/**
	 * Some events may require additional amount (LossPercent for a bond Factor Change event)
	 */
	private BigDecimal additionalAmount;

	/**
	 * Another additional amount. Will be 'accrued interest' in the case of Credit Events.
	 */
	private BigDecimal additionalAmount2;

	/**
	 * Used in rare cases when other amount fields are not sufficient.
	 */
	private BigDecimal additionalAmount3;

	/**
	 * Not persisted in the database, but populated during preview/forecast generation
	 * For forecast rows: can save the skip message and keep upcoming event list
	 * For previewing previously generated journal details so can see if amount was overridden
	 */
	@NonPersistentField
	private String skipMessage;

	/**
	 * Not persisted in the database, but
	 */
	@NonPersistentField
	private BigDecimal previewAmount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * For cases with 3 currencies, we may need 2 different exchange rates.  For cases, when {@link #exchangeRateToBase} field is used
	 * for Local to Settlement FX, {@link #additionalAmount3} field can be used for Local to Base FX (only if amount's label is "FX to Base")
	 */
	public BigDecimal getFxToBaseIfDefined() {
		if (getJournal() != null && getJournal().getJournalType() != null) {
			AccountingEventJournalType journalType = getJournal().getJournalType();
			if (journalType.isAdditionalAmount3FxToBase()) {
				return getAdditionalAmount3();
			}
		}
		return null;
	}


	public String getLabel() {
		return (getEventPayout() == null) ? getJournal().getLabel() : getEventPayout().getLabelLong();
	}


	public Date getEventDate() {
		InvestmentSecurityEventPayout payout = getEventPayout();
		if (payout != null && payout.getAdditionalPayoutDate() != null) {
			return payout.getAdditionalPayoutDate();
		}
		return getJournal().getSecurityEvent().getEventDate();
	}


	/**
	 * Accruals have been booked to receivables but reversals to cash have not been booked yet.
	 */
	public boolean isAccrualPostingOnly() {
		if (getJournal().getSecurityEvent().getSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
			return getJournal().getAccrualReversalBookingDate() == null && getJournal().getBookingDate() != null;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournal getJournal() {
		return this.journal;
	}


	public void setJournal(AccountingEventJournal journal) {
		this.journal = journal;
	}


	public AccountingTransaction getAccountingTransaction() {
		return this.accountingTransaction;
	}


	public void setAccountingTransaction(AccountingTransaction accountingTransaction) {
		this.accountingTransaction = accountingTransaction;
	}


	public InvestmentSecurityEventPayout getEventPayout() {
		return this.eventPayout;
	}


	public void setEventPayout(InvestmentSecurityEventPayout eventPayout) {
		this.eventPayout = eventPayout;
	}


	public BigDecimal getAffectedQuantity() {
		return this.affectedQuantity;
	}


	public void setAffectedQuantity(BigDecimal quantity) {
		this.affectedQuantity = quantity;
	}


	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}


	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}


	public BigDecimal getAffectedCost() {
		return this.affectedCost;
	}


	public void setAffectedCost(BigDecimal affectedCost) {
		this.affectedCost = affectedCost;
	}


	public BigDecimal getTransactionPrice() {
		return this.transactionPrice;
	}


	public void setTransactionPrice(BigDecimal transactionPrice) {
		this.transactionPrice = transactionPrice;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getAdditionalAmount() {
		return this.additionalAmount;
	}


	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}


	public BigDecimal getAdditionalAmount2() {
		return this.additionalAmount2;
	}


	public void setAdditionalAmount2(BigDecimal additionalAmount2) {
		this.additionalAmount2 = additionalAmount2;
	}


	public BigDecimal getAccruedInterest() {
		return this.additionalAmount2;
	}


	public void setAccruedInterest(BigDecimal accruedInterest) {
		this.additionalAmount2 = accruedInterest;
	}


	public BigDecimal getAdditionalAmount3() {
		return this.additionalAmount3;
	}


	public void setAdditionalAmount3(BigDecimal additionalAmount3) {
		this.additionalAmount3 = additionalAmount3;
	}


	public String getSkipMessage() {
		return this.skipMessage;
	}


	public void setSkipMessage(String skipMessage) {
		this.skipMessage = skipMessage;
	}


	public BigDecimal getPreviewAmount() {
		return this.previewAmount;
	}


	public void setPreviewAmount(BigDecimal previewAmount) {
		this.previewAmount = previewAmount;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	static abstract class BaseEventJournalDetail {

		private final AccountingEventJournalDetail detail;


		BaseEventJournalDetail(AccountingEventJournalDetail detail) {
			this.detail = detail;
		}

		////////////////////////////////////////////////////////////////////////


		public AccountingEventJournalDetail getDetail() {
			return this.detail;
		}


		public AccountingEventJournalType getJournalType() {
			return getDetail().getJournal().getJournalType();
		}


		@Override
		public String toString() {
			return getDetail().toString();
		}


		public String getLabel() {
			return (getDetail().getEventPayout() == null) ? getDetail().getJournal().getLabel() : getDetail().getEventPayout().getLabelLong();
		}


		public InvestmentSecurity getEventSecurity() {
			return getDetail().getJournal().getSecurityEvent().getSecurity();
		}


		public Date getEventDate() {
			return getDetail().getEventDate();
		}


		public AccountingAccount getAccountingAccount() {
			return getDetail().getAccountingTransaction().getAccountingAccount();
		}


		public Date getOriginalTransactionDate() {
			return getDetail().getAccountingTransaction().getOriginalTransactionDate();
		}
	}


	public String toStringFormatted(boolean includeHeader) {
		StringBuilder result = new StringBuilder();

		AccountingEventJournal eventJournal = getJournal();
		AccountingEventJournalType eventJournalType = eventJournal.getJournalType();
		if (includeHeader) {
			result.append("ID\tPID\tClient #\tHolding #\tGL Account\tTran Date\tFX Rate")
					// before event value headers
					.append('\t').append(eventJournalType.getAffectedQuantityLabel())
					.append('\t').append(eventJournalType.getAffectedCostLabel())
					.append('\t').append(eventJournalType.getTransactionPriceLabel())
					// after event value headers
					.append('\t').append(eventJournalType.getTransactionAmountLabel())
					.append('\t').append(eventJournalType.getAdditionalAmountLabel())
					.append('\t').append(eventJournalType.getAdditionalAmount2Label())
					.append('\t').append(eventJournalType.getAdditionalAmount3Label())
					.append("\tSkip Message");
		}

		AccountingTransaction transaction = getAccountingTransaction();
		AccountingJournalDetailDefinition parentEntity = ObjectUtils.coalesce(transaction.getParentTransaction(), transaction.getParentDefinition());
		Number parentId = parentEntity != null ? parentEntity.getId() : null;

		result.append(getIdentity())
				.append('\t').append(parentId)
				.append('\t').append(transaction.getClientInvestmentAccount().getNumber())
				.append('\t').append(transaction.getHoldingInvestmentAccount().getNumber())
				.append('\t').append(transaction.getAccountingAccount().getName())
				.append('\t').append(DateUtils.fromDateShort(transaction.getOriginalTransactionDate()))
				.append('\t').append(CoreMathUtils.formatNumberDecimal(getExchangeRateToBase()))
				.append('\t').append(CoreMathUtils.formatNumberDecimal(getAffectedQuantity()))
				.append('\t').append(CoreMathUtils.formatNumberDecimal(getAffectedCost()))
				.append('\t').append(CoreMathUtils.formatNumberDecimal(getTransactionPrice()))
				.append('\t').append(CoreMathUtils.formatNumberDecimal(getTransactionAmount()))
				.append('\t').append(CoreMathUtils.formatNumberDecimal(getAdditionalAmount()))
				.append('\t').append(CoreMathUtils.formatNumberDecimal(getAdditionalAmount2()))
				.append('\t').append(CoreMathUtils.formatNumberDecimal(getAdditionalAmount3()))
				.append('\t').append(getSkipMessage());

		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FractionToCloseEventJournalDetail<?> forFractionToClose() {
		return new FractionToCloseEventJournalDetail<>(this);
	}


	public static class FractionToCloseEventJournalDetail<T extends FractionToCloseEventJournalDetail<T>> extends BaseEventJournalDetail {

		FractionToCloseEventJournalDetail(AccountingEventJournalDetail detail) {
			super(detail);
		}


		public BigDecimal getOpeningQuantity() {
			return getDetail().getAdditionalAmount();
		}


		public T setOpeningQuantity(BigDecimal value) {
			getDetail().setAdditionalAmount(value);
			//noinspection unchecked
			return (T) this;
		}


		public BigDecimal getOpeningPrice() {
			return getDetail().getTransactionAmount();
		}


		public T setOpeningPrice(BigDecimal value) {
			getDetail().setTransactionAmount(value);
			//noinspection unchecked
			return (T) this;
		}


		public BigDecimal getOpeningCostBasis() {
			return getDetail().getAdditionalAmount2();
		}


		public T setOpeningCostBasis(BigDecimal value) {
			getDetail().setAdditionalAmount2(value);
			//noinspection unchecked
			return (T) this;
		}


		public BigDecimal getFractionToClose() {
			return getDetail().getAdditionalAmount3();
		}


		public T setFractionToClose(BigDecimal value) {
			getDetail().setAdditionalAmount3(value);
			//noinspection unchecked
			return (T) this;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////          Spinoff            ///////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Spinoff forSpinoff() {
		return new Spinoff(this);
	}


	public static class Spinoff extends FractionToCloseEventJournalDetail<Spinoff> {

		Spinoff(AccountingEventJournalDetail detail) {
			super(detail);
		}

		////////////////////////////////////////////////////////////////////////


		public Spinoff setParentShares(BigDecimal value) {
			getDetail().setAffectedQuantity(value);
			return this;
		}


		public BigDecimal getParentShares() {
			return getDetail().getAffectedQuantity();
		}


		public Spinoff setParentClosingCostBasis(BigDecimal value) {
			getDetail().setAffectedCost(value);
			return this;
		}


		public BigDecimal getParentClosingCostBasis() {
			return getDetail().getAffectedCost();
		}


		public Spinoff setParentNewCostBasis(BigDecimal value) {
			getDetail().setTransactionAmount(value);
			return this;
		}


		public BigDecimal getParentNewCostBasis() {
			return getDetail().getTransactionAmount();
		}


		public Spinoff setParentNewPrice(BigDecimal value) {
			getDetail().setTransactionPrice(value);
			return this;
		}


		public BigDecimal getParentNewPrice() {
			return getDetail().getTransactionPrice();
		}


		public Spinoff setSpinoffCostBasis(BigDecimal value) {
			getDetail().setAdditionalAmount2(value);
			return this;
		}


		public BigDecimal getSpinoffCostBasis() {
			return getDetail().getAdditionalAmount2();
		}


		public Spinoff setSpinoffShares(BigDecimal value) {
			getDetail().setAdditionalAmount(value);
			return this;
		}


		public BigDecimal getSpinoffShares() {
			return getDetail().getAdditionalAmount();
		}


		@Override
		public BigDecimal getOpeningQuantity() {
			return getSpinoffShares();
		}


		@Override
		public Spinoff setOpeningQuantity(BigDecimal value) {
			return setSpinoffShares(value);
		}


		@Override
		public Spinoff setOpeningCostBasis(BigDecimal value) {
			return setSpinoffCostBasis(value);
		}


		@Override
		public BigDecimal getOpeningCostBasis() {
			return getSpinoffCostBasis();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////            Scrip            ///////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Scrip forScrip() {
		return new Scrip(this);
	}


	public static class Scrip extends FractionToCloseEventJournalDetail<Scrip> {

		Scrip(AccountingEventJournalDetail detail) {
			super(detail);
		}


		public BigDecimal getExistingShares() {
			return getDetail().getAffectedQuantity();
		}


		/**
		 * Cash equivalent of selling new shares immediately at the same price: opening cost basis
		 */
		public Scrip setPaymentAmount(BigDecimal paymentAmount) {
			getDetail().setTransactionAmount(paymentAmount);
			return this;
		}


		@Override
		public BigDecimal getOpeningPrice() {
			return getDetail().getTransactionPrice();
		}


		@Override
		public Scrip setOpeningPrice(BigDecimal openingPrice) {
			getDetail().setTransactionPrice(openingPrice);
			return this;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////        Stock Split          ///////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public StockSplit forStockSplit() {
		return new StockSplit(this);
	}


	public static class StockSplit extends FractionToCloseEventJournalDetail<StockSplit> {

		StockSplit(AccountingEventJournalDetail detail) {
			super(detail);
		}

		////////////////////////////////////////////////////////////////////////


		public StockSplit setClosingQuantity(BigDecimal value) {
			getDetail().setAffectedQuantity(value);
			return this;
		}


		public BigDecimal getClosingQuantity() {
			return getDetail().getAffectedQuantity();
		}


		public StockSplit setClosingCostBasis(BigDecimal value) {
			getDetail().setAffectedCost(value);
			return this;
		}


		public BigDecimal getClosingCostBasis() {
			return getDetail().getAffectedCost();
		}


		public StockSplit setClosingPrice(BigDecimal value) {
			getDetail().setTransactionPrice(value);
			return this;
		}


		public BigDecimal getClosingPrice() {
			return getDetail().getTransactionPrice();
		}


		public BigDecimal getFractionalSharesPayment() {
			return MathUtils.subtract(getClosingCostBasis(), getOpeningCostBasis());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////////////          Stock Dividend            ///////////////////////
	////////////////////////////////////////////////////////////////////////////


	public StockDividend forStockDividend() {
		return new StockDividend(this);
	}


	public static class StockDividend extends FractionToCloseEventJournalDetail<StockDividend> {

		StockDividend(AccountingEventJournalDetail detail) {
			super(detail);
		}

		////////////////////////////////////////////////////////////////////////


		public StockDividend setOldQuantity(BigDecimal value) {
			getDetail().setAffectedQuantity(value);
			return this;
		}


		public BigDecimal getOldQuantity() {
			return getDetail().getAffectedQuantity();
		}


		public StockDividend setOldCostBasis(BigDecimal value) {
			getDetail().setAffectedCost(value);
			return this;
		}


		public BigDecimal getOldCostBasis() {
			return getDetail().getAffectedCost();
		}


		public StockDividend setOldPrice(BigDecimal value) {
			getDetail().setTransactionPrice(value);
			return this;
		}


		public BigDecimal getOldPrice() {
			return getDetail().getTransactionPrice();
		}


		public StockDividend setNewQuantity(BigDecimal value) {
			getDetail().setAdditionalAmount(value);
			return this;
		}


		public BigDecimal getNewQuantity() {
			return getDetail().getAdditionalAmount();
		}


		public StockDividend setNewCostBasis(BigDecimal value) {
			getDetail().setAdditionalAmount2(value);
			return this;
		}


		public BigDecimal getNewCostBasis() {
			return getDetail().getAdditionalAmount2();
		}


		public StockDividend setNewPrice(BigDecimal value) {
			getDetail().setTransactionAmount(value);
			return this;
		}


		public BigDecimal getNewPrice() {
			return getDetail().getTransactionAmount();
		}


		@Override
		public BigDecimal getOpeningQuantity() {
			return getNewQuantity();
		}


		@Override
		public StockDividend setOpeningQuantity(BigDecimal value) {
			return setNewQuantity(value);
		}


		@Override
		public StockDividend setOpeningCostBasis(BigDecimal value) {
			return setNewCostBasis(value);
		}


		@Override
		public BigDecimal getOpeningCostBasis() {
			return getNewCostBasis();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////////////             Maturity               ///////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Maturity forMaturity() {
		return new Maturity(this);
	}


	public static class Maturity extends BaseEventJournalDetail {

		Maturity(AccountingEventJournalDetail detail) {
			super(detail);
		}


		public BigDecimal getOpeningPrice() {
			return getDetail().getTransactionAmount();
		}


		public void setOpeningPrice(BigDecimal openingPrice) {
			getDetail().setTransactionAmount(openingPrice);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////////////         Forward Maturity           ///////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ForwardMaturity forForwardMaturity() {
		return new ForwardMaturity(this);
	}


	public static class ForwardMaturity extends Maturity {

		ForwardMaturity(AccountingEventJournalDetail detail) {
			super(detail);
		}


		public BigDecimal getMaturityPrice() {
			return getDetail().getTransactionPrice();
		}


		public void setMaturityPrice(BigDecimal maturityPrice) {
			getDetail().setTransactionPrice(maturityPrice);
		}


		public InvestmentSecurity getCurrency1() {
			return getEventSecurity().getInstrument().getTradingCurrency();
		}


		public BigDecimal getCurrencyAmount1() {
			return MathUtils.negate(getDetail().getAffectedCost());
		}


		public void setCurrencyAmount1(BigDecimal currencyAmount1) {
			getDetail().setAffectedCost(currencyAmount1);
		}


		public BigDecimal getExchangeRate1() {
			return getDetail().getExchangeRateToBase();
		}


		public void setExchangeRate1(BigDecimal exchangeRate1) {
			getDetail().setExchangeRateToBase(exchangeRate1);
		}


		public InvestmentSecurity getCurrency2() {
			return getEventSecurity().getInstrument().getUnderlyingInstrument().getTradingCurrency();
		}


		public BigDecimal getCurrencyAmount2() {
			return getDetail().getAffectedQuantity();
		}


		public void setCurrencyAmount2(BigDecimal currencyAmount2) {
			getDetail().setAffectedQuantity(currencyAmount2);
		}


		public BigDecimal getExchangeRate2() {
			return getDetail().getAdditionalAmount3();
		}


		public void setExchangeRate2(BigDecimal exchangeRate2) {
			getDetail().setAdditionalAmount3(exchangeRate2);
		}


		public Date getFixingDate() {
			return getEventSecurity().getEndDate();
		}


		public Date getSettlementDate() {
			return getEventSecurity().getFirstDeliveryDate();
		}


		public String getSymbol() {
			return getEventSecurity().getSymbol();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////////////          Return of Capital            ////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReturnOfCapital forReturnOfCapital() {
		return new ReturnOfCapital(this);
	}


	public static class ReturnOfCapital extends BaseEventJournalDetail {

		ReturnOfCapital(AccountingEventJournalDetail detail) {
			super(detail);
		}


		public ReturnOfCapital setOldCostBasis(BigDecimal value) {
			getDetail().setAffectedCost(value);
			return this;
		}


		public BigDecimal getOldCostBasis() {
			return getDetail().getAffectedCost();
		}


		public ReturnOfCapital setPrice(BigDecimal value) {
			getDetail().setTransactionPrice(value);
			return this;
		}


		public BigDecimal getPrice() {
			return getDetail().getTransactionPrice();
		}


		public ReturnOfCapital setPaymentAmount(BigDecimal value) {
			getDetail().setTransactionAmount(value);
			return this;
		}


		public BigDecimal getPaymentAmount() {
			return getDetail().getTransactionAmount();
		}


		public ReturnOfCapital setNewCostBasis(BigDecimal value) {
			getDetail().setAdditionalAmount2(value);
			return this;
		}


		public BigDecimal getNewCostBasis() {
			return getDetail().getAdditionalAmount2();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////              Merger             ///////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Merger forMerger() {
		return new Merger(this);
	}


	public static class Merger extends FractionToCloseEventJournalDetail<Merger> {

		Merger(AccountingEventJournalDetail detail) {
			super(detail);
		}


		public BigDecimal getQuantity() {
			return getDetail().getAffectedQuantity();
		}


		public Merger setQuantity(BigDecimal quantity) {
			getDetail().setAffectedQuantity(quantity);
			return this;
		}


		public BigDecimal getOldCostBasis() {
			return getDetail().getAffectedCost();
		}


		public Merger setOldCostBasis(BigDecimal oldCostBasis) {
			getDetail().setAffectedCost(oldCostBasis);
			return this;
		}


		public BigDecimal getPaymentAmount() {
			return getDetail().getTransactionAmount();
		}


		public Merger setPaymentAmount(BigDecimal paymentAmount) {
			getDetail().setTransactionAmount(paymentAmount);
			return this;
		}


		public BigDecimal getPrice() {
			return getDetail().getTransactionPrice();
		}


		public Merger setPrice(BigDecimal price) {
			getDetail().setTransactionPrice(price);
			return this;
		}


		public BigDecimal getNewQuantity() {
			return getDetail().getAdditionalAmount();
		}


		public Merger setNewQuantity(BigDecimal newQuantity) {
			getDetail().setAdditionalAmount(newQuantity);
			return this;
		}


		public BigDecimal getNewCostBasis() {
			return getDetail().getAdditionalAmount2();
		}


		public Merger setNewCostBasis(BigDecimal newCostBasis) {
			getDetail().setAdditionalAmount2(newCostBasis);
			return this;
		}
	}
}
