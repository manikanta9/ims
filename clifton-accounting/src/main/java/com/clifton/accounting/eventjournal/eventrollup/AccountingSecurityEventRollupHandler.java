package com.clifton.accounting.eventjournal.eventrollup;


import com.clifton.accounting.eventjournal.AccountingEventJournal;

import java.util.List;


/**
 * The <code>AccountingSecurityEventRollupHandler</code> defines generic methods
 * that can be used to retrieve objects that extend {@link AccountingSecurityEventRollup}
 * Methods populate first and second event legs and also populate payment info from {@link AccountingEventJournal}
 *
 * @author manderson
 */
public interface AccountingSecurityEventRollupHandler<T extends AccountingSecurityEventRollup> {

	/**
	 * Returns fully populated event rollup for the given class and security event id
	 * - if populatePayments is true will also look up existing payment information from journals, or generate from previews
	 */
	public T getAccountingSecurityEventRollup(Class<T> clazz, int securityEventId, boolean populatePayments);


	/**
	 * Returns a list of rollup events for the given security
	 */
	public List<T> getAccountingSecurityEventRollupList(Class<T> clazz, int securityId);


	/**
	 * Generic save method used to populate event types and security information on each leg and save - should be called after specific information
	 * is validated populated from calling method.
	 */
	public void saveAccountingSecurityEventRollup(T event);


	/**
	 * Deletes both event legs from the database
	 */
	public void deleteAccountingSecurityEventRollup(T event);
}
