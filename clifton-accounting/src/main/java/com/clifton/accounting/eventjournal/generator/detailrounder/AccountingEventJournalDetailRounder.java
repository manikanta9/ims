package com.clifton.accounting.eventjournal.generator.detailrounder;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;

import java.util.List;


/**
 * The AccountingEventJournalDetailRounder interface defines methods for adjusting for rounding mis-matches between aggregate
 * and lot specific calculations.
 *
 * @author vgomelsky
 */
public interface AccountingEventJournalDetailRounder {

	/**
	 * When an even is applied to multiple lots, it's possible that individual lot calculation logic may leave out pennies due to rounding.
	 * If the same calculation was performed for a single aggregate lot, the result may be a little different. This method will be passed the aggregated lot
	 * as well as all details.  It can compare the sum of details to the aggregate and perform detail adjustments as needed.
	 * For example, Cash Dividend Payment, Stock Split, Stock Spinoff, etc.
	 */
	public void adjustForLotRounding(AccountingEventJournalDetail aggregateDetail, List<AccountingEventJournalDetail> groupedDetailList);
}
