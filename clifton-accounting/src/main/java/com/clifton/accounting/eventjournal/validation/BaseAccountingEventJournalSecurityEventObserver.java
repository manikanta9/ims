package com.clifton.accounting.eventjournal.validation;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommandInternal;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.Collection;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.stream.Collectors;


/**
 * <code>BaseAccountingEventJournalSecurityEventObserver</code> is a base class that:
 * <br/>1. Validates/prevents updates of objects that are used by booked event journals.
 * <br/>2. For those used by unbooked journals only - After changes, re-processes existing journals and saves updated journal details
 *
 * @author NickK
 */
public abstract class BaseAccountingEventJournalSecurityEventObserver<T extends IdentityObject> extends SelfRegisteringDaoObserver<T> {

	private AccountingEventJournalService accountingEventJournalService;
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// get and cache original bean where applicable so it available in the after method validation
		getOriginalBean(dao, bean);
	}


	/**
	 * Perform validation after saved, but within transaction to trigger rollback when validation fails.
	 * This validation is here instead of before method to ensure new payouts and elections trigger an event reprocess.
	 * Doing the validation in the before method does not work for new payouts/events because set a bean context property in the before method to then retrieve in the after method fails to find the value (hashcode is based on ID).
	 */
	@Override
	protected void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			BiPredicate<T, T> updatePredicate = (newBean, original) -> true;
			Consumer<List<AccountingEventJournal>> bookedAccountingEventJournalsConsumer;

			if (event.isInsert()) {
				bookedAccountingEventJournalsConsumer = bookedJournals -> bookedAccountingEventJournalViolationOnInsert(bean, bookedJournals);
			}
			else if (event.isDelete()) {
				bookedAccountingEventJournalsConsumer = bookedJournals -> bookedAccountingEventJournalViolationOnDelete(bean, bookedJournals);
			}
			else {
				ObjectWrapper<List<String>> diffProps = new ObjectWrapper<>();
				updatePredicate = (newBean, original) -> {
					diffProps.setObject(CoreCompareUtils.getNoEqualProperties(newBean, original, false, getBeanUpdateNotEqualExcludedProperties()));
					return isBeanUpdatePropertiesInViolation(newBean, original, diffProps.getObject());
				};
				bookedAccountingEventJournalsConsumer = bookedJournals -> bookedAccountingEventJournalViolationOnUpdate(bean, diffProps.getObject(), bookedJournals);
			}

			T original = getOriginalBean(dao, bean);
			if (updatePredicate.test(bean, original)) {
				if (original != null) {
					// This could only be violation for Elections and Payouts. The client will not allow changing this field, but it is added just in case.
					ValidationUtils.assertEquals(getInvestmentSecurityEventForBean(bean), getInvestmentSecurityEventForBean(original), "Unable to update the Security Event referenced by the entity");
				}
				Collection<AccountingEventJournalDetail> eventJournalDetails = getReferencedAccountingEventJournalDetails(bean, original);
				if (!CollectionUtils.isEmpty(eventJournalDetails)) {
					List<AccountingEventJournal> bookedJournalList = eventJournalDetails.stream().map(AccountingEventJournalDetail::getJournal).filter(AccountingEventJournal::isBooked).distinct().collect(Collectors.toList());
					if (!CollectionUtils.isEmpty(bookedJournalList)) {
						bookedAccountingEventJournalsConsumer.accept(bookedJournalList);
					}
					if (isReprocessEventOnUpdate()) {
						// Otherwise Journals Exist - and none of them are booked flag this event to automatically reprocess the existing journals
						getAccountingEventJournalGeneratorService().generateAccountingEventJournal(EventJournalGeneratorCommandInternal.forEvent(getInvestmentSecurityEventForBean(bean), AccountingEventJournalGeneratorTypes.REPROCESS_EXISTING));
					}
				}
			}
		}
	}


	/**
	 * Method to override in order to throw an exception or perform additional validation on a security event related bean when an insert is in violation with a booked journal.
	 * This method does nothing in the base implementation to make extension of this class easier.
	 */
	protected void bookedAccountingEventJournalViolationOnInsert(T bean, List<AccountingEventJournal> bookedJournalList) {
		// Override to validate
	}


	/**
	 * Method to override in order to throw an exception or perform additional validation on a security event related bean when a delete is in violation with a booked journal.
	 * This method does nothing in the base implementation to make extension of this class easier.
	 */
	protected void bookedAccountingEventJournalViolationOnDelete(T bean, List<AccountingEventJournal> bookedJournalList) {
		// Override to validate
	}


	/**
	 * Returns the list of properties to exclude when evaluating modified fields on a saved security event bean.
	 * This method returns description and label by default and can be overridden to modify the list.
	 */
	protected String[] getBeanUpdateNotEqualExcludedProperties() {
		return new String[]{"description", "label"};
	}


	/**
	 * Returns true if the updated properties between the saved bean and the original are in violation.
	 * This method returns true if the different properties is not empty by default.
	 */
	protected boolean isBeanUpdatePropertiesInViolation(T bean, T original, List<String> differentBeanProperties) {
		return !CollectionUtils.isEmpty(differentBeanProperties);
	}


	/**
	 * Method to override in order to throw an exception or perform additional validation on a security event related bean when an update is in violation with a booked journal.
	 * This method does nothing in the base implementation to make extension of this class easier.
	 */
	protected void bookedAccountingEventJournalViolationOnUpdate(T bean, List<String> differentBeanProperties, List<AccountingEventJournal> bookedJournalList) {
		// Override to validate
	}


	/**
	 * Returns whether or not reprocessing of an security event should occur for journals based on the event. Reprocessing should be done for unbooked journals only.
	 */
	protected boolean isReprocessEventOnUpdate() {
		return true;
	}


	/**
	 * Returns the {@link InvestmentSecurityEvent} for the provided security event related bean (e.g. payout, client election, or security event).
	 */
	protected abstract InvestmentSecurityEvent getInvestmentSecurityEventForBean(T bean);


	/**
	 * Returns the {@link AccountingEventJournalDetail}s associated with the security event related bean.
	 */
	protected abstract Collection<AccountingEventJournalDetail> getReferencedAccountingEventJournalDetails(T bean, T original);

	////////////////////////////////////////////////////////////////////////////
	////////                  Getters and Setters                  /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}


	public AccountingEventJournalGeneratorService getAccountingEventJournalGeneratorService() {
		return this.accountingEventJournalGeneratorService;
	}


	public void setAccountingEventJournalGeneratorService(AccountingEventJournalGeneratorService accountingEventJournalGeneratorService) {
		this.accountingEventJournalGeneratorService = accountingEventJournalGeneratorService;
	}
}
