package com.clifton.accounting.eventjournal.generator.positionretriever;


import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.List;


/**
 * The <code>AccountingEventJournalPositionRetriever</code> interface defines methods for retrieving accounting positions
 * that must be affected by the specified event.
 * <p/>
 * Usually events apply to open positions however custom logic may also be applied to some events (Factor Change: Assumed).
 *
 * @author vgomelsky
 */
public interface AccountingEventJournalPositionRetriever {

	/**
	 * Returns AccountingPosition(s) that get affected by the specified securityEvent.
	 *
	 * @param journalType
	 * @param securityEvent
	 * @param clientInvestmentAccountId optional parameter: null to include all client account positions
	 */
	public List<AccountingPosition> getAffectedPositions(AccountingEventJournalType journalType, InvestmentSecurityEvent securityEvent, Integer clientInvestmentAccountId);
}
