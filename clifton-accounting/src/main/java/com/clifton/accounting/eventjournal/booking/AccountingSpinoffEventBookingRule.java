package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Set;


/**
 * The AccountingSpinoffEventBookingRule class generates the following entries for each detail lot:
 * - fully close parent lot at cost
 * - reopen closed parent lot at new price and reduced cost basis (uses Adjustment Factor from the event: the rest is allocated to the spinoff lot)
 * - open spinoff lot (such that the cost basis of both open lots is the same as this of closed parent lot)
 * - if fractional shares are use with Cash in Lieu option, immediately closes fractional portion of the spinoff lot and generates cash/currency from that close
 * (there will also be another booking rule that will calculate realized gain/loss from this close)
 *
 * @author vgomelsky
 */
public class AccountingSpinoffEventBookingRule extends BaseEventJournalBookingRule {

	private MarketDataRetriever marketDataRetriever;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		// close all parent positions and re-open them at new cost basis; open spinoff positions; add cash for fractional shares, if any
		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			AccountingEventJournalDetail.Spinoff spinoff = eventDetail.forSpinoff();

			// close parent lot and reopen at new cost basis
			AccountingJournalDetail parentClosing = newJournalDetail(journal, eventDetail, "Parent Closing from ", true);
			populateJournalDetail(parentClosing, spinoff.getParentShares().negate(), eventDetail.getAccountingTransaction().getPrice(), spinoff.getParentClosingCostBasis().negate());
			journal.addJournalDetail(parentClosing);

			AccountingJournalDetail parentOpening = newJournalDetail(journal, eventDetail, "Parent Reopening from ", false);
			populateJournalDetail(parentOpening, spinoff.getParentShares(), spinoff.getParentNewPrice(), spinoff.getParentNewCostBasis());
			journal.addJournalDetail(parentOpening);

			// open spinoff lot
			InvestmentSecurityEvent securityEvent = eventDetail.getJournal().getSecurityEvent();
			InvestmentSecurity spinoffSecurity = securityEvent.getAdditionalSecurity();
			InvestmentNotionalCalculatorTypes spinoffNotionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(securityEvent.getAdditionalSecurity());
			BigDecimal spinoffOpeningPrice = spinoffNotionalCalculator.calculatePriceFromNotional(spinoff.getSpinoffShares(), spinoff.getSpinoffCostBasis(), spinoffSecurity.getPriceMultiplier());

			AccountingJournalDetail spinoffOpening = newJournalDetail(journal, eventDetail, "Spinoff Opening from ", false);
			spinoffOpening.setInvestmentSecurity(spinoffSecurity);
			// spinoff is always just a Position: moving it to Collateral is done manually as a separate action
			spinoffOpening.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION));
			populatePositionParentTransaction(spinoffOpening, eventDetail.getAccountingTransaction(), false);
			populateJournalDetail(spinoffOpening, spinoff.getSpinoffShares(), spinoffOpeningPrice, spinoff.getSpinoffCostBasis());
			journal.addJournalDetail(spinoffOpening);

			getCurrentOpenPositions(bookingSession, spinoffOpening); // retrieve from GL in case there are existing positions
			bookingSession.addNewCurrentPosition(spinoffOpening);

			// if fractional shares: immediately partially "close" the fractional portion using Event Date price and realize gain/loss: Cash in Lieu
			if (!MathUtils.isNullOrZero(spinoff.getFractionToClose())) {
				AccountingJournalDetail fractionalClosing = newJournalDetail(journal, eventDetail, "Fractional Shares Closing from rounding from ", true);
				fractionalClosing.setInvestmentSecurity(spinoffSecurity);
				fractionalClosing.setAccountingAccount(spinoffOpening.getAccountingAccount());
				fractionalClosing.setParentTransaction(null);
				fractionalClosing.setParentDefinition(null);
				BigDecimal spinoffPrice = getMarketDataRetriever().getPrice(spinoffSecurity, securityEvent.getEventDate(), true, "Spinoff Security:");
				populateJournalDetail(fractionalClosing, spinoff.getFractionToClose().negate(), spinoffPrice, null);

				journal.addJournalDetail(fractionalClosing);
				journal.addJournalDetail(createCashCurrencyRecord(fractionalClosing));
				// NOTE: realized gain/loss will be calculated and added by the next rule
			}
		}

		bookingSession.sortPositions(AccountingPositionOrders.FIFO);
	}


	private AccountingJournalDetail newJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventDetail, String descriptionPrefix, boolean close) {
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(eventDetail.getId());
		detail.setSystemTable(getAccountingEventJournalDetailTable());

		detail.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		detail.setAccountingAccount(affectedLot.getAccountingAccount());
		detail.setExchangeRateToBase(affectedLot.getExchangeRateToBase());
		if (detail.getAccountingAccount().isExecutingBrokerSpecific()) {
			detail.setExecutingCompany(affectedLot.getExecutingCompany());
		}
		populatePositionParentTransaction(detail, affectedLot, close);

		detail.setDescription(eventDetail.getJournal().getLabel());
		if (!StringUtils.isEmpty(descriptionPrefix)) {
			detail.setDescription(descriptionPrefix + detail.getDescription());
		}

		detail.setSettlementDate(event.getEventDate());
		detail.setOriginalTransactionDate(affectedLot.getOriginalTransactionDate());
		detail.setTransactionDate(event.getEventDate());

		detail.setPositionCommission(BigDecimal.ZERO);
		detail.setOpening(!close);

		return detail;
	}


	private void populateJournalDetail(AccountingJournalDetail detail, BigDecimal quantity, BigDecimal price, BigDecimal positionCostBasis) {
		detail.setQuantity(quantity);
		detail.setPrice(price);
		if (positionCostBasis == null) {
			// calculate it if not specified
			positionCostBasis = InvestmentCalculatorUtils.getNotionalCalculator(detail.getInvestmentSecurity()).calculateNotional(price, detail.getInvestmentSecurity().getPriceMultiplier(), quantity);
		}
		if (detail.getInvestmentSecurity().getInstrument().getHierarchy().isNoPaymentOnOpen()) {
			detail.setLocalDebitCredit(BigDecimal.ZERO);
			detail.setBaseDebitCredit(BigDecimal.ZERO);
		}
		else {
			detail.setLocalDebitCredit(positionCostBasis);
			detail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(positionCostBasis, detail.getExchangeRateToBase(), detail.getClientInvestmentAccount()));
		}
		detail.setPositionCostBasis(positionCostBasis);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
