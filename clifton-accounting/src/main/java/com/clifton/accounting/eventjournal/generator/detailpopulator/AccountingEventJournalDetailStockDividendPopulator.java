package com.clifton.accounting.eventjournal.generator.detailpopulator;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;


/**
 * The AccountingEventJournalDetailStockDividendPopulator class calculates old and new quantities, prices and cost basis.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailStockDividendPopulator implements AccountingEventJournalDetailPopulator {

	/**
	 * Taxable and non-taxable (deferred taxes that may be taxed at lower rate) events generate different sets of GL entries.
	 */
	private boolean taxable;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private InvestmentCalculator investmentCalculator;
	private MarketDataRetriever marketDataRetriever;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		AccountingEventJournalDetail.StockDividend stockDividend = command.getJournalDetail().forStockDividend();

		// calculate the additional quantity; rounding will be done via the detail rounder
		EventData eventData = command.getEventData();
		BigDecimal additionalQuantity = MathUtils.multiply(command.getRemainingQuantity(), eventData.getAfterEventValue(), DataTypes.QUANTITY.getPrecision()).stripTrailingZeros();

		stockDividend.setOldQuantity(command.getRemainingQuantity())
				.setOldPrice(command.getOpeningPrice())
				.setOldCostBasis(command.getRemainingCostBasis())
				.setNewQuantity(MathUtils.add(stockDividend.getOldQuantity(), additionalQuantity)) // booking logic will handle fractional shares
				.setNewPrice(getNewPositionPrice(stockDividend, eventData))
				.setNewCostBasis(getNewPositionCostBasis(stockDividend, eventData, additionalQuantity))
				.setFractionToClose(MathUtils.subtract(stockDividend.getNewQuantity(), MathUtils.round(stockDividend.getNewQuantity(), 0, BigDecimal.ROUND_DOWN)));

		return false;
	}


	private BigDecimal getNewPositionPrice(AccountingEventJournalDetail.StockDividend stockDividend, EventData eventData) {
		if (isTaxable()) {
			// taxable event: lookup the latest price available on or before event date
			// if the actual price on Event Date changes by the time of accrual reversal, will need to generate corresponding adjustments
			return getMarketDataRetriever().getPriceFlexible(eventData.getSecurity(), eventData.getEventDate(), true);
		}

		// not taxable event: back into new price using new number of shares
		if (MathUtils.isEqual(stockDividend.getNewQuantity(), stockDividend.getOldQuantity())) {
			return stockDividend.getOldPrice();
		}
		InvestmentNotionalCalculatorTypes notionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(eventData.getSecurity());
		return notionalCalculator.calculatePriceFromNotional(stockDividend.getNewQuantity(), stockDividend.getOldCostBasis(), eventData.getSecurity().getPriceMultiplier());
	}


	private BigDecimal getNewPositionCostBasis(AccountingEventJournalDetail.StockDividend stockDividend, EventData eventData, BigDecimal additionalQuantity) {
		BigDecimal newCostBasis = stockDividend.getOldCostBasis();

		if (isTaxable()) {
			BigDecimal truncatedAdditionalQuantity = MathUtils.round(additionalQuantity, 0, RoundingMode.DOWN);
			if (!MathUtils.isNullOrZero(truncatedAdditionalQuantity)) {
				BigDecimal additionalCostBasis = getInvestmentCalculator().calculateNotional(eventData.getSecurity(), stockDividend.getNewPrice(), truncatedAdditionalQuantity, BigDecimal.ONE);
				newCostBasis = MathUtils.add(newCostBasis, additionalCostBasis);
			}
		}

		// new cost basis stays the same for non-taxable events
		return newCostBasis;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isTaxable() {
		return this.taxable;
	}


	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
