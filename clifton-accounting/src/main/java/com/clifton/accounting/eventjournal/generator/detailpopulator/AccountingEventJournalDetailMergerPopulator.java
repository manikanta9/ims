package com.clifton.accounting.eventjournal.generator.detailpopulator;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.ExchangeRateDateSelectors;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>AccountingEventJournalDetailMergerPopulator</code> calculates currency payment amount, opening lot Quantity, Price, Cost Basis, and Fraction to Close for Mergers.
 * The accounting event journals for merger will have the following fields populated:
 * <br/>- Quantity: existing lot to close quantity
 * <br/>- Old Cost Basis: existing lot to close cost basis
 * <br/>- Payment Amount: currency payout amount calculated with after payout value; null for security payout
 * <br/>- New Quantity: New security quantity for security payout calculated from before and after payout values; null for currency payout
 * <br/>- New Cost Basis: New security cost basis for security payout calculated from security price and new quantity; null for currency payout
 * <br/>- Fraction to Close: New security fractional close quantity resulting from the security payout; null for currency payout
 *
 * @author nickk
 */
public class AccountingEventJournalDetailMergerPopulator implements AccountingEventJournalDetailPopulator {

	private CalendarBusinessDayService calendarBusinessDayService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataRetriever marketDataRetriever;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {

		EventData eventData = command.getEventData();
		ValidationUtils.assertTrue(eventData.isPayoutDefined(), "Missing required payout for event: " + eventData.getLabel());

		AccountingEventJournalDetail.Merger merger = command.getJournalDetail().forMerger();
		InvestmentNotionalCalculatorTypes payoutSecurityNotionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(eventData.getPayoutSecurity());
		if (eventData.getPayout().getPayoutType().isAdditionalSecurityCurrency()) {
			// cash payout
			merger.setPrice(eventData.getAfterEventValue())
					.setPaymentAmount(payoutSecurityNotionalCalculator.calculateNotional(merger.getPrice(), eventData.getPayoutSecurity().getPriceMultiplier(), merger.getQuantity()));
		}
		else {
			merger.setPaymentAmount(null)
					.setNewQuantity(MathUtils.divide(MathUtils.multiply(merger.getQuantity(), eventData.getAfterEventValue()), eventData.getBeforeEventValue()))
					.setFractionToClose(MathUtils.subtract(merger.getNewQuantity(), MathUtils.round(merger.getNewQuantity(), 0, BigDecimal.ROUND_DOWN)));

			if (eventData.isSingleElectionPayout()) {
				// single payout; cost basis remains the same and price is updated to reflect old cost basis and new quantity
				merger.setNewCostBasis(merger.getOldCostBasis())
						.setPrice(payoutSecurityNotionalCalculator.calculatePriceFromNotional(merger.getNewQuantity(), merger.getNewCostBasis(), eventData.getPayoutSecurity().getPriceMultiplier()));
			}
			else {
				// With multiple payouts, cost basis is calculated from the new quantity and price for the event payout to determine if merger results in a gain or loss for the lot.
				// The cost basis is adjusted to the difference between the old cost basis and other payouts of the event depending on gain/loss classification.
				merger.setPrice(getMergerSecurityPayoutPrice(merger))
						.setNewCostBasis(payoutSecurityNotionalCalculator.calculateNotional(merger.getPrice(), merger.getNewQuantity(), eventData.getPayoutSecurity().getPriceMultiplier()));

				BigDecimal currencyPayoutAdjustedOriginalCostBasis = getRemainingCostBasisAfterCurrencyPayoutAdjustment(eventData, merger, command.getJournalDetail());
				if (MathUtils.isLessThan(merger.getNewCostBasis(), currencyPayoutAdjustedOriginalCostBasis)) {
					// loss, use remaining cost basis for new cost basis (no gain loss will be generated)
					merger.setNewCostBasis(currencyPayoutAdjustedOriginalCostBasis);
				}
				else {
					// Gain, add excess payout ((currency + security) - old cost basis) to remaining cost basis (old cost basis - currency payout)
					BigDecimal newCostBasis = MathUtils.add(currencyPayoutAdjustedOriginalCostBasis, MathUtils.subtract(merger.getNewCostBasis(), currencyPayoutAdjustedOriginalCostBasis));
					// Use the min of newCostBasis and old cost basis, gain loss will be calculated
					merger.setNewCostBasis(MathUtils.min(newCostBasis, merger.getOldCostBasis()));
				}
				merger.setPrice(payoutSecurityNotionalCalculator.calculatePriceFromNotional(merger.getNewQuantity(), merger.getNewCostBasis(), eventData.getPayoutSecurity().getPriceMultiplier()));
			}
		}
		return false;
	}


	private BigDecimal getMergerSecurityPayoutPrice(AccountingEventJournalDetail.Merger merger) {
		InvestmentSecurity security = merger.getDetail().getEventPayout().getPayoutSecurity();
		Date businessDayBeforePayout = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(merger.getEventDate()), -1);
		return getMarketDataRetriever().getPrice(security, businessDayBeforePayout, true, "Merger Security:");
	}


	private BigDecimal getRemainingCostBasisAfterCurrencyPayoutAdjustment(EventData eventData, AccountingEventJournalDetail.Merger merger, AccountingEventJournalDetail journalDetail) {
		BigDecimal currencyPayoutAdjustedOriginalCostBasis = eventData.getElectionPayoutList().stream()
				.filter(payout -> payout.getPayoutType().isAdditionalSecurityCurrency())
				.findFirst()
				.map(currencyPayout -> {
					// Adjust cost basis adjustment with exchange rate
					InvestmentNotionalCalculatorTypes notionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(currencyPayout.getPayoutSecurity());
					BigDecimal currencyPayoutValue = notionalCalculator.calculateNotional(currencyPayout.getAfterEventValue(), currencyPayout.getPayoutSecurity().getPriceMultiplier(), merger.getQuantity());
					InvestmentSecurity closingDetailCurrency = eventData.getEvent().getSecurity().getInstrument().getTradingCurrency();
					if (!closingDetailCurrency.equals(currencyPayout.getPayoutSecurity())) {
						// Base amount calculation here is for the currency of the existing position, not base for the client. We want values in the currency of the cost basis
						AccountingEventJournalType eventJournalType = journalDetail.getJournal().getJournalType();
						BigDecimal exchangeRate = getExchangeRate(currencyPayout.getPayoutSecurity(), closingDetailCurrency, merger.getDetail().getAccountingTransaction(), eventData.getEvent(), eventJournalType);
						currencyPayoutValue = InvestmentCalculatorUtils.calculateBaseAmount(currencyPayoutValue, exchangeRate, closingDetailCurrency);
					}
					return MathUtils.subtract(merger.getOldCostBasis(), currencyPayoutValue);
				}).orElse(merger.getOldCostBasis());
		return currencyPayoutAdjustedOriginalCostBasis;
	}


	private BigDecimal getExchangeRate(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, AccountingTransaction transaction, InvestmentSecurityEvent event, AccountingEventJournalType eventJournalType) {
		ExchangeRateDateSelectors dateSelector = eventJournalType.getExchangeRateDateSelector();
		boolean flexibleLookup = (dateSelector == null) || dateSelector.isFlexibleLookupAllowed(event);
		Date fxDate = (dateSelector == null) ? event.getEventDate() : dateSelector.getExchangeRateDate(event, getCalendarBusinessDayService());
		return getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(transaction.fxSourceCompany(), !transaction.fxSourceCompanyOverridden(), fromCurrency.getSymbol(), toCurrency.getSymbol(), fxDate)
				.flexibleLookup(flexibleLookup).fallbackToDefaultDataSource(flexibleLookup));
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
