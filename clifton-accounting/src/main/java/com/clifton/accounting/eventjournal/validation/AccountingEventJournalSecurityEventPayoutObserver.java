package com.clifton.accounting.eventjournal.validation;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>AccountingEventJournalSecurityEventPayoutObserver</code> class:
 * <br/>1. Validates/prevents updates of {@link InvestmentSecurityEventPayout} objects that are used by booked event journals.
 * <br/>2. For those used by unbooked journals only - After changes, re-processes existing journals and saves updated journal details
 * <p>
 * Note: payout description can be modified at any time
 *
 * @author vgomelsky
 * @see BaseAccountingEventJournalSecurityEventObserver
 */
@Component
public class AccountingEventJournalSecurityEventPayoutObserver extends BaseAccountingEventJournalSecurityEventObserver<InvestmentSecurityEventPayout> {


	@Override
	protected void bookedAccountingEventJournalViolationOnInsert(InvestmentSecurityEventPayout eventPayout, List<AccountingEventJournal> bookedJournalList) {
		throw new ValidationException("Cannot insert Security Event Payout with election number " + eventPayout.getElectionNumber() + " that is used by booked event journal: " + CollectionUtils.getFirstElementStrict(bookedJournalList).getLabel());
	}


	@Override
	protected void bookedAccountingEventJournalViolationOnDelete(InvestmentSecurityEventPayout eventPayout, List<AccountingEventJournal> bookedJournalList) {
		throw new ValidationException("Cannot delete Security Event Payout with election number " + eventPayout.getElectionNumber() + " that is used by booked event journal: " + CollectionUtils.getFirstElementStrict(bookedJournalList).getLabel());
	}


	@Override
	protected void bookedAccountingEventJournalViolationOnUpdate(InvestmentSecurityEventPayout eventPayout, List<String> differentBeanProperties, List<AccountingEventJournal> bookedJournalList) {
		throw new ValidationException("Cannot update " + differentBeanProperties + " fields for Security Event Payout that is used by booked event journal: " + CollectionUtils.getFirstElementStrict(bookedJournalList).getLabel());
	}


	@Override
	protected InvestmentSecurityEvent getInvestmentSecurityEventForBean(InvestmentSecurityEventPayout eventPayout) {
		return eventPayout.getSecurityEvent();
	}


	@Override
	protected Collection<AccountingEventJournalDetail> getReferencedAccountingEventJournalDetails(InvestmentSecurityEventPayout eventPayout, InvestmentSecurityEventPayout originalEventPayout) {
		final Set<AccountingEventJournalDetail> journalDetailSet = new HashSet<>();

		AccountingEventJournalDetailSearchForm searchForm = new AccountingEventJournalDetailSearchForm();
		if (!eventPayout.isNewBean()) {
			searchForm.setEventPayoutId(eventPayout.getId());
			journalDetailSet.addAll(getAccountingEventJournalService().getAccountingEventJournalDetailList(searchForm));
			// reset search form for election lookup
			searchForm = new AccountingEventJournalDetailSearchForm();
		}

		searchForm.setSecurityEventId(eventPayout.getSecurityEvent().getId());
		// look up by election unless the payout is a default payout
		if (!eventPayout.isDefaultElection()) {
			if (originalEventPayout == null || originalEventPayout.getElectionNumber() == eventPayout.getElectionNumber()) {
				searchForm.setEventPayoutElectionNumber(eventPayout.getElectionNumber());
			}
			else {
				searchForm.setEventPayoutElectionNumbers(new Short[]{originalEventPayout.getElectionNumber(), eventPayout.getElectionNumber()});
			}
		}
		journalDetailSet.addAll(getAccountingEventJournalService().getAccountingEventJournalDetailList(searchForm));
		return journalDetailSet;
	}
}
