package com.clifton.accounting.eventjournal.forecast.observers;


import com.clifton.accounting.eventjournal.forecast.AccountingEventJournalForecast;
import com.clifton.accounting.eventjournal.forecast.AccountingEventJournalForecastService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>AccountingEventJournalForecastInvestmentSecurityEventObserver</code> observer handles
 * cases when security events are deleted (deletes forecast rows if they exist) and updates when security events are updated
 * and there is a forecast row for them, will re-process the forecast for the event.  Inserts are not touched...if they fall within forecast
 * processing batch job date range, they'll get picked up the next morning
 *
 * @author manderson
 */
@Component
public class AccountingEventJournalForecastInvestmentSecurityEventObserver extends SelfRegisteringDaoObserver<InvestmentSecurityEvent> {

	private AccountingEventJournalForecastService accountingEventJournalForecastService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE_OR_DELETE;
	}


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentSecurityEvent> dao, DaoEventTypes event, InvestmentSecurityEvent bean) {
		// Before deleting the event - check if any forecast rows exist and if so delete them first
		if (event.isDelete()) {
			List<AccountingEventJournalForecast> list = getAccountingEventJournalForecastService().getAccountingEventJournalForecastListForSecurityEvent(bean.getId());
			if (!CollectionUtils.isEmpty(list)) {
				getAccountingEventJournalForecastService().deleteAccountingEventJournalForecastList(list);
			}
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentSecurityEvent> dao, DaoEventTypes event, InvestmentSecurityEvent bean, Throwable e) {
		// After updating the event, if any forecast rows exist for the event - re-process forecasts for that event
		if (e == null && event.isUpdate()) {
			List<AccountingEventJournalForecast> list = getAccountingEventJournalForecastService().getAccountingEventJournalForecastListForSecurityEvent(bean.getId());
			if (!CollectionUtils.isEmpty(list)) {
				getAccountingEventJournalForecastService().processAccountingEventJournalForecastListForSecurityEvent(bean.getId(), false);
			}
		}
	}


	public AccountingEventJournalForecastService getAccountingEventJournalForecastService() {
		return this.accountingEventJournalForecastService;
	}


	public void setAccountingEventJournalForecastService(AccountingEventJournalForecastService accountingEventJournalForecastService) {
		this.accountingEventJournalForecastService = accountingEventJournalForecastService;
	}
}
