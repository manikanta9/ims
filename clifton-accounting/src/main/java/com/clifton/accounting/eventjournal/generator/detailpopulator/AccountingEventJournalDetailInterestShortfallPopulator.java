package com.clifton.accounting.eventjournal.generator.detailpopulator;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>AccountingEventJournalDetailInterestShortfallPopulator</code> class calculates transaction amounts for
 * <code>InvestmentSecurityEventType#INTEREST_SHORTFALL_PAYMENT</code> and <code>InvestmentSecurityEventType#INTEREST_SHORTFALL_REIMBURSEMENT_PAYMENT</code>.
 *
 * @author michaelm
 */
public class AccountingEventJournalDetailInterestShortfallPopulator implements AccountingEventJournalDetailPopulator {

	boolean reimbursement;

	private static final BigDecimal DEFAULT_DIVISOR = new BigDecimal("1000000"); // also defaulted to 1,000,000 in the InterestShortfallPaymentEventWindow


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		boolean accrualBasedOnNotional = command.getEventData().getSecurity().getInstrument().getHierarchy().isAccrualBasedOnNotional();
		command.getJournalDetail().setAffectedCost(accrualBasedOnNotional ? command.getOpeningCostBasis() : command.getOpeningQuantity());
		command.getJournalDetail().setTransactionAmount(calculatePaymentAmount(command.getJournalDetail(), command.getEventData()));
		if (isReimbursement()) {
			command.getJournalDetail().setTransactionAmount(command.getJournalDetail().getTransactionAmount().negate());
		}
		return false;
	}


	private BigDecimal calculatePaymentAmount(AccountingEventJournalDetail interestShortfall, EventData eventData) {
		// Interest Shortfall Payment: ROUND(Unadjusted Qty / 1000000 X Event Value, CCY Convention Precision)
		InvestmentNotionalCalculatorTypes notionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(eventData.getSecurity());
		if (eventData.getAdditionalEventValue() == null) {
			eventData.getEvent().setAdditionalEventValue(DEFAULT_DIVISOR);
		}
		return notionalCalculator.round(MathUtils.multiply(MathUtils.divide(interestShortfall.getAffectedCost(), eventData.getAdditionalEventValue()), eventData.getAfterEventValue()));
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isReimbursement() {
		return this.reimbursement;
	}


	public void setReimbursement(boolean reimbursement) {
		this.reimbursement = reimbursement;
	}
}
