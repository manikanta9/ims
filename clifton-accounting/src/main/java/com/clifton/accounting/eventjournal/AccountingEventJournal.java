package com.clifton.accounting.eventjournal;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The <code>AccountingEventJournal</code> class represents a sub-system journal that processes investment
 * security events.  Each position lot that is open on the event date for the event security is adjusted by
 * this journal based on event type.
 * <p/>
 * Note:
 * Some event journals support 2 bookings (create two separate accounting journals). If accrualReversalAccount is defined
 * for the journal type, then the first journal includes accrued entries and the second one reverses accrued entries
 * and replaces them with the real once: accrue Dividend Receivable and later reverse it and replace with Cash.
 *
 * @author vgomelsky
 */
public class AccountingEventJournal extends BaseEntity<Integer> implements BookableEntity {

	private AccountingEventJournalType journalType;
	private InvestmentSecurityEvent securityEvent;

	/**
	 * The date when this simple journal was moved into AccountingJournal. This field is set by the system
	 * after the event occurs and prevents journal from being modified after it was set.
	 */
	private Date bookingDate;

	/**
	 * If event journal type supports accrual (accrualReversalAccount is set), then there are 2 different accounting journals that need to be booked:
	 * first is booked on "Ex Date" and will contain accrual entries (bookingDate is set),
	 * and the second is created on "Event Date": reverse accrual entries and add real entries with accrualReversalAccount.
	 * <p/>
	 * For example, stock dividend payment results in "Dividend Receivable" and "Dividend Income" entries on "Ex Date" and reversal of "Dividend Receivable" and "Cash" entries on "Event Date".
	 * <p/>
	 * For journal types that don't use accrual (stock split, symbol change, etc.), there is only 1 accounting journal created:
	 * both bookingDate and accrualReversalBookingDate are set at the same time usually on "Event Date".
	 */
	private Date accrualReversalBookingDate;

	/**
	 * For event types that support accrual/reversal, in some instances it may still make sense
	 * to combine both event into one to be processed on EventDate.
	 */
	private boolean accrualReversalCombined;

	/**
	 * Can contain explanation for overrides to automatically generated information.
	 */
	private String description;

	private List<AccountingEventJournalDetail> detailList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (this.securityEvent != null) {
			return this.securityEvent.getLabel();
		}
		StringBuilder result = new StringBuilder();
		if (this.journalType != null) {
			result.append(this.journalType.getLabel());
			result.append(": ");
		}
		result.append(getId());
		return result.toString();
	}


	@Override
	public Set<IdentityObject> getEntityLockSet() {
		return CollectionUtils.createHashSet(getSecurityEvent().getSecurity(), getSecurityEvent().getAdditionalSecurity());
	}


	public boolean isBooked() {
		return getBookingDate() != null;
	}


	@Override
	public Date getBookingDate() {
		return this.bookingDate;
	}


	@Override
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public AccountingEventJournalType getJournalType() {
		return this.journalType;
	}


	public void setJournalType(AccountingEventJournalType journalType) {
		this.journalType = journalType;
	}


	public InvestmentSecurityEvent getSecurityEvent() {
		return this.securityEvent;
	}


	public void setSecurityEvent(InvestmentSecurityEvent securityEvent) {
		this.securityEvent = securityEvent;
	}


	public List<AccountingEventJournalDetail> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<AccountingEventJournalDetail> detailList) {
		this.detailList = detailList;
	}


	public void addDetail(AccountingEventJournalDetail detail) {
		if (this.detailList == null) {
			this.detailList = new ArrayList<>();
		}
		this.detailList.add(detail);
		detail.setJournal(this);
	}


	public Date getAccrualReversalBookingDate() {
		return this.accrualReversalBookingDate;
	}


	public void setAccrualReversalBookingDate(Date accrualReversalBookingDate) {
		this.accrualReversalBookingDate = accrualReversalBookingDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public boolean isAccrualReversalCombined() {
		return this.accrualReversalCombined;
	}


	public void setAccrualReversalCombined(boolean accrualReversalCombined) {
		this.accrualReversalCombined = accrualReversalCombined;
	}
}
