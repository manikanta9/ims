package com.clifton.accounting.eventjournal;

import com.clifton.core.beans.BeanUtils;

import java.util.List;


/**
 * The EventJournalUtils class provides helpful API for working with Event Journals: generating, booking, etc.
 *
 * @author vgomelsky
 */
public class EventJournalUtils {


	/**
	 * Returns a List of details that will result in a Cash or Currency payment (skip security election for Scrip Dividend, etc.).
	 */
	public static List<AccountingEventJournalDetail> getCurrencyPaymentDetailList(AccountingEventJournal eventJournal) {
		return BeanUtils.filter(eventJournal.getDetailList(), EventJournalUtils::isCurrencyPaymentDetail);
	}


	/**
	 * Returns a List of details that will result on Position lots (stock from Scrip Dividend, etc.).
	 */
	public static List<AccountingEventJournalDetail> getNonCurrencyPaymentDetailList(AccountingEventJournal eventJournal) {
		return BeanUtils.filter(eventJournal.getDetailList(), d -> !EventJournalUtils.isCurrencyPaymentDetail(d));
	}


	private static boolean isCurrencyPaymentDetail(AccountingEventJournalDetail detail) {
		if (detail.getEventPayout() != null) {
			return detail.getEventPayout().getPayoutType().isAdditionalSecurityCurrency();
		}
		return true;
	}
}
