package com.clifton.accounting.eventjournal.generator.positionretriever;


import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.List;


/**
 * The <code>AccountingEventJournalForNothingPositionRetriever</code> class returns no positions.  It can be used as a place holder
 * for cases when implementation specific to Event Journal Type is located in a different project.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalForNothingPositionRetriever implements AccountingEventJournalPositionRetriever {

	@Override
	@SuppressWarnings("unused")
	public List<AccountingPosition> getAffectedPositions(AccountingEventJournalType journalType, InvestmentSecurityEvent securityEvent, Integer clientInvestmentAccountId) {
		return null;
	}
}
