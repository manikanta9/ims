package com.clifton.accounting.eventjournal.generator.detailpopulator;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author vgomelsky
 */
public class PopulatorCommand {

	private final AccountingEventJournalDetail journalDetail;
	private final EventData eventData;
	private final AccountingPosition openPosition;
	private final AccountingEventJournalGeneratorTypes generatorType;

	/**
	 * If the detail is skipped, contains information as to why the detail is skipped (used for previews to indicate why journals may be missing)
	 */
	private String skipMessage;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PopulatorCommand(AccountingEventJournalDetail journalDetail, EventData eventData, AccountingPosition openPosition, AccountingEventJournalGeneratorTypes generatorType) {
		this.journalDetail = journalDetail;
		this.eventData = eventData;
		this.openPosition = openPosition;
		this.generatorType = generatorType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSecurityMaturityEvent() {
		return InvestmentSecurityEventType.SECURITY_MATURITY.equals(getJournalDetail().getJournal().getSecurityEvent().getType().getName());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalDetail getJournalDetail() {
		return this.journalDetail;
	}


	public EventData getEventData() {
		return this.eventData;
	}


	public AccountingPosition getOpenPosition() {
		return this.openPosition;
	}


	public AccountingEventJournalGeneratorTypes getGeneratorType() {
		return this.generatorType;
	}


	public String getSkipMessage() {
		return this.skipMessage;
	}


	public void setSkipMessage(String skipMessage) {
		this.skipMessage = skipMessage;
	}


	public BigDecimal getOpeningPrice() {
		return getOpenPosition().getOpeningTransaction().getPrice();
	}


	public BigDecimal getOpeningQuantity() {
		return getOpenPosition().getOpeningTransaction().getQuantity();
	}


	public BigDecimal getOpeningCostBasis() {
		return getOpenPosition().getOpeningTransaction().getPositionCostBasis();
	}


	public BigDecimal getRemainingQuantity() {
		return getOpenPosition().getRemainingQuantity();
	}


	public BigDecimal getRemainingCostBasis() {
		return getOpenPosition().getRemainingCostBasis();
	}


	public Date getTransactionDate() {
		return getOpenPosition().getTransactionDate();
	}
}
