package com.clifton.accounting.eventjournal.forecast.observers;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.forecast.AccountingEventJournalForecast;
import com.clifton.accounting.eventjournal.forecast.AccountingEventJournalForecastService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingEventJournalForecastAccountingEventJournalObserver</code> will re-process forecast records for
 * an event on inserts (only if there are existing journals) and deletes - if in the future? will process a forecast record for it
 * <p/>
 *
 * @author manderson
 */
@Component
public class AccountingEventJournalForecastAccountingEventJournalObserver extends SelfRegisteringDaoObserver<AccountingEventJournal> {

	private AccountingEventJournalForecastService accountingEventJournalForecastService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.INSERT_OR_DELETE;
	}


	@Override
	protected void afterTransactionMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<AccountingEventJournal> dao, DaoEventTypes event, AccountingEventJournal bean, Throwable e) {
		if (e == null) {
			List<AccountingEventJournalForecast> eventJournalForecastList =
					getAccountingEventJournalForecastService().getAccountingEventJournalForecastListForSecurityEvent(bean.getSecurityEvent().getId());
			boolean securityEventDayAfterOrEqualsCurrentDay =
					bean.getSecurityEvent() != null && DateUtils.compare(bean.getSecurityEvent().getEventDate(), new Date(), false) >= 0;

			if ((event.isInsert() && !CollectionUtils.isEmpty(eventJournalForecastList)) || (event.isDelete() && securityEventDayAfterOrEqualsCurrentDay)) {
				getAccountingEventJournalForecastService().processAccountingEventJournalForecastListForSecurityEvent(bean.getSecurityEvent().getId(), false);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Getter and Setter Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalForecastService getAccountingEventJournalForecastService() {
		return this.accountingEventJournalForecastService;
	}


	public void setAccountingEventJournalForecastService(AccountingEventJournalForecastService accountingEventJournalForecastService) {
		this.accountingEventJournalForecastService = accountingEventJournalForecastService;
	}
}
