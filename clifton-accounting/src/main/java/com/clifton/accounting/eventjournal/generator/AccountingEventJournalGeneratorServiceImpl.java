package com.clifton.accounting.eventjournal.generator;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.eventjournal.booking.AccountingEventJournalBookingProcessor;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.PopulatorCommand;
import com.clifton.accounting.eventjournal.generator.detailrounder.AccountingEventJournalDetailRounder;
import com.clifton.accounting.eventjournal.generator.positionretriever.AccountingEventJournalPositionRetriever;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.action.processor.InvestmentSecurityEventActionProcessorService;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventClientElection;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventClientElectionSearchForm;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.shared.HoldingAccount;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;


/**
 * The <code>AccountingEventJournalGeneratorServiceImpl</code> class provides basic implementation of event journal generation.
 *
 * @author vgomelsky
 */
@Service
public class AccountingEventJournalGeneratorServiceImpl implements AccountingEventJournalGeneratorService {

	/**
	 * The key is the name of AccountingEventJournalType that should be used to populate journal details (or InvestmentSecurityEventType if the first one is not set).
	 * Some event journals may have event specific logic for this.  If the key is not defined, default logic is used.
	 * <p>
	 * Also, you may specify investment type or hierarchy specific populator by adding the type/hierarchy after semicolon after even type.
	 * Example: "Security Maturity: Commodities / LME"
	 */
	private Map<String, AccountingEventJournalDetailPopulator> detailPopulatorMap;

	/**
	 * The key is the name of AccountingEventJournalType that should be used to populate journal details (or InvestmentSecurityEventType if the first one is not set).
	 * Some event journals may have event specific logic for this.  If the key is not defined, no rounding adjustment is done.
	 * <p>
	 * Also, you may specify investment type or hierarchy specific populator by adding the type/hierarchy after semicolon after even type.
	 * Example: "Security Maturity: Commodities / LME"
	 */
	private Map<String, AccountingEventJournalDetailRounder> detailRounderMap;

	/**
	 * The key is the name of AccountingEventJournalType that corresponding affected position applies to.
	 * Some event types may allow multiple event journal types and may have event logic that identifies what positions get affected by the event.
	 * Most events apply positions open on Ex Date.
	 * <p>
	 * "DEFAULT" key is used for default position retriever.
	 * <p>
	 * Also, you may specify investment type or hierarchy specific populator by adding the type/hierarchy after semicolon after even type.
	 * Example: "Security Maturity: Commodities / LME"
	 */
	private Map<String, AccountingEventJournalPositionRetriever> affectedPositionsRetrieverMap;

	public static final String ACCOUNTING_EVENT_JOURNAL_POSITION_RETRIEVER_DEFAULT_KEY = "DEFAULT";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private AccountingBookingService<AccountingEventJournal> accountingBookingService;
	private AccountingEventJournalService accountingEventJournalService;
	private AccountingTransactionService accountingTransactionService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;
	private InvestmentSecurityEventActionProcessorService investmentSecurityEventActionProcessorService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPosition> getAccountingPositionListForEventJournal(int securityEventId, Short eventJournalTypeId) {
		InvestmentSecurityEvent securityEvent = getInvestmentSecurityEventService().getInvestmentSecurityEvent(securityEventId);
		ValidationUtils.assertNotNull(securityEvent, "Cannot find Investment Security Event with id = " + securityEventId);

		List<AccountingEventJournalType> journalTypeList;
		if (eventJournalTypeId == null) {
			journalTypeList = getAccountingEventJournalTypeForSecurityEvent(securityEvent);
		}
		else {
			journalTypeList = CollectionUtils.createList(getAccountingEventJournalService().getAccountingEventJournalType(eventJournalTypeId));
		}

		List<AccountingPosition> result = null;
		for (AccountingEventJournalType journalType : CollectionUtils.getIterable(journalTypeList)) {
			AccountingEventJournalPositionRetriever positionRetriever = getAccountingEventJournalPositionRetriever(securityEvent, journalType.getName());
			List<AccountingPosition> affectedPositionList = positionRetriever.getAffectedPositions(journalType, securityEvent, null);
			if (CollectionUtils.isEmpty(result)) {
				result = affectedPositionList;
			}
			else if (!CollectionUtils.isEmpty(affectedPositionList)) {
				result.addAll(affectedPositionList);
				result = CollectionUtils.removeDuplicates(result);
			}
		}

		return result;
	}


	@Override
	public int generateAccountingEventJournalList(EventJournalGeneratorCommand command) {
		ValidationUtils.assertNotNull(command.getGeneratorType(), "Generator Type is Required");

		// 1. Get Event(s) to Process
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventListForConfig(command);

		int count = 0;

		// If Only One Event (i.e. eventId is not null) and an override amount is populated - use it
		BigDecimal overrideAmount = null;
		if (command.getEventId() != null && command.getAmountOverride() != null) {
			overrideAmount = command.getAmountOverride();
			ValidationUtils.assertFalse(command.isSeparateJournalPerHoldingAccount(), "Override Amount is not supported when generating Separate Journals per Holding Account");
		}

		// 2. process each event
		StringBuilder skipMessage = new StringBuilder();
		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {
			count += CollectionUtils.getSize(generateAccountingEventJournalImpl(EventJournalGeneratorCommandInternal.forCommand(command, event).withOverrideAmount(overrideAmount).withSkipMessage(skipMessage)));
		}
		return count;
	}


	@Override
	public List<AccountingEventJournalDetail> getAccountingEventJournalListForPreview(EventJournalGeneratorCommand command) {
		if (command.getGeneratorType() == null) {
			command.setGeneratorType(AccountingEventJournalGeneratorTypes.PREVIEW);
		}
		ValidationUtils.assertTrue(command.getGeneratorType().isGenerate() && !command.getGeneratorType().isSave(),
				"Accounting Event Journal List Retrieval for Preview is allowed only for Generator Types that Generate Events but don't save them (PREVIEW or FORECAST)");

		// Get Event(s) to Process
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventListForConfig(command);

		// If no events - set message and return null;
		if (CollectionUtils.isEmpty(eventList)) {
			command.setResultMessages("No events found.");
			return null;
		}

		// Otherwise, process each event - append results to detailList and build skip messages
		command.setResultMessages(null); // Start with cleared result message
		List<AccountingEventJournalDetail> detailList = new ArrayList<>();

		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {
			StringBuilder eventMessages = new StringBuilder(16);
			List<AccountingEventJournal> list = generateAccountingEventJournalImpl(EventJournalGeneratorCommandInternal.forCommand(command, event).withSkipMessage(eventMessages));
			for (AccountingEventJournal j : CollectionUtils.getIterable(list)) {
				for (AccountingEventJournalDetail d : CollectionUtils.getIterable(j.getDetailList())) {
					detailList.add(d);
				}
			}
			if (eventMessages.length() > 0) {
				command.addResultMessage("<br/>&nbsp;&nbsp;Event [" + event.getLabel() + "]: " + eventMessages.toString());
			}
		}
		return detailList;
	}


	private List<InvestmentSecurityEvent> getInvestmentSecurityEventListForConfig(EventJournalGeneratorCommand command) {
		// Specific Event
		if (command.getEventId() != null) {
			InvestmentSecurityEvent event = getInvestmentSecurityEventService().getInvestmentSecurityEvent(command.getEventId());
			ValidationUtils.assertNotNull(event, "Cannot find investment security event for id = " + command.getEventId());
			return CollectionUtils.createList(event);
		}
		// Otherwise Search For Events On Given Date
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setEventDate(DateUtils.clearTime(command.getEventDate()));
		if (command.isSkipJournalsWithExDateAfterTomorrow()) {
			Date tomorrow = DateUtils.addDays(DateUtils.clearTime(new Date()), 1);
			searchForm.addSearchRestriction(new SearchRestriction("exDate", ComparisonConditions.LESS_THAN_OR_EQUALS, tomorrow));
		}
		searchForm.setTypeId(command.getEventTypeId());
		if (command.getSecurityId() != null && command.getSecurityId() > 0) {
			searchForm.setSecurityId(command.getSecurityId());
		}
		searchForm.setInvestmentGroupId(command.getInvestmentGroupId());
		// Order by Security/Event Type (Note: Type sorting is done on the event order which is what we need)
		searchForm.setOrderBy("securityId#typeId");
		return getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
	}


	@Override
	public List<AccountingEventJournal> generateAccountingEventJournal(EventJournalGeneratorCommandInternal command) {
		return generateAccountingEventJournalImpl(command);
	}


	@Transactional
	protected List<AccountingEventJournal> generateAccountingEventJournalImpl(EventJournalGeneratorCommandInternal command) {
		AccountingEventJournalGeneratorTypes generationType = command.getGeneratorType();
		ValidationUtils.assertNotNull(generationType, "Generator Type is Required");

		InvestmentSecurityEvent securityEvent = command.getSecurityEvent();
		ValidationUtils.assertTrue(securityEvent.getStatus().isEventJournalAllowed(), "Event Journal Generation is not allowed for current event status: " + securityEvent);
		securityEvent.validateRequiredFields();

		List<AccountingEventJournal> result = new ArrayList<>();
		List<AccountingEventJournalType> journalTypeList = getAccountingEventJournalTypeForSecurityEvent(securityEvent);
		for (AccountingEventJournalType journalType : CollectionUtils.getIterable(journalTypeList)) {
			// 1. get open positions for event security
			AccountingEventJournalPositionRetriever positionRetriever = getAccountingEventJournalPositionRetriever(securityEvent, journalType.getName());
			List<AccountingPosition> affectedPositionList = positionRetriever.getAffectedPositions(journalType, securityEvent, command.getClientAccountId());

			// 2. populate event details while filtering out positions that have been processed already for this event instance
			Map<Long, AccountingEventJournal> processedPositionIds = getProcessedPositionIds(securityEvent.getId(), journalType.getName());

			if (CollectionUtils.isEmpty(affectedPositionList)) {
				StringUtils.appendString(command.getSkipMessage(), journalType.getName() + ": No positions available in Active holding accounts to generate event journals for. ");
			}
			else if (securityEvent.getType().isActionAllowed()) {
				if (getInvestmentSecurityEventActionProcessorService().isInvestmentSecurityEventActionPendingProcessing(securityEvent.getId(), true)) {
					// need to process BEFORE actions first
					affectedPositionList = Collections.emptyList();
					StringUtils.appendString(command.getSkipMessage(), journalType.getName() + ": Before Event Journal security event Actions must be processed first. ");
				}
			}

			AccountingEventJournal journal = new AccountingEventJournal();
			journal.setJournalType(journalType);
			journal.setSecurityEvent(securityEvent);
			journal.setDetailList(new ArrayList<>());

			// For cases where we re-process details that have already been generated, store the detail records we need to save and save all at once
			List<AccountingEventJournalDetail> updateJournalDetailList = new ArrayList<>();

			boolean processedFound = false;
			for (AccountingPosition openPosition : CollectionUtils.getIterable(affectedPositionList)) {
				// Do not process events for swap openings on the same day as position open: likely comes from up-sizing a TRS position.
				// We know for sure that this should not apply to Cleared CDS. Will need to research other securities. Limit to TRS for now.
				if (DateUtils.getDaysDifference(securityEvent.getExDate(), openPosition.getOriginalTransactionDate()) == 1) {
					if (InvestmentUtils.isSecurityOfType(openPosition.getInvestmentSecurity(), InvestmentType.SWAPS) && InvestmentUtils.isSecurityOfTypeSubType(openPosition.getInvestmentSecurity(), InvestmentTypeSubType.TOTAL_RETURN_SWAPS)) {
						continue;
					}
				}

				if (processedPositionIds.containsKey(openPosition.getId())) {
					AccountingEventJournal existingJournal = processedPositionIds.get(openPosition.getId());
					// only mark the journal as processed if reversal is booked or if accrual and reversal were combined
					processedFound = existingJournal.getAccrualReversalBookingDate() != null;
					// do not generate duplicate journal for the same event
					if (generationType.isPost() && (!command.isAutoOnly() || journalType.isAutoPost())) {
						// may need to post existing journal that hasn't been posted yet
						bookAndPost(existingJournal, command);
					}
					// if need to re-process and update...(or preview and want to show calculations vs. actual)
					else if ((generationType.isReprocessExisting() && existingJournal.getBookingDate() == null) || generationType.isPreview()) {
						List<AccountingEventJournalDetail> existingPositionEventDetails = BeanUtils.filter(existingJournal.getDetailList(), detail -> openPosition.getId().equals(detail.getAccountingTransaction().getId()));
						if (!CollectionUtils.isEmpty(existingPositionEventDetails)) {
							// Regenerate Journal Detail for Existing; Populate a new skip message to pass so that if there is an error it is always populated for the user
							StringBuilder thisSkipMessage = new StringBuilder(16);
							List<AccountingEventJournalDetail> newJournalDetailList = generateAccountingEventJournalDetailListForPosition(existingJournal, openPosition, thisSkipMessage, generationType);
							ValidationUtils.assertNotEmpty(newJournalDetailList, () -> String.format("Unable to update existing un-booked journal detail with re-processed changes:  [%s]. Please fix this error, or delete existing journal.", thisSkipMessage));

							BiConsumer<AccountingEventJournalDetail, AccountingEventJournalDetail> existingDetailUpdater = (existingJournalDetail, newJournalDetail) -> {
								if (generationType.isPreview()) {
									boolean override = !MathUtils.isEqual(existingJournalDetail.getTransactionAmount(), newJournalDetail.getTransactionAmount());
									existingJournalDetail.setSkipMessage("Journal Detail Already Generated" + (existingJournal.getBookingDate() != null ? " and Booked" : ""));
									if (override) {
										existingJournalDetail.setSkipMessage(existingJournalDetail.getSkipMessage() +
												String.format(" with overridden amount %s. Calculated Amount is %s",
														CoreMathUtils.formatNumberMoney(existingJournalDetail.getTransactionAmount()),
														CoreMathUtils.formatNumberMoney(newJournalDetail.getTransactionAmount())));
										existingJournalDetail.setPreviewAmount(newJournalDetail.getTransactionAmount());
									}
									// Note: Don't use add method on the Journal, because we don't want to modify the row for the preview
									if (journal.getDetailList() == null) {
										journal.setDetailList(new ArrayList<>());
									}
									journal.getDetailList().add(existingJournalDetail);
								}
								else {
									// Use the same ID so it updates and add it to the list to update
									newJournalDetail.setId(existingJournalDetail.getId());
									updateJournalDetailList.add(newJournalDetail);
								}
							};
							for (AccountingEventJournalDetail jd : existingPositionEventDetails) {
								if (jd.getEventPayout() == null) {
									// existing detail was based on the event
									existingDetailUpdater.accept(jd, newJournalDetailList.get(0));
									if (CollectionUtils.getSize(newJournalDetailList) > 1) {
										// was a single payout, now multiple; add new details
										for (int i = 1; i < newJournalDetailList.size(); i++) {
											AccountingEventJournalDetail newJournalDetail = newJournalDetailList.get(i);
											if (generationType.isPreview()) {
												journal.getDetailList().add(newJournalDetail);
											}
											else {
												updateJournalDetailList.add(newJournalDetail);
											}
										}
									}
								}
								else {
									// existing detail was based on a payout, update the detail
									AccountingEventJournalDetail newJournalDetail = newJournalDetailList.stream()
											.filter(detail -> jd.getEventPayout().equals(detail.getEventPayout()))
											.findFirst()
											.orElseThrow(() -> new ValidationException(String.format("Unable to update existing un-booked journal detail with re-processed changes because a payout is no longer applicable. Please fix this error, or delete existing journal. The problematic payout is: %s", jd.getEventPayout())));
									existingDetailUpdater.accept(jd, newJournalDetail);
								}
							}
						}
					}
					continue;
				}

				if (!command.isAutoOnly() || journalType.isAutoGenerate()) {
					// Only need to do this when saving journals - preview do not require this
					if (generationType.isSave()) {
						// make sure that event journals have already been generated for all events that must come before this one
						AccountingEventJournalType earlierType = getEarlierJournalType(securityEvent, journalType, openPosition.getId());
						if (earlierType != null) {
							throw new ValidationException(journalType.getName() + " cannot be booked before " + earlierType.getName() + " for " + securityEvent + " position " + openPosition);
						}
					}
					appendAccountingEventJournalDetailForPosition(journal, openPosition, command.getSkipMessage(), generationType);
				}
			}

			// If Preview - Add Back in what is no longer valid with override amount of 0
			if (generationType.isPreview() && !CollectionUtils.isEmpty(processedPositionIds)) {
				for (Map.Entry<Long, AccountingEventJournal> processedEntry : processedPositionIds.entrySet()) {
					Long processedId = processedEntry.getKey();
					boolean found = false;
					for (AccountingPosition position : affectedPositionList) {
						if (position.getId().equals(processedId)) {
							found = true;
							break;
						}
					}
					if (!found) {
						AccountingEventJournal existingJournal = processedEntry.getValue();
						for (AccountingEventJournalDetail detail : existingJournal.getDetailList()) {
							if (detail.getAccountingTransaction().getId().equals(processedId)) {
								detail.setSkipMessage("Journal Detail Already Generated"
										+ (existingJournal.getBookingDate() != null ? " and Booked" : "")
										+ " with amount " + CoreMathUtils.formatNumberMoney(detail.getTransactionAmount()) + ". Event Journal processing no longer includes this position and transaction amount is now considered to be 0.");
								detail.setPreviewAmount(BigDecimal.ZERO);

								// Note: Don't use add method on the Journal, because we don't want to modify the row for the preview
								if (journal.getDetailList() == null) {
									journal.setDetailList(new ArrayList<>());
								}
								journal.getDetailList().add(detail);
								break;
							}
						}
					}
				}
			}

			adjustForLotRounding(journal, affectedPositionList, generationType);

			// 3a. If Reprocess Existing - update existing records only
			if (generationType.isReprocessExisting()) {
				if (!CollectionUtils.isEmpty(updateJournalDetailList)) {
					getAccountingEventJournalService().saveAccountingEventJournalDetailList(updateJournalDetailList);
				}
			}
			else {
				// 3b. save the journal only if it has details
				if (!CollectionUtils.isEmpty(journal.getDetailList())) {
					// If must preview prior to generate - throw exception (called here so that any existing journals will still be posted if they've already been generated)
					if (generationType.isSave() && !journalType.isAutoGenerate() && !generationType.isPreviewed()) {
						throw new ValidationException("You must preview generated event journals for type [" + journalType.getName()
								+ "].  Auto-Generation is allowed for this type only from the preview window."
								+ (processedFound && (generationType.isPost() && (!command.isAutoOnly() || journalType.isAutoPost())) ? " All previously existing un-booked journals have been posted." : ""));
					}

					if (generationType.isSave()) {
						saveAccountingEventJournals(journal, result, command);
					}
					else {
						result.add(journal);
					}
				}
				else if (processedFound) {
					StringUtils.appendString(command.getSkipMessage(), journalType.getName() + ": Event Journal Details have already been generated." + (generationType.isPost() && (!command.isAutoOnly() || journalType.isAutoPost()) ? " All previously existing un-booked journals have been posted." : ""));
				}
			}

			// 4. book and post processed journals for the same event if the time is right
			if (generationType.isPost() && (!command.isAutoOnly() || journalType.isAutoPost())) {
				List<AccountingEventJournal> processedJournals = CollectionUtils.removeDuplicates(processedPositionIds.values());
				for (AccountingEventJournal eventJournal : CollectionUtils.getIterable(processedJournals)) {
					if (bookAndPost(eventJournal, command)) {
						result.add(eventJournal);
					}
				}
			}
		}

		command.updateMessages();

		return result;
	}


	private void adjustForLotRounding(AccountingEventJournal journal, List<AccountingPosition> affectedPositionList, AccountingEventJournalGeneratorTypes generationType) {
		if (!CollectionUtils.isEmpty(affectedPositionList)) {
			final Map<Long, AccountingPosition> affectedPositionMap = BeanUtils.getBeanMap(affectedPositionList, AccountingPosition::getId);
			// some event types (Scrip/DRIP) use different Rounder for different details: group and process by rounder
			String eventJournalTypeName = journal.getJournalType().getName();

			// use LinkedHashSet for insert order dependency
			Map<EventData, Map<AccountingEventJournalDetailRounder, List<AccountingEventJournalDetail>>> payoutToDetailListMap = new LinkedHashMap<>();
			for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(journal.getDetailList())) {
				EventData eventData = EventData.forEventPayoutOrEvent(detail.getEventPayout(), journal.getSecurityEvent());
				Map<AccountingEventJournalDetailRounder, List<AccountingEventJournalDetail>> rounderToDetailListMap = payoutToDetailListMap.computeIfAbsent(eventData, k -> new HashMap<>());
				AccountingEventJournalDetailRounder rounder = getJournalDetailRounder(eventData, eventJournalTypeName);
				rounderToDetailListMap.computeIfAbsent(rounder, r -> new ArrayList<>()).add(detail);
			}
			payoutToDetailListMap.forEach((eventData, rounderToDetailListMap) -> rounderToDetailListMap.forEach((detailRounder, detailList) -> {
				// skip groups when no rounding is defined or only one element is present
				if (detailRounder != null) {
					Map<Integer, List<AccountingEventJournalDetail>> detailMap = BeanUtils.getBeansMap(detailList, d -> d.getAccountingTransaction().getHoldingInvestmentAccount().getId());
					detailMap.forEach((holdingAccountId, groupedDetailList) -> {
						List<AccountingPosition> groupedPositionList = groupedDetailList.stream()
								.map(d -> affectedPositionMap.get(d.getAccountingTransaction().getId()))
								.filter(Objects::nonNull)
								.collect(Collectors.toList());

						if (!CollectionUtils.isEmpty(groupedPositionList)) {
							// aggregate all lots for the holding account into a single "lot"
							AccountingTransaction aggregateTransaction = BeanUtils.cloneBean(groupedDetailList.get(0).getAccountingTransaction(), false, true);
							aggregateTransaction.setQuantity(CoreMathUtils.sumProperty(groupedDetailList, d -> d.getAccountingTransaction().getQuantity()));
							aggregateTransaction.setLocalDebitCredit(CoreMathUtils.sumProperty(groupedDetailList, d -> d.getAccountingTransaction().getLocalDebitCredit()));
							aggregateTransaction.setBaseDebitCredit(CoreMathUtils.sumProperty(groupedDetailList, d -> d.getAccountingTransaction().getBaseDebitCredit()));
							aggregateTransaction.setPositionCostBasis(CoreMathUtils.sumProperty(groupedDetailList, d -> d.getAccountingTransaction().getPositionCostBasis()));
							// Protect Against Divide By Zero
							if (MathUtils.isNullOrZero(aggregateTransaction.getLocalDebitCredit())) {
								HoldingAccount holdingAccount = aggregateTransaction.getHoldingInvestmentAccount().toHoldingAccount();
								aggregateTransaction.setExchangeRateToBase(getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(aggregateTransaction.fxSourceCompany(), !aggregateTransaction.fxSourceCompanyOverridden(), InvestmentUtils.getSecurityCurrencyDenominationSymbol(aggregateTransaction), holdingAccount.getBaseCurrencySymbol(), aggregateTransaction.getTransactionDate())));
							}
							else {
								aggregateTransaction.setExchangeRateToBase(MathUtils.divide(aggregateTransaction.getBaseDebitCredit(), aggregateTransaction.getLocalDebitCredit(), DataTypes.EXCHANGE_RATE.getPrecision()));
							}

							// skip 0 positions
							if (!(MathUtils.isNullOrZero(aggregateTransaction.getQuantity()) && MathUtils.isNullOrZero(aggregateTransaction.getPositionCostBasis()))) {
								aggregateTransaction.setPrice(InvestmentCalculatorUtils.getNotionalCalculator(aggregateTransaction.getInvestmentSecurity()).calculatePriceFromNotional(aggregateTransaction.getQuantity(), aggregateTransaction.getPositionCostBasis(), aggregateTransaction.getInvestmentSecurity().getPriceMultiplier()));

								AccountingPosition aggregatePosition = AccountingPosition.forOpeningTransaction(aggregateTransaction);
								aggregatePosition.setRemainingQuantity(CoreMathUtils.sumProperty(groupedPositionList, AccountingPosition::getRemainingQuantity));
								aggregatePosition.setRemainingBaseDebitCredit(CoreMathUtils.sumProperty(groupedPositionList, AccountingPosition::getRemainingBaseDebitCredit));
								aggregatePosition.setRemainingCostBasis(CoreMathUtils.sumProperty(groupedPositionList, AccountingPosition::getRemainingCostBasis));

								// generate single detail for the aggregated "lot" by payout
								AccountingEventJournalDetail aggregateDetail = generateAccountingEventJournalDetailForPositionAndPayout(journal, aggregatePosition, null, generationType, eventData);

								// the aggregate quantity can be zero and therefore not generate a journal record.
								if (aggregateDetail != null) {
									// use journal type specific populator to adjust for rounding inconsistencies
									detailRounder.adjustForLotRounding(aggregateDetail, groupedDetailList);
								}
							}
						}
					});
				}
			}));
		}
	}


	private void saveAccountingEventJournals(AccountingEventJournal journal, List<AccountingEventJournal> savedList, EventJournalGeneratorCommandInternal command) {
		List<AccountingEventJournal> newJournalList = new ArrayList<>();
		if (command.isSeparateJournalPerHoldingAccount()) {
			Map<Integer, List<AccountingEventJournalDetail>> detailMap = BeanUtils.getBeansMap(journal.getDetailList(), detail -> detail.getAccountingTransaction().getHoldingInvestmentAccount().getId());
			if (detailMap.size() > 1) {
				for (List<AccountingEventJournalDetail> detailList : detailMap.values()) {
					AccountingEventJournal newJournal = new AccountingEventJournal();
					newJournal.setJournalType(journal.getJournalType());
					newJournal.setSecurityEvent(journal.getSecurityEvent());
					newJournal.setDescription(detailList.get(0).getAccountingTransaction().getHoldingInvestmentAccount().getLabel());
					detailList.forEach(detail -> detail.setJournal(newJournal));
					newJournal.setDetailList(detailList);
					newJournalList.add(newJournal);
				}
			}
			else {
				journal.setDescription(journal.getDetailList().get(0).getAccountingTransaction().getHoldingInvestmentAccount().getLabel());
				newJournalList.add(journal);
			}
		}
		else {
			newJournalList.add(journal);
		}

		for (AccountingEventJournal newJournal : newJournalList) {
			getAccountingEventJournalService().saveAccountingEventJournal(newJournal);
			// If an overrideAmount - Update transaction amounts proportionally based on calculated amount total vs. override total
			if (command.getOverrideAmount() != null) {
				recalculateTransactionAmounts(newJournal, command.getOverrideAmount());
			}
			if (command.getGeneratorType().isPost() && (!command.isAutoOnly() || journal.getJournalType().isAutoPost())) {
				bookAndPost(newJournal, command);
			}
			savedList.add(newJournal);
		}
	}


	private List<AccountingEventJournalType> getAccountingEventJournalTypeForSecurityEvent(InvestmentSecurityEvent securityEvent) {
		List<AccountingEventJournalType> result = new ArrayList<>();

		// apply scope filter first
		List<AccountingEventJournalType> journalTypeList = getAccountingEventJournalService().getAccountingEventJournalTypeListByEventType(securityEvent.getType().getId());
		for (AccountingEventJournalType journalType : CollectionUtils.getIterable(journalTypeList)) {
			if (journalType.getEventJournalTypeScope().isSecurityInScope(securityEvent.getSecurity())) {
				result.add(journalType);
			}
		}

		// some security events may support more than one journal types: Factor Change, Factor Change - Assumed Buy, Factor Change - Assumed Sell
		if (result.size() > 1) {
			result.sort(Comparator.comparing(AccountingEventJournalType::getBookingOrder));
		}

		return result;
	}


	private void recalculateTransactionAmounts(AccountingEventJournal journal, BigDecimal overrideAmount) {
		if (!CollectionUtils.isEmpty(journal.getDetailList())) {
			if (CollectionUtils.getSize(journal.getDetailList()) == 1) {
				journal.getDetailList().get(0).setTransactionAmount(overrideAmount);
			}
			else {
				BigDecimal calculatedTotal = CoreMathUtils.sumProperty(journal.getDetailList(), AccountingEventJournalDetail::getTransactionAmount);

				for (AccountingEventJournalDetail detail : journal.getDetailList()) {
					BigDecimal percentage = CoreMathUtils.getPercentValue(detail.getTransactionAmount(), calculatedTotal);
					detail.setTransactionAmount(InvestmentCalculatorUtils.roundLocalAmount(MathUtils.getPercentageOf(percentage, overrideAmount), detail.getAccountingTransaction().getInvestmentSecurity()));
				}
				CoreMathUtils.applySumPropertyDifference(journal.getDetailList(), AccountingEventJournalDetail::getTransactionAmount, AccountingEventJournalDetail::setTransactionAmount, overrideAmount, true);
			}
			getAccountingEventJournalService().saveAccountingEventJournal(journal);
		}
	}


	private void appendAccountingEventJournalDetailForPosition(AccountingEventJournal journal, AccountingPosition openPosition, StringBuilder skipMessage, AccountingEventJournalGeneratorTypes generationType) {
		List<AccountingEventJournalDetail> detailList = generateAccountingEventJournalDetailListForPosition(journal, openPosition, skipMessage, generationType);
		for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(detailList)) {
			// skip 0 amount details: for example "0.49012701 to 0.482267832 Factor Change for 62889CAA7 on 04/06/2016"; unless Transaction Amount field is used for Price
			if (detail != null && (!MathUtils.isEqual(detail.getTransactionAmount(), 0) || StringUtils.contains(journal.getJournalType().getTransactionAmountLabel(), "Price"))) {
				journal.addDetail(detail);
			}
		}
	}


	private List<AccountingEventJournalDetail> generateAccountingEventJournalDetailListForPosition(AccountingEventJournal journal, AccountingPosition openPosition, StringBuilder skipMessage, AccountingEventJournalGeneratorTypes generatorType) {
		InvestmentSecurityEvent event = journal.getSecurityEvent();
		InvestmentSecurityEventType eventType = event.getType();

		if (eventType.isSinglePayoutOnly()) {
			EventData eventData = EventData.forEvent(event);
			AccountingEventJournalDetail eventJournalDetail = generateAccountingEventJournalDetailForPositionAndPayout(journal, openPosition, skipMessage, generatorType, eventData);
			return eventJournalDetail == null ? Collections.emptyList() : CollectionUtils.createList(eventJournalDetail);
		}

		List<InvestmentSecurityEventPayout> payoutList = getEventPayoutList(event, openPosition);
		if (CollectionUtils.isEmpty(payoutList)) {
			EventData eventData = EventData.forEvent(event);
			AccountingEventJournalDetail eventJournalDetail = generateAccountingEventJournalDetailForPositionAndPayout(journal, openPosition, skipMessage, generatorType, eventData);
			return eventJournalDetail == null ? Collections.emptyList() : CollectionUtils.createList(eventJournalDetail);
		}

		List<EventData> payoutEventDataList = new ArrayList<>();
		Map<Short, List<InvestmentSecurityEventPayout>> electionToPayoutList = BeanUtils.getBeansMap(payoutList, InvestmentSecurityEventPayout::getElectionNumber);
		for (List<InvestmentSecurityEventPayout> electionPayoutList : electionToPayoutList.values()) {
			electionPayoutList.forEach(payout -> {
				EventData eventData = EventData.forEventPayout(payout);
				eventData.setElectionPayoutList(electionPayoutList);
				payoutEventDataList.add(eventData);
			});
		}
		return payoutEventDataList.stream()
				.map(eventData -> generateAccountingEventJournalDetailForPositionAndPayout(journal, openPosition, skipMessage, generatorType, eventData))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}


	private AccountingEventJournalDetail generateAccountingEventJournalDetailForPositionAndPayout(AccountingEventJournal journal, AccountingPosition openPosition, StringBuilder skipMessage, AccountingEventJournalGeneratorTypes generatorType, EventData eventData) {
		AccountingEventJournalType journalType = journal.getJournalType();
		InvestmentSecurityEvent event = journal.getSecurityEvent();
		InvestmentSecurityEventType eventType = event.getType();

		AccountingEventJournalDetail detail = new AccountingEventJournalDetail();

		if (eventData.isPayoutDefined() && eventData.getPayout().isDeleted()) {
			String deletedMessage = String.format("Payout has been marked as deleted: %s", eventData.getPayout());
			if (skipMessage != null) {
				skipMessage.append(deletedMessage);
			}
			if (!generatorType.isGenerateInvalid()) {
				return null;
			}
			else {
				detail.setSkipMessage(deletedMessage);
			}
		}

		detail.setJournal(journal);
		detail.setEventPayout(eventData.getPayout());
		detail.setAccountingTransaction(getAccountingTransactionService().getAccountingTransaction(openPosition.getId()));
		if (journalType.isAffectedQuantitySupported()) {
			detail.setAffectedQuantity(openPosition.getRemainingQuantity());
		}
		if (journalType.isAffectedCostSupported()) {
			detail.setAffectedCost(openPosition.getRemainingCostBasis());
		}
		if (journalType.isTransactionPriceSupported()) {
			detail.setTransactionPrice(openPosition.getPrice());
		}
		if (journalType.isTransactionAmountSupported()) {
			if (eventType.isNewPositionCreated()) {
				// set amount to cost basis when a new position is created (stock split needs to know what's being closed
				detail.setTransactionAmount(openPosition.getRemainingCostBasis());
			}
			else {
				InvestmentNotionalCalculatorTypes notionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(eventData.getSecurity());
				detail.setTransactionAmount(notionalCalculator.round(eventData.getAfterEventValue().multiply(openPosition.getRemainingQuantity())));
			}
		}

		// set FX rate to base
		InvestmentSecurity baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(detail.getAccountingTransaction());
		InvestmentSecurity localCurrency = eventData.getSecurityCurrencyDenomination();
		InvestmentSecurity settlementCurrency = eventData.getSettlementCurrency();
		// for cases where 2 FX may be needed (3 currencies), use the most important one: the one that is used to calculate the payment (based in Event Type's IsEventValueInEventCurrencyUnits field):
		// Settlement to Base (Stock Dividends: amount is in Settlement Currency) or Local to Settlement (Swap Resets: amount is in Local Currency)
		InvestmentSecurity fromCurrency = settlementCurrency;
		InvestmentSecurity toCurrency = baseCurrency;
		if (!eventData.isEventValueInEventCurrencyUnits()) {
			// foreign Bond maturity is always Local to Base and Forwards use their own unique logic implemented in the populator that handles 3 currencies
			if (!InvestmentSecurityEventType.SECURITY_MATURITY.equals(eventType.getName())) {
				fromCurrency = localCurrency;
				toCurrency = settlementCurrency;
			}
			if (journalType.isAdditionalAmount3FxToBase()) {
				// need to save two FX rates for cases with 3 currencies: Local, Settlement, Base
				BigDecimal fxLocalToBase = getExchangeRate(localCurrency, baseCurrency, detail, generatorType);
				detail.setAdditionalAmount3(fxLocalToBase);
			}
		}

		ValidationUtils.assertNotNull(fromCurrency, () -> "Payout Currency is not defined for " + eventData.getLabel());
		if (fromCurrency.equals(toCurrency)) {
			detail.setExchangeRateToBase(BigDecimal.ONE);
		}
		else {
			BigDecimal fxRate = getExchangeRate(fromCurrency, toCurrency, detail, generatorType);
			detail.setExchangeRateToBase(fxRate);
		}

		// populate event/security specific information
		boolean skip = false;
		AccountingEventJournalDetailPopulator populator = getJournalDetailPopulator(eventData, journalType.getName());
		PopulatorCommand command = new PopulatorCommand(detail, eventData, openPosition, generatorType);
		if (populator != null) {
			skip = populator.populate(command);
			if (skipMessage != null && command.getSkipMessage() != null) {
				skipMessage.append(command.getSkipMessage());
			}
		}

		if (skip) {
			if (generatorType.isGenerateInvalid()) {
				detail.setSkipMessage(command.getSkipMessage());
			}
			else {
				return null;
			}
		}
		return detail;
	}


	private List<InvestmentSecurityEventPayout> getEventPayoutList(InvestmentSecurityEvent event, AccountingPosition openPosition) {
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setSecurityEventId(event.getId());
		List<InvestmentSecurityEventPayout> payoutList = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventPayoutList(payoutSearchForm);

		// if no payout ever existed: use the event by returning null
		if (CollectionUtils.isEmpty(payoutList)) {
			return null;
		}

		// check elections for effected position's account
		InvestmentSecurityEventClientElectionSearchForm electionSearchForm = new InvestmentSecurityEventClientElectionSearchForm();
		electionSearchForm.setSecurityEventId(event.getId());
		electionSearchForm.setClientInvestmentAccountId(openPosition.getClientInvestmentAccount().getId());
		List<InvestmentSecurityEventClientElection> electionList = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventClientElectionList(electionSearchForm);
		int elections = CollectionUtils.getSize(electionList);
		ValidationUtils.assertFalse(elections > 1, () -> "Cannot have multiple elections for the same Client Account for Security Event " + event);

		// filter to non-deleted payouts
		List<InvestmentSecurityEventPayout> nonDeletedPayoutList = BeanUtils.filter(payoutList, payout -> !payout.isDeleted());
		ValidationUtils.assertNotEmpty(nonDeletedPayoutList, () -> "Event Payouts have all been deleted for Security Event " + event);
		if (elections == 1) {
			InvestmentSecurityEventClientElection election = electionList.get(0);
			List<InvestmentSecurityEventPayout> electionPayoutList = BeanUtils.filter(nonDeletedPayoutList, payout -> payout.getElectionNumber() == election.getElectionNumber());
			ValidationUtils.assertNotEmpty(electionPayoutList, () -> "Cannot find Payout matching Security Event Client Election " + election);
			return electionPayoutList;
		}

		// no election specified for the client account: try to use default election
		List<InvestmentSecurityEventPayout> defaultPayoutList = BeanUtils.filter(nonDeletedPayoutList, InvestmentSecurityEventPayout::isDefaultElection);
		ValidationUtils.assertNotEmpty(defaultPayoutList, () -> "Multiple Payouts available for Security Event " + event + " but no Client Specific or Default Election specified.");
		return defaultPayoutList;
	}


	private BigDecimal getExchangeRate(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, AccountingEventJournalDetail detail, AccountingEventJournalGeneratorTypes generatorType) {
		// it's important to use a predefined exchange rate source. It should be specified as swap field.
		ExchangeRateDateSelectors dateSelector = detail.getJournal().getJournalType().getExchangeRateDateSelector();
		InvestmentSecurityEvent event = detail.getJournal().getSecurityEvent();
		boolean flexibleLookup = (dateSelector == null) || dateSelector.isFlexibleLookupAllowed(event);
		Date fxDate = (dateSelector == null) ? event.getAccrualEndDate() : dateSelector.getExchangeRateDate(event, getCalendarBusinessDayService());
		// use flexible lookup in generation of invalid data is allowed: FORECAST
		AccountingTransaction transaction = detail.getAccountingTransaction();
		return getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(transaction.fxSourceCompany(), !transaction.fxSourceCompanyOverridden(), fromCurrency.getSymbol(), toCurrency.getSymbol(), fxDate)
				.flexibleLookup(flexibleLookup || generatorType.isGenerateInvalid()).fallbackToDefaultDataSource(flexibleLookup || generatorType.isGenerateInvalid()));
	}


	/**
	 * Returns the most specific AccountingEventJournalPositionRetriever for the specified event.
	 */
	private AccountingEventJournalPositionRetriever getAccountingEventJournalPositionRetriever(InvestmentSecurityEvent securityEvent, String eventJournalTypeName) {
		String key = InvestmentUtils.getSecurityOverrideKey(eventJournalTypeName, securityEvent.getSecurity(), getAffectedPositionsRetrieverMap().keySet());
		if (key == null) {
			key = ACCOUNTING_EVENT_JOURNAL_POSITION_RETRIEVER_DEFAULT_KEY;
		}
		AccountingEventJournalPositionRetriever positionRetriever = getAffectedPositionsRetrieverMap().get(key);
		AssertUtils.assertNotNull(positionRetriever, "Cannot find specific or DEFAULT position retriever for event journal type: " + eventJournalTypeName);
		return positionRetriever;
	}


	/**
	 * Returns the most specific AccountingEventJournalDetailPopulator for the specified event.
	 * If nothing is returned, then no additional population is necessary.
	 */
	private AccountingEventJournalDetailPopulator getJournalDetailPopulator(EventData eventData, String eventJournalTypeName) {
		return getJournalDetailHandler(eventData, eventJournalTypeName, getDetailPopulatorMap());
	}


	/**
	 * Returns the most specific AccountingEventJournalDetailRounder for the specified event.
	 * If nothing is returned, then no additional population is necessary.
	 */
	private AccountingEventJournalDetailRounder getJournalDetailRounder(EventData eventData, String eventJournalTypeName) {
		return getJournalDetailHandler(eventData, eventJournalTypeName, getDetailRounderMap());
	}


	private <T> T getJournalDetailHandler(EventData eventData, String eventJournalTypeName, Map<String, T> handlerMap) {
		if (handlerMap != null) {
			String key;
			InvestmentSecurityEventPayout payout = eventData.getPayout();
			if (payout == null) {
				key = InvestmentUtils.getSecurityOverrideKey(eventJournalTypeName, eventData.getSecurity(), handlerMap.keySet());
				if (key == null) {
					key = InvestmentUtils.getSecurityOverrideKey(eventData.getEventTypeName(), eventData.getSecurity(), handlerMap.keySet());
				}
			}
			else {
				key = InvestmentUtils.getSecurityOverrideKey(eventJournalTypeName, payout, handlerMap.keySet());
				if (key == null) {
					key = InvestmentUtils.getSecurityOverrideKey(eventData.getEventTypeName(), payout, handlerMap.keySet());
				}
			}
			if (key != null) {
				return handlerMap.get(key);
			}
		}
		return null;
	}


	/**
	 * Returns a Map of accounting transaction (GL lot) id's to corresponding event journals
	 * that have been processed by event journals of the specified security event.
	 * This is done in order to avoid double processing.
	 */
	private Map<Long, AccountingEventJournal> getProcessedPositionIds(int securityEventId, String journalTypeName) {
		Map<Long, AccountingEventJournal> result = new HashMap<>();
		AccountingEventJournalSearchForm searchForm = new AccountingEventJournalSearchForm();
		searchForm.setJournalTypeName(journalTypeName);
		searchForm.setSecurityEventId(securityEventId);
		// event is usually processed once so this will often return no results
		List<AccountingEventJournal> processedJournals = getAccountingEventJournalService().getAccountingEventJournalList(searchForm);
		for (AccountingEventJournal journal : CollectionUtils.getIterable(processedJournals)) {
			// get journal with details
			journal = getAccountingEventJournalService().getAccountingEventJournal(journal.getId());
			for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(journal.getDetailList())) {
				result.put(detail.getAccountingTransaction().getId(), journal);
			}
		}
		return result;
	}


	private AccountingEventJournalType getEarlierJournalType(InvestmentSecurityEvent securityEvent, AccountingEventJournalType journalType, long positionId) {
		// get events including this one on the date
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setExcludeId(securityEvent.getId());
		searchForm.setSecurityId(securityEvent.getSecurity().getId());
		searchForm.setEventDate(securityEvent.getEventDate());
		searchForm.setStatusEventJournalNotApplicable(false);
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);

		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {
			if (event.getType().isBeforeSameAsAfter() && MathUtils.isEqual(event.getAfterEventValue(), BigDecimal.ZERO)) {
				// NOTE: a better way to do this could be to call the AccountingEventJournalDetailPopulator and check if it skips the event
				continue; // skip events that do not generate a journal: 0 coupon payment
			}
			// get event journal types and check for lower order
			List<AccountingEventJournalType> typeList = getAccountingEventJournalService().getAccountingEventJournalTypeListByEventType(event.getType().getId());
			int journalBookingOrder = securityEvent.getBookingOrderOverride() != null ? securityEvent.getBookingOrderOverride() : journalType.getBookingOrder();
			for (AccountingEventJournalType type : CollectionUtils.getIterable(typeList)) {
				if (type.getEventJournalTypeScope().isSecurityInScope(securityEvent.getSecurity())) {
					int typeBookingOrder = event.getBookingOrderOverride() != null ? event.getBookingOrderOverride() : type.getBookingOrder();
					// if security event overrides booking order, use that
					if (typeBookingOrder < journalBookingOrder) {
						AccountingEventJournalDetailSearchForm detailSearchForm = new AccountingEventJournalDetailSearchForm();
						detailSearchForm.setSecurityEventId(event.getId());
						detailSearchForm.setJournalTypeId(type.getId());
						detailSearchForm.setAccountingTransactionId(positionId);
						List<AccountingEventJournalDetail> detailList = getAccountingEventJournalService().getAccountingEventJournalDetailList(detailSearchForm);
						if (CollectionUtils.isEmpty(detailList)) {
							return type;
						}
					}
				}
			}
		}

		return null;
	}


	/**
	 * Book and post the journal (accrual and reversal) if the time is right.
	 */
	private boolean bookAndPost(AccountingEventJournal eventJournal, EventJournalGeneratorCommandInternal command) {
		Date generationDate = command.getGenerationDate() != null ? command.getGenerationDate() : new Date();
		if (!command.getGeneratorType().isPreview()) {
			ValidationUtils.assertTrue(DateUtils.compare(command.getGenerationDate(), new Date(), false) <= 0, () -> "Cannot generate event journals for future date " + DateUtils.fromDateSmart(command.getGenerationDate()) + ". Only journal previews can be generated for future dates.");
		}
		boolean posted = false;
		if (isEventJournalReadyForAccrualPosting(eventJournal, generationDate, command.getGeneratorType().isForceAccrualPosting())) {
			getAccountingBookingService().bookAccountingJournal(AccountingEventJournalBookingProcessor.JOURNAL_NAME, eventJournal.getId(), true);
			posted = true;
		}
		if (isEventJournalReadyForReversalPosting(eventJournal, generationDate)) {
			getAccountingBookingService().bookAccountingJournal(AccountingEventJournalBookingProcessor.JOURNAL_NAME, eventJournal.getId(), true);
			posted = true;
		}
		return posted;
	}


	/**
	 * Returns true if accrual is ready for posting for the specified journal on the specified date.
	 * Requirements: accrual hasn't been posted yet and postingDate is on or after event's Ex Date.
	 *
	 * @param eventJournal
	 * @param postingDate
	 * @param forceAccrualPosting if set, ignores Ex Date comparison
	 */
	private boolean isEventJournalReadyForAccrualPosting(AccountingEventJournal eventJournal, Date postingDate, boolean forceAccrualPosting) {
		// not posted yet and posting on or after Ex Date
		if (eventJournal.getBookingDate() == null) {
			if (forceAccrualPosting || DateUtils.compare(eventJournal.getSecurityEvent().getExDate(), postingDate, false) <= 0) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns true if reversal is ready for posting for the specified journal on the specified date.
	 * Requirements: reversal hasn't been posted yet, event has payment delay and postingDate is on or after event's Payment Date.
	 */
	private boolean isEventJournalReadyForReversalPosting(AccountingEventJournal eventJournal, Date postingDate) {
		// can post after Event Date (and there's not delay in payment
		if (eventJournal.getAccrualReversalBookingDate() == null) {
			InvestmentSecurityEvent securityEvent = eventJournal.getSecurityEvent();
			if (isReadyForPostingAfterCheckingActualSettlementDate(securityEvent)) {
				postingDate = securityEvent.getActualSettlementDate() != null ? securityEvent.getActualSettlementDate() : postingDate;
				if (DateUtils.compare(securityEvent.getEventDate(), postingDate, false) <= 0 && securityEvent.isPaymentDelayed()) {
					if (isReadyForPostingAfterFilteringOutTerminatedAccounts(eventJournal)) {
						return true;
					}
				}
			}
		}
		return false;
	}


	private boolean isReadyForPostingAfterCheckingActualSettlementDate(InvestmentSecurityEvent securityEvent) {
		if (securityEvent.getSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
			return securityEvent.getActualSettlementDate() != null;
		}
		return true;
	}


	private boolean isReadyForPostingAfterFilteringOutTerminatedAccounts(AccountingEventJournal eventJournal) {
		Date eventDate = eventJournal.getSecurityEvent().getEventDate();

		// skip details posting after account termination
		List<AccountingEventJournalDetail> filteredList = BeanUtils.filter(eventJournal.getDetailList(), d -> {
			AccountingTransaction t = d.getAccountingTransaction();
			return !(t.getClientInvestmentAccount().isAfterTerminationDate(eventDate) || t.getHoldingInvestmentAccount().isAfterTerminationDate(eventDate));
		});

		int countAfter = CollectionUtils.getSize(filteredList);
		if (countAfter > 0) {
			int countBefore = CollectionUtils.getSize(eventJournal.getDetailList());
			ValidationUtils.assertEquals(countBefore, countAfter, "Cannot post Accrual Reversal for Event Journal because it has details for both Active and Terminated accounts. Separate Event Journals must be generated per Holding Account.");
			return true;
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public InvestmentSecurityEventPayoutService getInvestmentSecurityEventPayoutService() {
		return this.investmentSecurityEventPayoutService;
	}


	public void setInvestmentSecurityEventPayoutService(InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService) {
		this.investmentSecurityEventPayoutService = investmentSecurityEventPayoutService;
	}


	public InvestmentSecurityEventActionProcessorService getInvestmentSecurityEventActionProcessorService() {
		return this.investmentSecurityEventActionProcessorService;
	}


	public void setInvestmentSecurityEventActionProcessorService(InvestmentSecurityEventActionProcessorService investmentSecurityEventActionProcessorService) {
		this.investmentSecurityEventActionProcessorService = investmentSecurityEventActionProcessorService;
	}


	public Map<String, AccountingEventJournalDetailPopulator> getDetailPopulatorMap() {
		return this.detailPopulatorMap;
	}


	public void setDetailPopulatorMap(Map<String, AccountingEventJournalDetailPopulator> detailPopulatorMap) {
		this.detailPopulatorMap = detailPopulatorMap;
	}


	public Map<String, AccountingEventJournalDetailRounder> getDetailRounderMap() {
		return this.detailRounderMap;
	}


	public void setDetailRounderMap(Map<String, AccountingEventJournalDetailRounder> detailRounderMap) {
		this.detailRounderMap = detailRounderMap;
	}


	public AccountingBookingService<AccountingEventJournal> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<AccountingEventJournal> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public Map<String, AccountingEventJournalPositionRetriever> getAffectedPositionsRetrieverMap() {
		return this.affectedPositionsRetrieverMap;
	}


	public void setAffectedPositionsRetrieverMap(Map<String, AccountingEventJournalPositionRetriever> affectedPositionsRetrieverMap) {
		this.affectedPositionsRetrieverMap = affectedPositionsRetrieverMap;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}

