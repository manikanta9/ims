package com.clifton.accounting.eventjournal.generator.detailpopulator;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingEventJournalDetailFactorChangePopulator</code> class sets TransactionAmount equal to factor payment
 * for the specified lot. It also sets AffectedCost to the amount of reduction in cost basis.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailFactorChangePopulator implements AccountingEventJournalDetailPopulator {

	private AccountingPositionService accountingPositionService;
	private InvestmentCalculator investmentCalculator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		// if position is currently fully closed, then do not process Factor Change event (Assumed Factor Change will handle this)
		AccountingEventJournalDetail detail = command.getJournalDetail();
		if (!isPositionStillOpen(detail.getAccountingTransaction())) {
			command.setSkipMessage("Position is currently fully closed.");
			return true;
		}

		// if previous and current factors are the same: skip the journal
		EventData eventData = command.getEventData();
		if (MathUtils.isEqual(eventData.getBeforeEventValue(), eventData.getAfterEventValue())) {
			command.setSkipMessage("Previous and Current Factors are the same.");
			return true;
		}

		// factor change to 0 fully closes existing position
		if (eventData.getAfterEventValue().compareTo(BigDecimal.ZERO) == 0) {
			detail.setTransactionAmount(command.getRemainingQuantity());
			detail.setAffectedCost(command.getRemainingCostBasis());
		}
		else {
			BigDecimal expectedEndFace = MathUtils.multiply(command.getOpeningQuantity(), eventData.getAfterEventValue(), 2);
			BigDecimal paymentAmount = MathUtils.subtract(detail.getAffectedQuantity(), expectedEndFace);
			detail.setTransactionAmount(paymentAmount);
			detail.setAffectedCost(getInvestmentCalculator().calculateNotional(eventData.getSecurity(), detail.getTransactionPrice(), paymentAmount, BigDecimal.ONE));
		}

		// if there is a loss because of default, calculate it and reduce payment amount by the amount of loss
		if (!MathUtils.isNullOrZero(eventData.getAdditionalEventValue())) {
			BigDecimal lossBasis = command.getOpeningQuantity();
			detail.setAdditionalAmount(MathUtils.divide(MathUtils.multiply(lossBasis, eventData.getAdditionalEventValue()), new BigDecimal("100"), 2));
			detail.setTransactionAmount(MathUtils.subtract(detail.getTransactionAmount(), detail.getAdditionalAmount()));
		}

		return false;
	}


	protected boolean isPositionStillOpen(AccountingTransaction t) {
		List<AccountingPosition> openPositionList = getAccountingPositionService().getAccountingPositionList(t.getClientInvestmentAccount().getId(), t.getHoldingInvestmentAccount().getId(),
				t.getInvestmentSecurity().getId(), DateUtils.clearTime(new Date()));
		for (AccountingPosition position : CollectionUtils.getIterable(openPositionList)) {
			if (position.getId().equals(t.getId())) {
				return true;
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}
}
