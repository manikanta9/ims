package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.eventjournal.EventJournalUtils;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingPaymentSameDayEventBookingRule</code> class implements a booking rule for event journals
 * that handles position based payments: dividends, coupons, etc.
 * <p>
 * This booking rule applies for event when there's no delay before payment (no accrual is necessary).
 *
 * @author vgomelsky
 */
public class AccountingPaymentSameDayEventBookingRule extends AccountingPaymentAccrualEventBookingRule {

	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		// process booking at the time of accrual: nothing at reversal
		if (eventJournal.getBookingDate() == null) {
			// add debit and credit lines for each payment
			Date transactionDate = getTransactionDate(eventJournal, false);
			for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(EventJournalUtils.getCurrencyPaymentDetailList(eventJournal))) {
				BigDecimal frankingCreditTransactionAmount = processFrankingCreditPayout(journal, detail, transactionDate);
				// frankingCreditTransactionAmount is added and then subtracted again because franking credits only apply to the accrual and not the reversal
				detail.setTransactionAmount(MathUtils.add(detail.getTransactionAmount(), frankingCreditTransactionAmount));
				journal.addJournalDetail(generateJournalDetail(journal, detail, false, transactionDate));
				detail.setTransactionAmount(MathUtils.subtract(detail.getTransactionAmount(), frankingCreditTransactionAmount));
				journal.addJournalDetail(generateJournalDetail(journal, detail, true, transactionDate));
			}
			eventJournal.setAccrualReversalCombined(true);
		}
	}


	@Override
	protected AccountingJournalDetail generateJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventJournalDetail, boolean debit, Date transactionDate) {
		AccountingJournalDetail detail = super.generateJournalDetail(journal, eventJournalDetail, debit, transactionDate);

		AccountingEventJournalType eventJournalType = eventJournalDetail.getJournal().getJournalType();
		if (detail.getAccountingAccount().isReceivable()) {
			// apply the new reversal account
			detail.setAccountingAccount(eventJournalType.getAccrualReversalAccount());
			InvestmentSecurity currencyDenomination = detail.getInvestmentSecurity().getInstrument().getTradingCurrency();
			ValidationUtils.assertNotNull(currencyDenomination, "Currency denomination must be defined for instrument: " + detail.getInvestmentSecurity().getInstrument().getLabel());
			detail.setInvestmentSecurity(currencyDenomination);

			// use Currency GL account for foreign payments
			if (!currencyDenomination.equals(InvestmentUtils.getClientAccountBaseCurrency(detail))) {
				detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
			}

			// no quantity or price for cash/currency
			detail.setQuantity(null);
			detail.setPrice(null);
			detail.setPositionCostBasis(BigDecimal.ZERO);
			detail.setDescription(eventJournalDetail.getLabel());
		}

		InvestmentSecurityEvent event = eventJournalDetail.getJournal().getSecurityEvent();
		detail.setSettlementDate(event.getEventDate());
		detail.setOriginalTransactionDate(event.getEventDate());
		detail.setTransactionDate(event.getEventDate());

		return detail;
	}
}
