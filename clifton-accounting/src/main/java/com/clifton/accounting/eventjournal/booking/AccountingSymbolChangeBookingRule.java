package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Set;


/**
 * The AccountingSymbolChangeBookingRule class is a booking rule that generates 2 entries for each event journal detail:
 * a closing position at cost and a new opening position for the new symbol at cost while preserving original transaction date.
 *
 * @author vgomelsky
 */
public class AccountingSymbolChangeBookingRule extends BaseEventJournalBookingRule {

	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		// close all open positions at cost and reopen the same positions using new security
		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			AccountingJournalDetail closingDetail = generateClosingJournalDetail(journal, eventDetail);
			journal.addJournalDetail(closingDetail);
			journal.addJournalDetail(generateOpeningJournalDetail(eventJournal, closingDetail));
		}
	}


	private AccountingJournalDetail generateOpeningJournalDetail(AccountingEventJournal eventJournal, AccountingJournalDetail closingDetail) {
		AccountingJournalDetail result = new AccountingJournalDetail();
		BeanUtils.copyProperties(closingDetail, result, new String[]{"parentTransaction"});

		result.setOpening(true);
		result.setInvestmentSecurity(eventJournal.getSecurityEvent().getAdditionalSecurity());
		populatePositionParentTransaction(result, closingDetail.getParentTransaction(), false);
		result.setQuantity(result.getQuantity().negate());
		result.setLocalDebitCredit(result.getLocalDebitCredit().negate());
		result.setBaseDebitCredit(result.getBaseDebitCredit().negate());
		result.setPositionCostBasis(result.getPositionCostBasis().negate());

		return result;
	}


	private AccountingJournalDetail generateClosingJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventDetail) {
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail result = new AccountingJournalDetail();
		result.setJournal(journal);
		result.setFkFieldId(eventDetail.getId());
		result.setSystemTable(getAccountingEventJournalDetailTable());
		result.setOpening(false);

		result.setParentTransaction(affectedLot);
		result.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		result.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		result.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		result.setAccountingAccount(affectedLot.getAccountingAccount());
		result.setExchangeRateToBase(eventDetail.getExchangeRateToBase());
		if (result.getAccountingAccount().isExecutingBrokerSpecific()) {
			result.setExecutingCompany(affectedLot.getExecutingCompany());
		}

		result.setPrice(eventDetail.getTransactionPrice());
		result.setQuantity(eventDetail.getAffectedQuantity().negate());

		BigDecimal positionCostBasis = eventDetail.getAffectedCost().negate();
		if (InvestmentUtils.isNoPaymentOnOpen(result.getInvestmentSecurity())) {
			result.setLocalDebitCredit(BigDecimal.ZERO);
		}
		else {
			result.setLocalDebitCredit(positionCostBasis);
		}
		result.setBaseDebitCredit(MathUtils.multiply(result.getLocalDebitCredit(), result.getExchangeRateToBase(), 2));
		result.setPositionCostBasis(positionCostBasis);

		StringBuilder description = new StringBuilder(100);
		description.append(event.getType().getName());
		description.append(" for ");
		description.append(event.getSecurity().getSymbol());
		description.append(" on ");
		description.append(DateUtils.fromDateShort(event.getEventDate()));

		result.setDescription(description.toString());

		result.setOriginalTransactionDate(affectedLot.getOriginalTransactionDate());
		result.setTransactionDate(event.getEventDate());
		result.setSettlementDate(event.getEventDate());

		result.setOpening(false);
		result.setPositionCommission(BigDecimal.ZERO);

		return result;
	}
}
