package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;
import java.util.Set;


/**
 * The <code>AccountingEventJournalPositionResetBookingRule</code> class is a booking rule for Swap Reset events:
 * Total Return Swaps, etc.  For each open position, it creates an entry that fully closes it at cost.
 * Also, if the rest event is not on security maturity date (last rest), this booking rule will also create a new
 * opening position for the same quantity but at different price.
 * <p/>
 * NOTE: it must be used with another booking rule that handles gain/loss realized at reset.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalPositionResetBookingRule extends BaseEventJournalBookingRule {

	private InvestmentCalculator investmentCalculator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		// for each open position
		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			// always close existing position at cost
			AccountingJournalDetailDefinition positionClosingDetail = generateResetPosition(journal, eventDetail, false);
			journal.addJournalDetail(positionClosingDetail);

			// re-open at new price ONLY if this is not the last reset: security matured
			InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();
			if (!event.getDayBeforeExDate().equals(event.getSecurity().getEndDate())) {
				AccountingJournalDetailDefinition positionOpeningDetail = generateResetPosition(journal, eventDetail, true);
				journal.addJournalDetail(positionOpeningDetail);
			}
		}
	}


	private AccountingJournalDetail generateResetPosition(AccountingJournal journal, AccountingEventJournalDetail eventDetail, boolean opening) {
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(eventDetail.getId());
		detail.setSystemTable(getAccountingEventJournalDetailTable());

		detail.setParentTransaction(opening ? null : affectedLot);
		if (opening) {
			// affected lot could be a trade fill
			AccountingTransaction parent = affectedLot.getParentTransaction() == null ? affectedLot : affectedLot.getParentTransaction();
			populatePositionParentTransaction(detail, parent, false);
		}
		detail.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		detail.setAccountingAccount(affectedLot.getAccountingAccount());
		detail.setExchangeRateToBase(affectedLot.getExchangeRateToBase());

		detail.setLocalDebitCredit(BigDecimal.ZERO);
		detail.setBaseDebitCredit(BigDecimal.ZERO);
		detail.setPrice(eventDetail.getTransactionPrice());

		if (opening) {
			detail.setQuantity(eventDetail.getAdditionalAmount()); // usually same as closing but can be different
			detail.setPositionCostBasis(getInvestmentCalculator().calculateNotional(detail.getInvestmentSecurity(), detail.getPrice(), detail.getQuantity(), BigDecimal.ONE));
			detail.setDescription("Position opening of " + detail.getInvestmentSecurity().getSymbol() + " from reset");
		}
		else {
			detail.setQuantity(eventDetail.getAffectedQuantity().negate());
			detail.setPositionCostBasis(eventDetail.getAffectedCost().negate());
			detail.setDescription("Position closing of " + detail.getInvestmentSecurity().getSymbol() + " from reset");
		}

		detail.setSettlementDate(event.getEventDate());
		detail.setOriginalTransactionDate(opening ? affectedLot.getOriginalTransactionDate() : event.getDayBeforeExDate());
		detail.setTransactionDate(event.getDayBeforeExDate());

		detail.setOpening(opening);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
