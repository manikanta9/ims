package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * <code>AccountingMergerEventBookingRule</code> generates journal details for each lot payout. Mergers support a currency payout, a new security payout, or a combination of those.
 * Each merger event results in a close of the existing lot and one or more of the following:
 * <br/>- Cash/currency payment for currency payout
 * <br/>- Open of new position for new security payout
 * <br/>- Close of fractional shares, if applicable, for the new security payout
 *
 * @author nickk
 */
public class AccountingMergerEventBookingRule extends BaseEventJournalBookingRule {

	private MarketDataRetriever marketDataRetriever;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();
		InvestmentSecurityEvent event = eventJournal.getSecurityEvent();

		/*
		 * The following two maps are used to adjust the closing detail for each lot applicable to the event.
		 * The closing detail's price and notional will be set according to the aggregated payouts received for proper gain/loss handling.
		 */
		Map<Long, AccountingJournalDetailDefinition> lotToClosingDetailMap = new HashMap<>();
		Map<Long, BigDecimal> lotToClosingDetailCostBasisMap = new HashMap<>();

		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			AccountingEventJournalDetail.Merger merger = eventDetail.forMerger();
			InvestmentSecurityEventPayout payout = eventDetail.getEventPayout();

			// close existing parent lot
			AccountingJournalDetailDefinition parentClosing = lotToClosingDetailMap.computeIfAbsent(eventDetail.getAccountingTransaction().getId(), id -> addParentLotClosing(journal, merger));

			if (MathUtils.isNullOrZero(merger.getNewQuantity())) {
				// Currency Payout
				AccountingJournalDetail currencyPayout = createCashCurrencyRecord(parentClosing, eventDetail, payout.getPayoutSecurity());
				populatePositionParentTransaction(currencyPayout, eventDetail.getAccountingTransaction(), false);
				currencyPayout.setParentDefinition(null);
				currencyPayout.setExchangeRateToBase(eventDetail.getExchangeRateToBase());
				currencyPayout.setLocalDebitCredit(merger.getPaymentAmount());
				currencyPayout.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(currencyPayout.getLocalDebitCredit(), currencyPayout.getExchangeRateToBase(), currencyPayout.getClientInvestmentAccount()));
				currencyPayout.setDescription("Cash payment from " + event.getSecurity().getSymbol() + " Merger on " + DateUtils.fromDate(event.getEventDate(), DateUtils.DATE_FORMAT_INPUT));
				journal.addJournalDetail(currencyPayout);
			}
			else {
				// Security Payout
				addJournalDetailsForSecurityOpening(journal, merger);
			}

			lotToClosingDetailCostBasisMap.compute(eventDetail.getAccountingTransaction().getId(), (lotId, currentValue) -> {
				// payment amount and new cost basis are both in local amounts of the payout's security
				BigDecimal closingCostBasisAdjustment = ObjectUtils.coalesce(merger.getNewCostBasis(), merger.getPaymentAmount());
				// Adjust cost basis adjustment with exchange rate
				InvestmentSecurity closingDetailCurrency = parentClosing.getInvestmentSecurity().getInstrument().getTradingCurrency();
				InvestmentSecurity payoutCurrency = payout.getPayoutType().isAdditionalSecurityCurrency() ? payout.getPayoutSecurity() : payout.getPayoutSecurity().getInstrument().getTradingCurrency();
				if (!closingDetailCurrency.equals(payoutCurrency)) {
					// Base amount calculation here is for the currency of the existing position, not base for the client. We want values in the currency of the cost basis
					BigDecimal exchangeRate = getExchangeRate(payoutCurrency, closingDetailCurrency, parentClosing, event, eventJournal.getJournalType());
					closingCostBasisAdjustment = InvestmentCalculatorUtils.calculateBaseAmount(closingCostBasisAdjustment, exchangeRate, closingDetailCurrency);
				}
				return MathUtils.subtract(currentValue, closingCostBasisAdjustment);
			});
		}

		// Adjust closing lot details according to the processed payout(s) of the event for realized gain/loss in next rule(s).
		lotToClosingDetailMap.forEach((lotId, closingDetail) -> populateJournalDetail(closingDetail, closingDetail.getQuantity(), closingDetail.getPrice(), lotToClosingDetailCostBasisMap.get(lotId)));

		bookingSession.sortPositions(AccountingPositionOrders.FIFO);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingJournalDetailDefinition addParentLotClosing(AccountingJournal journal, AccountingEventJournalDetail.Merger merger) {
		AccountingEventJournalDetail eventDetail = merger.getDetail();

		// parent lot closing not found, create and add it
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();
		InvestmentSecurityEvent event = merger.getDetail().getJournal().getSecurityEvent();
		AccountingJournalDetail closing = newJournalDetailForEventDetail(journal, eventDetail, false, merger.getQuantity().negate(), affectedLot.getPrice(), affectedLot.getPositionCostBasis().negate(), true);
		closing.setDescription("Position closing of " + event.getSecurity().getSymbol() + " from Merger on " + DateUtils.fromDate(event.getEventDate(), DateUtils.DATE_FORMAT_INPUT));
		// clear parent and set to opening for splitter rule to determine
		closing.setParentTransaction(null);
		closing.setOpening(true);
		journal.addJournalDetail(closing);
		// NOTE: realized gain/loss will be calculated and added by the next rule

		return closing;
	}


	/**
	 * Adds journal details for the opening position defined by the merger payout and lot. If fractional shares are applicable, a detail will be added for the openning position.
	 */
	private void addJournalDetailsForSecurityOpening(AccountingJournal journal, AccountingEventJournalDetail.Merger merger) {
		InvestmentSecurityEventPayout payout = merger.getDetail().getEventPayout();
		AccountingJournalDetail newPosition = newJournalDetailForEventDetail(journal, merger.getDetail(), true, merger.getNewQuantity(), merger.getPrice(), merger.getNewCostBasis(), false);
		AccountingTransaction parentTransaction = ObjectUtils.coalesce(merger.getDetail().getAccountingTransaction().getParentTransaction(), merger.getDetail().getAccountingTransaction());
		populatePositionParentTransaction(newPosition, parentTransaction, false);
		newPosition.setDescription("Position opening of " + payout.getPayoutSecurity().getSymbol() + " from Merger on " + DateUtils.fromDate(merger.getEventDate(), DateUtils.DATE_FORMAT_INPUT));
		journal.addJournalDetail(newPosition);

		// Fractional shares only applies when there is a security payout
		addJournalDetailsForFractionalSharesClosing(journal, merger, newPosition);
	}


	private void addJournalDetailsForFractionalSharesClosing(AccountingJournal journal, AccountingEventJournalDetail.Merger merger, AccountingJournalDetail openingPositionDetail) {
		if (!MathUtils.isNullOrZero(merger.getFractionToClose())) {
			BigDecimal fractionalSharesSecurityPrice = getMergerSecurityPrice(merger, true, false);
			AccountingJournalDetail fractionalClosing = newJournalDetailForEventDetail(journal, merger.getDetail(), true, merger.getFractionToClose().negate(), fractionalSharesSecurityPrice, null, true);
			fractionalClosing.setDescription("Fractional Shares Closing from rounding from " + merger.getDetail().getEventPayout().getLabelLong());
			fractionalClosing.setParentTransaction(null);
			fractionalClosing.setParentDefinition(openingPositionDetail);
			journal.addJournalDetail(fractionalClosing);

			AccountingJournalDetail cashProceeds = createCashCurrencyRecord(fractionalClosing);
			cashProceeds.setDescription(cashProceeds.getDescription() + " " + merger.getJournalType().getName() + " fractional shares");
			journal.addJournalDetail(cashProceeds);
			// NOTE: realized gain/loss will be calculated and added by the next rule
		}
	}


	private AccountingJournalDetail newJournalDetailForEventDetail(AccountingJournal journal, AccountingEventJournalDetail eventDetail, boolean usePayoutSecurity, BigDecimal quantity, BigDecimal price, BigDecimal positionCostBasis, boolean close) {
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(eventDetail.getId());
		detail.setSystemTable(getAccountingEventJournalDetailTable());

		detail.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(usePayoutSecurity ? eventDetail.getEventPayout().getPayoutSecurity() : affectedLot.getInvestmentSecurity());
		detail.setAccountingAccount(affectedLot.getAccountingAccount());
		if (detail.getAccountingAccount().isExecutingBrokerSpecific()) {
			detail.setExecutingCompany(affectedLot.getExecutingCompany());
		}
		populatePositionParentTransaction(detail, affectedLot, close);

		detail.setDescription(eventDetail.getLabel());

		detail.setSettlementDate(event.getPaymentDate());
		detail.setOriginalTransactionDate(affectedLot.getOriginalTransactionDate());
		detail.setTransactionDate(event.getEventDate());

		detail.setPositionCommission(BigDecimal.ZERO);
		detail.setOpening(!close);

		BigDecimal exchangeRateToBase = usePayoutSecurity ? eventDetail.getExchangeRateToBase() : affectedLot.getExchangeRateToBase();
		detail.setExchangeRateToBase(exchangeRateToBase);

		populateJournalDetail(detail, quantity, price, positionCostBasis);

		return detail;
	}


	private void populateJournalDetail(AccountingJournalDetailDefinition detail, BigDecimal quantity, BigDecimal price, BigDecimal positionCostBasis) {
		detail.setQuantity(quantity);
		detail.setPrice(price);
		if (positionCostBasis == null) {
			// calculate it if not specified
			positionCostBasis = InvestmentCalculatorUtils.getNotionalCalculator(detail.getInvestmentSecurity()).calculateNotional(price, detail.getInvestmentSecurity().getPriceMultiplier(), quantity);
		}
		if (detail.getInvestmentSecurity().getInstrument().getHierarchy().isNoPaymentOnOpen()) {
			detail.setLocalDebitCredit(BigDecimal.ZERO);
			detail.setBaseDebitCredit(BigDecimal.ZERO);
		}
		else {
			detail.setLocalDebitCredit(positionCostBasis);
			detail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(positionCostBasis, detail.getExchangeRateToBase(), detail.getClientInvestmentAccount()));
		}
		detail.setPositionCostBasis(positionCostBasis);
	}


	private BigDecimal getMergerSecurityPrice(AccountingEventJournalDetail.Merger merger, boolean usePayoutSecurity, boolean flexible) {
		InvestmentSecurity security = usePayoutSecurity ? merger.getDetail().getEventPayout().getPayoutSecurity() : merger.getEventSecurity();
		Date businessDayBeforePayout = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(merger.getEventDate()), -1);
		return flexible ? getMarketDataRetriever().getPriceFlexible(security, businessDayBeforePayout, true)
				: getMarketDataRetriever().getPrice(security, businessDayBeforePayout, true, "Merger Security:");
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
