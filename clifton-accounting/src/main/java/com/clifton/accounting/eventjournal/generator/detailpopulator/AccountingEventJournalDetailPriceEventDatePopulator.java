package com.clifton.accounting.eventjournal.generator.detailpopulator;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.core.util.date.DateUtils;
import com.clifton.marketdata.MarketDataRetriever;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingEventJournalDetailPriceEventDatePopulator</code> class populates TransactionPrice to the closing price for
 * event's security on event date or security end date price (whichever is earlier).
 * Used by most securities: futures, etc.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailPriceEventDatePopulator implements AccountingEventJournalDetailPopulator {

	private MarketDataRetriever marketDataRetriever;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		EventData eventData = command.getEventData();
		Date priceDate = eventData.getEventDate();
		// NDF forwards must use "Fixing Date" (security End Date)
		Date securityEndDate = eventData.getSecurity().getEndDate();
		if (securityEndDate != null && securityEndDate.before(priceDate)) {
			priceDate = securityEndDate;
		}

		BigDecimal closingPrice = getMarketDataRetriever().getPrice(eventData.getSecurity(), priceDate, false, null);
		if (closingPrice == null) {
			command.setSkipMessage("Cannot mature security " + eventData.getSecurity().getLabel() + " because no Price exists for it on " + DateUtils.fromDateShort(priceDate) + ". Validate that there is a price and that the End Date for the security is correct.");
			return true;
		}
		AccountingEventJournalDetail detail = command.getJournalDetail();
		detail.setTransactionPrice(closingPrice);

		if (command.isSecurityMaturityEvent()) {
			detail.forMaturity().setOpeningPrice(command.getOpeningPrice());
		}

		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
