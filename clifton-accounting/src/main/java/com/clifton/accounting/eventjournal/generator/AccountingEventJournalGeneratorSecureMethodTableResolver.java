package com.clifton.accounting.eventjournal.generator;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.core.security.authorization.SecureMethodTableResolver;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;


/**
 * The <code>AccountingEventJournalGeneratorSecureMethodTableResolver</code> class requires access to Event Journals
 * when Posting is requested.  It requires access to Security Events when posting is not requested.
 *
 * @author vgomelsky
 */
@Component
public class AccountingEventJournalGeneratorSecureMethodTableResolver implements SecureMethodTableResolver {

	@Override
	public String getTableName(@SuppressWarnings("unused") Method method, Map<String, ?> config) {
		String type = MapUtils.getParameterAsString("generatorType", config);
		if (type == null) {
			throw new ValidationException("Cannot authorize access because required parameter 'generatorType' is missing.");
		}
		AccountingEventJournalGeneratorTypes generatorType = AccountingEventJournalGeneratorTypes.valueOf(type);
		if (generatorType.isPost()) {
			return AccountingEventJournal.class.getSimpleName();
		}
		return InvestmentSecurityEvent.class.getSimpleName();
	}
}
