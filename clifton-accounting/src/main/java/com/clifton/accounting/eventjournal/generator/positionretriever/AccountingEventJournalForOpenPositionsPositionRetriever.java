package com.clifton.accounting.eventjournal.generator.positionretriever;


import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingEventJournalForOpenPositionsPositionRetriever</code> class provides default implementation that returns positions open one day before event's Ex Date.
 * <p>
 * Dividend Payments, Coupon Payments and other events apply to positions held at the end of day prior to Ex Date.
 * <p>
 * Depending on AccountingEventJournalType.isSettlementDateComparison(), either Settlement or Trade date is used to determine open positions. All bond events rely on Settlement
 * Date based positions while Dividend Payments rely on Trade date.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalForOpenPositionsPositionRetriever implements AccountingEventJournalPositionRetriever {

	/**
	 * Most event types should apply this logic to skip event generation for "Position Collateral", "Position Collateral Payable", "Position Collateral Receivable"
	 * depending on the type of accounts: Escrow, Margin.
	 * For event types that change position quantity (Stock Split, Stock Symbol Change), it's important to process all lots so that receivable matches payable.
	 */
	private boolean collateralSkipLogicEnabled = true; // NOTE: might be better (more flexibility) to add this flag to AccountingEventJournalType instead.

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingEventJournalService accountingEventJournalService;
	private AccountingPositionService accountingPositionService;
	private AccountingTransactionService accountingTransactionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional // need @Transactional to get journal property of transaction which is a proxy
	public List<AccountingPosition> getAffectedPositions(AccountingEventJournalType journalType, InvestmentSecurityEvent securityEvent, Integer clientInvestmentAccountId) {
		// Dividend Payments, Coupon Payments and other events apply to positions held at the end of day prior to Ex Date.
		Date positionDate = DateUtils.addDays(securityEvent.getExDate(), -journalType.getDaysBeforeExDateForPositionLookup());

		// get open positions using Settlement or Trade Date depending on journal type
		AccountingPositionCommand command = AccountingPositionCommand.onDate(journalType.isSettlementDateComparison(), positionDate)
				.forClientAccount(clientInvestmentAccountId)
				.forInvestmentSecurity(securityEvent.getSecurity().getId());
		List<AccountingPosition> openPositionList = getAccountingPositionService().getAccountingPositionListUsingCommand(command);

		openPositionList = filterPositionsToSkip(openPositionList, securityEvent, positionDate);
		LogUtils.info(getClass(), "Found " + openPositionList.size() + " open positions for " + securityEvent.getSecurity().getName());
		if (journalType.isSettlementDateComparison()) {
			if (securityEvent.isPaymentDelayed()) {
				openPositionList = adjustForUnsettledClosingPositions(openPositionList, positionDate);
			}
		}

		return openPositionList;
	}


	private List<AccountingPosition> filterPositionsToSkip(List<AccountingPosition> positionList, InvestmentSecurityEvent securityEvent, Date positionDate) {
		List<AccountingPosition> result = new ArrayList<>();
		for (AccountingPosition position : positionList) {
			boolean skip = false;

			if (!isAccountActiveOnPositionLookupDate(position, positionDate)) {
				// skip positions for non-active accounts: after an account is closed, we don't want any activity (delayed dividends, etc.)
				skip = true;
			}
			else if (isCollateralSkipLogicEnabled() && position.getAccountingAccount().isCollateral()) {
				// for collateral positions, we need to make sure that the event is generated only for 1 lot when there are matching Receivable and Payable lots
				// for "Escrow Receipt" accounts, no events should be generated: the event should be generated for Receivable entry (usually at Custodian)
				// for "Margin" (and other) accounts, the event should be generated for Position Collateral but not for Receivable and Payable lots
				if (InvestmentAccountType.ESCROW_RECEIPT.equals(position.getHoldingInvestmentAccount().getType().getName())) {
					skip = true;
				}
				else if (position.getAccountingAccount().isReceivable()) {
					if (AccountingAccountType.LIABILITY.equals(position.getAccountingAccount().getAccountType().getName())) {
						skip = true; // Position Collateral Payable
					}
					else {
						// Position Collateral Receivable
						// if there is a matching Payable position that comes from the same journal and is in Escrow Receipt account, then do NOT skip
						skip = true;
						for (AccountingPosition anotherPosition : positionList) {
							if (anotherPosition.getAccountingAccount().isReceivable() && InvestmentAccountType.ESCROW_RECEIPT.equals(anotherPosition.getHoldingInvestmentAccount().getType().getName())
									&& anotherPosition.getOpeningTransaction().getJournal().equals(position.getOpeningTransaction().getJournal())
									&& MathUtils.isEqual(position.getQuantity(), MathUtils.negate(anotherPosition.getQuantity()))) {
								// found Position Collateral Receivable with matching Escrow Receipt Payable: the event stays with the Receivable
								skip = false;
								break;
							}
						}
					}
				}
			}

			// skip positions that were already generated from this event (self reference for Stock Splits with Ex Date after Event Date)
			if (!skip) {
				AccountingJournal openingJournal = getOpeningEventJournal(position);
				if (openingJournal != null) {
					AccountingEventJournal eventJournal = getAccountingEventJournalService().getAccountingEventJournal(openingJournal.getFkFieldId());
					if (eventJournal != null) {
						if (securityEvent.equals(eventJournal.getSecurityEvent())) {
							skip = true;
						}
						// also skip events based on positions that were open from same day event before this one
						// for example, swap reset will open a new position and interest should be processed on the old position only
						if (DateUtils.compare(securityEvent.getEventDate(), eventJournal.getSecurityEvent().getEventDate(), false) == 0) {
							if (securityEvent.getType().getEventOrder() < eventJournal.getSecurityEvent().getType().getEventOrder()) {
								skip = true;
							}
						}
					}
				}
			}

			if (!skip) {
				result.add(position);
			}
		}
		return result;
	}


	/**
	 * Returns true both client and holding accounts are active on the specified date. The logic checks termination date
	 * for Active accounts that are pending termination.
	 */
	private boolean isAccountActiveOnPositionLookupDate(AccountingPosition position, Date positionDate) {
		InvestmentAccount clientAccount = position.getClientInvestmentAccount();
		InvestmentAccount holdingAccount = position.getHoldingInvestmentAccount();

		if (!WorkflowStatus.STATUS_ACTIVE.equals(clientAccount.getWorkflowStatus().getName())) {
			return false;
		}
		if (!WorkflowStatus.STATUS_ACTIVE.equals(holdingAccount.getWorkflowStatus().getName())) {
			return false;
		}

		if (clientAccount.isAfterTerminationDate(positionDate)) {
			return false;
		}
		if (holdingAccount.isAfterTerminationDate(positionDate)) {
			return false;
		}

		return true;
	}


	/**
	 * Returns accounting event journal for the specified position only if it's of event journal type.  Returns null otherwise.
	 * The logic will look at parent and grand parent positions to check if they are an event journal from the same date.
	 * This is necessary in order to prevent double processing of the same event if the position was transferred on the same date.
	 */
	private AccountingJournal getOpeningEventJournal(AccountingPosition position) {
		AccountingJournal openingJournal = position.getOpeningTransaction().getJournal();
		if (!AccountingJournalType.EVENT_JOURNAL.equals(openingJournal.getJournalType().getName())) {
			// it is still possible that the opening lot resulted from the same event journal if it was transferred on the same day: check parent and grandparent
			openingJournal = null;
			AccountingTransaction parent = position.getOpeningTransaction().getParentTransaction();
			if (parent != null && !parent.isDeleted() && parent.isOpening() && parent.getAccountingAccount().isPosition() && DateUtils.isEqualWithoutTime(parent.getTransactionDate(), position.getTransactionDate())) {
				if (AccountingJournalType.EVENT_JOURNAL.equals(parent.getJournal().getJournalType().getName())) {
					openingJournal = parent.getJournal();
				}
				else {
					AccountingTransaction grandParent = parent.getParentTransaction();
					if (grandParent != null && !grandParent.isDeleted() && grandParent.isOpening() && grandParent.getAccountingAccount().isPosition() && DateUtils.isEqualWithoutTime(grandParent.getTransactionDate(), position.getTransactionDate())) {
						if (AccountingJournalType.EVENT_JOURNAL.equals(grandParent.getJournal().getJournalType().getName())) {
							openingJournal = grandParent.getJournal();
						}
					}
				}
			}
		}
		return openingJournal;
	}


	private List<AccountingPosition> adjustForUnsettledClosingPositions(List<AccountingPosition> openPositionList, Date positionDate) {
		// bond Factor Change event uses Settlement Date position lookup
		// for delays longer than a month, we need to use Transaction Date in order to get accurate current face using Settlement Date lookup
		Date fromDate = DateUtils.addDays(positionDate, 1);
		for (AccountingPosition position : openPositionList) {
			// find closing positions after transaction date and use them to reduce existing position
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setParentId(position.getId());
			searchForm.setAccountingAccountId(position.getAccountingAccount().getId());
			searchForm.setOpening(false);
			searchForm.setMinSettlementDate(fromDate); // settled after lookup date
			searchForm.setMaxTransactionDate(positionDate); // but transacted on or before

			List<AccountingTransaction> transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
			for (AccountingTransaction transaction : CollectionUtils.getIterable(transactionList)) {
				position.setRemainingQuantity(MathUtils.add(position.getRemainingQuantity(), transaction.getQuantity()));
				position.setRemainingCostBasis(MathUtils.add(position.getRemainingCostBasis(), transaction.getPositionCostBasis()));
				position.setRemainingBaseDebitCredit(MathUtils.add(position.getRemainingBaseDebitCredit(), transaction.getBaseDebitCredit()));
			}
		}

		// filter out fully closed positions
		return BeanUtils.filter(openPositionList, position -> !MathUtils.isNullOrZero(position.getRemainingQuantity()) ||
				!MathUtils.isNullOrZero(position.getRemainingCostBasis()) ||
				!MathUtils.isNullOrZero(position.getRemainingBaseDebitCredit())
		);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCollateralSkipLogicEnabled() {
		return this.collateralSkipLogicEnabled;
	}


	public void setCollateralSkipLogicEnabled(boolean collateralSkipLogicEnabled) {
		this.collateralSkipLogicEnabled = collateralSkipLogicEnabled;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}
}
