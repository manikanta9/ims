package com.clifton.accounting.eventjournal.eventrollup;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommand;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AccountingSecurityEventLegsWithPaymentHandlerImpl</code>
 *
 * @author manderson
 */
@Component
public class AccountingSecurityEventRollupHandlerImpl<T extends AccountingSecurityEventRollup> implements AccountingSecurityEventRollupHandler<T> {

	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;
	private AccountingEventJournalService accountingEventJournalService;

	private InvestmentSecurityEventService investmentSecurityEventService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<T> getAccountingSecurityEventRollupList(Class<T> clazz, int securityId) {
		List<T> result = new ArrayList<>();

		T baseEvent = BeanUtils.newInstance(clazz);

		// start with first event
		String firstLegType = baseEvent.getEventLegTypes()[0];
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(securityId);
		searchForm.setTypeName(firstLegType);
		List<InvestmentSecurityEvent> firstLegList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);

		for (InvestmentSecurityEvent firstLeg : CollectionUtils.getIterable(firstLegList)) {
			T rollupEvent = BeanUtils.newInstance(clazz);
			rollupEvent.setId(firstLeg.getId());
			rollupEvent.setEventLeg(firstLegType, firstLeg);
			rollupEvent.setSecurity(firstLeg.getSecurity());
			result.add(rollupEvent);
		}

		// match and populate remaining events: error on mismatch
		for (int i = 1; i < baseEvent.getEventLegCount(); i++) {
			String nextLegType = baseEvent.getEventLegTypes()[i];
			searchForm = new InvestmentSecurityEventSearchForm();
			searchForm.setSecurityId(securityId);
			searchForm.setTypeName(nextLegType);
			List<InvestmentSecurityEvent> nextLegList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);

			for (InvestmentSecurityEvent nextLeg : CollectionUtils.getIterable(nextLegList)) {
				T matchingEvent = null;
				for (T rollupEvent : CollectionUtils.getIterable(result)) {
					if (rollupEvent.getEventLeg(firstLegType).getEventDate().equals(nextLeg.getEventDate())) {
						matchingEvent = rollupEvent;
						break;
					}
				}
				if (matchingEvent == null) {
					throw new ValidationException("Cannot find matching " + firstLegType + " for " + nextLeg.getLabel());
				}
				ValidationUtils.assertNull(matchingEvent.getEventLeg(nextLegType), "Cannot have more than one " + nextLegType + ". Second: " + nextLeg.getLabel());
				matchingEvent.setEventLeg(nextLegType, nextLeg);
			}
		}

		// populate other fields
		for (T rollupEvent : CollectionUtils.getIterable(result)) {
			populatePayments(rollupEvent);
		}

		return result;
	}


	@Override
	public T getAccountingSecurityEventRollup(Class<T> clazz, int securityEventId, boolean populatePayments) {
		T rollupEvent = BeanUtils.newInstance(clazz);
		rollupEvent.setId(securityEventId);

		InvestmentSecurityEvent securityEvent = getInvestmentSecurityEventService().getInvestmentSecurityEvent(securityEventId);
		ValidationUtils.assertNotNull(securityEvent, "Cannot find InvestmentSecurityEvent with id = " + rollupEvent.getId());

		for (String eventType : rollupEvent.getEventLegTypes()) {
			if (eventType.equals(securityEvent.getType().getName())) {
				rollupEvent.setEventLeg(eventType, securityEvent);
			}
			// some events might be optional: Dividend Leg Payment does not apply to Index TRS
			else if (getInvestmentSecurityEventService().isInvestmentSecurityEventTypeAllowedForSecurity(securityEvent.getSecurity().getId(), eventType)) {
				rollupEvent.setEventLeg(eventType, getMatchingLegEvent(securityEvent, eventType));
			}
		}

		rollupEvent.setSecurity(securityEvent.getSecurity());
		if (populatePayments) {
			populatePayments(rollupEvent);
		}
		return rollupEvent;
	}


	private InvestmentSecurityEvent getMatchingLegEvent(InvestmentSecurityEvent securityEvent, String eventTypeName) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(securityEvent.getSecurity().getId());
		searchForm.setEventDate(securityEvent.getEventDate());
		searchForm.setTypeName(eventTypeName);
		List<InvestmentSecurityEvent> securityEventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);

		ValidationUtils.assertNotEmpty(securityEventList, "Cannot find '" + eventTypeName + " for " + securityEvent.getSecurity().getLabel() + " on " + DateUtils.fromDateShort(securityEvent.getEventDate()));
		ValidationUtils.assertTrue(securityEventList.size() == 1, "Cannot have more than one '" + eventTypeName + " for " + securityEvent.getSecurity().getLabel() + " on " + DateUtils.fromDateShort(securityEvent.getEventDate()));

		return securityEventList.get(0);
	}


	@Override
	@Transactional
	public void saveAccountingSecurityEventRollup(AccountingSecurityEventRollup rollupEvent) {
		for (String eventType : rollupEvent.getEventLegTypes()) {
			InvestmentSecurityEvent event = rollupEvent.getEventLeg(eventType);
			if (event != null) {
				if (event.isNewBean()) {
					event.setSecurity(rollupEvent.getSecurity());
					event.setType(getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(eventType));
				}
				rollupEvent.setEventLeg(eventType, getInvestmentSecurityEventService().saveInvestmentSecurityEvent(event));
			}
		}

		// update payment info on the bean based on changes
		populatePayments(rollupEvent);
	}


	@Override
	@Transactional
	public void deleteAccountingSecurityEventRollup(AccountingSecurityEventRollup rollupEvent) {
		for (String eventType : rollupEvent.getEventLegTypes()) {
			InvestmentSecurityEvent event = rollupEvent.getEventLeg(eventType);
			if (event != null) {
				getInvestmentSecurityEventService().deleteInvestmentSecurityEvent(event.getId());
			}
		}
	}


	private void populatePayments(AccountingSecurityEventRollup rollupEvent) {
		// populate payments from existing journals, or previewed versions (not saved in database yet)
		for (String eventType : rollupEvent.getEventLegTypes()) {
			List<AccountingEventJournalDetail> detailList = null;
			InvestmentSecurityEvent event = rollupEvent.getEventLeg(eventType);
			if (event != null && !event.isNewBean()) {
				AccountingEventJournalDetailSearchForm searchForm = new AccountingEventJournalDetailSearchForm();
				searchForm.setSecurityEventId(event.getId());
				detailList = getAccountingEventJournalService().getAccountingEventJournalDetailList(searchForm);
				if (CollectionUtils.isEmpty(detailList)) {
					// Generate Preview Details and use that - set "Preview" flag on
					rollupEvent.setPaymentPreview(true);
					EventJournalGeneratorCommand command = EventJournalGeneratorCommand.ofEvent(event.getId());
					detailList = getAccountingEventJournalGeneratorService().getAccountingEventJournalListForPreview(command);
					if (!StringUtils.isEmpty(command.getResultMessages())) {
						if (rollupEvent.getPreviewSkipMessage() == null) {
							rollupEvent.setPreviewSkipMessage(command.getResultMessages());
						}
						else {
							rollupEvent.setPreviewSkipMessage(rollupEvent.getPreviewSkipMessage() + "<br/>" + command.getResultMessages());
						}
					}
				}
				else if (rollupEvent.getSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
					boolean missingReversal = false;
					for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(detailList)) {
						if (detail.getJournal().getAccrualReversalBookingDate() == null) {
							missingReversal = true;
							break;
						}
					}
					if (missingReversal) {
						rollupEvent.setPaymentPreview(true);
						String message = String.format("The accrual reversals have not been booked for [%s].", eventType);
						if (rollupEvent.getPreviewSkipMessage() == null) {
							rollupEvent.setPreviewSkipMessage(message);
						}
						else {
							rollupEvent.setPreviewSkipMessage(rollupEvent.getPreviewSkipMessage() + "<br/>" + message);
						}
					}
				}
			}

			if (!CollectionUtils.isEmpty(detailList)) {
				if (rollupEvent.getFirstEventJournalDetailId() == null) {
					rollupEvent.setFirstEventJournalDetailId(detailList.get(0).getId());
				}
				rollupEvent.setEventPayment(eventType, CoreMathUtils.sumProperty(detailList, AccountingEventJournalDetail::getTransactionAmount));
				rollupEvent.setEventPaymentBase(eventType, CoreMathUtils.sumProperty(detailList, detail -> {
					InvestmentNotionalCalculatorTypes calculator = InvestmentCalculatorUtils.getNotionalCalculator(detail.getJournal().getSecurityEvent().getSettlementCurrency());
					return calculator.round(MathUtils.multiply(detail.getExchangeRateToBase(), detail.getTransactionAmount()));
				}));
			}
		}
	}


	/////////////////////////////////////////////////////////////////
	//////////           Getter & Setter Methods         ////////////
	/////////////////////////////////////////////////////////////////


	public AccountingEventJournalGeneratorService getAccountingEventJournalGeneratorService() {
		return this.accountingEventJournalGeneratorService;
	}


	public void setAccountingEventJournalGeneratorService(AccountingEventJournalGeneratorService accountingEventJournalGeneratorService) {
		this.accountingEventJournalGeneratorService = accountingEventJournalGeneratorService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}
}
