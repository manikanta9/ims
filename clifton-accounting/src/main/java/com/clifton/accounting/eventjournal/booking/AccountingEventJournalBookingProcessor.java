package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.gl.booking.processor.TypeBasedRulesBookingProcessor;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalStatus;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingEventJournalBookingProcessor</code> class is a booking processor for accounting
 * journals of type "Event Journal".  Event journals generate transaction sets for lots that are open on the event day.
 * <p>
 * Note:
 * Some event journals support 2 bookings (create two separate accounting journals). If accrualReversalAccount is defined
 * for the journal type, then the first journal includes accrued entries and the second one reverses accrued entries
 * and replaces them with the real once: accrue Dividend Receivable and later reverse it and replace with Cash.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalBookingProcessor extends TypeBasedRulesBookingProcessor<AccountingEventJournal> {

	public static final String JOURNAL_NAME = "Event Journal";

	private AccountingEventJournalService accountingEventJournalService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJournalTypeName() {
		return JOURNAL_NAME;
	}


	@Override
	public AccountingEventJournal getBookableEntity(Serializable id) {
		return getAccountingEventJournalService().getAccountingEventJournal((Integer) id);
	}


	@Override
	protected String getBookingEntityType(AccountingEventJournal journal) {
		String eventJournalTypeName = journal.getJournalType().getName();
		InvestmentSecurityEventType eventType = journal.getJournalType().getEventType();

		// Factor Changes and Credit Events are a special type with special handling
		if (InvestmentSecurityEventType.FACTOR_CHANGE.equals(eventType.getName())) {
			return (journal.getBookingDate() == null) ? eventJournalTypeName : (eventJournalTypeName + "-AccrualReversal");
		}
		if (InvestmentSecurityEventType.CREDIT_EVENT.equals(eventType.getName())) {
			return (journal.getBookingDate() == null) ? InvestmentSecurityEventType.CREDIT_EVENT : (InvestmentSecurityEventType.CREDIT_EVENT + "-AccrualReversal");
		}

		if (eventType.isBeforeSameAsAfter() && !eventType.isNewPositionCreated() && !eventType.isNewSecurityCreated()) {
			// payments events
			String rulesSuffix = "-SameDay";
			if (journal.getSecurityEvent().isPaymentDelayed()) {
				rulesSuffix = (journal.getBookingDate() == null) ? "-Accrual" : "-AccrualReversal";
			}

			String rulesPrefix = "Payment";
			// check for event specific overrides
			if (!CollectionUtils.isEmpty(getBookingRulesMap().get(eventJournalTypeName + rulesSuffix))) {
				rulesPrefix = eventJournalTypeName;
			}
			return rulesPrefix + rulesSuffix;
		}

		// check for investment security specific overrides first
		InvestmentSecurity security = journal.getSecurityEvent().getSecurity();
		String securitySpecificKey = getBookingRulesKeyOverride(eventJournalTypeName, security);
		if (securitySpecificKey != null && !securitySpecificKey.equals(eventJournalTypeName)) {
			return securitySpecificKey;
		}

		if (journal.getSecurityEvent().isPaymentDelayed()) {
			return eventJournalTypeName + ((journal.getBookingDate() == null) ? "-Accrual" : "-AccrualReversal");
		}

		return eventJournalTypeName;
	}


	@Override
	public SimpleBookingSession<AccountingEventJournal> newBookingSession(AccountingJournal journal, AccountingEventJournal eventJournal) {
		if (eventJournal == null) {
			eventJournal = getBookableEntity(journal.getFkFieldId());
			ValidationUtils.assertNotNull(eventJournal, "Cannot find event journal with id = " + journal.getFkFieldId());
		}

		String description = eventJournal.getLabel();
		if (eventJournal.getJournalType().getAccrualReversalAccount() != null) {
			if (eventJournal.getBookingDate() == null) {
				description += " [Accruing]";
			}
			else {
				// booking reversal of previous accrual: find original journal
				AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
				searchForm.setFkFieldId(eventJournal.getId());
				searchForm.setJournalTypeId(journal.getJournalType().getId());
				searchForm.setExcludeJournalStatusName(AccountingJournalStatus.STATUS_DELETED);
				List<AccountingJournal> parentList = getAccountingJournalService().getAccountingJournalList(searchForm);
				if (CollectionUtils.isEmpty(parentList)) {
					throw new ValidationException("Cannot find original accrual journal for event journal id = " + eventJournal.getId());
				}
				if (CollectionUtils.getSize(parentList) > 1) {
					throw new ValidationException("Cannot have more than one journal when generating Accrual Reversal journal for event journal id = " + eventJournal.getId());
				}
				journal.setParent(getAccountingJournalService().populateJournal(CollectionUtils.getOnlyElement(parentList)));
				description += " [Accrual Reversal]";
			}
		}
		journal.setDescription(description);

		return new SimpleBookingSession<>(journal, eventJournal);
	}


	@Override
	public AccountingEventJournal markBooked(AccountingEventJournal eventJournal) {
		Date bookingDate = new Date();
		if (eventJournal.getBookingDate() == null) {
			if (bookingDate.before(eventJournal.getSecurityEvent().getDayBeforeExDate())) {
				throw new ValidationException("Cannot book journal '" + eventJournal.getLabel() + "' before Ex Date of " + DateUtils.fromDateShort(eventJournal.getSecurityEvent().getExDate()));
			}
			// first time booking: if accrual is supported, leave it for the second booking
			eventJournal.setBookingDate(bookingDate);

			if (!eventJournal.getSecurityEvent().isPaymentDelayed() || eventJournal.getJournalType().getAccrualReversalAccount() == null) {
				eventJournal.setAccrualReversalCombined(true);
			}
			if (eventJournal.isAccrualReversalCombined()) {
				eventJournal.setAccrualReversalBookingDate(bookingDate);
			}
		}
		else {
			bookingDate = getEventJournalReversalPostingDate(eventJournal, bookingDate);
			if (bookingDate.before(eventJournal.getSecurityEvent().getEventDate())) {
				throw new ValidationException("Cannot book accrual reversal for journal '" + eventJournal.getLabel() + "' before Event Date of "
						+ DateUtils.fromDateShort(eventJournal.getSecurityEvent().getEventDate()));
			}
			// second time booking: can only be accrual reversal
			ValidationUtils.assertNull(eventJournal.getAccrualReversalBookingDate(), "Cannot book accrual reversal for Event Journal with id = " + eventJournal.getId()
					+ " because it already has accrualReversalBookingDate set to " + eventJournal.getAccrualReversalBookingDate());
			eventJournal.setAccrualReversalBookingDate(bookingDate);
		}
		return getAccountingEventJournalService().saveAccountingEventJournal(eventJournal);
	}


	private Date getEventJournalReversalPostingDate(AccountingEventJournal eventJournal, Date postingDate) {
		InvestmentSecurityEvent securityEvent = eventJournal.getSecurityEvent();
		if (securityEvent.getSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
			// should never get through the pre-check validation with the actual settlement date
			ValidationUtils.assertNotNull(securityEvent.getActualSettlementDate(), "Cannot book accrual reversal for journal '" + eventJournal.getLabel() + " the actual settlement date is required.");
			return securityEvent.getActualSettlementDate();
		}
		return postingDate;
	}


	@Override
	@Transactional
	public AccountingEventJournal markUnbooked(int eventJournalId) {
		AccountingEventJournal eventJournal = getBookableEntity(eventJournalId);

		if (eventJournal.isAccrualReversalCombined()) {
			// only one journal is booked
			eventJournal.setBookingDate(null);
			eventJournal.setAccrualReversalBookingDate(null);
		}
		else {
			// 2 journals are booked: unbook the latest
			if (eventJournal.getAccrualReversalBookingDate() != null) {
				eventJournal.setAccrualReversalBookingDate(null);
			}
			else {
				eventJournal.setBookingDate(null);
			}
		}
		return getAccountingEventJournalService().saveAccountingEventJournal(eventJournal);
	}


	@Override
	public void deleteSourceEntity(int eventJournalId) {
		// cannot delete an entity that's marked as booked (clear booked status first)
		getAccountingEventJournalService().deleteAccountingEventJournal(eventJournalId);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}
}
