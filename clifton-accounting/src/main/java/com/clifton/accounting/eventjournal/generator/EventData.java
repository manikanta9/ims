package com.clifton.accounting.eventjournal.generator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * The EventData class is a holder for {@link InvestmentSecurityEvent} and {@link InvestmentSecurityEventPayout} objects.
 * It contains the logic to COALESCE appropriate Payout and Event fields and is intended for use during event journal generation and booking.
 *
 * @author vgomelsky
 */
public class EventData {

	private InvestmentSecurityEvent event;
	private InvestmentSecurityEventPayout payout;

	/**
	 * Some events such as Scrips, DRIPS, and mergers have multiple payouts for an election. This  represents payouts applicable for an event election for a client.
	 * In most cases, each payout can be processed independently. For some events, a payout's behavior differs based the existence of sibling payouts.
	 * For Mergers, a security payout's new cost basis differs when there are multiple payouts.
	 */
	private List<InvestmentSecurityEventPayout> electionPayoutList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructs new {@link EventData} for the specified event.  Use this method only for events without payouts.
	 */
	public static EventData forEvent(InvestmentSecurityEvent event) {
		EventData eventData = new EventData();
		eventData.event = event;
		return eventData;
	}


	/**
	 * Constructs new {@link EventData} for the specified payout and its corresponding event.
	 */
	public static EventData forEventPayout(InvestmentSecurityEventPayout payout) {
		EventData eventData = forEvent(payout.getSecurityEvent());
		eventData.payout = payout;
		return eventData;
	}


	/**
	 * Constructs new {@link EventData} for either the specified payout or the specified event.
	 */
	public static EventData forEventPayoutOrEvent(InvestmentSecurityEventPayout payout, InvestmentSecurityEvent event) {
		return (payout == null) ? forEvent(event) : forEventPayout(payout);
	}


	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		else if (!(object instanceof EventData)) {
			return false;
		}
		EventData toCompare = (EventData) object;
		return CompareUtils.isEqual(getPayout(), toCompare.getPayout()) && CompareUtils.isEqual(getEvent(), toCompare.getEvent());
	}


	@Override
	public int hashCode() {
		return Objects.hash(getPayout(), getEvent());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEvent getEvent() {
		return this.event;
	}


	public String getEventTypeName() {
		return getEvent().getType().getName();
	}


	public InvestmentSecurityEventPayout getPayout() {
		return this.payout;
	}


	public boolean isPayoutDefined() {
		return (getPayout() != null);
	}


	public boolean isEventValueInEventCurrencyUnits() {
		return isPayoutDefined() ? getPayout().getPayoutType().isEventValueInEventCurrencyUnits() : getEvent().getType().isEventValueInEventCurrencyUnits();
	}


	public boolean isEventOfEventType(String eventTypeName) {
		return eventTypeName.equals(getEvent().getType().getName());
	}


	/**
	 * Events usually settle in Currency Denomination of event's security.
	 * However, if event type allows settlement in a different currency and it is specified, then that currency will be returned.
	 */
	public InvestmentSecurity getSettlementCurrency() {
		if (isPayoutDefined() && getPayout().getPayoutType().isAdditionalSecurityCurrency()) {
			return getPayout().getPayoutSecurity();
		}

		return getEvent().getSettlementCurrency();
	}


	public InvestmentSecurity getSecurityCurrencyDenomination() {
		return getSecurity().getInstrument().getTradingCurrency();
	}


	public InvestmentSecurity getSecurity() {
		return getEvent().getSecurity();
	}


	public InvestmentSecurity getAdditionalSecurity() {
		return isPayoutDefined() ? getPayout().getPayoutSecurity() : getEvent().getAdditionalSecurity();
	}


	public InvestmentSecurity getPayoutSecurity() {
		return getPayout().getPayoutSecurity();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getBeforeEventValue() {
		return isPayoutDefined() ? getPayout().getBeforeEventValue() : getEvent().getBeforeEventValue();
	}


	public BigDecimal getAfterEventValue() {
		return isPayoutDefined() ? getPayout().getAfterEventValue() : getEvent().getAfterEventValue();
	}


	public BigDecimal getAdditionalEventValue() {
		return getEvent().getAdditionalEventValue();
	}


	public BigDecimal getAdditionalPayoutValue() {
		return getPayout().getAdditionalPayoutValue();
	}


	public BigDecimal getAdditionalValue() {
		return isPayoutDefined() ? getAdditionalPayoutValue() : getAdditionalEventValue();

	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getDeclareDate() {
		return getEvent().getDeclareDate();
	}


	public Date getAccrualStartDate() {
		return getEvent().getAccrualStartDate();
	}


	public Date getAccrualEndDate() {
		return getEvent().getAccrualEndDate();
	}


	public Date getExDate() {
		return getEvent().getExDate();
	}


	public Date getDayBeforeExDate() {
		return getEvent().getDayBeforeExDate();
	}


	public Date getPaymentDate() {
		return isPayoutDefined() ? ObjectUtils.coalesce(getPayout().getAdditionalPayoutDate(), getEvent().getPaymentDate()) : getEvent().getPaymentDate();
	}


	public Date getEventDate() {
		return isPayoutDefined() ? ObjectUtils.coalesce(getPayout().getAdditionalPayoutDate(), getEvent().getEventDate()) : getEvent().getEventDate();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return getEvent().getLabel();
	}


	public String getEventDescription() {
		return getEvent().getEventDescription();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isSingleElectionPayout() {
		return CollectionUtils.getSize(getElectionPayoutList()) < 2;
	}


	public List<InvestmentSecurityEventPayout> getElectionPayoutList() {
		return this.electionPayoutList;
	}


	public void setElectionPayoutList(List<InvestmentSecurityEventPayout> electionPayoutList) {
		this.electionPayoutList = electionPayoutList;
	}
}
