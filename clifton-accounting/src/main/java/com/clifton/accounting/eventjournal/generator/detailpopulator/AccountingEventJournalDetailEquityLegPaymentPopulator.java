package com.clifton.accounting.eventjournal.generator.detailpopulator;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.ResetTypes;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.math.BigDecimal;
import java.math.RoundingMode;


/**
 * The <code>AccountingEventJournalDetailEquityLegPaymentPopulator</code> class sets TransactionAmount to the gain/loss on
 * the Notional based on price change from start to end of the period.
 * <p>
 * Also sets TransactionPrice to the end of period price: new reset period
 * and AffectedQuantity to the quantity being closed/open.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailEquityLegPaymentPopulator implements AccountingEventJournalDetailPopulator {

	private InvestmentCalculator investmentCalculator;
	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		EventData eventData = command.getEventData();
		BigDecimal valuationEndPrice = eventData.getAfterEventValue();
		if (MathUtils.isNullOrZero(valuationEndPrice)) {
			command.setSkipMessage("Next Valuation Price is not populated.");
			return true; // skip payments when price is not available yet
		}

		// NOTE: Payment = ROUND(Quantity * (End Price - Start Price)) as opposed to ROUND(Quantity * End Price) - ROUND(Quantity * Start Price)
		// This seems to be the convention used by counter parties and it's necessary to avoid penny rounding
		BigDecimal valuationStartPrice;
		if (DateUtils.compare(eventData.getDeclareDate(), command.getTransactionDate(), false) < 0) {
			valuationStartPrice = command.getOpeningPrice();
		}
		else {
			valuationStartPrice = eventData.getBeforeEventValue();
		}
		if (valuationStartPrice == null) {
			valuationStartPrice = command.getOpeningPrice();
		}
		BigDecimal priceChange = MathUtils.subtract(valuationEndPrice, valuationStartPrice);
		BigDecimal paymentAmount = getInvestmentCalculator().calculateNotional(eventData.getSecurity(), priceChange, command.getRemainingQuantity(), BigDecimal.ONE);
		BigDecimal newPrice = eventData.getAfterEventValue();
		BigDecimal closingUnits = command.getRemainingQuantity();
		BigDecimal newUnits = closingUnits;

		// check if something needs to be adjusted based on reset type
		String resetTypeValue = (String) getSystemColumnValueHandler().getSystemColumnValueForEntity(eventData.getSecurity(), InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME,
				InvestmentSecurity.CUSTOM_FIELD_RESET_TYPE, false);
		if (resetTypeValue != null) {
			ResetTypes resetType = ResetTypes.valueOf(resetTypeValue.toUpperCase());
			if (ResetTypes.KEEP_QUANTITY == resetType) {
				// default calculation: don't need to do anything else
			}
			else if (ResetTypes.KEEP_NOTIONAL == resetType) {
				newUnits = MathUtils.divide(command.getRemainingCostBasis(), MathUtils.multiply(newPrice, eventData.getSecurity().getPriceMultiplier()));
				// verify that the calculation was accurate while keeping smallest precision that results in accurate notional
				int precision = 10;
				while (precision >= 0) {
					BigDecimal adjustedUnits = newUnits.setScale(precision, RoundingMode.HALF_UP);
					BigDecimal newNotional = getInvestmentCalculator().calculateNotional(eventData.getSecurity(), newPrice, adjustedUnits, BigDecimal.ONE);
					if (!MathUtils.isEqual(newNotional, command.getRemainingCostBasis())) {
						if (precision == 10) {
							throw new ValidationException("New notional " + newNotional + " calculated using new units " + newUnits + " does not match old notional "
									+ command.getRemainingCostBasis());
						}
						break;
					}
					precision--;
				}
				newUnits = newUnits.setScale(precision + 1, RoundingMode.HALF_UP);
			}
			else {
				throw new ValidationException("Unsupported Reset Type: " + resetTypeValue + " for " + eventData.getSecurity().getSymbol());
			}
		}

		AccountingEventJournalDetail detail = command.getJournalDetail();
		detail.setTransactionAmount(paymentAmount);
		detail.setTransactionPrice(newPrice);
		detail.setAffectedQuantity(closingUnits);
		detail.setAdditionalAmount(newUnits);

		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
