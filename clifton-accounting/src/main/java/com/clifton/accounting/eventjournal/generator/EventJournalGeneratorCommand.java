package com.clifton.accounting.eventjournal.generator;


import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>EventJournalGeneratorCommand</code> contains properties that
 * can be set for generating/previewing event journals
 *
 * @author manderson
 */
public class EventJournalGeneratorCommand {

	private Short eventTypeId;
	private Integer eventId;

	/**
	 * Used only when eventId is populated - i.e. for one event
	 * Will use calculated values for initial generation and then override the amount
	 * For cases where there are multiple details, the override amount - calculated amount difference will be applied proportionally
	 * i.e. : if one detail has $10 and second detail has $20
	 * and total payment override is $30.03
	 * result will be 10.01 and 20.02
	 */
	private BigDecimal amountOverride;

	private Date eventDate;

	/**
	 * Date used to determine whether accruals and reversals should be generated. For example, generating event journals on Ex Date will only
	 * generate accrual journals whereas generating event journals on or after Event Date will generate both accrual and reversal journals.
	 */
	private Date generationDate;

	private Integer securityId;
	private Short investmentGroupId;

	private Integer clientAccountId;

	private AccountingEventJournalGeneratorTypes generatorType;

	/**
	 * When set to true, will generate a separate journal for each holding account. Otherwise, generates a single journal across all accounts.
	 * NOTE: default to true to make it easier to manager high volumes of accounts for the same event: can unbook and adjust a single account.
	 */
	private boolean separateJournalPerHoldingAccount = true;

	private boolean skipJournalsWithExDateAfterTomorrow;

	/**
	 * Populated via preview generation to indicate why events may not have
	 * been generated - i.e. already generated, no accounts with open positions, missing information, etc.
	 */
	private String resultMessages;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static EventJournalGeneratorCommand ofEvent(Integer eventId) {
		EventJournalGeneratorCommand result = new EventJournalGeneratorCommand();
		result.setEventId(eventId);
		return result;
	}


	public static EventJournalGeneratorCommand ofEvent(Integer eventId, AccountingEventJournalGeneratorTypes generatorType) {
		EventJournalGeneratorCommand result = ofEvent(eventId);
		result.setGeneratorType(generatorType);
		return result;
	}


	public static EventJournalGeneratorCommand ofEventForClientAccount(Integer eventId, Integer clientAccountId, AccountingEventJournalGeneratorTypes generatorType) {
		EventJournalGeneratorCommand result = ofEvent(eventId);
		result.setClientAccountId(clientAccountId);
		result.setGeneratorType(generatorType);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addResultMessage(String message) {
		if (this.resultMessages == null) {
			this.resultMessages = message;
		}
		else {
			this.resultMessages += "<br/>" + message;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}


	public Integer getEventId() {
		return this.eventId;
	}


	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Date getGenerationDate() {
		return this.generationDate;
	}


	public void setGenerationDate(Date generationDate) {
		this.generationDate = generationDate;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public AccountingEventJournalGeneratorTypes getGeneratorType() {
		return this.generatorType;
	}


	public void setGeneratorType(AccountingEventJournalGeneratorTypes generatorType) {
		this.generatorType = generatorType;
	}


	public boolean isSeparateJournalPerHoldingAccount() {
		return this.separateJournalPerHoldingAccount;
	}


	public void setSeparateJournalPerHoldingAccount(boolean separateJournalPerHoldingAccount) {
		this.separateJournalPerHoldingAccount = separateJournalPerHoldingAccount;
	}


	public String getResultMessages() {
		return this.resultMessages;
	}


	public void setResultMessages(String resultMessages) {
		this.resultMessages = resultMessages;
	}


	public BigDecimal getAmountOverride() {
		return this.amountOverride;
	}


	public void setAmountOverride(BigDecimal amountOverride) {
		this.amountOverride = amountOverride;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public boolean isSkipJournalsWithExDateAfterTomorrow() {
		return this.skipJournalsWithExDateAfterTomorrow;
	}


	public void setSkipJournalsWithExDateAfterTomorrow(boolean skipJournalsWithExDateAfterTomorrow) {
		this.skipJournalsWithExDateAfterTomorrow = skipJournalsWithExDateAfterTomorrow;
	}
}
