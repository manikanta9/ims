package com.clifton.accounting.eventjournal.generator.detailpopulator;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.marketdata.MarketDataRetriever;

import java.math.BigDecimal;


/**
 * The AccountingEventJournalDetailScripDividendPopulator class calculates opening lot Quantity, Price, Cost Basis, and Fraction to Close.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailScripDividendPopulator implements AccountingEventJournalDetailPopulator {

	private InvestmentCalculator investmentCalculator;

	private MarketDataRetriever marketDataRetriever;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		EventData eventData = command.getEventData();
		ValidationUtils.assertNotNull(eventData.getAdditionalSecurity(), "Missing required Scrip Security for: " + eventData.getLabel());
		ValidationUtils.assertNotNull(eventData.getPayout(), "Missing required Scrip payout for: " + eventData.getLabel());

		BigDecimal additionalQuantity = MathUtils.multiply(command.getRemainingQuantity(), eventData.getAfterEventValue(), DataTypes.QUANTITY.getPrecision()).stripTrailingZeros();

		AccountingEventJournalDetail.Scrip scrip = command.getJournalDetail().forScrip();
		scrip.setOpeningQuantity(additionalQuantity)
				.setOpeningPrice(getOpeningPrice(eventData))
				.setOpeningCostBasis(getInvestmentCalculator().calculateNotional(eventData.getSecurity(), scrip.getOpeningPrice(), scrip.getOpeningQuantity(), BigDecimal.ONE))
				.setPaymentAmount(scrip.getOpeningCostBasis())
				.setFractionToClose(MathUtils.subtract(scrip.getOpeningQuantity(), MathUtils.round(scrip.getOpeningQuantity(), 0, BigDecimal.ROUND_DOWN)));


		return false;
	}


	private BigDecimal getOpeningPrice(EventData eventData) {
		BigDecimal payoutPrice = eventData.getAdditionalPayoutValue();
		if (payoutPrice == null) {
			// Use Ex Date to look up price as an estimate when the date is not defined on the payout. Event date could be in the future for accruals, which will always fail.
			payoutPrice = getMarketDataRetriever().getPrice(eventData.getSecurity(), eventData.getExDate(), true, "Payout Security:");
		}
		return payoutPrice;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
