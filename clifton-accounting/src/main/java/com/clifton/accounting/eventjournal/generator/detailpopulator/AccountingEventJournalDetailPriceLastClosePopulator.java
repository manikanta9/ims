package com.clifton.accounting.eventjournal.generator.detailpopulator;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingEventJournalDetailPriceLastClosePopulator</code> class populates TransactionPrice with the price of the last
 * lot "closed" for event's security in the corresponding client/holding account. It is applied to securities that close on maturity only.
 * <p>
 * If position was not fully "closed" (remaining quantity is not zero)
 * <p>
 * For example, Currency Forwards and LME's (securities that cannot be closed and stay on books until maturity).
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailPriceLastClosePopulator implements AccountingEventJournalDetailPopulator {

	private AccountingTransactionService accountingTransactionService;
	private MarketDataRetriever marketDataRetriever;

	private boolean maturityEvent;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		// find the last price that was used to purchase/sell a security
		BigDecimal price = BigDecimal.ZERO;
		EventData eventData = command.getEventData();
		InvestmentSecurity security = eventData.getSecurity();
		AccountingEventJournalDetail detail = command.getJournalDetail();
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setClientInvestmentAccountId(detail.getAccountingTransaction().getClientInvestmentAccount().getId());
		searchForm.setHoldingInvestmentAccountId(detail.getAccountingTransaction().getHoldingInvestmentAccount().getId()); // need this for Currency Forwards that use different counterparties
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);
		searchForm.setOpening(true);
		searchForm.setMaxTransactionDate(eventData.getEventDate());
		searchForm.setOrderBy("transactionDate:DESC");
		List<AccountingTransaction> transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);

		BigDecimal remainingQty = null;
		for (AccountingTransaction tran : CollectionUtils.getIterable(transactionList)) {
			remainingQty = MathUtils.add(remainingQty, tran.getQuantityNormalized());
		}

		if (MathUtils.isNullOrZero(remainingQty)) {
			// fully closed position: use last transaction price
			AccountingTransaction lastTransaction = CollectionUtils.getFirstElement(transactionList);
			if (lastTransaction != null && lastTransaction.getPrice() != null) {
				price = lastTransaction.getPrice();
			}
		}
		else {
			// still have open position: use Fixing Date price
			Date today = DateUtils.clearTime(new Date());
			if (DateUtils.compare(today, eventData.getExDate(), true) < 0) {
				// Ex Date has not occurred yet: lookup latest available price (FORECAST)
				price = getMarketDataRetriever().getPriceFlexible(security, today, true);
			}
			else {
				// Ex Date occurred: lookup precise price (ACTUAL EVENT)
				price = getMarketDataRetriever().getPrice(security, security.getEndDate(), true, "Event Journal with Open Position: ");
			}
		}

		detail.setTransactionPrice(price);

		if (isMaturityEvent()) {
			detail.forMaturity().setOpeningPrice(command.getOpeningPrice());

			if (InvestmentUtils.isInstrumentOfType(security.getInstrument(), InvestmentType.FORWARDS)) {
				// forward maturities have 2 currencies: need 2 fx to base: 1 is from currency denomination of forward and 2 is from its underlying currency
				AccountingEventJournalDetail.ForwardMaturity forwardMaturity = detail.forForwardMaturity();
				BigDecimal fxRate2 = BigDecimal.ONE;
				InvestmentSecurity fromCurrency = forwardMaturity.getCurrency2();
				InvestmentSecurity toCurrency = InvestmentUtils.getClientAccountBaseCurrency(detail.getAccountingTransaction());
				if (!fromCurrency.equals(toCurrency)) {
					// back into FX 2 from Maturity Price (FX at Maturity) and, if necessary, use cross-rate for 3 currencies
					fxRate2 = InvestmentCalculatorUtils.calculateExchangeRateFromForwardPrice(security, forwardMaturity.getMaturityPrice());
					fxRate2 = MathUtils.multiply(fxRate2, forwardMaturity.getExchangeRate1(), DataTypes.EXCHANGE_RATE.getPrecision());
				}
				else {
					// Currency 2 is the same as Base Currency and Currency 1 is different: recalculate Currency 1 FX Rate using Price
					BigDecimal fxRate = InvestmentCalculatorUtils.calculateExchangeRateFromForwardPrice(security, forwardMaturity.getMaturityPrice());
					// currency convention is the opposite: reverse underlying and currency denomination
					forwardMaturity.setExchangeRate1(MathUtils.divide(BigDecimal.ONE, fxRate, DataTypes.EXCHANGE_RATE.getPrecision()));
				}
				forwardMaturity.setExchangeRate2(fxRate2);
			}
		}

		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public boolean isMaturityEvent() {
		return this.maturityEvent;
	}


	public void setMaturityEvent(boolean maturityEvent) {
		this.maturityEvent = maturityEvent;
	}
}
