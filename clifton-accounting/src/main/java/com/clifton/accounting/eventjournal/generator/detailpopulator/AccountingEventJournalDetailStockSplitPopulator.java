package com.clifton.accounting.eventjournal.generator.detailpopulator;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The AccountingEventJournalDetailStockSplitPopulator class calculates and sets closing and opening quantity, price and cost basis for each lot to be split.
 * The logic also handles fractional shares which will be reduced to rounded shares, if necessary, by the corresponding booking rule.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailStockSplitPopulator implements AccountingEventJournalDetailPopulator {


	@Override
	public boolean populate(PopulatorCommand command) {
		AccountingEventJournalDetail.StockSplit stockSplit = command.getJournalDetail().forStockSplit();

		EventData eventData = command.getEventData();
		InvestmentNotionalCalculatorTypes notionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(eventData.getSecurity());

		stockSplit.setClosingQuantity(command.getRemainingQuantity())
				.setClosingCostBasis(command.getRemainingCostBasis())
				.setClosingPrice(command.getOpeningPrice())
				.setOpeningQuantity(getOpeningQuantityRounded(stockSplit.getClosingQuantity(), eventData))  // booking logic will handle fractional shares
				.setOpeningCostBasis(stockSplit.getClosingCostBasis())
				.setOpeningPrice(notionalCalculator.calculatePriceFromNotional(stockSplit.getOpeningQuantity(), stockSplit.getOpeningCostBasis(), eventData.getSecurity().getPriceMultiplier()))
				.setFractionToClose(MathUtils.subtract(stockSplit.getOpeningQuantity(), MathUtils.round(stockSplit.getOpeningQuantity(), 0, BigDecimal.ROUND_DOWN)));

		return MathUtils.isEqual(stockSplit.getClosingQuantity(), stockSplit.getOpeningQuantity());
	}


	private BigDecimal getOpeningQuantityRounded(BigDecimal closingQuantity, EventData eventData) {
		// divide in the end to avoid division rounding problems (3 to 1 Stock Split for 300 shares); rounding will be done via the detail rounder
		return MathUtils.divide(MathUtils.multiply(closingQuantity, eventData.getAfterEventValue()), eventData.getBeforeEventValue(), DataTypes.QUANTITY.getPrecision()).stripTrailingZeros();
	}
}
