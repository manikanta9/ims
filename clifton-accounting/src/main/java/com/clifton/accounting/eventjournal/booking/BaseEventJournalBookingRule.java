package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.ExchangeRateDateSelectors;
import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCommonBookingRule;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BaseEventJournalBookingRule</code> class should be extended by all event journal related
 * booking rules.
 *
 * @author vgomelsky
 */
public abstract class BaseEventJournalBookingRule extends AccountingCommonBookingRule<AccountingEventJournal> implements AccountingBookingRule<AccountingEventJournal> {

	private AccountingPeriodService accountingPeriodService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private SystemSchemaService systemSchemaService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected SystemTable getAccountingEventJournalDetailTable() {
		SystemTable result = getSystemSchemaService().getSystemTableByName("AccountingEventJournalDetail");
		AssertUtils.assertNotNull(result, "Cannot find system table with name: AccountingEventJournalDetail");
		return result;
	}


	/**
	 * Generates AccountingJournalDetail for the specified event journal detail.  Defaults can be modified after.
	 */
	protected AccountingJournalDetail generateAccountingJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventDetail, BigDecimal quantity, BigDecimal debitCredit, Date date) {
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(eventDetail.getId());
		detail.setSystemTable(getAccountingEventJournalDetailTable());

		detail.setParentTransaction(affectedLot);
		detail.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		detail.setAccountingAccount(affectedLot.getAccountingAccount());
		detail.setExchangeRateToBase(affectedLot.getExchangeRateToBase());

		detail.setPrice(quantity == null ? null : eventDetail.getTransactionPrice());
		detail.setQuantity(quantity);
		detail.setLocalDebitCredit(debitCredit);
		detail.setBaseDebitCredit(MathUtils.multiply(detail.getLocalDebitCredit(), affectedLot.getExchangeRateToBase(), 2));
		detail.setPositionCostBasis(detail.getLocalDebitCredit());

		detail.setDescription(eventDetail.getLabel());

		detail.setSettlementDate(date);
		detail.setOriginalTransactionDate(date);
		detail.setTransactionDate(date);

		detail.setOpening(quantity == null);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}


	/**
	 * Creates the offsetting Cash or Currency journal detail for the specified journal detail.
	 * Usually a payment for or from a position, commission, etc.
	 * <p>
	 * If the specified detail security has the same currency denomination as client account, then Cash entry is created.
	 * Otherwise, a Currency entry is created.
	 */
	protected AccountingJournalDetail createCashCurrencyRecord(AccountingJournalDetailDefinition oppositeEntry, AccountingEventJournalDetail eventDetail) {
		InvestmentSecurity currencyDenomination = oppositeEntry.getInvestmentSecurity().getInstrument().getTradingCurrency();
		return createCashCurrencyRecord(oppositeEntry, eventDetail, currencyDenomination);
	}


	protected AccountingJournalDetail createCashCurrencyRecord(AccountingJournalDetailDefinition oppositeEntry, AccountingEventJournalDetail eventDetail, InvestmentSecurity currencyDenomination) {
		InvestmentSecurity clientBaseCurrency = InvestmentUtils.getClientAccountBaseCurrency(oppositeEntry);

		AccountingJournalDetail result = newCashCurrencyRecord(oppositeEntry);
		result.setLocalDebitCredit(MathUtils.negate(oppositeEntry.getLocalDebitCredit()));
		result.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(result.getLocalDebitCredit(), result.getExchangeRateToBase(), oppositeEntry.getClientInvestmentAccount()));

		if (clientBaseCurrency.equals(currencyDenomination)) {
			result.setAccountingAccount(getCashAccountingAccount(oppositeEntry));
			result.setInvestmentSecurity(clientBaseCurrency);
		}
		else {
			result.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
			result.setInvestmentSecurity(currencyDenomination);
		}
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();
		result.setDescription(getCashCurrencyRecordDescriptionPrefix(result) + event.getType().getName() + " of " + event.getSecurity().getSymbol());

		return result;
	}


	/**
	 * Parent Transaction for closing positions will always be the effectedLot (the lot that it closes).
	 * For opening positions, it will also be affected lot, unless re-opening is for the same security and affected lot has
	 * parent that is also opening transaction (original opening).  In that case, it will point to the original opening
	 * to simplifying lot-level gain/loss attribution to the original opening in case we had multiple splits over time for
	 * the same security and wanted to calculate cumulative gains/losses for the original lot (3 levels deep vs N levels deep).
	 * <p>
	 * NOTE: this method must be called after other fields (InvestmentSecurity) were populated for the method parameters.
	 */
	protected void populatePositionParentTransaction(AccountingJournalDetail journalDetail, AccountingTransaction affectedLot, boolean close) {
		journalDetail.setParentTransaction(affectedLot);
		AccountingTransaction parent = affectedLot.getParentTransaction();
		if (!close && parent != null && parent.isOpening() && parent.getInvestmentSecurity().equals(journalDetail.getInvestmentSecurity())) {
			journalDetail.setParentTransaction(parent); // point openings to original opening for openings of the same security (re-open on split, spin-off)
		}
	}


	/**
	 * Returns the Exchange Rate from the specified FROM to the specified TO currency using proper data source and look-up date.
	 */
	protected BigDecimal getExchangeRate(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, AccountingObjectInfo transaction, InvestmentSecurityEvent event, AccountingEventJournalType eventJournalType) {
		// it's important to use a predefined exchange rate source. It should be specified as swap field.
		ExchangeRateDateSelectors dateSelector = eventJournalType.getExchangeRateDateSelector();
		boolean flexibleLookup = (dateSelector == null) || dateSelector.isFlexibleLookupAllowed(event);
		Date fxDate = (dateSelector == null) ? event.getAccrualEndDate() : dateSelector.getExchangeRateDate(event, getCalendarBusinessDayService());
		return getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(transaction.fxSourceCompany(), !transaction.fxSourceCompanyOverridden(), fromCurrency.getSymbol(), toCurrency.getSymbol(), fxDate)
				.flexibleLookup(flexibleLookup).fallbackToDefaultDataSource(flexibleLookup));
	}


	/**
	 * Returns Transaction Date to be used in the General Ledger for the specified event journal.
	 * It's the same as Event Date when accrual == false.
	 * When accrual == true, it is the day before Ex Date except for CASH_DIVIDEND_PAYMENT when it's Ex Date.
	 * <p>
	 * NOTE: If there is at least one event detail for an account with closed period on the day before Ex Date,
	 * then accrual will start on the first day of the following month: cannot go back in time and change closed period.
	 * This may happen for delayed payment bonds when new factor is not known until we're weeks into next period.
	 */
	protected Date getTransactionDate(AccountingEventJournal eventJournal, boolean accrual) {
		InvestmentSecurityEvent event = eventJournal.getSecurityEvent();
		Date result = event.getEventDate();
		if (accrual) {
			if (InvestmentSecurityEventType.CASH_DIVIDEND_PAYMENT.equals(event.getType().getName())) {
				// market convention for Dividend Income is to use Ex Date
				result = event.getExDate();
			}
			else {
				// Interest Income, Gain/Loss from Factor Change, etc. are earned as of end of period date and Ex Date is one day after
				result = event.getDayBeforeExDate();

				// For delayed bonds, new Factor or new variable Coupon may not be known until late next month (25 day delay).
				// By the time it's known, previous Accounting Period maybe closed and we won't be able to post Receivables into previous month.
				// Identify if this is the case and "shift" Transaction Date to the first of current month (keep Original Transaction Date).
				Date now = new Date();
				if (DateUtils.getMonthOfYear(result) != DateUtils.getMonthOfYear(now)) {
					// if accrual and payment dates are in same month, then no need to check this
					if (DateUtils.getMonthOfYear(result) != DateUtils.getMonthOfYear(event.getEventDate())) {
						if (DateUtils.getDaysDifference(now, result) > 7) { // periods shouldn't be closed this soon
							for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
								if (getAccountingPeriodService().isAccountingPeriodClosed(detail.getAccountingTransaction().getClientInvestmentAccount().getId(), result)) {
									// found closed period: shift the date to first of next month
									result = DateUtils.addMonths(result, 1);
									result = DateUtils.addDays(result, 1 - DateUtils.getDayOfMonth(result));
									break;
								}
							}
						}
					}
				}
			}
		}
		return result;
	}


	/**
	 * Returns Original Transaction Date to be used in the General Ledger for the specified event.
	 * It's the same as Event Date when accrual == false.
	 * When accrual == true, it is the day before Ex Date except for CASH_DIVIDEND_PAYMENT when it's Ex Date.
	 */
	protected Date getOriginalTransactionDate(EventData eventData, boolean accrual) {
		Date result = eventData.getEventDate();
		if (accrual) {
			if (eventData.isEventOfEventType(InvestmentSecurityEventType.CASH_DIVIDEND_PAYMENT)) {
				// market convention for Dividend Income is to use Ex Date
				result = eventData.getExDate();
			}
			else if (eventData.isEventOfEventType(InvestmentSecurityEventType.RETURN_OF_CAPITAL)) {
				// use payment date for Return of Capital accrual reversal
				result = eventData.getPaymentDate();
			}
			else {
				// Interest Income, Gain/Loss from Factor Change, etc. are earned as of end of period date and Ex Date is one day after
				result = eventData.getDayBeforeExDate();
			}
		}
		return result;
	}


	protected Date getOriginalTransactionDate(InvestmentSecurityEvent event, boolean accrual) {
		return getOriginalTransactionDate(EventData.forEvent(event), accrual);
	}


	protected AccountingJournalDetail populateFrankingCreditJournalDetails(AccountingJournalDetail frankingCredit, AccountingEventJournalDetail detail) {
		InvestmentSecurityEventPayout payout = detail.getEventPayout();
		frankingCredit.setOpening(true);
		frankingCredit.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_FRANKING_CREDIT));
		frankingCredit.setPrice(payout.getAdditionalPayoutValue2());
		frankingCredit.setPositionCostBasis(BigDecimal.ZERO);
		frankingCredit.setSettlementDate(detail.getEventDate());
		frankingCredit.setDescription("Franking Tax Credit from " + payout.getSecurityEvent().getType().getName() + " for " + payout.getSecurityEvent().getSecurity().getSymbol() +
				" on " + DateUtils.fromDateShort(detail.getEventDate()));
		return frankingCredit;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
