package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The <code>AccountingFactorChangeForAssumedSellBookingRule</code> class is a booking processor for adjustments to bond trades
 * that were sell at assumed factor.  When actual factor becomes known, we need to recalculate "Interest Income" and pay the
 * difference, adjust "Realized Gain / Loss" and pay the principal payment resulting from factor change which is accrued and
 * we'll get paid on payment date (accrual reversal).
 *
 * @author vgomelsky
 */
public class AccountingFactorChangeForAssumedSellBookingRule extends BaseEventJournalBookingRule {

	private boolean accrualReversal = false;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();
		InvestmentSecurityEvent event = eventJournal.getSecurityEvent();
		LogUtils.info(getClass(), "Processing eventJournal: " + eventJournal);

		Date transactionDate = DateUtils.clearTime(new Date());

		// if payment is not delayed, no need to accrue
		boolean doNotAccrue = false;

		// no point to accrue if we're booking after payment transactionDate
		if (transactionDate.after(event.getEventDate())) {
			transactionDate = event.getEventDate();
			doNotAccrue = true;
			if (isAccrualReversal()) {
				// need to process accrual reversal
				if (eventJournal.getAccrualReversalBookingDate() == null) {
					doNotAccrue = false;
				}
			}
			// NOTE: another option is to always accrue as of period end transactionDate
			//transactionDate = isAccrualReversal() ? transactionDate = event.getEventDate() : event.getAccrualEndDate();
			//doNotAccrue = false;
		}
		else if (!event.isPaymentDelayed()) {
			doNotAccrue = true;
		}

		// for each open position
		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			if (doNotAccrue) {
				if (!isAccrualReversal()) {
					// reduce Interest Income, adjust Realized Gain / Loss, and get/pay Cash
					AccountingJournalDetail gainLoss = generateAccountingJournalDetail(journal, eventDetail, null,
							MathUtils.subtract(eventDetail.getAffectedCost(), eventDetail.getAffectedQuantity()), transactionDate);
					gainLoss.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));

					AccountingJournalDetail interestIncome = generateAccountingJournalDetail(journal, eventDetail, null, eventDetail.getTransactionAmount().negate(), transactionDate);
					interestIncome.setAccountingAccount(eventJournal.getJournalType().getCreditAccount());

					addJournalDetails(journal, eventDetail, gainLoss, interestIncome);

					// set reversal booking transactionDate to avoid reversal booking
					eventJournal.setAccrualReversalCombined(true);
				}
			}
			else {
				if (isAccrualReversal()) {
					// reverse Receivable, get Cash, adjust Realized Gain / Loss
					AccountingJournalDetail gainLoss = generateAccountingJournalDetail(journal, eventDetail, null,
							MathUtils.subtract(eventDetail.getAffectedCost(), eventDetail.getAffectedQuantity()), transactionDate);
					gainLoss.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));

					AccountingJournalDetail receivable = generateAccountingJournalDetail(journal, eventDetail, eventDetail.getAffectedQuantity().negate(), eventDetail.getAffectedCost().negate(), transactionDate);
					receivable.setAccountingAccount(eventJournal.getJournalType().getDebitAccount());

					addJournalDetails(journal, eventDetail, receivable, gainLoss);
				}
				else {
					// accrue Receivable, reduce Interest Income, pay Cash
					AccountingJournalDetail receivable = generateAccountingJournalDetail(journal, eventDetail, eventDetail.getAffectedQuantity(), eventDetail.getAffectedCost(), transactionDate);
					receivable.setAccountingAccount(eventJournal.getJournalType().getDebitAccount());

					AccountingJournalDetail interestIncome = generateAccountingJournalDetail(journal, eventDetail, null, eventDetail.getTransactionAmount().negate(), transactionDate);
					interestIncome.setAccountingAccount(eventJournal.getJournalType().getCreditAccount());

					addJournalDetails(journal, eventDetail, receivable, interestIncome);
				}
			}
		}
	}


	private void addJournalDetails(AccountingJournal journal, AccountingEventJournalDetail eventDetail, AccountingJournalDetail firstDetail, AccountingJournalDetail secondDetail) {
		firstDetail.setPositionCostBasis(BigDecimal.ZERO);
		secondDetail.setPositionCostBasis(BigDecimal.ZERO);

		journal.addJournalDetail(firstDetail);
		journal.addJournalDetail(secondDetail);
		journal.addJournalDetail(createCashCurrencyRecord(firstDetail, eventDetail));
		journal.addJournalDetail(createCashCurrencyRecord(secondDetail, eventDetail));
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isAccrualReversal() {
		return this.accrualReversal;
	}


	public void setAccrualReversal(boolean accrualReversal) {
		this.accrualReversal = accrualReversal;
	}
}
