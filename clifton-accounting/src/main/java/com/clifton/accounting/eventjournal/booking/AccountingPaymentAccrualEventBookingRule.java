package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.eventjournal.EventJournalUtils;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingPaymentAccrualEventBookingRule</code> class implements a booking rule for event journals
 * that handles position based payments: dividends, coupons, etc.
 * <p/>
 * This booking rule accrues the payment which will be reversed on Event Date.
 *
 * @author vgomelsky
 */
public class AccountingPaymentAccrualEventBookingRule extends BaseEventJournalBookingRule {

	/**
	 * If true, books cash/currency and corresponding receivables to the holding account identified by cashLocationPurpose
	 * field on investment security hierarchy.  If false, keeps these entries in the same account with position.
	 */
	private boolean cashLocationUsed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		// add debit and credit lines for each payment
		Date transactionDate = getTransactionDate(eventJournal, (eventJournal.getJournalType().getAccrualReversalAccount() != null));
		for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(EventJournalUtils.getCurrencyPaymentDetailList(eventJournal))) {
			journal.addJournalDetail(generateJournalDetail(journal, detail, true, transactionDate));
			BigDecimal frankingCreditTransactionAmount = processFrankingCreditPayout(journal, detail, transactionDate);
			detail.setTransactionAmount(MathUtils.add(detail.getTransactionAmount(), frankingCreditTransactionAmount));
			journal.addJournalDetail(generateJournalDetail(journal, detail, false, transactionDate));
		}
	}


	protected AccountingJournalDetail generateJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventJournalDetail, boolean debit, Date transactionDate) {
		AccountingEventJournalType eventJournalType = eventJournalDetail.getJournal().getJournalType();
		EventData eventData = EventData.forEventPayoutOrEvent(eventJournalDetail.getEventPayout(), eventJournalDetail.getJournal().getSecurityEvent());
		AccountingTransaction affectedLot = eventJournalDetail.getAccountingTransaction();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(eventJournalDetail.getId());
		detail.setSystemTable(getAccountingEventJournalDetailTable());
		detail.setParentTransaction(eventJournalDetail.getAccountingTransaction());

		detail.setAccountingAccount(debit ? eventJournalType.getDebitAccount() : eventJournalType.getCreditAccount());
		detail.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		detail.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());

		// Set the dates first so the account lookup has them to find active account relationships on the transaction date
		detail.setSettlementDate(eventData.getPaymentDate());
		// use actual settlement date for reversals;  reversals are triggered by the presence of the booking date.
		if (eventJournalDetail.getJournal().getBookingDate() != null && eventData.getEvent().getActualSettlementDate() != null) {
			detail.setSettlementDate(eventData.getEvent().getActualSettlementDate());
		}
		detail.setOriginalTransactionDate(getOriginalTransactionDate(eventData, (eventJournalType.getAccrualReversalAccount() != null)));
		detail.setTransactionDate(transactionDate);

		if (isCashLocationUsed()) {
			if (detail.getAccountingAccount().isCurrency() || detail.getAccountingAccount().isReceivable()) {
				detail.setHoldingInvestmentAccount(getInvestmentSecurityUtilHandler().getCashLocationHoldingAccount(detail));
			}
		}

		InvestmentSecurity baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(detail);
		InvestmentSecurity localCurrency = affectedLot.getInvestmentSecurity().getInstrument().getTradingCurrency();
		InvestmentSecurity settlementCurrency = eventData.getSettlementCurrency(); // dividends can be paid in foreign currency
		ValidationUtils.assertTrue(settlementCurrency.isCurrency(), () -> "Settlement Currency " + settlementCurrency.getSymbol() + " must be a currency for security event: " + eventData.getEvent());

		BigDecimal amount = eventJournalDetail.getTransactionAmount();
		if (!debit) {
			amount = amount.negate();
		}

		// for cases where 2 FX may be needed (3 currencies), use the most important one: the one that is used to calculate the payment (based in Event Type's IsEventValueInEventCurrencyUnits field):
		// Settlement to Base (Stock Dividends: amount is in Settlement Currency) or Local to Settlement (Swap Resets: amount is in Local Currency)
		boolean eventValueInEventCurrencyUnits = eventData.isEventValueInEventCurrencyUnits();
		BigDecimal exchangeRateToBase = eventJournalDetail.getExchangeRateToBase();
		if (detail.getAccountingAccount().isReceivable()) {
			detail.setDescription("Accrual of " + eventJournalType.getEventType().getName() + " for " + affectedLot.getInvestmentSecurity().getSymbol() +
					" to be paid on " + DateUtils.fromDateShort(eventData.getPaymentDate()));
			detail.setInvestmentSecurity(settlementCurrency);

			boolean recalculateBaseAmount = true;
			if (eventValueInEventCurrencyUnits) {
				// amount is in Settlement Currency and FX is from Settlement to Base
				InvestmentNotionalCalculatorTypes notionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(settlementCurrency);
				detail.setLocalDebitCredit(notionalCalculator.round(amount));
			}
			else {
				// amount is in Local Currency and FX is from Local to Settlement
				detail.setLocalDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(amount, exchangeRateToBase, detail.getClientInvestmentAccount()));
				if (settlementCurrency.equals(baseCurrency)) {
					exchangeRateToBase = BigDecimal.ONE;
				}
				else {
					// need to lookup fx from local to base, then calculate the base amount using local amount and finally back into FX
					// this is done because when we have 3 currencies and 3 FX rates, Base Debit/Credit for both income and receivable must match: use the one from income
					exchangeRateToBase = eventJournalDetail.getFxToBaseIfDefined();
					if (exchangeRateToBase == null) {
						exchangeRateToBase = getExchangeRate(localCurrency, baseCurrency, affectedLot, eventData.getEvent(), eventJournalType);
					}
					detail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(amount, exchangeRateToBase, detail.getClientInvestmentAccount()));
					if (!localCurrency.equals(settlementCurrency)) {
						// back into FX Rate that results in accurate Base Debit/Credit where there are 3 currencies
						exchangeRateToBase = MathUtils.divide(detail.getBaseDebitCredit(), detail.getLocalDebitCredit(), DataTypes.EXCHANGE_RATE.getPrecision());
					}
					recalculateBaseAmount = false;
				}
			}
			detail.setExchangeRateToBase(exchangeRateToBase);
			if (recalculateBaseAmount) {
				detail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(detail.getLocalDebitCredit(), exchangeRateToBase, detail.getClientInvestmentAccount()));
			}

			// no quantity or price for cash/currency
			detail.setQuantity(null);
			detail.setPrice(null);
		}
		else {
			detail.setDescription(eventJournalDetail.getLabel());

			if (eventValueInEventCurrencyUnits) {
				// amount is in Settlement Currency and FX is from Settlement to Base
				detail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(amount, exchangeRateToBase, detail.getClientInvestmentAccount()));
				if (!localCurrency.equals(settlementCurrency)) {
					if (localCurrency.equals(baseCurrency)) {
						exchangeRateToBase = BigDecimal.ONE;
					}
					else {
						exchangeRateToBase = getExchangeRate(localCurrency, baseCurrency, affectedLot, eventData.getEvent(), eventJournalType);
					}
				}
				detail.setExchangeRateToBase(exchangeRateToBase);
				detail.setLocalDebitCredit(InvestmentCalculatorUtils.calculateLocalAmount(detail.getBaseDebitCredit(), exchangeRateToBase, baseCurrency));
			}
			else {
				// amount is in Local Currency and FX is from Local to Settlement
				InvestmentNotionalCalculatorTypes notionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(localCurrency);
				detail.setLocalDebitCredit(notionalCalculator.round(amount));
				if (!settlementCurrency.equals(baseCurrency)) {
					if (localCurrency.equals(baseCurrency)) {
						exchangeRateToBase = BigDecimal.ONE;
					}
					else {
						exchangeRateToBase = eventJournalDetail.getFxToBaseIfDefined();
						if (exchangeRateToBase == null) {
							exchangeRateToBase = getExchangeRate(localCurrency, baseCurrency, affectedLot, eventData.getEvent(), eventJournalType);
						}
					}
				}
				detail.setExchangeRateToBase(exchangeRateToBase);
				detail.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(detail.getLocalDebitCredit(), exchangeRateToBase, detail.getClientInvestmentAccount()));
			}

			detail.setQuantity(eventJournalDetail.getAffectedQuantity());
			detail.setPrice(eventJournalDetail.getTransactionPrice());
		}

		detail.setPositionCostBasis(BigDecimal.ZERO);


		// questionable fields other than possibly cost basis (may need to delete later)
		detail.setOpening(true);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}


	protected BigDecimal processFrankingCreditPayout(AccountingJournal journal, AccountingEventJournalDetail detail, Date transactionDate) {
		BigDecimal frankingCreditTransactionAmount = getFrankingCreditPayoutAmount(detail);
		if (detail.getEventPayout() != null && detail.getEventPayout().getPayoutType().isWithFrankingCredit()) {
			AccountingEventJournalDetail copyOfEventJournalDetail = new AccountingEventJournalDetail();
			BeanUtils.copyProperties(detail, copyOfEventJournalDetail);
			copyOfEventJournalDetail.setTransactionAmount(frankingCreditTransactionAmount);
			AccountingJournalDetail frankingCreditDetail = generateJournalDetail(journal, copyOfEventJournalDetail, true, transactionDate);
			journal.addJournalDetail(populateFrankingCreditJournalDetails(frankingCreditDetail, detail));
		}
		return frankingCreditTransactionAmount;
	}


	protected BigDecimal getFrankingCreditPayoutAmount(AccountingEventJournalDetail detail) {
		BigDecimal frankingCreditTransactionAmount = BigDecimal.ZERO;
		if (detail.getEventPayout() != null && detail.getEventPayout().getPayoutType().isWithFrankingCredit()) {
			frankingCreditTransactionAmount = MathUtils.multiply(detail.getEventPayout().getAdditionalPayoutValue2(), detail.getAffectedQuantity());
		}
		return frankingCreditTransactionAmount;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCashLocationUsed() {
		return this.cashLocationUsed;
	}


	public void setCashLocationUsed(boolean cashLocationUsed) {
		this.cashLocationUsed = cashLocationUsed;
	}
}
