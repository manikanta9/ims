package com.clifton.accounting.eventjournal.generator;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The <code>AccountingEventJournalGeneratorService</code> interface defines methods that create new
 * event journals for existing security events.
 *
 * @author vgomelsky
 */
public interface AccountingEventJournalGeneratorService {

	/**
	 * Return a List of AccountingPosition lot objects that will be affected by corresponding event journals.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingPosition> getAccountingPositionListForEventJournal(int securityEventId, Short eventJournalTypeId);


	/**
	 * Create a List of AccountingEventJournal objects for all security events for specified parameters on the config object
	 * <p>
	 * The journals will contain details only for open lots that haven't been processed already for this event.
	 * If not unprocessed lot exists, nothing will be created.
	 *
	 * @return the number of journals created
	 */
	@ModelAttribute("result")
	@SecureMethod(tableResolverBeanName = "accountingEventJournalGeneratorSecureMethodTableResolver")
	public int generateAccountingEventJournalList(EventJournalGeneratorCommand command);


	/**
	 * Similar to generateAccountingEventJournalList, but called to allow first previewing the journal details prior to any journals being created or posted.
	 */
	@SecureMethod(dtoClass = AccountingEventJournal.class)
	public List<AccountingEventJournalDetail> getAccountingEventJournalListForPreview(EventJournalGeneratorCommand command);


	/**
	 * Create and return a List of AccountingEventJournal(s) for the specified InvestmentSecurityEvent.
	 * Some events may allow more than one journal: Factor Change: regular, assumed buy, assumed sell
	 * The journal will contain details only for open lots that haven't been processed already for this event.
	 */
	@DoNotAddRequestMapping
	public List<AccountingEventJournal> generateAccountingEventJournal(EventJournalGeneratorCommandInternal command);
}
