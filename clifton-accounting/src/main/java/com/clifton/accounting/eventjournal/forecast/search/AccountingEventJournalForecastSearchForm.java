package com.clifton.accounting.eventjournal.forecast.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class AccountingEventJournalForecastSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "journalType.id")
	private Short journalTypeId;

	@SearchField(searchField = "securityEvent.id")
	private Integer securityEventId;

	@SearchField(searchField = "securityEvent.type.id")
	private Short securityEventTypeId;

	@SearchField(searchField = "security.id", searchFieldPath = "securityEvent")
	private Integer investmentSecurityId;

	@SearchField(searchField = "cusip", searchFieldPath = "securityEvent.security")
	private String cusip;

	@SearchField(searchField = "paymentCurrency.id", sortField = "paymentCurrency.symbol")
	private Integer paymentCurrencyId;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "securityEvent.security")
	private Integer underlyingSecurityId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "teamSecurityGroup.id", sortField = "teamSecurityGroup.name", searchFieldPath = "clientInvestmentAccount")
	private Short teamSecurityGroupId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "clientInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer clientInvestmentAccountGroupId;

	@SearchField(searchField = "holdingInvestmentAccount.id")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingInvestmentAccount")
	private Integer holdingAccountIssuerId;

	@SearchField(searchField = "type.id", searchFieldPath = "holdingInvestmentAccount")
	private Short holdingAccountTypeId;

	@SearchField(searchField = "eventDate", searchFieldPath = "securityEvent")
	private Date eventDate;

	@SearchField(searchField = "executingCompany.id")
	private Integer executingCompanyId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "securityEvent.security.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField
	private BigDecimal affectedQuantity;

	@SearchField
	private BigDecimal affectedCost;

	@SearchField
	private BigDecimal transactionAmount;

	@SearchField
	private BigDecimal exchangeRateToBase;

	@SearchField
	private BigDecimal transactionPrice;

	@SearchField
	private BigDecimal additionalAmount;


	public Short getJournalTypeId() {
		return this.journalTypeId;
	}


	public void setJournalTypeId(Short journalTypeId) {
		this.journalTypeId = journalTypeId;
	}


	public Integer getSecurityEventId() {
		return this.securityEventId;
	}


	public void setSecurityEventId(Integer securityEventId) {
		this.securityEventId = securityEventId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Integer getExecutingCompanyId() {
		return this.executingCompanyId;
	}


	public void setExecutingCompanyId(Integer executingCompanyId) {
		this.executingCompanyId = executingCompanyId;
	}


	public BigDecimal getAffectedQuantity() {
		return this.affectedQuantity;
	}


	public void setAffectedQuantity(BigDecimal affectedQuantity) {
		this.affectedQuantity = affectedQuantity;
	}


	public BigDecimal getAffectedCost() {
		return this.affectedCost;
	}


	public void setAffectedCost(BigDecimal affectedCost) {
		this.affectedCost = affectedCost;
	}


	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}


	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getTransactionPrice() {
		return this.transactionPrice;
	}


	public void setTransactionPrice(BigDecimal transactionPrice) {
		this.transactionPrice = transactionPrice;
	}


	public BigDecimal getAdditionalAmount() {
		return this.additionalAmount;
	}


	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public Integer getPaymentCurrencyId() {
		return this.paymentCurrencyId;
	}


	public void setPaymentCurrencyId(Integer paymentCurrencyId) {
		this.paymentCurrencyId = paymentCurrencyId;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Short getSecurityEventTypeId() {
		return this.securityEventTypeId;
	}


	public void setSecurityEventTypeId(Short securityEventTypeId) {
		this.securityEventTypeId = securityEventTypeId;
	}
}
