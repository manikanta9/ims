package com.clifton.accounting.eventjournal.forecast;


import com.clifton.accounting.eventjournal.forecast.observers.AccountingEventJournalForecastInvestmentSecurityEventObserver;
import com.clifton.accounting.eventjournal.forecast.search.AccountingEventJournalForecastSearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * @author manderson
 */
public interface AccountingEventJournalForecastService {

	////////////////////////////////////////////////////////////////////////////
	///////////////////////       Forecast Methods       ///////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalForecast getAccountingEventJournalForecast(int id);


	public List<AccountingEventJournalForecast> getAccountingEventJournalForecastList(AccountingEventJournalForecastSearchForm searchForm);


	public List<AccountingEventJournalForecast> getAccountingEventJournalForecastListForSecurityEvent(int securityEventId);


	public void deleteAccountingEventJournalForecastList(List<AccountingEventJournalForecast> list);


	////////////////////////////////////////////////////////////////////////////
	///////////////////     Forecast Generation Methods     ////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Processes/Rebuilds forecast journals associated with a specific event.
	 * Called from {@link AccountingEventJournalForecastInvestmentSecurityEventObserver} when events are updated
	 *
	 * @param securityEventId
	 * @param synchronous     - if false will schedule a runner to run the processing, otherwise will process it
	 */
	public void processAccountingEventJournalForecastListForSecurityEvent(int securityEventId, boolean synchronous);


	/**
	 * Processes/Rebuilds forecast journals for security events that fall within specified date range/search criteria held in the command object
	 */
	@ModelAttribute("result")
	public String processAccountingEventJournalForecastList(AccountingEventJournalForecastProcessorCommand command, boolean synchronous);
}
