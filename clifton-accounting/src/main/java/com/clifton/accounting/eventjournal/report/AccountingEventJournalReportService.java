package com.clifton.accounting.eventjournal.report;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;


/**
 * The <code>AccountingEventJournalReportService</code> ...
 *
 * @author manderson
 */
public interface AccountingEventJournalReportService {

	////////////////////////////////////////////////////////////////////////////
	//////     Event Journal Detail Report Business Methods               //////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = AccountingEventJournal.class)
	public FileWrapper downloadAccountingEventJournalDetailReport(int detailId, FileFormats format);
}
