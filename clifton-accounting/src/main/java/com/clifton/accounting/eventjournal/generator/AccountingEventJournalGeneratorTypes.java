package com.clifton.accounting.eventjournal.generator;


/**
 * The <code>AccountingEventJournalGeneratorTypes</code> ...
 *
 * @author manderson
 */
public enum AccountingEventJournalGeneratorTypes {

	PREVIEW(false), // PREVIEW EVENT JOURNAL
	FORECAST(true), // FORECAST EVENT JOURNAL - SIMILAR TO PREVIEW, BUT FOR CASES WITH MISSING INFORMATION WE STILL WANT TO GENERATE THE DETAIL WITH MESSAGE INSTEAD OF SKIPPING IT SO WE CAN CREATE THE FORECAST ROW (generateInvalid = true) 
	GENERATE(true, true, false, false, false, false), // GENERATE EVENTS ONLY (WILL ONLY POST IF EVENT IS autoPost = TRUE)
	POST_EXISTING(false, false, true, false, false, false), // POST EXISTING EVENTS ONLY (DOES NOT GENERATE ANY NEW EVENTS)
	POST(true, true, true, false, false, false), // GENERATE AND POST EVENTS (CONSIDERS RESULTS TO NOT HAVE BEEN PREVIEWED)
	POST_FORCE_ACCRUAL(true, true, true, false, false, true), // GENERATE AND POST EVENTS (CONSIDERS RESULTS TO NOT HAVE BEEN PREVIEWED) - FORCE ACCRUAL POSTING EVEN BEFORE EX DATE
	GENERATE_PREVIEWED(true, true, false, true, false, false), // GENERATE EVENTS (CONSIDERS RESULTS TO HAVE BEEN PREVIEWED)
	POST_PREVIEWED(true, true, true, true, false, false), // GENERATE AND POST EVENTS (CONSIDERS RESULTS TO HAVE BEEN PREVIEWED)
	REPROCESS_EXISTING(false, false, false, false, true, false); // RE-GENERATES EXISTING EVENT JOURNALS AND UPDATES - EVENTS CANNOT HAVE BEEN POSTED YET AND NO NEW JOURNALS WILL BE CREATED


	private final boolean generate;
	private final boolean generateInvalid; // Used for forecast to still generate the detail without payments amounts if there is some missing required data
	private final boolean save;
	private final boolean post;
	private final boolean previewed;
	private final boolean reprocessExisting;
	/**
	 * Post accrual journal for events with payment delay even before Ex Date.
	 * Need this in rare cases when Factor Change needs to be booked before Ex Date because
	 * the trade settles after factor payment date but is entered before factor Ex Date.
	 */
	private final boolean forceAccrualPosting;


	AccountingEventJournalGeneratorTypes(boolean generateInvalid) {
		this(true, generateInvalid, false, false, false, false, false);
	}


	AccountingEventJournalGeneratorTypes(boolean generate, boolean save, boolean post, boolean previewed, boolean reprocessExisting, boolean forceAccrualPosting) {
		this(generate, false, save, post, previewed, reprocessExisting, forceAccrualPosting);
	}


	AccountingEventJournalGeneratorTypes(boolean generate, boolean generateInvalid, boolean save, boolean post, boolean previewed, boolean reprocessExisting, boolean forceAccrualPosting) {
		this.generate = generate;
		this.generateInvalid = generateInvalid;
		this.save = save;
		this.post = post;
		this.previewed = previewed;
		this.reprocessExisting = reprocessExisting;
		this.forceAccrualPosting = forceAccrualPosting;
	}


	public boolean isPreview() {
		return isGenerate() && !isGenerateInvalid() && !isSave();
	}


	public boolean isSave() {
		return this.save;
	}


	public boolean isPost() {
		return this.post;
	}


	public boolean isPreviewed() {
		return this.previewed;
	}


	public boolean isGenerate() {
		return this.generate;
	}


	public boolean isReprocessExisting() {
		return this.reprocessExisting;
	}


	public boolean isForceAccrualPosting() {
		return this.forceAccrualPosting;
	}


	public boolean isGenerateInvalid() {
		return this.generateInvalid;
	}
}
