package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The <code>AccountingFactorChangeForAssumedBuyBookingRule</code> class is a booking processor for adjustments to bond trades
 * that were buys at assumed factor.  When actual factor becomes known, we need to recalculate "Interest Income" and get back
 * the difference as well as receive the principal payment resulting from factor change.
 *
 * @author vgomelsky
 */
public class AccountingFactorChangeForAssumedBuyBookingRule extends BaseEventJournalBookingRule {

	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();
		LogUtils.debug(getClass(), "Processing eventJournal: " + eventJournal);

		Date date = DateUtils.clearTime(new Date());
		if (date.after(eventJournal.getSecurityEvent().getAccrualEndDate())) {
			date = eventJournal.getSecurityEvent().getAccrualEndDate();
		}

		// for each open position
		AccountingJournal parentJournal = null;
		boolean parentFound = false;
		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			Date changeDate = date;
			if (date.before(eventDetail.getAccountingTransaction().getSettlementDate())) {
				changeDate = eventDetail.getAccountingTransaction().getSettlementDate(); // can't be before settlement date
			}

			// reduce Position, reduce Interest Income, get Cash
			AccountingJournalDetail position = generateAccountingJournalDetail(journal, eventDetail, eventDetail.getAffectedQuantity().negate(), eventDetail.getAffectedCost().negate(), changeDate);
			AccountingJournalDetail interestIncome = generateAccountingJournalDetail(journal, eventDetail, null, eventDetail.getTransactionAmount(), changeDate);
			interestIncome.setAccountingAccount(eventJournal.getJournalType().getCreditAccount());
			interestIncome.setPositionCostBasis(BigDecimal.ZERO);

			journal.addJournalDetail(position);
			journal.addJournalDetail(interestIncome);
			journal.addJournalDetail(createCashCurrencyRecord(position, eventDetail));
			journal.addJournalDetail(createCashCurrencyRecord(interestIncome, eventDetail));

			// set parent journal only if all open positions come from the same journal: single trade 
			AccountingJournal parent = eventDetail.getAccountingTransaction().getJournal();
			if (parentFound) {
				if (!parent.equals(parentJournal)) {
					parentFound = false;
				}
			}
			else if (parentJournal == null) {
				parentFound = true;
				parentJournal = parent;
			}
		}

		if (parentFound) {
			journal.setParent(parentJournal);
		}
	}
}
