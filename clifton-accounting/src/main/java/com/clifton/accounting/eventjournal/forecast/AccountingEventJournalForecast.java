package com.clifton.accounting.eventjournal.forecast;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * The <code>AccountingEventJournalForecast</code> is a previews of upcoming event journals expected to process on specified date for Client Account and Security Event.
 * Forecasts are useful for looking at previews of upcoming expected events and corresponding cash flows. However positions could potentially change prior to the event date.
 * Forecasts are rebuilt daily with latest client position and security event information.
 * <p/>
 * The information stored in the forecast table is a normalized version of what would be generated for the {@link AccountingEventJournal}
 * and {@link AccountingEventJournalDetail} tables.  Details currently breaks lines out by lots, but the forecast table will sum across the lots for
 * simplifying the information for users.
 * <p/>
 * Some of the information is also normalized from the {@link AccountingTransaction} table.  Since these are not actual journals
 * that will be posted we don't want to prevent positions from being unbooked or closed because of this reference.
 * <p/>
 * Each day this table is rebuilt (inserts/updates/deletes) where necessary and generated for a specified period of time (i.e next 30 days) - not for all future events.
 * When an event journal is actually generated, if there is a forecast row it will be removed, and the batch job will also verify/remove forecast rows that no longer apply.
 *
 * @author manderson
 */
public class AccountingEventJournalForecast extends BaseSimpleEntity<Integer> {

	///////////////////////////////////////////////////////
	// Event Information: Journal Type and Security Event
	///////////////////////////////////////////////////////

	private AccountingEventJournalType journalType;
	private InvestmentSecurityEvent securityEvent;

	///////////////////////////////////////////////////////	 
	// Position Information (From Accounting Transaction)
	///////////////////////////////////////////////////////

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	private BusinessCompany executingCompany;

	///////////////////////////////////////////////////////
	// Payment/Event Journal Detail Information
	// From {@link AccountingEventJournalDetail}
	///////////////////////////////////////////////////////

	/**
	 * The quantity of the specified transaction that's been affected (transaction can be partially closed).
	 */
	private BigDecimal affectedQuantity;

	/**
	 * The cost basis remaining of the specified transaction that's been affected.
	 */
	private BigDecimal affectedCost;

	/**
	 * Examples: total dividend/coupon payment for the specified lot in local currency.
	 * Holds remaining Cost Basis when a new position is created (stock split)
	 */
	private BigDecimal transactionAmount;

	private InvestmentSecurity paymentCurrency;

	/**
	 * Exchange Rate from local currency of event security to base currency of client account.
	 * Most events are in local currency so it's usually 1.
	 */
	private BigDecimal exchangeRateToBase = BigDecimal.ONE;

	/**
	 * When applicable, the price of event transactions: maturity price, dividend price per share, etc.
	 */
	private BigDecimal transactionPrice;

	/**
	 * Some events may require additional amount (LossPercent for a bond Factor Change event)
	 */
	private BigDecimal additionalAmount;

	/**
	 * Some events may not be able to generate because of missing information.  During generation for the forecasts, we want to keep the event
	 * to show it as an upcoming event, but also store the error message so users know why transaction amount may not be generated
	 */
	private String description;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		StringBuilder lbl = new StringBuilder(16);
		if (getClientInvestmentAccount() != null) {
			lbl.append(getClientInvestmentAccount().getLabel());
		}
		if (getSecurityEvent() != null) {
			lbl.append(" - ").append(getSecurityEvent().getLabel());
		}
		return lbl.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		// Two Forecasts are Equal if
		// 1. Events are Equal
		// 2. Client and Holding Accounts Are Equal
		// 3. Payment CCY is Equal
		if (!(o instanceof AccountingEventJournalForecast)) {
			return false;
		}
		AccountingEventJournalForecast oBean = (AccountingEventJournalForecast) o;

		if (CompareUtils.isEqual(this.getSecurityEvent(), oBean.getSecurityEvent())) {
			if (CompareUtils.isEqual(this.getClientInvestmentAccount(), oBean.getClientInvestmentAccount())) {
				if (CompareUtils.isEqual(this.getHoldingInvestmentAccount(), oBean.getHoldingInvestmentAccount())) {
					if (CompareUtils.isEqual(this.getPaymentCurrency(), oBean.getPaymentCurrency())) {
						return true;
					}
				}
			}
		}
		return false;
	}


	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(),
				this.securityEvent,
				this.clientInvestmentAccount,
				this.holdingInvestmentAccount,
				this.paymentCurrency);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalType getJournalType() {
		return this.journalType;
	}


	public void setJournalType(AccountingEventJournalType journalType) {
		this.journalType = journalType;
	}


	public InvestmentSecurityEvent getSecurityEvent() {
		return this.securityEvent;
	}


	public void setSecurityEvent(InvestmentSecurityEvent securityEvent) {
		this.securityEvent = securityEvent;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public BusinessCompany getExecutingCompany() {
		return this.executingCompany;
	}


	public void setExecutingCompany(BusinessCompany executingCompany) {
		this.executingCompany = executingCompany;
	}


	public BigDecimal getAffectedQuantity() {
		return this.affectedQuantity;
	}


	public void setAffectedQuantity(BigDecimal affectedQuantity) {
		this.affectedQuantity = affectedQuantity;
	}


	public BigDecimal getAffectedCost() {
		return this.affectedCost;
	}


	public void setAffectedCost(BigDecimal affectedCost) {
		this.affectedCost = affectedCost;
	}


	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}


	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getTransactionPrice() {
		return this.transactionPrice;
	}


	public void setTransactionPrice(BigDecimal transactionPrice) {
		this.transactionPrice = transactionPrice;
	}


	public BigDecimal getAdditionalAmount() {
		return this.additionalAmount;
	}


	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public InvestmentSecurity getPaymentCurrency() {
		return this.paymentCurrency;
	}


	public void setPaymentCurrency(InvestmentSecurity paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}
}
