package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.EventJournalUtils;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The AccountingScripDividendBookingRule class adds corresponding {@link AccountingJournalDetail} entries for each event journal detail.
 * It skips "Currency" payout elections and only processes non currency "Security (Existing)" payouts.
 * It supports delayed events (accrual and reversal) as well as events with no delay.
 * The logic generates Dividend Income entry and corresponding new position lot using the price specified by the Payout event.
 * Existing lots stay unchanged.
 *
 * @author vgomelsky
 */
public class AccountingScripDividendBookingRule extends BaseEventJournalBookingRule {

	private static final String RECEIVABLE_AGGREGATE_DETAIL_KEY = "RECEIVABLE_";
	private static final String REVERSAL_AGGREGATE_DETAIL_KEY = "REVERSAL_";
	private static final String NEW_AGGREGATE_DETAIL_KEY = "NEW_";
	private static final String FRACTION_CLOSE_AGGREGATE_DETAIL_KEY = "FRACTION_";
	private static final String FRACTION_CLOSE_CASH_DETAIL_KEY = "FRACTION_CASH_";

	/**
	 * Specifies whether this rule should be accruing Scrip Dividend (or actually getting the dividend).
	 */
	private boolean accrual;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return super.toString() + ", accrual = " + isAccrual();
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		// Map for containing aggregated transactions for the event.
		// The first AccountingEventJournalDetail will be the FK for the aggregated AccountingJournalDetail.
		Map<String, AccountingJournalDetail> journalDetailAggregationMap = new LinkedHashMap<>();
		// add debit and credit lines for each payment
		Date transactionDate = getTransactionDate(eventJournal, (eventJournal.getJournalType().getAccrualReversalAccount() != null));
		AccountingEventJournalDetail.Scrip firstScrip = null;
		for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(EventJournalUtils.getNonCurrencyPaymentDetailList(eventJournal))) {
			AccountingEventJournalDetail.Scrip scrip = detail.forScrip();
			if (firstScrip == null) {
				firstScrip = scrip;
			}

			if (isAccrual()) {
				// ACCRUAL: Position Receivable + Dividend Income + Franking Credit (if applicable)
				// dividend income is tied to each lot/detail as the parent
				processReceivables(journal, scrip, transactionDate);
				// the receivable position is a new position aggregated across all the lots/details
				AccountingJournalDetail positionReceivable = createReceivableForNewPosition(journal, scrip, transactionDate);
				addJournalDetailForAggregation(positionReceivable, RECEIVABLE_AGGREGATE_DETAIL_KEY, journalDetailAggregationMap);
			}
			else if (eventJournal.getSecurityEvent().isPaymentDelayed()) {
				// REVERSAL: reverse Position Receivable and replace with new Position
				AccountingJournalDetail reversal = createReversalOfReceivableForNewPosition(journal, scrip, transactionDate);
				addJournalDetailForAggregation(reversal, REVERSAL_AGGREGATE_DETAIL_KEY, journalDetailAggregationMap);
				AccountingJournalDetail newPosition = createNewPosition(journal, scrip, transactionDate);
				addJournalDetailForAggregation(newPosition, NEW_AGGREGATE_DETAIL_KEY, journalDetailAggregationMap);
				// fractional lot will close newly created position(s)
				// NOTE: if we want this logic to run through existing positions, we would need to load them here
				addFractionalPositionClosing(journal, scrip, transactionDate, journalDetailAggregationMap);
			}
			else {
				// NO DELAY: Position + Dividend Income + Franking Credit (if applicable)
				processReceivables(journal, scrip, transactionDate);
				AccountingJournalDetail opening = createNewPosition(journal, scrip, transactionDate);
				addJournalDetailForAggregation(opening, NEW_AGGREGATE_DETAIL_KEY, journalDetailAggregationMap);
				addFractionalPositionClosing(journal, scrip, transactionDate, journalDetailAggregationMap);
			}
		}

		for (Map.Entry<String, AccountingJournalDetail> journalDetailEntry : journalDetailAggregationMap.entrySet()) {
			String key = journalDetailEntry.getKey();
			AccountingJournalDetail detail = journalDetailEntry.getValue();
			if (key.startsWith(REVERSAL_AGGREGATE_DETAIL_KEY)) {
				// Update the aggregated reversal's parent to reference the accrual opening transaction that it is reversing.
				// The first AccountingEventJournalDetail processed will be the FK on the aggregated accrual opening transaction.
				AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
				searchForm.setFkFieldId(detail.getFkFieldId());
				searchForm.setJournalTypeId(journal.getJournalType().getId());
				searchForm.setAccountingAccountId(detail.getAccountingAccount().getId());
				searchForm.setOpening(true);
				searchForm.setQuantity(MathUtils.negate(detail.getQuantity()));
				List<AccountingTransaction> receivableList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
				String errorMessageTemplate = null;
				if (CollectionUtils.isEmpty(receivableList)) {
					// when reversing: there must always be a matching accrual
					errorMessageTemplate = "Cannot find accrual receivable position to reverse for %s";
				}
				else if (receivableList.size() > 1) {
					errorMessageTemplate = "Cannot have more than one accrual receivable to reverse for %s: " + receivableList;
				}
				if (!StringUtils.isEmpty(errorMessageTemplate)) {
					throw new IllegalStateException(String.format(errorMessageTemplate, firstScrip));
				}
				detail.setParentTransaction(receivableList.get(0));
			}
			else if (key.startsWith(NEW_AGGREGATE_DETAIL_KEY)) {
				bookingSession.addNewCurrentPosition(detail);
			}
		}

		journalDetailAggregationMap.values().forEach(journal::addJournalDetail);

		bookingSession.sortPositions(AccountingPositionOrders.FIFO);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected BigDecimal processFrankingCreditPayout(AccountingJournal journal, AccountingEventJournalDetail.Scrip scrip, Date transactionDate) {
		BigDecimal frankingCreditDebitCredit = BigDecimal.ZERO;
		if (scrip.getDetail().getEventPayout().getPayoutType().isWithFrankingCredit()) {
			frankingCreditDebitCredit = addFrankingCredit(journal, scrip, transactionDate).getLocalDebitCredit();
		}
		return frankingCreditDebitCredit;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 *
	 */
	private AccountingJournalDetail processReceivables(AccountingJournal journal, AccountingEventJournalDetail.Scrip scrip, Date transactionDate) {
		BigDecimal frankingCreditLocalDebitCredit = processFrankingCreditPayout(journal, scrip, transactionDate);
		AccountingJournalDetail dividend = generateAccountingJournalDetail(journal, scrip.getDetail(), scrip.getOpeningQuantity(), MathUtils.negate(MathUtils.add(scrip.getOpeningCostBasis(), frankingCreditLocalDebitCredit)), transactionDate);
		dividend.setOpening(true);
		dividend.setAccountingAccount(scrip.getJournalType().getCreditAccount());
		dividend.setPrice(scrip.getOpeningPrice());
		dividend.setPositionCostBasis(BigDecimal.ZERO);
		dividend.setSettlementDate(scrip.getEventDate());
		journal.addJournalDetail(dividend);

		return dividend;
	}


	private AccountingJournalDetail addFrankingCredit(AccountingJournal journal, AccountingEventJournalDetail.Scrip scrip, Date transactionDate) {
		AccountingEventJournalDetail detail = scrip.getDetail();
		InvestmentSecurityEventPayout payout = detail.getEventPayout();
		BigDecimal localDebitCredit = InvestmentCalculatorUtils.roundLocalAmount(MathUtils.multiply(payout.getAdditionalPayoutValue2(), detail.getAdditionalAmount()), scrip.getEventSecurity());
		AccountingJournalDetail frankingCredit = generateAccountingJournalDetail(journal, detail, detail.getAdditionalAmount(), localDebitCredit, transactionDate);
		frankingCredit = populateFrankingCreditJournalDetails(frankingCredit, detail);
		journal.addJournalDetail(frankingCredit);

		return frankingCredit;
	}


	private AccountingJournalDetail createReceivableForNewPosition(AccountingJournal journal, AccountingEventJournalDetail.Scrip scrip, Date transactionDate) {
		AccountingJournalDetail positionReceivable = generateAccountingJournalDetail(journal, scrip.getDetail(), scrip.getOpeningQuantity(), scrip.getOpeningCostBasis(), transactionDate);
		positionReceivable.setOpening(true);
		positionReceivable.setParentTransaction(null); // clear parent as this a new position resulting from the original
		AccountingAccount positionAccount = scrip.getAccountingAccount();
		AccountingAccount receivableAccount = positionAccount.isReceivable() ? positionAccount : positionAccount.getReceivableAccount();
		ValidationUtils.assertNotNull(receivableAccount, "Receivable GL Account is not defined for: " + positionAccount.getLabel());
		positionReceivable.setAccountingAccount(receivableAccount);
		positionReceivable.setPrice(scrip.getOpeningPrice());
		positionReceivable.setDescription("Receivable from " + positionReceivable.getDescription());
		positionReceivable.setSettlementDate(scrip.getEventDate());
		return positionReceivable;
	}


	private AccountingJournalDetail createReversalOfReceivableForNewPosition(AccountingJournal journal, AccountingEventJournalDetail.Scrip scrip, Date transactionDate) {
		AccountingJournalDetail reversal = createReceivableForNewPosition(journal, scrip, transactionDate);
		reversal.setOpening(false);
		reversal.reverseSignForGLAmounts();
		reversal.setDescription("Reversal of " + reversal.getDescription());
		return reversal;
	}


	private AccountingJournalDetail createNewPosition(AccountingJournal journal, AccountingEventJournalDetail.Scrip scrip, Date transactionDate) {
		AccountingJournalDetail positionDetail = generateAccountingJournalDetail(journal, scrip.getDetail(), scrip.getOpeningQuantity(), scrip.getOpeningCostBasis(), transactionDate);
		positionDetail.setOpening(true);
		positionDetail.setParentTransaction(null); // clear parent as this a new position resulting from the original
		positionDetail.setAccountingAccount(scrip.getAccountingAccount());
		positionDetail.setPrice(scrip.getOpeningPrice());
		positionDetail.setSettlementDate(scrip.getEventDate());

		return positionDetail;
	}


	private AccountingJournalDetail addFractionalPositionClosing(AccountingJournal journal, AccountingEventJournalDetail.Scrip scrip, Date transactionDate, Map<String, AccountingJournalDetail> journalDetailAggregationMap) {
		AccountingJournalDetail fractionalClosing = null;
		if (!MathUtils.isNullOrZero(scrip.getFractionToClose())) {
			BigDecimal eventDatePrice = scrip.getOpeningPrice(); // NOTE: may need to enhance this to lookup Event Date price and/or allow overriding closing price on event journal detail
			BigDecimal fractionalProceeds = InvestmentCalculatorUtils.getNotionalCalculator(scrip.getEventSecurity()).calculateNotional(eventDatePrice, scrip.getEventSecurity().getPriceMultiplier(), scrip.getFractionToClose());
			fractionalClosing = generateAccountingJournalDetail(journal, scrip.getDetail(), scrip.getFractionToClose(), fractionalProceeds, transactionDate);
			fractionalClosing.setParentTransaction(null);
			fractionalClosing.setPrice(eventDatePrice);
			fractionalClosing.reverseSignForGLAmounts();
			fractionalClosing.setDescription("Fractional Shares Closing from rounding from " + fractionalClosing.getDescription());
			fractionalClosing.setSettlementDate(scrip.getEventDate());

			AccountingJournalDetail cashProceeds = createCashCurrencyRecord(fractionalClosing);
			cashProceeds.setDescription(cashProceeds.getDescription() + " Fractional Shares from " + scrip.getJournalType().getName());

			addJournalDetailForAggregation(fractionalClosing, FRACTION_CLOSE_AGGREGATE_DETAIL_KEY, journalDetailAggregationMap);
			// cash details will be aggregated by the cash aggregation booking rule; add to the map so the detail order is consistent and after the fractional close detail
			addJournalDetailForAggregation(cashProceeds, FRACTION_CLOSE_CASH_DETAIL_KEY, journalDetailAggregationMap);
			// NOTE: realized gain/loss will be calculated and added by the next rule
		}
		return fractionalClosing;
	}


	private void addJournalDetailForAggregation(AccountingJournalDetail journalDetail, String detailLegPrefix, Map<String, AccountingJournalDetail> journalDetailAggregationMap) {
		String key = detailLegPrefix + journalDetail.createNaturalKey();
		journalDetailAggregationMap.compute(key, (k, existing) -> {
			if (existing == null) {
				return journalDetail;
			}

			existing.setQuantity(MathUtils.add(existing.getQuantity(), journalDetail.getQuantity()));
			existing.setLocalDebitCredit(MathUtils.add(existing.getLocalDebitCredit(), journalDetail.getLocalDebitCredit()));
			existing.setBaseDebitCredit(MathUtils.add(existing.getBaseDebitCredit(), journalDetail.getBaseDebitCredit()));
			existing.setPositionCostBasis(MathUtils.add(existing.getPositionCostBasis(), journalDetail.getPositionCostBasis()));
			return existing;
		});
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isAccrual() {
		return this.accrual;
	}


	public void setAccrual(boolean accrual) {
		this.accrual = accrual;
	}
}
