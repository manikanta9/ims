package com.clifton.accounting.eventjournal.generator.detailpopulator;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.marketdata.MarketDataRetriever;

import java.math.BigDecimal;


/**
 * The AccountingEventJournalDetailSpinoffPopulator class calculates parent and spinoff security quantities, prices and cost basis.
 * It will update Adjustment Factor on the even if one was not previously set.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailSpinoffPopulator implements AccountingEventJournalDetailPopulator {

	private InvestmentCalculator investmentCalculator;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private MarketDataRetriever marketDataRetriever;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		EventData eventData = command.getEventData();
		ValidationUtils.assertNotNull(eventData.getAdditionalSecurity(), "Missing required Spinoff Security for: " + eventData.getLabel());

		AccountingEventJournalDetail.Spinoff spinoff = command.getJournalDetail().forSpinoff();
		BigDecimal adjustmentFactor = getAdjustmentFactor(eventData.getEvent());
		InvestmentNotionalCalculatorTypes parentNotionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(eventData.getSecurity());

		spinoff.setParentShares(command.getRemainingQuantity())
				.setParentClosingCostBasis(command.getRemainingCostBasis())
				.setParentNewCostBasis(parentNotionalCalculator.round(MathUtils.multiply(command.getRemainingCostBasis(), adjustmentFactor)))
				.setParentNewPrice(parentNotionalCalculator.calculatePriceFromNotional(spinoff.getParentShares(), spinoff.getParentNewCostBasis(), eventData.getSecurity().getPriceMultiplier()))
				.setSpinoffCostBasis(MathUtils.subtract(command.getRemainingCostBasis(), spinoff.getParentNewCostBasis()))
				.setSpinoffShares(getSpinoffShares(spinoff.getParentShares(), eventData))
				.setFractionToClose(MathUtils.subtract(spinoff.getSpinoffShares(), MathUtils.round(spinoff.getSpinoffShares(), 0, BigDecimal.ROUND_DOWN)));

		return false;
	}


	private BigDecimal getSpinoffShares(BigDecimal parentShares, EventData eventData) {
		// calculate the spinoff shares; rounding will be done via the detail rounder
		return MathUtils.multiply(parentShares, MathUtils.divide(eventData.getAfterEventValue(), eventData.getBeforeEventValue()), DataTypes.QUANTITY.getPrecision()).stripTrailingZeros();
	}


	/**
	 * Returns adjustment factor from the event. If the factor is not set, calculates it using Event Date prices and Distribution Ratio.
	 * Will populate Adjustment Factor on the event and will save the event.
	 */
	private BigDecimal getAdjustmentFactor(InvestmentSecurityEvent securityEvent) {
		if (securityEvent.getAdditionalEventValue() == null) {
			// if the value is not defined, then need to calculate it
			BigDecimal parentShares = securityEvent.getBeforeEventValue();
			BigDecimal spinoffShares = securityEvent.getAfterEventValue();

			BigDecimal parentPrice = getMarketDataRetriever().getPrice(securityEvent.getSecurity(), securityEvent.getEventDate(), true, "Parent Security:");
			BigDecimal spinoffPrice = getMarketDataRetriever().getPrice(securityEvent.getAdditionalSecurity(), securityEvent.getEventDate(), true, "Spinoff Security:");

			BigDecimal parentNotional = getInvestmentCalculator().calculateNotional(securityEvent.getSecurity(), parentPrice, parentShares, BigDecimal.ONE);
			BigDecimal spinoffNotional = getInvestmentCalculator().calculateNotional(securityEvent.getAdditionalSecurity(), spinoffPrice, spinoffShares, BigDecimal.ONE);

			securityEvent.setAdditionalEventValue(MathUtils.divide(parentNotional, MathUtils.add(parentNotional, spinoffNotional)));

			// update adjustment factor on event
			getInvestmentSecurityEventService().saveInvestmentSecurityEvent(securityEvent);
		}

		return securityEvent.getAdditionalEventValue();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
