package com.clifton.accounting.eventjournal.transactiontype;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeRetriever;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeService;
import org.springframework.stereotype.Component;


/**
 * The <code>AccountingTransactionTypeEventJournalRetriever</code> handles the retrieval of {@link AccountingTransactionType}
 * based on the source entity ID for journal entries that are event journals.
 *
 * @author jgommels
 */
@Component
public class AccountingEventJournalTransactionTypeRetriever implements AccountingTransactionTypeRetriever<AccountingEventJournal> {

	private AccountingTransactionTypeService accountingTransactionTypeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingTransactionType getAccountingTransactionType(@SuppressWarnings("unused") AccountingJournalDetailDefinition journalDetail, AccountingEventJournal eventJournal) {
		//Get the AccountingEventJournal from the sourceEntityId

		//Return the appropriate transaction type based on the journal type
		if (eventJournal.getJournalType().getName().equals(AccountingEventJournalType.SECURITY_MATURITY)) {
			return getAccountingTransactionTypeService().getAccountingTransactionTypeByName(AccountingTransactionType.MATURITY);
		}

		return null;
	}


	@Override
	public String getJournalTypeName() {
		return AccountingJournalType.EVENT_JOURNAL;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTransactionTypeService getAccountingTransactionTypeService() {
		return this.accountingTransactionTypeService;
	}


	public void setAccountingTransactionTypeService(AccountingTransactionTypeService accountingTransactionTypeService) {
		this.accountingTransactionTypeService = accountingTransactionTypeService;
	}
}
