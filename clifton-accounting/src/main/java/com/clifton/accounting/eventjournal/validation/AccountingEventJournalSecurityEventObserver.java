package com.clifton.accounting.eventjournal.validation;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;


/**
 * The <code>AccountingEventJournalSecurityEventObserver</code> class :
 * <br/>1. Validates/prevents updates of {@link InvestmentSecurityEvent} objects that are used by booked event journals.
 * <br/>2. For those used by unbooked journals only - After changes, re-processes existing journals and saves updated journal details
 * <p>
 * Note: eventDescription cannot be modified at any time due to fractional shares logic stored in the description
 *
 * @author vgomelsky
 * @see BaseAccountingEventJournalSecurityEventObserver
 */
@Component
public class AccountingEventJournalSecurityEventObserver extends BaseAccountingEventJournalSecurityEventObserver<InvestmentSecurityEvent> {

	private static final String EVENT_STATUS_PROPERTY_NAME = "status";
	private static final String EVENT_ACTUAL_SETTLEMENT_DATE_PROPERTY_NAME = "actualSettlementDate";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE_OR_DELETE;
	}


	@Override
	protected void bookedAccountingEventJournalViolationOnDelete(InvestmentSecurityEvent securityEvent, List<AccountingEventJournal> bookedJournalList) {
		throw new ValidationException("Cannot delete Security Event that is used by booked event journal: " + CollectionUtils.getFirstElementStrict(bookedJournalList).getLabel());
	}


	@Override
	protected boolean isBeanUpdatePropertiesInViolation(InvestmentSecurityEvent securityEvent, InvestmentSecurityEvent originalSecurityEvent, List<String> differentBeanProperties) {
		if (CollectionUtils.isEmpty(differentBeanProperties)) {
			return false;
		}
		if (differentBeanProperties.size() > 2) {
			return true;
		}
		List<String> propertiesInViolation = new ArrayList<>(differentBeanProperties);
		if (securityEvent.getSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
			Collection<AccountingEventJournalDetail> eventJournalDetails = getReferencedAccountingEventJournalDetails(securityEvent, originalSecurityEvent);
			boolean hasReversals = CollectionUtils.getStream(eventJournalDetails)
					.map(AccountingEventJournalDetail::getJournal)
					.filter(j -> j.getJournalType().getAccrualReversalAccount() != null)
					.map(AccountingEventJournal::getAccrualReversalBookingDate)
					.anyMatch(Objects::nonNull);
			if (!hasReversals) {
				propertiesInViolation.remove(EVENT_ACTUAL_SETTLEMENT_DATE_PROPERTY_NAME);
			}
		}
		if (securityEvent.getStatus().isEventJournalAllowed()) {
			propertiesInViolation.remove(EVENT_STATUS_PROPERTY_NAME);
		}
		return !propertiesInViolation.isEmpty();
	}


	@Override
	protected void bookedAccountingEventJournalViolationOnUpdate(InvestmentSecurityEvent securityEvent, List<String> differentBeanProperties, List<AccountingEventJournal> bookedJournalList) {
		throw new ValidationException("Cannot update " + differentBeanProperties + " fields for security event that is used by booked event journal: " + CollectionUtils.getFirstElementStrict(bookedJournalList).getLabel());
	}


	@Override
	protected String[] getBeanUpdateNotEqualExcludedProperties() {
		return new String[]{"label"};
	}


	@Override
	protected InvestmentSecurityEvent getInvestmentSecurityEventForBean(InvestmentSecurityEvent securityEvent) {
		return securityEvent;
	}


	@Override
	protected Collection<AccountingEventJournalDetail> getReferencedAccountingEventJournalDetails(InvestmentSecurityEvent securityEvent, InvestmentSecurityEvent originalSecurityEvent) {
		AccountingEventJournalDetailSearchForm searchForm = new AccountingEventJournalDetailSearchForm();
		searchForm.setSecurityEventId(securityEvent.getId());
		return getAccountingEventJournalService().getAccountingEventJournalDetailList(searchForm);
	}
}
