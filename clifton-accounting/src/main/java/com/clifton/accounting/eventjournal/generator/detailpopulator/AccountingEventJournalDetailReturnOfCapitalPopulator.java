package com.clifton.accounting.eventjournal.generator.detailpopulator;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>AccountingEventJournalDetailReturnOfCapitalPopulator</code> class calculates old and new cost basis and fx rate to base.
 *
 * @author michaelm
 */
public class AccountingEventJournalDetailReturnOfCapitalPopulator implements AccountingEventJournalDetailPopulator {

	@Override
	public boolean populate(PopulatorCommand command) {
		AccountingEventJournalDetail.ReturnOfCapital returnOfCapital = command.getJournalDetail().forReturnOfCapital();
		EventData eventData = command.getEventData();
		returnOfCapital.setPaymentAmount(calculatePaymentAmount(returnOfCapital, eventData))
				.setOldCostBasis(command.getRemainingCostBasis())
				.setPrice(eventData.getAfterEventValue())
				.setNewCostBasis(getNewPositionCostBasis(returnOfCapital));
		return false;
	}


	private BigDecimal getNewPositionCostBasis(AccountingEventJournalDetail.ReturnOfCapital returnOfCapital) {
		return MathUtils.subtract(returnOfCapital.getOldCostBasis(), returnOfCapital.getPaymentAmount());
	}


	private BigDecimal calculatePaymentAmount(AccountingEventJournalDetail.ReturnOfCapital returnOfCapital, EventData eventData) {
		InvestmentNotionalCalculatorTypes notionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(eventData.getSecurity());
		return notionalCalculator.round(eventData.getAfterEventValue().multiply(returnOfCapital.getDetail().getAffectedQuantity()));
	}
}
