package com.clifton.accounting.eventjournal.generator.detailpopulator;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;

import java.math.BigDecimal;


/**
 * The <code>AccountingEventJournalDetailPriceConstantPopulator</code> class populates TransactionPrice to the specified constant.
 * For example, all Bonds use 100 for maturity price.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailPriceConstantPopulator implements AccountingEventJournalDetailPopulator {

	private BigDecimal price;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		if (getPrice() == null) {
			throw new IllegalStateException("The price cannot be null.");
		}
		AccountingEventJournalDetail detail = command.getJournalDetail();
		detail.setTransactionPrice(getPrice());

		if (command.isSecurityMaturityEvent()) {
			detail.forMaturity().setOpeningPrice(command.getOpeningPrice());
		}

		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
