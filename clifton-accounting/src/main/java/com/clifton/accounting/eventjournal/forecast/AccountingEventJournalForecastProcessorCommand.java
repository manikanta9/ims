package com.clifton.accounting.eventjournal.forecast;


import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;

import java.util.Date;


/**
 * The <code>AccountingEventJournalForecastProcessorCommand</code> defines parameters and stores results
 * for processing event journal forecast records for specified parameters
 *
 * @author manderson
 */
public class AccountingEventJournalForecastProcessorCommand {

	/**
	 * Rebuild forecasts starting with events on this date
	 */
	private Date startDate;

	/**
	 * Rebuild forecasts ending with events on this date
	 */
	private Date endDate;

	/**
	 * When doing full rebuild of history, we delete anything before the start date - usually starts today
	 * but allow to be configurable in case we want to allow users to rebuild a specific date
	 * and anything after end date
	 */
	private boolean deleteEventsBeforeStartDate;
	private boolean deleteEventsAfterEndDate;

	////////////////////////////////////////////////////////////////////////////

	private StatusHolderObject<Status> statusHolder;

	// Processing Results
	private int eventCount = 0;
	private int forecastCount = 0;
	private int errorCount = 0;

	private StringBuilder failedEventMessage;


	public void incrementEventCount(int count) {
		this.eventCount = this.eventCount + count;
	}


	public void incrementForecastCount(int count) {
		this.forecastCount = this.forecastCount + count;
	}


	public void incrementErrorCount() {
		this.errorCount = this.errorCount + 1;
	}


	public void addFailedEventMessage(String message) {
		if (this.failedEventMessage == null) {
			this.failedEventMessage = new StringBuilder(message);
		}
		else {
			this.failedEventMessage.append(message);
		}
	}


	public String getFailedEventMessageString() {
		if (this.failedEventMessage == null) {
			return null;
		}
		return this.failedEventMessage.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isDeleteEventsBeforeStartDate() {
		return this.deleteEventsBeforeStartDate;
	}


	public void setDeleteEventsBeforeStartDate(boolean deleteEventsBeforeStartDate) {
		this.deleteEventsBeforeStartDate = deleteEventsBeforeStartDate;
	}


	public boolean isDeleteEventsAfterEndDate() {
		return this.deleteEventsAfterEndDate;
	}


	public void setDeleteEventsAfterEndDate(boolean deleteEventsAfterEndDate) {
		this.deleteEventsAfterEndDate = deleteEventsAfterEndDate;
	}


	public int getEventCount() {
		return this.eventCount;
	}


	public int getForecastCount() {
		return this.forecastCount;
	}


	public int getErrorCount() {
		return this.errorCount;
	}


	public StringBuilder getFailedEventMessage() {
		return this.failedEventMessage;
	}


	public StatusHolderObject<Status> getStatusHolder() {
		return this.statusHolder;
	}


	public void setStatusHolder(StatusHolderObject<Status> statusHolder) {
		this.statusHolder = statusHolder;
	}
}
