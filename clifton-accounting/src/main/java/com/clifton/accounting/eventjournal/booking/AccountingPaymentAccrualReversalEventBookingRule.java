package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.eventjournal.EventJournalUtils;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingPaymentAccrualReversalEventBookingRule</code> class is a booking rule that reverses previously
 * accrued payments and replaces them with the proper GL Account (usually Cash or Currency).
 *
 * @author vgomelsky
 */
public class AccountingPaymentAccrualReversalEventBookingRule extends AccountingPaymentAccrualEventBookingRule {

	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();

		Date transactionDate = getTransactionDate(eventJournal, (eventJournal.getJournalType().getAccrualReversalAccount() != null));
		for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(EventJournalUtils.getCurrencyPaymentDetailList(eventJournal))) {
			BigDecimal frankingCreditTransactionAmount = getFrankingCreditPayoutAmount(detail);
			detail.setTransactionAmount(MathUtils.subtract(detail.getTransactionAmount(), frankingCreditTransactionAmount));
			journal.addJournalDetail(generateJournalDetail(journal, detail, true, transactionDate));
			journal.addJournalDetail(generateJournalDetail(journal, detail, false, transactionDate));
		}
	}


	@Override
	protected AccountingJournalDetail generateJournalDetail(AccountingJournal journal, AccountingEventJournalDetail eventJournalDetail, boolean debit, Date transactionDate) {
		AccountingJournalDetail detail = super.generateJournalDetail(journal, eventJournalDetail, true, transactionDate);

		AccountingEventJournal eventJournal = eventJournalDetail.getJournal();
		AccountingEventJournalType eventJournalType = eventJournal.getJournalType();
		EventData eventData = EventData.forEventPayoutOrEvent(eventJournalDetail.getEventPayout(), eventJournal.getSecurityEvent());

		if (debit) {
			// reverse the accrual amounts on reversal
			detail.setOpening(false);
			if (detail.getQuantity() != null) {
				detail.setQuantity(detail.getQuantity().negate());
			}
			detail.setLocalDebitCredit(detail.getLocalDebitCredit().negate());
			detail.setBaseDebitCredit(detail.getBaseDebitCredit().negate());
			detail.setDescription("Reverse accrual of " + eventJournalType.getEventType().getName() + " for " + eventJournalDetail.getJournal().getSecurityEvent().getSecurity().getSymbol() +
					" from " + DateUtils.fromDateShort(eventData.getDayBeforeExDate()));
		}
		else {
			// use Currency GL account for foreign payments
			InvestmentSecurity settlementCurrency = detail.getInvestmentSecurity();
			InvestmentSecurity baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(detail);
			if (settlementCurrency.equals(baseCurrency)) {
				detail.setAccountingAccount(eventJournalType.getAccrualReversalAccount());
			}
			else {
				detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
			}
			if (isCashLocationUsed()) {
				detail.setHoldingInvestmentAccount(getInvestmentSecurityUtilHandler().getCashLocationHoldingAccount(detail));
			}

			// no quantity or price for cash/currency
			detail.setQuantity(null);
			detail.setPrice(null);
			detail.setDescription(eventJournalDetail.getLabel());
		}

		// With the reversal of the accrual, we can now use payment date instead of event date for settlement, original transaction and transaction dates
		detail.setOriginalTransactionDate(eventData.getPaymentDate());
		detail.setTransactionDate(eventData.getPaymentDate());

		// the transaction date is the actual settlement for reversals (except the equity leg).
		if (eventData.getEvent().getActualSettlementDate() != null && !AccountingEventJournalType.EQUITY_LEG_PAYMENT.equals(eventJournalDetail.getJournal().getJournalType().getName())) {
			detail.setTransactionDate(eventData.getEvent().getActualSettlementDate());
		}

		return detail;
	}


	@Override
	protected BigDecimal processFrankingCreditPayout(AccountingJournal journal, AccountingEventJournalDetail detail, Date transactionDate) {
		return BigDecimal.ZERO;
	}
}
