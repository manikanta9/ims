package com.clifton.accounting.eventjournal.generator;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.Date;


/**
 * Some event types may require Exchange Rate from a very specific date for foreign transactions (Posting Date for Cleared CDS Premium Leg Payment).
 *
 * @author vgomelsky
 */
public enum ExchangeRateDateSelectors {

	/**
	 * Flexible lookup on Event Date (also known as Payment Date).
	 */
	EVENT_DATE_FLEXIBLE {
		@Override
		public Date getExchangeRateDate(InvestmentSecurityEvent event, CalendarBusinessDayService calendarBusinessDayService) {
			return event.getEventDate();
		}


		@Override
		public boolean isFlexibleLookupAllowed(InvestmentSecurityEvent event) {
			return true;
		}
	},

	/**
	 * One business day before Payment Date (Event Date). Requires Exchange Rate on Posting Date.
	 */
	POSTING_DATE {
		@Override
		public Date getExchangeRateDate(InvestmentSecurityEvent event, CalendarBusinessDayService calendarBusinessDayService) {
			// one business day before payment date
			Date baseDate = event.getEventDate();
			InvestmentExchange exchange = InvestmentUtils.getSecurityExchange(event.getSecurity());
			if (exchange != null) {
				if (exchange.getCalendar() != null) {
					return calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(baseDate, exchange.getCalendar().getId()), -1);
				}
			}
			return calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(baseDate), -1);
		}


		@Override
		public boolean isFlexibleLookupAllowed(InvestmentSecurityEvent event) {
			return false;
		}
	},

	/**
	 * One business day before Payment Date (Event Date). Requires Exchange Rate on Posting Date for OTC securities only.
	 * Allows flexible Exchange Rate lookup for non-OTC securities.
	 * <p/>
	 * Cleared Swaps need exact rate lookup while OTC require an early estimate and then may need trade date specific rate.
	 */
	POSTING_DATE_FLEXIBLE_FOR_OTC {
		@Override
		public Date getExchangeRateDate(InvestmentSecurityEvent event, CalendarBusinessDayService calendarBusinessDayService) {
			// one business day before payment date
			Date baseDate = event.getEventDate();
			InvestmentExchange exchange = InvestmentUtils.getSecurityExchange(event.getSecurity());
			if (exchange != null) {
				if (exchange.getCalendar() != null) {
					return calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(baseDate, exchange.getCalendar().getId()), -1);
				}
			}
			return calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(baseDate), -1);
		}


		@Override
		public boolean isFlexibleLookupAllowed(InvestmentSecurityEvent event) {
			return event.getSecurity().getInstrument().getHierarchy().isOtc();
		}
	};


	/**
	 * Returns the Date that must be used for Exchange Rate lookup for foreign transactions for the specified security event.
	 */
	public abstract Date getExchangeRateDate(InvestmentSecurityEvent event, CalendarBusinessDayService calendarBusinessDayService);


	/**
	 * Returns true if it's acceptable to use the latest available Exchange Rate when the rate on the date is not available yet.
	 */
	public abstract boolean isFlexibleLookupAllowed(InvestmentSecurityEvent event);
}
