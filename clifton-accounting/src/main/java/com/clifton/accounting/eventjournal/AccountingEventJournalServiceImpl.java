package com.clifton.accounting.eventjournal;


import com.clifton.accounting.eventjournal.booking.AccountingEventJournalBookingProcessor;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalTypeSearchForm;
import com.clifton.accounting.eventjournal.validation.AccountingEventJournalValidator;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventAction;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionService;
import com.clifton.investment.instrument.event.action.search.InvestmentSecurityEventActionSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The <code>AccountingEventJournalServiceImpl</code> class provides basic implementation of AccountingEventJournalService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingEventJournalServiceImpl implements AccountingEventJournalService {

	private AdvancedUpdatableDAO<AccountingEventJournal, Criteria> accountingEventJournalDAO;
	private AdvancedUpdatableDAO<AccountingEventJournalDetail, Criteria> accountingEventJournalDetailDAO;
	private AdvancedUpdatableDAO<AccountingEventJournalType, Criteria> accountingEventJournalTypeDAO;

	private DaoNamedEntityCache<AccountingEventJournalType> accountingEventJournalTypeCache;

	private AccountingBookingService<AccountingEventJournal> accountingBookingService;
	private AccountingJournalService accountingJournalService;
	private AccountingPostingService accountingPostingService;

	private InvestmentSecurityEventActionService investmentSecurityEventActionService;

	////////////////////////////////////////////////////////////////////////////
	////////         AccountingEventJournal Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public AccountingEventJournal getAccountingEventJournal(int id) {
		AccountingEventJournal journal = getAccountingEventJournalDAO().findByPrimaryKey(id);
		if (journal != null) {
			journal.setDetailList(getAccountingEventJournalDetailDAO().findByField("journal.id", id));
		}
		return journal;
	}


	@Override
	public List<AccountingEventJournal> getAccountingEventJournalList(final AccountingEventJournalSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getClientInvestmentAccountId() != null) {
					// if exist journal detail for the specified client account
					DetachedCriteria sub = DetachedCriteria.forClass(AccountingEventJournalDetail.class, "link");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.eqProperty("link.journal.id", criteria.getAlias() + ".id"));
					sub.createAlias("accountingTransaction", "at");
					sub.add(Restrictions.eq("at.clientInvestmentAccount.id", searchForm.getClientInvestmentAccountId()));
					criteria.add(Subqueries.exists(sub));
				}
			}
		};

		return getAccountingEventJournalDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	@Transactional
	public AccountingEventJournal adjustAccountingEventJournal(AccountingEventJournal bean) {
		// 1. unpost the specified event journal (can have up to 2 journals to unpost: accrual and reversal)
		int postCount = 0;
		String tableName = getAccountingEventJournalDAO().getConfiguration().getTableName();
		AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence(tableName, bean.getId(), 2);
		if (journal != null) {
			// unpost and unbook accrual reversal first
			getAccountingPostingService().unpostAccountingJournal(journal.getId());
			postCount++;
		}
		journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence(tableName, bean.getId(), 1);
		if (journal != null) {
			getAccountingPostingService().unpostAccountingJournal(journal.getId());
			postCount++;
		}

		// 2. update appropriate event journal detail fields
		AccountingEventJournal eventJournal = getAccountingEventJournal(bean.getId());
		eventJournal.setDescription(bean.getDescription());
		AccountingEventJournalType journalType = eventJournal.getJournalType();
		for (AccountingEventJournalDetail newDetail : bean.getDetailList()) {
			AccountingEventJournalDetail oldDetail = null;
			for (AccountingEventJournalDetail detail : eventJournal.getDetailList()) {
				if (detail.equals(newDetail)) {
					oldDetail = detail;
					break;
				}
			}
			if (oldDetail == null) {
				throw new ValidationException("Cannot find event journal detail: " + newDetail + " in event journal: " + eventJournal);
			}
			oldDetail.setExchangeRateToBase(newDetail.getExchangeRateToBase());
			if (journalType.isAffectedCostAdjustable()) {
				oldDetail.setAffectedCost(newDetail.getAffectedCost());
			}
			if (journalType.isAffectedQuantityAdjustable()) {
				oldDetail.setAffectedQuantity(newDetail.getAffectedQuantity());
			}
			if (journalType.isTransactionAmountAdjustable()) {
				oldDetail.setTransactionAmount(newDetail.getTransactionAmount());
			}
			if (journalType.isTransactionPriceAdjustable()) {
				oldDetail.setTransactionPrice(newDetail.getTransactionPrice());
			}
			if (journalType.isAdditionalAmountAdjustable()) {
				oldDetail.setAdditionalAmount(newDetail.getAdditionalAmount());
			}
			if (journalType.isAdditionalAmount2Adjustable()) {
				oldDetail.setAdditionalAmount2(newDetail.getAdditionalAmount2());
			}
			if (journalType.isAdditionalAmount3Adjustable()) {
				oldDetail.setAdditionalAmount3(newDetail.getAdditionalAmount3());
			}
		}
		saveAccountingEventJournal(eventJournal);

		// 3. rebook/repost event journal
		for (int i = 0; i < postCount; i++) {
			getAccountingBookingService().bookAccountingJournal(AccountingEventJournalBookingProcessor.JOURNAL_NAME, eventJournal.getId(), true);
		}
		return eventJournal;
	}


	/**
	 * @see AccountingEventJournalValidator
	 */
	@Override
	@Transactional
	public AccountingEventJournal saveAccountingEventJournal(AccountingEventJournal journal) {
		List<AccountingEventJournalDetail> journalDetailList = journal.getDetailList();
		journal = getAccountingEventJournalDAO().save(journal);
		getAccountingEventJournalDetailDAO().saveList(journalDetailList);
		journal.setDetailList(journalDetailList);
		return journal;
	}


	/**
	 * @see AccountingEventJournalValidator
	 */
	@Override
	@Transactional
	public void deleteAccountingEventJournal(int id) {
		AccountingEventJournal journal = getAccountingEventJournal(id);

		// make sure no after event actions exist that must be rolled back first
		if (journal.getSecurityEvent().getType().isActionAllowed()) {
			InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
			searchForm.setSecurityEventId(journal.getSecurityEvent().getId());
			searchForm.setProcessBeforeEventJournal(false);
			List<InvestmentSecurityEventAction> actionList = getInvestmentSecurityEventActionService().getInvestmentSecurityEventActionList(searchForm);
			if (!CollectionUtils.isEmpty(actionList)) {
				throw new ValidationException("After Event actions for the corresponding Investment Security Event must be rolled back before the event journal can be deleted: " + actionList);
			}
		}

		getAccountingEventJournalDetailDAO().deleteList(journal.getDetailList());
		getAccountingEventJournalDAO().delete(journal);
	}


	/**
	 * @see AccountingEventJournalValidator
	 */
	@Override
	@Transactional
	public void deleteAccountingEventJournalWithForcedUnposting(int id) {
		AccountingEventJournal eventJournal = getAccountingEventJournal(id);

		if (eventJournal.getAccrualReversalBookingDate() != null) {
			AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence(AccountingEventJournal.class.getSimpleName(), id, 2);
			if (journal != null) {
				getAccountingPostingService().unpostAccountingJournal(journal.getId());
			}
		}
		if (eventJournal.getBookingDate() != null) {
			AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence(AccountingEventJournal.class.getSimpleName(), id, 1);
			getAccountingPostingService().unpostAccountingJournal(journal.getId());
		}

		deleteAccountingEventJournal(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////     AccountingEventJournalDetail Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingEventJournalDetail getAccountingEventJournalDetail(int id) {
		return getAccountingEventJournalDetailDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingEventJournalDetail> getAccountingEventJournalDetailList(final AccountingEventJournalDetailSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			private Boolean journalNotBooked;
			private Boolean journalBooked;


			@Override
			protected void configureSearchForm() {
				if (Boolean.TRUE.equals(searchForm.getIncludeAccrualReceivables())) {
					this.journalNotBooked = searchForm.getJournalNotBooked();
					this.journalBooked = searchForm.getJournalBooked();

					searchForm.removeSearchRestrictionAndBeanField("journalNotBooked");
					searchForm.removeSearchRestrictionAndBeanField("journalBooked");
				}
				super.configureSearchForm();
			}


			@Override
			public void configureCriteria(Criteria criteria) {
				// change defaults for journal not booked
				if (Boolean.TRUE.equals(searchForm.getIncludeAccrualReceivables())) {
					if (getJournalBooked() != null || getJournalNotBooked() != null) {
						String journalAlias = getPathAlias("journal", criteria, JoinType.INNER_JOIN);
						String hierarchyAlias = getPathAlias("journal.securityEvent.security.instrument.hierarchy", criteria, JoinType.INNER_JOIN);
						Junction junction = Restrictions.disjunction(Restrictions.isNull(journalAlias + ".bookingDate"))
								.add(
										Restrictions.conjunction(
												Restrictions.isNull(journalAlias + ".accrualReversalBookingDate"),
												Restrictions.eq(hierarchyAlias + ".includeAccrualReceivables", true)
										)
								);
						if (Boolean.TRUE.equals(getJournalNotBooked()) || Boolean.FALSE.equals(getJournalBooked())) {
							criteria.add(junction);
						}
						else {
							criteria.add(Restrictions.not(junction));
						}
					}
				}

				super.configureCriteria(criteria);

				if (searchForm.getHoldingInvestmentAccountGroupId() != null || searchForm.getHoldingInvestmentAccountGroupIds() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "hag");
					sub.setProjection(Projections.property("id"));
					if (searchForm.getHoldingInvestmentAccountGroupId() != null) {
						sub.add(Restrictions.eq("referenceOne.id", searchForm.getHoldingInvestmentAccountGroupId()));
					}
					else {
						sub.add(Restrictions.in("referenceOne.id", searchForm.getHoldingInvestmentAccountGroupIds()));
					}
					sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("accountingTransaction.holdingInvestmentAccount", criteria) + ".id"));
					criteria.add(Subqueries.exists(sub));
				}

				if (searchForm.getExcludeHoldingInvestmentAccountGroupId() != null || searchForm.getExcludeHoldingInvestmentAccountGroupIds() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "eha");
					sub.setProjection(Projections.property("id"));
					if (searchForm.getExcludeHoldingInvestmentAccountGroupId() != null) {
						sub.add(Restrictions.eq("referenceOne.id", searchForm.getExcludeHoldingInvestmentAccountGroupId()));
					}
					else {
						sub.add(Restrictions.in("referenceOne.id", searchForm.getExcludeHoldingInvestmentAccountGroupIds()));
					}
					sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("accountingTransaction.holdingInvestmentAccount", criteria) + ".id"));
					criteria.add(Subqueries.notExists(sub));
				}

				if (searchForm.getAccrualPostingOnly() != null) {
					String journalAlias = getPathAlias("journal", criteria, JoinType.INNER_JOIN);
					String hierarchyAlias = getPathAlias("journal.securityEvent.security.instrument.hierarchy", criteria, JoinType.INNER_JOIN);
					Junction junction = Restrictions.conjunction(
							Restrictions.isNotNull(journalAlias + ".bookingDate"),
							Restrictions.isNull(journalAlias + ".accrualReversalBookingDate"),
							Restrictions.eq(hierarchyAlias + ".includeAccrualReceivables", true)
					);
					if (Boolean.TRUE.equals(searchForm.getAccrualPostingOnly())) {
						criteria.add(junction);
					}
					else {
						criteria.add(Restrictions.not(junction));
					}
				}
			}


			public Boolean getJournalNotBooked() {
				return this.journalNotBooked;
			}


			public Boolean getJournalBooked() {
				return this.journalBooked;
			}
		};
		return getAccountingEventJournalDetailDAO().findBySearchCriteria(config);
	}


	// Called from re-processing existing events after updating the security event when journal details exist but haven't been booked yet
	@Override
	public void saveAccountingEventJournalDetailList(List<AccountingEventJournalDetail> beanList) {
		getAccountingEventJournalDetailDAO().saveList(beanList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////      AccountingEventJournalType Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingEventJournalType getAccountingEventJournalType(short id) {
		return getAccountingEventJournalTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingEventJournalType getAccountingEventJournalTypeByName(String name) {
		return getAccountingEventJournalTypeCache().getBeanForKeyValueStrict(getAccountingEventJournalTypeDAO(), name);
	}


	@Override
	public List<AccountingEventJournalType> getAccountingEventJournalTypeListByEventType(short eventTypeId) {
		return getAccountingEventJournalTypeDAO().findByField("eventType.id", eventTypeId);
	}


	@Override
	public List<AccountingEventJournalType> getAccountingEventJournalTypeList(AccountingEventJournalTypeSearchForm searchForm) {
		return getAccountingEventJournalTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<AccountingEventJournalType> getAccountingEventJournalTypeList() {
		return getAccountingEventJournalTypeDAO().findAll();
	}


	@Override
	public AccountingEventJournalType saveAccountingEventJournalType(AccountingEventJournalType bean) {
		return getAccountingEventJournalTypeDAO().save(bean);
	}


	@Override
	public void deleteAccountingEventJournalType(short id) {
		getAccountingEventJournalTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingEventJournalType, Criteria> getAccountingEventJournalTypeDAO() {
		return this.accountingEventJournalTypeDAO;
	}


	public void setAccountingEventJournalTypeDAO(AdvancedUpdatableDAO<AccountingEventJournalType, Criteria> accountingEventJournalTypeDAO) {
		this.accountingEventJournalTypeDAO = accountingEventJournalTypeDAO;
	}


	public AdvancedUpdatableDAO<AccountingEventJournal, Criteria> getAccountingEventJournalDAO() {
		return this.accountingEventJournalDAO;
	}


	public void setAccountingEventJournalDAO(AdvancedUpdatableDAO<AccountingEventJournal, Criteria> accountingEventJournalDAO) {
		this.accountingEventJournalDAO = accountingEventJournalDAO;
	}


	public AdvancedUpdatableDAO<AccountingEventJournalDetail, Criteria> getAccountingEventJournalDetailDAO() {
		return this.accountingEventJournalDetailDAO;
	}


	public void setAccountingEventJournalDetailDAO(AdvancedUpdatableDAO<AccountingEventJournalDetail, Criteria> accountingEventJournalDetailDAO) {
		this.accountingEventJournalDetailDAO = accountingEventJournalDetailDAO;
	}


	public AccountingBookingService<AccountingEventJournal> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<AccountingEventJournal> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}


	public DaoNamedEntityCache<AccountingEventJournalType> getAccountingEventJournalTypeCache() {
		return this.accountingEventJournalTypeCache;
	}


	public void setAccountingEventJournalTypeCache(DaoNamedEntityCache<AccountingEventJournalType> accountingEventJournalTypeCache) {
		this.accountingEventJournalTypeCache = accountingEventJournalTypeCache;
	}


	public InvestmentSecurityEventActionService getInvestmentSecurityEventActionService() {
		return this.investmentSecurityEventActionService;
	}


	public void setInvestmentSecurityEventActionService(InvestmentSecurityEventActionService investmentSecurityEventActionService) {
		this.investmentSecurityEventActionService = investmentSecurityEventActionService;
	}
}
