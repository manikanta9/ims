package com.clifton.accounting.eventjournal;


import com.clifton.accounting.eventjournal.booking.AccountingEventJournalBookingProcessor;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommandInternal;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventStatus;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentSecurityEventProcessingJob</code> class generates accounting event journals that fall in
 * the specified date range.  It immediately posts generated journals if the date has come and autoPost = true.
 *
 * @author Mary Anderson
 */
public class AccountingEventJournalProcessorJob implements Task, StatusHolderObjectAware<Status> {

	/**
	 * Generate event journals with Event Date from this number of days in past from today to today.
	 */
	private Integer daysBack = 1;

	private boolean includeEventsForToday;

	/**
	 * Bond maturity needs to be instructed one day before maturity to avoid overdraft and match corresponding new security payment (bond roll).
	 * We should never be closing bond positions the day before maturity (bad pricing). Just in case, a compliance rule will catch trading attempts.
	 */
	private boolean bookBondMaturityTheDayBeforeExDate;

	/**
	 * Allows swap resets to be posted on the valuation date.
	 */
	private boolean bookResetsOnValuationDate;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////

	private InvestmentSecurityEventService investmentSecurityEventService;
	private AccountingBookingService<AccountingEventJournal> accountingBookingService;
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;
	private AccountingEventJournalService accountingEventJournalService;

	private StatusHolderObject<Status> statusHolder;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Date today = DateUtils.clearTime(new Date());
		Date endDate = today;
		Date startDate = DateUtils.addDays(endDate, -getDaysBack());
		if (!isIncludeEventsForToday()) {
			endDate = DateUtils.addDays(endDate, -1);
		}
		Date currentDate = startDate;

		StatusPostCounts counts = new StatusPostCounts(this.statusHolder.getStatus());
		while (DateUtils.isDateBetween(currentDate, startDate, endDate, false)) {

			// 1. get all events on the specified date
			// 2. process each event
			generateBookAndPostEvents(counts, currentDate, today);

			// 3. post reset events
			generateBookAndPostResetEvents(counts, currentDate);

			// 4. post unbooked accrual reversal event journals when event date comes
			bookAndPostAccrualReversalEvents(counts, currentDate);

			// Process Next Day
			currentDate = DateUtils.addDays(currentDate, 1);
		}

		// 5. also find all events created within the specified date range (factor changes for delayed payment bonds, etc)
		generateAndPostPastEvents(counts, startDate, today);
		counts.addTotalProcessStatus();

		return this.statusHolder.getStatus();
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private boolean isEventJournalGenerationAllowed(InvestmentSecurityEvent event, Date today) {
		if (event.getDayBeforeExDate().before(today)) {
			return true;
		}

		if (isBookBondMaturityTheDayBeforeExDate() && InvestmentSecurityEventType.SECURITY_MATURITY.equals(event.getType().getName())) {
			InvestmentInstrumentHierarchy hierarchy = event.getSecurity().getInstrument().getHierarchy();
			if (!hierarchy.isTradingOnEndDateAllowed() && InvestmentType.BONDS.equals(hierarchy.getInvestmentType().getName())) {
				return true;
			}
		}

		return false;
	}


	private void generateBookAndPostEvents(StatusPostCounts counts, Date date, Date today) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setStatusName(InvestmentSecurityEventStatus.STATUS_APPROVED);
		searchForm.setDayBeforeExOrEventDate(date);
		searchForm.setOrderBy("typeId"); // TypeId filter orders by eventOrder field

		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		counts.incrementEventCount(CollectionUtils.getSize(eventList));

		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {
			if (isEventJournalGenerationAllowed(event, today)) {
				try {
					List<AccountingEventJournal> journalList = getAccountingEventJournalGeneratorService().generateAccountingEventJournal(EventJournalGeneratorCommandInternal.forEvent(event, AccountingEventJournalGeneratorTypes.GENERATE, true));
					for (AccountingEventJournal journal : CollectionUtils.getIterable(journalList)) {
						if (journal.getJournalType().isAutoPost()) {
							getAccountingBookingService().bookAccountingJournal(AccountingEventJournalBookingProcessor.JOURNAL_NAME, journal.getId(), true);
							counts.incrementBookedCount();
						}
					}
				}
				catch (Exception e) {
					counts.incrementErrorCount();
					counts.addErrorMessage(event.getLabel() + ": " + e.getMessage());
					LogUtils.error(getClass(), "Error processing event " + event.getLabel(), e);
				}
			}
			counts.addEventStatusMessage();
		}
	}


	private void bookAndPostAccrualReversalEvents(StatusPostCounts counts, Date date) {
		AccountingEventJournalSearchForm eventJournalSearchForm = new AccountingEventJournalSearchForm();
		eventJournalSearchForm.setJournalBooked(true);
		eventJournalSearchForm.setAccrualReversalNotBooked(true);
		eventJournalSearchForm.setEventDate(date);

		List<AccountingEventJournal> eventJournalList = getAccountingEventJournalService().getAccountingEventJournalList(eventJournalSearchForm);
		for (AccountingEventJournal journal : CollectionUtils.getIterable(eventJournalList)) {
			// also post manually generated journals (autoPost == false) because they were already generated and accrual was posted
			try {
				getAccountingBookingService().bookAccountingJournal(AccountingEventJournalBookingProcessor.JOURNAL_NAME, journal.getId(), true);
				counts.incrementAccrualReversalCount();
			}
			catch (Exception e) {
				counts.incrementAccrualReversalErrorCount();
				counts.addErrorMessage(journal.getLabel() + ": " + e.getMessage());
				LogUtils.error(getClass(), "Error posting accounting journal " + journal.getLabel(), e);
			}
			counts.addAccrualReversalStatusMessage();
		}
	}


	private void generateBookAndPostResetEvents(StatusPostCounts counts, Date date) {
		if (isBookResetsOnValuationDate()) {
			List<InvestmentSecurityEvent> resetEvents = getResetEventList(date);
			for (InvestmentSecurityEvent resetEvent : CollectionUtils.getIterable(resetEvents)) {
				try {
					List<AccountingEventJournal> journalList = getAccountingEventJournalGeneratorService().generateAccountingEventJournal(EventJournalGeneratorCommandInternal.forEvent(resetEvent, AccountingEventJournalGeneratorTypes.POST, false));
					counts.incrementResetEventCount(CollectionUtils.getSize(journalList));
				}
				catch (Exception e) {
					counts.incrementResetEventErrorCount();
					counts.addErrorMessage(resetEvent.getLabel() + ": " + e.getMessage());
					LogUtils.error(getClass(), "Error processing reset event " + resetEvent.getLabel(), e);
				}
				counts.addResetEventStatusMessage();
			}
		}
	}


	private void generateAndPostPastEvents(StatusPostCounts counts, Date startDate, Date today) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setStatusName(InvestmentSecurityEventStatus.STATUS_APPROVED);
		searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.LESS_THAN_OR_EQUALS, DateUtils.getEndOfDay(today)));
		searchForm.setOrderBy("typeId"); // TypeId filter orders by eventOrder field

		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {
			if (event.getDayBeforeExDate().before(today)) {
				// time to execute this event
				try {
					List<AccountingEventJournal> journalList = getAccountingEventJournalGeneratorService().generateAccountingEventJournal(EventJournalGeneratorCommandInternal.forEvent(event, AccountingEventJournalGeneratorTypes.POST, true));
					counts.incrementPastEventCount(CollectionUtils.getSize(journalList));
				}
				catch (Exception e) {
					counts.incrementPastEventErrorCount();
					counts.addErrorMessage(event.getLabel() + ": " + e.getMessage());
					LogUtils.error(getClass(), "Error processing event " + event.getLabel(), e);
				}
				counts.addPastEventStatusMessage();
			}
		}
	}


	private List<InvestmentSecurityEvent> getResetEventList(Date recordDate) {
		List<InvestmentSecurityEvent> results = new ArrayList<>();

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setStatusName(InvestmentSecurityEventStatus.STATUS_APPROVED);
		searchForm.setRecordDate(recordDate);
		searchForm.setTypeName(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT);
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		for (InvestmentSecurityEvent equityEvent : eventList) {
			InvestmentInstrumentHierarchy hierarchy = equityEvent.getSecurity().getInstrument().getHierarchy();
			if (hierarchy.isIncludeAccrualReceivables()) {
				List<InvestmentSecurityEventType> typeList = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeListByHierarchy(hierarchy.getId());
				for (InvestmentSecurityEventType type : typeList) {
					if (!InvestmentSecurityEventType.EQUITY_LEG_PAYMENT.equals(type.getName())) {
						InvestmentSecurityEvent legEvent = getMatchingResetLegEvent(equityEvent, type);
						results.add(legEvent);
					}
				}
				results.add(equityEvent);
			}
		}
		results.sort(Comparator.comparingInt(o -> o.getType().getEventOrder()));
		return results;
	}


	private InvestmentSecurityEvent getMatchingResetLegEvent(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventType eventType) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(securityEvent.getSecurity().getId());
		searchForm.setEventDate(securityEvent.getEventDate());
		searchForm.setTypeId(eventType.getId());
		List<InvestmentSecurityEvent> securityEventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);

		ValidationUtils.assertNotEmpty(securityEventList, "Cannot find '" + eventType.getName() + " for " + securityEvent.getSecurity().getLabel() + " on " + DateUtils.fromDateShort(securityEvent.getEventDate()));
		ValidationUtils.assertTrue(securityEventList.size() == 1, "Cannot have more than one '" + eventType.getName() + " for " + securityEvent.getSecurity().getLabel() + " on " + DateUtils.fromDateShort(securityEvent.getEventDate()));

		return securityEventList.get(0);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class StatusPostCounts {

		private int eventCount = 0;
		private int bookedCount = 0;
		private int errorCount = 0;

		private int accrualReversalCount = 0;
		private int accrualReversalErrorCount = 0;

		private int resetEventCount = 0;
		private int resetEventErrorCount = 0;

		private int pastEventCount = 0;
		private int pastEventErrorCount = 0;

		private final Status status;


		private StatusPostCounts(Status status) {
			this.status = status;
		}


		public void addEventStatusMessage() {
			this.status.setMessage("Events " + this.eventCount + "; Booked " + this.bookedCount + "; Failed " + this.errorCount);
		}


		public void addAccrualReversalStatusMessage() {
			this.status.setMessage("Events " + this.eventCount + "; Booked " + this.bookedCount + "; Reversals " + this.accrualReversalCount + "; Failed " + this.errorCount
					+ "; Failed Reversals " + this.accrualReversalErrorCount);
		}


		public void addResetEventStatusMessage() {
			this.status.setMessage("Events " + this.eventCount + "; Booked " + this.bookedCount + "; Reversals " + this.accrualReversalCount + "; Reset Events " + this.resetEventCount
					+ "; Failed " + this.errorCount + "; Failed Reversals " + this.accrualReversalErrorCount + "; Failed Resets " + this.resetEventErrorCount);
		}


		public void addPastEventStatusMessage() {
			this.status.setMessage("Events " + this.eventCount + "; Booked " + this.bookedCount + "; Reversals " + this.accrualReversalCount + "; Reset Events " + this.resetEventCount
					+ "; Past Events " + this.pastEventCount + "; Failed " + this.errorCount + "; Failed Reversals " + this.accrualReversalErrorCount + "; Failed Resets "
					+ this.resetEventErrorCount + "; Failed Past Events " + this.pastEventErrorCount);
		}


		public void addTotalProcessStatus() {
			this.status.setMessage("Events Found: " + this.eventCount + "\nEvent Journals Booked: " + this.bookedCount + "\nAccrual Reversals Posted: " + this.accrualReversalCount
					+ "\nReset Event Count: " + this.resetEventCount + "\nPast Event Count: " + this.pastEventCount + "\nFailed Event Count: " + this.errorCount
					+ "\nFailed Reversal Count: " + this.accrualReversalErrorCount + "\nFailed Reset Count: " + this.resetEventErrorCount + "\nFailed Past Event Count: " + this.pastEventErrorCount);
		}


		public void addErrorMessage(String message) {
			this.status.addError(message);
		}


		public void incrementEventCount(int count) {
			this.eventCount = this.eventCount + count;
		}


		public void incrementBookedCount() {
			this.bookedCount++;
		}


		public void incrementErrorCount() {
			this.errorCount++;
		}


		public void incrementAccrualReversalCount() {
			this.accrualReversalCount++;
		}


		public void incrementAccrualReversalErrorCount() {
			this.accrualReversalErrorCount++;
		}


		public void incrementResetEventCount(int count) {
			this.resetEventCount = this.resetEventCount + count;
		}


		public void incrementResetEventErrorCount() {
			this.resetEventErrorCount++;
		}


		public void incrementPastEventCount(int count) {
			this.pastEventCount = this.pastEventCount + count;
		}


		public void incrementPastEventErrorCount() {
			this.pastEventErrorCount++;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDaysBack() {
		return this.daysBack;
	}


	public void setDaysBack(Integer daysBack) {
		this.daysBack = daysBack;
	}


	public boolean isIncludeEventsForToday() {
		return this.includeEventsForToday;
	}


	public void setIncludeEventsForToday(boolean includeEventsForToday) {
		this.includeEventsForToday = includeEventsForToday;
	}


	public boolean isBookBondMaturityTheDayBeforeExDate() {
		return this.bookBondMaturityTheDayBeforeExDate;
	}


	public void setBookBondMaturityTheDayBeforeExDate(boolean bookBondMaturityTheDayBeforeExDate) {
		this.bookBondMaturityTheDayBeforeExDate = bookBondMaturityTheDayBeforeExDate;
	}


	public boolean isBookResetsOnValuationDate() {
		return this.bookResetsOnValuationDate;
	}


	public void setBookResetsOnValuationDate(boolean bookResetsOnValuationDate) {
		this.bookResetsOnValuationDate = bookResetsOnValuationDate;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public AccountingEventJournalGeneratorService getAccountingEventJournalGeneratorService() {
		return this.accountingEventJournalGeneratorService;
	}


	public void setAccountingEventJournalGeneratorService(AccountingEventJournalGeneratorService accountingEventJournalGeneratorService) {
		this.accountingEventJournalGeneratorService = accountingEventJournalGeneratorService;
	}


	public AccountingBookingService<AccountingEventJournal> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<AccountingEventJournal> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}
}
