package com.clifton.accounting.eventjournal.generator;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The EventJournalGeneratorCommandInternal class wraps EventJournalGeneratorCommand and is intended to be
 * used by internal API's only (not from WEB UI).
 *
 * @author vgomelsky
 */
public class EventJournalGeneratorCommandInternal {

	private EventJournalGeneratorCommand command;
	private InvestmentSecurityEvent securityEvent;
	private BigDecimal overrideAmount;
	private StringBuilder skipMessage;
	private boolean autoOnly;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private EventJournalGeneratorCommandInternal() {
		// use static constructors instead
	}


	public static EventJournalGeneratorCommandInternal forCommand(EventJournalGeneratorCommand command, InvestmentSecurityEvent securityEvent) {
		ValidationUtils.assertNotNull(command, "Event Journal Generator Command is required");
		ValidationUtils.assertNotNull(securityEvent, "Investment Security Event is required");
		EventJournalGeneratorCommandInternal result = new EventJournalGeneratorCommandInternal();
		result.command = command;
		result.securityEvent = securityEvent;

		return result;
	}


	public static EventJournalGeneratorCommandInternal forEvent(InvestmentSecurityEvent securityEvent, AccountingEventJournalGeneratorTypes generatorType) {
		ValidationUtils.assertNotNull(securityEvent, "Investment Security Event is required");
		ValidationUtils.assertNotNull(generatorType, "Accounting Event Journal Generator Type is required");
		EventJournalGeneratorCommandInternal result = new EventJournalGeneratorCommandInternal();
		result.securityEvent = securityEvent;
		result.command = new EventJournalGeneratorCommand();
		result.command.setGeneratorType(generatorType);

		return result;
	}


	public static EventJournalGeneratorCommandInternal forEvent(InvestmentSecurityEvent securityEvent, AccountingEventJournalGeneratorTypes generatorType, boolean autoOnly) {
		EventJournalGeneratorCommandInternal result = forEvent(securityEvent, generatorType);
		result.autoOnly = autoOnly;

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void updateMessages() {
		if (StringUtils.isEmpty(this.command.getResultMessages()) && getSkipMessage() != null) {
			this.command.setResultMessages(getSkipMessage().toString());
		}
	}


	public EventJournalGeneratorCommandInternal withOverrideAmount(BigDecimal overrideAmount) {
		this.overrideAmount = overrideAmount;
		return this;
	}


	public EventJournalGeneratorCommandInternal withSkipMessage(StringBuilder skipMessage) {
		this.skipMessage = skipMessage;
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalGeneratorTypes getGeneratorType() {
		return this.command.getGeneratorType();
	}


	public Integer getClientAccountId() {
		return this.command.getClientAccountId();
	}


	public boolean isSeparateJournalPerHoldingAccount() {
		return this.command.isSeparateJournalPerHoldingAccount();
	}


	public Date getGenerationDate() {
		return this.command.getGenerationDate();
	}


	public InvestmentSecurityEvent getSecurityEvent() {
		return this.securityEvent;
	}


	public BigDecimal getOverrideAmount() {
		return this.overrideAmount;
	}


	public void setOverrideAmount(BigDecimal overrideAmount) {
		this.overrideAmount = overrideAmount;
	}


	public StringBuilder getSkipMessage() {
		return this.skipMessage;
	}


	public void setSkipMessage(StringBuilder skipMessage) {
		this.skipMessage = skipMessage;
	}


	public boolean isAutoOnly() {
		return this.autoOnly;
	}


	public void setAutoOnly(boolean autoOnly) {
		this.autoOnly = autoOnly;
	}
}
