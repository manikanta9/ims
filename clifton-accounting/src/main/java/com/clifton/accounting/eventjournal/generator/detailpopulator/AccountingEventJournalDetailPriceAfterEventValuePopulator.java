package com.clifton.accounting.eventjournal.generator.detailpopulator;


/**
 * The <code>AccountingEventJournalDetailPriceAfterEventValuePopulator</code> class populates TransactionPrice
 * to security event's after event value.
 * For example, dividend per share amount is set as price.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailPriceAfterEventValuePopulator implements AccountingEventJournalDetailPopulator {

	@Override
	public boolean populate(PopulatorCommand command) {
		command.getJournalDetail().setTransactionPrice(command.getEventData().getAfterEventValue());
		return false;
	}
}
