package com.clifton.accounting.eventjournal.report;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.InvestmentSecurityEventTypeHierarchy;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;


/**
 * The <code>AccountingEventJournalReportServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class AccountingEventJournalReportServiceImpl implements AccountingEventJournalReportService {

	private AccountingEventJournalService accountingEventJournalService;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private ReportExportService reportExportService;

	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////
	//////     Event Journal Detail Report Business Methods               //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadAccountingEventJournalDetailReport(int detailId, FileFormats format) {
		AccountingEventJournalDetail detail = getAccountingEventJournalService().getAccountingEventJournalDetail(detailId);
		InvestmentSecurityEventType eventType = detail.getJournal().getSecurityEvent().getType();
		InvestmentInstrumentHierarchy hierarchy = detail.getJournal().getSecurityEvent().getSecurity().getInstrument().getHierarchy();
		InvestmentSecurityEventTypeHierarchy typeHierarchy = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeHierarchyByEventTypeAndHierarchy(eventType.getId(), hierarchy.getId());
		if (typeHierarchy == null || typeHierarchy.getEventReport() == null) {
			throw new ValidationException("Unable to generate report. There is no report defined for Event Type [" + eventType.getName() + "], Hierarchy [" + hierarchy.getNameExpanded()
					+ "] mapping.");
		}

		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("EventJournalDetailID", detailId);
		parameters.put("RunAsUserID", getSecurityUserService().getSecurityUserCurrent().getId());
		ReportExportConfiguration exportConfiguration = new ReportExportConfiguration(typeHierarchy.getEventReport().getId(), parameters, true, true);
		exportConfiguration.setExportFormat(format == null ? FileFormats.PDF : format);
		return getReportExportService().downloadReportFile(exportConfiguration);
	}


	/////////////////////////////////////////////////////////////////////
	////////            Getter & Setter Methods                 /////////
	/////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
