package com.clifton.accounting.eventjournal.generator.detailpopulator;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.calculator.accrual.AccrualDatesCommand;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingEventJournalDetailInterestPaymentPopulator</code> class sets TransactionAmount equal to interest payment
 * for the specified lot. Performs calculations based on current face (original face * current factor or index ratio for TIPS).
 * <p/>
 * The populator also accounts for Instrument Hierarchy accrual settings (supports Swaps, etc.)
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailInterestPaymentPopulator implements AccountingEventJournalDetailPopulator {

	private InvestmentCalculator investmentCalculator;
	private InvestmentSecurityEventService investmentSecurityEventService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		// skip calculations if the position was fully closed
		AccountingEventJournalDetail detail = command.getJournalDetail();
		if (MathUtils.isNullOrZero(command.getRemainingQuantity()) && MathUtils.isNullOrZero(command.getRemainingCostBasis()) && MathUtils.isNullOrZero(detail.getTransactionAmount())) {
			command.setSkipMessage("Interest Payment for Fully Closed Position is Zero.");
			return true;
		}

		EventData eventData = command.getEventData();
		InvestmentSecurity security = eventData.getSecurity();
		boolean accrualBasedOnNotional = security.getInstrument().getHierarchy().isAccrualBasedOnNotional();

		BigDecimal notional = accrualBasedOnNotional ? command.getOpeningCostBasis() : command.getOpeningQuantity();
		// adjust original face for current factor if necessary
		InvestmentSecurityEvent factor = getInvestmentSecurityEventService().getInvestmentSecurityEventPreviousForAccrualEndDate(security.getId(),
				InvestmentSecurityEventType.FACTOR_CHANGE, eventData.getAccrualEndDate());
		if (factor != null) {
			notional = factor.getAfterEventValue().multiply(notional); // use previous period factor to calculate coupon (current may not be known yet so can't get previous from it)
		}
		else {
			//if it doesn't have a factor, then use remaining face
			notional = accrualBasedOnNotional ? command.getRemainingCostBasis() : command.getRemainingQuantity();
		}
		// adjust for index ratio (TIPS); returns 1 if index ratio is not applicable
		Date notionalMultiplierDate = eventData.getPaymentDate();
		if (security.getInstrument().getHierarchy().isIndexRatioCalculatedUsingMonthStartAfterPrevCoupon()) {
			// in order to get previous period need to pass accrual end date (one day before payment date)
			notionalMultiplierDate = eventData.getAccrualEndDate();
		}
		BigDecimal notionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(security, notionalMultiplierDate);
		AccrualDates accrualDates = getInvestmentCalculator().calculateAccrualDates(AccrualDatesCommand.of(detail.getAccountingTransaction(), eventData.getAccrualEndDate()));
		detail.setTransactionAmount(getInvestmentCalculator().calculateInterestPayment(eventData.getEvent(), notional, notionalMultiplier, InvestmentUtils.isInvestmentSecurityEventOfAccrualType2(eventData.getEvent()) ? accrualDates.getAccrueFromDate2() : accrualDates.getAccrueFromDate1()));

		if (MathUtils.isNullOrZero(detail.getTransactionAmount())) {
			command.setSkipMessage("Interest Payment is Zero.");
			return true; // skip zero payment event
		}

		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}
}
