package com.clifton.accounting.eventjournal;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.generator.ExchangeRateDateSelectors;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;


/**
 * The <code>AccountingEventJournalType</code> class defines an event journal that processes
 * a specific security event: dividend payment, coupon payment, stock split, bond factor change,
 * future maturity, etc.
 * <p>
 * Event journal types define meta-data for fields that are applicable to corresponding events.
 * Some event types allow certain fields to be "manually" modified before booking in order to match bank/broker.
 * <p>
 * Note:
 * Some event journals support 2 bookings (create two separate accounting journals). If accrualReversalAccount is defined
 * for the journal type, then the first journal includes accrued entries and the second one reverses accrued entries
 * and replaces them with the real once: accrue Dividend Receivable and later reverse it and replace with Cash.
 * <p>
 * Corresponding booking processor generates a set of accounting transactions that
 * reflects the results of the event in the General Ledger.
 *
 * @author vgomelsky
 */
@CacheByName
public class AccountingEventJournalType extends NamedEntity<Short> {

	/**
	 * transactionAmount == "Payment Amount": can be modified
	 * transactionPrice == "Price": can be modified
	 * affectedQuantity == "Quantity": cannot be modified
	 */
	public static final String CASH_DIVIDEND_PAYMENT = "Cash Dividend Payment";
	/**
	 * transactionAmount == "Payment Amount": can be modified
	 * affectedQuantity == "Quantity": cannot be modified
	 */
	public static final String CASH_COUPON_PAYMENT = "Cash Coupon Payment";
	/**
	 * affectedQuantity == "Current Face" which cannot be modified
	 * transactionAmount and affectedCost are "Payment Amount" and "Cost Basis Reduction": can be modified
	 * transactionPrice == "Price": opening lot price that can be modified
	 */
	public static final String FACTOR_CHANGE = "Factor Change";
	/**
	 * affectedQuantity == "Face Reduction" which can be modified = New Current Face - Previous Face
	 * transactionAmount and affectedCost are "Interest Expense" and "Cost Basis Reduction": can be modified
	 * transactionPrice == "Price": opening lot price that cannot be modified
	 */
	public static final String FACTOR_CHANGE_ASSUMED_BUY = "Factor Change - Assumed Buy";
	/**
	 * affectedQuantity == "Principal Payment" which can be modified
	 * affectedCost == "Cost Basis Reduction": can be modified (used to calculate gain/loss)
	 * transactionAmount == "Interest Income" which can be modified
	 * transactionPrice == "Price": opening lot price that cannot be modified
	 */
	public static final String FACTOR_CHANGE_ASSUMED_SELL = "Factor Change - Assumed Sell";
	/**
	 * transactionPrice == "Maturity Price": can be manually modified (affects realized gain/loss and cash)
	 * affectedQuantity and affectedCost are "Closing Qty" and "Closing Cost Basis": neither can be modified
	 */
	public static final String SECURITY_MATURITY = "Security Maturity";
	/**
	 * affectedQuantity and affectedCost are "Closing Qty" and "Closing Cost Basis": neither can be modified
	 * transactionPrice == "Price": opening lot price that cannot be modified
	 * transactionAmount == "New Qty": cannot be modified; new price = transactionPrice * affectedQuantity / transactionAmount
	 */
	public static final String STOCK_SPLIT = "Stock Split";
	/**
	 * Symbol change represents creation of new security that is identical to the old one but with a different symbol.
	 * The old security is de-activated and the new security is created. All existing positions are closed with no
	 * realized gains and reopen immediately with proper original transaction date.
	 */
	public static final String SYMBOL_CHANGE = "Symbol Change";
	/**
	 * transactionAmount == "Payment Amount": can be modified
	 * transactionPrice == "Price": can be modified
	 * affectedQuantity == "Quantity": cannot be modified
	 */
	public static final String RETURN_OF_CAPITAL = "Return of Capital";
	/**
	 * transactionAmount == "Payment Amount": can be modified
	 * additionalAmount == "Additional Amount": can be modified
	 * additionalAmount3 == "Additional Amount 3": can be modified
	 * transactionPrice == "Price": can be modified
	 * transactionPrice == "Price": opening lot price that can be modified
	 */
	public static final String EQUITY_LEG_PAYMENT = "Equity Leg Payment";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private InvestmentSecurityEventType eventType;

	private EventJournalTypeScopes eventJournalTypeScope;

	/**
	 * Certain event types may specify which GL account to debit and which to credit.
	 * For example, cash dividend payment for a stock will Debit the Cash account and Credit the Dividend Income account.
	 */
	private AccountingAccount debitAccount;
	private AccountingAccount creditAccount;

	/**
	 * Optional AccountingAccount that reverses "Ex Date" accrual entry and replaces it on "Event Date" with this entry.
	 * For example, "Dividend Receivable" asset Debit booked on "Ex Date", is reversed and replaced with "Cash" on "Event Date".
	 * Accrual reversal works only with payments when either debitAccount or creditAccount is revenue or expense (does not get reversed)
	 * and the other one is asset or liability (does get reversed).
	 */
	private AccountingAccount accrualReversalAccount;

	// define which event journal detail fields can be modified when reconciling with banks/brokers
	private boolean affectedQuantityAdjustable;
	private boolean affectedCostAdjustable;
	private boolean transactionAmountAdjustable;
	private boolean transactionPriceAdjustable;
	private boolean additionalAmountAdjustable;
	private boolean additionalAmount2Adjustable;
	private boolean additionalAmount3Adjustable;
	/**
	 * Specifies whether Settlement or Transaction date should be used when determining if a position should have events
	 * of this type applied to it. For example, bond coupon payments are based on settlement dates but stock splits on trade date.
	 */
	private boolean settlementDateComparison;
	/**
	 * Specifies the number of days to use from Ex Date when determining if a position should have events.
	 * The value is usually 1 (end of day before Ex Date gets the event). However, the value is 2 for Cash Coupon Payment events (2 days before Ex Date).
	 */
	private short daysBeforeExDateForPositionLookup;
	/**
	 * If true, journals of this type will be automatically generated and processed by the system when the dates come (most journals).
	 * Use false when the data that goes into calculations needs to be verified first (Equity/Interest Leg Payment on a swap).
	 * If false, then event journal will be generated but not posted into GL (numbers may need to change).
	 * Accrual reversal will always get posted if accrual was posted.
	 */
	private boolean autoPost;

	private boolean autoGenerate;

	// event type specific labels: if the label is null, then the field is not applicable to this event
	private String affectedQuantityLabel;
	private String affectedCostLabel;
	private String transactionAmountLabel;
	private String transactionPriceLabel;
	private String additionalAmountLabel;
	private String additionalAmount2Label;
	private String additionalAmount3Label;

	/**
	 * The order in which multiple journals for the same event type are booked: Assumed Factor processes before Actual.
	 */
	private int bookingOrder;

	/**
	 * Some event types may require Exchange Rate from a very specific date for foreign transactions (Posting Date for Cleared CDS Premium Leg Payment).
	 */
	private ExchangeRateDateSelectors exchangeRateDateSelector;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if additionalAmount3 field on event journal detail is used to represent Exchange Rate from
	 * Local to Base currency.  This is used for event journals with 3 currencies: Local vs Settlement vs Base: need two FX.
	 */
	public boolean isAdditionalAmount3FxToBase() {
		// "FX to Base", "FX Rate 2", etc.
		return StringUtils.startsWith(getAdditionalAmount3Label(), "FX ");
	}


	/**
	 * Returns if event journals of this type support affected quantity field.
	 */
	public boolean isAffectedQuantitySupported() {
		return (this.affectedQuantityLabel != null);
	}


	/**
	 * Returns if event journals of this type support affected cost field.
	 */
	public boolean isAffectedCostSupported() {
		return (this.affectedCostLabel != null);
	}


	/**
	 * Returns if event journals of this type support transaction amount field.
	 */
	public boolean isTransactionAmountSupported() {
		return (this.transactionAmountLabel != null);
	}


	/**
	 * Returns if event journals of this type support transaction price field.
	 */
	public boolean isTransactionPriceSupported() {
		return (this.transactionPriceLabel != null);
	}


	public InvestmentSecurityEventType getEventType() {
		return this.eventType;
	}


	public void setEventType(InvestmentSecurityEventType eventType) {
		this.eventType = eventType;
	}


	public EventJournalTypeScopes getEventJournalTypeScope() {
		return this.eventJournalTypeScope;
	}


	public void setEventJournalTypeScope(EventJournalTypeScopes eventJournalTypeScope) {
		this.eventJournalTypeScope = eventJournalTypeScope;
	}


	public AccountingAccount getDebitAccount() {
		return this.debitAccount;
	}


	public void setDebitAccount(AccountingAccount debitAccount) {
		this.debitAccount = debitAccount;
	}


	public AccountingAccount getCreditAccount() {
		return this.creditAccount;
	}


	public void setCreditAccount(AccountingAccount creditAccount) {
		this.creditAccount = creditAccount;
	}


	public AccountingAccount getAccrualReversalAccount() {
		return this.accrualReversalAccount;
	}


	public void setAccrualReversalAccount(AccountingAccount accrualReversalAccount) {
		this.accrualReversalAccount = accrualReversalAccount;
	}


	public String getAffectedQuantityLabel() {
		return this.affectedQuantityLabel;
	}


	public void setAffectedQuantityLabel(String affectedQuantityLabel) {
		this.affectedQuantityLabel = affectedQuantityLabel;
	}


	public String getAffectedCostLabel() {
		return this.affectedCostLabel;
	}


	public void setAffectedCostLabel(String affectedCostLabel) {
		this.affectedCostLabel = affectedCostLabel;
	}


	public String getTransactionAmountLabel() {
		return this.transactionAmountLabel;
	}


	public void setTransactionAmountLabel(String transactionAmountLabel) {
		this.transactionAmountLabel = transactionAmountLabel;
	}


	public String getTransactionPriceLabel() {
		return this.transactionPriceLabel;
	}


	public void setTransactionPriceLabel(String transactionPriceLabel) {
		this.transactionPriceLabel = transactionPriceLabel;
	}


	public boolean isAffectedQuantityAdjustable() {
		return this.affectedQuantityAdjustable;
	}


	public void setAffectedQuantityAdjustable(boolean affectedQuantityAdjustable) {
		this.affectedQuantityAdjustable = affectedQuantityAdjustable;
	}


	public boolean isAffectedCostAdjustable() {
		return this.affectedCostAdjustable;
	}


	public void setAffectedCostAdjustable(boolean affectedCostAdjustable) {
		this.affectedCostAdjustable = affectedCostAdjustable;
	}


	public boolean isTransactionAmountAdjustable() {
		return this.transactionAmountAdjustable;
	}


	public void setTransactionAmountAdjustable(boolean transactionAmountAdjustable) {
		this.transactionAmountAdjustable = transactionAmountAdjustable;
	}


	public boolean isTransactionPriceAdjustable() {
		return this.transactionPriceAdjustable;
	}


	public void setTransactionPriceAdjustable(boolean transactionPriceAdjustable) {
		this.transactionPriceAdjustable = transactionPriceAdjustable;
	}


	public boolean isSettlementDateComparison() {
		return this.settlementDateComparison;
	}


	public void setSettlementDateComparison(boolean settlementDateComparison) {
		this.settlementDateComparison = settlementDateComparison;
	}


	public int getBookingOrder() {
		return this.bookingOrder;
	}


	public void setBookingOrder(int bookingOrder) {
		this.bookingOrder = bookingOrder;
	}


	public boolean isAdditionalAmountAdjustable() {
		return this.additionalAmountAdjustable;
	}


	public void setAdditionalAmountAdjustable(boolean additionalAmountAdjustable) {
		this.additionalAmountAdjustable = additionalAmountAdjustable;
	}


	public String getAdditionalAmountLabel() {
		return this.additionalAmountLabel;
	}


	public void setAdditionalAmountLabel(String additionalAmountLabel) {
		this.additionalAmountLabel = additionalAmountLabel;
	}


	public boolean isAutoPost() {
		return this.autoPost;
	}


	public void setAutoPost(boolean autoPost) {
		this.autoPost = autoPost;
	}


	public boolean isAutoGenerate() {
		return this.autoGenerate;
	}


	public void setAutoGenerate(boolean autoGenerate) {
		this.autoGenerate = autoGenerate;
	}


	public boolean isAdditionalAmount2Adjustable() {
		return this.additionalAmount2Adjustable;
	}


	public void setAdditionalAmount2Adjustable(boolean additionalAmount2Adjustable) {
		this.additionalAmount2Adjustable = additionalAmount2Adjustable;
	}


	public String getAdditionalAmount2Label() {
		return this.additionalAmount2Label;
	}


	public void setAdditionalAmount2Label(String additionalAmount2Label) {
		this.additionalAmount2Label = additionalAmount2Label;
	}


	public boolean isAdditionalAmount3Adjustable() {
		return this.additionalAmount3Adjustable;
	}


	public void setAdditionalAmount3Adjustable(boolean additionalAmount3Adjustable) {
		this.additionalAmount3Adjustable = additionalAmount3Adjustable;
	}


	public String getAdditionalAmount3Label() {
		return this.additionalAmount3Label;
	}


	public void setAdditionalAmount3Label(String additionalAmount3Label) {
		this.additionalAmount3Label = additionalAmount3Label;
	}


	public ExchangeRateDateSelectors getExchangeRateDateSelector() {
		return this.exchangeRateDateSelector;
	}


	public void setExchangeRateDateSelector(ExchangeRateDateSelectors exchangeRateDateSelector) {
		this.exchangeRateDateSelector = exchangeRateDateSelector;
	}


	public short getDaysBeforeExDateForPositionLookup() {
		return this.daysBeforeExDateForPositionLookup;
	}


	public void setDaysBeforeExDateForPositionLookup(short daysBeforeExDateForPositionLookup) {
		this.daysBeforeExDateForPositionLookup = daysBeforeExDateForPositionLookup;
	}
}
