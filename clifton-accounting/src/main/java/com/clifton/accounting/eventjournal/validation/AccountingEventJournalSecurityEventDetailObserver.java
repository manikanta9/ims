package com.clifton.accounting.eventjournal.validation;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommandInternal;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>AccountingEventJournalSecurityEventDetailObserver</code> class:
 * <p>
 * 1. Validates/Prevents insert/update/delete of InvestmentSecurityEventDetail objects that are used by BOOKED event journals.
 * 2. For those used by unbooked journals only - After changes, re-processes existing journals and saves updated journal details
 *
 * @author vgomelsky
 */
@Component
public class AccountingEventJournalSecurityEventDetailObserver extends SelfRegisteringDaoObserver<InvestmentSecurityEventDetail> {

	private AccountingEventJournalService accountingEventJournalService;
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;

	private static final String REPROCESS_EVENT_JOURNAL_KEY = "reprocessEventJournals";


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentSecurityEventDetail> dao, DaoEventTypes event, InvestmentSecurityEventDetail bean) {
		AccountingEventJournalSearchForm searchForm = new AccountingEventJournalSearchForm();
		searchForm.setSecurityEventId(bean.getEvent().getId());
		List<AccountingEventJournal> journalList = getAccountingEventJournalService().getAccountingEventJournalList(searchForm);

		if (!CollectionUtils.isEmpty(journalList)) {
			List<AccountingEventJournal> bookedJournalList = BeanUtils.filter(journalList, AccountingEventJournal::isBooked, true);
			if (!CollectionUtils.isEmpty(bookedJournalList)) {
				throw new ValidationException("Cannot " + event.name() + " security event detail that is used by booked event journal: "
						+ CollectionUtils.getFirstElementStrict(bookedJournalList).getLabel());
			}
			// Otherwise Journals Exist - and none of them are booked flag this event to automatically reprocess the existing journals
			getDaoEventContext().setBeanAttribute(bean, REPROCESS_EVENT_JOURNAL_KEY, Boolean.TRUE);
		}
	}


	@Override
	protected void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentSecurityEventDetail> dao, @SuppressWarnings("unused") DaoEventTypes event, InvestmentSecurityEventDetail bean,
	                                   Throwable e) {
		if (e == null) {
			// Re-Process Only if existing journals were already found in before (prevents extra unnecessary look ups)
			if (Boolean.TRUE.equals(getDaoEventContext().getBeanAttribute(bean, REPROCESS_EVENT_JOURNAL_KEY))) {
				getAccountingEventJournalGeneratorService().generateAccountingEventJournal(EventJournalGeneratorCommandInternal.forEvent(bean.getEvent(), AccountingEventJournalGeneratorTypes.REPROCESS_EXISTING));
			}

			// Remove Re-process Event Journal DAO Attribute
			getDaoEventContext().removeBeanAttribute(bean, REPROCESS_EVENT_JOURNAL_KEY);
		}
	}


	//////////////////////////////////////////////////


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}


	public AccountingEventJournalGeneratorService getAccountingEventJournalGeneratorService() {
		return this.accountingEventJournalGeneratorService;
	}


	public void setAccountingEventJournalGeneratorService(AccountingEventJournalGeneratorService accountingEventJournalGeneratorService) {
		this.accountingEventJournalGeneratorService = accountingEventJournalGeneratorService;
	}
}
