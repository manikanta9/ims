package com.clifton.accounting.eventjournal.validation;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventClientElection;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;


/**
 * The <code>AccountingEventJournalSecurityEventClientElectionObserver</code> class:
 * <br/>1. Validates/prevents updates of {@link InvestmentSecurityEventClientElection} objects that are used by booked event journals.
 * <br/>2. For those used by unbooked journals only - After changes, re-processes existing journals and saves updated journal details
 * <p>
 * Note: client election description can be modified at any time
 *
 * @author NickK
 * @see BaseAccountingEventJournalSecurityEventObserver
 */
@Component
public class AccountingEventJournalSecurityEventClientElectionObserver extends BaseAccountingEventJournalSecurityEventObserver<InvestmentSecurityEventClientElection> {


	@Override
	protected void bookedAccountingEventJournalViolationOnInsert(InvestmentSecurityEventClientElection clientElection, List<AccountingEventJournal> bookedJournalList) {
		throw new ValidationException("Cannot insert Security Event Client Election with election number " + clientElection.getElectionNumber() + " that is used by booked event journal: " + CollectionUtils.getFirstElementStrict(bookedJournalList).getLabel());
	}


	@Override
	protected void bookedAccountingEventJournalViolationOnDelete(InvestmentSecurityEventClientElection clientElection, List<AccountingEventJournal> bookedJournalList) {
		throw new ValidationException("Cannot delete Security Event Client Election with election number " + clientElection.getElectionNumber() + " that is used by booked event journal: " + CollectionUtils.getFirstElementStrict(bookedJournalList).getLabel());
	}


	@Override
	protected void bookedAccountingEventJournalViolationOnUpdate(InvestmentSecurityEventClientElection clientElection, List<String> differentBeanProperties, List<AccountingEventJournal> bookedJournalList) {
		throw new ValidationException("Cannot update " + differentBeanProperties + " fields for Security Event Client Election that is used by booked event journal: " + CollectionUtils.getFirstElementStrict(bookedJournalList).getLabel());
	}


	@Override
	protected InvestmentSecurityEvent getInvestmentSecurityEventForBean(InvestmentSecurityEventClientElection clientElection) {
		return clientElection.getSecurityEvent();
	}


	@Override
	protected Collection<AccountingEventJournalDetail> getReferencedAccountingEventJournalDetails(InvestmentSecurityEventClientElection clientElection, InvestmentSecurityEventClientElection originalClientElection) {
		if (originalClientElection != null) {
			ValidationUtils.assertEquals(clientElection.getClientInvestmentAccount(), originalClientElection.getClientInvestmentAccount(), "Unable to update the Security Event Client Election's referenced Client Account");
		}
		AccountingEventJournalDetailSearchForm searchForm = new AccountingEventJournalDetailSearchForm();
		searchForm.setSecurityEventId(clientElection.getSecurityEvent().getId());
		searchForm.setClientInvestmentAccountId(clientElection.getClientInvestmentAccount().getId());
		return getAccountingEventJournalService().getAccountingEventJournalDetailList(searchForm);
	}
}
