package com.clifton.accounting.eventjournal.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingEventJournalDetailSearchForm</code> class defines search configuration for AccountingEventJournalDetail objects.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "accountingTransaction.id")
	private Long accountingTransactionId;

	@SearchField(searchField = "eventPayout.id")
	private Integer eventPayoutId;

	@SearchField(searchField = "electionNumber", searchFieldPath = "eventPayout")
	private Short eventPayoutElectionNumber;

	@SearchField(searchField = "electionNumber", searchFieldPath = "eventPayout", comparisonConditions = ComparisonConditions.IN)
	private Short[] eventPayoutElectionNumbers;

	@SearchField(searchField = "journal.id")
	private Integer journalId;

	@SearchField(searchField = "journal.journalType.id")
	private Short journalTypeId;

	@SearchField(searchField = "name", searchFieldPath = "journal.journalType", comparisonConditions = {ComparisonConditions.EQUALS})
	private String journalTypeName;

	@SearchField(searchField = "journal.securityEvent.id")
	private Integer securityEventId;

	@SearchField(searchField = "journal.securityEvent.type.id")
	private Short securityEventTypeId;

	@SearchField(searchField = "actualSettlementDate", searchFieldPath = "journal.securityEvent")
	private Date actualSettlementDate;

	@SearchField(searchField = "security.id", searchFieldPath = "journal.securityEvent")
	private Integer investmentSecurityId;

	@SearchField(searchField = "cusip", searchFieldPath = "journal.securityEvent.security")
	private String cusip;

	@SearchField(searchField = "instrument.id", searchFieldPath = "journal.securityEvent.security", sortField = "instrument.name")
	private Integer securityInstrumentId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "journal.securityEvent.security.instrument")
	private Integer securityCurrencyDenominationId;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "journal.securityEvent.security")
	private Integer underlyingSecurityId;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "journal.securityEvent.security.instrument", sortField = "hierarchy.name")
	private Short securityInstrumentHierarchyId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "journal.securityEvent.security.instrument.hierarchy", sortField = "investmentType.name")
	private Short securityInvestmentTypeId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "journal.securityEvent.security.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "accountingAccount.id", searchFieldPath = "accountingTransaction")
	private Short accountingAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id", searchFieldPath = "accountingTransaction")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "number,name", searchFieldPath = "accountingTransaction.clientInvestmentAccount")
	private String clientInvestmentAccount;

	@SearchField(searchField = "teamSecurityGroup.id", sortField = "teamSecurityGroup.name", searchFieldPath = "accountingTransaction.clientInvestmentAccount")
	private Short teamSecurityGroupId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "accountingTransaction.clientInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer clientInvestmentAccountGroupId;

	@SearchField(searchField = "holdingInvestmentAccount.id", searchFieldPath = "accountingTransaction")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "number,name", searchFieldPath = "accountingTransaction.holdingInvestmentAccount")
	private String holdingInvestmentAccount;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "accountingTransaction.holdingInvestmentAccount")
	private Integer holdingAccountIssuerId;

	// Custom Search Field
	private Integer holdingInvestmentAccountGroupId;

	// Custom Search Field
	private Integer[] holdingInvestmentAccountGroupIds;

	// Custom Search Field
	private Integer excludeHoldingInvestmentAccountGroupId;

	// Custom Search Field
	private Integer[] excludeHoldingInvestmentAccountGroupIds;

	// custom filter: flag on hierarchy used in conjunction with reversal booking date.
	private Boolean includeAccrualReceivables;

	// Custom Search Field
	private Boolean accrualPostingOnly;

	@SearchField(searchField = "executingCompany.id", searchFieldPath = "accountingTransaction")
	private Integer executingCompanyId;

	@SearchField(searchField = "type.id", searchFieldPath = "accountingTransaction.holdingInvestmentAccount")
	private Short holdingAccountTypeId;

	@SearchField(searchField = "originalTransactionDate", searchFieldPath = "accountingTransaction")
	private Date originalTransactionDate;

	@SearchField(searchField = "eventDate", searchFieldPath = "journal.securityEvent")
	private Date eventDate;

	@SearchField(searchField = "eventDate", searchFieldPath = "journal.securityEvent", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date eventDateBefore;

	@SearchField(searchField = "eventDate", searchFieldPath = "journal.securityEvent", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date eventDateAfter;

	@SearchField(searchField = "eventDescription", searchFieldPath = "journal.securityEvent")
	private String eventDescription;

	@SearchField(searchFieldPath = "journal")
	private Date bookingDate;

	@SearchField(searchField = "bookingDate", searchFieldPath = "journal", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean journalBooked;

	@SearchField(searchField = "bookingDate", searchFieldPath = "journal", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean journalNotBooked;

	@SearchField(searchFieldPath = "journal")
	private Date accrualReversalBookingDate;

	@SearchField(searchField = "accrualReversalBookingDate", searchFieldPath = "journal", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean accrualReversalBooked;

	@SearchField(searchField = "accrualReversalBookingDate", searchFieldPath = "journal", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean accrualReversalNotBooked;

	@SearchField
	private BigDecimal affectedQuantity;

	@SearchField
	private BigDecimal transactionPrice;

	@SearchField
	private BigDecimal transactionAmount;

	@SearchField
	private BigDecimal additionalAmount;

	@SearchField
	private BigDecimal affectedCost;

	@SearchField
	private BigDecimal exchangeRateToBase;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Long getAccountingTransactionId() {
		return this.accountingTransactionId;
	}


	public void setAccountingTransactionId(Long accountingTransactionId) {
		this.accountingTransactionId = accountingTransactionId;
	}


	public Integer getEventPayoutId() {
		return this.eventPayoutId;
	}


	public void setEventPayoutId(Integer eventPayoutId) {
		this.eventPayoutId = eventPayoutId;
	}


	public Short getEventPayoutElectionNumber() {
		return this.eventPayoutElectionNumber;
	}


	public void setEventPayoutElectionNumber(Short eventPayoutElectionNumber) {
		this.eventPayoutElectionNumber = eventPayoutElectionNumber;
	}


	public Short[] getEventPayoutElectionNumbers() {
		return this.eventPayoutElectionNumbers;
	}


	public void setEventPayoutElectionNumbers(Short[] eventPayoutElectionNumbers) {
		this.eventPayoutElectionNumbers = eventPayoutElectionNumbers;
	}


	public Integer getJournalId() {
		return this.journalId;
	}


	public void setJournalId(Integer journalId) {
		this.journalId = journalId;
	}


	public Short getJournalTypeId() {
		return this.journalTypeId;
	}


	public void setJournalTypeId(Short journalTypeId) {
		this.journalTypeId = journalTypeId;
	}


	public String getJournalTypeName() {
		return this.journalTypeName;
	}


	public void setJournalTypeName(String journalTypeName) {
		this.journalTypeName = journalTypeName;
	}


	public Integer getSecurityEventId() {
		return this.securityEventId;
	}


	public void setSecurityEventId(Integer securityEventId) {
		this.securityEventId = securityEventId;
	}


	public Short getSecurityEventTypeId() {
		return this.securityEventTypeId;
	}


	public void setSecurityEventTypeId(Short securityEventTypeId) {
		this.securityEventTypeId = securityEventTypeId;
	}


	public Date getActualSettlementDate() {
		return this.actualSettlementDate;
	}


	public void setActualSettlementDate(Date actualSettlementDate) {
		this.actualSettlementDate = actualSettlementDate;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public Integer getSecurityInstrumentId() {
		return this.securityInstrumentId;
	}


	public void setSecurityInstrumentId(Integer securityInstrumentId) {
		this.securityInstrumentId = securityInstrumentId;
	}


	public Integer getSecurityCurrencyDenominationId() {
		return this.securityCurrencyDenominationId;
	}


	public void setSecurityCurrencyDenominationId(Integer securityCurrencyDenominationId) {
		this.securityCurrencyDenominationId = securityCurrencyDenominationId;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public Short getSecurityInstrumentHierarchyId() {
		return this.securityInstrumentHierarchyId;
	}


	public void setSecurityInstrumentHierarchyId(Short securityInstrumentHierarchyId) {
		this.securityInstrumentHierarchyId = securityInstrumentHierarchyId;
	}


	public Short getSecurityInvestmentTypeId() {
		return this.securityInvestmentTypeId;
	}


	public void setSecurityInvestmentTypeId(Short securityInvestmentTypeId) {
		this.securityInvestmentTypeId = securityInvestmentTypeId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public String getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(String clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public String getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(String holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public Integer getHoldingInvestmentAccountGroupId() {
		return this.holdingInvestmentAccountGroupId;
	}


	public void setHoldingInvestmentAccountGroupId(Integer holdingInvestmentAccountGroupId) {
		this.holdingInvestmentAccountGroupId = holdingInvestmentAccountGroupId;
	}


	public Integer[] getHoldingInvestmentAccountGroupIds() {
		return this.holdingInvestmentAccountGroupIds;
	}


	public void setHoldingInvestmentAccountGroupIds(Integer[] holdingInvestmentAccountGroupIds) {
		this.holdingInvestmentAccountGroupIds = holdingInvestmentAccountGroupIds;
	}


	public Integer getExcludeHoldingInvestmentAccountGroupId() {
		return this.excludeHoldingInvestmentAccountGroupId;
	}


	public void setExcludeHoldingInvestmentAccountGroupId(Integer excludeHoldingInvestmentAccountGroupId) {
		this.excludeHoldingInvestmentAccountGroupId = excludeHoldingInvestmentAccountGroupId;
	}


	public Integer[] getExcludeHoldingInvestmentAccountGroupIds() {
		return this.excludeHoldingInvestmentAccountGroupIds;
	}


	public void setExcludeHoldingInvestmentAccountGroupIds(Integer[] excludeHoldingInvestmentAccountGroupIds) {
		this.excludeHoldingInvestmentAccountGroupIds = excludeHoldingInvestmentAccountGroupIds;
	}


	public Boolean getIncludeAccrualReceivables() {
		return this.includeAccrualReceivables;
	}


	public void setIncludeAccrualReceivables(Boolean includeAccrualReceivables) {
		this.includeAccrualReceivables = includeAccrualReceivables;
	}


	public Boolean getAccrualPostingOnly() {
		return this.accrualPostingOnly;
	}


	public void setAccrualPostingOnly(Boolean accrualPostingOnly) {
		this.accrualPostingOnly = accrualPostingOnly;
	}


	public Integer getExecutingCompanyId() {
		return this.executingCompanyId;
	}


	public void setExecutingCompanyId(Integer executingCompanyId) {
		this.executingCompanyId = executingCompanyId;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Date getOriginalTransactionDate() {
		return this.originalTransactionDate;
	}


	public void setOriginalTransactionDate(Date originalTransactionDate) {
		this.originalTransactionDate = originalTransactionDate;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Date getEventDateBefore() {
		return this.eventDateBefore;
	}


	public void setEventDateBefore(Date eventDateBefore) {
		this.eventDateBefore = eventDateBefore;
	}


	public Date getEventDateAfter() {
		return this.eventDateAfter;
	}


	public void setEventDateAfter(Date eventDateAfter) {
		this.eventDateAfter = eventDateAfter;
	}


	public String getEventDescription() {
		return this.eventDescription;
	}


	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Boolean getJournalBooked() {
		return this.journalBooked;
	}


	public void setJournalBooked(Boolean journalBooked) {
		this.journalBooked = journalBooked;
	}


	public Boolean getJournalNotBooked() {
		return this.journalNotBooked;
	}


	public void setJournalNotBooked(Boolean journalNotBooked) {
		this.journalNotBooked = journalNotBooked;
	}


	public Date getAccrualReversalBookingDate() {
		return this.accrualReversalBookingDate;
	}


	public void setAccrualReversalBookingDate(Date accrualReversalBookingDate) {
		this.accrualReversalBookingDate = accrualReversalBookingDate;
	}


	public Boolean getAccrualReversalBooked() {
		return this.accrualReversalBooked;
	}


	public void setAccrualReversalBooked(Boolean accrualReversalBooked) {
		this.accrualReversalBooked = accrualReversalBooked;
	}


	public Boolean getAccrualReversalNotBooked() {
		return this.accrualReversalNotBooked;
	}


	public void setAccrualReversalNotBooked(Boolean accrualReversalNotBooked) {
		this.accrualReversalNotBooked = accrualReversalNotBooked;
	}


	public BigDecimal getAffectedQuantity() {
		return this.affectedQuantity;
	}


	public void setAffectedQuantity(BigDecimal affectedQuantity) {
		this.affectedQuantity = affectedQuantity;
	}


	public BigDecimal getTransactionPrice() {
		return this.transactionPrice;
	}


	public void setTransactionPrice(BigDecimal transactionPrice) {
		this.transactionPrice = transactionPrice;
	}


	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}


	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}


	public BigDecimal getAdditionalAmount() {
		return this.additionalAmount;
	}


	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}


	public BigDecimal getAffectedCost() {
		return this.affectedCost;
	}


	public void setAffectedCost(BigDecimal affectedCost) {
		this.affectedCost = affectedCost;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}
}
