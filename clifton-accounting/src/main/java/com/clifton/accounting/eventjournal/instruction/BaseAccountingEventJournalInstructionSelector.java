package com.clifton.accounting.eventjournal.instruction;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instruction.selector.BaseInvestmentInstructionSelector;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;

import java.util.Date;
import java.util.List;


/**
 * The <code>BaseAccountingM2MInstructionSelector</code> ...
 *
 * @author manderson
 */
public abstract class BaseAccountingEventJournalInstructionSelector extends BaseInvestmentInstructionSelector<AccountingEventJournalDetail> {

	private AccountingEventJournalService accountingEventJournalService;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private InvestmentSecurityEventService investmentSecurityEventService;

	////////////////////////////////////////////////////////////////////////////////

	private Short eventJournalTypeId;

	private Integer holdingAccountId;

	private Integer holdingAccountIssuerId;

	private Short holdingAccountTypeId;

	private List<Integer> includeHoldingAccountGroupIds;

	private List<Integer> excludeHoldingAccountGroupIds;

	private Integer clientAccountId;

	private Integer investmentInstrumentId;

	private Integer executingBrokerId;

	private Short investmentGroupId;

	private Short investmentTypeId;

	private Integer payingSecurityId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getInstructionItemLabel(AccountingEventJournalDetail entity) {
		return entity.getJournal().getLabel() + " - " + entity.getAccountingTransaction().getClientInvestmentAccount().getLabel();
	}


	@Override
	public boolean isInstructionItemBooked(InvestmentInstructionCategory category, AccountingEventJournalDetail entity) {
		return entity.getJournal().getBookingDate() != null;
	}


	@Override
	public void populateInstructionSelectionDetails(InvestmentInstructionSelection selection, AccountingEventJournalDetail entity) {
		selection.setClientAccount(entity.getAccountingTransaction().getClientInvestmentAccount());
		selection.setHoldingAccount(entity.getAccountingTransaction().getHoldingInvestmentAccount());
		selection.setSecurity(entity.getJournal().getSecurityEvent().getSecurity());
		selection.setBooked(isInstructionItemBooked(selection.getCategory(), entity));
		selection.setDescription(entity.getJournal().getSecurityEvent().getLabelShort());
	}


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingEventJournalDetail> getEntityList(InvestmentInstructionCategory category, InvestmentInstructionDefinition definition, Date date) {
		AccountingEventJournalDetailSearchForm searchForm = new AccountingEventJournalDetailSearchForm();
		searchForm.setEventDate(date);

		searchForm.setJournalTypeId(getEventJournalTypeId());
		searchForm.setClientInvestmentAccountId(getClientAccountId());
		searchForm.setExecutingCompanyId(getExecutingBrokerId());
		searchForm.setSecurityCurrencyDenominationId(getPayingSecurityId());

		// Specific Holding Account
		if (getHoldingAccountId() != null) {
			searchForm.setHoldingInvestmentAccountId(getHoldingAccountId());
		}
		// Or Issuer and/or Type and/or account group filters
		else {
			searchForm.setHoldingAccountIssuerId(getHoldingAccountIssuerId());
			searchForm.setHoldingAccountTypeId(getHoldingAccountTypeId());
			searchForm.setHoldingInvestmentAccountGroupIds(CollectionUtils.toArrayOrNull(getIncludeHoldingAccountGroupIds(), Integer.class));
			searchForm.setExcludeHoldingInvestmentAccountGroupIds(CollectionUtils.toArrayOrNull(getExcludeHoldingAccountGroupIds(), Integer.class));
		}

		// Specific Instrument
		if (getInvestmentInstrumentId() != null) {
			searchForm.setSecurityInstrumentId(getInvestmentInstrumentId());
		}
		// Or Group and/or Type
		else {
			searchForm.setInvestmentGroupId(getInvestmentGroupId());
			searchForm.setSecurityInvestmentTypeId(getInvestmentTypeId());
		}

		List<AccountingEventJournalDetail> result = getAccountingEventJournalService().getAccountingEventJournalDetailList(searchForm);
		// skip details for reference securities that are instructed together with the parent (original TRS lot)
		result = BeanUtils.filter(result, d -> {
			InvestmentSecurity security = d.getAccountingTransaction().getInvestmentSecurity();
			return security.getReferenceSecurity() == null || !security.getInstrument().getHierarchy().isReferenceSecurityChildNotInstructedForEvents();
		});
		return result;
	}


	/**
	 * NOTE CUSTOM LOGIC IS APPLIED FOR TRS AND IRS RESETS. For instructions, users map only one of the legs to prevent duplicate instructions.
	 * The report knows to pull both legs and display as a "reset" combined event.  Because of this, for missing instructions the detail is only considered missing
	 * if there isn't an instruction associated directly with that detail, or it's matching counterpart leg detail.
	 */
	@Override
	protected boolean isInvestmentInstructionItemExists(AccountingEventJournalDetail entity, List<Integer> existingList) {
		boolean exists = super.isInvestmentInstructionItemExists(entity, existingList);
		if (exists) {
			return true;
		}
		Integer matchingId = getMatchingResetEventLeg(entity);
		if (matchingId != null && existingList.contains(matchingId)) {
			return true;
		}
		return false;
	}


	private Integer getMatchingResetEventLeg(AccountingEventJournalDetail entity) {
		InvestmentSecurityEvent event = entity.getJournal().getSecurityEvent();
		String matchingEventType = null;
		if (InvestmentUtils.isSecurityOfType(event.getSecurity(), InvestmentType.SWAPS)) {
			if (InvestmentUtils.isSecurityOfTypeSubType(event.getSecurity(), InvestmentTypeSubType.INTEREST_RATE_SWAPS)) {
				if (InvestmentSecurityEventType.FIXED_LEG_PAYMENT.equals(event.getType().getName())) {
					matchingEventType = InvestmentSecurityEventType.FLOATING_LEG_PAYMENT;
				}
				else if (InvestmentSecurityEventType.FLOATING_LEG_PAYMENT.equals(event.getType().getName())) {
					matchingEventType = InvestmentSecurityEventType.FIXED_LEG_PAYMENT;
				}
			}
			if (InvestmentUtils.isSecurityOfTypeSubType(event.getSecurity(), InvestmentTypeSubType.TOTAL_RETURN_SWAPS)) {
				if (InvestmentSecurityEventType.EQUITY_LEG_PAYMENT.equals(event.getType().getName())) {
					matchingEventType = InvestmentSecurityEventType.INTEREST_LEG_PAYMENT;
				}
				else if (InvestmentSecurityEventType.INTEREST_LEG_PAYMENT.equals(event.getType().getName())) {
					matchingEventType = InvestmentSecurityEventType.EQUITY_LEG_PAYMENT;
				}
			}
		}
		if (matchingEventType != null) {
			InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
			searchForm.setSecurityId(event.getSecurity().getId());
			searchForm.setEventDate(event.getEventDate());
			searchForm.setTypeName(matchingEventType);

			InvestmentSecurityEvent matchingEvent = CollectionUtils.getFirstElement(getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm));
			if (matchingEvent != null) {
				AccountingEventJournalDetailSearchForm journalDetailSearchForm = new AccountingEventJournalDetailSearchForm();
				journalDetailSearchForm.setAccountingTransactionId(entity.getAccountingTransaction().getId());
				journalDetailSearchForm.setSecurityEventId(matchingEvent.getId());
				List<AccountingEventJournalDetail> detailList = getAccountingEventJournalService().getAccountingEventJournalDetailList(journalDetailSearchForm);
				if (!CollectionUtils.isEmpty(detailList)) {
					return detailList.get(0).getId();
				}
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                   Getter & Setter Methods                 ///////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getEventJournalTypeId() {
		return this.eventJournalTypeId;
	}


	public void setEventJournalTypeId(Short eventJournalTypeId) {
		this.eventJournalTypeId = eventJournalTypeId;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public Integer getPayingSecurityId() {
		return this.payingSecurityId;
	}


	public void setPayingSecurityId(Integer payingSecurityId) {
		this.payingSecurityId = payingSecurityId;
	}


	public List<Integer> getIncludeHoldingAccountGroupIds() {
		return this.includeHoldingAccountGroupIds;
	}


	public void setIncludeHoldingAccountGroupIds(List<Integer> includeHoldingAccountGroupIds) {
		this.includeHoldingAccountGroupIds = includeHoldingAccountGroupIds;
	}


	public List<Integer> getExcludeHoldingAccountGroupIds() {
		return this.excludeHoldingAccountGroupIds;
	}


	public void setExcludeHoldingAccountGroupIds(List<Integer> excludeHoldingAccountGroupIds) {
		this.excludeHoldingAccountGroupIds = excludeHoldingAccountGroupIds;
	}


	public Integer getExecutingBrokerId() {
		return this.executingBrokerId;
	}


	public void setExecutingBrokerId(Integer executingBrokerId) {
		this.executingBrokerId = executingBrokerId;
	}
}
