package com.clifton.accounting.eventjournal;

import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The EventJournalTypeScopes enum allows to limit a specific {@link AccountingEventJournalType} to
 * a sub-set of securities that it should apply for. For example, "Premium Leg Payment" event may
 * have two event journal types with different scopes: one for OTC (auto-post) and one for Non OTC (manually post).
 *
 * @author vgomelsky
 */
public enum EventJournalTypeScopes {

	ALL_SECURITIES {
		@Override
		public boolean isSecurityInScope(InvestmentSecurity security) {
			return true;
		}
	},

	OTC_SECURITIES {
		@Override
		public boolean isSecurityInScope(InvestmentSecurity security) {
			return security.getInstrument().getHierarchy().isOtc();
		}
	},

	NON_OTC_SECURITIES {
		@Override
		public boolean isSecurityInScope(InvestmentSecurity security) {
			return !security.getInstrument().getHierarchy().isOtc();
		}
	};


	/**
	 * Returns true if the specified security is in scope: event journal of this type needs to be generated for it.
	 */
	public abstract boolean isSecurityInScope(InvestmentSecurity security);
}
