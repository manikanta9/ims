package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.setup.InvestmentType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The AccountingSecurityMaturityBookingRule class generates closing position entry for each journal detail as well as corresponding Cash/Currency entry.
 * Note: Deliverable Currency Forwards use special logic that results in both Currency 1 and Currency 2 entries in addition to closing position.
 */
public class AccountingSecurityMaturityBookingRule extends BaseEventJournalBookingRule {

	private InvestmentCalculator investmentCalculator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingEventJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingEventJournal eventJournal = bookingSession.getBookableEntity();
		InvestmentInstrument instrument = eventJournal.getSecurityEvent().getSecurity().getInstrument();

		// List to collect currency journal details pertaining to a maturing deliverable forward, which will be used to adjust base debit/credit for penny rounding.
		List<AccountingJournalDetail> deliverableForwardCurrencyDetailList = new ArrayList<>();

		// close all open positions since this security has matured
		for (AccountingEventJournalDetail eventDetail : CollectionUtils.getIterable(eventJournal.getDetailList())) {
			AccountingJournalDetailDefinition closingPositionDetail = generateJournalDetail(journal, bookingSession, eventDetail);
			journal.addJournalDetail(closingPositionDetail);

			if (instrument.isDeliverable() && InvestmentUtils.isInstrumentOfType(instrument, InvestmentType.FORWARDS)) {
				// Deliverable Currency Forward maturity results in BUY of one spot currency and SELL of the other: COST => CCY Denom; QTY => Underlying
				AccountingEventJournalDetail.ForwardMaturity forward = eventDetail.forForwardMaturity();
				AccountingJournalDetail detail1 = createCashCurrencyRecord(closingPositionDetail, eventDetail, forward.getCurrency1());
				detail1.setLocalDebitCredit(forward.getCurrencyAmount1());
				detail1.setExchangeRateToBase(forward.getExchangeRate1());
				detail1.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(detail1.getLocalDebitCredit(), detail1.getExchangeRateToBase(), detail1.getClientInvestmentAccount()));
				detail1.setDescription(getCashCurrencyRecordDescriptionPrefix(detail1) + detail1.getInvestmentSecurity().getSymbol() + " on maturity of " + forward.getSymbol() + " on " + DateUtils.fromDateShort(forward.getSettlementDate()));
				journal.addJournalDetail(detail1);
				deliverableForwardCurrencyDetailList.add(detail1);

				AccountingJournalDetail detail2 = createCashCurrencyRecord(closingPositionDetail, eventDetail, forward.getCurrency2());
				detail2.setLocalDebitCredit(forward.getCurrencyAmount2());
				detail2.setExchangeRateToBase(forward.getExchangeRate2());
				detail2.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(detail2.getLocalDebitCredit(), detail2.getExchangeRateToBase(), detail2.getClientInvestmentAccount()));
				detail2.setDescription(getCashCurrencyRecordDescriptionPrefix(detail2) + detail2.getInvestmentSecurity().getSymbol() + " on maturity of " + forward.getSymbol() + " on " + DateUtils.fromDateShort(forward.getSettlementDate()));
				journal.addJournalDetail(detail2);
				deliverableForwardCurrencyDetailList.add(detail2);

				// when opening and closing prices are the same (offsetting forward position), avoid penny rounding by matching base amounts for both legs: MUST MATCH TO CURRENCY 2 BECAUSE IT USES QUANTITY FIELD: QUANTITY DOES NOT CHANGE
				if (MathUtils.isEqual(forward.getOpeningPrice(), forward.getMaturityPrice())) {
					detail1.setBaseDebitCredit(MathUtils.negate(detail2.getBaseDebitCredit()));
				}
			}
			else {
				journal.addJournalDetail(createCashCurrencyRecord(closingPositionDetail, eventDetail));
			}

			if (!CollectionUtils.isEmpty(deliverableForwardCurrencyDetailList)) {
				adjustForwardMaturityCurrencyJournalDetailListForPennyRounding(eventJournal.getSecurityEvent().getSecurity(), deliverableForwardCurrencyDetailList);
			}
		}
	}


	/**
	 * Forward positions remain in GL until maturity. At this time, the positions are reversed and the forwards local currency (forward's currency denominator) is traded.
	 * Due to forward positions being performed in multiple lots, the currency positions must be aggregated and adjusted for penny rounding differences in the base currency.
	 */
	private void adjustForwardMaturityCurrencyJournalDetailListForPennyRounding(InvestmentSecurity forward, List<AccountingJournalDetail> deliverableForwardCurrencyDetailList) {
		// group forward currency details by client to correctly aggregate resulting currency details for the forward position reversal
		Map<InvestmentAccount, List<AccountingJournalDetail>> clientCurrencyDetailListMap = BeanUtils.getBeansMap(deliverableForwardCurrencyDetailList, AccountingJournalDetail::getClientInvestmentAccount);
		clientCurrencyDetailListMap.forEach((clientInvestmentAccount, clientCurrencyDetailList) -> {
			// Get the currency details for the forward's underlying currency. The underlying currency detail local amounts will match the quantit(y|ies) of the forward position(s) being reversed.
			// Aggregate the local and base amounts of the underlying currency details. Use the local amount to calculate an aggregated base amount, and adjust the base amount of one of the details by the difference.
			List<AccountingJournalDetail> forwardUnderlyingCurrencyDetailList = clientCurrencyDetailList.stream()
					.filter(detail -> forward.getUnderlyingSecurity().equals(detail.getInvestmentSecurity()))
					.collect(Collectors.toList());
			AccountingJournalDetail forwardUnderlyingCurrencyFirstDetail = CollectionUtils.getFirstElementStrict(forwardUnderlyingCurrencyDetailList);
			BigDecimal forwardUnderlyingCurrencyLocalAmountTotal = CoreMathUtils.sumProperty(forwardUnderlyingCurrencyDetailList, AccountingJournalDetail::getLocalDebitCredit);
			BigDecimal forwardUnderlyingCurrencyBaseAmountTotal = CoreMathUtils.sumProperty(forwardUnderlyingCurrencyDetailList, AccountingJournalDetail::getBaseDebitCredit);
			BigDecimal forwardUnderlyingCurrencyBaseAmountDifference = MathUtils.subtract(forwardUnderlyingCurrencyBaseAmountTotal, InvestmentCalculatorUtils.calculateBaseAmount(forwardUnderlyingCurrencyLocalAmountTotal, forwardUnderlyingCurrencyFirstDetail.getExchangeRateToBase(), forwardUnderlyingCurrencyFirstDetail.getClientInvestmentAccount()));
			if (!MathUtils.isNullOrZero(forwardUnderlyingCurrencyBaseAmountDifference)) {
				forwardUnderlyingCurrencyFirstDetail.setBaseDebitCredit(MathUtils.add(forwardUnderlyingCurrencyFirstDetail.getBaseDebitCredit(), MathUtils.negate(forwardUnderlyingCurrencyBaseAmountDifference)));
			}
		});
	}


	private AccountingJournalDetail generateJournalDetail(AccountingJournal journal, BookingSession<AccountingEventJournal> bookingSession, AccountingEventJournalDetail eventDetail) {
		InvestmentSecurityEvent event = eventDetail.getJournal().getSecurityEvent();
		AccountingTransaction affectedLot = eventDetail.getAccountingTransaction();

		// create and populate new journal detail based on the specified arguments
		AccountingJournalDetail result = new AccountingJournalDetail();
		result.setJournal(journal);
		result.setFkFieldId(eventDetail.getId());
		result.setSystemTable(getAccountingEventJournalDetailTable());

		result.setParentTransaction(affectedLot);
		result.setClientInvestmentAccount(affectedLot.getClientInvestmentAccount());
		result.setHoldingInvestmentAccount(affectedLot.getHoldingInvestmentAccount());
		result.setInvestmentSecurity(affectedLot.getInvestmentSecurity());
		result.setAccountingAccount(affectedLot.getAccountingAccount());
		result.setExchangeRateToBase(eventDetail.getExchangeRateToBase());
		result.setExecutingCompany(affectedLot.getExecutingCompany());

		result.setPrice(eventDetail.getTransactionPrice());
		result.setQuantity(eventDetail.getAffectedQuantity().negate());

		BigDecimal positionCostBasis = getInvestmentCalculator().calculateNotional(result.getInvestmentSecurity(), result.getPrice(), result.getQuantity(), event.getEventDate());

		if (InvestmentUtils.isNoPaymentOnOpen(result.getInvestmentSecurity())) {
			result.setLocalDebitCredit(BigDecimal.ZERO);
		}
		else {
			result.setLocalDebitCredit(positionCostBasis);
		}
		result.setBaseDebitCredit(MathUtils.multiply(result.getLocalDebitCredit(), result.getExchangeRateToBase(), 2));
		result.setPositionCostBasis(positionCostBasis);

		StringBuilder description = new StringBuilder(100);
		description.append(event.getType().getName());
		description.append(" for ");
		description.append(event.getSecurity().getSymbol());
		description.append(" on ");
		description.append(DateUtils.fromDateShort(event.getEventDate()));

		result.setDescription(description.toString());

		// Even Date logic takes into account early terminations and deliverable securities
		result.setOriginalTransactionDate(affectedLot.getOriginalTransactionDate());
		result.setTransactionDate(event.getEventDate());
		result.setSettlementDate(event.getEventDate());

		result.setOpening(false);
		result.setPositionCommission(BigDecimal.ZERO);

		// need to populate booking session with position being closed used by gain/loss rule
		BookingPosition positionBeingClosed = getCurrentPositionBeingClosed(bookingSession, result);
		ValidationUtils.assertNotNull(positionBeingClosed, "Cannot find position being closed for transaction: " + result.getParentTransaction());
		positionBeingClosed.setRemainingQuantity(BigDecimal.ZERO);

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
