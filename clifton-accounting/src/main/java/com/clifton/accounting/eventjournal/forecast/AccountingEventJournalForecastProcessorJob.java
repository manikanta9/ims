package com.clifton.accounting.eventjournal.forecast;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;

import java.util.Date;
import java.util.Map;


/**
 * The <code>AccountingEventJournalForecastProcessorJob</code> class generates accounting event journal forecasts that fall in
 * the specified date range.  Forecasted event journals represent upcoming events for client account positions that haven't been generated yet
 * since we don't actually generate the journal until the event date because we need to know the position quantity that is held on that day
 *
 * @author Mary Anderson
 */
public class AccountingEventJournalForecastProcessorJob implements Task, StatusHolderObjectAware<Status> {

	public static final Integer DEFAULT_DAYS_BACK = 1;
	public static final Integer DEFAULT_DAYS_FORWARD = 30;

	private Integer daysBack = DEFAULT_DAYS_BACK;
	private Integer daysForward = DEFAULT_DAYS_FORWARD;

	private AccountingEventJournalForecastService accountingEventJournalForecastService;

	private StatusHolderObject<Status> statusHolder;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Date endDate = DateUtils.clearTime(new Date());
		Date startDate = DateUtils.addDays(endDate, -getDaysBack());
		endDate = DateUtils.addDays(endDate, getDaysForward());

		this.statusHolder.getStatus().setMessage("Starting Forecast Rebuild for Date Range: " + DateUtils.fromDateRange(startDate, endDate, true));

		AccountingEventJournalForecastProcessorCommand command = new AccountingEventJournalForecastProcessorCommand();
		command.setStartDate(startDate);
		command.setEndDate(endDate);
		command.setDeleteEventsBeforeStartDate(true);
		command.setStatusHolder(this.statusHolder);
		getAccountingEventJournalForecastService().processAccountingEventJournalForecastList(command, true);

		this.statusHolder.getStatus().setMessage("Processing Complete" + ((command.getErrorCount() > 0) ? (" with [" + command.getErrorCount() + "] errors.") : ".") +
				" [" + command.getEventCount() + "] Events Processed. [" + command.getForecastCount() + "] Forecasts Processed.");
		return this.statusHolder.getStatus();
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDaysBack() {
		return this.daysBack;
	}


	public void setDaysBack(Integer daysBack) {
		this.daysBack = daysBack;
	}


	public Integer getDaysForward() {
		return this.daysForward;
	}


	public void setDaysForward(Integer daysForward) {
		this.daysForward = daysForward;
	}


	public AccountingEventJournalForecastService getAccountingEventJournalForecastService() {
		return this.accountingEventJournalForecastService;
	}


	public void setAccountingEventJournalForecastService(AccountingEventJournalForecastService accountingEventJournalForecastService) {
		this.accountingEventJournalForecastService = accountingEventJournalForecastService;
	}
}
