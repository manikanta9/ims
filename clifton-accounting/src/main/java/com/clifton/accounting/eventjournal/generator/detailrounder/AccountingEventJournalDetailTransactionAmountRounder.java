package com.clifton.accounting.eventjournal.generator.detailrounder;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The AccountingEventJournalDetailTransactionAmountRounder class checks if aggregated event journal detail Transaction Amount
 * is equal to the sum of all details (grouped by holding account).  If not, adjusts the largest detail to avoid rounding problems.
 *
 * @author vgomelsky
 */
public class AccountingEventJournalDetailTransactionAmountRounder implements AccountingEventJournalDetailRounder {

	private boolean adjustOpeningCostBasis;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void adjustForLotRounding(AccountingEventJournalDetail aggregateDetail, List<AccountingEventJournalDetail> groupedDetailList) {
		// by default, adjust Transaction Amount (usually payment amount)
		BigDecimal calculatedAmount = CoreMathUtils.sumProperty(groupedDetailList, AccountingEventJournalDetail::getTransactionAmount);
		BigDecimal actualAmount = aggregateDetail.getTransactionAmount();
		if (!MathUtils.isEqual(calculatedAmount, actualAmount)) {
			CoreMathUtils.applySumPropertyDifference(groupedDetailList, AccountingEventJournalDetail::getTransactionAmount, AccountingEventJournalDetail::setTransactionAmount, actualAmount, true, d -> true);
		}

		if (isAdjustOpeningCostBasis()) {
			CoreMathUtils.applySumPropertyDifference(groupedDetailList, AccountingEventJournalDetail::getAdditionalAmount2, AccountingEventJournalDetail::setAdditionalAmount2, aggregateDetail.getAdditionalAmount2(), true);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isAdjustOpeningCostBasis() {
		return this.adjustOpeningCostBasis;
	}


	public void setAdjustOpeningCostBasis(boolean adjustOpeningCostBasis) {
		this.adjustOpeningCostBasis = adjustOpeningCostBasis;
	}
}
