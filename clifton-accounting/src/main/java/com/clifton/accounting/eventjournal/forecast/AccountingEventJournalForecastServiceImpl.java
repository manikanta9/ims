package com.clifton.accounting.eventjournal.forecast;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.forecast.search.AccountingEventJournalForecastSearchForm;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommandInternal;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookingPreviewCommand;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.system.validation.DataAccessExceptionConverter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingEventJournalForecastServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class AccountingEventJournalForecastServiceImpl implements AccountingEventJournalForecastService {

	private AdvancedUpdatableDAO<AccountingEventJournalForecast, Criteria> accountingEventJournalForecastDAO;

	private AccountingBookingService<AccountingEventJournal> accountingBookingService;
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;
	private DataAccessExceptionConverter dataAccessExceptionConverter;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private RunnerHandler runnerHandler;


	////////////////////////////////////////////////////////////////////////////
	/////////                     Forecast  Methods                    /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingEventJournalForecast getAccountingEventJournalForecast(int id) {
		return getAccountingEventJournalForecastDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingEventJournalForecast> getAccountingEventJournalForecastList(AccountingEventJournalForecastSearchForm searchForm) {
		return getAccountingEventJournalForecastDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<AccountingEventJournalForecast> getAccountingEventJournalForecastListForSecurityEvent(int securityEventId) {
		return getAccountingEventJournalForecastDAO().findByField("securityEvent.id", securityEventId);
	}


	private void saveAccountingEventJournalForecastListForSecurityEvent(InvestmentSecurityEvent event, List<AccountingEventJournalForecast> forecastList) {
		List<AccountingEventJournalForecast> existingList = getAccountingEventJournalForecastDAO().findByField("securityEvent.id", event.getId());

		// Instead of Deleting and Inserting all as new - merge existing with new list to properly update/insert/delete
		// IF BOTH lists aren't empty, then need to go through them and properly set ids, etc for updates.
		if (!CollectionUtils.isEmpty(forecastList) && !CollectionUtils.isEmpty(existingList)) {
			for (AccountingEventJournalForecast forecast : CollectionUtils.getIterable(forecastList)) {
				for (AccountingEventJournalForecast existing : existingList) {
					// Call special overridden equals method in AccountingEventJournalForecast to see if both entities are really equal
					if (forecast.equals(existing)) {
						// Set ID on the the new list, so performs an update
						forecast.setId(existing.getId());
					}
				}
			}
		}
		getAccountingEventJournalForecastDAO().saveList(forecastList, existingList);
	}


	@Override
	public void deleteAccountingEventJournalForecastList(List<AccountingEventJournalForecast> list) {
		getAccountingEventJournalForecastDAO().deleteList(list);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////                Forecast Generation Methods               /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processAccountingEventJournalForecastListForSecurityEvent(final int securityEventId, boolean synchronous) {
		if (synchronous) {
			processAccountingEventJournalForecastForSecurityEvent(securityEventId);
		}
		else {
			// asynchronous support - schedules it for 10 seconds in the future so when generating actual event journals and we call this to rebuild, i.e. remove journals now generated
			// we only run it once and give it time to get all journal details inserted/updated
			String runId = "SECURITY_EVENT_" + securityEventId;
			final Date scheduledDate = DateUtils.addSeconds(new Date(), 10);

			Runner runner = new AbstractStatusAwareRunner("ACCOUNTING-EVENT-JOURNAL-FORECAST-", runId, scheduledDate) {

				@Override
				public void run() {
					processAccountingEventJournalForecastForSecurityEvent(securityEventId);
					getStatus().setMessage("Accounting Event Journal Forecast for Event ID [" + securityEventId + "] completed.");
				}
			};
			getRunnerHandler().rescheduleRunner(runner);
		}
	}


	@Override
	public String processAccountingEventJournalForecastList(final AccountingEventJournalForecastProcessorCommand command, boolean synchronous) {
		ValidationUtils.assertNotNull(command.getStartDate(), "Start Date is required for Accounting Event Journal Forecast Rebuild");
		ValidationUtils.assertNotNull(command.getEndDate(), "End Date is required for Accounting Event Journal Forecast Rebuild");
		// We default to 30 days, but for validation, let's keep it to not being able to go more than 2 months in the future
		ValidationUtils.assertFalse(DateUtils.getDaysDifference(command.getEndDate(), new Date()) > 60, "Cannot enter an end date more than 60 days in the future.");

		if (synchronous) {
			processAccountingEventJournalForecastListImpl(command);
			return "Processing Completed for Forecast Rebuilds for " + DateUtils.fromDateRange(command.getStartDate(), command.getEndDate(), true) + ".";
		}

		// asynchronous support
		String runId = DateUtils.fromDateShort(command.getStartDate()) + "_" + DateUtils.fromDateShort(command.getEndDate());
		final Date scheduledDate = new Date();

		Runner runner = new AbstractStatusAwareRunner("ACCOUNTING-EVENT-JOURNAL-FORECAST-", runId, scheduledDate) {

			@Override
			public void run() {
				processAccountingEventJournalForecastListImpl(command);
				getStatus().setMessage("Accounting Event Journal Forecast Rebuild for Date Range [" + DateUtils.fromDateShort(command.getStartDate()) + " to "
						+ DateUtils.fromDateShort(command.getEndDate()) + "] completed.");
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
		return "Processing Started for Forecast Rebuilds from  " + DateUtils.fromDateShort(command.getStartDate()) + " to " + DateUtils.fromDateShort(command.getEndDate())
				+ " and will be completed shortly.";
	}


	private void processAccountingEventJournalForecastListImpl(AccountingEventJournalForecastProcessorCommand command) {
		Date currentDate = command.getStartDate();

		// If opted to Delete forecasts outside of current date range
		if (command.isDeleteEventsBeforeStartDate()) {
			AccountingEventJournalForecastSearchForm searchForm = new AccountingEventJournalForecastSearchForm();
			searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.LESS_THAN, currentDate));
			deleteAccountingEventJournalForecastList(getAccountingEventJournalForecastList(searchForm));
		}
		if (command.isDeleteEventsAfterEndDate()) {
			AccountingEventJournalForecastSearchForm searchForm = new AccountingEventJournalForecastSearchForm();
			searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.GREATER_THAN, command.getEndDate()));
			deleteAccountingEventJournalForecastList(getAccountingEventJournalForecastList(searchForm));
		}

		// Get all events for date range in one lookup and convert to a map for easier look ups
		Map<Date, List<InvestmentSecurityEvent>> eventDateMap = getEventDateMap(command.getStartDate(), command.getEndDate());

		// Start Rebuild - Loop through Each Date to Get all Events for that Date, Generate Previews for each event and save previews as forecasts
		// Previews don't generate for existing generated event journals, so if they've already been generated, we'll delete them from the forecast table.
		while (DateUtils.isDateBetween(currentDate, command.getStartDate(), command.getEndDate(), false)) {
			// 1. get all events on the specified date
			List<InvestmentSecurityEvent> eventList = eventDateMap.get(currentDate);
			int eventListSize = CollectionUtils.getSize(eventList);
			command.incrementEventCount(eventListSize);

			if (eventListSize > 0) {

				// Sort Events in Order (type.eventOrder) as long as there is more than one
				if (eventListSize > 1) {
					eventList = BeanUtils.sortWithFunction(eventList, event -> event.getType().getEventOrder(), true);
				}

				if (command.getStatusHolder() != null) {
					command.getStatusHolder().getStatus().setMessage(
							"Processing Forecast Rebuilds for [" + DateUtils.fromDateRange(command.getStartDate(), command.getEndDate(), true) + "]: Current: " + DateUtils.fromDateShort(currentDate)
									+ ", Events: " + eventListSize);
				}

				// 2. process each event
				for (InvestmentSecurityEvent event : eventList) {
					try {
						command.incrementForecastCount(processAccountingEventJournalForecastForSecurityEventImpl(event));
					}
					catch (Throwable e) {
						if (e instanceof RuntimeException) {
							e = getDataAccessExceptionConverter().convert((RuntimeException) e);
						}
						command.incrementErrorCount();
						command.addFailedEventMessage("\n\n" + event.getLabel() + ": " + e.getMessage());
						if (command.getStatusHolder() != null) {
							command.getStatusHolder().getStatus().addError(event.getLabel() + ": " + ExceptionUtils.getDetailedMessage(e));
						}
						LogUtils.error(getClass(), "Error processing event " + event.getLabel(), e);
					}
				}
			}
			// Process Next Day
			currentDate = DateUtils.addDays(currentDate, 1);
		}
	}


	private int processAccountingEventJournalForecastForSecurityEvent(int securityEventId) {
		InvestmentSecurityEvent event = getInvestmentSecurityEventService().getInvestmentSecurityEvent(securityEventId);
		ValidationUtils.assertNotNull(event, "Cannot find InvestmentSecurityEvent for id: " + securityEventId);
		return processAccountingEventJournalForecastForSecurityEventImpl(event);
	}


	@Transactional
	protected int processAccountingEventJournalForecastForSecurityEventImpl(InvestmentSecurityEvent event) {
		List<AccountingEventJournal> journalList = getAccountingEventJournalGeneratorService().generateAccountingEventJournal(EventJournalGeneratorCommandInternal.forEvent(event, AccountingEventJournalGeneratorTypes.FORECAST));
		List<AccountingEventJournalForecast> forecastList = new ArrayList<>();

		for (AccountingEventJournal journal : CollectionUtils.getIterable(journalList)) {
			Map<AccountingEventJournalForecast, AccountingEventJournalForecast> journalForecastMap = new HashMap<>();
			for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(journal.getDetailList())) {
				AccountingEventJournalForecast forecast = createForecastFromEventJournalDetail(detail);
				journalForecastMap.compute(forecast, (key, current) -> {
					if (current == null) {
						return forecast;
					}

					// Merge Results
					// NOTE: Assuming Transaction Price, FX Rate, and Executing Company would be the same for the same event and client/holding account with multiple lots
					current.setAdditionalAmount(MathUtils.add(current.getAdditionalAmount(), forecast.getAdditionalAmount()));
					current.setAffectedCost(MathUtils.add(current.getAffectedCost(), forecast.getAffectedCost()));
					current.setAffectedQuantity(MathUtils.add(current.getAffectedQuantity(), forecast.getAffectedQuantity()));
					current.setTransactionAmount(MathUtils.add(current.getTransactionAmount(), forecast.getTransactionAmount()));

					// Skip Messages should likely be the same for missing information for the same event/client account but different lots, but if not, keep them both
					if (!StringUtils.isEqual(current.getDescription(), forecast.getDescription())) {
						if (StringUtils.isEmpty(current.getDescription())) {
							current.setDescription(forecast.getDescription());
						}
						else if (!StringUtils.isEmpty(forecast.getDescription())) {
							current.setDescription(current.getDescription() + ", " + forecast.getDescription());
						}
					}
					return current;
				});
			}
			// Get Booking Previews to add Cash/CCY amounts
			List<AccountingJournalDetailDefinition> bookingPreviewDetailList = getAccountingEventJournalBookingPreviewCashCurrencyList(journal);
			for (AccountingJournalDetailDefinition bookingDetail : CollectionUtils.getIterable(bookingPreviewDetailList)) {
				AccountingEventJournalForecast forecast = createForecastFromJournalDetail(bookingDetail, journal);
				journalForecastMap.compute(forecast, (key, current) -> {
					if (current == null) {
						return forecast;
					}

					// Merge Results - Amount ONLY
					current.setTransactionAmount(MathUtils.add(current.getTransactionAmount(), forecast.getTransactionAmount()));
					return current;
				});
			}

			forecastList.addAll(journalForecastMap.values());
		}

		saveAccountingEventJournalForecastListForSecurityEvent(event, forecastList);
		return CollectionUtils.getSize(forecastList);
	}


	private List<AccountingJournalDetailDefinition> getAccountingEventJournalBookingPreviewCashCurrencyList(AccountingEventJournal eventJournal) {
		AccountingJournal journal = getAccountingBookingService().getAccountingJournalPreview(BookingPreviewCommand.of(eventJournal, AccountingJournalType.EVENT_JOURNAL));
		List<AccountingJournalDetailDefinition> detailList = new ArrayList<>();
		for (AccountingJournalDetailDefinition d : CollectionUtils.getIterable(journal.getJournalDetailList())) {
			if (d.getAccountingAccount().isCash() || d.getAccountingAccount().isCurrency()) {
				detailList.add(d);
			}
		}
		return detailList;
	}


	private Map<Date, List<InvestmentSecurityEvent>> getEventDateMap(Date startDate, Date endDate) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		searchForm.setStatusEventJournalAllowed(true); // include only Approved, Manually Approved, Conditionally Approved
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);

		Map<Date, List<InvestmentSecurityEvent>> eventDateMap = new HashMap<>();
		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {
			List<InvestmentSecurityEvent> eventDateList = eventDateMap.computeIfAbsent(event.getEventDate(), key -> new ArrayList<>());
			eventDateList.add(event);
		}
		return eventDateMap;
	}


	private AccountingEventJournalForecast createForecastFromEventJournalDetail(AccountingEventJournalDetail journalDetail) {
		AccountingEventJournalForecast forecast = new AccountingEventJournalForecast();
		// Journal Properties
		forecast.setJournalType(journalDetail.getJournal().getJournalType());
		forecast.setSecurityEvent(journalDetail.getJournal().getSecurityEvent());
		forecast.setPaymentCurrency(journalDetail.getJournal().getSecurityEvent().getSecurity().getInstrument().getTradingCurrency());

		// Accounting Transaction Properties
		forecast.setClientInvestmentAccount(journalDetail.getAccountingTransaction().getClientInvestmentAccount());
		forecast.setHoldingInvestmentAccount(journalDetail.getAccountingTransaction().getHoldingInvestmentAccount());
		forecast.setExecutingCompany(journalDetail.getAccountingTransaction().getExecutingCompany());

		// Detail Properties
		forecast.setAffectedQuantity(journalDetail.getAffectedQuantity() == null ? BigDecimal.ZERO : journalDetail.getAffectedQuantity());
		forecast.setAffectedCost(journalDetail.getAffectedCost());
		forecast.setTransactionAmount(journalDetail.getTransactionAmount());
		forecast.setExchangeRateToBase(journalDetail.getExchangeRateToBase());
		forecast.setTransactionPrice(journalDetail.getTransactionPrice());
		forecast.setAdditionalAmount(journalDetail.getAdditionalAmount());

		forecast.setDescription(journalDetail.getSkipMessage());

		return forecast;
	}


	private AccountingEventJournalForecast createForecastFromJournalDetail(AccountingJournalDetailDefinition journalDetail, AccountingEventJournal eventJournal) {
		AccountingEventJournalForecast forecast = new AccountingEventJournalForecast();
		// Journal Properties
		forecast.setJournalType(eventJournal.getJournalType());
		forecast.setSecurityEvent(eventJournal.getSecurityEvent());
		forecast.setPaymentCurrency(journalDetail.getInvestmentSecurity());

		// Accounting Transaction Properties
		forecast.setClientInvestmentAccount(journalDetail.getClientInvestmentAccount());
		forecast.setHoldingInvestmentAccount(journalDetail.getHoldingInvestmentAccount());
		forecast.setExecutingCompany(journalDetail.getExecutingCompany());

		// Detail Properties
		forecast.setTransactionAmount(journalDetail.getBaseDebitCredit());

		return forecast;
	}


	////////////////////////////////////////////////////////////////////////////
	///////                   Getter & Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingEventJournalForecast, Criteria> getAccountingEventJournalForecastDAO() {
		return this.accountingEventJournalForecastDAO;
	}


	public void setAccountingEventJournalForecastDAO(AdvancedUpdatableDAO<AccountingEventJournalForecast, Criteria> accountingEventJournalForecastDAO) {
		this.accountingEventJournalForecastDAO = accountingEventJournalForecastDAO;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public AccountingEventJournalGeneratorService getAccountingEventJournalGeneratorService() {
		return this.accountingEventJournalGeneratorService;
	}


	public void setAccountingEventJournalGeneratorService(AccountingEventJournalGeneratorService accountingEventJournalGeneratorService) {
		this.accountingEventJournalGeneratorService = accountingEventJournalGeneratorService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public DataAccessExceptionConverter getDataAccessExceptionConverter() {
		return this.dataAccessExceptionConverter;
	}


	public void setDataAccessExceptionConverter(DataAccessExceptionConverter dataAccessExceptionConverter) {
		this.dataAccessExceptionConverter = dataAccessExceptionConverter;
	}


	public AccountingBookingService<AccountingEventJournal> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<AccountingEventJournal> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}
}
