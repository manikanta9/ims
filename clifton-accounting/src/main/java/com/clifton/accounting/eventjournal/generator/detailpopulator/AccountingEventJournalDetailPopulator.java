package com.clifton.accounting.eventjournal.generator.detailpopulator;


/**
 * The <code>AccountingEventJournalDetailPopulator</code> interface allows event/security specific
 * event journal detail field population.
 *
 * @author vgomelsky
 */
public interface AccountingEventJournalDetailPopulator {

	/**
	 * Updates event journal detail fields as appropriate.
	 *
	 * @return true if the specified event should be skipped for the specified position.
	 */
	public boolean populate(PopulatorCommand command);
}
