package com.clifton.accounting;


import com.clifton.accounting.gl.AccountingObjectInfo;


public class AccountingUtils {

	private AccountingUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Security + Client Account + Holding Account + GL Account
	 */
	public static String createAccountingObjectNaturalKey(AccountingObjectInfo accountingObject) {
		StringBuilder buffer = new StringBuilder(32);
		buffer.append("SE").append(accountingObject.getInvestmentSecurity().getIdentity());
		buffer.append("CA").append(accountingObject.getClientInvestmentAccount().getIdentity());
		buffer.append("HA").append(accountingObject.getHoldingInvestmentAccount().getIdentity());
		buffer.append("IA").append(accountingObject.getAccountingAccount().getIdentity());
		return buffer.toString();
	}
}
