package com.clifton.accounting;


import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentTransactionInfo;

import java.math.BigDecimal;


/**
 * The <code>AccountingBean</code> interface defines common methods for entities that get booked to the General Ledger.
 * It can be helpful when there is a need to implement common functionality across multiple sub-systems: for example compliance.
 * NOTE: Currently only AccountingPosition, Trade and LendingRepo objects implements it (should others like AccountingPositionTransferDetail, etc. also implement it?)
 */
public interface AccountingBean extends InvestmentTransactionInfo {

	/**
	 * Primary key of the source entity: trade id, REPO id, etc.
	 */
	public Integer getSourceEntityId();


	/**
	 * Usually a transaction (trade) settles in currency denomination of security (local currency).
	 * However, in some instances one may choose to settle in a different currency (settle in base currency on a foreign swap trade).
	 */
	public InvestmentSecurity getSettlementCurrency();


	public BigDecimal getQuantity();


	/**
	 * Returns accounting notional if quantity is null and security is currency. Otherwise returns quantity.
	 */
	public BigDecimal getQuantityNormalized();


	public BigDecimal getAccountingNotional();


	public BigDecimal getExchangeRateToBase();


	public boolean isBuy();


	public boolean isCollateral();
}
