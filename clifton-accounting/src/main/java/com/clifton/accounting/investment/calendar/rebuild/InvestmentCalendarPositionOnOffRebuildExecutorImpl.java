package com.clifton.accounting.investment.calendar.rebuild;

import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.beans.BatchEntityHolder;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.InvestmentEventType;
import com.clifton.investment.calendar.rebuild.InvestmentCalendarRebuildContext;
import com.clifton.investment.calendar.rebuild.InvestmentCalendarRebuildExecutor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
public class InvestmentCalendarPositionOnOffRebuildExecutorImpl implements InvestmentCalendarRebuildExecutor {

	private AccountingAccountIdsCache accountingAccountIdsCache;
	private AccountingPositionService accountingPositionService;
	private DataTableRetrievalHandler dataTableRetrievalHandler;
	private InvestmentAccountService investmentAccountService;
	private InvestmentCalendarService investmentCalendarService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * If true, looks for positions on, else positions off
	 */
	private boolean positionsOn;

	/**
	 * Shouldn't be used - positions on/off should not include collateral positions
	 */
	private boolean includeCollateral;

	/**
	 * Keys used in the rebuild context so that positions on/off rebuilds can utilize the same data retrievals
	 */
	private static final String CLIENT_ACCOUNT_POSITIONS_ON_OFF_DATE_MAP = "CLIENT_ACCOUNT_POSITIONS_ON_OFF_DATE_MAP";
	private static final String CLIENT_ACCOUNT_TRANSACTIONS_DATE_LIST_MAP = "CLIENT_ACCOUNT_TRANSACTIONS_DATE_LIST_MAP";


	@Override
	public BatchEntityHolder<InvestmentEvent> rebuildInvestmentEventList(InvestmentEventType eventType, InvestmentCalendarRebuildContext rebuildContext) {
		Date startDate = rebuildContext.getStartDateForEventType(eventType);
		Date endDate = rebuildContext.getEndDateForEventType(eventType, startDate);

		BatchEntityHolder<InvestmentEvent> result = new BatchEntityHolder<>(eventType.getName() + ": " + DateUtils.fromDateRange(startDate, endDate, true, true));

		Map<Integer, Set<Date>> clientAccountResultsMap = getClientAccountResultsMap(rebuildContext, startDate, endDate);
		Map<Integer, List<InvestmentEvent>> clientAccountEventsMap = BeanUtils.getBeansMap(rebuildContext.getExistingInvestmentEventList(getInvestmentCalendarService(), eventType, startDate, endDate), investmentEvent -> investmentEvent.getInvestmentAccount().getId());

		for (Map.Entry<Integer, Set<Date>> integerSetEntry : clientAccountResultsMap.entrySet()) {
			List<InvestmentEvent> existingEvents = clientAccountEventsMap.get(integerSetEntry.getKey());
			Set<Integer> processedEvents = new HashSet<>();
			for (Date date : CollectionUtils.getIterable(integerSetEntry.getValue())) {
				boolean found = false;
				for (InvestmentEvent event : CollectionUtils.getIterable(existingEvents)) {
					if (DateUtils.isEqualWithoutTime(date, event.getEventDate())) {
						// No Reason To Update - If the event exists, we just skip it
						processedEvents.add(event.getId());
						found = true;
						break;
					}
				}
				if (!found) {
					InvestmentEvent event = new InvestmentEvent();
					event.setInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(integerSetEntry.getKey()));
					event.setEventType(eventType);
					event.setEventDate(date);
					result.addToInsert(event);
				}
			}
			for (InvestmentEvent event : CollectionUtils.getIterable(existingEvents)) {
				if (!processedEvents.contains(event.getId())) {
					result.addToDelete(event);
				}
			}
		}

		for (Map.Entry<Integer, List<InvestmentEvent>> integerListEntry : clientAccountEventsMap.entrySet()) {
			if (!clientAccountResultsMap.containsKey(integerListEntry.getKey())) {
				result.addAllToDelete(integerListEntry.getValue());
			}
		}

		return result;
	}


	private Map<Integer, Set<Date>> getClientAccountResultsMap(InvestmentCalendarRebuildContext rebuildContext, Date startDate, Date endDate) {

		Map<Integer, List<Date>> clientAccountTransactionDateListMap = getClientAccountTransactionDateListMap(rebuildContext, startDate, endDate);
		Map<Integer, Set<Date>> clientAccountResultsMap = new HashMap<>();

		for (Map.Entry<Integer, List<Date>> integerListEntry : clientAccountTransactionDateListMap.entrySet()) {
			// Are positions on as of start date - 1 day
			boolean clientAccountPositionsOn = isClientAccountPositionsOn(integerListEntry.getKey(), DateUtils.addDays(startDate, -1), rebuildContext);

			// Then loop through each date of transactions for the client account and see if those transaction changed positions on or off
			List<Date> dateList = integerListEntry.getValue();
			Collections.sort(dateList);

			for (Date date : CollectionUtils.getIterable(dateList)) {
				boolean datePositionsOn = isClientAccountPositionsOn(integerListEntry.getKey(), date, rebuildContext);
				if (clientAccountPositionsOn != datePositionsOn) {
					if (isPositionsOn() == datePositionsOn) {
						Set<Date> accountResultDateSet = clientAccountResultsMap.get(integerListEntry.getKey());
						if (accountResultDateSet == null) {
							accountResultDateSet = new HashSet<>();
						}
						accountResultDateSet.add(date);
						clientAccountResultsMap.put(integerListEntry.getKey(), accountResultDateSet);
					}
					clientAccountPositionsOn = datePositionsOn;
				}
			}
		}
		return clientAccountResultsMap;
	}


	private boolean isClientAccountPositionsOn(Integer clientAccountId, Date date, InvestmentCalendarRebuildContext rebuildContext) {
		@SuppressWarnings("unchecked")
		Map<String, Boolean> clientAccountPositionsOnOffDateMap = (Map<String, Boolean>) rebuildContext.getObjectFromContext(CLIENT_ACCOUNT_POSITIONS_ON_OFF_DATE_MAP);
		if (clientAccountPositionsOnOffDateMap == null) {
			clientAccountPositionsOnOffDateMap = new HashMap<>();
		}
		String key = clientAccountId + "_" + DateUtils.fromDateShort(date);
		if (clientAccountPositionsOnOffDateMap.containsKey(key)) {
			return clientAccountPositionsOnOffDateMap.get(key);
		}

		// If not already exists, then we need to look it up
		AccountingPositionCommand accountingPositionCommand = AccountingPositionCommand.onPositionTransactionDate(date);
		accountingPositionCommand.setClientInvestmentAccountId(clientAccountId);
		// When including, we don't set the filter so we get Positions + Positions Collateral
		if (!isIncludeCollateral()) {
			accountingPositionCommand.setCollateral(false);
		}
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(accountingPositionCommand);

		boolean positions = !CollectionUtils.isEmpty(positionList);
		clientAccountPositionsOnOffDateMap.put(key, positions);
		rebuildContext.setObjectInContext(CLIENT_ACCOUNT_POSITIONS_ON_OFF_DATE_MAP, clientAccountPositionsOnOffDateMap);
		return positions;
	}


	private Map<Integer, List<Date>> getClientAccountTransactionDateListMap(InvestmentCalendarRebuildContext rebuildContext, Date startDate, Date endDate) {
		@SuppressWarnings("unchecked")
		Map<String, Map<Integer, List<Date>>> transactionMap = (Map<String, Map<Integer, List<Date>>>) rebuildContext.getObjectFromContext(CLIENT_ACCOUNT_TRANSACTIONS_DATE_LIST_MAP);
		if (transactionMap == null) {
			transactionMap = new HashMap<>();
		}
		String key = getAccountingAccountIds().name() + "_" + DateUtils.fromDateShort(startDate) + "_" + DateUtils.fromDateShort(endDate);
		if (transactionMap.containsKey(key)) {
			return transactionMap.get(key);
		}

		Short[] accountingAccountIds = getAccountingAccountIdsCache().getAccountingAccounts(getAccountingAccountIds());
		// Note we could just pull all positions transactions for the account, however to make it more efficient use custom SQL to just pull ClientAccountID and TransactionDate which is all that we need
		DataTable dataTable = getDataTableRetrievalHandler().findDataTable(new SqlSelectCommand(
				"SELECT ClientInvestmentAccountID, TransactionDate " + //
						"FROM AccountingTransaction " + //
						"WHERE AccountingAccountID " + //
						(accountingAccountIds.length == 1 ? (" = " + accountingAccountIds[0]) : "IN " + Arrays.toString(accountingAccountIds).replace("[", "(").replace("]", ")")) +
						" AND TransactionDate <= ? AND TransactionDate >= ? " + //
						" AND IsDeleted = 0 " + //
						"GROUP BY ClientInvestmentAccountID, TransactionDate")
				.addDateParameterValue(endDate)
				.addDateParameterValue(startDate)
		);

		Map<Integer, List<Date>> clientAccountDateListMap = new HashMap<>();
		if (dataTable != null && dataTable.getTotalRowCount() > 0) {
			for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
				DataRow row = dataTable.getRow(i);
				Integer clientAccountId = (Integer) row.getValue(0);
				Date transactionDate = (Date) row.getValue(1);
				if (!clientAccountDateListMap.containsKey(clientAccountId)) {
					clientAccountDateListMap.put(clientAccountId, new ArrayList<>());
				}
				List<Date> dateList = clientAccountDateListMap.get(clientAccountId);
				dateList.add(transactionDate);

				clientAccountDateListMap.put(clientAccountId, dateList);
			}
		}
		transactionMap.put(key, clientAccountDateListMap);
		rebuildContext.setObjectInContext(CLIENT_ACCOUNT_TRANSACTIONS_DATE_LIST_MAP, transactionMap);
		return clientAccountDateListMap;
	}


	private AccountingAccountIdsCacheImpl.AccountingAccountIds getAccountingAccountIds() {
		if (isIncludeCollateral()) {
			return AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS;
		}
		return AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_COLLATERAL_AND_RECEIVABLE;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}


	public boolean isPositionsOn() {
		return this.positionsOn;
	}


	public void setPositionsOn(boolean positionOn) {
		this.positionsOn = positionOn;
	}


	public boolean isIncludeCollateral() {
		return this.includeCollateral;
	}


	public void setIncludeCollateral(boolean includeCollateral) {
		this.includeCollateral = includeCollateral;
	}
}
