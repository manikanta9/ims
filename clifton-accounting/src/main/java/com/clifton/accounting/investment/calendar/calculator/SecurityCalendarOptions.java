package com.clifton.accounting.investment.calendar.calculator;


import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * Options defining which {@link com.clifton.calendar.setup.Calendar} or calendars to extract from a set of {@link com.clifton.investment.instrument.InvestmentSecurity}s.
 * <p>
 * SETTLEMENT_CALENDAR - Get the settlement calendar for the security's {@link com.clifton.investment.instrument.InvestmentInstrument}
 * EXCHANGE_CALENDAR - Get the calendar from the {@link com.clifton.investment.exchange.InvestmentExchange} of the securities {@link com.clifton.investment.instrument.InvestmentInstrument}
 * COALESCE - Coalesce the two options above, with exchange calendar taking precedence
 *
 * @author mitchellf
 */
public enum SecurityCalendarOptions {

	SETTLEMENT_CALENDAR,
	EXCHANGE_CALENDAR,
	COALESCE;


	public Calendar getCalendarFromSecurity(InvestmentSecurity security) {

		switch (this) {
			case COALESCE: {
				return security.getInstrument().getExchange() != null
						? ObjectUtils.coalesce(security.getInstrument().getExchange().getCalendar(), security.getInstrument().getSettlementCalendar())
						: security.getInstrument().getSettlementCalendar();
			}
			case EXCHANGE_CALENDAR: {
				if (security.getInstrument().getExchange() != null) {
					return security.getInstrument().getExchange().getCalendar();
				}
				return null;
			}
			case SETTLEMENT_CALENDAR: {
				return security.getInstrument().getSettlementCalendar();
			}
			default: {
				return null;
			}
		}
	}
}
