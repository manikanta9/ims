package com.clifton.accounting.investment.calendar.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * Enum to define options for which {@link com.clifton.investment.instrument.InvestmentSecurity} or securities to extract from a client position.
 * <p>
 * POSITION_SECURITY - This will extract the security for which the position is held.
 * POSITION_SECURITY_UNDERLYING - This will extract the underlying security of the position security.
 * POSITION_SECURITY_CCY - This will extract the trading currency for the {@link com.clifton.investment.instrument.InvestmentInstrument} of the security for which the position is held.
 *
 * @author mitchellf
 */
public enum PositionSecurityOptions {

	POSITION_SECURITY,
	POSITION_SECURITY_UNDERLYING,
	POSITION_SECURITY_CCY;


	public InvestmentSecurity getSecurityFromPosition(AccountingPosition position) {
		switch (this) {
			case POSITION_SECURITY: {
				return position.getInvestmentSecurity();
			}
			case POSITION_SECURITY_UNDERLYING: {
				return position.getInvestmentSecurity().getUnderlyingSecurity();
			}
			case POSITION_SECURITY_CCY: {
				return position.getInvestmentSecurity().getInstrument().getTradingCurrency();
			}
			default: {
				return null;
			}
		}
	}
}
