package com.clifton.accounting.investment.account.group.rebuild;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.rebuild.InvestmentAccountGroupEntityGroupRebuildAwareExecutor;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;


/**
 * <code>InvestmentAccountGroupAccountSearchRebuildAwareExecutor</code> is an {@link EntityGroupRebuildAwareExecutor} bean that
 * uses account search criteria to rebuild <code>InvestmentAccount</code> entities mapped to an <code>InvestmentAccountGroup</code>.
 * <p>
 * NOTE: This Bean is used by the Investment Project and is in the Accounting project to avoid introducing a circular dependency
 * between the Accounting and Investment projects.
 *
 * @author NickK
 */
public abstract class BaseInvestmentAccountGroupAccountSearchRebuildAwareExecutor implements InvestmentAccountGroupEntityGroupRebuildAwareExecutor {

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private AccountingPositionService accountingPositionService;

	////////////////////////////////////////////////////////////////////////////////

	private Integer clientRelationshipId;
	private Integer clientId;
	private Short[] clientCategoryIds;
	private Integer[] additionalInvestmentAccountIds;
	private Integer[] excludeInvestmentAccountIds;
	private Short[] accountTypeIds;
	private Short instrumentGroupId;
	private Short[] businessServiceIds;
	private Short[] excludeBusinessServiceIds;
	private Short teamSecurityGroupId;
	private Integer[] issuingCompanyIds;
	private Boolean ourAccount;
	private Short workflowStatusId;
	private Boolean clientDirected;
	private Boolean clientTaxable;
	private Boolean separatePositionRequests;
	private String[] relatedPurposes;
	private Integer[] relatedIssuingCompanyIds;

	////////////////////////////////////////////////////////////////////////////
	//    InvestmentAccountGroupAccountSearchRebuildAwareExecutor Methods    ///
	////////////////////////////////////////////////////////////////////////////

	@Transactional // transactional so all edits to the group are committed/rolled back together.
	@Override
	public Status executeRebuild(EntityGroupRebuildAware groupEntity, Status status) {
		validate(groupEntity);
		InvestmentAccountGroup investmentAccountGroup = (InvestmentAccountGroup) groupEntity;
		Set<InvestmentAccount> rebuildInvestmentAccountSet = new HashSet<>(getRebuildInvestmentAccountList(investmentAccountGroup));
		List<InvestmentAccountGroupAccount> currentInvestmentAccountGroupAccountList = getCurrentInvestmentAccountGroupAccountList(investmentAccountGroup);

		int deleted = 0;
		for (InvestmentAccountGroupAccount accountGroupAccount : CollectionUtils.getIterable(currentInvestmentAccountGroupAccountList)) {
			/*
			 * Remove the account from accounts to add list because it is already included in the security group.
			 * This will avoid adding a duplicate row.
			 */
			if (!rebuildInvestmentAccountSet.remove(accountGroupAccount.getReferenceTwo())) {
				/*
				 * Delete the account from the account group because it no longer matches the rebuild security criteria.
				 */
				getInvestmentAccountGroupService().deleteInvestmentAccountGroupAccount(accountGroupAccount);
				deleted++;
			}
		}
		/*
		 * Add accounts to the account group that previously did not exist in the group.
		 */
		getInvestmentAccountGroupService().linkInvestmentAccountGroupToAccountList(investmentAccountGroup, new ArrayList<>(rebuildInvestmentAccountSet));

		status.setMessage("Completed rebuild of Account Group [%s]; Removed: [%d] Added: [%d]", groupEntity, deleted, rebuildInvestmentAccountSet.size());
		return status;
	}


	private List<InvestmentAccountGroupAccount> getCurrentInvestmentAccountGroupAccountList(InvestmentAccountGroup investmentAccountGroup) {
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setGroupId(investmentAccountGroup.getId());
		return getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm);
	}


	private List<InvestmentAccount> getRebuildInvestmentAccountList(InvestmentAccountGroup investmentAccountGroup) {
		List<InvestmentAccount> rebuildInvestmentAccountList = getInvestmentAccountListFromSearchCriteria(investmentAccountGroup);
		rebuildInvestmentAccountList = applyInstrumentGroupFilter(rebuildInvestmentAccountList);

		if (rebuildInvestmentAccountList == null) {
			rebuildInvestmentAccountList = new ArrayList<>();
		}
		if (getAdditionalInvestmentAccountIds() != null && getAdditionalInvestmentAccountIds().length > 0) {
			for (Integer additionalAccountId : getAdditionalInvestmentAccountIds()) {
				InvestmentAccount account = getInvestmentAccountService().getInvestmentAccount(additionalAccountId);
				if (!rebuildInvestmentAccountList.contains(account)) {
					rebuildInvestmentAccountList.add(account);
				}
			}
		}
		return rebuildInvestmentAccountList;
	}


	private List<InvestmentAccount> getInvestmentAccountListFromSearchCriteria(InvestmentAccountGroup investmentAccountGroup) {
		InvestmentAccountSearchForm accountSearchForm = new InvestmentAccountSearchForm();
		accountSearchForm.setClientRelationshipId(getClientRelationshipId());
		accountSearchForm.setClientId(getClientId());
		if (investmentAccountGroup.getAccountType() != null) {
			accountSearchForm.setAccountTypeId(investmentAccountGroup.getAccountType().getId());
		}
		else if (getAccountTypeIds() != null) {
			if (getAccountTypeIds().length == 1) {
				accountSearchForm.setAccountTypeId(getAccountTypeIds()[0]);
			}
			else {
				accountSearchForm.setAccountTypeIds(getAccountTypeIds());
			}
		}
		if (getBusinessServiceIds() != null) {
			if (getBusinessServiceIds().length == 1) {
				accountSearchForm.setBusinessServiceId(getBusinessServiceIds()[0]);
			}
			else {
				accountSearchForm.setBusinessServiceIds(getBusinessServiceIds());
			}
		}
		if (getExcludeBusinessServiceIds() != null) {
			if (getExcludeBusinessServiceIds().length == 1) {
				accountSearchForm.setExcludeBusinessServiceId(getExcludeBusinessServiceIds()[0]);
			}
			else {
				accountSearchForm.setExcludeBusinessServiceIds(getExcludeBusinessServiceIds());
			}
		}
		accountSearchForm.setTeamSecurityGroupId(getTeamSecurityGroupId());
		if (getIssuingCompanyIds() != null) {
			if (getIssuingCompanyIds().length == 1) {
				accountSearchForm.setIssuingCompanyId(getIssuingCompanyIds()[0]);
			}
			else {
				accountSearchForm.setIssuingCompanyIds(getIssuingCompanyIds());
			}
		}
		if (getRelatedIssuingCompanyIds() != null) {
			if (getRelatedIssuingCompanyIds().length == 1) {
				accountSearchForm.setRelatedAccountIssuingCompanyId(getRelatedIssuingCompanyIds()[0]);
			}
			else {
				accountSearchForm.setRelatedAccountIssuingCompanyIds(getRelatedIssuingCompanyIds());
			}
		}
		if (getRelatedPurposes() != null) {
			if (getRelatedPurposes().length == 1) {
				accountSearchForm.setRelatedPurpose(getRelatedPurposes()[0]);
			}
			else {
				accountSearchForm.setRelatedPurposes(getRelatedPurposes());
			}
		}

		accountSearchForm.setOurAccount(getOurAccount());
		accountSearchForm.setWorkflowStatusId(getWorkflowStatusId());
		accountSearchForm.setClientDirected(getClientDirected());
		accountSearchForm.setClientTaxable(getClientTaxable());

		if (getClientCategoryIds() != null) {
			accountSearchForm.setClientCategoryIds(getClientCategoryIds());
		}
		if (getExcludeInvestmentAccountIds() != null) {
			accountSearchForm.setExcludeIds(getExcludeInvestmentAccountIds());
		}
		if (applyAdditionalInvestmentAccountSearchFormFilters(accountSearchForm)) {
			return getInvestmentAccountService().getInvestmentAccountList(accountSearchForm);
		}
		return new ArrayList<>();
	}


	/**
	 * If returns false, then no accounts applied to additional filters and should return empty list.
	 * Return true to continue with search form filtering
	 */
	public abstract boolean applyAdditionalInvestmentAccountSearchFormFilters(InvestmentAccountSearchForm accountSearchForm);


	/**
	 * If applyFilter is <code>true</code>, returns the subset of the provided {@link InvestmentAccount}s containing
	 * accounts with an open position on any security in {@link BaseInvestmentAccountGroupAccountSearchRebuildAwareExecutor#getInstrumentGroupId()}.
	 * <p>
	 * If applyFilter is <code>false</code>, will ignore the provided list and find all InvestmentAccounts with an open
	 * position on any security in {@link BaseInvestmentAccountGroupAccountSearchRebuildAwareExecutor#getInstrumentGroupId()}.
	 */
	private List<InvestmentAccount> applyInstrumentGroupFilter(List<InvestmentAccount> investmentAccountList) {
		if ((getInstrumentGroupId() == null) || CollectionUtils.isEmpty(investmentAccountList)) {
			return investmentAccountList;
		}

		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(new Date());
		command.setInvestmentGroupId(getInstrumentGroupId());

		if (BooleanUtils.isTrue(getSeparatePositionRequests())) {
			return removeInvestmentAccountsWithoutPosition(investmentAccountList, command);
		}
		else {
			return filterInvestmentAccountsToThoseWithPosition(investmentAccountList, command);
		}
	}


	/**
	 * Returns the List of Investment Account that have a position on a Security within the Investment Group on the provided {@link AccountingPositionCommand}.
	 * The positions will be looked up by grouping the provided Investment Account by their type (Client and/or Holding Account).
	 */
	private List<InvestmentAccount> filterInvestmentAccountsToThoseWithPosition(List<InvestmentAccount> investmentAccountsToFilter, AccountingPositionCommand command) {
		// Use a Set to collect the filtered accounts which will collect distinct accounts
		Set<InvestmentAccount> investmentAccountWithPositionSet = new HashSet<>();
		Map<Boolean, List<InvestmentAccount>> investmentAccountMap = BeanUtils.getBeansMap(investmentAccountsToFilter, investmentAccount -> investmentAccount.getType().isOurAccount());

		List<InvestmentAccount> clientAccountList = investmentAccountMap.get(Boolean.TRUE);
		if (!CollectionUtils.isEmpty(clientAccountList)) {
			command.setClientInvestmentAccountIds(BeanUtils.getBeanIdentityArray(clientAccountList, Integer.class));
			populateSetWithInvestmentAccountsWithPosition(investmentAccountWithPositionSet, clientAccountList, command, AccountingPosition::getClientInvestmentAccount);
			// clear Client Account IDs for Holding Account IDs search
			command.setClientInvestmentAccountIds(null);
		}

		List<InvestmentAccount> holdingAccountList = investmentAccountMap.get(Boolean.FALSE);
		if (!CollectionUtils.isEmpty(holdingAccountList)) {
			command.setHoldingInvestmentAccountIds(BeanUtils.getBeanIdentityArray(holdingAccountList, Integer.class));
			populateSetWithInvestmentAccountsWithPosition(investmentAccountWithPositionSet, holdingAccountList, command, AccountingPosition::getHoldingInvestmentAccount);
		}

		return new ArrayList<>(investmentAccountWithPositionSet);
	}


	/**
	 * Populates the provided Set with the Investment Accounts, from the list provided, having a position on a Security within the Instrument Group on the provided AccountingPositionCommand.
	 * The provided Function will be used to extract the Investment Account from an {@link AccountingPosition} (Client or Holding Account).
	 */
	private void populateSetWithInvestmentAccountsWithPosition(Set<InvestmentAccount> accountWithPositionSet, List<InvestmentAccount> accountsToFilter, AccountingPositionCommand command, Function<AccountingPosition, InvestmentAccount> accountingPositionAccountExtractor) {
		List<AccountingPosition> accountingPositionList = getAccountingPositionService().getAccountingPositionListUsingCommand(command);
		for (AccountingPosition accountingPosition : CollectionUtils.getIterable(accountingPositionList)) {
			int accountIndex = accountsToFilter.indexOf(accountingPositionAccountExtractor.apply(accountingPosition));
			if (accountIndex > -1) {
				accountWithPositionSet.add(accountsToFilter.get(accountIndex));
			}
		}
	}


	/**
	 * Returns the List of Investment Account that have a position on a Security within the InvestmentGroup set on the provided {@link AccountingPositionCommand}.
	 * The positions will be looked up individually by each provided Investment Account by its type (Client and/or Holding Account).
	 */
	private List<InvestmentAccount> removeInvestmentAccountsWithoutPosition(List<InvestmentAccount> accountsToFilter, AccountingPositionCommand command) {
		/*
		 * Tasks to clear command values to avoid additional condition checking and object creation
		 */
		ClearTask clearCommandClientAccountTask = () -> command.setClientInvestmentAccountId(null);
		ClearTask clearCommandHoldingAccountTask = () -> command.setHoldingInvestmentAccountId(null);

		Iterator<InvestmentAccount> accountIterator = CollectionUtils.getIterable(accountsToFilter).iterator();
		while (accountIterator.hasNext()) {
			InvestmentAccount account = accountIterator.next();
			ClearTask clearCommandTask;
			if (account.getType().isOurAccount()) {
				command.setClientInvestmentAccountId(account.getId());
				clearCommandTask = clearCommandClientAccountTask;
			}
			else {
				command.setHoldingInvestmentAccountId(account.getId());
				clearCommandTask = clearCommandHoldingAccountTask;
			}

			if (CollectionUtils.isEmpty(getAccountingPositionService().getAccountingPositionListUsingCommand(command))) {
				accountIterator.remove();
			}
			// reset command for next account
			clearCommandTask.execute();
		}
		return accountsToFilter;
	}


	@Override
	public void validate(EntityGroupRebuildAware groupEntity) {
		ValidationUtils.assertTrue(groupEntity instanceof InvestmentAccountGroup, "Rebuild executor applies to InvestmentAccountGroups only.");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Simple interface for defining a strategy for reverting an object's state between iterations.
	 */
	private static interface ClearTask {

		public void execute();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public Integer getClientRelationshipId() {
		return this.clientRelationshipId;
	}


	public void setClientRelationshipId(Integer clientRelationshipId) {
		this.clientRelationshipId = clientRelationshipId;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Short[] getClientCategoryIds() {
		return this.clientCategoryIds;
	}


	public void setClientCategoryIds(Short[] clientCategoryIds) {
		this.clientCategoryIds = clientCategoryIds;
	}


	public Integer[] getAdditionalInvestmentAccountIds() {
		return this.additionalInvestmentAccountIds;
	}


	public void setAdditionalInvestmentAccountIds(Integer[] additionalInvestmentAccountIds) {
		this.additionalInvestmentAccountIds = additionalInvestmentAccountIds;
	}


	public Integer[] getExcludeInvestmentAccountIds() {
		return this.excludeInvestmentAccountIds;
	}


	public void setExcludeInvestmentAccountIds(Integer[] excludeInvestmentAccountIds) {
		this.excludeInvestmentAccountIds = excludeInvestmentAccountIds;
	}


	public Short[] getAccountTypeIds() {
		return this.accountTypeIds;
	}


	public void setAccountTypeIds(Short[] accountTypeIds) {
		this.accountTypeIds = accountTypeIds;
	}


	public Short getInstrumentGroupId() {
		return this.instrumentGroupId;
	}


	public void setInstrumentGroupId(Short instrumentGroupId) {
		this.instrumentGroupId = instrumentGroupId;
	}


	public Short[] getBusinessServiceIds() {
		return this.businessServiceIds;
	}


	public void setBusinessServiceIds(Short[] businessServiceIds) {
		this.businessServiceIds = businessServiceIds;
	}


	public Short[] getExcludeBusinessServiceIds() {
		return this.excludeBusinessServiceIds;
	}


	public void setExcludeBusinessServiceIds(Short[] excludeBusinessServiceIds) {
		this.excludeBusinessServiceIds = excludeBusinessServiceIds;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Integer[] getIssuingCompanyIds() {
		return this.issuingCompanyIds;
	}


	public void setIssuingCompanyIds(Integer[] issuingCompanyIds) {
		this.issuingCompanyIds = issuingCompanyIds;
	}


	public Boolean getOurAccount() {
		return this.ourAccount;
	}


	public void setOurAccount(Boolean ourAccount) {
		this.ourAccount = ourAccount;
	}


	public Short getWorkflowStatusId() {
		return this.workflowStatusId;
	}


	public void setWorkflowStatusId(Short workflowStatusId) {
		this.workflowStatusId = workflowStatusId;
	}


	public Boolean getClientDirected() {
		return this.clientDirected;
	}


	public void setClientDirected(Boolean clientDirected) {
		this.clientDirected = clientDirected;
	}


	public Boolean getClientTaxable() {
		return this.clientTaxable;
	}


	public void setClientTaxable(Boolean clientTaxable) {
		this.clientTaxable = clientTaxable;
	}


	public Boolean getSeparatePositionRequests() {
		return this.separatePositionRequests;
	}


	public void setSeparatePositionRequests(Boolean separatePositionRequests) {
		this.separatePositionRequests = separatePositionRequests;
	}


	public String[] getRelatedPurposes() {
		return this.relatedPurposes;
	}


	public void setRelatedPurposes(String[] relatedPurposes) {
		this.relatedPurposes = relatedPurposes;
	}


	public Integer[] getRelatedIssuingCompanyIds() {
		return this.relatedIssuingCompanyIds;
	}


	public void setRelatedIssuingCompanyIds(Integer[] relatedIssuingCompanyIds) {
		this.relatedIssuingCompanyIds = relatedIssuingCompanyIds;
	}
}
