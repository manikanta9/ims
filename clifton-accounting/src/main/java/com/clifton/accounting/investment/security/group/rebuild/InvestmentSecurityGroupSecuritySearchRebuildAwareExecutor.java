package com.clifton.accounting.investment.security.group.rebuild;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>InvestmentSecurityGroupSecuritySearchRebuildAwareExecutor</code> is an {@link EntityGroupRebuildAwareExecutor} bean that
 * uses security search criteria to rebuild <code>InvestmentSecurity</code> entities mapped to an <code>InvestmentSecurityGroup</code>.
 *
 * @author NickK
 * @see InvestmentSecurityGroupSecurity
 */
public class InvestmentSecurityGroupSecuritySearchRebuildAwareExecutor implements EntityGroupRebuildAwareExecutor, ValidationAware {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private AccountingPositionService accountingPositionService;
	private BusinessCompanyService businessCompanyService;

	// Investment filtering criteria
	private Short investmentGroupId;
	private Short investmentTypeId;
	private Short investmentTypeSubTypeId;
	private Short investmentTypeSubType2Id;
	private Short investmentInstrumentHierarchyId;
	private Integer underlyingSecurityId;
	private Integer[] issuerIds;
	private Integer[] parentIssuerIds;
	private Integer[] obligorIssuerIds;
	private Integer[] clientAccountIds;
	private Integer clientAccountGroupId;

	// Security search form criterion array
	private String[] securityCriteria;

	// Custom column filters to filter securities
	private String customColumnName;
	private String customColumnValue;

	private Integer daysSinceExpiration;

	// Filter securities by whether they have open positions
	private Boolean openPositions;
	private Boolean separatePositionRequests; // look up positions uniquely for each potential security

	// Rebuild the group to include Securities used as an Underlying for positions or Securities matching the other rebuild criteria.
	private Boolean useUnderlyingSecurity;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		if (getSecurityCriteria() != null) {
			for (String criterion : getSecurityCriteria()) {
				String[] fieldValue = criterion.split("=");
				ValidationUtils.assertTrue(fieldValue.length == 2, "Expected to find a Security criterion with a field and a value only, found: " + Arrays.toString(fieldValue));
				ValidationUtils.assertTrue(BeanUtils.isPropertyPresent(SecuritySearchForm.class, fieldValue[0]), "Security search field does not exist for: " + fieldValue[0]);
			}
		}
		if (getClientAccountIds() != null || getClientAccountGroupId() != null) {
			ValidationUtils.assertNotNull(getOpenPositions(), "Open Positions must be true if client account or client account group is provided.");
			ValidationUtils.assertTrue(getOpenPositions(), "Open Positions must be true if client account or client account group is provided.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//   InvestmentSecurityGroupSecuritySearchRebuildAwareExecutor Methods   ///
	////////////////////////////////////////////////////////////////////////////


	@Transactional // transactional so all edits to the group are committed/rolled back together.
	@Override
	public Status executeRebuild(EntityGroupRebuildAware groupEntity, Status status) {
		validate(groupEntity);

		InvestmentSecurityGroup investmentSecurityGroup = (InvestmentSecurityGroup) groupEntity;
		Set<InvestmentSecurity> rebuildInvestmentSecuritySet = new HashSet<>(getRebuildInvestmentSecurityList());
		List<InvestmentSecurityGroupSecurity> currentInvestmentSecurityGroupSecurityList = getInvestmentSecurityGroupService().getInvestmentSecurityGroupSecurityListByGroup(investmentSecurityGroup.getId());

		int removed = 0;
		for (InvestmentSecurityGroupSecurity securityGroupSecurity : CollectionUtils.getIterable(currentInvestmentSecurityGroupSecurityList)) {
			/*
			 * Remove the security from securities to add list because it is already included in the security group.
			 * This will avoid adding a duplicate row.
			 */
			if (!rebuildInvestmentSecuritySet.remove(securityGroupSecurity.getReferenceTwo())) {
				/*
				 * Delete the security from the security group because it no longer matches the rebuild security criteria.
				 * The most likely cause is expiration of the security.
				 */
				getInvestmentSecurityGroupService().deleteInvestmentSecurityGroupSecurity(securityGroupSecurity);
				removed++;
			}
		}
		/*
		 * Add securities to the security group that previously did not exist in the group.
		 */
		getInvestmentSecurityGroupService().linkInvestmentSecurityGroupToSecurityList(investmentSecurityGroup, new ArrayList<>(rebuildInvestmentSecuritySet));

		status.setMessage(String.format("Rebuild execution completed for Investment Security Group [%s]; Processed: [%d] Removed: [%d] Linked: [%d]",
				investmentSecurityGroup.getName(), currentInvestmentSecurityGroupSecurityList.size(), removed, rebuildInvestmentSecuritySet.size()));
		return status;
	}


	/**
	 * Returns a list of Securities that match the rebuild criteria of the bean for inclusion in the group.
	 */
	private List<InvestmentSecurity> getRebuildInvestmentSecurityList() {
		List<InvestmentSecurity> rebuildInvestmentSecurityList = getInvestmentSecurityListFromSearchCriteria();
		rebuildInvestmentSecurityList = applyActivePositionFilter(rebuildInvestmentSecurityList);
		if (BooleanUtils.isTrue(getUseUnderlyingSecurity())) {
			rebuildInvestmentSecurityList = CollectionUtils.getStream(rebuildInvestmentSecurityList).map(InvestmentSecurity::getUnderlyingSecurity).distinct().collect(Collectors.toList());
		}
		return CollectionUtils.asNonNullList(rebuildInvestmentSecurityList);
	}


	/**
	 * Returns a list of securities for based on Security search criteria only.
	 */
	private List<InvestmentSecurity> getInvestmentSecurityListFromSearchCriteria() {
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setInvestmentGroupId(getInvestmentGroupId());
		securitySearchForm.setInvestmentTypeId(getInvestmentTypeId());
		securitySearchForm.setInvestmentTypeSubTypeId(getInvestmentTypeSubTypeId());
		securitySearchForm.setInvestmentTypeSubType2Id(getInvestmentTypeSubType2Id());
		securitySearchForm.setHierarchyId(getInvestmentInstrumentHierarchyId());
		securitySearchForm.setUnderlyingSecurityId(getUnderlyingSecurityId());
		securitySearchForm.setCustomColumnName(getCustomColumnName());
		securitySearchForm.setCustomColumnValue(getCustomColumnValue());

		Set<Integer> issuerSet = getIssuerSet();
		if (!issuerSet.isEmpty()){
			securitySearchForm.setBusinessCompanyIds(issuerSet.toArray(new Integer[0]));
		}
		if (getDaysSinceExpiration() != null) {
			securitySearchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.GREATER_THAN_OR_IS_NULL, DateUtils.addDays(new Date(), getDaysSinceExpiration())));
		}
		if (getSecurityCriteria() != null) {
			for (String criterion : getSecurityCriteria()) {
				String[] fieldValue = criterion.split("=");
				// TODO add support for different data types and search restriction conditions (e.g. >, <, etc.)
				BeanUtils.setPropertyValue(securitySearchForm, fieldValue[0], StringUtils.isEmpty(fieldValue[1]) ? null : fieldValue[1]);
			}
		}
		if (getOpenPositions() != null) {
			securitySearchForm.setTradingDisallowed(Boolean.FALSE);
		}


		return getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);
	}


	/**
	 * Returns a filtered list of Securities containing those with open positions.
	 */
	private List<InvestmentSecurity> applyActivePositionFilter(List<InvestmentSecurity> investmentSecurityList) {
		if (getOpenPositions() == null || CollectionUtils.isEmpty(investmentSecurityList)) {
			return investmentSecurityList;
		}

		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(new Date());

		if (getOpenPositions()) {
			if (!ArrayUtils.isEmpty(getClientAccountIds())) {
				if (getClientAccountIds().length == 1) {
					command.setClientInvestmentAccountId(getClientAccountIds()[0]);
				}
				else {
					command.setClientInvestmentAccountIds(getClientAccountIds());
				}
			}
			if (getClientAccountGroupId() != null) {
				command.setClientInvestmentAccountGroupId(getClientAccountGroupId());
			}
		}

		if (BooleanUtils.isTrue(getSeparatePositionRequests())) {
			return CollectionUtils.getStream(investmentSecurityList).filter(investmentSecurity -> {
				command.setInvestmentSecurityId(investmentSecurity.getId());
				boolean empty = CollectionUtils.isEmpty(getAccountingPositionService().getAccountingPositionListUsingCommand(command));
				return getOpenPositions() ? !empty : empty;
			}).collect(Collectors.toList());
		}
		else {
			command.setInvestmentSecurityIds(BeanUtils.getBeanIdentityArray(investmentSecurityList, Integer.class));
			List<AccountingPosition> accountingPositionList = getAccountingPositionService().getAccountingPositionListUsingCommand(command);
			List<InvestmentSecurity> securityWithPositionList = CollectionUtils.getStream(accountingPositionList).map(AccountingPosition::getInvestmentSecurity).filter(investmentSecurityList::contains).distinct().collect(Collectors.toList());
			if (getOpenPositions()) {
				// Return the objects from investmentSecurityList because they are hydrated with Underlying Security for future filtering.
				return CollectionUtils.getStream(investmentSecurityList).filter(securityWithPositionList::contains).collect(Collectors.toList());
			}
			else {
				investmentSecurityList.removeAll(securityWithPositionList);
				return investmentSecurityList;
			}
		}
	}


	@Override
	public void validate(EntityGroupRebuildAware groupEntity) {
		ValidationUtils.assertTrue(groupEntity instanceof InvestmentSecurityGroup, "Rebuild executor applies to InvestmentSecurityGroups only.");
	}


	private Set<Integer> getIssuerSet() {
		Set<Integer> issuers = new HashSet<>();

		ArrayUtils.getStream(getIssuerIds())
				.forEach(issuers::add);

		Set<BusinessCompany> ultimateIssuerCompanies = getUltimateIssuerCompanies();
		if (!CollectionUtils.isEmpty(ultimateIssuerCompanies)) {
			ultimateIssuerCompanies.forEach(company -> issuers.add(company.getId()));
		}
		ArrayUtils.getStream(getParentIssuerIds())
				.forEach(issuers::add);

		Set<BusinessCompany> ultimateObligorCompanies = getUltimateObligorCompanies();
		if (!CollectionUtils.isEmpty(ultimateObligorCompanies)) {
			ultimateObligorCompanies.forEach(company -> issuers.add(company.getId()));
		}
		ArrayUtils.getStream(getObligorIssuerIds())
				.forEach(issuers::add);

		return issuers;
	}


	private Set<BusinessCompany> getUltimateIssuerCompanies() {
		Set<BusinessCompany> result = new HashSet<>();
		if (!ArrayUtils.isEmpty(getParentIssuerIds())) {
			for (int parentId : getParentIssuerIds()) {
				result.addAll(getBusinessCompanyService().getBusinessCompanyChildrenListForParent(parentId));
			}
		}
		return result;
	}


	private Set<BusinessCompany> getUltimateObligorCompanies() {
		Set<BusinessCompany> result = new HashSet<>();
		if (!ArrayUtils.isEmpty(getObligorIssuerIds())) {
			for (int obligorId : getObligorIssuerIds()) {
				result.addAll(getBusinessCompanyService().getBusinessCompanyChildrenListForObligor(obligorId));
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Short getInvestmentInstrumentHierarchyId() {
		return this.investmentInstrumentHierarchyId;
	}


	public void setInvestmentInstrumentHierarchyId(Short investmentInstrumentHierarchyId) {
		this.investmentInstrumentHierarchyId = investmentInstrumentHierarchyId;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public Integer[] getIssuerIds() {
		return this.issuerIds;
	}


	public void setIssuerIds(Integer[] issuerIds) {
		this.issuerIds = issuerIds;
	}


	public Integer[] getClientAccountIds() {
		return this.clientAccountIds;
	}


	public void setClientAccountIds(Integer[] clientAccountIds) {
		this.clientAccountIds = clientAccountIds;
	}


	public Integer getClientAccountGroupId() {
		return this.clientAccountGroupId;
	}


	public void setClientAccountGroupId(Integer clientAccountGroupId) {
		this.clientAccountGroupId = clientAccountGroupId;
	}


	public String[] getSecurityCriteria() {
		return this.securityCriteria;
	}


	public void setSecurityCriteria(String[] securityCriteria) {
		this.securityCriteria = securityCriteria;
	}


	public String getCustomColumnName() {
		return this.customColumnName;
	}


	public void setCustomColumnName(String customColumnName) {
		this.customColumnName = customColumnName;
	}


	public String getCustomColumnValue() {
		return this.customColumnValue;
	}


	public void setCustomColumnValue(String customColumnValue) {
		this.customColumnValue = customColumnValue;
	}


	public Integer getDaysSinceExpiration() {
		return this.daysSinceExpiration;
	}


	public void setDaysSinceExpiration(Integer daysSinceExpiration) {
		this.daysSinceExpiration = daysSinceExpiration;
	}


	public Boolean getOpenPositions() {
		return this.openPositions;
	}


	public void setOpenPositions(Boolean openPositions) {
		this.openPositions = openPositions;
	}


	public Boolean getSeparatePositionRequests() {
		return this.separatePositionRequests;
	}


	public void setSeparatePositionRequests(Boolean separatePositionRequests) {
		this.separatePositionRequests = separatePositionRequests;
	}


	public Boolean getUseUnderlyingSecurity() {
		return this.useUnderlyingSecurity;
	}


	public void setUseUnderlyingSecurity(Boolean useUnderlyingSecurity) {
		this.useUnderlyingSecurity = useUnderlyingSecurity;
	}


	public Integer[] getParentIssuerIds() {
		return this.parentIssuerIds;
	}


	public void setParentIssuerIds(Integer[] parentIssuerIds) {
		this.parentIssuerIds = parentIssuerIds;
	}


	public Integer[] getObligorIssuerIds() {
		return this.obligorIssuerIds;
	}


	public void setObligorIssuerIds(Integer[] obligorIssuerIds) {
		this.obligorIssuerIds = obligorIssuerIds;
	}
}
