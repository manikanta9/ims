package com.clifton.accounting.investment.calendar.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.calendar.calculator.CalendarCalculationResult;
import com.clifton.calendar.calculator.CalendarListCalculator;
import com.clifton.calendar.calculator.CalendarManualCalendarListCalculator;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Implementation of {@link CalendarListCalculator} that filters the calendars by investment securities. This particular implementation used the defined parameters to find a list
 * of positions for a given client account or client account group.  The positions are then mapped to a list of securities using defined parameters, and that list of
 * securities is further mapped to the list of calendars, using additional parameters.
 *
 * @author mitchellf
 */
public class InvestmentSecurityCalendarListCalculator extends CalendarManualCalendarListCalculator {

	/**
	 * This field defines a single client account for which to find positions.
	 */
	private Integer clientAccountId;

	/**
	 * If this field is set, positions will be filtered to only those for securities in this {@link com.clifton.investment.setup.group.InvestmentSecurityGroup}
	 */
	private Short securityGroupId;

	/**
	 * If this field is set, positions will be filtered to only those for client accounts in this {@link com.clifton.investment.account.group.InvestmentAccountGroup}
	 */
	private Integer clientAccountGroupId;

	/**
	 * Defines which security/securities to extract from the calculated set of positions.  See {@link PositionSecurityOptions} for details.
	 */
	private List<PositionSecurityOptions> positionSecuritySelection;

	/**
	 * Defines which calendar/calendars to extract from the calculated set of securities.  See {@link SecurityCalendarOptions} for details.
	 */
	private List<SecurityCalendarOptions> securityCalendarSelection;

	/**
	 * If this field is set to true, the base currency of the client account for each position will also be included in the list of securities to process.
	 */
	private boolean extractPositionClientAcctBaseCCY;

	private AccountingPositionService accountingPositionService;
	private InvestmentGroupService investmentGroupService;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;


	@Override
	public CalendarCalculationResult calculate(Date balanceDate) {
		Status status = new Status();

		AccountingPositionCommand accountingPositionCommand = AccountingPositionCommand.onPositionTransactionDate(balanceDate)
				.forClientInvestmentAccountGroup(getClientAccountGroupId());

		if (getSecurityGroupId() != null) {
			// map to list of security ids
			Integer[] securityIds = CollectionUtils.getStream(getInvestmentSecurityGroupService().getInvestmentSecurityListForSecurityGroup(getSecurityGroupId(), null, null))
					.map(InvestmentSecurity::getId)
					.distinct()
					.toArray(Integer[]::new);

			accountingPositionCommand.setInvestmentSecurityIds(securityIds);
		}

		if (getClientAccountId() != null) {
			accountingPositionCommand.setClientInvestmentAccountId(getClientAccountId());
		}


		Set<Calendar> calendars = Collections.emptySet();
		CalendarCalculationResult result = new CalendarCalculationResult();

		try {
			List<AccountingPosition> positions = getAccountingPositionService().getAccountingPositionListUsingCommand(accountingPositionCommand);
			Set<InvestmentSecurity> securities = Collections.emptySet();
			if (!CollectionUtils.isEmpty(positions)) {
				securities = getSecurityList(positions);
				if (CollectionUtils.isEmpty(securities)) {
					status.addWarning("Did not find any securities for the calculated list of positions.");
				}
			}
			else {
				status.addWarning("Did not find any positions for the specified criteria.");
			}
			// we will still call this method even if there are no securities to account for "Calendars to Include"
			calendars = getCalendarSet(securities);
		}
		catch (Exception e) {
			status.addError(e.getMessage());
		}

		status.setMessage(calendars.isEmpty() ? "Did not find any calendars for the provided criteria." : "Successfully found " + calendars.size() + " calendars.");
		result.setCalendarList(CollectionUtils.toArrayList(calendars));
		result.setStatus(status);

		return result;
	}


	private Set<InvestmentSecurity> getSecurityList(Collection<AccountingPosition> positions) {
		Set<InvestmentSecurity> securities = new HashSet<>();
		for (AccountingPosition pos : positions) {
			for (PositionSecurityOptions option : getPositionSecuritySelection()) {
				InvestmentSecurity sec = option.getSecurityFromPosition(pos);
				if (sec != null) {
					securities.add(sec);
				}
			}
			if (isExtractPositionClientAcctBaseCCY()) {
				InvestmentSecurity baseCCY = pos.getClientInvestmentAccount().getBaseCurrency();
				if (baseCCY != null) {
					securities.add(baseCCY);
				}
			}
		}
		return securities;
	}


	private Set<Calendar> getCalendarSet(Collection<InvestmentSecurity> securities) {
		Set<Calendar> calendars = new HashSet<>();

		for (InvestmentSecurity security : securities) {
			for (SecurityCalendarOptions selection : getSecurityCalendarSelection()) {
				Calendar calendar = selection.getCalendarFromSecurity(security);
				if (calendar != null) {
					calendars.add(calendar);
				}
			}
		}

		addCalendarsToInclude(calendars);
		removedCalendarsToExclude(calendars);

		return calendars;
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getPositionSecuritySelection(), "You must specify which security to extract from client positions.");
		ValidationUtils.assertNotEmpty(getSecurityCalendarSelection(), "You must specify which calendar to extract from calculated securities.");
		if (getSecurityCalendarSelection().contains(SecurityCalendarOptions.COALESCE) && getSecurityCalendarSelection().size() != 1) {
			throw new ValidationException("If Coalesce option is selected for Security Calendar Selection, it must be the only option.");
		}
		if (!ObjectUtils.isNotNullPresent(getSecurityGroupId(), getClientAccountGroupId(), getClientAccountId()) && CollectionUtils.isEmpty(getCalendarIdsToInclude())) {
			throw new ValidationException("You must provide at least one of [Security Group, Client Account Group, Calendar List to Include, Client Account]");
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public void setSecurityGroupId(Short securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public Integer getClientAccountGroupId() {
		return this.clientAccountGroupId;
	}


	public void setClientAccountGroupId(Integer clientAccountGroupId) {
		this.clientAccountGroupId = clientAccountGroupId;
	}


	public List<PositionSecurityOptions> getPositionSecuritySelection() {
		return this.positionSecuritySelection;
	}


	public void setPositionSecuritySelection(List<PositionSecurityOptions> positionSecuritySelection) {
		this.positionSecuritySelection = positionSecuritySelection;
	}


	public List<SecurityCalendarOptions> getSecurityCalendarSelection() {
		return this.securityCalendarSelection;
	}


	public void setSecurityCalendarSelection(List<SecurityCalendarOptions> securityCalendarSelection) {
		this.securityCalendarSelection = securityCalendarSelection;
	}


	public boolean isExtractPositionClientAcctBaseCCY() {
		return this.extractPositionClientAcctBaseCCY;
	}


	public void setExtractPositionClientAcctBaseCCY(boolean extractPositionClientAcctBaseCCY) {
		this.extractPositionClientAcctBaseCCY = extractPositionClientAcctBaseCCY;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}
}
