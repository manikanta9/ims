package com.clifton.accounting.commission.calculation.calculators.impl;

import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionTier;
import com.clifton.accounting.commission.calculation.AccountingCommissionTierTypes;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionTypeCalculator;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionTypeCalculatorDecorator;
import com.clifton.core.util.validation.ValidationException;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * A {@link AccountingCommissionTypeCalculator} that will use the {@link AccountingCommissionTypeCalculatorSimple}
 * to determine if the commission amount on the {@link AccountingCommissionDefinition} is applicable.
 * If the defined value is defined by {@link AccountingCommissionTier}s,
 * the correct tier fee will be determined.
 * <p>
 * This implementation is thread safe during calculation and, therefore, caches instances for each {@link AccountingCommissionTierTypes} to avoid object creation.
 *
 * @author NickK
 */
public class AccountingCommissionTypeCalculatorTiered extends AccountingCommissionTypeCalculatorDecorator {

	private static final ConcurrentMap<AccountingCommissionTierTypes, AccountingCommissionTypeCalculatorTiered> commissionTierTypeToCalculators = new ConcurrentHashMap<>(AccountingCommissionTierTypes.values().length);

	private final AccountingCommissionTierTypes commissionTierType;


	private AccountingCommissionTypeCalculatorTiered(final AccountingCommissionTierTypes commissionTierType) {
		super(AccountingCommissionTypeCalculatorSimple.getInstance());
		this.commissionTierType = commissionTierType;
	}


	public static AccountingCommissionTypeCalculatorTiered getInstance(AccountingCommissionTierTypes commissionTierType) {
		AccountingCommissionTypeCalculatorTiered cachedInstance = commissionTierTypeToCalculators.get(commissionTierType);
		if (cachedInstance != null) {
			return cachedInstance;
		}
		cachedInstance = commissionTierTypeToCalculators.putIfAbsent(commissionTierType, new AccountingCommissionTypeCalculatorTiered(commissionTierType));
		return cachedInstance == null ? commissionTierTypeToCalculators.get(commissionTierType) : cachedInstance;
	}


	@Override
	public BigDecimal calculate(AccountingCommissionCalculationCommand commissionTypeCalculationCommand) {
		AccountingCommissionTier tier = this.commissionTierType.getCommissionTier(commissionTypeCalculationCommand);
		if (tier == null) {
			throw new ValidationException("Could not find an applicable " + this.commissionTierType.name() + " commission tier for the journal detail.");
		}
		return tier.getFeeAmount();
	}
}
