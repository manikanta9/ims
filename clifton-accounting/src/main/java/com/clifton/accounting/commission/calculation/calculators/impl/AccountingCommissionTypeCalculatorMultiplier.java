package com.clifton.accounting.commission.calculation.calculators.impl;

import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionTypeCalculator;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionTypeCalculatorDecorator;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * A {@link AccountingCommissionTypeCalculator} that will multiply the {@link BigDecimal} result
 * of a wrapped calculator by the provided multiplication factor.
 *
 * @author NickK
 */
public class AccountingCommissionTypeCalculatorMultiplier extends AccountingCommissionTypeCalculatorDecorator {

	final BigDecimal commissionMultiplier;


	public AccountingCommissionTypeCalculatorMultiplier(AccountingCommissionTypeCalculator calculatorToMultiply, BigDecimal multiplier) {
		super(calculatorToMultiply);
		this.commissionMultiplier = multiplier;
	}


	@Override
	public BigDecimal calculate(AccountingCommissionCalculationCommand commissionTypeCalculationCommand) {
		BigDecimal commission = this.baseCalculator.calculate(commissionTypeCalculationCommand);
		if (commission == null) {
			return commission;
		}
		return MathUtils.multiply(commission, this.commissionMultiplier);
	}
}
