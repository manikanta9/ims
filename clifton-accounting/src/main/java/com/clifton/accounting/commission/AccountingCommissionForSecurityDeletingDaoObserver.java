package com.clifton.accounting.commission;


import com.clifton.accounting.commission.search.AccountingCommissionDefinitionSearchForm;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>AccountingCommissionForSecurityDeletingDaoObserver</code> is an observer on {@link InvestmentSecurity} that deletes {@link AccountingCommissionDefinition} records
 * with a foreign key reference to the security when the security is being deleted.
 *
 * @author jgommels
 */
@Component
public class AccountingCommissionForSecurityDeletingDaoObserver extends SelfRegisteringDaoObserver<InvestmentSecurity> {

	private AccountingCommissionService accountingCommissionService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentSecurity> dao, DaoEventTypes event, InvestmentSecurity bean) {
		Integer securityId = bean.getId();
		//Sanity check
		if (event.isDelete() && securityId != null) {
			AccountingCommissionDefinitionSearchForm searchForm = new AccountingCommissionDefinitionSearchForm();
			searchForm.setInvestmentSecurityId(securityId);
			List<AccountingCommissionDefinition> commissions = getAccountingCommissionService().getAccountingCommissionDefinitionList(searchForm);
			for (AccountingCommissionDefinition commission : CollectionUtils.getIterable(commissions)) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(commission.getId());
			}
		}
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.DELETE;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingCommissionService getAccountingCommissionService() {
		return this.accountingCommissionService;
	}


	public void setAccountingCommissionService(AccountingCommissionService accountingCommissionService) {
		this.accountingCommissionService = accountingCommissionService;
	}
}
