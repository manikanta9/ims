package com.clifton.accounting.commission.calculation.calculators;

/**
 * The <code>AccountingCommissionTypeCalculatorDecorator</code> allows for wrapping {@link AccountingCommissionTypeCalculator}s
 * to add additional calculation logic to the commission calculation process.
 *
 * @author NickK
 */
public abstract class AccountingCommissionTypeCalculatorDecorator implements AccountingCommissionTypeCalculator {

	protected final AccountingCommissionTypeCalculator baseCalculator;


	public AccountingCommissionTypeCalculatorDecorator(AccountingCommissionTypeCalculator baseCalculator) {
		this.baseCalculator = baseCalculator;
	}
}
