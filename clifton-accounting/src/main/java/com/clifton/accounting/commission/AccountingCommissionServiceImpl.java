package com.clifton.accounting.commission;


import com.clifton.accounting.commission.search.AccountingCommissionDefinitionSearchForm;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingCommissionServiceImpl</code> class provides basic implementation of the AccountingCommissionService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingCommissionServiceImpl implements AccountingCommissionService {

	private AdvancedUpdatableDAO<AccountingCommissionDefinition, Criteria> accountingCommissionDefinitionDAO;
	private AdvancedUpdatableDAO<AccountingCommissionTier, Criteria> accountingCommissionTierDAO;

	private AccountingTransactionTypeService accountingTransactionTypeService;

	private DaoLocator daoLocator;

	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////
	////////        Accounting Commission Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingCommissionDefinition getAccountingCommissionDefinition(int id) {
		AccountingCommissionDefinition definition = getAccountingCommissionDefinitionDAO().findByPrimaryKey(id);
		populateAccountingCommissionDefinition(definition);
		return definition;
	}


	private void populateAccountingCommissionDefinition(AccountingCommissionDefinition definition) {
		if (definition != null) {
			if (definition.getCommissionType().hasRanges()) {
				//Get the list of tiers and sort them
				List<AccountingCommissionTier> tiers = getAccountingCommissionTierDAO().findByField("commissionDefinition.id", definition.getId());
				BeanUtils.sortWithFunctions(tiers, CollectionUtils.createList(AccountingCommissionTier::isMaxTier, AccountingCommissionTier::getThreshold), CollectionUtils.createList(true, true));
				definition.setCommissionTierList(tiers);
			}
			//Populate the name of the holder so the UI can display it on initial window open
			if (!MathUtils.isNullOrZero(definition.getAdditionalScopeFkFieldId()) && definition.getTransactionType() != null && definition.getTransactionType().getAdditionalScopeSystemTable() != null) {
				Integer additionalScopeId = definition.getAdditionalScopeFkFieldId();
				ReadOnlyDAO<?> dao = getDaoLocator().locate(definition.getTransactionType().getAdditionalScopeSystemTable().getName());
				IdentityObject object = dao.findByPrimaryKey(additionalScopeId);
				definition.setAdditionalScopeHolder(new AccountingCommissionDefinitionAdditionalScopeHolder(additionalScopeId, BeanUtils.getLabel(object)));
			}
			SystemBean bean = definition.getAdjustmentCalculatorBean();
			if (bean != null) {
				bean.setPropertyList(getSystemBeanService().getSystemBeanPropertyListByBeanId(bean.getId()));
			}
		}
	}


	@Override
	public List<AccountingCommissionDefinition> getAccountingCommissionDefinitionList(AccountingCommissionDefinitionSearchForm searchForm) {
		return getAccountingCommissionDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public void copyAccountingCommissionDefinitionListForSecurity(int fromSecurityId, InvestmentSecurity toSecurity) {
		ValidationUtils.assertNotNull(toSecurity, "To Security is required to copy commission definitions to.");

		AccountingCommissionDefinitionSearchForm searchForm = new AccountingCommissionDefinitionSearchForm();
		searchForm.setInvestmentSecurityId(fromSecurityId);

		List<AccountingCommissionDefinition> definitionList = getAccountingCommissionDefinitionList(searchForm);
		if (!CollectionUtils.isEmpty(definitionList)) {
			for (AccountingCommissionDefinition def : definitionList) {
				// Populate Tiers if necessary
				populateAccountingCommissionDefinition(def);

				AccountingCommissionDefinition newDef = BeanUtils.cloneBean(def, false, false);
				// Note: Instrument, Hierarchy, & Investment Type fields are already re-populated in save method based on the security
				newDef.setInvestmentSecurity(toSecurity);

				for (AccountingCommissionTier tier : CollectionUtils.getIterable(def.getCommissionTierList())) {
					AccountingCommissionTier newTier = BeanUtils.cloneBean(tier, false, false);
					newTier.setCommissionDefinition(newDef);
					newDef.addCommissionTier(newTier);
				}

				saveAccountingCommissionDefinition(newDef);
			}
		}
	}


	private void addRestrictionForMostSpecificField(AccountingCommissionDefinitionSearchForm searchForm, AccountingCommissionDefinition commission, String[] searchFormSpecificityArray,
	                                                String[] commissionSpecificityArray) {

		for (int i = 0; i < searchFormSpecificityArray.length; i++) {
			String searchFormField = searchFormSpecificityArray[i];
			String commissionField = commissionSpecificityArray[i];

			Object value = BeanUtils.getPropertyValue(commission, commissionField);
			if (value == null) {
				searchForm.addSearchRestriction(new SearchRestriction(searchFormField, ComparisonConditions.IS_NULL, null));
			}
			else {
				searchForm.addSearchRestriction(new SearchRestriction(searchFormField, ComparisonConditions.EQUALS, value));
				break;
			}
		}
	}


	/**
	 * @see com.clifton.accounting.commission.validation.AccountingCommissionValidator
	 */
	@Override
	@Transactional
	public AccountingCommissionDefinition saveAccountingCommissionDefinition(AccountingCommissionDefinition bean) {
		//Normalize Early Termination commissions to always be set to apply Only On Close
		if (bean.getTransactionType() != null && bean.getTransactionType().equals(getAccountingTransactionTypeService().getAccountingTransactionTypeByName(AccountingTransactionType.EARLY_TERMINATION))) {
			bean.setCommissionApplyMethod(AccountingCommissionApplyMethods.ONLY_ON_CLOSE);
		}

		/**
		 * If the scope holder object is set, take the id off of it and store it in the FKFieldID
		 */
		if (bean.getAdditionalScopeHolder() != null) {
			bean.setAdditionalScopeFkFieldId(bean.getAdditionalScopeHolder().getIdentifier());
			bean.setAdditionalScopeHolder(null);
		}

		/*
		 * NOTE: There is additional validation logic in AccountingCommissionValidator. The following validation logic only performs validation
		 * against existing commission definitions of the same level of "specificity" to ensure that there are no overlapping dates.
		 */
		AccountingCommissionDefinitionSearchForm searchForm = new AccountingCommissionDefinitionSearchForm();
		addRestrictionForMostSpecificField(searchForm, bean, AccountingCommissionDefinitionSearchForm.SECURITY_SPECIFICITY, AccountingCommissionDefinition.SECURITY_SPECIFICITY);
		addRestrictionForMostSpecificField(searchForm, bean, AccountingCommissionDefinitionSearchForm.HOLDING_SPECIFICITY, AccountingCommissionDefinition.HOLDING_SPECIFICITY);
		addRestrictionForMostSpecificField(searchForm, bean, AccountingCommissionDefinitionSearchForm.EXECUTING_SPECIFICITY, AccountingCommissionDefinition.EXECUTING_SPECIFICITY);
		addRestrictionForMostSpecificField(searchForm, bean, AccountingCommissionDefinitionSearchForm.EXCHANGE_SPECIFICITY, AccountingCommissionDefinition.EXCHANGE_SPECIFICITY);

		searchForm.setAccountingAccountId(bean.getExpenseAccountingAccount().getId());
		if (bean.getTransactionType() != null) {
			searchForm.setAccountingTransactionTypeId(bean.getTransactionType().getId());
		}

		/*
		 * If the apply method is 'Only on Open' or 'Only on Close', the only allowed setting for the new definition would be
		 * the opposite, so check for anything other than the opposite.
		 */
		if (bean.getCommissionApplyMethod() == AccountingCommissionApplyMethods.ONLY_ON_OPEN) {
			searchForm.setNotCommissionApplyMethod(AccountingCommissionApplyMethods.ONLY_ON_CLOSE);
		}
		else if (bean.getCommissionApplyMethod() == AccountingCommissionApplyMethods.ONLY_ON_CLOSE) {
			searchForm.setNotCommissionApplyMethod(AccountingCommissionApplyMethods.ONLY_ON_OPEN);
		}

		//Include the additional scope when searching for existing
		searchForm.setAdditionalScopeFkFieldId(bean.getAdditionalScopeFkFieldId());

		List<AccountingCommissionDefinition> existingDefinitions = getAccountingCommissionDefinitionList(searchForm);
		for (AccountingCommissionDefinition existingDefinition : CollectionUtils.getIterable(existingDefinitions)) {
			if (!existingDefinition.equals(bean)) {
				/*
				 * If the 'matching' existing Commission Definition is never-ending, then automatically set its End Date to a day before the Start Date of the new Commission Definition./
				 *
				 * NOTE: This also must handle the case where an old Commission Definition is being updated. This shouldn't update the End Date of a newer Commission Definition. Therefore, we must also
				 * check that the End Date of the Commission Definition being saved is null OR its End Date is after the Start Date of the existing definition, otherwise we don't update the existing definition.
				 */
				if (existingDefinition.getEndDate() == null && bean.getStartDate() != null
						&& (bean.getEndDate() == null || DateUtils.compare(bean.getEndDate(), existingDefinition.getStartDate(), false) > 0)) {
					Date newEndDate = DateUtils.addDays(bean.getStartDate(), -1);
					if (DateUtils.isOverlapInDates(existingDefinition.getStartDate(), newEndDate, bean.getStartDate(), bean.getEndDate())) {
						throw new ValidationException("Start and end dates cannot overlap with this existing endless Commission Definition: " + existingDefinition);
					}
					// end the previous endless rating
					existingDefinition.setEndDate(newEndDate);
					populateAccountingCommissionDefinition(existingDefinition);
					getAccountingCommissionDefinitionDAO().save(existingDefinition);
				}
				else if (DateUtils.isOverlapInDates(existingDefinition.getStartDate(), existingDefinition.getEndDate(), bean.getStartDate(), bean.getEndDate())) {
					throw new ValidationException("Start and end dates cannot overlap with this existing Commission Definition: " + existingDefinition);
				}
			}
		}

		//populate derived fields on save
		if (bean.getInvestmentSecurity() != null) {
			bean.setInvestmentInstrument(bean.getInvestmentSecurity().getInstrument());
		}
		if (bean.getInvestmentInstrument() != null) {
			bean.setInstrumentHierarchy(bean.getInvestmentInstrument().getHierarchy());
		}
		if (bean.getInstrumentHierarchy() != null) {
			bean.setInvestmentType(bean.getInstrumentHierarchy().getInvestmentType());
		}

		List<AccountingCommissionTier> originalTierList = null;
		if (!bean.isNewBean()) {
			originalTierList = getAccountingCommissionTierDAO().findByField("commissionDefinition.id", bean.getId());
		}

		if (bean.getCommissionType().hasRanges()) {
			for (AccountingCommissionTier tier : CollectionUtils.getIterable(bean.getCommissionTierList())) {
				tier.setCommissionDefinition(bean);
			}
		}

		AccountingCommissionDefinition saved = getAccountingCommissionDefinitionDAO().save(bean);
		getAccountingCommissionTierDAO().saveList(bean.getCommissionTierList(), originalTierList);
		saved.setCommissionTierList(bean.getCommissionTierList());
		return saved;
	}


	@Override
	@Transactional
	public void deleteAccountingCommissionDefinition(int id) {
		//Delete all AccountingCommissionTier records associated with this AccountingCommissionDefinition
		List<AccountingCommissionTier> tiers = getAccountingCommissionTierDAO().findByField("commissionDefinition.id", id);
		getAccountingCommissionTierDAO().deleteList(tiers);

		getAccountingCommissionDefinitionDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingCommissionDefinition, Criteria> getAccountingCommissionDefinitionDAO() {
		return this.accountingCommissionDefinitionDAO;
	}


	public void setAccountingCommissionDefinitionDAO(AdvancedUpdatableDAO<AccountingCommissionDefinition, Criteria> accountingCommissionDefinitionDAO) {
		this.accountingCommissionDefinitionDAO = accountingCommissionDefinitionDAO;
	}


	public AdvancedUpdatableDAO<AccountingCommissionTier, Criteria> getAccountingCommissionTierDAO() {
		return this.accountingCommissionTierDAO;
	}


	public void setAccountingCommissionTierDAO(AdvancedUpdatableDAO<AccountingCommissionTier, Criteria> accountingCommissionTierDAO) {
		this.accountingCommissionTierDAO = accountingCommissionTierDAO;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public AccountingTransactionTypeService getAccountingTransactionTypeService() {
		return this.accountingTransactionTypeService;
	}


	public void setAccountingTransactionTypeService(AccountingTransactionTypeService accountingTransactionTypeService) {
		this.accountingTransactionTypeService = accountingTransactionTypeService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
