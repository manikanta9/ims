package com.clifton.accounting.commission.search;


import com.clifton.accounting.commission.AccountingCommissionApplyMethods;
import com.clifton.accounting.commission.AccountingCommissionTypes;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationMethods;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


public class AccountingCommissionDefinitionSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	public static final String[] SECURITY_SPECIFICITY = {"investmentSecurityId", "investmentInstrumentId", "instrumentHierarchyId", "investmentTypeSubTypeId", "investmentTypeId"};
	public static final String[] HOLDING_SPECIFICITY = {"holdingAccountId", "holdingCompanyId"};
	public static final String[] EXECUTING_SPECIFICITY = {"executingCompanyId"};
	public static final String[] EXCHANGE_SPECIFICITY = {"investmentExchangeId"};

	@SearchField(leftJoin = true, searchField = "holdingCompany.name,investmentType.name,investmentTypeSubType.name,instrumentHierarchy.name,investmentInstrument.name,investmentSecurity.name,expenseAccountingAccount.name")
	private String searchPattern;

	//"SPECIFICITY" FIELDS
	@SearchField(searchField = "investmentExchange.id")
	private Short investmentExchangeId;

	@SearchField(searchField = "investmentExchange.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentExchangeId;

	@SearchField(searchField = "holdingAccount.id")
	private Integer holdingAccountId;

	@SearchField(searchField = "holdingAccount.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullHoldingAccountId;

	@SearchField(searchField = "holdingCompany.id")
	private Integer holdingCompanyId;

	@SearchField(searchField = "holdingCompany.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullHoldingCompanyId;

	@SearchField(searchField = "executingCompany.id")
	private Integer executingCompanyId;

	@SearchField(searchField = "executingCompany.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullExecutingCompanyId;

	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentType.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentTypeId;

	@SearchField(searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "investmentTypeSubType.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentTypeSubTypeId;

	@SearchField(searchField = "instrumentHierarchy.id")
	private Short instrumentHierarchyId;

	@SearchField(searchField = "investmentExchange.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInstrumentHierarchyId;

	@SearchField(searchField = "investmentInstrument.id")
	private Integer investmentInstrumentId;

	@SearchField(searchField = "investmentInstrument.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentInstrumentId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField(searchField = "investmentSecurity.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentSecurityId;

	//NON-"SPECIFICITY" FIELDS
	@SearchField
	private Integer id;

	@SearchField(searchField = "commissionCurrency.id")
	private Integer commissionCurrencyId;

	@SearchField(searchField = "expenseAccountingAccount.id")
	private Short accountingAccountId;

	@SearchField(searchFieldPath = "expenseAccountingAccount", searchField = "name", comparisonConditions = {ComparisonConditions.EQUALS})
	private String accountingAccountName;

	@SearchField(searchField = "name", searchFieldPath = "transactionType")
	private String accountingTransactionTypeName;

	@SearchField(searchField = "transactionType.id")
	private Short accountingTransactionTypeId;

	@SearchField(searchField = "transactionType.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] accountingTransactionTypeIdList;

	@SearchField(searchField = "transactionType.additionalScopeSystemTable.id")
	private Short additionalScopeSystemTableId;

	@SearchField(searchField = "additionalScopeFkFieldId")
	private Integer additionalScopeFkFieldId;

	@SearchField
	private AccountingCommissionTypes commissionType;

	@SearchField(searchField = "commissionType", comparisonConditions = ComparisonConditions.IN)
	private AccountingCommissionTypes[] commissionTypeList;

	@SearchField
	private AccountingCommissionApplyMethods commissionApplyMethod;

	@SearchField(searchField = "commissionApplyMethod", comparisonConditions = ComparisonConditions.IN)
	private AccountingCommissionApplyMethods[] commissionApplyMethodList;

	@SearchField(searchField = "commissionApplyMethod", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private AccountingCommissionApplyMethods notCommissionApplyMethod;

	@SearchField
	private AccountingCommissionCalculationMethods commissionCalculationMethod;

	@SearchField(searchField = "commissionCalculationMethod", comparisonConditions = ComparisonConditions.IN)
	private AccountingCommissionCalculationMethods[] commissionCalculationMethodList;

	@SearchField
	private BigDecimal commissionAmount;

	@SearchField
	private BigDecimal commissionMinimum;

	@SearchField
	private AccountingCommissionCalculationMethods minimumCalculationMethod;

	@SearchField
	private BigDecimal commissionMaximum;

	@SearchField
	private AccountingCommissionCalculationMethods maximumCalculationMethod;

	@SearchField(searchFieldPath = "adjustmentCalculatorBean.type", searchField = "name")
	private String adjustmentCalculatorBeanName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getInvestmentExchangeId() {
		return this.investmentExchangeId;
	}


	public void setInvestmentExchangeId(Short investmentExchangeId) {
		this.investmentExchangeId = investmentExchangeId;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public Integer getExecutingCompanyId() {
		return this.executingCompanyId;
	}


	public void setExecutingCompanyId(Integer executingCompanyId) {
		this.executingCompanyId = executingCompanyId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getCommissionCurrencyId() {
		return this.commissionCurrencyId;
	}


	public void setCommissionCurrencyId(Integer commissionCurrencyId) {
		this.commissionCurrencyId = commissionCurrencyId;
	}


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public AccountingCommissionApplyMethods getCommissionApplyMethod() {
		return this.commissionApplyMethod;
	}


	public void setCommissionApplyMethod(AccountingCommissionApplyMethods commissionApplyMethod) {
		this.commissionApplyMethod = commissionApplyMethod;
	}


	public AccountingCommissionApplyMethods getNotCommissionApplyMethod() {
		return this.notCommissionApplyMethod;
	}


	public void setNotCommissionApplyMethod(AccountingCommissionApplyMethods notCommissionApplyMethod) {
		this.notCommissionApplyMethod = notCommissionApplyMethod;
	}


	public String getAccountingTransactionTypeName() {
		return this.accountingTransactionTypeName;
	}


	public void setAccountingTransactionTypeName(String accountingTransactionTypeName) {
		this.accountingTransactionTypeName = accountingTransactionTypeName;
	}


	public Short getAccountingTransactionTypeId() {
		return this.accountingTransactionTypeId;
	}


	public void setAccountingTransactionTypeId(Short accountingTransactionTypeId) {
		this.accountingTransactionTypeId = accountingTransactionTypeId;
	}


	public Short[] getAccountingTransactionTypeIdList() {
		return this.accountingTransactionTypeIdList;
	}


	public void setAccountingTransactionTypeIdList(Short[] accountingTransactionTypeIdList) {
		this.accountingTransactionTypeIdList = accountingTransactionTypeIdList;
	}


	public String getAccountingAccountName() {
		return this.accountingAccountName;
	}


	public void setAccountingAccountName(String accountingAccountName) {
		this.accountingAccountName = accountingAccountName;
	}


	public AccountingCommissionTypes getCommissionType() {
		return this.commissionType;
	}


	public void setCommissionType(AccountingCommissionTypes commissionType) {
		this.commissionType = commissionType;
	}


	public AccountingCommissionTypes[] getCommissionTypeList() {
		return this.commissionTypeList;
	}


	public void setCommissionTypeList(AccountingCommissionTypes[] commissionTypeList) {
		this.commissionTypeList = commissionTypeList;
	}


	public AccountingCommissionApplyMethods[] getCommissionApplyMethodList() {
		return this.commissionApplyMethodList;
	}


	public void setCommissionApplyMethodList(AccountingCommissionApplyMethods[] commissionApplyMethodList) {
		this.commissionApplyMethodList = commissionApplyMethodList;
	}


	public AccountingCommissionCalculationMethods getCommissionCalculationMethod() {
		return this.commissionCalculationMethod;
	}


	public void setCommissionCalculationMethod(AccountingCommissionCalculationMethods commissionCalculationMethod) {
		this.commissionCalculationMethod = commissionCalculationMethod;
	}


	public AccountingCommissionCalculationMethods[] getCommissionCalculationMethodList() {
		return this.commissionCalculationMethodList;
	}


	public void setCommissionCalculationMethodList(AccountingCommissionCalculationMethods[] commissionCalculationMethodList) {
		this.commissionCalculationMethodList = commissionCalculationMethodList;
	}


	public Boolean getNullInvestmentExchangeId() {
		return this.nullInvestmentExchangeId;
	}


	public void setNullInvestmentExchangeId(Boolean nullInvestmentExchangeId) {
		this.nullInvestmentExchangeId = nullInvestmentExchangeId;
	}


	public Boolean getNullHoldingAccountId() {
		return this.nullHoldingAccountId;
	}


	public void setNullHoldingAccountId(Boolean nullHoldingAccountId) {
		this.nullHoldingAccountId = nullHoldingAccountId;
	}


	public Boolean getNullHoldingCompanyId() {
		return this.nullHoldingCompanyId;
	}


	public void setNullHoldingCompanyId(Boolean nullHoldingCompanyId) {
		this.nullHoldingCompanyId = nullHoldingCompanyId;
	}


	public Boolean getNullExecutingCompanyId() {
		return this.nullExecutingCompanyId;
	}


	public void setNullExecutingCompanyId(Boolean nullExecutingCompanyId) {
		this.nullExecutingCompanyId = nullExecutingCompanyId;
	}


	public Boolean getNullInvestmentTypeId() {
		return this.nullInvestmentTypeId;
	}


	public void setNullInvestmentTypeId(Boolean nullInvestmentTypeId) {
		this.nullInvestmentTypeId = nullInvestmentTypeId;
	}


	public Boolean getNullInvestmentTypeSubTypeId() {
		return this.nullInvestmentTypeSubTypeId;
	}


	public void setNullInvestmentTypeSubTypeId(Boolean nullInvestmentTypeSubTypeId) {
		this.nullInvestmentTypeSubTypeId = nullInvestmentTypeSubTypeId;
	}


	public Boolean getNullInstrumentHierarchyId() {
		return this.nullInstrumentHierarchyId;
	}


	public void setNullInstrumentHierarchyId(Boolean nullInstrumentHierarchyId) {
		this.nullInstrumentHierarchyId = nullInstrumentHierarchyId;
	}


	public Boolean getNullInvestmentInstrumentId() {
		return this.nullInvestmentInstrumentId;
	}


	public void setNullInvestmentInstrumentId(Boolean nullInvestmentInstrumentId) {
		this.nullInvestmentInstrumentId = nullInvestmentInstrumentId;
	}


	public Boolean getNullInvestmentSecurityId() {
		return this.nullInvestmentSecurityId;
	}


	public void setNullInvestmentSecurityId(Boolean nullInvestmentSecurityId) {
		this.nullInvestmentSecurityId = nullInvestmentSecurityId;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}


	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	public BigDecimal getCommissionMinimum() {
		return this.commissionMinimum;
	}


	public void setCommissionMinimum(BigDecimal commissionMinimum) {
		this.commissionMinimum = commissionMinimum;
	}


	public AccountingCommissionCalculationMethods getMinimumCalculationMethod() {
		return this.minimumCalculationMethod;
	}


	public void setMinimumCalculationMethod(AccountingCommissionCalculationMethods minimumCalculationMethod) {
		this.minimumCalculationMethod = minimumCalculationMethod;
	}


	public BigDecimal getCommissionMaximum() {
		return this.commissionMaximum;
	}


	public void setCommissionMaximum(BigDecimal commissionMaximum) {
		this.commissionMaximum = commissionMaximum;
	}


	public AccountingCommissionCalculationMethods getMaximumCalculationMethod() {
		return this.maximumCalculationMethod;
	}


	public void setMaximumCalculationMethod(AccountingCommissionCalculationMethods maximumCalculationMethod) {
		this.maximumCalculationMethod = maximumCalculationMethod;
	}


	public String getAdjustmentCalculatorBeanName() {
		return this.adjustmentCalculatorBeanName;
	}


	public void setAdjustmentCalculatorBeanName(String adjustmentCalculatorBeanName) {
		this.adjustmentCalculatorBeanName = adjustmentCalculatorBeanName;
	}


	public Short getAdditionalScopeSystemTableId() {
		return this.additionalScopeSystemTableId;
	}


	public void setAdditionalScopeSystemTableId(Short additionalScopeSystemTableId) {
		this.additionalScopeSystemTableId = additionalScopeSystemTableId;
	}


	public Integer getAdditionalScopeFkFieldId() {
		return this.additionalScopeFkFieldId;
	}


	public void setAdditionalScopeFkFieldId(Integer additionalScopeFkFieldId) {
		this.additionalScopeFkFieldId = additionalScopeFkFieldId;
	}
}
