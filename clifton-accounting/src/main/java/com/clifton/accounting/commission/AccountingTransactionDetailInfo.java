package com.clifton.accounting.commission;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;

import java.math.BigDecimal;


/**
 * The <code>AccountingTransactionDetailInfo</code> represents a single detail of a transaction.
 *
 * @author jgommels
 */
public class AccountingTransactionDetailInfo {

	private final AccountingJournalDetailDefinition journalDetail;
	private final BookableEntity bookableEntity;
	private final AccountingTransactionType transactionType;


	public AccountingTransactionDetailInfo(AccountingJournalDetailDefinition journalDetail, BookableEntity bookableEntity, AccountingTransactionType transactionType) {
		this.journalDetail = journalDetail;
		this.bookableEntity = bookableEntity;
		this.transactionType = transactionType;
	}


	public AccountingJournalDetailDefinition getJournalDetail() {
		return this.journalDetail;
	}


	public BookableEntity getBookableEntity() {
		return this.bookableEntity;
	}


	/**
	 * @return the {@link AccountingTransactionType} associated with this transaction detail. May be <code>null</code> if no transaction type could be determined.
	 */
	public AccountingTransactionType getTransactionType() {
		return this.transactionType;
	}


	/**
	 * @return whether the associated transaction type is a transfer. If the transaction type is <code>null</code> then <code>false</code> is returned.
	 */
	public boolean isTransfer() {
		return this.transactionType != null && this.transactionType.isTransfer();
	}


	public boolean isSell() {
		BigDecimal quantity = this.journalDetail.getQuantityNormalized();
		return quantity != null && quantity.compareTo(BigDecimal.ZERO) < 0;
	}
}
