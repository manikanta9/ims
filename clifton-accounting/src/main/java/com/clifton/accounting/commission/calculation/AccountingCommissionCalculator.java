package com.clifton.accounting.commission.calculation;


import com.clifton.accounting.commission.AccountingCommission;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.gl.booking.BookableEntity;

import java.util.List;


/**
 * The <code>AccountingCommissionCalculationService</code> interface defines methods for retrieving the calculated commissions applicable
 * to transactions.
 */
public interface AccountingCommissionCalculator {

	/**
	 * Calculates the commission amounts for the given journal detail and returns the list of commissions for each GL account.
	 */
	public <T extends BookableEntity> List<AccountingCommission> getAccountingCommissionListForJournalDetail(AccountingCommissionCalculationProperties<T> commissionCalculationProperties);


	/**
	 * Get a list of Commission Definitions that correspond to the supplied Commission Calculation Properties
	 */
	public <T extends BookableEntity> List<AccountingCommissionDefinition> getMatchingAccountingCommissionDefinitionList(AccountingCommissionCalculationProperties<T> commissionCalculationProperties);
}
