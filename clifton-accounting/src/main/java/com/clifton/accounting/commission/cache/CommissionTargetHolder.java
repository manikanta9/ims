package com.clifton.accounting.commission.cache;


import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.core.cache.specificity.SimpleSpecificityScope;
import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.SpecificityTargetHolder;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>CommissionTargetHolder</code> contains the meta data for a {@link AccountingCommissionDefinition}
 * and is used in the {@link AccountingCommissionDefinitionSpecificityCache}.
 *
 * @author jgommels
 */
class CommissionTargetHolder implements SpecificityTargetHolder {

	private final Integer id;
	private final short accountingAccountId;
	private final Date startDate;
	private final Date endDate;

	private final String key;
	private final SpecificityScope scope;

	private final AccountingCommissionDefinition commissionDefinition;


	public CommissionTargetHolder(String key, AccountingCommissionDefinition commission, boolean storeDefinition) {
		this.id = commission.getId();
		this.accountingAccountId = commission.getExpenseAccountingAccount().getId();
		this.startDate = DateUtils.clearTime(commission.getStartDate());
		this.endDate = DateUtils.clearTime(commission.getEndDate());

		this.key = key;

		this.scope = new SimpleSpecificityScope(this.accountingAccountId);

		if (storeDefinition) {
			this.commissionDefinition = commission;
		}
		else {
			this.commissionDefinition = null;
		}
	}


	/**
	 * @return the ID of the associated {@link AccountingCommissionDefinition}.
	 * <p/>
	 * <b>Note:</b> May return <code>null</code> if the commission definition is "in-memory"
	 * and doesn't have an actual ID. If this is the case, {@link #getCommissionDefinition()} should be used instead.
	 */
	public Integer getId() {
		return this.id;
	}


	public short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	@Override
	public String getKey() {
		return this.key;
	}


	@Override
	public SpecificityScope getScope() {
		return this.scope;
	}


	/**
	 * @return the underlying {@link AccountingCommissionDefinition} that this holder is referencing. Note that this will return <code>null</code> if the commission definition
	 * is not stored by the holder. If this is the case, then the commission definition must be retrieved by its ID using {@link #getId()}.
	 */
	public AccountingCommissionDefinition getCommissionDefinition() {
		return this.commissionDefinition;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.commissionDefinition == null) ? 0 : this.commissionDefinition.hashCode());
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CommissionTargetHolder other = (CommissionTargetHolder) obj;
		if (this.commissionDefinition == null) {
			if (other.commissionDefinition != null) {
				return false;
			}
		}
		else if (!this.commissionDefinition.equals(other.commissionDefinition)) {
			return false;
		}
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!this.id.equals(other.id)) {
			return false;
		}
		if (this.key == null) {
			if (other.key != null) {
				return false;
			}
		}
		else if (!this.key.equals(other.key)) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		return "CommissionTargetHolder [id=" + this.id + ", commissionAccountId=" + this.accountingAccountId + ", startDate=" + this.startDate + ", endDate=" + this.endDate + ", key=" + this.key
				+ ", scope=" + this.scope + "]";
	}
}
