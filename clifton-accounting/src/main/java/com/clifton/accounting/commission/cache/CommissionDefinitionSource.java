package com.clifton.accounting.commission.cache;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * The <code>CommissionDefinitionSource</code> is used with the {@link AccountingCommissionDefinitionSpecificityCache}
 * for retrieving relevant commissions.
 *
 * @author jgommels
 */
public class CommissionDefinitionSource {

	private final AccountingTransactionType transactionType;
	private final BusinessCompany clearingBroker;
	private final BusinessCompany executingBroker;
	private final InvestmentAccount holdingAccount;
	private final InvestmentSecurity security;
	private final Date commissionDate;
	private AccountingAccount accountingAccount;
	private Integer additionalSourceFkFieldId;


	public CommissionDefinitionSource(AccountingTransactionType transactionType, BusinessCompany clearingBroker, BusinessCompany executingBroker, InvestmentAccount holdingAccount, InvestmentSecurity security, Date commissionDate, AccountingAccount accountingAccount, Integer additionalScopeFkFieldId) {
		this.transactionType = transactionType;
		this.clearingBroker = clearingBroker;
		this.executingBroker = executingBroker;
		this.holdingAccount = holdingAccount;
		this.security = security;
		this.commissionDate = commissionDate;
		this.accountingAccount = accountingAccount;
		this.additionalSourceFkFieldId = additionalScopeFkFieldId;
	}


	public AccountingTransactionType getTransactionType() {
		return this.transactionType;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public BusinessCompany getClearingBroker() {
		return this.clearingBroker;
	}


	public BusinessCompany getExecutingBroker() {
		return this.executingBroker;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.security;
	}


	public Date getCommissionDate() {
		return this.commissionDate;
	}


	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	public Integer getAdditionalSourceFkFieldId() {
		return this.additionalSourceFkFieldId;
	}
}
