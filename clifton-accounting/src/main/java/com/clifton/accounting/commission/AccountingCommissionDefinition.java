package com.clifton.accounting.commission;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationMethods;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.system.bean.SystemBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingCommissionDefinition</code> class defines when and what commission/fee/etc. to apply.
 * Commissions and fees can be applied on position open, on position close or on both events.
 * Each AccountingCommissionDefinition object represents a single commission or fee that is applied.
 * Commissions and fees can also be defined to be applied per unit: Total Commission = Units * CommissionAmount
 * or as total value: Total Commission = CommissionAmount.
 * <p/>
 * Commissions and fees are retrieved for: Security, HoldingAccount, IsOpen fields.
 * More specific fees (have holdingAccount set) override less specific fees (no holding account).
 * Each most specific unique combination of expenseAccountingAccount and appliedPerUnit is applied.
 *
 * @author vgomelsky
 */
public class AccountingCommissionDefinition extends BaseEntity<Integer> implements LabeledObject {

	public static final String[] SECURITY_SPECIFICITY = {"investmentSecurity.id", "investmentInstrument.id", "instrumentHierarchy.id", "investmentTypeSubType.id", "investmentType.id"};
	public static final String[] HOLDING_SPECIFICITY = {"holdingAccount.id", "holdingCompany.id"};
	public static final String[] EXECUTING_SPECIFICITY = {"executingCompany.id"};
	public static final String[] EXCHANGE_SPECIFICITY = {"investmentExchange.id"};

	private AccountingTransactionType transactionType;

	private AccountingCommissionTypes commissionType;

	private InvestmentExchange investmentExchange;

	/**
	 * Limits commission application to a specific holding company (bank or broker).
	 * If holdingAccount is selected, then the company must not be defined (account has the company)
	 */
	private BusinessCompany holdingCompany;
	/**
	 * Limits commission application to a specific holding investment account.
	 * If holdingAccount is selected, then the company must not be defined (account has the company).
	 * If neither holding company nor account is selected, the commission applies to all securities of
	 * the specified type or hierarchy.
	 */
	private InvestmentAccount holdingAccount;
	// Limits commission application to a specific executing broker/company (bank or broker).
	private BusinessCompany executingCompany;

	private InvestmentType investmentType;
	private InvestmentTypeSubType investmentTypeSubType;
	private InvestmentInstrumentHierarchy instrumentHierarchy;
	private InvestmentInstrument investmentInstrument;
	private InvestmentSecurity investmentSecurity;

	/**
	 * The currency that the commission is in. If null, this indicates that the currency denomination of the security should be used.
	 */
	private InvestmentSecurity commissionCurrency;

	/**
	 * "Commission Expense", "NFA Fees", "SEC Fees"
	 */
	private AccountingAccount expenseAccountingAccount;

	/**
	 * How this commission is calculated for the end rate.
	 */
	private AccountingCommissionCalculationMethods commissionCalculationMethod;

	private AccountingCommissionApplyMethods commissionApplyMethod;

	/**
	 * Whether a non-internal transfer should be treated as half-turn, meaning that the source account will
	 * be charged on close and the destination account is only charged on close, but not for the open. This
	 * field is intended to be used for securities which have round-turn commissions or fees associated with
	 * a trade, thus this field is only evaluated if this commission definition is of type {@link AccountingTransactionType#TRADE}
	 * and the apply method is of type {@link AccountingCommissionApplyMethods#ROUND_TURN}.
	 */
	private boolean halfTurnOnTransfer;

	private BigDecimal commissionAmount;
	private BigDecimal commissionMinimum;
	private AccountingCommissionCalculationMethods minimumCalculationMethod;
	private BigDecimal commissionMaximum;
	private AccountingCommissionCalculationMethods maximumCalculationMethod;

	private Date startDate;
	private Date endDate;

	private List<AccountingCommissionTier> commissionTierList;

	/**
	 * A bean that will perform some additional calculation and 'adjust' the original calculated commission.
	 */
	private SystemBean adjustmentCalculatorBean;

	/**
	 * The ID of the additional scope object
	 */
	private Integer additionalScopeFkFieldId;

	/**
	 * A non-stored object to present the name and ID of the additionalScope object to the UI
	 */
	@NonPersistentField
	private AccountingCommissionDefinitionAdditionalScopeHolder additionalScopeHolder;


	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder builder = new StringBuilder();
		if (this.getHoldingAccount() != null) {
			builder.append(this.getHoldingAccount().getName()).append(" Expense ");
		}
		else if (this.getHoldingCompany() != null) {
			builder.append(this.getHoldingCompany().getName()).append(" Expense ");
		}
		else {
			builder.append("Global Expense ");
		}
		builder.append("for ");

		if (this.getInvestmentSecurity() != null) {
			builder.append(this.getInvestmentSecurity().getName());
		}
		else if (this.getInvestmentInstrument() != null) {
			builder.append(this.getInvestmentInstrument().getName());
		}
		else if (this.getInvestmentTypeSubType() != null) {
			builder.append(this.getInvestmentTypeSubType().getName());
		}
		else if (this.getInstrumentHierarchy() != null) {
			builder.append(this.getInstrumentHierarchy().getName());
		}
		else if (this.getInvestmentType() != null) {
			builder.append(this.getInvestmentType().getName());
		}

		if (this.getStartDate() != null) {
			builder.append(" starting on ").append(this.getStartDate());
		}
		if (this.getEndDate() != null) {
			builder.append(" ending on ").append(this.getEndDate());
		}
		else {
			builder.append(" never ending.");
		}
		return builder.toString();
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	public void addCommissionTier(AccountingCommissionTier tier) {
		if (this.commissionTierList == null) {
			this.commissionTierList = new ArrayList<>();
		}
		this.commissionTierList.add(tier);
	}


	////////////////////////////////////////////////////////////////////////////


	public AccountingCommissionTypes getCommissionType() {
		return this.commissionType;
	}


	public void setCommissionType(AccountingCommissionTypes commissionType) {
		this.commissionType = commissionType;
	}


	public InvestmentExchange getInvestmentExchange() {
		return this.investmentExchange;
	}


	public void setInvestmentExchange(InvestmentExchange investmentExchange) {
		this.investmentExchange = investmentExchange;
	}


	public BusinessCompany getHoldingCompany() {
		return this.holdingCompany;
	}


	public void setHoldingCompany(BusinessCompany holdingCompany) {
		this.holdingCompany = holdingCompany;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public BusinessCompany getExecutingCompany() {
		return this.executingCompany;
	}


	public void setExecutingCompany(BusinessCompany executingCompany) {
		this.executingCompany = executingCompany;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentInstrumentHierarchy getInstrumentHierarchy() {
		return this.instrumentHierarchy;
	}


	public void setInstrumentHierarchy(InvestmentInstrumentHierarchy instrumentHierarchy) {
		this.instrumentHierarchy = instrumentHierarchy;
	}


	public InvestmentInstrument getInvestmentInstrument() {
		return this.investmentInstrument;
	}


	public void setInvestmentInstrument(InvestmentInstrument instrument) {
		this.investmentInstrument = instrument;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public InvestmentSecurity getCommissionCurrency() {
		return this.commissionCurrency;
	}


	public void setCommissionCurrency(InvestmentSecurity commissionCurrency) {
		this.commissionCurrency = commissionCurrency;
	}


	public AccountingAccount getExpenseAccountingAccount() {
		return this.expenseAccountingAccount;
	}


	public void setExpenseAccountingAccount(AccountingAccount expenseAccountingAccount) {
		this.expenseAccountingAccount = expenseAccountingAccount;
	}


	public AccountingCommissionCalculationMethods getCommissionCalculationMethod() {
		return this.commissionCalculationMethod;
	}


	public void setCommissionCalculationMethod(AccountingCommissionCalculationMethods commissionCalculationMethod) {
		this.commissionCalculationMethod = commissionCalculationMethod;
	}


	public AccountingCommissionApplyMethods getCommissionApplyMethod() {
		return this.commissionApplyMethod;
	}


	public void setCommissionApplyMethod(AccountingCommissionApplyMethods commissionApplyMethod) {
		this.commissionApplyMethod = commissionApplyMethod;
	}


	/**
	 * {@link AccountingCommissionDefinition#halfTurnOnTransfer}
	 */
	public boolean isHalfTurnOnTransfer() {
		return this.halfTurnOnTransfer;
	}


	/**
	 * {@link AccountingCommissionDefinition#halfTurnOnTransfer}
	 */
	public void setHalfTurnOnTransfer(boolean halfTurnOnTransfer) {
		this.halfTurnOnTransfer = halfTurnOnTransfer;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public List<AccountingCommissionTier> getCommissionTierList() {
		return this.commissionTierList;
	}


	public void setCommissionTierList(List<AccountingCommissionTier> commissionTierList) {
		this.commissionTierList = commissionTierList;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}


	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	public BigDecimal getCommissionMinimum() {
		return this.commissionMinimum;
	}


	public void setCommissionMinimum(BigDecimal commissionMinimum) {
		this.commissionMinimum = commissionMinimum;
	}


	public AccountingCommissionCalculationMethods getMinimumCalculationMethod() {
		return this.minimumCalculationMethod;
	}


	public void setMinimumCalculationMethod(AccountingCommissionCalculationMethods minimumCalculationMethod) {
		this.minimumCalculationMethod = minimumCalculationMethod;
	}


	public BigDecimal getCommissionMaximum() {
		return this.commissionMaximum;
	}


	public void setCommissionMaximum(BigDecimal commissionMaximum) {
		this.commissionMaximum = commissionMaximum;
	}


	public AccountingCommissionCalculationMethods getMaximumCalculationMethod() {
		return this.maximumCalculationMethod;
	}


	public void setMaximumCalculationMethod(AccountingCommissionCalculationMethods maximumCalculationMethod) {
		this.maximumCalculationMethod = maximumCalculationMethod;
	}


	public SystemBean getAdjustmentCalculatorBean() {
		return this.adjustmentCalculatorBean;
	}


	public void setAdjustmentCalculatorBean(SystemBean adjustmentCalculatorBean) {
		this.adjustmentCalculatorBean = adjustmentCalculatorBean;
	}


	public AccountingTransactionType getTransactionType() {
		return this.transactionType;
	}


	public void setTransactionType(AccountingTransactionType transactionType) {
		this.transactionType = transactionType;
	}


	public Integer getAdditionalScopeFkFieldId() {
		return this.additionalScopeFkFieldId;
	}


	public void setAdditionalScopeFkFieldId(Integer additionalScopeFkFieldId) {
		this.additionalScopeFkFieldId = additionalScopeFkFieldId;
	}


	public AccountingCommissionDefinitionAdditionalScopeHolder getAdditionalScopeHolder() {
		return this.additionalScopeHolder;
	}


	public void setAdditionalScopeHolder(AccountingCommissionDefinitionAdditionalScopeHolder additionalScopeHolder) {
		this.additionalScopeHolder = additionalScopeHolder;
	}


	@Override
	public String toString() {
		return "AccountingCommissionDefinition [ID=" + this.getId() + ", Label=" + this.getLabel() + "]";
	}
}
