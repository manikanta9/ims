package com.clifton.accounting.commission.cache;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.commission.AccountingCommissionApplyMethods;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionService;
import com.clifton.accounting.commission.AccountingTransactionDetailInfo;
import com.clifton.accounting.commission.search.AccountingCommissionDefinitionSearchForm;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.DynamicSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.key.ScoredOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemTable;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * The <code>AccountingCommissionDefinitionSpecificityCacheImpl</code> is an implementation of {@link AbstractSpecificityCache} that
 * caches {@link AccountingCommissionDefinition}s by their "specificity". Given a {@link AccountingTransactionDetailInfo}, the most
 * specific commissions definitions that apply are returned.
 *
 * @author jgommels
 */
@Component
public class AccountingCommissionDefinitionSpecificityCacheImpl extends AbstractSpecificityCache<CommissionDefinitionSource, AccountingCommissionDefinition, CommissionTargetHolder> implements AccountingCommissionDefinitionSpecificityCache {

	private final CommissionCacheUpdater cacheUpdater;
	private AccountingCommissionService accountingCommissionService;
	private AccountingPositionTransferService accountingPositionTransferService;

	private AccountingAccountService accountingAccountService;
	private AccountingTransactionTypeService accountingTransactionTypeService;


	AccountingCommissionDefinitionSpecificityCacheImpl() {
		super(SpecificityCacheTypes.MATCHING_RESULT, new ScoredOrderingSpecificityKeyGenerator());
		this.cacheUpdater = new CommissionCacheUpdater();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of the most specific matching {@link AccountingCommissionDefinition}s for the clearing broker.
	 *
	 * @param transactionInfo the information related to the transaction detail
	 */
	@Override
	public AccountingMatchingCommissionDefinitionResultSet getMostSpecificCommissionDefinitions(AccountingTransactionDetailInfo transactionInfo) {
		AccountingJournalDetailDefinition journalDetail = transactionInfo.getJournalDetail();
		AccountingTransactionType transactionType = transactionInfo.getTransactionType();

		InvestmentAccount holdingAccount = journalDetail.getHoldingInvestmentAccount();
		InvestmentSecurity security = journalDetail.getInvestmentSecurity();
		Date commissionDate = journalDetail.getTransactionDate();
		BusinessCompany clearingBroker = InvestmentUtils.getClearingBroker(transactionInfo.getJournalDetail());
		BusinessCompany executingBroker = getExecutingBroker(transactionInfo);

		List<AccountingAccount> accountingAccounts = getAccountingAccountService().getAccountingAccountListByIds(AccountingAccountIdsCacheImpl.AccountingAccountIds.COMMISSION_ACCOUNTS);

		List<AccountingCommissionDefinition> commissions = new ArrayList<>();
		if (clearingBroker != null && transactionType != null && accountingAccounts != null) {
			Integer additionalScopeId = null;
			if (!StringUtils.isEmpty(transactionType.getAdditionalScopeBeanIdPath())) {
				Object additionalScopeObject = BeanUtils.getPropertyValue(transactionInfo.getBookableEntity(), transactionType.getAdditionalScopeBeanIdPath());
				if (additionalScopeObject != null) {
					Number additionalScopeNumber = ((Number) additionalScopeObject);
					additionalScopeId = additionalScopeNumber.intValue();
				}
			}

			commissions = getCommissionDefinitions(transactionType, clearingBroker, executingBroker, holdingAccount, security, commissionDate, accountingAccounts, additionalScopeId);

			/*
			 * Source accounts don't get charged for round-turn transfers; destination accounts do. Round-turn
			 * transfer fees are not common.
			 *
			 * If the transaction type is a transfer, then iterate through each commission definition. If a commission
			 * is of the same transaction type, is round turn, and the client account of the journal detail is equal to
			 * the source account of the transfer, then we remove the commission definition from the list. This is
			 * because a round-turn transfer fee should only be charged for the destination account, not the source account.
			 */
			if (transactionType.isTransfer()) {
				Iterator<AccountingCommissionDefinition> definitionIterator = commissions.iterator();
				while (definitionIterator.hasNext()) {
					AccountingCommissionDefinition commission = definitionIterator.next();
					if (commission.getTransactionType().equals(transactionType) && commission.getCommissionApplyMethod() == AccountingCommissionApplyMethods.ROUND_TURN) {
						AccountingPositionTransfer transfer = (AccountingPositionTransfer) transactionInfo.getBookableEntity();
						if (journalDetail.getClientInvestmentAccount().equals(transfer.getFromClientInvestmentAccount())) {
							definitionIterator.remove();
						}
					}
				}
			}
		}

		return new AccountingMatchingCommissionDefinitionResultSet(commissions, transactionInfo);
	}


	/**
	 * Gets the most specific Commission Definitions for each GL account
	 * <p>
	 * If no commissions are found for the given transaction type, the method will recurse using the parent type until Commission Definitions are found
	 */
	private List<AccountingCommissionDefinition> getCommissionDefinitions(AccountingTransactionType transactionType, BusinessCompany clearingBroker, BusinessCompany executingBroker, InvestmentAccount holdingAccount, InvestmentSecurity security, Date commissionDate, List<AccountingAccount> accountingAccounts, Integer additionalScopeFkFieldId) {
		List<AccountingCommissionDefinition> commissions = new ArrayList<>();
		for (AccountingAccount accountingAccount : accountingAccounts) {
			List<AccountingCommissionDefinition> glCommissions = getMostSpecificResultList(new CommissionDefinitionSource(transactionType, clearingBroker, executingBroker, holdingAccount, security, commissionDate, accountingAccount, additionalScopeFkFieldId));
			if (!CollectionUtils.isEmpty(glCommissions)) {
				commissions.addAll(glCommissions);
			}
		}
		//If no commissions are found for the given transactionType AND the transactionType has a parent, then call this method again with the parent type
		if (CollectionUtils.isEmpty(commissions) && transactionType.getParent() != null) {
			return getCommissionDefinitions(transactionType.getParent(), clearingBroker, executingBroker, holdingAccount, security, commissionDate, accountingAccounts, additionalScopeFkFieldId);
		}
		return commissions;
	}


	@Override
	protected SpecificityKeySource getKeySource(CommissionDefinitionSource source) {
		InvestmentSecurity security = source.getInvestmentSecurity();
		InvestmentInstrument instrument = security.getInstrument();
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		InvestmentType investmentType = hierarchy.getInvestmentType();

		Object[] securitySpecificity = {security.getId(), instrument.getId(), hierarchy.getId(), BeanUtils.getBeanIdentity(hierarchy.getInvestmentTypeSubType()), investmentType.getId(), null};
		Object[] holdingSpecificity = {BeanUtils.getBeanIdentity(source.getHoldingAccount()), BeanUtils.getBeanIdentity(source.getClearingBroker()), null};
		Object[] executingSpecificity = {BeanUtils.getBeanIdentity(source.getExecutingBroker()), null};
		Object[] exchangeSpecificity = {BeanUtils.getBeanIdentity(instrument.getExchange()), null};
		Object[] additionalScopeSpecificity = {getAdditionalScopeLink(source.getTransactionType().getAdditionalScopeSystemTable(), source.getAdditionalSourceFkFieldId()), null};
		Object[] valuesToAppend = {BeanUtils.getBeanIdentity(source.getTransactionType()), BeanUtils.getBeanIdentity(source.getAccountingAccount())};

		return buildKeySource(securitySpecificity, holdingSpecificity, executingSpecificity, exchangeSpecificity, additionalScopeSpecificity, valuesToAppend);
	}


	@Override
	protected AccountingCommissionDefinition getTarget(CommissionTargetHolder targetHolder) {
		//In some cases, the AccountingCommissionDefinition is set directly on the CommissionTargetHolder
		AccountingCommissionDefinition commission = targetHolder.getCommissionDefinition();

		//If the commission is not set, then we retrieve it using its ID
		if (commission == null) {
			commission = getAccountingCommissionService().getAccountingCommissionDefinition(targetHolder.getId());
		}

		return commission;
	}


	@Override
	protected boolean isMatch(CommissionDefinitionSource source, CommissionTargetHolder holder) {
		return DateUtils.isDateBetween(source.getCommissionDate(), holder.getStartDate(), holder.getEndDate(), false);
	}


	@Override
	protected Collection<CommissionTargetHolder> getTargetHolders() {
		List<CommissionTargetHolder> holders = new ArrayList<>();

		// Load all commission definitions. If we only include active for the current time, we run the risk of excluding a
		// recently expired commission definition when unposting/posting a transaction.
		List<AccountingCommissionDefinition> commissions = getAccountingCommissionService().getAccountingCommissionDefinitionList(new AccountingCommissionDefinitionSearchForm());
		for (AccountingCommissionDefinition commission : commissions) {
			holders.addAll(buildHolders(commission));
		}

		return holders;
	}


	@Override
	protected SpecificityCacheUpdater<CommissionTargetHolder> getCacheUpdater() {
		return this.cacheUpdater;
	}


	private String buildKeyForCommissionHolder(AccountingCommissionDefinition commission) {

		Object[] securitySpecificity = {BeanUtils.getBeanIdentity(commission.getInvestmentSecurity()), BeanUtils.getBeanIdentity(commission.getInvestmentInstrument()),
				BeanUtils.getBeanIdentity(commission.getInstrumentHierarchy()), BeanUtils.getBeanIdentity(commission.getInvestmentTypeSubType()),
				BeanUtils.getBeanIdentity(commission.getInvestmentType()), null};

		Object[] holdingSpecificity = {BeanUtils.getBeanIdentity(commission.getHoldingAccount()), BeanUtils.getBeanIdentity(commission.getHoldingCompany()), null};
		Object[] executingSpecificity = {BeanUtils.getBeanIdentity(commission.getExecutingCompany()), null};
		Object[] exchangeSpecificity = {BeanUtils.getBeanIdentity(commission.getInvestmentExchange()), null};
		Object[] additionalScopeSpecificity = {getAdditionalScopeLink(commission.getTransactionType().getAdditionalScopeSystemTable(), commission.getAdditionalScopeFkFieldId()), null};
		Object[] valuesToAppend = {BeanUtils.getBeanIdentity(commission.getTransactionType()), BeanUtils.getBeanIdentity(commission.getExpenseAccountingAccount())};

		return getKeyGenerator().generateKeyForTarget(buildKeySource(securitySpecificity, holdingSpecificity, executingSpecificity, exchangeSpecificity, additionalScopeSpecificity, valuesToAppend));
	}


	private String getAdditionalScopeLink(SystemTable additionalScopeTable, Integer additionalScopeFk) {
		Serializable tableId = BeanUtils.getBeanIdentity(additionalScopeTable);
		if (tableId != null && additionalScopeFk != null) {
			return tableId.toString() + '-' + additionalScopeFk;
		}
		return null;
	}


	private SpecificityKeySource buildKeySource(Object[] securitySpecificity, Object[] holdingSpecificity, Object[] executingSpecificity, Object[] exchangeSpecificity, Object[] additionalScopeSpecificity, Object[] valuesToAppend) {
		return SpecificityKeySource.builder(holdingSpecificity).addHierarchy(securitySpecificity).addHierarchy(executingSpecificity).addHierarchy(exchangeSpecificity).addHierarchy(additionalScopeSpecificity).setValuesToAppend(valuesToAppend).build();
	}


	List<CommissionTargetHolder> buildHolders(AccountingCommissionDefinition commission) {
		List<CommissionTargetHolder> holders = new ArrayList<>();

		CommissionTargetHolder holder = new CommissionTargetHolder(buildKeyForCommissionHolder(commission), commission, false);
		this.cacheUpdater.registerCacheUpdateEvent(commission, holder);
		holders.add(holder);

		/*
		 * If this is a ROUND_TURN commission definition for trades and isHalfTurnOnTransfer is true, then we also
		 * need to construct a cache entry for a non-internal transfer transaction. This means we make an in-memory
		 * copy of the commission definition, change the apply method to HALF_TURN and the transaction type to
		 * NON_INTERNAL_TRANSFER, then construct the specificity key for it and place the entry in the cache. We do
		 * this for convenience so that the user does not have to explicitly create duplicate commission definitions
		 * for both trades and transfers, since this logic is so common.
		 */
		if (commission.getTransactionType().equals(getAccountingTransactionTypeService().getAccountingTransactionTypeByName("Trade")) && commission.getCommissionApplyMethod() == AccountingCommissionApplyMethods.ROUND_TURN
				&& commission.isHalfTurnOnTransfer()) {
			AccountingCommissionDefinition copy = BeanUtils.cloneBean(commission, false, false);
			copy.setTransactionType(getAccountingTransactionTypeService().getAccountingTransactionTypeByName(AccountingTransactionType.NON_INTERNAL_TRANSFER));
			copy.setCommissionApplyMethod(AccountingCommissionApplyMethods.HALF_TURN);
			CommissionTargetHolder holderForTransfer = new CommissionTargetHolder(buildKeyForCommissionHolder(copy), copy, true);
			this.cacheUpdater.registerCacheUpdateEvent(commission, holderForTransfer);
			holders.add(holderForTransfer);
		}

		return holders;
	}


	private BusinessCompany getExecutingBroker(AccountingTransactionDetailInfo transactionInfo) {
		AccountingJournalDetailDefinition journalDetail = transactionInfo.getJournalDetail();

		BusinessCompany executingBroker = journalDetail.getExecutingCompany();

		//If the executing broker is not set on the transaction and the transaction is a transfer, then look at the executing broker of the position and use that
		if (executingBroker == null && transactionInfo.isTransfer()) {
			AccountingPositionTransferDetail transferDetail = getAccountingPositionTransferService().getAccountingPositionTransferDetail(journalDetail.getFkFieldId());
			if (transferDetail != null && transferDetail.getExistingPosition() != null) {
				executingBroker = transferDetail.getExistingPosition().getExecutingCompany();
			}
		}

		return executingBroker;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingTransactionTypeService getAccountingTransactionTypeService() {
		return this.accountingTransactionTypeService;
	}


	public void setAccountingTransactionTypeService(AccountingTransactionTypeService accountingTransactionTypeService) {
		this.accountingTransactionTypeService = accountingTransactionTypeService;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public AccountingCommissionService getAccountingCommissionService() {
		return this.accountingCommissionService;
	}


	public void setAccountingCommissionService(AccountingCommissionService accountingCommissionService) {
		this.accountingCommissionService = accountingCommissionService;
	}


	class CommissionCacheUpdater extends DynamicSpecificityCacheUpdater<CommissionTargetHolder> {

		CommissionCacheUpdater() {
			super(AccountingCommissionDefinition.class);
		}


		@Override
		protected List<CommissionTargetHolder> getTargetHoldersForCacheUpdate(Object updatedInstance) {
			if (updatedInstance instanceof AccountingCommissionDefinition) {
				return buildHolders((AccountingCommissionDefinition) updatedInstance);
			}

			return null;
		}
	}
}
