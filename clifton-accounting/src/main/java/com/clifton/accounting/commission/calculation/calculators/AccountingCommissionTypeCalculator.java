package com.clifton.accounting.commission.calculation.calculators;

import java.math.BigDecimal;


/**
 * The <code>AccountingCommissionTypeCalculator</code> defines a simple method for calculating commissions.
 *
 * @author NickK
 */
public interface AccountingCommissionTypeCalculator {

	BigDecimal calculate(AccountingCommissionCalculationCommand commissionTypeCalculationCommand);
}
