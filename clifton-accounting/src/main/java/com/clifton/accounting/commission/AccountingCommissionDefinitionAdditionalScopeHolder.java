package com.clifton.accounting.commission;

/**
 * A non-stored object to hold the name and ID of the additional scope entity for Commission Definitions
 *
 * @author theodorez
 */
public class AccountingCommissionDefinitionAdditionalScopeHolder {

	private Integer identifier;
	private String name;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingCommissionDefinitionAdditionalScopeHolder() {
		//default empty constructor
	}


	public AccountingCommissionDefinitionAdditionalScopeHolder(Integer identifier, String name) {
		this.identifier = identifier;
		this.name = name;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getIdentifier() {
		return this.identifier;
	}


	public String getName() {
		return this.name;
	}


	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}


	public void setName(String name) {
		this.name = name;
	}
}
