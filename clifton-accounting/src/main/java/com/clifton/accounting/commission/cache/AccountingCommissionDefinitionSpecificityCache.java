package com.clifton.accounting.commission.cache;

import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingTransactionDetailInfo;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;


/**
 * The <code>AccountingCommissionDefinitionSpecificityCache</code> is an implementation of {@link AbstractSpecificityCache} that
 * caches {@link AccountingCommissionDefinition}s by their "specificity". Given a {@link AccountingTransactionDetailInfo}, the most
 * specific commissions definitions that apply are returned.
 *
 * @author jgommels
 */
public interface AccountingCommissionDefinitionSpecificityCache {

	/**
	 * Returns a {@link AccountingMatchingCommissionDefinitionResultSet} that contains result lists which contain the most specific matching {@link AccountingCommissionDefinition} for the executing
	 * and clearing brokers of the journal detail. If either the executing or clearing brokers are null, then the resulting list of commission definitions for that broker will be empty.
	 *
	 * @param transactionInfo the information related to the transaction detail
	 */
	public AccountingMatchingCommissionDefinitionResultSet getMostSpecificCommissionDefinitions(AccountingTransactionDetailInfo transactionInfo);
}
