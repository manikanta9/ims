package com.clifton.accounting.commission.calculation.calculators.impl;

import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionTypeCalculator;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The base {@link AccountingCommissionTypeCalculator}
 * <p>
 * This implementation is thread safe during calculation and is, therefore, a singleton to avoid object creation.
 *
 * @author NickK
 */
public class AccountingCommissionTypeCalculatorSimple implements AccountingCommissionTypeCalculator {

	private static final AccountingCommissionTypeCalculatorSimple instance = new AccountingCommissionTypeCalculatorSimple();


	private AccountingCommissionTypeCalculatorSimple() {
	}


	public static AccountingCommissionTypeCalculator getInstance() {
		return instance;
	}


	@Override
	public BigDecimal calculate(AccountingCommissionCalculationCommand commissionTypeCalculationCommand) {
		BigDecimal amount = commissionTypeCalculationCommand.getCommissionDefinition().getCommissionAmount();
		return (MathUtils.isEqual(BigDecimal.ZERO, amount)) ? null : amount;
	}
}
