package com.clifton.accounting.commission.calculation;

import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionTier;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionTypeCalculator;
import com.clifton.accounting.commission.calculation.calculators.impl.AccountingCommissionTypeCalculatorTiered;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;


/**
 * The <code>AccountingCommissionTierTypes</code> is an enum that describes the type of {@link AccountingCommissionTier} of
 * a {@link AccountingCommissionDefinition}. Each tier type has logic for determining which tier is valid based on a particular
 * commission calculation action.
 *
 * @author NickK
 */
public enum AccountingCommissionTierTypes {

	AMORTIZATION("Days Since Position Open", true) {
		@Override
		Predicate<AccountingCommissionTier> getTypeSpecificTierPredicate(AccountingCommissionCalculationCommand command) {
			Date transactionDate = command.getCurrentJournalDetailDefinition().getTransactionDate();
			Date positionOpenDate = command.getParentJournalDetailDefinition().getOriginalTransactionDate();
			int numDaysSincePositionOpen = DateUtils.getDaysDifference(transactionDate, positionOpenDate);
			return tier -> MathUtils.isGreaterThan(tier.getThreshold(), numDaysSincePositionOpen);
		}
	},
	NOTIONAL("Notional Amount", false) {
		@Override
		Predicate<AccountingCommissionTier> getTypeSpecificTierPredicate(AccountingCommissionCalculationCommand command) {
			return tier -> tier.getThreshold().compareTo(command.getNotionalAmount().abs()) > 0;
		}
	},
	PRICE("Price", false) {
		@Override
		Predicate<AccountingCommissionTier> getTypeSpecificTierPredicate(AccountingCommissionCalculationCommand command) {
			return tier -> tier.getThreshold().compareTo(command.getCurrentJournalDetailDefinition().getPrice()) > 0;
		}
	},
	TRANSACTION_MATURITY("Months to Maturity", false) {
		@Override
		Predicate<AccountingCommissionTier> getTypeSpecificTierPredicate(AccountingCommissionCalculationCommand command) {
			/*
			Transaction Maturity is calculated as lesser of
			- (x) the number of full months from the clearing date for the transaction to the transaction maturity date
			- (y) the number of full months from the transaction effective date to the transaction maturity date
			 */
			Date clearingDate = command.getCurrentJournalDetailDefinition().getTransactionDate();
			Date effectiveDate = command.getCurrentJournalDetailDefinition().getSettlementDate();
			Date maturityDate = command.getCurrentJournalDetailDefinition().getInvestmentSecurity().getEndDate();
			int numMonthsClearedToMaturity = DateUtils.getFullMonthsDifference(maturityDate, clearingDate);
			int numMonthsEffectiveToMaturity = DateUtils.getFullMonthsDifference(maturityDate, effectiveDate);
			int transactionMaturity = Math.min(numMonthsClearedToMaturity, numMonthsEffectiveToMaturity);
			return tier -> MathUtils.isGreaterThan(tier.getThreshold(), transactionMaturity);
		}
	};

	private final String tierLabel;
	private final boolean parentJournalDetailDefinition;


	AccountingCommissionTierTypes(String tierLabel, boolean requiresParentJournalDetailDefinition) {
		this.tierLabel = tierLabel;
		this.parentJournalDetailDefinition = requiresParentJournalDetailDefinition;
	}


	/**
	 * @return The column label of the tier for display in the user interface
	 */
	@SuppressWarnings("unused")
	public String getTierLabel() {
		return this.tierLabel;
	}


	/**
	 * @return true if the {@link com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition} for the current
	 * action is needed for determining which tier to use in commission calculation.
	 */
	public boolean isParentJournalDetailDefinitionRequired() {
		return this.parentJournalDetailDefinition;
	}


	/**
	 * Returns the matching tier based on the provided commission calculation command.
	 *
	 * @param command the commission related information to determine which tier to use
	 * @return the matching {@link AccountingCommissionTier} or null
	 */
	public AccountingCommissionTier getCommissionTier(AccountingCommissionCalculationCommand command) {
		final List<AccountingCommissionTier> commissionTiers = command.getCommissionDefinition().getCommissionTierList();
		if (CollectionUtils.isEmpty(commissionTiers)) {
			throw new ValidationException("Commission definition [" + command.getCommissionDefinition() + "] is configured to have tiers, but none are defined.");
		}
		Predicate<AccountingCommissionTier> maxTierPredicate = AccountingCommissionTier::isMaxTier;
		Predicate<AccountingCommissionTier> tierPredicate = getTypeSpecificTierPredicate(command);
		Optional<AccountingCommissionTier> matchingTier = commissionTiers.stream().filter(maxTierPredicate.or(tierPredicate)).findFirst();
		return matchingTier.orElse(null);
	}


	abstract Predicate<AccountingCommissionTier> getTypeSpecificTierPredicate(AccountingCommissionCalculationCommand command);


	/**
	 * Returns the {@link AccountingCommissionTypeCalculator} for a tier.
	 */
	public AccountingCommissionTypeCalculator getCommissionCalculator() {
		return AccountingCommissionTypeCalculatorTiered.getInstance(this);
	}
}
