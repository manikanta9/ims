package com.clifton.accounting.commission;


import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.util.MathUtils;


/**
 * The <code>AccountingCommissionApplyMethods</code> is an enum that describes when a commission is applied. The main factors involved in this are:
 * <ul>
 * <li>Whether the transaction is opening or closing</li>
 * <li>Whether the transaction is a buy or sell</li>
 * <li>Whether the transaction is a transfer</li>
 * </ul>
 *
 * @author jgommels
 */
@SuppressWarnings("unused")
public enum AccountingCommissionApplyMethods {

	/**
	 * Indicates that the commission is always applied to a current transaction, no matter what state the transaction is in (assuming all other conditions of the commission definition
	 * hold true). This apply method is typically used when the concept of opening or closing does not apply or isn't significant.
	 */
	ALWAYS(false) {
		@Override
		public boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo transactionInfo) {
			return true;
		}


		@Override
		public boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
			//"Always" means every current transaction applies. This is not applicable to parent transactions because they would have already been charged.
			return false;
		}
	},

	/**
	 * Indicates that the commission is applied both on the opening and closing half of the trade.
	 */
	HALF_TURN(false) {
		@Override
		public boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo transactionInfo) {
			boolean applicable;
			if (transactionInfo.isTransfer()) {
				if (transactionInfo.getJournalDetail().isClosing()) {
					AccountingPositionTransfer transfer = (AccountingPositionTransfer) transactionInfo.getBookableEntity();
					applicable = transactionInfo.getJournalDetail().getHoldingInvestmentAccount().equals(transfer.getFromHoldingInvestmentAccount());
				}
				else {
					applicable = false;
				}
			}
			else {
				applicable = true;
			}

			return applicable;
		}


		@Override
		public boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
			//Half-turn commissions only apply to current transactions, not previous transactions
			return false;
		}
	},

	/**
	 * Indicates that the full commission is applied only on the closing half of the trade. There is a charge for the both the opening half and the closing half,
	 * but it is lumped together and charged on close.
	 */
	ROUND_TURN(true) {
		@Override
		public boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo transactionInfo) {
			return transactionInfo.getJournalDetail().isClosing();
		}


		@Override
		public boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
			//If it's a transfer, then it only applicable if this is the "destination" account of the transfer.
			if (parentOpeningTransactionInfo.isTransfer()) {
				AccountingPositionTransfer transfer = (AccountingPositionTransfer) parentOpeningTransactionInfo.getBookableEntity();
				return currentTransactionInfo.getJournalDetail().getHoldingInvestmentAccount().equals(transfer.getToHoldingInvestmentAccount());
			}

			//If not a transfer, then parent opening transactions are always applicable when round-turn
			return true;
		}
	},

	/**
	 * Indicates that the full commission is applied only on the closing half of the trade, and only if the close is due to a sell. Like regular round-turn, there is a charge
	 * for the both the opening half and the closing half, but it is lumped together and charged on close.
	 */
	ROUND_TURN_ON_SELL(true) {
		@Override
		public boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo transactionInfo) {
			return transactionInfo.getJournalDetail().isClosing() && transactionInfo.isSell();
		}


		@Override
		public boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
			return currentTransactionInfo.getJournalDetail().isClosing() && currentTransactionInfo.isSell();
		}
	},

	/**
	 * Indicates that the commission is only applied on the opening half of the trade.
	 */
	ONLY_ON_OPEN(false) {
		@Override
		public boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo transactionInfo) {
			return transactionInfo.getJournalDetail().isOpening();
		}


		@Override
		public boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
			//Only-on-open is only applicable to current opening transactions
			return false;
		}
	},

	/**
	 * Indicates that the commission is only applied on the closing half of the trade. Unlike {@link AccountingCommissionApplyMethods#ROUND_TURN}, there is no
	 * commission for the opening half of the trade.
	 */
	ONLY_ON_CLOSE(false) {
		@Override
		public boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo transactionInfo) {
			return transactionInfo.getJournalDetail().isClosing();
		}


		@Override
		public boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
			//Only-on-close is only applicable to current closing transactions
			return false;
		}
	},

	/**
	 * Indicates that the commission is only applied on the closing half of the trade when the price is greater than 0.
	 */
	ONLY_ON_CLOSE_WITH_PRICE_GREATER_THAN_ZERO(false) {
		@Override
		public boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo transactionInfo) {
			AccountingJournalDetailDefinition journalDetail = transactionInfo.getJournalDetail();
			return journalDetail.isClosing() && MathUtils.isPositive(journalDetail.getPrice());
		}


		@Override
		public boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
			//Only applicable to current closing transactions
			return false;
		}
	},

	/**
	 * Indicates that the commission is only applied on the closing half of the trade when the price equals 0.
	 */
	ONLY_ON_CLOSE_WITH_PRICE_EQUAL_TO_ZERO(false) {
		@Override
		public boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo transactionInfo) {
			AccountingJournalDetailDefinition journalDetail = transactionInfo.getJournalDetail();
			return journalDetail.isClosing() && MathUtils.isNullOrZero(journalDetail.getPrice());
		}


		@Override
		public boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
			//Only applicable to current closing transactions
			return false;
		}
	},

	/**
	 * Indicates that the commission is only applied on a sell
	 */
	ONLY_ON_SELL(false) {
		@Override
		public boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo transactionInfo) {
			return transactionInfo.isSell();
		}


		@Override
		public boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
			//Only-on-sell is only applicable to current selling transactions
			return false;
		}
	};

	private final boolean openingTransactionCommissionsAppliedOnClose;


	AccountingCommissionApplyMethods(boolean openingTransactionCommissionsAppliedOnClose) {
		this.openingTransactionCommissionsAppliedOnClose = openingTransactionCommissionsAppliedOnClose;
	}


	/**
	 * Returns whether a commission with this apply method is <b>currently</b> applicable to the <b>current</b> transaction. (For example, an apply method of {@link #ROUND_TURN}
	 * will only be applied when the position is closed, not when it is being opened.)
	 *
	 * @param currentTransactionInfo the transaction info for the current transaction
	 */
	public abstract boolean isCommissionCurrentlyApplicableToCurrentTransaction(AccountingTransactionDetailInfo currentTransactionInfo);


	/**
	 * Returns whether a commission with this apply method is <b>currently</b> applicable to a <b>parent opening transaction</b> of a current closing transaction.
	 *
	 * @param parentOpeningTransactionInfo
	 * @param currentTransactionInfo
	 */
	public boolean isCommissionCurrentlyApplicableToParentTransaction(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo) {
		if (parentOpeningTransactionInfo.getJournalDetail().isClosing()) {
			throw new IllegalArgumentException("parentOpeningTransactionInfo must be an opening transaction");
		}

		if (currentTransactionInfo.getJournalDetail().isOpening()) {
			throw new IllegalArgumentException("currentTransactionInfo must be a closing transaction");
		}

		return isCommissionCurrentlyApplicableToParentTransactionImpl(parentOpeningTransactionInfo, currentTransactionInfo);
	}


	abstract boolean isCommissionCurrentlyApplicableToParentTransactionImpl(AccountingTransactionDetailInfo parentOpeningTransactionInfo, AccountingTransactionDetailInfo currentTransactionInfo);


	/**
	 * @return whether or not the commission(s) from the opening side should be applied on close rather than at the time of open.
	 */
	public boolean isOpeningTransactionCommissionsAppliedOnClose() {
		return this.openingTransactionCommissionsAppliedOnClose;
	}
}
