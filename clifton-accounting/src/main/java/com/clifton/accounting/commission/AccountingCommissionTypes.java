package com.clifton.accounting.commission;


import com.clifton.accounting.commission.calculation.AccountingCommissionTierTypes;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionTypeCalculator;
import com.clifton.accounting.commission.calculation.calculators.impl.AccountingCommissionTypeCalculatorMultiplier;
import com.clifton.accounting.commission.calculation.calculators.impl.AccountingCommissionTypeCalculatorSimple;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingCommissionTypes</code> is an enum that describes a type of {@link AccountingCommissionDefinition}. This
 * essentially consists of simple, amortized, and tiered commissions, and whether they apply to executing, clearing, or both.
 *
 * @author jgommels
 */
public enum AccountingCommissionTypes {
	/**
	 * A simple commission that is not tiered or amortized
	 */
	SIMPLE {
		@Override
		public AccountingCommissionTypeCalculator getCommissionCalculator(@SuppressWarnings("unused") AccountingCommissionCalculationCommand command) {
			return AccountingCommissionTypeCalculatorSimple.getInstance();
		}


		@Override
		public boolean isCommissionAmountAllowed() {
			return true;
		}
	},


	TIERED_NOTIONAL {
		@Override
		public boolean hasRanges() {
			return true;
		}


		@Override
		public boolean isParentJournalDetailDefinitionRequiredForCalculation() {
			return AccountingCommissionTierTypes.NOTIONAL.isParentJournalDetailDefinitionRequired();
		}


		@Override
		public AccountingCommissionTypeCalculator getCommissionCalculator(AccountingCommissionCalculationCommand command) {
			return AccountingCommissionTierTypes.NOTIONAL.getCommissionCalculator();
		}
	},

	TIERED_PRICE {
		@Override
		public boolean hasRanges() {
			return true;
		}


		@Override
		public boolean isParentJournalDetailDefinitionRequiredForCalculation() {
			return AccountingCommissionTierTypes.PRICE.isParentJournalDetailDefinitionRequired();
		}


		@Override
		public AccountingCommissionTypeCalculator getCommissionCalculator(AccountingCommissionCalculationCommand command) {
			return AccountingCommissionTierTypes.PRICE.getCommissionCalculator();
		}
	},

	TIERED_MATURITY {
		@Override
		public boolean hasRanges() {
			return true;
		}


		@Override
		public boolean isParentJournalDetailDefinitionRequiredForCalculation() {
			return AccountingCommissionTierTypes.TRANSACTION_MATURITY.isParentJournalDetailDefinitionRequired();
		}


		@Override
		public AccountingCommissionTypeCalculator getCommissionCalculator(AccountingCommissionCalculationCommand command) {
			return AccountingCommissionTierTypes.TRANSACTION_MATURITY.getCommissionCalculator();
		}
	},

	TIERED_AMORTIZATION {
		@Override
		public boolean hasRanges() {
			return true;
		}


		@Override
		public boolean isParentJournalDetailDefinitionRequiredForCalculation() {
			return AccountingCommissionTierTypes.AMORTIZATION.isParentJournalDetailDefinitionRequired();
		}


		@Override
		public AccountingCommissionTypeCalculator getCommissionCalculator(AccountingCommissionCalculationCommand command) {
			return AccountingCommissionTierTypes.AMORTIZATION.getCommissionCalculator();
		}
	},

	/**
	 * An amortized commission that is only applied on executing
	 */
	TIERED_AMORTIZED_STRAIGHT_LINE {
		@Override
		public boolean isParentJournalDetailDefinitionRequiredForCalculation() {
			return true;
		}


		@Override
		public boolean isCommissionAmountAllowed() {
			return true;
		}


		/**
		 * Returns a {@link AccountingCommissionTypeCalculatorSimple} unless the current trade is closing and
		 * the commission broker type is executing. In the second case, the calculator is wrapped in a {@link AccountingCommissionTypeCalculatorMultiplier}
		 * with the amortized straight-line multiplication rate.
		 *
		 * @param command the necessary commission data for calculating the commission for this accounting commission type
		 * @return the calculator for this type
		 */
		@Override
		public AccountingCommissionTypeCalculator getCommissionCalculator(AccountingCommissionCalculationCommand command) {
			AccountingCommissionTypeCalculator baseCalculator = AccountingCommissionTypeCalculatorSimple.getInstance();

			if (!command.getCurrentJournalDetailDefinition().isClosing()) {
				return baseCalculator;
			}

			Date tradeDateOpen = command.getParentJournalDetailDefinition().getOriginalTransactionDate();
			Date tradeDateClose = command.getCurrentJournalDetailDefinition().getTransactionDate();
			Date endDate = command.getCurrentJournalDetailDefinition().getInvestmentSecurity().getEndDate();

			if (endDate == null) {
				throw new RuntimeException("The multiplier for amortized-straight-line cannot be calculated unless the end date of the security is set.");
			}

			BigDecimal numDaysLeft = new BigDecimal(DateUtils.getDaysDifference(endDate, tradeDateClose));
			BigDecimal numDaysTotal = new BigDecimal(DateUtils.getDaysDifference(endDate, tradeDateOpen));

			return new AccountingCommissionTypeCalculatorMultiplier(baseCalculator, MathUtils.divide(numDaysLeft, numDaysTotal));
		}
	};


	/**
	 * @return whether this commission type has {@link AccountingCommissionTier}s specified.
	 */
	public boolean hasRanges() {
		return false;
	}


	/**
	 * @return whether this commission type can have a {@link AccountingCommissionDefinition#commissionAmount}.
	 */
	public boolean isCommissionAmountAllowed() {
		return false;
	}


	/**
	 * @return true if the parent journal definition definition is required for the commission definition and broker commission type
	 */
	public boolean isParentJournalDetailDefinitionRequiredForCalculation() {
		return false;
	}


	/**
	 * Returns the {@link AccountingCommissionTypeCalculator} for calculating this accounting commission type's commission amount.
	 *
	 * @param command the necessary commission data for calculating the commission for this accounting commission type
	 */
	public abstract AccountingCommissionTypeCalculator getCommissionCalculator(AccountingCommissionCalculationCommand command);
}
