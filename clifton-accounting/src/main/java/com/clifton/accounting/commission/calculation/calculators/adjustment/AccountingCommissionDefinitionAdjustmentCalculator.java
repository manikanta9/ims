package com.clifton.accounting.commission.calculation.calculators.adjustment;

import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.core.converter.json.JsonHandler;

import java.math.BigDecimal;


/**
 * Implementing classes perform abnormally Commission Adjustment Calculators
 *
 * @author theodorez
 */
public interface AccountingCommissionDefinitionAdjustmentCalculator {

	/**
	 * Performs the calculation, taking into account the commission that has been calculated so far and the appropriate calculation command.
	 *
	 * @param rawCommission the commission that has been calculated up to this point
	 * @param command
	 * @return the final commission
	 */
	public BigDecimal computeCommission(BigDecimal rawCommission, AccountingCommissionCalculationCommand command, JsonHandler<?> jsonHandler);
}
