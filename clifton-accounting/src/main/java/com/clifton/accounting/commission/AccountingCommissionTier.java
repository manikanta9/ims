package com.clifton.accounting.commission;


import com.clifton.core.beans.BaseEntity;

import java.math.BigDecimal;


public class AccountingCommissionTier extends BaseEntity<Integer> {

	private AccountingCommissionDefinition commissionDefinition;
	private BigDecimal threshold;
	private BigDecimal feeAmount;


	public boolean isMaxTier() {
		return this.threshold == null;
	}


	public AccountingCommissionDefinition getCommissionDefinition() {
		return this.commissionDefinition;
	}


	public void setCommissionDefinition(AccountingCommissionDefinition commissionDefinition) {
		this.commissionDefinition = commissionDefinition;
	}


	public BigDecimal getThreshold() {
		return this.threshold;
	}


	public void setThreshold(BigDecimal threshold) {
		this.threshold = threshold;
	}


	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}


	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}
}
