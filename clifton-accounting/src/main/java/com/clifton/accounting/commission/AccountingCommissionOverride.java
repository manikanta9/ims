package com.clifton.accounting.commission;


import com.clifton.accounting.account.AccountingAccount;

import java.math.BigDecimal;


/**
 * The <code>AccountingCommissionOverride</code> is an interface that describes an
 * overridden commission.
 *
 * @author jgommels
 */
public interface AccountingCommissionOverride {

	public AccountingAccount getAccountingAccount();


	public void setAccountingAccount(AccountingAccount accountingAccount);


	public BigDecimal getAmountPerUnit();


	public void setAmountPerUnit(BigDecimal amountPerUnit);
}
