package com.clifton.accounting.commission.calculation;


import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionTypeCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


public enum AccountingCommissionCalculationMethods {

	FLAT_AMOUNT {
		@Override
		public BigDecimal applyCommissionMethod(BigDecimal rate, @SuppressWarnings("unused") AccountingCommissionCalculationCommand commissionCalculationCommand) {
			return normalize(rate, commissionCalculationCommand);
		}
	},

	PER_UNIT {
		@Override
		public BigDecimal applyCommissionMethod(BigDecimal rate, AccountingCommissionCalculationCommand commissionCalculationCommand) {
			return normalize(MathUtils.multiply(commissionCalculationCommand.getQuantity().abs(), rate), commissionCalculationCommand);
		}
	},

	PER_UNIT_BPS {
		@Override
		public BigDecimal applyCommissionMethod(BigDecimal rate, AccountingCommissionCalculationCommand commissionCalculationCommand) {
			BigDecimal result = MathUtils.multiply(commissionCalculationCommand.getQuantity().abs(), rate);
			result = applyBpsDivisor(result);
			return normalize(result, commissionCalculationCommand);
		}
	},

	PER_NOTIONAL {
		@Override
		public BigDecimal applyCommissionMethod(BigDecimal rate, AccountingCommissionCalculationCommand commissionCalculationCommand) {
			return normalize(MathUtils.multiply(commissionCalculationCommand.getNotionalAmount().abs(), rate), commissionCalculationCommand);
		}
	},

	PER_NOTIONAL_BPS {
		@Override
		public BigDecimal applyCommissionMethod(BigDecimal rate, AccountingCommissionCalculationCommand commissionCalculationCommand) {
			BigDecimal result = MathUtils.multiply(commissionCalculationCommand.getNotionalAmount().abs(), rate);
			result = applyBpsDivisor(result);
			return normalize(result, commissionCalculationCommand);
		}
	},

	/**
	 * IRS swap trades include a notional principal amount that is referenced in IMS as the quantity.
	 */
	PER_MILLION_QUANTITY {
		@Override
		public BigDecimal applyCommissionMethod(BigDecimal rate, AccountingCommissionCalculationCommand commissionCalculationCommand) {
			BigDecimal result = MathUtils.multiply(commissionCalculationCommand.getQuantity().abs(), rate);
			result = MathUtils.divide(result, BigDecimal.valueOf(1_000_000));
			return normalize(result, commissionCalculationCommand);
		}
	};


	public abstract BigDecimal applyCommissionMethod(BigDecimal rate, AccountingCommissionCalculationCommand commissionCalculationCommand);


	BigDecimal calculateCommissionAmount(AccountingCommissionCalculationCommand commissionCalculationCommand) {
		AccountingCommissionTypeCalculator commissionCalculator = commissionCalculationCommand.getCommissionDefinition().getCommissionType().getCommissionCalculator(commissionCalculationCommand);
		BigDecimal commissionRate = commissionCalculator.calculate(commissionCalculationCommand);
		// Use zero if rate is null to prevent NPE on normalize.
		return applyCommissionMethod(commissionRate == null ? BigDecimal.ZERO : commissionRate, commissionCalculationCommand);
	}


	BigDecimal normalize(BigDecimal result, AccountingCommissionCalculationCommand commissionCalculationCommand) {
		if (commissionCalculationCommand.getCommissionDefinition() != null && commissionCalculationCommand.getCommissionDefinition().getCommissionCurrency() != null) {
			InvestmentNotionalCalculatorTypes calculator = InvestmentCalculatorUtils.getNotionalCalculator(commissionCalculationCommand.getCommissionDefinition().getCommissionCurrency());
			if (calculator != null) {
				return calculator.round(result);
			}
		}
		return result.setScale(2, BigDecimal.ROUND_HALF_UP);
	}


	BigDecimal applyBpsDivisor(BigDecimal amount) {
		return MathUtils.divide(amount, BigDecimal.valueOf(10000));
	}
}
