package com.clifton.accounting.commission.calculation.calculators.adjustment;

import com.clifton.accounting.commission.AccountingCommissionTier;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;


/**
 * @author theodorez
 */
public class MerrillLynchCommissionAdjustmentCalculator implements AccountingCommissionDefinitionAdjustmentCalculator {

	//JSON Configuration for the calculator
	private String adjustmentCalculatorConfiguration;


	@Override
	public BigDecimal computeCommission(BigDecimal rawCommission, AccountingCommissionCalculationCommand command, JsonHandler<?> jsonHandler) {
		BigDecimal notional = MathUtils.abs(command.getNotionalAmount());
		BigDecimal quantity = MathUtils.abs(command.getQuantity());

		MerrillLynchCommissionAdjustmentCalculatorConfiguration configuration = (MerrillLynchCommissionAdjustmentCalculatorConfiguration) jsonHandler.fromJson(getAdjustmentCalculatorConfiguration(), MerrillLynchCommissionAdjustmentCalculatorConfiguration.class);

		BigDecimal initialPremiumCommission = configuration.getMinimumCommissionAmount();
		//Step 1 - calculate commission using option premium  (contracts*price*multiplier)
		if (MathUtils.isLessThan(notional, configuration.getMinimumCommissionThreshold())) {
			initialPremiumCommission = MathUtils.multiply(notional, configuration.getMinimumCommissionMultiplier());
		}

		//Step 2 - premium component
		BigDecimal premiumComponent = BigDecimal.ZERO;
		//If the notional is between 130 and 500 the premium component is always 65
		if (MathUtils.isBetween(notional, configuration.getPremiumLowerThreshold(), configuration.getPremiumUpperThreshold())) {
			premiumComponent = configuration.getPremiumMinimum();
		}
		else {
			CommissionOverrideTier premiumTier = getApplicableTier(configuration.getPremiumTiers(), tier -> tier.getThreshold().compareTo(notional) > 0);
			if (premiumTier != null) {
				premiumComponent = MathUtils.add(MathUtils.multiply(notional, premiumTier.getFeeAmount()), premiumTier.getBaseValue());
			}
		}

		//Step 3 - contract component
		BigDecimal contractComponent = BigDecimal.ZERO;
		//At a notional over $500 there is an additional fee per contract (plus any applicable base value)
		if (MathUtils.isGreaterThan(notional, configuration.getContractThreshold())) {
			CommissionOverrideTier contractTier = getApplicableTier(configuration.getContractTiers(), tier -> tier.getThreshold().compareTo(quantity) > 0);
			if (contractTier != null) {
				contractComponent = MathUtils.add(MathUtils.multiply(quantity, contractTier.getFeeAmount()), contractTier.getBaseValue());
			}
		}

		//Step 4 - Calculate maximum commission amount
		BigDecimal maxCommission = BigDecimal.ZERO;
		CommissionOverrideTier maxTier = getApplicableTier(configuration.getMaxCommissionTiers(), tier -> tier.getThreshold().compareTo(notional) > 0);
		if (maxTier != null) {
			maxCommission = MathUtils.add(MathUtils.multiply(notional, maxTier.getFeeAmount()), maxTier.getBaseValue());
		}

		//Step 5 - add #2 and #3 together
		BigDecimal calculatedCommission = MathUtils.add(premiumComponent, contractComponent);

		//Step 6 - Use the smaller value from #4 and #5
		calculatedCommission = MathUtils.min(maxCommission, calculatedCommission);

		//Step 7 - the final commission amount is the larger of #1 or #6
		calculatedCommission = MathUtils.max(initialPremiumCommission, calculatedCommission);

		if (MathUtils.isNullOrZero(calculatedCommission)) {
			return rawCommission;
		}

		if (command.getCommissionDefinition() != null && command.getCommissionDefinition().getCommissionCurrency() != null) {
			InvestmentNotionalCalculatorTypes calculator = InvestmentCalculatorUtils.getNotionalCalculator(command.getCommissionDefinition().getCommissionCurrency());
			if (calculator != null) {
				return calculator.round(calculatedCommission);
			}
		}
		return calculatedCommission.setScale(2, BigDecimal.ROUND_HALF_UP);
	}


	private static CommissionOverrideTier getApplicableTier(List<CommissionOverrideTier> tierArray, Predicate<CommissionOverrideTier> predicate) {
		final Predicate<CommissionOverrideTier> maxTierPredicate = CommissionOverrideTier::isMaxTier;
		return tierArray.stream().filter(maxTierPredicate.or(predicate)).findFirst().orElse(null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getAdjustmentCalculatorConfiguration() {
		return this.adjustmentCalculatorConfiguration;
	}


	public void setAdjustmentCalculatorConfiguration(String adjustmentCalculatorConfiguration) {
		this.adjustmentCalculatorConfiguration = adjustmentCalculatorConfiguration;
	}


	////////////////////////////////////////////////////////////////////////////
	////////                  Public Inner Classes                 /////////////
	////////////////////////////////////////////////////////////////////////////

	protected static class MerrillLynchCommissionAdjustmentCalculatorConfiguration {

		//The amount that determines whether the minimumCommissionAmount or minimumCommissionMultiplier will be used
		private BigDecimal minimumCommissionThreshold;
		//The minimum value of the commission if the notional is greather than or equal to than the mimimumCommissionThreshhold
		private BigDecimal minimumCommissionAmount;
		//The value the notional will be multiplied by if the notional is less than the mimimumCommissionThreshhold
		private BigDecimal minimumCommissionMultiplier;

		//The value the notional should be greater than or equal to for the premiumMinimum to apply
		private BigDecimal premiumLowerThreshold;
		//The value the notional should be less than or equal to for the premiumMinimum to apply
		private BigDecimal premiumUpperThreshold;
		private BigDecimal premiumMinimum;

		private List<MerrillLynchCommissionAdjustmentCalculator.CommissionOverrideTier> premiumTiers = new ArrayList<>();

		//The value the notional must be greater than or equal to for the contract tier to apply
		private BigDecimal contractThreshold;
		private List<MerrillLynchCommissionAdjustmentCalculator.CommissionOverrideTier> contractTiers = new ArrayList<>();


		private List<MerrillLynchCommissionAdjustmentCalculator.CommissionOverrideTier> maxCommissionTiers = new ArrayList<>();


		public MerrillLynchCommissionAdjustmentCalculatorConfiguration() {
			//default empty constructor
		}


		public BigDecimal getMinimumCommissionThreshold() {
			return this.minimumCommissionThreshold;
		}


		public void setMinimumCommissionThreshold(BigDecimal minimumCommissionThreshold) {
			this.minimumCommissionThreshold = minimumCommissionThreshold;
		}


		public BigDecimal getMinimumCommissionAmount() {
			return this.minimumCommissionAmount;
		}


		public void setMinimumCommissionAmount(BigDecimal minimumCommissionAmount) {
			this.minimumCommissionAmount = minimumCommissionAmount;
		}


		public BigDecimal getMinimumCommissionMultiplier() {
			return this.minimumCommissionMultiplier;
		}


		public void setMinimumCommissionMultiplier(BigDecimal minimumCommissionMultiplier) {
			this.minimumCommissionMultiplier = minimumCommissionMultiplier;
		}


		public BigDecimal getPremiumLowerThreshold() {
			return this.premiumLowerThreshold;
		}


		public void setPremiumLowerThreshold(BigDecimal premiumLowerThreshold) {
			this.premiumLowerThreshold = premiumLowerThreshold;
		}


		public BigDecimal getPremiumUpperThreshold() {
			return this.premiumUpperThreshold;
		}


		public void setPremiumUpperThreshold(BigDecimal premiumUpperThreshold) {
			this.premiumUpperThreshold = premiumUpperThreshold;
		}


		public BigDecimal getPremiumMinimum() {
			return this.premiumMinimum;
		}


		public void setPremiumMinimum(BigDecimal premiumMinimum) {
			this.premiumMinimum = premiumMinimum;
		}


		public List<MerrillLynchCommissionAdjustmentCalculator.CommissionOverrideTier> getPremiumTiers() {
			return this.premiumTiers;
		}


		public void setPremiumTiers(List<CommissionOverrideTier> premiumTiers) {
			this.premiumTiers = premiumTiers;
		}


		public BigDecimal getContractThreshold() {
			return this.contractThreshold;
		}


		public void setContractThreshold(BigDecimal contractThreshold) {
			this.contractThreshold = contractThreshold;
		}


		public List<MerrillLynchCommissionAdjustmentCalculator.CommissionOverrideTier> getContractTiers() {
			return this.contractTiers;
		}


		public void setContractTiers(List<MerrillLynchCommissionAdjustmentCalculator.CommissionOverrideTier> contractTiers) {
			this.contractTiers = contractTiers;
		}


		public List<MerrillLynchCommissionAdjustmentCalculator.CommissionOverrideTier> getMaxCommissionTiers() {
			return this.maxCommissionTiers;
		}


		public void setMaxCommissionTiers(List<MerrillLynchCommissionAdjustmentCalculator.CommissionOverrideTier> maxCommissionTiers) {
			this.maxCommissionTiers = maxCommissionTiers;
		}
	}

	protected static class CommissionOverrideTier extends AccountingCommissionTier {

		private BigDecimal baseValue;


		public CommissionOverrideTier() {
			//empty default constructor
		}


		public BigDecimal getBaseValue() {
			return this.baseValue;
		}


		public void setBaseValue(BigDecimal baseValue) {
			this.baseValue = baseValue;
		}
	}
}
