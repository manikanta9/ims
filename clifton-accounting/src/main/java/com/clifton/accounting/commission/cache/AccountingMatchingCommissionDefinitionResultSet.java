package com.clifton.accounting.commission.cache;


import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingTransactionDetailInfo;

import java.util.List;


/**
 * The <code>AccountingMatchingCommissionDefinitionResultSet</code> represents a result set from the {@link AccountingCommissionDefinitionSpecificityCache}. It contains the
 * "most specific" matching commission definitions for a given transaction. Note that depending on certain conditions such half-turn and round-turn, the commission definition(s)
 * in a result may or may not be <i>currently</i> applicable to the associated transaction.
 *
 * @author jgommels
 */
public class AccountingMatchingCommissionDefinitionResultSet {

	private final List<AccountingCommissionDefinition> commissionDefinitionList;
	private final AccountingTransactionDetailInfo transactionInfo;


	public AccountingMatchingCommissionDefinitionResultSet(List<AccountingCommissionDefinition> commissionDefinitionList,
	                                                       AccountingTransactionDetailInfo transactionInfo) {
		this.commissionDefinitionList = commissionDefinitionList;
		this.transactionInfo = transactionInfo;
	}


	public List<AccountingCommissionDefinition> getCommissionDefinitions() {
		return this.commissionDefinitionList;
	}


	public AccountingTransactionDetailInfo getTransactionInfo() {
		return this.transactionInfo;
	}


	public boolean isNoMatchesFound() {
		return this.commissionDefinitionList.isEmpty();
	}
}
