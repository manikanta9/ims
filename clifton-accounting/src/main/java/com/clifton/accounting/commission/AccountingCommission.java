package com.clifton.accounting.commission;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.beans.BeanUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


public class AccountingCommission implements Comparable<AccountingCommission> {

	private final AccountingAccount accountingAccount;
	private BigDecimal commissionAmount;

	/**
	 * The currency that the commission is in. If null, this indicates that the currency denomination of the security should be used.
	 */
	private InvestmentSecurity currency;


	public AccountingCommission(AccountingAccount accountingAccount, InvestmentSecurity currency) {
		this(accountingAccount, currency, BigDecimal.ZERO);
	}


	public AccountingCommission(AccountingAccount accountingAccount, InvestmentSecurity currency, BigDecimal commissionAmount) {
		this.accountingAccount = accountingAccount;
		this.currency = currency;
		this.commissionAmount = commissionAmount;
	}


	/**
	 * @param accountingAccount
	 * @param currency
	 * @return a key that used to identify an AccountingCommission based on the accounting account and commission currency
	 */
	public static String generateKey(AccountingAccount accountingAccount, InvestmentSecurity currency) {
		return accountingAccount.getId() + "-" + BeanUtils.getBeanIdentity(currency);
	}


	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}


	public void addCommissionAmount(BigDecimal amountToAdd) {
		this.commissionAmount = MathUtils.add(this.commissionAmount, amountToAdd);
	}


	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	public InvestmentSecurity getCurrency() {
		return this.currency;
	}


	public void setCurrency(InvestmentSecurity currency) {
		this.currency = currency;
	}


	@Override
	public String toString() {
		return "AccountingCommission [accountingAccount=" + this.accountingAccount + ", commissionAmount=" + this.commissionAmount + "]";
	}


	@Override
	public int compareTo(AccountingCommission otherCommission) {
		//Sort by accounting account name (This makes validation with automated tests more easy since commission/fee ordering will be consistent)
		return this.accountingAccount.getName().compareTo(otherCommission.accountingAccount.getName());
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.accountingAccount == null) ? 0 : this.accountingAccount.hashCode());
		result = prime * result + ((this.commissionAmount == null) ? 0 : this.commissionAmount.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AccountingCommission other = (AccountingCommission) obj;
		if (this.accountingAccount == null) {
			if (other.accountingAccount != null) {
				return false;
			}
		}
		else if (!this.accountingAccount.equals(other.accountingAccount)) {
			return false;
		}
		if (this.commissionAmount == null) {
			if (other.commissionAmount != null) {
				return false;
			}
		}
		else if (!this.commissionAmount.equals(other.commissionAmount)) {
			return false;
		}
		return true;
	}
}
