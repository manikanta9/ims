package com.clifton.accounting.commission.calculation;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.commission.AccountingCommission;
import com.clifton.accounting.commission.AccountingCommissionApplyMethods;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionOverride;
import com.clifton.accounting.commission.AccountingCommissionTypes;
import com.clifton.accounting.commission.AccountingTransactionDetailInfo;
import com.clifton.accounting.commission.cache.AccountingCommissionDefinitionSpecificityCache;
import com.clifton.accounting.commission.cache.AccountingMatchingCommissionDefinitionResultSet;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.accounting.commission.calculation.calculators.adjustment.AccountingCommissionDefinitionAdjustmentCalculator;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeLocator;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Component
public class AccountingCommissionCalculatorImpl implements AccountingCommissionCalculator {

	private AccountingBookingService<BookableEntity> accountingBookingService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private InvestmentCalculator investmentCalculator;
	private AccountingCommissionDefinitionSpecificityCache accountingCommissionDefinitionSpecificityCache;

	private AccountingTransactionTypeLocator accountingTransactionTypeLocator;

	private SystemBeanService systemBeanService;

	private JsonHandler<?> jsonHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends BookableEntity> List<AccountingCommission> getAccountingCommissionListForJournalDetail(AccountingCommissionCalculationProperties<T> commissionCalculationProperties) {
		//Get the "most specific" commission definitions that match the transaction
		//Use the result set that is passed in - if it is populated - to avoid an extra specificity cache lookup
		AccountingMatchingCommissionDefinitionResultSet resultSet;
		if (commissionCalculationProperties.getCommissionDefinitions() != null) {
			resultSet = new AccountingMatchingCommissionDefinitionResultSet(commissionCalculationProperties.getCommissionDefinitions(), getTransactionDetailInfo(commissionCalculationProperties));
		}
		else {
			resultSet = getMatchingAccountingCommissionResultSet(commissionCalculationProperties);
		}

		AccountingCommissionCalculationCommand commissionCalculationCommand = createCommissionCalculationCommand(commissionCalculationProperties.getPositionDetail(), commissionCalculationProperties.getOriginalQuantity());

		addOpeningTransactionCommissionDefinitionsPaidOnClose(commissionCalculationCommand, resultSet);
		processOverrides(commissionCalculationProperties.getOverrides(), resultSet.getCommissionDefinitions());

		List<AccountingCommission> results = calculateCommissionAmounts(resultSet, commissionCalculationCommand);
		Collections.sort(results);

		return results;
	}


	@DoNotAddRequestMapping
	@Override
	public <T extends BookableEntity> List<AccountingCommissionDefinition> getMatchingAccountingCommissionDefinitionList(AccountingCommissionCalculationProperties<T> commissionCalculationProperties) {
		return getMatchingAccountingCommissionResultSet(commissionCalculationProperties).getCommissionDefinitions();
	}


	private <T extends BookableEntity> AccountingTransactionDetailInfo getTransactionDetailInfo(AccountingCommissionCalculationProperties<T> commissionCalculationProperties) {
		AccountingJournalDetailDefinition journalDetail = commissionCalculationProperties.getPositionDetail();
		AccountingTransactionType transactionType = getAccountingTransactionTypeLocator().getAccountingTransactionType(journalDetail, commissionCalculationProperties.getBookableEntity());
		return new AccountingTransactionDetailInfo(journalDetail, commissionCalculationProperties.getBookableEntity(), transactionType);
	}


	private <T extends BookableEntity> AccountingMatchingCommissionDefinitionResultSet getMatchingAccountingCommissionResultSet(AccountingCommissionCalculationProperties<T> commissionCalculationProperties) {
		//Get the "most specific" commission definitions that match the transaction
		return getAccountingCommissionDefinitionSpecificityCache().getMostSpecificCommissionDefinitions(getTransactionDetailInfo(commissionCalculationProperties));
	}


	/**
	 * This method determines the correct quantity to use for the commission calculation and creates a {@link AccountingCommissionCalculationCommand}
	 * for calculating the commission.
	 * <p>
	 * This method is package private and static because used by unit tests.
	 *
	 * @param journalDetailDefinition the transaction journal detail
	 * @param originalQuantity        the original quantity from the {@link AccountingCommissionCalculationProperties}
	 */
	static AccountingCommissionCalculationCommand createCommissionCalculationCommand(AccountingJournalDetailDefinition journalDetailDefinition, BigDecimal originalQuantity) {
		BigDecimal quantity = getQuantity(journalDetailDefinition, originalQuantity);
		return new AccountingCommissionCalculationCommand(journalDetailDefinition, quantity);
	}


	/**
	 * If this is a closing transaction, then we must perform some additional logic for round turn commissions in
	 * case the commission on the opening side of the trade is different. The two main reasons this could occur is
	 * if the executing broker on the opening and closing side are different, or if the commission amount changed.
	 * <p>
	 * We don't need to look at the opening side if this is a transfer and this is the source account.
	 *
	 * @param commissionCalculationCommand
	 * @param currentTransactionCommissionResultSet
	 */
	private void addOpeningTransactionCommissionDefinitionsPaidOnClose(AccountingCommissionCalculationCommand commissionCalculationCommand, AccountingMatchingCommissionDefinitionResultSet currentTransactionCommissionResultSet) {
		AccountingJournalDetailDefinition journalDetailDefinition = commissionCalculationCommand.getCurrentJournalDetailDefinition();
		if (journalDetailDefinition.isClosing() && journalDetailDefinition.getParentTransaction() != null && !isTransferAndIsSourceAccount(currentTransactionCommissionResultSet.getTransactionInfo())) {
			addOpeningTransactionCommissionPaidOnClose(currentTransactionCommissionResultSet, commissionCalculationCommand);
		}
	}


	/**
	 * If there are any commission overrides, then replace the values of those overrides with the "per unit" amount.
	 * Overrides without a "per unit" amount in any commission definitions with a matching AccountingAccount
	 * are removed from the list.
	 */
	private void processOverrides(List<? extends AccountingCommissionOverride> overrideList, List<AccountingCommissionDefinition> commissionDefinitionList) {
		if (!CollectionUtils.isEmpty(overrideList)) {
			// remove commission definitions that match the overrides
			Iterator<AccountingCommissionDefinition> commissionDefinitionIterator = CollectionUtils.getIterable(commissionDefinitionList).iterator();
			while (commissionDefinitionIterator.hasNext()) {
				AccountingCommissionDefinition commissionDefinition = commissionDefinitionIterator.next();
				for (AccountingCommissionOverride override : overrideList) {
					//If the AmountPerUnit is set and either of the two conditions is met, remove the definition so that it can be overridden.
					//1. The definition GL account matches the override GL account
					// OR
					//2. The Definition GL Account is NOT a Fee and IS a Commission AND the override GL account is EXPENSE_COMMISSION (generic commission type)
					//      This should override all commissions (that are not fees)
					if (override.getAmountPerUnit() != null) {
						AccountingAccount definitionAccount = commissionDefinition.getExpenseAccountingAccount();
						if (definitionAccount.equals(override.getAccountingAccount())
								|| (definitionAccount.isCommission()
								&& !definitionAccount.isFee()
								&& AccountingAccount.EXPENSE_COMMISSION.equals(override.getAccountingAccount().getName()))) {
							commissionDefinitionIterator.remove();
						}
					}
				}
			}
			// add overrides
			for (AccountingCommissionOverride override : overrideList) {
				if (override.getAmountPerUnit() != null) {
					AccountingCommissionDefinition newCommission = new AccountingCommissionDefinition();
					newCommission.setExpenseAccountingAccount(override.getAccountingAccount());
					newCommission.setCommissionType(AccountingCommissionTypes.SIMPLE);
					newCommission.setCommissionApplyMethod(AccountingCommissionApplyMethods.ALWAYS);
					newCommission.setCommissionCalculationMethod(AccountingCommissionCalculationMethods.PER_UNIT);
					newCommission.setCommissionAmount(override.getAmountPerUnit());
					commissionDefinitionList.add(newCommission);
				}
			}
		}
	}


	/**
	 * @return whether the transaction is a transfer AND the client investment account of the transaction is equal to the "from" account of the transfer
	 */
	private boolean isTransferAndIsSourceAccount(AccountingTransactionDetailInfo transactionInfo) {
		if (transactionInfo.isTransfer()) {
			AccountingPositionTransfer transfer = (AccountingPositionTransfer) transactionInfo.getBookableEntity();
			if (transactionInfo.getJournalDetail().getClientInvestmentAccount().equals(transfer.getFromClientInvestmentAccount())) {
				return true;
			}
		}

		return false;
	}


	private List<AccountingCommission> calculateCommissionAmounts(AccountingMatchingCommissionDefinitionResultSet commissionDefinitionResultSet, AccountingCommissionCalculationCommand commissionCalculationCommand) {
		//Quick check to avoid unnecessary computations and lockup
		if (commissionDefinitionResultSet == null || commissionDefinitionResultSet.isNoMatchesFound()) {
			return new ArrayList<>();
		}

		//Calculate and set the notional amount
		commissionCalculationCommand.setNotionalAmount(calculateNotional(commissionCalculationCommand.getCurrentJournalDetailDefinition()));

		//Create a results map
		Map<String, AccountingCommission> amounts = new HashMap<>();
		for (AccountingCommissionDefinition commissionDefinition : CollectionUtils.getIterable(commissionDefinitionResultSet.getCommissionDefinitions())) {
			if (commissionDefinition.getCommissionApplyMethod().isCommissionCurrentlyApplicableToCurrentTransaction(commissionDefinitionResultSet.getTransactionInfo())) {
				commissionCalculationCommand.setCommissionDefinition(commissionDefinition);
				BigDecimal commissionAmount = computeCommission(commissionCalculationCommand);
				addAmountToMap(commissionAmount, commissionDefinition.getExpenseAccountingAccount(), commissionDefinition.getCommissionCurrency(), amounts);
			}
		}
		return new ArrayList<>(amounts.values());
	}


	private void addAmountToMap(BigDecimal amount, AccountingAccount commissionType, InvestmentSecurity commissionCurrency, Map<String, AccountingCommission> amounts) {
		String key = AccountingCommission.generateKey(commissionType, commissionCurrency);

		if (amounts.containsKey(key)) {
			amounts.get(key).addCommissionAmount(amount);
		}
		else {
			amounts.put(key, new AccountingCommission(commissionType, commissionCurrency, amount));
		}
	}


	/**
	 * Finds original (ignoring internal transfers) opening position for the specified transaction. If at the time of the open, commission definition
	 * delayed commission payment until closing (round-turn), then add corresponding opening commission definitions.
	 * <p>
	 * TODO This method does not handle cases where the position open is due to a transfer AND there is a round-turn commission for that transfer
	 * (i.e. the destination account gets charged for the transfer). This case is extremely rare, is fairly complicated to implement correctly, and
	 * may have performance implications.
	 * <p>
	 * If the parent transaction is obtained, the {@link AccountingCommissionCalculationCommand#setParentJournalDetailDefinition(AccountingJournalDetailDefinition)}
	 * will be called to avoid later lookup.
	 *
	 * @param commissionResultSetForCurrentTransaction the current transaction commission result set
	 * @param commissionCalculationCommand             the calculation command to set the parent transaction on if it is obtained
	 */
	private void addOpeningTransactionCommissionPaidOnClose(AccountingMatchingCommissionDefinitionResultSet commissionResultSetForCurrentTransaction, AccountingCommissionCalculationCommand commissionCalculationCommand) {
		AccountingTransactionDetailInfo currentTransactionInfo = commissionResultSetForCurrentTransaction.getTransactionInfo();
		AccountingJournalDetailDefinition currentTransaction = currentTransactionInfo.getJournalDetail();
		AccountingJournalDetailDefinition parentTransaction = getOriginalTransaction(currentTransaction);

		if (parentTransaction != null) {
			BookableEntity bookableEntityForParentTransaction = getAccountingBookingService().getBookableEntity(parentTransaction.getJournal());
			commissionCalculationCommand.setParentJournalDetailDefinition(parentTransaction);

			if (bookableEntityForParentTransaction != null) {
				AccountingTransactionType transactionTypeForParentTransaction = getAccountingTransactionTypeLocator().getAccountingTransactionType(parentTransaction, bookableEntityForParentTransaction);
				AccountingTransactionDetailInfo parentTransactionInfo = new AccountingTransactionDetailInfo(parentTransaction, bookableEntityForParentTransaction, transactionTypeForParentTransaction);
				AccountingMatchingCommissionDefinitionResultSet commissionResultSetForParentTransaction = getAccountingCommissionDefinitionSpecificityCache().getMostSpecificCommissionDefinitions(parentTransactionInfo);

				for (AccountingCommissionDefinition parentCommissionDefinition : CollectionUtils.getIterable(commissionResultSetForParentTransaction.getCommissionDefinitions())) {
					if (parentCommissionDefinition.getCommissionApplyMethod().isCommissionCurrentlyApplicableToParentTransaction(parentTransactionInfo, currentTransactionInfo)) {
						commissionResultSetForCurrentTransaction.getCommissionDefinitions().add(parentCommissionDefinition);
					}
				}
			}
		}
	}


	BigDecimal computeCommission(AccountingCommissionCalculationCommand commissionCalculationCommand) {
		AccountingCommissionDefinition commissionDefinition = commissionCalculationCommand.getCommissionDefinition();
		if (commissionCalculationCommand.getParentJournalDetailDefinition() == null && commissionDefinition.getCommissionType().isParentJournalDetailDefinitionRequiredForCalculation()) {
			commissionCalculationCommand.setParentJournalDetailDefinition(getOriginalTransaction(commissionCalculationCommand.getCurrentJournalDetailDefinition()));
		}
		BigDecimal rawCommission = commissionDefinition.getCommissionCalculationMethod().calculateCommissionAmount(commissionCalculationCommand);
		rawCommission = applyMinAndMax(rawCommission, commissionDefinition, commissionCalculationCommand);
		if (commissionCalculationCommand.getCommissionDefinition().getAdjustmentCalculatorBean() != null) {
			AccountingCommissionDefinitionAdjustmentCalculator calculatorComplex = (AccountingCommissionDefinitionAdjustmentCalculator) getSystemBeanService().getBeanInstance(commissionCalculationCommand.getCommissionDefinition().getAdjustmentCalculatorBean());
			return calculatorComplex.computeCommission(rawCommission, commissionCalculationCommand, getJsonHandler());
		}
		return rawCommission;
	}


	/**
	 * Applies the minimum or maximum as necessary
	 */
	BigDecimal applyMinAndMax(BigDecimal rawCommission, AccountingCommissionDefinition commissionDefinition, AccountingCommissionCalculationCommand commissionCalculationCommand) {
		//Apply the minimum if the raw amount is less than the calculated minimum amount
		if (!MathUtils.isNullOrZero(commissionDefinition.getCommissionMinimum()) && commissionDefinition.getMinimumCalculationMethod() != null) {
			BigDecimal minimumAmount = commissionDefinition.getMinimumCalculationMethod().applyCommissionMethod(commissionDefinition.getCommissionMinimum(), commissionCalculationCommand);
			if (MathUtils.isLessThan(rawCommission, minimumAmount)) {
				return minimumAmount;
			}
		}

		//Apply the maximum if the raw amount is greater than the calculated maximum amount
		if (!MathUtils.isNullOrZero(commissionDefinition.getCommissionMaximum()) && commissionDefinition.getMaximumCalculationMethod() != null) {
			BigDecimal maximumAmount = commissionDefinition.getMaximumCalculationMethod().applyCommissionMethod(commissionDefinition.getCommissionMaximum(), commissionCalculationCommand);
			if (MathUtils.isGreaterThan(rawCommission, maximumAmount)) {
				return maximumAmount;
			}
		}
		return rawCommission;
	}


	/**
	 * Returns the original transaction by recursively processing the parent transactions until a transaction is found that was not an internal transfer.
	 * For example, if <code>transaction</code> is a closing trade, then this method would return the trade or event that resulted in the original opening
	 * of the position.
	 *
	 * @param transaction the transaction to find the original transaction for
	 * @return the original transaction
	 */
	private AccountingJournalDetailDefinition getOriginalTransaction(AccountingJournalDetailDefinition transaction) {
		AccountingJournalDetailDefinition parentTransaction = transaction.getParentTransaction();

		//If there is no parent transaction, then 'transaction' is the original
		if (parentTransaction == null) {
			return transaction;
		}

		//If the parentTransaction is an internal transfer, then continue processing
		if (isTransfer(parentTransaction)) {
			AccountingPositionTransferDetail transferDetail = getAccountingPositionTransferService().getAccountingPositionTransferDetail(parentTransaction.getFkFieldId());
			AccountingPositionTransfer transfer = transferDetail.getPositionTransfer();
			if (transfer.getType().isInternalTransfer()) {
				return getOriginalTransaction(transferDetail.getExistingPosition());
			}
		}

		//If this point is reached, it means the parentTransaction was not an internal transfer, so return it
		return parentTransaction;
	}


	private BigDecimal calculateNotional(AccountingJournalDetailDefinition journalDetail) {
		BigDecimal notionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(journalDetail.getInvestmentSecurity(), journalDetail.getTransactionDate(), journalDetail.getSettlementDate(), false);

		return getInvestmentCalculator().calculateNotional(journalDetail.getInvestmentSecurity(), journalDetail.getPrice() == null ? BigDecimal.ONE : journalDetail.getPrice(),
				journalDetail.getQuantity() == null ? BigDecimal.ONE : journalDetail.getQuantity(), notionalMultiplier);
	}


	/**
	 * Returns the quantity to be used in commission calculation for the given journal detail and commission definition. In most cases this is simply
	 * <code>journalDetail.getQuantity()</code> but if <code>commissionDefinition.isUseOriginalQuantity()</code> is set and this is a closing transaction, then
	 * the quantity from the opening transaction is returned instead.
	 *
	 * @param currentTransaction the transaction quantity
	 * @param originalQuantity   the original transaction quantity
	 */
	private static BigDecimal getQuantity(AccountingJournalDetailDefinition currentTransaction, BigDecimal originalQuantity) {
		if (originalQuantity != null && canHaveFactorChange(currentTransaction.getInvestmentSecurity())) {
			return originalQuantity;
		}

		return currentTransaction.getQuantity();
	}


	private static boolean canHaveFactorChange(InvestmentSecurity security) {
		return security != null && security.getInstrument().getHierarchy().getFactorChangeEventType() != null;
	}


	private boolean isTransfer(AccountingJournalDetailDefinition transaction) {
		return AccountingJournalType.TRANSFER_JOURNAL.equals(transaction.getJournal().getJournalType().getName());
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public AccountingCommissionDefinitionSpecificityCache getAccountingCommissionDefinitionSpecificityCache() {
		return this.accountingCommissionDefinitionSpecificityCache;
	}


	public void setAccountingCommissionDefinitionSpecificityCache(AccountingCommissionDefinitionSpecificityCache accountingCommissionDefinitionSpecificityCache) {
		this.accountingCommissionDefinitionSpecificityCache = accountingCommissionDefinitionSpecificityCache;
	}


	public AccountingBookingService<BookableEntity> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<BookableEntity> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingTransactionTypeLocator getAccountingTransactionTypeLocator() {
		return this.accountingTransactionTypeLocator;
	}


	public void setAccountingTransactionTypeLocator(AccountingTransactionTypeLocator accountingTransactionTypeLocator) {
		this.accountingTransactionTypeLocator = accountingTransactionTypeLocator;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public JsonHandler<?> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<?> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}
}
