package com.clifton.accounting.commission.validation;


import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionTier;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;


@Component
public class AccountingCommissionValidator extends SelfRegisteringDaoValidator<AccountingCommissionDefinition> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(AccountingCommissionDefinition bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			if (bean.getInvestmentInstrument() == null && bean.getInvestmentTypeSubType() == null && bean.getInstrumentHierarchy() == null && bean.getInvestmentType() == null
					&& bean.getInvestmentSecurity() == null) {
				throw new FieldValidationException("Must select Investment Type, Investment Subtype, Instrument Hierarchy, Investment Instrument, or Investment Security");
			}

			if (bean.getMinimumCalculationMethod() == null && !MathUtils.isNullOrZero(bean.getCommissionMinimum())) {
				throw new FieldValidationException("Minimum Calculation Method must be populated when using a Minimum Commission Amount");
			}

			if (bean.getMaximumCalculationMethod() == null && !MathUtils.isNullOrZero(bean.getCommissionMaximum())) {
				throw new FieldValidationException("Maximum Calculation Method must be populated when using a Maximum Commission Amount");
			}

			//Check to make sure that the minimum value is below the maximum value
			if (bean.getMinimumCalculationMethod() != null && bean.getMaximumCalculationMethod() != null
					&& !MathUtils.isNullOrZero(bean.getCommissionMinimum()) && !MathUtils.isNullOrZero(bean.getCommissionMaximum())) {
				//Create a dummy command to use
				AccountingCommissionCalculationCommand command = new AccountingCommissionCalculationCommand(null, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
				command.setNotionalAmount(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
				BigDecimal minimumAmount = bean.getMinimumCalculationMethod().applyCommissionMethod(bean.getCommissionMinimum(), command);
				BigDecimal maximumAmount = bean.getMaximumCalculationMethod().applyCommissionMethod(bean.getCommissionMaximum(), command);
				ValidationUtils.assertFalse(MathUtils.isLessThanOrEqual(maximumAmount, minimumAmount), "Calculated Maximum Amount cannot be less than the calculated Minimum Amount");
			}


			if (bean.getStartDate() != null && bean.getEndDate() != null && DateUtils.compare(bean.getStartDate(), bean.getEndDate(), false) > 0) {
				throw new FieldValidationException("Start Date must be before End Date.");
			}

			if (!bean.getCommissionType().isCommissionAmountAllowed() && bean.getCommissionAmount() != null) {
				throw new FieldValidationException("Commission Amount is not allowed for Commission/Fee Type [" + bean.getCommissionType().name() + "].", "commissionAmount");
			}

			if (bean.getCommissionType().hasRanges()) {
				List<AccountingCommissionTier> tiers = bean.getCommissionTierList();

				if (tiers == null || tiers.size() < 2) {
					throw new FieldValidationException("Amortized/Tiered commission definitions must have at least two rates defined.");
				}

				//Sort the tiers to make validation simpler
				BeanUtils.sortWithFunctions(tiers, CollectionUtils.createList(AccountingCommissionTier::isMaxTier, AccountingCommissionTier::getThreshold), CollectionUtils.createList(true, true));

				short nullThresholdCount = 0;
				BigDecimal previousThreshold = null;
				for (AccountingCommissionTier tier : tiers) {
					BigDecimal threshold = tier.getThreshold();
					if (threshold != null && threshold.equals(previousThreshold)) {
						throw new FieldValidationException("Cannot have multiple commission rates with the same threshold value.");
					}

					if (threshold == null) {
						nullThresholdCount++;
						if (nullThresholdCount > 1) {
							throw new FieldValidationException("Cannot have more than one commission rate that has no threshold value specified.");
						}
					}

					previousThreshold = threshold;
				}
			}
			else if (bean.getCommissionTierList() != null) {
				throw new FieldValidationException("Non-Amortized/Tiered commission definitions must NOT have rates defined.");
			}
		}
	}
}
