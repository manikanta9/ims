package com.clifton.accounting.commission.calculation;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.commission.AccountingCommissionApplyMethods;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionOverride;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>AccountingCommissionCalculationProperties</code> is a set of properties used to calculate the commissions and fees
 * for a particular transaction.
 *
 * @param <T> the entity being booked
 * @author jgommels
 */
public class AccountingCommissionCalculationProperties<T extends BookableEntity> {

	/**
	 * The position detail that commissions and fees are being calculated for
	 */
	private AccountingJournalDetailDefinition positionDetail;

	/**
	 * The entity being booked
	 */
	private T bookableEntity;

	/**
	 * An optional list of commission overrides. For the given transaction, if there is a matching {@link AccountingCommissionDefinition} whose
	 * {@link AccountingAccount} is the same as a commission override in this list, then the commission/fee will be calculated using the {@link AccountingCommissionDefinition}
	 * but with the amount specified from {@link AccountingCommissionOverride#getAmountPerUnit()}. This effectively means that the commission/fee will still be calculated
	 * with settings of the commission definition (such as the {@link AccountingCommissionApplyMethods}), but the commission amount and calculation method is overridden.
	 * <p/>
	 * When there is no matching commission definition found with the same {@link AccountingAccount}, then the commission/fee is simply calculated by multiplying
	 * {@link AccountingCommissionOverride#getAmountPerUnit()} by {@link AccountingJournalDetailDefinition#getQuantity()}.
	 */
	private List<? extends AccountingCommissionOverride> overrides;

	/**
	 * An optional value which, if specified, indicates that this is the 'original' quantity associated with the transaction before
	 * a notional reduction. For example, this can be applicable to some trades of a Credit Default Swap (CDS) with an existing factor.
	 * For instance, a Sell of quantity=100 and factor=0.9 would result in an 'adjusted' quantity of 90, but the 'original' quantity would be 100.
	 */
	private BigDecimal originalQuantity;

	/**
	 * An optional list of commission definitions that can be passed to eliminate an extra lookup from the specificity cache
	 */
	private List<AccountingCommissionDefinition> commissionDefinitions;


	/**
	 * @param positionDetail the {@link #positionDetail}
	 * @param bookableEntity the {@link #bookableEntity}
	 */
	public AccountingCommissionCalculationProperties(AccountingJournalDetailDefinition positionDetail, T bookableEntity) {
		super();
		this.positionDetail = positionDetail;
		this.bookableEntity = bookableEntity;
	}


	/**
	 * @return the {@link #positionDetail}
	 */
	public AccountingJournalDetailDefinition getPositionDetail() {
		return this.positionDetail;
	}


	/**
	 * Sets {@link #positionDetail}.
	 *
	 * @param positionDetail the positionDetail to set
	 */
	public void setPositionDetail(AccountingJournalDetailDefinition positionDetail) {
		this.positionDetail = positionDetail;
	}


	/**
	 * @return the {@link #bookableEntity}
	 */
	public T getBookableEntity() {
		return this.bookableEntity;
	}


	/**
	 * Sets {@link #bookableEntity}.
	 *
	 * @param bookableEntity the bookableEntity to set
	 */
	public void setBookableEntity(T bookableEntity) {
		this.bookableEntity = bookableEntity;
	}


	/**
	 * @return the {@link #overrides}
	 */
	public List<? extends AccountingCommissionOverride> getOverrides() {
		return this.overrides;
	}


	/**
	 * Sets {@link #overrides}.
	 *
	 * @param overrides the overrides to set
	 */
	public void setOverrides(List<? extends AccountingCommissionOverride> overrides) {
		this.overrides = overrides;
	}


	/**
	 * @return the {@link #originalQuantity}
	 */
	public BigDecimal getOriginalQuantity() {
		return this.originalQuantity;
	}


	/**
	 * Sets {@link #originalQuantity}.
	 *
	 * @param originalQuantity the originalQuantity to set
	 */
	public void setOriginalQuantity(BigDecimal originalQuantity) {
		this.originalQuantity = originalQuantity;
	}


	public List<AccountingCommissionDefinition> getCommissionDefinitions() {
		return this.commissionDefinitions;
	}


	public void setCommissionDefinitions(List<AccountingCommissionDefinition> commissionDefinitions) {
		this.commissionDefinitions = commissionDefinitions;
	}
}
