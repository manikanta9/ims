package com.clifton.accounting.commission;


import com.clifton.accounting.commission.search.AccountingCommissionDefinitionSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.List;


/**
 * The <code>AccountingCommissionService</code> interface defines methods for setting up commission/fee definitions.
 *
 * @author vgomelsky
 */
public interface AccountingCommissionService {

	//////////////////////////////////////////////////////////////////////////// 
	////////         Accounting Commission Business Methods               ///////// 
	//////////////////////////////////////////////////////////////////////////// 


	/**
	 * Retrieves the {@link AccountingCommissionDefinition} by ID. If it has amortized or tiered commission rates, then
	 * the list of {@link AccountingCommissionTier}s is retrieved, sorted, and set on the bean.
	 */
	public AccountingCommissionDefinition getAccountingCommissionDefinition(int id);


	public List<AccountingCommissionDefinition> getAccountingCommissionDefinitionList(AccountingCommissionDefinitionSearchForm searchForm);


	/**
	 * Copies the commission definitions from the fromSecurity to the toSecurity
	 * Note: Copies those that are tied directly to the fromSecurity only.
	 * <p/>
	 * Used by OTC Swaps copy feature, users can optionally also copy the commission definitions
	 *
	 * @param fromSecurityId
	 * @param toSecurity
	 */
	@DoNotAddRequestMapping
	public void copyAccountingCommissionDefinitionListForSecurity(int fromSecurityId, InvestmentSecurity toSecurity);


	public AccountingCommissionDefinition saveAccountingCommissionDefinition(AccountingCommissionDefinition bean);


	/**
	 * Deletes the {@link AccountingCommissionDefinition} by ID. If it has any {@link AccountingCommissionTier}s associated with it,
	 * they are deleted first.
	 */
	public void deleteAccountingCommissionDefinition(int id);
}
