package com.clifton.accounting.commission.calculation.calculators;

import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;

import java.math.BigDecimal;


/**
 * The <code>AccountingCommissionCalculationCommand</code> is a object that contains the information necessary to calculate
 * the commission amount for an {@link AccountingCommissionDefinition}
 *
 * @author NickK
 */
public class AccountingCommissionCalculationCommand {

	private final AccountingJournalDetailDefinition currentJournalDetailDefinition;

	private final BigDecimal quantity;

	private AccountingCommissionDefinition commissionDefinition;

	private BigDecimal notionalAmount;

	private AccountingJournalDetailDefinition parentJournalDetailDefinition;


	////////////////////////////////////////////////////////////////////////////
	//////////           Command Business Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingCommissionCalculationCommand(AccountingJournalDetailDefinition currentJournalDetailDefinition, BigDecimal quantity) {
		this.currentJournalDetailDefinition = currentJournalDetailDefinition;
		this.quantity = quantity;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingCommissionDefinition getCommissionDefinition() {
		return this.commissionDefinition;
	}


	public void setCommissionDefinition(AccountingCommissionDefinition commissionDefinition) {
		this.commissionDefinition = commissionDefinition;
	}


	public AccountingJournalDetailDefinition getCurrentJournalDetailDefinition() {
		return this.currentJournalDetailDefinition;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public BigDecimal getNotionalAmount() {
		return this.notionalAmount;
	}


	public void setNotionalAmount(BigDecimal notionalAmount) {
		this.notionalAmount = notionalAmount;
	}


	public AccountingJournalDetailDefinition getParentJournalDetailDefinition() {
		return this.parentJournalDetailDefinition;
	}


	public void setParentJournalDetailDefinition(AccountingJournalDetailDefinition parentJournalDetailDefinition) {
		this.parentJournalDetailDefinition = parentJournalDetailDefinition;
	}
}
