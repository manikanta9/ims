package com.clifton.accounting.analytics;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionDataValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * <code>AbstractAccountingAnalyticsEntityDetail</code> is an abstract implementation of {@link AccountingAnalyticsEntityDetail}
 * providing common support for updating and obtaining {@link MarketDataSubscriptionDataValue}s for an entity.
 * <p>
 * The data values are stored in a {@link HashMap} and the modification is not synchronized.
 *
 * @author NickK
 */
public abstract class AbstractAccountingAnalyticsEntityDetail implements AccountingAnalyticsEntityDetail {

	/**
	 * Map of {@link MarketDataSubscriptionDataValue}s keyed by field name. This is a map so the
	 * values can be quickly updated so only the latest value is present.
	 */
	protected final Map<String, MarketDataSubscriptionDataValue> dataValueMap = new HashMap<>();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public boolean hasUpdates() {
		return getUpdatedValueList().isEmpty();
	}


	@Override
	public List<MarketDataSubscriptionDataValue> getUpdatedValueList() {
		return this.dataValueMap.values().stream()
				.filter(MarketDataSubscriptionDataValue::isUpdated)
				.collect(Collectors.toList());
	}


	@Override
	public void setDataValueList(List<MarketDataSubscriptionDataValue> dataValueList) {
		applyDataValueUpdates(() -> CollectionUtils.getStream(dataValueList));
	}


	@Override
	public void setDataValues(MarketDataSubscriptionDataValue... dataValues) {
		applyDataValueUpdates(() -> ArrayUtils.getStream(dataValues));
	}

	///////////////////////////////////////////////////////////////////////////


	private void applyDataValueUpdates(Supplier<Stream<MarketDataSubscriptionDataValue>> propertyValueStreamSupplier) {
		Stream<MarketDataSubscriptionDataValue> propertyValueStream = propertyValueStreamSupplier.get();
		if (propertyValueStream != null) {
			propertyValueStream.forEach(this::mergePropertyValueUpdate);
		}
	}


	private void mergePropertyValueUpdate(MarketDataSubscriptionDataValue propertyValue) {
		if (propertyValue != null && propertyValue.getFieldName() != null) {
			this.dataValueMap.compute(propertyValue.getFieldName(), (key, currentValue) -> {
				if (currentValue != null) {
					currentValue.setValue(propertyValue.getValue());
					return currentValue;
				}
				return propertyValue;
			});
		}
	}


	protected MarketDataSubscriptionDataValue getPropertyValue(String propertyName) {
		return this.dataValueMap.get(propertyName);
	}


	protected MarketDataSubscriptionDataValue getOrDefaultPropertyValue(String propertyName, Supplier<MarketDataSubscriptionDataValue> defaultSupplier) {
		return CollectionUtils.getValue(this.dataValueMap, propertyName, defaultSupplier);
	}


	protected Object getPropertyValueExtracted(String propertyName) {
		MarketDataSubscriptionDataValue value = getPropertyValue(propertyName);
		return value == null ? null : value.getValue();
	}
}
