package com.clifton.accounting.analytics;


import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionDataValue;

import java.util.List;


/**
 * <code>AccountingAnalyticsEntityDetail</code> represents an entity that can have data values updated and retrieved in realtime.
 *
 * @author NickK
 */
public interface AccountingAnalyticsEntityDetail {

	/**
	 * Returns true if any fields have been added or updated to this detail
	 */
	public boolean hasUpdates();


	/**
	 * Returns the list of added or updated data values for this detail
	 */
	public List<MarketDataSubscriptionDataValue> getUpdatedValueList();


	/**
	 * Atomically adds the provided list of data values to this detail
	 */
	public void setDataValueList(List<MarketDataSubscriptionDataValue> dataValueList);


	/**
	 * Atomically adds the provided array of data values to this detail
	 */
	public void setDataValues(MarketDataSubscriptionDataValue... dataValues);
}
