package com.clifton.accounting.analytics;


import java.util.Date;


//TODO: UNDER CONSTRUCTION
public class AnalyticsGroupingCommand {

	private Date startDate; // optional for snapshots
	private Date endDate;

	private String groupingColumn;
	private boolean orderByGroupingColumn;


	public String getGroupingColumn() {
		return this.groupingColumn;
	}


	public void setGroupingColumn(String groupingColumn) {
		this.groupingColumn = groupingColumn;
	}


	public boolean isOrderByGroupingColumn() {
		return this.orderByGroupingColumn;
	}


	public void setOrderByGroupingColumn(boolean orderByGroupingColumn) {
		this.orderByGroupingColumn = orderByGroupingColumn;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
