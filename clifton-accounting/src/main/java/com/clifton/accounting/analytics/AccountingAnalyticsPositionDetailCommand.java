package com.clifton.accounting.analytics;

import com.clifton.accounting.gl.search.GeneralLedgerSearchForm;
import com.clifton.core.dataaccess.search.form.SearchForm;


/**
 * <code>AccountingAnalyticsPositionDetailCommand</code> provides position filters and conditions for retrieving positions.
 *
 * @author NickK
 */
@SearchForm(hasOrmDtoClass = false)
public class AccountingAnalyticsPositionDetailCommand extends GeneralLedgerSearchForm {

	/**
	 * When true, tells the service to look up opening position commissions for calculating net cost basis of each position.
	 */
	private boolean includeOpeningCommissions;

	private boolean includeCollateralPositions;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isIncludeOpeningCommissions() {
		return this.includeOpeningCommissions;
	}


	public void setIncludeOpeningCommissions(boolean includeOpeningCommissions) {
		this.includeOpeningCommissions = includeOpeningCommissions;
	}


	public boolean isIncludeCollateralPositions() {
		return this.includeCollateralPositions;
	}


	public void setIncludeCollateralPositions(boolean includeCollateralPositions) {
		this.includeCollateralPositions = includeCollateralPositions;
	}
}
