package com.clifton.accounting.analytics;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;


/**
 * <code>AccountingAnalyticsPositionDetail</code> is a non-persistent DTO that represents a client aggregate position
 * with the ability to have realtime value updates based on market data changes.
 *
 * @author NickK
 */
@NonPersistentObject
public class AccountingAnalyticsPositionDetail extends AbstractAccountingAnalyticsEntityDetail {

	/**
	 * Key for the client's cash balance.
	 */
	protected static final String CASH_KEY = AccountingAccount.ASSET_CASH;

	protected static final String CASH_COLLATERAL_KEY = AccountingAccount.ASSET_CASH_COLLATERAL;
	/**
	 * Keys for the client's opening position commission balance in both base and local converted from the opening transaction exchange rate.
	 * If the opening position has been partially closed, the commission values are calculated from the original commission proportionally based on the remaining position.
	 */
	protected static final String BASE_COMMISSION_KEY = "Base " + AccountingAccount.EXPENSE_COMMISSION;
	protected static final String LOCAL_COMMISSION_KEY = "Local " + AccountingAccount.EXPENSE_COMMISSION;
	/**
	 * Key for the standard last traded price for a security.
	 */
	protected static final String PRICE_PROPERTY_KEY = MarketDataField.SUBSCRIPTION_EXTERNAL_FIELD_LAST_PRICE;
	/**
	 * Key for the real-time volatility for a security.
	 */
	protected static final String VOLATILITY_PROPERTY_KEY = MarketDataField.SUBSCRIPTION_EXTERNAL_FIELD_VOLATILITY;
	/**
	 * Key for a security's underlying security price. Not all securities have an underlying security.
	 */
	protected static final String UNDERLYING_PRICE_PROPERTY_KEY = "UNDERLYING_" + PRICE_PROPERTY_KEY;
	/**
	 * Key for an Option security's real-time delta value
	 */
	protected static final String DELTA_PROPERTY_KEY = MarketDataField.SUBSCRIPTION_EXTERNAL_FIELD_DELTA;
	/**
	 * The Original date of the Open Position.
	 */
	public static final String POSITION_OPEN_DATE = "Position Open Date";

	/**
	 * Aggregate of lot positions for client account, holding account, GL account, and security
	 */
	private final AccountingBalance position;

	///////////////////////////////////////////////////////////////////////////


	public AccountingAnalyticsPositionDetail(AccountingBalance position) {
		this.position = position;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AccountingBalance getAccountingBalance() {
		return this.position;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return getAccountingBalance().getClientInvestmentAccount();
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return getAccountingBalance().getHoldingInvestmentAccount();
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return getAccountingBalance().getInvestmentSecurity();
	}


	public BigDecimal getQuantity() {
		return getAccountingBalance().getQuantity();
	}


	public boolean isPending() {
		return getAccountingBalance().isPendingActivityIncluded();
	}


	public BigDecimal getActualQuantity() {
		return Optional.ofNullable(getAccountingBalance().getOriginalQuantity())
				.orElse(BigDecimal.ZERO);
	}


	public BigDecimal getPendingQuantity() {
		return isPending() ? getAccountingBalance().getPendingQuantity() : BigDecimal.ZERO;
	}


	public BigDecimal getOpeningLocalCommission() {
		return (BigDecimal) getPropertyValueExtracted(LOCAL_COMMISSION_KEY);
	}


	public BigDecimal getLocalCostBasis() {
		return getAccountingBalance().getLocalCostBasis();
	}


	public BigDecimal getActualLocalCostBasis() {
		return Optional.ofNullable(getAccountingBalance().getOriginalLocalCostBasis())
				.orElse(BigDecimal.ZERO);
	}


	public BigDecimal getNetCostBasis() {
		return MathUtils.add(getLocalCostBasis(), getOpeningLocalCommission());
	}

	public BigDecimal getCostPerShare() {
		return MathUtils.divide(MathUtils.divide(getNetCostBasis(), getQuantity(), BigDecimal.ZERO), getInvestmentSecurity().getPriceMultiplier());
	}


	public BigDecimal getActualNetCostBasis() {
		return MathUtils.add(getActualLocalCostBasis(), getOpeningLocalCommission());
	}


	public BigDecimal getActualCostPerShare() {
		return MathUtils.divide(MathUtils.divide(getActualNetCostBasis(), getActualQuantity(), BigDecimal.ZERO), getInvestmentSecurity().getPriceMultiplier());
	}


	public BigDecimal getPrice() {
		return (BigDecimal) getPropertyValueExtracted(PRICE_PROPERTY_KEY);
	}


	public BigDecimal getVolatility() {
		return (BigDecimal) getPropertyValueExtracted(VOLATILITY_PROPERTY_KEY);
	}


	public BigDecimal getTotalValue() {
		return MathUtils.multiply(getPriceCalculated(), getQuantity());
	}


	public BigDecimal getProfitAndLoss() {
		return MathUtils.multiply(MathUtils.multiply(getProfitAndLossPerShare(), getInvestmentSecurity().getPriceMultiplier()), MathUtils.abs(getQuantity()));
	}


	public BigDecimal getProfitAndLossPerShare() {
		return MathUtils.isGreaterThan(getQuantity(), BigDecimal.ZERO) ? MathUtils.subtract(getPrice(), getCostPerShare()) : MathUtils.subtract(getCostPerShare(), getPrice());
	}


	public BigDecimal getPercentRealized() {
		return CoreMathUtils.getPercentValue(getProfitAndLossPerShare(), getCostPerShare(), true);
	}


	public BigDecimal getUnderlyingPrice() {
		return (BigDecimal) getPropertyValueExtracted(UNDERLYING_PRICE_PROPERTY_KEY);
	}


	public BigDecimal getCashBalance() {
		return (BigDecimal) getPropertyValueExtracted(AccountingAccount.ASSET_CASH);
	}


	public BigDecimal getCashCollateralBalance() {
		return (BigDecimal) getPropertyValueExtracted((AccountingAccount.ASSET_CASH_COLLATERAL));
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////           Option Related Getters              ///////////////
	///////////////////////////////////////////////////////////////////////////


	public BigDecimal getStrikePrice() {
		return getInvestmentSecurity().getOptionStrikePrice();
	}


	public boolean isCall() {
		return getInvestmentSecurity().isCallOption();
	}


	public InvestmentSecurityOptionTypes getOptionType() {
		return getInvestmentSecurity().getOptionType();
	}


	public OptionStyleTypes getOptionStyle() {
		return getInvestmentSecurity().getOptionStyle();
	}


	public BigDecimal getDelta() {
		return (BigDecimal) getPropertyValueExtracted(DELTA_PROPERTY_KEY);
	}


	public BigDecimal getMoneyness() {
		return isCall() ? MathUtils.subtract(getUnderlyingPrice(), getStrikePrice()) : MathUtils.subtract(getStrikePrice(), getUnderlyingPrice());
	}


	public BigDecimal getPercentExtrinsic() {
		return CoreMathUtils.getPercentValue(MathUtils.subtract(getPrice(), getMoneyness()), getPrice(), true);
	}


	public BigDecimal getPercentInTheMoney() {
		return CoreMathUtils.getPercentValue(getMoneyness(), getUnderlyingPrice(), true);
	}


	public Date getPositionOpenDate() {
		return (Date) getPropertyValueExtracted(POSITION_OPEN_DATE);
	}


	public Integer getDaysToExpiration() {
		return getInvestmentSecurity().getLastDeliveryOrEndDate() == null ? null : DateUtils.getDaysDifference(getInvestmentSecurity().getLastDeliveryOrEndDate(), new Date());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the result of the price multiplied by the security price multiplier.
	 */
	private BigDecimal getPriceCalculated() {
		return MathUtils.multiply(getPrice(), getInvestmentSecurity().getPriceMultiplier());
	}
}
