package com.clifton.accounting.analytics;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


/**
 * <code>AccountingAnalyticsService</code> is a service for providing realtime analytics for accounting objects.
 */
public interface AccountingAnalyticsService {

	// TODO under construction
	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public List<AnalyticsGrouping> getAccountingAnalyticsGroupingForAUM(AnalyticsGroupingCommand command);

	///////////////////////////////////////////////////////////////////////////
	/////////            Position Analytics Methods                 ///////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of {@link AccountingAnalyticsPositionDetail}s resulting from the position list found for the provided {@link AccountingAnalyticsPositionDetailCommand}.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingAnalyticsPositionDetail> getAccountingAnalyticsPositionDetailList(AccountingAnalyticsPositionDetailCommand positionDetailCommand);
}
