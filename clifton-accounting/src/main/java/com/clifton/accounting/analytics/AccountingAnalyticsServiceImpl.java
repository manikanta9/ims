package com.clifton.accounting.analytics;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.AccountingBalanceUtils;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionDataValue;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


/**
 *
 */
@Service
public class AccountingAnalyticsServiceImpl implements AccountingAnalyticsService {

	private DataTableRetrievalHandler dataTableRetrievalHandler;

	private AccountingBalanceService accountingBalanceService;
	private AccountingPositionService accountingPositionService;

	private MarketDataRetriever marketDataRetriever;

	///////////////////////////////////////////////////////////////////////////


	// TODO: move common API under com.clifton.core.dataaccess.analytics ???
	//  objects, interfaces, retrieval service (don't web expose it)?
	@Override
	public List<AnalyticsGrouping> getAccountingAnalyticsGroupingForAUM(@SuppressWarnings("unused") AnalyticsGroupingCommand command) {
		String sql = "SELECT s.BusinessServiceID \"GroupingID\", s.ServiceName \"GroupingName\", SUM(c.PeriodEndAUM) \"Value\" ";
		sql += "FROM AccountingPeriodClosing c ";
		sql += "INNER JOIN AccountingPeriod p ON c.AccountingPeriodID = p.AccountingPeriodID ";
		sql += "INNER JOIN InvestmentAccount a ON c.InvestmentAccountID = a.InvestmentAccountID ";
		sql += "INNER JOIN BusinessService s ON a.BusinessServiceID = s.BusinessServiceID ";
		sql += "WHERE p.EndDate = '9/30/2012' AND c.PeriodEndAUM IS NOT NULL ";
		sql += "GROUP BY s.BusinessServiceID, s.ServiceName ";
		sql += "HAVING SUM(c.PeriodEndAUM) <> 0 ";
		sql += "ORDER BY 2";

		DataTable dataTable = getDataTableRetrievalHandler().findDataTable(sql);
		List<AnalyticsGrouping> result = new ArrayList<>();
		for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
			DataRow row = dataTable.getRow(i);
			AnalyticsGrouping value = new AnalyticsGrouping();
			value.setId((Integer) row.getValue(0));
			value.setName((String) row.getValue(1));
			value.setValue((BigDecimal) row.getValue(2));
			result.add(value);
		}

		return result;
	}

	///////////////////////////////////////////////////////////////////////////
	/////////            Position Analytics Methods                 ///////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public List<AccountingAnalyticsPositionDetail> getAccountingAnalyticsPositionDetailList(AccountingAnalyticsPositionDetailCommand positionDetailCommand) {
		prepareAccountingBalanceSearchForm(positionDetailCommand);
		List<AccountingPosition> lotPositionList = getAccountingPositionList(positionDetailCommand);
		if (CollectionUtils.isEmpty(lotPositionList)) {
			return Collections.emptyList();
		}

		List<AccountingBalance> balanceList = AccountingBalanceUtils.convertAndGroupAccountingPositionListIntoBalanceList(lotPositionList, true);
		Map<String, BigDecimal[]> accountingCommissionBalanceMap = positionDetailCommand.isIncludeOpeningCommissions() ? getAccountingCommissionBalanceMap(lotPositionList) : Collections.emptyMap();

		Map<Integer, BigDecimal> clientAccountCollateralCashBalanceMap = positionDetailCommand.isIncludeCollateralPositions() ? getAccountingCashBalanceList(balanceList, positionDetailCommand, AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_COLLATERAL_ACCOUNTS) : null;
		Map<Integer, BigDecimal> clientAccountCashBalanceMap = getAccountingCashBalanceList(balanceList, positionDetailCommand, AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS_EXCLUDE_COLLATERAL);

		Map<String, Date> positionOpenDateMap = getPositionOpenDate(lotPositionList);

		AccountingAnalyticsPositionConfig accountingAnalyticsPositionConfig = new AccountingAnalyticsPositionConfig(clientAccountCashBalanceMap, clientAccountCollateralCashBalanceMap, accountingCommissionBalanceMap, positionDetailCommand.getTransactionOrSettlementDate(), positionOpenDateMap);

		return convertPositionBalanceListToAnalyticsPositionDetailList(balanceList, accountingAnalyticsPositionConfig);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////           Helper Methods               ////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void prepareAccountingBalanceSearchForm(AccountingAnalyticsPositionDetailCommand positionDetailCommand) {
		if (positionDetailCommand.getTransactionOrSettlementDate() == null) {
			positionDetailCommand.setTransactionDate(new Date());
		}
	}


	/**
	 * Returns a map of Trade Open Dates keyed on the overall position
	 *
	 * @param positionList
	 */
	private Map<String, Date> getPositionOpenDate(List<AccountingPosition> positionList) {
		return CollectionUtils.getStream(positionList)
				.collect(Collectors.toMap(
						AccountingBalanceUtils::getBalanceKey,
						AccountingPosition::getOriginalTransactionDate,
						(current, newValue) -> DateUtils.isDateBefore(newValue, current, false) ? newValue : current));
	}


	/**
	 * Returns a list of {@link AccountingPosition}s according to the provided {@link AccountingAnalyticsPositionDetailCommand} criteria.
	 * The returned list will include merged pending activity.
	 */
	private List<AccountingPosition> getAccountingPositionList(AccountingAnalyticsPositionDetailCommand positionDetailCommand) {
		AccountingPositionCommand accountingPositionCommand = AccountingPositionCommand.onPositionTransactionDate(positionDetailCommand.getTransactionOrSettlementDate());
		BeanUtils.copyProperties(positionDetailCommand, accountingPositionCommand);

		if (!positionDetailCommand.isIncludeCollateralPositions()) {
			accountingPositionCommand.setCollateral(Boolean.FALSE);
		}

		SearchFormUtils.convertSearchRestrictionList(accountingPositionCommand);
		accountingPositionCommand.setExcludeReceivableGLAccounts(Boolean.TRUE);
		accountingPositionCommand.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_PREVIEW);

		return getAccountingPositionService().getAccountingPositionListUsingCommand(accountingPositionCommand);
	}


	/**
	 * Returns a map of commission balances (both base and local) for the provided position list. The commissions are adjusted to
	 * proportionally reflect each position's remaining cost basis.
	 * <p>
	 * The map contains a key generated with {@link AccountingBalanceUtils#getBalanceKey(AccountingObjectInfo)} and the value
	 * is a balance array consisting of the base value and local value respectfully.
	 */
	private Map<String, BigDecimal[]> getAccountingCommissionBalanceMap(List<AccountingPosition> positionList) {
		Long[] openingTransactionIds = positionList.stream()
				.map(position -> position.getOpeningTransaction().getId())
				.distinct()
				.toArray(Long[]::new);

		AccountingBalanceCommand command = AccountingBalanceCommand.forParentTransactionIdsAndAccountingAccountId(openingTransactionIds, AccountingAccountIdsCacheImpl.AccountingAccountIds.COMMISSION_ACCOUNTS);

		Map<Long, BigDecimal> positionBaseCommissionMap = getAccountingBalanceService().getAccountingChildBalanceMap(command);
		Map<String, BigDecimal[]> positionCommissionMap = new HashMap<>();
		for (AccountingPosition position : positionList) {
			AccountingJournalDetailDefinition openingTransaction = position.getOpeningTransaction();
			BigDecimal baseCommission = positionBaseCommissionMap.get(openingTransaction.getId());
			if (baseCommission != null) {
				// The commission applies to the opening transaction(s) of the position. If the position is partially closed, the position's remaining
				// cost basis serves as a percentage of the original position remaining. We need to adjust the commission accordingly to get an accurate net cost basis
				BigDecimal remainingBaseCommission;
				if (MathUtils.isEqual(openingTransaction.getPositionCostBasis(), position.getRemainingCostBasis())) {
					remainingBaseCommission = baseCommission;
				}
				else {
					BigDecimal positionRemainingPercent = MathUtils.divide(position.getRemainingCostBasis(), openingTransaction.getPositionCostBasis());
					remainingBaseCommission = MathUtils.multiply(baseCommission, positionRemainingPercent, baseCommission.scale());
				}
				BigDecimal remainingLocalCommission = InvestmentCalculatorUtils.calculateLocalAmount(remainingBaseCommission, openingTransaction.getExchangeRateToBase(), openingTransaction.getInvestmentSecurity());
				positionCommissionMap.compute(AccountingBalanceUtils.getBalanceKey(position.getOpeningTransaction()),
						(key, currentBalanceArray) -> {
							if (currentBalanceArray == null) {
								return new BigDecimal[]{remainingBaseCommission, remainingLocalCommission};
							}
							currentBalanceArray[0] = MathUtils.add(currentBalanceArray[0], remainingBaseCommission);
							currentBalanceArray[1] = MathUtils.add(currentBalanceArray[1], remainingLocalCommission);
							return currentBalanceArray;
						});
			}
		}
		return positionCommissionMap;
	}


	/**
	 * Returns a map of cash balances keyed by client {@link com.clifton.investment.account.InvestmentAccount} ID
	 */
	private Map<Integer, BigDecimal> getAccountingCashBalanceList(List<AccountingBalance> positionList, AccountingAnalyticsPositionDetailCommand positionDetailCommand, AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIds) {
		Set<Integer> clientAccountIdSet = positionList.stream()
				.map(position -> position.getClientInvestmentAccount().getId())
				.collect(Collectors.toSet());

		Integer clientInvestmentAccountGroupId = null;
		if (CollectionUtils.isEmpty(clientAccountIdSet)) {
			// No client account IDs, check for client account group
			clientInvestmentAccountGroupId = (Integer) positionDetailCommand.getSearchRestrictionValue("clientInvestmentAccountGroupId");
			if (clientInvestmentAccountGroupId == null) {
				return Collections.emptyMap();
			}
		}
		Date transactionDate = positionDetailCommand.getTransactionOrSettlementDate() == null ? new Date() : positionDetailCommand.getTransactionOrSettlementDate();

		AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountsOnTransactionDate(clientAccountIdSet.toArray(new Integer[0]), transactionDate)
				.withClientAccountGroupId(clientInvestmentAccountGroupId)
				.withAccountingAccountIdName(accountingAccountIds);

		return getAccountingBalanceService().getAccountingCashBalanceMap(command);
	}


	/**
	 * Returns a list of populated {@link AccountingAnalyticsPositionDetail}s for the provided list of {@link AccountingBalance}s.
	 */
	private List<AccountingAnalyticsPositionDetail> convertPositionBalanceListToAnalyticsPositionDetailList(List<AccountingBalance> positionList, AccountingAnalyticsPositionConfig accountingAnalyticsPositionConfig) {
		return positionList.stream()
				.map(position -> getAnalyticsPositionDetailForBalance(position, accountingAnalyticsPositionConfig))
				.collect(Collectors.toCollection(() -> {
					if (positionList instanceof PagingArrayList) {
						PagingArrayList<AccountingBalance> pagingBalanceList = (PagingArrayList<AccountingBalance>) positionList;
						return new PagingArrayList<>(new ArrayList<>(), pagingBalanceList.getFirstElementIndex(), pagingBalanceList.getTotalElementCount(), pagingBalanceList.getPageSize());
					}
					return new PagingArrayList<>();
				}));
	}


	/**
	 * Returns an {@link AccountingAnalyticsPositionDetail} with data values populated with the most recent
	 * applicable flexible values in the system for the position's {@link InvestmentSecurity}.
	 */
	@Transactional(readOnly = true)
	protected AccountingAnalyticsPositionDetail getAnalyticsPositionDetailForBalance(AccountingBalance position, AccountingAnalyticsPositionConfig accountingAnalyticsPositionConfig) {
		AccountingAnalyticsPositionDetail detail = new AccountingAnalyticsPositionDetail(position);
		List<MarketDataSubscriptionDataValue> valueList = new ArrayList<>();

		BigDecimal cashBalance = accountingAnalyticsPositionConfig.getClientAccountCashBalanceMap().computeIfAbsent(position.getClientInvestmentAccount().getId(),
				// Client cash balance should be in the map, but look up in event it is missing
				clientAccountId -> {
					AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountOnTransactionDate(clientAccountId, accountingAnalyticsPositionConfig.getBalanceDate())
							.withExcludeCashCollateral(true)
							.withNetOnly(true);
					return getAccountingBalanceService().getAccountingBalance(command);
				});
		valueList.add(new MarketDataSubscriptionDataValue(AccountingAnalyticsPositionDetail.CASH_KEY, DataTypeNames.DECIMAL, cashBalance));

		if (accountingAnalyticsPositionConfig.getClientAccountCollateralCashBalanceMap() != null) {
			BigDecimal collateralCashBalance = accountingAnalyticsPositionConfig.getClientAccountCollateralCashBalanceMap().computeIfAbsent(position.getClientInvestmentAccount().getId(),
					// Client cash collateral balance should be in the map, but look up in event it is missing
					clientAccountId -> {
						AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountOnTransactionDate(clientAccountId, accountingAnalyticsPositionConfig.getBalanceDate())
								.withAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_COLLATERAL_ACCOUNTS);
						return getAccountingBalanceService().getAccountingBalance(command);
					});
			valueList.add(new MarketDataSubscriptionDataValue(AccountingAnalyticsPositionDetail.CASH_COLLATERAL_KEY, DataTypeNames.DECIMAL, collateralCashBalance));
		}

		Optional.ofNullable(accountingAnalyticsPositionConfig.getAccountingCommissionBalanceMap().get(AccountingBalanceUtils.getBalanceKey(position)))
				.ifPresent(commissionBalance -> {
					valueList.add(new MarketDataSubscriptionDataValue(AccountingAnalyticsPositionDetail.BASE_COMMISSION_KEY, DataTypeNames.DECIMAL, commissionBalance[0]));
					valueList.add(new MarketDataSubscriptionDataValue(AccountingAnalyticsPositionDetail.LOCAL_COMMISSION_KEY, DataTypeNames.DECIMAL, commissionBalance[1]));
				});

		valueList.add(new MarketDataSubscriptionDataValue(AccountingAnalyticsPositionDetail.PRICE_PROPERTY_KEY, DataTypeNames.DECIMAL, getMarketDataRetriever().getPriceFlexible(detail.getInvestmentSecurity(), accountingAnalyticsPositionConfig.getBalanceDate(), false)));
		if (position.getInvestmentSecurity().getUnderlyingSecurity() != null) {
			valueList.add(new MarketDataSubscriptionDataValue(AccountingAnalyticsPositionDetail.UNDERLYING_PRICE_PROPERTY_KEY, DataTypeNames.DECIMAL, getMarketDataRetriever().getPriceFlexible(detail.getInvestmentSecurity().getUnderlyingSecurity(), accountingAnalyticsPositionConfig.getBalanceDate(), false)));
		}
		if (InvestmentUtils.isSecurityOfType(detail.getInvestmentSecurity(), InvestmentType.OPTIONS)) {
			valueList.add(new MarketDataSubscriptionDataValue(AccountingAnalyticsPositionDetail.DELTA_PROPERTY_KEY, DataTypeNames.DECIMAL, getMarketDataRetriever().getDeltaFlexible(detail.getInvestmentSecurity(), accountingAnalyticsPositionConfig.getBalanceDate(), false)));
		}

		Date positionOpenDate = accountingAnalyticsPositionConfig.getPositionOpenDateMap().get(AccountingBalanceUtils.getBalanceKey(position));
		if (positionOpenDate != null) {
			valueList.add(new MarketDataSubscriptionDataValue(AccountingAnalyticsPositionDetail.POSITION_OPEN_DATE, DataTypeNames.DATE, positionOpenDate));
		}

		detail.setDataValueList(valueList);
		return detail;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	private static final class AccountingAnalyticsPositionConfig {

		private final Map<Integer, BigDecimal> clientAccountCashBalanceMap;
		private final Map<Integer, BigDecimal> clientAccountCollateralCashBalanceMap;
		private final Map<String, BigDecimal[]> accountingCommissionBalanceMap;
		private final Date balanceDate;
		private final Map<String, Date> positionOpenDateMap;


		AccountingAnalyticsPositionConfig(Map<Integer, BigDecimal> clientAccountCashBalanceMap,
		                                  Map<Integer, BigDecimal> clientAccountCollateralCashBalanceMap,
		                                  Map<String, BigDecimal[]> accountingCommissionBalanceMap,
		                                  Date balanceDate,
		                                  Map<String, Date> positionOpenDateMap) {
			this.clientAccountCashBalanceMap = clientAccountCashBalanceMap;
			this.clientAccountCollateralCashBalanceMap = clientAccountCollateralCashBalanceMap;
			this.accountingCommissionBalanceMap = accountingCommissionBalanceMap;
			this.balanceDate = balanceDate;
			this.positionOpenDateMap = positionOpenDateMap;
		}


		public Map<Integer, BigDecimal> getClientAccountCashBalanceMap() {
			return this.clientAccountCashBalanceMap;
		}


		public Map<Integer, BigDecimal> getClientAccountCollateralCashBalanceMap() {
			return this.clientAccountCollateralCashBalanceMap;
		}


		public Map<String, BigDecimal[]> getAccountingCommissionBalanceMap() {
			return this.accountingCommissionBalanceMap;
		}


		public Date getBalanceDate() {
			return this.balanceDate;
		}


		public Map<String, Date> getPositionOpenDateMap() {
			return this.positionOpenDateMap;
		}
	}
}
