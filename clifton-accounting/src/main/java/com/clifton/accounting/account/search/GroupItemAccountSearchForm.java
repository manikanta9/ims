package com.clifton.accounting.account.search;


import com.clifton.accounting.account.AccountingAccountGroupItemAccountingAccount;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


@SearchForm(ormDtoClass = AccountingAccountGroupItemAccountingAccount.class)
public class GroupItemAccountSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchFieldPath = "referenceOne", searchField = "group.id")
	private Short groupId;

	@SearchField(searchField = "referenceOne.id")
	private Integer groupItemId;

	@SearchField(searchField = "referenceTwo.id")
	private Short accountId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public Integer getGroupItemId() {
		return this.groupItemId;
	}


	public void setGroupItemId(Integer groupItemId) {
		this.groupItemId = groupItemId;
	}


	public Short getAccountId() {
		return this.accountId;
	}


	public void setAccountId(Short accountId) {
		this.accountId = accountId;
	}
}
