package com.clifton.accounting.account.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class AccountingAccountSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "accountType.id")
	private Short accountTypeId;

	@SearchField(searchField = "name", searchFieldPath = "accountType")
	private String accountType;

	@SearchField(searchField = "name", searchFieldPath = "accountType", comparisonConditions = ComparisonConditions.EQUALS)
	private String accountTypeEquals;

	@SearchField(searchField = "groupItemList.referenceOne.group.name", comparisonConditions = ComparisonConditions.EXISTS)
	private String accountGroupName;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short accountGroupId;

	// Custom Search Field that limits accounts to those not in this group
	private Short excludedFromAccountGroupId;

	@SearchField(searchField = "balanceSheetAccount", searchFieldPath = "accountType")
	private Boolean balanceSheetAccount;

	@SearchField(searchField = "growingOnDebitSide", searchFieldPath = "accountType")
	private Boolean growingOnDebitSide;

	@SearchField
	private Boolean position;

	@SearchField
	private Boolean currency;

	@SearchField
	private Boolean cash;

	@SearchField
	private Boolean collateral;

	@SearchField
	private Boolean commission;

	@SearchField
	private Boolean fee;

	@SearchField
	private Boolean gainLoss;

	@SearchField
	private Boolean unrealizedGainLoss;

	@SearchField
	private Boolean currencyTranslation;

	@SearchField
	private Boolean receivable;

	// Custom Search Field
	private Boolean excludeReceivableCollateral;

	@SearchField
	private Boolean executingBrokerSpecific;

	@SearchField
	private Boolean notOurAccount;

	@SearchField
	private Boolean postingNotAllowed;

	@SearchField(searchField = "autoAccrualReversalOffsetAccount", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean autoReverseAccrualAccountingAccount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getPosition() {
		return this.position;
	}


	public void setPosition(Boolean position) {
		this.position = position;
	}


	public Boolean getCash() {
		return this.cash;
	}


	public void setCash(Boolean cash) {
		this.cash = cash;
	}


	public Short getAccountTypeId() {
		return this.accountTypeId;
	}


	public void setAccountTypeId(Short accountTypeId) {
		this.accountTypeId = accountTypeId;
	}


	public String getAccountType() {
		return this.accountType;
	}


	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	public String getAccountTypeEquals() {
		return this.accountTypeEquals;
	}


	public void setAccountTypeEquals(String accountTypeEquals) {
		this.accountTypeEquals = accountTypeEquals;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getBalanceSheetAccount() {
		return this.balanceSheetAccount;
	}


	public void setBalanceSheetAccount(Boolean balanceSheetAccount) {
		this.balanceSheetAccount = balanceSheetAccount;
	}


	public Boolean getUnrealizedGainLoss() {
		return this.unrealizedGainLoss;
	}


	public void setUnrealizedGainLoss(Boolean unrealizedGainLoss) {
		this.unrealizedGainLoss = unrealizedGainLoss;
	}


	public Boolean getGrowingOnDebitSide() {
		return this.growingOnDebitSide;
	}


	public void setGrowingOnDebitSide(Boolean growingOnDebitSide) {
		this.growingOnDebitSide = growingOnDebitSide;
	}


	public Boolean getCollateral() {
		return this.collateral;
	}


	public void setCollateral(Boolean collateral) {
		this.collateral = collateral;
	}


	public Boolean getCommission() {
		return this.commission;
	}


	public void setCommission(Boolean commission) {
		this.commission = commission;
	}


	public Boolean getFee() {
		return this.fee;
	}


	public void setFee(Boolean fee) {
		this.fee = fee;
	}


	public Boolean getGainLoss() {
		return this.gainLoss;
	}


	public void setGainLoss(Boolean gainLoss) {
		this.gainLoss = gainLoss;
	}


	public Boolean getCurrencyTranslation() {
		return this.currencyTranslation;
	}


	public void setCurrencyTranslation(Boolean currencyTranslation) {
		this.currencyTranslation = currencyTranslation;
	}


	public Boolean getReceivable() {
		return this.receivable;
	}


	public void setReceivable(Boolean receivable) {
		this.receivable = receivable;
	}


	public Boolean getPostingNotAllowed() {
		return this.postingNotAllowed;
	}


	public void setPostingNotAllowed(Boolean postingNotAllowed) {
		this.postingNotAllowed = postingNotAllowed;
	}


	public Boolean getAutoReverseAccrualAccountingAccount() {
		return this.autoReverseAccrualAccountingAccount;
	}


	public void setAutoReverseAccrualAccountingAccount(Boolean autoReverseAccrualAccountingAccount) {
		this.autoReverseAccrualAccountingAccount = autoReverseAccrualAccountingAccount;
	}


	public Boolean getCurrency() {
		return this.currency;
	}


	public void setCurrency(Boolean currency) {
		this.currency = currency;
	}


	public String getAccountGroupName() {
		return this.accountGroupName;
	}


	public void setAccountGroupName(String accountGroupName) {
		this.accountGroupName = accountGroupName;
	}


	public Boolean getExcludeReceivableCollateral() {
		return this.excludeReceivableCollateral;
	}


	public void setExcludeReceivableCollateral(Boolean excludeReceivableCollateral) {
		this.excludeReceivableCollateral = excludeReceivableCollateral;
	}


	public Boolean getNotOurAccount() {
		return this.notOurAccount;
	}


	public void setNotOurAccount(Boolean notOurAccount) {
		this.notOurAccount = notOurAccount;
	}


	public Short getAccountGroupId() {
		return this.accountGroupId;
	}


	public void setAccountGroupId(Short accountGroupId) {
		this.accountGroupId = accountGroupId;
	}


	public Boolean getExecutingBrokerSpecific() {
		return this.executingBrokerSpecific;
	}


	public void setExecutingBrokerSpecific(Boolean executingBrokerSpecific) {
		this.executingBrokerSpecific = executingBrokerSpecific;
	}


	public Short getExcludedFromAccountGroupId() {
		return this.excludedFromAccountGroupId;
	}


	public void setExcludedFromAccountGroupId(Short excludedFromAccountGroupId) {
		this.excludedFromAccountGroupId = excludedFromAccountGroupId;
	}
}
