package com.clifton.accounting.account;


import com.clifton.core.beans.ManyToManyEntity;


/**
 * The <code>AccountingAccountGroupItemAccount</code> class assigns a specific GL Account to a specific
 * account group item.
 *
 * @author Mary Anderson
 */
public class AccountingAccountGroupItemAccountingAccount extends ManyToManyEntity<AccountingAccountGroupItem, AccountingAccount, Integer> {

	/**
	 * Identifies whether (Credit - Debit) or (Debit - Credit) convention is used for this GL Account.
	 * Can be used to reverse the sign of (Debit - Credit) when aggregating entries of different GL Account types.
	 */
	private boolean creditMinusDebit;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCreditMinusDebit() {
		return this.creditMinusDebit;
	}


	public void setCreditMinusDebit(boolean creditMinusDebit) {
		this.creditMinusDebit = creditMinusDebit;
	}
}
