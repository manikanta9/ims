package com.clifton.accounting.account.cache;


import com.clifton.accounting.account.AccountingAccountGroupItemAccountingAccount;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>AccountingAccountIdListByGroupCacheImpl</code> caches a list of {@link com.clifton.accounting.account.AccountingAccount} ids for a given {@link com.clifton.accounting.account.AccountingAccountGroup} id
 * This cache observes the {@link com.clifton.accounting.account.AccountingAccountGroupItemAccountingAccount} for updates to clear the cache for the group when assignments change
 *
 * @author manderson
 */
@Component
public class AccountingAccountIdListByGroupCacheImpl extends SelfRegisteringDaoObserver<AccountingAccountGroupItemAccountingAccount> implements CustomCache<Short, List<Short>>, AccountingAccountIdListByGroupCache {

	private CacheHandler<Short, List<Short>> cacheHandler;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Short> getAccountingAccountIdList(Short groupId) {
		return getCacheHandler().get(getCacheName(), groupId);
	}


	@Override
	public void storeAccountingAccountIdList(Short groupId, List<Short> accountingAccountIdList) {
		getCacheHandler().put(getCacheName(), groupId, accountingAccountIdList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<AccountingAccountGroupItemAccountingAccount> dao, DaoEventTypes event, AccountingAccountGroupItemAccountingAccount bean, Throwable e) {
		if (e == null) {
			Short key = getBeanKey(bean);
			if (key != null) {
				getCacheHandler().remove(getCacheName(), key);
			}
			if (event.isUpdate()) {
				Short originalKey = getBeanKey(getOriginalBean(dao, bean));
				if (originalKey != null && !originalKey.equals(key)) {
					getCacheHandler().remove(getCacheName(), originalKey);
				}
			}
		}
	}


	private Short getBeanKey(AccountingAccountGroupItemAccountingAccount groupItemAccountingAccount) {
		if (groupItemAccountingAccount != null && groupItemAccountingAccount.getReferenceOne() != null && groupItemAccountingAccount.getReferenceOne().getGroup() != null) {
			return groupItemAccountingAccount.getReferenceOne().getGroup().getId();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<Short, List<Short>> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Short, List<Short>> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
