package com.clifton.accounting.account.cache;

import com.clifton.accounting.account.AccountingAccount;

import java.util.List;


/**
 * @author manderson
 */
public interface AccountingAccountIdsCache {

	/**
	 * Returns a list of fully populated AccountingAccounts from the Hibernate Cache
	 */
	public List<AccountingAccount> getAccountingAccountList(AccountingAccountIdsCacheImpl.AccountingAccountIds accountsKey);


	/**
	 * Returns an array of AccountingAccount id's for the specified key. Use constants on this class for available key names.
	 * NOTE: use to improve performance by avoiding extra joins on large tables (AccountingTransaction, etc.)
	 */
	public Short[] getAccountingAccounts(AccountingAccountIdsCacheImpl.AccountingAccountIds accountsKey);


	/**
	 * Returns an array of AccountingAccount id's for the Accounting Account Type.
	 * NOTE: use to improve performance by avoiding extra joins on large tables (AccountingTransaction, etc.)
	 */
	public Short[] getAccountingAccountsByAccountType(String accountType);
}
