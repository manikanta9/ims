package com.clifton.accounting.account;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>AccountingAccount</code> class represents a GL account DTO.
 *
 * @author vgomelsky
 */
@CacheByName
public class AccountingAccount extends NamedEntity<Short> {

	///////////////////////////
	//////   Asset      //////
	/////////////////////////
	// standard GL Accounts referenced by name in various parts of the system
	// NOTE: never reverence a GL Account by name directly; always use corresponding constant or even better corresponding boolean attribute(s)
	public static final String ASSET_CASH = "Cash";
	public static final String ASSET_CASH_COLLATERAL = "Cash Collateral";
	public static final String ASSET_MONEY_MARKET = "Money Market";

	public static final String ASSET_CURRENCY = "Currency";
	public static final String ASSET_CURRENCY_COLLATERAL = "Currency Collateral";
	public static final String ASSET_POSITION = "Position";
	public static final String ASSET_POSITION_COLLATERAL = "Position Collateral";
	public static final String ASSET_POSITION_RECEIVABLE = "Position Receivable";

	public static final String ASSET_DIVIDEND_RECEIVABLE = "Dividend Receivable";
	public static final String ASSET_INTEREST_RECEIVABLE = "Interest Receivable";
	public static final String ASSET_PAYMENT_RECEIVABLE = "Payment Receivable";

	public static final String ASSET_FRANKING_CREDIT = "Franking Credits";

	///////////////////////////
	//////   Equity     //////
	/////////////////////////
	public static final String EQUITY_CONTRIBUTION = "Contribution"; // cash contribution
	public static final String EQUITY_DISTRIBUTION = "Distribution"; // cash distribution
	public static final String EQUITY_SECURITY_CONTRIBUTION = "Security Contribution"; // security position contribution (bond, etc.)
	public static final String EQUITY_SECURITY_DISTRIBUTION = "Security Distribution"; // security position distribution (bond, etc.)

	///////////////////////////
	//////   Expense    //////
	/////////////////////////
	public static final String EXPENSE_CLEARING_ACTIVITY_FEE = "Clearing Activity Fee";
	public static final String EXPENSE_COLLATERAL_USAGE_FEE = "Collateral Usage Fee";
	public static final String EXPENSE_COMMISSION = "Commission";
	public static final String EXECUTING_COMMISSION_EXPENSE = "Executing Commission";
	public static final String CLEARING_COMMISSION_EXPENSE = "Clearing Commission";
	public static final String EXCHANGE_FEE_EXPENSE = "Exchange Fee";
	public static final String EXPENSE_INTEREST_EXPENSE = "Interest Expense";
	public static final String REPO_EXPENSE_INTEREST_EXPENSE = "REPO Interest Expense";
	public static final String EXPENSE_MANAGEMENT_FEE = "Management Fee";
	public static final String EXPENSE_NFA_FEES = "NFA Fees";
	public static final String EXPENSE_PAI = "PAI Expense";
	public static final String EXPENSE_PRINCIPAL_LOSSES = "Principal Losses"; // default on a bond, etc.
	public static final String EXPENSE_SEC_FEES = "SEC Fees";
	public static final String EXPENSE_TERMINATION_FEE = "Termination Fee";
	public static final String EXPENSE_TRANSACTION_FEE_OTHER = "Transaction Fee (Other)";

	///////////////////////////
	//////   Revenue    //////
	/////////////////////////
	public static final String REVENUE_REALIZED = "Realized Gain / Loss";
	public static final String REVENUE_UNREALIZED = "Unrealized Gain / Loss";
	public static final String REVENUE_DIVIDEND_INCOME = "Dividend Income";
	public static final String REVENUE_INTEREST_INCOME = "Interest Income";
	public static final String REVENUE_INTEREST_LEG = "Interest Leg";
	public static final String REVENUE_EQUITY_LEG = "Equity Leg";
	public static final String REVENUE_PREMIUM_LEG = "Premium Leg";
	public static final String REVENUE_PAI = "PAI Income";
	public static final String REPO_REVENUE_INTEREST_INCOME = "REPO Interest Income";
	public static final String CURRENCY_TRANSLATION_GAIN_LOSS = "Currency Translation Gain / Loss";

	///////////////////////////////////////////
	//////   Position and Collateral    //////
	/////////////////////////////////////////
	public static final String POSITION_COLLATERAL_RECEIVABLE = "Position Collateral Receivable";
	public static final String POSITION_COLLATERAL_PAYABLE = "Position Collateral Payable";
	public static final String CASH_COLLATERAL_RECEIVABLE = "Cash Collateral Receivable";
	public static final String CASH_COLLATERAL_PAYABLE = "Cash Collateral Payable";
	public static final String COUNTERPARTY_POSITION = "Counterparty Position";
	public static final String COUNTERPARTY_POSITION_COLLATERAL = "Counterparty Position Collateral";
	public static final String COUNTERPARTY_CASH = "Counterparty Cash";
	public static final String COUNTERPARTY_CASH_COLLATERAL = "Counterparty Cash Collateral";

	/////////////////////////////
	//////   Cash Flow    //////
	///////////////////////////
	public static final String CASH_CONTRIBUTION = "Contribution";
	public static final String CASH_DISTRIBUTION = "Distribution";

	///////////////////////////////////
	//////   Interest Income    //////
	/////////////////////////////////
	public static final String INTEREST_INCOME_MONEY_MARKET = "Interest Income (Money Market)";
	public static final String INTEREST_INCOME_NON_TAXABLE = "Interest Income (Non-Taxable)";
	public static final String INTEREST_INCOME_REPO = "REPO Interest Income";
	public static final String INTEREST_INCOME = "Interest Income";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private AccountingAccountType accountType;

	/**
	 * Specifies whether transactions for this account need to be tracked at lot level and can be closed in order
	 * to calculate gain/loss on a specific position. Various investment securities (BUT NOT CURRENCIES) are positions.
	 * Cash, Currency and accrual accounts do NOT need to be tracked at position level.
	 */
	private boolean position;

	/**
	 * Cash includes various Cash GL accounts (client's base currency).
	 */
	private boolean cash;

	/**
	 * Currency includes various Currency and Cash (cash is also a currency: client's base currency accounts.
	 * To separate base cash from currencies also use "cash" attribute which is true for cash but not for currencies.
	 */
	private boolean currency;

	/**
	 * Specifies whether this is a collateral account: Collateral Cash, Collateral Position, etc.
	 * Collateral assets must be treated separately from non-collateral assets and cannot be commingled.
	 * For example, a regular (non-collateral) position can never close existing collateral position.
	 */
	private boolean collateral;

	/**
	 * Specifies whether this account is a commission or a fee account.  Commission Only: (commission == true && fee == false)
	 */
	private boolean commission;

	/**
	 * Specifies whether this account is a fee (as opposed to commission)
	 */
	private boolean fee;

	/**
	 * Specifies whether this account tracks realized, unrealized or any other type of gain/loss
	 */
	private boolean gainLoss;

	/**
	 * Specifies whether this account tracks unrealized gain/loss
	 */
	private boolean unrealizedGainLoss;

	/**
	 * Gain/Loss from change in exchange rate from the time the position was open to the time it was closed.
	 * Local currency Gain/Loss is zero but in base currency it is usually not.
	 */
	private boolean currencyTranslation;

	/**
	 * Specifies whether this is Accounts Receivable account: Interest Receivable, Dividend Receivable, etc.
	 */
	private boolean receivable;

	/**
	 * Specifies whether posting to this GL account is not allowed. Some GL accounts must not be posted to GL
	 * and have other uses (Data Warehouse).  For example, Unrealized Gain / Loss, Market Value Adjustment.
	 */
	private boolean postingNotAllowed;

	/**
	 * Specifies whether a position can only be closed by the same broker that opened it.
	 */
	private boolean executingBrokerSpecific;

	/**
	 * Flag used to indicate counterparty assets/liabilities
	 */
	private boolean notOurAccount;

	/**
	 * If this field is set, then this GL account's balance will be automatically reversed by first
	 * closing position with the specified GL account.
	 * <p>
	 * For example, "Unrealized Gain / Loss" and "Recognized Gain / Loss" GL accounts maybe applied at year end for taxable account positions.
	 * When closing a position like this, the system will identify non zero "Unrealized Gain / Loss" balance tied to the opening
	 * and will auto-generate a GL entry that gets it to zero.  The offsetting account "Realized Gain / Loss" identified by this field
	 * will be used to make the journal balance.  It will affect actual "Realized Gain / Loss" from closing.
	 */
	private AccountingAccount autoAccrualReversalOffsetAccount;

	/**
	 * Each non Cash currency GL account must be mapped to corresponding unrealized Gain/Loss account.
	 * This is necessary because currency is not a position, yet has unrealized Currency Translation Gain/Loss
	 * from changes in FX rate from opening date. These GL accounts are necessary to be able to combine unrealized
	 * currency gain/loss with corresponding currency balance.
	 * <p>
	 * For example:
	 * Currency => Unrealized Currency Translation Gain / Loss
	 * Currency Collateral => Unrealized Currency Collateral Translation Gain / Loss
	 * Currency Money Market => Unrealized Currency Money Market Translation Gain / Loss
	 */
	private AccountingAccount unrealizedCurrencyAccount;


	/**
	 * The cash account that should be used when creating offsetting entries for this accounting account.
	 * <p>
	 * This only applies to position accounts.
	 * <p>
	 * Position Collateral => Cash Collateral
	 * Position Collateral Receivable => Cash Collateral Receivable
	 * Counterparty Position Collateral => Counterparty Cash Collateral
	 * Position Collateral Payable => Cash Collateral Payable
	 * Counterparty Position => Counterparty Cash
	 */
	private AccountingAccount cashAccount;

	/**
	 * The receivable account that should be used when generating receivables.
	 * <p>
	 * Position => Position Receivable
	 * Position Collateral => Position Collateral Receivable
	 * </p>
	 */
	private AccountingAccount receivableAccount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccount() {
		super();
	}


	public AccountingAccount(String name, short id) {
		this(null, name, id);
	}


	public AccountingAccount(AccountingAccountType accountType, String name, short id) {
		super();
		setAccountType(accountType);
		setName(name);
		setId(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (this.accountType == null) {
			return getName();
		}
		return getName() + " (" + this.accountType.getName() + ")";
	}


	/**
	 * Returns true if this GL account is Accrual account that is automatically reversed.
	 * For example, "Unrealized Gain / Loss" which is used with "Recognized Gain / Loss" for taxable account at year end.
	 */
	public boolean isAutoReverseAccrual() {
		return (this.autoAccrualReversalOffsetAccount != null);
	}


	public AccountingAccountType getAccountType() {
		return this.accountType;
	}


	public void setAccountType(AccountingAccountType accountType) {
		this.accountType = accountType;
	}


	public boolean isPosition() {
		return this.position;
	}


	public void setPosition(boolean position) {
		this.position = position;
	}


	public boolean isCash() {
		return this.cash;
	}


	public void setCash(boolean cash) {
		this.cash = cash;
	}


	public boolean isCollateral() {
		return this.collateral;
	}


	public void setCollateral(boolean collateral) {
		this.collateral = collateral;
	}


	public void setCommission(boolean commission) {
		this.commission = commission;
	}


	public boolean isCommission() {
		return this.commission;
	}


	public boolean isFee() {
		return this.fee;
	}


	public void setFee(boolean fee) {
		this.fee = fee;
	}


	public boolean isGainLoss() {
		return this.gainLoss;
	}


	public void setGainLoss(boolean gainLoss) {
		this.gainLoss = gainLoss;
	}


	public boolean isCurrencyTranslation() {
		return this.currencyTranslation;
	}


	public void setCurrencyTranslation(boolean currencyTranslation) {
		this.currencyTranslation = currencyTranslation;
	}


	public boolean isReceivable() {
		return this.receivable;
	}


	public void setReceivable(boolean receivable) {
		this.receivable = receivable;
	}


	public boolean isPostingNotAllowed() {
		return this.postingNotAllowed;
	}


	public void setPostingNotAllowed(boolean postingNotAllowed) {
		this.postingNotAllowed = postingNotAllowed;
	}


	public AccountingAccount getAutoAccrualReversalOffsetAccount() {
		return this.autoAccrualReversalOffsetAccount;
	}


	public void setAutoAccrualReversalOffsetAccount(AccountingAccount autoAccrualReversalOffsetAccount) {
		this.autoAccrualReversalOffsetAccount = autoAccrualReversalOffsetAccount;
	}


	public boolean isCurrency() {
		return this.currency;
	}


	public void setCurrency(boolean currency) {
		this.currency = currency;
	}


	public boolean isUnrealizedGainLoss() {
		return this.unrealizedGainLoss;
	}


	public void setUnrealizedGainLoss(boolean unrealizedGainLoss) {
		this.unrealizedGainLoss = unrealizedGainLoss;
	}


	public AccountingAccount getUnrealizedCurrencyAccount() {
		return this.unrealizedCurrencyAccount;
	}


	public void setUnrealizedCurrencyAccount(AccountingAccount unrealizedCurrencyAccount) {
		this.unrealizedCurrencyAccount = unrealizedCurrencyAccount;
	}


	public boolean isExecutingBrokerSpecific() {
		return this.executingBrokerSpecific;
	}


	public void setExecutingBrokerSpecific(boolean executingBrokerSpecific) {
		this.executingBrokerSpecific = executingBrokerSpecific;
	}


	public boolean isNotOurAccount() {
		return this.notOurAccount;
	}


	public void setNotOurAccount(boolean notOurAccount) {
		this.notOurAccount = notOurAccount;
	}


	public AccountingAccount getCashAccount() {
		return this.cashAccount;
	}


	public void setCashAccount(AccountingAccount cashAccount) {
		this.cashAccount = cashAccount;
	}


	public AccountingAccount getReceivableAccount() {
		return this.receivableAccount;
	}


	public void setReceivableAccount(AccountingAccount receivableAccount) {
		this.receivableAccount = receivableAccount;
	}
}
