package com.clifton.accounting.account.cache;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.search.AccountingAccountSearchForm;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * The <code>AccountingAccountIdsCacheImpl</code> class is a cache of AccountingAccount id's of specific types:
 * AUTO_REVERSE_ACCRUAL_ACCOUNTS, POSITION_ACCOUNTS, RECEIVABLE_ACCOUNTS.
 * <p>
 * It can be used to improve system performance by avoiding joins on large tables: AccountingTransaction, etc.
 * Instead, get corresponding AccountingAccount id's and use them in "IN" clause.
 *
 * @author vgomelsky
 */
@Component
public class AccountingAccountIdsCacheImpl extends SelfRegisteringSimpleDaoCache<AccountingAccount, String, Short[]> implements AccountingAccountIdsCache {

	private AccountingAccountService accountingAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static AccountingAccountIds getCurrencyNonCashWithCollateralOption(Boolean collateral) {
		if (collateral == null) {
			return AccountingAccountIdsCacheImpl.AccountingAccountIds.CURRENCY_NON_CASH_ACCOUNTS;
		}
		if (collateral) {
			return AccountingAccountIdsCacheImpl.AccountingAccountIds.CURRENCY_NON_CASH_COLLATERAL_ACCOUNTS;
		}
		return AccountingAccountIdsCacheImpl.AccountingAccountIds.CURRENCY_NON_CASH_NON_COLLATERAL_ACCOUNTS;
	}


	@Override
	public List<AccountingAccount> getAccountingAccountList(AccountingAccountIds accountsKey) {
		return ArrayUtils.getStream(getAccountingAccounts(accountsKey)).map(id -> getAccountingAccountService().getAccountingAccount(id)).collect(Collectors.toList());
	}


	@Override
	public Short[] getAccountingAccounts(AccountingAccountIds accountsKey) {
		return getFromCacheOrUpdate(accountsKey.name(), () -> {
			AccountingAccountSearchForm searchForm = new AccountingAccountSearchForm();
			searchForm.setAutoReverseAccrualAccountingAccount(accountsKey.getAutoReverseAccrualAccountingAccount());
			searchForm.setPosition(accountsKey.getPosition());
			searchForm.setReceivable(accountsKey.getReceivable());
			searchForm.setCash(accountsKey.getCash());
			searchForm.setCurrency(accountsKey.getCurrency());
			searchForm.setCollateral(accountsKey.getCollateral());
			searchForm.setExcludeReceivableCollateral(accountsKey.getExcludeReceivableCollateral());
			searchForm.setNotOurAccount((accountsKey.getExcludeNotOurAccount() != null) ? !accountsKey.getExcludeNotOurAccount() : null);
			searchForm.setCommission(accountsKey.getCommission());
			searchForm.setGainLoss(accountsKey.getGainLoss());
			return searchForm;
		});
	}


	@Override
	public Short[] getAccountingAccountsByAccountType(String accountType) {
		return getFromCacheOrUpdate("AccountType_" + accountType, () -> {
			AccountingAccountSearchForm searchForm = new AccountingAccountSearchForm();
			searchForm.setAccountTypeEquals(accountType);
			return searchForm;
		});
	}


	private Short[] getFromCacheOrUpdate(String cacheKey, Supplier<AccountingAccountSearchForm> searchFormSupplierIfMissing) {
		Short[] result = getCacheHandler().get(getCacheName(), cacheKey);
		if (result == null) {
			// not in cache: retrieve from service and store in cache for future calls
			AccountingAccountSearchForm searchForm = searchFormSupplierIfMissing.get();
			List<AccountingAccount> list = getAccountingAccountService().getAccountingAccountList(searchForm);

			int size = CollectionUtils.getSize(list);
			result = new Short[size];
			for (int i = 0; i < size; i++) {
				result[i] = list.get(i).getId();
			}
			getCacheHandler().put(getCacheName(), cacheKey, result);
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * The <code>AccountingAccountIds</code> enum defines various options for AccountingAccount searches.
	 *
	 * @author vgomelsky
	 */
	public enum AccountingAccountIds {
		AUTO_REVERSE_ACCRUAL_ACCOUNTS(new IdsBuilder().withAutoReverseAccrualAccountingAccount(true)),
		POSITION_ACCOUNTS(new IdsBuilder().withPosition(true)),
		POSITION_ACCOUNTS_EXCLUDE_RECEIVABLE(new IdsBuilder().withPosition(true).withReceivable(false)),
		POSITION_ACCOUNTS_EXCLUDE_NOT_OUR_ACCOUNTS_AND_RECEIVABLE_COLLATERAL(new IdsBuilder().withPosition(true).withExcludeNotOurAccount(true).withExcludeReceivableCollateral(true)),
		POSITION_ACCOUNTS_EXCLUDE_NOT_OUR_ACCOUNTS(new IdsBuilder().withPosition(true).withExcludeNotOurAccount(true)),
		POSITION_ACCOUNTS_EXCLUDE_COLLATERAL_AND_RECEIVABLE(new IdsBuilder().withPosition(true).withCollateral(false).withReceivable(false)),
		POSITION_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL(new IdsBuilder().withPosition(true).withExcludeReceivableCollateral(true)),
		POSITION_COLLATERAL_ACCOUNTS_EXCLUDE_RECEIVABLE(new IdsBuilder().withPosition(true).withCollateral(true).withReceivable(false)),

		NON_POSITION_ACCOUNTS(new IdsBuilder().withPosition(false)),
		NON_POSITION_COLLATERAL(new IdsBuilder().withPosition(false).withCollateral(true)),

		CASH_ACCOUNTS(new IdsBuilder().withCash(true)),
		CASH_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL(new IdsBuilder().withCash(true).withExcludeReceivableCollateral(true)),
		CASH_COLLATERAL_ACCOUNTS(new IdsBuilder().withCash(true).withCollateral(true)),
		CASH_COLLATERAL_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL(new IdsBuilder().withCash(true).withCollateral(true).withExcludeReceivableCollateral(true)),
		CASH_ACCOUNTS_EXCLUDE_COLLATERAL(new IdsBuilder().withCash(true).withCollateral(false)),

		COLLATERAL(new IdsBuilder().withCollateral(true)),
		COLLATERAL_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL_EXCLUDE_POSITIONS(new IdsBuilder().withCollateral(true).withExcludeReceivableCollateral(true).withPosition(false)),

		CURRENCY_NON_CASH_ACCOUNTS(new IdsBuilder().withCurrency(true).withCash(false)),
		CURRENCY_NON_CASH_COLLATERAL_ACCOUNTS(new IdsBuilder().withCurrency(true).withCash(false).withCollateral(true)),
		CURRENCY_NON_CASH_NON_COLLATERAL_ACCOUNTS(new IdsBuilder().withCurrency(true).withCash(false).withCollateral(false)),
		CURRENCY_NON_CASH_NON_GAIN_LOSS_ACCOUNTS(new IdsBuilder().withCurrency(true).withCash(false).withGainLoss(false)),
		CURRENCY_NON_GAIN_LOSS_ACCOUNTS(new IdsBuilder().withCurrency(true).withGainLoss(false)),

		RECEIVABLE_ACCOUNTS(new IdsBuilder().withReceivable(true)),
		RECEIVABLE_ACCOUNTS_EXCLUDE_POSITION_EXCLUDE_COLLATERAL(new IdsBuilder().withReceivable(true).withPosition(false).withCollateral(false)),
		RECEIVABLE_ACCOUNTS_EXCLUDE_POSITION(new IdsBuilder().withReceivable(true).withPosition(false)),
		EXCLUDE_RECEIVABLE_COLLATERAL(new IdsBuilder().withExcludeReceivableCollateral(true)),
		COMMISSION_ACCOUNTS(new IdsBuilder().withCommission(true)),
		GAIN_LOSS_ACCOUNTS(new IdsBuilder().withGainLoss(true));


		private final Boolean position;
		private final Boolean receivable;
		private final Boolean autoReverseAccrualAccountingAccount;

		private final Boolean cash;
		private final Boolean currency;

		private final Boolean collateral;
		private final Boolean excludeReceivableCollateral;
		private final Boolean excludeNotOurAccount;

		private final Boolean commission;
		private final Boolean gainLoss;

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		AccountingAccountIds(IdsBuilder builder) {
			this.position = builder.position;
			this.receivable = builder.receivable;
			this.autoReverseAccrualAccountingAccount = builder.autoReverseAccrualAccountingAccount;
			this.cash = builder.cash;
			this.currency = builder.currency;
			this.collateral = builder.collateral;
			this.excludeReceivableCollateral = builder.excludeReceivableCollateral;
			this.excludeNotOurAccount = builder.excludeNotOurAccount;
			this.commission = builder.commission;
			this.gainLoss = builder.gainLoss;
		}


		private static class IdsBuilder {

			private Boolean position;
			private Boolean receivable;
			private Boolean autoReverseAccrualAccountingAccount;

			private Boolean cash;
			private Boolean currency;

			private Boolean collateral;
			private Boolean excludeReceivableCollateral;
			private Boolean excludeNotOurAccount;

			private Boolean commission;
			private Boolean gainLoss;


			public IdsBuilder withPosition(boolean position) {
				this.position = position;
				return this;
			}


			public IdsBuilder withReceivable(boolean receivable) {
				this.receivable = receivable;
				return this;
			}


			public IdsBuilder withAutoReverseAccrualAccountingAccount(boolean autoReverseAccrualAccountingAccount) {
				this.autoReverseAccrualAccountingAccount = autoReverseAccrualAccountingAccount;
				return this;
			}


			public IdsBuilder withCash(boolean cash) {
				this.cash = cash;
				return this;
			}


			public IdsBuilder withCurrency(boolean currency) {
				this.currency = currency;
				return this;
			}


			public IdsBuilder withCollateral(boolean collateral) {
				this.collateral = collateral;
				return this;
			}


			public IdsBuilder withExcludeReceivableCollateral(boolean excludeReceivableCollateral) {
				this.excludeReceivableCollateral = excludeReceivableCollateral;
				return this;
			}


			public IdsBuilder withExcludeNotOurAccount(boolean excludeNotOurAccount) {
				this.excludeNotOurAccount = excludeNotOurAccount;
				return this;
			}


			public IdsBuilder withCommission(boolean commission) {
				this.commission = commission;
				return this;
			}


			public IdsBuilder withGainLoss(boolean gainLoss) {
				this.gainLoss = gainLoss;
				return this;
			}
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public Boolean getPosition() {
			return this.position;
		}


		public Boolean getReceivable() {
			return this.receivable;
		}


		public Boolean getAutoReverseAccrualAccountingAccount() {
			return this.autoReverseAccrualAccountingAccount;
		}


		public Boolean getCash() {
			return this.cash;
		}


		public Boolean getCurrency() {
			return this.currency;
		}


		public Boolean getCollateral() {
			return this.collateral;
		}


		public Boolean getExcludeReceivableCollateral() {
			return this.excludeReceivableCollateral;
		}


		public Boolean getExcludeNotOurAccount() {
			return this.excludeNotOurAccount;
		}


		public Boolean getCommission() {
			return this.commission;
		}


		public Boolean getGainLoss() {
			return this.gainLoss;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}
}
