package com.clifton.accounting.account.cache;

import java.util.List;


/**
 * The <code>AccountingAccountIdListByGroupCache</code> caches a list of {@link com.clifton.accounting.account.AccountingAccount} ids for a given {@link com.clifton.accounting.account.AccountingAccountGroup} id
 * This cache observes the {@link com.clifton.accounting.account.AccountingAccountGroupItemAccountingAccount} for updates to clear the cache for the group when assignments change
 *
 * @author manderson
 */
public interface AccountingAccountIdListByGroupCache {


	public List<Short> getAccountingAccountIdList(Short groupId);


	public void storeAccountingAccountIdList(Short groupId, List<Short> accountingAccountIdList);
}
