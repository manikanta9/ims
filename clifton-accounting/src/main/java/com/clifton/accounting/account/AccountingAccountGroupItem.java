package com.clifton.accounting.account;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>AccountingAccountGroupItem</code> class is used with AccountingAccountGroup to group
 * GL Accounts for reporting or filtering purposes.
 *
 * @author Mary Anderson
 */
public class AccountingAccountGroupItem extends NamedEntity<Integer> {

	private AccountingAccountGroup group;

	private Integer itemOrder;
	private boolean unrealizedIncluded;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountGroup getGroup() {
		return this.group;
	}


	public void setGroup(AccountingAccountGroup group) {
		this.group = group;
	}


	public boolean isUnrealizedIncluded() {
		return this.unrealizedIncluded;
	}


	public void setUnrealizedIncluded(boolean unrealizedIncluded) {
		this.unrealizedIncluded = unrealizedIncluded;
	}


	public Integer getItemOrder() {
		return this.itemOrder;
	}


	public void setItemOrder(Integer itemOrder) {
		this.itemOrder = itemOrder;
	}
}
