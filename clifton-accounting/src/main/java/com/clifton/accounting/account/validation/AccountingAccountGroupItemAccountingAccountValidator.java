package com.clifton.accounting.account.validation;


import com.clifton.accounting.account.AccountingAccountGroupItemAccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.search.GroupItemAccountSearchForm;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>AccountingAccountGroupItemAccountValidator</code> class validates GL Account assignments to corresponding group items
 * to make sure that there are no duplicate assignments for a given group.
 *
 * @author Mary Anderson
 */
@Component
public class AccountingAccountGroupItemAccountingAccountValidator extends SelfRegisteringDaoValidator<AccountingAccountGroupItemAccountingAccount> {

	private AccountingAccountService accountingAccountService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(AccountingAccountGroupItemAccountingAccount bean, DaoEventTypes config) throws ValidationException {
		if (!bean.getReferenceOne().getGroup().isDuplicationAllowed()) {
			AccountingAccountGroupItemAccountingAccount original = null;
			if (config.isUpdate()) {
				original = getOriginalBean(bean);
			}

			if (original == null || (!original.getReferenceOne().equals(bean.getReferenceOne()) || !original.getReferenceTwo().equals(bean.getReferenceTwo()))) {
				GroupItemAccountSearchForm searchForm = new GroupItemAccountSearchForm();
				searchForm.setGroupId(bean.getReferenceOne().getGroup().getId());
				searchForm.setAccountId(bean.getReferenceTwo().getId());
				List<AccountingAccountGroupItemAccountingAccount> list = getAccountingAccountService().getAccountingAccountGroupItemAccountingAccountList(searchForm);
				// Should Only Be One - If not - something already failed validation and we need to know this
				AccountingAccountGroupItemAccountingAccount existing = CollectionUtils.getOnlyElement(list);
				if (existing != null) {
					if (original == null || (!existing.equals(bean))) {
						throw new FieldValidationException("This account [" + bean.getReferenceTwo().getName() + "] is already assigned to group item [" + existing.getReferenceOne().getName()
								+ "] for this group.  Duplicates are not allowed.", "referenceOne.id");
					}
				}
			}
		}
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}
}
