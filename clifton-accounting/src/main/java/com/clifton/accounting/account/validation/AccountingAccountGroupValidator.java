package com.clifton.accounting.account.validation;


import com.clifton.accounting.account.AccountingAccountGroup;
import com.clifton.accounting.account.AccountingAccountGroupItemAccountingAccount;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AccountingAccountGroupValidator</code>
 * <p/>
 * If group is System Defined, Names cannot change
 *
 * @author Mary Anderson
 */
@Component
public class AccountingAccountGroupValidator extends SelfRegisteringDaoValidator<AccountingAccountGroup> {

	private AdvancedReadOnlyDAO<AccountingAccountGroupItemAccountingAccount, Criteria> accountingAccountGroupItemAccountingAccountDAO;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(AccountingAccountGroup bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("Deleting System Defined Accounting Account Groups is not allowed.");
			}
			if (config.isInsert()) {
				throw new ValidationException("Adding new System Defined Accounting Account Groups is not allowed.");
			}
		}
		if (config.isUpdate()) {
			AccountingAccountGroup original = getOriginalBean(bean);
			if (original.isSystemDefined() != bean.isSystemDefined()) {
				throw new FieldValidationException("You cannot edit the System Defined field for Accounting Account Groups", "systemDefined");
			}
			if (original.isSystemDefined()) {
				ValidationUtils.assertTrue(original.getName().equals(bean.getName()), "System Defined Accounting Account Group Names cannot be changed.", "name");
			}
			// Was allowing duplicate assignments, but now not going to - need to make sure there are not duplicate assignments already
			if (original.isDuplicationAllowed() && !bean.isDuplicationAllowed()) {
				List<String> list = getDuplicateAccountsForGroup(bean.getId());
				if (!CollectionUtils.isEmpty(list)) {
					throw new FieldValidationException("Cannot change this group to not allow duplicates because the following account(s) are currently assigned to multiple group items ["
							+ StringUtils.collectionToCommaDelimitedString(list) + "]", "duplicationAllowed");
				}
			}
		}
	}


	private List<String> getDuplicateAccountsForGroup(final int groupId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			criteria.createAlias("referenceOne", "itemOuter");
			criteria.createAlias("referenceTwo", "acctOuter");
			criteria.add(Restrictions.eq("itemOuter.group.id", groupId));

			DetachedCriteria sub = DetachedCriteria.forClass(AccountingAccountGroupItemAccountingAccount.class, "link");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("link.referenceOne", "item");
			sub.createAlias("link.referenceTwo", "acct");
			sub.add(Restrictions.eqProperty("acct.id", "acctOuter.id"));
			sub.add(Restrictions.neProperty("item.id", "itemOuter.id"));
			sub.add(Restrictions.eqProperty("item.group.id", "itemOuter.group.id"));
			criteria.add(Subqueries.exists(sub));
		};
		List<AccountingAccountGroupItemAccountingAccount> list = getAccountingAccountGroupItemAccountingAccountDAO().findBySearchCriteria(searchConfigurer);
		List<String> accountNames = new ArrayList<>();
		for (AccountingAccountGroupItemAccountingAccount acctLink : CollectionUtils.getIterable(list)) {
			if (!accountNames.contains(acctLink.getReferenceTwo().getName())) {
				accountNames.add(acctLink.getReferenceTwo().getName());
			}
		}
		return accountNames;
	}


	public AdvancedReadOnlyDAO<AccountingAccountGroupItemAccountingAccount, Criteria> getAccountingAccountGroupItemAccountingAccountDAO() {
		return this.accountingAccountGroupItemAccountingAccountDAO;
	}


	public void setAccountingAccountGroupItemAccountingAccountDAO(AdvancedReadOnlyDAO<AccountingAccountGroupItemAccountingAccount, Criteria> accountingAccountGroupItemAccountingAccountDAO) {
		this.accountingAccountGroupItemAccountingAccountDAO = accountingAccountGroupItemAccountingAccountDAO;
	}
}
