package com.clifton.accounting.account;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.account.search.AccountingAccountGroupItemSearchForm;
import com.clifton.accounting.account.search.AccountingAccountGroupSearchForm;
import com.clifton.accounting.account.search.AccountingAccountSearchForm;
import com.clifton.accounting.account.search.GroupItemAccountSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.List;


/**
 * The <code>AccountingAccountService</code> interface defines methods for working with GL Accounts
 * and related objects.
 *
 * @author vgomelsky
 */
public interface AccountingAccountService {

	////////////////////////////////////////////////////////////////////////////
	////////          AccountingAccount Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccount getAccountingAccount(short id);


	public AccountingAccount getAccountingAccountByName(String name);


	public List<AccountingAccount> getAccountingAccountList(AccountingAccountSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<AccountingAccount> getAccountingAccountListByIds(AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIds);


	public List<AccountingAccount> getAccountingAccountListForGroup(short groupId);


	public AccountingAccount saveAccountingAccount(AccountingAccount bean);


	public void deleteAccountingAccount(short id);


	////////////////////////////////////////////////////////////////////////////
	////////        AccountingAccountType Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountType getAccountingAccountType(short id);

	public AccountingAccountType getAccountingAccountTypeByName(String name);


	public List<AccountingAccountType> getAccountingAccountTypeList();


	////////////////////////////////////////////////////////////////////////////
	///////        Accounting Account Group Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountGroup getAccountingAccountGroup(short id);


	public List<AccountingAccountGroup> getAccountingAccountGroupList(AccountingAccountGroupSearchForm searchForm);


	public AccountingAccountGroup saveAccountingAccountGroup(AccountingAccountGroup bean);


	public void deleteAccountingAccountGroup(short id);


	////////////////////////////////////////////////////////////////////////////
	///////         AccountingAccount Group Item Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountGroupItem getAccountingAccountGroupItem(int id);


	public List<AccountingAccountGroupItem> getAccountingAccountGroupItemList(AccountingAccountGroupItemSearchForm searchForm);


	public AccountingAccountGroupItem saveAccountingAccountGroupItem(AccountingAccountGroupItem bean);


	public void deleteAccountingAccountGroupItem(int id);


	////////////////////////////////////////////////////////////////////////////
	//////   Accounting Account Group Item Account Business Methods       //////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountGroupItemAccountingAccount getAccountingAccountGroupItemAccountingAccount(int id);


	public List<AccountingAccountGroupItemAccountingAccount> getAccountingAccountGroupItemAccountingAccountList(GroupItemAccountSearchForm searchForm);


	public AccountingAccountGroupItemAccountingAccount saveAccountingAccountGroupItemAccountingAccount(AccountingAccountGroupItemAccountingAccount bean);


	public void deleteAccountingAccountGroupItemAccountingAccount(int id);
}
