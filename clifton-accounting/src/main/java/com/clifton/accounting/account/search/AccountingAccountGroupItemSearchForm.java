package com.clifton.accounting.account.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class AccountingAccountGroupItemSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "group.id")
	private Short groupId;

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField
	private Integer itemOrder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getItemOrder() {
		return this.itemOrder;
	}


	public void setItemOrder(Integer itemOrder) {
		this.itemOrder = itemOrder;
	}
}
