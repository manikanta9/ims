package com.clifton.accounting.account;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>AccountingAccountType</code> class defines the type of an accounting account: Asset, Liability, Equity, Revenue or Expense.
 *
 * @author vgomelsky
 */
@CacheByName
public class AccountingAccountType extends NamedEntity<Short> {

	public static final String ASSET = "Asset";
	public static final String LIABILITY = "Liability";
	public static final String EQUITY = "Equity";
	public static final String REVENUE = "Revenue";
	public static final String EXPENSE = "Expense";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Specifies whether this is a balance sheet or income statement account.
	 */
	private boolean balanceSheetAccount;
	/**
	 * Specifies whether Debit or Credit to this account type is growing the balance.
	 * Assets and Expenses grow on Debit side and Liabilities, Owners Equity and Revenue grow on the Credit side.
	 */
	private boolean growingOnDebitSide;
	private int accountingAccountTypeOrder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountType() {
		super();
	}


	public AccountingAccountType(String name, boolean balanceSheetAccount, boolean growingOnDebitSide) {
		super();
		setName(name);
		setBalanceSheetAccount(balanceSheetAccount);
		setGrowingOnDebitSide(growingOnDebitSide);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getAccountingAccountTypeOrder() {
		return this.accountingAccountTypeOrder;
	}


	public void setAccountingAccountTypeOrder(int accountingAccountTypeOrder) {
		this.accountingAccountTypeOrder = accountingAccountTypeOrder;
	}


	public boolean isBalanceSheetAccount() {
		return this.balanceSheetAccount;
	}


	public void setBalanceSheetAccount(boolean balanceSheetAccount) {
		this.balanceSheetAccount = balanceSheetAccount;
	}


	public boolean isGrowingOnDebitSide() {
		return this.growingOnDebitSide;
	}


	public void setGrowingOnDebitSide(boolean growingOnDebitSide) {
		this.growingOnDebitSide = growingOnDebitSide;
	}
}
