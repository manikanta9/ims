package com.clifton.accounting.account;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>AccountingAccountGroup</code> class is a 2 level deep grouping of GL Accounts:
 * accounts are assigned to named AccountingAccountGroupItem which are assigned to named AccountingAccountGroup.
 * <p/>
 * GL Account groups are usually used for reporting (income statement categories, etc.) or for filtering.
 *
 * @author Mary Anderson
 */
public class AccountingAccountGroup extends NamedEntity<Short> {

	/**
	 * If true, names cannot be edited.
	 */
	private boolean systemDefined;

	/**
	 * If true, then one account can belong to more than one group items for this group.  Otherwise, accounts
	 * may belong to only one group item.
	 */
	private boolean duplicationAllowed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isDuplicationAllowed() {
		return this.duplicationAllowed;
	}


	public void setDuplicationAllowed(boolean duplicationAllowed) {
		this.duplicationAllowed = duplicationAllowed;
	}
}
