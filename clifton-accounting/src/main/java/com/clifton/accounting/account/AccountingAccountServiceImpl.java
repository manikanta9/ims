package com.clifton.accounting.account;


import com.clifton.accounting.account.cache.AccountingAccountIdListByGroupCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.account.search.AccountingAccountGroupItemSearchForm;
import com.clifton.accounting.account.search.AccountingAccountGroupSearchForm;
import com.clifton.accounting.account.search.AccountingAccountSearchForm;
import com.clifton.accounting.account.search.GroupItemAccountSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AccountingAccountServiceImpl</code> class provides basic implementation of AccountingAccountService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingAccountServiceImpl implements AccountingAccountService {

	private AdvancedUpdatableDAO<AccountingAccount, Criteria> accountingAccountDAO;
	private AdvancedReadOnlyDAO<AccountingAccountType, Criteria> accountingAccountTypeDAO;

	private AdvancedUpdatableDAO<AccountingAccountGroup, Criteria> accountingAccountGroupDAO;
	private AdvancedUpdatableDAO<AccountingAccountGroupItem, Criteria> accountingAccountGroupItemDAO;
	private AdvancedUpdatableDAO<AccountingAccountGroupItemAccountingAccount, Criteria> accountingAccountGroupItemAccountingAccountDAO;

	private DaoNamedEntityCache<AccountingAccount> accountingAccountCache;
	private AccountingAccountIdListByGroupCache accountingAccountIdListByGroupCache;
	private DaoNamedEntityCache<AccountingAccountType> accountingAccountTypeCache;
	private AccountingAccountIdsCache accountingAccountIdsCache;


	////////////////////////////////////////////////////////////////////////////
	////////          AccountingAccount Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingAccount getAccountingAccount(short id) {
		return getAccountingAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingAccount getAccountingAccountByName(String name) {
		return getAccountingAccountCache().getBeanForKeyValueStrict(getAccountingAccountDAO(), name);
	}


	@Override
	public List<AccountingAccount> getAccountingAccountList(final AccountingAccountSearchForm searchForm) {
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (BooleanUtils.isTrue(searchForm.getExcludeReceivableCollateral())) {
					criteria.add(Restrictions.not(Restrictions.and(Restrictions.eq("receivable", true), Restrictions.eq("collateral", true))));
				}

				if (searchForm.getExcludedFromAccountGroupId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(AccountingAccountGroupItemAccountingAccount.class, "link");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceOne", "item");
					sub.add(Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".id"));
					sub.add(Restrictions.eq("item.group.id", searchForm.getExcludedFromAccountGroupId()));
					criteria.add(Subqueries.notExists(sub));
				}
			}
		};
		return getAccountingAccountDAO().findBySearchCriteria(configurer);
	}


	@Override
	public List<AccountingAccount> getAccountingAccountListByIds(AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIds) {
		return getAccountingAccountIdsCache().getAccountingAccountList(accountingAccountIds);
	}


	@Override
	public List<AccountingAccount> getAccountingAccountListForGroup(short groupId) {
		List<Short> accountingAccountIdList = getAccountingAccountIdListByGroupCache().getAccountingAccountIdList(groupId);
		if (accountingAccountIdList != null) {
			return getAccountingAccountDAO().findByPrimaryKeys(accountingAccountIdList.toArray(new Short[0]));
		}
		// If it's not in the cache - look up the accounts and set them in the cache
		AccountingAccountSearchForm searchForm = new AccountingAccountSearchForm();
		searchForm.setAccountGroupId(groupId);
		List<AccountingAccount> accountingAccountList = getAccountingAccountList(searchForm);
		if (accountingAccountList == null) {
			accountingAccountIdList = new ArrayList<>();
		}
		else {
			accountingAccountIdList = BeanUtils.getBeanIdentityList(accountingAccountList);
		}
		getAccountingAccountIdListByGroupCache().storeAccountingAccountIdList(groupId, accountingAccountIdList);
		return accountingAccountList;
	}


	@Override
	public AccountingAccount saveAccountingAccount(AccountingAccount bean) {
		if (!bean.isNewBean()) {
			throw new ValidationException("Changes to existing GL Account are not allowed.");
		}
		if (bean.isCash()) {
			ValidationUtils.assertTrue(bean.isCurrency(), "Cash GL Account must be a Currency", "currency");
		}
		else if (bean.isCurrency()) {
			// non-cash currency
			ValidationUtils.assertNotNull(bean.getUnrealizedCurrencyAccount(), "Non Cash Currency accounts must map to corresponding Unrealized Currency Gain/Loss account.",
					"unrealizedCurrencyAccount");
			ValidationUtils.assertTrue(bean.getUnrealizedCurrencyAccount().isCurrency(), "Non Cash Currency accounts must map to corresponding Unrealized Currency Gain/Loss account.",
					"unrealizedCurrencyAccount");
			ValidationUtils.assertTrue(bean.getUnrealizedCurrencyAccount().isUnrealizedGainLoss(), "Non Cash Currency accounts must map to corresponding Unrealized Currency Gain/Loss account.",
					"unrealizedCurrencyAccount");
		}
		if (bean.isPosition()) {
			ValidationUtils.assertFalse(bean.isCurrency(), "Position GL Account cannot be a Currency", "currency");
			ValidationUtils.assertFalse(bean.isCash(), "Position GL Account cannot be a Cash", "cash");
			ValidationUtils.assertFalse(bean.isGainLoss(), "Position GL Account cannot be a Gain/Loss", "gainLoss");
			ValidationUtils.assertFalse(bean.isCommission(), "Position GL Account cannot be a Commission", "commission");
			ValidationUtils.assertFalse(bean.isCurrencyTranslation(), "Position GL Account cannot be a Currency Translation", "currencyTranslation");
		}
		if (bean.isUnrealizedGainLoss()) {
			ValidationUtils.assertTrue(bean.isGainLoss(), "Unrealized Gain/Loss account must also be a Gain/Loss account.", "gainLoss");
		}
		ValidationUtils.assertTrue(bean.getCashAccount() == null || (bean.getCashAccount() != null && bean.isPosition()), "A cash account can only be set for position accounting accounts.", "cashAccount");
		return getAccountingAccountDAO().save(bean);
	}


	@Override
	public void deleteAccountingAccount(short id) {
		getAccountingAccountDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////        AccountingAccountType Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////

	@Override
	public AccountingAccountType getAccountingAccountType(short id) {
		return getAccountingAccountTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingAccountType getAccountingAccountTypeByName(String name) {
		return getAccountingAccountTypeCache().getBeanForKeyValueStrict(getAccountingAccountTypeDAO(), name);
	}


	@Override
	public List<AccountingAccountType> getAccountingAccountTypeList() {
		return getAccountingAccountTypeDAO().findAll();
	}


	////////////////////////////////////////////////////////////////////////////
	///////        Accounting Account Group Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingAccountGroup getAccountingAccountGroup(short id) {
		return getAccountingAccountGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingAccountGroup> getAccountingAccountGroupList(AccountingAccountGroupSearchForm searchForm) {
		return getAccountingAccountGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingAccountGroup saveAccountingAccountGroup(AccountingAccountGroup bean) {
		return getAccountingAccountGroupDAO().save(bean);
	}


	@Override
	public void deleteAccountingAccountGroup(short id) {
		getAccountingAccountGroupDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////         AccountingAccount Group Item Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingAccountGroupItem getAccountingAccountGroupItem(int id) {
		return getAccountingAccountGroupItemDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingAccountGroupItem> getAccountingAccountGroupItemList(AccountingAccountGroupItemSearchForm searchForm) {
		searchForm.setOrderBy("itemOrder#name");
		return getAccountingAccountGroupItemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingAccountGroupItem saveAccountingAccountGroupItem(AccountingAccountGroupItem bean) {
		return getAccountingAccountGroupItemDAO().save(bean);
	}


	@Override
	public void deleteAccountingAccountGroupItem(int id) {
		getAccountingAccountGroupItemDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////   Accounting Account Group Item Account Business Methods       //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingAccountGroupItemAccountingAccount getAccountingAccountGroupItemAccountingAccount(int id) {
		return getAccountingAccountGroupItemAccountingAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingAccountGroupItemAccountingAccount> getAccountingAccountGroupItemAccountingAccountList(GroupItemAccountSearchForm searchForm) {
		return getAccountingAccountGroupItemAccountingAccountDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingAccountGroupItemAccountingAccount saveAccountingAccountGroupItemAccountingAccount(AccountingAccountGroupItemAccountingAccount bean) {
		return getAccountingAccountGroupItemAccountingAccountDAO().save(bean);
	}


	@Override
	public void deleteAccountingAccountGroupItemAccountingAccount(int id) {
		getAccountingAccountGroupItemAccountingAccountDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingAccount, Criteria> getAccountingAccountDAO() {
		return this.accountingAccountDAO;
	}


	public void setAccountingAccountDAO(AdvancedUpdatableDAO<AccountingAccount, Criteria> accountingAccountDAO) {
		this.accountingAccountDAO = accountingAccountDAO;
	}


	public AdvancedReadOnlyDAO<AccountingAccountType, Criteria> getAccountingAccountTypeDAO() {
		return this.accountingAccountTypeDAO;
	}


	public void setAccountingAccountTypeDAO(AdvancedReadOnlyDAO<AccountingAccountType, Criteria> accountingAccountTypeDAO) {
		this.accountingAccountTypeDAO = accountingAccountTypeDAO;
	}


	public AdvancedUpdatableDAO<AccountingAccountGroup, Criteria> getAccountingAccountGroupDAO() {
		return this.accountingAccountGroupDAO;
	}


	public void setAccountingAccountGroupDAO(AdvancedUpdatableDAO<AccountingAccountGroup, Criteria> accountingAccountGroupDAO) {
		this.accountingAccountGroupDAO = accountingAccountGroupDAO;
	}


	public AdvancedUpdatableDAO<AccountingAccountGroupItem, Criteria> getAccountingAccountGroupItemDAO() {
		return this.accountingAccountGroupItemDAO;
	}


	public void setAccountingAccountGroupItemDAO(AdvancedUpdatableDAO<AccountingAccountGroupItem, Criteria> accountingAccountGroupItemDAO) {
		this.accountingAccountGroupItemDAO = accountingAccountGroupItemDAO;
	}


	public AdvancedUpdatableDAO<AccountingAccountGroupItemAccountingAccount, Criteria> getAccountingAccountGroupItemAccountingAccountDAO() {
		return this.accountingAccountGroupItemAccountingAccountDAO;
	}


	public void setAccountingAccountGroupItemAccountingAccountDAO(AdvancedUpdatableDAO<AccountingAccountGroupItemAccountingAccount, Criteria> accountingAccountGroupItemAccountingAccountDAO) {
		this.accountingAccountGroupItemAccountingAccountDAO = accountingAccountGroupItemAccountingAccountDAO;
	}


	public DaoNamedEntityCache<AccountingAccount> getAccountingAccountCache() {
		return this.accountingAccountCache;
	}


	public void setAccountingAccountCache(DaoNamedEntityCache<AccountingAccount> accountingAccountCache) {
		this.accountingAccountCache = accountingAccountCache;
	}


	public DaoNamedEntityCache<AccountingAccountType> getAccountingAccountTypeCache() {
		return this.accountingAccountTypeCache;
	}


	public void setAccountingAccountTypeCache(DaoNamedEntityCache<AccountingAccountType> accountingAccountTypeCache) {
		this.accountingAccountTypeCache = accountingAccountTypeCache;
	}


	public AccountingAccountIdListByGroupCache getAccountingAccountIdListByGroupCache() {
		return this.accountingAccountIdListByGroupCache;
	}


	public void setAccountingAccountIdListByGroupCache(AccountingAccountIdListByGroupCache accountingAccountIdListByGroupCache) {
		this.accountingAccountIdListByGroupCache = accountingAccountIdListByGroupCache;
	}


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}
}
