package com.clifton.accounting.period.cache;

import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;


/**
 * @author manderson
 */
public interface AccountingPeriodClosingCache {


	public AccountingPeriodClosing getAccountingPeriodClosingLastClosedForAccount(ReadOnlyDAO<AccountingPeriodClosing> dao, int investmentAccountId);
}
