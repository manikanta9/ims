package com.clifton.accounting.period.cache;


import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.accounting.period.search.AccountingPeriodClosingSearchForm;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>AccountingPeriodClosingCacheImpl</code> class caches the latest closed period per investment account.
 *
 * @author vgomelsky
 */
@Component
public class AccountingPeriodClosingCacheImpl extends SelfRegisteringSimpleDaoCache<AccountingPeriodClosing, Integer, ObjectWrapper<Integer>> implements AccountingPeriodClosingCache {

	protected Integer getBeanKeyValue(AccountingPeriodClosing bean) {
		if (bean.getInvestmentAccount() != null) {
			return bean.getInvestmentAccount().getId();
		}
		return null;
	}


	@Override
	public AccountingPeriodClosing getAccountingPeriodClosingLastClosedForAccount(ReadOnlyDAO<AccountingPeriodClosing> dao, int investmentAccountId) {
		ObjectWrapper<Integer> id = getCacheHandler().get(getCacheName(), investmentAccountId);
		if (id != null) {
			if (!id.isPresent()) {
				// id is not null, but not present then there are NO closed periods for the account, return null, don't look it up in the database again
				return null;
			}
			AccountingPeriodClosing bean = dao.findByPrimaryKey(id.getObject());
			// If bean is null, then transaction was rolled back, look it up again and reset the cache
			if (bean != null) {
				return bean;
			}
		}
		AccountingPeriodClosing bean = lookupBean(dao, investmentAccountId);
		setBean(investmentAccountId, bean);
		return bean;
	}


	protected AccountingPeriodClosing lookupBean(ReadOnlyDAO<AccountingPeriodClosing> dao, int investmentAccountId) {
		AccountingPeriodClosingSearchForm searchForm = new AccountingPeriodClosingSearchForm();
		searchForm.setInvestmentAccountId(investmentAccountId);
		searchForm.setClosed(true);
		searchForm.setOrderBy("accountingPeriodEndDate:desc");
		searchForm.setLimit(1);
		List<AccountingPeriodClosing> list = ((AdvancedReadOnlyDAO<AccountingPeriodClosing, Criteria>) dao).findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		return CollectionUtils.getFirstElement(list);
	}


	public void setBean(int investmentAccountId, AccountingPeriodClosing bean) {
		ObjectWrapper<Integer> id = (bean != null ? new ObjectWrapper<>(bean.getId()) : new ObjectWrapper<>());
		getCacheHandler().put(getCacheName(), investmentAccountId, id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<AccountingPeriodClosing> dao, DaoEventTypes event, AccountingPeriodClosing bean) {
		if (event.isUpdate()) { // Only needed for updates in case the key has changed - want to clear the original key so we don't leave bad objects in the cache
			// Call it so we have the original
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<AccountingPeriodClosing> dao, DaoEventTypes event, AccountingPeriodClosing bean, Throwable e) {
		if (e == null) {
			if (event.isUpdate()) {
				AccountingPeriodClosing originalBean = getOriginalBean(dao, bean);
				if (bean.isClosed() != originalBean.isClosed()) {
					getCacheHandler().remove(getCacheName(), getBeanKeyValue(originalBean));
					getCacheHandler().remove(getCacheName(), getBeanKeyValue(bean));
				}
			}
			else {
				getCacheHandler().remove(getCacheName(), getBeanKeyValue(bean));
			}
		}
	}
}
