package com.clifton.accounting.period;


import com.clifton.accounting.period.search.AccountingPeriodClosingExtendedSearchForm;
import com.clifton.accounting.period.search.AccountingPeriodClosingSearchForm;
import com.clifton.accounting.period.search.AccountingPeriodSearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingPeriodService</code> interface defines methods for working with Accounting Periods.
 *
 * @author vgomelsky
 */
public interface AccountingPeriodService {

	////////////////////////////////////////////////////////////////////////////
	////////           AccountingPeriod Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPeriod getAccountingPeriod(int id);


	/**
	 * Returns the AccountingPeriod the given date falls within
	 */
	public AccountingPeriod getAccountingPeriodForDate(Date date);


	public List<AccountingPeriod> getAccountingPeriodList(AccountingPeriodSearchForm searchForm);


	public AccountingPeriod saveAccountingPeriod(AccountingPeriod bean);


	public void deleteAccountingPeriod(int id);


	@SecureMethod(dtoClass = AccountingPeriod.class)
	public Status createAccountingPeriodsForYear(int year);


	////////////////////////////////////////////////////////////////////////////
	////////       AccountingPeriodClosing Business Methods        /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the period that the specified date falls into is closed for the
	 * specified investment account.
	 * <p/>
	 * Returns false otherwise or if the period doesn't exist (means open).
	 */
	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public boolean isAccountingPeriodClosed(int investmentAccountId, Date date);


	public AccountingPeriodClosing getAccountingPeriodClosing(int id);


	public AccountingPeriodClosing getAccountingPeriodClosingByPeriodAndAccount(int accountingPeriodId, int investmentAccountId);


	public List<AccountingPeriodClosing> getAccountingPeriodClosingList(AccountingPeriodClosingSearchForm searchForm);


	/**
	 * Allows updates to AUM field only: periodEndAUM.
	 * If creating a new period closing, only allows creation of open periods with AUM populated.
	 */
	public AccountingPeriodClosing saveAccountingPeriodClosing(AccountingPeriodClosing bean);


	/**
	 * Create the next open AccountingPeriodClosing for the given account
	 * If no closings exist yet for the account, defaults to previous month period
	 * Otherwise, finds the last accounting period closing record for the account and adds the next one
	 */
	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public void createAccountingPeriodNext(int investmentAccountId);


	/**
	 * Close the next non-closed period. If no closed periods exist, close the last period.
	 */
	@RequestMapping("accountingPeriodNextClose")
	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public void closeAccountingPeriodNext(int investmentAccountId, String note);


	/**
	 * Inserts or updates accounting period closing status for the specified investment account.
	 * Validates to make sure that are no open periods prior to the period being closed.
	 */
	@RequestMapping("accountingPeriodClose")
	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public void closeAccountingPeriod(int accountingPeriodId, int investmentAccountId, String note);


	/**
	 * Updates accounting period closing status for the specified investment account.
	 * Validates to make sure that are no closed periods after the period being closed.
	 */
	@RequestMapping("accountingPeriodOpen")
	@SecureMethod(dtoClass = AccountingPeriodClosing.class, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void openAccountingPeriod(int accountingPeriodId, int investmentAccountId, String note);


	/**
	 * Allows deleting an accounting period closing
	 * The one being deleted must be the last one available for the account so we don't create any gaps in the periods
	 * Also validates that all values are blank or 0.  This is used when an accounting period is accidentally created for an account that has been terminated
	 */
	public void deleteAccountingPeriodClosing(int id);


	////////////////////////////////////////////////////////////////////////////
	//////       AccountingPeriodClosing Extended Business Methods        //////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public List<AccountingPeriodClosingExtended> getAccountingPeriodClosingExtendedList(AccountingPeriodClosingExtendedSearchForm searchForm);
}
