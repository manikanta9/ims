package com.clifton.accounting.period.search;


import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * The <code>AccountingPeriodSearchForm</code> class defines search configuration for AccountingPeriod objects.
 *
 * @author vgomelsky
 */
public class AccountingPeriodSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {
	//Currently no AccountingPeriod-specific fields to search on
}
