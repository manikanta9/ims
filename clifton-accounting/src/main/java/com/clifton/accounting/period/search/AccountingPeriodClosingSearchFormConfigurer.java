package com.clifton.accounting.period.search;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;


/**
 * @author manderson
 */
public class AccountingPeriodClosingSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final AccountingPeriodClosingSearchForm accountingPeriodClosingSearchForm;


	public AccountingPeriodClosingSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		super(searchForm, true);
		this.accountingPeriodClosingSearchForm = (AccountingPeriodClosingSearchForm) searchForm;
		configureSearchForm();
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		AccountingPeriodClosingSearchForm searchForm = this.accountingPeriodClosingSearchForm;

		if (searchForm.getExcludeInvestmentAccountGroupId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "aga");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eq("referenceOne.id", searchForm.getExcludeInvestmentAccountGroupId()));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("investmentAccount", criteria) + ".id"));
			criteria.add(Subqueries.notExists(sub));
		}
	}
}
