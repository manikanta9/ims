package com.clifton.accounting.period;


import com.clifton.accounting.period.cache.AccountingPeriodClosingCache;
import com.clifton.accounting.period.search.AccountingPeriodClosingExtendedSearchForm;
import com.clifton.accounting.period.search.AccountingPeriodClosingSearchForm;
import com.clifton.accounting.period.search.AccountingPeriodClosingSearchFormConfigurer;
import com.clifton.accounting.period.search.AccountingPeriodSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingPeriodServiceImpl</code> class provides basic implementation of the AccountingPeriodService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingPeriodServiceImpl implements AccountingPeriodService {

	private AdvancedUpdatableDAO<AccountingPeriod, Criteria> accountingPeriodDAO;
	private AdvancedUpdatableDAO<AccountingPeriodClosing, Criteria> accountingPeriodClosingDAO;

	private AdvancedReadOnlyDAO<AccountingPeriodClosingExtended, Criteria> accountingPeriodClosingExtendedDAO;

	private AccountingPeriodClosingCache accountingPeriodClosingCache;

	private InvestmentAccountService investmentAccountService;


	////////////////////////////////////////////////////////////////////////////
	////////           AccountingPeriod Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingPeriod getAccountingPeriod(int id) {
		return getAccountingPeriodDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingPeriod getAccountingPeriodForDate(Date date) {
		AccountingPeriodSearchForm searchForm = new AccountingPeriodSearchForm();
		searchForm.setActiveOnDate(date);

		List<AccountingPeriod> periodList = getAccountingPeriodList(searchForm);
		if (CollectionUtils.isEmpty(periodList)) {
			throw new ValidationException("Cannot find an accounting period for Date [" + DateUtils.fromDateShort(date) + "]");
		}
		else if (periodList.size() == 1) {
			return periodList.get(0);
		}
		// Should never happen because of validation
		throw new ValidationException("Found [" + periodList.size() + "] accounting periods that apply for Date [" + DateUtils.fromDateShort(date) + "]: "
				+ BeanUtils.getPropertyValues(periodList, "label", ","));
	}


	@Override
	public List<AccountingPeriod> getAccountingPeriodList(AccountingPeriodSearchForm searchForm) {
		return getAccountingPeriodDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingPeriod saveAccountingPeriod(AccountingPeriod bean) {
		// make sure there are no overlaps with existing accounting periods
		List<AccountingPeriod> list = getAccountingPeriodList(new AccountingPeriodSearchForm());
		for (AccountingPeriod period : CollectionUtils.getIterable(list)) {
			if (period.equals(bean)) {
				continue;
			}
			if (DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), period.getStartDate(), period.getEndDate())) {
				throw new ValidationException("Accounting period dates cannot overlap with another period: " + period.getLabel() + ". Please enter a unique date range.");
			}
		}

		return getAccountingPeriodDAO().save(bean);
	}


	@Override
	public void deleteAccountingPeriod(int id) {
		getAccountingPeriodDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////       AccountingPeriodClosing Business Methods        /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isAccountingPeriodClosed(int investmentAccountId, Date date) {
		// get last closed
		AccountingPeriodClosing lastClosed = getAccountingPeriodClosingCache().getAccountingPeriodClosingLastClosedForAccount(getAccountingPeriodClosingDAO(), investmentAccountId);
		if (lastClosed == null) {
			return false;
		}
		return DateUtils.compare(date, lastClosed.getAccountingPeriod().getEndDate(), false) <= 0;
	}


	@Override
	public AccountingPeriodClosing getAccountingPeriodClosing(int id) {
		return getAccountingPeriodClosingDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingPeriodClosing getAccountingPeriodClosingByPeriodAndAccount(int accountingPeriodId, int investmentAccountId) {
		return getAccountingPeriodClosingDAO().findOneByFields(new String[]{"accountingPeriod.id", "investmentAccount.id"}, new Object[]{accountingPeriodId, investmentAccountId});
	}


	@Override
	public List<AccountingPeriodClosing> getAccountingPeriodClosingList(final AccountingPeriodClosingSearchForm searchForm) {
		return getAccountingPeriodClosingDAO().findBySearchCriteria(new AccountingPeriodClosingSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingPeriodClosing saveAccountingPeriodClosing(AccountingPeriodClosing bean) {
		if (bean.isNewBean()) {
			ValidationUtils.assertFalse(bean.isClosed(), "Cannot create 'closed' AccountingPeriodClosing.");
		}
		else {
			// validate existing bean: only AUM / Projected Revenue / External Note and Posted to Portal can be updated
			AccountingPeriodClosing original = getAccountingPeriodClosingDAO().findByPrimaryKey(bean.getId());
			List<String> diffs = CoreCompareUtils.getNoEqualProperties(bean, original, false);
			if (!CollectionUtils.isEmpty(diffs)) {
				// AUM & Projected Revenue are the only fields that can be changed
				diffs.remove("periodEndAUM");
				diffs.remove("preliminaryPeriodEndAUM");
				diffs.remove("periodProjectedRevenue");
				diffs.remove("periodProjectedRevenueAnnualMin");
				diffs.remove("periodProjectedRevenueTotal");
				diffs.remove("periodProjectedRevenueShareExternal");
				diffs.remove("periodProjectedRevenueShareInternal");
				diffs.remove("periodProjectedRevenueNetTotal");
				diffs.remove("externalNote");
				diffs.remove("combinedNote");
				// Also posted to portal can be updated
				diffs.remove("postedToPortal");

				if (!CollectionUtils.isEmpty(diffs)) {
					throw new ValidationException("Cannot update AccountingPeriodClosing because the following fields cannot be changed: " + diffs);
				}
			}
		}

		return getAccountingPeriodClosingDAO().save(bean);
	}


	@Override
	public void createAccountingPeriodNext(int investmentAccountId) {
		// Find Last Period for selected account
		AccountingPeriodClosingSearchForm searchForm = new AccountingPeriodClosingSearchForm();
		searchForm.setInvestmentAccountId(investmentAccountId);
		searchForm.setLimit(1);
		searchForm.setOrderBy("accountingPeriodStartDate:desc");
		List<AccountingPeriodClosing> list = getAccountingPeriodClosingList(searchForm);

		Date nextDate;
		if (CollectionUtils.isEmpty(list)) {
			// No existing periods - defaults to previous month
			nextDate = DateUtils.clearTime(DateUtils.addDays(DateUtils.getFirstDayOfMonth(new Date()), -1));
		}
		else {
			// Otherwise find accounting period for next (by previous period end + 1 day)
			nextDate = DateUtils.addDays(list.get(0).getAccountingPeriod().getEndDate(), 1);
		}
		AccountingPeriod period = getAccountingPeriodForDate(nextDate);
		if (period == null) {
			throw new ValidationException("Missing Accounting Period Defined that is active on [" + DateUtils.fromDateShort(nextDate) + "].  Cannot create next period for this account.");
		}

		AccountingPeriodClosing periodClosing = new AccountingPeriodClosing();
		periodClosing.setAccountingPeriod(period);
		periodClosing.setInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(investmentAccountId));
		saveAccountingPeriodClosing(periodClosing);
	}


	@Override
	public void closeAccountingPeriodNext(int investmentAccountId, String note) {
		// find last closed period for selected account
		AccountingPeriodClosingSearchForm searchForm = new AccountingPeriodClosingSearchForm();
		searchForm.setInvestmentAccountId(investmentAccountId);
		searchForm.setClosed(true);
		searchForm.setLimit(1);
		searchForm.setOrderBy("accountingPeriodStartDate:desc");
		List<AccountingPeriodClosing> list = getAccountingPeriodClosingList(searchForm);

		AccountingPeriod period = null;
		AccountingPeriodSearchForm periodSearchForm = new AccountingPeriodSearchForm();
		if (CollectionUtils.isEmpty(list)) {
			// try to find the first open period
			searchForm = new AccountingPeriodClosingSearchForm();
			searchForm.setInvestmentAccountId(investmentAccountId);
			searchForm.setClosed(false);
			searchForm.setLimit(1);
			searchForm.setOrderBy("accountingPeriodStartDate:asc");
			list = getAccountingPeriodClosingList(searchForm);
			if (CollectionUtils.isEmpty(list)) {
				// first closing: close most recently ended period
				periodSearchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.LESS_THAN, DateUtils.clearTime(new Date())));
				periodSearchForm.setOrderBy("endDate:desc");
			}
			else {
				period = list.get(0).getAccountingPeriod();
			}
		}
		else {
			// close the next open period
			periodSearchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.GREATER_THAN, list.get(0).getAccountingPeriod().getEndDate()));
			periodSearchForm.setOrderBy("endDate:asc");
		}
		if (period == null) {
			periodSearchForm.setLimit(1);
			List<AccountingPeriod> periodList = getAccountingPeriodList(periodSearchForm);
			ValidationUtils.assertNotEmpty(periodList, "Next accounting period to close is not defined.");
			period = periodList.get(0);
		}

		closeAccountingPeriod(period.getId(), investmentAccountId, note);
	}


	@Override
	public void closeAccountingPeriod(int accountingPeriodId, int investmentAccountId, String note) {
		// make sure period and account id's are valid
		AccountingPeriod accountingPeriod = getAccountingPeriod(accountingPeriodId);
		ValidationUtils.assertNotNull(accountingPeriod, "Cannot find accounting period with id = " + accountingPeriodId, "accountingPeriodId");
		InvestmentAccount investmentAccount = getInvestmentAccountService().getInvestmentAccount(investmentAccountId);
		ValidationUtils.assertNotNull(investmentAccount, "Cannot find investment account with id = " + investmentAccountId, "investmentAccountId");

		AccountingPeriodClosing periodClosing = getAccountingPeriodClosingByPeriodAndAccount(accountingPeriodId, investmentAccountId);
		if (periodClosing == null) {
			throw new ValidationException("There are no open periods for this account to close.");
		}
		// make sure not already closed
		ValidationUtils.assertFalse(periodClosing.isClosed(), "Cannot close already closed period.");

		periodClosing.setClosed(true);
		periodClosing.setNote(note);

		// can't close a period that hasn't ended yet
		if (DateUtils.compare(new Date(), accountingPeriod.getEndDate(), false) <= 0) {
			throw new ValidationException("Cannot close Accounting Period that ends after today: " + accountingPeriod.getLabel());
		}

		// make sure there are no open periods for the specified investment account prior to the specified period
		AccountingPeriodClosingSearchForm searchForm = new AccountingPeriodClosingSearchForm();
		searchForm.setInvestmentAccountId(investmentAccountId);
		searchForm.setClosed(false);
		searchForm.addSearchRestriction(new SearchRestriction("accountingPeriodStartDate", ComparisonConditions.LESS_THAN, accountingPeriod.getStartDate()));
		List<AccountingPeriodClosing> openList = getAccountingPeriodClosingList(searchForm);
		if (!CollectionUtils.isEmpty(openList)) {
			throw new ValidationException("Cannot close accounting period " + accountingPeriod.getLabel() + " for " + investmentAccount.getLabel() + " because there are open periods prior to it: "
					+ BeanUtils.getPropertyValues(openList, "label", ", ", "", "", "...", 5));
		}

		// save
		getAccountingPeriodClosingDAO().save(periodClosing);
	}


	@Override
	public void openAccountingPeriod(int accountingPeriodId, int investmentAccountId, String note) {
		// make sure period and account id's are valid
		AccountingPeriod accountingPeriod = getAccountingPeriod(accountingPeriodId);
		ValidationUtils.assertNotNull(accountingPeriod, "Cannot find accounting period with id = " + accountingPeriodId, "accountingPeriodId");
		InvestmentAccount investmentAccount = getInvestmentAccountService().getInvestmentAccount(investmentAccountId);
		ValidationUtils.assertNotNull(investmentAccount, "Cannot find investment account with id = " + investmentAccountId, "investmentAccountId");

		// make sure the record exists: otherwise there's nothing to reopen
		AccountingPeriodClosing periodClosing = getAccountingPeriodClosingByPeriodAndAccount(accountingPeriodId, investmentAccountId);
		ValidationUtils.assertNotNull(periodClosing, "Cannot reopen a period that has not been closed.");

		// require a note that's different than previous note
		ValidationUtils.assertFalse(StringUtils.isEmpty(note), "A note is required when reopening a period.");
		ValidationUtils.assertFalse(StringUtils.compare(periodClosing.getNote(), note) == 0, "A note cannot be the same as previous note.");
		periodClosing.setClosed(false);
		periodClosing.setNote(note);

		// make sure there are no closed periods for the specified investment account after the specified period
		AccountingPeriodClosingSearchForm searchForm = new AccountingPeriodClosingSearchForm();
		searchForm.setInvestmentAccountId(investmentAccountId);
		searchForm.setClosed(true);
		searchForm.addSearchRestriction(new SearchRestriction("accountingPeriodStartDate", ComparisonConditions.GREATER_THAN, accountingPeriod.getStartDate()));
		List<AccountingPeriodClosing> closedList = getAccountingPeriodClosingList(searchForm);
		if (!CollectionUtils.isEmpty(closedList)) {
			throw new ValidationException("Cannot re-open accounting period " + accountingPeriod.getLabel() + " for " + investmentAccount.getLabel() + " because there are closed periods after it: "
					+ BeanUtils.getPropertyValues(closedList, "label", ", ", "", "", "...", 5));
		}

		// save
		getAccountingPeriodClosingDAO().save(periodClosing);
	}


	@Override
	public void deleteAccountingPeriodClosing(int id) {
		AccountingPeriodClosing accountingPeriodClosing = getAccountingPeriodClosing(id);
		if (!MathUtils.isNullOrZero(accountingPeriodClosing.getPeriodEndAUM()) || !MathUtils.isNullOrZero(accountingPeriodClosing.getPreliminaryPeriodEndAUM())
				|| !MathUtils.isNullOrZero(accountingPeriodClosing.getPeriodProjectedRevenue()) || !MathUtils.isNullOrZero(accountingPeriodClosing.getPeriodProjectedRevenueAnnualMin())) {
			throw new ValidationException("Cannot delete selected accounting period because AUM and/or Projected Revenue values exist for the period.");
		}

		// Get the Last Period for the Account
		AccountingPeriodClosingSearchForm searchForm = new AccountingPeriodClosingSearchForm();
		searchForm.setInvestmentAccountId(accountingPeriodClosing.getInvestmentAccount().getId());
		searchForm.setLimit(1);
		searchForm.setOrderBy("accountingPeriodStartDate:desc");
		AccountingPeriodClosing last = CollectionUtils.getFirstElementStrict(getAccountingPeriodClosingList(searchForm));

		ValidationUtils.assertEquals(last.getAccountingPeriod(), accountingPeriodClosing.getAccountingPeriod(), "Cannot delete selected period for this account because it is not the last accounting period for the account.  The last accounting period is [" + last.getAccountingPeriod().getLabel() + "].");

		getAccountingPeriodClosingDAO().delete(id);
	}


	@Override
	public Status createAccountingPeriodsForYear(int year) {
		Date dateCounter = DateUtils.toDate("01/01/" + year);
		AccountingPeriodSearchForm searchForm = new AccountingPeriodSearchForm();
		searchForm.setActiveOnDateRangeStartDate(dateCounter);
		searchForm.setActiveOnDateRangeEndDate(DateUtils.getLastDayOfYear(dateCounter));
		Status status = new Status();
		List<AccountingPeriod> list = getAccountingPeriodList(searchForm);

		while (DateUtils.getYear(dateCounter) == year) {
			AccountingPeriod newPeriod = new AccountingPeriod();
			newPeriod.setStartDate(DateUtils.getFirstDayOfMonth(dateCounter));
			newPeriod.setEndDate(DateUtils.getLastDayOfMonth(dateCounter));
			dateCounter = DateUtils.addMonths(dateCounter, 1);
			boolean foundOverlap = false;
			for (AccountingPeriod existingPeriod : CollectionUtils.getIterable(list)) {
				if (DateUtils.isOverlapInDates(newPeriod.getStartDate(), newPeriod.getEndDate(), existingPeriod.getStartDate(), existingPeriod.getEndDate())) {
					status.addError("Could not create period " + newPeriod.getLabel() + " because it conflicts with " + existingPeriod.getLabel());
					foundOverlap = true;
					break;
				}
			}
			if (!foundOverlap) {
				getAccountingPeriodDAO().save(newPeriod);
			}
		}
		if (status.getErrorCount() > 0) {
			status.setMessage("Unable to create " + status.getErrorCount() + " of the 12 accounting periods for " + year);
		}
		else {
			status.setMessage("Created all accounting periods for the year " + year);
		}
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	//////       AccountingPeriodClosing Extended Business Methods        //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPeriodClosingExtended> getAccountingPeriodClosingExtendedList(final AccountingPeriodClosingExtendedSearchForm searchForm) {
		return getAccountingPeriodClosingExtendedDAO().findBySearchCriteria(new AccountingPeriodClosingSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingPeriod, Criteria> getAccountingPeriodDAO() {
		return this.accountingPeriodDAO;
	}


	public void setAccountingPeriodDAO(AdvancedUpdatableDAO<AccountingPeriod, Criteria> accountingPeriodDAO) {
		this.accountingPeriodDAO = accountingPeriodDAO;
	}


	public AdvancedUpdatableDAO<AccountingPeriodClosing, Criteria> getAccountingPeriodClosingDAO() {
		return this.accountingPeriodClosingDAO;
	}


	public void setAccountingPeriodClosingDAO(AdvancedUpdatableDAO<AccountingPeriodClosing, Criteria> accountingPeriodClosingDAO) {
		this.accountingPeriodClosingDAO = accountingPeriodClosingDAO;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public AccountingPeriodClosingCache getAccountingPeriodClosingCache() {
		return this.accountingPeriodClosingCache;
	}


	public void setAccountingPeriodClosingCache(AccountingPeriodClosingCache accountingPeriodClosingCache) {
		this.accountingPeriodClosingCache = accountingPeriodClosingCache;
	}


	public AdvancedReadOnlyDAO<AccountingPeriodClosingExtended, Criteria> getAccountingPeriodClosingExtendedDAO() {
		return this.accountingPeriodClosingExtendedDAO;
	}


	public void setAccountingPeriodClosingExtendedDAO(AdvancedReadOnlyDAO<AccountingPeriodClosingExtended, Criteria> accountingPeriodClosingExtendedDAO) {
		this.accountingPeriodClosingExtendedDAO = accountingPeriodClosingExtendedDAO;
	}
}
