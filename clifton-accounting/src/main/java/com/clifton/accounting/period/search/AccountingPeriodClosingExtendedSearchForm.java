package com.clifton.accounting.period.search;


import com.clifton.core.dataaccess.search.SearchField;


/**
 * The <code>AccountingPeriodClosingExtendedSearchForm</code> ...
 *
 * @author manderson
 */
public class AccountingPeriodClosingExtendedSearchForm extends AccountingPeriodClosingSearchForm {

	@SearchField(searchFieldPath = "consultantCompany", searchField = "name")
	private String consultantCompanyName;

	@SearchField(searchFieldPath = "fiduciaryCompany", searchField = "name")
	private String fiduciaryCompanyName;

	@SearchField(searchFieldPath = "outsourcedCIOCompany", searchField = "name")
	private String outsourcedCIOCompanyName;

	@SearchField(searchFieldPath = "custodianCompany", searchField = "name")
	private String custodianCompanyName;

	@SearchField(searchFieldPath = "relationshipManagerContact", searchField = "firstName,lastName", sortField = "lastName")
	private String relationshipManagerContactName;

	@SearchField(searchFieldPath = "portfolioManagerContact", searchField = "firstName,lastName", sortField = "lastName")
	private String portfolioManagerContactName;

	@SearchField(searchFieldPath = "newBusinessContact", searchField = "firstName,lastName", sortField = "lastName")
	private String newBusinessContactName;

	@SearchField(searchFieldPath = "investmentAnalystContact", searchField = "firstName,lastName", sortField = "lastName")
	private String investmentAnalystContactName;

	@SearchField
	private Boolean businessClientErisa;

	@SearchField
	private String businessClientAuthority;

	@SearchField
	private String riskDesignation;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getConsultantCompanyName() {
		return this.consultantCompanyName;
	}


	public void setConsultantCompanyName(String consultantCompanyName) {
		this.consultantCompanyName = consultantCompanyName;
	}


	public String getFiduciaryCompanyName() {
		return this.fiduciaryCompanyName;
	}


	public void setFiduciaryCompanyName(String fiduciaryCompanyName) {
		this.fiduciaryCompanyName = fiduciaryCompanyName;
	}


	public Boolean getBusinessClientErisa() {
		return this.businessClientErisa;
	}


	public void setBusinessClientErisa(Boolean businessClientErisa) {
		this.businessClientErisa = businessClientErisa;
	}


	public String getOutsourcedCIOCompanyName() {
		return this.outsourcedCIOCompanyName;
	}


	public void setOutsourcedCIOCompanyName(String outsourcedCIOCompanyName) {
		this.outsourcedCIOCompanyName = outsourcedCIOCompanyName;
	}


	public String getBusinessClientAuthority() {
		return this.businessClientAuthority;
	}


	public void setBusinessClientAuthority(String businessClientAuthority) {
		this.businessClientAuthority = businessClientAuthority;
	}


	public String getCustodianCompanyName() {
		return this.custodianCompanyName;
	}


	public void setCustodianCompanyName(String custodianCompanyName) {
		this.custodianCompanyName = custodianCompanyName;
	}


	public String getRelationshipManagerContactName() {
		return this.relationshipManagerContactName;
	}


	public void setRelationshipManagerContactName(String relationshipManagerContactName) {
		this.relationshipManagerContactName = relationshipManagerContactName;
	}


	public String getPortfolioManagerContactName() {
		return this.portfolioManagerContactName;
	}


	public void setPortfolioManagerContactName(String portfolioManagerContactName) {
		this.portfolioManagerContactName = portfolioManagerContactName;
	}


	public String getInvestmentAnalystContactName() {
		return this.investmentAnalystContactName;
	}


	public void setInvestmentAnalystContactName(String investmentAnalystContactName) {
		this.investmentAnalystContactName = investmentAnalystContactName;
	}


	public String getNewBusinessContactName() {
		return this.newBusinessContactName;
	}


	public void setNewBusinessContactName(String newBusinessContactName) {
		this.newBusinessContactName = newBusinessContactName;
	}


	public String getRiskDesignation() {
		return this.riskDesignation;
	}


	public void setRiskDesignation(String riskDesignation) {
		this.riskDesignation = riskDesignation;
	}
}
