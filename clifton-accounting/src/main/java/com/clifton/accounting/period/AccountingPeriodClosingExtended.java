package com.clifton.accounting.period;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contact.BusinessContact;
import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.math.BigDecimal;


/**
 * The <code>AccountingPeriodClosingExtended</code> ...
 *
 * @author manderson
 */
@NonPersistentObject
public class AccountingPeriodClosingExtended extends AccountingPeriodClosing {

	/**
	 * Generated from the Client-Company Relationships
	 */
	private BusinessCompany consultantCompany;

	/**
	 * Generated from the Client-Company Relationships
	 */
	private BusinessCompany fiduciaryCompany;

	/**
	 * Generated from the Client-Company Relationships
	 */
	private BusinessCompany outsourcedCIOCompany;

	/**
	 * Generated from Client-Contact Relationships
	 */
	private BusinessContact relationshipManagerContact;

	/**
	 * Generated from Client-Contact Relationships
	 */
	private BusinessContact portfolioManagerContact;


	/**
	 * Generated from Client-Contact Relationships  "NBD Client Service Representative"
	 */
	private BusinessContact newBusinessContact;

	/**
	 * Generated from Client-Contact Relationships
	 */
	private BusinessContact investmentAnalystContact;

	/**
	 * Determined from account relationships where Holding Account Type = Custodian and Relationship Purpose = Custodian
	 * Normally there would only be one custodian issuer active for an account, but as a pre-caution will return the issuer
	 * of the first active relationship
	 */
	private BusinessCompany custodianCompany;

	/**
	 * Generated from the Client Contract IMA Clauses
	 * If at least one IMA for the Client has ERISA = true, then the Client is considered to be ERISA = true
	 */
	private boolean businessClientErisa;

	/**
	 * Generated from the Client Contract IMA Clauses
	 * If at least one IMA for the Client has ERISA = true, then the Client is considered to be ERISA = true
	 */
	private String businessClientAuthority;

	/**
	 * Comes from Client Account Dated Custom Field Values
	 * for the Risk Designation as of the last day of the accounting period (last day of the month)
	 */
	private String riskDesignation;

	/**
	 * If any related InvestmentAccounts are wrap accounts this value is true. If there are no related accounts or all wrapAccount values are null for all related accounts this value is false
	 */
	private boolean wrapAccount;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCoalescePeriodEndPreliminaryAUM() {
		if (getPeriodEndAUM() == null) {
			return getPreliminaryPeriodEndAUM();
		}
		return getPeriodEndAUM();
	}


	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getConsultantCompany() {
		return this.consultantCompany;
	}


	public void setConsultantCompany(BusinessCompany consultantCompany) {
		this.consultantCompany = consultantCompany;
	}


	public BusinessCompany getFiduciaryCompany() {
		return this.fiduciaryCompany;
	}


	public void setFiduciaryCompany(BusinessCompany fiduciaryCompany) {
		this.fiduciaryCompany = fiduciaryCompany;
	}


	public BusinessCompany getOutsourcedCIOCompany() {
		return this.outsourcedCIOCompany;
	}


	public void setOutsourcedCIOCompany(BusinessCompany outsourcedCIOCompany) {
		this.outsourcedCIOCompany = outsourcedCIOCompany;
	}


	public BusinessContact getRelationshipManagerContact() {
		return this.relationshipManagerContact;
	}


	public void setRelationshipManagerContact(BusinessContact relationshipManagerContact) {
		this.relationshipManagerContact = relationshipManagerContact;
	}


	public BusinessContact getPortfolioManagerContact() {
		return this.portfolioManagerContact;
	}


	public void setPortfolioManagerContact(BusinessContact portfolioManagerContact) {
		this.portfolioManagerContact = portfolioManagerContact;
	}


	public BusinessContact getNewBusinessContact() {
		return this.newBusinessContact;
	}


	public void setNewBusinessContact(BusinessContact newBusinessContact) {
		this.newBusinessContact = newBusinessContact;
	}


	public BusinessContact getInvestmentAnalystContact() {
		return this.investmentAnalystContact;
	}


	public void setInvestmentAnalystContact(BusinessContact investmentAnalystContact) {
		this.investmentAnalystContact = investmentAnalystContact;
	}


	public BusinessCompany getCustodianCompany() {
		return this.custodianCompany;
	}


	public void setCustodianCompany(BusinessCompany custodianCompany) {
		this.custodianCompany = custodianCompany;
	}


	public boolean isBusinessClientErisa() {
		return this.businessClientErisa;
	}


	public void setBusinessClientErisa(boolean businessClientErisa) {
		this.businessClientErisa = businessClientErisa;
	}


	public String getBusinessClientAuthority() {
		return this.businessClientAuthority;
	}


	public void setBusinessClientAuthority(String businessClientAuthority) {
		this.businessClientAuthority = businessClientAuthority;
	}


	public String getRiskDesignation() {
		return this.riskDesignation;
	}


	public void setRiskDesignation(String riskDesignation) {
		this.riskDesignation = riskDesignation;
	}


	public boolean isWrapAccount() {
		return this.wrapAccount;
	}


	public void setWrapAccount(boolean wrapAccount) {
		this.wrapAccount = wrapAccount;
	}
}
