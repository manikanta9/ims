package com.clifton.accounting.period.report;

import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.document.DocumentManagementService;
import com.clifton.document.DocumentRecord;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentSetupService;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.search.InvestmentGroupSearchForm;
import com.clifton.portal.file.PortalFileDuplicateException;
import com.clifton.report.definition.Report;
import com.clifton.report.definition.ReportDefinitionService;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;
import com.clifton.report.posting.ReportPostingController;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;


/**
 * The <code>AccountingPeriodReportServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class AccountingPeriodReportServiceImpl implements AccountingPeriodReportService {

	private static final String ACCOUNTING_STATEMENT_REPORT_NAME = "Accounting Statement";
	private static final String ACCOUNTING_STATEMENT_REPORT_TEMPLATE_NAME = "Accounting Statement";
	// Non-USD accounts will use investment groups with name prefix + CCY Symbol
	private static final String ACCOUNTING_STATEMENT_REPORT_INVESTMENT_GROUP_NAME = "Accounting Reporting";

	private static final String PORTAL_FILE_GROUP_ACCOUNTING_STATEMENTS = "Investment Statement";
	private static final String PORTAL_FILE_GROUP_ACCOUNTING_STATEMENTS_PRELIMINARY = "Preliminary Investment Statement";
	private static final String PORTAL_FILE_GROUP_ACCOUNTING_RECONCILIATION = "Reconciliation of Market Value";

	private static final String PORTAL_FILE_ACCOUNTING_STATEMENTS_SOURCE_SECTION_NAME = "Accounting Period Closing";

	////////////////////////////////////////////////////////////////////////////////

	private AccountingPeriodService accountingPeriodService;

	private DocumentManagementService documentManagementService;
	private DocumentSetupService documentSetupService;

	private InvestmentGroupService investmentGroupService;

	private ReportDefinitionService reportDefinitionService;
	private ReportExportService reportExportService;
	private ReportPostingController<AccountingPeriodClosing> reportPostingController;


	////////////////////////////////////////////////////////////////////////////////
	//////////          Accounting Period Closing Report Methods          //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadAccountingPeriodClosingReport(int periodClosingId, FileFormats format) {
		return downloadAccountingPeriodClosingReportImpl(getAccountingPeriodService().getAccountingPeriodClosing(periodClosingId), format);
	}


	private FileWrapper downloadAccountingPeriodClosingReportImpl(AccountingPeriodClosing periodClosing, FileFormats format) {
		Report report = getReportDefinitionService().getReportByNameAndTemplate(ACCOUNTING_STATEMENT_REPORT_NAME, ACCOUNTING_STATEMENT_REPORT_TEMPLATE_NAME);
		ValidationUtils.assertNotNull(report, "Missing report with name [" + ACCOUNTING_STATEMENT_REPORT_NAME + "] and template name [" + ACCOUNTING_STATEMENT_REPORT_TEMPLATE_NAME + "]");

		String reportFileName = getReportFileNameForAccountingPeriodClosing(periodClosing);

		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("ClientInvestmentAccountID", periodClosing.getInvestmentAccount().getId());
		parameters.put("ReportingDate", DateUtils.fromDateShort(periodClosing.getAccountingPeriod().getEndDate()));
		InvestmentGroup investmentGroup = getAccountingPeriodClosingReportInvestmentGroupParameter(periodClosing);
		if (investmentGroup != null) {
			parameters.put("InvestmentGroupID", investmentGroup.getId());
		}

		ReportExportConfiguration reportExportConfiguration = new ReportExportConfiguration(report.getId(), parameters, true, true);
		reportExportConfiguration.setExportFormat(format == null ? FileFormats.PDF : format);
		reportExportConfiguration.setReportFileName(reportFileName);

		return getReportExportService().downloadReportFile(reportExportConfiguration);
	}


	/**
	 * Returns the Investment Group that is Accounting Reporting or has template of Accounting Reporting that matches the account base currency
	 * Throws exception if not found.
	 */
	private InvestmentGroup getAccountingPeriodClosingReportInvestmentGroupParameter(AccountingPeriodClosing periodClosing) {
		InvestmentSecurity accountBaseCurrency = periodClosing.getInvestmentAccount().getBaseCurrency();
		InvestmentGroup investmentGroup = getInvestmentGroupService().getInvestmentGroupByName(ACCOUNTING_STATEMENT_REPORT_INVESTMENT_GROUP_NAME);
		ValidationUtils.assertNotNull(investmentGroup, "Missing instrument group [" + ACCOUNTING_STATEMENT_REPORT_INVESTMENT_GROUP_NAME + "]");

		// USD accounts are the default
		if (CompareUtils.isEqual(investmentGroup.getBaseCurrency(), accountBaseCurrency)) {
			return investmentGroup;
		}

		// Non-USD accounts - Find groups using Accounting Reporting as a template with the same base currency as the account
		// Note: If the group is missing throw an exception - it should be set up appropriately (per Jane)
		InvestmentGroupSearchForm searchForm = new InvestmentGroupSearchForm();
		searchForm.setTemplateInvestmentGroupId(investmentGroup.getId());
		searchForm.setBaseCurrencyId(accountBaseCurrency.getId());
		List<InvestmentGroup> investmentGroupList = getInvestmentGroupService().getInvestmentGroupList(searchForm);

		ValidationUtils.assertNotEmpty(investmentGroupList, "Missing Instrument Group with template [" + ACCOUNTING_STATEMENT_REPORT_INVESTMENT_GROUP_NAME + "] and base currency [" + accountBaseCurrency.getSymbol() + "]. This group is expected to be set up in order to accurately pull the accounting statement for accounts using that base currency");
		ValidationUtils.assertFalse(investmentGroupList.size() > 1, "Multiple Instrument Groups found using template [" + ACCOUNTING_STATEMENT_REPORT_INVESTMENT_GROUP_NAME + "] and base currency [" + accountBaseCurrency.getSymbol() + "]: " + StringUtils.collectionToCommaDelimitedString(investmentGroupList, InvestmentGroup::getName));
		return investmentGroupList.get(0);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////          Accounting Period Closing Posting Methods          //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void postAccountingPeriodClosingReportReplace(int periodClosingId, FileFormats format) {
		postAccountingPeriodClosingReport(periodClosingId, format, FileDuplicateActions.REPLACE);
	}


	@Override
	public void postAccountingPeriodClosingReport(int periodClosingId, FileFormats format, FileDuplicateActions fileDuplicateAction) {
		AccountingPeriodClosing periodClosing = getAccountingPeriodService().getAccountingPeriodClosing(periodClosingId);

		try {
			// If no format defined, post both xls and pdf
			if (format == null) {
				postAccountingPeriodClosingReportImpl(periodClosing, FileFormats.EXCEL, fileDuplicateAction);
				postAccountingPeriodClosingReportImpl(periodClosing, FileFormats.PDF, fileDuplicateAction);
			}
			// Otherwise just the format supplied
			else {
				postAccountingPeriodClosingReportImpl(periodClosing, format, fileDuplicateAction);
			}
		}
		catch (Throwable e) {
			Throwable original = ExceptionUtils.getOriginalException(e);
			if (original instanceof PortalFileDuplicateException) {
				if (((PortalFileDuplicateException) original).isReplaceAllowed()) {
					throw new UserIgnorableValidationException(getClass(), "postAccountingPeriodClosingReportReplace", original.getMessage() + "<br><br>Note: Clicking Yes will post the file(s) using the Replace option.");
				}
			}
			throw e;
		}
	}


	private void postAccountingPeriodClosingReportImpl(AccountingPeriodClosing periodClosing, FileFormats format, FileDuplicateActions fileDuplicateAction) {
		// First Get the File
		FileWrapper report = downloadAccountingPeriodClosingReportImpl(periodClosing, format);

		if (report == null || report.getFile() == null) {
			throw new ValidationException("Unable to Generate Accounting Statement Report: Period Closing ID: " + periodClosing.getId() + " Format " + format);
		}


		ReportPostingFileConfiguration postingFileConfiguration = populateReportPostingFileConfigurationForAccountingPeriodClosing(periodClosing, false, fileDuplicateAction);
		postingFileConfiguration.setFileWrapper(report);
		getReportPostingController().postClientFile(postingFileConfiguration, periodClosing);
	}


	@Override
	public void postAccountingPeriodClosingAttachmentsReplace(int periodClosingId) {
		postAccountingPeriodClosingAttachments(periodClosingId, FileDuplicateActions.REPLACE);
	}


	@Override
	public void postAccountingPeriodClosingAttachments(int periodClosingId, FileDuplicateActions fileDuplicateAction) {
		List<DocumentFile> documentFileList = getDocumentSetupService().getDocumentFileListForEntity(AccountingPeriodClosing.ACCOUNTING_PERIOD_CLOSING_TABLE_NAME, MathUtils.getNumberAsLong(periodClosingId));
		documentFileList = BeanUtils.filter(documentFileList, DocumentFile::isActive, true);
		if (CollectionUtils.isEmpty(documentFileList)) {
			throw new ValidationException("No active file attachments available to post.");
		}

		AccountingPeriodClosing periodClosing = getAccountingPeriodService().getAccountingPeriodClosing(periodClosingId);

		try {

			for (DocumentFile documentFile : documentFileList) {
				DocumentRecord record = getDocumentManagementService().getDocumentRecord(DocumentFile.DOCUMENT_FILE_TABLE_NAME, documentFile.getId());
				// Might be missing file in doc system.  This would be likely to happen if not running in a Production environment
				if (record != null && !StringUtils.isEmpty(record.getDocumentId())) {
					ReportPostingFileConfiguration postingFileConfiguration = populateReportPostingFileConfigurationForAccountingPeriodClosing(periodClosing, true, fileDuplicateAction);
					postingFileConfiguration.setDisplayName(documentFile.getName());
					postingFileConfiguration.setFileWrapper(getDocumentManagementService().downloadDocumentRecord(record.getDocumentId()));
					getReportPostingController().postClientFile(postingFileConfiguration, periodClosing);
				}
				else {
					throw new ValidationException("Cannot find file for " + documentFile.getName());
				}
			}
		}
		catch (Throwable e) {
			Throwable original = ExceptionUtils.getOriginalException(e);
			if (original instanceof PortalFileDuplicateException) {
				if (((PortalFileDuplicateException) original).isReplaceAllowed()) {
					throw new UserIgnorableValidationException(getClass(), "postAccountingPeriodClosingAttachmentsReplace", original.getMessage() + "<br><br>Note: Clicking Yes will post the file(s) using the Replace option.");
				}
			}
			throw e;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private ReportPostingFileConfiguration populateReportPostingFileConfigurationForAccountingPeriodClosing(AccountingPeriodClosing periodClosing, boolean reconciliation, FileDuplicateActions fileDuplicateAction) {
		ReportPostingFileConfiguration postingFileConfiguration = new ReportPostingFileConfiguration();
		postingFileConfiguration.setFileDuplicateAction(fileDuplicateAction);
		if (reconciliation) {
			postingFileConfiguration.setPortalFileCategoryName(PORTAL_FILE_GROUP_ACCOUNTING_RECONCILIATION);
		}
		else if (periodClosing.isClosed()) {
			postingFileConfiguration.setPortalFileCategoryName(PORTAL_FILE_GROUP_ACCOUNTING_STATEMENTS);
		}
		else {
			postingFileConfiguration.setPortalFileCategoryName(PORTAL_FILE_GROUP_ACCOUNTING_STATEMENTS_PRELIMINARY);
		}

		postingFileConfiguration.setPostEntitySourceTableName(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME);
		postingFileConfiguration.setPostEntitySourceFkFieldId(periodClosing.getInvestmentAccount().getId());

		postingFileConfiguration.setSourceEntitySectionName(PORTAL_FILE_ACCOUNTING_STATEMENTS_SOURCE_SECTION_NAME);
		postingFileConfiguration.setSourceEntityFkFieldId(periodClosing.getId());
		postingFileConfiguration.setReportDate(periodClosing.getAccountingPeriod().getEndDate());
		return postingFileConfiguration;
	}


	private String getReportFileNameForAccountingPeriodClosing(AccountingPeriodClosing accountingPeriodClosing) {
		StringBuilder reportFileName = new StringBuilder(20);
		reportFileName.append(accountingPeriodClosing.getInvestmentAccount().getNumber());
		reportFileName.append(".");
		reportFileName.append(accountingPeriodClosing.getInvestmentAccount().getName());
		reportFileName.append(".");
		reportFileName.append(DateUtils.fromDate(accountingPeriodClosing.getAccountingPeriod().getEndDate(), "yyyy.MM"));
		if (!accountingPeriodClosing.isClosed()) {
			reportFileName.append("-Preliminary");
		}
		return reportFileName.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public ReportDefinitionService getReportDefinitionService() {
		return this.reportDefinitionService;
	}


	public void setReportDefinitionService(ReportDefinitionService reportDefinitionService) {
		this.reportDefinitionService = reportDefinitionService;
	}


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public ReportPostingController<AccountingPeriodClosing> getReportPostingController() {
		return this.reportPostingController;
	}


	public void setReportPostingController(ReportPostingController<AccountingPeriodClosing> reportPostingController) {
		this.reportPostingController = reportPostingController;
	}
}
