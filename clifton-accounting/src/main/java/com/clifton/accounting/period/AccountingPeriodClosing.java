package com.clifton.accounting.period;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.document.DocumentFileAware;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portal.file.PortalFileAware;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>AccountingPeriodClosing</code> class identifies whether a period is closed for a given
 * client investment account.  AccountingPeriod + InvestmentAccount must be unique.
 * <p/>
 * There should be business logic that doesn't allow re-opening of a period if there are closed periods
 * for the same client account after this one.  Or re-closing a period if there are open periods before.
 *
 * @author vgomelsky
 */
public class AccountingPeriodClosing extends BaseEntity<Integer> implements LabeledObject, DocumentFileAware, PortalFileAware {

	public static final String ACCOUNTING_PERIOD_CLOSING_TABLE_NAME = "AccountingPeriodClosing";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private AccountingPeriod accountingPeriod;
	private InvestmentAccount investmentAccount;

	private boolean closed;
	/**
	 * Optional note which we may require to be populated when re-opening a closed period and re-closing one.
	 */
	private String note;

	/**
	 * Optional note that applies to AUM/Projected revenue and included in files
	 * generates for PPA
	 */
	private String externalNote;

	/**
	 * Asset Under Management at the end of this accounting period.
	 * Not sure if this is the best place to store AUM. Until we improve and automate the process, it's the closest.
	 */
	private BigDecimal periodEndAUM;

	/**
	 * AUM preliminary estimates for the account - Value estimate done before accounts have been reconciled
	 */
	private BigDecimal preliminaryPeriodEndAUM;

	/**
	 * Projected Revenue for the accounting period.
	 * Note: Revenue estimates for most accounts using Billing Definition Setup to generate
	 * invoices for the quarter using this period's billing basis.  The invoice is then prorated for the number
	 * of days in the accounting period (month) as well as billing start/end dates to determine the portion that applies
	 * to the period only.  Note: If the invoice is set up to run monthly the prorating is not necessary.
	 * <p/>
	 * This amount DOES NOT include Annual Min schedules (applied separately) as those types of schedules
	 * make historical comparisons difficult. i.e. 3rd Quarter is the Clients Annual minimum of 25,000  Client has only been billed YTD 10,000
	 * That quarter would result in an additional 15,000 (or 5,000 per month) which has nothing to do with billing basis, etc.
	 */
	private BigDecimal periodProjectedRevenue;

	/**
	 * Annual Minimum portion of the projected revenue for the period
	 * Annual Min schedules (applied separately) as those types of schedules
	 * make historical comparisons difficult. i.e. 3rd Quarter is the Clients Annual minimum of 25,000  Client has only been billed YTD 10,000
	 * That quarter would result in an additional 15,000 (or 5,000 per month) which has nothing to do with billing basis, etc.
	 */
	private BigDecimal periodProjectedRevenueAnnualMin;


	/**
	 * Projected External Revenue Share for the accounting period. a.k.a. Broker Fee
	 * This also uses billing definitions to generate amounts as defined above, however this applies to external revenue share type invoices.
	 * These invoices depend on the client revenue.
	 * For example: We have an agreement with Merrill that for specific accounts we charge the client 5 basis points and Merrill gets 2 basis points of that total.
	 * This portion is NOT counted towards Gross Revenue, Only Net Revenue
	 */
	private BigDecimal periodProjectedRevenueShareExternal;


	/**
	 * Projected Internal Revenue Share for the accounting period. a.k.a. Revenue Share, EV Portion of the Revenue
	 * This also uses billing definitions to generate amounts as defined above, however this applies to internal revenue share type invoices.
	 * These invoices depend on the client revenue.
	 * For example: The client is either shared with EV, or EV is responsible for selling the account, so we share the portion of the fees paid with them
	 */
	private BigDecimal periodProjectedRevenueShareInternal;


	private boolean postedToPortal;


	///////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(100);
		if (this.accountingPeriod != null) {
			result.append(this.accountingPeriod.getLabel());
		}
		result.append(" is ");
		result.append(this.closed ? "CLOSED" : "OPEN");
		result.append(" for ");
		if (this.investmentAccount != null) {
			result.append(this.investmentAccount.getLabel());
		}
		return result.toString();
	}


	/**
	 * Gross Revenue Only
	 */
	public BigDecimal getPeriodProjectedRevenueTotal() {
		return MathUtils.add(getPeriodProjectedRevenue(), getPeriodProjectedRevenueAnnualMin());
	}


	/**
	 * Net Revenue: Gross + Broker Fee
	 * Internal Revenue Share is NOT included
	 */
	public BigDecimal getPeriodProjectedRevenueNetTotal() {
		return MathUtils.add(getPeriodProjectedRevenueTotal(), getPeriodProjectedRevenueShareExternal());
	}


	public String getCombinedNote() {
		StringBuilder sb = new StringBuilder(16);
		if (!StringUtils.isEmpty(getNote())) {
			sb.append("<b>Accounting Notes:</b> ").append(getNote());
			if (!StringUtils.isEmpty(getExternalNote())) {
				sb.append("<br>");
			}
		}
		if (!StringUtils.isEmpty(getExternalNote())) {
			sb.append("<b>AUM/Projected Revenue Notes:</b> ").append(getExternalNote());
		}
		return sb.toString();
	}


	///////////////////////////////////////////////////////


	public AccountingPeriod getAccountingPeriod() {
		return this.accountingPeriod;
	}


	public void setAccountingPeriod(AccountingPeriod accountingPeriod) {
		this.accountingPeriod = accountingPeriod;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public boolean isClosed() {
		return this.closed;
	}


	public void setClosed(boolean closed) {
		this.closed = closed;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public BigDecimal getPeriodEndAUM() {
		return this.periodEndAUM;
	}


	public void setPeriodEndAUM(BigDecimal periodEndAUM) {
		this.periodEndAUM = periodEndAUM;
	}


	public BigDecimal getPreliminaryPeriodEndAUM() {
		return this.preliminaryPeriodEndAUM;
	}


	public void setPreliminaryPeriodEndAUM(BigDecimal preliminaryPeriodEndAUM) {
		this.preliminaryPeriodEndAUM = preliminaryPeriodEndAUM;
	}


	public BigDecimal getPeriodProjectedRevenue() {
		return this.periodProjectedRevenue;
	}


	public void setPeriodProjectedRevenue(BigDecimal periodProjectedRevenue) {
		this.periodProjectedRevenue = periodProjectedRevenue;
	}


	public BigDecimal getPeriodProjectedRevenueAnnualMin() {
		return this.periodProjectedRevenueAnnualMin;
	}


	public void setPeriodProjectedRevenueAnnualMin(BigDecimal periodProjectedRevenueAnnualMin) {
		this.periodProjectedRevenueAnnualMin = periodProjectedRevenueAnnualMin;
	}


	public String getExternalNote() {
		return this.externalNote;
	}


	public void setExternalNote(String externalNote) {
		this.externalNote = externalNote;
	}


	@Override
	public boolean isPostedToPortal() {
		return this.postedToPortal;
	}


	@Override
	public void setPostedToPortal(boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}


	public BigDecimal getPeriodProjectedRevenueShareExternal() {
		return this.periodProjectedRevenueShareExternal;
	}


	public void setPeriodProjectedRevenueShareExternal(BigDecimal periodProjectedRevenueShareExternal) {
		this.periodProjectedRevenueShareExternal = periodProjectedRevenueShareExternal;
	}


	public BigDecimal getPeriodProjectedRevenueShareInternal() {
		return this.periodProjectedRevenueShareInternal;
	}


	public void setPeriodProjectedRevenueShareInternal(BigDecimal periodProjectedRevenueShareInternal) {
		this.periodProjectedRevenueShareInternal = periodProjectedRevenueShareInternal;
	}
}
