package com.clifton.accounting.period;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>AccountingPeriod</code> class represents an accounting period (usually a month).
 * There should be no overlaps in period dates.
 *
 * @author vgomelsky
 */
public class AccountingPeriod extends BaseEntity<Integer> implements LabeledObject {

	private Date startDate;
	private Date endDate;


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(25);
		result.append('[');
		result.append(DateUtils.fromDateShort(this.startDate));
		result.append(" - ");
		result.append(DateUtils.fromDateShort(this.endDate));
		result.append(']');
		return result.toString();
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
