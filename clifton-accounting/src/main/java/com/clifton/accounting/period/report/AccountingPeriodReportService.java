package com.clifton.accounting.period.report;

import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;


/**
 * @author manderson
 */
public interface AccountingPeriodReportService {

	////////////////////////////////////////////////////////////////////////////////
	//////////          Accounting Period Closing Report Methods          //////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public FileWrapper downloadAccountingPeriodClosingReport(int periodClosingId, FileFormats format);


	////////////////////////////////////////////////////////////////////////////////
	/////////          Accounting Period Closing Posting Methods          //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Post the Accounting Reports (Investment Statement) to the portal in the selected format (or both PDF and Excel if no format supplied)
	 * This will replace unapproved files and error out if the file exists already and is approved.  Called by a user ignorable exception when duplicate is found
	 * by postAccountingPeriodClosingReport and the option is available to replace the file
	 */
	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public void postAccountingPeriodClosingReportReplace(int periodClosingId, FileFormats format);


	/**
	 * Post the Accounting Reports (Investment Statement) to the portal in the selected format (or both PDF and Excel if no format supplied)
	 */
	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public void postAccountingPeriodClosingReport(int periodClosingId, FileFormats format, FileDuplicateActions fileDuplicateAction);


	/**
	 * Post the Accounting Reports Supplemental Docs (Reconciliation of Market value) to the portal for the Client File Attachments associated with the period closing
	 * This will replace unapproved files and error out if the file exists already and is approved.  Called by a user ignorable exception when duplicate is found
	 * by postAccountingPeriodClosingAttachments and the option is available to replace the file
	 */
	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public void postAccountingPeriodClosingAttachmentsReplace(int periodClosingId);


	/**
	 * Post the Accounting Reports Supplemental Docs (Reconciliation of Market value) to the portal for the Client File Attachments associated with the period closing
	 */
	@SecureMethod(dtoClass = AccountingPeriodClosing.class)
	public void postAccountingPeriodClosingAttachments(int periodClosingId, FileDuplicateActions fileDuplicateAction);
}
