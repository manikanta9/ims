package com.clifton.accounting.period.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingPeriodClosingSearchForm</code> class defines search configuration for AccountingPeriodClosing objects.
 *
 * @author vgomelsky
 */
public class AccountingPeriodClosingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "accountingPeriod.id")
	private Integer accountingPeriodId;

	@SearchField(searchField = "startDate", searchFieldPath = "accountingPeriod")
	private Date accountingPeriodStartDate;

	@SearchField(searchField = "endDate", searchFieldPath = "accountingPeriod")
	private Date accountingPeriodEndDate;

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.id", sortField = "investmentAccount.number", comparisonConditions = ComparisonConditions.IN)
	private Integer[] investmentAccountIds;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	// Custom Search Field: Not Exists
	private Integer excludeInvestmentAccountGroupId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "investmentAccount")
	private Integer investmentAccountIssuingCompanyId;

	@SearchField(searchField = "number,name", searchFieldPath = "investmentAccount", sortField = "number")
	private String investmentAccountNumberOrName;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "baseCurrency.id", sortField = "baseCurrency.symbol")
	private Integer investmentAccountBaseCurrencyId;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "inceptionDate")
	private Date investmentAccountInceptionDate;

	@SearchField(searchField = "wrapAccount")
	private Boolean wrapAccount;

	@SearchField(searchField = "name", searchFieldPath = "investmentAccount.workflowState", comparisonConditions = {ComparisonConditions.BEGINS_WITH, ComparisonConditions.LIKE})
	private String investmentAccountWorkflowStateName;

	@SearchField(searchField = "name", searchFieldPath = "investmentAccount.workflowStatus", comparisonConditions = {ComparisonConditions.BEGINS_WITH, ComparisonConditions.LIKE})
	private String investmentAccountWorkflowStatusName;

	@SearchField(searchField = "discretionType", searchFieldPath = "investmentAccount")
	private String investmentAccountDiscretionType;

	@SearchField(searchField = "clientAccountType", searchFieldPath = "investmentAccount")
	private String clientAccountType;

	@SearchField(searchField = "text", searchFieldPath = "investmentAccount.businessClient.clientType")
	private String clientTypeName;

	@SearchField(searchField = "value,text", searchFieldPath = "investmentAccount.externalPortfolioCode", sortField = "text")
	private String investmentAccountExternalPortfolioCodeName;

	@SearchField(searchField = "value,text", searchFieldPath = "investmentAccount.externalProductCode", sortField = "text")
	private String investmentAccountExternalProductCodeName;

	@SearchField(searchField = "value,text", searchFieldPath = "investmentAccount.clientAccountChannel", sortField = "text")
	private String investmentAccountChannelName;

	@SearchField(searchField = "federalTaxNumber", searchFieldPath = "investmentAccount.businessClient")
	private String federalTaxNumber;

	@SearchField(searchField = "taxable", searchFieldPath = "investmentAccount.businessClient")
	private Boolean taxable;

	@SearchField(searchField = "state", searchFieldPath = "investmentAccount.businessClient.company")
	private String clientAddressState;

	@SearchField(searchField = "businessService.id", searchFieldPath = "investmentAccount")
	private Short businessServiceId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "investmentAccount.businessService.id,investmentAccount.businessService.parent.id,investmentAccount.businessService.parent.parent.id,investmentAccount.businessService.parent.parent.parent.id", leftJoin = true, sortField = "investmentAccount.businessService.name")
	private Short businessServiceOrParentId;

	@SearchField(searchField = "businessServiceList.businessService.id", searchFieldPath = "investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Short businessServiceComponentId;

	@SearchField(searchField = "serviceProcessingType.id", searchFieldPath = "investmentAccount")
	private Short businessServiceProcessingTypeId;

	@SearchField(searchField = "name", searchFieldPath = "investmentAccount.businessService.application")
	private String businessServiceApplicationName;

	@SearchField(searchField = "teamSecurityGroup.id", searchFieldPath = "investmentAccount")
	private Short teamSecurityGroupId;

	@SearchField(searchField = "name", searchFieldPath = "investmentAccount.businessClient")
	private String clientName;

	@SearchField(searchField = "category.id", searchFieldPath = "investmentAccount.businessClient", sortField = "category.name")
	private Short clientCategoryId;

	@SearchField(searchField = "name", searchFieldPath = "investmentAccount.businessClient.clientRelationship")
	private String clientRelationshipName;

	@SearchField(searchFieldPath = "investmentAccount.businessClient.clientRelationship", searchField = "relationshipCategory.id", sortField = "relationshipCategory.name")
	private Short clientRelationshipCategoryId;

	@SearchField(searchFieldPath = "investmentAccount.businessClient.clientRelationship.relationshipCategory", searchField = "name")
	private String clientRelationshipCategoryName;

	@SearchField(searchFieldPath = "investmentAccount.businessClient.clientRelationship.relationshipCategory", searchField = "retail", searchFieldCustomType = SearchFieldCustomTypes.COALESCE_FALSE, leftJoin = true)
	private Boolean clientRelationshipCategoryRetail;

	@SearchField(searchFieldPath = "investmentAccount.businessClient.clientRelationship.relationshipType", searchField = "text")
	private String clientRelationshipTypeName;

	@SearchField(searchFieldPath = "investmentAccount.businessClient.clientRelationship.relationshipSubType", searchField = "text")
	private String clientRelationshipSubTypeName;

	// Note: Uses Left Join so we can include Sister Clients or other accounts that don't use relationships
	@SearchField(searchFieldPath = "investmentAccount.businessClient.clientRelationship", searchField = "inceptionDate", leftJoin = true)
	private Date clientRelationshipInceptionDate;

	// Note: Uses Left Join so we can include Sister Clients or other accounts that don't use relationships
	// Can use the above (UI), but for the query export we need this field and comparison defined explicitly (value = AUM Date)
	@SearchField(searchFieldPath = "investmentAccount.businessClient.clientRelationship", searchField = "inceptionDate", leftJoin = true, comparisonConditions = {ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL})
	private Date maxClientRelationshipInceptionDate;

	// Note: Uses Left Join so we can include Sister Clients or other accounts that don't use relationships
	@SearchField(searchFieldPath = "investmentAccount.businessClient.clientRelationship", searchField = "terminateDate", leftJoin = true)
	private Date clientRelationshipTerminateDate;

	// Note: Uses Left Join so we can include Sister Clients or other accounts that don't use relationships
	// Can use the above (UI), but for the query export we need this field and comparison defined explicitly (value = First Day of AUM Date Month)
	@SearchField(searchFieldPath = "investmentAccount.businessClient.clientRelationship", searchField = "terminateDate", leftJoin = true, comparisonConditions = {ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL})
	private Date minClientRelationshipTerminateDate;


	@SearchField(searchField = "name", searchFieldPath = "investmentAccount.businessClient.clientRelationship.workflowState", comparisonConditions = {ComparisonConditions.BEGINS_WITH, ComparisonConditions.LIKE})
	private String clientRelationshipWorkflowStateName;

	@SearchField(searchField = "name", searchFieldPath = "investmentAccount.businessClient.clientRelationship.workflowStatus", comparisonConditions = {ComparisonConditions.BEGINS_WITH, ComparisonConditions.LIKE})
	private String clientRelationshipWorkflowStatusName;

	@SearchField
	private BigDecimal periodEndAUM;

	@SearchField
	private BigDecimal preliminaryPeriodEndAUM;

	@SearchField(searchField = "periodEndAUM,preliminaryPeriodEndAUM", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal coalescePeriodEndPreliminaryAUM;

	@SearchField
	private BigDecimal periodProjectedRevenue;

	@SearchField
	private BigDecimal periodProjectedRevenueAnnualMin;

	@SearchField
	private BigDecimal periodProjectedRevenueShareExternal;

	@SearchField
	private BigDecimal periodProjectedRevenueShareInternal;

	@SearchField(searchField = "periodProjectedRevenue,periodProjectedRevenueAnnualMin", searchFieldCustomType = SearchFieldCustomTypes.MATH_ADD_NULLABLE)
	private BigDecimal periodProjectedRevenueTotal;

	@SearchField(searchField = "periodProjectedRevenue,periodProjectedRevenueAnnualMin,periodProjectedRevenueShareExternal", searchFieldCustomType = SearchFieldCustomTypes.MATH_ADD_NULLABLE)
	private BigDecimal periodProjectedRevenueNetTotal;

	@SearchField
	private Boolean closed;

	@SearchField
	private String note;

	@SearchField
	private String externalNote;

	@SearchField(searchField = "note,externalNote")
	private String combinedNote;

	@SearchField
	private Boolean postedToPortal;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "aumCalculatorOverrideBean.id,businessService.aumCalculatorBean.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private Integer investmentAccountAumCalculatorBeanId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getAccountingPeriodId() {
		return this.accountingPeriodId;
	}


	public void setAccountingPeriodId(Integer accountingPeriodId) {
		this.accountingPeriodId = accountingPeriodId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Boolean getClosed() {
		return this.closed;
	}


	public void setClosed(Boolean closed) {
		this.closed = closed;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getInvestmentAccountWorkflowStatusName() {
		return this.investmentAccountWorkflowStatusName;
	}


	public void setInvestmentAccountWorkflowStatusName(String investmentAccountWorkflowStatusName) {
		this.investmentAccountWorkflowStatusName = investmentAccountWorkflowStatusName;
	}


	public String getInvestmentAccountNumberOrName() {
		return this.investmentAccountNumberOrName;
	}


	public void setInvestmentAccountNumberOrName(String investmentAccountNumberOrName) {
		this.investmentAccountNumberOrName = investmentAccountNumberOrName;
	}


	public Date getAccountingPeriodStartDate() {
		return this.accountingPeriodStartDate;
	}


	public void setAccountingPeriodStartDate(Date accountingPeriodStartDate) {
		this.accountingPeriodStartDate = accountingPeriodStartDate;
	}


	public Date getAccountingPeriodEndDate() {
		return this.accountingPeriodEndDate;
	}


	public void setAccountingPeriodEndDate(Date accountingPeriodEndDate) {
		this.accountingPeriodEndDate = accountingPeriodEndDate;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public BigDecimal getPeriodEndAUM() {
		return this.periodEndAUM;
	}


	public void setPeriodEndAUM(BigDecimal periodEndAUM) {
		this.periodEndAUM = periodEndAUM;
	}


	public BigDecimal getPreliminaryPeriodEndAUM() {
		return this.preliminaryPeriodEndAUM;
	}


	public void setPreliminaryPeriodEndAUM(BigDecimal preliminaryPeriodEndAUM) {
		this.preliminaryPeriodEndAUM = preliminaryPeriodEndAUM;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public String getClientTypeName() {
		return this.clientTypeName;
	}


	public void setClientTypeName(String clientTypeName) {
		this.clientTypeName = clientTypeName;
	}


	public Boolean getTaxable() {
		return this.taxable;
	}


	public void setTaxable(Boolean taxable) {
		this.taxable = taxable;
	}


	public Short getBusinessServiceProcessingTypeId() {
		return this.businessServiceProcessingTypeId;
	}


	public void setBusinessServiceProcessingTypeId(Short businessServiceProcessingTypeId) {
		this.businessServiceProcessingTypeId = businessServiceProcessingTypeId;
	}


	public Integer getInvestmentAccountBaseCurrencyId() {
		return this.investmentAccountBaseCurrencyId;
	}


	public void setInvestmentAccountBaseCurrencyId(Integer investmentAccountBaseCurrencyId) {
		this.investmentAccountBaseCurrencyId = investmentAccountBaseCurrencyId;
	}


	public BigDecimal getPeriodProjectedRevenue() {
		return this.periodProjectedRevenue;
	}


	public void setPeriodProjectedRevenue(BigDecimal periodProjectedRevenue) {
		this.periodProjectedRevenue = periodProjectedRevenue;
	}


	public BigDecimal getPeriodProjectedRevenueAnnualMin() {
		return this.periodProjectedRevenueAnnualMin;
	}


	public void setPeriodProjectedRevenueAnnualMin(BigDecimal periodProjectedRevenueAnnualMin) {
		this.periodProjectedRevenueAnnualMin = periodProjectedRevenueAnnualMin;
	}


	public BigDecimal getPeriodProjectedRevenueTotal() {
		return this.periodProjectedRevenueTotal;
	}


	public void setPeriodProjectedRevenueTotal(BigDecimal periodProjectedRevenueTotal) {
		this.periodProjectedRevenueTotal = periodProjectedRevenueTotal;
	}


	public Integer getInvestmentAccountIssuingCompanyId() {
		return this.investmentAccountIssuingCompanyId;
	}


	public void setInvestmentAccountIssuingCompanyId(Integer investmentAccountIssuingCompanyId) {
		this.investmentAccountIssuingCompanyId = investmentAccountIssuingCompanyId;
	}


	public String getInvestmentAccountDiscretionType() {
		return this.investmentAccountDiscretionType;
	}


	public void setInvestmentAccountDiscretionType(String investmentAccountDiscretionType) {
		this.investmentAccountDiscretionType = investmentAccountDiscretionType;
	}


	public String getBusinessServiceApplicationName() {
		return this.businessServiceApplicationName;
	}


	public void setBusinessServiceApplicationName(String businessServiceApplicationName) {
		this.businessServiceApplicationName = businessServiceApplicationName;
	}


	public String getExternalNote() {
		return this.externalNote;
	}


	public void setExternalNote(String externalNote) {
		this.externalNote = externalNote;
	}


	public String getCombinedNote() {
		return this.combinedNote;
	}


	public void setCombinedNote(String combinedNote) {
		this.combinedNote = combinedNote;
	}


	public String getClientAccountType() {
		return this.clientAccountType;
	}


	public void setClientAccountType(String clientAccountType) {
		this.clientAccountType = clientAccountType;
	}


	public String getFederalTaxNumber() {
		return this.federalTaxNumber;
	}


	public void setFederalTaxNumber(String federalTaxNumber) {
		this.federalTaxNumber = federalTaxNumber;
	}


	public String getClientAddressState() {
		return this.clientAddressState;
	}


	public void setClientAddressState(String clientAddressState) {
		this.clientAddressState = clientAddressState;
	}


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public String getClientRelationshipTypeName() {
		return this.clientRelationshipTypeName;
	}


	public void setClientRelationshipTypeName(String clientRelationshipTypeName) {
		this.clientRelationshipTypeName = clientRelationshipTypeName;
	}


	public String getClientRelationshipSubTypeName() {
		return this.clientRelationshipSubTypeName;
	}


	public void setClientRelationshipSubTypeName(String clientRelationshipSubTypeName) {
		this.clientRelationshipSubTypeName = clientRelationshipSubTypeName;
	}


	public Date getClientRelationshipInceptionDate() {
		return this.clientRelationshipInceptionDate;
	}


	public void setClientRelationshipInceptionDate(Date clientRelationshipInceptionDate) {
		this.clientRelationshipInceptionDate = clientRelationshipInceptionDate;
	}


	public Integer[] getInvestmentAccountIds() {
		return this.investmentAccountIds;
	}


	public void setInvestmentAccountIds(Integer[] investmentAccountIds) {
		this.investmentAccountIds = investmentAccountIds;
	}


	public String getInvestmentAccountWorkflowStateName() {
		return this.investmentAccountWorkflowStateName;
	}


	public void setInvestmentAccountWorkflowStateName(String investmentAccountWorkflowStateName) {
		this.investmentAccountWorkflowStateName = investmentAccountWorkflowStateName;
	}


	public String getInvestmentAccountExternalPortfolioCodeName() {
		return this.investmentAccountExternalPortfolioCodeName;
	}


	public void setInvestmentAccountExternalPortfolioCodeName(String investmentAccountExternalPortfolioCodeName) {
		this.investmentAccountExternalPortfolioCodeName = investmentAccountExternalPortfolioCodeName;
	}


	public String getInvestmentAccountExternalProductCodeName() {
		return this.investmentAccountExternalProductCodeName;
	}


	public void setInvestmentAccountExternalProductCodeName(String investmentAccountExternalProductCodeName) {
		this.investmentAccountExternalProductCodeName = investmentAccountExternalProductCodeName;
	}


	public Date getInvestmentAccountInceptionDate() {
		return this.investmentAccountInceptionDate;
	}


	public void setInvestmentAccountInceptionDate(Date investmentAccountInceptionDate) {
		this.investmentAccountInceptionDate = investmentAccountInceptionDate;
	}


	public Boolean getWrapAccount() {
		return this.wrapAccount;
	}


	public void setWrapAccount(Boolean wrapAccount) {
		this.wrapAccount = wrapAccount;
	}


	public String getClientRelationshipWorkflowStateName() {
		return this.clientRelationshipWorkflowStateName;
	}


	public void setClientRelationshipWorkflowStateName(String clientRelationshipWorkflowStateName) {
		this.clientRelationshipWorkflowStateName = clientRelationshipWorkflowStateName;
	}


	public String getClientRelationshipWorkflowStatusName() {
		return this.clientRelationshipWorkflowStatusName;
	}


	public void setClientRelationshipWorkflowStatusName(String clientRelationshipWorkflowStatusName) {
		this.clientRelationshipWorkflowStatusName = clientRelationshipWorkflowStatusName;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Date getClientRelationshipTerminateDate() {
		return this.clientRelationshipTerminateDate;
	}


	public void setClientRelationshipTerminateDate(Date clientRelationshipTerminateDate) {
		this.clientRelationshipTerminateDate = clientRelationshipTerminateDate;
	}


	public Integer getExcludeInvestmentAccountGroupId() {
		return this.excludeInvestmentAccountGroupId;
	}


	public void setExcludeInvestmentAccountGroupId(Integer excludeInvestmentAccountGroupId) {
		this.excludeInvestmentAccountGroupId = excludeInvestmentAccountGroupId;
	}


	public BigDecimal getCoalescePeriodEndPreliminaryAUM() {
		return this.coalescePeriodEndPreliminaryAUM;
	}


	public void setCoalescePeriodEndPreliminaryAUM(BigDecimal coalescePeriodEndPreliminaryAUM) {
		this.coalescePeriodEndPreliminaryAUM = coalescePeriodEndPreliminaryAUM;
	}


	public Short getBusinessServiceComponentId() {
		return this.businessServiceComponentId;
	}


	public void setBusinessServiceComponentId(Short businessServiceComponentId) {
		this.businessServiceComponentId = businessServiceComponentId;
	}


	public Short getClientCategoryId() {
		return this.clientCategoryId;
	}


	public void setClientCategoryId(Short clientCategoryId) {
		this.clientCategoryId = clientCategoryId;
	}


	public Date getMaxClientRelationshipInceptionDate() {
		return this.maxClientRelationshipInceptionDate;
	}


	public void setMaxClientRelationshipInceptionDate(Date maxClientRelationshipInceptionDate) {
		this.maxClientRelationshipInceptionDate = maxClientRelationshipInceptionDate;
	}


	public Date getMinClientRelationshipTerminateDate() {
		return this.minClientRelationshipTerminateDate;
	}


	public void setMinClientRelationshipTerminateDate(Date minClientRelationshipTerminateDate) {
		this.minClientRelationshipTerminateDate = minClientRelationshipTerminateDate;
	}


	public Boolean getPostedToPortal() {
		return this.postedToPortal;
	}


	public void setPostedToPortal(Boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}


	public String getInvestmentAccountChannelName() {
		return this.investmentAccountChannelName;
	}


	public void setInvestmentAccountChannelName(String investmentAccountChannelName) {
		this.investmentAccountChannelName = investmentAccountChannelName;
	}


	public Integer getInvestmentAccountAumCalculatorBeanId() {
		return this.investmentAccountAumCalculatorBeanId;
	}


	public void setInvestmentAccountAumCalculatorBeanId(Integer investmentAccountAumCalculatorBeanId) {
		this.investmentAccountAumCalculatorBeanId = investmentAccountAumCalculatorBeanId;
	}


	public Short getClientRelationshipCategoryId() {
		return this.clientRelationshipCategoryId;
	}


	public void setClientRelationshipCategoryId(Short clientRelationshipCategoryId) {
		this.clientRelationshipCategoryId = clientRelationshipCategoryId;
	}


	public String getClientRelationshipCategoryName() {
		return this.clientRelationshipCategoryName;
	}


	public void setClientRelationshipCategoryName(String clientRelationshipCategoryName) {
		this.clientRelationshipCategoryName = clientRelationshipCategoryName;
	}


	public Boolean getClientRelationshipCategoryRetail() {
		return this.clientRelationshipCategoryRetail;
	}


	public void setClientRelationshipCategoryRetail(Boolean clientRelationshipCategoryRetail) {
		this.clientRelationshipCategoryRetail = clientRelationshipCategoryRetail;
	}


	public BigDecimal getPeriodProjectedRevenueShareExternal() {
		return this.periodProjectedRevenueShareExternal;
	}


	public void setPeriodProjectedRevenueShareExternal(BigDecimal periodProjectedRevenueShareExternal) {
		this.periodProjectedRevenueShareExternal = periodProjectedRevenueShareExternal;
	}


	public BigDecimal getPeriodProjectedRevenueShareInternal() {
		return this.periodProjectedRevenueShareInternal;
	}


	public void setPeriodProjectedRevenueShareInternal(BigDecimal periodProjectedRevenueShareInternal) {
		this.periodProjectedRevenueShareInternal = periodProjectedRevenueShareInternal;
	}


	public BigDecimal getPeriodProjectedRevenueNetTotal() {
		return this.periodProjectedRevenueNetTotal;
	}


	public void setPeriodProjectedRevenueNetTotal(BigDecimal periodProjectedRevenueNetTotal) {
		this.periodProjectedRevenueNetTotal = periodProjectedRevenueNetTotal;
	}
}
