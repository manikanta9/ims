package com.clifton.accounting.m2m;


import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.util.List;


/**
 * The <code>AccountingM2MExpenseGroup</code> is a grouping of {@link AccountingM2MDailyExpense} records to be saved at once.
 *
 * @author jgommels
 */
@NonPersistentObject
public class AccountingM2MExpenseGroup {

	private List<AccountingM2MDailyExpense> expenseList;


	public List<AccountingM2MDailyExpense> getExpenseList() {
		return this.expenseList;
	}


	public void setExpenseList(List<AccountingM2MDailyExpense> expenseList) {
		this.expenseList = expenseList;
	}
}
