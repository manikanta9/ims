package com.clifton.accounting.m2m.rebuild;


import com.clifton.accounting.m2m.M2MRebuildCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;

import java.util.Date;
import java.util.Map;


/**
 * The <code>CollateralBalanceRebuildJob</code> class is a batch job that rebuilds (Today - daysBack) collateral balances for specified parameters
 *
 * @author Mary Anderson
 */
public class AccountingM2MDailyRebuildJob implements Task, StatusHolderObjectAware<Status> {

	private CalendarBusinessDayService calendarBusinessDayService;
	private AccountingM2MRebuildService accountingM2MRebuildService;

	private StatusHolderObject<Status> statusHolder;

	/////////////////////////////////////////////////////////////////////////

	private static final Integer DEFAULT_DAYS_BACK = 1;

	/**
	 * Rebuild m2m data that for the past x business days
	 */
	private Integer businessDaysBack = DEFAULT_DAYS_BACK;
	/**
	 * Weather or not to delete non-booked and/or non-reconciled mark records.
	 */
	private boolean deleteExistingMark;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Date date = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -getBusinessDaysBack());
		Date today = DateUtils.clearTime(new Date());

		StringBuilder result = new StringBuilder("Dates Processed: ");
		Status status = this.statusHolder.getStatus();
		status.setMessage(result.toString());

		while (date.before(today)) {
			M2MRebuildCommand command = new M2MRebuildCommand(date, null, null, null, null, isDeleteExistingMark());
			command.setStatusHolder(this.statusHolder);
			getAccountingM2MRebuildService().rebuildAccountingM2MDaily(command);

			result.append(DateUtils.fromDateShort(date)).append("; ");
			status.setMessage(result.toString());

			// Move forward to the next week day
			date = DateUtils.getNextWeekday(date);
		}

		status.setMessage("Processing Complete. " + result.toString());
		return status;
	}


	//////////////////////////////////////////////////////////////////////////// 
	////////////             Getter and Setter Methods            ////////////// 
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public Integer getBusinessDaysBack() {
		return this.businessDaysBack;
	}


	public void setBusinessDaysBack(Integer businessDaysBack) {
		this.businessDaysBack = businessDaysBack;
	}


	public boolean isDeleteExistingMark() {
		return this.deleteExistingMark;
	}


	public void setDeleteExistingMark(boolean deleteExistingMark) {
		this.deleteExistingMark = deleteExistingMark;
	}


	public AccountingM2MRebuildService getAccountingM2MRebuildService() {
		return this.accountingM2MRebuildService;
	}


	public void setAccountingM2MRebuildService(AccountingM2MRebuildService accountingM2MRebuildService) {
		this.accountingM2MRebuildService = accountingM2MRebuildService;
	}
}
