package com.clifton.accounting.m2m.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MDailyExpense;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingM2MDailyExpenseBookingRule</code> class creates 2 journal details for each expense object.
 * If account with futures is not a "Hold" account and "Mark to Market" relationship is not to a "Money Market" account,
 * then 2 more journal details are created to transfer the expense from broker to custodian.
 *
 * @author mwacker
 */
public class AccountingM2MDailyExpenseBookingRule implements AccountingBookingRule<AccountingM2MDaily> {

	private AccountingAccountService accountingAccountService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;


	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<AccountingM2MDaily> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingM2MDaily m2m = bookingSession.getBookableEntity();

		for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(m2m.getExpenseList())) {
			BigDecimal transferAmount = expense.getExpenseAmount();
			if (!MathUtils.isNullOrZero(transferAmount)) {
				// book interest income/expense and corresponding cash/currency
				journal.addJournalDetail(generateExpenseDetail(journal, expense, false));
				journal.addJournalDetail(generateExpenseDetail(journal, expense, true));
			}
		}
	}


	private AccountingJournalDetail generateExpenseDetail(AccountingJournal journal, AccountingM2MDailyExpense expense, boolean cash) {
		// mark is done for previous business day so transfer is one business day after the mark
		AccountingM2MDaily m2m = expense.getM2mDaily();
		Date markDate = m2m.getMarkDate();

		BigDecimal amount = cash ? expense.getExpenseAmount() : expense.getExpenseAmount().negate();

		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(journal.getFkFieldId());
		detail.setSystemTable(journal.getSystemTable());
		detail.setParentTransaction(null);

		detail.setClientInvestmentAccount(m2m.getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(m2m.getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(expense.getExpenseCurrency());

		AccountingAccount accountingAccount = expense.getType().getAccountingAccount();
		boolean baseCurrency = isBaseCurrency(expense);
		if (cash) {
			accountingAccount = getAccountingAccountService().getAccountingAccountByName(baseCurrency ? AccountingAccount.ASSET_CASH : AccountingAccount.ASSET_CURRENCY);
		}
		detail.setAccountingAccount(accountingAccount);

		detail.setPrice(null);

		detail.setTransactionDate(markDate);
		detail.setOriginalTransactionDate(markDate);
		// mark is done for previous business day so transfer and settlement is one business day after the mark
		detail.setSettlementDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(markDate), expense.getType().getDaysToSettle()));

		detail.setLocalDebitCredit(amount);
		detail.setQuantity(null);
		if (baseCurrency) {
			detail.setBaseDebitCredit(amount);
			detail.setExchangeRateToBase(BigDecimal.ONE);
		}
		else {
			// need to lookup latest FX rate from holding company
			String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(detail);
			String toCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(detail);
			BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(detail.getHoldingInvestmentAccount().toHoldingAccount(), fromCurrency, toCurrency, markDate).flexibleLookup());
			detail.setExchangeRateToBase(fxRate);
			detail.setBaseDebitCredit(MathUtils.multiply(amount, fxRate, 2));
		}

		detail.setPositionCostBasis(BigDecimal.ZERO);
		detail.setPositionCommission(BigDecimal.ZERO);
		detail.setOpening(true);

		detail.setDescription("M2M " + expense.getType().getAccountingAccount().getName());
		if (InvestmentAccountType.OTC_CLEARED.equals(m2m.getHoldingInvestmentAccount().getType().getName())) {
			// different descriptions for cleared vs non-cleared marks to assist accounting with cash reconciliation
			detail.setDescription("Cleared OTC " + detail.getDescription());
		}

		return detail;
	}


	/**
	 * Returns true if expense currency is equal to the base currency of the holding account: Cash.
	 */
	private boolean isBaseCurrency(AccountingM2MDailyExpense expense) {
		return expense.getExpenseCurrency().equals(expense.getM2mDaily().getHoldingInvestmentAccount().getBaseCurrency());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
