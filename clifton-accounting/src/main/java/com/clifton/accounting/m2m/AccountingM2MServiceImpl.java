package com.clifton.accounting.m2m;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.m2m.search.AccountingM2MDailyExpenseSearchForm;
import com.clifton.accounting.m2m.search.AccountingM2MDailyExpenseTypeSearchForm;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchFormConfigurer;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.rates.InvestmentRatesService;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingM2MServiceImpl</code> class provides basis implementation for the AccountingM2MService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingM2MServiceImpl implements AccountingM2MService {

	private static final String PAI_INTEREST_RATES_GROUP = "PAI";

	////////////////////////////////////////////////////////////////////////////

	private AdvancedUpdatableDAO<AccountingM2MDaily, Criteria> accountingM2MDailyDAO;
	private AdvancedUpdatableDAO<AccountingM2MDailyExpense, Criteria> accountingM2MDailyExpenseDAO;
	private AdvancedUpdatableDAO<AccountingM2MDailyExpenseType, Criteria> accountingM2MDailyExpenseTypeDAO;


	private AccountingPositionDailyService accountingPositionDailyService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;
	private MarketDataRatesRetriever marketDataRatesRetriever;
	private InvestmentRatesService investmentRatesService;
	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String accountingMarkToMarketRelationshipPurposeName = InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME;
	/**
	 * Mark to Market usually follows bank holidays (custodians).
	 */
	private String accountingMarkToMarketCalendarName = "US Banks Calendar";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingM2MDaily getAccountingM2MDaily(int id) {
		return getAccountingM2MDaily(id, false);
	}


	@Override
	public AccountingM2MDaily getAccountingM2MDailyPopulated(int id) {
		return getAccountingM2MDaily(id, true);
	}


	private AccountingM2MDaily getAccountingM2MDaily(int id, boolean populated) {
		AccountingM2MDaily result = getAccountingM2MDailyDAO().findByPrimaryKey(id);
		if (result != null && populated) {
			result.setExpenseList(getAccountingM2MDailyExpenseDAO().findByField("m2mDaily.id", id));
		}
		return result;
	}


	@Override
	public AccountingM2MDaily getAccountingM2MDailyByHoldingAccount(InvestmentAccount holdingAccount, Date markDate, InvestmentSecurity markCurrency) {
		return CollectionUtils.getOnlyElement(getAccountingM2MDailyList(markDate, holdingAccount, isM2MinLocalCurrency(holdingAccount) ? markCurrency : null, true));
	}


	private List<AccountingM2MDaily> getAccountingM2MDailyList(Date markDate, InvestmentAccount holdingAccount, InvestmentSecurity markCurrency, boolean populate) {
		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setHoldingInvestmentAccountId(holdingAccount.getId());
		searchForm.setMarkDate(markDate);
		if (markCurrency != null) {
			searchForm.setMarkCurrencyId(markCurrency.getId());
		}
		return getAccountingM2MDailyList(searchForm, populate);
	}


	@Override
	public AccountingM2MDaily getAccountingM2MDailyByClientAccountAndDate(int clientAccountId, Date date) {
		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setClientInvestmentAccountId(clientAccountId);
		searchForm.setMarkDate(date);
		List<AccountingM2MDaily> dailyList = getAccountingM2MDailyList(searchForm);
		if (CollectionUtils.isEmpty(dailyList)) {
			throw new ValidationException("Cannot find any M2M Daily records for Client Account Id [" + clientAccountId + "] on [" + DateUtils.fromDateShort(date) + "]");
		}
		if (CollectionUtils.getSize(dailyList) > 1) {
			throw new ValidationException("Found multiple M2M Daily records for Client Account Id [" + clientAccountId + "] on [" + DateUtils.fromDateShort(date) + "]");
		}
		return dailyList.get(0);
	}


	@Override
	public List<AccountingM2MDaily> getAccountingM2MDailyListPopulated(final AccountingM2MDailySearchForm searchForm) {
		return getAccountingM2MDailyList(searchForm, true);
	}


	@Override
	public List<AccountingM2MDaily> getAccountingM2MDailyList(final AccountingM2MDailySearchForm searchForm) {
		return getAccountingM2MDailyList(searchForm, false);
	}


	private List<AccountingM2MDaily> getAccountingM2MDailyList(final AccountingM2MDailySearchForm searchForm, boolean populate) {
		List<AccountingM2MDaily> accountingM2MDailyList = getAccountingM2MDailyDAO().findBySearchCriteria(new AccountingM2MDailySearchFormConfigurer(searchForm));
		if (!CollectionUtils.isEmpty(accountingM2MDailyList) && populate) {
			for (AccountingM2MDaily accountingM2MDaily : CollectionUtils.getIterable(accountingM2MDailyList)) {
				accountingM2MDaily.setExpenseList(getAccountingM2MDailyExpenseDAO().findByField("m2mDaily.id", accountingM2MDaily.getId()));
			}
		}
		return accountingM2MDailyList;
	}


	@Override
	public AccountingM2MDaily saveAccountingM2MDaily(AccountingM2MDaily bean) {
		if (bean.getClientInvestmentAccount() == null && bean.getHoldingInvestmentAccount() != null) {
			bean.setClientInvestmentAccount(getAccountingM2MInvestmentAccount(bean.getHoldingInvestmentAccount(), bean.getMarkDate(), true));
		}

		return getAccountingM2MDailyDAO().save(bean);
	}


	@Override
	@Transactional
	public AccountingM2MDaily saveAccountingM2MDailyWithExpenses(AccountingM2MDaily bean, boolean recalculate) {
		List<AccountingM2MDailyExpense> oldExpenseList = bean.isNewBean() ? null : getAccountingM2MDailyExpenseDAO().findByField("m2mDaily.id", bean.getId());

		if (recalculate) {
			// back out old expenses
			for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(oldExpenseList)) {
				bean.setOurTransferAmount(MathUtils.subtract(bean.getOurTransferAmount(), expense.getExpenseAmountInMarkCurrency()));
			}

			// set the expected transfer amount to the new value
			BigDecimal oldExpectedExpenseAmountAdjustment = CoreMathUtils.sumProperty(oldExpenseList, (AccountingM2MDailyExpense e) -> e.getType().isAdjustExpectedTransferAmount() ? e.getExpectedExpenseAmount() : BigDecimal.ZERO);
			BigDecimal expectedExpenseAmountAdjustment = getExpectedExpenseAmountAdjustment(bean);
			bean.setExpectedTransferAmount(MathUtils.subtract(MathUtils.add(bean.getExpectedTransferAmount(), oldExpectedExpenseAmountAdjustment), expectedExpenseAmountAdjustment));
		}

		for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(bean.getExpenseList())) {
			expense.setM2mDaily(bean);
			validateAccountingM2MDailyExpense(expense);
			if (recalculate) {
				// add expenses
				bean.setOurTransferAmount(MathUtils.add(bean.getOurTransferAmount(), expense.getExpenseAmountInMarkCurrency()));
			}
		}

		List<AccountingM2MDailyExpense> newExpenseList = bean.getExpenseList();
		bean = saveAccountingM2MDaily(bean);
		getAccountingM2MDailyExpenseDAO().saveList(newExpenseList, oldExpenseList);
		bean.setExpenseList(newExpenseList);
		return bean;
	}


	@Override
	public void saveAccountingM2MDailyList(List<AccountingM2MDaily> beanList) {
		getAccountingM2MDailyDAO().saveList(beanList);
	}


	@Override
	public void deleteAccountingM2MDaily(AccountingM2MDaily bean) {
		getAccountingM2MDailyExpenseDAO().deleteList(getAccountingM2MDailyExpenseDAO().findByField("m2mDaily.id", bean.getId()));
		getAccountingM2MDailyDAO().delete(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public AccountingM2MDailyExpense getAccountingM2MDailyExpenseForSpecificMark(AccountingM2MDaily m2mDaily, AccountingM2MDailyExpenseType type, InvestmentSecurity expenseCurrency, Integer sourceFkFieldId) {
		if (!CollectionUtils.isEmpty(m2mDaily.getExpenseList())) {
			for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(m2mDaily.getExpenseList())) {
				if (expense.getM2mDaily().equals(m2mDaily) && expense.getType().equals(type)
						&& expense.getExpenseCurrency().equals(expenseCurrency)
						&& ((expense.getType().getExpenseSourceFkTable() == null) || expense.getSourceFkFieldId().equals(sourceFkFieldId))) {
					return expense;
				}
			}
		}
		if (!m2mDaily.isNewBean()) {
			AccountingM2MDailyExpenseSearchForm searchForm = new AccountingM2MDailyExpenseSearchForm();
			searchForm.setM2mDailyId(m2mDaily.getId());
			searchForm.setTypeId(type.getId());
			searchForm.setExpenseCurrencyId(expenseCurrency.getId());
			searchForm.setSourceFkFieldId(sourceFkFieldId);
			return CollectionUtils.getOnlyElement(getAccountingM2MDailyExpenseList(searchForm));
		}
		return null;
	}


	@Override
	public List<AccountingM2MDailyExpense> getAccountingM2MDailyExpenseListByMark(Integer m2mDailyId) {
		return getAccountingM2MDailyExpenseDAO().findByField("m2mDaily.id", m2mDailyId);
	}


	@Override
	public List<AccountingM2MDailyExpense> getAccountingM2MDailyExpenseList(AccountingM2MDailyExpenseSearchForm searchForm) {
		return getAccountingM2MDailyExpenseDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BigDecimal getExpectedExpenseAmountAdjustment(AccountingM2MDaily m2mDaily) {
		if (!CollectionUtils.isEmpty(m2mDaily.getExpenseList())) {
			return CoreMathUtils.sumProperty(m2mDaily.getExpenseList(), (AccountingM2MDailyExpense e) -> e.getType().isAdjustExpectedTransferAmount() ? e.getExpectedExpenseAmount() : BigDecimal.ZERO);
		}
		AccountingM2MDailyExpenseSearchForm searchForm = new AccountingM2MDailyExpenseSearchForm();
		searchForm.setM2mDailyId(m2mDaily.getId());
		searchForm.setAdjustExpectedTransferAmount(true);
		List<AccountingM2MDailyExpense> expenseList = getAccountingM2MDailyExpenseList(searchForm);
		return CoreMathUtils.sumProperty(expenseList, AccountingM2MDailyExpense::getExpectedExpenseAmount);
	}


	@Override
	@Transactional
	public AccountingM2MDailyExpense saveAccountingM2MDailyExpense(AccountingM2MDailyExpense expense) {
		ValidationUtils.assertTrue(expense.isNewBean(), "Cannot update existing expense. Only creation of new expenses is allowed for mark: " + expense.getM2mDaily().getLabel());
		validateAccountingM2MDailyExpense(expense);

		// need to also update m2m amounts
		AccountingM2MDaily m2m = getAccountingM2MDaily(expense.getM2mDaily().getId());
		ValidationUtils.assertFalse(m2m.isReconciled(), "Cannot update expense(s) for a reconciled mark: " + m2m.getLabel());

		expense = getAccountingM2MDailyExpenseDAO().save(expense);

		// add expense to mark's transfer amounts
		m2m.setOurTransferAmount(MathUtils.add(m2m.getOurTransferAmount(), expense.getExpenseAmountInMarkCurrency()));
		getAccountingM2MDailyDAO().save(m2m);
		return expense;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void saveAccountingM2MExpenseGroup(AccountingM2MExpenseGroup expenseGroup, boolean errorIfExists) {
		List<AccountingM2MDailyExpense> expenseList = expenseGroup.getExpenseList();

		for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(expenseList)) {
			expense.setM2mDaily(getAccountingM2MDailyPopulated(expense.getM2mDaily().getId()));
			AccountingM2MDaily m2mDaily = expense.getM2mDaily();

			if (errorIfExists && expense.getType().getAccountingAccount() != null) {
				AccountingM2MDailyExpenseSearchForm searchForm = new AccountingM2MDailyExpenseSearchForm();
				searchForm.setAccountingAccountId(expense.getType().getAccountingAccount().getId());
				searchForm.setM2mDailyId(m2mDaily.getId());
				List<AccountingM2MDailyExpense> list = this.getAccountingM2MDailyExpenseList(searchForm);
				ValidationUtils.assertTrue(CollectionUtils.isEmpty(list), "A transaction with GL Account '" + expense.getType().getAccountingAccount().getName() + "' already exists for client account '"
						+ m2mDaily.getClientInvestmentAccount().getName() + "' and Holding Account '" + m2mDaily.getHoldingInvestmentAccount().getNumber() + "' on Mark Date " + m2mDaily.getMarkDate());
			}

			saveAccountingM2MDailyExpense(expense);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingM2MDailyExpenseType getAccountingM2MDailyExpenseType(short id) {
		return getAccountingM2MDailyExpenseTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingM2MDailyExpenseType getAccountingM2MDailyExpenseTypeByName(String name) {
		return getAccountingM2MDailyExpenseTypeDAO().findOneByField("name", name);
	}


	@Override
	public AccountingM2MDailyExpenseType getAccountingM2MDailyExpenseTypeByExternalNameAndAmount(String externalName, BigDecimal expenseAmount) {
		AccountingM2MDailyExpenseTypeSearchForm searchForm = new AccountingM2MDailyExpenseTypeSearchForm();
		searchForm.setExternalName(externalName);
		List<AccountingM2MDailyExpenseType> resultList = getAccountingM2MDailyExpenseTypeList(searchForm);
		if (resultList.size() == 2) {
			return CollectionUtils.getOnlyElement(BeanUtils.filter(resultList, AccountingM2MDailyExpenseType::isIncome, MathUtils.isPositive(expenseAmount)));
		}
		else if (resultList.size() == 1) {
			return CollectionUtils.getOnlyElement(resultList);
		}
		throw new RuntimeException("Could not find an expense type for external name [" + externalName + "].");
	}


	@Override
	public AccountingM2MDailyExpenseType getAccountingM2MDailyExpenseTypeByAccountingAccount(short accountingAccountId) {
		return getAccountingM2MDailyExpenseTypeDAO().findOneByField("accountingAccount.id", accountingAccountId);
	}


	@Override
	public List<AccountingM2MDailyExpenseType> getAccountingM2MDailyExpenseTypeList(AccountingM2MDailyExpenseTypeSearchForm searchForm) {
		return getAccountingM2MDailyExpenseTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingM2MDailyExpenseType saveAccountingM2MDailyExpenseType(AccountingM2MDailyExpenseType bean) {
		if (bean.getAccountingAccount() != null) {
			ValidationUtils.assertNull(bean.getExpenseSourceFkTable(), "Expense source table cannot be set when an accounting account is specified.");

			AccountingM2MDailyExpenseType existingType = getAccountingM2MDailyExpenseTypeByAccountingAccount(bean.getAccountingAccount().getId());
			ValidationUtils.assertTrue(existingType == null || (existingType.equals(bean)), "Existing expense type [" + (existingType != null ? existingType.getLabel() : "") + "] is already linked to accounting account [" + bean.getAccountingAccount().getLabel() + "].");
		}
		else {
			ValidationUtils.assertNotNull(bean.getExpenseSourceFkTable(), "Expense source table must be populated if no accounting account is specified.");
		}
		if (!StringUtils.isEmpty(bean.getExternalName())) {
			AccountingM2MDailyExpenseTypeSearchForm searchForm = new AccountingM2MDailyExpenseTypeSearchForm();
			searchForm.setExternalName(bean.getExternalName());
			searchForm.setIncome(bean.isIncome());
			List<AccountingM2MDailyExpenseType> expenseTypes = getAccountingM2MDailyExpenseTypeList(searchForm);
			ValidationUtils.assertTrue((bean.isNewBean() && expenseTypes.isEmpty()) || (!bean.isNewBean() && (expenseTypes.size() <= 1)), "[" + expenseTypes.size() + "] types already exists for external name = [" + bean.getExternalName() + "] and income = [" + bean.isIncome() + "].");
			if (expenseTypes.size() == 1) {
				ValidationUtils.assertTrue(bean.equals(CollectionUtils.getOnlyElement(expenseTypes)), "A type exists for external name = [" + bean.getExternalName() + "] and income = [" + bean.isIncome() + "] already exists.");
			}
		}
		return getAccountingM2MDailyExpenseTypeDAO().save(bean);
	}


	@Override
	public void deleteAccountingM2MDailyExpenseType(short id) {
		getAccountingM2MDailyExpenseTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getAccountingM2MInvestmentAccount(InvestmentAccount holdingAccount, Date markDate, boolean ourAccount) {
		ValidationUtils.assertNotNull(holdingAccount, "Required argument 'holdingAccount' is missing.");
		List<InvestmentAccount> accountList = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(holdingAccount.getId(), null,
				getAccountingMarkToMarketRelationshipPurposeName(), markDate);
		ValidationUtils.assertNotEmpty(accountList, "Cannot find " + (ourAccount ? "main" : "related") + " account for 'Mark to Market' purpose for " + holdingAccount.getLabel());
		ValidationUtils.assertTrue(accountList.size() < 3, "Cannot have more than 2 'Mark to Market' purpose links for: " + holdingAccount.getLabel());

		// there can be up to 2 "Mark to Market" links: one to custodian holding account and one to our account
		InvestmentAccount custodialAccount = null;
		InvestmentAccount clientAccount = null;
		for (InvestmentAccount account : accountList) {
			if (account.getType().isOurAccount()) {
				ValidationUtils.assertNull(clientAccount, "Cannot have more than 1 'our' account for 'Mark to Market' purpose for: " + holdingAccount.getLabel());
				clientAccount = account;
			}
			else {
				ValidationUtils.assertNull(custodialAccount, "Cannot have more than 1 NOT 'our' account for 'Mark to Market' purpose for: " + holdingAccount.getLabel());
				custodialAccount = account;
			}
		}

		if (ourAccount) {
			ValidationUtils.assertNotNull(clientAccount, "Cannot find main account for 'Mark to Market' purpose for: " + holdingAccount.getLabel());
			return clientAccount;
		}
		ValidationUtils.assertNotNull(custodialAccount, "Cannot find related account for 'Mark to Market' purpose for: " + holdingAccount.getLabel());
		return custodialAccount;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isM2MinLocalCurrency(InvestmentAccount account) {
		Boolean result = (Boolean) getSystemColumnValueHandler().getSystemColumnValueForEntity(account, InvestmentAccount.HOLDING_ACCOUNT_COLUMN_GROUP_NAME,
				InvestmentAccount.HOLDING_ACCOUNT_M2M_LOCAL_CURRENCY, true);
		return result != null && result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPositionDaily> getAccountingPositionDailyLiveForM2MList(AccountingPositionDailyLiveSearchForm searchForm) {
		searchForm.setCollateralAccountingAccount(false); // collateral positions are never marked
		searchForm.setUseSettlementDate(true);

		Calendar m2mCalendar = getCalendarSetupService().getCalendarByName(getAccountingMarkToMarketCalendarName());
		ValidationUtils.assertNotNull(m2mCalendar, "Cannot find Calendar with name: " + getAccountingMarkToMarketCalendarName());
		searchForm.setRealizedAdjustmentDate(getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(searchForm.getSnapshotDate(), m2mCalendar.getId())));

		List<AccountingPositionDaily> result = getAccountingPositionDailyService().getAccountingPositionDailyLiveList(searchForm);

		populateAccountingPositionDailyPAI(result, searchForm.getSnapshotDate(), searchForm.getSnapshotDate());

		List<AccountingPositionDaily> currencyList = getAccountingPositionDailyService().getAccountingPositionDailyLiveForCurrencyList(searchForm);
		if (currencyList != null) {
			if (result == null) {
				result = currencyList;
			}
			else {
				result.addAll(currencyList);
			}
		}

		// sort by client account number, then by security symbol, then by lot id to make it UI friendly
		if (!CollectionUtils.isEmpty(result)) {
			result.sort((o1, o2) -> {
				AccountingTransactionInfo t1 = o1.getAccountingTransaction();
				AccountingTransactionInfo t2 = o2.getAccountingTransaction();
				int c = StringUtils.compare(t1.getClientInvestmentAccount().getNumber(), t2.getClientInvestmentAccount().getNumber());
				if (c == 0) {
					c = StringUtils.compare(t1.getInvestmentSecurity().getSymbol(), t2.getInvestmentSecurity().getSymbol());
					if (c == 0) {
						c = StringUtils.compare(t1.getHoldingInvestmentAccount().getNumber(), t2.getHoldingInvestmentAccount().getNumber());
						if (c == 0 && t1.getId() != null) {
							c = t1.getId().compareTo(t2.getId());
						}
					}
				}
				return c;
			});
		}

		return result;
	}


	@Override
	public void populateAccountingPositionDailyPAI(List<AccountingPositionDaily> positionList, Date markStartDate, Date markDate) {
		if (!CollectionUtils.isEmpty(positionList)) {
			Map<Integer, BigDecimal> paiRateMap = new HashMap<>(); // currency id to corresponding rate on paiRateDate
			Calendar m2mCalendar = getCalendarSetupService().getCalendarByName(getAccountingMarkToMarketCalendarName());
			int paiDays = DateUtils.getDaysDifference(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(markDate, m2mCalendar.getId()), 1), markStartDate);

			for (AccountingPositionDaily position : positionList) {
				AccountingTransactionInfo positionTransaction = position.getAccountingTransaction();
				InvestmentSecurity security = positionTransaction.getInvestmentSecurity();
				if (position.getPositionDate().equals(markStartDate) && !security.isCurrency() && !positionTransaction.getAccountingAccount().isCollateral()) {
					// only OTC Cleared accounts can have PAI and have corresponding Interest Rates configured
					InvestmentAccount holdingAccount = positionTransaction.getHoldingInvestmentAccount();
					if (InvestmentAccountType.OTC_CLEARED.equals(holdingAccount.getType().getName())) {
						BigDecimal paiBasis = position.getPriorMarketValueLocal(); // PAI is always calculated in LOCAL currency
						if (!MathUtils.isNullOrZero(paiBasis)) {
							InvestmentSecurity currency = security.getInstrument().getTradingCurrency();
							BigDecimal paiRate = paiRateMap.get(currency.getId());
							if (paiRate == null) {
								InvestmentInterestRateIndex paiIndex = getInvestmentRatesService().getInvestmentInterestRateIndexClosest(PAI_INTEREST_RATES_GROUP, currency.getId(), paiDays, markStartDate);
								if (paiIndex == null) {
									throw new ValidationException("Cannot find PAI interest rate index in '" + PAI_INTEREST_RATES_GROUP + "' group for currency: " + currency.getSymbol() + ";\nSecurity: " + security);
								}
								else if (paiIndex.getCalendar() == null) {
									throw new ValidationException("Calendar is required for PAI interest rate index '" + paiIndex.getName() + "' for currency: " + currency.getSymbol() + ";\nSecurity: " + security);
								}

								Date paiRateDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(markStartDate, paiIndex.getCalendar().getId()), -1);
								paiRate = getMarketDataRatesRetriever().getMarketDataInterestRateForDate(paiIndex, paiRateDate, false);
								if (paiRate == null) {
									throw new RuntimeException("Cannot find PAI interest rate for '" + paiIndex.getName() + "' on " + DateUtils.fromDateShort(paiRateDate) + ";\nSecurity: " + security);
								}
								paiRateMap.put(currency.getId(), paiRate);
							}
							BigDecimal paiValue = MathUtils.divide(MathUtils.multiply(MathUtils.multiply(paiBasis, paiRate), paiDays), new BigDecimal("36000"));
							position.setPaiAmountLocal(paiValue.negate());
						}
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateAccountingM2MDailyExpense(AccountingM2MDailyExpense expense) {
		ValidationUtils.assertNotNull(expense.getType(), "Expense type is required for each expense", "type");
		AccountingAccount glAccount = expense.getType().getAccountingAccount();
		if (glAccount != null) {
			ValidationUtils.assertFalse(glAccount.getAccountType().isBalanceSheetAccount(), "Only 'Income Statement' GL Accounts can be used for M2M expenses: '" + glAccount.getName() + "'.");

			boolean expenseGL = glAccount.getAccountType().isGrowingOnDebitSide();
			validateAccountingM2MDailyExpenseSign(expense.getOurExpenseAmount(), glAccount.getName(), expenseGL, "Our Amount");
			validateAccountingM2MDailyExpenseSign(expense.getExpectedExpenseAmount(), glAccount.getName(), expenseGL, "Broker Amount");
			validateAccountingM2MDailyExpenseSign(expense.getExpenseAmount(), glAccount.getName(), expenseGL, "Expense Amount");
		}
	}


	private void validateAccountingM2MDailyExpenseSign(BigDecimal amount, String glAccount, boolean glIsExpense, String expenseField) {
		if (!MathUtils.isNullOrZero(amount)) {
			if (glIsExpense) {
				ValidationUtils.assertFalse(MathUtils.isPositive(amount), "Expense GL Account '" + glAccount + "' for '" + expenseField + "' must be negative.");
			}
			else {
				ValidationUtils.assertTrue(MathUtils.isPositive(amount), "Revenue GL Account '" + glAccount + "' for '" + expenseField + "' must be positive.");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingM2MDaily, Criteria> getAccountingM2MDailyDAO() {
		return this.accountingM2MDailyDAO;
	}


	public void setAccountingM2MDailyDAO(AdvancedUpdatableDAO<AccountingM2MDaily, Criteria> accountingM2MDailyDAO) {
		this.accountingM2MDailyDAO = accountingM2MDailyDAO;
	}


	@Override
	public String getAccountingMarkToMarketRelationshipPurposeName() {
		return this.accountingMarkToMarketRelationshipPurposeName;
	}


	public void setAccountingMarkToMarketRelationshipPurposeName(String accountingMarkToMarketRelationshipPurposeName) {
		this.accountingMarkToMarketRelationshipPurposeName = accountingMarkToMarketRelationshipPurposeName;
	}


	@Override
	public String getAccountingMarkToMarketCalendarName() {
		return this.accountingMarkToMarketCalendarName;
	}


	public void setAccountingMarkToMarketCalendarName(String accountingMarkToMarketCalendarName) {
		this.accountingMarkToMarketCalendarName = accountingMarkToMarketCalendarName;
	}


	public AdvancedUpdatableDAO<AccountingM2MDailyExpense, Criteria> getAccountingM2MDailyExpenseDAO() {
		return this.accountingM2MDailyExpenseDAO;
	}


	public void setAccountingM2MDailyExpenseDAO(AdvancedUpdatableDAO<AccountingM2MDailyExpense, Criteria> accountingM2MDailyExpenseDAO) {
		this.accountingM2MDailyExpenseDAO = accountingM2MDailyExpenseDAO;
	}


	public AdvancedUpdatableDAO<AccountingM2MDailyExpenseType, Criteria> getAccountingM2MDailyExpenseTypeDAO() {
		return this.accountingM2MDailyExpenseTypeDAO;
	}


	public void setAccountingM2MDailyExpenseTypeDAO(AdvancedUpdatableDAO<AccountingM2MDailyExpenseType, Criteria> accountingM2MDailyExpenseTypeDAO) {
		this.accountingM2MDailyExpenseTypeDAO = accountingM2MDailyExpenseTypeDAO;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public InvestmentRatesService getInvestmentRatesService() {
		return this.investmentRatesService;
	}


	public void setInvestmentRatesService(InvestmentRatesService investmentRatesService) {
		this.investmentRatesService = investmentRatesService;
	}


	public MarketDataRatesRetriever getMarketDataRatesRetriever() {
		return this.marketDataRatesRetriever;
	}


	public void setMarketDataRatesRetriever(MarketDataRatesRetriever marketDataRatesRetriever) {
		this.marketDataRatesRetriever = marketDataRatesRetriever;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
