package com.clifton.accounting.m2m;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.m2m.search.AccountingM2MDailyExpenseSearchForm;
import com.clifton.accounting.m2m.search.AccountingM2MDailyExpenseTypeSearchForm;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingM2MService</code> interface defines methods for performing Mark to Market activities.
 *
 * @author vgomelsky
 */
public interface AccountingM2MService {

	public static final String M2M_ACCOUNT_REBUILD_COMPLETE_AFTER_SAVE_EVENT_NAME = "M2M_ACCOUNT_REBUILD_COMPLETE_AFTER_SAVE_EVENT_NAME";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = AccountingM2MDaily.class)
	public String getAccountingMarkToMarketRelationshipPurposeName();


	@SecureMethod(dtoClass = AccountingM2MDaily.class)
	public String getAccountingMarkToMarketCalendarName();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingM2MDaily getAccountingM2MDaily(int id);


	public AccountingM2MDaily getAccountingM2MDailyPopulated(int id);


	/**
	 * Lookup the <code>AccountingM2MDaily</code> by holding account, mark date and currency.
	 * <p>
	 * NOTE:  This method will NOT check if the m2m is in local currency.  That must be done be this call
	 * and if it's NOT in local currency, then submit null for markCurrency.  This is done to prevent extra look ups
	 * by allowing the caller to cache the value where possible.
	 */
	@DoNotAddRequestMapping
	public AccountingM2MDaily getAccountingM2MDailyByHoldingAccount(InvestmentAccount holdingAccount, Date markDate, InvestmentSecurity markCurrency);


	public AccountingM2MDaily getAccountingM2MDailyByClientAccountAndDate(int clientAccountId, Date date);


	public List<AccountingM2MDaily> getAccountingM2MDailyListPopulated(AccountingM2MDailySearchForm searchForm);


	public List<AccountingM2MDaily> getAccountingM2MDailyList(AccountingM2MDailySearchForm searchForm);


	public AccountingM2MDaily saveAccountingM2MDaily(AccountingM2MDaily bean);


	/**
	 * Saves the specified AccountingM2MDaily object.  Also updates expense list associated with the bean.
	 * If 'recalculate' is specified, will also recalculate transfer amounts.
	 */
	public AccountingM2MDaily saveAccountingM2MDailyWithExpenses(AccountingM2MDaily bean, boolean recalculate);


	public void saveAccountingM2MDailyList(List<AccountingM2MDaily> beanList);


	/**
	 * Returns the sum of expectedExpenseAmount for all expenses that with types that have adjustExpectedTransferAmount = true.
	 */
	@DoNotAddRequestMapping
	public BigDecimal getExpectedExpenseAmountAdjustment(AccountingM2MDaily m2mDaily);


	@DoNotAddRequestMapping
	public void deleteAccountingM2MDaily(AccountingM2MDaily bean);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingM2MDailyExpense saveAccountingM2MDailyExpense(AccountingM2MDailyExpense expense);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingM2MDailyExpense getAccountingM2MDailyExpenseForSpecificMark(AccountingM2MDaily m2mDaily, AccountingM2MDailyExpenseType type, InvestmentSecurity expenseCurrency, Integer sourceFkFieldId);


	public List<AccountingM2MDailyExpense> getAccountingM2MDailyExpenseListByMark(Integer m2mDailyId);


	public List<AccountingM2MDailyExpense> getAccountingM2MDailyExpenseList(AccountingM2MDailyExpenseSearchForm searchForm);


	/**
	 * Saves a group of {@link AccountingM2MDailyExpense} records.
	 *
	 * @param expenseGroup  the group of expenses to save
	 * @param errorIfExists if true, indicates that an error should be returned if there is an existing {@link AccountingM2MDailyExpense} with the same
	 *                      client account, holding account, GL expense accounting account, and mark date.
	 */
	@SecureMethod(dtoClass = AccountingM2MDaily.class)
	public void saveAccountingM2MExpenseGroup(AccountingM2MExpenseGroup expenseGroup, boolean errorIfExists);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public AccountingM2MDailyExpenseType getAccountingM2MDailyExpenseType(short id);


	public AccountingM2MDailyExpenseType getAccountingM2MDailyExpenseTypeByName(String name);


	public AccountingM2MDailyExpenseType getAccountingM2MDailyExpenseTypeByExternalNameAndAmount(String externalName, BigDecimal expenseAmount);


	public AccountingM2MDailyExpenseType getAccountingM2MDailyExpenseTypeByAccountingAccount(short accountingAccountId);


	public List<AccountingM2MDailyExpenseType> getAccountingM2MDailyExpenseTypeList(AccountingM2MDailyExpenseTypeSearchForm searchForm);


	public AccountingM2MDailyExpenseType saveAccountingM2MDailyExpenseType(AccountingM2MDailyExpenseType bean);


	public void deleteAccountingM2MDailyExpenseType(short id);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the "Mark to Market" purpose investment account (our or custodian) for the "Mark to Market" account
	 * linked to the specified holding account on the specified date.
	 *
	 * @throws ValidationException if the relationship doesn't exist or more than one is found.
	 */
	@DoNotAddRequestMapping
	public InvestmentAccount getAccountingM2MInvestmentAccount(InvestmentAccount holdingAccount, Date markDate, boolean ourAccount);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public boolean isM2MinLocalCurrency(InvestmentAccount account);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns "Live" positions for the specified parameters. Calculates everything real time instead of looking up date in pre-built snapshot.
	 * Includes currency balances in results even though currency is not a position.
	 * All results are calculated using Settlement Date lookup.
	 */
	@RequestMapping("accountingPositionDailyLiveForM2MList") // return ArrayList vs PagingArrayList
	public List<AccountingPositionDaily> getAccountingPositionDailyLiveForM2MList(AccountingPositionDailyLiveSearchForm searchForm);


	/**
	 * Populates the PAI amount for each position in the list.
	 */
	@DoNotAddRequestMapping
	public void populateAccountingPositionDailyPAI(List<AccountingPositionDaily> positionList, Date markStartDate, Date markDate);
}
