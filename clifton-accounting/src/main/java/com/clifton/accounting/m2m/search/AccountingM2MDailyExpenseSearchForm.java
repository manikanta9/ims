package com.clifton.accounting.m2m.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class AccountingM2MDailyExpenseSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "m2mDaily.id")
	private Integer m2mDailyId;

	@SearchField(searchField = "accountingAccount.id", searchFieldPath = "type")
	private Short accountingAccountId;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String typeName;

	@SearchField(searchField = "m2mDaily.markDate")
	private Date markDate;

	@SearchField(searchField = "expenseCurrency.id")
	private Integer expenseCurrencyId;

	@SearchField(searchField = "clientInvestmentAccount.id", searchFieldPath = "m2mDaily")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.id", searchFieldPath = "m2mDaily")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "m2mDaily.holdingInvestmentAccount")
	private Integer holdingAccountIssuerId;

	@SearchField(searchField = "type.id", searchFieldPath = "m2mDaily.holdingInvestmentAccount")
	private Short holdingInvestmentAccountTypeId;

	@SearchField(searchField = "name", searchFieldPath = "m2mDaily.holdingInvestmentAccount.type", comparisonConditions = ComparisonConditions.EQUALS)
	private String holdingInvestmentAccountType;

	@SearchField(searchField = "name", searchFieldPath = "m2mDaily.holdingInvestmentAccount.type", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String holdingInvestmentAccountTypeNotEqual;

	@SearchField
	private Integer sourceFkFieldId;

	@SearchField(searchFieldPath = "type")
	private Boolean adjustExpectedTransferAmount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getM2mDailyId() {
		return this.m2mDailyId;
	}


	public void setM2mDailyId(Integer m2mDailyId) {
		this.m2mDailyId = m2mDailyId;
	}


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public Date getMarkDate() {
		return this.markDate;
	}


	public void setMarkDate(Date markDate) {
		this.markDate = markDate;
	}


	public Integer getExpenseCurrencyId() {
		return this.expenseCurrencyId;
	}


	public void setExpenseCurrencyId(Integer expenseCurrencyId) {
		this.expenseCurrencyId = expenseCurrencyId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public Short getHoldingInvestmentAccountTypeId() {
		return this.holdingInvestmentAccountTypeId;
	}


	public void setHoldingInvestmentAccountTypeId(Short holdingInvestmentAccountTypeId) {
		this.holdingInvestmentAccountTypeId = holdingInvestmentAccountTypeId;
	}


	public String getHoldingInvestmentAccountType() {
		return this.holdingInvestmentAccountType;
	}


	public void setHoldingInvestmentAccountType(String holdingInvestmentAccountType) {
		this.holdingInvestmentAccountType = holdingInvestmentAccountType;
	}


	public String getHoldingInvestmentAccountTypeNotEqual() {
		return this.holdingInvestmentAccountTypeNotEqual;
	}


	public void setHoldingInvestmentAccountTypeNotEqual(String holdingInvestmentAccountTypeNotEqual) {
		this.holdingInvestmentAccountTypeNotEqual = holdingInvestmentAccountTypeNotEqual;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public Boolean getAdjustExpectedTransferAmount() {
		return this.adjustExpectedTransferAmount;
	}


	public void setAdjustExpectedTransferAmount(Boolean adjustExpectedTransferAmount) {
		this.adjustExpectedTransferAmount = adjustExpectedTransferAmount;
	}
}
