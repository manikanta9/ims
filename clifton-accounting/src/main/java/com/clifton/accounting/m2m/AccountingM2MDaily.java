package com.clifton.accounting.m2m;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The <code>AccountingM2MDaily</code> class represents daily Mark To Market by holding account.
 * All amounts are in holding account's base currency.
 *
 * @author vgomelsky
 */
public class AccountingM2MDaily extends BaseEntity<Integer> implements BookableEntity {

	public static final String HOLD_ACCOUNT_PATTERN = "(Hold)";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private InvestmentAccount holdingInvestmentAccount;
	/**
	 * Optional client account if there's one mapped.  If multiple client accounts are associated with
	 * above holding account, then  selects one with smallest account number.
	 */
	private InvestmentAccount clientInvestmentAccount;

	/**
	 * Custodian investment account that is related to the holding account via Mark to Market relationship type.
	 */
	private InvestmentAccount custodianInvestmentAccount;

	/**
	 * Optional currency of the mark. If null, then the mark is in base currency of client's investment account.
	 * If not null, then the mark is for that currency only and does not include securities denominated in other currencies.
	 */
	private InvestmentSecurity markCurrency;

	private Date markDate;

	/**
	 * Our mark calculation for the day based on change in OTE.
	 * This amount may also include related expenses (AccountingM2MDailyExpense) depending on inclusion attributes.
	 */
	private BigDecimal ourTransferAmount;
	/**
	 * Transfer amount expected by the issuer of the holding account. Ideally it's the same as ourMarkAmount.
	 * However, it can be different if they have different calculations or if the transfer amount includes
	 * additional information: previous mark(s) that didn't get wired, collateral move, interest payment, etc.
	 */
	private BigDecimal expectedTransferAmount;
	/**
	 * Actual transfer amount that we agreed upon during reconciliation with holding account issuer.
	 * If expectedTransferAmount and ourTransferAmount are the same, then transferAmount will also be the same.
	 * During reconciliation, ourTransferAmount can be adjusted to account for any discrepancies.
	 */
	private BigDecimal transferAmount;

	private boolean reconciled;
	private String note;

	private Date bookingDate;

	private List<AccountingM2MDailyExpense> expenseList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns Our Mark Amount: calculated by taking Our Transfer Amount and baking out our expenses.
	 */
	public BigDecimal getOurMarkAmount() {
		return MathUtils.subtract(getOurTransferAmount(), getAdjustmentAmountInMarkCurrency());
	}


	/**
	 * Returns Expected Mark Amount: calculated by taking Expected Transfer Amount and baking out expected expenses.
	 */
	public BigDecimal getExpectedMarkAmount() {
		return MathUtils.subtract(getExpectedTransferAmount(), getExpectedAdjustmentAmount());
	}


	public BigDecimal getDifferenceInMarkAmount() {
		BigDecimal ourMark = getOurMarkAmount();
		BigDecimal expectedMark = getExpectedMarkAmount();
		if (expectedMark == null) {
			return (ourMark == null) ? BigDecimal.ZERO : ourMark;
		}
		if (ourMark == null) {
			return expectedMark.negate();
		}
		return ourMark.subtract(expectedMark);
	}


	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getOurAdjustmentAmount() {
		BigDecimal result = BigDecimal.ZERO;
		for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(getExpenseList())) {
			result = MathUtils.add(result, expense.getExpenseAmountInMarkCurrency());
		}
		return result;
	}


	public BigDecimal getExpectedAdjustmentAmount() {
		BigDecimal result = BigDecimal.ZERO;
		for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(getExpenseList())) {
			result = MathUtils.add(result, MathUtils.multiply(ObjectUtils.coalesce(expense.getExpectedExpenseAmount(), expense.getExpenseAmountInMarkCurrency()), expense.getExpenseFxRate(), 2));
		}
		return result;
	}


	public BigDecimal getAdjustmentAmountInMarkCurrency() {
		BigDecimal total = BigDecimal.ZERO;

		for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(getExpenseList())) {
			total = MathUtils.add(expense.getExpenseAmountInMarkCurrency(), total);
		}

		return total;
	}


	public BigDecimal getDifferenceInAdjustmentAmount() {
		BigDecimal ourAdjustment = getOurAdjustmentAmount();
		BigDecimal expectedAdjustment = getExpectedAdjustmentAmount();
		if (expectedAdjustment == null) {
			return (ourAdjustment == null) ? BigDecimal.ZERO : ourAdjustment;
		}
		if (ourAdjustment == null) {
			return expectedAdjustment.negate();
		}
		return ourAdjustment.subtract(expectedAdjustment);
	}


	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the difference between ourTransferAmount and expectedTransferAmount.
	 * Ideally this should be 0.
	 */
	public BigDecimal getDifferenceInTransferAmount() {
		if (this.expectedTransferAmount == null) {
			return (this.ourTransferAmount == null) ? BigDecimal.ZERO : this.ourTransferAmount;
		}
		if (this.ourTransferAmount == null) {
			return this.expectedTransferAmount.negate();
		}
		return this.ourTransferAmount.subtract(this.expectedTransferAmount);
	}


	/**
	 * If the mark was reconciled, returns Transfer Amount.
	 * Otherwise, returns Our Mark Amount.
	 */
	public BigDecimal getCoalesceTransferOurMarkAmount() {
		if (isReconciled()) {
			return getTransferAmount();
		}
		return getOurMarkAmount();
	}


	/**
	 * Returns true if holding investment account is a hold account (no daily m2m transfers as long as the balance is positive).
	 */
	public boolean isHoldAccount() {
		if (this.holdingInvestmentAccount != null) {
			InvestmentAccountType type = this.holdingInvestmentAccount.getType();
			if (type != null && type.getName() != null && type.getName().contains(HOLD_ACCOUNT_PATTERN)) {
				return true;
			}
		}
		return false;
	}


	public boolean isBooked() {
		return (this.bookingDate != null);
	}


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(100);
		result.append(this.holdingInvestmentAccount == null ? "NO HOLDING ACCOUNT" : this.holdingInvestmentAccount.getLabel());
		result.append(" on ");
		result.append(DateUtils.fromDateShort(this.markDate));
		if (this.markCurrency != null) {
			result.append(" in ");
			result.append(this.markCurrency.getSymbol());
		}
		return result.toString();
	}


	@Override
	public Set<IdentityObject> getEntityLockSet() {
		return CollectionUtils.createHashSet(getClientInvestmentAccount());
	}


	@Override
	public Date getBookingDate() {
		return this.bookingDate;
	}


	@Override
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public Date getMarkDate() {
		return this.markDate;
	}


	public void setMarkDate(Date markDate) {
		this.markDate = markDate;
	}


	public BigDecimal getOurTransferAmount() {
		return this.ourTransferAmount;
	}


	public void setOurTransferAmount(BigDecimal ourTransferAmount) {
		this.ourTransferAmount = ourTransferAmount;
	}


	public BigDecimal getExpectedTransferAmount() {
		return this.expectedTransferAmount;
	}


	public void setExpectedTransferAmount(BigDecimal expectedTransferAmount) {
		this.expectedTransferAmount = expectedTransferAmount;
	}


	public BigDecimal getTransferAmount() {
		return this.transferAmount;
	}


	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}


	public boolean isReconciled() {
		return this.reconciled;
	}


	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public InvestmentSecurity getMarkCurrency() {
		return this.markCurrency;
	}


	public void setMarkCurrency(InvestmentSecurity markCurrency) {
		this.markCurrency = markCurrency;
	}


	public InvestmentAccount getCustodianInvestmentAccount() {
		return this.custodianInvestmentAccount;
	}


	public void setCustodianInvestmentAccount(InvestmentAccount custodianInvestmentAccount) {
		this.custodianInvestmentAccount = custodianInvestmentAccount;
	}


	public List<AccountingM2MDailyExpense> getExpenseList() {
		return this.expenseList;
	}


	public void setExpenseList(List<AccountingM2MDailyExpense> expenseList) {
		this.expenseList = expenseList;
	}
}
