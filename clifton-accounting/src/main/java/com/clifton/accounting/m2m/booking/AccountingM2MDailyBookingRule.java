package com.clifton.accounting.m2m.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingM2MDailyBookingRule</code> class is a booking rule that handles Mark to Market
 * cash transfers: transfer cash from broker to custodian or vice versa.
 *
 * @author vgomelsky
 */
public class AccountingM2MDailyBookingRule implements AccountingBookingRule<AccountingM2MDaily> {

	private AccountingAccountService accountingAccountService;
	private AccountingM2MService accountingM2MService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<AccountingM2MDaily> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingM2MDaily m2m = bookingSession.getBookableEntity();

		// skip zero amount transfers
		if (!MathUtils.isNullOrZero(m2m.getTransferAmount())) {
			String description = (isBaseCurrency(m2m) ? "M2M cash transfer" : "M2M currency transfer");
			if (InvestmentAccountType.OTC_CLEARED.equals(m2m.getHoldingInvestmentAccount().getType().getName())) {
				// different descriptions for cleared vs non-cleared marks to assist accounting with cash reconciliation
				description = "Cleared OTC " + description;
			}
			journal.setDescription(StringUtils.isEmpty(m2m.getNote()) ? description : m2m.getNote());
			journal.addJournalDetail(generateJournalDetail(journal, m2m, true, description));
			journal.addJournalDetail(generateJournalDetail(journal, m2m, false, description));
		}
	}


	private AccountingJournalDetail generateJournalDetail(AccountingJournal journal, AccountingM2MDaily m2m, boolean mainEntry, String description) {
		Date markDate = m2m.getMarkDate();

		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(journal.getFkFieldId());
		detail.setSystemTable(journal.getSystemTable());
		if (m2m.getClientInvestmentAccount() == null) {
			throw new FieldValidationException("Client Investment Account is required for Mark to Market entry: " + m2m, "clientInvestmentAccount");
		}
		detail.setClientInvestmentAccount(m2m.getClientInvestmentAccount());
		// usually full m2m is in base currency of holding account but it could also be in local currency of each position
		if (isBaseCurrency(m2m)) {
			detail.setInvestmentSecurity(m2m.getHoldingInvestmentAccount().getBaseCurrency());
		}
		else {
			detail.setInvestmentSecurity(m2m.getMarkCurrency());
		}
		detail.setDescription(description);

		BigDecimal amount = m2m.getTransferAmount();
		if (mainEntry) {
			amount = amount.negate();
			detail.setHoldingInvestmentAccount(m2m.getHoldingInvestmentAccount());
		}
		else {
			InvestmentAccount m2mAccount = getAccountingM2MService().getAccountingM2MInvestmentAccount(m2m.getHoldingInvestmentAccount(), markDate, false);
			detail.setHoldingInvestmentAccount(m2mAccount);
		}

		// money market accounts have purchase/sale of Money Market as opposed to Cash
		if (AccountingAccount.ASSET_MONEY_MARKET.equals(detail.getHoldingInvestmentAccount().getType().getName())) {
			ValidationUtils.assertTrue(isBaseCurrency(m2m), "Money Market transfers are not supported for currencies. Holding Account: " + m2m.getHoldingInvestmentAccount().getLabel());
			detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_MONEY_MARKET));
		}
		else {
			if (isBaseCurrency(m2m)) {
				detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CASH));
			}
			else {
				detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
			}
		}

		detail.setLocalDebitCredit(amount);
		if (isBaseCurrency(m2m)) {
			detail.setBaseDebitCredit(amount);
			detail.setPositionCostBasis(BigDecimal.ZERO);
			detail.setExchangeRateToBase(BigDecimal.ONE);
		}
		else {
			// need to lookup latest FX rate from holding company
			String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(detail);
			String toCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(detail);
			BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(detail.getHoldingInvestmentAccount().toHoldingAccount(), fromCurrency, toCurrency, markDate).flexibleLookup());
			detail.setExchangeRateToBase(fxRate);
			detail.setBaseDebitCredit(MathUtils.multiply(amount, fxRate, 2));
			detail.setPositionCostBasis(amount);
			detail.setPositionCommission(BigDecimal.ZERO);
		}
		detail.setQuantity(null);
		detail.setPrice(null);
		detail.setOpening(true);

		detail.setTransactionDate(markDate);
		detail.setOriginalTransactionDate(markDate);
		// mark is done for previous business day so transfer and settlement is one business day after the mark
		detail.setSettlementDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(markDate), 1));

		return detail;
	}


	/**
	 * Returns true if the mark is in base currency of the holding account: Cash.
	 */
	private boolean isBaseCurrency(AccountingM2MDaily m2m) {
		if (m2m.getMarkCurrency() != null) {
			if (!m2m.getMarkCurrency().equals(m2m.getHoldingInvestmentAccount().getBaseCurrency())) {
				return false;
			}
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
