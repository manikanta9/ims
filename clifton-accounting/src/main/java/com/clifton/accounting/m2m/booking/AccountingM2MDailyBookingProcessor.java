package com.clifton.accounting.m2m.booking;


import com.clifton.accounting.gl.booking.processor.SameRulesBookingProcessor;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AccountingM2MDailyBookingProcessor extends SameRulesBookingProcessor<AccountingM2MDaily> {

	public static final String JOURNAL_NAME = "Mark to Market Journal";

	private AccountingM2MService accountingM2MService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJournalTypeName() {
		return JOURNAL_NAME;
	}


	@Override
	public AccountingM2MDaily getBookableEntity(Serializable id) {
		return getAccountingM2MService().getAccountingM2MDailyPopulated((Integer) id);
	}


	@Override
	public List<AccountingM2MDaily> getUnbookedEntityList() {
		// return unbooked but reconciled with non 0 transfer amount (excludes "Hold" accounts)
		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setBooked(false);
		searchForm.setReconciled(true);
		searchForm.setHoldingAccountIsHold(false);
		searchForm.addSearchRestriction(new SearchRestriction("transferAmount", ComparisonConditions.NOT_EQUALS, BigDecimal.ZERO));
		List<AccountingM2MDaily> unbookedList = getAccountingM2MService().getAccountingM2MDailyList(searchForm);

		// need to exclude entries the day before a holiday (don't get booked)
		Calendar m2mCalendar = getCalendarSetupService().getCalendarByName(getAccountingM2MService().getAccountingMarkToMarketCalendarName());
		ValidationUtils.assertNotNull(m2mCalendar, "Cannot find Calendar with name: " + getAccountingM2MService().getAccountingMarkToMarketCalendarName());
		List<AccountingM2MDaily> result = new ArrayList<>();
		for (AccountingM2MDaily m2m : CollectionUtils.getIterable(unbookedList)) {
			Date date = DateUtils.getNextWeekday(m2m.getMarkDate());
			if (getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(date, m2mCalendar.getId()))) {
				result.add(m2m);
			}
		}

		return result;
	}


	@Override
	public SimpleBookingSession<AccountingM2MDaily> newBookingSession(AccountingJournal journal, AccountingM2MDaily m2mDaily) {
		if (m2mDaily == null) {
			m2mDaily = getBookableEntity(journal.getFkFieldId());
			ValidationUtils.assertNotNull(m2mDaily, "Cannot find daily mark to market with id = " + journal.getFkFieldId());
		}

		return new SimpleBookingSession<>(journal, m2mDaily);
	}


	@Override
	public AccountingM2MDaily markBooked(AccountingM2MDaily m2mDaily) {
		ValidationUtils.assertNull(m2mDaily.getBookingDate(), "Cannot book Mark To Market with id = " + m2mDaily.getId() + " because it already has bookingDate set to " + m2mDaily.getBookingDate());
		m2mDaily.setBookingDate(new Date());
		return getAccountingM2MService().saveAccountingM2MDaily(m2mDaily);
	}


	@Override
	public AccountingM2MDaily markUnbooked(int m2mDailyId) {
		AccountingM2MDaily m2mDaily = getBookableEntity(m2mDailyId);
		m2mDaily.setBookingDate(null);
		return getAccountingM2MService().saveAccountingM2MDaily(m2mDaily);
	}


	@Override
	public void deleteSourceEntity(int m2mDailyId) {
		throw new IllegalArgumentException("Cannot delete daily mark to market entries from unbooking process.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
