package com.clifton.accounting.m2m.instruction.handler;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.InstructionMessage;


public interface AccountingPositionInstructionHandler {

	InstructionMessage generateInstructionMessage(Instruction instruction, AccountingPositionTransferDetail positionTransferDetail);


	boolean accepts(AccountingPositionTransfer accountingPositionTransfer);
}
