package com.clifton.accounting.m2m.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingM2MDailySearchForm</code> class defines search configuration for AccountingM2MDaily objects.
 *
 * @author vgomelsky
 */
public class AccountingM2MDailySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "holdingInvestmentAccount.id")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "type.id", searchFieldPath = "holdingInvestmentAccount")
	private Short holdingInvestmentAccountTypeId;

	@SearchField(searchField = "name", searchFieldPath = "holdingInvestmentAccount.type", comparisonConditions = ComparisonConditions.EQUALS)
	private String holdingInvestmentAccountType;

	@SearchField(searchField = "name", searchFieldPath = "holdingInvestmentAccount.type", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String holdingInvestmentAccountTypeNotEqual;

	@SearchField(searchField = "number", searchFieldPath = "holdingInvestmentAccount")
	private String holdingInvestmentAccountNumber;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingInvestmentAccount")
	private Integer holdingAccountIssuerId;

	@SearchField(searchField = "name", searchFieldPath = "holdingInvestmentAccount.issuingCompany")
	private String holdingAccountIssuerName;

	// Custom Search Field
	private Integer holdingAccountGroupId;

	// Custom Search Field
	private Integer[] holdingAccountGroupIds;

	// Custom Search Field
	private Integer excludeHoldingAccountGroupId;

	// Custom Search Field
	private Integer[] excludeHoldingAccountGroupIds;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchField = "number,name", searchFieldPath = "clientInvestmentAccount", sortField = "number")
	private String clientInvestmentAccount;

	@SearchField(searchField = "custodianInvestmentAccount.id")
	private Integer custodianInvestmentAccountId;

	@SearchField(searchField = "type.id", searchFieldPath = "custodianInvestmentAccount")
	private Short custodianInvestmentAccountTypeId;

	@SearchField(searchFieldPath = "custodianInvestmentAccount", searchField = "name")
	private String custodianInvestmentAccountName;

	@SearchField(searchFieldPath = "custodianInvestmentAccount", searchField = "number")
	private String custodianInvestmentAccountNumber;

	@SearchField(searchField = "name", searchFieldPath = "custodianInvestmentAccount.issuingCompany")
	private String custodianInvestmentAccountIssuerName;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "custodianInvestmentAccount")
	private Integer custodianInvestmentAccountIssuerId;

	@SearchField(searchField = "markCurrency.id")
	private Integer markCurrencyId;

	// Custom Search Filter - If no currency on M2M record, uses Client Account's Base CCY
	private Integer coalesceMarkClientAccountCurrencyId;

	@SearchField(searchField = "symbol", searchFieldPath = "markCurrency")
	private String markCurrency;

	@SearchField(searchField = "number", searchFieldPath = "clientInvestmentAccount")
	private String clientInvestmentAccountNumber;

	// custom search filter
	private Boolean holdingAccountIsHold;

	// Custom Search Filter
	private Integer clientInvestmentAccountGroupId;

	@SearchField
	private Date markDate;

	@SearchField(searchField = "markDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date startMarkDate;

	@SearchField(searchField = "markDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date endMarkDate;

	@SearchField
	private BigDecimal ourTransferAmount;
	@SearchField
	private BigDecimal expectedTransferAmount;
	// custom filter: ourTransferAmount - expectedTransferAmount
	private BigDecimal differenceInTransferAmount;

	@SearchField
	private BigDecimal transferAmount;

	@SearchField(searchField = "transferAmount", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private BigDecimal notTransferAmount;

	@SearchField
	private Boolean reconciled;

	@SearchField
	private String note;

	@SearchField
	private Date bookingDate;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean booked;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Date getMarkDate() {
		return this.markDate;
	}


	public void setMarkDate(Date markDate) {
		this.markDate = markDate;
	}


	public BigDecimal getOurTransferAmount() {
		return this.ourTransferAmount;
	}


	public void setOurTransferAmount(BigDecimal ourTransferAmount) {
		this.ourTransferAmount = ourTransferAmount;
	}


	public BigDecimal getExpectedTransferAmount() {
		return this.expectedTransferAmount;
	}


	public void setExpectedTransferAmount(BigDecimal expectedTransferAmount) {
		this.expectedTransferAmount = expectedTransferAmount;
	}


	public BigDecimal getTransferAmount() {
		return this.transferAmount;
	}


	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getHoldingInvestmentAccountNumber() {
		return this.holdingInvestmentAccountNumber;
	}


	public void setHoldingInvestmentAccountNumber(String holdingInvestmentAccountNumber) {
		this.holdingInvestmentAccountNumber = holdingInvestmentAccountNumber;
	}


	public String getClientInvestmentAccountNumber() {
		return this.clientInvestmentAccountNumber;
	}


	public void setClientInvestmentAccountNumber(String clientInvestmentAccountNumber) {
		this.clientInvestmentAccountNumber = clientInvestmentAccountNumber;
	}


	public String getHoldingAccountIssuerName() {
		return this.holdingAccountIssuerName;
	}


	public void setHoldingAccountIssuerName(String holdingAccountIssuerName) {
		this.holdingAccountIssuerName = holdingAccountIssuerName;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Boolean getBooked() {
		return this.booked;
	}


	public void setBooked(Boolean booked) {
		this.booked = booked;
	}


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public String getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(String clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public Integer getCustodianInvestmentAccountId() {
		return this.custodianInvestmentAccountId;
	}


	public void setCustodianInvestmentAccountId(Integer custodianInvestmentAccountId) {
		this.custodianInvestmentAccountId = custodianInvestmentAccountId;
	}


	public String getCustodianInvestmentAccountName() {
		return this.custodianInvestmentAccountName;
	}


	public void setCustodianInvestmentAccountName(String custodianInvestmentAccountName) {
		this.custodianInvestmentAccountName = custodianInvestmentAccountName;
	}


	public String getCustodianInvestmentAccountNumber() {
		return this.custodianInvestmentAccountNumber;
	}


	public void setCustodianInvestmentAccountNumber(String custodianInvestmentAccountNumber) {
		this.custodianInvestmentAccountNumber = custodianInvestmentAccountNumber;
	}


	public String getCustodianInvestmentAccountIssuerName() {
		return this.custodianInvestmentAccountIssuerName;
	}


	public void setCustodianInvestmentAccountIssuerName(String custodianInvestmentAccountIssuerName) {
		this.custodianInvestmentAccountIssuerName = custodianInvestmentAccountIssuerName;
	}


	public BigDecimal getDifferenceInTransferAmount() {
		return this.differenceInTransferAmount;
	}


	public void setDifferenceInTransferAmount(BigDecimal differenceInTransferAmount) {
		this.differenceInTransferAmount = differenceInTransferAmount;
	}


	public Date getStartMarkDate() {
		return this.startMarkDate;
	}


	public void setStartMarkDate(Date startMarkDate) {
		this.startMarkDate = startMarkDate;
	}


	public Date getEndMarkDate() {
		return this.endMarkDate;
	}


	public void setEndMarkDate(Date endMarkDate) {
		this.endMarkDate = endMarkDate;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Boolean getHoldingAccountIsHold() {
		return this.holdingAccountIsHold;
	}


	public void setHoldingAccountIsHold(Boolean holdingAccountIsHold) {
		this.holdingAccountIsHold = holdingAccountIsHold;
	}


	public Integer getMarkCurrencyId() {
		return this.markCurrencyId;
	}


	public void setMarkCurrencyId(Integer markCurrencyId) {
		this.markCurrencyId = markCurrencyId;
	}


	public String getMarkCurrency() {
		return this.markCurrency;
	}


	public void setMarkCurrency(String markCurrency) {
		this.markCurrency = markCurrency;
	}


	public Integer getCustodianInvestmentAccountIssuerId() {
		return this.custodianInvestmentAccountIssuerId;
	}


	public void setCustodianInvestmentAccountIssuerId(Integer custodianInvestmentAccountIssuerId) {
		this.custodianInvestmentAccountIssuerId = custodianInvestmentAccountIssuerId;
	}


	public Short getHoldingInvestmentAccountTypeId() {
		return this.holdingInvestmentAccountTypeId;
	}


	public void setHoldingInvestmentAccountTypeId(Short holdingInvestmentAccountTypeId) {
		this.holdingInvestmentAccountTypeId = holdingInvestmentAccountTypeId;
	}


	public Short getCustodianInvestmentAccountTypeId() {
		return this.custodianInvestmentAccountTypeId;
	}


	public void setCustodianInvestmentAccountTypeId(Short custodianInvestmentAccountTypeId) {
		this.custodianInvestmentAccountTypeId = custodianInvestmentAccountTypeId;
	}


	public Integer getCoalesceMarkClientAccountCurrencyId() {
		return this.coalesceMarkClientAccountCurrencyId;
	}


	public void setCoalesceMarkClientAccountCurrencyId(Integer coalesceMarkClientAccountCurrencyId) {
		this.coalesceMarkClientAccountCurrencyId = coalesceMarkClientAccountCurrencyId;
	}


	public String getHoldingInvestmentAccountType() {
		return this.holdingInvestmentAccountType;
	}


	public void setHoldingInvestmentAccountType(String holdingInvestmentAccountType) {
		this.holdingInvestmentAccountType = holdingInvestmentAccountType;
	}


	public String getHoldingInvestmentAccountTypeNotEqual() {
		return this.holdingInvestmentAccountTypeNotEqual;
	}


	public void setHoldingInvestmentAccountTypeNotEqual(String holdingInvestmentAccountTypeNotEqual) {
		this.holdingInvestmentAccountTypeNotEqual = holdingInvestmentAccountTypeNotEqual;
	}


	public Integer getHoldingAccountGroupId() {
		return this.holdingAccountGroupId;
	}


	public void setHoldingAccountGroupId(Integer holdingAccountGroupId) {
		this.holdingAccountGroupId = holdingAccountGroupId;
	}


	public Integer getExcludeHoldingAccountGroupId() {
		return this.excludeHoldingAccountGroupId;
	}


	public void setExcludeHoldingAccountGroupId(Integer excludeHoldingAccountGroupId) {
		this.excludeHoldingAccountGroupId = excludeHoldingAccountGroupId;
	}


	public BigDecimal getNotTransferAmount() {
		return this.notTransferAmount;
	}


	public void setNotTransferAmount(BigDecimal notTransferAmount) {
		this.notTransferAmount = notTransferAmount;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public Integer[] getHoldingAccountGroupIds() {
		return this.holdingAccountGroupIds;
	}


	public void setHoldingAccountGroupIds(Integer[] holdingAccountGroupIds) {
		this.holdingAccountGroupIds = holdingAccountGroupIds;
	}


	public Integer[] getExcludeHoldingAccountGroupIds() {
		return this.excludeHoldingAccountGroupIds;
	}


	public void setExcludeHoldingAccountGroupIds(Integer[] excludeHoldingAccountGroupIds) {
		this.excludeHoldingAccountGroupIds = excludeHoldingAccountGroupIds;
	}
}
