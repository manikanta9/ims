package com.clifton.accounting.m2m.instruction;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instruction.selector.BaseInvestmentInstructionSelector;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BaseAccountingM2MInstructionSelector</code> ...
 *
 * @author manderson
 */
public abstract class BaseAccountingM2MInstructionSelector extends BaseInvestmentInstructionSelector<AccountingM2MDaily> {

	private AccountingM2MService accountingM2MService;

	///////////////////////////////////////////////////

	private Integer holdingAccountId;

	private Integer holdingAccountIssuerId;

	private Short holdingAccountTypeId;

	private List<Integer> includeHoldingAccountGroupIds;

	private List<Integer> excludeHoldingAccountGroupIds;

	private Integer clientAccountId;

	private Integer custodianAccountId;

	private Short custodianAccountTypeId;

	private Integer custodianAccountIssuerId;

	private Integer markCurrencyId;

	private boolean includeZeroTransferAmount;


	////////////////////////////////////////////////////


	@Override
	public String getInstructionItemLabel(AccountingM2MDaily entity) {
		return entity.getLabel();
	}


	@Override
	public boolean isInstructionItemBooked(InvestmentInstructionCategory category, AccountingM2MDaily entity) {
		return entity.getBookingDate() != null;
	}


	@Override
	public void populateInstructionSelectionDetails(InvestmentInstructionSelection selection, AccountingM2MDaily entity) {
		selection.setClientAccount(entity.getClientInvestmentAccount());
		selection.setHoldingAccount(entity.getHoldingInvestmentAccount());
		selection.setSecurity(entity.getMarkCurrency() == null ? entity.getHoldingInvestmentAccount().getBaseCurrency() : entity.getMarkCurrency());
		selection.setBooked(isInstructionItemBooked(selection.getCategory(), entity));
		selection.setDescription("Transfer Amount: " + CoreMathUtils.formatNumberMoney(entity.getTransferAmount()));
	}


	////////////////////////////////////////////////////


	@Override
	public List<AccountingM2MDaily> getEntityList(InvestmentInstructionCategory category, InvestmentInstructionDefinition definition, Date date) {
		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setMarkDate(date);
		searchForm.setClientInvestmentAccountId(getClientAccountId());
		searchForm.setCoalesceMarkClientAccountCurrencyId(getMarkCurrencyId());
		searchForm.setHoldingAccountGroupIds(CollectionUtils.toArrayOrNull(getIncludeHoldingAccountGroupIds(), Integer.class));
		searchForm.setExcludeHoldingAccountGroupIds(CollectionUtils.toArrayOrNull(getExcludeHoldingAccountGroupIds(), Integer.class));

		// Only include those with an actual Transfer Amount (Unless specified to Include 0 Amounts)
		// Note: Check Definition Exists so when looking for "missing" (no definition) we always see all
		if (definition != null && !isIncludeZeroTransferAmount()) {
			searchForm.addSearchRestriction(new SearchRestriction("transferAmount", ComparisonConditions.NOT_EQUALS, BigDecimal.ZERO));
		}

		// Specific Holding Account
		if (getHoldingAccountId() != null) {
			searchForm.setHoldingInvestmentAccountId(getHoldingAccountId());
		}
		// Or Issuer and/or Type
		else {
			searchForm.setHoldingAccountIssuerId(getHoldingAccountIssuerId());
			searchForm.setHoldingInvestmentAccountTypeId(getHoldingAccountTypeId());
		}
		// Specific Custodian Account
		if (getCustodianAccountId() != null) {
			searchForm.setCustodianInvestmentAccountId(getCustodianAccountId());
		}
		// Or Issuer and/or Type
		else {
			searchForm.setCustodianInvestmentAccountIssuerId(getCustodianAccountIssuerId());
			searchForm.setCustodianInvestmentAccountTypeId(getCustodianAccountTypeId());
		}

		return getAccountingM2MService().getAccountingM2MDailyList(searchForm);
	}


	////////////////////////////////////////////////////
	////           Getter & Setter Methods          ////
	////////////////////////////////////////////////////


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Short getCustodianAccountTypeId() {
		return this.custodianAccountTypeId;
	}


	public void setCustodianAccountTypeId(Short custodianAccountTypeId) {
		this.custodianAccountTypeId = custodianAccountTypeId;
	}


	public Integer getMarkCurrencyId() {
		return this.markCurrencyId;
	}


	public void setMarkCurrencyId(Integer markCurrencyId) {
		this.markCurrencyId = markCurrencyId;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer getCustodianAccountId() {
		return this.custodianAccountId;
	}


	public void setCustodianAccountId(Integer custodianAccountId) {
		this.custodianAccountId = custodianAccountId;
	}


	public Integer getCustodianAccountIssuerId() {
		return this.custodianAccountIssuerId;
	}


	public void setCustodianAccountIssuerId(Integer custodianAccountIssuerId) {
		this.custodianAccountIssuerId = custodianAccountIssuerId;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}


	public List<Integer> getIncludeHoldingAccountGroupIds() {
		return this.includeHoldingAccountGroupIds;
	}


	public void setIncludeHoldingAccountGroupIds(List<Integer> includeHoldingAccountGroupIds) {
		this.includeHoldingAccountGroupIds = includeHoldingAccountGroupIds;
	}


	public List<Integer> getExcludeHoldingAccountGroupIds() {
		return this.excludeHoldingAccountGroupIds;
	}


	public void setExcludeHoldingAccountGroupIds(List<Integer> excludeHoldingAccountGroupIds) {
		this.excludeHoldingAccountGroupIds = excludeHoldingAccountGroupIds;
	}


	public boolean isIncludeZeroTransferAmount() {
		return this.includeZeroTransferAmount;
	}


	public void setIncludeZeroTransferAmount(boolean includeZeroTransferAmount) {
		this.includeZeroTransferAmount = includeZeroTransferAmount;
	}
}
