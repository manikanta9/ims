package com.clifton.accounting.m2m.validation;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>AccountingM2MDailyValidator</code> class validates insert/update/delete to AccountingM2MDaily
 * objects using corresponding business rules:
 * <p/>
 * - Cannot delete or modify booked entries
 * - Only booking date and note can be updated for reconciled entries
 *
 * @author vgomelsky
 */
@Component
public class AccountingM2MDailyValidator extends SelfRegisteringDaoValidator<AccountingM2MDaily> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(AccountingM2MDaily bean, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			if (bean.getBookingDate() != null) {
				throw new ValidationException("Cannot delete daily mark to market that has been booked.");
			}
		}
		else if (config.isUpdate()) {
			AccountingM2MDaily originalBean = getOriginalBean(bean);
			ValidationUtils.assertNotNull(originalBean, "Cannot find daily m2m with id = " + bean.getId());
			if (originalBean.getBookingDate() != null && bean.getBookingDate() != null) {
				throw new ValidationException("Cannot modify daily mark to market that has been booked.");
			}

			if (originalBean.isReconciled() && bean.isReconciled()) {
				// only booking date and note can be updated for reconciled entries
				List<String> diffs = CoreCompareUtils.getNoEqualProperties(bean, originalBean, false, "expenseList", "ourMarkAmount", "expectedMarkAmount", "differenceInMarkAmount",
						"adjustmentAmountInMarkCurrency", "ourAdjustmentAmount", "expectedAdjustmentAmount", "differenceInAdjustmentAmount", "differenceInTransferAmount",
						"coalesceTransferOurMarkAmount", "booked", "bookingDate", "note");
				if (diffs != null) {
					if (!diffs.isEmpty()) {
						throw new FieldValidationException("Cannot update field '" + diffs.get(0) + "' for reconciled entry [" + bean.getLabel() + "]", diffs.get(0));
					}
				}
			}
		}
	}
}
