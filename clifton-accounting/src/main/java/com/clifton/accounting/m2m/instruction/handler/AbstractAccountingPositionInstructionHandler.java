package com.clifton.accounting.m2m.instruction.handler;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.UnaryOperator;


public abstract class AbstractAccountingPositionInstructionHandler<I extends Instruction> implements AccountingPositionInstructionHandler {

	public static final UnaryOperator<String> TRIM_TO_NULL = s -> Optional.ofNullable(s)
			.map(StringUtils::trim)
			.filter(t -> !t.isEmpty())
			.orElse(null);

	private BusinessCompanyUtilHandler businessCompanyUtilHandler;
	private InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler;
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	protected String getSenderBic() {
		String senderBIC = getBusinessCompanyUtilHandler().getParametricBusinessIdentifierCode();
		ValidationUtils.assertNotEmpty(senderBIC, "The sender BIC is required.");
		return senderBIC;
	}


	protected String getTransactionReferenceNumber(Instruction instruction, AccountingPositionTransfer positionTransfer, AccountingPositionTransferDetail positionTransferDetail) {
		return getInstructionUtilHandler().getTransactionReferenceNumber(Arrays.asList(instruction, positionTransfer, positionTransferDetail));
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public BusinessCompanyUtilHandler getBusinessCompanyUtilHandler() {
		return this.businessCompanyUtilHandler;
	}


	public void setBusinessCompanyUtilHandler(BusinessCompanyUtilHandler businessCompanyUtilHandler) {
		this.businessCompanyUtilHandler = businessCompanyUtilHandler;
	}


	public InstructionUtilHandler<I, InstructionDeliveryFieldCommand> getInstructionUtilHandler() {
		return this.instructionUtilHandler;
	}


	public void setInstructionUtilHandler(InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler) {
		this.instructionUtilHandler = instructionUtilHandler;
	}


	public InvestmentInstructionDeliveryUtilHandler getInvestmentInstructionDeliveryUtilHandler() {
		return this.investmentInstructionDeliveryUtilHandler;
	}


	public void setInvestmentInstructionDeliveryUtilHandler(InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler) {
		this.investmentInstructionDeliveryUtilHandler = investmentInstructionDeliveryUtilHandler;
	}
}
