package com.clifton.accounting.m2m.instruction;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;


/**
 * The <code>AccountingM2MCounterpartyInstructionSelector</code> defines the bean type
 * used to select corresponding {@link InvestmentInstructionDefinition} for each {@link AccountingM2MDaily} record
 * that defines the instructions for the "counterparty" i.e. Holding Account
 * that is sent to the custodian "Recipient"
 *
 * @author manderson
 */
public class AccountingM2MCounterpartyInstructionSelector extends BaseAccountingM2MInstructionSelector {

	@Override
	public BusinessCompany getRecipient(AccountingM2MDaily m2m) {
		if (m2m.getCustodianInvestmentAccount() == null) {
			throw new ValidationException("Custodian account is missing for M2M Record: [" + m2m.getLabel() + "].  Unable to generate instructions without a custodian issuer to use as the recipient.");
		}
		return m2m.getCustodianInvestmentAccount().getIssuingCompany();
	}
}
