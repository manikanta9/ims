package com.clifton.accounting.m2m.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class AccountingM2MDailyExpenseTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String name;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String externalName;

	@SearchField(searchField = "accountingAccount.id")
	private Short accountingAccountId;

	@SearchField(searchField = "expenseSourceFkTable.id")
	private Short expenseSourceFkTableId;

	@SearchField
	private Boolean income;

	@SearchField
	private Boolean deleteOnM2MRebuild;

	@SearchField
	private Boolean adjustExpectedTransferAmount;

	@SearchField
	private Integer daysToSettle;


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getExternalName() {
		return this.externalName;
	}


	public void setExternalName(String externalName) {
		this.externalName = externalName;
	}


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public Short getExpenseSourceFkTableId() {
		return this.expenseSourceFkTableId;
	}


	public void setExpenseSourceFkTableId(Short expenseSourceFkTableId) {
		this.expenseSourceFkTableId = expenseSourceFkTableId;
	}


	public Boolean getIncome() {
		return this.income;
	}


	public void setIncome(Boolean income) {
		this.income = income;
	}


	public Boolean getDeleteOnM2MRebuild() {
		return this.deleteOnM2MRebuild;
	}


	public void setDeleteOnM2MRebuild(Boolean deleteOnM2MRebuild) {
		this.deleteOnM2MRebuild = deleteOnM2MRebuild;
	}


	public Boolean getAdjustExpectedTransferAmount() {
		return this.adjustExpectedTransferAmount;
	}


	public void setAdjustExpectedTransferAmount(Boolean adjustExpectedTransferAmount) {
		this.adjustExpectedTransferAmount = adjustExpectedTransferAmount;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}
}
