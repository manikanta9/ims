package com.clifton.accounting.m2m.rebuild;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.m2m.M2MRebuildCommand;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mwacker
 */
public interface AccountingM2MRebuildService {


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Rebuilds daily mark to market for the specified date. Holding account (on the command param) is optional and, if not specified,
	 * will rebuild for all accounts.
	 *
	 * @param command encapsulates markDate, holdingInvestmentAccountId, clientInvestmentAccountGroupId,
	 *                includePreviousMark, excludePreviousDayMark, and deleteExistingMark
	 * @return returns stats for rebuild
	 */
	@ModelAttribute("result")
	public String rebuildAccountingM2MDaily(M2MRebuildCommand command);


	/**
	 * Mark all un-reconciled entries for the specified mark date as reconciled if the
	 * difference between our mark and expected transfer amount is within the specified threshold.
	 * Sets user friendly note if it was empty.
	 *
	 * @param markDate
	 * @param issuingCompanyId               optionally limit holding accounts to this issuer
	 * @param clientInvestmentAccountGroupId optionally limit to accounts in this group
	 * @param thresholdAmount
	 * @param useOurMark                     specifies whether our mark or externally expected transfer amount should be used
	 * @param useZeroForHoldAccounts         set transfer amount to 0 for reconciled hold accounts; set it to what 'useOurMark' calls for otherwise
	 * @param holdingAccountTypeId
	 * @param custodianCompanyId             optionally limit to custodian company issuers
	 * @return returns the number of rows that were reconciled
	 */
	@ModelAttribute("data")
	@RequestMapping("accountingM2MDailyReconcile")
	public int reconcileAccountingM2MDaily(Date markDate, Integer issuingCompanyId, Integer clientInvestmentAccountGroupId, BigDecimal thresholdAmount, boolean useOurMark,
	                                       boolean useZeroForHoldAccounts, Short holdingAccountTypeId, Integer custodianCompanyId);


	/**
	 * Book and post a journal for each unbooked but reconciled daily mark to market entry for selected date
	 * and optional issuing company or client account.
	 *
	 * @param markDate
	 * @param holdingAccountTypeId
	 * @param issuingCompanyId
	 * @param clientInvestmentAccountGroupId optionally limit to accounts in this group
	 * @param clientInvestmentAccountId
	 * @param includeHoldAccounts
	 * @param allowHolidayPosting            if false and markDate is a day before a holiday, throw an exception
	 */
	@ModelAttribute("data")
	@RequestMapping("accountingM2MDailyBookAndPost")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public int bookAndPostAccountingM2MDaily(Date markDate, Short holdingAccountTypeId, Integer issuingCompanyId, Integer clientInvestmentAccountGroupId, Integer clientInvestmentAccountId,
	                                         boolean includeHoldAccounts, boolean allowHolidayPosting);


	/**
	 * Unpost and unbook each mark to market entry for the selected date and optional client account, client account group, or issuing company.  Also
	 * offers the option to unreconcile the entry in addition to unbooking.
	 *
	 * @param markDate
	 * @param holdingAccountTypeId
	 * @param issuingCompanyId               filter for specific issuing company
	 * @param clientInvestmentAccountGroupId filter for an account group
	 * @param clientInvestmentAccountId      filter for a specific account
	 * @param includeHoldAccounts
	 * @param unreconcile                    if true the unposted entry will also be unreconciled
	 * @return the number of unposted accounts
	 */
	@ModelAttribute("data")
	@RequestMapping("accountingM2MDailyUnpost")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public int unpostAccountingM2MDaily(Date markDate, Short holdingAccountTypeId, Integer issuingCompanyId, Integer clientInvestmentAccountGroupId, Integer clientInvestmentAccountId,
	                                    boolean includeHoldAccounts, boolean unreconcile);
}
