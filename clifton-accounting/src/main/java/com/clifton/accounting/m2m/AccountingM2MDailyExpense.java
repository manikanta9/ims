package com.clifton.accounting.m2m;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>AccountingM2MDailyExpense</code> class represents an expense item associated with corresponding mark.
 * For example, Interest Expense, Collateral Usage Fee, PAI, etc.  Expenses can be in different currencies.
 *
 * @author vgomelsky
 */
public class AccountingM2MDailyExpense extends BaseEntity<Integer> {

	private AccountingM2MDailyExpenseType type;

	private AccountingM2MDaily m2mDaily;
	private InvestmentSecurity expenseCurrency;

	/**
	 * If the expense is a price adjustment, then this field is the security for the adjustment.
	 */
	private Integer sourceFkFieldId;
	/**
	 * Expense amount in local expenseCurrency calculated by our system: PAI, etc.
	 * <p>
	 * In some cases (Interest Expense) we may not calculated our amount and will always go with the broker.
	 */
	private BigDecimal ourExpenseAmount;
	/**
	 * Expense amount in local expenseCurrency calculated and sent to us by the broker: Interest Expense, Collateral Usage Fee, PAI, etc.
	 * <p>
	 * Note: In the IMS user interface, this is typically displayed as "Broker Amount".
	 */
	private BigDecimal expectedExpenseAmount;
	/**
	 * Expense amount in localExpenseCurrency that we agreed to. Hopefully both our amounts and expected amounts are the same.
	 * If not, we generally go with expected broker amount if the difference is small. Research otherwise.
	 * <p>
	 * In some cases (Interest Expense) we may not calculated our amount and will always go with the broker.
	 * <p>
	 * Note: In the IMS user interface, this is typically displayed as "Adjustment Amount".
	 */
	private BigDecimal expenseAmount;
	/**
	 * Broker specific exchange rate used to convert our and expected expense amounts to mark currency.
	 * Transfer amount is always in mark currency.
	 */
	private BigDecimal expenseFxRate;

	private String note;


	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the difference between ourExpenseAmount and expectedExpenseAmount.
	 * Ideally this should be 0.
	 * <p>
	 * <p>
	 * Note: In the IMS user interface, this is typically displayed as "Mark Amount".
	 */
	public BigDecimal getDifferenceInExpenseAmount() {
		if (this.expectedExpenseAmount == null) {
			return (this.ourExpenseAmount == null) ? BigDecimal.ZERO : this.ourExpenseAmount;
		}
		if (this.ourExpenseAmount == null) {
			return this.expectedExpenseAmount.negate();
		}
		return this.ourExpenseAmount.subtract(this.expectedExpenseAmount);
	}


	public BigDecimal getExpenseAmountInMarkCurrency() {
		return MathUtils.multiply(getExpenseAmount(), getExpenseFxRate(), 2);
	}


	public AccountingM2MDaily getM2mDaily() {
		return this.m2mDaily;
	}


	public void setM2mDaily(AccountingM2MDaily m2mDaily) {
		this.m2mDaily = m2mDaily;
	}


	public InvestmentSecurity getExpenseCurrency() {
		return this.expenseCurrency;
	}


	public void setExpenseCurrency(InvestmentSecurity expenseCurrency) {
		this.expenseCurrency = expenseCurrency;
	}


	public BigDecimal getOurExpenseAmount() {
		return this.ourExpenseAmount;
	}


	public void setOurExpenseAmount(BigDecimal ourExpenseAmount) {
		this.ourExpenseAmount = ourExpenseAmount;
	}


	public BigDecimal getExpectedExpenseAmount() {
		return this.expectedExpenseAmount;
	}


	public void setExpectedExpenseAmount(BigDecimal expectedExpenseAmount) {
		this.expectedExpenseAmount = expectedExpenseAmount;
	}


	public BigDecimal getExpenseAmount() {
		return this.expenseAmount;
	}


	public void setExpenseAmount(BigDecimal expenseAmount) {
		this.expenseAmount = expenseAmount;
	}


	public BigDecimal getExpenseFxRate() {
		return this.expenseFxRate;
	}


	public void setExpenseFxRate(BigDecimal expenseFxRate) {
		this.expenseFxRate = expenseFxRate;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public AccountingM2MDailyExpenseType getType() {
		return this.type;
	}


	public void setType(AccountingM2MDailyExpenseType type) {
		this.type = type;
	}
}
