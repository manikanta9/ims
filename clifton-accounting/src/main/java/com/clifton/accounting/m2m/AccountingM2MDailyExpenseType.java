package com.clifton.accounting.m2m;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.beans.NamedEntity;
import com.clifton.system.schema.SystemTable;


/**
 * Defines an M2M expense type.
 * <p>
 * NOTE:  accountingAccount can be null but only if expenseSourceFkTable is not null.  Also, if it is populated it must be unique
 * across all types (only 1 type can point at the accountingAccount).
 *
 * @author mwacker
 */
public class AccountingM2MDailyExpenseType extends NamedEntity<Short> {

	public static final String M2M_PRICE_ADJUSTMENT = "M2M Price Adjustment";

	public static final String PAI_EXPENSE_TYPE_EXTERNAL_NAME = "PAI";

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////

	/**
	 * The name that an external system will have for this expense.  Will be unique with the income field.
	 * <p>
	 * For example:  We can have 2 type with "PAI" as the external name but one with income = false and the other with income = true.
	 */
	private String externalName;
	/**
	 * The source table of the fkFieldId on the AccountingM2MDailyExpense.
	 */
	private SystemTable expenseSourceFkTable;

	/**
	 * The accounting account that the expense will be booked to.
	 */
	private AccountingAccount accountingAccount;

	/**
	 * Indicates if this is an income or expense type.
	 */
	private boolean income;

	/**
	 * Indicates that this is a system defined type and cannot be edited from the UI.
	 */
	private boolean systemDefined;

	/**
	 * Indicates that the expenses should be deleted when the M2M entry is rebuilt.
	 */
	private boolean deleteOnM2MRebuild;

	/**
	 * Indicates that the adjustment should adjust the M2M expected transfer amount by the amount of the expectedExpenseAmount value.
	 */
	private boolean adjustExpectedTransferAmount;

	/**
	 * Indicates the number of days to settlement for this type of expense.
	 */
	private int daysToSettle;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public SystemTable getExpenseSourceFkTable() {
		return this.expenseSourceFkTable;
	}


	public void setExpenseSourceFkTable(SystemTable expenseSourceFkTable) {
		this.expenseSourceFkTable = expenseSourceFkTable;
	}


	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}


	public boolean isIncome() {
		return this.income;
	}


	public void setIncome(boolean income) {
		this.income = income;
	}


	public String getExternalName() {
		return this.externalName;
	}


	public void setExternalName(String externalName) {
		this.externalName = externalName;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isDeleteOnM2MRebuild() {
		return this.deleteOnM2MRebuild;
	}


	public void setDeleteOnM2MRebuild(boolean deleteOnM2MRebuild) {
		this.deleteOnM2MRebuild = deleteOnM2MRebuild;
	}


	public boolean isAdjustExpectedTransferAmount() {
		return this.adjustExpectedTransferAmount;
	}


	public void setAdjustExpectedTransferAmount(boolean adjustExpectedTransferAmount) {
		this.adjustExpectedTransferAmount = adjustExpectedTransferAmount;
	}


	public int getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(int daysToSettle) {
		this.daysToSettle = daysToSettle;
	}
}
