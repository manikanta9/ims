package com.clifton.accounting.m2m.instruction;

import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionCancellationHandler;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.transfers.AbstractTransferMessage;
import com.clifton.instruction.messages.transfers.GeneralFinancialInstitutionTransferMessage;
import com.clifton.instruction.messages.transfers.NoticeToReceiveTransferMessage;
import com.clifton.instruction.messages.transfers.TransferMessagePurposes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Will convert a AccountingM2MDaily to either a GeneralFinancialInstitutionTransferMessage or NoticeToReceiveTransferMessage instruction messaging
 * depending on the direction of the instruction.
 *
 * @author mwacker
 */
@Component
public class AccountingM2MCounterpartyInstructionGenerator implements InstructionGenerator<InstructionMessage, AccountingM2MDaily, InstructionDeliveryFieldCommand> {

	private BusinessCompanyUtilHandler businessCompanyUtilHandler;
	private SystemSchemaUtilHandler systemSchemaUtilHandler;
	private InstructionCancellationHandler instructionCancellationHandler;
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;
	private DaoLocator daoLocator;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public Class<AccountingM2MDaily> getSupportedMessageClass() {
		return AccountingM2MDaily.class;
	}


	@Override
	public boolean isCancellationSupported() {
		return true;
	}


	@Override
	public List<InstructionMessage> generateInstructionCancelMessageList(Instruction instruction) {
		return getInstructionCancellationHandler().generateCancellationMessageList(instruction);
	}


	@Override
	public InstructionDeliveryFieldCommand getDeliveryInstructionProvider(Instruction instruction, AccountingM2MDaily accountingM2MDaily) {
		return new InstructionDeliveryFieldCommand() {
			@Override
			public Instruction getInstruction() {
				return instruction;
			}


			@Override
			public BusinessCompany getDeliveryCompany() {
				return accountingM2MDaily.getHoldingInvestmentAccount().getIssuingCompany();
			}


			@Override
			public InvestmentAccount getDeliveryAccount() {
				return accountingM2MDaily.getHoldingInvestmentAccount();
			}


			@Override
			public InvestmentSecurity getDeliveryCurrency() {
				return accountingM2MDaily.getMarkCurrency() == null ? accountingM2MDaily.getHoldingInvestmentAccount().getBaseCurrency() : accountingM2MDaily.getMarkCurrency();
			}


			@Override
			public InvestmentAccount getDeliveryClientAccount() {
				return accountingM2MDaily.getClientInvestmentAccount();
			}
		};
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("ConstantConditions")
	public List<InstructionMessage> generateInstructionMessageList(Instruction instruction) {
		AccountingM2MDaily accountingM2MDaily = getIdentityObject(instruction);
		InstructionDeliveryFieldCommand deliveryFieldProvider = getDeliveryInstructionProvider(instruction, accountingM2MDaily);
		if (MathUtils.isNullOrZero(accountingM2MDaily.getTransferAmount())) {
			return Collections.emptyList();
		}
		AbstractTransferMessage result;
		if (MathUtils.isNegative(accountingM2MDaily.getTransferAmount())) {
			result = new GeneralFinancialInstitutionTransferMessage();
		}
		else {
			result = new NoticeToReceiveTransferMessage();
		}
		List<InstructionMessage> resultList = new ArrayList<>();
		resultList.add(result);

		String custodianBIC = accountingM2MDaily.getCustodianInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode();
		result.setSenderBIC(getBusinessCompanyUtilHandler().getParametricBusinessIdentifierCode());
		result.setReceiverBIC(custodianBIC);

		if (InvestmentAccountType.OTC_CLEARED.equals(accountingM2MDaily.getHoldingInvestmentAccount().getType().getName())) {
			result.setMessagePurpose(TransferMessagePurposes.DAILY_MARGIN_OTC);
		}
		else {
			result.setMessagePurpose(TransferMessagePurposes.DAILY_MARGIN);
		}

		result.setTransactionReferenceNumber(getTransactionReferenceNumber(instruction, accountingM2MDaily));

		result.setAmount(MathUtils.abs(accountingM2MDaily.getTransferAmount()));
		result.setCurrency(deliveryFieldProvider.getDeliveryCurrency().getSymbol());
		result.setValueDate(new Date());

		if (MathUtils.isNegative(accountingM2MDaily.getTransferAmount())) {
			GeneralFinancialInstitutionTransferMessage transferMessage = (GeneralFinancialInstitutionTransferMessage) result;
			transferMessage.setCustodyBIC(custodianBIC);
			transferMessage.setCustodyAccountNumber(accountingM2MDaily.getCustodianInvestmentAccount().getNumber());

			transferMessage.setBeneficiaryBIC(accountingM2MDaily.getHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode());
			transferMessage.setBeneficiaryAccountNumber(accountingM2MDaily.getHoldingInvestmentAccount().getNumber());

			InvestmentInstructionDelivery deliveryInstruction = getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(instruction,
					deliveryFieldProvider.getDeliveryCompany(), deliveryFieldProvider.getDeliveryCurrency());
			if (deliveryInstruction != null) {
				String intermediaryBIC = ObjectUtils.coalesce(deliveryInstruction.getDeliverySwiftCode(), deliveryInstruction.getDeliveryCompany().getBusinessIdentifierCode());
				if (!StringUtils.isEmpty(intermediaryBIC)) {
					transferMessage.setIntermediaryBIC(intermediaryBIC);
				}
				Optional.ofNullable(deliveryInstruction.getDeliveryAccountNumber()).ifPresent(transferMessage::setIntermediaryAccountNumber);
			}
		}
		else {
			NoticeToReceiveTransferMessage transferMessage = (NoticeToReceiveTransferMessage) result;

			transferMessage.setReceiverAccountNumber(accountingM2MDaily.getCustodianInvestmentAccount().getNumber());
			transferMessage.setOrderingBIC(accountingM2MDaily.getHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode());

			InvestmentInstructionDelivery deliveryInstruction = getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(instruction,
					deliveryFieldProvider.getDeliveryCompany(), deliveryFieldProvider.getDeliveryCurrency());
			String intermediaryBIC = ObjectUtils.coalesce(deliveryInstruction.getDeliverySwiftCode(), deliveryInstruction.getDeliveryCompany().getBusinessIdentifierCode());
			if (!StringUtils.isEmpty(intermediaryBIC)) {
				transferMessage.setIntermediaryBIC(intermediaryBIC);
			}
		}

		return resultList;
	}


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private AccountingM2MDaily getIdentityObject(Instruction instruction) {
		ReadOnlyDAO<AccountingM2MDaily> dao = getDaoLocator().locate(instruction.getSystemTable().getName());
		return dao.findByPrimaryKey(instruction.getFkFieldId());
	}


	public String getTransactionReferenceNumber(Instruction instruction, AccountingM2MDaily accountingM2MDaily) {
		SystemSchemaUtilHandler schemaUtilHandler = getSystemSchemaUtilHandler();
		List<String> referenceComponents = Stream.of(instruction, accountingM2MDaily)
				.filter(Objects::nonNull)
				.map(schemaUtilHandler::getEntityUniqueId)
				.collect(Collectors.toList());
		return String.join(InstructionGenerator.REFERENCE_COMPONENT_DELIMITER, referenceComponents);
	}


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public BusinessCompanyUtilHandler getBusinessCompanyUtilHandler() {
		return this.businessCompanyUtilHandler;
	}


	public void setBusinessCompanyUtilHandler(BusinessCompanyUtilHandler businessCompanyUtilHandler) {
		this.businessCompanyUtilHandler = businessCompanyUtilHandler;
	}


	public SystemSchemaUtilHandler getSystemSchemaUtilHandler() {
		return this.systemSchemaUtilHandler;
	}


	public void setSystemSchemaUtilHandler(SystemSchemaUtilHandler systemSchemaUtilHandler) {
		this.systemSchemaUtilHandler = systemSchemaUtilHandler;
	}


	public InstructionCancellationHandler getInstructionCancellationHandler() {
		return this.instructionCancellationHandler;
	}


	public void setInstructionCancellationHandler(InstructionCancellationHandler instructionCancellationHandler) {
		this.instructionCancellationHandler = instructionCancellationHandler;
	}


	public InvestmentInstructionDeliveryUtilHandler getInvestmentInstructionDeliveryUtilHandler() {
		return this.investmentInstructionDeliveryUtilHandler;
	}


	public void setInvestmentInstructionDeliveryUtilHandler(InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler) {
		this.investmentInstructionDeliveryUtilHandler = investmentInstructionDeliveryUtilHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
