package com.clifton.accounting.m2m.instruction;

import com.clifton.accounting.m2m.instruction.handler.AccountingPositionInstructionHandler;
import com.clifton.accounting.m2m.instruction.handler.AccountingPositionInstructionHandlerLocator;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionCancellationHandler;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * <code>AccountingPositionTransferInstructionGenerator</code> Populate InstructionMessage from Accounting Position Transfer Records with the information necessary to
 * generate: 'Instructions to Deliver Free of Payment' or (Receive) messages.  These messages are used to generate SWIFT MT540 or MT542.
 */
@Component
public class AccountingPositionTransferInstructionGenerator implements InstructionGenerator<InstructionMessage, AccountingPositionTransfer, InstructionDeliveryFieldCommand> {

	private InstructionCancellationHandler instructionCancellationHandler;
	private AccountingPositionTransferService accountingPositionTransferService;

	private AccountingPositionInstructionHandlerLocator accountingPositionInstructionHandlerLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<AccountingPositionTransfer> getSupportedMessageClass() {
		return AccountingPositionTransfer.class;
	}


	@Override
	public boolean isCancellationSupported() {
		return false;
	}


	@Override
	public List<InstructionMessage> generateInstructionCancelMessageList(Instruction instruction) {
		return getInstructionCancellationHandler().generateCancellationMessageList(instruction);
	}


	@Override
	public InstructionDeliveryFieldCommand getDeliveryInstructionProvider(Instruction instruction, AccountingPositionTransfer accountingPositionTransfer) {
		return new InstructionDeliveryFieldCommand() {
			@Override
			public Instruction getInstruction() {
				return instruction;
			}


			@Override
			public BusinessCompany getDeliveryCompany() {
				return accountingPositionTransfer.getToHoldingInvestmentAccount().getIssuingCompany();
			}


			@Override
			public InvestmentAccount getDeliveryAccount() {
				return accountingPositionTransfer.getToHoldingInvestmentAccount();
			}


			@Override
			public InvestmentSecurity getDeliveryCurrency() {
				return accountingPositionTransfer.getToClientInvestmentAccount().getBaseCurrency();
			}


			@Override
			public InvestmentAccount getDeliveryClientAccount() {
				return accountingPositionTransfer.getToClientInvestmentAccount();
			}
		};
	}


	@Override
	public List<InstructionMessage> generateInstructionMessageList(Instruction instruction) {
		Objects.requireNonNull(instruction, "Instruction parameter is required.");

		AccountingPositionTransfer accountingPositionTransfer = getAccountingPositionTransferService().getAccountingPositionTransfer(instruction.getFkFieldId());
		AssertUtils.assertNotNull(accountingPositionTransfer, "Cannot load Accounting Position Transfer from Instruction [%s]", instruction.getIdentity());

		AccountingPositionInstructionHandler accountingPositionInstructionHandler = getAccountingPositionInstructionHandlerLocator().locate(accountingPositionTransfer);
		AssertUtils.assertNotNull(accountingPositionInstructionHandler, "Could not determine message handler for Accounting Position Transfer.");

		if (accountingPositionTransfer.getDetailList().isEmpty()) {
			LogUtils.warn(this.getClass(), "The Accounting Position Transfer Detail list cannot be empty.");
		}
		return CollectionUtils.getStream(accountingPositionTransfer.getDetailList())
				.map(td -> accountingPositionInstructionHandler.generateInstructionMessage(instruction, td))
				.collect(Collectors.toList());
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public InstructionCancellationHandler getInstructionCancellationHandler() {
		return this.instructionCancellationHandler;
	}


	public void setInstructionCancellationHandler(InstructionCancellationHandler instructionCancellationHandler) {
		this.instructionCancellationHandler = instructionCancellationHandler;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public AccountingPositionInstructionHandlerLocator getAccountingPositionInstructionHandlerLocator() {
		return this.accountingPositionInstructionHandlerLocator;
	}


	public void setAccountingPositionInstructionHandlerLocator(AccountingPositionInstructionHandlerLocator accountingPositionInstructionHandlerLocator) {
		this.accountingPositionInstructionHandlerLocator = accountingPositionInstructionHandlerLocator;
	}
}
