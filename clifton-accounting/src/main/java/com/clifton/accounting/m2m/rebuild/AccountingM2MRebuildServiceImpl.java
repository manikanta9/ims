package com.clifton.accounting.m2m.rebuild;

import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MDailyExpense;
import com.clifton.accounting.m2m.AccountingM2MDailyExpenseType;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.M2MRebuildCommand;
import com.clifton.accounting.m2m.booking.AccountingM2MDailyBookingProcessor;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.event.EventObject;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingM2MRebuildServiceImpl</code> handles rebuilding AccountingM2MDaily objects.
 *
 * @author mwacker
 */
@Service
public class AccountingM2MRebuildServiceImpl implements AccountingM2MRebuildService {

	private AccountingM2MService accountingM2MService;
	private AccountingBookingService<AccountingM2MDaily> accountingBookingService;
	private AccountingJournalService accountingJournalService;
	private AccountingPostingService accountingPostingService;

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private RunnerHandler runnerHandler;
	private EventHandler eventHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int reconcileAccountingM2MDaily(Date markDate, Integer issuingCompanyId, Integer clientInvestmentAccountGroupId, BigDecimal thresholdAmount, boolean useOurMark, boolean useZeroForHoldAccounts,
	                                       Short holdingAccountTypeId, Integer custodianCompanyId) {
		ValidationUtils.assertNotNull(markDate, "'markDate' required argument is missing", "markDate");
		ValidationUtils.assertNotNull(holdingAccountTypeId, "'Holding Account Type' required argument is missing", "holdingAccountTypeId");
		ValidationUtils.assertNotNull(thresholdAmount, "'thresholdAmount' required argument is missing", "thresholdAmount");
		ValidationUtils.assertFalse(MathUtils.isLessThan(thresholdAmount, BigDecimal.ZERO), "'thresholdAmount' cannot be negative: " + thresholdAmount, "thresholdAmount");

		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setMarkDate(markDate);
		searchForm.setHoldingAccountIssuerId(issuingCompanyId);
		searchForm.setHoldingInvestmentAccountTypeId(holdingAccountTypeId);
		searchForm.setClientInvestmentAccountGroupId(clientInvestmentAccountGroupId);
		searchForm.setCustodianInvestmentAccountIssuerId(custodianCompanyId);
		searchForm.setReconciled(false);
		List<AccountingM2MDaily> unreconciledList = getAccountingM2MService().getAccountingM2MDailyList(searchForm);
		List<AccountingM2MDaily> updateList = new ArrayList<>();
		for (AccountingM2MDaily entry : CollectionUtils.getIterable(unreconciledList)) {
			if (!MathUtils.isGreaterThan(entry.getDifferenceInTransferAmount().abs(), thresholdAmount)) {
				entry.setTransferAmount(useOurMark ? entry.getOurTransferAmount() : entry.getExpectedTransferAmount());
				if (useZeroForHoldAccounts && entry.isHoldAccount()) {
					entry.setTransferAmount(BigDecimal.ZERO);
				}
				entry.setReconciled(true);
				if (StringUtils.isEmpty(entry.getNote()) && !MathUtils.isNullOrZero(entry.getDifferenceInTransferAmount())) {
					entry.setNote("Auto-reconciled because the difference of " + CoreMathUtils.formatNumberMoney(entry.getDifferenceInTransferAmount()) + " is within the threshold of "
							+ CoreMathUtils.formatNumberMoney(thresholdAmount));
				}
				updateList.add(entry);
			}
		}

		getAccountingM2MService().saveAccountingM2MDailyList(updateList);
		return updateList.size();
	}


	@Override
	public int bookAndPostAccountingM2MDaily(Date markDate, Short holdingAccountTypeId, Integer issuingCompanyId, Integer clientInvestmentAccountGroupId, Integer clientInvestmentAccountId,
	                                         boolean includeHoldAccounts, boolean allowHolidayPosting) {
		ValidationUtils.assertNotNull(markDate, "'markDate' required argument is missing", "markDate");
		ValidationUtils.assertNotNull(holdingAccountTypeId, "'Holding Account Type' required argument is missing", "holdingAccountTypeId");
		if (!allowHolidayPosting) {
			Calendar m2mCalendar = getCalendarSetupService().getCalendarByName(getAccountingM2MService().getAccountingMarkToMarketCalendarName());
			ValidationUtils.assertNotNull(m2mCalendar, "Cannot find Calendar with name: " + getAccountingM2MService().getAccountingMarkToMarketCalendarName());
			ValidationUtils.assertTrue(getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(DateUtils.getNextWeekday(markDate), m2mCalendar.getId())), "Mark Date is the day before '"
					+ getAccountingM2MService().getAccountingMarkToMarketCalendarName() + "' Holiday and allow posting on the day before a holiday option was not selected.");
		}

		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setMarkDate(markDate);
		searchForm.setHoldingInvestmentAccountTypeId(holdingAccountTypeId);
		searchForm.setHoldingAccountIssuerId(issuingCompanyId);
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		searchForm.setClientInvestmentAccountGroupId(clientInvestmentAccountGroupId);
		searchForm.setReconciled(true);
		searchForm.setBooked(false);
		List<AccountingM2MDaily> unbookedList = getAccountingM2MService().getAccountingM2MDailyList(searchForm);
		int bookedCount = 0;
		for (AccountingM2MDaily m2m : CollectionUtils.getIterable(unbookedList)) {
			// skip hold accounts if such request was made
			if (!includeHoldAccounts && m2m.isHoldAccount()) {
				continue;
			}
			// skip zero transfers
			if (!MathUtils.isNullOrZero(m2m.getTransferAmount())) {
				m2m.setExpenseList(getAccountingM2MService().getAccountingM2MDailyExpenseListByMark(m2m.getId()));
				AccountingJournal journal = getAccountingBookingService().bookAccountingJournal(AccountingM2MDailyBookingProcessor.JOURNAL_NAME, m2m, true);
				if (journal != null) {
					bookedCount++;
				}
			}
		}

		return bookedCount;
	}


	@Override
	public int unpostAccountingM2MDaily(Date markDate, Short holdingAccountTypeId, Integer issuingCompanyId, Integer clientInvestmentAccountGroupId, Integer clientInvestmentAccountId,
	                                    boolean includeHoldAccounts, boolean unreconcile) {
		ValidationUtils.assertNotNull(markDate, "'markDate' required argument is missing", "markDate");
		ValidationUtils.assertNotNull(holdingAccountTypeId, "'Holding Account Type' required argument is missing", "holdingAccountTypeId");
		ValidationUtils.assertTrue(issuingCompanyId != null || clientInvestmentAccountGroupId != null || clientInvestmentAccountId != null,
				"Please choose an Issuing Company, Client Account Group, or Client Account to process.");

		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setMarkDate(markDate);
		searchForm.setHoldingInvestmentAccountTypeId(holdingAccountTypeId);
		searchForm.setHoldingAccountIssuerId(issuingCompanyId);
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		searchForm.setClientInvestmentAccountGroupId(clientInvestmentAccountGroupId);
		searchForm.setReconciled(true);
		searchForm.setBooked(true);

		List<AccountingM2MDaily> postedEntries = getAccountingM2MService().getAccountingM2MDailyList(searchForm);
		List<AccountingM2MDaily> unreconcileList = new ArrayList<>();

		// used for exception handling in the for-loop below
		StringBuilder errors = null;
		Throwable firstCause = null;

		int unpostedCount = 0;
		for (AccountingM2MDaily entry : CollectionUtils.getIterable(postedEntries)) {
			// skip hold accounts if such request was made
			if (!includeHoldAccounts && entry.isHoldAccount()) {
				continue;
			}

			AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySource("AccountingM2MDaily", entry.getId());
			if (journal != null && journal.getPostingDate() != null) {
				try {

					getAccountingPostingService().unpostAccountingJournal(journal.getId());
					if (unreconcile) {
						entry = getAccountingM2MService().getAccountingM2MDaily(entry.getId());
						entry.setReconciled(false);
						unreconcileList.add(entry);
					}
					unpostedCount++;
				}
				catch (Throwable e) {
					boolean firstError = false;
					if (errors == null) {
						firstCause = e;
						firstError = true;
						errors = new StringBuilder(1024);
					}
					else {
						errors.append("\n\n");
					}
					errors.append("Could not unpost M2M entry [").append(entry.getLabel()).append("]");
					if (!firstError) {
						errors.append("\nCAUSED BY ");
						errors.append(ExceptionUtils.getDetailedMessage(e));
					}
				}
			}
		}
		getAccountingM2MService().saveAccountingM2MDailyList(unreconcileList);

		if (errors != null) {
			throw new RuntimeException(errors.toString(), firstCause);
		}

		return unpostedCount;
	}


	@Override
	public String rebuildAccountingM2MDaily(final M2MRebuildCommand command) {
		if (!command.isAsynchronous()) {
			return doRebuildAccountingM2MDaily(command, command.getStatusHolder());
		}
		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = new Date();
		Runner runner = new AbstractStatusAwareRunner("ACCOUNTING-M2M", runId, now) {

			@Override
			public void run() {
				try {
					getStatus().setMessage(doRebuildAccountingM2MDaily(command, getStatusHolder()));
				}
				catch (Throwable e) {
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding mark to market for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);
		return "Started mark to market rebuild: " + runId + ". Processing will be completed shortly.";
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String doRebuildAccountingM2MDaily(M2MRebuildCommand command, StatusHolderObject<Status> statusHolder) {
		// get holding accounts for mark to market based on command parameters
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setId(command.getHoldingInvestmentAccountId());
		searchForm.setIssuingCompanyId(command.getHoldingAccountIssuingCompanyId());
		searchForm.setAccountTypeId(command.getHoldingAccountTypeId());
		searchForm.setOurAccount(false);
		searchForm.setMainPurpose(getAccountingM2MService().getAccountingMarkToMarketRelationshipPurposeName()); // only mark to market account
		searchForm.setMainPurposeActiveOnDate(command.getMarkDate()); // avoid duplicate data form in-active relationships
		searchForm.setMainPurposeInvestmentAccountGroupId(command.getClientInvestmentAccountGroupId());
		searchForm.setOrderBy("number");

		List<InvestmentAccount> holdingAccountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);

		// don't fail on validation errors for but throw exception in the very end with details
		int totalAccounts = CollectionUtils.getSize(holdingAccountList);
		int errorCount = 0;
		int processedAccounts = 0;
		Throwable firstError = null;
		for (InvestmentAccount holdingAccount : CollectionUtils.getIterable(holdingAccountList)) {
			try {
				doRebuildAccountingM2MDailyForOneAccount(holdingAccount, command.getFromMarkDate(), command.getMarkDate(), command.isDeleteExistingMark());
				processedAccounts++;
			}
			catch (Throwable e) {
				errorCount++;
				if (statusHolder != null) {
					statusHolder.getStatus().addError(holdingAccount + ": " + ExceptionUtils.getDetailedMessage(e));
				}
				LogUtils.error(getClass(), "Failed to rebuild m2m for " + holdingAccount.getLabel(), e);
				if (firstError == null) {
					firstError = e;
				}
			}
			if (statusHolder != null) {
				statusHolder.getStatus().setMessage("Accounts: " + totalAccounts + "; Processed: " + processedAccounts + " snapshots; Failed: " + errorCount + " snapshots; Last: " + holdingAccount.getNumber()
						+ " on " + DateUtils.fromDateShort(command.getMarkDate()));
			}
		}

		return "Processing Complete. Total accounts: " + totalAccounts + "; Failed accounts: " + errorCount + "; Processed accounts: " + processedAccounts
				+ (firstError == null ? "" : ("\n\nFirst Error: " + firstError.getMessage()));
	}


	private void doRebuildAccountingM2MDailyForOneAccount(InvestmentAccount holdingAccount, Date fromMarkDate, Date markDate, boolean deleteExistingMark) {
		if (deleteExistingMark) {
			deleteExistingAccountingM2MDaily(markDate, holdingAccount.getId());
		}

		List<AccountingPositionDaily> positionList = null;
		if (fromMarkDate == null) {
			positionList = getAccountingM2MService().getAccountingPositionDailyLiveForM2MList(AccountingPositionDailyLiveSearchForm.forSnapshotDate(markDate).forHoldingAccount(holdingAccount.getId()));
		}
		else {
			ValidationUtils.assertFalse(DateUtils.compare(markDate, fromMarkDate, false) < 0, "'fromMarkDate' cannot be after markDate", "fromMarkDate");
			// add marks for other day(s) - used to include holiday marks (may not be a holiday in foreign countries)
			Date date = fromMarkDate;
			do {
				List<AccountingPositionDaily> dayList = getAccountingM2MService().getAccountingPositionDailyLiveForM2MList(AccountingPositionDailyLiveSearchForm.forSnapshotDate(date).forHoldingAccount(holdingAccount.getId()));
				if (dayList != null) {
					if (positionList == null) {
						positionList = dayList;
					}
					else {
						positionList.addAll(dayList);
					}
				}
				date = DateUtils.getNextWeekday(date);
			}
			while (DateUtils.compare(date, markDate, false) <= 0);
		}

		Date markStartDate = (fromMarkDate != null) ? fromMarkDate : markDate;
		if (InvestmentAccountType.OTC_CLEARED.equals(holdingAccount.getType().getName())) {
			getAccountingM2MService().populateAccountingPositionDailyPAI(positionList, markStartDate, markDate);
		}

		// get existing records to be updated
		AccountingM2MDailySearchForm markSearchForm = new AccountingM2MDailySearchForm();
		markSearchForm.setMarkDate(markDate);
		markSearchForm.setHoldingInvestmentAccountId(holdingAccount.getId());
		List<AccountingM2MDaily> existingList = getAccountingM2MService().getAccountingM2MDailyList(markSearchForm);

		// create maps of records to be created/updated
		Map<String, AccountingM2MDaily> updateMap = new HashMap<>();
		Map<String, AccountingM2MDaily> existingMap = getM2MMap(existingList);
		MultiValueMap<String, AccountingM2MExpenseHolder> expenseMap = new MultiValueHashMap<>(false);
		Map<Integer, Boolean> markTypeCache = new HashMap<>();
		for (AccountingPositionDaily position : CollectionUtils.getIterable(positionList)) {
			AccountingTransactionInfo tran = position.getAccountingTransaction();
			if (!isValidM2MPosition(tran, markDate)) {
				continue;
			}
			boolean localMark = isM2MinLocalCurrency(tran.getHoldingInvestmentAccount(), markTypeCache);
			String key = getM2mKeyFromTransaction(tran, localMark);
			AccountingM2MDaily m2m = updateMap.get(key);
			if (m2m == null) {
				m2m = existingMap.remove(key);
				if (m2m == null) {
					m2m = new AccountingM2MDaily();
					m2m.setHoldingInvestmentAccount(tran.getHoldingInvestmentAccount());
					m2m.setClientInvestmentAccount(getAccountingM2MService().getAccountingM2MInvestmentAccount(tran.getHoldingInvestmentAccount(), markDate, true));
					m2m.setMarkDate(markDate);

					try {
						m2m.setCustodianInvestmentAccount(getAccountingM2MService().getAccountingM2MInvestmentAccount(tran.getHoldingInvestmentAccount(), markDate, false));
					}
					catch (ValidationException e) {
						// It's possible that there is no custodian account for hold accounts, so suppressing ValidationException here
					}

					if (localMark) {
						m2m.setMarkCurrency(tran.getInvestmentSecurity().getInstrument().getTradingCurrency());
					}
				}
				else if (m2m.isReconciled()) {
					existingMap.put(key, m2m);
					continue; // don't update reconciled records
				}
				else {
					if (m2m.getClientInvestmentAccount() == null) {
						m2m.setClientInvestmentAccount(getAccountingM2MService().getAccountingM2MInvestmentAccount(tran.getHoldingInvestmentAccount(), markDate, true));
					}

					if (m2m.getCustodianInvestmentAccount() == null) {

						try {
							m2m.setCustodianInvestmentAccount(getAccountingM2MService().getAccountingM2MInvestmentAccount(tran.getHoldingInvestmentAccount(), markDate, false));
						}
						catch (ValidationException e) {
							// It's possible that there is no custodian account for hold accounts, so suppressing ValidationException here
						}
					}
				}

				m2m.setOurTransferAmount(BigDecimal.ZERO);
				updateMap.put(key, m2m);
			}
			m2m.setOurTransferAmount(m2m.getOurTransferAmount().add(localMark ? position.getMarkAmountLocal() : position.getMarkAmountBase()));

			InvestmentSecurity security = tran.getInvestmentSecurity();
			if (!MathUtils.isNullOrZero(position.getPaiAmountLocal()) && position.getPositionDate().equals(markStartDate)) {
				// find existing PAI expense with matching currency or create new one
				InvestmentSecurity currency = security.getInstrument().getTradingCurrency();
				AccountingM2MExpenseHolder expense = null;
				if (expenseMap.get(key) == null) {
					expenseMap.putAll(key, new ArrayList<>());
				}
				else {
					for (AccountingM2MExpenseHolder exp : CollectionUtils.getIterable(expenseMap.get(key))) {
						if (AccountingM2MDailyExpenseType.PAI_EXPENSE_TYPE_EXTERNAL_NAME.equals(exp.getAccountingM2MExpenseTypeExternalName()) && currency.equals(exp.getExpenseCurrency())) {
							expense = exp;
							break;
						}
					}
				}
				if (expense == null) {
					expense = new AccountingM2MExpenseHolder(AccountingM2MDailyExpenseType.PAI_EXPENSE_TYPE_EXTERNAL_NAME, m2m, currency, null, position.getMarketFxRate());
					expense.setExpenseAmount(BigDecimal.ZERO);
					expenseMap.put(key, expense);
				}
				expense.setExpenseAmount(MathUtils.add(expense.getExpenseAmount(), position.getPaiAmountLocal()));
			}
		}


		// create or update the expenses, will also populate the m2m with the existing expenses
		createOrUpdateM2MExpenseList(updateMap.values(), expenseMap);


		// if external mark was populated first but our mark doesn't exist, still set the client account for unreconciled marks
		for (AccountingM2MDaily m2m : existingMap.values()) {
			if (!m2m.isReconciled() && m2m.getHoldingInvestmentAccount() != null) {
				if (m2m.getClientInvestmentAccount() == null) {
					m2m.setClientInvestmentAccount(getAccountingM2MService().getAccountingM2MInvestmentAccount(m2m.getHoldingInvestmentAccount(), markDate, true));
					if (m2m.getClientInvestmentAccount() != null) {
						updateMap.put(getM2MKey(m2m), m2m);
					}
				}
				if (m2m.getCustodianInvestmentAccount() == null) {
					try {
						m2m.setCustodianInvestmentAccount(getAccountingM2MService().getAccountingM2MInvestmentAccount(m2m.getHoldingInvestmentAccount(), markDate, false));
						if (m2m.getCustodianInvestmentAccount() != null) {
							updateMap.put(getM2MKey(m2m), m2m);
						}
					}
					catch (ValidationException e) {
						// It's possible that there is no custodian account for hold accounts, so suppressing ValidationException here
					}
				}
			}
		}

		// save changes and trigger events
		for (AccountingM2MDaily m2m : updateMap.values()) {
			DaoUtils.executeWithPostUpdateFlushEnabled(() -> getAccountingM2MService().saveAccountingM2MDailyWithExpenses(m2m, false));

			getEventHandler().raiseEvent(EventObject.ofEventTarget(AccountingM2MService.M2M_ACCOUNT_REBUILD_COMPLETE_AFTER_SAVE_EVENT_NAME, m2m));
		}
	}


	private void createOrUpdateM2MExpenseList(Collection<AccountingM2MDaily> m2mDailyList, MultiValueMap<String, AccountingM2MExpenseHolder> expenseMap) {
		for (AccountingM2MDaily m2m : CollectionUtils.getIterable(m2mDailyList)) {
			String key = getM2MKey(m2m);
			// populate the m2m with the existing expenses
			List<AccountingM2MDailyExpense> expenseList = getAccountingM2MService().getAccountingM2MDailyExpenseListByMark(m2m.getId());
			m2m.setExpenseList(new ArrayList<>());
			// only add the expense that are not marked for deletion during an M2M rebuild.
			for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(expenseList)) {
				if (!expense.getType().isDeleteOnM2MRebuild()) {
					m2m.getExpenseList().add(expense);
				}
			}

			if (!expenseMap.keySet().isEmpty()) {
				// loop through the expense holders that were created
				for (AccountingM2MExpenseHolder expense : CollectionUtils.getIterable(expenseMap.get(key))) {
					// round PAI in the end to avoid per lot rounding issues
					InvestmentNotionalCalculatorTypes calculator = InvestmentCalculatorUtils.getNotionalCalculator(expense.getExpenseCurrency());
					BigDecimal ourExpenseAmount = calculator.round(expense.getExpenseAmount());
					if (MathUtils.compare(ourExpenseAmount, BigDecimal.ZERO) == 0) {
						continue;
					}
					// look up the correct expense type.
					AccountingM2MDailyExpenseType expenseType = getAccountingM2MService().getAccountingM2MDailyExpenseTypeByExternalNameAndAmount(expense.accountingM2MExpenseTypeExternalName, expense.getExpenseAmount());

					// get the existing expense
					AccountingM2MDailyExpense m2mDailyExpense = getAccountingM2MService().getAccountingM2MDailyExpenseForSpecificMark(m2m, expenseType, expense.getExpenseCurrency(), expense.getSourceFkFieldId());
					if (m2mDailyExpense == null) {
						m2mDailyExpense = new AccountingM2MDailyExpense();
						m2mDailyExpense.setM2mDaily(m2m);
						m2mDailyExpense.setType(expenseType);
						m2mDailyExpense.setExpenseCurrency(expense.getExpenseCurrency());
						m2mDailyExpense.setExpenseFxRate(expense.getFxRate());
						m2m.getExpenseList().add(m2mDailyExpense);
					}
					// set our expense amount
					m2mDailyExpense.setOurExpenseAmount(ourExpenseAmount);
					// set the expense amount if it's null and the external amount is null
					if (m2mDailyExpense.getExpenseAmount() == null && m2mDailyExpense.getExpectedExpenseAmount() == null) {
						m2mDailyExpense.setExpenseAmount(ourExpenseAmount);
					}
				}

				// adjust our mark amount to include PAI and all other expenses
				for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(m2m.getExpenseList())) {
					m2m.setOurTransferAmount(MathUtils.add(m2m.getOurTransferAmount(), expense.getExpenseAmountInMarkCurrency()));
				}
			}
		}
	}


	/**
	 * Creates a Map of unique key (see getM2MKey) to corresponding AccountingM2MDaily object mappings.
	 */
	private Map<String, AccountingM2MDaily> getM2MMap(List<AccountingM2MDaily> m2mList) {
		Map<String, AccountingM2MDaily> result = new HashMap<>();
		for (AccountingM2MDaily m2m : CollectionUtils.getIterable(m2mList)) {
			result.put(getM2MKey(m2m), m2m);
		}
		return result;
	}


	/**
	 * Returns unique key for daily mark to market on a given day: holding account + mark's currency
	 */
	private String getM2MKey(AccountingM2MDaily m2m) {
		StringBuilder result = new StringBuilder();
		result.append(m2m.getHoldingInvestmentAccount().getId());
		result.append('-');
		result.append(m2m.getMarkCurrency() == null ? m2m.getHoldingInvestmentAccount().getBaseCurrency().getSymbol() : m2m.getMarkCurrency().getSymbol());
		return result.toString();
	}


	/**
	 * Returns unique key for daily mark to market on a given day: holding account + mark's currency
	 */
	private String getM2mKeyFromTransaction(AccountingTransactionInfo tran, boolean localMark) {
		StringBuilder result = new StringBuilder();
		result.append(tran.getHoldingInvestmentAccount().getId());
		result.append('-');
		if (localMark) {
			result.append(tran.getInvestmentSecurity().getInstrument().getTradingCurrency().getSymbol());
		}
		else {
			result.append(InvestmentUtils.getHoldingAccountBaseCurrency(tran).getSymbol());
		}
		return result.toString();
	}


	/**
	 * Returns true if mark to market for the specified account should be performed in local
	 * currency of each position: separate mark per currency denomination of holdings.
	 */
	private boolean isM2MinLocalCurrency(InvestmentAccount account, Map<Integer, Boolean> markTypeCache) {
		// account that uses CAD and USD marks: 157-85098
		return markTypeCache.computeIfAbsent(account.getId(), k -> getAccountingM2MService().isM2MinLocalCurrency(account));
	}


	/**
	 * Returns if the specified position should be marked to market. If a position belongs to a holding account and there is
	 * an active "Mark to Market" relationship from client investment account to this holding account, then it's valid.
	 * <p>
	 * Excludes currency positions at the bank (relationship from holding account and not from client).
	 */
	private boolean isValidM2MPosition(AccountingTransactionInfo tran, Date markDate) {
		List<InvestmentAccountRelationship> list = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurpose(tran.getHoldingInvestmentAccount().getId(),
				getAccountingM2MService().getAccountingMarkToMarketRelationshipPurposeName(), markDate, false);
		for (InvestmentAccountRelationship relation : CollectionUtils.getIterable(list)) {
			if (relation.getReferenceOne().getType().isOurAccount()) {
				return true;
			}
		}
		return false;
	}


	@Transactional
	protected void deleteExistingAccountingM2MDaily(Date markDate, Integer holdingInvestmentAccountId) {
		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setMarkDate(markDate);
		searchForm.setHoldingInvestmentAccountId(holdingInvestmentAccountId);
		searchForm.setBooked(false);
		searchForm.setReconciled(false);
		List<AccountingM2MDaily> m2mDailyList = getAccountingM2MService().getAccountingM2MDailyList(searchForm);
		for (AccountingM2MDaily m2m : CollectionUtils.getIterable(m2mDailyList)) {
			getAccountingM2MService().deleteAccountingM2MDaily(m2m);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * The <code>AccountingM2MExpenseHolder</code> holds the expense items during the M2M build so the
	 * expenses can be created after.
	 *
	 * @author mwacker
	 */
	public class AccountingM2MExpenseHolder {

		private final String accountingM2MExpenseTypeExternalName;
		private final AccountingM2MDaily m2mDaily;
		private final InvestmentSecurity expenseCurrency;
		private final Integer sourceFkFieldId;
		private final BigDecimal fxRate;

		private BigDecimal expenseAmount;


		public AccountingM2MExpenseHolder(String accountingM2MExpenseTypeExternalName, AccountingM2MDaily m2mDaily, InvestmentSecurity expenseCurrency, Integer sourceFkFieldId, BigDecimal fxRate) {
			this.accountingM2MExpenseTypeExternalName = accountingM2MExpenseTypeExternalName;
			this.m2mDaily = m2mDaily;
			this.expenseCurrency = expenseCurrency;
			this.sourceFkFieldId = sourceFkFieldId;
			this.fxRate = fxRate;
		}


		public AccountingM2MDaily getM2mDaily() {
			return this.m2mDaily;
		}


		public String getAccountingM2MExpenseTypeExternalName() {
			return this.accountingM2MExpenseTypeExternalName;
		}


		public BigDecimal getExpenseAmount() {
			return this.expenseAmount;
		}


		public void setExpenseAmount(BigDecimal expenseAmount) {
			this.expenseAmount = expenseAmount;
		}


		public InvestmentSecurity getExpenseCurrency() {
			return this.expenseCurrency;
		}


		public BigDecimal getFxRate() {
			return this.fxRate;
		}


		public Integer getSourceFkFieldId() {
			return this.sourceFkFieldId;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public AccountingBookingService<AccountingM2MDaily> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<AccountingM2MDaily> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}
}
