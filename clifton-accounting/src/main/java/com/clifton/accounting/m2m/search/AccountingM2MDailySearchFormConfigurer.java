package com.clifton.accounting.m2m.search;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;


public class AccountingM2MDailySearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final AccountingM2MDailySearchForm accountingM2MDailySearchForm;
	private final SearchRestriction differenceRestriction;


	public AccountingM2MDailySearchFormConfigurer(BaseEntitySearchForm searchForm) {
		super(searchForm);

		this.accountingM2MDailySearchForm = (AccountingM2MDailySearchForm) searchForm;
		this.differenceRestriction = searchForm.getSearchRestriction("differenceInTransferAmount");
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		AccountingM2MDailySearchForm searchForm = this.accountingM2MDailySearchForm;
		if (searchForm.getDifferenceInTransferAmount() != null) {
			String comparison = "=";
			if (this.differenceRestriction != null) {
				comparison = this.differenceRestriction.getComparison().getComparisonExpression();
			}
			criteria.add(Restrictions.sqlRestriction("(COALESCE({alias}.ourTransferAmount, 0) - COALESCE({alias}.expectedTransferAmount, 0)) " + comparison + " ?",
					searchForm.getDifferenceInTransferAmount(), new org.hibernate.type.BigDecimalType()));
		}
		if (searchForm.getClientInvestmentAccountGroupId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "cag");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("clientInvestmentAccount", criteria) + ".id"));
			sub.add(Restrictions.eq("referenceOne.id", searchForm.getClientInvestmentAccountGroupId()));
			criteria.add(Subqueries.exists(sub));
		}
		if (searchForm.getHoldingAccountIsHold() != null) {
			// "Hold" accounts do not move M2M daily but are over-funded instead and only have M2M move when the balance goes negative
			// not the best way to identify hold accounts but works for now
			String type = super.getPathAlias("holdingInvestmentAccount.type", criteria);
			if (Boolean.TRUE.equals(searchForm.getHoldingAccountIsHold())) {
				criteria.add(Restrictions.like(type + ".name", AccountingM2MDaily.HOLD_ACCOUNT_PATTERN, MatchMode.ANYWHERE));
			}
			else {
				criteria.add(Restrictions.not(Restrictions.like(type + ".name", AccountingM2MDaily.HOLD_ACCOUNT_PATTERN, MatchMode.ANYWHERE)));
			}
		}
		if (searchForm.getCoalesceMarkClientAccountCurrencyId() != null) {
			String clientAccountAlias = super.getPathAlias("clientInvestmentAccount", criteria);
			criteria.add(Restrictions.or(Restrictions.eq("markCurrency.id", searchForm.getCoalesceMarkClientAccountCurrencyId()),
					Restrictions.and(Restrictions.isNull("markCurrency.id"), Restrictions.eq(clientAccountAlias + ".baseCurrency.id", searchForm.getCoalesceMarkClientAccountCurrencyId()))));
		}

		if (searchForm.getHoldingAccountGroupId() != null || searchForm.getHoldingAccountGroupIds() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "hag");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("holdingInvestmentAccount", criteria) + ".id"));
			if (searchForm.getHoldingAccountGroupId() != null) {
				sub.add(Restrictions.eq("referenceOne.id", searchForm.getHoldingAccountGroupId()));
			}
			else {
				sub.add(Restrictions.in("referenceOne.id", searchForm.getHoldingAccountGroupIds()));
			}
			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getExcludeHoldingAccountGroupId() != null || searchForm.getExcludeHoldingAccountGroupIds() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "eha");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("holdingInvestmentAccount", criteria) + ".id"));
			if (searchForm.getExcludeHoldingAccountGroupId() != null) {
				sub.add(Restrictions.eq("referenceOne.id", searchForm.getExcludeHoldingAccountGroupId()));
			}
			else {
				sub.add(Restrictions.in("referenceOne.id", searchForm.getExcludeHoldingAccountGroupIds()));
			}
			criteria.add(Subqueries.notExists(sub));
		}
	}


	@Override
	protected String getOrderByFieldName(OrderByField field, Criteria criteria) {
		String result = field.getName();
		if ("booked".equals(result)) {
			return "bookingDate";
		}
		if ("holdingAccountIsHold".equals(result)) {
			String type = super.getPathAlias("holdingInvestmentAccount.type", criteria); // make sure the join is present
			return type + ".name";
		}
		if ("differenceInTransferAmount".equals(result)) {
			return "ourTransferAmount";
		}
		return super.getOrderByFieldName(field, criteria);
	}
}
