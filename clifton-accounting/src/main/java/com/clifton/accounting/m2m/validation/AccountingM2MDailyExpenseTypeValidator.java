package com.clifton.accounting.m2m.validation;

import com.clifton.accounting.m2m.AccountingM2MDailyExpenseType;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.search.AccountingM2MDailyExpenseTypeSearchForm;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>AccountingM2MDailyExpenseTypeValidator</code> class validates a <code>AccountingM2MDailyExpenseType</code> item to make
 * sure that the type is not edited incorrectly.
 *
 * @author mwacker
 */
@Component
public class AccountingM2MDailyExpenseTypeValidator extends SelfRegisteringDaoValidator<AccountingM2MDailyExpenseType> {

	private AccountingM2MService accountingM2MService;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(AccountingM2MDailyExpenseType bean, DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertFalse(bean.isSystemDefined(), "System Defined M2M expense types cannot be created via the UI", "systemDefined");

		if (bean.getAccountingAccount() != null) {
			ValidationUtils.assertNull(bean.getExpenseSourceFkTable(), "Expense source table cannot be set when an accounting account is specified.");

			AccountingM2MDailyExpenseType existingType = getAccountingM2MService().getAccountingM2MDailyExpenseTypeByAccountingAccount(bean.getAccountingAccount().getId());
			ValidationUtils.assertTrue(existingType == null || existingType.equals(bean), "Existing expense type [" + (existingType != null ? existingType.getLabel() : "") + "] is already linked to accounting account [" + bean.getAccountingAccount().getLabel() + "].");
		}
		else {
			ValidationUtils.assertNotNull(bean.getExpenseSourceFkTable(), "Expense source table must be populated if no accounting account is specified.");
		}
		if (!StringUtils.isEmpty(bean.getExternalName())) {
			AccountingM2MDailyExpenseTypeSearchForm searchForm = new AccountingM2MDailyExpenseTypeSearchForm();
			searchForm.setExternalName(bean.getExternalName());
			searchForm.setIncome(bean.isIncome());
			List<AccountingM2MDailyExpenseType> expenseTypes = getAccountingM2MService().getAccountingM2MDailyExpenseTypeList(searchForm);
			ValidationUtils.assertTrue((bean.isNewBean() && expenseTypes.isEmpty()) || (!bean.isNewBean() && (expenseTypes.size() <= 1)), "[" + expenseTypes.size() + "] types already exists for external name = [" + bean.getExternalName() + "] and income = [" + bean.isIncome() + "].");
			if (expenseTypes.size() == 1) {
				ValidationUtils.assertTrue(bean.equals(CollectionUtils.getOnlyElement(expenseTypes)), "A type exists for external name = [" + bean.getExternalName() + "] and income = [" + bean.isIncome() + "] already exists.");
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}
}
