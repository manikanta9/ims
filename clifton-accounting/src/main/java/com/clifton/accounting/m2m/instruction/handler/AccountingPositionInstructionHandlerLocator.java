package com.clifton.accounting.m2m.instruction.handler;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.util.beans.Lazy;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author terrys
 */
@Component
public class AccountingPositionInstructionHandlerLocator implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	private Lazy<List<AccountingPositionInstructionHandler>> handlers = new Lazy<>(this::initialize);


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public AccountingPositionInstructionHandler locate(AccountingPositionTransfer accountingPositionTransfer) {
		return this.handlers.get().stream().filter(h -> h.accepts(accountingPositionTransfer)).findFirst().orElse(null);
	}


	protected List<AccountingPositionInstructionHandler> initialize() {
		final Map<String, AccountingPositionInstructionHandler> implementationsMap = this.applicationContext.getBeansOfType(AccountingPositionInstructionHandler.class);
		return new ArrayList<>(implementationsMap.values());
	}
}
