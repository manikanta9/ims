package com.clifton.accounting.m2m;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;

import java.util.Date;


/**
 * The <code>M2MRebuildCommand</code> encapsulates parameters to rebuild daily Mark to Market
 *
 * @author rbrooks
 */
public class M2MRebuildCommand {

	/**
	 * Date for which to recalculate mark to market
	 */
	private Date markDate;

	/**
	 * Optional date that specifies additional dates to include the mark for.  For example, if fromMarkDate = 6/5/2012
	 * and markDate = 6/7/2012, then the calculation will include the mark on 6/5/2012, 6/6/2012 and 6/7/2012.
	 * <p/>
	 * Leave blank or equal to markDate in order to include mark for only 1 day.
	 */
	private Date fromMarkDate;

	/**
	 * Optional filter to calculate for a specific Client Account Group
	 */
	private Integer clientInvestmentAccountGroupId;

	/**
	 * Optional filter to calculate for a specific Holding Account
	 */
	private Integer holdingInvestmentAccountId;

	/**
	 * Optional filter to calculate for a specific Holding Accounts of selected type (i.e. all Futures Broker accounts)
	 */
	private Short holdingAccountTypeId;

	/**
	 * Optional filter to calculate for a specific Holding Company
	 */
	private Integer holdingAccountIssuingCompanyId;

	/**
	 * Option to delete any non-reconciled or non-booked mark records
	 * (used to rebuild when the initial build didn't work correctly)
	 */
	private boolean deleteExistingMark;

	/**
	 * Specifies whether rebuild should be executed asynchronously (runners) or synchronously.
	 */
	private boolean asynchronous;

	// allows to send "live" updates of rebuild status back to the caller (assume the caller sets this field)
	private StatusHolderObject<Status> statusHolder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Default constructor used for Spring Binding
	 */
	public M2MRebuildCommand() {
		// no logic necessary
	}


	/**
	 * Overloaded constructor used for direct implementation in code
	 */
	public M2MRebuildCommand(Date markDate, Integer clientInvestmentAccountGroupId, Integer holdingInvestmentAccountId, Integer holdingAccountIssuingCompanyId, Date fromMarkDate,
	                         boolean deleteExistingMark) {
		this.markDate = markDate;
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
		this.holdingAccountIssuingCompanyId = holdingAccountIssuingCompanyId;
		this.fromMarkDate = fromMarkDate;
		this.deleteExistingMark = deleteExistingMark;
	}


	public String getRunId() {
		StringBuilder runId = new StringBuilder("");
		if (getHoldingInvestmentAccountId() != null) {
			runId.append("HA_").append(getHoldingInvestmentAccountId());
		}
		if (getHoldingAccountTypeId() != null) {
			runId.append("HAT_").append(getHoldingAccountTypeId());
		}
		if (getHoldingAccountIssuingCompanyId() != null) {
			runId.append("HAC_").append(getHoldingAccountIssuingCompanyId());
		}
		if (getClientInvestmentAccountGroupId() != null) {
			runId.append("CAG_").append(getClientInvestmentAccountGroupId());
		}
		if (runId.length() == 0) {
			runId.append("ALL");
		}
		runId.append("-").append(DateUtils.fromDateShort(getMarkDate()));
		return runId.toString();
	}


	public Date getMarkDate() {
		return this.markDate;
	}


	public void setMarkDate(Date markDate) {
		this.markDate = markDate;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer getHoldingAccountIssuingCompanyId() {
		return this.holdingAccountIssuingCompanyId;
	}


	public void setHoldingAccountIssuingCompanyId(Integer holdingAccountIssuingCompanyId) {
		this.holdingAccountIssuingCompanyId = holdingAccountIssuingCompanyId;
	}


	public boolean isDeleteExistingMark() {
		return this.deleteExistingMark;
	}


	public void setDeleteExistingMark(boolean deleteExistingMark) {
		this.deleteExistingMark = deleteExistingMark;
	}


	public Date getFromMarkDate() {
		return this.fromMarkDate;
	}


	public void setFromMarkDate(Date fromMarkDate) {
		this.fromMarkDate = fromMarkDate;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public boolean isAsynchronous() {
		return this.asynchronous;
	}


	public void setAsynchronous(boolean asynchronous) {
		this.asynchronous = asynchronous;
	}


	public StatusHolderObject<Status> getStatusHolder() {
		return this.statusHolder;
	}


	public void setStatusHolder(StatusHolderObject<Status> statusHolder) {
		this.statusHolder = statusHolder;
	}
}
