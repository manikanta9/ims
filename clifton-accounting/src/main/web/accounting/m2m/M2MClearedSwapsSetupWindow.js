TCG.use('Clifton.accounting.m2m.M2MSetupWindow');

Clifton.accounting.m2m.M2MClearedSwapsSetupWindow = Ext.extend(Clifton.accounting.m2m.M2MSetupWindow, {
	id: 'accountingM2MClearedSwapsSetupWindow',
	title: 'Mark To Market - Cleared Swaps',
	clearedOTC: true
});
