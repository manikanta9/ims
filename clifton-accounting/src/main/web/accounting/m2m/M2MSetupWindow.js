Clifton.accounting.m2m.M2MSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingM2MSetupWindow',
	title: 'Mark To Market - Futures',
	iconCls: 'exchange',
	width: 1500,
	height: 700,

	clearedOTC: false,
	getHoldingAccountType: function() {
		return this.clearedOTC ? 'OTC Cleared' : 'Futures Broker';
	},

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Reconciliation',
				items: [{
					xtype: 'accounting-m2m-grid',
					configureToolsMenu: function(menu) {
						const gridPanel = this;
						menu.add('-');
						menu.add({
							text: 'Preview Instruction Message',
							tooltip: 'Preview Instruction SWIFT Message',
							iconCls: 'shopping-cart',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								const collateral = sm.getSelections();
								if (sm.getCount() !== 1) {
									TCG.showError('Please select a single M2M to preview.', 'Incorrect Selection');
								}
								else {
									const id = collateral[0].id;
									Clifton.instruction.openInstructionPreview(id, 'AccountingM2MDaily', gridPanel);
								}
							}
						});
					}
				}]
			},


			{
				title: 'M2M Details',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingPositionDailyLiveForM2MList',
					instructions: 'Daily lot level position details for the specified date and arguments built live from the General Ledger and market data.  Also includes currency balances and mark.',
					pageSize: 1000,
					reloadOnRender: false,
					appendStandardColumns: false,
					forceFit: false,
					viewNames: ['Open Positions', 'Realized and Commission', 'Export Friendly'],
					columns: [
						{header: 'Transaction ID', dataIndex: 'accountingTransaction.id', width: 50, hidden: true},
						{header: 'Issuing Company', width: 130, dataIndex: 'accountingTransaction.clientInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'clientAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true&ourCompany=true'}, hidden: true},
						{header: 'Account #', dataIndex: 'accountingTransaction.clientInvestmentAccount.number', width: 65, filter: {type: 'combo', searchFieldName: 'clientAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Client Account', dataIndex: 'accountingTransaction.clientInvestmentAccount.name', width: 210, filter: {type: 'combo', searchFieldName: 'clientAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', dataIndex: 'accountingTransaction.holdingInvestmentAccount.number', width: 100, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Holding Company', width: 130, dataIndex: 'accountingTransaction.holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'GL Account', dataIndex: 'accountingTransaction.accountingAccount.name', width: 90, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}, hidden: true},
						{header: 'Security', dataIndex: 'accountingTransaction.investmentSecurity.symbol', width: 80, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', dataIndex: 'accountingTransaction.investmentSecurity.name', width: 100, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'CCY Denom', dataIndex: 'accountingTransaction.investmentSecurity.instrument.tradingCurrency.symbol', width: 80, filter: {type: 'combo', searchFieldName: 'investmentInstrumentCurrencyDenominationId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Price Multiplier', dataIndex: 'accountingTransaction.investmentSecurity.priceMultiplier', type: 'float', useNull: true, width: 100, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Initial Margin per Qty', dataIndex: 'accountingTransaction.initialMarginPerUnit', width: 130, type: 'currency', useNull: true, sortable: false, hidden: true, viewNames: ['Export Friendly'], tooltip: 'Actual Initial Margin Requirement for one Unit of Quantity. The calculation takes into account whether the holding account is sued for hedging or speculation as well as account specific Initial Margin Multiplier.'},
						{header: 'Remaining Qty', dataIndex: 'remainingQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 100, viewNames: ['Open Positions', 'Export Friendly']},
						{header: 'Open Qty', dataIndex: 'accountingTransaction.quantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 95, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Market Price', dataIndex: 'marketPrice', type: 'float', useNull: true, negativeInRed: true, width: 90},
						{header: 'Prior Price', dataIndex: 'priorPrice', type: 'float', useNull: true, negativeInRed: true, width: 90, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Open Price', dataIndex: 'accountingTransaction.price', type: 'float', useNull: true, negativeInRed: true, width: 90, hidden: true, viewNames: ['Export Friendly']},
						{header: 'FX Rate', dataIndex: 'marketFxRate', type: 'float', width: 80},
						{header: 'Local Cost Basis', dataIndex: 'remainingCostBasisLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Base Cost Basis', dataIndex: 'remainingCostBasisBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true, summaryType: 'sum'},
						{header: 'Local OTE', dataIndex: 'openTradeEquityLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Base OTE', dataIndex: 'openTradeEquityBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', viewNames: ['Open Positions']},
						{header: 'Prior Local OTE', dataIndex: 'priorOpenTradeEquityLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Prior Base OTE', dataIndex: 'priorOpenTradeEquityBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true},
						{header: 'Local Notional', dataIndex: 'notionalValueLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, viewNames: ['Open Positions']},
						{header: 'Base Notional', dataIndex: 'notionalValueBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', viewNames: ['Open Positions']},
						{header: 'Today Closed Qty', dataIndex: 'todayClosedQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 110, hidden: true, viewNames: ['Realized and Commission', 'Export Friendly']},
						{header: 'Local Realized', dataIndex: 'todayRealizedGainLossLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true, viewNames: ['Realized and Commission']},
						{header: 'Base Realized', dataIndex: 'todayRealizedGainLossBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true, viewNames: ['Realized and Commission', 'Export Friendly']},
						{header: 'Local Commission', dataIndex: 'todayCommissionLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 120, hidden: true, viewNames: ['Realized and Commission']},
						{header: 'Base Commission', dataIndex: 'todayCommissionBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 120, summaryType: 'sum', hidden: true, viewNames: ['Realized and Commission', 'Export Friendly']},
						{header: 'Local M2M', dataIndex: 'markAmountLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Base M2M', dataIndex: 'markAmountBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true, viewNames: ['Export Friendly']},
						{header: 'Open Date', dataIndex: 'accountingTransaction.transactionDate', width: 90, hidden: true, viewNames: ['Export Friendly']},

						{header: 'Prior Local Accrual', dataIndex: 'priorAccrualLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Prior Base Accrual', dataIndex: 'priorAccrualBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
						{header: 'Local Accrual', dataIndex: 'accrualLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base Accrual', dataIndex: 'accrualBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},

						{header: 'PAI', dataIndex: 'paiAmountLocal', type: 'currency', negativeInRed: true, width: 40, hidden: true, tooltip: 'Price Alignment Interest in Local Currency (not included in the Mark)'},

						{header: 'Prior Local Market Value', dataIndex: 'priorMarketValueLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Prior Base Market Value', dataIndex: 'priorMarketValueBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', viewNames: ['Cleared OTC']},
						{header: 'Local Market Value', dataIndex: 'marketValueLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base Market Value', dataIndex: 'marketValueBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', viewNames: ['Cleared OTC']},

						{header: 'Local Daily Gain/Loss', dataIndex: 'dailyGainLossLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Base Daily Gain/Loss', dataIndex: 'dailyGainLossBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true, viewNames: ['Export Friendly']}
					],
					plugins: {ptype: 'gridsummary'},
					listeners: {
						afterRender: function() {
							this.switchToViewColumns('Export Friendly');
							const g = TCG.data.getData('investmentGroupByName.json?name=Marked to Market', this, 'investment.group.Marked to Market');
							if (TCG.isNotNull(g)) {
								TCG.getChildByName(this.getTopToolbar(), 'investmentGroupId').setValue({value: g.id, text: g.name});
							}
						}
					},
					getLoadParams: function(first) {
						const tb = this.getTopToolbar();
						const lp = {readUncommittedRequested: true};
						const md = TCG.getChildByName(tb, 'markDate').getValue();
						if (!md) {
							TCG.showError('Mark Date is required.', 'Validation');
							return false;
						}
						lp.snapshotDate = md.format('m/d/Y');
						let v = TCG.getChildByName(tb, 'investmentGroupId').getValue();
						if (v) {
							lp.investmentGroupId = v;
						}
						v = TCG.getChildByName(tb, 'holdingAccountGroupId').getValue();
						if (v) {
							lp.holdingAccountGroupId = v;
						}
						return lp;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 200, url: 'investmentGroupListFind.json',
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							},
							{
								fieldLabel: 'Holding Acct Group', name: 'holdingAccountGroupId', xtype: 'toolbar-combo', width: 200, url: 'investmentAccountGroupListFind.json',
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							},
							{fieldLabel: 'Mark Date', xtype: 'toolbar-datefield', name: 'markDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')}
						];
					}
				}]
			},


			{
				title: 'Mark Adjustments',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingM2MDailyExpenseListFind',
					importTableName: 'AccountingM2MDailyExpense',
					instructions: 'A list of all mark to market adjustments for the specified date.',
					pageSize: 1000,
					reloadOnRender: false,
					appendStandardColumns: false,
					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true},
						{header: 'M2M ID', dataIndex: 'm2mDaily.id', hidden: true},
						{header: 'Client Account', width: 275, dataIndex: 'm2mDaily.clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, defaultSortColumn: true},
						{header: 'Holding Account', width: 100, dataIndex: 'm2mDaily.holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Holding Company', width: 150, dataIndex: 'm2mDaily.holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuerId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Hold', width: 50, dataIndex: 'm2mDaily.holdAccount', type: 'boolean', filter: {searchFieldName: 'holdingAccountIsHold'}},
						{header: 'GL Account', width: 150, dataIndex: 'accountingAccount.name', filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json?accountGroupName=M2M Expenses'}},
						{header: 'CCY', dataIndex: 'expenseCurrency.symbol', width: 60, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'FX Rate', width: 75, dataIndex: 'expenseFxRate', type: 'float', useNull: true},
						{header: 'Our Amount', width: 110, dataIndex: 'ourExpenseAmount', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, tooltip: 'Adjustment amount in local currency calculated by our system'},
						{header: 'Broker Amount', width: 110, dataIndex: 'expectedExpenseAmount', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, tooltip: 'Expense amount in local currency calculated and sent to use by the broker'},
						{header: 'Adjustment Amount', width: 125, dataIndex: 'expenseAmount', type: 'currency', negativeInRed: true, positiveInGreen: true, useNull: true}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.m2m.M2MDailyWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.m2mDaily.id;
						}
					},
					getLoadParams: function(first) {
						const tb = this.getTopToolbar();
						const lp = {readUncommittedRequested: true};
						const md = TCG.getChildByName(tb, 'markDate').getValue();
						if (!md) {
							TCG.showError('Mark Date is required.', 'Validation');
							return false;
						}
						lp.markDate = md.format('m/d/Y');
						if (this.getWindow().clearedOTC) {
							lp.holdingInvestmentAccountType = 'OTC Cleared';
						}
						else {
							lp.holdingInvestmentAccountTypeNotEqual = 'OTC Cleared';
						}
						return lp;
					},
					getTopToolbarFilters: function(toolbar) {
						return [{fieldLabel: 'Mark Date', xtype: 'toolbar-datefield', name: 'markDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')}];
					}
				}]
			},


			{
				title: 'Adjustment Entry',
				items: [
					{
						xtype: 'formpanel',
						defaults: {anchor: '0'},
						loadValidation: false,
						loadDefaultDataAfterRender: true,
						instructions: 'Use this screen to submit multiple M2M adjustments at once. Select appropriate filters and then click <b>Load Accounts</b>. After entering the adjustment information, click <b>Create Adjustments</b>.',

						getDefaultData: function(win) {
							const type = this.getWindow().getHoldingAccountType();
							const ht = TCG.data.getData('investmentAccountTypeByName.json?name=' + type, this, 'investment.account.type.' + type);

							let dd = win.defaultData || {};

							dd = Ext.apply({
								holdingInvestmentAccountTypeId: {value: ht.id, text: ht.name}
							}, dd);

							return dd;

						},

						listeners: {
							afterrender: function() {
								this.setFormValue('markDate', TCG.getPreviousWeekday().format('m/d/Y'), true);
							}
						},

						items: [
							{
								xtype: 'panel',
								layout: 'column',
								items: [
									{
										columnWidth: .34,
										layout: 'form',
										labelWidth: 150,
										items: [
											{fieldLabel: 'Holding Account Type', name: 'holdingInvestmentAccountTypeId', xtype: 'combo', url: 'investmentAccountTypeListFind.json?ourAccount=false', pageSize: 10, qtip: 'Filter holding company and/or holding account selections by account type'},
											{fieldLabel: 'Holding Company', name: 'holdingCompany.name', hiddenName: 'holdingCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true', qtip: 'Filter by the issuing company of the holding account'}
										]
									},

									{
										columnWidth: .34,
										layout: 'form',
										labelWidth: 150,
										items: [
											{fieldLabel: 'Custodian Company', name: 'custodianInvestmentAccountIssuerId', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', qtip: 'Filter by the Custodian Company.'},
											{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', qtip: 'Filter by a Client Account Group'}

										]
									},

									{
										columnWidth: .26,
										layout: 'form',
										items: [
											{fieldLabel: 'Mark Date', name: 'markDate', xtype: 'datefield', allowBlank: false, qtip: 'The mark date to create adjustments for'},
											{fieldLabel: 'Adjustment Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', loadAll: true, url: 'accountingM2MDailyExpenseTypeListFind.json', qtip: 'The Expense type (which has the GL Account) that the expense(s) should be booked under. This selection will act as a \'default\' for rows that do not have an explicit selection for \'GL Account\'.'}
										]
									}
								]
							},
							{
								xtype: 'formgrid-scroll',
								name: 'expenseEntryGrid',
								loadURL: 'accountingM2MDailyListFind.json',
								storeRoot: 'data',
								height: 500,
								heightResized: true,
								readOnly: true,
								collapsible: false,
								additionalPropertiesToRequest: 'clientInvestmentAccount.baseCurrency.id',
								appendStandardColumns: false,
								border: 1,
								frame: 1,
								flex: 1,


								columnsConfig: [
									{header: 'ID', dataIndex: 'id', hidden: true},
									{header: 'Client Account', width: 250, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
									{header: 'Holding Account', width: 95, dataIndex: 'holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
									{header: 'Holding Company', width: 145, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuerId', url: 'businessCompanyListFind.json?issuer=true'}},
									{header: 'Hold', width: 45, dataIndex: 'holdAccount', type: 'boolean', filter: {searchFieldName: 'holdingAccountIsHold'}},
									{header: 'Expense Type', width: 140, dataIndex: 'type.name', idDataIndex: 'type.id', editor: {xtype: 'combo', loadAll: true, displayField: 'label', allowBlank: true, url: 'accountingM2MDailyExpenseTypeListFind.json'}},
									{header: 'CCY', dataIndex: 'expenseCurrency.symbol', idDataIndex: 'expenseCurrency.id', width: 60, sortable: false, editor: {xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', displayField: 'label', allowBlank: true}},
									{header: 'FX Rate', width: 75, dataIndex: 'expenseFxRate', type: 'float', useNull: true, sortable: false, editor: {xtype: 'floatfield', allowBlank: true}},
									{header: 'Our Amount', width: 100, useNull: true, dataIndex: 'ourExpenseAmount', type: 'currency', sortable: false, tooltip: 'Expense amount in local currency calculated by our system', editor: {xtype: 'currencyfield', allowBlank: true}},
									{header: 'Broker Amount', width: 100, dataIndex: 'expectedExpenseAmount', type: 'currency', useNull: true, editor: {xtype: 'currencyfield', allowBlank: true}, sortable: false, tooltip: 'Adjustment amount in local currency calculated and sent to use by the broker'},
									{header: 'Adjustment Amount', width: 115, dataIndex: 'expenseAmount', type: 'currency', useNull: true, editor: {xtype: 'currencyfield', allowBlank: true}, sortable: false},
									{header: 'Mark Date', width: 50, dataIndex: 'markDate', type: 'datefield', hidden: true}
								],
								addToolbarButtons: function(toolBar) {
									const gp = this;
									toolBar.add({
										text: 'Load Accounts',
										tooltip: 'Loads the unreconciled M2M accounts for the filters above.',
										iconCls: 'table-refresh',
										scope: this,
										handler: function() {
											gp.loadAccounts();
										}
									});

									toolBar.add('-');

									toolBar.add({
										text: 'Create Adjustments',
										tooltip: 'Creates adjustments for the selected rows.',
										iconCls: 'shopping-cart',
										scope: this,
										handler: function() {
											gp.createExpenses();
										}
									});
									toolBar.add('-');
									toolBar.add({
										text: 'Use Our Amount',
										tooltip: 'Copy "Our Amount" values into "Adjustment Amount" for the selected rows.',
										iconCls: 'row_reconciled',
										scope: this,
										handler: function() {
											gp.copyValues('ourExpenseAmount', 'expenseAmount');
										}
									});
									toolBar.add('-');
									toolBar.add({
										text: 'Use Broker Amount',
										tooltip: 'Copy "Broker Amount" values into "Adjustment Amount" for the selected rows.',
										iconCls: 'row_reconciled',
										scope: this,
										handler: function() {
											gp.copyValues('expectedExpenseAmount', 'expenseAmount');
										}
									});
									toolBar.add('-');
								},


								loadAccounts: function() {
									const grid = this;
									const loader = new TCG.data.JsonLoader({
										waitTarget: TCG.getParentFormPanel(grid).getEl(),
										params: grid.getLoadParams(),
										scope: grid,
										onLoad: function(recordList, conf) {
											const wrapper = {};
											wrapper.data = recordList;
											grid.store.loadData(wrapper);
										}
									});
									loader.load('accountingM2MDailyListFind.json');
								},
								getLoadParams: function() {
									const fp = TCG.getParentFormPanel(this);

									let dateValue;
									const markDate = TCG.getChildByName(fp, 'markDate');
									if (TCG.isNotBlank(markDate.getValue())) {
										dateValue = (markDate.getValue()).format('m/d/Y');
									}
									else {
										const prevBD = TCG.getPreviousWeekday();
										dateValue = prevBD.format('m/d/Y');
										markDate.setValue(dateValue);
									}

									const lp = {};
									lp.reconciled = false;
									lp.markDate = dateValue;

									let v = TCG.getChildByName(fp, 'holdingInvestmentAccountTypeId').getValue();
									if (v) {
										lp.holdingInvestmentAccountTypeId = v;
									}

									v = TCG.getChildByName(fp, 'holdingCompany.name').getValue();
									if (v) {
										lp.holdingAccountIssuerId = v;
									}

									v = TCG.getChildByName(fp, 'custodianInvestmentAccountIssuerId').getValue();
									if (v) {
										lp.custodianInvestmentAccountIssuerId = v;
									}

									v = TCG.getChildByName(fp, 'groupName').getValue();
									if (v) {
										lp.clientInvestmentAccountGroupId = v;
									}

									if (fp.getWindow().clearedOTC) {
										lp.holdingInvestmentAccountType = 'OTC Cleared';
									}
									else {
										lp.holdingInvestmentAccountTypeNotEqual = 'OTC Cleared';
									}

									return lp;
								},

								isPagingEnabled: function() {
									return false;
								},

								copyValues: function(from, to) {
									const grid = this;
									const store = grid.store;
									for (let i = 0; i < store.getCount(); i++) {
										const rec = store.getAt(i);
										rec.set(to, rec.get(from));
									}
								},
								createExpenses: function() {
									const grid = this;
									const store = grid.store;
									const fp = TCG.getParentFormPanel(this);
									const expenseTypeOverride = TCG.getChildByName(fp, 'type.name').getValue();

									const expenses = [];

									for (let i = 0; i < store.getCount(); i++) {
										const rec = store.getAt(i);
										const expense = rec.json;
										const rowInfo = rec.data;

										//Only process rows which have an expense amount entered
										if (Ext.isNumber(rowInfo.expenseAmount) || Ext.isNumber(rowInfo.expectedExpenseAmount) || Ext.isNumber(rowInfo.ourExpenseAmount)) {

											const baseCurrencyId = expense.clientInvestmentAccount.baseCurrency.id;
											const expenseCurrencyId = Ext.isNumber(rowInfo['expenseCurrency.id']) && rowInfo['expenseCurrency.id'] !== 0 ? rowInfo['expenseCurrency.id'] : baseCurrencyId;

											//Determine Expense GL Account
											let expenseTypeId = null;
											if (Ext.isNumber(rowInfo['type.id']) && rowInfo['type.id'] !== 0) {
												expenseTypeId = rowInfo['type.id'];
											}
											else if (Ext.isNumber(expenseTypeOverride) && expenseTypeOverride !== 0) {
												expenseTypeId = expenseTypeOverride;
											}

											//Determine the FX Rate
											let fx = rowInfo.expenseFxRate;
											if (!fx) {
												if (expenseCurrencyId === baseCurrencyId) {
													fx = 1;
												}
												else {
													const fxRate = TCG.data.getData('marketDataExchangeRateForDateFlexible.json?companyId=' + expense.holdingInvestmentAccount.issuingCompany.id + '&fromCurrencyId=' + baseCurrencyId + '&toCurrencyId=' + expenseCurrencyId + '&rateDate=' + new Date(expense.markDate).format('m/d/Y'), grid);
													//If an exchange rate was returned by the server, use that, otherwise set the fx rate to null.
													fx = fxRate ? fxRate.exchangeRate : null;
												}
											}

											//Determine the Adjustment Amount
											let expenseAmount = rowInfo.expenseAmount;
											if (!expenseAmount) {
												if (rowInfo.ourExpenseAmount && !rowInfo.expectedExpenseAmount) {
													expenseAmount = rowInfo.ourExpenseAmount;
												}
												else if (!rowInfo.ourExpenseAmount && rowInfo.expectedExpenseAmount) {
													expenseAmount = rowInfo.expectedExpenseAmount;
												}

											}

											const expenseInfo = {
												'class': 'com.clifton.accounting.m2m.AccountingM2MDailyExpense',
												'm2mDaily.id': expense.id,
												'expenseCurrency.id': expenseCurrencyId,
												'expenseAmount': expenseAmount,
												'expenseFxRate': fx
											};

											if (TCG.isNotNull(expenseTypeId)) {
												expenseInfo['type.id'] = expenseTypeId;
											}

											if (TCG.isNotNull(rowInfo.ourExpenseAmount)) {
												expenseInfo.ourExpenseAmount = rowInfo.ourExpenseAmount;
											}

											if (TCG.isNotNull(rowInfo.expectedExpenseAmount)) {
												expenseInfo.expectedExpenseAmount = rowInfo.expectedExpenseAmount;
											}


											expenses.push(expenseInfo);
										}
									}

									if (expenses.length === 0) {
										TCG.showError('You must enter at least one adjustment before submitting.', 'No Adjustments Entered');
									}
									else {
										Ext.Msg.confirm('Submit M2M Adjustments', 'Would you like to submit these ' + expenses.length + ' adjustments?', function(a) {
											if (a === 'yes') {
												const expenseListStr = Ext.util.JSON.encode(expenses);

												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Processing...',

													params: {expenseList: expenseListStr, errorIfExists: true},
													onLoad: function(record, conf) {
														grid.loadAccounts();

													}
												});
												loader.load('accountingM2MExpenseGroupSave.json');
											}
										});
									}
								}
							}
						]
					}
				]
			},


			{
				title: 'Adjustment Types',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingM2MDailyExpenseTypeListFind',
					instructions: 'A list of all mark to market adjustment types.',
					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true},
						{header: 'Adjustment Name', dataIndex: 'name'},
						{header: 'Description', dataIndex: 'description', hidden: true},
						{header: 'External Name', dataIndex: 'externalName', qtip: 'The name that an external system will have for this expense.  Must be unique with the income field.<br/>For example:  We can have 2 type with "PAI" as the external name but one with income = false and the other with income = true.'},
						{header: 'GL Account', width: 120, dataIndex: 'accountingAccount.name', filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json?accountGroupName=M2M Expenses'}},
						{header: 'Source Table', width: 120, dataIndex: 'expenseSourceFkTable.name', filter: {type: 'combo', searchFieldName: 'expenseSourceFkTableId', displayField: 'label', url: 'systemTableListFind.json?defaultDataSource=true'}},
						{header: 'Days To Settle', width: 50, dataIndex: 'daysToSettle', type: 'int'},
						{header: 'Income', width: 50, dataIndex: 'income', type: 'boolean'},
						{header: 'Delete on Rebuild', width: 50, dataIndex: 'deleteOnM2MRebuild', type: 'boolean'},
						{header: 'Adjust Expected Amount', width: 55, dataIndex: 'adjustExpectedTransferAmount', type: 'boolean'},
						{header: 'System Defined', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.m2m.M2MExpenseTypeWindow'
					}
				}]
			},


			{
				title: 'Administration',
				items: [{
					xtype: 'formpanel',
					loadValidation: false,
					validatedOnLoad: false,

					listeners: {
						afterrender: function() {
							this.setFormValue('markDate', TCG.getPreviousWeekday().format('m/d/Y'), true);
						}
					},

					getDefaultData: function(form) {
						const type = this.getWindow().getHoldingAccountType();
						const ht = TCG.data.getData('investmentAccountTypeByName.json?name=' + type, this, 'investment.account.type.' + type);
						return {holdingAccountTypeId: {value: ht.id, text: ht.name}};
					},

					items: [
						{
							xtype: 'fieldset',
							title: 'Rebuild Mark to Market',
							buttonAlign: 'right',
							labelWidth: 130,
							instructions: 'Recalculates "Our Mark" amount on the specified date (includes additional adjustments). "Our Mark" is built for securities that have "M2M Calculator" defined for holding accounts with the main purpose of "Mark to Market". Skips marks that have been Reconciled.',

							items: [
								{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json'},
								{fieldLabel: 'Holding Account Type', xtype: 'combo', hiddenName: 'holdingAccountTypeId', url: 'investmentAccountTypeListFind.json?ourAccount=false', pageSize: 10, qtip: 'Filter holding company and/or holding account selections by account type'},
								{
									fieldLabel: 'Holding Company', xtype: 'combo', hiddenName: 'holdingAccountIssuingCompanyId',
									url: 'businessCompanyListFind.json?issuer=true', pageSize: 10, qtip: 'Filter the rebuild by selected holding company',
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										const atv = f.findField('holdingAccountTypeId').getValue();
										combo.store.baseParams = atv ? {activeIssuerInvestmentAccountTypeId: atv} : {};
									}

								},
								{
									fieldLabel: 'Holding Account', xtype: 'combo', hiddenName: 'holdingInvestmentAccountId', url: 'investmentAccountListFind.json?ourAccount=false', displayField: 'label', pageSize: 10, qtip: 'Filter the rebuild by selected holding account',
									listeners: {
										beforequery: function(queryEvent) {
											const bp = {};

											const combo = queryEvent.combo;
											const f = combo.getParentForm().getForm();
											const v = f.findField('holdingAccountIssuingCompanyId').getValue();
											if (v) {
												bp.issuingCompanyId = v;
											}
											const atv = f.findField('holdingAccountTypeId').getValue();
											if (atv) {
												bp.accountTypeId = atv;
											}
											combo.store.baseParams = bp;
										}
									}
								},
								{
									fieldLabel: 'Mark Date', xtype: 'panel', layout: 'hbox',
									items: [
										{name: 'markDate', xtype: 'datefield', width: 95, allowBlank: false},
										{xtype: 'displayfield', flex: 1}
									]
								},
								{
									fieldLabel: 'From Date', xtype: 'panel', layout: 'hbox',
									items: [
										{name: 'fromMarkDate', xtype: 'datefield', width: 95},
										{xtype: 'displayfield', flex: 1, value: '&nbsp;Optionally add M2M for each day from this date to Mark Date (use this to include Holiday marks)'}
									]
								},
								{boxLabel: 'Delete any unreconciled mark records before rebuilding (also removes broker mark: imports will need to be reprocessed)', name: 'deleteExistingMark', xtype: 'checkbox'},
								{boxLabel: 'Run rebuild asynchronously (can see current status below)', name: 'asynchronous', xtype: 'checkbox', checked: true}
							],
							buttons: [{
								text: 'Rebuild M2M',
								iconCls: 'run',
								width: 150,
								handler: function() {
									const owner = this.findParentByType('formpanel');
									const form = owner.getForm();
									form.submit(Ext.applyIf({
										url: 'accountingM2MDailyRebuild.json?enableOpenSessionInView=true',
										waitMsg: 'Rebuilding...',
										success: function(form, action) {
											Ext.Msg.alert('Mark to Market', action.result.result, function() {
												const grid = owner.items.get(1);
												grid.reload.defer(300, grid);
											});
										}
									}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
								}
							}]
						},
						{
							xtype: 'core-scheduled-runner-grid',
							typeName: 'ACCOUNTING-M2M',
							instantRunner: true,
							title: 'Current Processing',
							frame: true,
							height: 250
						}
					]
				}]
			},


			{
				title: 'External Loads',
				items: [{
					xtype: 'integration-importRunEventGrid',
					targetApplicationName: 'IMS',
					importEventName: 'Broker Daily M2M'
				}]
			},


			{
				title: 'Cash Instructions',
				items: [{
					xtype: 'investment-instruction-grid',
					tableName: 'AccountingM2MDaily',
					defaultCategoryName: 'M2M Counterparty',
					getDefaultTagName: function() {
						if (TCG.isTrue(this.getWindow().clearedOTC)) {
							return 'Cleared OTC';
						}
						return 'Futures';
					},
					defaultViewName: 'Group by Recipient Company'
				}]
			}]
	}]
});


Clifton.accounting.m2m.AccountListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'accountingM2MDailyListFind',
	importTableName: [
		{table: 'AccountingM2MDaily', label: 'Marks'},
		{table: 'AccountingM2MDailyExpense', label: 'Adjustments'}
	],
	queryExportTagName: 'Mark to Market',
	xtype: 'gridpanel',
	instructions: 'Account level summary for mark to market reconciled on the specified date. "Our Mark" is built from daily position snapshots for holding accounts with the main purpose of "Mark to Market"',
	pageSize: 500,
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json'},
			{fieldLabel: 'Mark Date', xtype: 'toolbar-datefield', name: 'markDate'}
		];
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Client Account', width: 140, dataIndex: 'clientInvestmentAccount.label', filter: {searchFieldName: 'clientInvestmentAccount'}, defaultSortColumn: true},
		{header: 'Holding Account', width: 50, dataIndex: 'holdingInvestmentAccount.number', filter: {searchFieldName: 'holdingInvestmentAccountNumber'}},
		{header: 'Holding Type', width: 45, dataIndex: 'holdingInvestmentAccount.type.name', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountTypeId', url: 'investmentAccountTypeListFind.json'}, hidden: true},
		{header: 'Holding Company', width: 70, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'holdingAccountIssuerName'}},
		{header: 'Hold', width: 25, dataIndex: 'holdAccount', type: 'boolean', filter: {searchFieldName: 'holdingAccountIsHold'}},
		{header: 'Mark CCY', width: 30, dataIndex: 'markCurrency.symbol', filter: {searchFieldName: 'markCurrency'}, hidden: true},
		{header: 'Custodian Account', width: 55, dataIndex: 'custodianInvestmentAccount.number', filter: {searchFieldName: 'custodianInvestmentAccountNumber'}},
		{header: 'Custodian Company', width: 70, dataIndex: 'custodianInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'custodianInvestmentAccountIssuerName'}},
		{header: 'Our Amount', width: 50, dataIndex: 'ourTransferAmount', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, tooltip: 'Mark Mark Amount calculated by our system which also includes all adjustments.'},
		{header: 'Broker Amount', width: 50, dataIndex: 'expectedTransferAmount', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, tooltip: 'Broker Mark Amount calculated and sent to us by the broker which also includes all adjustments.'},
		{
			header: 'Difference', width: 40, dataIndex: 'differenceInTransferAmount', type: 'currency',
			renderer: function(value, metaData, r) {
				if (r.data.note) {
					metaData.attr = 'qtip="' + Ext.util.Format.htmlEncode(r.data.note) + '"';
					metaData.css = 'amountAdjusted';
				}
				if (value !== 0) {
					return TCG.numberFormat(value, '0,000.00');
				}
				return '';
			}
		},
		{
			header: 'Transfer Amount', width: 55, dataIndex: 'transferAmount', type: 'currency', css: 'BORDER-LEFT: #c0c0c0 1px solid; BORDER-RIGHT: #c0c0c0 1px solid;',
			renderer: function(value, metaData, r) {
				metaData.attr = 'style="FONT-WEIGHT: bold"';
				if (value !== 0) {
					return TCG.renderAmount(value, true, '0,000.00');
				}
				return '';
			}
		},
		{header: 'Reconciled', width: 35, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'},
		{header: 'Booked', width: 28, dataIndex: 'booked', type: 'boolean'},
		{header: 'Note', width: 200, dataIndex: 'note', hidden: true}
	],
	listeners: {
		afterRender: function() {
			if (this.getWindow().clearedOTC) {
				const sm = this.getColumnModel();
				sm.setHidden(sm.findColumnIndex('holdAccount'), true);
			}
		}
	},
	getLoadParams: function(firstLoad) {
		const t = this.getTopToolbar();
		let dateValue;
		const md = TCG.getChildByName(t, 'markDate');
		if (TCG.isNotBlank(md.getValue())) {
			dateValue = (md.getValue()).format('m/d/Y');
		}
		else {
			const dd = this.getWindow().defaultData;
			if (firstLoad && dd && dd.markDate) { // markDate can be defaulted by the caller of window open
				dateValue = dd.markDate.format('m/d/Y');
			}
			else {
				dateValue = TCG.getPreviousWeekday().format('m/d/Y');
			}
			md.setValue(dateValue);
		}
		const result = {
			markDate: dateValue,
			enableOpenSessionInView: true,
			requestedMaxDepth: 4
		};
		const grp = TCG.getChildByName(t, 'groupName');
		if (TCG.isNotBlank(grp.getValue())) {
			result.clientInvestmentAccountGroupId = grp.getValue();
		}
		if (this.getWindow().clearedOTC) {
			result.holdingInvestmentAccountType = 'OTC Cleared';
		}
		else {
			result.holdingInvestmentAccountTypeNotEqual = 'OTC Cleared';
		}
		return result;
	},
	editor: {
		detailPageClass: 'Clifton.accounting.m2m.M2MDailyWindow',
		drillDownOnly: true,
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Auto-Reconcile...',
				tooltip: 'Auto-reconcile all accounts that have the difference smaller than the specified threshold.',
				iconCls: 'row_reconciled',
				scope: this,
				handler: function() {
					gridPanel.openActionWindow('Clifton.accounting.m2m.AutoReconcileWindow', t, gridPanel);
				}
			});
			t.add('-');
			t.add({
				text: 'Book and Post...',
				tooltip: 'Book this reconciled but unbooked entries and immediately post them to the General Ledger',
				iconCls: 'book-red',
				scope: this,
				handler: function() {
					gridPanel.openActionWindow('Clifton.accounting.m2m.BookAndPostWindow', t, gridPanel);
				}
			});
			t.add('-');
			t.add({
				text: 'Unpost...',
				tooltip: 'Unpost and unbook one or more mark to market entries',
				iconCls: 'undo',
				scope: this,
				handler: function() {
					gridPanel.openActionWindow('Clifton.accounting.m2m.UnpostWindow', t, gridPanel);
				}
			});
			t.add('-');
		}
	},
	openActionWindow: function(windowClass, toolBar, gridPanel) {
		const type = this.getWindow().getHoldingAccountType();
		const ht = TCG.data.getData('investmentAccountTypeByName.json?name=' + type, this, 'investment.account.type.' + type);
		TCG.createComponent(windowClass, {
			defaultData: {
				markDate: TCG.getChildByName(toolBar, 'markDate').getValue().format('m/d/Y'),
				holdingAccountTypeId: ht.id,
				holdingAccountTypeName: ht.name
			},
			openerCt: gridPanel
		});
	}
});
Ext.reg('accounting-m2m-grid', Clifton.accounting.m2m.AccountListGrid);


Clifton.accounting.m2m.AutoReconcileWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Auto-Reconcile',
	iconCls: 'row_reconciled',
	height: 385,
	width: 575,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Enter maximum threshold amount that should be used for auto-reconciliation. If the difference between <b>Our Amount</b> and <b>Broker Amount</b> is smaller than the threshold, then the account will be marked as reconciled.',
		labelWidth: 150,
		items: [
			{name: 'markDate', xtype: 'hidden'},
			{fieldLabel: 'Max Threshold Amount', name: 'thresholdAmount', xtype: 'currencyfield', value: '0.50', minValue: 0, allowBlank: false},
			{fieldLabel: 'Account Type', xtype: 'combo', name: 'holdingAccountTypeName', hiddenName: 'holdingAccountTypeId', url: 'investmentAccountTypeListFind.json?ourAccount=false', pageSize: 10, qtip: 'Filter issuing company selections by account type'},
			{
				fieldLabel: 'Issuing Company', xtype: 'combo', hiddenName: 'issuingCompanyId', url: 'businessCompanyListFind.json?issuer=true', pageSize: 10,
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const f = combo.getParentForm().getForm();
					const atv = f.findField('holdingAccountTypeId').getValue();
					combo.store.baseParams = atv ? {activeIssuerInvestmentAccountTypeId: atv} : {};
				}
			},
			{fieldLabel: 'Custodian Company', xtype: 'combo', hiddenName: 'custodianCompanyId', url: 'businessCompanyListFind.json?issuer=true'},
			{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json'},
			{
				xtype: 'fieldset',
				title: 'Transfer Amount',
				labelWidth: 5,
				items: [
					{
						xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'useOurMark',
						items: [
							{boxLabel: 'Use <b>Broker Amount</b> for transfer', xtype: 'radio', name: 'useOurMark', inputValue: 'false', checked: true},
							{boxLabel: 'Use <b>Our Amount</b> for transfer', xtype: 'radio', name: 'useOurMark', inputValue: 'true'}
						]
					},
					{boxLabel: 'Set Transfer Amount to 0 for reconciled hold accounts (no transfer)', name: 'useZeroForHoldAccounts', xtype: 'checkbox', checked: true}
				]
			}
		],
		getSaveURL: function() {
			return 'accountingM2MDailyReconcile.json';
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Auto-reconciled ' + panel.getForm().formValues + ' accounts.', 'Auto-Reconcile Status');
			}
		}
	}]
});


Clifton.accounting.m2m.BookAndPostWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Book and Post',
	iconCls: 'book-red',
	height: 300,
	width: 510,
	modal: true,
	saveTimeout: 240,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Book reconciled but unbooked mark to market entries for the following selection and immediately post them to the General Ledger.',
		labelWidth: 130,
		items: [
			{name: 'markDate', xtype: 'hidden'},
			{fieldLabel: 'Account Type', xtype: 'combo', name: 'holdingAccountTypeName', hiddenName: 'holdingAccountTypeId', url: 'investmentAccountTypeListFind.json?ourAccount=false', pageSize: 10, qtip: 'Filter issuing company selections by account type'},
			{
				fieldLabel: 'Issuing Company', xtype: 'combo', hiddenName: 'issuingCompanyId', url: 'businessCompanyListFind.json?issuer=true', pageSize: 10, mutuallyExclusiveFields: ['clientInvestmentAccountId'],
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const f = combo.getParentForm().getForm();
					const atv = f.findField('holdingAccountTypeId').getValue();
					combo.store.baseParams = atv ? {activeIssuerInvestmentAccountTypeId: atv} : {};
				}
			},
			{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json'},
			{fieldLabel: 'Client Account', xtype: 'combo', hiddenName: 'clientInvestmentAccountId', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', pageSize: 10, mutuallyExclusiveFields: ['issuingCompanyId']},
			{boxLabel: 'Also book and post Transfer Amount for Hold accounts', xtype: 'checkbox', name: 'includeHoldAccounts'},
			{boxLabel: 'Allow posting on the day before a Holiday', xtype: 'checkbox', name: 'allowHolidayPosting'}
		],
		getSaveURL: function() {
			return 'accountingM2MDailyBookAndPost.json';
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Booked and posted ' + panel.getForm().formValues + ' accounts.', 'Posting Status');
			}
		}
	}]
});


Clifton.accounting.m2m.UnpostWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Unpost',
	iconCls: 'undo',
	height: 300,
	width: 510,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Unpost and unbook mark to market entries for the following selection, with the option to unreconcile',
		labelWidth: 130,
		items: [
			{name: 'markDate', xtype: 'hidden'},
			{fieldLabel: 'Account Type', xtype: 'combo', name: 'holdingAccountTypeName', hiddenName: 'holdingAccountTypeId', url: 'investmentAccountTypeListFind.json?ourAccount=false', pageSize: 10, qtip: 'Filter issuing company selections by account type'},
			{
				fieldLabel: 'Issuing Company', xtype: 'combo', hiddenName: 'issuingCompanyId', url: 'businessCompanyListFind.json?issuer=true', pageSize: 10, mutuallyExclusiveFields: ['clientInvestmentAccountId'],
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const f = combo.getParentForm().getForm();
					const atv = f.findField('holdingAccountTypeId').getValue();
					combo.store.baseParams = atv ? {activeIssuerInvestmentAccountTypeId: atv} : {};
				}
			},
			{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json'},
			{fieldLabel: 'Client Account', xtype: 'combo', hiddenName: 'clientInvestmentAccountId', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', pageSize: 10, mutuallyExclusiveFields: ['issuingCompanyId']},
			{boxLabel: 'Also unpost entries for Hold accounts', xtype: 'checkbox', name: 'includeHoldAccounts'},
			{boxLabel: 'Also unreconcile unposted mark to market entries', xtype: 'checkbox', name: 'unreconcile'}
		],
		getSaveURL: function() {
			return 'accountingM2MDailyUnpost.json';
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Unposted ' + panel.getForm().formValues + ' accounts.', 'Posting Status');
			}
		}
	}]
});
