Clifton.accounting.m2m.M2MDailyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Mark to Market',
	width: 1400,
	height: 700,
	iconCls: 'exchange',

	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Details',

				items: [{
					xtype: 'formpanel',
					url: 'accountingM2MDailyPopulated.json?requestedMaxDepth=4',
					table: 'AccountingM2MDaily',
					childTables: 'AccountingM2MDailyExpense',
					getValidationBeanName: function(url) {
						return 'accountingM2MDaily';
					},
					labelWidth: 120,

					getSaveURL: function() {
						return 'accountingM2MDailyWithExpensesSave.json?recalculate=true';
					},
					getHoldingAccountType: function() {
						return this.getFormValue('holdingInvestmentAccount.type.name');
					},
					listeners: {
						afterload: function(panel) {
							if (TCG.isTrue(this.getFormValue('booked'))) {
								this.setReadOnly(true);
							}
							if (this.getHoldingAccountType() === 'OTC Cleared') {
								this.getForm().findField('expectedTransferAmount').setReadOnly(false);
								this.setFieldSubmitValue('expectedTransferAmount', true);
							}
						}
					},

					getWarningMessage: function(form) {
						let msg = undefined;
						const b1 = TCG.getChildByName(form.ownerCt, 'brokerButton');
						const b2 = TCG.getChildByName(form.ownerCt, 'ourButton');
						const tb = TCG.getChildByName(form.ownerCt, 'expenseListGrid').getTopToolbar();
						if (form.formValues.bookingDate) {
							msg = 'This mark to market transfer was processed on ' + TCG.renderDate(form.formValues.bookingDate) + ' and can no longer be modified.';
							b1.disable();
							b2.disable();
							tb.hide();
						}
						else {
							b1.enable();
							b2.enable();
							tb.show();
						}
						return msg;
					},

					items: [
						{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
						{
							xtype: 'panel',
							layout: 'column',
							labelWidth: 120,
							items: [
								{
									columnWidth: .7,
									layout: 'form',
									items: [
										{fieldLabel: 'Custodian Account', name: 'custodianInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'custodianInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
										{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
										{fieldLabel: 'Mark Date', name: 'markDate', xtype: 'displayfield', type: 'date'}
									]
								},
								{
									columnWidth: .3,
									layout: 'form',
									labelWidth: 100,
									items: [
										{fieldLabel: 'Custodian', name: 'custodianInvestmentAccount.issuingCompany.label', xtype: 'linkfield', detailIdField: 'custodianInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
										{fieldLabel: 'Holding Company', name: 'holdingInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
										{xtype: 'accounting-bookingdate', sourceTable: 'AccountingM2MDaily'}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							labelWidth: 120,
							items: [
								{
									columnWidth: .24,
									layout: 'form',
									defaults: {anchor: '-2'},
									items: [
										{
											xtype: 'button', name: 'brokerButton', text: 'Our Amount', style: 'margin-left: 125px', tooltip: 'Reconcile and use <b>Our Amount</b> for mark to market transfer', iconCls: 'row_reconciled', enableToggle: true, toggleGroup: 'reconcileSelection',
											listeners: {
												click: function() {
													const p = TCG.getParentFormPanel(this);
													p.setFormValue('transferAmount', p.getFormValue('ourTransferAmount', true));
													p.setFormValue('reconciled', true);
												}
											}
										},
										{fieldLabel: 'Mark Amount', name: 'ourMarkAmount', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Adjustments', name: 'adjustmentAmountInMarkCurrency', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Total', qtip: 'Mark Amount + Adjustments', name: 'ourTransferAmount', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{fieldLabel: '<b>Transfer Amount</b>', name: 'transferAmount', xtype: 'currencyfield', qtip: 'Actual Transfer Amount in Mark Currency that will be transferred between Holding and Custodian investment accounts.'}
									]
								},
								{
									columnWidth: .12,
									layout: 'form',
									labelWidth: 1,
									defaults: {anchor: '-2'},
									items: [
										{
											xtype: 'button', name: 'ourButton', text: 'Broker Amount', style: 'margin-left: 7px', tooltip: 'Reconcile and use <b>Broker Amount</b> for mark to market transfer', iconCls: 'row_reconciled', enableToggle: true, toggleGroup: 'reconcileSelection',
											listeners: {
												click: function() {
													const p = TCG.getParentFormPanel(this);
													p.setFormValue('transferAmount', p.getFormValue('expectedTransferAmount', true));
													p.setFormValue('reconciled', true);
												}
											}
										},
										{name: 'expectedMarkAmount', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{name: 'expectedAdjustmentAmount', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{name: 'expectedTransferAmount', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{value: 'Reconciled:', xtype: 'displayfield', style: 'text-align: RIGHT'}
									]
								},
								{
									columnWidth: .12,
									layout: 'form',
									labelWidth: 1,
									defaults: {anchor: '-2'},
									items: [
										{value: 'Difference', xtype: 'displayfield', style: 'text-align: RIGHT', qtip: 'Our Amount - Broker Amount'},
										{name: 'differenceInMarkAmount', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{name: 'fieldThatIsNotUsed', xtype: 'currencyfield', value: 0, readOnly: true, submitValue: false},
										{name: 'differenceInTransferAmount', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{name: 'reconciled', xtype: 'checkbox'}
									]
								},
								{
									columnWidth: .52,
									layout: 'form',
									labelWidth: 75,
									defaults: {anchor: '0'},
									items: [
										{value: '&nbsp;', xtype: 'displayfield'},
										{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 95, labelStyle: 'text-align: RIGHT'}
									]
								}
							]
						},

						{
							xtype: 'formgrid',
							storeRoot: 'expenseList',
							name: 'expenseListGrid',
							dtoClass: 'com.clifton.accounting.m2m.AccountingM2MDailyExpense',
							doNotSubmitFields: ['expenseAmountInMarkCurrency'],
							title: 'Adjustments to Mark to Market',
							viewConfig: {emptyText: 'No Adjustments found on this mark day', deferEmptyText: false},
							anchor: '0',
							columnsConfig: [
								{header: 'Expense Type', dataIndex: 'type.name', idDataIndex: 'type.id', width: 150, editor: {xtype: 'combo', url: 'accountingM2MDailyExpenseTypeListFind.json', loadAll: true, displayField: 'label', allowBlank: false}},
								{header: 'CCY', dataIndex: 'expenseCurrency.symbol', idDataIndex: 'expenseCurrency.id', width: 80, editor: {xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', displayField: 'label', allowBlank: false}},
								{
									header: 'Our Amount', width: 105, useNull: true, dataIndex: 'ourExpenseAmount', type: 'currency', tooltip: 'Adjustment amount in local currency calculated by our system',
									editor: {xtype: 'currencyfield', allowDecimals: false}
								},
								{header: 'Broker Amount', width: 105, useNull: true, dataIndex: 'expectedExpenseAmount', editor: {xtype: 'currencyfield'}, type: 'currency', tooltip: 'Adjustment amount in local currency calculated and sent to use by the broker'},
								{header: 'Adjustment Amount', width: 120, useNull: true, dataIndex: 'expenseAmount', editor: {xtype: 'currencyfield'}, type: 'currency', tooltip: 'Actual adjustment amount in local currency used in transfer amount calculation: either our or broker amount'},
								{header: 'FX Rate', width: 80, dataIndex: 'expenseFxRate', editor: {xtype: 'floatfield'}, type: 'float'},
								{header: 'Mark Amount', width: 110, dataIndex: 'expenseAmountInMarkCurrency', type: 'currency', summaryType: 'sum', tooltip: 'Adjustment Amount * FX Rate'},
								{header: 'Note', width: 300, useNull: true, dataIndex: 'note', editor: {xtype: 'textfield'}}
							],
							plugins: {ptype: 'gridsummary'},
							getNewRowDefaults: function() {
								const fp = this.getWindow().getMainFormPanel();
								let ccy = fp.getFormValue('markCurrency');
								if (TCG.isNull(ccy)) {
									ccy = fp.getFormValue('clientInvestmentAccount.baseCurrency');
								}
								return {
									'expenseCurrency.id': ccy.id,
									'expenseCurrency.symbol': ccy.symbol,
									expenseFxRate: 1
								};
							},
							addToolbarButtons: function(toolBar, grid) {
								toolBar.add({
									text: 'Use Our Amount',
									tooltip: 'Copy "Our Amount" values into "Adjustment Amount"',
									iconCls: 'row_reconciled',
									scope: grid,
									handler: function() {
										const store = this.getStore();
										const count = store.getCount();
										for (let i = 0; i < count; i++) {
											const rec = store.getAt(i);
											rec.set('expenseAmount', rec.get('ourExpenseAmount'));
											rec.set('expenseAmountInMarkCurrency', rec.get('ourExpenseAmount') * rec.get('expenseFxRate'));
										}
										this.markModified();
									}
								});
								toolBar.add('-');
								toolBar.add({
									text: 'Use Broker Amount',
									tooltip: 'Copy "Broker Amount" values into "Adjustment Amount"',
									iconCls: 'row_reconciled',
									scope: grid,
									handler: function() {
										const store = this.getStore();
										const count = store.getCount();
										for (let i = 0; i < count; i++) {
											const rec = store.getAt(i);
											rec.set('expenseAmount', rec.get('expectedExpenseAmount'));
											rec.set('expenseAmountInMarkCurrency', rec.get('expectedExpenseAmount') * rec.get('expenseFxRate'));
										}
										this.markModified();
									}
								});
								toolBar.add('-');
								toolBar.add({
									text: 'Info',
									tooltip: 'Adjustments will appear in the "Adjustments" column, resulting in a change to <b>Total Our Amount</b> and the <b>Broker Mark Amount</b> (Our Mark Amount and Broker Total Amount are never adjusted). Enter client adjustments as negative and revenue as positive.',
									iconCls: 'info'
								});
								toolBar.add('-');
							}
						},

						{xtype: 'label', html: '&nbsp;'},
						{
							name: 'collateralBalanceM2MDailyExtendedListFind',
							xtype: 'gridpanel',
							title: 'Mark to Market History',
							border: false,
							frame: true,
							collapsible: true,
							hideStandardButtons: true,
							anchor: '0',
							isPagingEnabled: function() {
								return false;
							},
							height: 150,//175,
							columns: [
								{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
								{header: 'Mark Date', width: 15, dataIndex: 'markDate', searchFieldName: 'markDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
								{header: 'Transfer Amount', width: 23, dataIndex: 'transferAmount', type: 'currency', positiveInGreen: true, negativeInRed: true},
								{header: 'Collateral Amount', width: 23, dataIndex: 'collateralTransferAmount', type: 'currency', negativeInRed: true, positiveInGreen: true},
								{header: 'Difference', width: 20, dataIndex: 'differenceInTransferAmount', type: 'currency', negativeInRed: true, positiveInGreen: true, tooltip: 'Our Amount - Broker Amount'},
								{header: 'Note', width: 115, dataIndex: 'note'},
								{
									header: '&nbsp;', width: 5, dataIndex: 'note',
									renderer: function(clz, args, r) {
										return TCG.renderActionColumn('copy', '', 'Copy this note from this day to current mark note', 'CopyNote');
									}
								}
							],
							getLoadParams: function() {
								const win = this.getWindow();
								const accountingM2MDailyId = win.params.id;
								return {
									accountingM2MDailyId: accountingM2MDailyId,
									enableOpenSessionInView: true
								};
							},
							editor: {
								detailPageClass: 'Clifton.accounting.m2m.M2MDailyWindow',
								drillDownOnly: true
							},
							gridConfig: {
								listeners: {
									'rowclick': function(grid, rowIndex, evt) {
										if (TCG.isActionColumn(evt.target)) {
											const eventName = TCG.getActionColumnEventName(evt);
											const row = grid.store.data.items[rowIndex];
											if (eventName === 'CopyNote') {
												grid.ownerGridPanel.getWindow().getMainFormPanel().setFormValue('note', row.json.note);
											}
										}
									}
								}
							}
						}
					]
				}]
			},


			{
				title: 'Mark Positions',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingPositionDailyLiveForM2MList',
					instructions: 'Daily positions for selected Mark Date are recalculated LIVE from the General Ledger and Market Data.<br/><br/>NOTE: Mark amount is only for the Mark Date. It does not include previous day(s) after a holiday.',
					groupField: 'accountingTransaction.investmentSecurity.label',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Lots" : "Lot"]}',
					viewNames: ['Futures', 'Cleared OTC', 'Cleared OTC - Detailed'],
					appendStandardColumns: false,
					columns: [
						{header: 'Transaction ID', dataIndex: 'accountingTransaction.id', type: 'int', width: 50, hidden: true},
						{header: 'GL Account', dataIndex: 'accountingTransaction.accountingAccount.name', width: 90, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}, hidden: true},
						{header: 'Security', dataIndex: 'accountingTransaction.investmentSecurity.label', width: 70, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
						{header: 'CCY', tooltip: 'Currency Denomination of Security', dataIndex: 'accountingTransaction.investmentSecurity.instrument.tradingCurrency.symbol', width: 40, filter: {type: 'combo', searchFieldName: 'investmentInstrumentCurrencyDenominationId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}, hidden: true, viewNames: ['Cleared OTC', 'Cleared OTC - Detailed']},
						{header: 'Prior Qty', dataIndex: 'priorQuantityNormalized', type: 'float', useNull: true, negativeInRed: true, width: 80, summaryType: 'sum', hideGrandTotal: true, hidden: true},
						{header: 'Remaining Qty', dataIndex: 'remainingQuantityNormalized', type: 'float', useNull: true, negativeInRed: true, width: 80, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Today Closed Qty', dataIndex: 'todayClosedQuantityNormalized', type: 'float', useNull: true, negativeInRed: true, width: 90, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Open Price', dataIndex: 'accountingTransaction.price', type: 'float', useNull: true, negativeInRed: true, width: 70, hidden: true},
						{header: 'Prior Price', dataIndex: 'priorPrice', type: 'float', useNull: true, negativeInRed: true, width: 70, viewNames: ['Futures', 'Cleared OTC - Detailed']},
						{header: 'Market Price', dataIndex: 'marketPrice', type: 'float', useNull: true, negativeInRed: true, width: 70},
						{header: 'Prior FX Rate', dataIndex: 'priorFxRate', type: 'float', width: 60, hidden: true, viewNames: ['Cleared OTC - Detailed']},
						{header: 'FX Rate', dataIndex: 'marketFxRate', type: 'float', width: 60},

						{header: 'Local Cost Basis', dataIndex: 'remainingCostBasisLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base Cost Basis', dataIndex: 'remainingCostBasisBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
						{header: 'Prior Local Notional', dataIndex: 'priorNotionalValueLocal', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Prior Base Notional', dataIndex: 'priorNotionalValueBase', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum'},
						{header: 'Local Notional', dataIndex: 'notionalValueLocal', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base Notional', dataIndex: 'notionalValueBase', type: 'currency', negativeInRed: true, width: 90, summaryType: 'sum', hidden: true},

						{header: 'Prior Local Accrual', dataIndex: 'priorAccrualLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Prior Base Accrual', dataIndex: 'priorAccrualBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
						{header: 'Local Accrual 1', dataIndex: 'accrual1Local', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true, viewNames: ['Cleared OTC - Detailed']},
						{header: 'Local Accrual 2', dataIndex: 'accrual2Local', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true, viewNames: ['Cleared OTC - Detailed']},
						{header: 'Local Accrual', dataIndex: 'accrualLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true, viewNames: ['Cleared OTC']},
						{header: 'Base Accrual 1', dataIndex: 'accrual1Base', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
						{header: 'Base Accrual 2', dataIndex: 'accrual2Base', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
						{header: 'Base Accrual', dataIndex: 'accrualBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},

						{header: 'Prior Local Realized', dataIndex: 'priorRealizedGainLossLocal', type: 'currency', negativeInRed: true, width: 90, summaryType: 'sum', hideGrandTotal: true, hidden: true},
						{header: 'Prior Base Realized', dataIndex: 'priorRealizedGainLossBase', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum'},
						{header: 'Local Realized', dataIndex: 'todayRealizedGainLossLocal', type: 'currency', negativeInRed: true, width: 90, summaryType: 'sum', hideGrandTotal: true, viewNames: ['Futures', 'Cleared OTC', 'Cleared OTC - Detailed']},
						{header: 'Base Realized', dataIndex: 'todayRealizedGainLossBase', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum'},
						{header: 'Local Realized Adjustment', dataIndex: 'realizedGainLossAdjustmentLocal', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base Realized Adjustment', dataIndex: 'realizedGainLossAdjustmentBase', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum'},
						{header: 'Local Commission', dataIndex: 'todayCommissionLocal', type: 'currency', negativeInRed: true, width: 90, summaryType: 'sum', hideGrandTotal: true, viewNames: ['Futures', 'Cleared OTC', 'Cleared OTC - Detailed']},
						{header: 'Base Commission', dataIndex: 'todayCommissionBase', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum'},

						{header: 'Local Gain/Loss', dataIndex: 'dailyGainLossLocal', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base Gain/Loss', dataIndex: 'dailyGainLossBase', type: 'currency', negativeInRed: true, width: 90, hidden: true, summaryType: 'sum'},
						{header: 'Prior Local OTE', dataIndex: 'priorOpenTradeEquityLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Prior Base OTE', dataIndex: 'priorOpenTradeEquityBase', type: 'currency', negativeInRed: true, width: 80, summaryType: 'sum', viewNames: ['Futures']},
						{header: 'Local OTE', dataIndex: 'openTradeEquityLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base OTE', dataIndex: 'openTradeEquityBase', type: 'currency', negativeInRed: true, width: 80, summaryType: 'sum', viewNames: ['Futures']},

						{header: 'Prior Local NPV', dataIndex: 'priorNpvLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true, viewNames: ['Cleared OTC - Detailed']},
						{header: 'Prior Base NPV', dataIndex: 'priorNpvBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', viewNames: ['Cleared OTC - Detailed']},
						{header: 'Local NPV', dataIndex: 'npvLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true, viewNames: ['Cleared OTC', 'Cleared OTC - Detailed']},
						{header: 'Base NPV', dataIndex: 'npvBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', viewNames: ['Cleared OTC', 'Cleared OTC - Detailed']},

						{header: 'Prior Local Market Value', dataIndex: 'priorMarketValueLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Prior Base Market Value', dataIndex: 'priorMarketValueBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
						{header: 'Local Market Value', dataIndex: 'marketValueLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base Market Value', dataIndex: 'marketValueBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},

						{header: 'PAI', dataIndex: 'paiAmountLocal', type: 'currency', negativeInRed: true, width: 40, hidden: true, summaryType: 'sum', hideGrandTotal: true, viewNames: ['Cleared OTC', 'Cleared OTC - Detailed'], tooltip: 'Price Alignment Interest in Local Currency (not included in the Mark)'},

						{header: 'Local Mark', dataIndex: 'markAmountLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true, viewNames: ['Cleared OTC', 'Cleared OTC - Detailed']},
						{header: 'Base Mark', dataIndex: 'markAmountBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 80, summaryType: 'sum', css: 'BORDER-LEFT: #c0c0c0 1px solid;'}
					],
					plugins: {ptype: 'gridsummary'},
					updateCount: function() {
						// don't need record count
					},
					listeners: {
						afterRender: function() {
							if (this.getWindow().getMainFormPanel().getHoldingAccountType() === 'OTC Cleared') {
								this.setDefaultView('Cleared OTC');
							}
						}
					},
					editor: {
						detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.accountingTransaction.id;
						}
					},
					getTopToolbarFilters: function(toolbar) {
						const grid = TCG.getParentByClass(toolbar, TCG.grid.GridPanel);
						const f = grid.getWindow().getMainFormPanel();
						return [{fieldLabel: 'Mark Date', xtype: 'toolbar-datefield', name: 'markDate', value: TCG.parseDate(f.getFormValue('markDate')).format('m/d/Y')}];
					},
					getLoadParams: function(first) {
						const f = this.getWindow().getMainFormPanel();
						const localMark = TCG.isNotNull(f.getFormValue('markCurrency'));
						if (first) {
							if (localMark) {
								const cm = this.getColumnModel();
								cm.setHidden(cm.findColumnIndex('priorOpenTradeEquityBase'), true);
								cm.setHidden(cm.findColumnIndex('priorOpenTradeEquityLocal'), false);
								cm.setHidden(cm.findColumnIndex('openTradeEquityBase'), true);
								cm.setHidden(cm.findColumnIndex('openTradeEquityLocal'), false);
								cm.setHidden(cm.findColumnIndex('markAmountBase'), true);
								cm.setHidden(cm.findColumnIndex('markAmountLocal'), false);
							}
						}
						const params = {
							snapshotDate: TCG.getChildByName(this.getTopToolbar(), 'markDate').getValue().format('m/d/Y'),
							holdingAccountId: f.getFormValue('holdingInvestmentAccount.id')
						};
						if (localMark) {
							params.investmentInstrumentCurrencyDenominationId = f.getFormValue('markCurrency.id');
						}
						return params;
					}
				}]
			},


			{
				title: 'Account Reconciliation',
				items: [{
					xtype: 'reconcile-position-match-grid',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setDefaultView('Mark to Market View');
						}
						const f = this.getWindow().getMainFormPanel();
						return {
							holdingAccountId: f.getFormValue('holdingInvestmentAccount.id'),
							positionDate: TCG.renderDate(f.getFormValue('markDate')),
							excludeClosedPositions: (this.viewName && this.viewName === 'Position Reconciliation')
						};
					}
				}]
			},


			{
				title: 'Transactions',
				items: [{
					xtype: 'accounting-transactionGrid',
					getLoadParams: function(firstRun) {
						if (firstRun) {
							const cm = this.getColumnModel();
							cm.setHidden(cm.findColumnIndex('id'), true);
							cm.setHidden(cm.findColumnIndex('parentTransaction.id'), true);
							cm.setHidden(cm.findColumnIndex('clientInvestmentAccount.label'), true);
							cm.setHidden(cm.findColumnIndex('holdingInvestmentAccount.label'), true);
						}
						return {
							holdingInvestmentAccountId: this.getWindow().getMainFormPanel().getFormValue('holdingInvestmentAccount.id')
						};
					}
				}]
			},


			{
				title: 'Prior Day Trade Fills',
				items: [{
					name: 'tradeFillListFind',
					xtype: 'gridpanel',
					instructions: 'Yesterday\'s trade fills for the selected mark to market holding account.',
					forceFit: false,
					viewConfig: {emptyText: 'No prior day trade fills for this mark to market account'},

					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{header: 'Trade ID', width: 50, dataIndex: 'trade.id', hidden: true},
						{header: 'Workflow Status', width: 100, dataIndex: 'trade.workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
						{header: 'Workflow State', width: 100, dataIndex: 'trade.workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Trade Type', width: 90, dataIndex: 'trade.tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}, hidden: true},
						{header: 'Trade Group Type', width: 100, dataIndex: 'trade.tradeGroup.tradeGroupType.name', filter: {type: 'combo', searchFieldName: 'tradeGroupTypeId', url: 'tradeGroupTypeListFind.json'}},
						{header: 'Destination', width: 90, dataIndex: 'trade.tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}, hidden: true},
						{header: 'Account #', width: 70, dataIndex: 'trade.clientInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
						{header: 'Client Account', width: 250, dataIndex: 'trade.clientInvestmentAccount.name', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
						{header: 'Holding Account', width: 100, dataIndex: 'trade.holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'Holding Company', width: 150, dataIndex: 'trade.holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Executing Broker', width: 150, dataIndex: 'trade.executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Description', width: 200, dataIndex: 'trade.description', hidden: true},
						{
							header: 'Buy/Sell', width: 55, dataIndex: 'trade.buy', filter: {searchFieldName: 'buy'}, type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{
							header: 'Open/Close', width: 10, dataIndex: 'trade.openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeList.json', loadAll: true}, viewNames: ['Export Friendly'],
							renderer: function(v, metaData) {
								metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
								return v;
							}
						},
						{header: 'Original Face', width: 80, dataIndex: 'trade.originalFace', type: 'float', useNull: true, hidden: true},
						{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float', useNull: true},
						{header: 'Notional Multiplier', width: 100, dataIndex: 'trade.notionalMultiplier', type: 'float', useNull: true, hidden: true},
						{header: 'Price Multiplier', width: 100, dataIndex: 'trade.investmentSecurity.priceMultiplier', type: 'float', hidden: true, filter: {searchFieldName: 'priceMultiplier'}},
						{header: 'Security', width: 70, dataIndex: 'trade.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Investment Hierarchy', width: 150, dataIndex: 'trade.investmentSecurity.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}, hidden: true},
						{header: 'Security Name', width: 100, dataIndex: 'trade.investmentSecurity.name', hidden: true},
						{header: 'CCY Denom', width: 100, dataIndex: 'trade.investmentSecurity.instrument.tradingCurrency.symbol', filter: {type: 'combo', searchFieldName: 'securityCurrencyDenominationId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
						{header: 'Exchange Rate', width: 100, dataIndex: 'trade.exchangeRateToBase', filter: {searchFieldName: 'exchangeRateToBaseToBase'}, type: 'float', useNull: true},
						{header: 'Fill Price', width: 90, dataIndex: 'notionalUnitPrice', type: 'float', useNull: true},
						{header: 'Accrual', width: 90, dataIndex: 'trade.accrualAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Commission Per Unit', width: 120, dataIndex: 'trade.commissionPerUnit', filter: {searchFieldName: 'tradeCommissionPerUnit'}, type: 'float', hidden: true},
						{header: 'Commission Amount', width: 120, dataIndex: 'trade.commissionAmount', filter: {searchFieldName: 'tradeCommissionAmount'}, type: 'currency'},
						{header: 'Fee Amount', width: 90, dataIndex: 'trade.feeAmount', filter: {searchFieldName: 'tradeFeeAmount'}, type: 'currency'},
						{header: 'Block', width: 50, dataIndex: 'trade.blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
						{header: 'Trader', width: 100, dataIndex: 'trade.traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
						{header: 'Traded On', width: 90, dataIndex: 'trade.tradeDate', filter: {searchFieldName: 'tradeDate'}, defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'Settled On', width: 90, dataIndex: 'trade.settlementDate', filter: {searchFieldName: 'settlementDate'}}
					],
					getLoadParams: function(firstLoad) {
						const win = TCG.getParentByClass(this, Ext.Window);
						const formPanel = win.getMainFormPanel();

						if (formPanel) {
							return {
								requestedMaxDepth: 6,
								tradeDate: TCG.parseDate(formPanel.getFormValue('markDate')).format('m/d/Y'),
								holdingInvestmentAccountId: formPanel.getFormValue('holdingInvestmentAccount.id')
							};
						}
						return false;
					},
					editor: {
						detailPageClass: 'Clifton.trade.TradeWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.trade.id;
						}
					}
				}]
			},


			{
				title: 'Cash Instructions',
				items: [{
					xtype: 'investment-instruction-item-grid',
					tableName: 'AccountingM2MDaily',
					defaultCategoryName: 'M2M Counterparty',
					getInstructionDate: function() {
						return TCG.getValue('markDate', this.getWindow().getMainForm().formValues);
					}
				}]
			},


			{
				title: 'SWIFT Messages',
				items: [{
					xtype: 'swift-message-grid-panel',
					instructions: 'SWIFT messages created for these Mark to Market Instructions.',
					tableName: 'AccountingM2MDaily'
				}]
			}]
	}]
});
