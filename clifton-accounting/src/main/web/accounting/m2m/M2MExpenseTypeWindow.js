Clifton.accounting.m2m.M2MExpenseTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'M2M Adjustment Type',
	width: 700,

	items: [{
		xtype: 'formpanel',
		url: 'accountingM2MDailyExpenseType.json',
		items: [
			{fieldLabel: 'Adjustment Name', name: 'name'},
			{fieldLabel: 'External Name', name: 'externalName', qtip: 'The name that an external system will have for this expense.  Will be unique with the income field.<br/>For example:  We can have 2 type with "PAI" as the external name but one with income = false and the other with income = true.'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},

			{fieldLabel: 'GL Account', name: 'accountingAccount.name', hiddenName: 'accountingAccount.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'accountingAccountListFind.json'},
			{fieldLabel: 'Source Table', name: 'expenseSourceFkTable.name', hiddenName: 'expenseSourceFkTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow', qtip: 'The source table for an object the will create events of this type.'},

			{fieldLabel: 'Days To Settle', name: 'daysToSettle', xtype: 'integerfield'},

			{boxLabel: 'Income Adjustment (as opposed to Expense)', name: 'income', xtype: 'checkbox'},
			{boxLabel: 'Delete all expenses of this type when the M2M is rebuilt.', name: 'deleteOnM2MRebuild', xtype: 'checkbox'},
			{boxLabel: 'Add the expected expense amount to the expected transfer amount on the M2M entry.', name: 'adjustExpectedTransferAmount', xtype: 'checkbox'},
			{boxLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox'}
		]
	}]
});
