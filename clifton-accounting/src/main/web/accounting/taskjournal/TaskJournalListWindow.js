Clifton.accounting.taskjournal.TaskJournalListWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingTaskJournalListWindow',
	title: 'Task Journals',
	iconCls: 'task',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,

		items: [
			{
				title: 'Pending Journals',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingTaskJournalListFind',
					instructions: 'Task journals are automatically generated based on the logic associated with corresponding journal type.  The journal also has a workflow associated with it that it must go through.',
					rowSelectionModel: 'checkbox',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30},
						{header: 'Workflow Status', width: 65, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
						{header: 'Workflow State', width: 65, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalTypeId', url: 'accountingTaskJournalTypeListFind.json'}},
						{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 50, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Holding Issuer', dataIndex: 'holdingInvestmentAccount.issuingCompany.name', width: 80, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountIssuerId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'To Client Account', dataIndex: 'toClientInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'toClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
						{header: 'To Holding Account', dataIndex: 'toHoldingInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'To Holding Issuer', dataIndex: 'toHoldingInvestmentAccount.issuingCompany.name', width: 80, filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountIssuerId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'Transaction', dataIndex: 'transactionDate', width: 57, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Settlement', dataIndex: 'settlementDate', width: 50, hidden: true},
						{header: 'Quantity', dataIndex: 'quantity', width: 50, type: 'float', useNull: true},
						{header: 'Amount', dataIndex: 'amount', width: 50, type: 'currency'}
					],
					getLoadParams: function() {
						return {
							journalNotBooked: true
						};
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Journal Type', width: 180, xtype: 'toolbar-combo', pageSize: 20, url: 'accountingTaskJournalTypeListFind.json', linkedFilter: 'journalType.name'}
						];
					},
					editor: {
						allowToDeleteMultiple: true,
						detailPageClass: 'Clifton.accounting.taskjournal.TaskJournalWindow',
						addToolbarAddButton: function(toolBar) {
							toolBar.add({
								text: 'Book and Post',
								tooltip: 'Book selected task journal(s) and immediately post them to the General Ledger',
								iconCls: 'book-red',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									const gridPanel = this.getGridPanel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select row(s) to be booked.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Post Selected Row(s)', 'Would you like to book and post all selected rows(s)?', function(a) {
											if (a === 'yes') {
												const journals = sm.getSelections();
												gridPanel.bookSelectedJournal(true, journals, 0, gridPanel);
											}
										});
									}
								}
							});
							toolBar.add('-');
						}
					},
					bookSelectedJournal: function(post, journals, count, gridPanel) {
						const journal = journals[count].json;
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Booking and Posting Journal',
							waitTarget: this,
							params: {
								journalTypeName: 'Task Journal',
								sourceEntityId: journal.id,
								postJournal: post
							},
							timeout: 180000,
							onLoad: function(record, conf) {
								count++;
								if (count === journals.length) { // refresh after all journals were posted
									gridPanel.reload();
								}
								else {
									gridPanel.bookSelectedJournal(post, journals, count, gridPanel);
								}
							}
						});
						loader.load('accountingJournalBook.json?requestedPropertiesRoot=data&requestedProperties=id');
					}
				}]
			},


			{
				title: 'Booked Journals',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingTaskJournalListFind',
					instructions: 'Task journals are automatically generated based on the logic associated with corresponding journal type.  The journal also has a workflow associated with it that it must go through.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30},
						{header: 'Workflow Status', width: 65, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
						{header: 'Workflow State', width: 65, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalTypeId', url: 'accountingTaskJournalTypeListFind.json'}},
						{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 50, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Holding Issuer', dataIndex: 'holdingInvestmentAccount.issuingCompany.name', width: 80, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountIssuerId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'To Client Account', dataIndex: 'toClientInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'toClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
						{header: 'To Holding Account', dataIndex: 'toHoldingInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'To Holding Issuer', dataIndex: 'toHoldingInvestmentAccount.issuingCompany.name', width: 80, filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountIssuerId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'Transaction', dataIndex: 'transactionDate', width: 57, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Settlement', dataIndex: 'settlementDate', width: 50, hidden: true},
						{header: 'Booked', dataIndex: 'bookingDate', width: 50, hidden: true},
						{header: 'Quantity', dataIndex: 'quantity', width: 50, type: 'float', useNull: true},
						{header: 'Amount', dataIndex: 'amount', width: 50, type: 'currency'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -30)});
						}
						return {
							journalBooked: true
						};
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Journal Type', width: 180, xtype: 'toolbar-combo', url: 'accountingTaskJournalTypeListFind.json', linkedFilter: 'journalType.name'}
						];
					},
					editor: {
						detailPageClass: 'Clifton.accounting.taskjournal.TaskJournalWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Journal Types',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingTaskJournalTypeListFind',
					instructions: 'Task journal types define journal generation and booking logic specific to each type.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Name', dataIndex: 'name', width: 100},
						{header: 'Description', dataIndex: 'description', width: 250},
						{header: 'Journal Generator Bean', dataIndex: 'journalGeneratorBean.name', width: 100},
						{header: 'Amount Field', dataIndex: 'amountFieldName', width: 50}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.taskjournal.TaskJournalTypeWindow'
					}
				}]
			},


			{
				title: 'Generator',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						height: 200,
						instructions: 'Find all task journals that should be generated for the specified date range and create them if they do not already exist. Optionally limit journal generation to selected type.',
						items: [
							{fieldLabel: 'From Date', name: 'fromDate', xtype: 'datefield', allowBlank: false},
							{fieldLabel: 'To Date', name: 'toDate', xtype: 'datefield', allowBlank: false},
							{fieldLabel: 'Journal Type', hiddenName: 'taskJournalTypeId', xtype: 'combo', url: 'accountingTaskJournalTypeListFind.json', detailPageClass: 'Clifton.accounting.taskjournal.TaskJournalTypeWindow'}
							//,{name: 'postEvent', xtype: 'checkbox', boxLabel: 'Immediately post generated journal(s) to GL'}
						],
						buttons: [{
							text: 'Generate Task Journal(s)',
							xtype: 'button',
							iconCls: 'run',
							width: 170,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								Ext.Msg.confirm('Generate Journals', 'Would you like to generate task journals using selected filters?', function(a) {
									if (a === 'yes') {
										form.submit(Ext.applyIf({
											url: 'accountingTaskJournalListGenerate.json',
											waitMsg: 'Generating Task Journals ...',
											success: function(form, action) {
												Ext.Msg.alert('Processing Started', action.result.result.message, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reload.defer(300, grid);
												});
											}
										}, Ext.applyIf({timeout: 300}, TCG.form.submitDefaults)));
									}
								});
							}
						}]
					},

					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'TASK-JOURNALS',
						instantRunner: true,
						title: 'Current Processing',
						flex: 1
					}
				]
			}
		]
	}]
});
