Clifton.accounting.taskjournal.TaskJournalTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Task Journal Type',
	iconCls: 'task',

	items: [{
		xtype: 'formpanel',
		instructions: 'Task journal types define journal generation and booking logic specific to each type.',
		url: 'accountingTaskJournalType.json',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{
				fieldLabel: 'Journal Generator Bean', name: 'journalGeneratorBean.name', hiddenName: 'journalGeneratorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Accounting Task Journal Generator',
				detailPageClass: 'Clifton.system.bean.BeanWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Accounting Task Journal Generator'}}};
				}
			},
			{fieldLabel: 'Amount Field', name: 'amountFieldName'}
		]
	}]
});
