Clifton.accounting.taskjournal.TaskJournalWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Task Journal',
	iconCls: 'task',
	width: 800,

	tbar: {
		xtype: 'workflow-toolbar',
		tableName: 'AccountingTaskJournal',
		//finalState: 'Receipt Confirmed',
		enableOpenSessionInView: true
	},
	items: [{
		xtype: 'formpanel',
		url: 'accountingTaskJournal.json?requestedMaxDepth=3',
		labelWidth: 130,
		readOnly: true,
		items: [
			{fieldLabel: 'Journal Type', name: 'journalType.name', detailIdField: 'journalType.id', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.taskjournal.TaskJournalTypeWindow'},
			{fieldLabel: 'Accounting Journal', name: 'accountingJournal.label', detailIdField: 'accountingJournal.id', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.journal.JournalWindow'},
			{fieldLabel: 'Security', name: 'investmentSecurity.label', detailIdField: 'investmentSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', detailIdField: 'clientInvestmentAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', detailIdField: 'holdingInvestmentAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'To Client Account', name: 'toClientInvestmentAccount.label', detailIdField: 'toClientInvestmentAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'To Holding Account', name: 'toHoldingInvestmentAccount.label', detailIdField: 'toHoldingInvestmentAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{xtype: 'label', html: '<hr/>'},
			{
				xtype: 'panel',
				layout: 'column',
				items: [{
					columnWidth: .60,
					layout: 'form',
					labelWidth: 130,
					items: [
						{fieldLabel: 'Quantity', name: 'quantity', xtype: 'floatfield'},
						{fieldLabel: 'Amount', name: 'amount', xtype: 'currencyfield'}
					]
				},
					{
						columnWidth: .40,
						layout: 'form',
						labelWidth: 130,
						items: [
							{fieldLabel: 'Transaction Date', name: 'transactionDate', xtype: 'datefield'},
							{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'},
							{xtype: 'accounting-bookingdate', sourceTable: 'AccountingTaskJournal'}
						]
					}]
			}
		]
	}]
});
