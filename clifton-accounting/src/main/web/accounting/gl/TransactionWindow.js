Clifton.accounting.gl.TransactionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'GL Transaction',
	iconCls: 'book-red',
	width: 1000,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Transaction',
				tbar: [{
					text: 'Adjustments',
					disabled: true,
					menu: {
						xtype: 'menu',
						items: [
							{
								text: 'Custom Adjustment',
								tooltip: 'Create a custom journal that adjusts this position',
								iconCls: 'book-open-blue',
								handler: function() {
									const fp = TCG.getParentTabPanel(this).items.get(0).items.get(0);
									const t = fp.getForm().formValues;
									const jDetail = {
										parentTransaction: t,
										clientInvestmentAccount: t.clientInvestmentAccount,
										holdingInvestmentAccount: t.holdingInvestmentAccount,
										investmentSecurity: t.investmentSecurity,
										exchangeRateToBase: t.exchangeRateToBase,
										transactionDate: new Date(),
										originalTransactionDate: t.originalTransactionDate,
										settlementDate: t.originalTransactionDate
									};
									const jType = TCG.data.getData('accountingJournalTypeByName.json?journalTypeName=General Journal', this);
									const journal = {
										journalType: jType,
										journalDetailList: [jDetail, jDetail, jDetail, jDetail]
									};
									const config = {
										defaultDataIsReal: true,
										defaultData: journal
									};
									TCG.createComponent('Clifton.accounting.journal.EditableJournalWindow', config);
								}
							},
							{
								text: 'Realized Gain / Loss Adjustment',
								tooltip: 'Create a new journal that adjusts Realized Gain / Loss for selected position',
								iconCls: 'book-red',
								handler: function() {
									const fp = TCG.getParentTabPanel(this).items.get(0).items.get(0);
									TCG.createComponent('Clifton.accounting.gl.RealizedAdjustmentWindow', {
										defaultData: {accountingPositionId: fp.getFormValue('id')},
										openerCt: fp
									});
								}
							},
							{
								text: 'Commission Adjustment',
								tooltip: 'Create a new journal that adjusts Commission for selected position',
								iconCls: 'book-red',
								handler: function() {
									const fp = TCG.getParentTabPanel(this).items.get(0).items.get(0);
									TCG.createComponent('Clifton.accounting.gl.CommissionAdjustmentWindow', {
										defaultData: {accountingPositionId: fp.getFormValue('id')},
										openerCt: fp
									});
								}
							},
							{
								text: 'Dividend Adjustment',
								tooltip: 'Create a new journal that adjusts Dividend for selected position',
								iconCls: 'book-red',
								handler: function() {
									const fp = TCG.getParentTabPanel(this).items.get(0).items.get(0);
									TCG.createComponent('Clifton.accounting.gl.DividendAdjustmentWindow', {
										defaultData: {accountingPositionId: fp.getFormValue('id')},
										openerCt: fp
									});
								}
							},
							{
								text: 'Coupon Payment Adjustment',
								tooltip: 'Create a new journal that adjusts Coupon Payment for selected position',
								iconCls: 'event',
								handler: function() {
									const fp = TCG.getParentTabPanel(this).items.get(0).items.get(0);
									TCG.createComponent('Clifton.accounting.gl.CouponAdjustmentWindow', {
										defaultData: {
											accountingPositionId: fp.getFormValue('id')
										},
										openerCt: fp
									});
								}
							}]
					}
				}, '-'],

				items: [{
					xtype: 'formpanel',
					url: 'accountingTransaction.json?requestedMaxDepth=5',
					readOnly: true,
					labelFieldName: 'id',
					labelWidth: 120,
					defaults: {anchor: '0'},
					listeners: {
						afterload: function(fp) {
							if (TCG.isTrue(fp.getFormValue('accountingAccount.position'))) {
								fp.ownerCt.getTopToolbar().items.get(0).setDisabled(false);
							}
						}
					},
					getWarningMessage: function(form) {
						if (TCG.getValue('deleted', form.formValues)) {
							return 'The Journal of this Transaction was Unposted and is no longer valid in the General Ledger (preserved for historic analysis only).';
						}
						else if (TCG.getValue('modified', form.formValues)) {
							return 'The Journal of this Transaction was Unposted and then Re-Posted with changes. See previously deleted Journal or source entity Audit Trail for more details.';
						}
						return undefined;
					},
					items: [
						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .36,
									layout: 'form',
									items: [
										{fieldLabel: 'Transaction ID', name: 'id', xtype: 'displayfield'},
										{fieldLabel: 'Parent Transaction', name: 'parentTransaction.id', xtype: 'linkfield', detailIdField: 'parentTransaction.id', detailPageClass: 'Clifton.accounting.gl.TransactionWindow'},
										{fieldLabel: 'Security', name: 'investmentSecurity.symbol', xtype: 'linkfield', detailIdField: 'investmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
										{fieldLabel: 'Quantity', name: 'quantity', xtype: 'floatfield'},
										{fieldLabel: 'Local Debit', name: 'localDebit', xtype: 'currencyfield'},
										{fieldLabel: 'Base Debit', name: 'baseDebit', xtype: 'currencyfield'},
										{fieldLabel: 'Local Cost Basis', name: 'positionCostBasis', xtype: 'currencyfield'}
									]
								},
								{
									columnWidth: .33,
									layout: 'form',
									labelWidth: 90,
									items: [
										{fieldLabel: 'Journal Type', name: 'journal.journalType.name', xtype: 'displayfield'},
										{fieldLabel: 'Journal ID', name: 'journal.id', xtype: 'linkfield', detailIdField: 'journal.id', detailPageClass: 'Clifton.accounting.journal.JournalWindow'},
										{fieldLabel: 'GL Account', name: 'accountingAccount.label', xtype: 'linkfield', detailIdField: 'accountingAccount.id', detailPageClass: 'Clifton.accounting.account.AccountWindow'},
										{fieldLabel: 'Price', name: 'price', xtype: 'floatfield'},
										{fieldLabel: 'Local Credit', name: 'localCredit', xtype: 'currencyfield'},
										{fieldLabel: 'Base Credit', name: 'baseCredit', xtype: 'currencyfield'},
										{fieldLabel: 'FX Rate', name: 'exchangeRateToBase', xtype: 'floatfield'}
									]
								},
								{
									columnWidth: .31,
									layout: 'form',
									items: [
										{fieldLabel: 'Source Table', xtype: 'displayfield', name: 'journal.journalType.systemTable.name', qtip: ' Source Table from corresponding Accounting Journal Type'},
										{
											fieldLabel: 'Source FK Field ID', name: 'journal.fkFieldId', xtype: 'linkfield', detailIdField: 'journal.fkFieldId', qtip: 'FK Field ID from corresponding Accounting Journal (not Accounting Transaction)',
											getDetailPageClass: function(fp) {
												return TCG.getValue('journal.journalType.systemTable.detailScreenClass', fp.getForm().formValues);
											}
										},
										{fieldLabel: 'Opening', xtype: 'checkbox', name: 'opening'},
										{fieldLabel: 'Transaction Date', name: 'transactionDate', xtype: 'datefield'},
										{fieldLabel: 'Original Date', name: 'originalTransactionDate', xtype: 'datefield'},
										{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'},
										{fieldLabel: 'Posting Date', name: 'journal.postingDate', xtype: 'displayfield', type: 'date'}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
						{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
						{fieldLabel: 'Executing Company', name: 'executingCompany.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'executingCompany.id'},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50}
					]
				}]
			},


			{
				title: 'Child Transactions',
				items: [{
					xtype: 'accounting-transactionGrid',
					instructions: 'The following GL transaction have selected GL transaction as parent.',
					includeClientAccountFilter: false,
					columnOverrides: [
						{dataIndex: 'parentTransaction.id', hidden: true},
						{dataIndex: 'clientInvestmentAccount.label', hidden: true},
						{dataIndex: 'holdingInvestmentAccount.label', hidden: true}
					],
					getAdditionalTopToolbarFilters: function(grid, toolbar) {
						return [
							'-',
							{boxLabel: 'Include Grandchildren', xtype: 'toolbar-checkbox', name: 'includeGrandChildren', qtip: 'Check to include grand-child transactions in addition to child transactions.'}
						];
					},
					getCustomFilters: function(firstLoad) {
						if (TCG.getChildByName(this.getTopToolbar(), 'includeGrandChildren').checked) {
							return {
								parentOrGrandParentId: this.getWindow().getMainFormId()
							};
						}
						return {
							parentId: this.getWindow().getMainFormId()
						};
					},
					plugins: [
						{ptype: 'gridsummary'},
						{ptype: 'investment-contextmenu-plugin', enableAsyncLookups: true},
						{ptype: 'accounting-transaction-grideditor'}
					]
				}]
			},


			{
				title: 'Security Transactions',
				items: [{
					xtype: 'accounting-transactionGrid',
					instructions: 'The following GL transaction are for the same security and client/holding accounts.',
					includeClientAccountFilter: false,
					columnOverrides: [
						{dataIndex: 'investmentSecurity.symbol', hidden: true},
						{dataIndex: 'clientInvestmentAccount.label', hidden: true},
						{dataIndex: 'holdingInvestmentAccount.label', hidden: true}
					],
					getCustomFilters: function(firstLoad) {
						const fp = this.getWindow().getMainFormPanel();
						return {
							clientInvestmentAccountId: fp.getFormValue('clientInvestmentAccount.id'),
							holdingInvestmentAccountId: fp.getFormValue('holdingInvestmentAccount.id'),
							investmentSecurityId: fp.getFormValue('investmentSecurity.id')
						};
					},
					plugins: [
						{ptype: 'gridsummary'},
						{ptype: 'investment-contextmenu-plugin', enableAsyncLookups: true},
						{ptype: 'accounting-transaction-grideditor'}
					]
				}]
			}
		]
	}]
});


Clifton.accounting.gl.RealizedAdjustmentWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Realized Gain / Loss Adjustment',
	iconCls: 'book-red',
	height: 260,
	width: 510,
	modal: true,

	items: [{
		xtype: 'formpanel',
		labelWidth: 120,
		loadValidation: false,
		instructions: 'Created and posts a journal that adjusts Realized Gain / Loss for selected closing position by the specified amount (local currency).' +
			'<br /><b>Note:</b> use NEGATIVE Adjustment Amount when the CLIENT PAYS this amount.',
		items: [
			{name: 'accountingPositionId', xtype: 'hidden'},
			{fieldLabel: 'Adjustment Amount', name: 'realizedAdjustmentAmount', xtype: 'currencyfield', allowBlank: false},
			{fieldLabel: 'Private Note', name: 'note', xtype: 'textarea', height: 70}
		],
		getSaveURL: function() {
			return 'accountingJournalAdjustRealized.json?enableOpenSessionInView=true';
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Created adjusting journal with id = ' + panel.getForm().formValues.id + '.', 'Journal Status');
			}
		}
	}]
});


Clifton.accounting.gl.CommissionAdjustmentWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Commission Adjustment',
	iconCls: 'book-red',
	height: 260,
	width: 510,
	modal: true,

	items: [{
		xtype: 'formpanel',
		labelWidth: 120,
		loadValidation: false,
		instructions: 'Creates and posts a journal that adjusts Commission for selected closing position by the specified amount (local currency).' +
			'<br /><b>Note:</b> use NEGATIVE Adjustment Amount when the CLIENT PAYS this amount.',
		items: [
			{name: 'accountingPositionId', xtype: 'hidden'},
			{fieldLabel: 'Adjustment Amount', name: 'commissionAmount', xtype: 'currencyfield', allowBlank: false},
			{fieldLabel: 'Private Note', name: 'note', xtype: 'textarea', height: 70}
		],
		getSaveURL: function() {
			return 'accountingJournalAdjustCommission.json?enableOpenSessionInView=true';
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Created adjusting journal with id = ' + panel.getForm().formValues.id + '.', 'Journal Status');
			}
		}
	}]
});


Clifton.accounting.gl.DividendAdjustmentWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Dividend Adjustment',
	iconCls: 'book-red',
	height: 260,
	width: 510,
	modal: true,

	items: [{
		xtype: 'formpanel',
		labelWidth: 120,
		loadValidation: false,
		instructions: 'Creates and posts a journal that adjusts Dividend for selected closing position by the specified amount (local currency).' +
			'<br /><b>Note:</b> use NEGATIVE Adjustment Amount when the CLIENT PAYS this amount.',
		items: [
			{name: 'accountingPositionId', xtype: 'hidden'},
			{
				fieldLabel: 'Dividend Event', name: 'dividendTransactionDescription', hiddenName: 'dividendTransactionId', xtype: 'combo', url: 'accountingTransactionListFind.json?accountingAccountName=Dividend Income&orderBy=transactionDate:DESC&requestedPropertiesRoot=data&requestedProperties=id|description',
				displayField: 'description', allowBlank: false,
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const f = combo.getParentForm().getForm();
						combo.store.baseParams.parentId = f.findField('accountingPositionId').getValue();
					}
				}
			},
			{fieldLabel: 'Adjustment Amount', name: 'adjustmentAmount', xtype: 'currencyfield', allowBlank: false}
		],
		getSaveURL: function() {
			return 'accountingJournalAdjustDividend.json?enableOpenSessionInView=true';
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Created adjusting journal with id = ' + panel.getForm().formValues.id + '.', 'Journal Status');
			}
		}
	}]
});


Clifton.accounting.gl.CouponAdjustmentWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Coupon Payment Adjustment',
	iconCls: 'event',
	height: 260,
	width: 600,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Creates and posts a journal that adjusts Coupon Payment for selected closing position by the specified amount (local currency).' +
			'<br /><b>Note:</b> use NEGATIVE Adjustment Amount when the CLIENT PAYS this amount.',
		labelWidth: 120,
		items: [
			{name: 'accountingPositionId', xtype: 'hidden'},
			{
				fieldLabel: 'Coupon Event', name: 'couponTransactionDescription', hiddenName: 'couponTransactionId', xtype: 'combo', url: 'accountingTransactionListFind.json?accountingAccountName=Interest Income&orderBy=transactionDate:DESC&requestedPropertiesRoot=data&requestedProperties=id|description',
				displayField: 'description', allowBlank: false,
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const f = combo.getParentForm().getForm();
						combo.store.baseParams.parentId = f.findField('accountingPositionId').getValue();
					}
				}
			},
			{fieldLabel: 'Adjustment Amount', name: 'adjustmentAmount', xtype: 'currencyfield', allowBlank: false}
		],
		getSaveURL: function() {
			return 'accountingJournalAdjustCouponPayment.json?enableOpenSessionInView=true';
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Created adjusting journal with id = ' + panel.getForm().formValues.id + '.', 'Journal Status');
			}
		}
	}]
});
