Clifton.accounting.gl.BalanceRemovalWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Remove Balances From Account',
	iconCls: 'import',
	height: 350,
	width: 750,
	hideOKButton: true,
	applyButtonText: 'Remove',
	applyButtonTooltip: 'Remove Balances from Selected Account',
	doNotWarnOnCloseModified: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelWidth: 165,
		instructions: 'Remove positions (via position transfers out of the account) and/or non-position assets and liabilities (via general journal) out of the selected account(s) on the given date.  The accounts must currently be in an active status in order to book and post to it. If both a client and holding account are selected only balances for that combination will be removed.',

		listeners: {
			afterRender: function() {
				if (this.getWindow().defaultData && TCG.isNotBlank(this.getWindow().defaultData.holdingInvestmentAccountId)) {
					this.getForm().findField('holdingInvestmentAccountLabel').setReadOnly(true);
				}
				if (this.getWindow().defaultData && TCG.isNotBlank(this.getWindow().defaultData.clientInvestmentAccountId)) {
					this.getForm().findField('clientInvestmentAccountLabel').setReadOnly(true);
				}
			}
		},

		items: [
			{
				fieldLabel: 'Client Account', name: 'clientInvestmentAccountLabel', hiddenName: 'clientInvestmentAccountId', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', detailPageClass: 'Clifton.investment.account.AccountWindow',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm();
					if (TCG.isNotBlank(f.getFormValue('holdingInvestmentAccountId'))) {
						queryEvent.combo.store.setBaseParam('relatedAccountId', f.getFormValue('holdingInvestmentAccountId'));
					}
					else {
						queryEvent.combo.store.setBaseParam('relatedAccountId', null);
					}
				}
			},
			{
				fieldLabel: 'Holding Account', name: 'holdingInvestmentAccountLabel', hiddenName: 'holdingInvestmentAccountId', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false&workflowStatusNameEquals=Active', detailPageClass: 'Clifton.investment.account.AccountWindow',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm();
					if (TCG.isNotBlank(f.getFormValue('clientInvestmentAccountId'))) {
						queryEvent.combo.store.setBaseParam('mainAccountId', f.getFormValue('clientInvestmentAccountId'));
					}
					else {
						queryEvent.combo.store.setBaseParam('mainAccountId', null);
					}
				}
			},
			{fieldLabel: 'Removal Date', name: 'removalDate', xtype: 'datefield', allowBlank: false},
			{fieldLabel: 'Removal Note', name: 'removalNote', xtype: 'textarea', height: 30, allowBlank: false, qtip: 'Note/Description to be used on the journals'},
			{fieldLabel: '', boxLabel: 'Distribute all positions out of selected account on Removal Date', xtype: 'checkbox', name: 'distributePositions', checked: true},
			{fieldLabel: '', boxLabel: 'Distribute non Position Assets and Liabilities out of selected account on Removal Date', xtype: 'checkbox', name: 'distributeNonPositionAssetsAndLiabilities', checked: true}
		],

		getSaveURL: function() {
			return 'accountingBalanceRemovalProcess.json';
		}

	}],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Processing...',
			success: function(form, action) {
				const result = action.result.result;
				TCG.createComponent('Clifton.core.StatusWindow', {
					title: 'Balance Removal Status',
					defaultData: {status: result}
				});
				win.closeWindow();
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	}
});
