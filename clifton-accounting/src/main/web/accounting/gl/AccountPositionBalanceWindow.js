Clifton.accounting.gl.AccountPositionBalanceWindow = Ext.extend(TCG.app.Window, {
	title: 'Position Balances',
	iconCls: 'chart-pie',
	width: 1200,
	height: 600,

	items: [{xtype: 'accounting-positionBalance-gridpanel'}]
});
