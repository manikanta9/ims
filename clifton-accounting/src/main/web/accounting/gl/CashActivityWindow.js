Clifton.accounting.gl.CashActivityWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Cash Activity',
	iconCls: 'book-red',
	width: 800,

	items: [
		{
			xtype: 'formpanel',
			labelFieldName: 'clientInvestmentAccount.label',
			readOnly: true,
			loadValidation: false,
			layout: 'vbox',
			layoutConfig: {
				align: 'stretch'
			},

			getDefaultData: async function(window) {
				const defaultData = window.defaultData || {};
				const promises = [];
				const fetchClient = TCG.isNotNull(defaultData.clientInvestmentAccount) && TCG.isNotNull(defaultData.clientInvestmentAccount.id);
				const fetchHolding = TCG.isNotNull(defaultData.holdingInvestmentAccount) && TCG.isNotNull(defaultData.holdingInvestmentAccount.id);

				if (!fetchClient) {
					this.getForm().findField('clientInvestmentAccount.label').hide();
				}
				else {
					promises.push(TCG.data.getDataPromise('investmentAccount.json', this, {
						params: {
							id: defaultData.clientInvestmentAccount.id,
							requestedPropertiesRoot: 'data',
							requestedProperties: 'label|id|baseCurrency.symbol|baseCurrency.id'
						}
					}));
				}

				if (!fetchHolding) {
					this.getForm().findField('holdingInvestmentAccount.label').hide();
				}
				else {
					promises.push(TCG.data.getDataPromise('investmentAccount.json', this, {
						params: {
							id: defaultData.holdingInvestmentAccount.id,
							requestedPropertiesRoot: 'data',
							requestedProperties: 'label|id|baseCurrency.symbol|baseCurrency.id'
						}
					}));
				}

				const promiseValues = await Promise.all(promises);
				if (fetchClient) {
					defaultData.clientInvestmentAccount = promiseValues[0];
					defaultData.baseCurrency = defaultData.clientInvestmentAccount.baseCurrency;
					if (fetchHolding) {
						defaultData.holdingInvestmentAccount = promiseValues[1];
						defaultData.baseCurrency = defaultData.holdingInvestmentAccount.baseCurrency;
					}
				}
				else {
					if (fetchHolding) {
						defaultData.holdingInvestmentAccount = promiseValues[0];
						defaultData.baseCurrency = defaultData.holdingInvestmentAccount.baseCurrency;
					}
				}

				return defaultData;
			},

			afterRenderPromise: function(formPanel) {
				formPanel.updateTitle();
				formPanel.updateBeginningCashValue()
					.then(() => {
						const gridPanel = TCG.getChildByName(formPanel, 'accountingTransactionListFind');
						gridPanel.reload();
					});
			},

			updateBeginningCashValue: function() {
				const formPanel = this;
				const form = formPanel.getForm();
				const formValues = form.formValues;
				const cashBalanceParams = {
					clientInvestmentAccountId: formValues.clientInvestmentAccount ? formValues.clientInvestmentAccount.id : void 0,
					holdingInvestmentAccountId: formValues.holdingInvestmentAccount ? formValues.holdingInvestmentAccount.id : void 0,
					accountingAccountIdName: 'CASH_ACCOUNTS_EXCLUDE_COLLATERAL'
				};
				const gridPanel = TCG.getChildByName(this, 'accountingTransactionListFind');
				const minDate = TCG.getChildByName(gridPanel.getTopToolbar(), 'minDate');
				if (minDate) {
					if (TCG.isNotBlank(minDate.getValue())) {
						const lookupDateType = TCG.getChildByName(gridPanel.getTopToolbar(), 'lookupDateType');
						if (lookupDateType && TCG.isNotBlank(lookupDateType.getValue())) {
							const previousDay = minDate.getValue().add(Date.DAY, -1);
							cashBalanceParams[lookupDateType.getValue()] = previousDay.format('m/d/Y');
						}
					}
				}
				return TCG.data.getDataPromise('accountingBalanceListFind.json', this, {params: cashBalanceParams})
					.then(cashBalances => {
						let balance = 0;
						for (const cashBalance of cashBalances) {
							balance += cashBalance.localAmount;
						}
						form.findField('beginningCashBalance').setValue(balance);
						formPanel.updateCurrentCashValue(balance);
					});
			},

			updateCurrentCashValue: function(endingCashValue) {
				const endingCashField = this.getForm().findField('endingCashBalance');
				if (endingCashField) {
					endingCashField.setValue(endingCashValue);
				}
			},

			items: [
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 100,
					defaults: {layout: 'form', defaults: {anchor: '-20'}},
					items: [
						{
							columnWidth: .65,
							items: [
								{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{
									fieldLabel: 'Base Currency', name: 'baseCurrency.symbol', xtype: 'linkfield', detailIdField: 'baseCurrency.id', detailPageClass: 'Clifton.accounting.gl.TransactionListWindow',
									getDefaultData: function(formPanel) {
										const f = formPanel.getForm();
										const filters = [];
										const clientAccountField = f.findField('clientInvestmentAccount.id');
										if (TCG.isNotBlank(clientAccountField.getValue())) {
											filters.push({
												field: 'clientInvestmentAccountId',
												comparison: 'EQUALS',
												value: clientAccountField.getValue()
											});
										}
										const holdingAccountField = f.findField('holdingInvestmentAccount.id');
										if (TCG.isNotBlank(holdingAccountField.getValue())) {
											filters.push({
												field: 'holdingInvestmentAccountId',
												comparison: 'EQUALS',
												value: holdingAccountField.getValue()
											});
										}
										filters.push({
											field: 'investmentSecurityId',
											comparison: 'EQUALS',
											value: f.findField('baseCurrency.id').getValue()
										});
										const gridPanel = TCG.getChildByName(formPanel, 'accountingTransactionListFind');
										const maxDate = TCG.getChildByName(gridPanel.getTopToolbar(), 'maxDate').value;
										filters.push({
											field: 'transactionDate',
											comparison: 'LESS_THAN',
											value: maxDate
										});
										return filters;
									}
								}
							]
						},
						{
							columnWidth: .35,
							items: [
								{fieldLabel: 'Beginning Cash', name: 'beginningCashBalance', xtype: 'currencyfield'},
								{fieldLabel: 'Ending Cash', name: 'endingCashBalance', xtype: 'currencyfield'}
							]
						}
					]
				},
				{
					xtype: 'accounting-transactionGrid',
					appendStandardColumns: false,
					includeAccountingGroupFilter: false,
					includeInstrumentGroupFilter: false,
					includeClientAccountGroupFilter: false,
					includeClientAccountFilter: false,
					includeJournalTypeFilter: false,
					includeDateFilter: false,
					includeDisplayFilter: false,
					reloadOnRender: false, // reload by parent form so default data can be set
					remoteSort: true,
					forceFit: true,
					showToolbarRecordCount: false,
					flex: 1,

					isEditEnabled: () => false,
					isPagingEnabled: () => false,

					getAdditionalTopToolbarFilters: function(grid, toolbar) {
						return [
							//Date filter (auto-reload)
							{
								name: 'lookupDateType', value: 'TransactionDate', width: 110, minListWidth: 110, displayField: 'name', valueField: 'value', mode: 'local', xtype: 'toolbar-combo',
								store: {
									xtype: 'arraystore',
									fields: ['value', 'name', 'description'],
									data: [
										['TransactionDate', 'Transaction Date', 'Aggregate General Ledger transactions using Transaction Date up to and including this date'],
										['SettlementDate', 'Settlement Date', 'Aggregate General Ledger transactions using Settlement Date up to and including this date']
									]
								}
							},
							{fieldLabel: 'From', xtype: 'toolbar-datefield', name: 'minDate', value: grid.getDefaultPositionsDate(), allowBlank: false},
							{fieldLabel: 'To', xtype: 'toolbar-datefield', name: 'maxDate', value: grid.getDefaultPositionsDate(), allowBlank: false}
						];
					},

					columns: [
						{
							header: 'ID', dataIndex: 'id', type: 'int', width: 70, hidden: true, sortable: false,
							renderer: function(v, metaData, r) {
								if (r.data.deleted) {
									metaData.css = 'ruleViolationBig';
									metaData.attr = 'qtip=\'This Transaction was Unposted from the General Ledger\'';
								}
								else if (r.data.modified) {
									metaData.css = 'ruleViolation';
									metaData.attr = 'qtip=\'This Transaction was Re-Posted to the General Ledger with changes\'';
								}
								return v;
							}
						},
						{header: 'PID', dataIndex: 'parentTransaction.id', type: 'int', doNotFormat: true, useNull: true, width: 60, hidden: true, sortable: false, filter: {searchFieldName: 'parentId'}},
						{header: 'Transaction Date', dataIndex: 'transactionDate', width: 95, defaultSortColumn: true, sortable: false, defaultSortDirection: 'ASC'},
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 140, hidden: true, sortable: false, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', width: 140, hidden: true, sortable: false, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'Holding Account Type', dataIndex: 'holdingInvestmentAccount.type.name', width: 140, hidden: true, sortable: false, filter: {type: 'combo', searchFieldName: 'holdingAccountTypeId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Clearing Company', width: 120, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', hidden: true, sortable: false, filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Executing Company', width: 120, dataIndex: 'executingCompany.name', hidden: true, sortable: false, filter: {type: 'combo', searchFieldName: 'executingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 65, sortable: false, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json', showNotEquals: true}},
						{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 100, hidden: true, sortable: false, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Description', dataIndex: 'description', width: 300, sortable: false},
						{header: 'Amount', dataIndex: 'localDebitCredit', type: 'currency', width: 90, sortable: false, filter: {searchFieldName: 'localDebitOrCredit'}, summaryType: 'sum'},
						{header: 'Settlement Date', dataIndex: 'settlementDate', width: 95, hidden: true, sortable: false}
					],

					viewConfig: {emptyText: 'No transactions affecting cash found on the specified date.'},

					listeners: {
						afterrender: function() {
							const store = this.grid.getStore();
							store.on('load', () => {
								const formPanel = TCG.getParentFormPanel(this);
								formPanel.updateBeginningCashValue()
									.then(() => {
										const formValues = formPanel.getForm().formValues;
										const gridPanel = TCG.getChildByName(formPanel, 'accountingTransactionListFind');
										const minDate = TCG.getChildByName(gridPanel.getTopToolbar(), 'minDate').value;
										const maxDate = TCG.getChildByName(gridPanel.getTopToolbar(), 'maxDate').value;
										const beginningCashBalance = formPanel.getForm().findField('beginningCashBalance').getNumericValue();

										if (store.getCount() > 0) {
											store.insert(0, new store.recordType({
												id: -10,
												clientInvestmentAccount: formValues.clientInvestmentAccount,
												holdingInvestmentAccount: formValues.holdingInvestmentAccount,
												investmentSecurity: formValues.baseCurrency,
												description: '<b>Beginning Balance</b>',
												localDebitCredit: beginningCashBalance,
												transactionDate: minDate,
												settlementDate: minDate
											}));
											let currentLocalDebitCredit = 0;
											store.each(record => {
												currentLocalDebitCredit += parseFloat(record.get('localDebitCredit'));
											});
											store.add(new store.recordType({
												id: -100,
												clientInvestmentAccount: formValues.clientInvestmentAccount,
												holdingInvestmentAccount: formValues.holdingInvestmentAccount,
												investmentSecurity: formValues.baseCurrency,
												description: '<b>Ending Balance</b>',
												localDebitCredit: currentLocalDebitCredit,
												transactionDate: maxDate,
												settlementDate: maxDate
											}));
											formPanel.updateCurrentCashValue(currentLocalDebitCredit);
										}
									});
							});
						}
					},

					getCustomFilters: function() {
						const formPanel = TCG.getParentFormPanel(this);
						const formValues = formPanel.getForm().formValues;
						const params = {
							clientInvestmentAccountId: formValues.clientInvestmentAccount ? formValues.clientInvestmentAccount.id : void 0,
							holdingInvestmentAccountId: formValues.holdingInvestmentAccount ? formValues.holdingInvestmentAccount.id : void 0,
							cashAccountingAccount: true,
							deleted: false
						};

						//Set the date filter range values
						const toolbar = this.getTopToolbar();
						const minDateFilter = TCG.getChildByName(toolbar, 'minDate');
						const maxDateFilter = TCG.getChildByName(toolbar, 'maxDate');
						if (minDateFilter) {
							if (TCG.isBlank(minDateFilter.getValue())) {
								minDateFilter.setValue(this.getDefaultPositionsDate());
							}
							if (TCG.isBlank(maxDateFilter.getValue())) {
								maxDateFilter.setValue(this.getDefaultPositionsDate());
							}
							const lookupDateType = TCG.getChildByName(toolbar, 'lookupDateType');
							if (lookupDateType && TCG.isNotBlank(lookupDateType.getValue())) {
								params['min' + lookupDateType.getValue()] = minDateFilter.getValue().format('m/d/Y');
								params['max' + lookupDateType.getValue()] = maxDateFilter.getValue().format('m/d/Y');
							}
						}

						return params;
					}
				}
			]
		}
	]
});
