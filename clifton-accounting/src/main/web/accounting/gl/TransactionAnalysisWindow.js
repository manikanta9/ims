Clifton.accounting.gl.TransactionAnalysisWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingTransactionAnalysisListWindow',
	title: 'General Ledger Analysis',
	iconCls: 'book-red',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		listeners: {
			// keep selected client account
			beforetabchange: function(tabPanel, newTab, currentTab) {
				if (currentTab) {
					const o = TCG.getChildByName(currentTab.items.get(0).getTopToolbar(), 'clientInvestmentAccountId');
					if (o.getValue()) {
						tabPanel.savedClientAccount = {
							value: o.getValue(),
							text: o.getRawValue()
						};
					}
				}
			},
			tabchange: function(tabPanel, tab) {
				const s = tabPanel.savedClientAccount;
				if (s) {
					const o = TCG.getChildByName(tab.items.get(0).getTopToolbar(), 'clientInvestmentAccountId');
					if (o.getValue() !== s.value) {
						o.setValue(s);
						o.fireEvent('select', o, s);
					}
				}
			}
		},
		items: [
			{
				title: 'General Ledger',
				items: [{
					xtype: 'accounting-transactionGrid',
					instructions: 'General Ledger contains all posted transactions from the beginning of time.',
					reloadOnRender: false,
					includeInstrumentGroupFilter: true,
					includeSecurityGroupFilter: true,
					includeJournalTypeFilter: false,
					includeClientAccountGroupFilter: true,
					getCustomFilters: function(firstLoad) {
						if (firstLoad) {
							// default to last 7 days of transactions
							this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -7)});
						}
					}
				}]
			},


			{
				title: 'Assets',
				items: [{
					xtype: 'accounting-assetsGrid',
					reloadOnRender: false
				}]
			},


			{
				title: 'Income Statement',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingIncomeStatementListFind',
					additionalPropertiesToRequest: 'id|clientInvestmentAccount.id|holdingInvestmentAccount.id|accountingAccount.id|investmentSecurity.id',
					instructions: 'Income statement account balances for the specified date range grouped by client account, holding account, GL account and security.',
					reloadOnRender: false,
					pageSize: 500,
					columns: [
						{header: 'Client Account', width: 125, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, defaultSortColumn: true},
						{header: 'Holding Account', width: 125, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 70, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}},
						{header: 'Security', width: 60, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Quantity', dataIndex: 'quantity', width: 45, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
						{header: 'Local Amount', dataIndex: 'localAmount', type: 'currency', width: 55, negativeInRed: true, positiveInGreen: true},
						{header: 'Base Amount', dataIndex: 'baseAmount', type: 'currency', width: 55, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
						{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.label', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json'}, hidden: true}

					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function(firstLoad) {

						const lp = {readUncommittedRequested: true};

						//Set the date filter values
						lp.transactionStartDate = TCG.getChildByName(this.getTopToolbar(), 'transactionStartDate').getValue().format('m/d/Y');
						lp.transactionDate = TCG.getChildByName(this.getTopToolbar(), 'transactionDate').getValue().format('m/d/Y');

						return lp;
					},
					getTopToolbarFilters: function(toolbar) {
						const today = new Date().format('m/d/Y');
						const start = '01/01/' + today.substring(6);
						return [
							{fieldLabel: 'Client Account', xtype: 'combo', name: 'clientInvestmentAccountId', width: 200, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label', displayField: 'label'},
							{fieldLabel: 'From', xtype: 'toolbar-datefield', name: 'transactionStartDate', value: start},
							{fieldLabel: 'To', xtype: 'toolbar-datefield', name: 'transactionDate', value: today}
						];
					},
					editor: {
						detailPageClass: 'Clifton.accounting.gl.TransactionListWindow',
						drillDownOnly: true,
						getDefaultData: function(gridPanel, row) {
							const filters = [];
							const data = row.json;

							filters.push({
								field: 'clientInvestmentAccountId',
								comparison: 'EQUALS',
								value: data.clientInvestmentAccount.id
							});
							filters.push({
								field: 'holdingInvestmentAccountId',
								comparison: 'EQUALS',
								value: data.holdingInvestmentAccount.id
							});
							filters.push({
								field: 'accountingAccountId',
								comparison: 'EQUALS',
								value: data.accountingAccount.id
							});
							filters.push({
								field: 'investmentSecurityId',
								comparison: 'EQUALS',
								value: data.investmentSecurity.id
							});
							const nextDay = new Date(TCG.getChildByName(gridPanel.getTopToolbar(), 'transactionDate').value).add(Date.DAY, 1).format('m/d/Y');
							filters.push({
								field: 'transactionDate',
								comparison: 'LESS_THAN',
								value: nextDay
							});
							return filters;
						}
					}
				}]
			},


			{
				title: 'Balance Sheet',
				items: [{
					xtype: 'accounting-balanceSheetGrid',
					reloadOnRender: false
				}]
			}
		]
	}]
});
