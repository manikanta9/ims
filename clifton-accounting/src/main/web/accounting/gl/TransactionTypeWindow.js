Clifton.accounting.gl.TransactionTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Transaction Type',
	iconCls: 'accounts',
	closeWindowTrail: true,
	height: 450,
	width: 700,

	items: [{
		xtype: 'formpanel',
		url: 'accountingTransactionType.json',
		labelWidth: 180,
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true, height: 35},
			{
				fieldLabel: 'Parent Type',
				name: 'parent.labelExpanded',
				xtype: 'combo',
				hiddenName: 'parent.id',
				displayField: 'nameExpanded',
				detailPageClass: 'Clifton.accounting.gl.TransactionTypeWindow',
				detailIdField: 'parent.id',
				disableAddNewItem: true,
				loadAll: true,
				url: 'accountingTransactionTypeListFind.json?orderBy=displayOrder#asc',
				qtip: 'The Type that will be used if THIS Type is invalid'
			},
			{fieldLabel: 'Display Order', name: 'displayOrder'},
			{boxLabel: 'Transfer Transaction', name: 'transfer', xtype: 'checkbox'},
			{boxLabel: 'Show "Commission Apply Method" in Commission Definition UI', name: 'commissionApplyMethodApplicable', xtype: 'checkbox', qtip: 'Controls whether or not to display the Commission Apply Method field for this particular type.'},
			{boxLabel: 'Restrict this Transaction Type to OTC Investment Hierarchy', name: 'restrictToOtcHierarchy', xtype: 'checkbox', qtip: 'If checked, this value will ONLY appear in the Type list when the Instrument or Hierarchy is OTC traded.'},
			{
				fieldLabel: 'Additional Scope System Table',
				name: 'additionalScopeSystemTable.name',
				xtype: 'combo',
				hiddenName: 'additionalScopeSystemTable.id',
				detailPageClass: 'Clifton.system.schema.TableWindow',
				url: 'systemTableListFind.json?defaultDataSource=true',
				disableAddNewItem: true,
				loadAll: true,
				qtip: 'Table that additional scope values can come from.'
			},
			{fieldLabel: 'Additional Scope Field Label', name: 'additionalScopeUserInterfaceLabel', qtip: 'The label that will be applied to the additional scope field. If not set the name of the System Table will be used.'},
			{fieldLabel: 'Additional Scope Bean ID Path', name: 'additionalScopeBeanIdPath', qtip: 'The path to the ID of the additional scope object.'}
		]
	}]
});
