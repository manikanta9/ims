Clifton.accounting.gl.TransactionListWindow = Ext.extend(TCG.app.Window, {
	title: 'GL Drill Down',
	iconCls: 'book-red',
	width: 1400,
	closeWindowTrail: true,

	items: [{
		xtype: 'accounting-transactionGrid',
		instructions: 'The following GL transactions make up the drill down row.',
		includeClientAccountFilter: false,
		getCustomFilters: function(firstLoad) {
			const data = this.getWindow().defaultData;
			if (data) {
				return {
					readUncommittedRequested: true,
					restrictionList: Ext.util.JSON.encode(data)
				};
			}
			return false;
		},
		plugins: [
			{ptype: 'investment-contextmenu-plugin', enableAsyncLookups: true},
			{ptype: 'accounting-transaction-grideditor'},
			{ptype: 'gridsummary'}
		]
	}]
});
