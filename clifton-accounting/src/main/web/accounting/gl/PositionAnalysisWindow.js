Clifton.accounting.gl.PositionAnalysisWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingPositionAnalysisListWindow',
	title: 'Client Positions',
	iconCls: 'chart-pie',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		listeners: {
			// keep selected client account and group
			beforetabchange: function(tabPanel, newTab, currentTab) {
				if (currentTab) {
					const tb = currentTab.items.get(0).getTopToolbar();
					if (tb) {
						let o = TCG.getChildByName(tb, 'clientInvestmentAccountId');
						if (o && o.getValue()) {
							tabPanel.savedClientAccount = {
								value: o.getValue(),
								text: o.getRawValue()
							};
						}
						else {
							tabPanel.savedClientAccount = null;
						}
						o = TCG.getChildByName(tb, 'clientInvestmentAccountGroupId');
						if (o && o.getValue()) {
							tabPanel.savedClientAccountGroup = {
								value: o.getValue(),
								text: o.getRawValue()
							};
						}
						else {
							tabPanel.savedClientAccountGroup = null;
						}
					}
				}
			},
			tabchange: function(tabPanel, tab) {
				const tb = tab.items.get(0).getTopToolbar();
				if (tb) {
					let o = TCG.getChildByName(tb, 'clientInvestmentAccountId');
					let s = tabPanel.savedClientAccount;
					if (o) {
						if (s) {
							if (o.getValue() !== s.value) {
								o.setValue(s);
								o.fireEvent('select', o, s);
							}
						}
						else {
							o.reset();
						}
					}
					o = TCG.getChildByName(tb, 'clientInvestmentAccountGroupId');
					s = tabPanel.savedClientAccountGroup;
					if (s) {
						if (o.getValue() !== s.value) {
							o.setValue(s);
							o.fireEvent('select', o, s);
						}
					}
					else {
						o.reset();
					}
				}
			}
		},
		items: [
			{
				title: 'Daily Positions',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingPositionDailyListFind',
					instructions: 'Daily lot-level positions are built from the General Ledger and market data. They may get stale and may need to be rebuilt in order to see accurate data when GL or market data changes.',
					pageSize: 500,
					reloadOnRender: false,
					appendStandardColumns: false,
					forceFit: false,
					viewNames: ['Open Positions', 'Realized and Commission', 'Export Friendly'],
					columns: [
						{header: 'Transaction ID', dataIndex: 'accountingTransaction.id', width: 100, type: 'int', filter: {searchFieldName: 'accountingTransactionId'}, hidden: true},
						{header: 'Client', dataIndex: 'accountingTransaction.clientInvestmentAccount.businessClient.name', hidden: true, width: 210, filter: {type: 'combo', searchFieldName: 'clientId', displayField: 'label', url: 'businessClientListFind.json'}},
						{header: 'Account #', dataIndex: 'accountingTransaction.clientInvestmentAccount.number', width: 65, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Client Account', dataIndex: 'accountingTransaction.clientInvestmentAccount.name', width: 210, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Issuing Company', width: 130, dataIndex: 'accountingTransaction.clientInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'clientAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true&ourCompany=true'}, hidden: true},
						{header: 'Holding Account', dataIndex: 'accountingTransaction.holdingInvestmentAccount.number', width: 100, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Holding Company', width: 130, dataIndex: 'accountingTransaction.holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'GL Account', dataIndex: 'accountingTransaction.accountingAccount.name', width: 90, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}, hidden: true},
						{header: 'Primary Exchange', dataIndex: 'accountingTransaction.investmentSecurity.instrument.exchange.exchangeCode', hidden: true, width: 80, filter: {type: 'combo', searchFieldName: 'exchangeId', displayField: 'label', url: 'investmentExchangeListFind.json'}},
						{header: 'Composite Exchange', dataIndex: 'accountingTransaction.investmentSecurity.instrument.compositeExchange.exchangeCode', hidden: true, width: 80, filter: {type: 'combo', searchFieldName: 'compositeExchangeId', displayField: 'label', url: 'investmentExchangeListFind.json'}},
						{header: 'Investment Type', dataIndex: 'accountingTransaction.investmentSecurity.instrument.hierarchy.investmentType.name', hidden: true, width: 80, filter: {type: 'combo', searchFieldName: 'investmentTypeId', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Sub Type', dataIndex: 'accountingTransaction.investmentSecurity.instrument.hierarchy.investmentTypeSubType.name', hidden: true, width: 80, filter: {type: 'combo', searchFieldName: 'investmentTypeSubTypeId', url: 'investmentTypeSubTypeListFind.json'}},
						{header: 'Sub Type 2', dataIndex: 'accountingTransaction.investmentSecurity.instrument.hierarchy.investmentTypeSubType2.name', hidden: true, width: 80, filter: {type: 'combo', searchFieldName: 'investmentTypeSubType2Id', url: 'investmentTypeSubType2ListFind.json'}},
						{header: 'Hierarchy', dataIndex: 'accountingTransaction.investmentSecurity.instrument.hierarchy.nameExpanded', hidden: true, width: 80, filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 600}},
						{header: 'Security', dataIndex: 'accountingTransaction.investmentSecurity.symbol', width: 80, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', dataIndex: 'accountingTransaction.investmentSecurity.name', width: 120, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'CCY Denom', dataIndex: 'accountingTransaction.investmentSecurity.instrument.tradingCurrency.symbol', width: 80, filter: {type: 'combo', searchFieldName: 'investmentInstrumentCurrencyDenominationId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}, hidden: true},
						{header: 'Country of Risk', width: 80, dataIndex: 'accountingTransaction.investmentSecurity.instrument.countryOfRisk.value', filter: {type: 'combo', searchFieldName: 'countryOfRiskId', displayField: 'labelLong', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'}, hidden: true},
						{header: 'Country Of Incorporation', width: 80, dataIndex: 'accountingTransaction.investmentSecurity.instrument.countryOfIncorporation.value', filter: {type: 'combo', searchFieldName: 'countryOfIncorporationId', displayField: 'labelLong', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'}, hidden: true},
						{header: 'Price Multiplier', dataIndex: 'accountingTransaction.investmentSecurity.priceMultiplier', type: 'float', useNull: true, width: 100, hidden: true, viewNames: ['Export Friendly'], filter: {searchFieldName: 'priceMultiplier'}},
						{header: 'Security End Date', dataIndex: 'accountingTransaction.investmentSecurity.endDate', width: 100, hidden: true, filter: {searchFieldName: 'securityEndDate'}},
						{header: 'Underlying Hierarchy', dataIndex: 'accountingTransaction.investmentSecurity.underlyingSecurity.instrument.hierarchy.nameExpanded', hidden: true, width: 80, filter: {type: 'combo', searchFieldName: 'underlyingHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 600}},
						{header: 'Underlying Symbol', dataIndex: 'accountingTransaction.investmentSecurity.underlyingSecurity.symbol', hidden: true, width: 100, filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Initial Margin per Qty', dataIndex: 'accountingTransaction.initialMarginPerUnit', width: 130, type: 'currency', useNull: true, sortable: false, hidden: true, viewNames: ['Export Friendly'], tooltip: 'Actual Initial Margin Requirement for one Unit of Quantity. The calculation takes into account whether the holding account is sued for hedging or speculation as well as account specific Initial Margin Multiplier.'},
						{header: 'Unadjusted Qty', dataIndex: 'unadjustedQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 100, hidden: true, viewNames: ['Export Friendly'], tooltip: 'Opening Quantity (Original Notional or Face) for securities with Factor Changes and Remaining Quantity for those without.'},
						{header: 'Remaining Qty', dataIndex: 'remainingQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 100, viewNames: ['Open Positions', 'Export Friendly']},
						{header: 'Open Qty', dataIndex: 'accountingTransaction.quantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 95, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Market Price', dataIndex: 'marketPrice', type: 'float', useNull: true, negativeInRed: true, width: 90},
						{header: 'Prior Price', dataIndex: 'priorPrice', type: 'float', useNull: true, negativeInRed: true, width: 90, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Open Price', dataIndex: 'accountingTransaction.price', type: 'float', useNull: true, negativeInRed: true, width: 90, hidden: true, viewNames: ['Export Friendly']},
						{header: 'FX Rate', dataIndex: 'marketFxRate', type: 'float', width: 80},
						{header: 'Local Cost Basis', dataIndex: 'remainingCostBasisLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Base Cost Basis', dataIndex: 'remainingCostBasisBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true, summaryType: 'sum'},
						{header: 'Local OTE', dataIndex: 'openTradeEquityLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Base OTE', dataIndex: 'openTradeEquityBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', viewNames: ['Open Positions']},
						{header: 'Prior Local OTE', dataIndex: 'priorOpenTradeEquityLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Prior Base OTE', dataIndex: 'priorOpenTradeEquityBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true},
						{header: 'Local Notional', dataIndex: 'notionalValueLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, viewNames: ['Open Positions']},
						{header: 'Base Notional', dataIndex: 'notionalValueBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', viewNames: ['Open Positions']},
						{header: 'Today Closed Qty', dataIndex: 'todayClosedQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 110, hidden: true, viewNames: ['Realized and Commission', 'Export Friendly']},
						{header: 'Local Realized', dataIndex: 'todayRealizedGainLossLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true, viewNames: ['Realized and Commission']},
						{header: 'Base Realized', dataIndex: 'todayRealizedGainLossBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true, viewNames: ['Realized and Commission', 'Export Friendly']},
						{header: 'Local Commission', dataIndex: 'todayCommissionLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 120, hidden: true, viewNames: ['Realized and Commission']},
						{header: 'Base Commission', dataIndex: 'todayCommissionBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 120, summaryType: 'sum', hidden: true, viewNames: ['Realized and Commission', 'Export Friendly']},
						{header: 'Local M2M', dataIndex: 'markAmountLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Base M2M', dataIndex: 'markAmountBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true, viewNames: ['Export Friendly']},
						{header: 'Open Date', dataIndex: 'accountingTransaction.transactionDate', width: 90, hidden: true, viewNames: ['Export Friendly'], filter: {searchFieldName: 'transactionDate'}},
						{header: 'Open Settlement Date', dataIndex: 'accountingTransaction.settlementDate', width: 90, hidden: true, filter: {searchFieldName: 'settlementDate'}},

						{header: 'Prior Local Accrual', dataIndex: 'priorAccrualLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Prior Base Accrual', dataIndex: 'priorAccrualBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
						{header: 'Local Accrual', dataIndex: 'accrualLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base Accrual', dataIndex: 'accrualBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},

						{header: 'Prior Local Market Value', dataIndex: 'priorMarketValueLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Prior Base Market Value', dataIndex: 'priorMarketValueBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', viewNames: ['Cleared OTC']},
						{header: 'Local Market Value', dataIndex: 'marketValueLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true},
						{header: 'Base Market Value', dataIndex: 'marketValueBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', viewNames: ['Cleared OTC']},

						{header: 'Local Daily Gain/Loss', dataIndex: 'dailyGainLossLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
						{header: 'Base Daily Gain/Loss', dataIndex: 'dailyGainLossBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true, viewNames: ['Export Friendly']}


					],
					plugins: {ptype: 'gridsummary'},
					editor: {
						detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.accountingTransaction.id;
						}
					},
					getLoadParams: function(first) {
						const tb = this.getTopToolbar();
						const lp = {readUncommittedRequested: true};
						lp.positionDate = TCG.getChildByName(tb, 'positionDate').getValue().format('m/d/Y');
						let v = TCG.getChildByName(tb, 'investmentGroupId').getValue();
						if (v) {
							lp.investmentGroupId = v;
						}
						v = TCG.getChildByName(tb, 'clientInvestmentAccountGroupId').getValue();
						if (v) {
							lp.clientInvestmentAccountGroupId = v;
						}
						v = TCG.getChildByName(tb, 'holdingInvestmentAccountGroupId').getValue();
						if (v) {
							lp.holdingInvestmentAccountGroupId = v;
						}
						return lp;
					},
					getTopToolbarFilters: function(toolbar) {
						const filters = [];
						filters.push({fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'});
						filters.push({fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'});
						filters.push({fieldLabel: 'Holding Acct Group', xtype: 'toolbar-combo', name: 'holdingInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'});
						filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'positionDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')});
						return filters;
					}
				}]
			},


			{
				title: 'Positions',
				items: [{
					xtype: 'accounting-assetsGrid',
					instructions: 'Position GL account balances on the specified date grouped by holding account, GL account and security.',
					reloadOnRender: false,
					includeInvestmentGroupFilter: true,
					skipAccountFiltersAll: false,
					skipAccountFilter: true,
					includeHoldingAccountGroupFilter: true,

					getCustomFilters: function() {
						return {positionAccountingAccount: true};
					}
				}]
			},


			{
				title: 'Currency Balances',
				items: [{
					xtype: 'accounting-assetsGrid',
					name: 'accountingBalanceValueListFind',
					instructions: 'Retrieves live Currency GL account balances and Exchange Rates on the specified date grouped by holding account, GL account and security.',
					reloadOnRender: false,

					getCustomFilters: function() {
//						return {accountingAccountIdName: 'CURRENCY_NON_CASH_ACCOUNTS'}; // for some reason this condition more than doubles the time of the next one
						return {accountingAccountType: 'Asset', currencyAccountingAccount: true, cashAccountingAccount: false};
					},

					columns: [
						{header: 'Client Account', width: 125, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', width: 125, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.label', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json'}, hidden: true},
						{header: 'Team', dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', width: 50, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'teamSecurityGroupList.json'}},
						{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 50, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}},
						{header: 'Security', width: 50, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Local Amount', dataIndex: 'localNotional', type: 'currency', width: 55, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true},
						{header: 'FX Rate', dataIndex: 'exchangeRateToBase', width: 55, filter: false, sortable: false, type: 'currency', numberFormat: '0,000.000000000000'},
						{header: 'Base Amount', dataIndex: 'baseNotional', type: 'currency', width: 55, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'}
					],

					getTopToolbarFilters: function(toolbar) {
						const grid = this;
						return [
							{fieldLabel: 'Client Account', xtype: 'combo', name: 'clientInvestmentAccountId', displayField: 'label', linkedFilter: 'clientInvestmentAccount.label', width: 150, url: 'investmentAccountListFind.json?ourAccount=true'},
							{fieldLabel: 'Client Account Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'},
							{
								name: 'lookupDateType', value: 'transactionDate', width: 110, minListWidth: 110, displayField: 'name', valueField: 'value', mode: 'local', xtype: 'toolbar-combo',
								store: {
									xtype: 'arraystore',
									fields: ['value', 'name', 'description'],
									data: [
										['transactionDate', 'Transaction Date', 'Aggregate General Ledger transactions using Transaction Date up to and including this date'],
										['settlementDate', 'Settlement Date', 'Aggregate General Ledger transactions using Settlement Date up to and including this date']
									]
								}
							},
							{xtype: 'toolbar-datefield', name: 'lookupDate', value: grid.getDefaultPositionsDate()}
						];
					}

				}]
			},


			{
				title: 'Cash Balances',
				items: [{
					xtype: 'accounting-assetsGrid',
					instructions: 'Cash balances on the specified date grouped by holding account, GL account and security.',
					reloadOnRender: false,

					getCustomFilters: function() {
						return {cashAccountingAccount: true};
					}
				}]
			},


			{
				title: 'Collateral Balances',
				items: [{
					xtype: 'accounting-assetsGrid',
					instructions: 'Collateral GL account balances on the specified date grouped by holding account, GL account and security.',
					reloadOnRender: false,

					getCustomFilters: function() {
						return {collateralAccountingAccount: true};
					}
				}]
			},


			{
				title: 'Administration',
				items: [Clifton.accounting.DailyPositionsRebuildForm]
			}
		]
	}]
});
