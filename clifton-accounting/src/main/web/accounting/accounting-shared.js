Ext.ns('Clifton.accounting', 'Clifton.investment.calendar.calculator', 'Clifton.accounting.account', 'Clifton.accounting.booking', 'Clifton.accounting.commission', 'Clifton.accounting.gl', 'Clifton.accounting.journal', 'Clifton.accounting.eventjournal', 'Clifton.accounting.m2m', 'Clifton.accounting.period', 'Clifton.accounting.positiontransfer', 'Clifton.accounting.positiontransfer.upload', 'Clifton.accounting.simplejournal', 'Clifton.accounting.simplejournal.upload', 'Clifton.accounting.taskjournal', 'Clifton.accounting.positiontransfer.collateral', 'Clifton.accounting.position', 'Clifton.accounting.position.calculator');

/*
 * Constants
 */

Clifton.system.schema.SimpleUploadWindows['AccountingPositionTransfer'] = 'Clifton.accounting.positiontransfer.upload.PositionTransferUploadWindow';

Clifton.investment.instrument.InstrumentStandardAdditionalTabs.push(
	{
		title: 'Open Positions',
		items: [{
			xtype: 'accounting-securityLiveHoldingsGrid',
			instructions: 'Live position balances for selected instrument on the specified date.',
			groupField: 'investmentSecurity.symbol',
			hiddenColumns: ['holdingInvestmentAccount.label', 'price', 'exchangeRateToBase', 'localCostBasis', 'baseNotional', 'baseMarketValue', 'holdingInvestmentAccount.issuingCompany.label', 'accountingAccount.name'],
			getCustomFilters: function() {
				return {positionAccountingAccount: true, investmentInstrumentId: this.getWindow().getMainFormId()};
			}
		}]
	},
	{
		title: 'Transactions',
		items: [{
			xtype: 'accounting-transactionGrid',
			instructions: 'The following GL transaction have been posted to the General Ledger for selected Investment Instrument.',
			includeClientAccountFilter: false,
			columnOverrides: [
				{dataIndex: 'id', hidden: true},
				{dataIndex: 'parentTransaction.id', hidden: true},
				{dataIndex: 'holdingInvestmentAccount.label', hidden: true}
			],
			getCustomFilters: function(firstLoad) {
				if (firstLoad) {
					this.setFilterValue('transactionDate', {'after': new Date().add(Date.MONTH, -1)});
				}
				return {
					investmentInstrumentId: this.getWindow().getMainFormId()
				};
			}
		}]
	}
);

Clifton.investment.instrument.SecurityAdditionalAccountingTabs = [
	{
		title: 'Open Positions',
		items: [{
			xtype: 'accounting-securityLiveHoldingsGrid',
			skipAccountFiltersAll: false,
			skipAccountFilter: true
		}]
	},
	{
		title: 'Transactions',
		items: [{
			xtype: 'accounting-transactionGrid',
			instructions: 'The following GL transaction have been posted to the General Ledger for selected Investment Security.',
			includeClientAccountFilter: false,
			columnOverrides: [
				{dataIndex: 'id', hidden: true},
				{dataIndex: 'parentTransaction.id', hidden: true},
				{dataIndex: 'holdingInvestmentAccount.label', hidden: true}
			],
			getCustomFilters: function(firstLoad) {
				if (firstLoad) {
					this.setFilterValue('transactionDate', {'after': new Date().add(Date.MONTH, -3)});
				}
				return {
					investmentSecurityId: this.getWindow().getMainFormId()
				};
			}
		}]
	}
];
Clifton.investment.instrument.SecurityAdditionalTabsByType['DEFAULT'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['DEFAULT'], ...Clifton.investment.instrument.SecurityAdditionalAccountingTabs);
Clifton.investment.instrument.SecurityAdditionalTabsByType['Bond'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Bond'], ...Clifton.investment.instrument.SecurityAdditionalAccountingTabs);
Clifton.investment.instrument.SecurityAdditionalTabsByType['Currency'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Currency'], {
	title: 'Holdings',
	items: [{
		xtype: 'accounting-securityLiveHoldingsGrid',
		instructions: 'Live currency balances from the General Ledger for selected currency on the specified date.',
		columnOverrides: [{dataIndex: 'localNotional', summaryType: 'sum', hidden: false}, {dataIndex: 'localMarketValue', summaryType: 'sum'}],
		hiddenColumns: ['investmentSecurity.symbol', 'quantity', 'price', 'exchangeRateToBase', 'localCostBasis', 'baseMarketValue', 'holdingInvestmentAccount.issuingCompany.label'],
		viewNames: undefined,
		limitToPositions: false,
		skipAccountFiltersAll: false,
		skipAccountFilter: true,
		getCustomFilters: function() {
			return {currencyAccountingAccount: true, investmentSecurityId: this.getWindow().getMainFormId()};
		}
	}]
});
Clifton.investment.instrument.SecurityAdditionalTabsByType['OTC'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['OTC'], ...Clifton.investment.instrument.SecurityAdditionalAccountingTabs);
Clifton.investment.instrument.SecurityAdditionalTabsByType['Proprietary'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Proprietary'], ...Clifton.investment.instrument.SecurityAdditionalAccountingTabs);
Clifton.investment.instrument.SecurityAdditionalTabsByType['Stock'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Stock'], ...Clifton.investment.instrument.SecurityAdditionalAccountingTabs);
Clifton.investment.instrument.SecurityAdditionalTabsByType['Swap'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Swap'], ...Clifton.investment.instrument.SecurityAdditionalAccountingTabs);

Clifton.investment.account.securitytarget.SecurityTargetToolbarItems.push(
	{
		text: 'Transfer',
		tooltip: 'Add a Contribution or Distribution applicable to this Security Target',
		iconCls: 'add',
		menu: {
			items: [
				{
					text: 'Security Target Contribution', iconCls: 'arrow-left-green', className: 'Clifton.accounting.positiontransfer.TransferToWindow',
					tooltip: 'Create a contribution to our account by defining the positions being transfer into the system',
					handler: async function() {
						const formValues = TCG.getParentFormPanel(this).getForm().formValues;
						if (!formValues.id) {
							TCG.showInfo('Please save the security target before we can contribute to the account.', 'Unable to Transfer');
							return;
						}
						const today = new Date();
						const [transferType, clientHoldingAccounts] = await Promise.all([
							TCG.data.getDataPromiseUsingCaching('accountingPositionTransferTypeByName.json?name=Security Target Contribution', this, 'accounting.positiontransfer.type.Security Target Contribution'),
							TCG.data.getDataPromise('investmentAccountListFind.json?ourAccount=false', this, {
								params: {
									mainAccountId: formValues.clientInvestmentAccount.id,
									mainPurposeActiveOnDate: today.format('m/d/Y'),
									requestedPropertiesRoot: 'data',
									requestedProperties: 'name|label|id'
								}
							})
						]);
						TCG.createComponent(this.className, {
							defaultData: {
								type: transferType,
								settlementDate: today.format('Y-m-d 00:00:00'),
								transactionDate: today.format('Y-m-d 00:00:00'),
								sourceSystemTable: transferType.sourceSystemTable,
								sourceFkFieldId: formValues.id,
								toClientInvestmentAccount: formValues.clientInvestmentAccount,
								toHoldingInvestmentAccount: clientHoldingAccounts && clientHoldingAccounts.length === 1 ? clientHoldingAccounts[0] : void 0
							}
						});
					}
				},
				{
					text: 'Security Target Distribution', iconCls: 'arrow-right-red', className: 'Clifton.accounting.positiontransfer.TransferFromWindow',
					tooltip: 'Create a distribution from our account by selecting lot positions',
					handler: async function() {
						const formValues = TCG.getParentFormPanel(this).getForm().formValues;
						if (!formValues.id) {
							TCG.showInfo('Please save the security target before we can distribute from the account.', 'Unable to Transfer');
							return;
						}
						const today = new Date();
						const [transferType, clientHoldingAccounts] = await Promise.all([
							TCG.data.getDataPromiseUsingCaching('accountingPositionTransferTypeByName.json?name=Security Target Distribution', this, 'accounting.positiontransfer.type.Security Target Distribution'),
							TCG.data.getDataPromise('investmentAccountListFind.json?ourAccount=false', this, {
								params: {
									mainAccountId: formValues.clientInvestmentAccount.id,
									mainPurposeActiveOnDate: today.format('m/d/Y'),
									requestedPropertiesRoot: 'data',
									requestedProperties: 'name|label|id'
								}
							})
						]);

						TCG.createComponent(this.className, {
							defaultData: {
								type: transferType,
								settlementDate: today.format('Y-m-d 00:00:00'),
								transactionDate: today.format('Y-m-d 00:00:00'),
								sourceSystemTable: transferType.sourceSystemTable,
								sourceFkFieldId: formValues.id,
								fromClientInvestmentAccount: formValues.clientInvestmentAccount,
								fromHoldingInvestmentAccount: clientHoldingAccounts && clientHoldingAccounts.length === 1 ? clientHoldingAccounts[0] : void 0
							}
						});
					}
				}
			]
		}
	}, '-');


Clifton.investment.calendar.calculator.PositionSecurityOptions = [
	['POSITION_SECURITY', 'Position Security', 'Select the security for which the position is held.'],
	['POSITION_SECURITY_UNDERLYING', 'Position Security Underlying', 'Select the underlying security of the security for which the position is held.'],
	['POSITION_SECURITY_CCY', 'Position Security Base CCY', 'Select the trading currency of the security\'s instrument']
];

Clifton.investment.calendar.calculator.SecurityCalendarOptions = [
	['SETTLEMENT_CALENDAR', 'Settlement Calendar', 'Select the settlement calendar for the security\'s InvestmentInstrument'],
	['EXCHANGE_CALENDAR', 'Exchange Calendar', 'Select the calendar of the InvestmentExchange for the security\'s InvestmentInstrument'],
	['COALESCE', 'Coalesce Settlement and Exchange Calendars', 'Coalesce the above options, with exchange calendar taking precedence.']
];

Clifton.accounting.commission.CommissionTypes = {
	'SIMPLE': {
		displayName: 'Simple',
		optionInfo: ['Simple', 'SIMPLE', 'A flat commission/fee'],
		hasRanges: false,
		showCommissionAmount: true
	},
	'TIERED_NOTIONAL': {
		displayName: 'Tiered - Notional',
		optionInfo: ['Tiered - Notional', 'TIERED_NOTIONAL', 'A commission/fee that is tiered based on transaction maturity. Generally only used with the Clearing GL Account. Ranges are manually specified.'],
		showCommissionAmount: false,
		hasRanges: true,
		rangeColumnName: 'Months to Maturity',
		validOption: function(hierarchyIsOTC) {
			return !hierarchyIsOTC;
		}
	},
	'TIERED_PRICE': {
		displayName: 'Tiered - Price',
		optionInfo: ['Tiered - Price', 'TIERED_PRICE', 'A commission/fee that is tiered based on the price of the security traded.'],
		showCommissionAmount: false,
		hasRanges: true,
		rangeColumnName: 'Security Price',
		validOption: function(hierarchyIsOTC) {
			return !hierarchyIsOTC;
		}
	},
	'TIERED_MATURITY': {
		displayName: 'Tiered - Transaction Maturity',
		optionInfo: ['Tiered - Transaction Maturity', 'TIERED_MATURITY', 'A commission/fee that is tiered based on transaction maturity. Generally only used with the Clearing GL Account. Ranges are manually specified.'],
		showCommissionAmount: false,
		hasRanges: true,
		rangeColumnName: 'Months to Maturity',
		validOption: function(hierarchyIsOTC) {
			return !hierarchyIsOTC;
		}
	},
	'TIERED_AMORTIZATION': {
		displayName: 'Tiered - Amortization',
		optionInfo: ['Tiered - Amortization', 'TIERED_AMORTIZATION', 'A commission/fee that is amortized over time.'],
		showCommissionAmount: false,
		hasRanges: true,
		rangeColumnName: 'Days Since Position Open',
		validOption: function(hierarchyIsOTC) {
			return !hierarchyIsOTC;
		}
	},
	'TIERED_AMORTIZED_STRAIGHT_LINE': {
		displayName: 'Tiered - Amortized - Straight Line',
		optionInfo: ['Tiered - Amortized - Straight Line', 'TIERED_AMORTIZED_STRAIGHT_LINE',
			'A commission/fee that is amortized over time. Should be used with the Executing GL Account. Commission amount is calculated using the formula: <br><br>' +
			'<b>Commission = (Maturity Date - Trade Close Date) / (Maturity Date - Trade Open Date) * (Calculated Commission Amount)</b><br><br>' +
			'In the above formula, <i>(Calculated Commission Amount)</i> is the amount based on the <b>Calculation Method</b>, which will typically be <b>Per Notional</b> or <b>Per Notional (BPS)</b>.<br><br>' +
			'Note: This option is only available for securities that are OTC traded.'],
		hasRanges: false,
		showCommissionAmount: true,
		validOption: function(hierarchyIsOTC) {
			return hierarchyIsOTC;
		}
	}
};

Clifton.accounting.commission.CommissionApplyMethods = {
	'ALWAYS': {
		displayName: 'Always'
	},
	'HALF_TURN': {
		displayName: 'Half-Turn (Open and Close)'
	},
	'ROUND_TURN': {
		displayName: 'Round-Turn (Double on Close)'
	},
	'ROUND_TURN_ON_SELL': {
		displayName: 'Round-Turn On Sell (Double on Closing Sell)'
	},
	'ONLY_ON_OPEN': {
		displayName: 'Only On Open'
	},
	'ONLY_ON_CLOSE': {
		displayName: 'Only On Close'
	},
	'ONLY_ON_SELL': {
		displayName: 'Only On Sell'
	},
	'ONLY_ON_CLOSE_WITH_PRICE_GREATER_THAN_ZERO': {
		displayName: 'Only On Sell When Price Greater Than Zero'
	},
	'ONLY_ON_CLOSE_WITH_PRICE_EQUAL_TO_ZERO': {
		displayName: 'Only On Sell When Price Equals Zero'
	}
};

Clifton.accounting.commission.CommissionCalculationMethods = {
	'FLAT_AMOUNT': {
		displayName: 'Flat Amount',
		commissionCalculationMethod: 'FLAT_AMOUNT',
		description: 'Apply flat amount to whole transaction (Total Fee = Fee Amount)'
	},
	'PER_UNIT': {
		displayName: 'Per Unit',
		commissionCalculationMethod: 'PER_UNIT',
		description: 'Apply commission amount per unit of quantity (Total Fee = Quantity * Fee Amount)'
	},
	'PER_UNIT_BPS': {
		displayName: 'Per Unit (BPS)',
		commissionCalculationMethod: 'PER_UNIT_BPS',
		description: 'Apply commission amount as basis points per unit of quantity (Total Fee = Quantity * Fee Amount / 10,000)'
	},
	'PER_NOTIONAL': {
		displayName: 'Per Notional',
		commissionCalculationMethod: 'PER_NOTIONAL',
		description: 'Apply commission percentage amount to accounting notional value of transaction (Total Fee = Accounting Notional * Fee Amount)'
	},
	'PER_NOTIONAL_BPS': {
		displayName: 'Per Notional (BPS)',
		commissionCalculationMethod: 'PER_NOTIONAL_BPS',
		description: 'Apply commission percentage amount as basis points to accounting notional value of transaction (Total Fee = Accounting Notional * Fee Amount / 10,000)'
	},
	'PER_MILLION_QUANTITY': {
		displayName: 'Per Million Quantity',
		commissionCalculationMethod: 'PER_MILLION_QUANTITY',
		description: 'Apply commission amount to each million of quantity value of transaction (Total Fee = quantity * Fee Amount / 1,000,000). Some securities, such as IRS, use a face notional amount as quantity in IMS.'
	}
};

Clifton.accounting.position.calculator.NOTIONAL_PRICE_FIELD = [
	['STANDARD', 'Standard', 'Use the standard notional value of the position as its notional value.'],
	['STRIKE', 'Strike', 'Use the strike price of the position as its notional value.'],
	['UNDERLYING', 'Underlying', 'Use the notional value of the position\'s underlying security as its notional value.']
];

Clifton.accounting.position.POSITION_TYPES = [
	['ANY_POSITION', 'All', 'Include all position types.'],
	['COLLATERAL_POSITION', 'Collateral', 'Include collateral positions only.'],
	['NON_COLLATERAL_POSITION', 'Non-Collateral', 'Include non-collateral positions only.']
];

/*
 * Classes
 */

Clifton.accounting.getFilterListFromType = function(type) {
	const list = [];

	for (const typeName in type) {
		if (type.hasOwnProperty(typeName)) {
			list.push([typeName, type[typeName].displayName]);
		}
	}

	return list;
};

Clifton.accounting.eventjournal.EventJournalGeneratorForm = {
	xtype: 'formpanel',
	items: [{
		xtype: 'fieldset',
		title: 'Event Journal Generation',
		instructions: 'Find all security events using the specified filters and generate corresponding event journal(s) with a detail for each open position lot affected by this event.',
		items: [
			{fieldLabel: 'Event Date', name: 'eventDate', xtype: 'datefield'},
			{fieldLabel: 'Event Type', hiddenName: 'eventTypeId', xtype: 'combo', url: 'investmentSecurityEventTypeListFind.json', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
			{fieldLabel: 'Investment Group', hiddenName: 'investmentGroupId', xtype: 'combo', url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow'},
			{fieldLabel: 'Security', hiddenName: 'securityId', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{
				fieldLabel: 'Security Event', hiddenName: 'eventId', xtype: 'combo', displayField: 'label', url: 'investmentSecurityEventListFind.json', detailPageClass: 'Clifton.investment.instrument.event.EventWindow',
				requiredFields: ['eventTypeId', 'securityId'],
				beforequery: function(queryEvent) {
					const cmb = queryEvent.combo;
					const f = TCG.getParentFormPanel(cmb).getForm();
					cmb.store.baseParams = {
						typeId: f.findField('eventTypeId').getValue(),
						securityId: f.findField('securityId').getValue()
					};
				}
			},
			{fieldLabel: 'Client Account', hiddenName: 'clientAccountId', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{name: 'postEvent', xtype: 'checkbox', boxLabel: 'Immediately post generated journal(s) to the General Ledger'},
			{name: 'skipJournalsWithExDateAfterTomorrow', xtype: 'checkbox', checked: true, boxLabel: 'Do not generate journals for events with Ex Date after tomorrow'},
			{name: 'strictFilters', xtype: 'checkbox', checked: true, boxLabel: 'Require "Event Date", "Event Type", and "Investment Group" filters (if unchecked, make sure to limit event generation to only those that you need)'}
		],
		buttons: [{
			text: 'Generate Event Journal(s)',
			width: 165,
			xtype: 'button',
			handler: function() {
				const f = TCG.getParentFormPanel(this);
				const params = {
					generatorType: (f.getFormValue('postEvent') === true) ? 'POST' : 'GENERATE',
					skipJournalsWithExDateAfterTomorrow: (f.getFormValue('skipJournalsWithExDateAfterTomorrow') === true)
				};
				let v = f.getFormValue('eventId');
				if (v) {
					params.eventId = v;
				}
				else {
					v = f.getForm().findField('eventDate').value;
					if (v) {
						params.eventDate = v;
					}
					else {
						TCG.showError('Event Date or Event selection is required.', 'Missing Filters');
						return;
					}
					v = f.getFormValue('eventTypeId');
					if (v) {
						params.eventTypeId = v;
					}
					v = f.getFormValue('securityId');
					if (v) {
						params.securityId = v;
					}
					v = f.getFormValue('investmentGroupId');
					if (v) {
						params.investmentGroupId = v;
					}
					v = f.getFormValue('clientAccountId');
					if (v) {
						params.clientAccountId = v;
					}
				}

				if (f.getFormValue('strictFilters') === true) {
					if (!(params.eventDate && params.eventTypeId && params.investmentGroupId)) {
						TCG.showError('"Event Date", "Event Type", and "Investment Group" filters are required in most cases. For advanced event generation, make sure that your filters limit event generation to only those that you need and uncheck corresponding safety checkbox.', 'Required Filters');
						return;
					}
				}

				Ext.Msg.confirm('Generate Journals', 'Would you like to generate event journals using selected filters?', function(a) {
					if (a === 'yes') {
						const loader = new TCG.data.JsonLoader({
							waitTarget: f,
							waitMsg: 'Generating...',
							root: 'result',
							params: params,
							onLoad: function(record, conf) {
								TCG.showInfo('Created ' + record + ' event journals', 'Event Journal Generation');
							}
						});
						loader.load('accountingEventJournalListGenerate.json');
					}
				});
			}
		}]
	}]
};


Clifton.accounting.eventjournal.downloadEventJournalDetailReport = function(detailId, format, componentScope) {
	let url = 'accountingEventJournalDetailReportDownload.json?detailId=' + detailId;
	if (format) {
		url += '&format=' + format;
	}
	TCG.downloadFile(url, null, componentScope);
};

// add event journals tab to security event windows
Clifton.investment.instrument.event.AdditionalTabs[Clifton.investment.instrument.event.AdditionalTabs.length] = {
	title: 'Event Journals',
	items: [{
		xtype: 'gridpanel',
		name: 'accountingEventJournalListFind',
		instructions: 'The following event journals have been generated for this security event.',
		topToolbarSearchParameter: 'description',
		separateJournalPerHoldingAccount: true,
		columns: [
			{header: 'ID', dataIndex: 'id', width: 30},
			{header: 'Journal Type', dataIndex: 'journalType.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalTypeId', displayField: 'label', url: 'accountingEventJournalTypeListFind.json'}},
			{header: 'Journal Note', dataIndex: 'description', width: 150, defaultSortColumn: true},
			{header: 'Booking Date', dataIndex: 'bookingDate', width: 50},
			{header: 'Accrual Reversal Date', dataIndex: 'accrualReversalBookingDate', width: 50}
		],
		getTopToolbarInitialLoadParams: function(firstLoad) {
			return {securityEventId: this.getWindow().getMainFormId()};
		},
		getExDate: function() {
			const exDate = TCG.parseDate(this.getWindow().getMainFormPanel().getFormValue('exDate'));
			if (TCG.isNotBlank(exDate)) {
				return exDate.format('m/d/Y');
			}
			return exDate;
		},
		getEventDate: function() {
			const eventDate = TCG.parseDate(this.getWindow().getMainFormPanel().getFormValue('eventDate'));
			if (TCG.isNotBlank(eventDate)) {
				return eventDate.format('m/d/Y');
			}
			return eventDate;
		},
		getTopToolbarFilters: function(toolbar) {
			const filters = TCG.callOverridden(this, 'getTopToolbarFilters', arguments) || [];
			filters.push({
				fieldLabel: 'Generation Date', name: 'generationDateType', value: 'today', width: 100, minListWidth: 100, displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
				tooltip: 'Date used to determine whether accruals and reversals should be generated. For example, generating event journals on Ex Date option will only generate accrual journals whereas generating event journals on or after Event Date will generate both accrual and reversal journals.',
				listeners: {
					beforeselect: function(field, selection) {
						const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
						let date = null;
						switch (selection.data.value) {
							case 'exDate':
								date = gridPanel.getExDate();
								if (TCG.isBlank(date)) {
									TCG.showError('Ex Date is invalid because it is not defined for this security event.', 'Empty Ex Date');
									return false;
								}
								break;
							case 'eventDate':
								date = gridPanel.getEventDate();
								if (TCG.isBlank(date)) {
									TCG.showError('Event Date is invalid because it is not defined for this security event.', 'Empty Event Date');
									return false;
								}
								break;
							case 'today':
								date = new Date().format('m/d/Y');
								break;
							default:
							// Do nothing
						}
						if (TCG.isNotBlank(date)) {
							TCG.getChildByName(gridPanel.topToolbar, 'generationDate').setValue(date);
						}
					}
				},
				store: {
					xtype: 'arraystore',
					fields: ['value', 'name', 'description'],
					data: [
						['exDate', 'Ex Date', 'Generate event journals on Ex Date of this event. This option will generate accrual journals only.'],
						['eventDate', 'Event Date', 'Generate event journals on Event Date of this event. This option will generate accrual and reversal journals.'],
						['today', 'Today', 'Generate event journals today.'],
						['customDate', 'Custom Date', 'Generate event journals on a custom date.']
					]
				}
			});
			filters.push({
				xtype: 'toolbar-datefield', name: 'generationDate', value: new Date().format('m/d/Y'),
				listeners: {
					select: function(field) {
						const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
						let generationDateType = 'customDate';
						switch (field.value) {
							case gridPanel.getExDate():
								generationDateType = 'exDate';
								break;
							case gridPanel.getEventDate():
								generationDateType = 'eventDate';
								break;
							case new Date().format('m/d/Y'):
								generationDateType = 'today';
								break;
							default:
						}
						TCG.getChildByName(gridPanel.topToolbar, 'generationDateType').setValue(generationDateType);
					}
				}
			});
			return filters;
		},
		generateAndPost: function(generatorType) {
			const gridPanel = this;
			Ext.Msg.confirm('Event Journal', 'Would you like to generate Event Journal for this Security Event and immediately post it to the General Ledger?', function(a) {
				if (a === 'yes') {
					const loader = new TCG.data.JsonLoader({
						waitTarget: gridPanel,
						root: 'eventJournalGeneratorCommand',
						params: {
							eventId: gridPanel.getWindow().getMainFormId(),
							generatorType: generatorType,
							separateJournalPerHoldingAccount: gridPanel.separateJournalPerHoldingAccount,
							generationDate: TCG.getChildByName(gridPanel.getTopToolbar(), 'generationDate').getValue().format('m/d/Y')
						},
						onLoad: function(record, conf) {
							const msg = TCG.getValue('resultMessages', record);
							if (msg) {
								TCG.showError(msg, 'Generation Status');
							}
							gridPanel.reload();
						}
					});
					loader.load('accountingEventJournalListGenerate.json');
				}
			});
		},
		deleteEventJournal: function(autoUnpost) {
			const gridPanel = this;
			const grid = this.grid;
			const sm = grid.getSelectionModel();
			if (sm.getCount() === 0) {
				TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
			}
			else {
				const editor = this.editor;
				Ext.Msg.confirm('Delete Selected Row(s)', 'Would you like to delete all selected row(s)?', function(a) {
					if (a === 'yes') {
						editor.autoUnpost = autoUnpost;
						editor.deleteSelection(sm, gridPanel, 0);
					}
				});
			}
		},
		editor: {
			detailPageClass: 'Clifton.accounting.eventjournal.EventJournalWindow',
			allowToDeleteMultiple: true,
			getDeleteURL: function() {
				return this.autoUnpost ? 'accountingEventJournalWithForcedUnpostingDelete.json' : 'accountingEventJournalDelete.json';
			},
			addToolbarAddButton: function(toolBar, gridPanel) {
				toolBar.add({
					text: 'Generate and Post',
					xtype: 'splitbutton',
					tooltip: 'Generate event journal for this security event and immediately post it to the General Ledger',
					iconCls: 'book-red',
					scope: gridPanel,
					handler: function() {
						gridPanel.generateAndPost('POST');
					},
					menu: new Ext.menu.Menu({
						items: [{
							text: 'Generate Only (DO NOT POST)',
							tooltip: 'Generate and Save event journal(s) but DO NOT post them to the General Leger',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								gridPanel.generateAndPost('GENERATE');
							}
						}, {
							text: 'Generate and Post (FORCE ACCRUAL POSTING)',
							tooltip: 'Force accrual posting even if now is before Ex Date.',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								gridPanel.generateAndPost('POST_FORCE_ACCRUAL');
							}
						}, '-', {
							text: 'Separate Journal per Holding Account',
							tooltip: 'Generate a separate Event Journal for each Holding Account',
							xtype: 'menucheckitem',
							checked: true,
							handler: function(item, event) {
								gridPanel.separateJournalPerHoldingAccount = !item.checked;
							}
						}]
					})
				});
				toolBar.add('-');
				toolBar.add({
					text: 'Preview',
					tooltip: 'Preview auto generated event journal(s) for this security event.  Event Journals are not saved or posted.',
					iconCls: 'preview',
					scope: gridPanel,
					handler: function() {
						const defaultData = {
							eventId: gridPanel.getWindow().getMainFormId()
						};

						const className = 'Clifton.accounting.eventjournal.EventJournalPreviewWindow';
						const cmpId = TCG.getComponentId(className, gridPanel.getWindow().getMainFormId());
						TCG.createComponent(className, {
							id: cmpId,
							defaultData: defaultData,
							openerCt: gridPanel
						});

					}
				});
				toolBar.add('-');
			},
			addToolbarDeleteButton: function(toolBar, gridPanel) {
				toolBar.add({
					text: 'Remove',
					xtype: 'splitbutton',
					tooltip: 'Remove selected item(s)',
					iconCls: 'remove',
					scope: this,
					handler: function() {
						gridPanel.deleteEventJournal(false);
					},
					menu: new Ext.menu.Menu({
						items: [{
							text: 'Remove (FORCE UNPOST)',
							tooltip: 'Unpost corresponding accounting journal(s) first if they have been posted.',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								gridPanel.deleteEventJournal(true);
							}
						}]
					})
				});
				toolBar.add('-');
			}
		}
	}]
};
Clifton.investment.instrument.event.AdditionalTabs[Clifton.investment.instrument.event.AdditionalTabs.length] = {
	title: 'Event Journal Details',
	items: [{
		xtype: 'gridpanel',
		name: 'accountingEventJournalDetailListFind',
		instructions: 'A list of Even Journal Details from event Journal(s) for selected Event. Open the journal to see journal type specific column names.',
		topToolbarSearchParameter: 'clientInvestmentAccount',
		columns: [
			{header: 'ID', dataIndex: 'id', width: 70, type: 'int', hidden: true},
			{header: 'PID', dataIndex: 'accountingTransaction.id', width: 70, type: 'int', doNotFormat: true, hidden: true},
			{header: 'Journal ID', width: 70, dataIndex: 'journal.id', type: 'int', doNotFormat: true, filter: {searchFieldName: 'journalId'}, hidden: true},
			{header: 'Client Account', dataIndex: 'accountingTransaction.clientInvestmentAccount.label', width: 250, filter: {searchFieldName: 'clientInvestmentAccount'}, defaultSortColumn: true},
			{header: 'Holding Account', dataIndex: 'accountingTransaction.holdingInvestmentAccount.label', filter: {searchFieldName: 'holdingInvestmentAccount'}, width: 250},
			{header: 'GL Account', dataIndex: 'accountingTransaction.accountingAccount.label', width: 150, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json', showNotEquals: true}, hidden: true},
			{header: 'Date Open', dataIndex: 'accountingTransaction.originalTransactionDate', width: 80, filter: {searchFieldName: 'originalTransactionDate'}},
			{header: 'Quantity', dataIndex: 'affectedQuantity', type: 'float', width: 90, summaryType: 'sum'},
			{header: 'Price', dataIndex: 'transactionPrice', type: 'float', useNull: true, width: 90},
			{header: 'Amount', dataIndex: 'transactionAmount', type: 'float', useNull: true, width: 110, summaryType: 'sum'},
			{header: 'Additional Amount', dataIndex: 'additionalAmount', type: 'float', useNull: true, width: 110},
			{header: 'Additional Amount 2', dataIndex: 'additionalAmount2', type: 'float', useNull: true, width: 110, summaryType: 'sum'},
			{header: 'Additional Amount 3', dataIndex: 'additionalAmount3', type: 'float', useNull: true, width: 110, summaryType: 'sum'},
			{header: 'Cost', dataIndex: 'affectedCost', type: 'currency', useNull: true, width: 95, summaryType: 'sum'},
			{header: 'FX Rate', dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, width: 95}
		],
		plugins: {ptype: 'gridsummary'},
		getTopToolbarInitialLoadParams: function(firstLoad) {
			return {securityEventId: this.getWindow().getMainFormId()};
		},
		editor: {
			detailPageClass: 'Clifton.accounting.eventjournal.EventJournalWindow',
			getDetailPageId: function(gridPanel, row) {
				return row.json.journal.id;
			},
			drillDownOnly: true
		}
	}]
};
Clifton.investment.instrument.event.AdditionalTabs[Clifton.investment.instrument.event.AdditionalTabs.length] = {
	title: 'Event Positions',
	items: [{
		xtype: 'accounting-liveHoldingsGrid',
		instructions: 'Holdings for positions open one day before Ex Date: positions that must be processed for selected event.',
		columnOverrides: [{dataIndex: 'quantity', summaryType: 'sum'}, {dataIndex: 'localNotional', summaryType: 'sum', hidden: false}, {dataIndex: 'localMarketValue', summaryType: 'sum'}],
		hiddenColumns: ['investmentSecurity.symbol', 'price', 'exchangeRateToBase', 'baseNotional', 'baseMarketValue'],
		skipAccountFiltersAll: true,
		limitToPositions: true,

		getCustomFilters: function() {
			return {investmentSecurityId: this.getWindow().getMainFormPanel().getFormValue('security.id')};
		},
		getDefaultPositionsDate: function() {
			// default to 1 day before Ex Date
			return TCG.parseDate(this.getWindow().getMainFormPanel().getFormValue('exDate')).add(Date.DAY, -1).format('m/d/Y');
		}
	}]
};
Clifton.investment.instrument.event.AdditionalTabs[Clifton.investment.instrument.event.AdditionalTabs.length] = {
	title: 'Event Lots Preview',
	items: [{
		xtype: 'gridpanel',
		name: 'accountingPositionListForEventJournal',
		instructions: 'A list of open position lots that will be effected by this event. Executes "preview" of corresponding event journal(s) generation in order to display accurate positions. NOTE: for booked events that close positions, this logic may not return anything because the position is already closed.',
		remoteSort: false,
		includeAllColumnsView: false,
		columns: [
			{header: 'Transaction ID', width: 50, dataIndex: 'id', type: 'int', hidden: true},
			{header: 'Client Account', width: 125, dataIndex: 'openingTransaction.clientInvestmentAccount.label', defaultSortColumn: true},
			{header: 'Holding Account', width: 110, dataIndex: 'openingTransaction.holdingInvestmentAccount.label'},
			{header: 'Holding Company', dataIndex: 'openingTransaction.holdingInvestmentAccount.issuingCompany.label', hidden: true},
			{header: 'GL Account', dataIndex: 'openingTransaction.accountingAccount.name', width: 65},
			{header: 'Date Open', width: 50, dataIndex: 'openingTransaction.transactionDate'},
			{header: 'Original Date', width: 50, dataIndex: 'openingTransaction.originalTransactionDate', hidden: true},
			{header: 'Quantity', dataIndex: 'remainingQuantity', width: 50, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum', tooltip: 'Remaining Quantity'},
			{header: 'Cost Basis', dataIndex: 'remainingCostBasis', type: 'currency', negativeInRed: true, width: 60, tooltip: 'Remaining Local Cost Basis'},
			{header: 'Base Debit/Credit', dataIndex: 'remainingBaseDebitCredit', type: 'currency', negativeInRed: true, width: 60, hidden: true, tooltip: 'Remaining Base Debit/Credit'}
		],
		plugins: {ptype: 'gridsummary'},
		getLoadParams: function(firstLoad) {
			return {securityEventId: this.getWindow().getMainFormId()};
		},
		editor: {
			detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
			drillDownOnly: true
		}
	}]
};


Clifton.business.client.ClientFundWindowAdditionalTabs.push({
	addAdditionalTabs: function(w) {
		const tabs = w.items.get(0);
		const pos = w.getTabPosition(w, 'Manager Accounts');
		tabs.insert(pos, this.investorHoldingsTab);
	},

	investorHoldingsTab: {
		title: 'Investor Holdings',
		items: [{
			xtype: 'accounting-liveHoldingsGrid',
			hiddenColumns: ['accountingAccount.name', 'investmentSecurity.symbol', 'exchangeRateToBase', 'baseNotional'],
			skipAccountFiltersAll: true,
			groupField: 'investmentSecurity.symbol',
			groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Investors" : "Investor"]})',

			getCustomFilters: function() {
				return {securityFundClientId: this.getWindow().getMainFormId()};
			}
		}]
	}
});


Clifton.accounting.TransactionGridEditor = function(config) {
	Ext.apply(this, config);
};

Ext.extend(Clifton.accounting.TransactionGridEditor, Ext.util.Observable, {
	init: function(grid) {
		this.grid = grid.grid;
		if (!grid.editor) {
			// grid plugins were already initialized: add one more
			grid.editor = Ext.ComponentMgr.createPlugin({
				ptype: 'grideditor',
				drillDownOnly: true,
				getDetailPageClass: function(gridPanel, row) {
					if (!Ext.isNumber(row.id)) {
						TCG.showInfo('The navigation was canceled because the identity of the row could not be determined. This can result if a row represents pending or transient data.', 'Navigation Failed');
						return false;
					}
					if (this.screenSelection === 'TRANSACTION') {
						this.detailPageId = row.id;
						return 'Clifton.accounting.gl.TransactionWindow';
					}
					const tran = TCG.data.getData('accountingTransaction.json?requestedPropertiesRoot=data&requestedProperties=journal.id|journal.fkFieldId|journal.journalType.systemTable.detailScreenClass|journal.journalType.systemTable.name&id=' + row.id, gridPanel);
					this.detailPageId = TCG.getValue('journal.fkFieldId', tran);
					let pageClass = 'Clifton.accounting.journal.JournalWindow';
					let fkField = 'journal.id';
					if (this.screenSelection !== 'JOURNAL' && TCG.isNotNull(TCG.getValue('journal.fkFieldId', tran))) {
						// sub-system specific screen
						pageClass = TCG.getValue('journal.journalType.systemTable.detailScreenClass', tran);
						fkField = 'journal.fkFieldId';
						if (!pageClass) {
							const tableName = TCG.getValue('journal.journalType.systemTable.name', tran);
							TCG.showError('Screen class is not defined for table: ' + tableName, 'Undefined Sub-System Screen');
							return undefined;
						}
					}
					this.detailPageId = TCG.getValue(fkField, tran);
					return pageClass;
				},
				getDetailPageId: function(gridPanel, row) {
					this.screenSelection = undefined;
					return this.detailPageId;
				}
			});
			grid.editor.init(this.grid);
			this.grid.on('afterrender', function() {
				const el = this.getEl();
				el.on('contextmenu', function(e, target) {
					const g = this;
					g.contextRowIndex = g.view.findRowIndex(target);
					e.preventDefault();
					if (!g.drillDownMenu) {
						g.drillDownMenu = new Ext.menu.Menu({
							items: [
								{
									text: 'Open Source Screen', iconCls: 'book-open',
									handler: function() {
										grid.editor.startEditing(g, g.contextRowIndex);
									}
								},
								{
									text: 'Open Journal', iconCls: 'book-open-blue',
									handler: function() {
										grid.editor.screenSelection = 'JOURNAL';
										grid.editor.startEditing(g, g.contextRowIndex);
									}
								},
								{
									text: 'Open Transaction', iconCls: 'book-red',
									handler: function() {
										grid.editor.screenSelection = 'TRANSACTION';
										grid.editor.startEditing(g, g.contextRowIndex);
									}
								}
							]
						});
					}
					TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
				}, this);
			});
		}
	},
	onDestroy: function() {
		Ext.destroy(this.grid.drillDownMenu);
		Clifton.accounting.TransactionGridEditor.superclass.onDestroy.apply(this, arguments);
	}
});
Ext.preg('accounting-transaction-grideditor', Clifton.accounting.TransactionGridEditor);


Clifton.accounting.TransactionGrid = Ext.extend(TCG.grid.GridPanel, {
	instructions: 'These are detailed entries found in the general ledger.',
	name: 'accountingTransactionListFind',
	forceFit: false,
	appendStandardColumns: false,
	includeAccountingGroupFilter: true,
	includeInstrumentGroupFilter: false,
	includeSecurityGroupFilter: false,
	includeClientAccountGroupFilter: false,
	includeClientAccountFilter: true,
	includeJournalTypeFilter: true,
	includeDateFilter: false,
	includeDisplayFilter: true,
	requestIdDataIndex: true,
	columns: [
		{
			header: 'ID', dataIndex: 'id', type: 'int', width: 70,
			renderer: function(v, metaData, r) {
				if (r.data.deleted) {
					metaData.css = 'ruleViolationBig';
					metaData.attr = 'qtip=\'This Transaction was Unposted from the General Ledger\'';
				}
				else if (r.data.modified) {
					metaData.css = 'ruleViolation';
					metaData.attr = 'qtip=\'This Transaction was Re-Posted to the General Ledger with changes\'';
				}
				return v;
			}
		},
		{header: 'PID', dataIndex: 'parentTransaction.id', type: 'int', doNotFormat: true, useNull: true, width: 60, filter: {searchFieldName: 'parentId'}},
		// skip extra join {header: 'JID', dataIndex: 'journal.id', type: 'int', useNull: true, width: 60, filter: {searchFieldName: 'journalId'}},
		{header: 'Opening', dataIndex: 'opening', width: 65, type: 'boolean', tooltip: 'Indicates whether this is an opening or closing transaction. This is mostly applicable to Position GL Accounts.'},
		{header: 'Deleted', dataIndex: 'deleted', width: 65, type: 'boolean', hidden: true, tooltip: 'When checked, indicates that the journal with this transaction was unposted from the General Ledger. This transaction is ignored by processing logic and is used only to preserve history.'},
		{header: 'Modified', dataIndex: 'modified', width: 65, type: 'boolean', hidden: true, tooltip: 'When checked, indicates that the journal with this transaction was unposted and then re-posted for the source entity. After reposting, something was updated/modified.'},
		{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', width: 140, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', width: 140, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
		{header: 'Holding Account Type', dataIndex: 'holdingInvestmentAccount.type.name', width: 140, filter: {type: 'combo', searchFieldName: 'holdingAccountTypeId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
		{header: 'Clearing Company', width: 120, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
		{header: 'Executing Company', width: 120, dataIndex: 'executingCompany.name', filter: {type: 'combo', searchFieldName: 'executingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
		{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 100, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json', showNotEquals: true}},
		{header: 'Investment Hierarchy', dataIndex: 'investmentSecurity.instrument.hierarchy.labelExpanded', width: 150, filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', queryParam: 'labelExpanded', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', listWidth: 600}, hidden: true},
		{header: 'Investment Instrument', dataIndex: 'investmentSecurity.instrument.labelShort', idDataIndex: 'investmentSecurity.instrument.id', width: 140, filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json', listWidth: 600}, hidden: true},
		{header: 'Security', dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', width: 100, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Security Name', dataIndex: 'investmentSecurity.name', width: 100, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
		{header: 'CCY Denom', dataIndex: 'investmentSecurity.instrument.tradingCurrency.symbol', width: 70, filter: {type: 'combo', searchFieldName: 'payingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json', showNotEquals: true}, hidden: true},
		{header: 'Price', dataIndex: 'price', type: 'float', useNull: true, negativeInRed: true, width: 80},
		{header: 'Qty', dataIndex: 'quantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 80, summaryType: 'sum'},
		{header: 'Local Debit', dataIndex: 'localDebit', type: 'currency', width: 90, filter: {searchFieldName: 'localDebitOrCredit'}, summaryType: 'sum'},
		{header: 'Local Credit', dataIndex: 'localCredit', type: 'currency', width: 90, filter: {searchFieldName: 'localDebitOrCredit'}, summaryType: 'sum'},
		{header: 'Tran Date', dataIndex: 'transactionDate', width: 80, defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Description', dataIndex: 'description', width: 250},
		{header: 'FX Rate', dataIndex: 'exchangeRateToBase', type: 'currency', numberFormat: '0,000.0000000000', negativeInRed: true, width: 80},
		{header: 'Base Debit', dataIndex: 'baseDebit', type: 'currency', width: 90, filter: {searchFieldName: 'baseDebitOrCredit'}, summaryType: 'sum'},
		{header: 'Base Credit', dataIndex: 'baseCredit', type: 'currency', width: 90, filter: {searchFieldName: 'baseDebitOrCredit'}, summaryType: 'sum'},
		{header: 'Cost Basis', dataIndex: 'positionCostBasis', type: 'currency', width: 90, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
		{header: 'Original Date', dataIndex: 'originalTransactionDate', width: 80},
		{header: 'Settlement Date', dataIndex: 'settlementDate', width: 90}
	],
	getDefaultPositionsDate: function() {
		return new Date().format('m/d/Y');
	},
	getTopToolbarFilters: function(toolbar) {
		const grid = this;
		const filters = [];
		if (this.includeInstrumentGroupFilter) {
			filters.push({fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'});
		}
		if (this.includeSecurityGroupFilter) {
			filters.push({fieldLabel: 'Security Group', xtype: 'toolbar-combo', name: 'securityGroupId', width: 150, url: 'investmentSecurityGroupListFind.json'});
		}
		if (this.includeAccountingGroupFilter) {
			filters.push({fieldLabel: 'GL Account Group', xtype: 'toolbar-combo', name: 'accountingAccountGroupId', width: 180, url: 'accountingAccountGroupListFind.json'});
		}
		if (this.includeClientAccountGroupFilter) {
			filters.push({
				fieldLabel: 'Client Account Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json',
				listeners: {
					select: function(field) {
						field.resetAccountCombo(field);
						TCG.getParentByClass(field, Ext.Panel).reload();
					},
					change: function(field) {
						// Clearing the account combo
						if (TCG.isBlank(field.getValue())) {
							field.resetAccountCombo(field);
						}
					}
				},
				resetAccountCombo: function(field) {
					const accountField = TCG.getChildByName(field.ownerCt, 'clientInvestmentAccountId');
					accountField.clearValue();
					accountField.reset();
					accountField.store.removeAll();
					accountField.lastQuery = null;
					// clear linked field
					TCG.getParentByClass(accountField, Ext.Panel).setFilterValue(accountField.linkedFilter, {value: '', text: ''}, true);
				}
			});
		}
		if (this.includeClientAccountFilter) {
			filters.push({fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientInvestmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label', displayField: 'label'});
		}
		if (this.includeJournalTypeFilter) {
			filters.push({fieldLabel: 'Journal Type', xtype: 'toolbar-combo', name: 'accountingJournalTypeId', width: 180, url: 'accountingJournalTypeList.json', loadAll: true});
		}
		if (this.includeDateFilter) {
			//Date filter (auto-reload)
			filters.push({
				name: 'lookupDateType', value: 'transactionDate', width: 110, minListWidth: 110, displayField: 'name', valueField: 'value', mode: 'local', xtype: 'toolbar-combo',
				store: {
					xtype: 'arraystore',
					fields: ['value', 'name', 'description'],
					data: [
						['transactionDate', 'Transaction Date', 'Aggregate General Ledger transactions using Transaction Date up to and including this date'],
						['settlementDate', 'Settlement Date', 'Aggregate General Ledger transactions using Settlement Date up to and including this date']
					]
				}
			});
			filters.push({xtype: 'toolbar-datefield', name: 'lookupDate', value: grid.getDefaultPositionsDate()});
		}
		if (this.includeDisplayFilter) {
			filters.push({
				fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 70, minListWidth: 70, forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value', value: 'CURRENT',
				store: new Ext.data.ArrayStore({
					fields: ['value', 'name', 'description'],
					data: [
						['ALL', 'All', 'Display all transactions (including deleted/unposted transactions)'],
						['CURRENT', 'Current', 'Display all current transactions (hide deleted transactions)'],
						['DELETED', 'Deleted', 'Display Deleted transactions only']
					]
				})
			});
		}
		const additionalFilters = this.getAdditionalTopToolbarFilters(grid, toolbar);
		if (additionalFilters) {
			filters.push(...additionalFilters);
		}
		return filters;
	},
	getAdditionalTopToolbarFilters: function(grid, toolbar) {
		return [];
	},
	getCustomFilters: function(firstLoad) {
		return {};
	},
	getLoadParams: function(firstLoad) {
		let params = this.getCustomFilters(firstLoad) || {};
		if (params !== false) {
			params = Ext.applyIf(params, {allowDeleted: true, readUncommittedRequested: true});

			const t = this.getTopToolbar();
			let field = TCG.getChildByName(t, 'accountingAccountGroupId');
			if (field && TCG.isNotBlank(field.getValue())) {
				params.accountingAccountGroupId = field.getValue();
			}
			field = TCG.getChildByName(t, 'accountingJournalTypeId');
			if (field && TCG.isNotBlank(field.getValue())) {
				params.journalTypeId = field.getValue();
			}
			field = TCG.getChildByName(t, 'accountingAccountGroupId');
			if (field && TCG.isNotBlank(field.getValue())) {
				params.accountingAccountGroupId = field.getValue();
			}
			field = TCG.getChildByName(t, 'clientInvestmentAccountGroupId');
			if (field && TCG.isNotBlank(field.getValue())) {
				params.clientInvestmentAccountGroupId = field.getValue();
			}
			field = TCG.getChildByName(t, 'investmentGroupId');
			if (field && TCG.isNotBlank(field.getValue())) {
				params.investmentGroupId = field.getValue();
			}
			field = TCG.getChildByName(t, 'securityGroupId');
			if (field && TCG.isNotBlank(field.getValue())) {
				params.securityGroupId = field.getValue();
			}
			//Set the date filter value
			const dateFilter = TCG.getChildByName(t, 'lookupDate');
			if (dateFilter) {
				if (TCG.isBlank(dateFilter.getValue())) {
					dateFilter.setValue(this.getDefaultPositionsDate());
				}
				field = TCG.getChildByName(t, 'lookupDateType');
				if (field && TCG.isNotBlank(field.getValue())) {
					params[field.getValue()] = dateFilter.getValue().format('m/d/Y');
				}
			}

			const displayTypeField = TCG.getChildByName(t, 'displayType');
			if (displayTypeField) {
				const displayType = displayTypeField.getValue();
				if (displayType === 'CURRENT') {
					this.setFilterValue('deleted', false);
				}
				else if (displayType === 'DELETED') {
					this.setFilterValue('deleted', true);
				}
				else {
					this.clearFilter('deleted', true);
				}
			}
		}
		return params;
	},
	plugins: [
		{ptype: 'investment-contextmenu-plugin', enableAsyncLookups: true},
		{ptype: 'accounting-transaction-grideditor'},
		{ptype: 'gridsummary'}
	]
});
Ext.reg('accounting-transactionGrid', Clifton.accounting.TransactionGrid);


Clifton.accounting.BalanceSheetGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'accountingBalanceSheetListFind',
	additionalPropertiesToRequest: 'id|clientInvestmentAccount.id|holdingInvestmentAccount.id|accountingAccount.id|investmentSecurity.id',
	instructions: 'Balance sheet account balances on the specified date grouped by client account, holding account, GL account and security.',
	pageSize: 500,
	columns: [
		{header: 'Client Account', width: 125, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, defaultSortColumn: true},
		{header: 'Holding Account', width: 125, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
		{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 70, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json', showNotEquals: true}},
		{header: 'Security', width: 60, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Quantity', dataIndex: 'quantity', width: 45, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
		{header: 'Local Debit', dataIndex: 'localDebit', type: 'currency', width: 55},
		{header: 'Local Credit', dataIndex: 'localCredit', type: 'currency', width: 55},
		{header: 'Base Debit', dataIndex: 'baseDebit', type: 'currency', width: 55},
		{header: 'Base Credit', dataIndex: 'baseCredit', type: 'currency', width: 55},
		{header: 'Local Cost Basis', dataIndex: 'localCostBasis', type: 'currency', width: 60, negativeInRed: true, positiveInGreen: true},
		{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.label', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json'}, hidden: true}
	],

	//Grids that extend this can override this function to provide additional filters
	getCustomFilters: function() {
		return {};
	},

	getLoadParams: function(firstLoad) {
		const lp = this.getCustomFilters();
		lp.readUncommittedRequested = true;

		//Set the date filter value
		const dateFilter = TCG.getChildByName(this.getTopToolbar(), 'transactionDate');
		if (dateFilter) {
			if (TCG.isBlank(dateFilter.getValue())) {
				dateFilter.setValue(this.getDefaultPositionsDate());
			}
			lp.transactionDate = dateFilter.getValue().format('m/d/Y');
		}

		return lp;
	},

	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientInvestmentAccountId', linkedFilter: 'clientInvestmentAccount.label', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
			{fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'transactionDate', value: new Date().format('m/d/Y')}
		];
	},
	editor: {
		detailPageClass: 'Clifton.accounting.gl.TransactionListWindow',
		drillDownOnly: true,
		getDefaultData: function(gridPanel, row) {
			const filters = [];
			const data = row.json;

			filters.push({
				field: 'clientInvestmentAccountId',
				comparison: 'EQUALS',
				value: data.clientInvestmentAccount.id
			});
			filters.push({
				field: 'holdingInvestmentAccountId',
				comparison: 'EQUALS',
				value: data.holdingInvestmentAccount.id
			});
			filters.push({
				field: 'accountingAccountId',
				comparison: 'EQUALS',
				value: data.accountingAccount.id
			});
			filters.push({
				field: 'investmentSecurityId',
				comparison: 'EQUALS',
				value: data.investmentSecurity.id
			});
			const nextDay = new Date(TCG.getChildByName(gridPanel.getTopToolbar(), 'transactionDate').value).add(Date.DAY, 1).format('m/d/Y');
			filters.push({
				field: 'transactionDate',
				comparison: 'LESS_THAN',
				value: nextDay
			});
			return filters;
		}
	}
});
Ext.reg('accounting-balanceSheetGrid', Clifton.accounting.BalanceSheetGrid);

Clifton.accounting.AssetsGrid = Ext.extend(TCG.grid.GridPanel, {
	skipAccountFilter: false, //Adds the filter for Client Account
	skipAccountGroupFilter: false, //Adds the filter for Client Account Group
	skipAccountFiltersAll: false, //If set to true, will not add the Client Account or Client Account Group filters
	includeInvestmentGroupFilter: false,
	name: 'accountingAssetListFind',
	additionalPropertiesToRequest: 'id|clientInvestmentAccount.id|holdingInvestmentAccount.id|accountingAccount.id|investmentSecurity.id',
	requestIdDataIndex: true,
	appendStandardColumns: false,
	hiddenColumns: undefined, //an array columns to hide
	instructions: 'Asset account balances on the specified date grouped by holding account, GL account and security.',
	pageSize: 500,
	columns: [
		{header: 'Client Account', width: 125, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 125, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
		{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.label', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json'}, hidden: true},
		{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 70, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json', showNotEquals: true}},
		{header: 'Security', width: 60, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Instrument', width: 60, dataIndex: 'investmentSecurity.instrument.label', idDataIndex: 'investmentSecurity.instrument.id', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentInstrumentListFind.json'}, hidden: true},
		{header: 'Quantity', dataIndex: 'quantity', width: 45, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
		{header: 'Local Amount', dataIndex: 'localAmount', type: 'currency', width: 55, negativeInRed: true, positiveInGreen: true},
		{header: 'Base Amount', dataIndex: 'baseAmount', type: 'currency', width: 55, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
		{header: 'Local Cost Basis', dataIndex: 'localCostBasis', type: 'currency', width: 60, negativeInRed: true, positiveInGreen: true},
		{header: 'Underlying Instrument', width: 75, dataIndex: 'investmentSecurity.instrument.underlyingInstrument.label', filter: false, sortable: false, hidden: true},
		{header: 'Security One Per Instrument', dataIndex: 'investmentSecurity.instrument.hierarchy.oneSecurityPerInstrument', type: 'boolean', hidden: true}
	],
	plugins: [
		{ptype: 'gridsummary'},
		{ptype: 'accounting-position-balance-contextmenu-plugin'},
		{ptype: 'investment-contextmenu-plugin'}
	],

	//Grids that extend this can override this function to provide additional filters
	getCustomFilters: function() {
		return {};
	},

	getDefaultPositionsDate: function() {
		return new Date().format('m/d/Y');
	},

	initComponent: function() {
		Clifton.accounting.AssetsGrid.superclass.initComponent.call(this);

		let sort = 'clientInvestmentAccount.label';
		const cm = this.getColumnModel();
		if (this.hiddenColumns) {
			for (let i = 0; i < this.hiddenColumns.length; i++) {
				const name = this.hiddenColumns[i];
				if (name === sort) {
					sort = 'investmentSecurity.symbol';
				}
				const colIndex = cm.findColumnIndex(name);
				cm.config[colIndex].hidden = true;
			}
		}
		cm.config[cm.findColumnIndex(sort)].defaultSortColumn = true;
	},

	getLoadParams: function(firstLoad) {
		const lp = this.getCustomFilters();
		lp.readUncommittedRequested = true;
		lp.orderBy = 'clientInvestmentAccountId#investmentSecurityId';

		const tb = this.getTopToolbar();

		//Set the Instrument Group filter value
		if (this.includeInvestmentGroupFilter) {
			const investmentGroupId = TCG.getChildByName(tb, 'investmentGroupId').getValue();
			if (TCG.isNotBlank(investmentGroupId)) {
				lp.investmentGroupId = investmentGroupId;
			}
		}

		//Set the account group filter value
		const accountGroupFilter = TCG.getChildByName(tb, 'clientInvestmentAccountGroupId');
		if (accountGroupFilter && TCG.isNotBlank(accountGroupFilter.getValue())) {
			lp.clientInvestmentAccountGroupId = accountGroupFilter.getValue();
		}

		//Set the date filter value
		const dateFilter = TCG.getChildByName(tb, 'lookupDate');
		if (dateFilter) {
			if (TCG.isBlank(dateFilter.getValue())) {
				dateFilter.setValue(this.getDefaultPositionsDate());
			}
			lp[TCG.getChildByName(tb, 'lookupDateType').getValue()] = dateFilter.getValue().format('m/d/Y');
		}

		if (this.includeHoldingAccountGroupFilter) {
			const holdingAccountGroupFilter = TCG.getChildByName(tb, 'holdingInvestmentAccountGroupId');
			if (holdingAccountGroupFilter) {
				const holdingAccountGroupId = holdingAccountGroupFilter.getValue();
				if (TCG.isNotBlank(holdingAccountGroupId)) {
					lp.holdingInvestmentAccountGroupId = holdingAccountGroupId;
				}
			}
		}

		return lp;
	},
	getTopToolbarFilters: function(toolbar) {
		const grid = this;
		const filters = [];

		//Instrument Group filter (optional)
		if (this.includeInvestmentGroupFilter) {
			filters.push({fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'});
		}

		//Client Account Group and Client Account filters (optional)
		if (this.skipAccountFiltersAll) {
			this.skipAccountFilter = true;
			this.skipAccountGroupFilter = true;
		}

		if (!this.skipAccountFilter) {
			filters.push({fieldLabel: 'Client Acct', xtype: 'combo', name: 'clientInvestmentAccountId', displayField: 'label', linkedFilter: 'clientInvestmentAccount.label', width: 150, url: 'investmentAccountListFind.json?ourAccount=true'});
		}
		if (!this.skipAccountGroupFilter) {
			filters.push({fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'});
		}

		if (this.includeHoldingAccountGroupFilter) {
			filters.push({fieldLabel: 'Holding Acct Group', xtype: 'toolbar-combo', name: 'holdingInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'});
		}

		//Date filter (auto-reload)
		filters.push({
			name: 'lookupDateType', value: 'transactionDate', width: 110, minListWidth: 110, displayField: 'name', valueField: 'value', mode: 'local', xtype: 'toolbar-combo',
			store: {
				xtype: 'arraystore',
				fields: ['value', 'name', 'description'],
				data: [
					['transactionDate', 'Transaction Date', 'Aggregate General Ledger transactions using Transaction Date up to and including this date'],
					['settlementDate', 'Settlement Date', 'Aggregate General Ledger transactions using Settlement Date up to and including this date']
				]
			}
		});
		filters.push({xtype: 'toolbar-datefield', name: 'lookupDate', value: grid.getDefaultPositionsDate()});
		return filters;
	},
	editor: {
		detailPageClass: 'Clifton.accounting.gl.TransactionListWindow',
		drillDownOnly: true,
		getDefaultData: function(gridPanel, row) {
			const filters = [];
			const data = row.json;

			filters.push({
				field: 'clientInvestmentAccountId',
				comparison: 'EQUALS',
				value: data.clientInvestmentAccount.id
			});
			filters.push({
				field: 'holdingInvestmentAccountId',
				comparison: 'EQUALS',
				value: data.holdingInvestmentAccount.id
			});
			filters.push({
				field: 'accountingAccountId',
				comparison: 'EQUALS',
				value: data.accountingAccount.id
			});
			filters.push({
				field: 'investmentSecurityId',
				comparison: 'EQUALS',
				value: data.investmentSecurity.id
			});
			const nextDay = new Date(TCG.getChildByName(gridPanel.getTopToolbar(), 'lookupDate').value).add(Date.DAY, 1).format('m/d/Y');
			filters.push({
				field: TCG.getChildByName(gridPanel.getTopToolbar(), 'lookupDateType').getValue(),
				comparison: 'LESS_THAN',
				value: nextDay
			});
			return filters;
		}
	}
});
Ext.reg('accounting-assetsGrid', Clifton.accounting.AssetsGrid);


Clifton.accounting.LiveHoldingsGrid = Ext.extend(Clifton.accounting.AssetsGrid, {
	name: 'accountingBalanceValueListFind',
	instructions: 'Asset and Liability account balances (live from the General Ledger + valuation using corresponding market data) on the specified date grouped by holding account, GL account and security.',
	remoteSort: false,
	includeAllColumnsView: false,
	includeUnderlyingPrice: false,
	skipAccountFiltersAll: true,
	limitToAssetsAndLiabilities: true,
	limitToPositions: false,
	showRemovalButton: false, // Used on Account windows to open shortcut window to remove balances from account
	getRemovalDefaultData: function() {
		return {};
	},
	viewNames: ['Exclude Receivable Collateral', 'Show All'],
	defaultViewName: 'Exclude Receivable Collateral',
	configureViews: function(menu, gridPanel) {
		menu.add('-');
		menu.add({
			text: 'Include Pending Activity',
			tooltip: 'Include impact of pending trades, transfers, etc.',
			xtype: 'menucheckitem',
			handler: function(item, event) {
				gridPanel.pendingActivityRequest = !item.checked ? 'ALL_WITH_PREVIEW' : null;
				gridPanel.reload();
			}
		});
		menu.add({
			text: 'Limit to Positions',
			tooltip: 'Only include GL Account Position entries',
			xtype: 'menucheckitem',
			checked: this.limitToPositions,
			handler: function(item, event) {
				gridPanel.limitToPositions = !item.checked;
				gridPanel.reload();
			}
		});
		menu.add({
			text: 'Include Underlying Price',
			tooltip: 'Include price of Underlying Security',
			xtype: 'menucheckitem',
			checked: this.includeUnderlyingPrice,
			handler: function(item, event) {
				gridPanel.includeUnderlyingPrice = !item.checked;
				// Hide/Show Underlying Security column
				const cm = gridPanel.getColumnModel();
				cm.setHidden(cm.findColumnIndex('underlyingPrice'), (item.checked));
				gridPanel.reload();
			}
		});
		menu.add({
			text: 'Include Unsettled Payments',
			tooltip: 'Include unsettled accrual and equity leg payment amounts. Equity leg payments are included with the Market Value.',
			xtype: 'menucheckitem',
			handler: function(item, event) {
				gridPanel.unsettledPaymentRequest = !item.checked;
				gridPanel.reload();
			}
		});

		gridPanel.configureAdditionalViews(menu, gridPanel);
	},

	listeners: {
		afterrender: function(fp, isClosing) {
			const defaultView = this.getDefaultViewName();
			if (defaultView) {
				this.setDefaultView(defaultView);
			}
		}
	},

	configureAdditionalViews: function(menu, gridPanel) {
		// can be overridden to add additional view options
	},

	getDefaultViewName: function() {
		return this.defaultViewName;
	},

	columns: [
		{header: 'Pending', dataIndex: 'pendingActivity', type: 'boolean', width: 30, hidden: true},
		{header: 'Client Account', width: 125, dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 125, dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
		{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.label', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json'}, hidden: true},
		{header: 'Team', width: 60, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
		{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 65, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json', showNotEquals: true}},
		{header: 'Security', width: 60, dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Security Name', width: 80, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
		{header: 'Instrument', width: 60, dataIndex: 'investmentSecurity.instrument.label', idDataIndex: 'investmentSecurity.instrument.id', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentInstrumentListFind.json'}, hidden: true},
		{header: 'Underlying Instrument', width: 85, dataIndex: 'investmentSecurity.instrument.underlyingInstrument.label', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}, hidden: true},
		{header: 'Underlying Security', width: 100, dataIndex: 'investmentSecurity.underlyingSecurity.label', filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
		{header: 'Quantity', dataIndex: 'quantity', width: 45, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true},
		{header: 'Price', dataIndex: 'price', width: 50, type: 'float', nonPersistentField: true, useNull: true},
		{header: 'Underlying Price', dataIndex: 'underlyingPrice', width: 70, type: 'float', nonPersistentField: true, useNull: true, sortable: false, filter: false, hidden: true},
		{header: 'Multiplier', width: 40, dataIndex: 'investmentSecurity.priceMultiplier', type: 'float', hidden: true},
		{header: 'FX Rate', dataIndex: 'exchangeRateToBase', width: 50, type: 'float', nonPersistentField: true},
		{header: 'Local Cost', dataIndex: 'localCost', type: 'currency', negativeInRed: true, width: 65, hidden: true},
		{header: 'Base Cost', dataIndex: 'baseCost', type: 'currency', negativeInRed: true, width: 65, summaryType: 'sum', hidden: true},
		{header: 'Local Cost Basis', dataIndex: 'localCostBasis', type: 'currency', negativeInRed: true, width: 65, hidden: true},
		{header: 'Base Cost Basis', dataIndex: 'baseCostBasis', type: 'currency', negativeInRed: true, width: 65, summaryType: 'sum', hidden: true},
		{header: 'Local Accrual 1', dataIndex: 'localAccrual1', type: 'currency', negativeInRed: true, width: 65, hidden: true},
		{header: 'Local Accrual 2', dataIndex: 'localAccrual2', type: 'currency', negativeInRed: true, width: 65, hidden: true},
		{header: 'Local Accrual', dataIndex: 'localAccrual', type: 'currency', negativeInRed: true, width: 65, hidden: true},
		{header: 'Base Accrual 1', dataIndex: 'baseAccrual1', type: 'currency', negativeInRed: true, width: 65, summaryType: 'sum', hidden: true, nonPersistentField: true},
		{header: 'Base Accrual 2', dataIndex: 'baseAccrual2', type: 'currency', negativeInRed: true, width: 65, summaryType: 'sum', hidden: true, nonPersistentField: true},
		{header: 'Base Accrual', dataIndex: 'baseAccrual', type: 'currency', negativeInRed: true, width: 65, summaryType: 'sum', hidden: true, nonPersistentField: true},
		{
			header: 'Local Notional', dataIndex: 'localNotional', type: 'currency', negativeInRed: true, width: 65, hidden: true,
			renderer: function(v, metaData, r) {
				if (r.data.pendingActivity) {
					metaData.css = 'ruleViolation';
					metaData.attr = 'qtip=\'Pending Activity that has not been Posted to the General Ledger yet\'';
				}
				return TCG.renderAmount(v, true, '0,000.00');
			}
		},
		{
			header: 'Base Notional', dataIndex: 'baseNotional', type: 'currency', negativeInRed: true, width: 65, summaryType: 'sum', nonPersistentField: true,
			renderer: function(v, metaData, r) {
				if (r.data.pendingActivity) {
					metaData.css = 'ruleViolation';
					metaData.attr = 'qtip=\'Pending Activity that has not been Posted to the General Ledger yet\'';
				}
				return TCG.renderAmount(v, true, '0,000.00');
			}
		},
		{header: 'Local Market Value', dataIndex: 'localMarketValue', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 75, hidden: true, nonPersistentField: true},
		{header: 'Base Market Value', dataIndex: 'baseMarketValue', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 75, summaryType: 'sum', nonPersistentField: true},
		{header: 'Security One Per Instrument', dataIndex: 'investmentSecurity.instrument.hierarchy.oneSecurityPerInstrument', type: 'boolean', hidden: true}
	],

	getLoadParams: function(firstLoad) {
		const params = Clifton.accounting.LiveHoldingsGrid.superclass.getLoadParams.call(this, firstLoad);
		if (this.limitToAssetsAndLiabilities && !this.limitToPositions) {
			params.accountingAccountTypes = ['Asset', 'Liability'];
		}
		if (this.currentViewName === 'Exclude Receivable Collateral') {
			params.accountingAccountIdName = (this.limitToPositions) ? 'POSITION_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL' : 'EXCLUDE_RECEIVABLE_COLLATERAL';
		}
		else if (this.limitToPositions) {
			params.accountingAccountIdName = 'POSITION_ACCOUNTS';
		}
		if (!TCG.isBlank(this.pendingActivityRequest)) {
			params.pendingActivityRequest = this.pendingActivityRequest;
		}
		if (!TCG.isBlank(this.unsettledPaymentRequest)) {
			params.includeUnsettledLegPayments = this.unsettledPaymentRequest;
		}
		params.includeUnderlyingPrice = this.includeUnderlyingPrice;
		params.orderBy = 'clientInvestmentAccountId#holdingInvestmentAccountId#accountingAccountId#investmentSecurityId';
		return params;
	},

	addToolbarButtons: function(toolBar, gridPanel) {
		if (TCG.isTrue(this.showRemovalButton)) {
			toolBar.add({
				iconCls: 'remove',
				text: 'Remove Balances From Account',
				tooltip: 'Can be used during account termination to remove all positions and/or non-position assets and liabilities from the account',
				scope: this,
				handler: function() {
					const className = 'Clifton.accounting.gl.BalanceRemovalWindow';
					const cmpId = TCG.getComponentId(className, gridPanel.getWindow().getMainFormId());
					TCG.createComponent(className, {
						id: cmpId,
						defaultData: this.getRemovalDefaultData(),
						openerCt: gridPanel
					});
				}
			}, '-');
		}
		if (TCG.isNotNull(this.getCashActivityDefaultData())) {
			toolBar.add({
				text: 'Cash Activity', iconCls: 'book-red', tooltip: 'Display cash activity for this account',
				handler: () => {
					const componentName = 'Clifton.accounting.gl.CashActivityWindow';
					TCG.createComponent(componentName, {
						defaultData: this.getCashActivityDefaultData(),
						openerCt: this
					});
				}
			}, '-');
		}
	},
	/**
	 * Can be overridden to return default data for the cash activity window.
	 * Default data should be an object containing clientInvestmentAccount, holdingInvestmentAccount, or both.
	 */
	getCashActivityDefaultData: function() {
		return void 0;
	}
});
Ext.reg('accounting-liveHoldingsGrid', Clifton.accounting.LiveHoldingsGrid);


Clifton.accounting.SecurityLiveHoldingsGrid = Ext.extend(Clifton.accounting.LiveHoldingsGrid, {
	instructions: 'Live position balances for selected security on the specified date.',
	groupField: 'holdingInvestmentAccount.issuingCompany.label',
	skipAccountFiltersAll: true,
	viewNames: [{text: 'Exclude Receivable Collateral', tooltip: 'Aggregate level view excluding receivable collateral. Default view for most securities.'}, {text: 'Lot Level', tooltip: 'A lot level view of positions that includes opening transaction details. Default view for OTC and factor related securities.', url: 'accountingPositionValueListFind'}, 'Show All'],
	customSecurityColumnsToShow: ['Spread', 'Reference Rate'],
	viewConfig: {markDirty: false},
	columnOverrides: [
		{dataIndex: 'quantity', summaryType: 'sum'},
		{dataIndex: 'localNotional', summaryType: 'sum', hidden: false},
		{dataIndex: 'localMarketValue', summaryType: 'sum'},
		{header: 'Counterparty', dataIndex: 'holdingInvestmentAccount.issuingCompany.abbreviation', tooltip: 'Holding account issuing company\'s abbreviation', width: 40, useNull: true, viewNames: ['Lot Level']},
		{header: 'Opening Quantity', dataIndex: 'openingTransaction.quantity', width: 45, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum', viewNames: ['Lot Level']},
		{header: 'Opening Price', dataIndex: 'openingTransaction.price', width: 50, type: 'float', useNull: true, viewNames: ['Lot Level']},
		{header: 'Opening Date', dataIndex: 'openingTransaction.originalTransactionDate', width: 50, type: 'date', useNull: true, viewNames: ['Lot Level']},
		{header: 'Maturity Date', dataIndex: 'investmentSecurity.endDate', tooltip: 'The maturity, or end date, of the security', width: 50, type: 'date', useNull: true, viewNames: ['Lot Level']}
	],

	hiddenColumns: ['investmentSecurity.symbol', 'price', 'exchangeRateToBase', 'localCostBasis', 'baseNotional', 'baseMarketValue', 'holdingInvestmentAccount.issuingCompany.label'],
	limitToPositions: true,

	// include position only (exclude dividend receivable, etc.)
	getCustomFilters: function() {
		return {investmentSecurityId: this.getWindow().getMainFormId()};
	},

	isOtcSecurity: function() {
		this.otc = TCG.getValue('hierarchy.otc', this.getWindow().getMainForm().formValues)
			|| TCG.getValue('instrument.hierarchy.otc', this.getWindow().getMainForm().formValues);
		this.factorEventTypeName = TCG.getValue('hierarchy.factorChangeEventType.name', this.getWindow().getMainForm().formValues)
			|| TCG.getValue('instrument.hierarchy.factorChangeEventType.name', this.getWindow().getMainForm().formValues);
		return (TCG.isTrue(this.otc) || TCG.isNotBlank(this.factorEventTypeName));
	},

	getDefaultViewName: function() {
		return TCG.isTrue(this.isOtcSecurity()) ? 'Lot Level' : this.defaultViewName;
	},

	configureAdditionalViews: function(menu, gridPanel) {
		if (gridPanel.isOtcSecurity()) {
			gridPanel.includeFactor = TCG.isNotBlank(this.factorEventTypeName);
			gridPanel.includeCustomColumns = true;
			gridPanel.includeTerminationFee = true;
		}
		const executeWithMask = async function() {
			gridPanel.grid.loadMask.show();
			try {
				await gridPanel.applyDynamicColumnRecordData.call(gridPanel.grid.getStore());
			}
			finally {
				gridPanel.grid.loadMask.hide();
			}
		};

		menu.add('-',
			{
				text: 'Include Factor',
				tooltip: 'Include security factor column and value in the grid',
				xtype: 'menucheckitem',
				checked: gridPanel.includeFactor,
				handler: function(item, event) {
					gridPanel.includeFactor = !item.checked; // value is value before click, on click event
					executeWithMask();
				}
			},
			{
				text: 'Include Custom Security Columns',
				tooltip: 'Add custom security columns to the grid',
				xtype: 'menucheckitem',
				checked: gridPanel.includeCustomColumns,
				menu: {items: []},
				name: 'customColumnViewItem',
				handler: function(item, event) {
					gridPanel.includeCustomColumns = !item.checked; // value is value before click, on click event
					executeWithMask();
				}
			},
			{
				text: 'Include Termination Fee',
				tooltip: 'Include security termination fee column and value in the grid',
				xtype: 'menucheckitem',
				checked: gridPanel.includeTerminationFee,
				handler: function(item, event) {
					gridPanel.includeTerminationFee = !item.checked; // value is value before click, on click event
					executeWithMask();
				}
			});
	},

	modifySecurityCustomColumnViewMenuItems: function(gridPanel, remove) {
		const customColumnViewItems = gridPanel.getTopToolbar().findBy(item => item.text === 'Views')
			.filter(menu => menu.menu)
			.map(menu => TCG.getChildByName(menu.menu, 'customColumnViewItem'))
			.filter(item => item);
		if (customColumnViewItems[0]) {
			if (TCG.isTrue(remove)) {
				customColumnViewItems[0].menu.removeAll();
			}
			else if (gridPanel.grid.customColumns) {
				const menuItems = gridPanel.grid.customColumns.map(columnDefinition => ({
					text: columnDefinition.dataIndex,
					xtype: 'menucheckitem',
					checked: !columnDefinition.hidden,
					handler: function(item, event) {
						gridPanel.setColumnHidden(columnDefinition.dataIndex, item.checked);
					}
				}));
				if (menuItems.length > 0) {
					customColumnViewItems[0].menu.add(...menuItems);
				}
			}
		}
	},

	beforeSwitchToView: function(viewName) {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const store = grid.getStore();
		if (!store.gridPanel) {
			store.gridPanel = this;
			store.on('load', this.applyDynamicColumnRecordData);
		}
		if (viewName === 'Lot Level') {
			this.resetStoreRoot('data', store);
		}
		else {
			this.resetStoreRoot('data.rows', store);
		}
	},

	resetStoreRoot: function(storeRoot, store = this.grid.getStore()) {
		store.root = storeRoot;
		store.reader.meta.root = store.root;
		delete store.reader.ef;
		store.reader.buildExtractors();
	},

	applyDynamicColumnRecordData: async function() {
		const store = this;
		const gridPanel = store.gridPanel;
		const grid = gridPanel.grid;
		const securityToRowListMap = {};
		store.each(record => {
			const securityId = record.get('investmentSecurity.id');
			let rows = securityToRowListMap[securityId];
			if (!rows) {
				rows = [];
				securityToRowListMap[securityId] = rows;
			}
			rows.push(record);
		});

		const updateRecordColumnValue = function(record, columnName, columnValue) {
			if (record) {
				const currentValue = record.get(columnName);
				if (TCG.isNotEquals(currentValue, columnValue)) {
					if (!record.editing) {
						record.beginEdit();
					}
					record.set(columnName, columnValue);
				}
			}
		};

		// Batch all columns to add/remove so they can be added to the grid at the same time.
		// The grid will refresh/rerender upon each batch of addition/removal.
		const columnsToAdd = [];
		const columnsToRemove = [];
		const columnModel = gridPanel.grid.getColumnModel();
		const factorColumnExists = columnModel.findColumnIndex('factor') > -1;
		if (TCG.isTrue(gridPanel.includeFactor) && !factorColumnExists) {
			columnsToAdd.push({header: 'Factor', dataIndex: 'factor', width: 50, type: 'float', useNull: true});
		}
		else if (!TCG.isTrue(gridPanel.includeFactor) && factorColumnExists) {
			columnsToRemove.push('factor');
		}
		const feeColumnExists = columnModel.findColumnIndex('fees') > -1;
		if (TCG.isTrue(gridPanel.includeTerminationFee) && !feeColumnExists) {
			columnsToAdd.push({
				header: 'Termination Fee', dataIndex: 'fees', width: 50, type: 'float', useNull: true, tooltip: 'Early Termination fee for closing position',
				renderer: function(value, metadata, record) {
					return (value === 'NA') ? '' : TCG.renderValueWithTooltip(value, metadata);
				}
			});
		}
		else if (!TCG.isTrue(gridPanel.includeTerminationFee) && feeColumnExists) {
			columnsToRemove.push('fees');
		}

		const securityIds = Object.keys(securityToRowListMap);
		const today = new Date();
		for (const securityId of securityIds) {
			try {
				if (TCG.isTrue(gridPanel.includeFactor)) {
					// look up factor for security
					const factor = await gridPanel.getSecurityFactor(securityId, today, gridPanel);
					securityToRowListMap[securityId].forEach(record => updateRecordColumnValue(record, 'factor', factor));
				}

				if (TCG.isTrue(gridPanel.includeCustomColumns)) {
					// Security Custom Columns
					const customColumnValues = await gridPanel.getSecurityCustomColumns(securityId, gridPanel);
					if (customColumnValues && customColumnValues.length > 0) {
						if (!grid.customColumns) {
							grid.customColumns = customColumnValues
								.map(column => column.column.name)
								.map(columnName => {
									const header = TCG.camelCaseToTitle(columnName.substring(columnName.lastIndexOf('.')));
									const hidden = !gridPanel.customSecurityColumnsToShow.includes(columnName);
									return {header: header, dataIndex: columnName, hidden: hidden, doNotRequest: true};
								});
							columnsToAdd.push(...grid.customColumns);
							// Add menu items to custom column view menu item
							gridPanel.modifySecurityCustomColumnViewMenuItems(gridPanel);
						}

						customColumnValues.forEach(customColumn => {
							const columnName = TCG.getValue('column.name', customColumn);
							const columnValue = TCG.getValue('text', customColumn);
							securityToRowListMap[securityId].forEach(record => updateRecordColumnValue(record, columnName, columnValue));
						});
					}
				}
				else if (grid.customColumns) {
					const dataIndexes = grid.customColumns.map(columnDefinition => columnDefinition.dataIndex);
					columnsToRemove.push(...dataIndexes);
					grid.customColumns = void 0;
					gridPanel.modifySecurityCustomColumnViewMenuItems(gridPanel, true);
				}

				if (gridPanel.includeTerminationFee) {
					const fees = await gridPanel.getSecurityFeesString(securityId, gridPanel);
					securityToRowListMap[securityId].forEach(record => updateRecordColumnValue(record, 'fees', fees));
				}
			}
			finally {
				// empty the edited records while ending edit for each. If there are columns to add/remove, we can
				// commit silently as the grid refresh on column edits will show the values on modified records.
				const commitSilently = (columnsToAdd.length > 0) || (columnsToRemove.length > 0);
				store.each(record => {
					if (record.editing) {
						if (commitSilently) {
							record.commit(true);
						}
						else {
							record.endEdit();
						}
					}
				});
				// adding columns will populate the record data
				if (columnsToAdd.length > 0) {
					grid.addColumn(...columnsToAdd);
					// clear to avoid adding columns again
					columnsToAdd.splice(0, columnsToAdd.length);
				}
				if (columnsToRemove.length > 0) {
					grid.removeColumn(...columnsToRemove);
					// clear to avoid removing columns again
					columnsToRemove.splice(0, columnsToRemove.length);
				}
			}
		}
	},

	getSecurityFactor: async function(securityId, valuationDate, scope = this) {
		return TCG.data.getFromCacheOrCompute(securityId, scope, 'factorCache', async () => {
			let factor = 1;
			const params = {
				securityId: securityId,
				typeName: (scope && scope.factorEventTypeName) ? scope.factorEventTypeName : 'Factor Change',
				valuationDate: (valuationDate ? valuationDate : new Date()).format('m/d/Y'),
				requestedPropertiesRoot: 'data',
				requestedProperties: 'afterEventValue'
			};
			const event = await TCG.data.getDataPromise('investmentSecurityEventWithLatestExDate.json', this, {params: params});
			if (event && TCG.isNotBlank(event.afterEventValue)) {
				factor = event.afterEventValue;
			}
			return factor;
		});
	},

	getSecurityCustomColumns: async function(securityId, scope) {
		return TCG.data.getFromCacheOrCompute(securityId, scope, 'columnCache', async () => {
			const params = {
				columnGroupName: 'Security Custom Fields',
				fkFieldId: securityId,
				requestedPropertiesRoot: 'data',
				requestedProperties: 'column.name|text',
				requestedPropertiesToExclude: 'systemColumnValueSearchForm'
			};
			return await TCG.data.getDataPromise('systemColumnValueListFind.json', this, {params: params});
		});
	},

	getSecurityFeesString: async function(securityId, scope) {
		return TCG.data.getFromCacheOrCompute(securityId, scope, 'feeCache', async () => {
			const params = {
				investmentSecurityId: securityId,
				accountingTransactionTypeName: 'Early Termination',
				active: true,
				limit: 1,
				requestedPropertiesRoot: 'data',
				requestedProperties: 'commissionApplyMethod|commissionAmount|commissionCalculationMethod',
				requestedPropertiesToExclude: 'accountingCommissionDefinitionSearchForm'
			};
			const fees = await TCG.data.getDataPromise('accountingCommissionDefinitionListFind.json', this, {params: params});
			if (fees && fees.length > 0) {
				return fees.map(fee => {
					const amount = TCG.getValue('commissionAmount', fee);
					let calculationMethod = TCG.getValue('commissionCalculationMethod', fee);
					if (Clifton.accounting.commission.CommissionCalculationMethods[calculationMethod]) {
						calculationMethod = Clifton.accounting.commission.CommissionCalculationMethods[calculationMethod].displayName;
					}
					let applyMethod = TCG.getValue('commissionApplyMethod', fee);
					if (Clifton.accounting.commission.CommissionApplyMethods[applyMethod]) {
						applyMethod = Clifton.accounting.commission.CommissionApplyMethods[applyMethod].displayName;
					}
					return `${amount} ${calculationMethod} ${applyMethod}`;
				}).join();
			}
			return 'NA';
		});
	},

	isSkipExport: function(column) {
		const lotViewColumn = column.dataIndex.startsWith('openingTransaction');
		const lotView = this.currentViewName === 'Lot Level';
		const customColumn = this.grid.customColumns && this.grid.customColumns.filter(c => c.dataIndex === column.dataIndex).length > 0;

		return (lotViewColumn && !lotView) || customColumn || ['factor', 'fees'].includes(column.dataIndex);
	}
});
Ext.reg('accounting-securityLiveHoldingsGrid', Clifton.accounting.SecurityLiveHoldingsGrid);


Clifton.accounting.SecurityAssetsGrid = Ext.extend(Clifton.accounting.AssetsGrid, {
	groupField: 'holdingInvestmentAccount.issuingCompany.label',
	skipAccountFiltersAll: true,
	hiddenColumns: ['investmentSecurity.symbol', 'localAmount', 'baseAmount', 'localCostBasis', 'holdingInvestmentAccount.issuingCompany.label', 'accountingAccount.name'],

	// include position only (exclude dividend receivable, etc.)
	getCustomFilters: function() {
		return {positionAccountingAccount: true, investmentSecurityId: this.getWindow().getMainFormId()};
	}
});
Ext.reg('accounting-securityAssetsGrid', Clifton.accounting.SecurityAssetsGrid);


Clifton.accounting.gl.PositionAnalysisGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'accountingPositionDailyListFind',
	instructions: 'Daily lot-level positions are built from the General Ledger and market data. They may get stale and may need to be rebuilt in order to see accurate data when GL or market data changes.',
	pageSize: 500,
	reloadOnRender: false,
	appendStandardColumns: false,
	forceFit: false,
	viewNames: ['Open Positions', 'Realized and Commission', 'Export Friendly'],
	columns: [
		{header: 'Transaction ID', dataIndex: 'accountingTransaction.id', width: 50, hidden: true},
		{header: 'Position Date', dataIndex: 'positionDate', width: '90', hidden: true},
		{header: 'Account #', dataIndex: 'accountingTransaction.clientInvestmentAccount.number', width: 65, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Client Account', dataIndex: 'accountingTransaction.clientInvestmentAccount.name', width: 210, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', dataIndex: 'accountingTransaction.holdingInvestmentAccount.number', width: 100, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
		{header: 'Holding Company', width: 130, dataIndex: 'accountingTransaction.holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'GL Account', dataIndex: 'accountingTransaction.accountingAccount.name', width: 90, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json', showNotEquals: true}, hidden: true},
		{header: 'Security', dataIndex: 'accountingTransaction.investmentSecurity.symbol', width: 80, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Security Name', dataIndex: 'accountingTransaction.investmentSecurity.name', width: 100, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true, viewNames: ['Export Friendly']},
		{header: 'CCY Denom', dataIndex: 'accountingTransaction.investmentSecurity.instrument.tradingCurrency.symbol', width: 80, filter: {type: 'combo', searchFieldName: 'investmentInstrumentCurrencyDenominationId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}, hidden: true},
		{header: 'Price Multiplier', dataIndex: 'accountingTransaction.investmentSecurity.priceMultiplier', type: 'float', useNull: true, width: 100, hidden: true, viewNames: ['Export Friendly'], filter: {searchFieldName: 'priceMultiplier'}},
		{header: 'Initial Margin per Qty', dataIndex: 'accountingTransaction.initialMarginPerUnit', width: 130, type: 'currency', useNull: true, sortable: false, hidden: true, viewNames: ['Export Friendly'], tooltip: 'Actual Initial Margin Requirement for one Unit of Quantity. The calculation takes into account whether the holding account is sued for hedging or speculation as well as account specific Initial Margin Multiplier.'},
		{header: 'Remaining Qty', dataIndex: 'remainingQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 100, viewNames: ['Open Positions', 'Export Friendly']},
		{header: 'Open Qty', dataIndex: 'accountingTransaction.quantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 95, hidden: true, viewNames: ['Export Friendly']},
		{header: 'Market Price', dataIndex: 'marketPrice', type: 'float', useNull: true, negativeInRed: true, width: 90},
		{header: 'Prior Price', dataIndex: 'priorPrice', type: 'float', useNull: true, negativeInRed: true, width: 90, hidden: true, viewNames: ['Export Friendly']},
		{header: 'Open Price', dataIndex: 'accountingTransaction.price', type: 'float', useNull: true, negativeInRed: true, width: 90, hidden: true, viewNames: ['Export Friendly']},
		{header: 'FX Rate', dataIndex: 'marketFxRate', type: 'float', width: 80},
		{header: 'Local Cost Basis', dataIndex: 'remainingCostBasisLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
		{header: 'Base Cost Basis', dataIndex: 'remainingCostBasisBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true, summaryType: 'sum'},
		{header: 'Local OTE', dataIndex: 'openTradeEquityLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
		{header: 'Base OTE', dataIndex: 'openTradeEquityBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', viewNames: ['Open Positions']},
		{header: 'Prior Local OTE', dataIndex: 'priorOpenTradeEquityLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
		{header: 'Prior Base OTE', dataIndex: 'priorOpenTradeEquityBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true},
		{header: 'Local Notional', dataIndex: 'notionalValueLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, viewNames: ['Open Positions']},
		{header: 'Base Notional', dataIndex: 'notionalValueBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', viewNames: ['Open Positions']},
		{header: 'Today Closed Qty', dataIndex: 'todayClosedQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 110, hidden: true, viewNames: ['Realized and Commission', 'Export Friendly']},
		{header: 'Local Realized', dataIndex: 'todayRealizedGainLossLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true, viewNames: ['Realized and Commission']},
		{header: 'Base Realized', dataIndex: 'todayRealizedGainLossBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true, viewNames: ['Realized and Commission', 'Export Friendly']},
		{header: 'Local Commission', dataIndex: 'todayCommissionLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 120, hidden: true, viewNames: ['Realized and Commission']},
		{header: 'Base Commission', dataIndex: 'todayCommissionBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 120, summaryType: 'sum', hidden: true, viewNames: ['Realized and Commission', 'Export Friendly']},
		{header: 'Local M2M', dataIndex: 'markAmountLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
		{header: 'Base M2M', dataIndex: 'markAmountBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true, viewNames: ['Export Friendly']},
		{header: 'Open Date', dataIndex: 'accountingTransaction.transactionDate', width: 90, hidden: true, viewNames: ['Export Friendly']},

		{header: 'Prior Local Accrual', dataIndex: 'priorAccrualLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true},
		{header: 'Prior Base Accrual', dataIndex: 'priorAccrualBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
		{header: 'Local Accrual', dataIndex: 'accrualLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true},
		{header: 'Base Accrual', dataIndex: 'accrualBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},

		{header: 'Prior Local Market Value', dataIndex: 'priorMarketValueLocal', type: 'currency', negativeInRed: true, width: 82, hidden: true, summaryType: 'sum', hideGrandTotal: true},
		{header: 'Prior Base Market Value', dataIndex: 'priorMarketValueBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', viewNames: ['Cleared OTC']},
		{header: 'Local Market Value', dataIndex: 'marketValueLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', hideGrandTotal: true},
		{header: 'Base Market Value', dataIndex: 'marketValueBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum', viewNames: ['Cleared OTC']},

		{header: 'Local Daily Gain/Loss', dataIndex: 'dailyGainLossLocal', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, hidden: true},
		{header: 'Base Daily Gain/Loss', dataIndex: 'dailyGainLossBase', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 100, summaryType: 'sum', hidden: true, viewNames: ['Export Friendly']}
	],
	plugins: {ptype: 'gridsummary'},
	editor: {
		detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
		drillDownOnly: true,
		getDetailPageId: function(gridPanel, row) {
			return row.json.accountingTransaction.id;
		}
	},
	getLoadParams: function() {
		const toolBar = this.getTopToolbar();
		const loadParams = {};
		loadParams.positionDate = TCG.getChildByName(toolBar, 'positionDate').getValue().format('m/d/Y');
		let v = TCG.getChildByName(toolBar, 'investmentGroupId').getValue();
		if (v) {
			loadParams.investmentGroupId = v;
		}
		v = TCG.getChildByName(toolBar, 'clientInvestmentAccountGroupId').getValue();
		if (v) {
			loadParams.clientInvestmentAccountGroupId = v;
		}
		v = TCG.getChildByName(toolBar, 'holdingInvestmentAccountGroupId').getValue();
		if (v) {
			loadParams.holdingInvestmentAccountGroupId = v;
		}
		return loadParams;
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'});
		filters.push({fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'});
		filters.push({fieldLabel: 'Holding Acct Group', xtype: 'toolbar-combo', name: 'holdingInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'});
		filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'positionDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')});
		return filters;
	}
});
Ext.reg('accounting-positionAnalysisGrid', Clifton.accounting.gl.PositionAnalysisGrid);


Clifton.accounting.DailyPositionsRebuildForm = {
	xtype: 'formpanel',
	loadValidation: false,
	validatedOnLoad: false,

	getDefaultData: function(form) {
		if (this.ownerCt && this.ownerCt.defaultHoldingAccountTypeName) {
			const fb = TCG.data.getData('investmentAccountTypeByName.json?name=' + this.ownerCt.defaultHoldingAccountTypeName, this, 'investment.account.type.' + this.ownerCt.defaultHoldingAccountTypeName);
			return {holdingAccountTypeId: {value: fb.id, text: fb.name}};
		}
	},

	listeners: {
		afterrender: function() {
			this.setFormValue('startSnapshotDate', Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), true);
			this.setFormValue('endSnapshotDate', Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), true);
		}
	},
	items: [
		{
			xtype: 'fieldset',
			title: 'Rebuild Positions',
			buttonAlign: 'right',
			items: [{
				xtype: 'panel',
				layout: 'column',
				items: [
					{
						columnWidth: .49,
						items: [{
							xtype: 'formfragment',
							frame: false,
							labelWidth: 150,
							items: [
								// These are all mutually exclusive - except for Account Type and Issuing Company - Those two can be used together or individually ( or used to filter holding account)
								{
									fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'clientAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label',
									mutuallyExclusiveFields: ['groupName', 'holdingAccountTypeName', 'holdingCompany', 'holdingAccount', 'securityName']
								},
								{
									fieldLabel: 'Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow',
									mutuallyExclusiveFields: ['accountLabel', 'holdingAccountTypeName', 'holdingCompany', 'holdingAccount', 'securityName']
								},
								{
									fieldLabel: 'Holding Account Type', xtype: 'combo', name: 'holdingAccountTypeName', hiddenName: 'holdingAccountTypeId', url: 'investmentAccountTypeListFind.json?ourAccount=false', pageSize: 10, qtip: 'Filter holding company selections by account type',
									mutuallyExclusiveFields: ['accountLabel', 'groupName', 'securityName']
								},
								{
									fieldLabel: 'Holding Company', xtype: 'combo', name: 'holdingCompany', hiddenName: 'holdingCompanyId',
									url: 'businessCompanyListFind.json?issuer=true', pageSize: 10, qtip: 'Filter the rebuild by selected holding company',
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										const atv = f.findField('holdingAccountTypeId').getValue();
										combo.store.baseParams = atv ? {activeIssuerInvestmentAccountTypeId: atv} : {};
									},
									mutuallyExclusiveFields: ['accountLabel', 'groupName', 'securityName']
								},
								{
									fieldLabel: 'Holding Account', xtype: 'combo', name: 'holdingAccount', hiddenName: 'holdingAccountId',
									url: 'investmentAccountListFind.json?ourAccount=false', pageSize: 10, displayField: 'label', qtip: 'Filter the rebuild by selected holding account',
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										combo.store.baseParams = {};
										const atv = f.findField('holdingAccountTypeId').getValue();
										if (atv) {
											combo.store.baseParams.accountTypeId = atv;
										}
										const hcv = f.findField('holdingCompanyId').getValue();
										if (hcv) {
											combo.store.baseParams.issuingCompanyId = hcv;
										}
									},
									mutuallyExclusiveFields: ['accountLabel', 'groupName', 'securityName']
								},
								{
									fieldLabel: 'Security', name: 'securityName', hiddenName: 'investmentSecurityId', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json',
									mutuallyExclusiveFields: ['accountLabel', 'holdingAccountTypeName', 'holdingCompany', 'groupName']
								}
							]
						}]
					},
					{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
					{
						columnWidth: .49,
						items: [{
							xtype: 'formfragment',
							frame: false,
							labelWidth: 150,
							items: [
								{fieldLabel: 'Start Date', name: 'startSnapshotDate', xtype: 'datefield', allowBlank: false},
								{fieldLabel: 'End Date', name: 'endSnapshotDate', xtype: 'datefield', allowBlank: false},
								{fieldLabel: '', boxLabel: 'Allow Today Snapshots', name: 'allowTodaySnapshots', xtype: 'checkbox', qtip: 'By default the last snapshot allowed is for the previous day.  Checking this box will allow you to rebuild snapshots for today.'},
								{fieldLabel: '', boxLabel: 'Allow Future Snapshots', name: 'allowFutureSnapshots', xtype: 'checkbox', qtip: 'By default the last snapshot allowed is for the previous day.  Checking this box will allow you to rebuild snapshots for the future (limit of no more than 60 days).'}
							]
						}]
					}
				]
			}],
			buttons: [{
				text: 'Rebuild Positions',
				iconCls: 'run',
				width: 150,
				handler: function() {
					const owner = this.findParentByType('formpanel');
					const form = owner.getForm();
					form.submit(Ext.applyIf({
						url: 'accountingPositionDailyRebuild.json?UI_SOURCE=AccountingShared',
						waitMsg: 'Rebuilding...',
						success: function(form, action) {
							Ext.Msg.alert('Processing Started', action.result.data.message, function() {
								const grid = owner.items.get(1);
								grid.reload.defer(300, grid);
							});
						}
					}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
				}
			}]
		},

		{
			xtype: 'core-scheduled-runner-grid',
			typeName: 'POSITION-DAILY',
			instantRunner: true,
			title: 'Current Processing',
			frame: true,
			height: 250
		}
	]
};


Clifton.accounting.booking.BookingDateField = Ext.extend(TCG.form.LinkField, {
	sourceTable: 'REQUIRED-SOURCE-TABLE-NAME',
	fieldLabel: 'Booking Date',
	name: 'bookingDate',
	journalSequence: 1, // if more than one journals can be returned
	detailIdField: 'id',
	detailPageClass: 'Clifton.accounting.journal.JournalWindow',
	submitDetailField: false,
	type: 'date',

	getDetailIdFieldValue: function(formPanel) {
		const sourceId = formPanel.getForm().findField(this.detailIdField, true).getValue();
		const journal = TCG.data.getData('accountingJournalBySourceAndSequence.json?requestedMaxDepth=2&sourceTable=' + this.sourceTable + '&sourceId=' + sourceId + '&journalSequence=' + this.journalSequence, formPanel);
		return journal ? journal.id : null;
	}
});
Ext.reg('accounting-bookingdate', Clifton.accounting.booking.BookingDateField);


Clifton.accounting.BookingToolbar = Ext.extend(TCG.toolbar.Toolbar, {
	journalTypeName: 'CHANGE-ME',

	initComponent: function() {
		Clifton.workflow.Toolbar.superclass.initComponent.apply(this, arguments);
		const tb = this;
		this.add({
			text: 'Book',
			tooltip: 'Book this ' + tb.journalTypeName,
			iconCls: 'book-open-blue',
			disabled: true,
			handler: function() {
				Ext.Msg.confirm('Book ' + tb.journalTypeName, 'Would you like to book this ' + tb.journalTypeName + '?', function(a) {
					if (a === 'yes') {
						tb.bookJournal(false);
					}
				});
			}
		});
		this.add('-');
		this.add({
			text: 'Book and Post',
			tooltip: 'Book and Post this ' + tb.journalTypeName,
			iconCls: 'book-red',
			disabled: true,
			handler: function() {
				Ext.Msg.confirm('Book and Post ' + tb.journalTypeName, 'Would you like to book this ' + tb.journalTypeName + ' and immediately post it to the General Ledger?', function(a) {
					if (a === 'yes') {
						tb.bookJournal(true);
					}
				});
			}
		});
	},

	onRender: function() {
		Clifton.workflow.Toolbar.superclass.onRender.apply(this, arguments);
		const fp = this.getFormPanel();
		fp.on('afterload', function() {
			fp.ownerCt.getTopToolbar().setDisabled(TCG.isNotBlank(fp.getFormValue('bookingDate')));
		}, fp);
	},

	getFormPanel: function() {
		return this.ownerCt.items.get(0);
	},

	bookJournal: function(postJournal) {
		const fp = this.getFormPanel();
		const f = fp.getForm();
		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			waitMsg: 'Booking...',
			params: {
				journalTypeName: this.journalTypeName,
				sourceEntityId: f.formValues.id,
				postJournal: postJournal,
				autoAdjust: false
			},
			timeout: 180000,
			onLoad: function(record, conf) {
				const bd = f.findField('bookingDate');
				const v = TCG.renderDate(record.createDate);
				bd.setRawValue(v);
				bd.originalValue = v;
				fp.ownerCt.getTopToolbar().disable();
				fp.getWindow().savedSinceOpen = true;
			}
		});
		loader.load('accountingJournalBook.json?enableOpenSessionInView=true');
	}
});
Ext.reg('accounting-booking-toolbar', Clifton.accounting.BookingToolbar);


Clifton.accounting.commission.SwapCommissionsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'accountingCommissionDefinitionListFind',
	instructions: 'Commissions and termination fees specific to this swap are listed here. A full list of commissions and fees can be viewed in Operations > Commissions and Fees.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'GL Account', width: 140, dataIndex: 'expenseAccountingAccount.label', filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json?commission=true'}},
		{
			header: 'Transaction Type', width: 100, dataIndex: 'transactionType.name', filter: {type: 'combo', searchFieldName: 'accountingTransactionTypeId', displayField: 'name', url: 'accountingTransactionTypeListFind.json', loadAll: true}
		},
		{
			header: 'Commission Type', width: 100, dataIndex: 'commissionType',
			renderer: function(value, metaData, r) {
				return Clifton.accounting.commission.CommissionTypes[value].displayName;
			}
		},
		{
			header: 'Apply On', width: 90, dataIndex: 'commissionApplyMethod',
			renderer: function(value, metaData, r) {
				return Clifton.accounting.commission.CommissionApplyMethods[value].displayName;
			}
		},
		{header: 'Amount', width: 60, dataIndex: 'commissionAmount', type: 'float'},
		{
			header: 'Calc Method', width: 100, dataIndex: 'commissionCalculationMethod',
			renderer: function(value, metaData, r) {
				return Clifton.accounting.commission.CommissionCalculationMethods[value].displayName;
			}
		},
		{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean', sortable: false, hidden: true} //Don't allow sorting on this field (not supported)
	],
	getLoadParams: function() {
		return {investmentSecurityId: this.getWindow().getMainFormId()};
	},
	editor: {
		detailPageClass: 'Clifton.accounting.commission.CommissionWindow',
		addToolbarAddButton: function(toolBar) {
			const gridPanel = this.getGridPanel();
			const editor = this;

			const menu = new Ext.menu.Menu();
			menu.add({
				text: 'Commission', iconCls: 'fee', className: 'Clifton.accounting.commission.CommissionWindow', handler: function() {
					editor.defaultData = gridPanel.getCommissionDefaults();
					TCG.createComponent('Clifton.accounting.commission.CommissionWindow', {
						defaultData: gridPanel.getCommissionDefaults(),
						enabledFields: gridPanel.enabledCommissionFields,
						openerCt: gridPanel
					});
				}
			});

			menu.add({
				text: 'Termination Fee', iconCls: 'fee', className: 'Clifton.accounting.commission.CommissionWindow', handler: function() {
					editor.defaultData = gridPanel.getTerminationFeeDefaults();
					TCG.createComponent('Clifton.accounting.commission.CommissionWindow', {
						defaultData: gridPanel.getTerminationFeeDefaults(),
						enabledFields: gridPanel.enabledCommissionFields,
						openerCt: gridPanel
					});
				}
			});

			toolBar.add({
				text: 'Add',
				tooltip: 'Add a new item',
				iconCls: 'add',
				menu: menu
			});

			toolBar.add('-');
		},


		openDetailPage: function(className, gridPanel, id, row, itemText) {
			const defaultData = id ? this.getDefaultDataForExisting(gridPanel, row, className, itemText) : this.getDefaultData(gridPanel, row, className, itemText);
			if (defaultData === false) {
				return;
			}

			const params = id ? this.getDetailPageParams(id) : undefined;
			const cmpId = TCG.getComponentId(className, id);
			TCG.createComponent(className, {
				id: cmpId,
				defaultData: defaultData,
				enabledFields: gridPanel.enabledCommissionFields,
				params: params,
				openerCt: gridPanel,
				defaultIconCls: gridPanel.getWindow().iconCls
			});
		},

		getDefaultData: function(gridPanel) {
			return this.defaultData;
		}
	},

	enabledCommissionFields: ['commissionType', 'commissionAmount', 'commissionApplyMethod'],

	getCommissionDefaults: function() {
		const defaultVals = this.getCommissionDefinitionDefaults(this.getWindow().getMainFormId(), 'Commission');
		defaultVals.transactionType = 'Trade';
		defaultVals.commissionApplyMethod = 'ONLY_ON_CLOSE';
		defaultVals.commissionCalculationMethod = 'PER_NOTIONAL_BPS';

		return defaultVals;
	},
	getTerminationFeeDefaults: function() {
		const defaultVals = this.getCommissionDefinitionDefaults(this.getWindow().getMainFormId(), 'Termination Fee');
		defaultVals.transactionType = 'Early Termination';
		defaultVals.commissionApplyMethod = 'ONLY_ON_CLOSE';
		defaultVals.commissionCalculationMethod = 'PER_NOTIONAL_BPS';

		return defaultVals;
	},
	getCommissionDefinitionDefaults: function(securityId, accountingAccountName) {
		const security = TCG.data.getData('investmentSecurity.json?id=' + securityId, this);
		const accountingAccount = TCG.data.getData('accountingAccountByName.json?name=' + accountingAccountName, this);

		const defaults = {
			investmentSecurity: security,
			investmentInstrument: security.instrument,
			instrumentHierarchy: security.instrument.hierarchy,
			investmentType: security.instrument.hierarchy.investmentType,
			expenseAccountingAccount: accountingAccount,
			startDate: security.startDate,
			endDate: security.endDate
		};

		return defaults;
	}
});
Ext.reg('swap-commissions-grid', Clifton.accounting.commission.SwapCommissionsGrid);


Clifton.accounting.positiontransfer.TransferHeaderPanel = Ext.extend(TCG.form.FormPanelColumn, {
	columns: [
		{
			config: {columnWidth: 0.45},
			rows: [
				{fieldLabel: 'Transfer ID', name: 'id', xtype: 'displayfield'},
				{fieldLabel: 'Transfer Type', name: 'type.name', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.positiontransfer.TransferTypeWindow', detailIdField: 'type.id'},
				{fieldLabel: 'Violation Status', name: 'violationStatus.name', xtype: 'displayfield'}
			]
		},
		{
			config: {columnWidth: 0.25, labelWidth: 105},
			rows: [
				{
					fieldLabel: 'Price Date', name: 'exposureDate', xtype: 'datefield',
					qtip: 'Also known as Exposure Date.  For Portfolio reports, when populated, this is the date to reflect the transfer as exposure.  If blank, the transaction date is used.  For example, today\'s transfers should be reflected in Portfolio report from yesterday.',
					listeners: {
						change: function(field, newValue, oldValue) {
							const fp = TCG.getParentFormPanel(field);
							const f = fp.getForm();
							if (TCG.isNotBlank(newValue) && newValue !== oldValue) {
								// if value is set, move transaction date to date + 1 business day
								f.findField('transactionDate').setValue(Clifton.calendar.getBusinessDayFromDate(newValue, 1));
							}
						}
					}
				},
				{fieldLabel: 'Transaction Date', name: 'transactionDate', xtype: 'datefield'},
				{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'}
			]
		},
		{
			config: {columnWidth: 0.30, labelWidth: 110},
			rows: [
				{xtype: 'accounting-bookingdate', sourceTable: 'AccountingPositionTransfer'},
				{fieldLabel: 'Source Table', name: 'sourceSystemTable.name', xtype: 'linkfield', detailIdField: 'sourceSystemTable.id', detailPageClass: 'Clifton.system.schema.TableWindow'},
				{
					fieldLabel: 'Source FK Field ID', name: 'sourceFkFieldId', xtype: 'linkfield', detailIdField: 'sourceFkFieldId',
					qtip: 'For transfers that originated from a different part of the system (Collateral, etc.), allows to navigate back to the source entity that initiated this transfer.',
					getDetailPageClass: function(fp) {
						return TCG.getValue('sourceSystemTable.detailScreenClass', fp.getForm().formValues);
					}
				}
			]
		}
	]
});
Ext.reg('accounting-positiontransfer-header-panel', Clifton.accounting.positiontransfer.TransferHeaderPanel);

Clifton.accounting.gl.PositionBalanceGrid = Ext.extend(TCG.grid.GridPanel, {
	skipAccountFilter: false, //Adds the filter for Client Account
	skipAccountGroupFilter: false, //Adds the filter for Client Account Group
	skipAccountFiltersAll: false, //If set to true, will not add the Client Account or Client Account Group filters
	includeInvestmentGroupFilter: false,
	name: 'accountingPositionBalanceListFind',
	additionalPropertiesToRequest: 'id|clientInvestmentAccount.id|holdingInvestmentAccount.id|accountingAccount.id|investmentSecurity.id',
	requestIdDataIndex: true,
	appendStandardColumns: false,
	hiddenColumns: undefined, //an array columns to hide
	instructions: 'Position account balances on the specified date grouped by holding account, GL account and security.',
	storeRoot: 'data',
	groupField: 'clientInvestmentAccount.label',
	groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]}',
	customSecurityColumnsToShow: ['Spread', 'Reference Rate'],
	viewConfig: {markDirty: false},

	viewNames: ['By Client Account', 'By Holding Account', 'By Security', 'By Instrument'],
	configureViews: function(menu, gridPanel) {
		menu.add('-');
		menu.add({
			text: 'Include Pending Activity',
			tooltip: 'Include impact of pending trades, transfers, etc.',
			xtype: 'menucheckitem',
			handler: function(item, event) {
				gridPanel.pendingActivityRequest = !item.checked ? 'ALL_WITH_PREVIEW' : void 0;
				gridPanel.reload();
			}
		});
		menu.add({
			text: 'Include Collateral Positions',
			tooltip: 'Include Collateral Positions.',
			xtype: 'menucheckitem',
			handler: function(item, event) {
				gridPanel.includeCollateral = !item.checked;
				gridPanel.reload();
			}
		});
		menu.add({
			text: 'Include Collateral Receivable Positions',
			tooltip: 'Include Collateral Receivable Positions.',
			xtype: 'menucheckitem',
			handler: function(item, event) {
				gridPanel.includeCollateralReceivable = !item.checked;
				gridPanel.reload();
			}
		});


		const store = gridPanel.grid.getStore();
		if (!store.gridPanel) {
			store.gridPanel = this;
			store.on('load', this.applyDynamicColumnRecordData);
		}

		const executeWithMask = async function() {
			gridPanel.grid.loadMask.show();
			try {
				await gridPanel.applyDynamicColumnRecordData.call(gridPanel.grid.getStore());
			}
			finally {
				gridPanel.grid.loadMask.hide();
			}
		};

		menu.add('-',
			{
				text: 'Include Factor',
				tooltip: 'Include security factor column and value in the grid',
				xtype: 'menucheckitem',
				checked: gridPanel.includeFactor,
				handler: function(item, event) {
					gridPanel.includeFactor = !item.checked; // value is value before click, on click event
					executeWithMask();
				}
			},
			{
				text: 'Include Custom Security Columns',
				tooltip: 'Add custom security columns to the grid',
				xtype: 'menucheckitem',
				checked: gridPanel.includeCustomColumns,
				menu: {
					items: [],
					listeners: {
						itemclick: function(item, event) {
							this.childSelected = true;
						},
						beforehide: function() {
							const childSelected = TCG.isTrue(this.childSelected);
							this.childSelected = void 0; // clear childSelected
							return !childSelected; // hide if child not selected
						}
					}
				},
				name: 'customColumnViewItem',
				handler: function(item, event) {
					gridPanel.includeCustomColumns = !item.checked; // value is value before click, on click event
					executeWithMask();
				}
			},
			{
				text: 'Include Termination Fee',
				tooltip: 'Include security termination fee column and value in the grid',
				xtype: 'menucheckitem',
				checked: gridPanel.includeTerminationFee,
				handler: function(item, event) {
					gridPanel.includeTerminationFee = !item.checked; // value is value before click, on click event
					executeWithMask();
				}
			});
	},
	switchToViewBeforeReload: function(viewName) {
		if (this.grid && this.grid.getStore) {
			const store = this.grid.getStore();
			if (store) {
				let columnDataIndex = void 0;
				switch (viewName) {
					case 'By Client Account': {
						columnDataIndex = 'clientInvestmentAccount.label';
						break;
					}
					case 'By Holding Account': {
						columnDataIndex = 'holdingInvestmentAccount.label';
						break;
					}
					case 'By Security': {
						columnDataIndex = 'investmentSecurity.symbol';
						break;
					}
					case 'By Instrument': {
						columnDataIndex = 'investmentSecurity.instrument.label';
						break;
					}
					default: // do nothing
				}
				this.groupField = columnDataIndex;
				store.groupBy(columnDataIndex, false);
				store.setDefaultSort(columnDataIndex, 'asc');
			}
		}
	},
	columns: [
		{header: 'Client Account', width: 125, dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, viewNames: ['By Holding Account', 'By Security', 'By Instrument']},
		{header: 'Holding Account', width: 125, dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, viewNames: ['By Client Account', 'By Security', 'By Instrument']},
		{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.label', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json'}, hidden: true},
		{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 40, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json', showNotEquals: true}},
		{header: 'Security', width: 60, dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['By Client Account', 'By Holding Account', 'By Instrument']},
		{header: 'Instrument', width: 60, dataIndex: 'investmentSecurity.instrument.label', idDataIndex: 'investmentSecurity.instrument.id', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}, hidden: true},
		{header: 'Remaining Qty', dataIndex: 'remainingQuantity', width: 45, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
		{header: 'Unadjusted Qty', dataIndex: 'unadjustedQuantity', width: 45, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
		{header: 'Base Amount', dataIndex: 'remainingBaseDebitCredit', type: 'currency', width: 55, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
		{header: 'Local Cost Basis', dataIndex: 'remainingCostBasis', type: 'currency', width: 60, negativeInRed: true, positiveInGreen: true},
		{header: 'Underlying Instrument', width: 75, dataIndex: 'investmentSecurity.instrument.underlyingInstrument.label', filter: false, sortable: false, hidden: true},
		{header: 'Factor Type', width: '60', dataIndex: 'investmentSecurity.instrument.hierarchy.factorChangeEventType.name', filter: false, sortable: false, hidden: true},
		{header: 'OTC', width: 40, dataIndex: 'investmentSecurity.instrument.hierarchy.otc', type: 'boolean', filter: false, sortable: false, hidden: true},
		{header: 'Security One Per Instrument', dataIndex: 'investmentSecurity.instrument.hierarchy.oneSecurityPerInstrument', type: 'boolean', hidden: true}
	],
	plugins: [
		{ptype: 'gridsummary'},
		{ptype: 'accounting-position-balance-contextmenu-plugin'},
		{ptype: 'investment-contextmenu-plugin'}
	],

	//Grids that extend this can override this function to provide additional filters
	getCustomFilters: function(firstLoad) {
		const data = this.getWindow().defaultData;
		if (firstLoad) {
			if (data.viewName) {
				const view = this.viewNames.find(v => v === data.viewName);
				if (view) {
					this.setDefaultView(view);
				}
			}

			if (data.clientInvestmentAccount) {
				this.setFilterValue('clientInvestmentAccount.label', {equals: {value: data.clientInvestmentAccount.id, text: data.clientInvestmentAccount.label}}, false, true);
			}
			if (data.holdingInvestmentAccount) {
				this.setFilterValue('holdingInvestmentAccount.label', {equals: {value: data.holdingInvestmentAccount.id, text: data.holdingInvestmentAccount.label}}, false, true);
			}
			if (data.investmentSecurity) {
				if (data.investmentSecurity.id) {
					this.setFilterValue('investmentSecurity.symbol', {equals: {value: data.investmentSecurity.id, text: data.investmentSecurity.symbol}}, false, true);
				}
				else if (data.investmentSecurity.instrument) {
					this.setFilterValue('investmentSecurity.instrument.label', {equals: {value: data.investmentSecurity.instrument.id, text: data.investmentSecurity.instrument.label}}, false, true);
				}
			}
		}

		return {};
	},

	getDefaultPositionsDate: function() {
		return new Date().format('m/d/Y');
	},

	getLoadParams: function(firstLoad) {
		const params = this.getCustomFilters(firstLoad) || {};
		params.orderBy = 'clientInvestmentAccountId#investmentSecurityId';

		const tb = this.getTopToolbar();

		//Set the Instrument Group filter value
		if (this.includeInvestmentGroupFilter) {
			const investmentGroupId = TCG.getChildByName(tb, 'investmentGroupId').getValue();
			if (TCG.isNotBlank(investmentGroupId)) {
				params.investmentGroupId = investmentGroupId;
			}
		}

		//Set the account group filter value
		const accountGroupFilter = TCG.getChildByName(tb, 'clientInvestmentAccountGroupId');
		if (accountGroupFilter && TCG.isNotBlank(accountGroupFilter.getValue())) {
			params.clientInvestmentAccountGroupId = accountGroupFilter.getValue();
		}

		//Set the date filter value
		const dateFilter = TCG.getChildByName(tb, 'lookupDate');
		if (dateFilter) {
			if (TCG.isBlank(dateFilter.getValue())) {
				dateFilter.setValue(this.getDefaultPositionsDate());
			}
			params[TCG.getChildByName(tb, 'lookupDateType').getValue()] = dateFilter.getValue().format('m/d/Y');
		}

		if (!TCG.isBlank(this.pendingActivityRequest)) {
			params.pendingActivityRequest = this.pendingActivityRequest;
		}
		params.collateral = TCG.isTrue(this.includeCollateral) ? void 0 : false;
		params.excludeReceivableGLAccounts = TCG.isTrue(this.includeCollateralReceivable) ? void 0 : true;

		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		const grid = this;
		const filters = [];

		//Instrument Group filter (optional)
		if (this.includeInvestmentGroupFilter) {
			filters.push({fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'});
		}

		//Client Account Group and Client Account filters (optional)
		if (this.skipAccountFiltersAll) {
			this.skipAccountFilter = true;
			this.skipAccountGroupFilter = true;
		}

		if (!this.skipAccountFilter) {
			filters.push({fieldLabel: 'Client Acct', xtype: 'combo', name: 'clientInvestmentAccountId', displayField: 'label', linkedFilter: 'clientInvestmentAccount.label', width: 150, url: 'investmentAccountListFind.json?ourAccount=true'});
		}
		if (!this.skipAccountGroupFilter) {
			filters.push({fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'});
		}

		//Date filter (auto-reload)
		filters.push({
			name: 'lookupDateType', value: 'transactionDate', width: 110, minListWidth: 110, displayField: 'name', valueField: 'value', mode: 'local', xtype: 'toolbar-combo',
			store: {
				xtype: 'arraystore',
				fields: ['value', 'name', 'description'],
				data: [
					['transactionDate', 'Transaction Date', 'Aggregate General Ledger transactions using Transaction Date up to and including this date'],
					['settlementDate', 'Settlement Date', 'Aggregate General Ledger transactions using Settlement Date up to and including this date']
				]
			}
		});
		filters.push({xtype: 'toolbar-datefield', name: 'lookupDate', value: grid.getDefaultPositionsDate()});
		return filters;
	},

	modifySecurityCustomColumnViewMenuItems: function(gridPanel, remove) {
		const customColumnViewItems = gridPanel.getTopToolbar().findBy(item => item.text === 'Views')
			.filter(menu => menu.menu)
			.map(menu => TCG.getChildByName(menu.menu, 'customColumnViewItem'))
			.filter(item => item);
		if (customColumnViewItems[0]) {
			const customColumnMenu = customColumnViewItems[0].menu;
			if (TCG.isTrue(remove)) {
				customColumnMenu.removeAll();
			}
			else if (gridPanel.grid.customColumnsBySecurityId) {
				Object.values(gridPanel.grid.customColumnsBySecurityId).forEach(securityColumnDefinitions =>
					securityColumnDefinitions.forEach(columnDefinition => {
						const existing = customColumnMenu.findBy(i => i.text === columnDefinition.dataIndex);
						if (existing && existing.length < 1) {
							const menuDefinition = {
								text: columnDefinition.dataIndex,
								xtype: 'menucheckitem',
								checked: !columnDefinition.hidden,
								handler: function(item, event) {
									gridPanel.setColumnHidden(columnDefinition.dataIndex, item.checked);
								}
							};
							customColumnMenu.add(menuDefinition);
						}
					}));
			}
		}
	},

	applyDynamicColumnRecordData: async function() {
		const store = this;
		const gridPanel = store.gridPanel;
		const grid = gridPanel.grid;
		const securityToRowListMap = {};
		store.each(record => {
			const securityId = record.get('investmentSecurity.id');
			let rows = securityToRowListMap[securityId];
			if (!rows) {
				rows = [];
				securityToRowListMap[securityId] = rows;
			}
			rows.push(record);
		});

		const updateRecordColumnValue = function(record, columnName, columnValue) {
			if (record) {
				const currentValue = record.get(columnName);
				if (TCG.isNotEquals(currentValue, columnValue)) {
					if (!record.editing) {
						record.beginEdit();
					}
					record.set(columnName, columnValue);
				}
			}
		};

		// Batch all columns to add/remove so they can be added to the grid at the same time.
		// The grid will refresh/rerender upon each batch of addition/removal.
		const columnsToAdd = [];
		const columnsToRemove = [];
		const columnModel = gridPanel.grid.getColumnModel();
		const factorColumnExists = columnModel.findColumnIndex('factor') > -1;
		if (TCG.isTrue(gridPanel.includeFactor) && !factorColumnExists) {
			columnsToAdd.push({header: 'Factor', dataIndex: 'factor', width: 50, type: 'float', useNull: true, tooltip: 'The factor for the security of this row. The value will be blank if factor is not applicable.'});
		}
		else if (!TCG.isTrue(gridPanel.includeFactor) && factorColumnExists) {
			columnsToRemove.push('factor');
		}
		const feeColumnExists = columnModel.findColumnIndex('fees') > -1;
		if (TCG.isTrue(gridPanel.includeTerminationFee) && !feeColumnExists) {
			columnsToAdd.push({
				header: 'Termination Fee', dataIndex: 'fees', width: 50, type: 'float', useNull: true, tooltip: 'Early Termination fee for closing position',
				renderer: function(value, metadata, record) {
					return (value === 'NA') ? '' : TCG.renderValueWithTooltip(value, metadata);
				}
			});
		}
		else if (!TCG.isTrue(gridPanel.includeTerminationFee) && feeColumnExists) {
			columnsToRemove.push('fees');
		}

		const securityIds = Object.keys(securityToRowListMap);
		let lookupDate = new Date();
		const dateFilter = TCG.getChildByName(gridPanel.getTopToolbar(), 'lookupDate');
		if (dateFilter && TCG.isNotBlank(dateFilter.getValue())) {
			lookupDate = dateFilter.getValue();
		}

		for (const securityId of securityIds) {
			try {
				if (TCG.isTrue(gridPanel.includeFactor)) {
					const securityIdRows = securityToRowListMap[securityId];
					if (securityIdRows && securityIdRows.length > 0) {
						const eventTypeName = TCG.getValue('investmentSecurity.instrument.hierarchy.factorChangeEventType.name', securityIdRows[0].json);
						if (TCG.isNotBlank(eventTypeName)) {
							// look up factor for security
							const factor = await gridPanel.getSecurityFactor(securityId, lookupDate, gridPanel);
							securityIdRows.forEach(record => updateRecordColumnValue(record, 'factor', factor));
						}
					}
				}

				if (TCG.isTrue(gridPanel.includeCustomColumns)) {
					// Security Custom Columns
					const customColumnValues = await gridPanel.getSecurityCustomColumns(securityId, gridPanel);
					if (customColumnValues && customColumnValues.length > 0) {
						if (!grid.customColumnsBySecurityId) {
							grid.customColumnsBySecurityId = {};
						}
						if (!grid.customColumnsBySecurityId[securityId]) {
							const securityCustomColumns = [];
							grid.customColumnsBySecurityId[securityId] = securityCustomColumns;
							customColumnValues.forEach(customColumn => {
								const columnName = customColumn.column.name;
								const header = TCG.camelCaseToTitle(columnName.substring(columnName.lastIndexOf('.')));
								const hidden = !gridPanel.customSecurityColumnsToShow.includes(columnName);
								const gridColumn = {header: header, dataIndex: columnName, hidden: hidden, doNotRequest: true, type: gridPanel.getCustomColumnType(customColumn)};
								securityCustomColumns.push(gridColumn);
								if (!columnsToAdd.some(existingColumn => existingColumn.dataIndex === columnName)) {
									// add column if not exists
									columnsToAdd.push(gridColumn);
								}

								const columnValue = TCG.getValue('text', customColumn);
								securityToRowListMap[securityId].forEach(record => updateRecordColumnValue(record, columnName, columnValue));
							});
							// Add menu items to custom column view menu item
							gridPanel.modifySecurityCustomColumnViewMenuItems(gridPanel);
						}
					}
				}
				else if (grid.customColumnsBySecurityId) {
					let dataIndexes = [];
					Object.values(grid.customColumnsBySecurityId).forEach(securityCustomColumns =>
						securityCustomColumns.forEach(customColumn => dataIndexes.push(customColumn.dataIndex))
					);
					dataIndexes = [...new Set(dataIndexes)];
					columnsToRemove.push(...dataIndexes);
					grid.customColumnsBySecurityId = void 0;
					gridPanel.modifySecurityCustomColumnViewMenuItems(gridPanel, true);
				}

				if (gridPanel.includeTerminationFee) {
					const fees = await gridPanel.getSecurityFeesString(securityId, gridPanel);
					securityToRowListMap[securityId].forEach(record => updateRecordColumnValue(record, 'fees', fees));
				}
			}
			finally {
				// empty the edited records while ending edit for each. If there are columns to add/remove, we can
				// commit silently as the grid refresh on column edits will show the values on modified records.
				const commitSilently = (columnsToAdd.length > 0) || (columnsToRemove.length > 0);
				store.each(record => {
					if (record.editing) {
						if (commitSilently) {
							record.commit(true);
						}
						else {
							record.endEdit();
						}
					}
				});
				// adding columns will populate the record data
				if (columnsToAdd.length > 0) {
					grid.addColumn(...columnsToAdd);
					// clear to avoid adding columns again
					columnsToAdd.splice(0, columnsToAdd.length);
				}
				if (columnsToRemove.length > 0) {
					grid.removeColumn(...columnsToRemove);
					// clear to avoid removing columns again
					columnsToRemove.splice(0, columnsToRemove.length);
				}
			}
		}
	},

	getSecurityFactor: async function(securityId, valuationDate, scope = this) {
		return TCG.data.getFromCacheOrCompute(securityId, scope, 'factorCache', async () => {
			let factor = 1;
			const params = {
				securityId: securityId,
				typeName: (scope && scope.factorEventTypeName) ? scope.factorEventTypeName : 'Factor Change',
				valuationDate: (valuationDate ? valuationDate : new Date()).format('m/d/Y'),
				requestedPropertiesRoot: 'data',
				requestedProperties: 'afterEventValue'
			};
			const event = await TCG.data.getDataPromise('investmentSecurityEventWithLatestExDate.json', this, {params: params});
			if (event && TCG.isNotBlank(event.afterEventValue)) {
				factor = event.afterEventValue;
			}
			return factor;
		});
	},

	getSecurityCustomColumns: async function(securityId, scope) {
		return TCG.data.getFromCacheOrCompute(securityId, scope, 'columnCache', async () => {
			const params = {
				columnGroupName: 'Security Custom Fields',
				fkFieldId: securityId,
				requestedPropertiesRoot: 'data',
				requestedProperties: 'column.name|text|column.dataType.name',
				requestedPropertiesToExclude: 'systemColumnValueSearchForm'
			};
			return await TCG.data.getDataPromise('systemColumnValueListFind.json', this, {params: params});
		});
	},

	getSecurityFeesString: async function(securityId, scope) {
		return TCG.data.getFromCacheOrCompute(securityId, scope, 'feeCache', async () => {
			const params = {
				investmentSecurityId: securityId,
				accountingTransactionTypeName: 'Early Termination',
				active: true,
				limit: 1,
				requestedPropertiesRoot: 'data',
				requestedProperties: 'commissionApplyMethod|commissionAmount|commissionCalculationMethod',
				requestedPropertiesToExclude: 'accountingCommissionDefinitionSearchForm'
			};
			const fees = await TCG.data.getDataPromise('accountingCommissionDefinitionListFind.json', this, {params: params});
			if (fees && fees.length > 0) {
				return fees.map(fee => {
					const amount = TCG.getValue('commissionAmount', fee);
					let calculationMethod = TCG.getValue('commissionCalculationMethod', fee);
					if (Clifton.accounting.commission.CommissionCalculationMethods[calculationMethod]) {
						calculationMethod = Clifton.accounting.commission.CommissionCalculationMethods[calculationMethod].displayName;
					}
					let applyMethod = TCG.getValue('commissionApplyMethod', fee);
					if (Clifton.accounting.commission.CommissionApplyMethods[applyMethod]) {
						applyMethod = Clifton.accounting.commission.CommissionApplyMethods[applyMethod].displayName;
					}
					return `${amount} ${calculationMethod} ${applyMethod}`;
				}).join();
			}
			return 'NA';
		});
	},

	isSkipExport: function(column) {
		const customColumn = this.grid.customColumns && this.grid.customColumns.filter(c => c.dataIndex === column.dataIndex).length > 0;

		return customColumn || ['factor', 'fees'].includes(column.dataIndex);
	},

	getCustomColumnType: function(customColumn) {
		switch (customColumn.column.dataType.name) {
			case 'INTEGER': {
				return 'int';
			}
			case 'DECIMAL': {
				return 'float';
			}
			case 'BOOLEAN': {
				return 'boolean';
			}
			case 'DATE': {
				return 'date';
			}
			default: {
				return 'string';
			}
		}
	},

	editor: {
		detailPageClass: 'Clifton.accounting.gl.TransactionListWindow',
		drillDownOnly: true,
		getDefaultData: function(gridPanel, row) {
			const filters = [];
			const data = row.json;

			filters.push({
				field: 'clientInvestmentAccountId',
				comparison: 'EQUALS',
				value: data.clientInvestmentAccount.id
			});
			filters.push({
				field: 'holdingInvestmentAccountId',
				comparison: 'EQUALS',
				value: data.holdingInvestmentAccount.id
			});
			filters.push({
				field: 'accountingAccountId',
				comparison: 'EQUALS',
				value: data.accountingAccount.id
			});
			filters.push({
				field: 'investmentSecurityId',
				comparison: 'EQUALS',
				value: data.investmentSecurity.id
			});
			const nextDay = new Date(TCG.getChildByName(gridPanel.getTopToolbar(), 'lookupDate').value).add(Date.DAY, 1).format('m/d/Y');
			filters.push({
				field: TCG.getChildByName(gridPanel.getTopToolbar(), 'lookupDateType').getValue(),
				comparison: 'LESS_THAN',
				value: nextDay
			});
			return filters;
		}
	}
});
Ext.reg('accounting-positionBalance-gridpanel', Clifton.accounting.gl.PositionBalanceGrid);

Clifton.accounting.gl.PositionBalanceContextMenuPlugin = Ext.extend(Ext.util.Observable, {
	grid: null,

	constructor: function(config) {
		Object.assign(this, config);
		TCG.callSuper(this, 'constructor', arguments);
	},

	init: function(gridOrPanel) {
		// Unwrap grid panel if necessary
		this.grid = (gridOrPanel instanceof TCG.grid.GridPanel) ? gridOrPanel.grid : gridOrPanel;
		const positionBalancePlugin = this;
		gridOrPanel.addAccountPositionBalanceToolbarButton = positionBalancePlugin.addAccountPositionBalanceToolbarButton;
		gridOrPanel.getAccountPositionBalanceWindowDefaultData = positionBalancePlugin.getAccountPositionBalanceWindowDefaultData;

		const existingRowContextMenuItemsFunction = gridOrPanel.getGridRowContextMenuItems
			? gridOrPanel.getGridRowContextMenuItems : () => [];
		gridOrPanel.getGridRowContextMenuItems = async (...args) => {
			const menuItems = await existingRowContextMenuItemsFunction(...args) || [];
			const positionBalancePluginItems = await positionBalancePlugin.getPositionBalanceGridRowContextMenuItems(...args);
			if (positionBalancePluginItems && positionBalancePluginItems.length > 0) {
				if (menuItems.length > 0) {
					menuItems.push('-');
				}
				menuItems.push(...positionBalancePluginItems);
			}
			return menuItems;
		};
	},

	getPositionBalanceGridRowContextMenuItems: function(grid, rowIndex, record) {
		const menu = [];

		if (this.isNavigationAllowed(record)) {
			const navigationTypes = [];
			const defaultDataNavigation = grid.ownerGridPanel.getAccountPositionBalanceWindowDefaultData();
			const appendNavigationFunction = function(objectPath, label, windowName = 'Clifton.accounting.gl.AccountPositionBalanceWindow') {
				let object = TCG.getValue(objectPath, record.json);
				if (TCG.isNull(object) && defaultDataNavigation && defaultDataNavigation.viewName.includes(label)) {
					object = TCG.getValue(objectPath, defaultDataNavigation);
				}
				if (object && object.id) {
					navigationTypes.push(
						{
							text: `For ${label}`,
							tooltip: `Launch the Position Balance window for the ${label} of this row`,
							iconCls: 'chart-pie',
							handler: function() {
								const id = object.id;
								const defaultData = {viewName: `By ${label}`};
								const pathSegments = objectPath.split('.');
								let index = 0;
								let segmentObject = defaultData;
								while (index < pathSegments.length) {
									const segment = pathSegments[index];
									segmentObject[segment] = (index < pathSegments.length - 1) ? {} : object;
									segmentObject = segmentObject[segment];
									index++;
								}
								TCG.createComponent(windowName, {
									id: TCG.getComponentId(windowName, id),
									defaultData: defaultData,
									openerCt: this
								});
							}
						}
					);
				}
			};
			appendNavigationFunction('clientInvestmentAccount', 'Client Account');
			appendNavigationFunction('holdingInvestmentAccount', 'Holding Account');
			appendNavigationFunction('investmentSecurity', 'Security');

			if (navigationTypes.length > 0) {
				menu.push({
					text: 'View Position Balances',
					iconCls: 'chart-pie',
					menu: {
						xtype: 'menu',
						items: navigationTypes
					}
				});
			}
		}

		return menu;
	},

	isNavigationAllowed: function(record) {
		const accountingAccountName = TCG.getValue('accountingAccount.name', record.json);
		return accountingAccountName && accountingAccountName.includes('Position');
	},

	addAccountPositionBalanceToolbarButton: function(toolBar, gridPanel) {
		const defaultData = this.getAccountPositionBalanceWindowDefaultData();
		if (defaultData && Object.keys(defaultData).length > 0) {
			toolBar.add({
				text: 'Position Balances', iconCls: 'chart-pie', tooltip: 'Display position balances for the details of this window\'s account or security',
				handler: () =>
					TCG.createComponent('Clifton.accounting.gl.AccountPositionBalanceWindow', {
						defaultData: defaultData,
						openerCt: this
					})
			}, '-');
		}
	},

	getAccountPositionBalanceWindowDefaultData: function() {
		const window = this.getWindow ? this.getWindow() : void 0;
		if (window) {
			if (window.getMainForm) {
				const mainFormValues = window.getMainForm().formValues;
				const defaultData = {};
				if (mainFormValues['number']) {
					// account
					if (TCG.isTrue(TCG.getValue('type.ourAccount', mainFormValues))) {
						defaultData['clientInvestmentAccount'] = {id: mainFormValues.id, label: mainFormValues.label};
						defaultData.viewName = 'By Client Account';
					}
					else {
						defaultData['holdingInvestmentAccount'] = {id: mainFormValues.id, label: mainFormValues.label};
						defaultData.viewName = 'By Holding Account';
					}
				}
				else if (mainFormValues['symbol']) {
					// security
					defaultData['investmentSecurity'] = {id: mainFormValues.id, symbol: mainFormValues.symbol};
					defaultData.viewName = 'By Security';
				}
				return defaultData;
			}
		}
		return void 0;
	}
});
Ext.preg('accounting-position-balance-contextmenu-plugin', Clifton.accounting.gl.PositionBalanceContextMenuPlugin);

