Clifton.accounting.account.AccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'GL Account',
	iconCls: 'accounts',
	width: 650,
	height: 625,

	items: [{
		xtype: 'formpanel',
		url: 'accountingAccount.json',
		labelWidth: 170,
		items: [
			{fieldLabel: 'Account Type', name: 'accountType.name', hiddenName: 'accountType.id', xtype: 'combo', url: 'accountingAccountTypeList.json', loadAll: true},
			{fieldLabel: 'GL Account Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true, height: 35},
			{fieldLabel: 'Auto Reversal Account', name: 'autoAccrualReversalOffsetAccount.label', hiddenName: 'autoAccrualReversalOffsetAccount.id', displayField: 'label', xtype: 'combo', url: 'accountingAccountListFind.json', qtip: 'Automatically reverses GL Account balance (makes it 0) and replaces it with this GL Account. Does this on Position Closing only.'},
			{fieldLabel: 'Unrealized Currency Account', name: 'unrealizedCurrencyAccount.label', hiddenName: 'unrealizedCurrencyAccount.id', displayField: 'label', xtype: 'combo', url: 'accountingAccountListFind.json?currency=true&unrealizedGainLoss=true', qtip: 'Non Cash Currency accounts must point to corresponding unrealized currency gain/loss accounts.'},
			{fieldLabel: 'Cash Account', name: 'cashAccount.label', hiddenName: 'cashAccount.id', displayField: 'label', xtype: 'combo', url: 'accountingAccountListFind.json?cash=true', qtip: 'The cash account that should be used when creating offsetting entries for this accounting account.'},
			{fieldLabel: 'Receivable Account', name: 'receivableAccount.label', hiddenName: 'receivableAccount.id', displayField: 'label', xtype: 'combo', url: 'accountingAccountListFind.json?receivable=true', qtip: 'The receivable account that should be used when generating receivables.'},
			{fieldLabel: 'Position', name: 'position', xtype: 'checkbox', boxLabel: ' (tracked at Lot-Level and supports Open/Closed lot concepts)'},
			{fieldLabel: 'Currency', name: 'currency', xtype: 'checkbox'},
			{fieldLabel: 'Cash', name: 'cash', xtype: 'checkbox', boxLabel: ' (base Currency of Client Account)'},
			{fieldLabel: 'Collateral', name: 'collateral', xtype: 'checkbox'},
			{fieldLabel: 'Receivable', name: 'receivable', xtype: 'checkbox'},
			{fieldLabel: 'Gain/Loss', name: 'gainLoss', xtype: 'checkbox'},
			{fieldLabel: 'Unrealized Gain/Loss', name: 'unrealizedGainLoss', xtype: 'checkbox'},
			{fieldLabel: 'Currency Translation', name: 'currencyTranslation', xtype: 'checkbox', boxLabel: ' (Gain/Loss from change in FX rate)'},
			{fieldLabel: 'Commission/Fee', name: 'commission', xtype: 'checkbox', boxLabel: ' (Commission OR Fee)'},
			{fieldLabel: 'Fee', name: 'fee', xtype: 'checkbox', boxLabel: ' (only Fee and NOT Commission)'},
			{fieldLabel: 'Executing Broker Specific', name: 'executingBrokerSpecific', xtype: 'checkbox', boxLabel: ' (Position can only be closed by the same broker that opened it)'},
			{fieldLabel: 'Posting Not Allowed', name: 'postingNotAllowed', xtype: 'checkbox', boxLabel: ' (cannot be posted to the General Ledger)'},
			{fieldLabel: 'Not Our Account', name: 'notOurAccount', xtype: 'checkbox', boxLabel: ' (exclude from our client\'s assets)'}
		]
	}]
});
