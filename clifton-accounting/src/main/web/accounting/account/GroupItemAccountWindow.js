Clifton.accounting.account.GroupItemAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'GL Account Group Item Account',
	iconCls: 'grouping',

	items: [{
		xtype: 'formpanel',
		instructions: 'A GL Account Item Account assigns a GL Account to a specific Group Item.',
		url: 'accountingAccountGroupItemAccountingAccount.json',
		items: [
			{fieldLabel: 'Group', name: 'referenceOne.group.name', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.account.GroupWindow', detailIdField: 'referenceOne.group.id', submitValue: false},
			{
				fieldLabel: 'Group Item', name: 'referenceOne.name', hiddenName: 'referenceOne.id', xtype: 'combo', detailPageClass: 'Clifton.accounting.account.GroupItemWindow',
				url: 'accountingAccountGroupItemListFind.json',
				beforequery: function(queryEvent) {
					const cmb = queryEvent.combo;
					const f = TCG.getParentFormPanel(cmb).getForm();
					cmb.store.baseParams = {
						groupId: f.findField('referenceOne.group.id').getValue()
					};
				}
			},
			{fieldLabel: 'GL Account', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', displayField: 'label', xtype: 'combo', url: 'accountingAccountListFind.json'},
			{boxLabel: 'Credit Minus Debit', name: 'creditMinusDebit', xtype: 'checkbox'}
		]
	}]
});
