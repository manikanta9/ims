Clifton.accounting.account.AccountListWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingAccountListWindow',
	title: 'GL Accounts',
	iconCls: 'accounts',
	width: 1300,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'GL Accounts',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingAccountListFind',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Chart of accounts is a list of all accounting accounts available in the system.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'GL Account Name', dataIndex: 'name', width: 120},
						{header: 'Account Type', dataIndex: 'accountType.name', width: 60, filter: {searchFieldName: 'accountType'}, defaultSortColumn: true},
						{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
						{header: 'Auto Reversal Account', dataIndex: 'autoAccrualReversalOffsetAccount.label', width: 100, tooltip: 'Automatically reverses GL Account balance (makes it 0) and replaces it with this GL Account. Does this on Position Closing only.', hidden: true},
						{header: 'Unrealized Currency Account', dataIndex: 'unrealizedCurrencyAccount.label', width: 100, tooltip: 'Non Cash Currency accounts must point to corresponding unrealized currency gain/loss accounts.', hidden: true},
						{header: 'Cash Account', dataIndex: 'cashAccount.label', width: 100, tooltip: 'The cash account that should be used when creating offsetting entries for this accounting account.', hidden: true},
						{header: 'Receivable Account', dataIndex: 'receivableAccount.label', width: 100, tooltip: 'The receivable account that should be used when generating receivables.', hidden: true},
						{header: 'Position', dataIndex: 'position', width: 50, type: 'boolean'},
						{header: 'Currency', dataIndex: 'currency', width: 50, type: 'boolean'},
						{header: 'Cash', dataIndex: 'cash', width: 40, type: 'boolean'},
						{header: 'Collateral', dataIndex: 'collateral', width: 50, type: 'boolean'},
						{header: 'Receivable', dataIndex: 'receivable', width: 50, type: 'boolean'},
						{header: 'Gain/Loss', dataIndex: 'gainLoss', width: 50, type: 'boolean'},
						{header: 'Unrealized', dataIndex: 'unrealizedGainLoss', width: 50, type: 'boolean', tooltip: 'Unrealized Gain/Loss'},
						{header: 'Currency Translation', dataIndex: 'currencyTranslation', width: 50, type: 'boolean', hidden: true},
						{header: 'Commission', dataIndex: 'commission', width: 50, type: 'boolean', tooltip: 'Commission OR Fee'},
						{header: 'Fee', dataIndex: 'fee', width: 40, type: 'boolean', tooltip: 'Only Fee and NOT Commission'},
						{header: 'Broker Specific', dataIndex: 'executingBrokerSpecific', width: 50, type: 'boolean', tooltip: 'Specifies whether a position can only be closed by the same broker that opened it.', hidden: true},
						{header: 'No Posting', dataIndex: 'postingNotAllowed', width: 50, type: 'boolean'},
						{header: 'Not Our', dataIndex: 'notOurAccount', width: 50, type: 'boolean', tooltip: 'Specifies the items in this accounting account should be excluded from assets.'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.account.AccountWindow'
					}
				}]
			},


			{
				title: 'GL Account Types',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingAccountTypeList',
					instructions: 'Standard GL account types and corresponding attributes.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Type Name', dataIndex: 'name', width: 100},
						{header: 'Description', dataIndex: 'description', width: 200},
						{header: 'Balance Sheet', dataIndex: 'balanceSheetAccount', width: 60, type: 'boolean'},
						{header: 'Debit Growth', dataIndex: 'growingOnDebitSide', width: 60, type: 'boolean'},
						{header: 'Order', dataIndex: 'accountingAccountTypeOrder', width: 40, type: 'int'}
					]
				}]
			},


			{
				title: 'GL Account Groups',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingAccountGroupListFind',
					instructions: 'GL account groups.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Group Name', dataIndex: 'name', width: 100, filter: {searchField: 'searchPattern'}},
						{header: 'Description', dataIndex: 'description', width: 300, filter: false},
						{header: 'System Defined', dataIndex: 'systemDefined', width: 50, type: 'boolean'},
						{header: 'Duplication Allowed', dataIndex: 'duplicationAllowed', width: 50, type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.account.GroupWindow'
					}
				}]
			}
		]
	}]
});
