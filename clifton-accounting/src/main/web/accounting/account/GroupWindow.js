Clifton.accounting.account.GroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'GL Account Group',
	iconCls: 'grouping',
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Group',
				items: [{
					xtype: 'formpanel',
					instructions: 'GL Account groups classifies GL Accounts for reporting purposes.',
					url: 'accountingAccountGroup.json',
					items: [
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{boxLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true},
						{boxLabel: 'Allow the same GL Account to be assigned to more than one group item', name: 'duplicationAllowed', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Items',
				items: [{
					name: 'accountingAccountGroupItemListFind',
					xtype: 'gridpanel',
					instructions: 'The following items are associated with this group. Group Items are used to classify GL Accounts for reporting purposes.',
					columns: [
						{header: 'Item Name', width: 250, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Order', width: 70, dataIndex: 'itemOrder', type: 'int', useNull: true},
						{header: 'Unrealized Included', width: 70, dataIndex: 'unrealizedIncluded', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.account.GroupItemWindow',
						getDefaultData: function(gridPanel) { // defaults client account for the detail page
							return {
								group: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {'groupId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'GL Account Assignments',
				items: [{
					name: 'accountingAccountGroupItemAccountingAccountListFind',
					xtype: 'gridpanel',
					instructions: 'The following accounts assignments are for this group.',
					getLoadParams: function() {
						return {'groupId': this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Group Item', dataIndex: 'referenceOne.name', width: 150},
						{header: 'GL Account', dataIndex: 'referenceTwo.name', width: 150},
						{header: 'Account Type', dataIndex: 'referenceTwo.accountType.name', width: 60},
						{header: 'Credit Minus Debit', dataIndex: 'creditMinusDebit', width: 50, type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.account.GroupItemAccountWindow',
						getDefaultData: function(gridPanel) { // defaults client account for the detail page
							return {
								referenceOne: {group: gridPanel.getWindow().getMainForm().formValues}
							};
						}
					}
				}]
			},


			{
				title: 'Missing GL Accounts',
				items: [{
					name: 'accountingAccountListFind',
					xtype: 'gridpanel',
					instructions: 'The following accounts are missing assignments from this group.',
					getLoadParams: function() {
						return {excludedFromAccountGroupId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'GL Account', dataIndex: 'name', width: 120},
						{header: 'Type', dataIndex: 'accountType.name', width: 60, filter: {searchFieldName: 'accountType'}, defaultSortColumn: true},
						{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
						{header: 'Position', dataIndex: 'position', width: 50, type: 'boolean'},
						{header: 'Currency', dataIndex: 'currency', width: 50, type: 'boolean'},
						{header: 'Cash', dataIndex: 'cash', width: 40, type: 'boolean', hidden: true},
						{header: 'Collateral', dataIndex: 'collateral', width: 50, type: 'boolean'},
						{header: 'Receivable', dataIndex: 'receivable', width: 50, type: 'boolean'},
						{header: 'Gain/Loss', dataIndex: 'gainLoss', width: 50, type: 'boolean'},
						{header: 'Commission', dataIndex: 'commission', width: 50, type: 'boolean'},
						{header: 'Fee', dataIndex: 'fee', width: 40, type: 'boolean', tooltip: 'Only Fee and NOT Commission'},
						{header: 'No Posting', dataIndex: 'postingNotAllowed', width: 50, type: 'boolean'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.accounting.account.AccountWindow'
					}
				}]
			}
		]
	}]
});

