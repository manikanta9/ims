Clifton.accounting.account.GroupItemWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'GL Account Group Item',
	iconCls: 'grouping',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Group Item',
				items: [{
					xtype: 'formpanel',
					instructions: 'Group Items are used to classify GL Accounts for reporting purposes.',
					url: 'accountingAccountGroupItem.json',
					items: [
						{fieldLabel: 'Group', name: 'group.name', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.account.GroupWindow', detailIdField: 'group.id'},
						{fieldLabel: 'Item Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Order', name: 'itemOrder', xtype: 'spinnerfield', minValue: 1, maxValue: 1000},
						{boxLabel: 'Unrealized Included', name: 'unrealizedIncluded', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'GL Account Assignments',
				items: [{
					name: 'accountingAccountGroupItemAccountingAccountListFind',
					xtype: 'gridpanel',
					instructions: 'The following GL Accounts are tied to this group item.',
					columns: [
						{header: 'GL Account', dataIndex: 'referenceTwo.label', width: 200, filter: {type: 'combo', searchFieldName: 'accountId', displayField: 'label', url: 'accountingAccountListFind.json'}, defaultSortColumn: true},
						{header: 'Credit Minus Debit', dataIndex: 'creditMinusDebit', width: 70, type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.account.GroupItemAccountWindow',
						getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
							return {
								referenceOne: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {
							groupItemId: this.getWindow().getMainFormId()
						};
					}
				}]
			}
		]
	}]
});
