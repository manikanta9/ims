Clifton.accounting.positiontransfer.PositionSelectWindow = Ext.extend(TCG.app.SelectWindow, {
	title: 'Select Position(s)',
	iconCls: 'book-red',
	width: 1050,
	height: 500,

	items: [{
		name: 'accountingPositionList',
		xtype: 'gridpanel',
		instructions: 'Select one or more positions below that are currently open.',
		rowSelectionModel: 'checkbox',
		additionalPropertiesToRequest: 'investmentSecurity.id',
		forceLocalFiltering: true,
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Security', width: 70, dataIndex: 'investmentSecurity.symbol', defaultSortColumn: true},
			{header: 'Hierarchy', width: 130, dataIndex: 'investmentSecurity.instrument.hierarchy.nameExpanded'},
			{header: 'GL Account', width: 50, dataIndex: 'accountingAccount.name'},
			{header: 'Open Date', width: 50, dataIndex: 'originalTransactionDate'},
			{header: 'Open Price', width: 60, dataIndex: 'price', type: 'float', negativeInRed: true, useNull: true},
			{header: 'FX Rate', width: 50, dataIndex: 'exchangeRateToBase', type: 'currency', numberFormat: '0,000.0000000000', negativeInRed: true},
			{header: 'Open Qty', width: 50, dataIndex: 'quantity', type: 'float', negativeInRed: true, useNull: true},
			{header: 'Remaining Qty', width: 60, dataIndex: 'remainingQuantity', type: 'float', negativeInRed: true, summaryType: 'sum'},
			{header: 'Remaining Cost Basis', width: 80, dataIndex: 'remainingCostBasis', type: 'currency', negativeInRed: true, summaryType: 'sum'}
		],
		plugins: {ptype: 'gridsummary'},
		getLoadParams: function() {
			const params = this.getWindow().params;
			return {
				clientInvestmentAccountId: params.clientAccountId,
				holdingInvestmentAccountId: params.holdingAccountId,
				transactionDate: params.transactionDate
			};
		}
	}]
});
