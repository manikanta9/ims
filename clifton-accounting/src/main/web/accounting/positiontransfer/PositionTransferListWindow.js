Clifton.accounting.positiontransfer.PositionTransferListWindow = Ext.extend(TCG.app.Window, {
	id: 'positionTransferListWindow',
	title: 'Position Transfers',
	iconCls: 'transfer',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Pending Transfers',
				items: [{
					name: 'accountingPositionTransferListFind',
					xtype: 'gridpanel',
					instructions: 'Position transfers are used to transfer existing positions (use Simple Journals for cash movements) between investment accounts. Pending transfers are transfers that have not been booked yet.',
					importTableName: 'AccountingPositionTransfer',
					importComponentName: 'Clifton.accounting.positiontransfer.upload.PositionTransferUploadWindow',
					columns: [
						{header: 'Transfer ID', width: 50, dataIndex: 'id'},
						{header: 'Transfer Type', width: 130, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'transferTypeId', url: 'accountingPositionTransferTypeListFind.json'}},
						{header: 'From (Client Account)', width: 180, dataIndex: 'fromClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'From (Holding Account)', width: 180, dataIndex: 'fromHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'To (Client Account)', width: 180, dataIndex: 'toClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'To (Holding Account)', width: 180, dataIndex: 'toHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'Collateral', width: 50, dataIndex: 'collateralTransfer', type: 'boolean'},
						{header: 'Note', width: 200, dataIndex: 'note', hidden: true},
						{
							header: 'Violation Status', width: 80, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
							renderer: function(v, p, r) {
								return (Clifton.rule.violation.renderViolationStatus(v));
							}
						},
						{header: 'FK Field ID', width: 50, dataIndex: 'sourceFkFieldId', useNull: true, hidden: true, tooltip: 'Source Entity ID for when this transfer was initiated from a different part of the system'},
						{header: 'Price Date', width: 50, dataIndex: 'exposureDate', hidden: true},
						{header: 'Date', width: 50, dataIndex: 'transactionDate'},
						{header: 'Settlement Date', width: 50, dataIndex: 'settlementDate', hidden: true}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', url: 'investmentAccountGroupListFind.json'}
						];
					},
					getLoadParams: function(firstLoad) {
						const params = {journalNotBooked: true},
							topToolbar = this.getTopToolbar();

						const clientInvestmentAccountGroupId = TCG.getChildByName(topToolbar, 'clientInvestmentAccountGroupId').getValue();
						if (TCG.isNotBlank(clientInvestmentAccountGroupId)) {
							params.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
						}
						const investmentGroupId = TCG.getChildByName(topToolbar, 'investmentGroupId').getValue();
						if (TCG.isNotBlank(investmentGroupId)) {
							params.investmentGroupId = investmentGroupId;
						}

						return params;
					},
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								return '';
							},
							items: [
								{
									text: 'Transfer To Our Account', iconCls: 'arrow-left-green', className: 'Clifton.accounting.positiontransfer.TransferToWindow',
									tooltip: 'Create a transfer to our account by defining the positions being transfer into the system.'
								},
								{
									text: 'Transfer From Our Account', iconCls: 'arrow-right-red', className: 'Clifton.accounting.positiontransfer.TransferFromWindow',
									tooltip: 'Create a transfer from our account by selecting lot positions.'
								},
								{
									text: 'Transfer Between Our Accounts', iconCls: 'transfer', className: 'Clifton.accounting.positiontransfer.TransferBetweenWindow',
									tooltip: 'Create a transfer between our accounts by selecting lot positions.'
								},
								{xtype: 'label', html: '<hr />'},
								{
									text: 'Transfer With Auto-Select Lots', iconCls: 'run', className: 'Clifton.accounting.positiontransfer.TransferFromAutoSelectWindow',
									tooltip: 'Create transfer(s) by specifying search criteria to enable the system to automatically select lot position(s).'
								}
							]
						},
						getDetailPageClassByText: function(text) {
							return 'Clifton.accounting.positiontransfer.TransferWindow';
						},
						getDefaultData: function() { // default to today
							return {
								settlementDate: new Date().format('Y-m-d 00:00:00'),
								transactionDate: new Date().format('Y-m-d 00:00:00')
							};
						},
						getDeleteURL: function() {
							return 'accountingPositionTransferDelete.json';
						},
						addEditButtons: function(toolBar, gridPanel) {
							this.addToolbarAddButton(toolBar, gridPanel);
							this.addToolbarDeleteButton(toolBar, gridPanel);
							toolBar.add({
								text: 'Book',
								tooltip: 'Book selected transfer',
								iconCls: 'book-open-blue',
								scope: this,
								handler: function() {
									this.bookSelectedTransfer(false);
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Book and Post',
								tooltip: 'Book selected transfer and immediately post it to the General Ledger',
								iconCls: 'book-red',
								scope: this,
								handler: function() {
									this.bookSelectedTransfer(true);
								}
							});
							toolBar.add('-');
						},
						bookSelectedTransfer: function(post) {
							let confirmTitle = 'Book Transfer';
							let confirmMessage = 'Would you like to book selected transfer?';
							if (post) {
								confirmTitle = 'Book & Post Transfer';
								confirmMessage = 'Would you like to book selected transfer and immediately post it to the General Ledger?';
							}

							const grid = this.grid;
							const sm = grid.getSelectionModel();
							if (TCG.isEquals(sm.getCount(), 0)) {
								TCG.showError('Please select a row to be booked.', 'No Row(s) Selected');
							}
							else if (TCG.isNotEquals(sm.getCount(), 1)) {
								TCG.showError('Multi-selection bookings are not supported yet.  Please select one row.', 'NOT SUPPORTED');
							}
							else {
								Ext.Msg.confirm(confirmTitle, confirmMessage, function(a) {
									if (TCG.isEquals(a, 'yes')) {
										const loader = new TCG.data.JsonLoader({
											waitTarget: grid,
											waitMsg: post ? 'Booking and Posting...' : 'Booking...',
											params: {
												journalTypeName: 'Transfer Journal',
												sourceEntityId: sm.getSelected().id,
												postJournal: post
											},
											timeout: 180000,
											onLoad: function(record, conf) {
												grid.ownerGridPanel.reload();
											}
										});
										loader.load('accountingJournalBook.json?enableOpenSessionInView=true&requestedPropertiesRoot=data&requestedProperties=id');
									}
								});
							}
						}
					}
				}]
			},


			{
				title: 'Booked Transfers',
				items: [{
					name: 'accountingPositionTransferListFind',
					xtype: 'gridpanel',
					instructions: 'Position transfers are used to transfer existing positions (use Simple Journals for cash movements) between investment accounts. Booked transfers create actual accounting journals that get posted to the General Ledger and can no longer be modified.',
					columns: [
						{header: 'Transfer ID', width: 60, dataIndex: 'id'},
						{header: 'Transfer Type', width: 170, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'transferTypeId', displayField: 'label', url: 'accountingPositionTransferTypeListFind.json'}},
						{header: 'From (Client Account)', width: 180, dataIndex: 'fromClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'From (Holding Account)', width: 180, dataIndex: 'fromHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'To (Client Account)', width: 180, dataIndex: 'toClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'To (Holding Account)', width: 180, dataIndex: 'toHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'Collateral', width: 55, dataIndex: 'collateralTransfer', type: 'boolean'},
						{header: 'Note', width: 200, dataIndex: 'note', hidden: true},
						{
							header: 'Violation Status', width: 100, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
							renderer: function(v, p, r) {
								return (Clifton.rule.violation.renderViolationStatus(v));
							}
						},
						{header: 'FK Field ID', width: 60, dataIndex: 'sourceFkFieldId', useNull: true, hidden: true, tooltip: 'Source Entity ID for when this transfer was initiated from a different part of the system'},
						{header: 'Price Date', width: 60, dataIndex: 'exposureDate'},
						{header: 'Transfered', width: 60, dataIndex: 'transactionDate'},
						{header: 'Settled', width: 60, dataIndex: 'settlementDate'},
						{header: 'Booked', width: 80, dataIndex: 'bookingDate'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', url: 'investmentAccountGroupListFind.json'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of transfers
							this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -30)});
						}
						const params = {journalBooked: true},
							topToolbar = this.getTopToolbar();

						const clientInvestmentAccountGroupId = TCG.getChildByName(topToolbar, 'clientInvestmentAccountGroupId').getValue();
						if (TCG.isNotBlank(clientInvestmentAccountGroupId)) {
							params.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
						}
						const investmentGroupId = TCG.getChildByName(topToolbar, 'investmentGroupId').getValue();
						if (TCG.isNotBlank(investmentGroupId)) {
							params.investmentGroupId = investmentGroupId;
						}

						return params;
					},
					configureToolsMenu: function(menu) {
						const gridPanel = this;
						menu.add('-');
						menu.add({
							text: 'Preview Instruction Message',
							tooltip: 'Preview SWIFT Instruction Message',
							iconCls: 'transfer',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								const fills = sm.getSelections();
								if (TCG.isNotEquals(1, sm.getCount())) {
									TCG.showError('Please select a single transfer to preview.', 'Incorrect Selection');
								}
								else {
									const id = fills[0].id;
									Clifton.instruction.openInstructionPreview(id, 'AccountingPositionTransfer');
								}
							}
						});
					},
					editor: {
						detailPageClass: 'Clifton.accounting.positiontransfer.TransferWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Transfer Details',
				items: [{
					name: 'accountingPositionTransferDetailListFind',
					xtype: 'gridpanel',
					instructions: 'Position transfers are used to transfer existing positions (use Simple Journals for cash movements) between investment accounts.',
					additionalPropertiesToRequest: 'id|positionTransfer.id|positionTransfer.transferType|positionTransfer.fromHoldingInvestmentAccount.id|positionTransfer.fromHoldingInvestmentAccount.label|positionTransfer.transactionDate|positionTransfer.fromClientInvestmentAccount.id|positionTransfer.fromClientInvestmentAccount.label|positionTransfer.toClientInvestmentAccount.id|positionTransfer.toClientInvestmentAccount.label|positionTransfer.toHoldingInvestmentAccount.id|positionTransfer.toHoldingInvestmentAccount.label',
					columns: [
						{header: 'Detail ID', width: 80, dataIndex: 'id', hidden: true},
						{header: 'Transaction Date', dataIndex: 'positionTransfer.transactionDate', filter: {searchFieldName: 'transactionDate'}, width: 90},
						{header: 'Security', dataIndex: 'security.label', width: 200, filter: {type: 'combo', searchFieldName: 'securityId', url: 'investmentSecurityListFind.json', displayField: 'label'}},
						{header: 'Open Date', dataIndex: 'originalPositionOpenDate', width: 90, hidden: true},
						{header: 'Open FX Rate', dataIndex: 'exchangeRateToBase', type: 'currency', numberFormat: '0,000.0000000000', width: 100, hidden: true},
						{header: 'From (Client Account)', width: 200, dataIndex: 'positionTransfer.fromClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'From (Holding Account)', width: 200, dataIndex: 'positionTransfer.fromHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'To (Client Account)', width: 200, dataIndex: 'positionTransfer.toClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'To (Holding Account)', width: 200, dataIndex: 'positionTransfer.toHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'Cost Price', dataIndex: 'costPrice', type: 'float', width: 100, useNull: true},
						{header: 'Transfer Price', dataIndex: 'transferPrice', type: 'float', width: 100, useNull: true},
						{header: 'Orig Face', dataIndex: 'originalFace', type: 'float', width: 80, useNull: true},
						{header: 'Quantity', dataIndex: 'quantity', type: 'float', width: 80, useNull: true},
						{header: 'Cost Basis', dataIndex: 'positionCostBasis', type: 'currency', width: 100}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', url: 'investmentAccountGroupListFind.json'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of transfers
							this.setFilterValue('positionTransfer.transactionDate', {'after': new Date().add(Date.DAY, -30)});
						}
						const params = {},
							topToolbar = this.getTopToolbar();

						const clientInvestmentAccountGroupId = TCG.getChildByName(topToolbar, 'clientInvestmentAccountGroupId').getValue();
						if (TCG.isNotBlank(clientInvestmentAccountGroupId)) {
							params.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
						}
						const investmentGroupId = TCG.getChildByName(topToolbar, 'investmentGroupId').getValue();
						if (TCG.isNotBlank(investmentGroupId)) {
							params.investmentGroupId = investmentGroupId;
						}

						return params;
					},
					editor: {
						detailPageClass: 'Clifton.accounting.positiontransfer.TransferWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.positionTransfer.id;
						}
					}
				}]
			},


			{
				title: 'Transfer Types',
				items: [{
					name: 'accountingPositionTransferTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Position transfer types are used to determine .',
					columns: [
						{header: 'Transfer Type ID', width: 80, dataIndex: 'id', hidden: true},
						{header: 'Type Name', dataIndex: 'name', width: 110},
						{header: 'Description', dataIndex: 'description', width: 250, hidden: true},

						{header: 'To GL', dataIndex: 'toAccountingAccount.name', width: 90},
						{header: 'To Offsetting GL', dataIndex: 'toOffsettingAccountingAccount.name', width: 90, hidden: true},
						{header: 'From GL', dataIndex: 'fromAccountingAccount.name', width: 90},
						{header: 'From Offsetting GL', dataIndex: 'fromOffsettingAccountingAccount.name', width: 90, hidden: true},

						{header: 'Source Table', dataIndex: 'sourceSystemTable.name', width: 90},

						{header: 'Receiving', width: 50, dataIndex: 'receivingAsset', type: 'boolean'},
						{header: 'Sending', width: 50, dataIndex: 'sendingAsset', type: 'boolean'},
						{header: 'Internal', width: 50, dataIndex: 'internalTransfer', type: 'boolean'},

						{header: 'Cash', width: 50, dataIndex: 'cash', type: 'boolean'},
						{header: 'Collateral', width: 50, dataIndex: 'collateral', type: 'boolean', qtip: 'Indicates a collateral transfer.  Transfers of this type that are not cash, require the useOriginalTransactionDate field on the transfer to be true.'},
						{header: 'Collateral Posting', width: 55, dataIndex: 'collateralPosting', type: 'boolean', qtip: 'True indicates that the transfer is posting collateral, and false indicates a collateral return.'},
						{header: 'Counterparty Collateral', width: 55, dataIndex: 'counterpartyCollateral', type: 'boolean', qtip: 'The collateral being transfered belongs to the Counterparty.'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.positiontransfer.TransferTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Transfer Instructions',
				items: [{
					xtype: 'investment-instruction-grid',
					tableName: 'AccountingPositionTransfer',
					defaultCategoryName: 'Position Transfer Counterparty',
					defaultViewName: 'Group by Recipient'
				}]
			}
		]
	}]
});
