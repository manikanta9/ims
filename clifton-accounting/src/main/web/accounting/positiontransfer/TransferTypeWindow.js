Clifton.accounting.positiontransfer.TransferTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Position Transfer Type Window',
	iconCls: 'transfer',
	height: 520,

	items: [{
		xtype: 'formpanel',
		url: 'accountingPositionTransferType.json',
		labelWidth: 160,
		readOnly: true,
		items: [
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},

			{fieldLabel: 'To GL Account', name: 'toAccountingAccount.name', hiddenName: 'toAccountingAccount.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'accountingAccountListFind.json'},
			{fieldLabel: 'To Offsetting GL Account', name: 'toOffsettingAccountingAccount.name', hiddenName: 'toOffsettingAccountingAccount.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'accountingAccountListFind.json'},
			{fieldLabel: 'From GL Account', name: 'fromAccountingAccount.name', hiddenName: 'fromAccountingAccount.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'accountingAccountListFind.json'},
			{fieldLabel: 'From Offsetting GL Account', name: 'fromOffsettingAccountingAccount.name', hiddenName: 'fromOffsettingAccountingAccount.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'accountingAccountListFind.json'},

			{fieldLabel: 'Source Table', name: 'sourceSystemTable.name', hiddenName: 'sourceSystemTable.id', xtype: 'combo', url: 'systemTableListFind.json', qtip: 'An optional system table all transfers of this type should use for the source linked ot the transfer'},

			{boxLabel: 'Receiving', name: 'receivingAsset', xtype: 'checkbox'},
			{boxLabel: 'Sending', name: 'sendingAsset', xtype: 'checkbox'},
			{boxLabel: 'Internal', name: 'internalTransfer', xtype: 'checkbox'},
			{boxLabel: 'Cash', name: 'cash', xtype: 'checkbox'},
			{boxLabel: 'Collateral', name: 'collateral', xtype: 'checkbox', tooltip: 'Indicates a collateral transfer.  Transfers of this type that are not cash, require the useOriginalTransactionDate field on the transfer to be true.'},
			{boxLabel: 'Collateral Posting', name: 'collateralPosting', xtype: 'checkbox', tooltip: 'True indicates that the transfer is posting collateral, and false indicates a collateral return.'},
			{boxLabel: 'Counterparty Collateral', name: 'counterpartyCollateral', xtype: 'checkbox', tooltip: 'The collateral being transfered belongs to the Counterparty.'}
		]
	}]
});
