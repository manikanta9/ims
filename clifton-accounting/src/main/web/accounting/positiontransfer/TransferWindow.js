// not the actual window but a window selector based on 'transferType'

Clifton.accounting.positiontransfer.TransferWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'accountingPositionTransfer.json?requestedMaxDepth=4',

	getClassName: function(config, entity) {
		if (entity && entity.transactionDate && TCG.isBlank(entity.settlementDate)) {
			entity.settlementDate = entity.transactionDate;
		}
		if (config && config.defaultData && config.defaultData.transactionDate && TCG.isBlank(config.defaultData.settlementDate)) {
			config.defaultData.settlementDate = config.defaultData.transactionDate;
		}
		const type = entity?.type ?? config?.defaultData?.type;
		if (type) {
			const sending = TCG.isTrue(type.sendingAsset);
			const receiving = TCG.isTrue(type.receivingAsset);
			if (receiving && !sending) {
				return 'Clifton.accounting.positiontransfer.TransferToWindow';
			}
			else if (sending && !receiving) {
				return 'Clifton.accounting.positiontransfer.TransferFromWindow';
			}
			else if (type.collateral === true && type.cash === true) {
				return 'Clifton.accounting.positiontransfer.collateral.TransferCashCollateralWindow';
			}
			else if (type.collateral === true && type.cash === false) {
				if (type.collateralPosting && type.counterpartyCollateral) {
					return 'Clifton.accounting.positiontransfer.collateral.TransferPostCounterpartyCollateralPositionWindow';
				}
				return 'Clifton.accounting.positiontransfer.collateral.TransferCollateralPositionWindow';
			}
		}
		return 'Clifton.accounting.positiontransfer.TransferBetweenWindow';
	}
});
