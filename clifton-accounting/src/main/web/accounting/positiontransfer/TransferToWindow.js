TCG.use('Clifton.accounting.positiontransfer.BaseTransferWindow');

Clifton.accounting.positiontransfer.TransferToWindow = Ext.extend(Clifton.accounting.positiontransfer.BaseTransferWindow, {
	titlePrefix: 'Transfer To Our Account',
	iconCls: 'arrow-left-green',
	width: 1200,
	height: 650,

	transferTab: {
		title: 'Info',
		tbar: {xtype: 'accounting-booking-toolbar', journalTypeName: 'Transfer Journal'},
		items: [{
			xtype: 'formpanel',
			url: 'accountingPositionTransfer.json',
			labelWidth: 130,
			getDefaultData: function(win) {
				const dd = win.defaultData;
				if (!win.defaultDataIsReal && !dd.type) {
					dd.type = TCG.data.getData('accountingPositionTransferTypeByName.json?name=To Our Account', this, 'accounting.positiontransfer.type.TO');
				}
				return dd;
			},
			getWarningMessage: function(f) {
				return Clifton.rule.violation.getRuleViolationWarningMessage('transfer', f.formValues, true);
			},
			items: [
				{xtype: 'accounting-positiontransfer-header-panel'},

				{xtype: 'label', html: '<hr />'},
				{fieldLabel: 'To Client Account', name: 'toClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'toClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false},
				{
					fieldLabel: 'To Holding Account', name: 'toHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'toHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['toClientInvestmentAccount.label'], allowBlank: false,
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.baseParams = {
							mainAccountId: f.findField('toClientInvestmentAccount.id').value,
							mainPurposeActiveOnDate: f.findField('transactionDate').value
						};
					}
				},
				{boxLabel: '"To" account receives collateral positions', name: 'collateralTransfer', xtype: 'checkbox'},
				{fieldLabel: 'Notes', name: 'note', xtype: 'textarea', height: 65},

				{
					xtype: 'formgrid-scroll',
					storeRoot: 'detailList',
					height: 270,
					heightResized: true,
					dtoClass: 'com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail',
					columnsConfig: [
						{header: 'Security', dataIndex: 'security.label', idDataIndex: 'security.id', width: 200, editor: {xtype: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label', allowBlank: false}},
						{header: 'Open Date', dataIndex: 'originalPositionOpenDate', width: 90, editor: {xtype: 'datefield'}},
						{header: 'Open FX Rate', dataIndex: 'exchangeRateToBase', type: 'float', width: 100, editor: {xtype: 'floatfield'}},
						{header: 'Cost Price', dataIndex: 'costPrice', type: 'float', width: 100, editor: {xtype: 'floatfield'}},
						{
							header: 'Transfer Price', dataIndex: 'transferPrice', type: 'float', width: 100, editor: {
								xtype: 'floatfield',
								onChangeHandler: function(field, newValue, oldValue) {
									const grid = field.gridEditor.containerGrid;
									const row = field.gridEditor.row;
									const rowData = grid.getStore().getAt(row).data;
									const transactionDate = TCG.getChildByName(grid.getWindow(), 'transactionDate').value;
									const notionalMultiplier = TCG.getResponseText('marketDataIndexRatioForSecurity.json?securityId=' + rowData['security.id'] + '&date=' + transactionDate, grid);
									const cb = TCG.getResponseText('investmentSecurityNotional.json?securityId=' + rowData['security.id'] + '&price=' + (newValue.replace(/,/g, '') * notionalMultiplier) + '&quantity=' + (rowData.quantity || 0), grid);
									grid.startEditing(row, 8);
									const costBasis = grid.getColumnModel().getCellEditor(8, row).field;
									costBasis.setValue(cb);
								}
							}
						},
						{header: 'Orig Face', dataIndex: 'originalFace', type: 'float', width: 80, summaryType: 'sum', editor: {xtype: 'currencyfield'}, useNull: true},
						{
							header: 'Quantity', dataIndex: 'quantity', type: 'float', width: 80, summaryType: 'sum', editor: {
								xtype: 'currencyfield',
								onChangeHandler: function(field, newValue, oldValue) {
									const grid = field.gridEditor.containerGrid;
									const row = field.gridEditor.row;
									const rowData = grid.getStore().getAt(row).data;
									const transactionDate = TCG.getChildByName(grid.getWindow(), 'transactionDate').value;
									const notionalMultiplier = TCG.getResponseText('marketDataIndexRatioForSecurity.json?securityId=' + rowData['security.id'] + '&date=' + transactionDate, grid);
									const cb = TCG.getResponseText('investmentSecurityNotional.json?securityId=' + rowData['security.id'] + '&price=' + (rowData.transferPrice * notionalMultiplier || 0) + '&quantity=' + newValue.replace(/,/g, ''), grid);
									grid.startEditing(row, 8);
									const costBasis = grid.getColumnModel().getCellEditor(8, row).field;
									costBasis.setValue(cb);
								}
							}
						},
						{header: 'Commission Per Unit', width: 80, dataIndex: 'commissionPerUnitOverride', type: 'float', useNull: true, editor: {xtype: 'floatfield', allowBlank: true}, tooltip: 'Commission Per Unit Override. Leave Blank to apply default schedule.'},
						{header: 'Cost Basis', dataIndex: 'positionCostBasis', type: 'currency', width: 100, summaryType: 'sum', editor: {xtype: 'currencyfield'}}
					],
					plugins: {ptype: 'gridsummary'}
				}
			]
		}]
	}
});
