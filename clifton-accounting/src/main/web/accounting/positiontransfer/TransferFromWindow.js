TCG.use('Clifton.accounting.positiontransfer.BaseTransferWindow');
TCG.use('Clifton.accounting.positiontransfer.TransferExistingGrid');

Clifton.accounting.positiontransfer.TransferFromWindow = Ext.extend(Clifton.accounting.positiontransfer.BaseTransferWindow, {
	titlePrefix: 'Transfer From Our Account',
	iconCls: 'arrow-right-red',
	width: 1200,
	height: 620,

	transferTab: {
		title: 'Info',
		tbar: {xtype: 'accounting-booking-toolbar', journalTypeName: 'Transfer Journal'},
		items: [{
			xtype: 'formpanel',
			url: 'accountingPositionTransfer.json',
			labelWidth: 130,
			getDefaultData: function(win) {
				const dd = win.defaultData;
				if (!win.defaultDataIsReal && !dd.type) {
					dd.type = TCG.data.getData('accountingPositionTransferTypeByName.json?name=From Our Account', this, 'accounting.positiontransfer.type.FROM');
				}
				return dd;
			},
			getWarningMessage: function(f) {
				return Clifton.rule.violation.getRuleViolationWarningMessage('transfer', f.formValues, true);
			},
			items: [
				{xtype: 'accounting-positiontransfer-header-panel'},

				{xtype: 'label', html: '<hr />'},
				{fieldLabel: 'From Client Account', name: 'fromClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false},
				{
					fieldLabel: 'From Holding Account', name: 'fromHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['fromClientInvestmentAccount.label'], allowBlank: false,
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.baseParams = {
							mainAccountId: f.findField('fromClientInvestmentAccount.id').value,
							mainPurposeActiveOnDate: f.findField('transactionDate').value
						};
					}
				},
				{fieldLabel: 'Notes', name: 'note', xtype: 'textarea', height: 65},

				Clifton.accounting.positiontransfer.TransferExistingGrid
			]
		}]
	}
});
