TCG.use('Clifton.accounting.positiontransfer.BaseTransferWindow');
TCG.use('Clifton.accounting.positiontransfer.TransferExistingGrid');

Clifton.accounting.positiontransfer.collateral.TransferCollateralPositionWindow = Ext.extend(Clifton.accounting.positiontransfer.BaseTransferWindow, {
	titlePrefix: 'Transfer',
	setTitle: function(title, iconCls) {
		const win = this;
		const dd = win.defaultData;
		if (dd && dd.type) {
			title = dd.type.name + ' - ' + title;
		}
		title = this.titlePrefix ? this.titlePrefix + ' - ' + title : title;

		TCG.Window.superclass.setTitle.call(this, title, iconCls);
	},
	iconCls: 'transfer',
	width: 1500,
	height: 720,

	loadGrids: function() {
		const win = this;
		const fp = this.getMainFormPanel();

		const collateralPositionListGrid = TCG.getChildByName(win, 'collateralPositionList');
		if (collateralPositionListGrid) {
			const toClientAccountId = fp.getFormValue('toClientInvestmentAccount.id', true);
			const toHoldingAccountId = fp.getFormValue('toHoldingInvestmentAccount.id', true);
			if (toClientAccountId && toHoldingAccountId && toClientAccountId !== '' && toHoldingAccountId !== '') {
				collateralPositionListGrid.enable();
				collateralPositionListGrid.reload();
			}
			else {
				collateralPositionListGrid.disable();
			}
		}


		const accountingBalanceValueListFindGrid = TCG.getChildByName(win, 'accountingBalanceValueListFind');
		if (accountingBalanceValueListFindGrid) {
			const fromClientAccountId = fp.getFormValue('fromClientInvestmentAccount.id', true);
			const fromHoldingAccountId = fp.getFormValue('fromHoldingInvestmentAccount.id', true);
			if (fromClientAccountId && fromHoldingAccountId && fromClientAccountId !== '' && fromHoldingAccountId !== '') {
				accountingBalanceValueListFindGrid.enable();
				accountingBalanceValueListFindGrid.reload();
			}
			else {
				accountingBalanceValueListFindGrid.disable();
			}
		}
	},


	transferTab: {
		title: 'Info',
		layout: 'border',
		tbar: {xtype: 'accounting-booking-toolbar', journalTypeName: 'Transfer Journal'},

		init: function() {
			const win = this.getWindow();
			const formPanel = this.items[0];
			if (win.defaultDataIsReal && win.defaultData.bookingDate) {
				this.layout = undefined;
				formPanel.region = undefined;
				formPanel.split = undefined;
				formPanel.width = undefined;
				const newItems = [];
				newItems.push(this.items[0]);
				this.items = newItems;
			}
			else {
				this.layout = 'border';
				formPanel.region = 'west';
				formPanel.split = true;
			}
		},

		items: [
			{
				region: 'west',
				split: true,
				xtype: 'formpanel',
				url: 'accountingPositionTransfer.json',
				labelWidth: 130,
				width: 825,
				loadDefaultDataAfterRender: true,
				onRender: function() {
					TCG.form.FormPanel.prototype.onRender.apply(this, arguments);

					// use a delayed event to ensure that the default data is loaded before trying to load the grids
					this.on('afterrender', function(fp) {
						const win = fp.getWindow();
						win.loadGrids();
						// update the collateral transfer flag based on the transfer type
						const type = fp.getFormValue('type');
						const checkbox = fp.getForm().findField('collateralTransfer');
						checkbox.setValue(type.collateralPosting);
						checkbox.disable();
					}, this, {delay: 250});
				},
				items: [
					{name: 'type.id', xtype: 'hidden'},
					{
						xtype: 'columnpanel',
						columns: [{
							rows: [
								{fieldLabel: 'Exposure Date', name: 'exposureDate', xtype: 'hidden'},
								{
									fieldLabel: 'Transaction Date', name: 'transactionDate', xtype: 'datefield',
									listeners: {
										change: function(field, newValue, oldValue) {
											const fp = TCG.getParentFormPanel(field);
											fp.getWindow().loadGrids();
										}
									}
								},
								{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'}
							]
						}, {
							rows: [
								{xtype: 'accounting-bookingdate', sourceTable: 'AccountingPositionTransfer'},
								{name: 'sourceSystemTable.id', xtype: 'hidden'},
								{
									fieldLabel: 'Source FK Field ID', name: 'sourceFkFieldId', xtype: 'linkfield', detailIdField: 'sourceFkFieldId',
									qtip: 'For transfers that originated from a different part of the system (Collateral, etc.), allows to navigate back to the source entity that initiated this transfer.',
									getDetailPageClass: function(fp) {
										return TCG.getValue('sourceSystemTable.detailScreenClass', fp.getForm().formValues);
									}
								}
							]
						}]
					},

					{xtype: 'label', html: '<hr />'},
					{name: 'fromClientInvestmentAccount.businessClient.id', xtype: 'hidden'},
					{
						fieldLabel: 'From Client Account', name: 'fromClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false,
						beforequery: function(queryEvent) {
							queryEvent.combo.store.baseParams = {
								requestedPropertiesRoot: 'data',
								requestedProperties: 'id|label|description|businessClient.id'
							};
						},
						listeners: {
							select: function(combo, record, index) {
								const acctClient = record.json.businessClient.id;
								const fp = combo.getParentForm();
								const f = fp.getForm();
								f.findField('fromClientInvestmentAccount.businessClient.id').setValue(acctClient);
							}
						}
					},
					{
						fieldLabel: 'From Holding Account', name: 'fromHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['fromClientInvestmentAccount.label'], allowBlank: false,
						beforequery: function(queryEvent) {
							const f = queryEvent.combo.getParentForm().getForm();
							queryEvent.combo.store.baseParams = {
								mainAccountId: f.findField('fromClientInvestmentAccount.id').value,
								mainPurposeActiveOnDate: f.findField('transactionDate').value
							};
						},
						listeners: {
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.getWindow().loadGrids();
							},
							change: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.getWindow().loadGrids();
							}
						}
					},
					{xtype: 'label', html: '<hr />'},
					{
						fieldLabel: 'To Client Account', name: 'toClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'toClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false,
						requiredFields: ['fromClientInvestmentAccount.id'],
						beforequery: function(queryEvent) {
							const clientId = queryEvent.combo.getParentForm().getForm().findField('fromClientInvestmentAccount.businessClient.id').value;
							if (queryEvent.combo.getParentForm().getForm().findField('limitSameClient').checked === true) {
								queryEvent.combo.store.baseParams = {clientId: clientId};
							}
							else {
								queryEvent.combo.store.baseParams = {clientId: null};
							}
						}
					},
					{
						boxLabel: 'Limit to same client as from account (Uncheck to select any client account as the to account)', xtype: 'checkbox', name: 'limitSameClient', submitValue: 'false',
						checked: true,
						listeners: {
							check: function(f) {
								const p = TCG.getParentFormPanel(f);
								const bc = p.getForm().findField('toClientInvestmentAccount.id');
								bc.clearValue();
								bc.store.removeAll();
								bc.lastQuery = null;
							}
						}
					},

					{
						fieldLabel: 'To Holding Account', name: 'toHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'toHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['toClientInvestmentAccount.label'], allowBlank: false,
						beforequery: function(queryEvent) {
							const f = queryEvent.combo.getParentForm().getForm();
							queryEvent.combo.store.baseParams = {
								mainAccountId: f.findField('toClientInvestmentAccount.id').value,
								mainPurposeActiveOnDate: f.findField('transactionDate').value
							};
						},
						listeners: {
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.getWindow().loadGrids();
							},
							change: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.getWindow().loadGrids();
							}
						}
					},

					{boxLabel: '"To" account receives collateral positions', name: 'collateralTransfer', xtype: 'checkbox'},

					{fieldLabel: 'Notes', name: 'note', xtype: 'textarea', height: 65},
					{
						xtype: 'radiogroup', fieldLabel: '', allowBlank: false, value: true, disabled: true, columns: [200, 300],
						items: [
							{boxLabel: 'Keep Original Transaction Date', name: 'useOriginalTransactionDate', inputValue: true},
							{boxLabel: 'Use Transaction Date for opening positions', name: 'useOriginalTransactionDate', inputValue: false}
						]
					},
					Ext.applyIf({
						height: 260,
						name: 'transferDetailList',
						addToolbarAddButton: function(toolBar) {
							return false;
						}, listeners: {
							afterLoad: async function(panel) {
								const recordVisitor = async function(record) {
									const value = await panel.calculateMarketValue(record.json.existingPosition.id, record.data.quantity);
									record.set('marketValue', value);
								};
								panel.store.each(record => recordVisitor(record));
							}
						}
					}, Clifton.accounting.positiontransfer.TransferExistingGrid)
				]
			},


			{
				xtype: 'panel',
				region: 'center',
				layout: {
					type: 'border',
					align: 'stretch'
				},
				name: 'positionDetailGrids',
				items: [
					{
						autoLoad: false,
						title: 'Pledged',
						height: 325,
						xtype: 'gridpanel',
						split: true,
						region: 'north',
						name: 'collateralPositionList',
						instructions: 'Collateral Positions for the holding account on the balance date.',
						getLoadParams: function() {
							const fp = this.getWindow().getMainFormPanel();
							const type = fp.getFormValue('type');
							const clientAccountFieldName = type.collateralPosting === true ? 'toClientInvestmentAccount.id' : 'fromClientInvestmentAccount.id';
							const clientAccountId = fp.getFormValue(clientAccountFieldName, true, false);
							const holdingAccountFieldName = type.collateralPosting === true ? 'toHoldingInvestmentAccount.id' : 'fromHoldingInvestmentAccount.id';
							const holdingAccountId = fp.getFormValue(holdingAccountFieldName, true, false);
							const transactionDate = fp.getFormValue('transactionDate', true, false);
							return {
								clientInvestmentAccountId: clientAccountId,
								holdingInvestmentAccountId: holdingAccountId,
								transactionDate: TCG.parseDate(transactionDate).format('m/d/Y')
							};
						},
						columns: [
							{header: 'GL Account', width: 200, dataIndex: 'accountingAccount.label', filter: false},
							{header: 'Security', width: 75, dataIndex: 'security.symbol', filter: false},
							{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', hidden: true, type: 'float', filter: false},
							{header: 'Quantity', width: 70, dataIndex: 'quantity', type: 'currency', useNull: true},
							{header: 'Cost Price', width: 80, dataIndex: 'costPrice', type: 'currency', numberFormat: '0,000.0000', useNull: true},
							{header: 'Market Price', width: 80, dataIndex: 'marketPrice', type: 'currency', numberFormat: '0,000.0000', useNull: true},
							{header: 'Haircut', width: 80, dataIndex: 'haircut', type: 'currency', hidden: true},
							{header: 'Collateral Market Value (Original)', width: 150, hidden: true, dataIndex: 'collateralMarketValue', useNull: true, type: 'currency'},
							{
								header: 'Collateral Market Value', width: 150, dataIndex: 'collateralMarketValueAdjusted', type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
								renderer: function(v, p, r) {
									return TCG.renderAdjustedAmount(v, v !== r.data['collateralMarketValue'], r.data['collateralMarketValue'], '0,000.00');
								},
								useNull: true,
								summaryType: 'sum',
								summaryRenderer: function(v) {
									if (TCG.isNotBlank(v)) {
										return TCG.renderAmount(v, false, '0,000.00');
									}
								}
							},
							{header: '&nbsp;', width: 1} // spacer to fix last column width bug
						],
						plugins: {ptype: 'gridsummary'}
					},


					{
						autoLoad: false,
						xtype: 'editorgrid',
						region: 'center',
						title: 'Available to Pledged/Return',
						name: 'accountingBalanceValueListFind',
						instructions: 'Select one or more positions below that are currently open.',
						rowSelectionModel: 'checkbox',
						additionalPropertiesToRequest: 'investmentSecurity.id|clientInvestmentAccount.id|holdingInvestmentAccount.id|transactionDate',
						forceLocalFiltering: true,
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Security', width: 70, dataIndex: 'investmentSecurity.symbol'},//, defaultSortColumn: true},
							{header: 'Hierarchy', width: 130, dataIndex: 'investmentSecurity.instrument.hierarchy.nameExpanded', hidden: true},
							{header: 'GL Account', width: 50, dataIndex: 'accountingAccount.name'},
							{header: 'Price', dataIndex: 'price', width: 50, type: 'float', useNull: true},
							{header: 'FX Rate', width: 50, dataIndex: 'exchangeRateToBase', type: 'currency', numberFormat: '0,000.0000000000', negativeInRed: true, hidden: true},
							{header: 'Quantity', dataIndex: 'quantity', width: 45, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, filter: false, summaryType: 'sum'},
							{
								header: 'Transfer Amount', dataIndex: 'transferAmount', width: 45, type: 'float', useNull: true, sortable: false, summaryType: 'sum',
								editor: {
									xtype: 'floatfield',
									onChangeHandler: function(field, newValue, oldValue) {
										const grid = field.gridEditor.containerGrid;
										const row = field.gridEditor.row;
										grid.updateMarketValue(grid, row, newValue);
									}
								}
							},
							{header: 'Local Cost Basis', dataIndex: 'localCostBasis', type: 'currency', negativeInRed: true, width: 65, hidden: true},
							{header: 'Base Cost Basis', dataIndex: 'baseCostBasis', type: 'currency', negativeInRed: true, width: 65, summaryType: 'sum'},
							{
								header: 'Market Value', dataIndex: 'marketValue', type: 'currency', width: 45, sortable: false, editor: {xtype: 'currencyfield', readOnly: true}, summaryType: 'sum',
								summaryRenderer: function(v) {
									if (v !== '') {
										return TCG.renderAmount(v, false, '0,000.00');
									}
								}
							}
						],
						updateRecord: function() {
							// DO NOT REMOVE: This is here to avoid automatic saving of the grid
						},
						updateMarketValue: function(gridPanel, row, quantity) {
							const grid = gridPanel.grid;
							const fp = this.getWindow().getMainFormPanel();
							const rowJson = grid.getStore().getAt(row).json;
							const type = fp.getFormValue('type');

							const securityQuantityList = [{
								class: 'com.clifton.accounting.gl.position.closing.AccountingPositionClosingSecurityQuantity',
								investmentSecurityId: rowJson.investmentSecurity.id,
								quantity: quantity
							}];

							const transferDetailList = TCG.getChildByName(fp, 'transferDetailList');
							const selectedPositionList = transferDetailList.getSelectedPositions('com.clifton.accounting.gl.position.closing.AccountingPositionClosingTransaction');

							const balanceDate = fp.getFormValue('transactionDate', true, false);

							const params = {
								clientInvestmentAccountId: rowJson.clientInvestmentAccount.id,
								holdingInvestmentAccountId: rowJson.holdingInvestmentAccount.id,
								settlementDate: TCG.parseDate(balanceDate).format('m/d/Y'),
								securityQuantityList: Ext.util.JSON.encode(securityQuantityList),
								selectedPositionList: Ext.util.JSON.encode(selectedPositionList),
								collateral: type.collateralPosting !== true,
								order: type.collateralPosting !== true ? 'FIFO' : 'LIFO'
							};

							TCG.data.getDataValue('accountingPositionClosingLotMarketValue.json', this, function(data) {
								gridPanel.setMarketValue(data, row);
							}, this, params);
						},
						setMarketValue: function(value, row) {
							const grid = this.grid;

							const columnIndex = grid.getColumnModel().findColumnIndex('marketValue');
							grid.startEditing(row, columnIndex);
							const marketValue = grid.getColumnModel().getCellEditor(columnIndex, row).field;
							marketValue.setValue(value);
							grid.startEditing(row, 4);
						},
						plugins: {ptype: 'gridsummary'},
						getLoadParams: function() {
							const fp = this.getWindow().getMainFormPanel();
							const type = fp.getFormValue('type');
							const clientAccountId = fp.getFormValue('fromClientInvestmentAccount.id', true, false);
							const holdingAccountId = fp.getFormValue('fromHoldingInvestmentAccount.id', true, false);
							const balanceDate = fp.getFormValue('transactionDate', true, false);
							return {
								clientInvestmentAccountId: clientAccountId,
								holdingInvestmentAccountId: holdingAccountId,
								collateralAccountingAccount: type.collateralPosting !== true,
								positionAccountingAccount: true,
								pendingActivityRequest: 'POSITIONS_WITH_MERGE',
								receivableAccountingAccount: false,
								transactionDate: TCG.parseDate(balanceDate).format('m/d/Y')
							};
						},
						editor: {
							drillDownOnly: true,
							addEditButtons: function(t, gridPanel) {
								t.add({
									text: 'Select',
									tooltip: 'Select existing position(s)',
									iconCls: 'add',
									scope: this,
									handler: function() {
										const fp = this.getWindow().getMainFormPanel();
										const type = fp.getFormValue('type');
										const sm = gridPanel.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select at least one row or click Cancel.', 'No Row(s) Selected');
										}
										else {
											const url = 'accountingPositionClosingLotListFind.json?enableOpenSessionInView=true';

											const firstBalance = sm.getSelections()[0].json;
											const securityQuantityList = [];
											const selections = sm.getSelections();

											for (let i = 0; i < selections.length; i++) {
												const investmentSecurityId = selections[i].json.investmentSecurity.id;
												const transferAmount = selections[i].data.transferAmount;
												if (transferAmount) {
													securityQuantityList.push({
														class: 'com.clifton.accounting.gl.position.closing.AccountingPositionClosingSecurityQuantity',
														investmentSecurityId: investmentSecurityId,
														quantity: transferAmount
													});

													selections[i].set('transferAmount', '');
													selections[i].set('marketValue', '');
												}
											}

											const transferDetailList = TCG.getChildByName(fp, 'transferDetailList');
											const selectedPositionList = transferDetailList.getSelectedPositions('com.clifton.accounting.gl.position.closing.AccountingPositionClosingTransaction');

											const balanceDate = fp.getFormValue('transactionDate', true, false);
											const params = {
												clientInvestmentAccountId: firstBalance.clientInvestmentAccount.id,
												holdingInvestmentAccountId: firstBalance.holdingInvestmentAccount.id,
												settlementDate: TCG.parseDate(balanceDate).format('m/d/Y'),
												securityQuantityList: Ext.util.JSON.encode(securityQuantityList),
												selectedPositionList: Ext.util.JSON.encode(selectedPositionList),
												collateral: type.collateralPosting !== true,
												order: type.collateralPosting !== true ? 'FIFO' : 'LIFO'
											};

											sm.clearSelections();
											if (transferDetailList && transferDetailList.processSelection) {
												TCG.data.getDataPromise(url, this, {params: params})
													.then(function(data) {
														transferDetailList.getStore().removeAll();
														transferDetailList.processSelection(data.rows);
													});
											}
										}
									}
								});
								t.add('-');
							}
						}
					}]
			}
		]
	}
});
