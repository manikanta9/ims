TCG.use('Clifton.accounting.positiontransfer.BaseTransferWindow');

Clifton.accounting.positiontransfer.collateral.TransferCashCollateralWindow = Ext.extend(Clifton.accounting.positiontransfer.BaseTransferWindow, {
	titlePrefix: 'Transfer Cash Collateral',
	iconCls: 'transfer',
	width: 1200,
	height: 520,

	transferTab: {
		title: 'Info',
		tbar: {xtype: 'accounting-booking-toolbar', journalTypeName: 'Transfer Journal'},
		items: [{
			xtype: 'formpanel',
			doNotUpdateFormWithDefaultData: true,
			url: 'accountingPositionTransfer.json',
			labelWidth: 130,
			loadDefaultDataAfterRender: true,
			getSubmitParams: function() {
				const params = {};
				params['detailList[0].originalPositionOpenDate'] = TCG.parseDate(this.getForm().findField('transactionDate').getValue()).format('m/d/Y');
				return params;
			},
			getDefaultData: function(win) {
				const dd = win.defaultData;
				if (!win.defaultDataIsReal) {
					let detail;
					if (!dd.detailList) {
						dd.detailList = [];
						detail = {};
						dd.detailList.push(detail);
					}
					dd.detailList[0].security = TCG.data.getData('investmentSecurityBySymbol.json?symbol=USD&currency=true', this, 'investment.security.usd');
				}
				return dd;
			},
			getWarningMessage: function(f) {
				return Clifton.rule.violation.getRuleViolationWarningMessage('transfer', f.formValues, true);
			},
			items: [
				{xtype: 'accounting-positiontransfer-header-panel'},

				{xtype: 'label', html: '<hr />'},
				{name: 'fromClientInvestmentAccount.businessClient.id', xtype: 'hidden'},
				{
					fieldLabel: 'From Client Account', name: 'fromClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false,
					beforequery: function(queryEvent) {
						queryEvent.combo.store.baseParams = {
							requestedPropertiesRoot: 'data',
							requestedProperties: 'id|label|description|businessClient.id'
						};
					},
					listeners: {
						select: function(combo, record, index) {
							const f = combo.getParentForm().getForm();
							f.findField('fromClientInvestmentAccount.businessClient.id').setValue(record.json.businessClient.id);
						}
					}
				},
				{
					fieldLabel: 'From Holding Account', name: 'fromHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['fromClientInvestmentAccount.label'], allowBlank: false,
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.baseParams = {
							mainAccountId: f.findField('fromClientInvestmentAccount.id').value,
							mainPurposeActiveOnDate: f.findField('transactionDate').value
						};
					}
				},
				{xtype: 'label', html: '<hr />'},
				{
					fieldLabel: 'To Client Account', name: 'toClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'toClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false,
					requiredFields: ['fromClientInvestmentAccount.id'],
					beforequery: function(queryEvent) {
						const clientId = queryEvent.combo.getParentForm().getForm().findField('fromClientInvestmentAccount.businessClient.id').value;
						if (TCG.isTrue(queryEvent.combo.getParentForm().getForm().findField('limitSameClient').checked)) {
							queryEvent.combo.store.baseParams = {clientId: clientId};
						}
						else {
							queryEvent.combo.store.baseParams = {clientId: null};
						}
					}
				},
				{
					boxLabel: 'Limit to same client as from account (Uncheck to select any client account as the to account)', xtype: 'checkbox', name: 'limitSameClient', submitValue: 'false',
					checked: true,
					listeners: {
						check: function(f) {
							const p = TCG.getParentFormPanel(f);
							const bc = p.getForm().findField('toClientInvestmentAccount.id');
							bc.clearValue();
							bc.store.removeAll();
							bc.lastQuery = null;
						}
					}
				},

				{
					fieldLabel: 'To Holding Account', name: 'toHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'toHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['toClientInvestmentAccount.label'], allowBlank: false,
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.baseParams = {
							mainAccountId: f.findField('toClientInvestmentAccount.id').value,
							mainPurposeActiveOnDate: f.findField('transactionDate').value
						};
					}
				},

				{fieldLabel: 'Notes', name: 'note', xtype: 'textarea', height: 65},
				{xtype: 'label', html: '<hr />'},

				{fieldLabel: 'Transfer Currency', name: 'detailList[0].security.label', allowBlank: false, displayField: 'label', hiddenName: 'detailList[0].security.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?currency=true'},
				{fieldLabel: 'Transfer Amount', name: 'detailList[0].positionCostBasis', xtype: 'currencyfield', allowBlank: false},
				{name: 'detailList[0].exchangeRateToBase', xtype: 'hidden', value: 1}
			]
		}]
	}
});
