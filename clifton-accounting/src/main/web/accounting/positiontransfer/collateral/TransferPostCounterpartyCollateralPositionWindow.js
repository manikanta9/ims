TCG.use('Clifton.accounting.positiontransfer.BaseTransferWindow');
TCG.use('Clifton.accounting.positiontransfer.TransferExistingGrid');


Clifton.accounting.positiontransfer.collateral.TransferPostCounterpartyCollateralPositionWindow = Ext.extend(Clifton.accounting.positiontransfer.BaseTransferWindow, {
	titlePrefix: 'Transfer',
	setTitle: function(title, iconCls) {
		const win = this;
		const dd = win.defaultData;
		if (dd && dd.type) {
			title = dd.type.name + ' - ' + title;
		}
		title = this.titlePrefix ? this.titlePrefix + ' - ' + title : title;

		TCG.Window.superclass.setTitle.call(this, title, iconCls);
	},
	iconCls: 'transfer',
	width: 1400,
	height: 690,

	loadGrids: function() {
		const win = this;
		const fp = this.getMainFormPanel();

		const collateralPositionListGrid = TCG.getChildByName(win, 'collateralPositionList');
		if (collateralPositionListGrid) {
			const toClientAccountId = fp.getFormValue('toClientInvestmentAccount.id', true);
			const toHoldingAccountId = fp.getFormValue('toHoldingInvestmentAccount.id', true);
			if (toClientAccountId && toHoldingAccountId && TCG.isNotBlank(toClientAccountId) && TCG.isNotBlank(toHoldingAccountId)) {
				collateralPositionListGrid.enable();
				collateralPositionListGrid.reload();
			}
			else {
				collateralPositionListGrid.disable();
			}
		}
	},

	transferTab: {
		title: 'Info',
		layout: 'border',
		tbar: {xtype: 'accounting-booking-toolbar', journalTypeName: 'Transfer Journal'},

		init: function() {
			const win = this.getWindow();
			const formPanel = this.items[0];
			if (win.defaultDataIsReal && win.defaultData.bookingDate) {
				this.layout = undefined;
				formPanel.region = undefined;
				formPanel.split = undefined;
				formPanel.width = undefined;
				const newItems = [];
				newItems.push(this.items[0]);
				this.items = newItems;
			}
			else {
				this.layout = 'border';
				formPanel.region = 'west';
				formPanel.split = true;
			}
		},

		items: [
			{
				region: 'west',
				width: 825,
				split: true,
				xtype: 'formpanel',
				url: 'accountingPositionTransfer.json',
				labelWidth: 130,
				loadDefaultDataAfterRender: true,
				onRender: function() {
					TCG.form.FormPanel.prototype.onRender.apply(this, arguments);

					// use a delayed event to ensure that the default data is loaded before trying to load the grids
					this.on('afterrender', function(fp) {
						const win = fp.getWindow();
						win.loadGrids();
						// update the collateral transfer flag based on the transfer type
						const type = fp.getFormValue('type');
						const checkbox = fp.getForm().findField('collateralTransfer');
						checkbox.setValue(type.collateralPosting);
						checkbox.disable();
					}, this, {delay: 250});
				},
				items: [
					{name: 'type.id', xtype: 'hidden'},
					{
						xtype: 'columnpanel',
						columns: [{
							rows: [
								{fieldLabel: 'Exposure Date', name: 'exposureDate', xtype: 'hidden'},
								{
									fieldLabel: 'Transaction Date', name: 'transactionDate', xtype: 'datefield',
									listeners: {
										change: function(field, newValue, oldValue) {
											const fp = TCG.getParentFormPanel(field);
											fp.getWindow().loadGrids();
										}
									}
								},
								{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'}
							]
						}, {
							rows: [
								{xtype: 'accounting-bookingdate', sourceTable: 'AccountingPositionTransfer'},
								{name: 'sourceSystemTable.id', xtype: 'hidden'},
								{
									fieldLabel: 'Source FK Field ID', name: 'sourceFkFieldId', xtype: 'linkfield', detailIdField: 'sourceFkFieldId',
									qtip: 'For transfers that originated from a different part of the system (Collateral, etc.), allows to navigate back to the source entity that initiated this transfer.',
									getDetailPageClass: function(fp) {
										return TCG.getValue('sourceSystemTable.detailScreenClass', fp.getForm().formValues);
									}
								}
							]
						}]
					},

					{xtype: 'label', html: '<hr />'},
					{name: 'fromClientInvestmentAccount.businessClient.id', xtype: 'hidden'},
					{
						fieldLabel: 'From Client Account', name: 'fromClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false,
						beforequery: function(queryEvent) {
							queryEvent.combo.store.baseParams = {
								requestedPropertiesRoot: 'data',
								requestedProperties: 'id|label|description|businessClient.id'
							};
						},
						listeners: {
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.setFieldValue('fromClientInvestmentAccount.businessClient.id', record.json.businessClient.id);
							}
						}
					},
					{
						fieldLabel: 'From Holding Account', name: 'fromHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['fromClientInvestmentAccount.label'], allowBlank: false,
						beforequery: function(queryEvent) {
							const f = queryEvent.combo.getParentForm().getForm();
							queryEvent.combo.store.baseParams = {
								mainAccountId: f.findField('fromClientInvestmentAccount.id').value,
								mainPurposeActiveOnDate: f.findField('transactionDate').value
							};
						},
						listeners: {
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.getWindow().loadGrids();
							},
							change: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.getWindow().loadGrids();
							}
						}
					},
					{xtype: 'label', html: '<hr />'},
					{
						fieldLabel: 'To Client Account', name: 'toClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'toClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false,
						requiredFields: ['fromClientInvestmentAccount.id'],
						beforequery: function(queryEvent) {
							const clientId = queryEvent.combo.getParentForm().getForm().findField('fromClientInvestmentAccount.businessClient.id').value;
							if (TCG.isTrue(queryEvent.combo.getParentForm().getForm().findField('limitSameClient').checked)) {
								queryEvent.combo.store.baseParams = {clientId: clientId};
							}
							else {
								queryEvent.combo.store.baseParams = {clientId: null};
							}
						}
					},
					{
						boxLabel: 'Limit to same client as from account (Uncheck to select any client account as the to account)', xtype: 'checkbox', name: 'limitSameClient', submitValue: 'false',
						checked: true,
						listeners: {
							check: function(f) {
								const p = TCG.getParentFormPanel(f);
								const bc = p.getForm().findField('toClientInvestmentAccount.id');
								bc.clearValue();
								bc.store.removeAll();
								bc.lastQuery = null;
							}
						}
					},

					{
						fieldLabel: 'To Holding Account', name: 'toHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'toHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['toClientInvestmentAccount.label'], allowBlank: false,
						beforequery: function(queryEvent) {
							const f = queryEvent.combo.getParentForm().getForm();
							queryEvent.combo.store.baseParams = {
								mainAccountId: f.findField('toClientInvestmentAccount.id').value,
								mainPurposeActiveOnDate: f.findField('transactionDate').value
							};
						},
						listeners: {
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.getWindow().loadGrids();
							},
							change: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.getWindow().loadGrids();
							}
						}
					},

					{boxLabel: '"To" account receives collateral positions', name: 'collateralTransfer', xtype: 'checkbox'},

					{fieldLabel: 'Notes', name: 'note', xtype: 'textarea', height: 65},
					{
						xtype: 'radiogroup', fieldLabel: '', allowBlank: false, value: true, disabled: true, columns: [200, 300],
						items: [
							{boxLabel: 'Keep Original Transaction Date', name: 'useOriginalTransactionDate', inputValue: true},
							{boxLabel: 'Use Transaction Date for opening positions', name: 'useOriginalTransactionDate', inputValue: false}
						]
					},
					Ext.applyIf({
						height: 250,
						name: 'transferDetailList',
						addToolbarAddButton: function(toolBar) {
							TCG.grid.FormGridPanel.prototype.addToolbarAddButton.call(this, toolBar);
						},
						getNewRowDefaults: function() {
							return {exchangeRateToBase: 1};
						},
						listeners: {},
						columnOverrides: [
							{header: 'Security', dataIndex: 'security.label', idDataIndex: 'security.id', width: 150, editor: {xtype: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label', allowBlank: false}},
							{header: 'Open Date', dataIndex: 'originalPositionOpenDate', width: 80, editor: {xtype: 'datefield'}},
							{header: 'FX Rate', editor: {xtype: 'floatfield', value: 1}, hidden: false}
						]
					}, Clifton.accounting.positiontransfer.TransferExistingGrid)
				]
			},

			{
				region: 'center',
				autoLoad: false,
				title: 'Pledged',
				height: 325,
				xtype: 'gridpanel',
				name: 'collateralPositionList',
				instructions: 'Collateral Positions for the holding account on the balance date.',
				getLoadParams: function() {
					const fp = this.getWindow().getMainFormPanel();
					const type = fp.getFormValue('type');
					const clientAccountFieldName = TCG.isTrue(type.collateralPosting) ? 'toClientInvestmentAccount.id' : 'fromClientInvestmentAccount.id';
					const clientAccountId = fp.getFormValue(clientAccountFieldName, true, false);
					const holdingAccountFieldName = TCG.isTrue(type.collateralPosting) ? 'toHoldingInvestmentAccount.id' : 'fromHoldingInvestmentAccount.id';
					const holdingAccountId = fp.getFormValue(holdingAccountFieldName, true, false);
					const transactionDate = fp.getFormValue('transactionDate', false, false);
					return {
						clientInvestmentAccountId: clientAccountId,
						holdingInvestmentAccountId: holdingAccountId,
						transactionDate: TCG.parseDate(transactionDate).format('m/d/Y')
					};
				},
				columns: [
					{header: 'Accounting Account', width: 175, dataIndex: 'accountingAccount.label', filter: false},
					{header: 'Security', width: 75, dataIndex: 'security.symbol', filter: false},
					{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', hidden: true, type: 'float', filter: false},
					{header: 'Quantity', width: 75, dataIndex: 'quantity', type: 'currency'},
					{header: 'Cost Price', width: 100, dataIndex: 'costPrice', type: 'currency', numberFormat: '0,000.0000'},
					{header: 'Market Price', width: 100, dataIndex: 'marketPrice', type: 'currency', numberFormat: '0,000.0000'},
					{header: 'Haircut', width: 100, dataIndex: 'haircut', type: 'currency', hidden: true},

					{header: 'Collateral Market Value (Original)', width: 100, hidden: true, dataIndex: 'collateralMarketValue', type: 'currency'},
					{
						header: 'Collateral Market Value', width: 150, dataIndex: 'collateralMarketValueAdjusted', type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
						renderer: function(v, p, r) {
							return TCG.renderAdjustedAmount(v, v !== r.data['collateralMarketValue'], r.data['collateralMarketValue'], '0,000.00');
						},
						useNull: true,
						summaryType: 'sum',
						summaryRenderer: function(v) {
							if (TCG.isNotBlank(v)) {
								return TCG.renderAmount(v, false, '0,000.00');
							}
						}
					}
				],
				plugins: {ptype: 'gridsummary'}
			}]
	}
});
