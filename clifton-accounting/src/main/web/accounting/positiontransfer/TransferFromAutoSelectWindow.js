Clifton.accounting.positiontransfer.TransferFromAutoSelectWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Transfer From Our Account Auto-Select Position(s)',
	width: 1020,
	height: 690,
	hideOKButton: true,
	applyButtonText: 'Generate',
	applyButtonTooltip: 'Generate position transfers.',
	dtoClassForBinding: 'com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommand',
	saveUrl: 'accountingPositionTransferListGenerate.json?enableValidatingBinding=true&requestedMaxDepth=2',

	items: [
		{
			xtype: 'formpanel',
			instructions: 'Specify the security criteria that the system will use to select open lot positions for the from client and holding accounts. The resulting lot positions may be from multiple securities, which may result in more than one position transfer. A summary of created pending position transfers will be shown after submission.',
			labelWidth: 140,
			getDefaultData: function(win) {
				const dd = win.defaultData;
				if (!win.defaultDataIsReal && !dd.type) {
					dd.type = TCG.data.getData('accountingPositionTransferTypeByName.json?name=From Our Account', this, 'accounting.positiontransfer.type.FROM');
					dd.lotSelectionOrder = {value: 'FIFO', text: 'First In First Out'};
				}
				return dd;
			},

			getSaveURL: function() {
				return this.getWindow().saveUrl;
			},

			listeners: {
				aftercreate: function() {
					const fp = this;
					const win = fp.getWindow();
					const results = fp.getForm().formValues.resultString;
					if (results && results.startsWith('No Accounting Position Transfers were generated')) {
						TCG.showInfo('No position transfers could be generated from the specified data. Review the security specifications and try again.', 'No Position Transfers Generated');
					}
					else {
						const resultWindow = new TCG.app.Window({
							title: 'Accounting Position Transfer(s) Created',
							iconCls: 'run',
							width: 600,
							height: 300,
							modal: true,
							minimizable: false,
							maximizable: false,
							resizable: false,
							border: true,
							bodyStyle: 'padding: 20px;',
							openerCt: this,

							items: [
								{xtype: 'textarea', value: results, readOnly: true}
							],
							buttons: [
								{
									text: 'OK',
									tooltip: 'Closes this Window and the Transfer Auto-Select Position Window.',
									width: 120,
									handler: function() {
										resultWindow.win.closeWindow();
										win.closeWindow();
									}
								}
							],
							defaultButton: 0,
							windowOnShow: function() {
								this.focus(); // allow keyboard navigation selection: unfortunately visual highlight only happens after the user hits tab once (anywhere/anytime after our app is loaded)
							}
						});
					}
				}
			},

			toggleToAccountsFieldSet: function(enable) {
				const fp = this;
				const fieldSet = TCG.getChildByName(fp, 'toClientAccountsFieldSet');
				const toClientAccountField = fp.getForm().findField('toClientInvestmentAccount.label');
				if (enable) {
					fieldSet.enable();
					fieldSet.expand(true);
					toClientAccountField.allowBlank = false;
				}
				else {
					fieldSet.disable();
					fieldSet.collapse(true);
					toClientAccountField.allowBlank = true;
				}
			},

			items: [
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {
						layout: 'form',
						defaults: {
							anchor: '-20'
						}
					},
					items: [
						{
							columnWidth: .60,
							items: [
								{
									fieldLabel: 'Transfer Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', url: 'accountingPositionTransferTypeListFind.json?sendingAsset=true&collateral=false&internalTransfer=false',
									listeners: {
										change: function(combo, newValue, oldValue) {
											const fp = TCG.getParentFormPanel(combo);
											fp.toggleToAccountsFieldSet.call(fp, combo.lastSelectionText === 'Between Our Accounts');
										}
									}
								},
								{
									fieldLabel: 'Lot Selection Order', name: 'lotSelectionOrder', hiddenName: 'lotSelectionOrder', displayField: 'name', valueField: 'lotSelectionOrder', xtype: 'combo', mode: 'local', width: 225,
									qtip: 'The order in which the system will choose lot position(s) to transfer. The default value, if not specified, is First In First Out (FIFO).',
									store: {
										xtype: 'arraystore',
										fields: ['name', 'lotSelectionOrder', 'description'],
										data: [
											['First In First Out', 'FIFO', 'Select positions by choosing those that are oldest in the system first.'],
											['Last In First Out', 'LIFO', 'Select positions by choosing those that are youngest in the system first.']
										]
									}
								}
							]
						},
						{
							defaults: {
								anchor: '0'
							},
							columnWidth: .40,
							items: [
								{
									fieldLabel: 'Price Date', name: 'exposureDate', xtype: 'datefield',
									qtip: 'The date to use for looking up a price for each transfer detail generated. If left blank, the transfer price for each detail generated will be the historic/cost price of the lot position being transferred.',
									listeners: {
										change: function(field, newValue, oldValue) {
											const fp = TCG.getParentFormPanel(field);
											const f = fp.getForm();
											if (newValue !== '' && newValue !== oldValue) {
												// if value is set, move transaction date to date + 1 business day
												f.findField('transactionDate').setValue(Clifton.calendar.getBusinessDayFromDate(newValue, 1));
											}
										}
									}
								},
								{fieldLabel: 'Transaction Date', name: 'transactionDate', xtype: 'datefield'},
								{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'}
							]
						}
					]
				},
				{name: 'fromClientInvestmentAccount.businessClient.id', xtype: 'hidden'},
				{
					fieldLabel: 'From Client Account', name: 'fromClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false,
					beforequery: function(queryEvent) {
						queryEvent.combo.store.baseParams = {
							requestedPropertiesRoot: 'data',
							requestedProperties: 'id|label|description|businessClient.id'
						};
					},
					listeners: {
						select: function(combo, record, index) {
							const acctClient = record.json.businessClient.id;
							const fp = combo.getParentForm();
							const f = fp.getForm();
							f.findField('fromClientInvestmentAccount.businessClient.id').setValue(acctClient);
						}
					}
				},
				{
					fieldLabel: 'From Holding Account', name: 'fromHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'fromHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['fromClientInvestmentAccount.label'], allowBlank: false,
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.baseParams = {
							mainAccountId: f.findField('fromClientInvestmentAccount.id').value,
							mainPurposeActiveOnDate: f.findField('transactionDate').value
						};
					}
				},
				{
					xtype: 'fieldset',
					name: 'toClientAccountsFieldSet',
					collapsed: true,
					disabled: true,
					title: 'To Client Account Details',
					instructions: 'Specify the Client and Holding Investment Accounts for Between Our Accounts transfers.',
					items: [
						{
							fieldLabel: 'To Client Account', name: 'toClientInvestmentAccount.label', xtype: 'combo', hiddenName: 'toClientInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: true,
							requiredFields: ['fromClientInvestmentAccount.id'],
							beforequery: function(queryEvent) {
								const clientId = queryEvent.combo.getParentForm().getForm().findField('fromClientInvestmentAccount.businessClient.id').value;
								if (queryEvent.combo.getParentForm().getForm().findField('limitSameClient').checked === true) {
									queryEvent.combo.store.baseParams = {clientId: clientId};
								}
								else {
									queryEvent.combo.store.baseParams = {clientId: null};
								}
							}
						},
						{
							boxLabel: 'Limit to same client as from account (Uncheck to select any client account as the to account).', xtype: 'checkbox', name: 'limitSameClient', submitValue: 'false',
							checked: true,
							listeners: {
								check: function(f) {
									const p = TCG.getParentFormPanel(f);
									const bc = p.getForm().findField('toClientInvestmentAccount.id');
									bc.clearValue();
									bc.store.removeAll();
									bc.lastQuery = null;
								}
							}
						},
						{
							fieldLabel: 'To Holding Account', name: 'toHoldingInvestmentAccount.label', xtype: 'combo', hiddenName: 'toHoldingInvestmentAccount.id', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['toClientInvestmentAccount.label'], allowBlank: false,
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm().getForm();
								queryEvent.combo.store.baseParams = {
									mainAccountId: f.findField('toClientInvestmentAccount.id').value,
									mainPurposeActiveOnDate: f.findField('transactionDate').value
								};
							}
						},
						{boxLabel: '"To" account receives collateral positions', name: 'collateralTransfer', xtype: 'checkbox'},
						{
							xtype: 'radiogroup', fieldLabel: '', allowBlank: false, columns: [200, 300],
							items: [
								{boxLabel: 'Keep Original Transaction Date', name: 'useOriginalTransactionDate', inputValue: true, checked: true},
								{boxLabel: 'Use Transaction Date for opening positions', name: 'useOriginalTransactionDate', inputValue: false}
							]
						}
					]
				},
				{fieldLabel: 'Notes', name: 'note', xtype: 'textarea', height: 40, allowBlank: false},

				// Below items are position selection criteria
				{xtype: 'sectionheaderfield', header: 'Security(ies) Filter'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {
						columnWidth: .50,
						layout: 'form',
						defaults: {
							anchor: '-20'
						}
					},
					items: [
						{
							items: [
								{
									fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'investmentTypeList.json',
									mutuallyExclusiveFields: ['instrumentHierarchy.labelExpanded', 'investmentInstrument.label'],
									listeners: {
										select: function(combo, record, index) {
											const form = combo.getParentForm().getForm();
											form.findField('investmentTypeSubType.name').clearAndReset();
											form.findField('investmentTypeSubType2.name').clearAndReset();
											form.findField('instrumentHierarchy.labelExpanded').clearAndReset();
											form.findField('investmentInstrument.label').clearAndReset();
											form.findField('security.label').clearAndReset();
										}
									}
								},
								{
									fieldLabel: 'Investment Sub Type', name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id', displayField: 'name', xtype: 'combo', loadAll: true, requiredFields: ['investmentType.name'], doNotClearIfRequiredChanges: true, url: 'investmentTypeSubTypeListByType.json', detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow',
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const f = combo.getParentForm().getForm();
											combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
										},
										select: function(combo, record, index) {
											const form = combo.getParentForm().getForm();
											form.findField('instrumentHierarchy.labelExpanded').clearAndReset();
											form.findField('investmentInstrument.label').clearAndReset();
											form.findField('security.label').clearAndReset();
										}
									}
								}, {
									fieldLabel: 'Investment Sub Type 2', name: 'investmentTypeSubType2.name', hiddenName: 'investmentTypeSubType2.id', displayField: 'name', xtype: 'combo', loadAll: true, requiredFields: ['investmentType.name'], doNotClearIfRequiredChanges: true, url: 'investmentTypeSubType2ListByType.json',
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const f = combo.getParentForm().getForm();
											combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
										},
										select: function(combo, record, index) {
											const form = combo.getParentForm().getForm();
											form.findField('instrumentHierarchy.labelExpanded').clearAndReset();
											form.findField('investmentInstrument.label').clearAndReset();
											form.findField('security.label').clearAndReset();
										}
									}
								},
								{
									fieldLabel: 'Investment Hierarchy', queryParam: 'labelExpanded', name: 'instrumentHierarchy.labelExpanded', hiddenName: 'instrumentHierarchy.id', displayField: 'labelExpanded', xtype: 'combo', limitRequestedProperties: false, url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', listWidth: 600, detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
									mutuallyExclusiveFields: ['investmentType.name', 'investmentInstrument.label'],
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const form = queryEvent.combo.getParentForm().getForm();
											combo.store.baseParams = {
												investmentTypeId: form.findField('investmentType.id').getValue(),
												investmentTypeSubTypeId: form.findField('investmentTypeSubType.id').getValue(),
												investmentTypeSubType2Id: form.findField('investmentTypeSubType2.id').getValue()
											};
										},
										select: function(combo, record, index) {
											const form = combo.getParentForm().getForm();
											const investmentType = record.json.investmentType;
											form.findField('investmentType.name').setValue({value: investmentType.id, text: investmentType.name});
											form.findField('investmentInstrument.label').clearAndReset();
											form.findField('security.label').clearAndReset();
										}
									}
								}
							]
						},
						{
							defaults: {
								anchor: '0'
							},
							items: [
								{
									fieldLabel: 'Instrument Group', name: 'investmentGroup.label', hiddenName: 'investmentGroup.id', xtype: 'combo', disableAddNewItem: true, url: 'investmentGroupListFind.json', width: 225,
									mutuallyExclusiveFields: ['securityGroup.label'],
									listeners: {
										select: function(combo, record, index) {
											const form = combo.getParentForm().getForm();
											form.findField('instrumentHierarchy.labelExpanded').clearAndReset();
											form.findField('investmentInstrument.label').clearAndReset();
											form.findField('security.label').clearAndReset();
										}
									}
								},
								{
									fieldLabel: 'Security Group', name: 'securityGroup.label', hiddenName: 'securityGroup.id', xtype: 'combo', width: 225, loadAll: true, url: 'investmentSecurityGroupListFind.json',
									mutuallyExclusiveFields: ['investmentGroup.label'],
									listeners: {
										select: function(combo, record, index) {
											const form = combo.getParentForm().getForm();
											form.findField('instrumentHierarchy.labelExpanded').clearAndReset();
											form.findField('investmentInstrument.label').clearAndReset();
											form.findField('security.label').clearAndReset();
										}
									}
								},
								{
									fieldLabel: 'Investment Instrument', name: 'investmentInstrument.label', hiddenName: 'investmentInstrument.id', displayField: 'label', xtype: 'combo', limitRequestedProperties: false, url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
									mutuallyExclusiveFields: ['investmentType.name', 'instrumentHierarchy.labelExpanded'],
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const form = combo.getParentForm().getForm();
											combo.store.baseParams = {
												hierarchyId: form.findField('instrumentHierarchy.id').getValue(),
												investmentTypeId: form.findField('investmentType.id').getValue(),
												investmentTypeSubTypeId: form.findField('investmentTypeSubType.id').getValue(),
												investmentTypeSubType2Id: form.findField('investmentTypeSubType2.id').getValue(),
												investmentGroupId: form.findField('investmentGroup.id').getValue()
											};
										},
										select: function(combo, record, index) {
											const form = combo.getParentForm().getForm();
											const hierarchy = record.json.hierarchy;
											const investmentType = hierarchy.investmentType;
											form.findField('investmentType.name').setValue({value: investmentType.id, text: investmentType.name});
											form.findField('instrumentHierarchy.labelExpanded').setValue({value: hierarchy.id, text: hierarchy.labelExpanded});
											form.findField('security.label').clearAndReset();
										}
									}
								},
								{
									fieldLabel: 'Investment Security', name: 'security.label', hiddenName: 'security.id', displayField: 'label', xtype: 'combo', limitRequestedProperties: false, url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const form = combo.getParentForm().getForm();
											combo.store.baseParams = {
												hierarchyId: form.findField('instrumentHierarchy.id').getValue(),
												instrumentId: form.findField('investmentInstrument.id').getValue(),
												investmentTypeId: form.findField('investmentType.id').getValue(),
												investmentTypeSubTypeId: form.findField('investmentTypeSubType.id').getValue(),
												investmentTypeSubType2Id: form.findField('investmentTypeSubType2.id').getValue(),
												securityGroupId: form.findField('securityGroup.id').getValue()
											};
										},
										select: function(combo, record, index) {
											const form = combo.getParentForm().getForm();
											const instrument = record.json.instrument;
											const hierarchy = instrument.hierarchy;
											const investmentType = hierarchy.investmentType;
											form.findField('investmentType.name').setValue({value: investmentType.id, text: investmentType.name});
											form.findField('instrumentHierarchy.labelExpanded').setValue({value: hierarchy.id, text: hierarchy.labelExpanded});
											form.findField('investmentInstrument.label').setValue({value: instrument.id, text: instrument.label});
										},
										change: function(combo, newValue, oldValue) {
											if (newValue === '') {
												const form = combo.getParentForm().getForm();
												form.findField('investmentType.name').clearAndReset();
												form.findField('instrumentHierarchy.labelExpanded').clearAndReset();
												form.findField('investmentInstrument.label').clearAndReset();
											}
										}
									}
								}
							]
						}
					]
				},
				{xtype: 'sectionheaderfield', header: 'Transfer Amount'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {
						columnWidth: .50,
						layout: 'form',
						defaults: {
							anchor: '-20'
						}
					},
					items: [
						{
							items: [
								{
									fieldLabel: 'Quantity', name: 'quantity', xtype: 'floatfield', allowDecimals: true, decimalPrecision: 10,
									mutuallyExclusiveFields: ['quantityPercent', 'originalFace', 'originalFacePercent'],
									qtip: 'The quantity to transfer. Specifying a quantity greater than what is available, will return only the total number actually available.'
								},
								{
									fieldLabel: 'Quantity Percent', name: 'quantityPercent', xtype: 'floatfield', minValue: 0, maxValue: 100, decimalPrecision: 10,
									mutuallyExclusiveFields: ['quantity', 'originalFace', 'originalFacePercent'],
									qtip: 'The percentage of the total net security position to transfer. Specify a value between 0 and 100.'
								},
								{
									fieldLabel: 'Percent Rounding Mode', name: 'percentRoundingMode', hiddenName: 'percentRoundingMode', xtype: 'combo', mode: 'local', valueField: 'percentRoundingMode',
									qtip: 'Required if Quantity Percent or Original Face Percent are specified. Defines how the calculated transfer amount from the percent will be rounded.',
									store: {
										xtype: 'arraystore',
										fields: ['name', 'percentRoundingMode', 'description'],
										data: [
											['Round Down', 'DOWN', 'Round the quantity calculated from the provided percentage down.'],
											['Round Up', 'UP', 'Round the quantity calculated from the provided percentage up.'],
											['Round Half Up', 'HALF_UP', 'Use traditional rounding to round the quantity calculated from the provided percentage up if over X.5, otherwise round down.']
										]
									}
								}
							]
						},
						{
							defaults: {
								anchor: '0'
							},
							items: [
								{
									fieldLabel: 'Original Face', name: 'originalFace', xtype: 'currencyfield', allowNegative: true,
									mutuallyExclusiveFields: ['quantity', 'quantityPercent', 'originalFacePercent'],
									qtip: 'The face amount to transfer. Original Face is used for Bonds or another security type that allows factor change events.'
								},
								{
									fieldLabel: 'Original Face Percent', name: 'originalFacePercent', xtype: 'floatfield', minValue: 0, maxValue: 100, decimalPrecision: 10,
									mutuallyExclusiveFields: ['quantity', 'quantityPercent', 'originalFace'],
									qtip: 'The percentage of total face amount to transfer. Specify a value between 0 and 100. Original Face Percent is used for Bonds or another security type that allows factor change events.'
								},
								{
									fieldLabel: 'Original Face Rounding', name: 'originalFaceRounding', xtype: 'floatfield', requiredFields: ['originalFacePercent'], allowBlank: false,
									qtip: 'The rounding mode for the face amount calculated from the specified percentage. Examples include: 1000, 100, 10.'
								}
							]
						}
					]
				}
			]
		}
	]
});
