Clifton.accounting.positiontransfer.TransferExistingGrid = {
	xtype: 'formgrid-scroll',
	storeRoot: 'detailList',
	height: 270,
	heightResized: true,
	dtoClass: 'com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail',
	doNotSubmitFields: ['marketValue'],

	afterRender: function() {
		TCG.grid.FormGridPanel.prototype.afterRender.apply(this, arguments);
	},


	columnsConfig: [
		{header: 'Position ID', dataIndex: 'existingPosition.id', width: 75},
		{header: 'Security', dataIndex: 'security.label', idDataIndex: 'security.id', width: 150},
		{header: 'Orig Face', dataIndex: 'originalFace', type: 'float', width: 80, summaryType: 'sum', editor: {xtype: 'currencyfield'}, useNull: true, hidden: true},
		{
			header: 'Quantity', dataIndex: 'quantity', type: 'float', width: 80, summaryType: 'sum', editor: {
				xtype: 'floatfield',
				onChangeHandler: function(field, newValue, oldValue) {
					const grid = field.gridEditor.containerGrid;
					const row = field.gridEditor.row;
					grid.updateCostBasis(grid, row, null, newValue, true);
				}
			}
		},
		{header: 'Open Date', dataIndex: 'originalPositionOpenDate', width: 80},
		{header: 'FX Rate', dataIndex: 'exchangeRateToBase', type: 'currency', numberFormat: '0,000.0000000000', width: 95, editor: {xtype: 'floatfield'}, hidden: true},
		{header: 'Cost Price', dataIndex: 'costPrice', type: 'float', width: 95, useNull: true},
		{
			header: 'Transfer Price', dataIndex: 'transferPrice', type: 'float', width: 95, userNull: true, editor: {
				xtype: 'floatfield',
				onChangeHandler: function(field, newValue, oldValue) {
					const grid = field.gridEditor.containerGrid;
					const row = field.gridEditor.row;
					grid.updateCostBasis(grid, row, newValue, null, false);
				}
			}
		},
		{header: 'Commission Per Unit', width: 80, dataIndex: 'commissionPerUnitOverride', type: 'float', useNull: true, editor: {xtype: 'floatfield', allowBlank: true}, tooltip: 'Commission Per Unit Override. Leave Blank to apply default schedule.', hidden: true},
		{header: 'Cost Basis', dataIndex: 'positionCostBasis', type: 'currency', width: 95, editor: {xtype: 'currencyfield', readOnly: true}, summaryType: 'sum'},
		{
			header: 'Market Value', dataIndex: 'marketValue', type: 'currency', width: 95, editor: {xtype: 'currencyfield', readOnly: true}, summaryType: 'sum',
			summaryRenderer: function(v) {
				if (v !== '') {
					return TCG.renderAmount(v, false, '0,000.00');
				}
			}
		}
	],
	plugins: {ptype: 'gridsummary'},
	addToolbarRemoveButton: function(toolBar) {
		TCG.grid.FormGridPanel.prototype.addToolbarRemoveButton.apply(this, arguments);
		toolBar.add('-');

		const grid = this;
		toolBar.add({
			text: 'Remove All',
			tooltip: 'Remove selected item',
			iconCls: 'remove',
			scope: this,
			handler: function() {
				const store = grid.getStore();
				store.removeAll();
				grid.markModified();
			}
		});
	},
	addToolbarAddButton: function(toolBar) {
		toolBar.add({
			text: 'Add',
			tooltip: 'Select existing position(s)',
			iconCls: 'add',
			scope: this,
			handler: function() {
				const f = TCG.getParentFormPanel(this).getForm();
				const clientId = f.findField('fromClientInvestmentAccount.id').value;
				const holdingId = f.findField('fromHoldingInvestmentAccount.id').value;
				const transactionDate = f.findField('transactionDate').value;
				if (Ext.isNumber(holdingId)) {
					TCG.createComponent('Clifton.accounting.positiontransfer.PositionSelectWindow', {
						modal: true,
						params: {
							clientAccountId: clientId,
							holdingAccountId: holdingId,
							transactionDate: transactionDate
						},
						openerCt: this
					});
				}
				else {
					TCG.showError('You must select accounts FROM which positions are being transfered.', 'Required Field');
				}
			}
		});
	},
	listeners: {
		// drill into position being transfered
		cellclick: function(grid, rowIndex, cellIndex, evt) {
			if (cellIndex === 0) {
				const row = grid.store.data.items[rowIndex];
				const id = row.data['existingPosition.id'];
				if (id !== '') {
					TCG.createComponent('Clifton.accounting.gl.TransactionWindow', {
						id: TCG.getComponentId('Clifton.accounting.gl.TransactionWindow', id),
						params: {id: id},
						openerCt: grid
					});
				}
			}
		}
	},

	getSelectedPositions: function(className) {
		const grid = this;
		const store = grid.getStore();
		const positions = [];
		store.each(function(record) {
			const r = {
				class: className,
				transactionId: record.data['existingPosition.id'],
				investmentSecurityId: record.data['security.id'],
				closingPrice: record.data.transferPrice,
				closingQuantity: record.data.quantity,
				marketValue: record.data.marketValue
			};

			if (true === TCG.data.getData('investmentSecurityEventTypeAllowedForSecurity.json?securityId=' + r.investmentSecurityId + '&eventTypeName=Factor Change', grid, 'security.event.type.allowed.factorChange.' + r.investmentSecurityId)) {
				r.quantity = record.data.originalFace;
			}
			positions.push(r);
		});
		return positions;
	},

	processSelection: function(records) {
		if (records && records.length > 0) {
			const grid = this;
			grid.loading = true;
			try {
				// When actually entering position transfers - make sure market value is displayed
				const cm = grid.getColumnModel();
				cm.setHidden(cm.findColumnIndex('marketValue'), false);

				const store = grid.getStore();
				const RowClass = store.recordType;
				grid.stopEditing();
				for (let i = 0; i < records.length; i++) {
					const row = new RowClass();
					const r = records[i].json ? records[i].json : records[i];
					const t = r;
					row.set('existingPosition.id', r.id);
					row.set('security.id', t.investmentSecurity.id);
					row.set('security.label', t.investmentSecurity.symbol);
					row.set('originalPositionOpenDate', TCG.parseDate(t.originalTransactionDate).format('m/d/Y'));
					row.set('exchangeRateToBase', t.exchangeRateToBase);
					row.set('costPrice', t.price);
					row.set('transferPrice', t.closingPrice ? t.closingPrice : t.price);
					row.set('quantity', r.remainingQuantity);
					row.set('positionCostBasis', r.remainingCostBasis);
					if (true === TCG.data.getData('investmentSecurityEventTypeAllowedForSecurity.json?securityId=' + t.investmentSecurity.id + '&eventTypeName=Factor Change', grid, 'security.event.type.allowed.factorChange.' + t.investmentSecurity.id)) {
						row.set('originalFace', t.quantity);
						cm.setHidden(cm.findColumnIndex('originalFace'), false);
					}
					if (r.marketValue) {
						row.set('marketValue', r.marketValue);
					}
					store.add(row);
					grid.markModified();
				}
				grid.startEditing(0, 3);
				grid.getView().refresh();
			}
			finally {
				grid.loading = false;
			}
		}
	},

	updateCostBasis: async function(grid, row, price, quantity, updateMV) {
		if (grid.loading === true) {
			return;
		}
		const f = TCG.getParentFormPanel(this).getForm();
		const rowData = grid.getStore().getAt(row).data;
		const securityId = rowData['security.id'];
		if (!price) {
			price = rowData.transferPrice;
		}
		if (typeof price == 'string') {
			price = price.replace(/,/g, '');
		}
		if (!quantity) {
			quantity = rowData.quantity;
		}
		if (typeof quantity == 'string') {
			quantity = quantity.replace(/,/g, '');
		}
		let date = f.findField('exposureDate').value;
		if (!date) {
			date = f.findField('transactionDate').value;
		}

		if (securityId && date && quantity && price) {
			const notionalMultiplier = await TCG.data.getDataValuePromise('marketDataIndexRatioForSecurity.json?securityId=' + securityId + '&date=' + date, this);
			const cb = await TCG.data.getDataValuePromise('investmentSecurityNotional.json?securityId=' + securityId + '&price=' + (price * notionalMultiplier) + '&quantity=' + quantity, this);
			const columnIndex = grid.getColumnModel().findColumnIndex('positionCostBasis');
			grid.startEditing(row, columnIndex);
			const costBasis = grid.getColumnModel().getCellEditor(columnIndex, row).field;
			costBasis.setValue(cb);
		}

		/** If Updating MV (set to true when quantity column is edited only)**/
		const existingPositionId = rowData['existingPosition.id'];
		if (updateMV === true && existingPositionId) {
			const mv = await this.calculateMarketValue(rowData['existingPosition.id'], quantity);
			const columnIndex = grid.getColumnModel().findColumnIndex('marketValue');
			grid.startEditing(row, columnIndex);
			const marketValue = grid.getColumnModel().getCellEditor(columnIndex, row).field;
			marketValue.setValue(mv);
		}
	},

	calculateMarketValue: async function(existingPosId, quantity) {
		const f = TCG.getParentFormPanel(this).getForm();

		// Once the position transfer has been booked, the existing position has likely been closed, this position is considered to no longer existing because remaining quantity = 0
		if (f.findField('bookingDate').getValue() === '') {
			let date = f.findField('exposureDate').value;
			if (!date) {
				date = f.findField('transactionDate').value;
			}

			return TCG.data.getDataValuePromise('accountingPositionMarketValueForExistingPosition.json?existingPositionId=' + existingPosId + '&overrideQuantity=' + quantity + '&date=' + date, this);
		}
		return null;
	}
}
;
