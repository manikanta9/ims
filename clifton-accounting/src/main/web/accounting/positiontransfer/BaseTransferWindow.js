Clifton.accounting.positiontransfer.BaseTransferWindow = Ext.extend(TCG.app.DetailWindow, {

	transferTab: {
		title: 'OVERRIDE ME',
		items: [{}]
	},

	init: function() {
		// replace the first tab with the override
		const tabs = this.items[0].items;
		tabs[0] = this.transferTab;
		Clifton.accounting.positiontransfer.BaseTransferWindow.superclass.init.apply(this, arguments);
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{}, // first tab to be overridden


			{
				title: 'Violations',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'AccountingPositionTransfer'
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'AccountingPositionTransfer'
				}]
			},


			{
				title: 'Transfer Instructions',
				items: [{
					xtype: 'investment-instruction-item-grid',
					tableName: 'AccountingPositionTransfer',
					getInstructionDate: function() {
						return TCG.getValue('transactionDate', this.getWindow().getMainForm().formValues);
					}
				}]
			}
		]
	}]
});
