Clifton.accounting.positiontransfer.upload.PositionTransferUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Position Transfers Import',
	iconCls: 'import',
	height: 550,
	hideOKButton: true,
	saveTimeout: 240,

	items: [
		{
			xtype: 'tabpanel',
			reloadOnChange: true,
			items: [
				{
					title: 'Position Transfer Import',
					tbar: [
						{
							text: 'Sample File',
							iconCls: 'excel',
							tooltip: 'Download Excel Sample File for Custom Accounting Position Transfer Uploads.',
							handler: function() {
								TCG.openFile('accounting/positiontransfer/upload/CustomPositionTransferUploadFileSample.xls');
							}
						},
						{
							text: 'Sample File (With Comments)',
							iconCls: 'excel',
							tooltip: 'Download Excel Sample File for Custom Accounting Position Transfer Uploads containing comments for columns.',
							handler: function() {
								TCG.openFile('accounting/positiontransfer/upload/CustomPositionTransferUploadFileSampleWithComments.xls');
							}
						}
					],
					items: [
						{
							xtype: 'formpanel',
							loadValidation: false,
							loadDefaultDataAfterRender: true,
							fileUpload: true,
							instructions: 'Position Transfer Uploads allow you to import position transfers from files directly into the system.  Use the options below to provide defaults for values undefined in the upload file.',

							getDefaultData: function(win) {
								let dd = win.defaultData || {};
								if (win.defaultDataIsReal) {
									return dd;
								}
								const now = new Date().format('Y-m-d 00:00:00');
								dd = Ext.apply({transactionDate: now}, dd);
								return dd;
							},

							items: [
								{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
								{fieldLabel: 'Transaction Date', name: 'transactionDate', xtype: 'datefield', allowBlank: false},
								{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false},
								{fieldLabel: 'Price Date', name: 'priceDate', xtype: 'datefield', allowBlank: true},
								{fieldLabel: 'Transfer Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', url: 'accountingPositionTransferTypeListFind.json'},
								{xtype: 'label', html: 'Position transfers without a specific description in the upload file will have this note as the Position Transfer Description.'},
								{fieldLabel: 'Notes', name: 'note', xtype: 'textarea'},
								{
									xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
									items: [
										{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
									]
								}
							],

							getSaveURL: function() {
								return 'accountingPositionTransferFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&requestedPropertiesRoot=data&requestedProperties=uploadResultsString';
							}
						}
					]
				}
			]
		}
	]
});
