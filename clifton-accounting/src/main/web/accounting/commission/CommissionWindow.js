Clifton.accounting.commission.CommissionWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Commission and Fee Definition',
	iconCls: 'fee',
	width: 800,
	height: 840,

	getOptionArrayFromMap: function(map, hierarchyIsOTC) {
		const options = [];
		for (const key in map) {
			if (map.hasOwnProperty(key) && (TCG.isEquals(map[key].validOption, null) || map[key].validOption(hierarchyIsOTC))) {
				options.push(map[key]['optionInfo']);
			}
		}
		return options;
	},

	getCommissionTypeOptions: function(hierarchyIsOTC) {
		return this.getOptionArrayFromMap(Clifton.accounting.commission.CommissionTypes, hierarchyIsOTC);
	},

	isCommissionApplyMethodRelevant: function(transactionTypeField) {
		return transactionTypeField.getParentForm().form.formValues.transactionType.commissionApplyMethodApplicable;
	},

	hasExplicitCommissionRanges: function(commissionType) {
		return Clifton.accounting.commission.CommissionTypes[commissionType]['hasRanges'];
	},

	shouldShowCommissionAmount: function(commissionType) {
		const showCommissionAmount = Clifton.accounting.commission.CommissionTypes[commissionType]['showCommissionAmount'];
		return TCG.isNull(showCommissionAmount) || TCG.isTrue(showCommissionAmount);
	},

	getRangeColumnName: function(transactionType) {
		return Clifton.accounting.commission.CommissionTypes[transactionType]['rangeColumnName'];
	},


	showOrHideApplyOnSelection: function(form) {
		const transactionTypeCombo = form.findField('transactionType.nameExpanded');
		const commissionApplyMethodCombo = form.findField('commissionApplyMethod');
		const halfTurnTransferCheckbox = form.findField('halfTurnOnTransfer');

		const shouldRenderApplyMethodCombo = this.isCommissionApplyMethodRelevant(transactionTypeCombo);

		if (shouldRenderApplyMethodCombo) {
			commissionApplyMethodCombo.show();
			halfTurnTransferCheckbox.show();
		}
		else {
			commissionApplyMethodCombo.hide();
			halfTurnTransferCheckbox.hide();
		}
	},

	showOrHideApplyHalfTurnOnTransfer: function(form) {
		const checkbox = form.findField('halfTurnOnTransfer');
		if (form.findField('commissionApplyMethod').value === 'ROUND_TURN') {
			checkbox.show();
			checkbox.setValue(true);
		}
		else {
			checkbox.hide();
		}
	},

	updateAdditionalScopeFieldBasedOnTransactionType: function(form, transactionType) {
		const additionalScopeField = form.findField('additionalScopeHolder.name');
		const transactionTypeData = transactionType ? transactionType : TCG.getValue('transactionType', form.formValues);
		if (transactionTypeData && transactionTypeData.additionalScopeSystemTable && transactionTypeData.additionalScopeBeanIdPath) {
			const additionalScopeTable = transactionTypeData.additionalScopeSystemTable;

			let additionalScopeLabel = transactionTypeData.additionalScopeUserInterfaceLabel;

			if (TCG.isBlank(additionalScopeLabel)) {
				additionalScopeLabel = additionalScopeTable.label;
			}
			additionalScopeField.setFieldLabel(additionalScopeLabel);

			const additionalScopeUrl = additionalScopeTable.entityListUrl;
			if (TCG.isNotBlank(additionalScopeUrl)) {
				additionalScopeField.setUrl(additionalScopeUrl);
			}

			const detailPageClass = additionalScopeTable.detailScreenClass;
			if (TCG.isNotBlank(detailPageClass)) {
				additionalScopeField.detailPageClass = detailPageClass;
			}

			additionalScopeField.show();
		}
		else {
			additionalScopeField.hide();
		}
	},

	hierarchyIsOTC: false, //initialize

	updateCommissionTypeOptions: function(fp) {
		const window = fp.getWindow();
		const data = window.getCommissionTypeOptions(this.hierarchyIsOTC);
		const commissionTypeField = fp.getForm().findField('commissionType');
		commissionTypeField.getStore().loadData(data, false);

		//Refresh the dropdown to use the right display text
		window.refreshComboBox(commissionTypeField);
	},

	refreshComboBox: function(comboField) {
		const disabled = comboField.disabled;

		comboField.setDisabled(false);
		comboField.setValue(comboField.getValue());

		if (disabled) {
			comboField.setDisabled(true);
		}
	},

	showOrHideCommissionRates: function(fp) {
		const form = fp.getForm();
		const window = fp.getWindow();

		const commissionType = form.findField('commissionType').value;
		const commissionAmount = form.findField('commissionAmount');

		if (window.shouldShowCommissionAmount(commissionType)) {
			commissionAmount.show();
		}
		else {
			commissionAmount.hide();
		}

		const commissionTiersFieldSet = TCG.getChildByName(fp, 'commissionTiersFieldSet');
		if (window.hasExplicitCommissionRanges(commissionType)) {
			const columnName = window.getRangeColumnName(commissionType);
			commissionTiersFieldSet.getComponent('tiersGrid').getColumnModel().setColumnHeader(0, columnName);
			commissionTiersFieldSet.show();
		}
		else {
			commissionTiersFieldSet.hide();
		}
	},

	constructViewBasedOnSelection: function(fp) {
		this.showOrHideCommissionRates(fp);
		this.showOrHideApplyOnSelection(fp.getForm());
		this.showOrHideApplyHalfTurnOnTransfer(fp.getForm());
		this.updateCommissionTypeOptions(fp);
		this.updateAdditionalScopeFieldBasedOnTransactionType(fp.getForm());
	},

	getInvestmentTypeField: function(form) {
		return form.findField('investmentType.name');
	},

	getHierarchyField: function(form) {
		return form.findField('instrumentHierarchy.labelExpanded');
	},

	getInstrumentField: function(form) {
		return form.findField('investmentInstrument.label');
	},

	getSecurityField: function(form) {
		return form.findField('investmentSecurity.label');
	},

	clearFieldStore: function(field) {
		field.store.removeAll();
		field.lastQuery = null;
	},

	setDatesFromSecurity: function(form, security) {
		if (TCG.isNotBlank(security.startDate)) {
			form.findField('startDate').setValue(new Date(security.startDate));
		}

		if (TCG.isNotBlank(security.endDate)) {
			form.findField('endDate').setValue(new Date(security.endDate));
		}
	},

	getTransactionTypeByName: function(name) {
		return TCG.data.getDataPromiseUsingCaching('accountingTransactionTypeByName.json?name=' + name + '&requestedPropertiesRoot=data', this, 'accounting.gl.TransactionType ' + name);
	},

	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Details',
				xtype: 'formpanel',
				url: 'accountingCommissionDefinition.json',
				layout: 'anchor',
				defaults: {anchor: '-15'},
				labelWidth: 140,
				getDefaultData: async function(win) {
					const dd = {...win.defaultData};
					if (win.defaultDataIsReal) {
						return dd;
					}

					const promises = [];

					let transactionTypeObj = dd.transactionType;
					if (TCG.isEquals(transactionTypeObj, null)) {
						transactionTypeObj = 'Trade';
					}
					if (transactionTypeObj) {
						//In this case the type object is a string that represents the name
						if (typeof transactionTypeObj === 'string' || transactionTypeObj instanceof String) {
							promises.push((async () => dd.transactionType = await win.getTransactionTypeByName(transactionTypeObj))());
						}
					}

					if (TCG.isEquals(dd.commissionType, null)) {
						dd.commissionType = 'SIMPLE';
					}

					if (TCG.isEquals(dd.commissionCalculationMethod, null)) {
						dd.commissionCalculationMethod = 'PER_UNIT';
					}

					if (TCG.isEquals(dd.commissionApplyMethod, null)) {
						dd.commissionApplyMethod = 'HALF_TURN';
					}

					//Set the 'hierarchyIsOTC' field if the hierarchy has been set in defaultData
					if (dd.instrumentHierarchy) {
						const url = 'investmentInstrumentHierarchy.json?id=' + dd.instrumentHierarchy.id;
						promises.push((async () => win.hierarchyIsOTC = (await TCG.data.getDataPromise(url, this)).otc)());
					}
					await Promise.all(promises);

					return dd;
				},
				listeners: {
					/*
					 * We have to specify both afterrender() and afterload() because afterload() only gets called when loading
					 * an existing commission definition, and in some cases we populate the window with default values
					 * and still need to construct the view based on those defaults.
					 */
					afterrender: function(fp, isClosing) {
						const window = fp.getWindow();

						//If enabled fields is set, then disable all fields not included in the array
						const enabledFields = window.enabledFields;
						if (enabledFields) {
							const form = fp.getForm();
							const fieldValues = form.getFieldValues();
							for (const fieldName in fieldValues) {
								if (fieldValues.hasOwnProperty(fieldName)) {
									form.findField(fieldName).setDisabled(true);
								}
							}

							for (let i = 0; i < enabledFields.length; i++) {
								const field = form.findField(enabledFields[i]);
								if (field) {
									field.setDisabled(false);
								}
							}
						}

						/*
						 * A bit of a "hack" to prevent an exception from being thrown if this is an existing commission definition. commissionType
						 * will not be null if this is a new commission because of the default values being set.
						 */
						if (TCG.isNotBlank(fp.getForm().findField('commissionType').value)) {
							window.constructViewBasedOnSelection(fp);
						}
					},
					afterload: function(fp, isClosing) {
						const window = fp.getWindow();
						const form = fp.getForm();

						//First, do a lookup to determine whether the selected hierarchy is OTC.
						const hierarchyField = window.getHierarchyField(form);
						const hierarchyId = hierarchyField.getValue();
						if (TCG.isNotBlank(hierarchyId)) {
							const url = 'investmentInstrumentHierarchy.json?id=' + hierarchyId;
							const hierarchy = TCG.data.getData(url, fp);
							window.hierarchyIsOTC = hierarchy.otc;
						}

						//If an "optional" filter is set, then expand the "Scope: Optional" fieldset
						if (TCG.isNotBlank(form.findField('holdingCompany.id').getValue())
							|| TCG.isNotBlank(form.findField('holdingAccount.id').getValue())
							|| TCG.isNotBlank(form.findField('investmentExchange.id').getValue())
							|| TCG.isNotBlank(form.findField('executingCompany.id').getValue())) {
							TCG.getChildByName(fp, 'optionalScopeFieldset').expand();
						}

						//Update the view based on selections
						window.constructViewBasedOnSelection(fp);
					}
				},
				items: [
					{
						xtype: 'fieldset',
						title: 'Scope: Investment Security',
						instructions: 'Apply this commission to investment type, hierarchy, instrument, or security. The most specific selection takes precedence when determining which Commission Definition gets applied to a transaction.',
						items: [
							{
								fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', displayField: 'name', xtype: 'combo', loadAll: true, limitRequestedProperties: false,
								url: 'investmentTypeList.json',
								listeners: {
									//TODO Consider refactoring these sections to be consistent and use clearAndReset() for all needed fields.
									beforeselect: function(combo, record) {
										const f = combo.getParentForm().getForm();
										const subType = f.findField('investmentTypeSubType.name');
										subType.clearAndReset();
									},
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										const form = fp.getForm();
										const window = fp.getWindow();

										const hierarchyField = window.getHierarchyField(form);
										const instrumentField = window.getInstrumentField(form);
										const securityField = window.getSecurityField(form);

										//Reset the values
										hierarchyField.reset();
										instrumentField.reset();
										securityField.reset();

										//Clear the stores
										window.clearFieldStore(hierarchyField);
										window.clearFieldStore(instrumentField);
										window.clearFieldStore(securityField);

									}
								}
							},
							{
								fieldLabel: 'Investment Sub Type', name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id', displayField: 'name', xtype: 'combo', loadAll: true, limitRequestedProperties: false, requiredFields: ['investmentType.name'],
								url: 'investmentTypeSubTypeListByType.json',
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
									},
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										const form = fp.getForm();
										const window = fp.getWindow();

										const hierarchyField = window.getHierarchyField(form);
										const instrumentField = window.getInstrumentField(form);
										const securityField = window.getSecurityField(form);

										//Reset the values
										hierarchyField.reset();
										instrumentField.reset();
										securityField.reset();

										//Clear the stores
										window.clearFieldStore(hierarchyField);
										window.clearFieldStore(instrumentField);
										window.clearFieldStore(securityField);

									}
								}
							},
							{
								fieldLabel: 'Investment Hierarchy', queryParam: 'labelExpanded', name: 'instrumentHierarchy.labelExpanded', hiddenName: 'instrumentHierarchy.id', displayField: 'labelExpanded', xtype: 'combo', limitRequestedProperties: false,
								url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', listWidth: 600, detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const fp = combo.getParentForm();
										const form = fp.getForm();

										combo.store.baseParams = {
											investmentTypeId: form.findField('investmentType.id').getValue()
										};
									},
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										const form = fp.getForm();
										const window = fp.getWindow();

										const investmentTypeField = window.getInvestmentTypeField(form);
										const instrumentField = window.getInstrumentField(form);
										const securityField = window.getSecurityField(form);

										const investmentType = record.json.investmentType;

										investmentTypeField.setValue({value: investmentType.id, text: investmentType.name});
										instrumentField.reset();
										securityField.reset();

										window.hierarchyIsOTC = record.json.otc;
										// clear existing destinations
										window.clearFieldStore(instrumentField);
										window.clearFieldStore(securityField);
										form.findField('transactionType.nameExpanded').clearAndReset();
									}
								}
							},
							{
								fieldLabel: 'Investment Instrument', name: 'investmentInstrument.label', hiddenName: 'investmentInstrument.id', displayField: 'label', xtype: 'combo', limitRequestedProperties: false,
								url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										combo.store.baseParams = {
											hierarchyId: f.findField('instrumentHierarchy.id').getValue()
										};
									},
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										const form = fp.getForm();
										const window = fp.getWindow();

										const investmentTypeField = window.getInvestmentTypeField(form);
										const hierarchyField = window.getHierarchyField(form);
										const securityField = window.getSecurityField(form);

										const hierarchy = record.json.hierarchy;
										const investmentType = hierarchy.investmentType;

										investmentTypeField.setValue({value: investmentType.id, text: investmentType.name});
										hierarchyField.setValue({value: hierarchy.id, text: hierarchy.labelExpanded});
										securityField.reset();

										window.hierarchyIsOTC = record.json.hierarchy.otc;
										// clear existing destinations
										window.clearFieldStore(securityField);
									}
								}
							},
							{
								fieldLabel: 'Investment Security', name: 'investmentSecurity.label', hiddenName: 'investmentSecurity.id', displayField: 'label', xtype: 'combo', limitRequestedProperties: false,
								url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										const instrumentIdVal = f.findField('investmentInstrument.id').getValue();
										if (TCG.isNotBlank(instrumentIdVal)) {
											combo.store.baseParams = {
												instrumentId: instrumentIdVal
											};
										}
									},
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										const form = fp.getForm();
										const window = fp.getWindow();

										const instrument = record.json.instrument;
										const hierarchy = instrument.hierarchy;
										const investmentType = hierarchy.investmentType;

										const investmentTypeField = window.getInvestmentTypeField(form);
										const hierarchyField = window.getHierarchyField(form);
										const instrumentField = window.getInstrumentField(form);

										investmentTypeField.setValue({value: investmentType.id, text: investmentType.name});
										hierarchyField.setValue({value: hierarchy.id, text: hierarchy.labelExpanded});
										instrumentField.setValue({value: instrument.id, text: instrument.label});

										window.setDatesFromSecurity(form, record.json);

										window.hierarchyIsOTC = record.json.instrument.hierarchy.otc;
									}
								}
							}
						]
					},

					{
						xtype: 'fieldset',
						name: 'optionalScopeFieldset',
						collapsed: true,
						title: 'Scope: Optional',
						instructions: 'Optionally apply this commission/fee to a specific broker and/or broker account or exchange.  If none are specified, then use security scope selection only.',
						items: [
							{fieldLabel: 'Holding Company', name: 'holdingCompany.name', hiddenName: 'holdingCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?tradingAllowed=true', detailPageClass: 'Clifton.business.company.CompanyWindow', mutuallyExclusiveFields: ['holdingAccount.id'], disableAddNewItem: true},
							{fieldLabel: 'Holding Account', name: 'holdingAccount.label', hiddenName: 'holdingAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=false', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', mutuallyExclusiveFields: ['holdingCompany.id'], disableAddNewItem: true},
							{fieldLabel: 'Executing Company', name: 'executingCompany.name', hiddenName: 'executingCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?tradingAllowed=true', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true},
							{fieldLabel: 'Exchange / CCP', name: 'investmentExchange.name', hiddenName: 'investmentExchange.id', xtype: 'combo', url: 'investmentExchangeListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true},
							{
								fieldLabel: 'Additional Scope Fk Field ID',
								name: 'additionalScopeHolder.name',
								hiddenName: 'additionalScopeHolder.identifier',
								xtype: 'combo',
								url: 'OVERRIDDEN',
								detailPageClass: 'OVERRIDDEN',
								loadAll: true,
								hidden: true
							}
						]
					},

					{
						xtype: 'fieldset',
						title: 'Commission Definition',
						instructions: 'Commission Amount is defined in Currency Denomination of security that the commissions is applied to.',
						items: [
							{fieldLabel: 'GL Account', name: 'expenseAccountingAccount.name', hiddenName: 'expenseAccountingAccount.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'accountingAccountListFind.json?commission=true'},
							{
								fieldLabel: 'Transaction Type', name: 'transactionType.nameExpanded', hiddenName: 'transactionType.id', displayField: 'name', comboDisplayField: 'nameExpanded', xtype: 'combo', url: 'accountingTransactionTypeListFind.json?orderBy=displayOrder#asc', loadAll: true,
								detailPageClass: 'Clifton.accounting.gl.TransactionTypeWindow',
								disableAddNewItem: true,
								requestedProps: 'additionalScopeSystemTable|additionalScopeBeanIdPath',
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const fp = combo.getParentForm();
										const window = fp.getWindow();
										//If the hierarchyIsOTC variable is NOT set, we want to exclude any transaction type where it is set
										if (!window.hierarchyIsOTC) {
											combo.store.setBaseParam('restrictToOtcHierarchy', false);
										}
									},

									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										const window = fp.getWindow();
										window.showOrHideApplyOnSelection(fp.getForm());
										window.updateAdditionalScopeFieldBasedOnTransactionType(fp.getForm(), record.json);
									}
								}
							},
							{
								fieldLabel: 'Commission Currency', name: 'commissionCurrency.name', hiddenName: 'commissionCurrency.id', displayField: 'name', xtype: 'combo',
								url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
								qtip: 'Indicates the currency type of the commission. This should usually be left blank, which indicates that the currency denomination of the security is used.'
							},
							{
								fieldLabel: 'Commission/Fee Type', name: 'commissionType', hiddenName: 'commissionType', displayField: 'name', valueField: 'commissionType', xtype: 'combo', mode: 'local',
								store: {
									xtype: 'arraystore',
									fields: ['name', 'commissionType', 'description'],
									data: ['null', 'null', 'null'] //will be overridden
								},
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const fp = combo.getParentForm();
										const window = fp.getWindow();

										window.updateCommissionTypeOptions(fp);
									},

									select: function(combo, record, index) {
										const window = combo.getParentForm().getWindow();
										window.showOrHideCommissionRates(combo.getParentForm());
									}
								}
							},
							{
								xtype: 'fieldset',
								name: 'commissionTiersFieldSet',
								title: 'Commission Rates',
								instructions: 'Configure commission/fee rates here.',
								hidden: true,
								items: [{
									id: 'tiersGrid',
									xtype: 'formgrid',
									storeRoot: 'commissionTierList',
									dtoClass: 'com.clifton.accounting.commission.AccountingCommissionTier',
									columnsConfig: [
										{header: 'Notional Amount', width: 150, useNull: true, dataIndex: 'threshold', editor: {xtype: 'floatfield'}, type: 'float'},
										{header: 'Fee Amount', width: 150, useNull: false, dataIndex: 'feeAmount', editor: {xtype: 'floatfield'}, type: 'float'}
									]
								}]
							},
							{fieldLabel: 'Commission Amount', name: 'commissionAmount', xtype: 'floatfield', value: 0},
							{
								fieldLabel: 'Calculation Method', name: 'commissionCalculationMethod', hiddenName: 'commissionCalculationMethod', displayField: 'name', valueField: 'commissionCalculationMethod', xtype: 'combo', mode: 'local',
								store: {
									xtype: 'arraystore',
									fields: ['name', 'commissionCalculationMethod', 'description']
								},
								listeners: {
									beforerender: function(combo) {
										const methods = Clifton.accounting.commission.CommissionCalculationMethods;
										const options = [];
										for (const key in methods) {
											if (methods.hasOwnProperty(key)) {
												const calculationMethod = methods[key];
												options.push([calculationMethod.displayName, calculationMethod.commissionCalculationMethod, calculationMethod.description]);
											}
										}
										combo.getStore().loadData(options, false);
									}
								}
							},
							{fieldLabel: 'Min Amount', name: 'commissionMinimum', xtype: 'floatfield', value: 0},
							{
								fieldLabel: 'Min Calculation Method', name: 'minimumCalculationMethod', hiddenName: 'minimumCalculationMethod', displayField: 'name', valueField: 'commissionCalculationMethod', xtype: 'combo', mode: 'local',
								store: {
									xtype: 'arraystore',
									fields: ['name', 'commissionCalculationMethod', 'description']
								},
								listeners: {
									beforerender: function(combo) {
										const methods = Clifton.accounting.commission.CommissionCalculationMethods;
										const options = [];
										for (const key in methods) {
											if (methods.hasOwnProperty(key)) {
												const calculationMethod = methods[key];
												options.push([calculationMethod.displayName, calculationMethod.commissionCalculationMethod, calculationMethod.description]);
											}
										}
										combo.getStore().loadData(options, false);
									}
								}
							},
							{fieldLabel: 'Max Amount', name: 'commissionMaximum', xtype: 'floatfield', value: 0},
							{
								fieldLabel: 'Max Calculation Method', name: 'maximumCalculationMethod', hiddenName: 'maximumCalculationMethod', displayField: 'name', valueField: 'commissionCalculationMethod', xtype: 'combo', mode: 'local',
								store: {
									xtype: 'arraystore',
									fields: ['name', 'commissionCalculationMethod', 'description']
								},
								listeners: {
									beforerender: function(combo) {
										const methods = Clifton.accounting.commission.CommissionCalculationMethods;
										const options = [];
										for (const key in methods) {
											if (methods.hasOwnProperty(key)) {
												const calculationMethod = methods[key];
												options.push([calculationMethod.displayName, calculationMethod.commissionCalculationMethod, calculationMethod.description]);
											}
										}
										combo.getStore().loadData(options, false);
									}
								}
							},
							{
								fieldLabel: 'Apply On', name: 'commissionApplyMethod', hiddenName: 'commissionApplyMethod', displayField: 'name', valueField: 'commissionApplyMethod', xtype: 'combo', mode: 'local',
								store: {
									xtype: 'arraystore',
									fields: ['name', 'commissionApplyMethod', 'description'],
									data: [
										['Half-Turn (Open and Close)', 'HALF_TURN', 'The commission is applied half-turn (on open and on close).'],
										['Round-Turn (Double on Close)', 'ROUND_TURN', 'The commission is applied round-turn (only on close). Upon close, the commission amount will be <b>double</b> the commission amount(s) shown in this window.'],
										['Round-Turn On Sell (Double on Closing Sell)', 'ROUND_TURN_ON_SELL', 'The commission is applied round-turn (only on close), but only for a sell. When there is a closing sell, the commission amount will be <b>double</b> the commission amount(s) shown in this window. This option is commonly used for LMEs.'],
										['Only On Open', 'ONLY_ON_OPEN', 'The commission is only applied on the opening side of the trade.'],
										['Only On Close', 'ONLY_ON_CLOSE', 'The commission is only applied on the closing side of the trade.'],
										['Only On Close When Price Greater Than Zero', 'ONLY_ON_CLOSE_WITH_PRICE_GREATER_THAN_ZERO', 'The commission is only applied on the closing side of the trade when the price of the transaction is greater than zero. This may be used in conjunction with transaction type of Maturity to identify an expiring option position that is "in the money".'],
										['Only On Close When Price Equals Zero', 'ONLY_ON_CLOSE_WITH_PRICE_EQUAL_TO_ZERO', 'The commission is only applied on the closing side of the trade when the price of the transaction is greater equal to zero. This may be used in conjunction with transaction type of Maturity to identify an expiring option position that is "out of the money".'],
										['Only On Sell', 'ONLY_ON_SELL', 'The commission is only applied on a sell.']
									]
								},
								listeners: {
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										fp.getWindow().showOrHideApplyHalfTurnOnTransfer(fp.getForm());

									}
								}
							},
							{
								boxLabel: 'Apply half-turn commission on non-internal transfer', name: 'halfTurnOnTransfer', xtype: 'checkbox', hidden: true,
								qtip: 'Applies this commission/fee to a non-internal transfer of this security using half-turn logic. This effectively means that the source account will get charged for the transfer, ' +
									'and the destination account will only be charged for the close. This avoids the need to create a duplicate Commission Definition for non-internal transfers unless different settings are required.'
							},
							{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
							{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
							{
								fieldLabel: 'Adjustment Calculator',
								beanName: 'adjustmentCalculatorBean',
								xtype: 'system-bean-combo',
								groupName: 'Commission Adjustment Calculator',
								disableAddNewItem: true,
								qtip: 'This calculation is performed last and will adjust any previously calculated value.'
							}
						]
					}
				]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'AccountingCommissionDefinition'
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'AccountingCommissionDefinition'
				}]
			}
		]
	}]
});
