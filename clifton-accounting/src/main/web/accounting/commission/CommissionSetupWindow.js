Clifton.accounting.commission.CommissionSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingCommissionSetupWindow',
	iconCls: 'fee',
	title: 'Commissions and Fees',
	width: 1500,
	height: 700,
	maximized: true,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Commission Definitions',
				items: [{
					name: 'accountingCommissionDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'Commission definitions determine what commission(s) or fee(s) to charge to a specific transaction. Examples of transactions are trades, transfers, and maturity events. For any given transaction, ' +
						'the most specific commission definition is always applied. For example, commission definitions that specify a specific type of Future will override a commission definition that applies to all Futures. ' +
						'Furthermore, if Holding Company, Holding Account, or Exchange are specified, this will take precedence over commission definitions without it being specified (all else being equal). The commission amount ' +
						'is always defined in the currency denomination of the investment instrument that the commissions is applied to, unless the Commission Currency has been specified.',
					rowSelectionModel: 'checkbox',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'instrumentHierarchy.id|investmentType.id|expenseAccountingAccount.id|investmentInstrument.id|investmentSecurity.id|additionalScopeHolder',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Type', width: 70, dataIndex: 'investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Investment Sub Type', width: 80, dataIndex: 'investmentTypeSubType.name', hidden: true, filter: {searchFieldName: 'investmentTypeSubType', displayField: 'name'}},
						{header: 'Investment Hierarchy', width: 170, dataIndex: 'instrumentHierarchy.labelExpanded', filter: {type: 'combo', queryParam: 'labelExpanded', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', listWidth: 600}},
						{header: 'Investment Instrument', width: 140, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{header: 'Identifier Prefix', width: 70, dataIndex: 'investmentInstrument.identifierPrefix', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', displayField: 'identifierPrefix', url: 'investmentInstrumentListFind.json'}},
						{header: 'Investment Security', width: 120, dataIndex: 'investmentSecurity.label', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Clearing Broker', width: 100, dataIndex: 'holdingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingCompanyId', url: 'businessCompanyListFind.json?tradingAllowed=true'}},
						{header: 'Holding Account', width: 100, dataIndex: 'holdingAccount.label', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Executing Broker', width: 100, dataIndex: 'executingCompany.name', filter: {type: 'combo', searchFieldName: 'executingCompanyId', url: 'businessCompanyListFind.json?tradingAllowed=true'}},
						{header: 'Exchange', width: 100, dataIndex: 'investmentExchange.name', hidden: true, filter: {type: 'combo', searchFieldName: 'investmentExchangeId', displayField: 'label', url: 'investmentExchangeListFind.json'}},
						{header: 'GL Account', width: 100, dataIndex: 'expenseAccountingAccount.name', filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json?commission=true'}},
						{header: 'Commission Currency', width: 140, dataIndex: 'commissionCurrency.name', hidden: true, filter: {type: 'combo', searchFieldName: 'commissionCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{
							header: 'Transaction Type', width: 90, dataIndex: 'transactionType.name', filter: {type: 'combo', searchFieldName: 'accountingTransactionTypeId', displayField: 'name', url: 'accountingTransactionTypeListFind.json', loadAll: true}
						},
						{
							header: 'Additional Scope Table', width: 110, dataIndex: 'transactionType.additionalScopeSystemTable.name', filter: {type: 'combo', searchFieldName: 'additionalScopeSystemTableId', displayField: 'name', url: 'systemTableListFind.json?defaultDataSource=true', loadAll: true}, hidden: true
						},
						{
							header: 'Additional Scope FK ID', width: 50, dataIndex: 'additionalScopeFkFieldId', type: 'int', hidden: true
						},
						{
							header: 'Commission Type', width: 90, dataIndex: 'commissionType', filter: {type: 'list', options: Clifton.accounting.getFilterListFromType(Clifton.accounting.commission.CommissionTypes)}, renderer: function(value, metaData, r) {
								return Clifton.accounting.commission.CommissionTypes[value].displayName;
							}
						},
						{
							header: 'Apply On', width: 90, dataIndex: 'commissionApplyMethod', filter: {type: 'list', options: Clifton.accounting.getFilterListFromType(Clifton.accounting.commission.CommissionApplyMethods)}, renderer: function(value, metaData, r) {
								return Clifton.accounting.commission.CommissionApplyMethods[value].displayName;
							}
						},
						{
							header: 'Half Turn for Transfers', width: 80, dataIndex: 'halfTurnOnTransfer', type: 'boolean', hidden: true,
							tooltip: 'If the commission definition is a Round-Turn Trade transaction, applies this commission/fee to a non-internal transfer of this security using half-turn logic. This effectively means that the source account will get ' +
								'charged for the transfer, and the destination account will only be charged for the close. This avoids the need to create a duplicate Commission Definition for non-internal transfers unless different settings are required.'
						},
						{header: 'Commission Amount', width: 70, dataIndex: 'commissionAmount', type: 'float'},
						{
							header: 'Calc Method', width: 110, dataIndex: 'commissionCalculationMethod', hidden: true, filter: {type: 'list', options: Clifton.accounting.getFilterListFromType(Clifton.accounting.commission.CommissionCalculationMethods)}, renderer: function(value, metaData, r) {
								return Clifton.accounting.commission.CommissionCalculationMethods[value].displayName;
							}
						},
						{header: 'Min Amount', width: 70, dataIndex: 'commissionMinimum', type: 'float'},
						{
							header: 'Min Calc Method', width: 90, dataIndex: 'minimumCalculationMethod', hidden: true, filter: {type: 'list', options: Clifton.accounting.getFilterListFromType(Clifton.accounting.commission.CommissionCalculationMethods)}, renderer: function(value, metaData, r) {
								const method = Clifton.accounting.commission.CommissionCalculationMethods[value];
								if (method) {
									return Clifton.accounting.commission.CommissionCalculationMethods[value].displayName;
								}
							}
						},
						{header: 'Max Amount', width: 70, dataIndex: 'commissionMaximum', type: 'float'},
						{
							header: 'Max Calc Method', width: 90, dataIndex: 'maximumCalculationMethod', hidden: true, filter: {type: 'list', options: Clifton.accounting.getFilterListFromType(Clifton.accounting.commission.CommissionCalculationMethods)}, renderer: function(value, metaData, r) {
								const method = Clifton.accounting.commission.CommissionCalculationMethods[value];
								if (method) {
									return Clifton.accounting.commission.CommissionCalculationMethods[value].displayName;
								}
							}
						},
						{header: 'Adjustment Calculator', width: 150, dataIndex: 'adjustmentCalculatorBean.name', filter: {searchFieldName: 'adjustmentCalculatorBeanName'}, hidden: true},
						{header: 'Start Date', width: 70, dataIndex: 'startDate', type: 'date', hidden: true},
						{header: 'End Date', width: 70, dataIndex: 'endDate', type: 'date', hidden: true},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean', sortable: false} //Don't allow sorting on this field (not supported)
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
						return {};
					},
					editor: {
						detailPageClass: 'Clifton.accounting.commission.CommissionWindow',
						addEditButtons: function(toolBar) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);


							const gridPanel = this.getGridPanel();
							toolBar.add({
								text: 'Add Note',
								tooltip: 'Add a note linked to selected trades (NOTE: All trades must be for the same Trade Type)',
								iconCls: 'pencil',
								scope: this,
								handler: function() {
									gridPanel.addNoteToSelectedRows();
								}
							});
							toolBar.add({
								text: TCG.grid.GridEditor.prototype.copyButtonName,
								tooltip: 'Use selected definition as a template for creating a new definition (NOTE: In the event the selected definition is active, the newly created definition can end the selected one and replace it)',
								iconCls: 'copy',
								scope: this,
								handler: function() {
									gridPanel.copyAccountingCommissionDefinition();
								}
							});
							toolBar.add('-');
						}
					},
					addNoteToSelectedRows: function() {
						const gridPanel = this;
						const grid = this.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select at least one row to add a note to.', 'No Row(s) Selected');
						}
						else {
							const cIds = [];
							const ut = sm.getSelections();
							for (let i = 0; i < ut.length; i++) {
								cIds.push(ut[i].json.id);
							}

							let className = 'Clifton.system.note.SingleNoteWindow';

							const tbl = TCG.data.getData('systemTableByName.json?tableName=AccountingCommissionDefinition', gridPanel, 'system.table.AccountingCommissionDefinition');
							let dd = {
								noteType: {table: tbl}
							};
							if (cIds.length === 1) {
								dd = Ext.apply(dd, {linkedToMultiple: false, fkFieldId: cIds[0]});
							}
							else {
								className = 'Clifton.system.note.LinkedNoteWindow';
								dd = Ext.apply(dd, {linkedToMultiple: true, fkFieldIds: cIds});
							}
							const cmpId = TCG.getComponentId(className);
							TCG.createComponent(className, {
								id: cmpId,
								defaultData: dd,
								openerCt: gridPanel
							});
						}
					},
					copyAccountingCommissionDefinition: function() {
						const gridPanel = this;
						const grid = gridPanel.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to be base the copy on.', 'No Row(s) Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							const selectedRow = sm.getSelected();
							const rowJson = selectedRow.json;
							rowJson.id = undefined;
							TCG.createComponent(gridPanel.editor.detailPageClass, {
								id: undefined,
								defaultData: rowJson,
								openerCt: gridPanel
							});
						}
					}
				}]
			},


			{
				title: 'Commission Transactions',
				items: [{
					xtype: 'accounting-transactionGrid',
					instructions: 'General Ledger transactions for Commission and Fees GL accounts.',

					getLoadParams: function(firstLoad) {
						const params = Clifton.accounting.TransactionGrid.prototype.getLoadParams.call(this, firstLoad);

						if (firstLoad) {
							// default to last 30 days of transactions
							this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -30)});
						}

						params.commissionAccountingAccount = true;

						return params;
					}
				}]
			},


			{
				title: 'Transaction Types',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingTransactionTypeListFind',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'The available transaction types in the system. Types can be hierarchical - if a child type does not exist then the parent specified will be used if it exists',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Type Name', dataIndex: 'name', width: 70},
						{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
						{header: 'Parent Type', dataIndex: 'parent.name', width: 70, tooltip: 'The parent type that this type is a subtype of'},
						{header: 'Hierarchy', dataIndex: 'nameExpanded', width: 200, hidden: true, tooltip: 'The hierarchy string from the root down to this type'},
						{header: 'Additional Scope Table', dataIndex: 'additionalScopeSystemTable.name', width: 50},
						{header: 'Additional Scope Path', dataIndex: 'additionalScopeBeanIdPath', width: 50},
						{header: 'Transfer', dataIndex: 'transfer', type: 'boolean', width: 30},
						{header: 'Show Commission Apply Method', dataIndex: 'commissionApplyMethodApplicable', type: 'boolean', width: 50, tooltip: 'Denotes whether the Commission Apply Method field should be shown in the Commission Definition window'},
						{header: 'OTC Hierarchy', dataIndex: 'restrictToOtcHierarchy', type: 'boolean', width: 35, tooltip: 'When selected, this type will ONLY be shown when the Instrument/Hierarchy is OTC traded'},
						{header: 'Display Order', dataIndex: 'displayOrder', width: 30, type: 'int', defaultSortColumn: true, tooltip: 'The order in which the types should be shown in combo boxes'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.gl.TransactionTypeWindow'
					}
				}]
			}
		]
	}]
});

