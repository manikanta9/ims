Clifton.accounting.period.PeriodWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Accounting Period',
	iconCls: 'calendar',
	width: 900,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Details',
				items: [{
					xtype: 'formpanel',
					url: 'accountingPeriod.json',
					instructions: 'Accounting periods (usually monthly) define the time periods at the end of which accounting data is reconciled to external parties. After a period is closed for a specific client account, no GL activity (book/unbook) can be done for that period.',
					items: [
						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
					]
				}]
			},


			{
				title: 'Accounting Closings',
				items: [{
					name: 'accountingPeriodClosingListFind',
					xtype: 'gridpanel',
					useBufferView: false, // allow cell wrapping
					instructions: 'The following client investment accounts have been closed or re-open for this accounting period.',
					columns: [
						{header: 'Client Relationship', width: 150, dataIndex: 'investmentAccount.businessClient.clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'}, hidden: true},
						{header: 'Client', width: 150, dataIndex: 'investmentAccount.businessClient.label', filter: {searchFieldName: 'clientName'}, hidden: true},
						{header: 'Client Type', width: 80, dataIndex: 'investmentAccount.businessClient.clientType.text', filter: {searchFieldName: 'clientTypeName'}, hidden: true},
						{header: 'Issuing Company', width: 250, dataIndex: 'investmentAccount.issuingCompany.name', filter: {searchFieldName: 'investmentAccountIssuingCompanyId', type: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=true'}, hidden: true},
						{header: 'Client Account', width: 150, dataIndex: 'investmentAccount.label', filter: {searchFieldName: 'investmentAccountNumberOrName'}, defaultSortColumn: true},
						{header: 'Pooled Vehicle Type', width: 70, dataIndex: 'investmentAccount.clientAccountType', filter: {searchFieldName: 'clientAccountType'}, hidden: true},
						{header: 'GIPS', width: 70, dataIndex: 'investmentAccount.discretionType', filter: {searchFieldName: 'investmentAccountDiscretionType'}, hidden: true},
						{header: 'Service', width: 120, dataIndex: 'investmentAccount.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
						{header: 'EV Portfolio Code', width: 80, dataIndex: 'investmentAccount.externalPortfolioCode.text', hidden: true, filter: {searchFieldName: 'investmentAccountExternalPortfolioCodeName'}},
						{header: 'EV Product Code', width: 80, dataIndex: 'investmentAccount.externalProductCode.text', hidden: true, filter: {searchFieldName: 'investmentAccountExternalProductCodeName'}},
						{header: 'Channel', width: 100, dataIndex: 'investmentAccount.clientAccountChannel.text', hidden: true, filter: {searchFieldName: 'investmentAccountChannelName'}, renderer: TCG.renderValueWithTooltip},
						{header: 'Accounting Note', width: 100, dataIndex: 'note', hidden: true},
						{header: 'AUM/Revenue Note', width: 100, dataIndex: 'externalNote', hidden: true},
						{header: 'Notes', width: 250, dataIndex: 'combinedNote', sortable: false},
						{header: 'Preliminary AUM', width: 80, dataIndex: 'preliminaryPeriodEndAUM', type: 'currency', useNull: true, tooltip: 'Preliminary Estimate of Assets Under Management at the end of the period'},
						{header: 'AUM', width: 70, dataIndex: 'periodEndAUM', type: 'currency', useNull: true, tooltip: 'Assets Under Management at the end of the period'},
						{header: 'Projected Revenue (Gross)', width: 70, dataIndex: 'periodProjectedRevenue', type: 'currency', useNull: true, tooltip: 'Projected Revenue for this account for the accounting period (excluding annual minimum)', hidden: true},
						{header: 'Projected Revenue (Annual Min)', width: 70, dataIndex: 'periodProjectedRevenueAnnualMin', type: 'currency', useNull: true, tooltip: 'Annual Minimum portion of the projected revenue for the account and period.', hidden: true},

						{
							header: 'Projected Revenue Total (Gross)', width: 70, dataIndex: 'periodProjectedRevenueTotal', type: 'currency', useNull: true, tooltip: 'Total Projected Revenue for the Account and Accounting Period (Including Annual Minimum portion)',
							renderer: function(v, metaData, r) {
								const annualMin = r.data['periodProjectedRevenueAnnualMin'];
								if (TCG.isNotNull(annualMin) && annualMin !== 0) {
									return TCG.renderAdjustedAmount(v, true, r.data['periodProjectedRevenue'], '0,000.00', 'Annual Min Applied', false);
								}
								return TCG.renderAmount(v, false, '0,000.00');
							}
						},
						{header: 'Projected Broker Fee', width: 70, dataIndex: 'periodProjectedRevenueShareExternal', type: 'currency', useNull: true, tooltip: 'Portion of the projected revenue for the account and period that is paid to external party (Broker).', hidden: true},
						{
							header: 'Projected Revenue (Net)', width: 70, dataIndex: 'periodProjectedRevenueNetTotal', type: 'currency', useNull: true, tooltip: 'Gross Projected Revenue less Broker Fees for this account for the accounting period',
							renderer: function(v, metaData, r) {
								const brokerFee = r.data['periodProjectedRevenueShareExternal'];
								if (TCG.isNotNull(brokerFee) && brokerFee !== 0) {
									return TCG.renderAdjustedAmount(v, true, r.data['periodProjectedRevenueTotal'], '0,000.00', 'Broker Fee', false);
								}
								return TCG.renderAmount(v, false, '0,000.00');
							}
						},
						{header: 'Projected Revenue Share', width: 70, dataIndex: 'periodProjectedRevenueShareInternal', type: 'currency', useNull: true, tooltip: 'Portion of the projected revenue for the account and period that is shared with an internal party (i.e. EV).'},
						{header: 'Closed', width: 50, dataIndex: 'closed', type: 'boolean'},
						{header: '<div class="www" style="WIDTH:16px">&nbsp;</div>', width: 15, tooltip: 'If checked, there is at least one APPROVED file associated with this period on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.period.PeriodClosingWindow',
						drillDownOnly: true
					},
					getLoadParams: function() {
						return {'accountingPeriodId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
