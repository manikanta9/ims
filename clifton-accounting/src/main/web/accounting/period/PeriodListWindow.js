Clifton.accounting.period.PeriodListWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingPeriodListWindow',
	title: 'Accounting Periods',
	iconCls: 'calendar',
	width: 1500,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Periods',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingPeriodListFind',
					instructions: 'Accounting periods (usually monthly) define the time periods at the end of which accounting data is reconciled to external parties. After a period is closed for a specific client account, no GL activity (book/unbook) can be done for that period.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Period', dataIndex: 'label', width: 200},
						{header: 'Start Date', dataIndex: 'startDate', width: 100, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'End Date', dataIndex: 'endDate', width: 100}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								text: 'Create All Periods For',
								tooltip: 'Create all accounting periods for the entered year',
								iconCls: 'run',
								//scope: toolbar,
								handler: function(field) {
									TCG.data.getDataPromise('accountingPeriodsForYearCreate.json?year=' + TCG.getChildByName(toolbar, 'year').value, this)
										.then(function(status) {
											TCG.createComponent('Clifton.core.StatusWindow', {
												defaultData: {status: status}
											});
											TCG.getParentByClass(toolbar, TCG.grid.GridPanel).reload();
										});

								}
							},
							{xtype: 'spinnerfield', name: 'year', dataType: 'int', width: 55, minValue: (new Date()).getFullYear()}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							const year = TCG.getChildByName(this.getTopToolbar(), 'year');
							if (year) {
								year.setValue((new Date()).getFullYear());
							}
						}
					},
					editor: {
						detailPageClass: 'Clifton.accounting.period.PeriodWindow'
					}
				}]
			},


			{
				title: 'Period Closings',
				items: [{
					name: 'accountingPeriodClosingListFind',
					xtype: 'gridpanel',
					instructions: 'The following client investment accounts have been closed or re-open.',
					importTableName: 'AccountingPeriodClosing',
					useBufferView: false, // allow cell wrapping
					columns: [
						{header: 'Start Date', width: 60, dataIndex: 'accountingPeriod.startDate', filter: {searchFieldName: 'accountingPeriodStartDate'}, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'End Date', width: 60, dataIndex: 'accountingPeriod.endDate', filter: {searchFieldName: 'accountingPeriodEndDate'}},
						{header: 'Client Relationship', width: 150, dataIndex: 'investmentAccount.businessClient.clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'}, hidden: true},
						{header: 'Relationship Category', width: 70, hidden: true, dataIndex: 'investmentAccount.businessClient.clientRelationship.relationshipCategory.name', filter: {searchFieldName: 'clientRelationshipCategoryId', type: 'combo', url: 'businessClientRelationshipCategoryListFind.json'}},
						{header: 'Retail', width: 35, hidden: true, dataIndex: 'investmentAccount.businessClient.clientRelationship.relationshipCategory.retail', type: 'boolean', filter: {searchFieldName: 'clientRelationshipCategoryRetail'}},
						{header: 'Firm Category', width: 80, dataIndex: 'investmentAccount.businessClient.clientRelationship.relationshipType.text', filter: {searchFieldName: 'clientRelationshipTypeName'}, hidden: true},
						{header: 'Firm Sub-Category', width: 80, dataIndex: 'investmentAccount.businessClient.clientRelationship.relationshipSubType.text', filter: {searchFieldName: 'clientRelationshipSubTypeName'}, hidden: true},
						{header: 'Client', width: 150, dataIndex: 'investmentAccount.businessClient.label', filter: {searchFieldName: 'clientName'}, hidden: true},
						{header: 'Client Type', width: 80, dataIndex: 'investmentAccount.businessClient.clientType.text', filter: {searchFieldName: 'clientTypeName'}, hidden: true},
						{header: 'Taxable', width: 50, dataIndex: 'investmentAccount.businessClient.taxable', type: 'boolean', filter: {searchFieldName: 'taxable'}, hidden: true},
						{header: 'Issuing Company', width: 250, dataIndex: 'investmentAccount.issuingCompany.name', filter: {searchFieldName: 'investmentAccountIssuingCompanyId', type: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=true'}, hidden: true},
						{header: 'Client Account', width: 160, dataIndex: 'investmentAccount.label', filter: {searchFieldName: 'investmentAccountNumberOrName'}},
						{header: 'Pooled Vehicle Type', width: 70, dataIndex: 'investmentAccount.clientAccountType', filter: {searchFieldName: 'clientAccountType'}, hidden: true},
						{header: 'GIPS', width: 70, dataIndex: 'investmentAccount.discretionType', filter: {searchFieldName: 'investmentAccountDiscretionType'}, hidden: true},
						{header: 'Service', width: 120, dataIndex: 'investmentAccount.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
						{header: 'Service Vehicle', width: 120, dataIndex: 'investmentAccount.businessService.application.name', filter: {searchFieldName: 'businessServiceApplicationName'}, hidden: true},
						{header: 'Service Processing Type', width: 120, dataIndex: 'investmentAccount.serviceProcessingType.name', filter: {type: 'combo', searchFieldName: 'businessServiceProcessingTypeId', displayField: 'name', url: 'businessServiceProcessingTypeListFind.json'}, hidden: true},
						{header: 'EV Portfolio Code', width: 80, dataIndex: 'investmentAccount.externalPortfolioCode.text', hidden: true, filter: {searchFieldName: 'investmentAccountExternalPortfolioCodeName'}},
						{header: 'EV Product Code', width: 80, dataIndex: 'investmentAccount.externalProductCode.text', hidden: true, filter: {searchFieldName: 'investmentAccountExternalProductCodeName'}},
						{header: 'Channel', width: 80, dataIndex: 'investmentAccount.clientAccountChannel.text', hidden: true, filter: {searchFieldName: 'investmentAccountChannelName'}, renderer: TCG.renderValueWithTooltip},
						{header: 'Team', width: 70, dataIndex: 'investmentAccount.teamSecurityGroup.name', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
						{header: 'Notes', width: 250, dataIndex: 'combinedNote', sortable: false},
						{header: 'Accounting Note', width: 250, dataIndex: 'note', hidden: true},
						{header: 'AUM/Revenue Note', width: 250, dataIndex: 'externalNote', hidden: true},
						{header: 'Preliminary AUM', width: 70, dataIndex: 'preliminaryPeriodEndAUM', type: 'currency', useNull: true, tooltip: 'Preliminary Estimate of Assets Under Management at the end of the period'},
						{header: 'AUM', width: 70, dataIndex: 'periodEndAUM', type: 'currency', useNull: true, tooltip: 'Assets Under Management at the end of the period'},
						{header: 'Projected Revenue (Gross)', width: 70, dataIndex: 'periodProjectedRevenue', type: 'currency', useNull: true, tooltip: 'Projected Revenue for this account for the accounting period (excluding annual minimum)', hidden: true},
						{header: 'Projected Revenue (Annual Min)', width: 70, dataIndex: 'periodProjectedRevenueAnnualMin', type: 'currency', useNull: true, tooltip: 'Annual Minimum portion of the projected revenue for the account and period.', hidden: true},

						{
							header: 'Projected Revenue Total (Gross)', width: 70, dataIndex: 'periodProjectedRevenueTotal', type: 'currency', useNull: true, tooltip: 'Total Projected Revenue for the Account and Accounting Period (Including Annual Minimum portion)',
							renderer: function(v, metaData, r) {
								const annualMin = r.data['periodProjectedRevenueAnnualMin'];
								if (TCG.isNotNull(annualMin) && annualMin !== 0) {
									return TCG.renderAdjustedAmount(v, true, r.data['periodProjectedRevenue'], '0,000.00', 'Annual Min Applied', false);
								}
								return TCG.renderAmount(v, false, '0,000.00');
							}
						},
						{header: 'Projected Broker Fee', width: 70, dataIndex: 'periodProjectedRevenueShareExternal', type: 'currency', useNull: true, tooltip: 'Portion of the projected revenue for the account and period that is paid to external party (Broker).', hidden: true},
						{
							header: 'Projected Revenue (Net)', width: 70, dataIndex: 'periodProjectedRevenueNetTotal', type: 'currency', useNull: true, tooltip: 'Gross Projected Revenue less Broker Fees for this account for the accounting period',
							renderer: function(v, metaData, r) {
								const brokerFee = r.data['periodProjectedRevenueShareExternal'];
								if (TCG.isNotNull(brokerFee) && brokerFee !== 0) {
									return TCG.renderAdjustedAmount(v, true, r.data['periodProjectedRevenueTotal'], '0,000.00', 'Broker Fee', false);
								}
								return TCG.renderAmount(v, false, '0,000.00');
							}
						},
						{header: 'Projected Revenue Share', width: 70, dataIndex: 'periodProjectedRevenueShareInternal', type: 'currency', useNull: true, tooltip: 'Portion of the projected revenue for the account and period that is shared with an internal party (i.e. EV).'},
						{header: 'Closed', width: 50, dataIndex: 'closed', type: 'boolean'},
						{header: '<div class="www" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'If checked, there is at least one APPROVED file associated with this period on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.period.PeriodClosingWindow',
						drillDownOnly: true
					},
					getTopToolbarFilters: function(toolbar) {
						return [{fieldLabel: 'Account Group', width: 180, name: 'investmentAccountGroup', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json'}];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('accountingPeriod.startDate', {'after': new Date().add(Date.DAY, -70)});
						}

						const toolbar = this.getTopToolbar();
						const params = {};
						const accountGroup = TCG.getChildByName(toolbar, 'investmentAccountGroup');
						if (TCG.isNotBlank(accountGroup.getValue())) {
							params.investmentAccountGroupId = accountGroup.getValue();
						}
						return params;
					}
				}]
			},


			{
				title: 'Period AUM',
				items: [{
					name: 'accountingPeriodClosingExtendedListFind',
					xtype: 'gridpanel',
					pageSize: 550, // Want to get all accounts at once
					forceFit: false,
					appendStandardColumns: false,
					instructions: 'The following shows the client investment account AUM (from Period Closings) with various account, client, and client relationships information. Note: If an account has a client relationship, by default will filter to those accounts where the relationship is active on the AUM date.  You can show the <i>Inception Date (Client Relationship) and Terminate Date (Client Relationship)</i> columns to turn those filters off if necessary.',
					viewNames: ['Default View', 'Account View', 'Client View', 'Marketing View'],
					columns: [
						// Client Relationship Information
						{header: 'CR ID', width: 10, hidden: true, dataIndex: 'investmentAccount.businessClient.clientRelationship.id', filter: false, sortable: false, type: 'int'},
						{
							header: 'Client Relationship', width: 175, dataIndex: 'investmentAccount.businessClient.clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'},
							viewNames: ['Default View', 'Marketing View']
						},
						{header: 'Client Relationship Workflow Status', width: 80, dataIndex: 'investmentAccount.businessClient.clientRelationship.workflowStatus.name', filter: {searchFieldName: 'clientRelationshipWorkflowStatusName'}, hidden: true},
						{
							header: 'Inception Date (Client Relationship)', hidden: true, width: 85, dataIndex: 'investmentAccount.businessClient.clientRelationship.inceptionDate', filter: {orNull: true, searchFieldName: 'clientRelationshipInceptionDate'},
							viewNames: ['Marketing View']
						},
						{header: 'Terminate Date (Client Relationship)', hidden: true, width: 85, dataIndex: 'investmentAccount.businessClient.clientRelationship.terminateDate', filter: {orNull: true, searchFieldName: 'clientRelationshipTerminateDate'}},
						{header: 'Relationship Category', width: 70, hidden: true, dataIndex: 'investmentAccount.businessClient.clientRelationship.relationshipCategory.name', filter: {searchFieldName: 'clientRelationshipCategoryId', type: 'combo', url: 'businessClientRelationshipCategoryListFind.json'}},
						{header: 'Retail', width: 50, dataIndex: 'investmentAccount.businessClient.clientRelationship.relationshipCategory.retail', type: 'boolean', filter: {searchFieldName: 'clientRelationshipCategoryRetail'}},
						{
							header: 'Firm Category', width: 100, dataIndex: 'investmentAccount.businessClient.clientRelationship.relationshipType.text', filter: {searchFieldName: 'clientRelationshipTypeName'},
							viewNames: ['Default View', 'Marketing View']
						},
						{
							header: 'Firm Sub-Category', width: 100, dataIndex: 'investmentAccount.businessClient.clientRelationship.relationshipSubType.text', filter: {searchFieldName: 'clientRelationshipSubTypeName'}, hidden: true,
							viewNames: ['Marketing View']
						},

						// Contact Relationships
						{header: 'Relationship Manager', width: 100, dataIndex: 'relationshipManagerContact.nameLabel', hidden: true, filter: {searchFieldName: 'relationshipManagerContactName'}},
						{header: 'Portfolio Manager', width: 100, dataIndex: 'portfolioManagerContact.nameLabel', hidden: true, filter: {searchFieldName: 'portfolioManagerContactName'}},
						{header: 'NBD Client Rep', width: 100, dataIndex: 'newBusinessContact.nameLabel', hidden: true, filter: {searchFieldName: 'newBusinessContactName'}},
						{header: 'Investment Analyst', width: 100, dataIndex: 'investmentAnalystContact.nameLabel', hidden: true, filter: {searchFieldName: 'investmentAnalystContactName'}},

						// Client Information
						{
							header: 'Client', width: 175, dataIndex: 'investmentAccount.businessClient.label', filter: {searchFieldName: 'clientName'}, hidden: true,
							viewNames: ['Client View']
						},
						{header: 'Client Category', width: 85, dataIndex: 'investmentAccount.businessClient.category.name', filter: {searchFieldName: 'clientCategoryId', type: 'combo', url: 'businessClientCategoryListFind.json'}, hidden: true},
						{
							header: 'Client Type', width: 85, dataIndex: 'investmentAccount.businessClient.clientType.text', filter: {searchFieldName: 'clientTypeName'}, hidden: true,
							viewNames: ['Client View']
						},
						{
							header: 'ERISA', width: 50, dataIndex: 'businessClientErisa', type: 'boolean', filter: {searchFieldName: 'businessClientErisa'}, hidden: true,
							tooltip: 'If at least one active contract clause has the client selected as ERISA, then the Client here is considered to be ERISA.  Checks IMA first, then checks the Subscription Agreement (used for Fund Investors)',
							viewNames: ['Client View']
						},
						{
							header: 'Investment Manager Authority', width: 85, dataIndex: 'businessClientAuthority', filter: {searchFieldName: 'businessClientAuthority'}, hidden: true,
							viewNames: ['Client View']
						},
						{
							header: 'Taxable', width: 50, dataIndex: 'investmentAccount.businessClient.taxable', type: 'boolean', filter: {searchFieldName: 'taxable'}, hidden: true,
							viewNames: ['Client View']
						},
						{header: 'Federal Tax #', width: 80, dataIndex: 'investmentAccount.businessClient.federalTaxNumber', filter: {searchFieldName: 'clientFederalTaxNumber'}, hidden: true},

						{header: 'State', width: 80, dataIndex: 'investmentAccount.businessClient.company.state', filter: {searchFieldName: 'clientAddressState'}, hidden: true},

						// Client-Company Relationships Information
						{
							header: 'Consultant', width: 125, dataIndex: 'consultantCompany.name', filter: {searchFieldName: 'consultantCompanyName'}, hidden: true,
							tooltip: 'Consultant is defined by the client\'s company relationships - this displays the parent company name (if exists) so that clients that utilize multiple branches for a consultant display here under the main company only.',
							viewNames: ['Client View']
						},
						{
							header: 'Fiduciary', width: 125, dataIndex: 'fiduciaryCompany.name', filter: {searchFieldName: 'fiduciaryCompanyName'}, hidden: true,
							tooltip: 'Fiduciary is defined by the client\'s company relationships - this displays the parent company name (if exists) so that clients that utilize multiple branches for a fiduciary display here under the main company only.',
							viewNames: ['Client View']
						},
						{
							header: 'Outsourced CIO', width: 125, dataIndex: 'outsourcedCIOCompany.name', filter: {searchFieldName: 'outsourcedCIOCompanyName'}, hidden: true,
							tooltip: 'Outsourced CIO is defined by the client\'s company relationships - this displays the parent company name (if exists) so that clients that utilize multiple branches for this relationship display here under the main company only.',
							viewNames: ['Client View']
						},
						// Account Information
						{header: 'Issuing Company', width: 250, dataIndex: 'investmentAccount.issuingCompany.name', filter: {searchFieldName: 'investmentAccountIssuingCompanyId', type: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=true'}, hidden: true},
						{
							header: 'Account #', width: 75, dataIndex: 'investmentAccount.number', filter: {searchFieldName: 'investmentAccountNumberOrName'},
							viewNames: ['Default View', 'Marketing View', 'Client View', 'Account View']
						},
						{
							header: 'Client Account', width: 225, dataIndex: 'investmentAccount.name', filter: {searchFieldName: 'investmentAccountNumberOrName'},
							viewNames: ['Default View', 'Marketing View', 'Client View', 'Account View']
						},
						{
							header: 'Wrap Account', width: 60, type: 'boolean', dataIndex: 'wrapAccount', filter: {searchFieldName: 'wrapAccount'}, hidden: true
						},
						{
							header: 'Positions On', width: 75, dataIndex: 'investmentAccount.inceptionDate', hidden: true, filter: {searchFieldName: 'investmentAccountInceptionDate'},
							viewNames: ['Account View']
						},
						{
							header: 'Risk Designation', width: 100, hidden: true, dataIndex: 'riskDesignation', tooltip: 'Account Risk Designations can be found on the Client Account window Data Fields tab.  The designation displayed for each row is the account designation that was active as of the last day of the accounting period.', filter: {type: 'combo', valueField: 'text', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Risk Designations'},
							viewNames: ['Account View']
						},
						{
							header: 'Account Workflow State', width: 100, hidden: true, dataIndex: 'investmentAccount.workflowState.name', filter: {searchFieldName: 'investmentAccountWorkflowStateName'},
							viewNames: ['Account View']
						},
						{
							header: 'Base CCY', width: 70, dataIndex: 'investmentAccount.baseCurrency.symbol', filter: {searchFieldName: 'investmentAccountBaseCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true'}, hidden: true,
							viewNames: ['Account View']
						},
						{
							header: 'Team', width: 100, dataIndex: 'investmentAccount.teamSecurityGroup.name', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true,
							viewNames: ['Account View']
						},
						{
							header: 'Pooled Vehicle Type', width: 70, dataIndex: 'investmentAccount.clientAccountType', filter: {searchFieldName: 'clientAccountType'}, hidden: true,
							viewNames: ['Account View']
						},
						{
							header: 'GIPS', width: 70, dataIndex: 'investmentAccount.discretionType', filter: {searchFieldName: 'investmentAccountDiscretionType'}, hidden: true,
							viewNames: ['Account View']
						},
						{header: 'EV Portfolio Code', width: 80, dataIndex: 'investmentAccount.externalPortfolioCode.text', hidden: true, filter: {searchFieldName: 'investmentAccountExternalPortfolioCodeName'}},
						{header: 'EV Product Code', width: 80, dataIndex: 'investmentAccount.externalProductCode.text', hidden: true, filter: {searchFieldName: 'investmentAccountExternalProductCodeName'}},
						{header: 'Channel', width: 100, dataIndex: 'investmentAccount.clientAccountChannel.text', hidden: true, filter: {searchFieldName: 'investmentAccountChannelName'}, renderer: TCG.renderValueWithTooltip},
						{
							header: 'Custodian', width: 150, dataIndex: 'custodianCompany.name', filter: {searchFieldName: 'custodianCompanyName'},
							viewNames: ['Default View', 'Marketing View', 'Client View', 'Account View']
						},

						// Business Service Information
						{
							header: 'Franchise', width: 150, dataIndex: 'investmentAccount.businessService.franchiseName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?serviceLevelType=FRANCHISE', listWidth: 500}, hidden: true,
							tooltip: 'A franchise is the highest level of bucketing investment strategies (services).  The franchise is consistent with how the public website Our Approach Page is set up in bucketing all services into two categories.'
						},
						{
							header: 'Super Strategy', width: 150, dataIndex: 'investmentAccount.businessService.superStrategyName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?serviceLevelType=SUPER_STRATEGY', listWidth: 500}, hidden: true,
							tooltip: 'A super strategy consists of a roll up of similar services bucketed to be consistent with the public website Our Approach Page.'
						},
						{
							header: 'Service', width: 150, dataIndex: 'investmentAccount.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500},
							viewNames: ['Default View', 'Account View']
						},
						{
							header: 'Service Vehicle', width: 100, dataIndex: 'investmentAccount.businessService.application.name', filter: {searchFieldName: 'businessServiceApplicationName'},
							viewNames: ['Default View', 'Account View', 'Marketing View']
						},
						{
							header: 'Service Processing Type', width: 100, dataIndex: 'investmentAccount.serviceProcessingType.name', filter: {type: 'combo', searchFieldName: 'businessServiceProcessingTypeId', displayField: 'name', url: 'businessServiceProcessingTypeListFind.json'}, hidden: true,
							viewNames: ['Account View']
						},

						{header: 'Accounting Note', width: 250, dataIndex: 'note', hidden: true},
						{header: 'AUM/Revenue Note', width: 250, dataIndex: 'externalNote', hidden: true},

						{header: 'Preliminary AUM', width: 100, dataIndex: 'preliminaryPeriodEndAUM', type: 'currency', useNull: true, tooltip: 'Preliminary Estimate of Assets Under Management at the end of the period', summaryType: 'sum'},
						{header: 'Period End AUM', width: 100, dataIndex: 'periodEndAUM', type: 'currency', useNull: true, tooltip: 'Assets Under Management at the end of the period', summaryType: 'sum'},

						{header: 'Projected Revenue (Gross)', width: 70, dataIndex: 'periodProjectedRevenue', type: 'currency', useNull: true, tooltip: 'Projected Revenue for this account for the accounting period (excluding annual minimum)', hidden: true},
						{header: 'Projected Revenue (Annual Min)', width: 70, dataIndex: 'periodProjectedRevenueAnnualMin', type: 'currency', useNull: true, tooltip: 'Annual Minimum portion of the projected revenue for the account and period.', hidden: true},

						{
							header: 'Projected Revenue Total (Gross)', width: 70, dataIndex: 'periodProjectedRevenueTotal', type: 'currency', useNull: true, tooltip: 'Total Projected Revenue for the Account and Accounting Period (Including Annual Minimum portion)',
							renderer: function(v, metaData, r) {
								const annualMin = r.data['periodProjectedRevenueAnnualMin'];
								if (TCG.isNotNull(annualMin) && annualMin !== 0) {
									return TCG.renderAdjustedAmount(v, true, r.data['periodProjectedRevenue'], '0,000.00', 'Annual Min Applied', false);
								}
								return TCG.renderAmount(v, false, '0,000.00');
							}
						},
						{header: 'Projected Broker Fee', width: 70, dataIndex: 'periodProjectedRevenueShareExternal', type: 'currency', useNull: true, tooltip: 'Portion of the projected revenue for the account and period that is paid to external party (Broker).', hidden: true},
						{
							header: 'Projected Revenue (Net)', width: 70, dataIndex: 'periodProjectedRevenueNetTotal', type: 'currency', useNull: true, tooltip: 'Gross Projected Revenue less Broker Fees for this account for the accounting period',
							renderer: function(v, metaData, r) {
								const brokerFee = r.data['periodProjectedRevenueShareExternal'];
								if (TCG.isNotNull(brokerFee) && brokerFee !== 0) {
									return TCG.renderAdjustedAmount(v, true, r.data['periodProjectedRevenueTotal'], '0,000.00', 'Broker Fee', false);
								}
								return TCG.renderAmount(v, false, '0,000.00');
							}
						},
						{header: 'Projected Revenue Share', width: 70, dataIndex: 'periodProjectedRevenueShareInternal', type: 'currency', useNull: true, tooltip: 'Portion of the projected revenue for the account and period that is shared with an internal party (i.e. EV).'}
					],
					plugins: {ptype: 'gridsummary'},
					editor: {
						detailPageClass: 'Clifton.accounting.period.PeriodClosingWindow',
						drillDownOnly: true
					},
					getLoadParams: function(firstLoad) {
						const toolbar = this.getTopToolbar();
						let dateValue;
						const df = TCG.getChildByName(toolbar, 'aumDate');
						if (TCG.isNotBlank(df.getValue())) {
							dateValue = (df.getValue()).format('m/d/Y');
						}
						else {
							const prevME = TCG.getLastDateOfPreviousMonth();
							dateValue = prevME.format('m/d/Y');
							df.setValue(dateValue);
							this.resetClientRelationshipDateFilters();
						}
						const result = {accountingPeriodEndDate: dateValue};
						const serviceComponent = TCG.getChildByName(toolbar, 'serviceComponent');
						if (TCG.isNotNull(serviceComponent.getValue())) {
							result.businessServiceComponentOrParentId = serviceComponent.getValue();
						}
						const accountGroup = TCG.getChildByName(toolbar, 'investmentAccountGroup');
						if (TCG.isNotBlank(accountGroup.getValue())) {
							result.investmentAccountGroupId = accountGroup.getValue();
						}
						const excludeAccountGroup = TCG.getChildByName(toolbar, 'excludeInvestmentAccountGroup');
						if (TCG.isNotBlank(excludeAccountGroup.getValue())) {
							result.excludeInvestmentAccountGroupId = excludeAccountGroup.getValue();
						}
						else if (firstLoad) {
							return TCG.data.getDataPromiseUsingCaching('investmentAccountGroupByName.json?name=Accounts Excluded from PPA-MN AUM Reporting&requestedPropertiesRoot=data&requestedProperties=id|name', this, 'investment.account.group.Accounts Excluded from PPA-MN AUM Reporting')
								.then(function(accountGroup) {
									if (accountGroup) {
										result.excludeInvestmentAccountGroupId = accountGroup.id;
										excludeAccountGroup.setValue({value: accountGroup.id, text: accountGroup.name});
									}
									return result;
								});
						}
						return result;
					},
					getExportFileName: function() {
						let fileName = 'Period AUM';
						const t = this.getTopToolbar();
						const df = TCG.getChildByName(t, 'aumDate');
						if (TCG.isNotBlank(df.getValue())) {
							fileName += ' ' + (df.getValue()).format('Y_m_d');
						}
						return fileName;
					},
					resetClientRelationshipDateFilters: function() {
						const toolbar = this.getTopToolbar();
						const df = TCG.getChildByName(toolbar, 'aumDate');
						if (TCG.isNotBlank(df.getValue())) {
							const dateValue = TCG.parseDate(df.getValue());
							this.setFilterValue('investmentAccount.businessClient.clientRelationship.inceptionDate', {'before': dateValue.getLastDateOfMonth().add(Date.DAY, 1)});
							this.setFilterValue('investmentAccount.businessClient.clientRelationship.terminateDate', {'after': dateValue.getFirstDateOfMonth().add(Date.DAY, -1)});
						}
						else {
							this.clearFilter('investmentAccount.businessClient.clientRelationship.inceptionDate', true);
							this.clearFilter('investmentAccount.businessClient.clientRelationship.terminateDate', true);
						}
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Acct Group', width: 160, minListWidth: 300, name: 'investmentAccountGroup', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
							{fieldLabel: 'Exclude Acct Group', width: 160, minListWidth: 300, name: 'excludeInvestmentAccountGroup', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
							{fieldLabel: 'Service Component', width: 170, minListWidth: 500, name: 'serviceComponent', xtype: 'toolbar-combo', url: 'businessServiceListFind.json?orderBy=name&serviceLevelType=SERVICE_COMPONENT', displayField: 'nameWithParent', listWidth: 350},
							{
								fieldLabel: 'AUM Date', xtype: 'toolbar-datefield', name: 'aumDate',
								listeners: {
									select: function(field) {
										const gp = TCG.getParentByClass(field, TCG.grid.GridPanel);
										gp.resetClientRelationshipDateFilters();
										gp.reload();
									},
									specialkey: function(field, e) {
										if (e.getKey() === e.ENTER) {
											const gp = TCG.getParentByClass(field, TCG.grid.GridPanel);
											gp.resetClientRelationshipDateFilters();
											gp.reload();
										}
									}
								}
							}
						];
					}
				}]
			}
		]
	}]
});
