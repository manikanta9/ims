Clifton.accounting.period.PeriodClosingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Accounting Period Closing',
	iconCls: 'calendar',
	height: 650,
	width: 900,

	gridTabWithCount: 'Portal Files', // show portal file count in  the tab

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',

				tbar: {
					xtype: 'toolbar',

					getParentFormPanel: function() {
						return TCG.getParentTabPanel(this).getWindow().getMainFormPanel();
					},

					initComponent: function() {
						const tb = this;
						this.items = [
							{
								text: 'Close this Period',
								tooltip: 'Lock selected Accounting Period for selected Client Account',
								iconCls: 'lock',
								handler: function() {
									const fp = tb.getParentFormPanel();
									Ext.Msg.confirm('Close Accounting Period', 'Would you like to Close this Accounting Period for selected Client Account?', function(a) {
										if (a === 'yes') {
											TCG.createComponent('Clifton.accounting.period.ClosePeriodWindow', {
												defaultData: fp.getForm().formValues,
												openerCt: fp
											});
										}
									});
								}
							},
							'-',
							{
								text: 'Re-Open this Period',
								tooltip: 'Re-open selected Accounting Period for selected Client Account',
								iconCls: 'unlock',
								handler: function() {
									const fp = tb.getParentFormPanel();
									Ext.Msg.confirm('Re-Open Accounting Period', 'Would you like to Re-Open this Accounting Period for selected Client Account?', function(a) {
										if (a === 'yes') {
											TCG.createComponent('Clifton.accounting.period.ReOpenPeriodWindow', {
												defaultData: fp.getForm().formValues,
												openerCt: fp
											});
										}
									});
								}
							},
							'-',
							{
								text: 'Update AUM/Projected Revenue',
								tooltip: 'Enter or update AUM (Assets Under Management) at the end of this period or Projected Revenue.',
								iconCls: 'pencil',
								handler: function() {
									const fp = tb.getParentFormPanel();
									TCG.createComponent('Clifton.accounting.period.UpdateAUMWindow', {
										defaultData: fp.getForm().formValues,
										openerCt: fp
									});
								}
							},
							'-',
							{
								text: 'Accounting Statement',
								tooltip: 'View Accounting Statement.',
								iconCls: 'pdf',
								xtype: 'splitbutton',
								handler: function() {
									const fp = tb.getParentFormPanel();
									fp.downloadStatement('PDF');
								},
								menu: {
									items: [
										{
											text: 'PDF',
											tooltip: 'View PDF version of the Accounting Statement',
											iconCls: 'pdf',
											handler: function(b) {
												const fp = tb.getParentFormPanel();
												fp.downloadStatement('PDF');
											}
										},

										{
											text: 'Excel',
											tooltip: 'View Excel version of the Accounting Statement',
											iconCls: 'excel',
											handler: function(b) {
												const fp = tb.getParentFormPanel();
												fp.downloadStatement('EXCEL');
											}
										}
									]
								}
							},
							'-',
							{
								text: 'Post',
								tooltip: 'Post Accounting Statement (PDF & Excel).',
								iconCls: 'www',
								xtype: 'splitbutton',
								handler: function() {
									const fp = tb.getParentFormPanel();
									fp.postStatement();
								},
								menu: new Ext.menu.Menu({
									items: [
										{
											text: 'Post PDF & Excel',
											tooltip: 'Post PDF and Excel version of the Accounting Statement',
											iconCls: 'www',
											handler: function(b) {
												const fp = tb.getParentFormPanel();
												fp.postStatement();
											}
										},
										{
											text: 'Post PDF',
											tooltip: 'Post PDF version of the Accounting Statement',
											iconCls: 'pdf',
											handler: function(b) {
												const fp = tb.getParentFormPanel();
												fp.postStatement('PDF');
											}
										},
										{
											text: 'Post Excel',
											tooltip: 'Post Excel version of the Accounting Statement',
											iconCls: 'excel',
											handler: function(b) {
												const fp = tb.getParentFormPanel();
												fp.postStatement('EXCEL');
											}
										},
										{
											text: 'Post Reconciliation Attachments',
											tooltip: 'Post all current active file attachments under Reconciliation Attachments',
											iconCls: 'attach',
											handler: function(b) {
												const fp = tb.getParentFormPanel();
												fp.postDocumentFiles();
											}
										}
									]
								})
							},
							'-'
						];
						Ext.Toolbar.prototype.initComponent.call(this, arguments);
					}
				},

				items: [{
					xtype: 'formpanel',
					url: 'accountingPeriodClosing.json',
					readOnly: true,
					labelWidth: 120,

					downloadStatement: function(format) {
						TCG.downloadFile('accountingPeriodClosingReportDownload.json?periodClosingId=' + this.getWindow().getMainFormId() + '&format=' + format, null, this);
					},

					postStatement: function(format) {
						const id = this.getWindow().getMainFormId();
						const loader = new TCG.data.JsonLoader({
							waitTarget: this.getWindow().getMainFormPanel(),
							waitMsg: 'Posting...',
							params: {periodClosingId: id, format: format}
						});
						loader.load('accountingPeriodClosingReportPost.json');
					},

					postDocumentFiles: function() {
						const id = this.getWindow().getMainFormId();
						const loader = new TCG.data.JsonLoader({
							waitTarget: this.getWindow().getMainFormPanel(),
							waitMsg: 'Posting...',
							params: {periodClosingId: id}
						});
						loader.load('accountingPeriodClosingAttachmentsPost.json');
					},

					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('accountingPeriodClosing.json?id=' + w.getMainFormId(), this), true);
						w.setModified(w.isModified());
						// to load the formgrid
						this.fireEvent('afterload', this);
					},
					items: [
						{fieldLabel: 'Accounting Period', name: 'accountingPeriod.label', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.period.PeriodWindow', detailIdField: 'accountingPeriod.id'},
						{fieldLabel: 'Investment Account', name: 'investmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'investmentAccount.id'},
						{fieldLabel: 'Closed', name: 'closed', xtype: 'checkbox'},
						{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 50},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										bodyStyle: 'padding: 0',
										frame: false,
										labelWidth: 120,
										items: [
											{fieldLabel: 'Preliminary AUM', name: 'preliminaryPeriodEndAUM', xtype: 'currencyfield'},
											{fieldLabel: 'AUM', name: 'periodEndAUM', xtype: 'currencyfield'}
										]
									}]
								},
								{columnWidth: .02, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										bodyStyle: 'padding: 0',
										frame: false,
										labelWidth: 160,
										defaults: {anchor: '0'},
										items: [
											{fieldLabel: 'Projected Revenue', name: 'periodProjectedRevenue', xtype: 'currencyfield'},
											{
												fieldLabel: 'Annual Min Applied', name: 'periodProjectedRevenueAnnualMin', xtype: 'currencyfield',
												qtip: 'Annual Minimum portion of the projected revenue for the period.  This is kept separate so it is easier to track revenue changes with respect to AUM fluctuations.'
											},
											{
												fieldLabel: 'Projected Broker Fee', name: 'periodProjectedRevenueShareExternal', xtype: 'currencyfield',
												qtip: 'Projected External Revenue Share that is paid to an external party (i.e. Brokers)'
											},
											{
												fieldLabel: 'Projected Revenue Share', name: 'periodProjectedRevenueShareInternal', xtype: 'currencyfield',
												qtip: 'Projected Internal Revenue Share that is allocated to an internal party (i.e. EV).'
											}
										]
									}]
								}
							]
						},
						{
							fieldLabel: 'AUM/Revenue Note', name: 'externalNote', xtype: 'textarea', height: 50,
							qtip: 'External note that applies to AUM/Projected Revenue.'
						},
						{
							title: 'Reconciliation Attachments',
							xtype: 'documentFileGrid-simple',
							definitionName: 'AccountingPeriodClosingReconciliationAttachments'
						}
					]
				}]
			},


			{
				title: 'Portal Files',
				reloadOnTabChange: true,
				items: [
					{
						xtype: 'portal-files-grid-for-source-entity',
						sourceTableName: 'AccountingPeriodClosing'
					}
				]
			}
		]
	}]
});


Clifton.accounting.period.ClosePeriodWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Close Accounting Period',
	iconCls: 'lock',
	height: 275,
	width: 550,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Lock selected accounting period for selected investment account (cannot post/unpost).',
		labelWidth: 120,
		items: [
			{fieldLabel: 'Accounting Period', name: 'accountingPeriod.label', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.period.PeriodWindow', detailIdField: 'accountingPeriod.id'},
			{fieldLabel: 'Investment Account', name: 'investmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'investmentAccount.id'},
			{fieldLabel: 'Note', name: 'note', xtype: 'textarea'}
		],
		getSaveURL: function() {
			return 'accountingPeriodClose.json';
		},
		getSubmitParams: function() {
			return {
				accountingPeriodId: this.getFormValue('accountingPeriod.id'),
				investmentAccountId: this.getFormValue('investmentAccount.id')
			};
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Closed selected accounting period.', 'Closing Status');
			}
		}
	}]
});


Clifton.accounting.period.ReOpenPeriodWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Re-Open Accounting Period',
	iconCls: 'unlock',
	height: 275,
	width: 550,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Unlock selected accounting period for selected investment account (allow post/unpost).',
		labelWidth: 120,
		items: [
			{fieldLabel: 'Accounting Period', name: 'accountingPeriod.label', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.period.PeriodWindow', detailIdField: 'accountingPeriod.id'},
			{fieldLabel: 'Investment Account', name: 'investmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'investmentAccount.id'},
			{fieldLabel: 'Note', name: 'note', xtype: 'textarea', allowBlank: false}
		],
		getSaveURL: function() {
			return 'accountingPeriodOpen.json';
		},
		getSubmitParams: function() {
			return {
				accountingPeriodId: this.getFormValue('accountingPeriod.id'),
				investmentAccountId: this.getFormValue('investmentAccount.id')
			};
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Opened selected accounting period.', 'Opening Status');
			}
		}
	}]
});


Clifton.accounting.period.UpdateAUMWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Update AUM/Projected Revenue',
	iconCls: 'calendar',
	height: 400,
	width: 700,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Enter or modify Assets Under Management (AUM) at the end of selected accounting period or Projected Revenue for the period',
		labelWidth: 120,
		items: [
			{name: 'id', xtype: 'hidden'},
			{fieldLabel: 'Accounting Period', name: 'accountingPeriod.label', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.period.PeriodWindow', detailIdField: 'accountingPeriod.id'},
			{fieldLabel: 'Investment Account', name: 'investmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'investmentAccount.id'},
			{
				xtype: 'panel',
				layout: 'column',
				items: [
					{
						columnWidth: .49,
						items: [{
							xtype: 'formfragment',
							bodyStyle: 'padding: 0',
							frame: false,
							labelWidth: 120,
							items: [
								{fieldLabel: 'Preliminary AUM', name: 'preliminaryPeriodEndAUM', xtype: 'currencyfield'},
								{fieldLabel: 'AUM', name: 'periodEndAUM', xtype: 'currencyfield'}
							]
						}]
					},
					{columnWidth: .02, items: [{xtype: 'label', html: '&nbsp;'}]},
					{
						columnWidth: .49,
						items: [{
							xtype: 'formfragment',
							bodyStyle: 'padding: 0',
							frame: false,
							labelWidth: 160,
							defaults: {anchor: '0'},
							items: [
								{fieldLabel: 'Projected Revenue', name: 'periodProjectedRevenue', xtype: 'currencyfield'},
								{
									fieldLabel: 'Annual Min Applied', name: 'periodProjectedRevenueAnnualMin', xtype: 'currencyfield',
									qtip: 'Annual Minimum portion of the projected revenue for the period.  This is kept separate so it is easier to track revenue changes with respect to AUM fluctuations.'
								},
								{
									fieldLabel: 'Projected Broker Fee', name: 'periodProjectedRevenueShareExternal', xtype: 'currencyfield',
									qtip: 'Projected External Revenue Share that is paid to an external party (i.e. Brokers)'
								},
								{
									fieldLabel: 'Projected Revenue Share', name: 'periodProjectedRevenueShareInternal', xtype: 'currencyfield',
									qtip: 'Projected Internal Revenue Share that is allocated to an internal party (i.e. EV).'
								}
							]
						}]
					}
				]
			},
			{
				fieldLabel: 'AUM/Revenue Note', name: 'externalNote', xtype: 'textarea', height: 50,
				qtip: 'External note that applies to AUM/Projected Revenue.'
			}
		],
		getSaveURL: function() {
			return 'accountingPeriodClosingSave.json';
		},
		getSubmitParams: function() {
			return {
				accountingPeriodId: this.getFormValue('id')
			};
		}
	}]
});
