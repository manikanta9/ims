Clifton.accounting.simplejournal.SimpleJournalListWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingSimpleJournalListWindow',
	title: 'Simple Journals',
	iconCls: 'cash',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Pending Journals',
				reloadOnTabChange: true,
				items: [{
					xtype: 'gridpanel',
					name: 'accountingSimpleJournalListFind',
					importTableName: 'AccountingSimpleJournal',
					importComponentName: 'Clifton.accounting.simplejournal.upload.SimpleJournalUploadWindow',
					instructions: 'Simple journals are used to simplify data entry for "simple" accounting transactions. Each simple journal always generates 2 general ledger entries: main row and offsetting row. The sum of debits and credits across both rows is always 0.<br/><br/>Pending journals are journals that have not been booked yet.',
					rowSelectionModel: 'checkbox',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30},
						{header: 'Type', dataIndex: 'journalType.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalTypeId', url: 'accountingSimpleJournalTypeListFind.json'}},
						{header: 'Description', dataIndex: 'description', width: 150, hidden: true},
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Main GL Account', dataIndex: 'mainAccountingAccount.label', width: 80, filter: {type: 'combo', searchFieldName: 'mainAccountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}},
						{header: 'Offsetting GL Account', dataIndex: 'offsettingAccountingAccount.label', width: 80, filter: {type: 'combo', searchFieldName: 'offsettingAccountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}},
						{header: 'Transaction', dataIndex: 'transactionDate', width: 50, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Original Transaction', dataIndex: 'originalTransactionDate', width: 50, hidden: true},
						{header: 'Settlement', dataIndex: 'settlementDate', width: 50, hidden: true},
						{header: 'Amount', dataIndex: 'amount', width: 50, type: 'currency'},
						{header: 'FX Rate', dataIndex: 'exchangeRateToBase', width: 50, type: 'currency', numberFormat: '0,000.000000000000'}
					],
					getLoadParams: function() {
						const params = {};
						params.journalNotBooked = true;
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.category2Name = 'Accounting Simple Journal Type Tags';
							params.category2TableName = 'AccountingSimpleJournalType';
							params.category2LinkFieldPath = 'journalType';
							params.category2HierarchyId = tag.getValue();
						}
						else if (TCG.isNotNull(this.getWindow().category2Name)) {
							//TODO better way to do this to set the onload, but also the actual toolbar filter?
							params.category2Name = this.getWindow().category2Name;
							params.category2TableName = this.getWindow().category2TableName;
							params.category2LinkFieldPath = this.getWindow().category2LinkFieldPath;
							params.category2HierarchyName = this.getWindow().category2HierarchyName;
						}
						return params;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Journal Type', width: 180, xtype: 'toolbar-combo', url: 'accountingSimpleJournalTypeListFind.json', linkedFilter: 'journalType.name'},
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Accounting Simple Journal Type Tags'}
						];
					},
					editor: {
						allowToDeleteMultiple: true,
						detailPageClass: 'Clifton.accounting.simplejournal.SimpleJournalWindow',
						getDefaultData: function(gridPanel) {
							return this.savedDefaultData;
						},
						addToolbarAddButton: function(toolBar, gridPanel) {
							const editor = this;
							toolBar.add({
								text: 'Book',
								tooltip: 'Book this simple journal',
								iconCls: 'book-open-blue',
								handler: function() {
									const sm = gridPanel.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a journal to be booked.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Book Simple Journal', 'Would you like to book the selected simple journal(s)?', function(a) {
											if (a === 'yes') {
												editor.bookSelection(sm, gridPanel, false);
											}
										});
									}
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Book and Post',
								tooltip: 'Book selected simple journal and immediately post it to the General Ledger',
								iconCls: 'book-red',
								handler: function() {
									const sm = gridPanel.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a journal to be posted.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Book & Post Simple Journal', 'Would you like to book and post the selected simple journal(s)?', function(a) {
											if (a === 'yes') {
												editor.bookSelection(sm, gridPanel, true);
											}
										});
									}
								}
							});
							toolBar.add('-');
							toolBar.add(new TCG.form.ComboBox({name: 'journalType', url: 'accountingSimpleJournalTypeListFind.json?systemGenerated=false', limitRequestedProperties: false, width: 150}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Create a new simple journal of selected type',
								iconCls: 'add',
								scope: this,
								handler: function() {
									let journalType = TCG.getChildByName(toolBar, 'journalType');
									const journalTypeId = journalType.getValue();
									if (TCG.isBlank(journalTypeId)) {
										TCG.showError('You must first select desired journal type from the list.');
									}
									else {
										journalType = journalType.store.getById(journalTypeId);
										this.savedDefaultData = {
											journalType: journalType.json
										};
										this.openDetailPage(this.getDetailPageClass(), gridPanel);
									}
								}
							});
							toolBar.add('-');
						},
						bookSelection: function(selectionModel, gridPanel, postJournal) {
							const editor = this;
							const grid = editor.grid;
							const selection = selectionModel.getSelected();
							const loader = new TCG.data.JsonLoader({
								waitTarget: grid.ownerCt,
								waitMsg: 'Booking...',
								params: editor.getBookParams(selectionModel, postJournal),
								timeout: 180000,
								onLoad: function(record, conf) {
									if (!selectionModel.hasSelection()) {
										gridPanel.reload();
									}
									else {
										grid.getStore().remove(selection);
										grid.ownerCt.updateCount();
									}

									if (selectionModel.hasSelection()) {
										editor.bookSelection(selectionModel, gridPanel, postJournal);
									}
								}
							});
							loader.load('accountingJournalBook.json?enableOpenSessionInView=true');
						},
						getBookParams: function(selectionModel, postJournal) {
							return {
								journalTypeName: 'Simple Journal',
								sourceEntityId: selectionModel.getSelected().id,
								postJournal: postJournal,
								autoAdjust: false
							};
						}
					}
				}]
			},


			{
				title: 'Booked Journals',
				reloadOnTabChange: true,
				items: [{
					xtype: 'gridpanel',
					name: 'accountingSimpleJournalListFind',
					instructions: 'Simple journals are used to simplify data entry for "simple" accounting transactions. Each simple journal always generates 2 general ledger entries: main row and offsetting row. The sum of debits and credits across both rows is always 0.<br/><br/>Booked simple journals created actual journals that get posted to the General Ledger and can no longer be modified.',
					rowSelectionModel: 'checkbox',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30},
						{header: 'Type', dataIndex: 'journalType.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalTypeId', url: 'accountingSimpleJournalTypeListFind.json'}},
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Description', dataIndex: 'description', width: 150, hidden: true},
						{header: 'Main GL Account', dataIndex: 'mainAccountingAccount.label', width: 80, filter: {type: 'combo', searchFieldName: 'mainAccountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}},
						{header: 'Offsetting GL Account', dataIndex: 'offsettingAccountingAccount.label', width: 80, filter: {type: 'combo', searchFieldName: 'offsettingAccountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}},
						{header: 'Transaction', dataIndex: 'transactionDate', width: 50, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Original Transaction', dataIndex: 'originalTransactionDate', width: 50, hidden: true},
						{header: 'Settlement', dataIndex: 'settlementDate', width: 50, hidden: true},
						{header: 'Booked', dataIndex: 'bookingDate', width: 55},
						{header: 'Amount', dataIndex: 'amount', width: 45, type: 'currency'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -30)});
						}
						const params = {};
						params.journalBooked = true;
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.category2Name = 'Accounting Simple Journal Type Tags';
							params.category2TableName = 'AccountingSimpleJournalType';
							params.category2LinkFieldPath = 'journalType';
							params.category2HierarchyId = tag.getValue();
						}
						return params;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Journal Type', width: 180, xtype: 'combo', url: 'accountingSimpleJournalTypeListFind.json', linkedFilter: 'journalType.name'},
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Accounting Simple Journal Type Tags'}
						];
					},
					editor: {
						detailPageClass: 'Clifton.accounting.simplejournal.SimpleJournalWindow',
						getDefaultData: function(gridPanel) {
							return this.savedDefaultData;
						},
						addToolbarAddButton: function(toolBar, gridPanel) {
							const editor = this;
							toolBar.add({
								text: 'Unpost Journal',
								tooltip: 'Remove this journal from GL, delete it, and mark corresponding source entity as un-booked.',
								iconCls: 'undo',
								handler: function() {
									const sm = gridPanel.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a journal to be unbooked.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Unpost Simple Journal', 'Would you like to unpost the selected simple journal(s)?', function(a) {
											if (a === 'yes') {
												editor.unpostSelection(sm, gridPanel);
											}
										});
									}
								}
							});
							toolBar.add('-');
						},
						unpostSelection: function(selectionModel, gridPanel) {
							const editor = this;
							const grid = editor.grid;
							const selection = selectionModel.getSelected();
							const loader = new TCG.data.JsonLoader({
								waitTarget: grid.ownerCt,
								waitMsg: 'Unposting...',
								params: editor.getUnpostParams(selectionModel),
								onLoad: function(record, conf) {
									if (!selectionModel.hasSelection()) {
										gridPanel.reload();
									}
									else {
										grid.getStore().remove(selection);
										grid.ownerCt.updateCount();
									}

									if (selectionModel.hasSelection()) {
										editor.unpostSelection(selectionModel, gridPanel);
									}
								}
							});
							loader.load('accountingJournalForSourceEntityUnpost.json');
						},
						getUnpostParams: function(selectionModel) {
							return {
								sourceTable: 'AccountingSimpleJournal',
								sourceEntityId: selectionModel.getSelected().id
							};
						}
					}
				}]
			},


			{
				title: 'Journal Types',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingSimpleJournalTypeListFind',
					instructions: 'Simple journal types define variations in simple journals and rules that apply to them. Each simple journal always generates 2 general ledger entries: main row and offsetting row. The sum of debits and credits across both rows is always 0.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Type Name', dataIndex: 'name', width: 80},
						{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
						{header: 'Default Journal Description', dataIndex: 'defaultJournalDescription', width: 200, hidden: true},
						{header: 'Main GL Account', dataIndex: 'mainAccountingAccount.label', width: 80},
						{header: 'Main Currency GL Account', dataIndex: 'mainCurrencyAccountingAccount.label', width: 80, hidden: true},
						{header: 'Main Security Base Currency', dataIndex: 'mainSecurityBaseCurrency', width: 40, type: 'boolean', hidden: true},
						{header: 'Offsetting GL Account', dataIndex: 'offsettingAccountingAccount.label', width: 80},
						{header: 'Offsetting Currency GL Account', dataIndex: 'offsettingCurrencyAccountingAccount.label', width: 80, hidden: true},
						{header: 'Offsetting Security Base Currency', dataIndex: 'offsettingSecurityBaseCurrency', width: 40, type: 'boolean', hidden: true},
						{header: 'Same Holding Account', dataIndex: 'sameHoldingAccount', width: 35, type: 'boolean'},
						{header: 'Positive Amount', dataIndex: 'positiveAmountOnly', width: 30, type: 'boolean'},
						{header: 'Credit Main GL Account', dataIndex: 'creditMainAccountingAccount', width: 35, type: 'boolean'},
						{header: 'Same Security', dataIndex: 'sameSecurity', width: 35, type: 'boolean', hidden: true},
						{header: 'Require Description', dataIndex: 'descriptionRequired', width: 35, type: 'boolean'},
						{header: 'System Generated', dataIndex: 'systemGenerated', width: 35, type: 'boolean'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 180, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Accounting Simple Journal Type Tags'},
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							return {
								category2Name: 'Accounting Simple Journal Type Tags',
								category2TableName: 'AccountingSimpleJournalType',
								category2HierarchyId: tag.getValue()
							};
						}
					},
					editor: {
						detailPageClass: 'Clifton.accounting.simplejournal.SimpleJournalTypeWindow'
					}
				}]
			}
		]
	}]
});
