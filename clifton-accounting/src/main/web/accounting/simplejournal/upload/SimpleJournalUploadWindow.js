Clifton.accounting.simplejournal.upload.SimpleJournalUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'accountingSimpleJournalUploadWindow',
	title: 'Accounting Simple Journal Import',
	iconCls: 'import',
	height: 600,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			Clifton.system.downloadSampleUploadFile('AccountingSimpleJournal', false, this);
		}
	}, '-', {
		text: 'Sample File (All Data)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File (Contains All (up to 500 rows) Existing Data)',
		handler: function() {
			Clifton.system.downloadSampleUploadFile('AccountingSimpleJournal', true, this);
		}
	}, '-', {
		text: 'Sample File (Simple Upload)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File for Simple Market Data Value Uploads',
		handler: function() {
			TCG.openFile('accounting/simplejournal/upload/AccountingSimpleJournalUploadFileSample.xls');
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Accounting Simple Journal Uploads allow you to import simple journals from files directly into the system.  Use the options below to customize how to upload your data.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},

			{
				xtype: 'fieldset-checkbox',
				title: 'Simple Accounting Simple Journal Uploads',
				checkboxName: 'simple',
				instructions: 'Simple Accounting Simple Journal Uploads allow setting less columns in the upload file and the system will make certain assumptions in order to correctly lookup data based on less information. Client Account Relationship Purpose can be used to lookup related main clifton accounts when multiple accounts apply.  If fields are specified in the file for a row, then the data for that row will not be overwritten with this default data.',
				items: [
					// Entity Modify Fields
					{fieldLabel: 'Transaction Date', name: 'transactionDate', xtype: 'datefield'},
					{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'},
					{fieldLabel: 'Client Account Purpose', name: 'purpose.name', hiddenName: 'purpose.id', xtype: 'combo', url: 'investmentAccountRelationshipPurposeListFind.json'},
					{boxLabel: 'Immediately Book and Post Simple Journals to the General Ledger (Only performed if all rows in the file are created successfully)', name: 'bookAndPost', xtype: 'checkbox', checked: true}
				]
			},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'accountingSimpleJournalUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		}

	}]
});
