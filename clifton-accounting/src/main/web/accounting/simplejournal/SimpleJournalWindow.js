Clifton.accounting.simplejournal.SimpleJournalWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Simple Journal',
	iconCls: 'cash',
	width: 750,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Journal',
				tbar: {xtype: 'accounting-booking-toolbar', journalTypeName: 'Simple Journal'},

				items: [{
					xtype: 'formpanel',
					url: 'accountingSimpleJournal.json',
					labelFieldName: 'id',
					labelWidth: 120,
					items: [
						{fieldLabel: 'Journal Type', name: 'journalType.name', xtype: 'linkfield', detailIdField: 'journalType.id', detailPageClass: 'Clifton.accounting.simplejournal.SimpleJournalTypeWindow'},
						{name: 'journalType.description', xtype: 'displayfield'},
						{
							fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', limitRequestedProperties: false, displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow',
							listeners: {
								beforeselect: function(combo, record) {
									const fp = combo.getParentForm();
									const f = fp.getForm();
									const jt = f.formValues.journalType;
									const data = {journalType: jt, clientInvestmentAccount: record.json};

									if (TCG.isTrue(jt.mainSecurityBaseCurrency) || TCG.isNull(fp.getFormValue('mainSecurityBaseCurrency.id', true, true))) {
										data.mainInvestmentSecurity = record.json.baseCurrency;
									}
									if (TCG.isTrue(jt.offsettingSecurityBaseCurrency) || TCG.isNull(fp.getFormValue('offsettingInvestmentSecurity.id', true, true))) {
										data.offsettingInvestmentSecurity = record.json.baseCurrency;
									}
									f.setValues(data);
								}
							}
						},
						{
							fieldLabel: 'Local Amount', xtype: 'panel', layout: 'hbox', // NOTE: work around composite field bugs
							items: [
								{name: 'amount', xtype: 'currencyfield', flex: 1},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FX Rate:', xtype: 'displayfield', width: 145, name: 'FxLabel'},
								{name: 'exchangeRateToBase', xtype: 'floatfield', flex: 1}
							]
						},
						{
							fieldLabel: 'Transaction Date', xtype: 'container', layout: 'hbox',
							items: [
								{
									name: 'transactionDate', xtype: 'datefield', flex: 1,
									listeners: {
										change: function(field, newValue) {
											const f = TCG.getParentByClass(field, Ext.Panel).getForm();
											f.findField('originalTransactionDate').setValue(field.getRawValue());
											f.findField('settlementDate').setValue(field.getRawValue());
										}
									}
								},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Booking Date:', xtype: 'displayfield', width: 145},
								{xtype: 'accounting-bookingdate', sourceTable: 'AccountingSimpleJournal', flex: 1}
							]
						},
						{
							fieldLabel: 'Settlement Date', xtype: 'container', layout: 'hbox',
							items: [
								{name: 'settlementDate', xtype: 'datefield', flex: 1},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Original Tran Date:', xtype: 'displayfield', width: 145},
								{name: 'originalTransactionDate', xtype: 'datefield', flex: 1}
							]
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 55},
						{fieldLabel: 'Private Note', name: 'privateNote', xtype: 'textarea', height: 55},
						{
							xtype: 'fieldset',
							title: 'Main Entry',
							anchor: '0',
							labelWidth: 110,
							items: [
								{
									fieldLabel: 'Holding Account', name: 'mainHoldingInvestmentAccount.label', hiddenName: 'mainHoldingInvestmentAccount.id', displayField: 'label', xtype: 'combo', limitRequestedProperties: false, url: 'investmentAccountListFind.json?ourAccount=false', loadAll: true, detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['clientInvestmentAccount.label', 'transactionDate'],
									listeners: {
										beforequery: function(queryEvent) {
											queryEvent.combo.store.baseParams = {
												mainAccountId: queryEvent.combo.getParentForm().getForm().findField('clientInvestmentAccount.id').value,
												mainPurposeActiveOnDate: queryEvent.combo.getParentForm().getForm().findField('transactionDate').value
											};
										},
										beforeselect: function(combo, record) {
											// when main and offsetting holding accounts are the same, default it
											const f = combo.getParentForm().getForm();
											const fp = combo.getParentForm();
											const values = {
												journalType: f.formValues.journalType,
												clientInvestmentAccount: f.formValues.clientInvestmentAccount,
												mainHoldingInvestmentAccount: record.json
											};
											if (TCG.isTrue(values.journalType.sameHoldingAccount)) {
												values.offsettingHoldingInvestmentAccount = record.json;
											}
											f.setValues(values);
											fp.updateExchangeRate.call(fp);
										}
									}
								},
								{fieldLabel: 'GL Account', name: 'mainAccountingAccount.label', hiddenName: 'mainAccountingAccount.id', displayField: 'label', xtype: 'combo', url: 'accountingAccountListFind.json'},
								{
									fieldLabel: 'Security', name: 'mainInvestmentSecurity.label', hiddenName: 'mainInvestmentSecurity.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json', limitRequestedProperties: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
									listeners: {
										beforeselect: function(combo, record) {
											// when main and offsetting holding accounts are the same, default it
											const f = combo.getParentForm().getForm();
											const fp = combo.getParentForm();
											const jt = f.formValues.journalType;
											const clientAccount = f.formValues.clientInvestmentAccount;
											const mainHoldingAccount = f.formValues.mainHoldingInvestmentAccount;
											if (TCG.isTrue(jt.sameSecurity)) {
												f.setValues({
													journalType: jt,
													clientInvestmentAccount: clientAccount,
													offsettingInvestmentSecurity: record.json,
													mainHoldingInvestmentAccount: mainHoldingAccount
												});
											}
											const denomCCY = record.json.instrument.tradingCurrency ? record.json.instrument.tradingCurrency.id : record.json.id;
											if (clientAccount && clientAccount.baseCurrency.id !== denomCCY) {
												f.setValues({
													journalType: jt,
													mainAccountingAccount: jt.mainCurrencyAccountingAccount,
													offsettingAccountingAccount: jt.offsettingCurrencyAccountingAccount,
													clientInvestmentAccount: clientAccount,
													mainHoldingInvestmentAccount: mainHoldingAccount
												});
												fp.updateExchangeRate.call(fp, denomCCY);
											}
											else {
												f.setValues({
													journalType: jt,
													clientInvestmentAccount: clientAccount,
													mainAccountingAccount: jt.mainAccountingAccount,
													offsettingAccountingAccount: jt.offsettingAccountingAccount,
													mainHoldingInvestmentAccount: mainHoldingAccount
												});
											}
										}
									}
								}
							]
						},
						{
							xtype: 'fieldset',
							title: 'Offsetting Entry',
							anchor: '0',
							labelWidth: 110,
							items: [
								{
									fieldLabel: 'Holding Account', name: 'offsettingHoldingInvestmentAccount.label', hiddenName: 'offsettingHoldingInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=false', loadAll: true, detailPageClass: 'Clifton.investment.account.AccountWindow', requiredFields: ['clientInvestmentAccount.label', 'transactionDate'],
									beforequery: function(queryEvent) {
										queryEvent.combo.store.baseParams = {
											mainAccountId: queryEvent.combo.getParentForm().getForm().findField('clientInvestmentAccount.id').value,
											mainPurposeActiveOnDate: queryEvent.combo.getParentForm().getForm().findField('transactionDate').value
										};
									}
								},
								{fieldLabel: 'GL Account', name: 'offsettingAccountingAccount.label', hiddenName: 'offsettingAccountingAccount.id', displayField: 'label', xtype: 'combo', url: 'accountingAccountListFind.json'},
								{fieldLabel: 'Security', name: 'offsettingInvestmentSecurity.label', hiddenName: 'offsettingInvestmentSecurity.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}
							]
						}
					],

					listeners: {
						afterload: function() {
							this.initFormField();
						}
					},
					getWarningMessage: function(form) {
						let msg = undefined;
						if (form.formValues.bookingDate) {
							msg = 'This simple journal was posted on ' + TCG.renderDate(form.formValues.bookingDate) + ' and can no longer be modified.';
						}
						return msg;
					},
					updateExchangeRate: function(securityId) {
						const f = this.getForm();
						let holdingAccountIssuingCompanyId = null;
						if (!securityId) {
							securityId = f.findField('mainInvestmentSecurity.id').value;
						}
						const settlementDate = f.findField('settlementDate').value;
						if (f.formValues.mainHoldingInvestmentAccount) {
							holdingAccountIssuingCompanyId = f.formValues.mainHoldingInvestmentAccount.issuingCompany.id;
						}
						const clientAccount = f.formValues.clientInvestmentAccount;
						if (securityId && settlementDate && holdingAccountIssuingCompanyId && clientAccount) {
							let fx = 1;
							if (TCG.isNotEquals(securityId, clientAccount.baseCurrency.id)) {
								const fxRate = TCG.data.getData('marketDataExchangeRateForDateFlexible.json?companyId=' + holdingAccountIssuingCompanyId + '&fromCurrencyId=' + securityId + '&toCurrencyId=' + clientAccount.baseCurrency.id + '&rateDate=' + settlementDate, this);
								fx = fxRate ? fxRate.exchangeRate : '';
							}
							f.findField('exchangeRateToBase').setValue(fx);
						}
					},
					prepareDefaultData: function(defaultData) {
						// populate defaults
						const jt = defaultData.journalType;
						defaultData.mainAccountingAccount = jt.mainAccountingAccount;
						defaultData.offsettingAccountingAccount = jt.offsettingAccountingAccount;
						defaultData.description = jt.defaultJournalDescription || jt.name;
						defaultData.exchangeRateToBase = 1;
						this.initFormField(jt);
						return defaultData;
					},
					initFormField: function(jt) {
						const f = this.getForm();
						const fv = f.formValues;

						// disable fields that are read-only
						if (!jt && fv) {
							jt = fv.journalType;
						}
						if (jt) {
							if (jt.mainAccountingAccount) {
								f.findField('mainAccountingAccount.label').disable();
							}
							if (TCG.isTrue(jt.mainSecurityBaseCurrency)) {
								f.findField('mainInvestmentSecurity.label').disable();
							}
							if (jt.offsettingAccountingAccount) {
								f.findField('offsettingAccountingAccount.label').disable();
							}
							if (TCG.isTrue(jt.offsettingSecurityBaseCurrency)) {
								f.findField('offsettingInvestmentSecurity.label').disable();
							}
							const fs = this.findByType('fieldset');
							fs[0].setTitle(jt.mainLabel);
							fs[1].setTitle(jt.offsettingLabel);
						}
					}
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'AccountingSimpleJournal'
				}]
			}
		]
	}]
});
