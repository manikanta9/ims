Clifton.accounting.eventjournal.EventJournalWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Event Journal',
	iconCls: 'event',
	width: 1200,
	height: 500,

	items: [{
		xtype: 'formpanel',
		url: 'accountingEventJournal.json?requestedMaxDepth=5',
		getSaveURL: function() {
			return 'accountingEventJournalAdjust.json?requestedPropertiesRoot=data&requestedProperties=id';
		},
		labelFieldName: 'label',
		confirmBeforeSaveMsgTitle: 'Modify Event Journal',
		confirmBeforeSaveMsg: 'Are you sure you want to update this event journal? If the journal is booked, unbook it, save updated fields, and rebook the journal.',
		items: [
			{fieldLabel: 'Journal Type', name: 'journalType.name', xtype: 'linkfield', detailIdField: 'journalType.id', detailPageClass: 'Clifton.accounting.eventjournal.EventJournalTypeWindow'},
			{fieldLabel: 'Security Event', name: 'securityEvent.label', xtype: 'linkfield', detailIdField: 'securityEvent.id', detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow'},
			{
				xtype: 'columnpanel',
				columns: [
					{
						rows: [
							{xtype: 'accounting-bookingdate', sourceTable: 'AccountingEventJournal'}
						],
						config: {columnWidth: 0.7}
					},
					{
						rows: [
							{fieldLabel: 'Accrual Reversal Booking Date', xtype: 'accounting-bookingdate', sourceTable: 'AccountingEventJournal', name: 'accrualReversalBookingDate', journalSequence: 2}
						],
						config: {columnWidth: 0.3, labelWidth: 180}
					}
				]
			},
			{fieldLabel: 'Note', name: 'description', xtype: 'textarea', height: 40},
			{
				xtype: 'formgrid-scroll',
				storeRoot: 'detailList',
				readOnly: true,
				height: 290,
				heightResized: true,
				detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
				dtoClass: 'com.clifton.accounting.eventjournal.AccountingEventJournalDetail',
				getDetailPageId: function(grid, row) {
					return row.json.accountingTransaction.id;
				},
				columnsConfig: [
					{header: 'ID', dataIndex: 'id', width: 70, type: 'int', hidden: true},
					{header: 'PID', dataIndex: 'accountingTransaction.id', width: 70, type: 'int', doNotFormat: true},
					{header: 'Election #', dataIndex: 'eventPayout.electionNumber', width: 70, type: 'int', useNull: true, hidden: true},
					{header: 'Payout #', dataIndex: 'eventPayout.payoutNumber', width: 70, type: 'int', useNull: true, hidden: true},
					{header: 'Client Account', dataIndex: 'accountingTransaction.clientInvestmentAccount.label', width: 250, defaultSortColumn: true},
					{header: 'Holding Account', dataIndex: 'accountingTransaction.holdingInvestmentAccount.label', width: 250},
					{header: 'GL Account', dataIndex: 'accountingTransaction.accountingAccount.label', width: 150, hidden: true},
					{header: 'Date Open', dataIndex: 'accountingTransaction.originalTransactionDate', width: 80},
					{header: 'Quantity', dataIndex: 'affectedQuantity', type: 'float', width: 90, summaryType: 'sum', editor: {xtype: 'floatfield'}, editable: false},
					{header: 'Cost', dataIndex: 'affectedCost', type: 'float', useNull: true, width: 95, summaryType: 'sum', editor: {xtype: 'floatfield'}, editable: false},
					{header: 'Price', dataIndex: 'transactionPrice', type: 'float', useNull: true, width: 90, editor: {xtype: 'floatfield'}, editable: false},
					{header: 'Amount', dataIndex: 'transactionAmount', type: 'float', useNull: true, width: 110, summaryType: 'sum', editor: {xtype: 'floatfield'}, editable: false},
					{header: 'Additional Amount', dataIndex: 'additionalAmount', type: 'float', useNull: true, width: 110, summaryType: 'sum', editor: {xtype: 'floatfield'}, editable: false},
					{header: 'Additional Amount 2', dataIndex: 'additionalAmount2', type: 'float', useNull: true, width: 110, summaryType: 'sum', editor: {xtype: 'floatfield'}, editable: false},
					{header: 'Additional Amount 3', dataIndex: 'additionalAmount3', type: 'float', useNull: true, width: 110, summaryType: 'sum', editor: {xtype: 'floatfield'}, editable: false},
					{
						header: 'FX Rate', dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, width: 95, editor: {xtype: 'floatfield'}, editable: true, tooltip: '‘FX Rate’ is the Primary exchange rate that is always visible and based on the Event Type’s IsEventValueInEventCurrencyUnits field.' +
							'<br/><br/>If true (Stock Dividend), then the exchange rate is event currency to client account base currency and will always update the exchange rate on the transaction.' +
							'<br/><br/>If field is false (Swap Reset), then the exchange rate is security currency to event currency. If the event currency matches security currency or if the event, security, and client account base currency are all different (3 currencies), ‘FX to Base’ controls the exchange rate on the transaction represented as a security currency to client account base currency rate. If the event currency mismatches security currency, ‘FX Rate’ controls the exchange rate on the transaction and ‘FX to Base’ does not have any impact.'
					}
				],
				plugins: {ptype: 'gridsummary'}
			}
		],
		getDataAfterSave: function(data) {
			return TCG.data.getData(this.url + '&id=' + data.id, this);
		},
		getPositionsGrid: function() {
			return this.findByType('formgrid-scroll')[0].get(0);
		},
		listeners: {
			afterload: function(panel) {
				const grid = panel.getPositionsGrid();
				const sm = grid.getColumnModel();
				if (TCG.isFalse(panel.getFormValue('securityEvent.type.singlePayoutOnly'))) {
					sm.setHidden(sm.findColumnIndex('eventPayout.payoutNumber'), false);
				}
				let v = panel.getFormValue('journalType.affectedQuantityLabel');
				if (v) {
					sm.setColumnHeader(sm.findColumnIndex('affectedQuantity'), v);
					sm.setEditable(sm.findColumnIndex('affectedQuantity'), panel.getFormValue('journalType.affectedQuantityAdjustable'));
				}
				else {
					sm.setHidden(sm.findColumnIndex('affectedQuantity'), true);
				}
				v = panel.getFormValue('journalType.affectedCostLabel');
				if (v) {
					sm.setColumnHeader(sm.findColumnIndex('affectedCost'), v);
					sm.setEditable(sm.findColumnIndex('affectedCost'), panel.getFormValue('journalType.affectedCostAdjustable'));
				}
				else {
					sm.setHidden(sm.findColumnIndex('affectedCost'), true);
				}
				v = panel.getFormValue('journalType.transactionAmountLabel');
				if (v) {
					sm.setColumnHeader(sm.findColumnIndex('transactionAmount'), v);
					sm.setEditable(sm.findColumnIndex('transactionAmount'), panel.getFormValue('journalType.transactionAmountAdjustable'));
				}
				else {
					sm.setHidden(sm.findColumnIndex('transactionAmount'), true);
				}

				v = panel.getFormValue('journalType.additionalAmountLabel');
				if (v) {
					sm.setColumnHeader(sm.findColumnIndex('additionalAmount'), v);
					sm.setEditable(sm.findColumnIndex('additionalAmount'), panel.getFormValue('journalType.additionalAmountAdjustable'));
				}
				else {
					sm.setHidden(sm.findColumnIndex('additionalAmount'), true);
				}

				v = panel.getFormValue('journalType.additionalAmount2Label');
				if (v) {
					sm.setColumnHeader(sm.findColumnIndex('additionalAmount2'), v);
					sm.setEditable(sm.findColumnIndex('additionalAmount2'), panel.getFormValue('journalType.additionalAmount2Adjustable'));
				}
				else {
					sm.setHidden(sm.findColumnIndex('additionalAmount2'), true);
				}

				v = panel.getFormValue('journalType.additionalAmount3Label');
				if (v) {
					const i = sm.findColumnIndex('additionalAmount3');
					sm.setColumnHeader(i, v);
					if (v === 'FX to Base') {
						sm.setColumnTooltip(i, 'Secondary exchange rate that is only visible if Event Type’s IsEventValueInEventCurrencyUnits field is false (Swap Reset). The exchange rate is security currency denomination to client account base currency. The value controls the exchange rate on the transaction if the event currency and security match or if the event, security, and client account base currency are all different (3 currencies); otherwise, does not have any impact.');
					}
					sm.setEditable(i, panel.getFormValue('journalType.additionalAmount3Adjustable'));
				}
				else {
					sm.setHidden(sm.findColumnIndex('additionalAmount3'), true);
				}

				v = panel.getFormValue('journalType.transactionPriceLabel');
				if (v) {
					sm.setColumnHeader(sm.findColumnIndex('transactionPrice'), v);
					sm.setEditable(sm.findColumnIndex('transactionPrice'), panel.getFormValue('journalType.transactionPriceAdjustable'));
				}
				else {
					sm.setHidden(sm.findColumnIndex('transactionPrice'), true);
				}
			}
		}
	}]
});
