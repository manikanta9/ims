Clifton.accounting.eventjournal.EventJournalListWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingEventJournalListWindow',
	title: 'Event Journals',
	iconCls: 'event',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Upcoming Journals',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingEventJournalForecastListFind',
					instructions: 'Event journal forecasts are previews of upcoming event journals expected to process on specified date for Client Account and Security Event. Forecasts are useful for looking at previews of upcoming events expected to generate, however positions could potentially change prior to the event date.  Forecasts are rebuilt daily with latest client position information.',
					groupField: 'securityEvent.eventDate',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Events" : "Event"]}',
					appendStandardColumns: false,
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{
							header: '', width: 10, filter: false, sortable: false,
							renderer: function(v, metaData, r) {
								let imgCls = null;
								let tooltip = '';
								if (TCG.isNotBlank(r.data.description)) {
									imgCls = 'flag-red';
									if (r.data['description']) {
										tooltip = '<b>Event did not fully generate:</b><br/>' + r.data['description'];
									}
								}
								if (imgCls) {
									return '<span ext:qtip="' + tooltip + '"><span style="width: 16px; height: 16px; float: left;" class="' + imgCls + '" ext:qtip="' + tooltip + '">&nbsp;</span> </span>';
								}
							}
						},
						{header: 'Date', dataIndex: 'securityEvent.eventDate', width: 35, filter: {searchFieldName: 'eventDate'}, defaultSortColumn: true},
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 130, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.number', width: 50, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'Executing Broker', dataIndex: 'executingCompany.name', width: 80, filter: {type: 'combo', searchFieldName: 'executingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 70, hidden: true, filter: {type: 'combo', searchFieldName: 'journalTypeId', displayField: 'label', url: 'accountingEventJournalTypeListFind.json'}},
						{header: 'Security', dataIndex: 'securityEvent.security.symbol', width: 50, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Event', dataIndex: 'securityEvent.labelShort', width: 100, filter: {type: 'combo', searchFieldName: 'securityEventId', displayField: 'label', url: 'investmentSecurityEventListFind.json'}},
						{header: 'Quantity', dataIndex: 'affectedQuantity', width: 50, type: 'float', useNull: true},
						{header: 'Price', dataIndex: 'transactionPrice', width: 50, type: 'float', hidden: true, useNull: true},
						{header: 'Amount', dataIndex: 'transactionAmount', width: 50, type: 'float', useNull: true},
						{header: 'Additional Amount', dataIndex: 'additionalAmount', width: 50, type: 'float', hidden: true, useNull: true},
						{header: 'Cost', dataIndex: 'affectedCost', width: 50, type: 'float', useNull: true},
						{header: 'FX Rate', dataIndex: 'exchangeRateToBase', width: 50, type: 'float', hidden: true, useNull: true},
						{header: 'Forecast Description', dataIndex: 'description', width: 100, hidden: true},
						{header: 'Event Description', dataIndex: 'securityEvent.eventDescription', filter: {searchFieldName: 'eventDescription'}, width: 150, hidden: true}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setDateFilter();
						}

						const params = {};
						const t = this.getTopToolbar();
						const ig = TCG.getChildByName(t, 'investmentGroupId');
						if (TCG.isNotBlank(ig.getValue())) {
							params.investmentGroupId = ig.getValue();
						}
						const ca = TCG.getChildByName(t, 'clientInvestmentAccountId');
						if (TCG.isNotBlank(ca.getValue())) {
							params.clientInvestmentAccountId = ca.getValue();
						}
						return params;
					},
					setDateFilter: function(dv) {
						if (dv) {
							this.setFilterValue('securityEvent.eventDate', {'on': dv});
						}
						else {
							this.setFilterValue('securityEvent.eventDate', {'before': new Date().add(Date.DAY, 6)});
						}
					},
					editor: {
						detailPageClass: 'Clifton.accounting.eventjournal.EventJournalForecastWindow',
						drillDownOnly: true,
						addEditButtons: function(toolBar, gridPanel) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

							toolBar.add({
								text: 'Rebuild',
								tooltip: 'Rebuild forecast rows for a date range',
								iconCls: 'run',
								scope: this,
								handler: function() {
									const dd = {};
									dd.startDate = new Date().add(Date.DAY, -1).format('Y-m-d 00:00:00');
									dd.endDate = new Date().add(Date.DAY, 30).format('Y-m-d 00:00:00');
									TCG.createComponent('Clifton.accounting.eventjournal.EventJournalForecastRebuildWindow', {
										defaultData: dd,
										openerCt: gridPanel
									});
								}
							});
							toolBar.add('-');
						}
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 180, url: 'investmentGroupListFind.json', displayField: 'label'},
							{fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientInvestmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
							{fieldLabel: 'Journal Type', width: 180, xtype: 'toolbar-combo', url: 'accountingEventJournalTypeListFind.json', displayField: 'label', linkedFilter: 'journalType.name'}
						];
					}
				}]
			},


			{
				title: 'Pending Journals',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingEventJournalListFind',
					instructions: 'Event journals are sub-system journals that process investment security events. Each position lot for event security that is open on [Ex Date] gets corresponding event journal detail.<br/><br/>' +
						'Pending journals are journals that have not been booked yet. Event journals of type with [Accrual Reversal Account] create 2 separate accounting journals and are booked twice: first time accrual entries are created on [Ex Date] and second time accrued part is reversed and the real GL Account is used on [Event Date]. For example, accrue Dividend Receivable and then reverse the accrual and replace it with Cash.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 70, filter: {type: 'combo', searchFieldName: 'journalTypeId', displayField: 'label', url: 'accountingEventJournalTypeListFind.json'}},
						{header: 'Security Event', dataIndex: 'securityEvent.label', width: 150, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Event Description', dataIndex: 'securityEvent.eventDescription', filter: {searchFieldName: 'eventDescription'}, width: 150},
						{header: 'Event Date', dataIndex: 'securityEvent.eventDate', width: 35},
						{header: 'Booking Date', dataIndex: 'bookingDate', width: 50}
					],
					getLoadParams: function(firstLoad) {
						const params = {accrualReversalNotBooked: true};
						const t = this.getTopToolbar();
						const ig = TCG.getChildByName(t, 'investmentGroupId');
						if (TCG.isNotBlank(ig.getValue())) {
							params.investmentGroupId = ig.getValue();
						}
						const ca = TCG.getChildByName(t, 'clientInvestmentAccountId');
						if (TCG.isNotBlank(ca.getValue())) {
							params.clientInvestmentAccountId = ca.getValue();
						}
						return params;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientInvestmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 180, url: 'investmentGroupListFind.json', displayField: 'label'},
							{fieldLabel: 'Journal Type', width: 180, xtype: 'toolbar-combo', url: 'accountingEventJournalTypeListFind.json', displayField: 'label', linkedFilter: 'journalType.name'}
						];
					},
					editor: {
						detailPageClass: 'Clifton.accounting.eventjournal.EventJournalWindow',
						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Book',
								tooltip: 'Book selected event journal',
								iconCls: 'book-open-blue',
								scope: gridPanel,
								handler: function() {
									this.bookSelectedJournal(false);
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Book and Post',
								tooltip: 'Book selected event journal and immediately post it to the General Ledger',
								iconCls: 'book-red',
								scope: gridPanel,
								handler: function() {
									this.bookSelectedJournal(true);
								}
							});
							toolBar.add('-');
						}
					},
					bookSelectedJournal: function(post) {
						let confirmTitle = 'Book Event Journal';
						let confirmMessage = 'Would you like to book selected event journal?';
						if (post) {
							confirmTitle = 'Book & Post Event Journal';
							confirmMessage = 'Would you like to book selected event journal and immediately post it to the General Ledger?';
						}

						const panel = this;
						const grid = this.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to be booked.', 'No Row(s) Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selection bookings are not supported yet.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							Ext.Msg.confirm(confirmTitle, confirmMessage, function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid,
										params: {
											journalTypeName: 'Event Journal',
											sourceEntityId: sm.getSelected().id,
											postJournal: post
										},
										timeout: 180000,
										onLoad: function(record, conf) {
											panel.reload();
										}
									});
									loader.load('accountingJournalBook.json?enableOpenSessionInView=true');
								}
							});
						}
					}
				}]
			},


			{
				title: 'Booked Journals',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingEventJournalListFind',
					instructions: 'Event journals are sub-system journals that process investment security events. Each position lot for event security that is open on [Ex Date] gets corresponding event journal detail.<br/><br/>' +
						'Booked event journals create actual accounting journals that get posted to the General Ledger and can no longer be modified. Event journals of type with [Accrual Reversal Account] create 2 separate accounting journals and are booked twice: first time accrual entries are created on [Ex Date] and second time accrued part is reversed and the real GL Account is used on [Event Date]. For example, accrue Dividend Receivable and then reverse the accrual and replace it with Cash.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalTypeId', displayField: 'label', url: 'accountingEventJournalTypeListFind.json'}},
						{header: 'Investment Type', width: 60, dataIndex: 'securityEvent.security.instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'securityInvestmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
						{header: 'Investment Hierarchy', width: 130, dataIndex: 'securityEvent.security.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'securityInstrumentHierarchyId', queryParam: 'labelExpanded', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', listWidth: 600}, hidden: true},
						{header: 'Security', width: 100, dataIndex: 'securityEvent.security.label', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
						{header: 'Security Event', dataIndex: 'securityEvent.label', width: 150, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Event Description', dataIndex: 'securityEvent.eventDescription', filter: {searchFieldName: 'eventDescription'}, width: 150},
						{header: 'Journal Description', dataIndex: 'description', width: 150, hidden: true},
						{header: 'Ex Date', dataIndex: 'securityEvent.exDate', width: 35, hidden: true},
						{header: 'Event Date', dataIndex: 'securityEvent.eventDate', width: 35, hidden: true},
						{header: 'Booking Date', dataIndex: 'bookingDate', width: 50},
						{header: 'Accrual Reversal', dataIndex: 'accrualReversalBookingDate', width: 50, title: 'Date when Accrual Reversal journal was booked'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('bookingDate', {'after': new Date().add(Date.DAY, -30)});
						}
						const params = {journalBooked: true};
						const t = this.getTopToolbar();
						const ig = TCG.getChildByName(t, 'investmentGroupId');
						if (TCG.isNotBlank(ig.getValue())) {
							params.investmentGroupId = ig.getValue();
						}
						const ca = TCG.getChildByName(t, 'clientInvestmentAccountId');
						if (TCG.isNotBlank(ca.getValue())) {
							params.clientInvestmentAccountId = ca.getValue();
						}
						return params;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientInvestmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 180, url: 'investmentGroupListFind.json', displayField: 'label'},
							{fieldLabel: 'Journal Type', width: 180, xtype: 'toolbar-combo', url: 'accountingEventJournalTypeListFind.json', displayField: 'label', linkedFilter: 'journalType.name'}
						];
					},
					editor: {
						detailPageClass: 'Clifton.accounting.eventjournal.EventJournalWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Journal Types',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingEventJournalTypeListFind',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Event journal type maps to a corresponding security event type and defines rules for booking security events of that type. Event journals of type with [Accrual Reversal Account] create 2 separate accounting journals and are booked twice: first time accrual entries are created on [Ex Date] and second time accrued part is reversed and the real GL Account is used on [Event Date]. For example, accrue Dividend Receivable and then reverse the accrual and replace it with Cash.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Journal Type Name', dataIndex: 'name', width: 100, defaultSortColumn: true},
						{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
						{header: 'Event Type', dataIndex: 'eventType.name', width: 80},
						{header: 'Scope', dataIndex: 'eventJournalTypeScope', width: 70},
						{header: 'FX Date Selector', dataIndex: 'exchangeRateDateSelector', width: 100},
						{header: 'Debit Account', dataIndex: 'debitAccount.label', width: 100, hidden: true},
						{header: 'Credit Account', dataIndex: 'creditAccount.label', width: 100, hidden: true},
						{header: 'Accrual Reversal Account', dataIndex: 'accrualReversalAccount.label', width: 100, hidden: true},
						{header: 'Settlement Date', dataIndex: 'settlementDateComparison', width: 50, type: 'boolean'},
						{header: 'Before Ex Date', dataIndex: 'daysBeforeExDateForPositionLookup', width: 50, type: 'int', useNull: true, tooltip: 'Specifies the number of days to use from Ex Date when determining if a position should have events.'},
						{header: 'Auto Generate', dataIndex: 'autoGenerate', width: 50, type: 'boolean'},
						{header: 'Auto Post', dataIndex: 'autoPost', width: 40, type: 'boolean', tooltip: 'Immediately post generated event journals to the General Ledger'},
						{header: 'Order', dataIndex: 'bookingOrder', width: 30, type: 'int', useNull: true},

						{header: 'Affected Quantity Label', dataIndex: 'affectedQuantityLabel', width: 50, hidden: true},
						{header: 'Affected Cost Label', dataIndex: 'affectedCostLabel', width: 50, hidden: true},
						{header: 'Transaction Price Label', dataIndex: 'transactionPriceLabel', width: 50, hidden: true},
						{header: 'Transaction Amount Label', dataIndex: 'transactionAmountLabel', width: 50, hidden: true},
						{header: 'Additional Amount Label', dataIndex: 'additionalAmountLabel', width: 50, hidden: true},
						{header: 'Additional Amount 2 Label', dataIndex: 'additionalAmount2Label', width: 50, hidden: true},
						{header: 'Additional Amount 3 Label', dataIndex: 'additionalAmount3Label', width: 50, hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.eventjournal.EventJournalTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Security Events',
				items: [{
					xtype: 'investment-security-events-grid'
				}]
			},


			{
				title: 'Event Payouts',
				items: [{
					xtype: 'investment-security-event-payouts-grid'
				}]
			},


			{
				title: 'Client Elections',
				items: [{
					name: 'investmentSecurityEventClientElectionListFind',
					xtype: 'gridpanel',
					instructions: 'For corporate actions that offer elections, identifies specific payout elections that each client that held corresponding position made. Election Quantity is usually equal to the total quantity held. However, it is possible to split existing position across more than one election.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'CA ID', width: 35, dataIndex: 'securityEvent.corporateActionIdentifier', filter: {searchFieldName: 'corporateActionIdentifier'}, type: 'int', numberFormat: '0', useNull: true, tooltip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
						{header: 'Event Type', width: 80, dataIndex: 'securityEvent.type.name', filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
						{header: 'Event Status', width: 70, dataIndex: 'securityEvent.status.name', renderer: Clifton.investment.instrument.event.renderEventStatus, filter: {type: 'combo', searchFieldName: 'statusId', url: 'investmentSecurityEventStatusListFind.json', showNotEquals: true}},
						{header: 'Description', width: 200, dataIndex: 'securityEvent.eventDescription', filter: {searchFieldName: 'eventDescription'}, hidden: true},
						{header: 'Security', width: 100, dataIndex: 'securityEvent.security.label', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Ex Date', width: 45, dataIndex: 'securityEvent.exDate', filter: {searchFieldName: 'exDate'}, hidden: true},
						{header: 'Record Date', width: 45, dataIndex: 'securityEvent.recordDate', filter: {searchFieldName: 'recordDate'}, hidden: true},
						{header: 'Event Date', width: 45, dataIndex: 'securityEvent.eventDate', filter: {searchFieldName: 'eventDate'}, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Client Account', width: 180, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Election #', width: 50, dataIndex: 'electionNumber', type: 'int'},
						{header: 'Election Qty', width: 50, dataIndex: 'electionQuantity', type: 'float', useNull: true, tooltip: 'Empty Election Quantity, means total quantity held by the client (most common election).'},
						{header: 'Election Value', width: 45, dataIndex: 'electionValue', type: 'float', useNull: true, tooltip: 'Election Value will be used to allow clients to declare a bid on Dutch Auctions for consideration by the Event Offeror.'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 200, url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Event Type', xtype: 'toolbar-combo', width: 200, url: 'investmentSecurityEventTypeListFind.json', linkedFilter: 'securityEvent.type.name'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('securityEvent.eventDate', {
								after: new Date().add(Date.DAY, -30),
								before: new Date().add(Date.DAY, 30)
							});
						}
						const v = TCG.getChildByName(this.getTopToolbar(), 'investmentGroupId').getValue();
						if (v) {
							return {investmentGroupId: v};
						}
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.payout.SecurityEventClientElectionWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Generator',
				items: [Clifton.accounting.eventjournal.EventJournalGeneratorForm]
			}
		]
	}]
});
