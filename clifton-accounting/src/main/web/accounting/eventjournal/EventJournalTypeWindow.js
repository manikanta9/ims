Clifton.accounting.eventjournal.EventJournalTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Event Journal Type',
	iconCls: 'event',
	height: 650,
	width: 800,

	items: [{
		xtype: 'formpanel',
		url: 'accountingEventJournalType.json',
		instructions: 'Event journal type maps to a corresponding security event type and defines rules for booking security events of that type. Event journals of type with [Accrual Reversal Account] create 2 separate accounting journals and are booked twice: first time accrual entries are created on [Ex Date] and second time accrued part is reversed and the real GL Account is used on [Event Date]. For example, accrue Dividend Receivable and then reverse the accrual and replace it with Cash.',
		readOnly: true,
		labelWidth: 145,
		items: [
			{fieldLabel: 'Event Type', name: 'eventType.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow', detailIdField: 'eventType.id'},
			{fieldLabel: 'Scope', name: 'eventJournalTypeScope', qtip: 'Optionally limits this Event Journal Type to a sub-set of securities.'},
			{fieldLabel: 'Journal Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},

			{xtype: 'sectionheaderfield', header: 'Configuration', fieldLabel: ''},
			{fieldLabel: 'Booking Order', name: 'bookingOrder'},
			{fieldLabel: 'FX Date Selector', name: 'exchangeRateDateSelector', qtip: 'Some event types may require Exchange Rate from a very specific date for foreign transactions (Posting Date for Cleared CDS Premium Leg Payment).'},
			{fieldLabel: 'Days Before Ex Date', name: 'daysBeforeExDateForPositionLookup', qtip: 'Specifies the number of days to use from Ex Date when determining if a position should have events.'},
			{boxLabel: 'Use Settlement Date for affected position lookup', name: 'settlementDateComparison', xtype: 'checkbox'},
			{boxLabel: 'Automatically Generate journals of this type', name: 'autoGenerate', xtype: 'checkbox'},
			{boxLabel: 'Automatically Post journals of this type to the General Ledger', name: 'autoPost', xtype: 'checkbox'},

			{xtype: 'sectionheaderfield', header: 'GL Accounts', fieldLabel: ''},
			{fieldLabel: 'Debit Account', name: 'debitAccount.label', xtype: 'displayfield'},
			{fieldLabel: 'Credit Account', name: 'creditAccount.label', xtype: 'displayfield'},
			{fieldLabel: 'Accrual Reversal Account', name: 'accrualReversalAccount.label', xtype: 'displayfield'},

			{xtype: 'sectionheaderfield', header: 'Event Journal Detail Fields', fieldLabel: ''},
			{
				fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
				defaults: {
					xtype: 'displayfield',
					flex: 1
				},
				items: [
					{value: 'Label'},
					{value: 'Adjustable'}
				]
			},
			{
				fieldLabel: 'Affected Quantity', xtype: 'compositefield',
				defaults: {
					flex: 1
				},
				items: [
					{name: 'affectedQuantityLabel', xtype: 'textfield'},
					{name: 'affectedQuantityAdjustable', xtype: 'checkbox'}
				]
			},
			{
				fieldLabel: 'Affected Cost', xtype: 'compositefield',
				defaults: {
					flex: 1
				},
				items: [
					{name: 'affectedCostLabel', xtype: 'textfield'},
					{name: 'affectedCostAdjustable', xtype: 'checkbox'}
				]
			},
			{
				fieldLabel: 'Transaction Price', xtype: 'compositefield',
				defaults: {
					flex: 1
				},
				items: [
					{name: 'transactionPriceLabel', xtype: 'textfield'},
					{name: 'transactionPriceAdjustable', xtype: 'checkbox'}
				]
			},
			{
				fieldLabel: 'Transaction Amount', xtype: 'compositefield',
				defaults: {
					flex: 1
				},
				items: [
					{name: 'transactionAmountLabel', xtype: 'textfield'},
					{name: 'transactionAmountAdjustable', xtype: 'checkbox'}
				]
			},
			{
				fieldLabel: 'Additional Amount', xtype: 'compositefield',
				defaults: {
					flex: 1
				},
				items: [
					{name: 'additionalAmountLabel', xtype: 'textfield'},
					{name: 'additionalAmountAdjustable', xtype: 'checkbox'}
				]
			},
			{
				fieldLabel: 'Additional Amount 2', xtype: 'compositefield',
				defaults: {
					flex: 1
				},
				items: [
					{name: 'additionalAmount2Label', xtype: 'textfield'},
					{name: 'additionalAmount2Adjustable', xtype: 'checkbox'}
				]
			},
			{
				fieldLabel: 'Additional Amount 3', xtype: 'compositefield',
				defaults: {
					flex: 1
				},
				items: [
					{name: 'additionalAmount3Label', xtype: 'textfield'},
					{name: 'additionalAmount3Adjustable', xtype: 'checkbox'}
				]
			}
		]
	}]
});
