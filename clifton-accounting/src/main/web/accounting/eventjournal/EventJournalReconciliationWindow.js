Ext.ns('Clifton.accounting.eventjournal.EventJournalReconciliationUtil');


Clifton.accounting.eventjournal.EventJournalReconciliationWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingEventJournalReconciliationWindow',
	title: 'Event Journal Reconciliation',
	iconCls: 'event',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		listeners: {
			// keep selected date range, instrument group, client account group
			beforetabchange: function(tabPanel, newTab, currentTab) {
				if (currentTab) {
					const tb = currentTab.items.get(0).getTopToolbar();
					tabPanel.saveToolbarFilter(tb, 'displayDateRange');
					tabPanel.saveToolbarFilter(tb, 'investmentGroupId');
					tabPanel.saveToolbarFilter(tb, 'clientInvestmentAccountGroupId');
					tabPanel.saveToolbarFilter(tb, 'journalTypeId');
				}
			},
			tabchange: function(tabPanel, tab) {
				const tb = tab.items.get(0).getTopToolbar();
				tabPanel.restoreToolbarFilter(tb, 'displayDateRange');
				tabPanel.restoreToolbarFilter(tb, 'investmentGroupId');
				tabPanel.restoreToolbarFilter(tb, 'clientInvestmentAccountGroupId');
				tabPanel.restoreToolbarFilter(tb, 'journalTypeId');
			}
		},
		items: [
			{
				title: 'Upcoming Journals',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingEventJournalForecastListFind',
					instructions: 'Event journal forecasts are previews of upcoming event journals expected to process on specified date for Client Account and Security Event. Forecasts are useful for looking at previews of upcoming events expected to generate, however positions could potentially change prior to the event date.  Forecasts are rebuilt daily with latest client position information.',
					viewNames: ['All Securities', 'Swaps by Date', 'Swaps by Security', 'Bond Factor Changes - Assumed Buy', 'Bond Factor Changes - Assumed Sell', 'CCY Forward Maturities'],
					groupField: 'securityEvent.eventDate',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Events" : "Event"]}',
					appendStandardColumns: false,
					additionalPropertiesToRequest: 'securityEvent.id|securityEvent.security.id',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{
							header: '', width: 10, filter: false, sortable: false,
							renderer: function(v, metaData, r) {
								let imgCls;
								let tooltip = '';
								if (TCG.isNotBlank(r.data.description)) {
									imgCls = 'flag-red';
									if (r.data['description']) {
										tooltip = '<b>Event did not fully generate:</b><br/>' + r.data['description'];
									}
								}
								if (imgCls) {
									return '<span ext:qtip="' + tooltip + '"><span style="width: 16px; height: 16px; float: left;" class="' + imgCls + '" ext:qtip="' + tooltip + '">&nbsp;</span> </span>';
								}
							}
						},
						{header: 'Date', dataIndex: 'securityEvent.eventDate', width: 36, defaultSortColumn: true, filter: {searchFieldName: 'eventDate'}},
						{header: 'Settlement Date', dataIndex: 'journal.securityEvent.actualSettlementDate', hidden: true, width: 45, filter: {searchFieldName: 'actualSettlementDate'}, tooltip: 'The actual settlement date, can be different from the contractual settlement date.'},
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 105, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Team', dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', hidden: true, width: 50, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
						{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.number', width: 50, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.name', width: 70, filter: {type: 'combo', searchFieldName: 'holdingAccountIssuerId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Executing Company', dataIndex: 'executingCompany.name', hidden: true, width: 70, filter: {type: 'combo', searchFieldName: 'executingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 70, hidden: true, filter: {type: 'combo', searchFieldName: 'journalTypeId', displayField: 'label', url: 'accountingEventJournalTypeListFind.json'}},
						{header: 'Security', dataIndex: 'securityEvent.security.symbol', width: 60, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Internal Identifier', dataIndex: 'securityEvent.security.cusip', width: 60, hidden: true, filter: {searchFieldName: 'cusip'}},
						{header: 'CCY', dataIndex: 'paymentCurrency.symbol', width: 20, filter: {type: 'combo', searchFieldName: 'paymentCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Underlying', dataIndex: 'securityEvent.security.underlyingSecurity.symbol', width: 40, filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Event Type', dataIndex: 'securityEvent.type.name', hidden: true, width: 90, filter: {type: 'combo', searchFieldName: 'securityEventTypeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
						{header: 'Security Event', dataIndex: 'securityEvent.labelShort', width: 90, filter: {type: 'combo', searchFieldName: 'securityEventId', displayField: 'label', url: 'investmentSecurityEventListFind.json'}},
						{header: 'Quantity', dataIndex: 'affectedQuantity', width: 50, type: 'float', useNull: true, summaryType: 'sum'},
						{header: 'Price', dataIndex: 'transactionPrice', width: 50, type: 'float', hidden: true, useNull: true},
						{
							header: 'Amount', dataIndex: 'transactionAmount', width: 50, type: 'currency', useNull: true, summaryType: 'sum', allViews: true, viewNameHeaders: [{name: 'All Securities', label: 'Amount'}, {name: 'Swaps by Date', label: 'Payment'}, {name: 'Swaps by Security', label: 'Payment'}],
							tooltip: 'Transaction amount that applies to the event, i.e. Payment Amount, or Principal for Credit Events.'
						},
						{
							header: 'Additional Amount', dataIndex: 'additionalAmount', width: 50, type: 'float', hidden: true, useNull: true,
							tooltip: 'Additional amount that applies to the event, i.e. Loss Amount for Credit Events.'
						},
						{
							header: 'Additional Amount 2', dataIndex: 'additionalAmount2', width: 50, type: 'float', hidden: true, useNull: true, summaryType: 'sum',
							tooltip: 'Second additional amount that applies to the event, i.e. Accrued Interest for Credit Events.'
						},
						{
							header: 'Additional Amount 3', dataIndex: 'additionalAmount3', width: 50, type: 'float', hidden: true, useNull: true, summaryType: 'sum',
							tooltip: 'Third additional amount that applies to the event.'
						},
						{header: 'Cost', dataIndex: 'affectedCost', width: 50, type: 'currency', useNull: true, summaryType: 'sum', allViews: true, viewNameHeaders: [{name: 'All Securities', label: 'Cost'}, {name: 'Swaps by Date', label: 'Notional'}, {name: 'Swaps by Security', label: 'Notional'}]},
						{header: 'FX Rate', dataIndex: 'exchangeRateToBase', width: 50, type: 'float', hidden: true, useNull: true},
						{header: 'Forecast Description', dataIndex: 'description', width: 100, hidden: true},
						{header: 'Event Description', dataIndex: 'securityEvent.eventDescription', filter: {searchFieldName: 'eventDescription'}, width: 150, hidden: true}
					],
					plugins: {ptype: 'gridsummary', showGrandTotal: false},
					getLoadParams: function(firstLoad) {
						return Clifton.accounting.eventjournal.EventJournalReconciliationUtil.getLoadParams(this, {}, firstLoad, 'securityEvent.eventDate');
					},
					switchToViewBeforeReload: function(viewName) {
						Clifton.accounting.eventjournal.EventJournalReconciliationUtil.setDefaultTopToolbarFilterValues(this, viewName, 'securityEvent.eventDate', 'securityEvent.security.symbol');
					},
					getTopToolbarFilters: function(toolbar) {
						return Clifton.accounting.eventjournal.EventJournalReconciliationUtil.getTopToolbarFilters(toolbar, 'securityEvent.eventDate', 'journalType.name');
					},
					editor: {
						detailPageClass: 'Clifton.accounting.eventjournal.EventJournalForecastWindow',
						drillDownOnly: true,
						addEditButtons: function(toolBar, gridPanel) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
							toolBar.add({
								text: 'Rebuild',
								tooltip: 'Rebuild forecast rows for a date range',
								iconCls: 'run',
								scope: this,
								handler: function() {
									const dd = {};
									dd.startDate = new Date().add(Date.DAY, -1).format('Y-m-d 00:00:00');
									dd.endDate = new Date().add(Date.DAY, 30).format('Y-m-d 00:00:00');
									TCG.createComponent('Clifton.accounting.eventjournal.EventJournalForecastRebuildWindow', {
										defaultData: dd,
										openerCt: gridPanel
									});
								}
							});
							toolBar.add('-');
						},
						// Used to Open the Security Event Window
						openWindowFromContextMenu: function(grid, rowIndex, screen) {
							const gridPanel = this.getGridPanel();
							const row = grid.store.data.items[rowIndex];
							let clazz = 'Clifton.investment.instrument.event.SecurityEventWindow';
							let id = row.json.securityEvent.id;
							if (screen === 'SECURITY') {
								clazz = 'Clifton.investment.instrument.SecurityWindow';
								id = row.json.securityEvent.security.id;
							}
							if (clazz) {
								this.openDetailPage(clazz, gridPanel, id, row);
							}
						}
					},
					listeners: {
						afterrender: function(gridPanel) {
							const el = gridPanel.getEl();
							el.on('contextmenu', function(e, target) {
								const g = gridPanel.grid;
								g.contextRowIndex = g.view.findRowIndex(target);
								e.preventDefault();
								if (!g.drillDownMenu) {
									g.drillDownMenu = new Ext.menu.Menu({
										items: [
											{
												text: 'Open Security', iconCls: 'stock-chart',
												handler: function() {
													gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'SECURITY');
												}
											}, '-',
											{
												text: 'Open Security Event', iconCls: 'event',
												handler: function() {
													gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'SECURITY_EVENT');
												}
											}
										]
									});
								}
								TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
							}, gridPanel);
						}
					}
				}]
			},


			{
				title: 'Pending Journal Details',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingEventJournalDetailListFind',
					instructions: 'Event journals are sub-system journals that process investment security events. Each position lot for event security that is open on [Ex Date] gets corresponding event journal detail.<br/><br/>' +
						'Pending journals are journals that have not been booked yet. Event journals of type with [Accrual Reversal Account] create 2 separate accounting journals and are booked twice: first time accrual entries are created on [Ex Date] and second time accrued part is reversed and the real GL Account is used on [Event Date]. For example, accrue Dividend Receivable and then reverse the accrual and replace it with Cash.',
					viewNames: ['All Securities', 'Swaps by Date', 'Swaps by Security', 'Bond Factor Changes - Assumed Buy', 'Bond Factor Changes - Assumed Sell', 'CCY Forward Maturities'],
					groupField: 'journal.securityEvent.eventDate',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Journal Details" : "Journal Detail"]}',
					rowSelectionModel: 'multiple',
					additionalPropertiesToRequest: 'id|journal.securityEvent.id|journal.securityEvent.security.id',
					pageSize: 200,
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Date', dataIndex: 'journal.securityEvent.eventDate', width: 45, filter: {searchFieldName: 'eventDate'}},
						{header: 'Settlement Date', dataIndex: 'journal.securityEvent.actualSettlementDate', hidden: true, width: 45, filter: {searchFieldName: 'actualSettlementDate'}, tooltip: 'The actual settlement date, can be different from the contractual settlement date.'},
						{header: 'JournalID', dataIndex: 'journal.id', hidden: true, width: 30, filter: {searchFieldName: 'journalId'}},
						{header: 'Client Account', dataIndex: 'accountingTransaction.clientInvestmentAccount.label', width: 120, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Team', dataIndex: 'accountingTransaction.clientInvestmentAccount.teamSecurityGroup.name', hidden: true, width: 50, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
						{header: 'Holding Account', dataIndex: 'accountingTransaction.holdingInvestmentAccount.number', width: 55, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'accountingTransaction.clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'Holding Company', dataIndex: 'accountingTransaction.holdingInvestmentAccount.issuingCompany.name', width: 75, filter: {type: 'combo', searchFieldName: 'holdingAccountIssuerId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Executing Company', dataIndex: 'accountingTransaction.executingCompany.name', hidden: true, width: 70, filter: {type: 'combo', searchFieldName: 'executingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Journal Type', dataIndex: 'journal.journalType.name', hidden: true, width: 70, filter: {type: 'combo', searchFieldName: 'journalTypeId', displayField: 'label', url: 'accountingEventJournalTypeListFind.json'}},
						{header: 'Security', dataIndex: 'journal.securityEvent.security.symbol', width: 55, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, defaultSortColumn: true},
						{header: 'Internal Identifier', dataIndex: 'journal.securityEvent.security.cusip', width: 55, filter: {searchFieldName: 'cusip'}, hidden: true},
						{header: 'CCY', dataIndex: 'journal.securityEvent.security.instrument.tradingCurrency.symbol', width: 20, filter: {type: 'combo', searchFieldName: 'securityCurrencyDenominationId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Underlying', dataIndex: 'journal.securityEvent.security.underlyingSecurity.symbol', width: 45, filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{
							header: 'Security Event', dataIndex: 'journal.securityEvent.labelShort', width: 110, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'},
							renderer: function(v, metaData, r) {
								const ed = r.json.journal.securityEvent.eventDescription;
								if (ed) {
									return '<div class="amountAdjusted" qtip="' + Ext.util.Format.htmlEncode(ed) + '">' + v + '</div>';
								}
								return v;
							}
						},
						{header: 'Event Type', dataIndex: 'journal.securityEvent.type.name', hidden: true, width: 90, filter: {type: 'combo', searchFieldName: 'securityEventTypeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
						{header: 'Event Description', dataIndex: 'journal.securityEvent.eventDescription', hidden: true, filter: {searchFieldName: 'eventDescription'}, width: 150},
						{header: 'Quantity', dataIndex: 'affectedQuantity', width: 50, type: 'float', useNull: true, summaryType: 'sum'},
						{header: 'Price', dataIndex: 'transactionPrice', width: 50, type: 'float', hidden: true, useNull: true},
						{
							header: 'Amount', dataIndex: 'transactionAmount', width: 50, type: 'currency', useNull: true, summaryType: 'sum', allViews: true, viewNameHeaders: [{name: 'All Securities', label: 'Amount'}, {name: 'Swaps by Date', label: 'Payment'}, {name: 'Swaps by Security', label: 'Payment'}],
							tooltip: 'Transaction amount that applies to the event, i.e. Payment Amount, or Principal for Credit Events.'
						},
						{
							header: 'Additional Amount', dataIndex: 'additionalAmount', width: 50, type: 'float', hidden: true, useNull: true,
							tooltip: 'Additional amount that applies to the event, i.e. Loss Amount for Credit Events.'
						},
						{
							header: 'Additional Amount 2', dataIndex: 'additionalAmount2', width: 50, type: 'float', hidden: true, useNull: true, summaryType: 'sum',
							tooltip: 'Second additional amount that applies to the event, i.e. Accrued Interest for Credit Events.'
						},
						{
							header: 'Additional Amount 3', dataIndex: 'additionalAmount3', width: 50, type: 'float', hidden: true, useNull: true, summaryType: 'sum',
							tooltip: 'Third additional amount that applies to the event.'
						},
						{header: 'Cost', dataIndex: 'affectedCost', width: 50, type: 'currency', useNull: true, summaryType: 'sum', allViews: true, viewNameHeaders: [{name: 'All Securities', label: 'Cost'}, {name: 'Swaps by Date', label: 'Notional'}, {name: 'Swaps by Security', label: 'Notional'}]},
						{header: 'FX Rate', dataIndex: 'exchangeRateToBase', width: 50, type: 'float', hidden: true, useNull: true},
						{
							header: 'Accruals Only', dataIndex: 'accrualPostingOnly', width: 50, type: 'boolean', sortable: false,
							tooltip: 'The journal has been booked but not the accrual reversals.  Reversals will not book until the Actual Settlement Date is specified.'
						}
					],
					plugins: {ptype: 'gridsummary', showGrandTotal: false},
					getLoadParams: function(firstLoad) {
						const params = {
							journalBooked: false,
							includeAccrualReceivables: this.checkReversalBooking('Swaps by Date')
						};
						return Clifton.accounting.eventjournal.EventJournalReconciliationUtil.getLoadParams(this, params, firstLoad);
					},
					checkReversalBooking: function(defaultView) {
						const viewName = this.currentViewName ? this.currentViewName : defaultView;
						return ['Swaps by Date', 'Swaps by Security'].includes(viewName);
					},
					switchToViewBeforeReload: function(viewName) {
						Clifton.accounting.eventjournal.EventJournalReconciliationUtil.setDefaultTopToolbarFilterValues(this, viewName);
					},
					getTopToolbarFilters: function(toolbar) {
						return Clifton.accounting.eventjournal.EventJournalReconciliationUtil.getTopToolbarFilters(toolbar);
					},
					listeners: {
						afterrender: function(gridPanel) {
							const el = gridPanel.getEl();
							el.on('contextmenu', function(e, target) {
								const g = gridPanel.grid;
								g.contextRowIndex = g.view.findRowIndex(target);
								e.preventDefault();
								if (!g.drillDownMenu) {
									g.drillDownMenu = new Ext.menu.Menu({
										items: [
											{
												text: 'Open Security', iconCls: 'stock-chart',
												handler: function() {
													gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'SECURITY');
												}
											}, '-',
											{
												text: 'Open Event Journal', iconCls: 'event',
												handler: function() {
													gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'EVENT_JOURNAL');
												}
											},
											{
												text: 'Open Security Event', iconCls: 'event',
												handler: function() {
													gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'SECURITY_EVENT');
												}
											}, '-',
											{
												text: 'View Report', iconCls: 'pdf',
												handler: function() {
													const row = g.store.data.items[g.contextRowIndex];
													const detailId = row.json.id;
													Clifton.accounting.eventjournal.downloadEventJournalDetailReport(detailId, null, gridPanel);
												}
											},
											{
												text: 'View Report', iconCls: 'excel_open_xml',
												handler: function() {
													const row = g.store.data.items[g.contextRowIndex];
													const detailId = row.json.id;
													Clifton.accounting.eventjournal.downloadEventJournalDetailReport(detailId, 'EXCEL_OPEN_XML', gridPanel);
												}
											},
											{
												text: 'View Report (2003-2007)', iconCls: 'excel',
												handler: function() {
													const row = g.store.data.items[g.contextRowIndex];
													const detailId = row.json.id;
													Clifton.accounting.eventjournal.downloadEventJournalDetailReport(detailId, 'EXCEL', gridPanel);
												}
											}, '-',
											{
												text: 'Add Settlement Notice Note', iconCls: 'pencil',
												tooltip: 'Create a note and link it to securities for selected events. Defaults to Settlement Notice, but note type can be changed.',
												handler: function() {
													gridPanel.addNoteToSelectedJournals();
												}
											}
										]
									});
								}
								TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
							}, gridPanel);
						}
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
						getDetailPageId: function(grid, row) {
							return row.json.journal.securityEvent.id;
						},
						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Book and Post',
								tooltip: 'Book selected event journal(s) and immediately post them to the General Ledger',
								iconCls: 'book-red',
								scope: gridPanel,
								handler: function() {
									this.bookSelectedJournals(true);
								}
							});
							toolBar.add('-');
						},
						addToolbarDeleteButton: function(toolBar) {
							toolBar.add({
								text: 'Remove',
								tooltip: 'Remove selected item(s)',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									const gridPanel = this.getGridPanel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Delete Selected Row(s)', 'Would you like to delete all selected rows(s)?', function(a) {
											if (a === 'yes') {
												const recordList = gridPanel.getJournalListForSelection(sm);
												gridPanel.deleteJournal(recordList, 0, gridPanel);
											}
										});
									}
								}
							});
							toolBar.add('-');
						},

						// Used to Open the Security Event Window
						openWindowFromContextMenu: function(grid, rowIndex, screen) {
							const gridPanel = this.getGridPanel();
							const row = grid.store.data.items[rowIndex];
							let clazz = this.detailPageClass;
							let id = row.json.journal.securityEvent.id;

							if (screen === 'EVENT_JOURNAL') {
								clazz = 'Clifton.accounting.eventjournal.EventJournalWindow';
								id = row.json.journal.id;
							}
							else if (screen === 'SECURITY') {
								clazz = 'Clifton.investment.instrument.SecurityWindow';
								id = row.json.journal.securityEvent.security.id;
							}
							if (clazz) {
								this.openDetailPage(clazz, gridPanel, id, row);
							}
						}
					},
					addNoteToSelectedJournals: function() {
						// Find Distinct Security IDs
						const gridPanel = this;
						const grid = this.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select at least one row to add a note to.', 'No Row(s) Selected');
						}
						else {
							const securityIds = [];
							const ut = sm.getSelections();
							for (let i = 0; i < ut.length; i++) {
								// add each individual security only once - could have multiple event details selected
								const record = ut[i].json;
								let skip = false;
								for (let j = 0; j < securityIds.length; j++) {
									if (TCG.isEquals(record.journal.securityEvent.security.id, securityIds[j])) {
										skip = true;
										break;
									}
								}
								if (!skip) {
									securityIds.push(record.journal.securityEvent.security.id);
								}
							}
							let className = 'Clifton.system.note.SingleNoteWindow';
							const tbl = TCG.data.getData('systemTableByName.json?tableName=InvestmentSecurity', gridPanel, 'system.table.InvestmentSecurity');
							const nt = TCG.data.getData('systemNoteTypeByTableAndName.json?tableId=' + tbl.id + '&noteTypeName=Settlement Notice', gridPanel);
							let dd = {
								noteType: nt
							};
							if (securityIds.length === 1) {
								dd = Ext.apply(dd, {linkedToMultiple: false, fkFieldId: securityIds[0]});
							}
							else {
								className = 'Clifton.system.note.LinkedNoteWindow';
								dd = Ext.apply(dd, {linkedToMultiple: true, fkFieldIds: securityIds});
							}
							const cmpId = TCG.getComponentId(className);
							TCG.createComponent(className, {
								id: cmpId,
								defaultData: dd,
								openerCt: gridPanel
							});
						}
					},
					deleteJournal: function(recordList, count, gridPanel) {
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Deleting Event Journal',
							waitTarget: this,
							params: {
								id: recordList[count].journal.id
							},
							onLoad: function(record, conf) {
								count++;
								if (count === recordList.length) { // refresh after all journals were deleted
									gridPanel.reload();
								}
								else {
									gridPanel.deleteJournal(recordList, count, gridPanel);
								}
							}
						});
						loader.load('accountingEventJournalDelete.json');
					},
					bookSelectedJournals: function(post) {
						let confirmTitle = 'Book Event Journal(s)';
						let confirmMessage = 'Would you like to book selected event journal(s)?';
						if (post) {
							confirmTitle = 'Book & Post Event Journal(s)';
							confirmMessage = 'Would you like to book selected event journal(s) and immediately post them to the General Ledger?';
						}
						confirmMessage += '<br/><br/><b>NOTE:</b> If selected row detail includes other details rows in the journal they will also be booked.  Drill into the journal record to see all of the details that will be booked.';
						const gridPanel = this;
						const grid = this.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to be booked.', 'No Row(s) Selected');
						}
						else {
							Ext.Msg.confirm(confirmTitle, confirmMessage, function(a) {
								if (a === 'yes') {
									const recordList = gridPanel.getJournalListForSelection(sm);
									gridPanel.doBookSelectedJournals(post, recordList, 0, gridPanel);
								}
							});
						}
					},
					// sm - the selection model with selected row information
					getJournalListForSelection: function(sm) {
						const ut = sm.getSelections();
						const recordList = [];
						for (let i = 0; i < ut.length; i++) {
							// add multiple details from the same journal only once
							const record = ut[i].json;
							let skip = false;
							for (let j = 0; j < recordList.length; j++) {
								if (TCG.isEquals(record.journal.id, recordList[j].journal.id)) {
									skip = true;
									break;
								}
							}
							if (!skip) {
								recordList.push(record);
							}
						}
						return recordList;
					},
					doBookSelectedJournals: function(post, recordList, count, gridPanel) {
						const grid = this.grid;
						const r = recordList[count];
						const loader = new TCG.data.JsonLoader({
							waitTarget: grid,
							params: {
								journalTypeName: 'Event Journal',
								sourceEntityId: r.journal.id,
								postJournal: post
							},
							timeout: 180000,
							onLoad: function(record, conf) {
								count++;
								if (count === recordList.length) { // refresh after all records were posted
									gridPanel.reload();
								}
								else {
									gridPanel.doBookSelectedJournals(post, recordList, count, gridPanel);
								}
							}
						});
						loader.load('accountingJournalBook.json?enableOpenSessionInView=true&requestedPropertiesRoot=data&requestedProperties=id');
					}
				}]
			},


			{
				title: 'Booked Journal Details',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingEventJournalDetailListFind',
					instructions: 'Event journals are sub-system journals that process investment security events. Each position lot for event security that is open on [Ex Date] gets corresponding event journal detail.<br/><br/>' +
						'Booked event journals create actual accounting journals that get posted to the General Ledger and can no longer be modified. Event journals of type with [Accrual Reversal Account] create 2 separate accounting journals and are booked twice: first time accrual entries are created on [Ex Date] and second time accrued part is reversed and the real GL Account is used on [Event Date]. For example, accrue Dividend Receivable and then reverse the accrual and replace it with Cash.',
					viewNames: ['All Securities', 'Swaps by Date', 'Swaps by Security', 'Bond Factor Changes - Assumed Buy', 'Bond Factor Changes - Assumed Sell', 'CCY Forward Maturities'],
					groupField: 'journal.securityEvent.eventDate',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Journal Details" : "Journal Detail"]}',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Date', dataIndex: 'journal.securityEvent.eventDate', width: 50, filter: {searchFieldName: 'eventDate'}},
						{header: 'Settlement Date', dataIndex: 'journal.securityEvent.actualSettlementDate', hidden: true, width: 45, filter: {searchFieldName: 'actualSettlementDate'}, tooltip: 'The actual settlement date, can be different from the contractual settlement date.'},
						{header: 'JournalID', dataIndex: 'journal.id', hidden: true, width: 30, filter: {searchFieldName: 'journalId'}},
						{header: 'Client Account', dataIndex: 'accountingTransaction.clientInvestmentAccount.label', width: 110, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Team', dataIndex: 'accountingTransaction.clientInvestmentAccount.teamSecurityGroup.name', hidden: true, width: 50, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
						{header: 'Holding Account', dataIndex: 'accountingTransaction.holdingInvestmentAccount.number', width: 60, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'accountingTransaction.clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'Holding Company', dataIndex: 'accountingTransaction.holdingInvestmentAccount.issuingCompany.name', width: 70, filter: {type: 'combo', searchFieldName: 'holdingAccountIssuerId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Executing Company', dataIndex: 'accountingTransaction.executingCompany.name', hidden: true, width: 70, filter: {type: 'combo', searchFieldName: 'executingCompanyId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Journal Type', dataIndex: 'journal.journalType.name', hidden: true, width: 70, filter: {type: 'combo', searchFieldName: 'journalTypeId', displayField: 'label', url: 'accountingEventJournalTypeListFind.json'}},
						{header: 'Security', dataIndex: 'journal.securityEvent.security.symbol', width: 50, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Internal Identifier', dataIndex: 'journal.securityEvent.security.cusip', width: 55, filter: {searchFieldName: 'cusip'}, hidden: true},
						{header: 'CCY', dataIndex: 'journal.securityEvent.security.instrument.tradingCurrency.symbol', width: 25, filter: {type: 'combo', searchFieldName: 'securityCurrencyDenominationId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Underlying', dataIndex: 'journal.securityEvent.security.underlyingSecurity.symbol', width: 45, filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
						{header: 'Security Event', dataIndex: 'journal.securityEvent.labelShort', width: 120, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Event Type', dataIndex: 'journal.securityEvent.type.name', hidden: true, width: 90, filter: {type: 'combo', searchFieldName: 'securityEventTypeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
						{header: 'Event Description', dataIndex: 'journal.securityEvent.eventDescription', hidden: true, filter: {searchFieldName: 'eventDescription'}, width: 150},
						{header: 'Quantity', dataIndex: 'affectedQuantity', width: 50, type: 'float', useNull: true, summaryType: 'sum'},
						{header: 'Price', dataIndex: 'transactionPrice', width: 50, type: 'float', hidden: true, useNull: true},
						{
							header: 'Amount', dataIndex: 'transactionAmount', width: 50, type: 'float', useNull: true, summaryType: 'sum', allViews: true, viewNameHeaders: [{name: 'All Securities', label: 'Amount'}, {name: 'Swaps by Date', label: 'Payment'}, {name: 'Swaps by Security', label: 'Payment'}],
							tooltip: 'Transaction amount that applies to the event, i.e. Payment Amount, or Principal for Credit Events.'
						},
						{
							header: 'Additional Amount', dataIndex: 'additionalAmount', width: 50, type: 'float', hidden: true, useNull: true,
							tooltip: 'Additional amount that applies to the event, i.e. Loss Amount for Credit Events.'
						},
						{
							header: 'Additional Amount 2', dataIndex: 'additionalAmount2', width: 50, type: 'float', hidden: true, useNull: true, summaryType: 'sum',
							tooltip: 'Second additional amount that applies to the event, i.e. Accrued Interest for Credit Events.'
						},
						{
							header: 'Additional Amount 3', dataIndex: 'additionalAmount3', width: 50, type: 'float', hidden: true, useNull: true, summaryType: 'sum',
							tooltip: 'Third additional amount that applies to the event.'
						},
						{header: 'Cost', dataIndex: 'affectedCost', width: 50, type: 'float', useNull: true, summaryType: 'sum', allViews: true, viewNameHeaders: [{name: 'All Securities', label: 'Cost'}, {name: 'Swaps by Date', label: 'Notional'}, {name: 'Swaps by Security', label: 'Notional'}]},
						{header: 'FX Rate', dataIndex: 'exchangeRateToBase', width: 50, type: 'float', hidden: true, useNull: true},
						{header: 'Booking Date', dataIndex: 'journal.bookingDate', width: 50, searchFieldName: 'bookingDate'},
						{header: 'Accrual Reversal', dataIndex: 'journal.accrualReversalBookingDate', width: 60, searchFieldName: 'accrualReversalBookingDate'}
					],
					plugins: {ptype: 'gridsummary', showGrandTotal: false},
					getLoadParams: function(firstLoad) {
						const params = {
							journalBooked: true,
							includeAccrualReceivables: this.checkReversalBooking('Swaps by Date')
						};
						return Clifton.accounting.eventjournal.EventJournalReconciliationUtil.getLoadParams(this, params, firstLoad);
					},
					checkReversalBooking: function(defaultView) {
						const viewName = this.currentViewName ? this.currentViewName : defaultView;
						return ['Swaps by Date', 'Swaps by Security'].includes(viewName);
					},
					getTopToolbarFilters: function(toolbar) {
						return Clifton.accounting.eventjournal.EventJournalReconciliationUtil.getTopToolbarFilters(toolbar);
					},
					switchToViewBeforeReload: function(viewName) {
						Clifton.accounting.eventjournal.EventJournalReconciliationUtil.setDefaultTopToolbarFilterValues(this, viewName);
					},
					listeners: {
						afterrender: function(gridPanel) {
							const el = gridPanel.getEl();
							el.on('contextmenu', function(e, target) {
								const g = gridPanel.grid;
								g.contextRowIndex = g.view.findRowIndex(target);
								e.preventDefault();
								if (!g.drillDownMenu) {
									g.drillDownMenu = new Ext.menu.Menu({
										items: [
											{
												text: 'View Report', iconCls: 'pdf',
												handler: function() {
													const row = g.store.data.items[g.contextRowIndex];
													const detailId = row.json.id;
													Clifton.accounting.eventjournal.downloadEventJournalDetailReport(detailId, null, gridPanel);
												}
											},
											{
												text: 'View Report', iconCls: 'excel',
												handler: function() {
													const row = g.store.data.items[g.contextRowIndex];
													const detailId = row.json.id;
													Clifton.accounting.eventjournal.downloadEventJournalDetailReport(detailId, 'EXCEL', gridPanel);
												}
											}
										]
									});
								}
								TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
							}, gridPanel);
						}
					},
					editor: {
						detailPageClass: 'Clifton.accounting.eventjournal.EventJournalWindow',
						drillDownOnly: true,
						getDetailPageId: function(grid, row) {
							return row.json.journal.id;
						}
					}
				}]
			},


			{
				title: 'Generator',
				items: [Clifton.accounting.eventjournal.EventJournalGeneratorForm]
			},


			{
				title: 'Cash Instructions',
				items: [{
					xtype: 'investment-instruction-grid',
					tableName: 'AccountingEventJournalDetail',
					defaultCategoryName: 'Event Journal Counterparty',
					defaultViewName: 'Group by Recipient'
				}]
			}
		]
	}]
});


Clifton.accounting.eventjournal.EventJournalReconciliationUtil.getTopToolbarFilters = function(toolbar, dateField, journalTypeField) {
	return [
		{
			fieldLabel: 'Type', xtype: 'combo', name: 'journalTypeId', width: 130, url: 'accountingEventJournalTypeListFind.json',
			linkedFilter: journalTypeField || 'journal.journalType.name',
			listeners: {
				select: function(field) {
					TCG.getParentByClass(field, Ext.Panel).reload();
				}
			}
		},
		{
			fieldLabel: 'Instrument Group', xtype: 'combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json',
			listeners: {
				select: function(field) {
					TCG.getParentByClass(field, Ext.Panel).reload();
				}
			}
		},
		{
			fieldLabel: 'Client Acct Group', xtype: 'combo', name: 'clientInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json',
			listeners: {
				select: function(field) {
					TCG.getParentByClass(field, Ext.Panel).reload();
				}
			}
		},
		{
			fieldLabel: 'Display', xtype: 'combo', name: 'displayDateRange', width: 120, minListWidth: 150, mode: 'local', emptyText: 'Now to +5 business days', displayField: 'name', valueField: 'value',
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name', 'description'],
				data: [
					['PAST_DUE', 'Past Due', 'Display past due events: payment before today'],
					['PREVIOUS_MONTH', '-30 Days to Now', 'Display events from 30 days in the past to today'],
					['TODAY', 'Today', 'Display events for today'],
					['T1', 'Next Business Day', 'Display events for next day'],
					['WEEK', 'Now to +5 business days', 'Display events from today to 5 business days in the future'],
					['MONTH', 'Now to +30 days', 'Display events from today to 30 days in the future']
				]
			}),
			listeners: {
				select: function(field) {
					const v = field.getValue();
					const now = new Date();
					let from, to;
					if (v === 'PAST_DUE') {
						from = null;
						to = now;
					}
					else if (v === 'PREVIOUS_MONTH') {
						from = now.add(Date.DAY, -30);
						to = now.add(Date.DAY, 1);
					}
					else if (v === 'TODAY') {
						from = now.add(Date.DAY, -1);
						to = now.add(Date.DAY, 1);
					}
					else if (v === 'T1') {
						const t1 = Clifton.calendar.getBusinessDayFromDate(now, 1);
						from = t1.add(Date.DAY, -1);
						to = t1.add(Date.DAY, 1);
					}
					else if (v === 'MONTH') {
						from = now.add(Date.DAY, -1);
						to = now.add(Date.DAY, 30);
					}
					else {
						from = now.add(Date.DAY, -1);
						to = Clifton.calendar.getBusinessDayFromDate(now, 5);
					}
					const gp = TCG.getParentByClass(field, Ext.Panel);
					gp.setFilterValue(dateField || 'journal.securityEvent.eventDate', {
						'after': from,
						'before': to
					});
					gp.reload();
				}
			}
		}
	];
};

Clifton.accounting.eventjournal.EventJournalReconciliationUtil.setDefaultTopToolbarFilterValues = function(gp, viewName, dateField, securityField) {
	const ig = TCG.getChildByName(gp.getTopToolbar(), 'investmentGroupId');
	const jt = TCG.getChildByName(gp.getTopToolbar(), 'journalTypeId');

	if (viewName === 'Swaps by Date' || viewName === 'Swaps by Security') {
		const swaps = TCG.data.getData('investmentGroupByName.json?name=Swaps - ALL', gp, 'investment.group.Swaps - ALL');
		ig.setValue({text: swaps.name, value: swaps.id});
		jt.reload(null, true);
		jt.fireEvent('blur', jt);

		gp.grid.getStore().groupBy((viewName === 'Swaps by Security') ? (securityField || 'journal.securityEvent.security.symbol') : (dateField || 'journal.securityEvent.eventDate'), false);
		gp.ds.setDefaultSort((viewName === 'Swaps by Security') ? (dateField || 'journal.securityEvent.eventDate') : (securityField || 'journal.securityEvent.security.symbol'), 'ASC');
	}
	else if (viewName === 'Bond Factor Changes - Assumed Buy' || viewName === 'Bond Factor Changes - Assumed Sell') {
		const bonds = TCG.data.getData('investmentGroupByName.json?name=Bonds', gp, 'investment.group.Bonds');
		ig.setValue({text: bonds.name, value: bonds.id});

		let jtName = 'Factor Change - Assumed Buy';
		if (viewName === 'Bond Factor Changes - Assumed Sell') {
			jtName = 'Factor Change - Assumed Sell';
		}
		const fc = TCG.data.getData('accountingEventJournalTypeByName.json?name=' + jtName, gp, 'accounting.event.journal.type.' + jtName);
		jt.setValue({text: fc.name, value: fc.id});
		jt.fireEvent('select', jt);

		const dd = TCG.getChildByName(gp.getTopToolbar(), 'displayDateRange');
		dd.setValue({text: '-30 Days to Now', value: 'PREVIOUS_MONTH'});
		dd.fireEvent('select', dd);
	}
	else if (viewName === 'CCY Forward Maturities') {
		const ccy = TCG.data.getData('investmentGroupByName.json?name=Currency Forwards', gp, 'investment.group.Currency Forwards');
		ig.setValue({text: ccy.name, value: ccy.id});

		const sm = TCG.data.getData('accountingEventJournalTypeByName.json?name=Security Maturity', gp, 'accounting.event.journal.type.Security Maturity');
		jt.setValue({text: sm.name, value: sm.id});
		jt.fireEvent('select', jt);
	}
	else {
		ig.reload(null, true);
		jt.reload(null, true);
		jt.fireEvent('blur', jt);
	}
};

Clifton.accounting.eventjournal.EventJournalReconciliationUtil.getLoadParams = function(grid, params, firstLoad, dateField) {
	if (firstLoad) {
		const now = new Date();
		grid.setFilterValue(dateField || 'journal.securityEvent.eventDate', {
			'after': now.add(Date.DAY, -1),
			'before': Clifton.calendar.getBusinessDayFromDate(now, 5)
		});
		grid.setDefaultView('Swaps by Date'); // sets it as "checked"
		grid.switchToView('Swaps by Date', true);
	}
	const tb = grid.getTopToolbar();
	let v = TCG.getChildByName(tb, 'investmentGroupId').getValue();
	if (v) {
		params.investmentGroupId = v;
	}
	v = TCG.getChildByName(tb, 'clientInvestmentAccountGroupId').getValue();
	if (v) {
		params.clientInvestmentAccountGroupId = v;
	}
	return params;
};
