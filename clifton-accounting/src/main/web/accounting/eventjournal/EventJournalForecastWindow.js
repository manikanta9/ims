Clifton.accounting.eventjournal.EventJournalForecastWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'Upcoming Event Journal',
	iconCls: 'event',
	width: 800,
	height: 510,

	tbar: [{
		text: 'Preview',
		tooltip: 'Preview auto generated event journal(s) for this security event.  Event Journals are not saved or posted.',
		iconCls: 'preview',
		handler: function() {
			const fp = this.ownerCt.ownerCt.items.get(0);
			const eventId = TCG.getValue('securityEvent.id', fp.getForm().formValues);
			const defaultData = {
				eventId: eventId
			};

			const className = 'Clifton.accounting.eventjournal.EventJournalPreviewWindow';
			const cmpId = TCG.getComponentId(className, eventId);
			TCG.createComponent(className, {
				id: cmpId,
				defaultData: defaultData,
				openerCt: fp.getWindow()
			});
		}
	}],

	items: [{
		xtype: 'formpanel',
		url: 'accountingEventJournalForecast.json',
		labelFieldName: 'label',
		labelWidth: 140,
		readOnly: true,

		instructions: 'Event Journal Forecasts detail upcoming event journal details for a specified security event, client account, and holding account.  The actual event journal created may combine multiple forecast rows into one journal record where applicable.  Click <b>Preview</b> to view the actual details of the event journal and to generate and/or post it.',

		getWarningMessage: function(form) {
			let msg = form.formValues.description;
			if (msg && msg !== '') {
				msg = '<b>Event did not fully generate:</b> ' + msg;
			}
			return msg;
		},

		items: [
			{fieldLabel: 'Journal Type', name: 'journalType.name', xtype: 'displayfield'},
			{fieldLabel: 'Security Event', name: 'securityEvent.label', xtype: 'linkfield', detailIdField: 'securityEvent.id', detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow'},
			{fieldLabel: 'Event Date', name: 'securityEvent.eventDate', xtype: 'datefield'},
			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'Executing Company', name: 'executingCompany.name', xtype: 'linkfield', detailIdField: 'executingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Quantity', name: 'affectedQuantity', xtype: 'floatfield'},
			{fieldLabel: 'Price', name: 'transactionPrice', xtype: 'floatfield'},
			{fieldLabel: 'Amount', name: 'transactionAmount', xtype: 'floatfield'},
			{fieldLabel: 'Additional Amount', name: 'additionalAmount', xtype: 'floatfield'},
			{fieldLabel: 'Cost', name: 'affectedCost', xtype: 'currencyfield'},
			{fieldLabel: 'FX Rate', name: 'exchangeRateToBase', xtype: 'floatfield'},
			{fieldLabel: 'Note', name: 'description', xtype: 'hidden'}
		],

		// Updated Field Labels for Journal Details based on properties from the journal type
		// If field label is undefined, will hide the field in UI
		resetFieldLabel: function(fieldName, fieldLabel) {
			const f = this.getForm();

			const fld = f.findField(fieldName);
			if (fieldLabel) {
				fld.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(fieldLabel + ':');
				fld.fieldLabel = fieldLabel;
				fld.setVisible(true);
			}
			else {
				fld.setVisible(false);
			}
		},
		listeners: {
			afterload: function(panel) {
				panel.resetFieldLabel('affectedQuantity', panel.getFormValue('journalType.affectedQuantityLabel'));
				panel.resetFieldLabel('affectedCost', panel.getFormValue('journalType.affectedCostLabel'));
				panel.resetFieldLabel('transactionAmount', panel.getFormValue('journalType.transactionAmountLabel'));
				panel.resetFieldLabel('additionalAmount', panel.getFormValue('journalType.additionalAmountLabel'));
				panel.resetFieldLabel('transactionPrice', panel.getFormValue('journalType.transactionPriceLabel'));
			}
		}
	}]
});
