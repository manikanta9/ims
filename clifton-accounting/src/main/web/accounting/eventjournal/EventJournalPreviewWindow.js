Clifton.accounting.eventjournal.EventJournalPreviewWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Event Journal Generation Preview',
	width: 1300,
	height: 600,

	okButtonText: 'Generate and Post',
	okButtonTooltip: 'Generate and Post event journals for the security event as viewed on this screen.',

	layout: 'border',
	items: [
		{
			xtype: 'formpanel',
			region: 'north',
			height: 150,
			confirmBeforeSaveMsg: 'Are you sure you would like to generate these Event Journal(s) and immediately post it to the General Ledger?',

			items: [

				{xtype: 'hidden', name: 'eventId'},
				{xtype: 'hidden', name: 'securityId'},
				{xtype: 'hidden', name: 'eventDate'},
				{xtype: 'hidden', name: 'generatorType', value: 'POST_PREVIEWED'},
				{xtype: 'displayfield', name: 'resultMessages'},
				{
					xtype: 'formfragment',
					bodyStyle: 'padding: 0',
					frame: false,
					name: 'generateOptions',
					items: [
						{
							xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'generateForSpecificAccount', allowBlank: false,
							items: [
								{boxLabel: 'Generate and Post for <b>All</b> Accounts Previewed Below', xtype: 'radio', name: 'generateForSpecificAccount', inputValue: false},
								{boxLabel: 'Generate and Post for <b>Specified Account</b> Selected', xtype: 'radio', name: 'generateForSpecificAccount', inputValue: true}
							],
							listeners: {
								change: function(rg, r) {
									const p = TCG.getParentFormPanel(rg);
									const clientAccountIdField = p.getForm().findField('clientAccountId');
									if (TCG.isTrue(r.inputValue)) {
										clientAccountIdField.setDisabled(false);
									}
									else {
										clientAccountIdField.reset();
										clientAccountIdField.setDisabled(true);
									}
								}
							}
						},
						{
							fieldLabel: 'Client Account', xtype: 'combo', hiddenName: 'clientAccountId', name: 'clientAccountLabel', allowBlank: false, displayField: 'label',
							mode: 'local',
							disabled: true,
							qtip: 'Generate and post previewed journals for selected account only.'
						},
						{
							fieldLabel: 'Override Amount', name: 'amountOverride', xtype: 'currencyfield',
							qtip: 'Optionally override the payment amount for the selected client account.',
							requiredFields: ['clientAccountId']
						}
					]
				}
			],
			getSaveURL: function() {
				return 'accountingEventJournalListGenerate.json';
			}
		},
		{
			xtype: 'panel',
			region: 'center',
			autoScroll: true,

			listeners: {
				afterrender: function(p) {
					p.reload.defer(100, p);
				}
			},

			getWindow: function() {
				let result = this.findParentByType(Ext.Window);
				if (TCG.isNull(result)) {
					result = this.findParentBy(function(o) {
						return o.baseCls === 'x-window';
					});
				}
				return result;
			},

			reload: function() {
				const p = this;
				const params = p.getWindow().getMainFormPanel().getForm().formValues;
				params.requestedProperties = 'journal.journalType.name|journal.securityEvent.label|accountingTransaction.id|accountingTransaction.clientInvestmentAccount.id|accountingTransaction.clientInvestmentAccount.label|accountingTransaction.holdingInvestmentAccount.label|accountingTransaction.accountingAccount.label|accountingTransaction.originalTransactionDate|affectedQuantity|transactionPrice|transactionAmount|additionalAmount|additionalAmount2|additionalAmount3|affectedCost|currency|currencyActualBaseAmount|exchangeRateToBase|journal.journalType.affectedQuantityLabel|journal.journalType.affectedCostLabel|journal.journalType.transactionAmountLabel|journal.journalType.additionalAmountLabel|journal.journalType.additionalAmount2Label|journal.journalType.additionalAmount3Label|journal.journalType.transactionPriceLabel|skipMessage|previewAmount';
				params.requestedPropertiesRoot = 'accountingEventJournalDetailList';
				const loader = new TCG.data.JsonLoader({
					waitTarget: p,
					waitMsg: 'Loading Preview...',
					params: params,
					success: function(response, opts) {
						if (this.isUseWaitMsg()) {
							this.getMsgTarget().unmask();
						}
						const result = Ext.decode(response.responseText);
						if (result.success) {
							this.onLoad(result, this.conf);
						}
						else {
							this.onFailure();
							TCG.data.ErrorHandler.handleFailure(this, result);
						}
					},
					onLoad: function(data, conf) {
						p.refresh.call(p, data);
					}
				});
				loader.load('accountingEventJournalListForPreview.json');
			},

			refresh: function(resp) {
				this.removeAll(true);

				const accountData = [];

				const msg = resp.eventJournalGeneratorCommand.resultMessages;
				const data = resp.accountingEventJournalDetailList;
				const form = this.getWindow().getMainFormPanel().getForm();

				const fp = this.getWindow().getMainFormPanel();
				if (TCG.isNull(data) || data.length === 0) {
					this.addEmptyMessage(fp, 'Preview payment amounts have not been fully generated: ' + msg);
				}
				else {
					const l = data.length;
					let currData = [];
					let currLabel = undefined;
					for (let i = 0; i < l; i++) {
						const d = data[i];
						if (TCG.isBlank(d.skipMessage)) {
							accountData.push([d.accountingTransaction.clientInvestmentAccount.id, d.accountingTransaction.clientInvestmentAccount.label]);
						}
						if (d.journal.securityEvent.label !== currLabel) { // found new journal type
							if (currData.length > 0) {
								this.addGrid(currData);
							}
							currLabel = d.journal.securityEvent.label;
							currData = [];
						}
						currData[currData.length] = d;
						if (i === (l - 1)) {
							this.addGrid(currData);
						}
					}

					// If no accounts to generate/post then all previewed rows have already been generated
					if (TCG.isNull(accountData) || accountData.length === 0) {
						this.addEmptyMessage(fp, 'All event journal details listed below have already been generated.  See below to see calculated vs. actual payment amounts.');
					}
				}

				const store = new Ext.data.ArrayStore({
					fields: ['id', 'label'],
					data: accountData
				});
				form.findField('clientAccountId').bindStore(store);
				this.doLayout();
			},


			addEmptyMessage: function(fp, msg) {
				if (TCG.isNotBlank(msg)) {
					fp.insert(0, {xtype: 'container', layout: 'hbox', autoEl: 'div', cls: 'warning-msg', items: [{xtype: 'label', html: msg}]});
				}
				fp.getForm().findField('clientAccountId').setVisible(false);
				this.getWindow().buttons[0].disable();
				TCG.getChildByName(fp, 'generateOptions').setVisible(false);
				fp.setHeight(250);
				fp.doLayout();
			},

			addGrid: function(data) {
				const jt = data[0].journal.journalType;
				const lbl = data[0].journal.securityEvent.label;
				const cols = [
					{header: 'Journal Type', dataIndex: 'journal.journalType.name', width: 80, hidden: true},
					{
						header: '', width: 10, dataIndex: 'skipMessage', filter: false, sortable: false,
						renderer: function(v, metaData, r) {
							let imgCls = null;
							let tooltip = '';
							if (TCG.isNotBlank(v)) {
								if (Ext.isNumber(r.data['previewAmount'])) {
									imgCls = 'flag-red';
								}
								else {
									imgCls = 'flag-blue';
								}
								tooltip = v;
							}
							if (imgCls) {
								return '<span ext:qtip="' + tooltip + '"><span style="width: 16px; height: 16px; float: left;" class="' + imgCls + '" ext:qtip="' + tooltip + '">&nbsp;</span> </span>';
							}
						}
					},
					{header: 'PID', dataIndex: 'accountingTransaction.id', width: 90, type: 'int', doNotFormat: true},
					{header: 'Client Account', dataIndex: 'accountingTransaction.clientInvestmentAccount.label', width: 250, defaultSortColumn: true},
					{header: 'Holding Account', dataIndex: 'accountingTransaction.holdingInvestmentAccount.label', width: 250},
					{header: 'GL Account', dataIndex: 'accountingTransaction.accountingAccount.label', width: 150, hidden: true},
					{header: 'Date Open', dataIndex: 'accountingTransaction.originalTransactionDate', width: 80},
					{header: 'Quantity', dataIndex: 'affectedQuantity', type: 'float', width: 100, summaryType: 'sum'},
					{header: 'Cost', dataIndex: 'affectedCost', type: 'float', useNull: true, width: 100, summaryType: 'sum'},
					{header: 'Price', dataIndex: 'transactionPrice', type: 'float', useNull: true, width: 100},
					{header: 'Preview Amount', hidden: true, dataIndex: 'previewAmount', type: 'float', useNull: true, width: 110, summaryType: 'sum'},
					{
						header: 'Amount', dataIndex: 'transactionAmount', type: 'float', useNull: true, width: 110, summaryType: 'sum',
						renderer: function(v, metaData, r) {
							if (Ext.isNumber(r.data['previewAmount'])) {
								metaData.css = 'amountAdjusted';
								let qtip = '<table><tr><td>Calculated Value: </td><td align="right">' + TCG.numberFormat(r.data['previewAmount'], '0,000', true) + '</td></tr>';
								qtip += '<tr><td>Overridden Value: </td><td align="right">' + TCG.numberFormat(v, '0,000', true) + '</td></tr></table>';
								metaData.attr = 'qtip=\'' + qtip + '\'';
							}
							return TCG.numberFormat(v, '0,000', true);
						},
						summaryRenderer: function(v) {
							return TCG.numberFormat(v, '0,000', true);
						}
					},
					{header: 'Additional Amount', dataIndex: 'additionalAmount', type: 'float', useNull: true, width: 110, summaryType: 'sum'},
					{header: 'Additional Amount 2', dataIndex: 'additionalAmount2', type: 'float', useNull: true, width: 110, summaryType: 'sum'},
					{header: 'Additional Amount 3', dataIndex: 'additionalAmount3', type: 'float', useNull: true, width: 110, summaryType: 'sum'},
					{header: 'FX Rate', dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, width: 100}
				];

				TCG.grid.fillColumnMetaData(cols, [], []);

				this.add(new Ext.grid.GridPanel({
					autoHeight: true,
					collapsible: true,
					journalType: jt,
					viewConfig: {forceFit: true},
					title: lbl,
					store: new Ext.data.JsonStore({
						fields: ['journal.journalType.name', 'journal.securityEvent.label', 'accountingTransaction.id', 'accountingTransaction.clientInvestmentAccount.label', 'accountingTransaction.holdingInvestmentAccount.label'
							, 'accountingTransaction.accountingAccount.label', 'accountingTransaction.originalTransactionDate', 'affectedQuantity', 'transactionPrice', 'transactionAmount',
							'additionalAmount', 'additionalAmount2', 'additionalAmount3', 'affectedCost', 'currency', 'currencyActualBaseAmount', 'exchangeRateToBase', 'skipMessage', 'previewAmount'],
						data: data
					}),
					colModel: new Ext.grid.ColumnModel({columns: cols}),
					plugins: {ptype: 'gridsummary'},
					listeners: {
						rowdblclick: function(grid, rowIndex, evt) {
							const className = 'Clifton.accounting.gl.TransactionWindow';
							const row = grid.store.data.items[rowIndex];
							const id = row.data['accountingTransaction.id'];
							const cmpId = TCG.getComponentId(className, id);
							TCG.createComponent(className, {
								id: cmpId,
								params: {id: id},
								openerCt: grid
							});
						}
					},

					setColumnHeaderAndVisibility: function(columnModel, columnPropertyName, columnHeader) {
						const index = columnModel.findColumnIndex(columnPropertyName);
						if (columnHeader) {
							columnModel.setColumnHeader(index, columnHeader);
						}
						else {
							columnModel.setHidden(index, true);
						}
					},
					initComponent: function() {
						Ext.grid.GridPanel.prototype.initComponent.call(this, arguments);
						const cm = this.getColumnModel();
						const journalType = this.journalType;

						this.setColumnHeaderAndVisibility(cm, 'affectedQuantity', journalType.affectedQuantityLabel);
						this.setColumnHeaderAndVisibility(cm, 'affectedCost', journalType.affectedCostLabel);
						this.setColumnHeaderAndVisibility(cm, 'transactionAmount', journalType.transactionAmountLabel);
						this.setColumnHeaderAndVisibility(cm, 'additionalAmount', journalType.additionalAmountLabel);
						this.setColumnHeaderAndVisibility(cm, 'additionalAmount2', journalType.additionalAmount2Label);
						this.setColumnHeaderAndVisibility(cm, 'additionalAmount3', journalType.additionalAmount3Label);
						this.setColumnHeaderAndVisibility(cm, 'transactionPrice', journalType.transactionPriceLabel);
					}
				}));
			}
		}
	]
});

