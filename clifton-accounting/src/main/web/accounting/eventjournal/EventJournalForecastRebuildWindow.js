Clifton.accounting.eventjournal.EventJournalForecastRebuildWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Rebuild Event Journal Forecasts',
	iconCls: 'event',
	height: 250,
	width: 400,

	okButtonText: 'Rebuild',
	okButtonTooltip: 'Rebuild Event Journal Forecast Records for Selected Date Range',

	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			success: function(form, action) {
				TCG.showInfo(action.result.result, 'Rebuild Forecasts');
				win.closeWindow();
				win.openerCt.reload();
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	},

	items: [{
		xtype: 'formpanel',
		instructions: 'Enter a start and end date.',
		getSaveURL: function() {
			return 'accountingEventJournalForecastListProcess.json';
		},
		items: [
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: false},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', allowBlank: false}
		]
	}]
});
