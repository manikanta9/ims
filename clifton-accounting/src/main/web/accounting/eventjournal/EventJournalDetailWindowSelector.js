// not the actual window but looks up the event journal for the detail and opens that window
Clifton.accounting.eventjournal.EventJournalDetailWindowSelector = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'accountingEventJournalDetail.json?enableOpenSessionInView=true',

	openEntityWindow: function(config, entity) {
		const className = 'Clifton.accounting.eventjournal.EventJournalWindow';
		if (entity) {
			config.params.id = entity.journal.id;
			entity = TCG.data.getData('accountingEventJournal.json?id=' + entity.journal.id, this);
		}
		this.doOpenEntityWindow(config, entity, className);
	}
});
