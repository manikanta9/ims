Clifton.accounting.journal.JournalListWindow = Ext.extend(TCG.app.Window, {
	id: 'accountingJournalListWindow',
	title: 'Journals',
	iconCls: 'book-open-blue',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Unposted Journals',
				reloadOnTabChange: true,
				items: [{
					xtype: 'gridpanel',
					name: 'accountingJournalListFind',
					instructions: 'Posting is the process of moving a journal into the General Ledger. The following journals have not been posted to the General Ledger yet. Posting Date is not set.',
					rowSelectionModel: 'checkbox',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 40},
						{header: 'PID', dataIndex: 'parent.id', useNull: true, width: 40, type: 'int', doNotFormat: true},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalTypeId', loadAll: true, url: 'accountingJournalTypeList.json'}},
						{header: 'Journal Status', dataIndex: 'journalStatus.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalStatusId', loadAll: true, url: 'accountingJournalStatusList.json'}},
						{header: 'Journal Description', dataIndex: 'description', width: 300}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Journal Type', width: 180, xtype: 'toolbar-combo', url: 'accountingJournalTypeList.json', loadAll: true, linkedFilter: 'journalType.name'},
							{fieldLabel: 'Journal Status', width: 100, minListWidth: 100, xtype: 'toolbar-combo', url: 'accountingJournalStatusList.json', loadAll: true, linkedFilter: 'journalStatus.name'}
						];
					},
					getLoadParams: function() {
						return {unpostedJournalsOnly: true};
					},
					editor: {
						allowToDeleteMultiple: true,
						detailPageClass: 'Clifton.accounting.journal.JournalWindow',
						getDefaultData: function(gridPanel) {
							return {journalType: TCG.data.getData('accountingJournalTypeByName.json?journalTypeName=General Journal', gridPanel)};
						},
						addEditButtons: function(toolBar) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

							let menu = new Ext.menu.Menu();
							menu.add({
								text: 'Post Selected',
								iconCls: 'book-red',
								scope: this,
								handler: function() {
									this.getGridPanel().postSelectedJournals();
								}
							});
							menu.add({
								text: 'Post All',
								iconCls: 'book-red',
								scope: this,
								handler: function() {
									TCG.showInfo('Posting of ALL journals has not been implemented yet. Please post one journal at a time.');
								}
							});
							toolBar.add({
								text: 'Post',
								iconCls: 'book-red',
								tooltip: 'Post selected or all journals to the General Ledger.',
								menu: menu
							});
							toolBar.add('-');

							menu = new Ext.menu.Menu();
							menu.add({
								text: 'Unbook',
								iconCls: 'undo',
								scope: this,
								handler: function() {
									this.getGridPanel().unbookSelectedJournal(false);
								}
							});
							menu.add({
								text: 'Unbook & Delete',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									this.getGridPanel().unbookSelectedJournal(true);
								}
							});
							toolBar.add({
								text: 'Unbook',
								iconCls: 'undo',
								tooltip: 'Delete selected journal and clear source entry booking status or delete the source entry',
								menu: menu
							});
						}
					},
					unbookSelectedJournal: function(deleteSource) {
						const panel = this;
						const grid = this.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to be unbooked.', 'No Row(s) Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selection unbooking is not supported yet.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							Ext.Msg.confirm('Unbook Selected Row', 'Would you like to unbook selected row?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid,
										waitMsg: 'Unbooking...',
										params: {
											journalId: sm.getSelected().id,
											deleteSource: deleteSource
										},
										onLoad: function(record, conf) {
											panel.reload();
										}
									});
									loader.load('accountingJournalUnbook.json');
								}
							});
						}
					},
					postSelectedJournalList: function(journals, count, gridPanel) {
						const journal = journals[count].json;
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							waitMsg: 'Posting...',
							params: {
								journalId: journal.id
							},
							onLoad: function(record, conf) {
								count++;
								if (count === journals.length) { // refresh after all journals were posted
									gridPanel.reload();
								}
								else {
									gridPanel.postSelectedJournalList(journals, count, gridPanel);
								}
							}
						});
						loader.load('accountingJournalPost.json?enableOpenSessionInView=true');
					},
					postSelectedJournals: function() {
						const panel = this;
						const grid = this.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to be posted.', 'No Row(s) Selected');
						}
						else {
							Ext.Msg.confirm('Post Selected Row(s)', 'Would you like to post selected row(s)?', function(a) {
								if (a === 'yes') {
									const journals = sm.getSelections();
									panel.postSelectedJournalList(journals, 0, panel);
								}
							});
						}
					}
				}]
			},


			{
				title: 'Unbooked Entries',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingUnbookedEntityList',
					additionalPropertiesToRequest: 'id|journalType.systemTable.detailScreenClass',
					forceLocalFiltering: true,
					instructions: 'Booking is the process of creating a journal from one of the subsystems (trading, m2m, simple journals, etc.). The following entries have not been booked yet.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 100},
						{header: 'Unbooked Entry', dataIndex: 'label', width: 250},
						{header: 'Created', dataIndex: 'createDate', width: 70}
					],
					editor: {
						deleteEnabled: false,
						getDetailPageClass: function(grid, row) {
							return row.json.journalType.systemTable.detailScreenClass;
						},
						addToolbarAddButton: function(toolBar) {
							const menu = new Ext.menu.Menu();
							menu.add({
								text: 'Book',
								iconCls: 'book-open-blue',
								scope: this,
								handler: function() {
									this.getGridPanel().bookSimpleJournal(false);
								}
							});
							menu.add({
								text: 'Book & Post',
								iconCls: 'book-red',
								scope: this,
								handler: function() {
									this.getGridPanel().bookSimpleJournal(true);
								}
							});
							toolBar.add({
								text: 'Book',
								iconCls: 'book-open-blue',
								tooltip: 'Book selected entry and, optionally, immediately post it to the General Ledger.',
								menu: menu
							});
							toolBar.add('-');
						}
					},
					bookSimpleJournal: function(postJournal) {
						const panel = this;
						const grid = this.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to be booked.', 'No Row(s) Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							Ext.Msg.confirm('Book Selected Row', 'Would you like to book selected row?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid,
										waitMsg: 'Booking...',
										params: {
											journalTypeName: sm.getSelected().json.journalType.name,
											sourceEntityId: sm.getSelected().id,
											postJournal: postJournal
										},
										timeout: 180000,
										onLoad: function(record, conf) {
											panel.reload();
										}
									});
									loader.load('accountingJournalBook.json');
								}
							});
						}
					}
				}]
			},


			{
				title: 'All Journals',
				reloadOnTabChange: true,
				items: [{
					xtype: 'gridpanel',
					name: 'accountingJournalListFind',
					instructions: 'This section lists all journals: posted and unposted. Journals that have Posting Date populated, have already been posted to the General Ledger.',
					rowSelectionModel: 'checkbox',
					columns: [
						{
							header: 'ID', dataIndex: 'id', type: 'int', width: 40,
							renderer: function(v, metaData, r) {
								const status = r.data['journalStatus.name'];
								if (status === 'Deleted') {
									metaData.css = 'ruleViolationBig';
									metaData.attr = 'qtip=\'This Journal was Unposted from the General Ledger\'';
								}
								else if (status === 'Modified') {
									metaData.css = 'ruleViolation';
									metaData.attr = 'qtip=\'This Journal was Re-Posted to the General Ledger with changes\'';
								}
								return Ext.util.Format.number(v, '0,000');
							}
						},
						{header: 'PID', dataIndex: 'parent.id', useNull: true, width: 40, type: 'int', doNotFormat: true},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalTypeId', loadAll: true, url: 'accountingJournalTypeList.json', showNotEquals: true}},
						{header: 'Journal Status', dataIndex: 'journalStatus.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalStatusId', loadAll: true, url: 'accountingJournalStatusList.json', showNotEquals: true}},
						{header: 'Journal Description', dataIndex: 'description', width: 250},
						{header: 'Source Table', dataIndex: 'systemTable.name', width: 70, hidden: true, filter: {type: 'combo', searchFieldName: 'systemTableId', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
						{header: 'Source FK Field ID', dataIndex: 'fkFieldId', useNull: true, width: 70, type: 'int', hidden: true},
						{header: 'Posting Date', width: 70, dataIndex: 'postingDate', defaultSortColumn: true, defaultSortDirection: 'DESC'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 7 days of trades
							this.setFilterValue('postingDate', {'after': new Date().add(Date.DAY, -7)});
						}
						return {
							enableOpenSessionInView: true
						};
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Journal Type', width: 180, xtype: 'toolbar-combo', url: 'accountingJournalTypeList.json', loadAll: true, linkedFilter: 'journalType.name'},
							{fieldLabel: 'Journal Status', width: 100, minListWidth: 100, xtype: 'toolbar-combo', url: 'accountingJournalStatusList.json', loadAll: true, linkedFilter: 'journalStatus.name'}
						];
					},
					editor: {
						detailPageClass: 'Clifton.accounting.journal.JournalWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Journal Types',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingJournalTypeList',
					instructions: 'Journal types define available types of journals. Subsystem journals are journals generated automatically by corresponding subsystems: trading, mark to market, etc.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Type Name', dataIndex: 'name', width: 100},
						{header: 'Description', dataIndex: 'description', width: 250, hidden: true},
						{header: 'Subsystem Table', dataIndex: 'systemTable.name', width: 70, filter: {type: 'combo', searchFieldName: 'systemTableId', url: 'systemTableListFind.json?defaultDataSource=true'}},
						{header: 'Transaction Table', dataIndex: 'transactionTable.name', width: 70, filter: {type: 'combo', searchFieldName: 'transactionTableId', url: 'systemTableListFind.json?defaultDataSource=true'}},
						{header: 'Max Count', dataIndex: 'maxSubsystemJournalCount', width: 30, type: 'int', useNull: true, tooltip: 'Maximum number of journals allowed for a single sub-system entity'},
						{header: 'Subsystem', dataIndex: 'subsystemJournal', width: 30, type: 'boolean'},
						{header: 'Archivable', dataIndex: 'archiveAllowed', width: 30, type: 'boolean', tooltip: 'If checked, journals of this journal type can be archived when booked.'},
						{header: 'Same User Unbooking', dataIndex: 'sameUserAllowedToUnbook', width: 50, type: 'boolean', tooltip: 'If checked, journals of this type can be unbooked by someone without DELETE permissions to the "AccountingJournal" resource if they were the one that created the entry.'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.journal.JournalTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Journal Status',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingJournalStatusList',
					instructions: 'Journal status define available statuses of journals.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Status Name', dataIndex: 'name', width: 70},
						{header: 'Description', dataIndex: 'description', width: 300},
						{header: 'Posted', dataIndex: 'posted', width: 30, type: 'boolean'},
						{header: 'Posting Allowed', dataIndex: 'postingAllowed', width: 50, type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.journal.JournalStatusWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Journal Actions',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingJournalActionListFind',
					instructions: 'A list of all accounting journal actions defined in the system.  Actions are executed immediately after posting/unposting to the General Ledger.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Action Name', dataIndex: 'name', width: 100},
						{header: 'Description', dataIndex: 'description', width: 300, hidden: true},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 100, filter: {type: 'combo', searchFieldName: 'journalTypeId', loadAll: true, url: 'accountingJournalTypeList.json', showNotEquals: true}},
						{header: 'Scope Condition', dataIndex: 'scopeSystemCondition.name', width: 100, filter: {type: 'combo', searchFieldName: 'scopeSystemConditionId', url: 'systemConditionListFind.json'}},
						{header: 'Action Bean', dataIndex: 'actionBean.name', width: 100, filter: {type: 'combo', searchFieldName: 'actionBeanId', url: 'systemBeanListFind.json?groupName=Accounting Journal Action Processor'}},
						{header: 'Processing Order', dataIndex: 'processingOrder', width: 40, type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.accounting.journal.JournalActionWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
