// not the actual window but a window selector:
//   - editable journal if unposted and non subsystem type
//   - read only journal otherwise

Clifton.accounting.journal.JournalWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'accountingJournal.json',
	// limit data returned to only what's used because journals with 100+ details can results in megabytes of data
	requestedProperties: 'id|parent.id|fkFieldId|description|postingDate|createUserId|createDate|updateUserId|updateDate|journalStatus.id|journalStatus.name|journalType.id|journalType.name|journalType.subsystemJournal|journalType.systemTable.detailScreenClass|journalDetailList.id|journalDetailList.parentTransaction.id|journalDetailList.parentDefinition.id|journalDetailList.opening|journalDetailList.deleted|journalDetailList.modified|journalDetailList.clientInvestmentAccount.id|journalDetailList.clientInvestmentAccount.label|journalDetailList.holdingInvestmentAccount.id|journalDetailList.holdingInvestmentAccount.label|journalDetailList.executingCompany.id|journalDetailList.executingCompany.name|journalDetailList.accountingAccount.id|journalDetailList.accountingAccount.name|journalDetailList.investmentSecurity.id|journalDetailList.investmentSecurity.symbol|journalDetailList.price|journalDetailList.quantity|journalDetailList.localDebit|journalDetailList.localCredit|journalDetailList.description|journalDetailList.exchangeRateToBase|journalDetailList.baseDebit|journalDetailList.baseCredit|journalDetailList.positionCostBasis|journalDetailList.transactionDate|journalDetailList.originalTransactionDate|journalDetailList.settlementDate|journalDetailList.systemTable.name|journalDetailList.fkFieldId',
	requestedPropertiesRoot: 'data',

	getClassName: function(config, entity) {
		let className = 'Clifton.accounting.journal.ReadOnlyJournalWindow';
		if ((entity && !entity.postingDate && entity.journalType.subsystemJournal === false) || (!entity && config.defaultData.journalType.subsystemJournal === false)) {
			className = 'Clifton.accounting.journal.EditableJournalWindow';
		}
		return className;
	}
});
