Clifton.accounting.journal.JournalTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Journal Type',
	iconCls: 'book-open-blue',
	width: 700,
	height: 450,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',

				items: [{
					xtype: 'formpanel',
					instructions: 'Journal types define available types of journals. Subsystem journals are journals generated automatically by corresponding subsystems: trading, mark to market, etc.',
					url: 'accountingJournalType.json',
					readOnly: true,

					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Subsystem Table', name: 'systemTable.name', xtype: 'linkfield', detailIdField: 'systemTable.id', detailPageClass: 'Clifton.system.schema.TableWindow', qtip: 'The main table that this journal type is associated with: AccountingJournal when subsystemJournal = false, AccountingSimpleJournal, etc.'},
						{fieldLabel: 'Transaction Table', name: 'transactionTable.name', xtype: 'linkfield', detailIdField: 'transactionTable.id', detailPageClass: 'Clifton.system.schema.TableWindow', qtip: 'The table that each transaction of this journal type are associated with. Usually, it\'s the same as systemTable. However, it might also be a different table: AccountingPositionTransferDetails vs AccountingPositionTransfer, AccountingEventJournalDetail vs AccountingEventJournal.'},
						{fieldLabel: 'Max Count', name: 'maxSubsystemJournalCount', qtip: 'Optional maximum number of journal that are allowed for the same sub-system entity. This can be used to disallow multiple adjustments of the same type: one should have all values.'},
						{boxLabel: 'This journal was automatically generated by a subsystem', name: 'subsystemJournal', xtype: 'checkbox'},
						{boxLabel: 'This journal can be archived when booked.', name: 'archiveAllowed', xtype: 'checkbox', qtip: 'If checked, journals of this journal type can be archived when booked.'},
						{boxLabel: 'This journal can be unbooked by the user that created it.', name: 'sameUserAllowedToUnbook', xtype: 'checkbox', qtip: 'If checked, journals of this type can be unbooked by someone without DELETE permissions to the "AccountingJournal" resource if they were the one that created the entry.'}
					]
				}]
			},


			{
				title: 'Actions',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingJournalActionListFind',
					instructions: 'A list of accounting journal actions for selected journal type.  Actions are executed immediately after posting/unposting to the General Ledger.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Action Name', dataIndex: 'name', width: 100},
						{header: 'Description', dataIndex: 'description', width: 300, hidden: true},
						{header: 'Scope Condition', dataIndex: 'scopeSystemCondition.name', width: 100, filter: {type: 'combo', searchFieldName: 'scopeSystemConditionId', url: 'systemConditionListFind.json'}},
						{header: 'Action Bean', dataIndex: 'actionBean.name', width: 100, filter: {type: 'combo', searchFieldName: 'actionBeanId', url: 'systemBeanListFind.json?groupName=Accounting Journal Action Processor'}, hidden: true},
						{header: 'Order', dataIndex: 'processingOrder', width: 40, type: 'int'}
					],
					getLoadParams: function(firstLoad) {
						return {journalTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.accounting.journal.JournalActionWindow',
						getDefaultData: function(gridPanel) {
							return {
								journalType: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			}
		]
	}]
});
