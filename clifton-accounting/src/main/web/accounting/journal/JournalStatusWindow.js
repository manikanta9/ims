Clifton.accounting.journal.JournalStatusWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Journal Status',
	iconCls: 'book-open-blue',

	items: [{
		xtype: 'formpanel',
		instructions: 'Journal status define available statuses of journals.',
		url: 'accountingJournalStatus.json',
		labelWidth: 100,
		readOnly: true,

		items: [
			{fieldLabel: 'Status Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'Journals in this Status have already been posted to the General Ledger', name: 'posted', xtype: 'checkbox'},
			{boxLabel: 'Journals in this Status can be posted to the General Ledger', name: 'postingAllowed', xtype: 'checkbox'}
		]
	}]
});
