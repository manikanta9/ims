Clifton.accounting.journal.JournalActionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Accounting Journal Action',
	iconCls: 'run',

	items: [{
		xtype: 'formpanel',
		instructions: 'A list of all accounting journal actions defined in the system.  Actions are executed immediately after posting/unposting to the General Ledger.',
		url: 'accountingJournalAction.json',

		items: [
			{fieldLabel: 'Action Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Journal Type', name: 'journalType.name', xtype: 'linkfield', detailIdField: 'journalType.id', detailPageClass: 'Clifton.accounting.journal.JournalTypeWindow'},
			{
				fieldLabel: 'Scope Condition', name: 'scopeSystemCondition.name', hiddenName: 'scopeSystemCondition.id', xtype: 'system-condition-combo',
				qtip: 'Optionally limits this action to a sub-set of journals of this type that have this condition evaluate to true. For example, one can limit actions to a specific trade or transfer type, specific service, security type, etc.'
			},
			{
				fieldLabel: 'Action Bean', name: 'actionBean.name', hiddenName: 'actionBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Accounting Journal Action Processor', detailPageClass: 'Clifton.system.bean.BeanWindow',
				qtip: 'When the journal type and scope conditions are met, {@link AccountingJournalActionProcessor} will be processed or rolled back for the journal/entity being booked/unbooked.'
			},
			{fieldLabel: 'Processing Order', name: 'processingOrder', xtype: 'spinnerfield', boxLabel: 'When multiple actions are defined for the specified journal type, defines the order of execution.'}
		]
	}]
});
