Clifton.accounting.journal.EditableJournalWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Journal',
	iconCls: 'book-open-blue',
	width: 1200,
	height: 545,

	items: [{
		xtype: 'formpanel',
		url: 'accountingJournal.json',
		getSaveURL: function() {
			// avoid getting journal which is lazy
			return 'accountingJournalSave.json?requestedPropertiesRoot=data.journalDetailList&requestedProperties=id|opening|parentTransaction.id|parentDefinition.id|clientInvestmentAccount|holdingInvestmentAccount|accountingAccount|investmentSecurity|price|quantity|localDebit|localCredit|description|exchangeRateToBase|baseDebit|baseCredit|positionCostBasis|transactionDate|originalTransactionDate|settlementDate';
		},
		labelFieldName: 'id',
		items: [
			{
				xtype: 'panel',
				layout: 'column',
				items: [
					{
						columnWidth: .30,
						layout: 'form',
						items: [
							{fieldLabel: 'Journal ID', name: 'id', xtype: 'displayfield'}
						]
					},
					{
						columnWidth: .35,
						layout: 'form',
						items: [
							{fieldLabel: 'Journal Type', name: 'journalType.name', xtype: 'linkfield', detailIdField: 'journalType.id', detailPageClass: 'Clifton.accounting.journal.JournalTypeWindow'}
						]
					},
					{
						columnWidth: .35,
						layout: 'form',
						items: [
							{fieldLabel: 'Journal Status', name: 'journalStatus.name', xtype: 'linkfield', detailIdField: 'journalStatus.id', detailPageClass: 'Clifton.accounting.journal.JournalStatusWindow'}
						]
					}]
			},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
			{
				xtype: 'formgrid-scroll',
				storeRoot: 'journalDetailList',
				dtoClass: 'com.clifton.accounting.gl.journal.AccountingJournalDetail',
				alwaysSubmitFields: ['opening'],
				doNotSubmitFields: ['localDebit', 'localCredit', 'baseDebit', 'baseCredit'],
				height: 370,
				heightResized: true,
				columnsConfig: [
					{header: 'ID', dataIndex: 'id', width: 70, useNull: true, hidden: true},
					{header: 'PID', dataIndex: 'parentTransaction.id', useNull: true, width: 65, editor: {xtype: 'integerfield'}, tooltip: 'Parent Accounting Transaction ID: existing General Ledger transaction'},
					{header: 'PD ID', dataIndex: 'parentDefinition.id', useNull: true, width: 65, editor: {xtype: 'integerfield'}, hidden: true, tooltip: 'Parent Journal Detail ID: ID of another entry in this Journal'},
					{header: 'Opening', dataIndex: 'opening', width: 65, type: 'boolean', editor: {xtype: 'checkbox'}, tooltip: 'Indicates whether this is an opening or closing transaction. This is mostly applicable to Position GL Accounts.'},
					{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', width: 180, editor: {xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', allowBlank: false}},
					{
						header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', width: 180,
						editor: {
							xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=false', displayField: 'label', allowBlank: false,
							beforequery: function(queryEvent) {
								// limit holding accounts to related accounts
								const record = queryEvent.combo.gridEditor.record;
								queryEvent.combo.store.baseParams = {mainAccountId: record.get('clientInvestmentAccount.id')};
							}
						}
					},
					{header: 'Executing Company', dataIndex: 'executingCompany.name', idDataIndex: 'executingCompany.id', width: 130, editor: {xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', hidden: true}},
					{header: 'GL Account', dataIndex: 'accountingAccount.name', idDataIndex: 'accountingAccount.id', width: 100, editor: {xtype: 'combo', url: 'accountingAccountListFind.json?postingNotAllowed=false', displayField: 'label', allowBlank: false}},
					{header: 'Security', dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', width: 70, editor: {xtype: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label', allowBlank: false}},
					{header: 'Price', dataIndex: 'price', type: 'float', width: 80, useNull: true, negativeInRed: true, editor: {xtype: 'floatfield'}},
					{header: 'Qty', dataIndex: 'quantity', type: 'float', width: 70, useNull: true, negativeInRed: true, summaryType: 'sum', editor: {xtype: 'floatfield'}},
					{header: 'Local Debit', dataIndex: 'localDebit', type: 'currency', width: 90, summaryType: 'sum', editor: {xtype: 'currencyfield'}},
					{header: 'Local Credit', dataIndex: 'localCredit', type: 'currency', width: 90, summaryType: 'sum', editor: {xtype: 'currencyfield'}},
					{header: 'Description', dataIndex: 'description', width: 250, editor: {xtype: 'textfield'}},
					{header: 'FX Rate', dataIndex: 'exchangeRateToBase', type: 'float', width: 80, editor: {xtype: 'floatfield'}},
					{header: 'Base Debit', dataIndex: 'baseDebit', type: 'currency', width: 90, summaryType: 'sum'},
					{header: 'Base Credit', dataIndex: 'baseCredit', type: 'currency', width: 90, summaryType: 'sum'},
					{header: 'Cost Basis', dataIndex: 'positionCostBasis', type: 'currency', width: 90, summaryType: 'sum', editor: {xtype: 'currencyfield'}},
					{header: 'Tran Date', dataIndex: 'transactionDate', width: 85, editor: {xtype: 'datefield'}},
					{header: 'Original Date', dataIndex: 'originalTransactionDate', width: 80, editor: {xtype: 'datefield'}},
					{header: 'Settlement Date', dataIndex: 'settlementDate', width: 85, editor: {xtype: 'datefield'}}
				],
				plugins: {ptype: 'gridsummary'},
				getNewRowDefaults: function() {
					return {exchangeRateToBase: 1, opening: true};
				},
				// submit the difference between debit and credit only
				onAfterUpdateFieldValue: function(editor, field) {
					const f = editor.field;
					if (f === 'localDebit' || f === 'localCredit') {
						let debitValue;
						let creditValue;
						if (f === 'localDebit') {
							debitValue = editor.value;
							creditValue = editor.record.get('localCredit');
						}
						else {
							creditValue = editor.value;
							debitValue = editor.record.get('localDebit');
						}
						if (!Ext.isNumber(debitValue)) {
							debitValue = 0;
						}
						if (!Ext.isNumber(creditValue)) {
							creditValue = 0;
						}
						editor.record.set('localDebitCredit', debitValue - creditValue);
						editor.record.set('baseDebitCredit', ''); // force server-side recalculation
					}
					if (f === 'exchangeRateToBase') {
						editor.record.set('baseDebitCredit', ''); // force server-side recalculation
					}
				},
				listeners: {
					// drill into parent journal
					'cellclick': function(grid, rowIndex, cellIndex, evt) {
						if (cellIndex === 0) {
							const row = grid.store.data.items[rowIndex];
							const pid = row.data['parentTransaction.id'];
							if (TCG.isNotBlank(pid)) {
								const id = TCG.data.getData('accountingTransaction.json?id=' + pid, grid).journal.id;
								TCG.createComponent('Clifton.accounting.journal.JournalWindow', {
									id: TCG.getComponentId('Clifton.accounting.journal.JournalWindow', id),
									params: {id: id},
									openerCt: grid
								});
							}
						}
					}
				}
			}
		]
	}]
});
