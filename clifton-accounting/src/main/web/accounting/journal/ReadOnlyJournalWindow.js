Clifton.accounting.journal.ReadOnlyJournalWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Journal',
	iconCls: 'book-open-blue',
	width: 1200,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Journal',
				tbar: [{
					text: 'Unpost Journal',
					tooltip: 'Remove this journal from GL, delete it, and mark corresponding source entity as un-booked.',
					iconCls: 'undo',
					handler: function() {
						const fp = this.ownerCt.ownerCt.items.get(0);
						const w = fp.getWindow();
						Ext.Msg.confirm('Unpost Journal', 'Would you like to unpost and unbook this journal?', function(a) {
							if (a === 'yes') {
								const loader = new TCG.data.JsonLoader({
									waitTarget: w,
									params: {
										journalId: w.getMainFormId()
									},
									onLoad: function(record, conf) {
										TCG.showInfo('The journal was successfully unposted');
										w.closeWindow();
									}
								});
								loader.load('accountingJournalUnpost.json');
							}
						});
					}
				}, '-'],

				items: [{
					xtype: 'formpanel',
					url: 'accountingJournal.json',
					labelFieldName: 'id',
					getWarningMessage: function(form) {
						const status = TCG.getValue('journalStatus.name', form.formValues);
						if (status === 'Booked') {
							return 'This journal was booked but has not been posted to the General Ledger yet. Post the journal to see its effects on reporting and other parts of the system.';
						}
						else if (status === 'Deleted') {
							return 'This journal was unposted and is no longer valid in the General Ledger (preserved for historic analysis only).';
						}
						else if (status === 'Modified') {
							return 'This journal was unposted and then re-posted with changes. See source entity Audit Trail for more details.';
						}
						return undefined;
					},
					items: [
						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .35,
									layout: 'form',
									items: [
										{fieldLabel: 'Journal ID', name: 'id', xtype: 'displayfield'},
										{fieldLabel: 'Parent Journal', name: 'parent.id', xtype: 'linkfield', detailIdField: 'parent.id', detailPageClass: 'Clifton.accounting.journal.JournalWindow'}
									]
								},
								{
									columnWidth: .35,
									layout: 'form',
									items: [
										{fieldLabel: 'Journal Type', name: 'journalType.name', xtype: 'linkfield', detailIdField: 'journalType.id', detailPageClass: 'Clifton.accounting.journal.JournalTypeWindow'},
										{fieldLabel: 'Journal Status', name: 'journalStatus.name', xtype: 'linkfield', detailIdField: 'journalStatus.id', detailPageClass: 'Clifton.accounting.journal.JournalStatusWindow'}
									]
								},
								{
									columnWidth: .30,
									layout: 'form',
									labelWidth: 125,
									items: [
										{fieldLabel: 'Posting Date', name: 'postingDate', xtype: 'displayfield', type: 'date'},
										{fieldLabel: 'Source Table', name: 'systemTable.name', xtype: 'displayfield'},
										{
											fieldLabel: 'Source FK Field ID', name: 'fkFieldId', xtype: 'linkfield', detailIdField: 'fkFieldId',
											getDetailPageClass: function(fp) {
												return TCG.getValue('journalType.systemTable.detailScreenClass', fp.getForm().formValues);
											}
										}
									]
								}
							]
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50, readOnly: true},
						{
							xtype: 'formgrid-scroll',
							storeRoot: 'journalDetailList',
							readOnly: true,
							height: 300,
							heightResized: true,
							detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
							columnsConfig: [
								{header: 'ID', dataIndex: 'id', width: 70, hidden: true},
								{header: 'PID', dataIndex: 'parentTransaction.id', useNull: true, width: 65, type: 'int', doNotFormat: true},
								{header: 'Opening', dataIndex: 'opening', width: 65, type: 'boolean', tooltip: 'Indicates whether this is an opening or closing transaction. This is mostly applicable to Position GL Accounts.'},
								{header: 'Deleted', dataIndex: 'deleted', width: 65, type: 'boolean', hidden: true, tooltip: 'When checked, indicates that the journal with this transaction was unposted from the General Ledger. This transaction is ignored by processing logic and is used only to preserve history.'},
								{header: 'Modified', dataIndex: 'modified', width: 65, type: 'boolean', hidden: true, tooltip: 'When checked, indicates that the journal with this transaction was unposted and then re-posted for the source entity. After reposting, something was updated/modified.'},
								{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 180},
								{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', width: 180},
								{header: 'Executing Company', dataIndex: 'executingCompany.name', width: 130, hidden: true},
								{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 100},
								{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 70},
								{header: 'Price', dataIndex: 'price', type: 'float', useNull: true, negativeInRed: true, width: 80},
								{header: 'Qty', dataIndex: 'quantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 70, summaryType: 'sum'},
								{header: 'Local Debit', dataIndex: 'localDebit', type: 'currency', width: 90, summaryType: 'sum'},
								{header: 'Local Credit', dataIndex: 'localCredit', type: 'currency', width: 90, summaryType: 'sum'},
								{header: 'Description', dataIndex: 'description', width: 250},
								{header: 'FX Rate', dataIndex: 'exchangeRateToBase', type: 'float', width: 100},
								{header: 'Base Debit', dataIndex: 'baseDebit', type: 'currency', width: 90, summaryType: 'sum'},
								{header: 'Base Credit', dataIndex: 'baseCredit', type: 'currency', width: 90, summaryType: 'sum'},
								{header: 'Cost Basis', dataIndex: 'positionCostBasis', type: 'currency', width: 90, summaryType: 'sum', negativeInRed: true, positiveInGreen: true},
								{header: 'Tran Date', dataIndex: 'transactionDate', width: 80},
								{header: 'Original Date', dataIndex: 'originalTransactionDate', width: 80},
								{header: 'Settlement Date', dataIndex: 'settlementDate', width: 90},
								{header: 'FK Table', dataIndex: 'systemTable.name', width: 80, hidden: true},
								{header: 'FK Field ID', dataIndex: 'fkFieldId', width: 80, type: 'int', hidden: true}
							],
							plugins: {ptype: 'gridsummary'},
							listeners: {
								// drill into parent journal
								cellclick: function(grid, rowIndex, cellIndex, evt) {
									if (cellIndex === 0) {
										const row = grid.store.data.items[rowIndex];
										const pid = row.data['parentTransaction.id'];
										if (TCG.isNotBlank(pid)) {
											const id = TCG.data.getData('accountingTransaction.json?id=' + pid, grid).journal.id;
											TCG.createComponent('Clifton.accounting.journal.JournalWindow', {
												id: TCG.getComponentId('Clifton.accounting.journal.JournalWindow', id),
												params: {id: id},
												openerCt: grid
											});
										}
									}
								}
							}
						}
					]
				}]
			},


			{
				title: 'Child Journal Details',
				items: [{
					xtype: 'accounting-transactionGrid',
					instructions: 'The following accounting transaction have selected journal details as parent entries.',
					includeClientAccountFilter: false,
					columnOverrides: [
						{dataIndex: 'parentTransaction.id', hidden: true}
					],
					getCustomFilters: function(firstLoad) {
						return {parentJournalId: this.getWindow().getMainFormId()};
					},
					plugins: [
						{ptype: 'accounting-transaction-grideditor'},
						{ptype: 'investment-contextmenu-plugin', enableAsyncLookups: true},
						{ptype: 'gridsummary'}
					]
				}]
			},


			{
				title: 'Related Journals',
				items: [{
					xtype: 'gridpanel',
					name: 'accountingJournalListFind',
					instructions: 'The following journals are associated with the same source entity (Trade, Event Journal, etc.) that this journal is linked to. It also includes Deleted journals if any.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 40},
						{header: 'PID', dataIndex: 'parent.id', useNull: true, width: 40, type: 'int', doNotFormat: true},
						{header: 'Journal Type', dataIndex: 'journalType.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalTypeId', loadAll: true, url: 'accountingJournalTypeList.json', showNotEquals: true}},
						{header: 'Journal Status', dataIndex: 'journalStatus.name', width: 80, filter: {type: 'combo', searchFieldName: 'journalStatusId', loadAll: true, url: 'accountingJournalStatusList.json', showNotEquals: true}},
						{header: 'Journal Description', dataIndex: 'description', width: 250},
						{header: 'Source Table', dataIndex: 'systemTable.name', width: 70, hidden: true, filter: {type: 'combo', searchFieldName: 'systemTableId', url: 'systemTableListFind.json?defaultDataSource=true'}},
						{header: 'Source FK Field ID', dataIndex: 'fkFieldId', useNull: true, width: 70, type: 'int', hidden: true},
						{header: 'Posting Date', width: 70, dataIndex: 'postingDate', defaultSortColumn: true, defaultSortDirection: 'DESC'}
					],
					getLoadParams: function(firstLoad) {
						const fp = this.getWindow().getMainFormPanel();
						const fkFieldId = fp.getFormValue('fkFieldId');
						if (fkFieldId) {
							return {
								fkFieldId: fkFieldId,
								systemTableId: fp.getFormValue('systemTable.id')
							};
						}
						return false;
					},
					editor: {
						detailPageClass: 'Clifton.accounting.journal.JournalWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Archived Entities',
				items: [
					{
						xtype: 'archive-entity-gridpanel',
						name: 'accountingJournalArchiveEntityDescriptorList.json',
						useArchiveEntityDescriptor: true,
						includeTopToolbarDefinitionFilter: false,

						additionalAddToolbarButtons: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Compare Entity With Live Preview',
								tooltip: 'Compare the entity JSON between an archived entity and a live JSON preview of the journal',
								iconCls: 'diff',
								scope: gridPanel,
								handler: function() {
									const selectionModel = this.grid.getSelectionModel();
									if (selectionModel.getCount() < 1) {
										TCG.showError('Please select one row to compare.', 'No Row Selected');
									}
									else if (selectionModel.getCount() > 1) {
										TCG.showError('Only one row can be selected. Please select only one row.', 'NOT SUPPORTED');
									}
									else {
										const row1 = selectionModel.getSelections()[0].json;
										const entityJsonPromise1 = TCG.data.getDataPromise('archiveEntity.json?requestedPropertiesRoot=data&requestedProperties=entityJson', this, {params: {id: row1.id}})
												.then(function(entity) {
													if (entity) {
														return entity.entityJson;
													}
												}),
											realTimeEntityJsonPromise = TCG.data.getDataPromise('accountingJournalArchiveEntityDescriptorPreview.json?requestedPropertiesRoot=data&requestedProperties=entityJson', this, {params: {id: this.getWindow().getMainFormId()}})
												.then(function(entity) {
													if (entity) {
														return entity.entityJson;
													}
												});
										Promise.all([entityJsonPromise1, realTimeEntityJsonPromise])
											.then(function(values) {
												Clifton.archive.ShowJsonDifference(values[0], values[1]);
											});
									}
								}
							}, '-');
							toolBar.add({
								text: 'Live Preview',
								tooltip: 'Get a live JSON preview of the journal as if it were being archived now',
								iconCls: 'diff',
								scope: gridPanel,
								handler: function() {
									TCG.data.getDataPromise('accountingJournalArchiveEntityDescriptorPreview.json?requestedPropertiesToExcludeGlobally=entityTemplate', this, {params: {id: this.getWindow().getMainFormId()}})
										.then(function(entity) {
											if (entity) {
												TCG.createComponent(gridPanel.editor.detailPageClass, {
													defaultData: entity,
													defaultDataIsReal: true,
													useArchiveEntityDescriptor: true
												});
											}
										});
								}
							}, '-');
						},

						getLoadParams: function(firstLoad) {
							return {
								id: this.getWindow().getMainFormId()
							};
						}
					}
				]
			},


			{
				title: 'Journal Life Cycle',
				items: [{
					xtype: 'system-lifecycle-grid',
					tableName: 'AccountingJournal'
				}]
			}
		]
	}]
});
