import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant


val sharedVariant = registerVariant("shared", upstreamForMain = true)

val jmhSourceSet = cliftonJmh.enable()

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	sharedVariant.api(project(":clifton-investment")) { usingVariant("shared") }

	api(project(":clifton-marketdata"))
	api(project(":clifton-report"))
	api(project(":clifton-archive")) { usingVariant("client") }
	api(project(":clifton-portal")) { usingVariant("api") }

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-marketdata")))
	testFixturesApi(testFixtures(project(":clifton-report")))
	testFixturesApi(testFixtures(project(":clifton-investment")))

	jmhSourceSet.implementationConfigurationName(project(":clifton-core")) { usingVariant("jmh") }
}
