import com.clifton.gradle.plugin.build.usingVariant

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// Used by Crowd
	runtimeOnly("org.codehaus.xfire:xfire-aegis:1.2.6") {
		exclude(module = "XmlSchema")
		exclude(module = "jaxen")
		exclude(module = "junit")
	}

	// WebJars
	// implementation("org.webjars:extjs:3.3.0") // TODO: Switch to official webjars distribution
	implementation("sencha:extjs:3.3.0:clifton-webjar")
	implementation("sencha:extjs-printer:1.0.0:webjar")
	implementation("org.webjars.npm:jsoneditor:5.5.7")
	implementation("org.webjars:google-diff-match-patch:20121119-1")

	implementation("org.springframework.boot:spring-boot-starter-actuator") {
		exclude(module = "spring-boot-starter-logging")
	}


	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	javascript(project(":clifton-core"))
	javascript(project(":clifton-core:clifton-core-messaging"))
	javascript(project(":clifton-security"))
	javascript(project(":clifton-system"))
	javascript(project(":clifton-system:clifton-system-search"))
	javascript(project(":clifton-system:clifton-system-statistic"))
	javascript(project(":clifton-system:clifton-system-note"))
	javascript(project(":clifton-system:clifton-system-navigation"))
	javascript(project(":clifton-system:clifton-system-priority"))
	javascript(project(":clifton-system:clifton-system-query"))
	javascript(project(":clifton-calendar"))
	javascript(project(":clifton-calendar:clifton-calendar-schedule"))
	javascript(project(":clifton-batch"))
	javascript(project(":clifton-document"))
	javascript(project(":clifton-workflow"))
	javascript(project(":clifton-notification"))
	javascript(project(":clifton-websocket"))
	javascript(project(":clifton-integration"))
	javascript(project(":clifton-reconciliation"))

	implementation(project(":clifton-core"))
	implementation(project(":clifton-application"))
	implementation(project(":clifton-core:clifton-core-messaging"))
	implementation(project(":clifton-security"))
	implementation(project(":clifton-system"))
	implementation(project(":clifton-system:clifton-system-search"))
	implementation(project(":clifton-system:clifton-system-statistic"))
	implementation(project(":clifton-system:clifton-system-note"))
	implementation(project(":clifton-system:clifton-system-navigation"))
	implementation(project(":clifton-system:clifton-system-priority"))
	implementation(project(":clifton-system:clifton-system-query"))
	implementation(project(":clifton-calendar"))
	implementation(project(":clifton-calendar:clifton-calendar-schedule"))
	implementation(project(":clifton-batch"))
	implementation(project(":clifton-document"))
	implementation(project(":clifton-workflow"))
	implementation(project(":clifton-notification"))
	implementation(project(":clifton-websocket"))
	implementation(project(":clifton-integration")) { usingVariant("api") }
	implementation(project(":clifton-reconciliation"))


	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////


	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-application")))
	testFixturesApi(testFixtures(project(":clifton-calendar:clifton-calendar-schedule")))
}

cliftonSpringBoot {
	mainClass.set("com.clifton.reconciliation.ReconciliationApplication")
}
