<#-- @ftlvariable name="applicationContext" type="org.springframework.context.ApplicationContext" -->
<#-- @ftlvariable name="handler" type="com.clifton.reconciliation.handler.ReconciliationHealthStatusHandler" -->
<#-- @ftlvariable name="request" type="javax.servlet.http.HttpServletRequest" -->
<#assign handler = applicationContext.getBean("reconciliationHealthStatusHandler")>
<#assign parameters = handler.parseParameters(request)>
<body style="FONT-FAMILY: arial,serif; FONT-SIZE: 12px;">
${handler.impersonateUserPage()}
<b>SYSTEM INFORMATION</b>
<br/>Environment = ${handler.getEnvironment()}
<br/>
<br/><b>PAGE PARAMETERS</b>
<br/>maxTimeAllowed = ${(parameters.getMaxTimeAllowed())!"null"}
<br/>nameOfTestToRun = ${(parameters.getTestToRun())!"null"}
<br/>enableTimeIntervals = ${parameters.isEnableIntervals()?c}
<br/>
<br/><b>TESTS</b><br/>
<#assign initTime = .now?long>
<!-- Database maintenance tasks window: 11:00 PM - 12:00 AM -->
${handler.executeDbTest("CliftonReconciliation DB", parameters, 500, ["11:00 PM - 12:00 AM"])}
${handler.executeOAuthTest("Integration OAuth Status", parameters, 3000, [])}

<#assign elapsedTime = .now?long - initTime >
<br/><b>PROCESSING TIME: ${elapsedTime} milliseconds</b>
</body>
