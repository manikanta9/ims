-- ==============================================================
-- ==============================================================
-- Test Users Begin
-- The following users can be used to log into the system.  If a group with the given group name (separated by |) exists, then that test user will be assigned to that group - note that the group is assumed to exist and will not be created
-- ==============================================================
-- ==============================================================
DECLARE @TestUsers TABLE (
	UserName    NVARCHAR(50),
	DisplayName NVARCHAR(50),
	GroupName   NVARCHAR(500)
)
INSERT INTO @TestUsers
	-- PW: password1
SELECT 'imstestuser1', 'Test Admin User', 'Administrators|'


INSERT INTO SecurityUser (SecurityUserName, IntegrationPseudonym, DisplayName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT x.UserName, x.UserName, x.DisplayName, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
WHERE NOT EXISTS(SELECT * FROM SecurityUser u WHERE u.SecurityUserName = x.UserName)


INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT u.SecurityUserID, g.SecurityGroupID, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
	 INNER JOIN SecurityUser u ON x.UserName = u.SecurityUserName
	 INNER JOIN SecurityGroup g ON x.GroupName LIKE '%' + g.SecurityGroupName + '|%'
WHERE NOT EXISTS(SELECT * FROM SecurityUserGroup ug WHERE u.SecurityUserID = ug.SecurityUserID AND g.SecurityGroupID = ug.SecurityGroupID)

-- ==============================================================
-- ==============================================================
-- Test Users End
-- ==============================================================
-- ==============================================================


UPDATE SystemDataSource
SET DatabaseName  = '@dataSource.databaseName@',
	ConnectionUrl = '@dataSource.url@'
WHERE DataSourceName = 'dataSource';

UPDATE WorkflowState
SET WorkflowStatusID = (SELECT WorkflowStatusID FROM WorkflowStatus WHERE WorkflowStatusName = 'Active')
WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Reconciliation Definition Workflow');

/*
I really don't like that this isn't normalized and that I have to update the status on the DTO in addition to on the state...
We should not be adding the status to the DTO, just the state; was done for performance apparently (potential large impact on trade/trade workflow)

I made a note:
I'm leaving for now; but made a note; one thought would be that we should use the normalized whenever possible,
but in the case of large items like trade, could add the status to the Trade Object and override the getWorkflowStatus
method on the 'BasedNamedWorkflowAwareEntity' to return the local status rather than the derived status from the state;
this would allow normalization in the majority of cases and force explicit override when performance is critical
(assuming that the hit is large which I will look at sometime)
*/
UPDATE ReconciliationDefinition
SET WorkflowStatusID = (SELECT WorkflowStatusID FROM WorkflowStatus WHERE WorkflowStatusName = 'Active')
WHERE WorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Reconciliation Definition Workflow'));
