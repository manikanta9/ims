package com.clifton.reconciliation;

import com.clifton.application.service.ApplicationServiceRunner;
import com.clifton.core.context.spring.AnnotationUsingOurConventionBeanNameGenerator;
import com.clifton.core.context.spring.DelimitedXmlBeanDefinitionReader;
import com.clifton.core.context.spring.SpringLoadTimeWeavingConfiguration;
import org.springframework.boot.actuate.autoconfigure.context.ShutdownEndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.endpoint.EndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.endpoint.jmx.JmxEndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.servlet.WebMvcEndpointManagementContextConfiguration;
import org.springframework.boot.actuate.autoconfigure.health.HealthContributorAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.health.HealthEndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.info.InfoEndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.management.ThreadDumpEndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.web.mappings.MappingsEndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.web.server.ManagementContextAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration;
import org.springframework.boot.autoconfigure.context.ConfigurationPropertiesAutoConfiguration;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.boot.autoconfigure.info.ProjectInfoAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.web.embedded.EmbeddedWebServerFactoryCustomizerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;


/**
 * Entry point of the Spring Boot Application
 * The @SpringBootApplication annotation is given a dummy argument to override/disable the default @ComponentScan.
 *
 * @author jacke
 */
@SuppressWarnings("SpringComponentScan")
@PropertySource("classpath:/META-INF/app-clifton-reconciliation.properties")
@ImportAutoConfiguration({
		ConfigurationPropertiesAutoConfiguration.class,
		DispatcherServletAutoConfiguration.class,
		EmbeddedWebServerFactoryCustomizerAutoConfiguration.class,
		ProjectInfoAutoConfiguration.class,
		PropertyPlaceholderAutoConfiguration.class,
		ServletWebServerFactoryAutoConfiguration.class,
		WebMvcAutoConfiguration.class,
		FreeMarkerAutoConfiguration.class,
		WebEndpointAutoConfiguration.class,
		ManagementContextAutoConfiguration.class,
		EndpointAutoConfiguration.class,
		WebMvcEndpointManagementContextConfiguration.class,
		HealthContributorAutoConfiguration.class,
		HealthEndpointAutoConfiguration.class,
		InfoEndpointAutoConfiguration.class,
		ThreadDumpEndpointAutoConfiguration.class,
		ShutdownEndpointAutoConfiguration.class,
		JmxAutoConfiguration.class,
		JmxEndpointAutoConfiguration.class,
		MappingsEndpointAutoConfiguration.class,
		SpringApplicationAdminJmxAutoConfiguration.class
})
@ImportResource(value = "classpath:/META-INF/spring/app-clifton-reconciliation-context.xml;${reconciliation.security.config.files}", reader = DelimitedXmlBeanDefinitionReader.class)
@SpringBootApplication(scanBasePackages = "dummy.nonexistent.package")
public class ReconciliationApplication extends ApplicationServiceRunner {

	public static void main(String... args) {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(ReconciliationApplication.class);
		AnnotationConfigApplicationContext parent = new AnnotationConfigApplicationContext(SpringLoadTimeWeavingConfiguration.class);
		builder.parent(parent);
		builder.beanNameGenerator(new AnnotationUsingOurConventionBeanNameGenerator());
		builder.registerShutdownHook(true);
		builder.run(args);
	}
}
