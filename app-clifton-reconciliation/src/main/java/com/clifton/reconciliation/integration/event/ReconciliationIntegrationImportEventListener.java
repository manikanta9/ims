package com.clifton.reconciliation.integration.event;

import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.dataaccess.file.container.FileContainerNative;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.reconciliation.ReconciliationService;
import com.clifton.reconciliation.file.ReconciliationFileCommand;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.function.Function;


@Component
public class ReconciliationIntegrationImportEventListener extends BaseEventListener<IntegrationImportEvent> {

	private IntegrationFileService integrationFileService;
	private ReconciliationService reconciliationService;


	@Override
	public List<String> getEventNameList() {
		return CollectionUtils.createList(IntegrationImportRun.INTEGRATION_EVENT_BROKER_IMPORT, IntegrationImportRun.INTEGRATION_EVENT_BROKER_DAILY_CASH, IntegrationImportRun.INTEGRATION_EVENT_BROKER_DAILY_POSITIONS);
	}


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		AssertUtils.assertNotNull(run, "No run exists for [" + run.getId() + "] for event with name [" + event.getEventName() + "].");

		Function<Integer, File> fileRetrievalFunction = id -> (FileContainerNative) FileContainerFactory.getFileContainer(getIntegrationFileService().downloadIntegrationFile(id).getFile());

		ReconciliationFileCommand uploadFileCommand = new ReconciliationFileCommand();
		uploadFileCommand.setImportDate(run.getEffectiveDate());
		uploadFileCommand.setFileRetrievalId(run.getFile().getId());
		uploadFileCommand.setFileRetrievalFunction(fileRetrievalFunction);
		uploadFileCommand.setFileName(run.getFile().getFileDefinition().getFileName());
		uploadFileCommand.setAutomatedUpload(true);
		getReconciliationService().uploadReconciliationFile(uploadFileCommand);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public ReconciliationService getReconciliationService() {
		return this.reconciliationService;
	}


	public void setReconciliationService(ReconciliationService reconciliationService) {
		this.reconciliationService = reconciliationService;
	}
}
