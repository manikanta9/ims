package com.clifton.reconciliation.handler;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.health.HealthCheckHandler;
import com.clifton.core.health.HealthCheckParameters;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.search.IntegrationFileDefinitionSearchForm;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.security.user.search.SecurityUserSearchForm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.StringWriter;
import java.util.List;


/**
 * Handler for HealthCheck template view, resolved by Freemarker View Resolver. This component will resolve the test execution.
 *
 * @author ChristianM.
 */
@Component
public class ReconciliationHealthStatusHandlerImpl implements ReconciliationHealthStatusHandler, ApplicationContextAware {

	private ApplicationContext applicationContext;

	private ContextHandler contextHandler;

	private HealthCheckHandler healthCheckHandler;

	private IntegrationFileService integrationFileService;

	private SecurityUserService securityUserService;

	@Value("${application.environment.level:prod}")
	private String environment;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void impersonateUserPage() {
		SecurityUser pageUser = new SecurityUser();
		pageUser.setId((short) 999);
		pageUser.setUserName(SecurityUser.SYSTEM_USER);
		getContextHandler().setBean(Context.USER_BEAN_NAME, pageUser);
	}


	@Override
	public HealthCheckParameters parseParameters(HttpServletRequest request) {
		return getHealthCheckHandler().getHealthCheckPageParameters(request);
	}


	/*
	 * CliftonReconciliation database access: systemuser should never be deleted
	 */
	@Override
	public String executeDbTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervalList) {
		final StringWriter writer = new StringWriter();
		getHealthCheckHandler().runTest(testName, maxAllowedTimeInMillis, disabledIntervalList, writer, healthCheckParameters, () -> {
			SecurityUserSearchForm securityUserSearchForm = new SecurityUserSearchForm();
			securityUserSearchForm.setLimit(1);
			securityUserSearchForm.setUserNameEquals(SecurityUser.SYSTEM_USER);
			SecurityUser user = CollectionUtils.getFirstElementStrict(getSecurityUserService().getSecurityUserList(securityUserSearchForm));
			ValidationUtils.assertNotNull(user, "Cannot find security user: " + SecurityUser.SYSTEM_USER);
		});
		return writer.toString();
	}


	/*
	 * Integration application OAuth connection
	 */
	@Override
	public String executeOAuthTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervalList) {
		final StringWriter writer = new StringWriter();

		getHealthCheckHandler().runTest(testName, maxAllowedTimeInMillis, disabledIntervalList, writer, healthCheckParameters, () -> {
			IntegrationFileDefinitionSearchForm searchForm = new IntegrationFileDefinitionSearchForm();
			searchForm.setLimit(1);
			IntegrationFileDefinition fileDefinition = CollectionUtils.getFirstElement(getIntegrationFileService().getIntegrationFileDefinitionList(searchForm));
			if (fileDefinition == null) {
				throw new RuntimeException("Error occurred checking OAuth connection with the Integration Application. Review the logs for further details");
			}
		});
		return writer.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public HealthCheckHandler getHealthCheckHandler() {
		return this.healthCheckHandler;
	}


	public void setHealthCheckHandler(HealthCheckHandler healthCheckHandler) {
		this.healthCheckHandler = healthCheckHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}

	@Override
	public String getEnvironment() {
		return this.environment;
	}


	public void setEnvironment(String environment) {
		this.environment = environment;
	}
}
