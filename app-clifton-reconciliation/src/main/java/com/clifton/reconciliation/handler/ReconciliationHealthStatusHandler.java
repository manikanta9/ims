package com.clifton.reconciliation.handler;

import com.clifton.core.health.HealthCheckParameters;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Handler to render the static templates for Reconciliation Application. Should be implemented in order to handle a HealthCheck page template, and resolve all back-end behaviour
 * related to test execution.
 *
 * @author ChristianM.
 */
public interface ReconciliationHealthStatusHandler {

	public HealthCheckParameters parseParameters(HttpServletRequest request);


	public void impersonateUserPage();


	public String getEnvironment();


	public String executeDbTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervalList);


	public String executeOAuthTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervalList);
}
