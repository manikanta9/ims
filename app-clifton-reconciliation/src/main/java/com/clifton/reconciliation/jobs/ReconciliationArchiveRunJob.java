package com.clifton.reconciliation.jobs;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.archive.ReconciliationArchiveService;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author jonathanr
 */
public class ReconciliationArchiveRunJob implements Task {

	private Integer daysBack;
	private ReconciliationRunService reconciliationRunService;
	private ReconciliationArchiveService reconciliationArchiveService;


	@Override
	public Status run(Map<String, Object> context) {
		if (this.daysBack == null) {
			this.daysBack = 90;
		}
		Date dateBack = DateUtils.addDays(new Date(), Math.negateExact(this.daysBack));
		ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
		searchForm.addSearchRestriction("runDate", ComparisonConditions.LESS_THAN_OR_EQUALS, dateBack);
		List<ReconciliationRun> reconciliationRunList = getReconciliationRunService().getReconciliationRunList(searchForm);
		return getReconciliationArchiveService().archiveReconciliationRunList(reconciliationRunList);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////               Getters and Setters             /////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getDaysBack() {
		return this.daysBack;
	}


	public void setDaysBack(Integer daysBack) {
		this.daysBack = daysBack;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationArchiveService getReconciliationArchiveService() {
		return this.reconciliationArchiveService;
	}


	public void setReconciliationArchiveService(ReconciliationArchiveService reconciliationArchiveService) {
		this.reconciliationArchiveService = reconciliationArchiveService;
	}
}
