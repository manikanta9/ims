package com.clifton.reconciliation.jobs;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.config.NoTableDAOConfig;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.NamedEntityCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentAccountAliasSearchForm;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentSecurityAliasSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.search.SystemConditionSearchForm;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


/**
 * @author TerryS
 */
public class ReconciliationPreLoadCacheJob implements Task {

	private DaoLocator daoLocator;

	private CalendarBusinessDayService calendarBusinessDayService;
	private ApplicationContextService applicationContextService;
	private SystemBeanService systemBeanService;
	private SystemConditionService systemConditionService;

	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	private ReconciliationInvestmentService reconciliationInvestmentService;

	private CacheHandler<?, ?> cacheHandler;

	///////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofMessage("Starting Cache Pre Load");
		ReconciliationPreLoadCacheJobResult result = new ReconciliationPreLoadCacheJobResult(status);

		Date runDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);
		result.setRunDate(runDate);
		result.appendResultMessage("Initialized Cache Loading", true);

		processStep("Clear Cache", this::clearCache, result);
		processStep("Load All Lookup Tables", this::loadAllLookupTables, result);
		processStep("Load All Self Registering Named Entity Caches", this::loadAllSelfRegisteringNamedEntityCache, result);
		processStep("Load System Info", this::loadSystemInfo, result);
		processStep("Load Workflow Info", this::loadWorkflowInfo, result);
		processStep("Load Reconciliation Caches", this::loadReconciliationCaches, result);

		status.setMessage(result.getResultMessage());
		return status;
	}

	///////////////////////////////////////////////////////


	private void processStep(String stepName, Consumer<ReconciliationPreLoadCacheJobResult> cacheStep, ReconciliationPreLoadCacheJobResult result) {
		try {
			cacheStep.accept(result);
		}
		catch (Throwable e) {
			String message = "Error processing step " + stepName + ": " + ExceptionUtils.getDetailedMessage(e);
			result.getStatus().addError(message);
			LogUtils.errorOrInfo(getClass(), message, e);
		}
	}


	private void clearCache(ReconciliationPreLoadCacheJobResult result) {
		String[] caches = getCacheHandler().getCacheNames();
		if (caches != null && caches.length > 0) {
			for (String cacheName : caches) {
				try {
					getCacheHandler().clear(cacheName);
				}
				catch (Throwable e) {
					String msg = "Error clearing cache [" + cacheName + "]: " + ExceptionUtils.getDetailedMessage(e);
					LogUtils.errorOrInfo(getClass(), msg, e);
					result.getStatus().addError(msg);
					result.appendResultMessage(msg, false);
				}
			}
			result.appendResultMessage("Cleared [" + caches.length + "] caches.", true);
		}
	}


	/**
	 * Runs findAll on all Dao's that use a Short for PK
	 * which means the data in the table will be small and used for look ups
	 */
	private void loadAllLookupTables(ReconciliationPreLoadCacheJobResult result) {
		int count = 0;
		Collection<ReadOnlyDAO<?>> daoList = getDaoLocator().locateAll();

		for (ReadOnlyDAO<?> dao : CollectionUtils.getIterable(daoList)) {
			DAOConfiguration<?> config = dao.getConfiguration();
			// Skip DAO Configs with no Tables
			if (config instanceof NoTableDAOConfig<?>) {
				continue;
			}
			if (!config.getTable().isCacheUsed()) {
				continue;
			}

			try {
				DataTypes dt = config.getPrimaryKeyColumn().getDataType();
				if (DataTypes.IDENTITY_SHORT == dt) {
					dao.findAll();
					count++;
				}
			}
			catch (Throwable e) {
				String msg = "Error loading table " + config.getTableName() + " for pre-caching: " + ExceptionUtils.getDetailedMessage(e);
				LogUtils.errorOrInfo(getClass(), msg, e);
				result.getStatus().addError(msg);
				result.appendResultMessage(msg, false);
			}
		}
		result.appendResultMessage("All Rows loaded for [" + count + "] table(s).", true);
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	private void loadAllSelfRegisteringNamedEntityCache(ReconciliationPreLoadCacheJobResult result) {
		Map<String, NamedEntityCache> cacheMap = getApplicationContextService().getContextBeansOfType(NamedEntityCache.class);

		int count = 0;
		if (cacheMap != null) {
			// for each DAO event cache bean
			for (Map.Entry<String, NamedEntityCache> stringNamedEntityCacheEntry : cacheMap.entrySet()) {
				try {
					NamedEntityCache<NamedObject> cacheBean = stringNamedEntityCacheEntry.getValue();
					Class<NamedObject> dtoClass = cacheBean.getDtoClass();
					ReadOnlyDAO<NamedObject> dao = getDaoLocator().locate(dtoClass);
					List<NamedObject> list = dao.findAll();

					for (NamedObject bean : CollectionUtils.getIterable(list)) {
						cacheBean.setBean(bean);
					}
					count++;
				}
				catch (Throwable e) {
					String msg = "Error loading cache " + stringNamedEntityCacheEntry.getKey() + ": " + ExceptionUtils.getDetailedMessage(e);
					LogUtils.errorOrInfo(getClass(), msg, e);
					result.getStatus().addError(msg);
					result.appendResultMessage(msg, false);
				}
			}
		}

		result.appendResultMessage("Entities loaded for [" + count + "] BaseDaoNamedEntityCache(s).", true);
	}


	private void loadSystemInfo(ReconciliationPreLoadCacheJobResult result) {
		List<SystemBean> beanList = getSystemBeanService().getSystemBeanList(new SystemBeanSearchForm());
		int singletonCount = 0;
		int count = 0;
		for (SystemBean bean : CollectionUtils.getIterable(beanList)) {
			try {
				// Singleton beans - load the instance since it is cached
				if (bean.getType().isSingleton()) {
					getSystemBeanService().getBeanInstance(bean);
					singletonCount++;
				}
				// Otherwise - load the properties for the bean (which is done for singletons in above call already)
				else {
					getSystemBeanService().getSystemBeanPropertyListByBeanId(bean.getId());
				}
				count++;
			}
			catch (Exception e) {
				String msg = "Error loading SystemBean cache: " + ExceptionUtils.getDetailedMessage(e);
				LogUtils.errorOrInfo(getClass(), msg, e);
				result.getStatus().addError(msg);
				result.appendResultMessage(msg, false);
			}
		}
		result.appendResultMessage("Loaded [" + count + "] system beans and their properties.  [" + singletonCount + "] of which are singletons and object instance was fully loaded.", true);

		List<SystemCondition> conditionList = getSystemConditionService().getSystemConditionList(new SystemConditionSearchForm());
		for (SystemCondition condition : CollectionUtils.getIterable(conditionList)) {
			// cache all condition and entries
			getSystemConditionService().getSystemConditionPopulated(condition.getId());
		}

		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(conditionList) + "] system conditions and their child conditions and entries.", true);
	}


	private void loadWorkflowInfo(ReconciliationPreLoadCacheJobResult result) {
		List<Workflow> workflowList = getWorkflowDefinitionService().getWorkflowList();
		int count = 0;
		for (Workflow workflow : CollectionUtils.getIterable(workflowList)) {
			if (workflow.isActive()) {
				List<WorkflowState> stateList = getWorkflowDefinitionService().getWorkflowStateListByWorkflow(workflow.getId());
				// Load First Transition (Initial Assignment)
				getWorkflowTransitionService().getWorkflowTransitionFirst(workflow);
				for (WorkflowState state : CollectionUtils.getIterable(stateList)) {
					// Cache Workflow State By Name (For Individual Workflow)
					getWorkflowDefinitionService().getWorkflowStateByName(workflow.getId(), state.getName());
					// Cache Workflow State List By Name (Global)
					getWorkflowDefinitionService().getWorkflowStateListByName(state.getName());
					// Cache Transition List for the State
					getWorkflowTransitionService().getWorkflowTransitionListByStartOrEndState(state);
				}
				List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionListByWorkflow(workflow.getId());
				for (WorkflowTransition transition : CollectionUtils.getIterable(transitionList)) {
					// Transitions - Cache List of Actions
					getWorkflowTransitionService().getWorkflowTransitionActionListForTransitionId(transition.getId());
				}
				count++;
			}
		}
		result.appendResultMessage("Loaded [" + count + "] active workflows and their states, transitions, and actions.", true);
	}


	private void loadReconciliationCaches(ReconciliationPreLoadCacheJobResult result) {
		getReconciliationInvestmentService().getReconciliationInvestmentAccountByNumber("0");
		result.appendResultMessage("Populate Account Cache.", true);

		getReconciliationInvestmentService().getReconciliationInvestmentSecurityByIdentifier("0");
		result.appendResultMessage("Populate Security Cache.", true);

		ReconciliationInvestmentAccountAliasSearchForm accountAliasSearchForm = new ReconciliationInvestmentAccountAliasSearchForm();
		accountAliasSearchForm.setActiveOnDate(result.getRunDate());
		getReconciliationInvestmentService().getReconciliationInvestmentAccountAliasList(accountAliasSearchForm);
		result.appendResultMessage("Populate AccountAliasMap.", true);

		ReconciliationInvestmentSecurityAliasSearchForm securityAliasSearchForm = new ReconciliationInvestmentSecurityAliasSearchForm();
		securityAliasSearchForm.setActiveOnDate(result.getRunDate());
		getReconciliationInvestmentService().getReconciliationInvestmentSecurityAliasList(securityAliasSearchForm);
		result.appendResultMessage("Populate SecurityAliasMap.", true);
	}

	//////////////////////////////////////////////////////
	//////////     Getter & Setter Methods      //////////
	//////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}


	public CacheHandler<?, ?> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<?, ?> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
