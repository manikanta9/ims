package com.clifton.reconciliation.jobs;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;

import java.util.Date;


public class ReconciliationPreLoadCacheJobResult {

	private final StringBuilder result = new StringBuilder(16);

	private long currentStart = System.currentTimeMillis();

	private Date runDate;

	private Status status;


	////////////////////////////////////////////////////////////////////////////


	public ReconciliationPreLoadCacheJobResult(Status status) {
		this.status = status;
	}


	public void appendResultMessage(String resultMsg, boolean logAndEndDuration) {
		this.result.append(resultMsg);
		if (logAndEndDuration) {
			long end = System.currentTimeMillis();
			this.result.append("\n* Duration: ").append(DateUtils.getTimeDifference((end - this.currentStart), false)).append("\n");
			this.currentStart = end;
		}
		this.result.append("\n");
	}


	public String getResultMessage() {
		return this.result.toString();
	}


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public Status getStatus() {
		return this.status;
	}
}
