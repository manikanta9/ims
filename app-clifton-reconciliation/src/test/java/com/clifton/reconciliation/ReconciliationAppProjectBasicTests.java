package com.clifton.reconciliation;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * The {@link ReconciliationAppProjectBasicTests} are disabled due to the current package structure. The {@link BasicProjectTests}
 * find relevant beans for testing by using the package name - i.e. com.clifton.reconciliation, which is identical to the package name
 * used in the clifton-reconciliation module. These tests may currently fail, or may fail after future updates, because of beans from
 * the clifton-reconciliation module. Rather than adding skipMethods/skipServices, we'll leave this disabled until the package structure
 * can be updated (For example, using com.clifton.reconciliation.app)
 *
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
@Disabled
public class ReconciliationAppProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "reconciliation";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.beans.factory.BeanFactory");
		imports.add("org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration");
		imports.add("org.springframework.core.Ordered");
		imports.add("org.springframework.http.converter.");
		imports.add("org.springframework.security.oauth2.http.converter.");
		imports.add("org.springframework.web.");
		imports.add("javax.servlet.http.HttpServletRequest");
		imports.add("org.springframework.boot.actuate.autoconfigure.");
		imports.add("org.springframework.boot.autoconfigure.");
		imports.add("org.springframework.boot.builder.SpringApplicationBuilder");
		imports.add("org.springframework.context.annotation.");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.springframework.boot.test.context.SpringBootTest");
	}
}
