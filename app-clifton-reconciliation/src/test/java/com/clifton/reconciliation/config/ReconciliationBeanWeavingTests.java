package com.clifton.reconciliation.config;

import com.clifton.core.context.spring.AbstractBeanWeavingTests;
import com.clifton.core.context.spring.SpringLoadTimeWeavingConfiguration;
import com.clifton.reconciliation.ReconciliationApplication;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;


/**
 * The {@link ReconciliationBeanWeavingTests} attempts to validate that all types are properly woven during context initialization.
 *
 * @author MikeH
 */
@SpringBootTest
// Evaluate context hierarchy as done in the application class (https://stackoverflow.com/q/41693685/1941654)
@ContextHierarchy({
		@ContextConfiguration(name = "root", classes = SpringLoadTimeWeavingConfiguration.class),
		@ContextConfiguration(name = "child", classes = ReconciliationApplication.class)
})
@Tag("memory-db") // Run with in-memory database tests so that load-time weaving for transactions is enabled; weaving is disabled during standard unit tests
@Disabled // TODO: Tests do not work in CI environments where property replacements are not evaluated; we are unable to use this test until property replacements are removed
public class ReconciliationBeanWeavingTests extends AbstractBeanWeavingTests {

	public ReconciliationBeanWeavingTests(ApplicationContext applicationContext) {
		super(applicationContext);
	}
}
