package com.clifton.system.query.execute;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.sql.Types;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author KellyJ
 */
@ContextConfiguration(locations = "../SystemQueryServiceImplTests-context.xml")
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class SystemQueryExecutionServiceImplTests {

	@Resource
	private SystemQueryExecutionService systemQueryExecutionService;

	@Resource
	private SystemQueryService systemQueryService;

	@Resource
	private DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private static final int TEST_QUERY_GROUPING_ID = 10;
	private static final int TEST_QUERY_GROUPING_NOPARAMS_ID = 20;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGenerateDefaultGroupingQueryParameterList() {
		DataTableRetrievalHandler dataTableService = Mockito.mock(DataTableRetrievalHandler.class);
		Mockito.when(this.dataTableRetrievalHandlerLocator.locate(ArgumentMatchers.anyString())).thenReturn(dataTableService);

		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing1"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTable());
		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing2"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTableTwo());

		List<SystemQueryParameter> parameterList = this.systemQueryService.generateSystemQueryDefaultParameterList(TEST_QUERY_GROUPING_ID);
		List<SystemQueryParameterValue> parameterValueList = createSystemQueryParameterValues(parameterList);
		SystemQuery systemQuery = this.systemQueryService.getSystemQuery(TEST_QUERY_GROUPING_ID);
		Assertions.assertNotNull(systemQuery, "No query returned when there should be");
		systemQuery.setParameterValueList(parameterValueList);

		validateForValidQuery(systemQuery);
	}


	@Test
	public void testGenerateGroupingQueryNoParameters() {
		DataTableRetrievalHandler dataTableService = Mockito.mock(DataTableRetrievalHandler.class);
		Mockito.when(this.dataTableRetrievalHandlerLocator.locate(ArgumentMatchers.anyString())).thenReturn(dataTableService);

		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing1"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTable());
		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing2"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTableTwo());

		List<SystemQueryParameter> parameterList = this.systemQueryService.generateSystemQueryDefaultParameterList(TEST_QUERY_GROUPING_NOPARAMS_ID);
		Assertions.assertTrue(CollectionUtils.isEmpty(parameterList), "There should be no parameters for this test and there are");
		List<SystemQueryParameterValue> parameterValueList = null;
		SystemQuery systemQuery = this.systemQueryService.getSystemQuery(TEST_QUERY_GROUPING_NOPARAMS_ID);
		Assertions.assertNotNull(systemQuery, "No query returned when there should be");
		systemQuery.setParameterValueList(parameterValueList);

		validateForValidQuery(systemQuery);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGenerateGroupingQueryMismatchColumnCount() {
		DataTableRetrievalHandler dataTableService = Mockito.mock(DataTableRetrievalHandler.class);
		Mockito.when(this.dataTableRetrievalHandlerLocator.locate(ArgumentMatchers.anyString())).thenReturn(dataTableService);

		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing1"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTable());
		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing2"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTableThree());

		validateForInvalidQuery("Variable column counts is not supported");
	}


	@Test
	public void testGenerateDefaultGroupingQueryParameterListMismatchDataType() {
		DataTableRetrievalHandler dataTableService = Mockito.mock(DataTableRetrievalHandler.class);
		Mockito.when(this.dataTableRetrievalHandlerLocator.locate(ArgumentMatchers.anyString())).thenReturn(dataTableService);

		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing1"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTable());
		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing2"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTableWithIntegerColumn());

		validateForInvalidQuery("Data Types and Names of columns must match");
	}


	@Test
	public void testGenerateDefaultGroupingQueryParameterListMismatchNames() {
		DataTableRetrievalHandler dataTableService = Mockito.mock(DataTableRetrievalHandler.class);
		Mockito.when(this.dataTableRetrievalHandlerLocator.locate(ArgumentMatchers.anyString())).thenReturn(dataTableService);

		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing1"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTable());
		Mockito.when(dataTableService.findDataTable(ArgumentMatchers.eq("testing2"), ArgumentMatchers.any(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(createDataTableWithDifferentNames());

		validateForInvalidQuery("Data Types and Names of columns must match");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void validateForValidQuery(SystemQuery systemQuery) {
		DataTable table = this.systemQueryExecutionService.getSystemQueryResult(systemQuery);
		Assertions.assertNotNull(table, "No data table returned when there should be");

		Assertions.assertEquals(7, table.getTotalRowCount());
		Assertions.assertEquals(3, table.getColumnCount());

		// compare values of the rows
		DataTable expectedOne = createDataTable();
		DataTable expectedTwo = createDataTableTwo();
		for (int i = 0; i < table.getTotalRowCount(); i++) {
			DataRow actualRow = table.getRow(i);
			DataRow expectedRow;
			if (i < expectedOne.getTotalRowCount()) {
				expectedRow = expectedOne.getRow(i);
			}
			else {
				expectedRow = expectedTwo.getRow(i - expectedOne.getTotalRowCount());
			}

			for (int j = 0; j < table.getColumnCount(); j++) {
				Assertions.assertEquals(expectedRow.getValue(j), actualRow.getValue(j));
			}
		}
	}


	protected void validateForInvalidQuery(final String message) {
		List<SystemQueryParameter> parameterList = this.systemQueryService.generateSystemQueryDefaultParameterList(TEST_QUERY_GROUPING_ID);
		List<SystemQueryParameterValue> parameterValueList = createSystemQueryParameterValues(parameterList);
		SystemQuery systemQuery = this.systemQueryService.getSystemQuery(TEST_QUERY_GROUPING_ID);
		systemQuery.setParameterValueList(parameterValueList);
		Assertions.assertNotNull(systemQuery, "No query returned when there should be");

		// this should fail
		Exception error = null;
		try {
			this.systemQueryExecutionService.getSystemQueryResult(systemQuery);
		}
		catch (Exception e) {
			error = e;
		}
		Assertions.assertNotNull(error, "Expected an error but did not get one");
		Assertions.assertNotNull(error.getMessage(), "Expected an error message but did not get one");
		Assertions.assertTrue(error.getMessage().contains(message), String.format("Expected error [%s] but was [%s]", message, error.getMessage()));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected DataTable createDataTable() {
		List<DataColumn> columnList = CollectionUtils.createList(createColumn("one"), createColumn("two"), createColumn("three"));
		DataTable result = new PagingDataTableImpl(columnList.toArray(new DataColumn[0]));
		result.addRow(createRow(result, "apples", "ten", "red"));
		result.addRow(createRow(result, "apples", "twelve", "green"));
		result.addRow(createRow(result, "oranges", "one", "orange"));
		return result;
	}


	protected DataTable createDataTableTwo() {
		List<DataColumn> columnList = CollectionUtils.createList(createColumn("one"), createColumn("two"), createColumn("three"));
		DataTable result = new PagingDataTableImpl(columnList.toArray(new DataColumn[0]));
		result.addRow(createRow(result, "peppers", "five", "yellow"));
		result.addRow(createRow(result, "apples", "six", "red"));
		result.addRow(createRow(result, "lemons", "three", "yellow"));
		result.addRow(createRow(result, "grapes", "nine", "green"));
		return result;
	}


	protected DataTable createDataTableThree() {
		List<DataColumn> columnList = CollectionUtils.createList(createColumn("one"), createColumn("two"), createColumn("three"), createColumn("four"));
		DataTable result = new PagingDataTableImpl(columnList.toArray(new DataColumn[0]));
		result.addRow(createRow(result, "peppers", "five", "yellow"));
		result.addRow(createRow(result, "apples", "six", "red"));
		result.addRow(createRow(result, "lemons", "three", "yellow"));
		result.addRow(createRow(result, "grapes", "nine", "green"));
		return result;
	}


	protected DataTable createDataTableWithIntegerColumn() {
		List<DataColumn> columnList = CollectionUtils.createList(createColumn("one"), createColumn("two"), createColumnNumber("three"));
		DataTable result = new PagingDataTableImpl(columnList.toArray(new DataColumn[0]));
		result.addRow(createRow(result, "peppers", "five", "yellow"));
		result.addRow(createRow(result, "apples", "six", "red"));
		result.addRow(createRow(result, "lemons", "three", "yellow"));
		result.addRow(createRow(result, "grapes", "nine", "green"));
		return result;
	}


	protected DataTable createDataTableWithDifferentNames() {
		List<DataColumn> columnList = CollectionUtils.createList(createColumn("one"), createColumn("two"), createColumn("four"));
		DataTable result = new PagingDataTableImpl(columnList.toArray(new DataColumn[0]));
		result.addRow(createRow(result, "peppers", "five", "yellow"));
		result.addRow(createRow(result, "apples", "six", "red"));
		result.addRow(createRow(result, "lemons", "three", "yellow"));
		result.addRow(createRow(result, "grapes", "nine", "green"));
		return result;
	}


	protected DataColumn createColumn(String name) {
		return createColumn(name, Types.VARCHAR);
	}


	protected DataColumn createColumnNumber(String name) {
		return createColumn(name, Types.INTEGER);
	}


	protected DataColumn createColumn(String name, int dataType) {
		return new DataColumnImpl(name, dataType);
	}


	protected DataRow createRow(DataTable dataTable, String... data) {
		return new DataRowImpl(dataTable, data);
	}


	protected List<SystemQueryParameterValue> createSystemQueryParameterValues(List<SystemQueryParameter> parameterList) {
		return CollectionUtils.isEmpty(parameterList) ? null : parameterList.stream().map(parameter -> {
			String value = parameter.getDataType().getName().equalsIgnoreCase(DataTypes.DATE.name()) ? "01/01/2010" : "testValue";
			return createSystemQueryParameterValue(parameter, value);
		}).collect(Collectors.toList());
	}


	protected SystemQueryParameterValue createSystemQueryParameterValue(SystemQueryParameter parameter, String value) {
		SystemQueryParameterValue parameterValue = new SystemQueryParameterValue();
		parameterValue.setParameter(parameter);
		parameterValue.setText(value);
		parameterValue.setValue(value);
		return parameterValue;
	}
}
