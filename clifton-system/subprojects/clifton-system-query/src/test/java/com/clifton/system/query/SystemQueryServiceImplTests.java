package com.clifton.system.query;


import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.schema.SystemSchemaService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemQueryServiceImplTests {

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	@Resource
	private SystemQueryService systemQueryService;

	@Resource
	private SystemSchemaService systemSchemaService;

	private static final short TEST_TABLE_ID = 1;

	private static final String TEST_SQL = " SELECT * FROM SystemHierarchy ";

	private static final List<String> TEST_PARAMETER_NAMES = CollectionUtils.createList("Start Date", "End Date", "Name");
	private static final List<String> TEST_PARAMETER_NAMES_OVERRIDDEN = CollectionUtils.createList("Start Date[one]", "Start Date[two]", "End Date", "Name");
	private static final int TEST_APPEND_QUERY_ID = 10;
	private static final int TEST_APPEND_QUERY_NOPARAMS_ID = 20;
	private static final int TEST_APPEND_QUERY_OVERRIDDEN_ID = 30;
	private static final int TEST_APPEND_QUERY_EXCEPTION_ID = 40;


	////////////////////////////////////////////////////////////
	///  TEST SET NOCOUNT ON/OFF IN SQL QUERY


	@Test
	public void testNoNoCountInSql() {
		SystemQuery query = newSystemQuery("Test 1", TEST_SQL);
		this.systemQueryService.saveSystemQuery(query);
	}


	@Test
	public void testSetNoCountOn_MissingOffInSql_1() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemQuery query = newSystemQuery("Test 2", SqlSelectCommand.SET_NO_COUNT_ON + TEST_SQL);
			this.systemQueryService.saveSystemQuery(query);
		});
		Assertions.assertEquals("Invalid SQL.  'SET NOCOUNT ON' found at position 0, but there is NOT a matching 'SET NOCOUNT OFF' after that position.", ve.getMessage());
	}


	@Test
	public void testSetNoCountOn_MissingOffInSql_2() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemQuery query = newSystemQuery("Test 3", SqlSelectCommand.SET_NO_COUNT_ON + TEST_SQL + SqlSelectCommand.SET_NO_COUNT_OFF + TEST_SQL + SqlSelectCommand.SET_NO_COUNT_ON);
			this.systemQueryService.saveSystemQuery(query);
		});
		Assertions.assertEquals("Invalid SQL.  'SET NOCOUNT ON' found at position 91, but there is NOT a matching 'SET NOCOUNT OFF' after that position.", ve.getMessage());
	}


	@Test
	public void testSetNoCountOn_MissingOffInSql_3() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemQuery query = newSystemQuery("Test 4", SqlSelectCommand.SET_NO_COUNT_ON + TEST_SQL + SqlSelectCommand.SET_NO_COUNT_OFF + TEST_SQL + SqlSelectCommand.SET_NO_COUNT_ON + " " + SqlSelectCommand.SET_NO_COUNT_ON);
			this.systemQueryService.saveSystemQuery(query);
		});
		Assertions.assertEquals("Invalid SQL.  'SET NOCOUNT ON' found at position 91, but there is NOT a matching 'SET NOCOUNT OFF' after that position.", ve.getMessage());
	}


	@Test
	public void testSetNoCountOn_PresentOffInSql_1() {
		SystemQuery query = newSystemQuery("Test 5", SqlSelectCommand.SET_NO_COUNT_ON + TEST_SQL + SqlSelectCommand.SET_NO_COUNT_OFF);
		this.systemQueryService.saveSystemQuery(query);
	}


	@Test
	public void testSetNoCountOn_PresentOffInSql_2() {
		SystemQuery query = newSystemQuery("Test 6", SqlSelectCommand.SET_NO_COUNT_ON + TEST_SQL + SqlSelectCommand.SET_NO_COUNT_OFF + TEST_SQL + SqlSelectCommand.SET_NO_COUNT_ON + " " + SqlSelectCommand.SET_NO_COUNT_OFF);
		this.systemQueryService.saveSystemQuery(query);
	}


	@Test
	public void testSetNoCountOn_PresentOffInSql_3() {
		SystemQuery query = newSystemQuery("Test 7", SqlSelectCommand.SET_NO_COUNT_ON + TEST_SQL + SqlSelectCommand.SET_NO_COUNT_OFF + TEST_SQL + SqlSelectCommand.SET_NO_COUNT_ON + " " + SqlSelectCommand.SET_NO_COUNT_ON + " " + SqlSelectCommand.SET_NO_COUNT_OFF);
		this.systemQueryService.saveSystemQuery(query);
	}


	@Test
	public void testGenerateDefaultGroupingQueryParameterList() {
		List<SystemQueryParameter> parameterList = this.systemQueryService.generateSystemQueryDefaultParameterList(TEST_APPEND_QUERY_ID);
		Assertions.assertNotNull(parameterList, "No parameters were generated and there should be");
		Assertions.assertEquals(3, parameterList.size());
		for (int i = 0; i < parameterList.size(); i++) {
			Assertions.assertEquals(TEST_PARAMETER_NAMES.get(i), parameterList.get(i).getName());
		}
	}


	@Test
	public void testGenerateDefaultGroupingQueryParameterListForNoParameters() {
		List<SystemQueryParameter> parameterList = this.systemQueryService.generateSystemQueryDefaultParameterList(TEST_APPEND_QUERY_NOPARAMS_ID);
		Assertions.assertTrue(CollectionUtils.isEmpty(parameterList), "parameters were generated and there NOT should be");
	}


	@Test
	public void testGenerateDefaultGroupingQueryParameterListForOverriddenParameters() {
		List<SystemQueryParameter> parameterList = this.systemQueryService.generateSystemQueryDefaultParameterList(TEST_APPEND_QUERY_OVERRIDDEN_ID);
		Assertions.assertNotNull(parameterList, "No parameters were generated and there should be");
		Assertions.assertEquals(4, parameterList.size());
		for (int i = 0; i < parameterList.size(); i++) {
			Assertions.assertEquals(TEST_PARAMETER_NAMES_OVERRIDDEN.get(i), parameterList.get(i).getName());
		}
	}


	@Test
	public void testGenerateDefaultGroupingQueryParameterListForSameNameDifferentAttributesParameters() {
		List<SystemQueryParameter> parameterList = null;
		Exception error = null;
		try {
			parameterList = this.systemQueryService.generateSystemQueryDefaultParameterList(TEST_APPEND_QUERY_EXCEPTION_ID);
		}
		catch (Exception e) {
			error = e;
		}
		Assertions.assertNull(parameterList, "Expected validation exception but parameters were populated");
		Assertions.assertNotNull(error, "Expected validation exception and there wasn't one");
		String expectedMessage = String.format("Parameter [End Date] already exists with the same name but different attributes.  Please look at the attributes for the following [Peppers]: %nData Type");
		Assertions.assertEquals(expectedMessage, error.getMessage());
	}


	private SystemQuery newSystemQuery(String queryName, String sql) {
		SystemQuery query = new SystemQuery();
		query.setQueryCategory(new SystemQueryCategory());
		query.setTable(this.systemSchemaService.getSystemTable(TEST_TABLE_ID));
		query.setName(queryName);
		query.setSqlStatement(sql);

		// disable security
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);

		return query;
	}
}
