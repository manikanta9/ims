package com.clifton.system.query;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemQueryProjectBasicTests extends BasicProjectTests {


	@Override
	public String getProjectPrefix() {
		return "system-query";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		// NOTE: refactor DB calls into CORE API?
		imports.add("java.sql.Types");
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("execute");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("saveSystemQuery");

		if (skipMethods.contains(method.getName())) {
			return true;
		}
		return super.isMethodSkipped(method);
	}
}
