Clifton.system.query.QueryExportWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Query Export',
	iconCls: 'excel',
	width: 700,
	height: 475,

	items: [{
		xtype: 'formwithdynamicfields',
		url: 'systemQuery.json',

		dynamicTriggerFieldName: 'name',
		dynamicTriggerValueFieldName: 'id',
		dynamicFieldTypePropertyName: 'parameter',
		dynamicFieldListPropertyName: 'parameterValueList',
		dynamicFieldsUrl: 'systemQueryParameterListByQuery.json',
		dynamicFieldsUrlParameterName: 'queryId',
		dynamicFieldClass: 'com.clifton.system.query.SystemQueryParameterValue',

		listeners: {
			afterload: function(fp, isClosing) {
				const w = fp.getWindow();
				const form = fp.getForm();
				const button = TCG.getChildByName(w, 'previewSql', true);
				const data = form.formValues;

				if (TCG.isTrue(data.queryType.dynamicStatement)) {
					button.setVisible(true);
				}
				else {
					button.setVisible(false);
				}
			}
		},

		labelWidth: 200,
		items: [
			{fieldLabel: 'Query', name: 'name', detailIdField: 'id', xtype: 'linkfield', detailPageClass: 'Clifton.system.query.QueryWindow'},
			{name: 'description', xtype: 'textarea', height: 90, readOnly: true, grow: true},
			{xtype: 'label', html: '<hr/>'}
		],

		previewSql: function() {
			const params = this.getQuerySubmitParams();
			if (TCG.isNull(params)) {
				return;
			}
			TCG.data.getDataValuePromise('systemQueryDynamicStatement.json', this, this, params)
				.then(function(value) {
						TCG.createComponent('TCG.app.CloseWindow', {
							title: 'SQL Preview',
							iconCls: 'run',
							width: 900,
							height: 600,
							modal: true,

							items: [{
								xtype: 'formpanel',
								labelWidth: 1,
								items: [{xtype: 'textarea', value: value, boxMinHeight: 500, anchor: '-35 -350'}]
							}]
						});
					}
				);
		},

		exportResults: function(outputFormat) {
			const params = this.getQuerySubmitParams();
			if (TCG.isNull(params)) {
				return;
			}
			params.fileName = TCG.getValue('name', this.getForm().formValues);
			params.outputFormat = outputFormat;
			TCG.downloadFile('systemQueryResult.json', params, this);
		},

		getQuerySubmitParams: function() {
			const panel = this;

			const o = panel.getFirstInValidField();
			if (TCG.isNotNull(o)) {
				o.focus(TCG.isTrue(o.el.dom.select));
				TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
				return;
			}
			let params = this.getSubmitParams();
			if (TCG.isNull(params)) {
				params = {};
			}
			params.id = this.getForm().findField('id').getValue();
			return params;
		},

		buttons: [
			{
				text: 'Preview SQL',
				hidden: true,
				name: 'previewSql',
				iconCls: 'run',
				handler: function() {
					TCG.getParentFormPanel(this).previewSql();
				}

			},
			{
				xtype: 'excel_splitbutton',
				exportMethod: function(exportFormat) {
					TCG.getParentFormPanel(this).exportResults(exportFormat);
				}
			}, {
				text: 'PDF',
				iconCls: 'pdf',
				handler: function() {
					TCG.getParentFormPanel(this).exportResults('pdf');
				}
			}, {
				xtype: 'csv_splitbutton',
				exportMethod: function(exportFormat) {
					TCG.getParentFormPanel(this).exportResults(exportFormat);
				}
			}
		]
	}]
});
