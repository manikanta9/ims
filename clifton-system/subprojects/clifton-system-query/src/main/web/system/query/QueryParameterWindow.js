Clifton.system.query.QueryParameterWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Query Parameter',
	iconCls: 'list',
	height: 500,

	items: [{
		xtype: 'formpanel',
		instructions: 'Query Parameter describes a single parameter that is passed to the SQL of the query during execution. If the parameter is not required and not defined, NULL will be passed so the SQL Query must handle that.',
		url: 'systemQueryParameter.json',
		items: [
			{fieldLabel: 'Query', name: 'query.name', detailIdField: 'query.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.query.QueryWindow'},
			{fieldLabel: 'Parameter Name', name: 'name'},
			{fieldLabel: 'Parameter Label', name: 'label', qtip: 'Optionally override to the parameter name to display'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true, height: 50},
			{fieldLabel: 'Data Type', name: 'dataType.name', hiddenName: 'dataType.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'systemDataTypeList.json', detailPageClass: 'Clifton.system.schema.DataTypeWindow'},
			{fieldLabel: 'Value List Table', name: 'valueTable.name', hiddenName: 'valueTable.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'},
			{fieldLabel: 'Value List URL', name: 'valueListUrl'},
			{fieldLabel: 'UI Config', name: 'userInterfaceConfig', xtype: 'textarea', grow: true, height: 50},
			{boxLabel: 'Bypass System Field Modification', name: 'notSystemModifiedUserInterfaceConfig', xtype: 'checkbox', qtip: 'If checked, will bypass automatic system modification of the field when rendered in the UI. Example: When using dynamic fields, Date fields will be modified to use the Date Generation Options'},
			{fieldLabel: 'Parameter Order', name: 'parameterOrder', qtip: 'The order in which parameter appears in the SQL statement'},
			{fieldLabel: 'Display Order', name: 'order', qtip: 'The order in which parameter is displayed on Query Export window'},
			{fieldLabel: '', boxLabel: 'Required', name: 'required', xtype: 'checkbox'},
			{fieldLabel: '', boxLabel: 'Exclude From Execution', name: 'excludedFromExecution', xtype: 'checkbox', qtip: 'Parameter will not be passed to SQL execution.  Currently used for Select with Freemarker, to pass parameter value to freemarker evaluation, not to the SQL execution'}
		]
	}]
});

