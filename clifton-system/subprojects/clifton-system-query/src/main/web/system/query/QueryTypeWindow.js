Clifton.system.query.QueryTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Query Type',
	iconCls: 'list',
	width: 650,

	items: [{
		xtype: 'formpanel',
		url: 'systemQueryType.json',
		instructions: 'System Query types define how the query is executed.',
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'This query type executes an Update to the System', name: 'updateStatement', xtype: 'checkbox'},
			{
				boxLabel: 'This query type generates a dynamic statement first to be executed', name: 'dynamicStatement', xtype: 'checkbox',
				qtip: 'Examples: Select of Select and Select with Freemarker where SQL is generated based on parameters (and you can preview that dynamically generated SQL)'
			},
			{
				boxLabel: 'This query type requires a SQL definition',
				name: 'sqlRequired',
				xtype: 'checkbox',
				qtip: 'Query groupings do not require SQL to be defined',
				mutuallyExclusiveFields: ['groupingQuery']
			},
			{
				boxLabel: 'This query type indicates that multiple queries are grouped and executed together',
				name: 'groupingQuery',
				xtype: 'checkbox',
				qtip: 'Indicates that this is not itself a query but is associated with multiple queries',
				mutuallyExclusiveFields: ['sqlRequired']
			},
			{fieldLabel: 'SQL Label', name: 'sqlLabel', requiredFields: ['sqlRequired'], qtip: 'Optionally Override <i>SQL</i> field label on the Query window to what the field represents.'},
			{
				fieldLabel: 'Executor Bean', name: 'queryExecutorBean.name', hiddenName: 'queryExecutorBean.id', xtype: 'combo', detailPageClass: 'Clifton.system.bean.BeanWindow', url: 'systemBeanListFind.json?groupName=System Query Executor',
				getDefaultData: function() {
					return {type: {group: {name: 'System Query Executor'}}};
				}
			}
		]
	}]
});
