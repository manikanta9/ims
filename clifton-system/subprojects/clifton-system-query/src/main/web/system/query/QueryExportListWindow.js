Clifton.system.query.QueryExportListWindow = Ext.extend(TCG.app.Window, {
	id: 'systemQueryExportListWindow',
	title: 'Query Exports',
	iconCls: 'excel',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Queries',
				layout: 'border',
				items: [
					{
						region: 'west',
						id: 'systemQueryTree',
						xtype: 'system-hierarchy-folders',
						tableName: 'SystemQuery',
						hierarchyCategoryName: 'System Query Folders',
						rootNodeText: 'Queries',
						ddGroup: 'queryToFolder',
						itemComponentId: 'systemQueryListGrid',
						// Override to use correct param name for search form
						onNodeSelected: function(id) {
							const grid = this.getItemComponent();
							grid.getFolderHierarchyId = function() {
								return id;
							};
							grid.reload();
						}
					},

					{
						region: 'center',
						layout: 'fit',
						items: [{
							id: 'systemQueryListGrid',
							name: 'queryList',
							xtype: 'gridpanel',
							instructions: 'System Queries (this section only includes Queries tagged as "Query Export") can be used to retrieve various information from the system.  They are SQL statements or Service Calls that are organized in folders. Each query is associated with the main Table and requires READ access to corresponding Security Resource. Click Run to execute the query.',
							wikiPage: 'IT/System+Query',
							folderView: true,
							viewNames: ['Organized in Folders', 'Global Search'], // Customized switch to view to remove/add folder tree/searches
							includeAllColumnsView: false,
							groupField: 'table.name',
							groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Queries" : "Query"]})',
							columns: [
								{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
								{header: 'Query Category', width: 50, hidden: true, dataIndex: 'queryCategory.name', filter: {searchFieldName: 'queryCategoryId', type: 'combo', url: 'systemQueryCategoryList.json', loadAll: true}},
								{header: 'Query Type', width: 50, hidden: true, dataIndex: 'queryType.name', filter: {searchFieldName: 'queryTypeId', type: 'combo', url: 'systemQueryTypeList.json', loadAll: true}},
								{header: 'Query Name', width: 100, dataIndex: 'name'},
								{header: 'Data Source', width: 60, dataIndex: 'dataSourceForExecution.name', hidden: true, filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'systemDataSourceList.json', loadAll: true}, tooltip: 'The Data Source directly associated with this Query or via the Main Table.'},
								{header: 'Main Table', hidden: true, width: 100, dataIndex: 'table.name'},
								{header: 'Description', width: 200, dataIndex: 'description'},
								{
									header: 'Run', width: 25, dataIndex: 'url',
									renderer: function(url, args) {
										return TCG.renderActionColumn('run', 'Run', 'Execute this query');
									}
								},
								{header: 'Max Execution Time (ms)', width: 50, dataIndex: 'maxExecutionTimeMillis', type: 'int', useNull: true, hidden: true, qtip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'}
							],
							isFolderView: function() {
								return this.folderView;
							},
							getLoadURL: function() {
								return 'systemQueryListFind.json';
							},
							listeners: {
								afterRender: function() {
									const gp = this;
									gp.grid.store.addListener('load', gp.switchGrouping, this);
								}
							},
							switchGrouping: function() {
								// Needs to be called after load so that grouping is enabled for the store
								if (this.folderView === true) {
									this.grid.store.clearGrouping();
								}
								else {
									this.grid.store.groupBy('table.name');
								}
							},
							switchToView: function(viewName, doNotReload) {
								if (viewName === 'All Columns') {
									this.switchToViewColumns('All Columns');
								}
								const showFolders = (viewName !== 'Global Search');
								if (this.folderView !== showFolders) {
									this.folderView = showFolders;
									this.savedLoadParams = this.getLoadParams();
									if (showFolders === true) {
										Ext.getCmp('systemQueryTree').show();
									}
									else {
										Ext.getCmp('systemQueryTree').hide();
									}
									this.getTopToolbar().find('text', 'Views')[0].menu.findBy(i => i.text && i.text === 'Global Search')[0].setChecked(true);
									this.ownerCt.ownerCt.doLayout();
								}
								if (doNotReload !== true) {
									this.reload();
								}
							},
							getTopToolbarFilters: function(toolbar) {
								return [
									'-',
									{
										fieldLabel: 'Tag', width: 150, xtype: 'combo', name: 'queryTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=System Query Tags',
										listeners: {
											select: function(field) {
												const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
												gridPanel.switchToView('Global Search');
											},
											specialkey: function(field, e) {
												if (e.getKey() === e.ENTER) {
													const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
													gridPanel.switchToView('Global Search');
												}
											}
										}
									},
									{
										fieldLabel: 'Search', width: 130, xtype: 'textfield', name: 'searchPattern',
										listeners: {
											specialkey: function(field, e) {
												if (e.getKey() === e.ENTER) {
													const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
													if (TCG.isNotNull(gridPanel.getFolderHierarchyId())) {
														gridPanel.reload();
													}
													else {
														gridPanel.switchToView('Global Search');
													}
												}
											}
										}
									}
								];
							},
							// Overridden by tree on left when folder is selected so don't override load params
							getFolderHierarchyId: function() {
								return null;
							},
							getLoadParams: function(firstLoad) {
								const searchPattern = TCG.getChildByName(this.getTopToolbar(), 'searchPattern');
								const tag = TCG.getChildByName(this.getTopToolbar(), 'queryTagHierarchyId');
								// First Load - Window Default Data (Came from Menu Item from Grid That Passed Tag Name)
								if (firstLoad) {
									if (this.getWindow().defaultData) {
										const tagName = this.getWindow().defaultData.tagName;
										if (tagName) {
											const tagValue = TCG.data.getData('systemHierarchyByCategoryAndNameExpanded.json?categoryName=System Query Tags&name=' + tagName, this, 'system.hierarchy.System Query Tags.' + tagName);
											tag.setValue(tagValue.id);
											tag.setRawValue(tagValue.name);
											this.setDefaultView('Global Search'); // sets it as "checked"
											this.switchToView('Global Search', true);
										}
									}
									searchPattern.focus(false, 500);
								}

								const params = {};
								params.categoryName = 'System Query Folders';

								if (this.folderView === false) {
									params.categoryIncludeAllIfNull = true;
								}
								else {
									params.categoryHierarchyId = this.getFolderHierarchyId();
									params.categoryIncludeAllIfNull = false;
								}
								params.category2Name = 'System Query Tags';
								params.category2HierarchyName = 'Query Export';

								if (TCG.isNotBlank(searchPattern.getValue())) {
									params.searchPatternAdvanced = searchPattern.getValue();
								}

								if (TCG.isNotBlank(tag.getValue())) {
									params.category3Name = 'System Query Tags';
									params.category3HierarchyId = tag.getValue();
								}
								return params;
							},
							editor: {
								detailPageClass: 'Clifton.system.query.QueryWindow',
								drillDownOnly: true
							},
							gridConfig: {
								localFilteringEnabled: true,
								// allow drag and drop of multiple items at a time
								sm: new Ext.grid.RowSelectionModel({singleSelect: false}),
								enableDragDrop: true,
								ddGroup: 'queryToFolder',
								listeners: {
									'rowclick': function(grid, rowIndex, evt) {
										if (TCG.isActionColumn(evt.target)) {
											const row = grid.store.data.items[rowIndex];
											const className = 'Clifton.system.query.QueryExportWindow';
											const cmpId = TCG.getComponentId(className, row.id);
											TCG.createComponent(className, {
												id: cmpId,
												defaultData: undefined,
												params: {id: row.id},
												openerCt: grid
											});
										}
									}
								}
							}
						}]
					}
				]
			},


			{
				title: 'Run History',
				items: [{xtype: 'system-query-run-history-grid'}]
			},


			{
				title: 'Data Warehouse',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						loadValidation: false, // using the form only to get background color/padding
						height: 200,
						labelWidth: 180,
						instructions: 'Many reports use precalculated data from the Data Warehouse which can be updated using this section. Rebuild for a specific date or for multiple dates based on filters below. Dimensions (investment securities, investment accounts, accounting accounts, etc.) change infrequently and are rebuilt automatically every night.',
						listeners: {
							afterrender: function() {
								this.setFormValue('startSnapshotDate', Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), true);
								this.setFormValue('endSnapshotDate', Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), true);
							}
						},
						items: [
							{
								xtype: 'panel',
								layout: 'column',
								items: [{
									columnWidth: .60,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'clientAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', mutuallyExclusiveFields: ['groupName']},
											{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['accountLabel'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
											{boxLabel: 'Rebuild each month end from client\'s inception date', name: 'buildEachMonthEnd', xtype: 'checkbox'}
										]
									}]
								},
									{columnWidth: .15, items: [{xtype: 'label', html: '&nbsp;'}]},
									{
										columnWidth: .25,
										items: [{
											xtype: 'formfragment',
											frame: false,
											labelWidth: 100,
											items: [
												{fieldLabel: 'Start Date', name: 'startSnapshotDate', xtype: 'datefield', allowBlank: false},
												{fieldLabel: 'End Date', name: 'endSnapshotDate', xtype: 'datefield', allowBlank: false},
												{fieldLabel: '', boxLabel: 'Allow Today Snapshots', name: 'allowTodaySnapshots', xtype: 'checkbox', qtip: 'By default the last snapshot allowed is for the previous day.  Checking this box will allow you to rebuild snapshots for today.'},
												{fieldLabel: '', boxLabel: 'Allow Future Snapshots', name: 'allowFutureSnapshots', xtype: 'checkbox', qtip: 'By default the last snapshot allowed is for the previous day.  Checking this box will allow you to rebuild snapshots for the future (limit of no more than 60 days).'}
											]
										}]
									}]
							}
						],
						buttons: [
							{
								text: 'Rebuild Dimensions',
								tooltip: 'Update investment security, investment account and accounting account information.',
								iconCls: 'run',
								width: 170,
								handler: function() {
									const formPanel = TCG.getParentFormPanel(this);
									const loader = new TCG.data.JsonLoader({
										waitTarget: formPanel,
										onLoad: function(record, conf) {
											TCG.showInfo('Data Warehouse Dimension tables (updates to investment securities, investment accounts, accounting accounts, etc.) have been rebuilt.', 'Data Warehouse');
										}
									});
									loader.load('dwDimensionsRebuild.json');
								}
							},
							{
								text: 'Rebuild Snapshots',
								tooltip: 'Update position and balance snapshot information',
								iconCls: 'run',
								width: 170,
								handler: function() {
									const owner = this.findParentByType('formpanel');
									const form = owner.getForm();
									const url = 'dwAccountingSnapshotsRebuild.json?UI_SOURCE=QueryExport';
									form.submit(Ext.applyIf({
										url: encodeURI(url),
										waitMsg: 'Rebuilding...',
										success: function(form, action) {
											Ext.Msg.alert('Processing Started', action.result.result.message, function() {
												const grid = owner.ownerCt.items.get(1);
												grid.reload.defer(300, grid);
											});
										}
									}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
								}
							}
						]
					},
					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'DW-SNAPSHOTS',
						instantRunner: true,
						title: 'Current Processing',
						flex: 1
					}
				]
			}
		]
	}]
});

