Clifton.system.query.QueryCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Query Category',
	iconCls: 'list',
	width: 700,

	items: [{
		xtype: 'formpanel',
		url: 'systemQueryCategory.json',
		instructions: 'System Query Categories are used to group Queries that may require different security access for different data sources.',
		labelWidth: 150,
		items: [
			{fieldLabel: 'Category Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Data Source', name: 'dataSource.name', hiddenName: 'dataSource.id', xtype: 'combo', url: 'systemDataSourceList.json', loadAll: true, qtip: 'Optional way to limit all queries to only this data sources.'},
			{fieldLabel: 'Query Modify Condition', name: 'queryModifyCondition.name', hiddenName: 'queryModifyCondition.id', xtype: 'system-condition-combo', qtip: 'Must evaluate to true in order to be able to create or update queries for this category.'},
			{fieldLabel: 'Query Execute Condition', name: 'queryExecuteCondition.name', hiddenName: 'queryExecuteCondition.id', xtype: 'system-condition-combo', qtip: 'Must evaluate to true in order to be able to execute queries for this category.'}
		]
	}]
});
