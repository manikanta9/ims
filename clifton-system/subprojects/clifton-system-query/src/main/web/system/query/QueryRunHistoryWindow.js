Clifton.system.query.QueryRunHistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Query Run History',
	iconCls: 'list',
	width: 1100,
	height: 500,

	items: [{
		xtype: 'formpanel',
		url: 'systemQueryRunHistory.json',
		instructions: 'Execution stats for selected System Query run. The message contains the number of rows returned by the query as well as parameter values used.',
		labelFieldName: 'query.name',
		readOnly: true,
		items: [
			{fieldLabel: 'Query', name: 'query.name', detailIdField: 'query.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.query.QueryWindow'},
			{fieldLabel: 'Runner User', name: 'runnerUser.label', detailIdField: 'runnerUser.id', xtype: 'linkfield', detailPageClass: 'Clifton.security.user.UserWindow'},
			{fieldLabel: 'Start Time', name: 'startDate'},
			{fieldLabel: 'End Time', name: 'endDate'},
			{fieldLabel: 'Duration', name: 'executionTimeFormatted'},
			{fieldLabel: 'Message', name: 'description', xtype: 'textarea', anchor: '-35 -175'}
		]
	}]
});
