Clifton.system.query.QueryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Query',
	iconCls: 'list',
	width: 950,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Query',
				items: [{
					xtype: 'formpanel',
					url: 'systemQuery.json',
					childTables: 'SystemQueryParameter', // used by audit trail
					labelWidth: 150,
					items: [
						{fieldLabel: 'Query Category', name: 'queryCategory.name', hiddenName: 'queryCategory.id', xtype: 'combo', url: 'systemQueryCategoryList.json', loadAll: true, detailPageClass: 'Clifton.system.query.QueryCategoryWindow', qtip: 'Selected Category defines Query modification and execution conditions as well as may limit the data source.'},
						{
							fieldLabel: 'Query Type', name: 'queryType.name', hiddenName: 'queryType.id', xtype: 'combo', url: 'systemQueryTypeList.json', loadAll: true, detailPageClass: 'Clifton.system.query.QueryTypeWindow',
							limitRequestedProperties: false,
							listeners: {
								// reset SQL label
								select: function(combo, record, index) {
									combo.ownerCt.resetSqlLabel(record.json);

									// process system query type flags
									const form = combo.getParentForm();
									form.handleSqlRequired(form, record.json.sqlRequired);
									form.handleGroupingQuery(form, record.json.groupingQuery);
								}
							}
						},
						{fieldLabel: 'Query Name', name: 'name'},
						{fieldLabel: 'Main Table', name: 'table.name', hiddenName: 'table.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded', disableAddNewItem: true, detailPageClass: 'Clifton.system.schema.TableWindow', qtip: 'Specifies the main table that the query uses. Query execution requires READ access to this table.'},
						{fieldLabel: 'Data Source', name: 'dataSource.name', hiddenName: 'dataSource.id', xtype: 'combo', url: 'systemDataSourceListFind.json', displayField: 'name', disableAddNewItem: true, detailPageClass: 'Clifton.system.schema.DataSourceWindow', qtip: 'Defines that data sources (usually database) that the query will run against.'},
						{boxLabel: 'Check to disable Run History recording (slightly improves performance)', name: 'runHistoryDisabled', xtype: 'checkbox'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
						{fieldLabel: 'Folder', name: 'hierarchy.nameExpanded', hiddenName: 'hierarchy.id', xtype: 'system-hierarchy-combo', tableName: 'SystemQuery', hierarchyCategoryName: 'System Query Folders'},
						{fieldLabel: 'Max Execution Time (ms)', name: 'maxExecutionTimeMillis', xtype: 'integerfield', qtip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'},
						{fieldLabel: 'SQL', name: 'sqlStatement', xtype: 'textarea', hidden: true, anchor: '-35 -360'},
						{
							xtype: 'formgrid',
							title: 'Query Grouping',
							id: 'queryGroupingList',
							name: 'queryGroupingListFormGrid',
							storeRoot: 'queryGroupingList',
							dtoClass: 'com.clifton.system.query.SystemQueryGrouping',
							hidden: true,
							columnsConfig: [
								{
									header: 'Query', width: 400, dataIndex: 'childQuery.name', idDataIndex: 'childQuery.id',
									editor: {
										xtype: 'combo', url: 'systemQueryListForGroupingSimilar.json', loadAll: true, allowBlank: false, detailIdField: 'childQuery.id', detailPageClass: 'Clifton.system.query.QueryWindow', disableAddNewItem: true,
										beforequery: function(queryEvent) {
											const grid = queryEvent.combo.gridEditor.containerGrid;
											const f = TCG.getParentFormPanel(grid).getForm();

											// find the last query in the list of groupings and use that query id to filter the available query list
											const queryList = JSON.parse(f.findField('queryGroupingList').value);
											const index = queryList.length > 0 ? queryList.length - 1 : 0;

											// finds valid query id value to retrieve similar queries
											// starts from back since that is typically the first one added
											let result;
											for (let i = index; i >= 0; i--) {
												const val = queryList[i]['childQuery.id'];
												if (val) {
													result = val;
													break;
												}
											}
											queryEvent.combo.store.setBaseParam('queryId', result);
										}
									}
								},
								{header: 'Name Override', width: 300, dataIndex: 'queryNameOverride', editor: {xtype: 'textfield'}},
								{header: 'Order', width: 60, dataIndex: 'queryOrder', type: 'int', editor: {xtype: 'integerfield', allowBlank: false}},
								{
									header: 'Run', width: 60, filter: false, sortable: false,
									renderer: function(url, args) {
										return TCG.renderActionColumn('run', 'Run', 'Execute this Query');
									}
								}
							],
							listeners: {
								'cellclick': function(grid, rowIndex, cellIndex, evt) {
									if (TCG.isActionColumn(evt.target)) {
										const row = grid.store.data.items[rowIndex];
										const id = row.data['childQuery.id'];
										const className = 'Clifton.system.query.QueryExportWindow';
										const cmpId = TCG.getComponentId(className, id);
										TCG.createComponent(className, {
											id: cmpId,
											defaultData: undefined,
											params: {id: id},
											openerCt: grid
										});
									}
								}
							}
						},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'SystemQuery',
							hierarchyCategoryName: 'System Query Tags'
						}
					],
					listeners: {
						afterload: function(fp, isClosing) {
							this.resetSqlLabel();
							this.handleSqlRequired(fp, fp.form.formValues.queryType.sqlRequired);
							this.handleGroupingQuery(fp, fp.form.formValues.queryType.groupingQuery);
						}
					},
					resetSqlLabel: function(record) {
						const recordOrQueryType = record || TCG.getValue('queryType', this.getForm().formValues);
						if (recordOrQueryType) {
							const f = this.getForm();
							const sqlLabel = recordOrQueryType.sqlLabel || 'SQL';
							const sqlField = f.findField('sqlStatement');
							sqlField.setFieldLabel(sqlLabel);
						}
					},
					handleSqlRequired: function(form, isSqlRequired) {
						const sqlInput = TCG.getChildByName(form, 'sqlStatement');
						sqlInput.allowBlank = !isSqlRequired;
					},
					handleGroupingQuery: function(form, isGroupingQuery) {
						const formGrid = TCG.getChildByName(form, 'queryGroupingListFormGrid');
						if (isGroupingQuery) {
							// hide SQL field and show Query Groupings formgrid
							form.hideField('sqlStatement');
							formGrid.setVisible(true);
						}
						else {
							// show SQL field and hide Query Groupings formgrid
							form.showField('sqlStatement');
							formGrid.setVisible(false);
						}
					}
				}]
			},


			{
				title: 'Parameters',
				items: [{
					name: 'systemQueryParameterListByQuery',
					xtype: 'gridpanel',
					instructions: 'The following parameters are associated with this query.  All parameters values are passed to the SQL during execution, therefore not required parameters that are not defined will pass NULL to the query execution and the SQL must handle these cases.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Parameter Name', width: 100, dataIndex: 'name'},
						{header: 'Parameter Label', width: 100, dataIndex: 'label'},
						{header: 'Data Type', width: 50, dataIndex: 'dataType.name'},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Display Order', width: 40, dataIndex: 'order', type: 'int'},
						{header: 'Parameter Order', width: 50, dataIndex: 'parameterOrder', type: 'int'},
						{header: 'Required', width: 30, dataIndex: 'required', type: 'boolean'},
						{header: 'Excluded from Execution', width: 30, dataIndex: 'excludedFromExecution', type: 'boolean', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.query.QueryParameterWindow',
						deleteURL: 'systemQueryParameterDelete.json',
						getDefaultData: function(gridPanel) { // defaults queryId for the detail page
							return {
								query: gridPanel.getWindow().getMainForm().formValues
							};
						},
						addEditButtons: function(toolBar, gridPanel) {
							const fp = gridPanel.getWindow().getMainFormPanel();
							const isGroupingQuery = fp.form.formValues.queryType.groupingQuery;

							// only add the Generate Defaults button if the query is a grouping query
							if (isGroupingQuery) {
								// add a Generate Defaults button to generate and save the default grouping parameters
								toolBar.add({
									text: 'Generate Defaults',
									tooltip: 'Generates and persists default parameters for grouping queries.  Parameters from the child queries are combined and can be manually overridden using the Name Override - ex: ParameterName[NameOverride]',
									iconCls: 'run',
									handler: function() {
										const queryId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											params: {queryId: queryId},
											onLoad: function(record, conf) {
												gridPanel.reload();
											}
										});
										loader.load('systemQueryDefaultParameterListGenerate.json');
									}
								});
								toolBar.add('-');
							}

							// add the Add and Remove buttons
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
						}
					},
					getLoadParams: function() {
						return {'queryId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Run History',
				items: [{
					xtype: 'system-query-run-history-grid',
					instructions: 'History of runs for selected system query.',
					columnOverrides: [
						{dataIndex: 'query.name', hidden: true},
						{dataIndex: 'query.updateStatement', hidden: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -30)});
						}
						return {queryId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'SystemQuery'
				}]
			}
		]
	}]
});
