Ext.ns('Clifton.system.query');

// Add System Query Tabs to the System List Window
Clifton.system.list.ListSetupWindowAdditionalTabs.push(
		{
			title: 'Queries',
			items: [{
				xtype: 'system-query-grid',
				addQueryCategoryFilter: true,
				addQueryTypeFilter: true
			}]
		},
		{
			title: 'Run History',
			items: [{
				xtype: 'system-query-run-history-grid'
			}]
		},
		{
			title: 'Query Categories',
			items: [{
				xtype: 'system-query-category-grid'
			}]
		},
		{
			title: 'Query Types',
			items: [{
				xtype: 'system-query-type-grid'
			}]
		}
);


Clifton.system.query.QueryGridPanel = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	name: 'systemQueryListFind',
	instructions: 'This section managed named SQL queries that can be used in various parts of the system. Each query should be tied to a main table which also defines its data source.',
	wikiPage: 'IT/System+Query',
	topToolbarSearchParameter: 'searchPatternAdvanced',
	addQueryCategoryFilter: false,
	addQueryTypeFilter: false,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Query Category', width: 40, dataIndex: 'queryCategory.name', filter: {searchFieldName: 'queryCategoryId', type: 'combo', url: 'systemQueryCategoryList.json', loadAll: true}},
		{header: 'Query Type', width: 40, dataIndex: 'queryType.name', filter: {searchFieldName: 'queryTypeId', type: 'combo', url: 'systemQueryTypeList.json', loadAll: true}},
		{header: 'Update', width: 30, hidden: true, dataIndex: 'queryType.updateStatement', type: 'boolean', filter: {searchFieldName: 'updateStatement'}},
		{header: 'Dynamic Statement', width: 30, hidden: true, dataIndex: 'queryType.dynamicStatement', type: 'boolean', filter: {searchFieldName: 'dynamicStatement'}},
		{header: 'Sql Required', width: 30, hidden: true, dataIndex: 'queryType.sqlRequired', type: 'boolean', filter: {searchFieldName: 'sqlRequired'}},
		{header: 'Grouping Query', width: 30, hidden: true, dataIndex: 'queryType.groupingQuery', type: 'boolean', filter: {searchFieldName: 'groupingQuery'}},
		{header: 'Query Name', width: 150, dataIndex: 'name'},
		{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
		{header: 'Data Source', width: 60, dataIndex: 'dataSourceForExecution.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'systemDataSourceList.json', loadAll: true}, tooltip: 'The Data Source directly associated with this Query or via the Main Table.'},
		{header: 'Main Table', width: 80, dataIndex: 'table.name', filter: {searchFieldName: 'tableId', type: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
		{header: 'Security Resource', width: 100, dataIndex: 'table.securityResource.labelExpanded', filter: {searchFieldName: 'securityResourceName'}, hidden: true, tooltip: 'Security Resource associated with the Main Table of the Query. READ access to this resource is required in order to executed the query.'},
		{header: 'No History', width: 30, dataIndex: 'runHistoryDisabled', type: 'boolean'},
		{header: 'Max Execution Time (ms)', width: 55, dataIndex: 'maxExecutionTimeMillis', type: 'int', useNull: true, qtip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'},
		{
			header: 'Run', width: 25, dataIndex: 'url',
			tooltip: 'Opens up run window to enter parameters and export result.  Does not apply to update statements.',
			renderer: function(url, args, r) {
				if (TCG.isFalse(r.data['queryType.updateStatement'])) {
					return TCG.renderActionColumn('run', 'Run', 'Execute this query');
				}
			}
		}
	],
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		if (this.addQueryCategoryFilter) {
			filters.push({fieldLabel: 'Query Category', xtype: 'toolbar-combo', width: 150, url: 'systemQueryCategoryList.json', loadAll: true, linkedFilter: 'queryCategory.name'});
		}
		if (this.addQueryTypeFilter) {
			filters.push({fieldLabel: 'Query Type', xtype: 'toolbar-combo', width: 150, url: 'systemQueryTypeList.json', loadAll: true, linkedFilter: 'queryType.name'});
		}
		filters.push({fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'queryTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=System Query Tags'});
		filters.push({fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'});
		return filters;
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const tag = TCG.getChildByName(this.getTopToolbar(), 'queryTagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			return {
				categoryName: 'System Query Tags',
				categoryHierarchyId: tag.getValue()
			};
		}
	},
	editor: {
		detailPageClass: 'Clifton.system.query.QueryWindow',
		deleteURL: 'systemQueryDelete.json',
		copyURL: 'systemQueryCopy.json'
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const row = grid.store.data.items[rowIndex];
					const className = 'Clifton.system.query.QueryExportWindow';
					const cmpId = TCG.getComponentId(className, row.id);
					TCG.createComponent(className, {
						id: cmpId,
						defaultData: undefined,
						params: {id: row.id},
						openerCt: grid
					});
				}
			}
		}
	}
});
Ext.reg('system-query-grid', Clifton.system.query.QueryGridPanel);


Clifton.system.query.QueryRunHistoryGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'systemQueryRunHistoryListFind',
	xtype: 'gridpanel',
	instructions: 'History of execution for all system queries',
	wikiPage: 'IT/System+Query',
	appendStandardColumns: false,
	additionalPropertiesToRequest: 'id|query.maxExecutionTimeMillis',
	topToolbarSearchParameter: 'description',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Query ID', width: 40, dataIndex: 'query.id', filter: {searchFieldName: 'queryId'}, hidden: true},
		{header: 'Query Name', width: 100, dataIndex: 'query.name', filter: {searchFieldName: 'queryName'}},
		{header: 'Query Table', width: 80, dataIndex: 'query.table.name', filter: {searchFieldName: 'queryTable'}, hidden: true},
		{header: 'Update', width: 30, dataIndex: 'query.updateStatement', filter: {searchFieldName: 'updateStatement'}, type: 'boolean'},
		{header: 'Runner', width: 50, dataIndex: 'runnerUser.label', filter: {type: 'combo', searchFieldName: 'runnerUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true}},
		{header: 'Started', width: 60, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Ended', width: 60, dataIndex: 'endDate', hidden: true},
		{
			header: 'Duration', width: 50, dataIndex: 'executionTimeFormatted', filter: {type: 'int', sortFieldName: 'executionTimeInSeconds', searchFieldName: 'executionTimeInSeconds'},
			renderer: function(v, metaData, r) {
				return TCG.renderMaxSLA(v, TCG.getValue('executionTimeInMillis', r.json), TCG.getValue('query.maxExecutionTimeMillis', r.json), metaData);
			}
		},
		{header: 'Duration In Seconds', width: 50, dataIndex: 'executionTimeInSeconds', type: 'int', hidden: true},
		{header: 'Duration In Millis', width: 50, dataIndex: 'executionTimeInMillis', type: 'int', hidden: true},
		{header: 'Max Time Millis', width: 40, dataIndex: 'query.maxExecutionTimeMillis', type: 'int', useNull: true, filter: {searchFieldName: 'maxExecutionTimeMillis'}, hidden: true, tooltip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'},
		{header: 'Message', width: 200, dataIndex: 'description'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'queryTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=System Query Tags'},
			{fieldLabel: 'Message', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const params = {readUncommittedRequested: true};
		if (firstLoad) {
			this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -10)});
		}
		const tag = TCG.getChildByName(this.getTopToolbar(), 'queryTagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'System Query Tags';
			params.categoryLinkFieldPath = 'query';
			params.categoryHierarchyId = tag.getValue();
		}
		return params;
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.system.query.QueryRunHistoryWindow'
	},
	addFirstToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add({
			text: 'Run',
			tooltip: 'Run selected Query',
			iconCls: 'run',
			scope: this,
			handler: function() {
				const grid = this.grid;
				const sm = grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select a Query to be run.', 'No Query Selected');
				}
				else {
					const id = sm.getSelected().data['query.id'];
					const className = 'Clifton.system.query.QueryExportWindow';
					const cmpId = TCG.getComponentId(className, id);
					TCG.createComponent(className, {
						id: cmpId,
						defaultData: undefined,
						params: {id: id},
						openerCt: gridPanel
					});
				}
			}
		});
		toolBar.add('-');
	}
});
Ext.reg('system-query-run-history-grid', Clifton.system.query.QueryRunHistoryGridPanel);


Clifton.system.query.QueryCategoryGridPanel = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	name: 'systemQueryCategoryList',
	instructions: 'System Query Categories are used to group Queries that may require different security access for different data sources.',
	wikiPage: 'IT/System+Query',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Category Name', width: 100, dataIndex: 'name'},
		{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
		{header: 'Data Source', width: 70, dataIndex: 'dataSource.name'},
		{header: 'Query Modify Condition', width: 100, dataIndex: 'queryModifyCondition.name'},
		{header: 'Query Execute Condition', width: 100, dataIndex: 'queryExecuteCondition.name'}
	],
	editor: {
		detailPageClass: 'Clifton.system.query.QueryCategoryWindow'
	}
});
Ext.reg('system-query-category-grid', Clifton.system.query.QueryCategoryGridPanel);


Clifton.system.query.QueryTypeGridPanel = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	name: 'systemQueryTypeList',
	instructions: 'System Query Types are used to define how the query is executed. For example, a SQL Select, a SQL Update, Service/JSON call, etc.',
	wikiPage: 'IT/System+Query',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Type Name', width: 80, dataIndex: 'name'},
		{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
		{header: 'Update', width: 30, dataIndex: 'updateStatement', type: 'boolean'},
		{header: 'Dynamic', width: 30, dataIndex: 'dynamicStatement', type: 'boolean'},
		{header: 'Sql Required', width: 30, dataIndex: 'sqlRequired', type: 'boolean'},
		{header: 'Grouping Query', width: 30, dataIndex: 'groupingQuery', type: 'boolean'},
		{header: 'SQL Label', width: 50, dataIndex: 'sqlLabel'},
		{header: 'Query Executor', width: 100, dataIndex: 'queryExecutorBean.name'}
	],
	editor: {
		detailPageClass: 'Clifton.system.query.QueryTypeWindow'
	}
});
Ext.reg('system-query-type-grid', Clifton.system.query.QueryTypeGridPanel);


Clifton.system.query.SystemQueryPieChart = Ext.extend(TCG.form.ChartPanel, {
	chartType: 'pie',
	collapsible: true,
	bodyStyle: 'padding: 5px 0px 0px 5px',
	cellCls: 'vertical-align-top',
	colorScheme: ['#3997ab', '#febf34', '#46535c', '#b8c228', '#a8b2bd', '#db5d39', '#00aa9b', '#5c6e76', '#cab303', '#67b859', '#d7dfe7'],
	chartOptions: {
		legend: {position: 'right'},
		plugins: {
			datalabels: {
				display: true,
				color: '#ffffff',
				font: {weight: 'bold'},
				formatter: function(value, context) {
					return value.toLocaleString();   // format to include thousands separators
				}
			}
		}
	},
	labelColumnIndex: 0,
	labelColumnConverter: function(o) {
		return this.labelColumnIsDate ? TCG.renderDate(o) : o;
	},
	dataColumnIndex: 1,
	dataColumnConverter: parseFloat,

	initComponent: function() {
		const panel = this;
		this.addTool({
			id: 'search',
			qtip: 'Open execution window for the Query Export used to render this chart.',
			handler: function() {
				TCG.data.getDataPromiseUsingCaching('systemQueryByName.json?name=' + panel.getQueryName(), panel, 'system.query.' + panel.getQueryName())
						.then(function(query) {
							const className = 'Clifton.system.query.QueryExportWindow';
							const cmpId = TCG.getComponentId(className, query.id);
							TCG.createComponent(className, {
								id: cmpId,
								params: {id: query.id},
								openerCt: panel
							});
						});
			}
		});
		this.addTool({
			id: 'refresh',
			qtip: 'Update this chart with the latest data.',
			handler: this.reload.createDelegate(this, [])
		});
		Clifton.system.query.SystemQueryPieChart.superclass.initComponent.apply(this, arguments);
	},
	getQueryName: function() {
		return this.queryName || this.title; // System Query name is usually the same as panel's title
	},
	getChartData: function() {
		const chart = this;
		const config = {'root': 'dataTable'};
		return Promise.resolve()
				.then(function() { // first get query parameters
					if (chart.queryParams) {
						return (typeof chart.queryParams === 'function') ? chart.queryParams() : chart.queryParams;
					}
					return {};
				})
				.then(function(queryParams) { // then convert parameters to server-side syntax
					const params = [];
					for (const param of Object.keys(queryParams)) {
						params.push({
							'class': 'com.clifton.system.query.execute.command.QueryParameter',
							name: param,
							value: queryParams[param]
						});
					}
					config.params = {queryParameterList: JSON.stringify(params)};
				})
				.then(function() { // now execute the query using specified parameters
					return TCG.data.getDataPromise('systemQueryResultUsingCommand.json?queryName=' + chart.getQueryName(), chart, config);
				})
				.then(function(queryResult) { // finally generate chart configuration and data
					if (queryResult.totalRows > 0) {
						chart.labelColumnIsDate = TCG.contains(queryResult.columns[chart.labelColumnIndex], 'Date');
						return chart.generateChartData(queryResult.rows, chart);
					}
					return {datasets: [], labels: []};
				});
	},
	generateChartData: function(rows, chart) {
		const data = [];
		const labels = [];
		rows.forEach(row => {
			data.push(chart.dataColumnConverter((row[chart.dataColumnIndex])));
			labels.push(chart.labelColumnConverter(row[chart.labelColumnIndex]));
		});
		return {
			datasets: [{
				data: data, //[10, 20, 30, 7, 10],
				fullData: rows, // preserve original dataset whih may contain additional data useful for drill down
				backgroundColor: chart.colorScheme
			}],
			labels: labels //['User 1', 'User 2', 'User 3', 'User 4', 'User 5']
		};
	}
});
Ext.reg('system-query-pie-chart', Clifton.system.query.SystemQueryPieChart);


Clifton.system.query.SystemQueryDougnutChart = Ext.extend(Clifton.system.query.SystemQueryPieChart, {
	chartType: 'doughnut',
	chartOptions: {
		legend: {position: 'right'},
		plugins: {
			datalabels: {
				display: true,
				color: '#ffffff',
				font: {weight: 'bold'},
				formatter: function(value, context) {
					return value.toLocaleString();   // format to include thousands separators
				}
			},
			doughnuttotal: {calculateTotal: true}
		}
	}
});
Ext.reg('system-query-doughnut-chart', Clifton.system.query.SystemQueryDougnutChart);


Clifton.system.query.SystemQueryBarChart = Ext.extend(Clifton.system.query.SystemQueryPieChart, {
	chartType: 'bar',
	chartOptions: {
		plugins: {
			datalabels: {display: false}
		},
		legend: {display: false},
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true,
					userCallback: function(value, index, values) {
						return value.toLocaleString();   // format to include thousands separators
					}
				}
			}]
		}
	}
});
Ext.reg('system-query-bar-chart', Clifton.system.query.SystemQueryBarChart);


Clifton.system.query.SystemQueryStackedBarChart = Ext.extend(Clifton.system.query.SystemQueryBarChart, {
	stackColumnIndex: 1,
	dataColumnIndex: 2,
	chartOptions: {
		plugins: {
			datalabels: {display: false}
		},
		legend: {display: true},
		tooltips: {
			mode: 'index',
			intersect: false
		},
		scales: {
			xAxes: [{
				stacked: true,
				ticks: {
					userCallback: function(value, index, values) {
						return value.toLocaleString();   // format to include thousands separators
					}
				}
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					userCallback: function(value, index, values) {
						return value.toLocaleString();   // format to include thousands separators
					}
				}
			}]
		}
	},
	generateChartData: function(rows, chart) {
		const labels = [];
		const stacks = [];
		rows.forEach(row => {
			let v = row[chart.stackColumnIndex];
			if (!stacks.includes(v)) {
				stacks.push(v);
			}
			v = chart.labelColumnConverter(row[chart.labelColumnIndex]);
			if (!labels.includes(v)) {
				labels.push(v);
			}
		});
		stacks.sort();

		const datasets = [];
		for (let i = 0; i < stacks.length; i++) {
			const stack = stacks[i];
			const data = [];
			datasets.push({
				label: stack,
				backgroundColor: chart.colorScheme[i],
				data: data
			});
			labels.forEach(label => {
				// find and retreive matching value
				let value = 0;
				for (let j = 0; j < rows.length; j++) {
					const row = rows[j];
					if (label === chart.labelColumnConverter(row[chart.labelColumnIndex]) && stack === row[chart.stackColumnIndex]) {
						value = chart.dataColumnConverter((row[chart.dataColumnIndex]));
						break;
					}
				}
				data.push(value);
			});
		}
		return {
			datasets: datasets,
			labels: labels
		};
	}
});
Ext.reg('system-query-stackedbar-chart', Clifton.system.query.SystemQueryStackedBarChart);
