package com.clifton.system.query.execute;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.command.BaseSystemQueryExecuteCommand;
import com.clifton.system.query.execute.command.SystemQueryExecuteCommand;
import com.clifton.system.schema.SystemDataTypeConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Contains methods and service references that are common to Select and Update queries
 *
 * @author theodorez
 */
public abstract class BaseSystemQueryExecutor<R, T extends SystemQueryExecuteCommand<R>> implements SystemQueryExecutor<R, T> {


	private SystemQueryService systemQueryService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected Map<SystemQueryParameter, Object> getSystemQueryParameterValueMap(BaseSystemQueryExecuteCommand<R> queryCommand) {
		List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(queryCommand.getQuery().getId());
		Map<SystemQueryParameter, Object> parameterValueMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(parameterList)) {
			for (SystemQueryParameter parameter : parameterList) {
				SystemQueryParameterValue value = CollectionUtils.getOnlyElement(BeanUtils.filter(queryCommand.getParameterValueList(), SystemQueryParameterValue::getParameter, parameter));
				if (value == null && parameter.isRequired()) {
					throw new ValidationException("Query Parameter [" + parameter.getName() + "] is required to execute query [" + queryCommand.getQuery().getName() + "]");
				}
				SystemDataTypeConverter converter = new SystemDataTypeConverter(parameter.getDataType());
				Object paramValue = (value == null ? null : converter.convert(value.getValue()));

				parameterValueMap.put(parameter, paramValue);

				if (value != null && value.getValue() != null && queryCommand.getRunDescription() != null) {
					if (queryCommand.getRunDescription().length() > 0) {
						queryCommand.getRunDescription().append("; ");
					}
					queryCommand.getRunDescription().append(parameter.getName());
					queryCommand.getRunDescription().append('=');
					queryCommand.getRunDescription().append(value.getValue());
				}
			}
		}
		return parameterValueMap;
	}


	protected SqlParameterValue[] getSqlParameterValues(Map<SystemQueryParameter, Object> parameterValueMap) {
		SqlParameterValue[] sqlParams = null;
		if (!CollectionUtils.isEmpty(parameterValueMap)) {
			List<SystemQueryParameter> parameterList = new ArrayList<>(parameterValueMap.keySet());
			// Filter out those that shouldn't be passed to SQL execution
			parameterList = BeanUtils.filter(parameterList, systemQueryParameter -> !systemQueryParameter.isExcludedFromExecution());

			if (!CollectionUtils.isEmpty(parameterList)) {
				sqlParams = new SqlParameterValue[CollectionUtils.getSize(parameterList)];
				parameterList = BeanUtils.sortWithFunction(parameterList, SystemQueryParameter::getParameterOrder, true);
				int index = 0;
				for (SystemQueryParameter parameter : parameterList) {
					sqlParams[index] = SqlParameterValue.ofType(parameter.getDataType().getName(), parameterValueMap.get(parameter));
					index++;
				}
			}
		}
		return sqlParams;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemQueryService getSystemQueryService() {
		return this.systemQueryService;
	}


	public void setSystemQueryService(SystemQueryService systemQueryService) {
		this.systemQueryService = systemQueryService;
	}
}
