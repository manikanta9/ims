package com.clifton.system.query.bean.rebuild;

import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import com.clifton.system.query.BaseSystemQueryBeanPropertyProvider;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.execute.SystemQueryExecutionService;


/**
 * The <code>SystemQueryEntityGroupRebuildAwareExecutor</code> is a {@link SystemBeanType} that defines beans that can be used
 * to rebuild associated entities mapped to an <code>EntityGroupRebuildAware</code> entity by executing a {@link SystemQuery}
 *
 * @author NickK
 */
public class SystemQueryEntityGroupRebuildAwareExecutor extends BaseSystemQueryBeanPropertyProvider implements EntityGroupRebuildAwareExecutor {


	private SystemQueryExecutionService systemQueryExecutionService;


	////////////////////////////////////////////////////////////////////////////
	/////////   SystemQueryEntityGroupRebuildAwareExecutor Methods   ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Execute the rebuild for the specified group by executing the bean's defined system query.
	 *
	 * @param groupEntity group entity to rebuild associated entity mappings
	 * @see EntityGroupRebuildAware
	 * @see SystemQuery
	 */
	@Override
	public Status executeRebuild(EntityGroupRebuildAware groupEntity, Status status) {
		validate(groupEntity);
		getSystemQuery().setParameterValueList(getPropertyList(null));
		int rebuilds = getSystemQueryExecutionService().executeSystemQueryUpdate(getSystemQuery(), null);
		status.setMessage(String.format("System query for [%s] was executed with [%d] updates.", groupEntity, rebuilds));
		return status;
	}


	@Override
	public void validate(EntityGroupRebuildAware groupEntity) {
		SystemQuery rebuildQuery = getSystemQueryService().getSystemQuery(getSystemQueryId()); // hydrate query
		ValidationUtils.assertTrue(rebuildQuery.getQueryType().isUpdateStatement(), "Rebuild System Query must be an update statement.");
		ValidationUtils.assertTrue(groupEntity.getRebuildTableName().equals(rebuildQuery.getTable().getName()),
				"Rebuild System Query update must be tied to " + groupEntity.getRebuildTableName() + " table.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public void setSystemQueryExecutionService(SystemQueryExecutionService systemQueryExecutionService) {
		this.systemQueryExecutionService = systemQueryExecutionService;
	}


	public SystemQueryExecutionService getSystemQueryExecutionService() {
		return this.systemQueryExecutionService;
	}
}
