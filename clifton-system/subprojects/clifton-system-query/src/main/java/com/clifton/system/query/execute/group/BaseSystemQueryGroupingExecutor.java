package com.clifton.system.query.execute.group;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryGrouping;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.SystemQueryExecutor;
import com.clifton.system.query.execute.command.SelectSystemQueryExecuteCommand;
import com.clifton.system.query.execute.command.SystemQueryExecuteCommand;
import com.clifton.system.query.execute.command.SystemQueryGroupingExecuteCommand;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Abstract class used to handle common logic across all SystemQueryGroupingExecutors.
 * Responsible for executing child queries and passing the execution results down to the grouping logic in the concrete classes.
 *
 * @author KellyJ
 */
public abstract class BaseSystemQueryGroupingExecutor implements SystemQueryGroupingExecutor {

	private SystemBeanService systemBeanService;
	private SystemQueryService systemQueryService;


	protected abstract DataTable applyGrouping(List<DataTable> results);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Handles the execution of a grouping query command.
	 * It takes the parent command/query and from that executes each child query to get a list of results.
	 * Then the appropriate grouping logic is applied to the results and the end result is set on the original query command.
	 *
	 * @param queryCommand
	 */
	@Override
	public void executeSystemQueryCommand(SelectSystemQueryExecuteCommand queryCommand) {
		if (queryCommand != null) {
			// execute each child query and return a list of DataTables resulting from each child query
			List<DataTable> results = execute(queryCommand);

			// apply the appropriate grouping type to the results
			DataTable result = applyGrouping(results);

			// set the end result on the original command
			queryCommand.setResult(result);
		}
	}


	/**
	 * Takes the parent query command and creates a list of SystemQueryGroupingExecuteCommands which represent the group and child query executor.
	 *
	 * @param queryCommand
	 */
	@SuppressWarnings("unchecked")
	private List<DataTable> execute(SelectSystemQueryExecuteCommand queryCommand) {
		// create grouping commands for each child query from the parent query
		List<SystemQueryGroupingExecuteCommand> groupingCommandList = createGroupingCommands(queryCommand);
		StringBuilder runDescription = queryCommand.getRunDescription();

		// for each command, execute the child query (uses the SqlSelectSystemQueryExecutor to execute the actual sql query)
		return groupingCommandList.stream().map(groupingCommand -> {
			SystemQueryExecutor<?, SystemQueryExecuteCommand<?>> executorBean = (SystemQueryExecutor<?, SystemQueryExecuteCommand<?>>) getSystemBeanService().getBeanInstance(groupingCommand.getQuery().getQueryType().getQueryExecutorBean());
			executorBean.executeSystemQueryCommand(groupingCommand.getCommand());
			if (runDescription != null) {
				if (runDescription.length() > 0) {
					runDescription.append("\n");
				}
				if (groupingCommand.getQuery() != null) {
					runDescription.append("QID = ").append(groupingCommand.getQuery().getId()).append(": ");
				}
				if (groupingCommand.getCommand() != null) {
					runDescription.append(groupingCommand.getCommand().getResultString());
				}
			}
			return groupingCommand.getResult();
		}).collect(Collectors.toList());
	}


	/**
	 * Creates a list of SystemQueryGrouping and SystemQueryExecuteCommand relationships
	 *
	 * @param queryCommand
	 */
	protected List<SystemQueryGroupingExecuteCommand> createGroupingCommands(SystemQueryExecuteCommand<?> queryCommand) {
		// retrieve the list of SystemQueryGrouping values from the parent query
		List<SystemQueryGrouping> groupingList = getSystemQueryService().getSystemQueryGroupingListForParent(queryCommand.getQuery().getId());

		// create a map of input parameters to parameter names
		Map<String, SystemQueryParameterValue> inputParameterValueMap = BeanUtils.getBeanMap(queryCommand.getQuery().getParameterValueList(), (pv -> pv.getParameter().getName()));

		// for each query grouping (parent-child relationship), create the child query select command
		List<SystemQueryGroupingExecuteCommand> result = groupingList.stream()
				.map(group -> {
					String alias = group.getQueryNameOverride();
					SystemQuery childQuery = group.getChildQuery();

					// retrieve the child query parameters as we do not have them yet for the child query
					List<SystemQueryParameter> childQueryParameters = getSystemQueryService().getSystemQueryParameterListByQuery(childQuery.getId());

					// get the defined parameters for the child query
					if (!CollectionUtils.isEmpty(childQueryParameters)) {
						// set the parameter values on the child query that are defined on the parent group
						List<SystemQueryParameterValue> resultParameterValues = childQueryParameters.stream()
								.map(childQueryParameter -> {
									SystemQueryParameterValue parameterValue = findParameterValue(inputParameterValueMap, childQueryParameter, alias);
									ValidationUtils.assertNotNull(parameterValue, String.format("Cannot find parameter value for [%d-%s]", childQueryParameter.getId(), childQueryParameter.getName()));
									return parameterValue;
								}).collect(Collectors.toList());

						childQuery.setParameterValueList(resultParameterValues);
					}

					// create the select query command for the child query
					SystemQueryExecuteCommand<?> command = SelectSystemQueryExecuteCommand.forQuerySelect(childQuery, SqlSelectCommand.DEFAULT_LIMIT);

					// create and return the select query command and group relationship
					return SystemQueryGroupingExecuteCommand.create(group, command);
				}).sorted(Comparator.comparing(SystemQueryGroupingExecuteCommand::getOrder))  // sort the results based on query order for each group
				.collect(Collectors.toList());

		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a default parameter list.
	 * This default behavior includes an appended parameter list of each child query in the format ParameterName[QueryNameOverride]
	 *
	 * @param parentQuery
	 */
	@Override
	public List<SystemQueryParameter> getDefaultParameterList(SystemQuery parentQuery) {
		List<SystemQueryParameter> result = new ArrayList<>();
		if (parentQuery != null) {
			// retrieve a list of parameters already defined for this query
			List<SystemQueryParameter> persistedParameterList = getSystemQueryService().getSystemQueryParameterListByQuery(parentQuery.getId());
			boolean hasPersistedParameters = !CollectionUtils.isEmpty(persistedParameterList);

			// map of Key=persisted parameter id, Value=persisted parameter order for easy lookup
			Map<Integer, Integer> persistedParameterOrderMap = hasPersistedParameters
					? persistedParameterList.stream().collect(Collectors.toMap(SystemQueryParameter::getId, SystemQueryParameter::getParameterOrder))
					: null;

			// retrieve a list of child queries
			List<SystemQueryGrouping> childQueryList = parentQuery.getQueryGroupingList();

			if (!CollectionUtils.isEmpty(childQueryList)) {
				int order = 1;
				for (SystemQueryGrouping group : childQueryList) {
					String childQueryAlias = group.getQueryNameOverride();

					// retrieve the list of parameters for the child query
					List<SystemQueryParameter> childParameterList = getSystemQueryService().getSystemQueryParameterListByQuery(group.getChildQuery().getId());

					if (!CollectionUtils.isEmpty(childParameterList)) {
						// check if the child query parameters are already persisted in the parent query and keep track of parameter orders
						for (SystemQueryParameter childParameter : childParameterList) {
							// is this already persisted and if not what is a good parameter order to assign it to
							if (hasPersistedParameters) {
								if (!containsParameter(persistedParameterList, childParameter, childQueryAlias)) {
									order = generateParameterOrder(order, childParameter, persistedParameterOrderMap);
									SystemQueryParameter newParameter = newParameter(childParameter, order++, parentQuery);
									newParameter.setName(formatAlias(childQueryAlias, childParameter.getName()));
									newParameter.setLabel(formatAlias(childQueryAlias, childParameter.getLabel()));
									result.add(newParameter);
								}
							}
							else {
								SystemQueryParameter newParameter = newParameter(childParameter, order++, parentQuery);
								newParameter.setName(formatAlias(childQueryAlias, childParameter.getName()));
								newParameter.setLabel(formatAlias(childQueryAlias, childParameter.getLabel()));
								result.add(newParameter);
							}
						}
					}
				}
			}
		}
		return result;
	}


	protected int generateParameterOrder(int order, SystemQueryParameter childParameter, Map<Integer, Integer> persistedParameterOrderMap) {
		return persistedParameterOrderMap.containsValue(order)
				? (persistedParameterOrderMap.values().stream().mapToInt(Integer::valueOf).max().orElse(order) + 1)
				: order;
	}


	protected boolean containsParameter(List<SystemQueryParameter> parameters, SystemQueryParameter childQueryParameter, String alias) {
		return parameters.stream().anyMatch(listParameter -> isSameParameter(listParameter, childQueryParameter, alias));
	}


	/**
	 * Matches on Name Alias, isRequired, Data Type, Value Table, Value List URL, Label Alias, and UI Config
	 */
	protected boolean isSameParameter(SystemQueryParameter parentParameter, SystemQueryParameter childQueryParameter, String alias) {
		return parentParameter.getName().equalsIgnoreCase(formatAlias(alias, childQueryParameter.getName())) &&
				parentParameter.isRequired() == childQueryParameter.isRequired() &&
				((parentParameter.getDataType() == null && childQueryParameter.getDataType() == null) || (parentParameter.getDataType() != null && childQueryParameter.getDataType() != null && parentParameter.getDataType().getId().equals(childQueryParameter.getDataType().getId()))) &&
				((parentParameter.getValueTable() == null && childQueryParameter.getValueTable() == null) || (parentParameter.getValueTable() != null && childQueryParameter.getValueTable() != null && parentParameter.getValueTable().getId().equals(childQueryParameter.getValueTable().getId()))) &&
				StringUtils.isEqualIgnoreCase(parentParameter.getValueListUrl(), childQueryParameter.getValueListUrl()) &&
				StringUtils.isEqualIgnoreCase(parentParameter.getLabel(), formatAlias(alias, childQueryParameter.getLabel())) &&
				StringUtils.isEqualIgnoreCase(parentParameter.getUserInterfaceConfig(), childQueryParameter.getUserInterfaceConfig());
	}


	protected SystemQueryParameterValue findParameterValue(Map<String, SystemQueryParameterValue> parameterValueMap, SystemQueryParameter childQueryParameter, String alias) {
		String newParameterAlias = formatAlias(alias, childQueryParameter.getName());
		SystemQueryParameterValue parameterValue = parameterValueMap.get(newParameterAlias);
		return createParameter(parameterValue, childQueryParameter);
	}


	protected SystemQueryParameterValue createParameter(SystemQueryParameterValue val, SystemQueryParameter childQueryParameter) {
		SystemQueryParameterValue value = new SystemQueryParameterValue();
		value.setText(val != null ? val.getText() : null);
		value.setValue(val != null ? val.getValue() : null);
		value.setParameter(childQueryParameter);
		return value;
	}


	protected String formatAlias(String alias, String name) {
		return String.format("%s[%s]", name, alias);
	}


	protected SystemQueryParameter newParameter(SystemQueryParameter parameter, int order, SystemQuery parentQuery) {
		SystemQueryParameter result = BeanUtils.cloneBean(parameter, false, false);
		result.setLabel(parameter.getLabel());
		result.setUserInterfaceConfig(parameter.getUserInterfaceConfig());
		result.setQuery(parentQuery);
		result.setParameterOrder(order);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemQueryService getSystemQueryService() {
		return this.systemQueryService;
	}


	public void setSystemQueryService(SystemQueryService systemQueryService) {
		this.systemQueryService = systemQueryService;
	}
}
