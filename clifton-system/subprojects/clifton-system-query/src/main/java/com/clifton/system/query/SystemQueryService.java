package com.clifton.system.query;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.query.search.SystemQueryRunHistorySearchForm;
import com.clifton.system.query.search.SystemQuerySearchForm;

import java.util.List;


/**
 * The <code>SystemQueryService</code> interface defines methods for working with system queries
 * and other dependent objects.
 *
 * @author vgomelsky
 */
public interface SystemQueryService {


	////////////////////////////////////////////////////////////////////////////////
	//////////          SystemQueryCategory Business Methods              //////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemQueryCategory getSystemQueryCategory(short id);


	public List<SystemQueryCategory> getSystemQueryCategoryList();


	public SystemQueryCategory saveSystemQueryCategory(SystemQueryCategory bean);


	public void deleteSystemQueryCategory(short id);


	////////////////////////////////////////////////////////////////////////////////
	//////////            SystemQueryType Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemQueryType getSystemQueryType(short id);


	public List<SystemQueryType> getSystemQueryTypeList();


	public SystemQueryType saveSystemQueryType(SystemQueryType bean);


	public void deleteSystemQueryType(short id);


	////////////////////////////////////////////////////////////////////////////////
	//////////           SystemQuery Business Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dynamicTableNameBeanPath = "table.name")
	public SystemQuery getSystemQuery(int id);


	public SystemQuery getSystemQueryByName(String name);


	public List<SystemQuery> getSystemQueryList(SystemQuerySearchForm searchForm);


	public List<SystemQuery> getSystemQueryListByTable(final short tableId, SystemQuerySearchForm searchForm);


	public SystemQuery saveSystemQuery(SystemQuery query);


	public void copySystemQuery(int id, String name);


	public void deleteSystemQuery(int id);


	////////////////////////////////////////////////////////////////////////////////
	//////////          SystemQueryParameter Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemQueryParameter getSystemQueryParameter(int id);


	public List<SystemQueryParameter> getSystemQueryParameterListByQuery(int queryId);


	public SystemQueryParameter saveSystemQueryParameter(SystemQueryParameter query);


	public void deleteSystemQueryParameter(int id);


	////////////////////////////////////////////////////////////////////////////////
	//////////         SystemQueryRunHistory Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemQueryRunHistory getSystemQueryRunHistory(int id);


	public List<SystemQueryRunHistory> getSystemQueryRunHistoryList(SystemQueryRunHistorySearchForm searchForm);


	/**
	 * Note: Called internally from execution service only
	 */
	@DoNotAddRequestMapping
	public SystemQueryRunHistory saveSystemQueryRunHistory(SystemQueryRunHistory bean);


	////////////////////////////////////////////////////////////////////////////////
	///////////         SystemQueryGrouping Business Methods             ///////////
	////////////////////////////////////////////////////////////////////////////////
	public SystemQueryGrouping getSystemQueryGrouping(int id);


	public List<SystemQueryGrouping> getSystemQueryGroupingListForParent(int parentQueryId);


	public List<SystemQuery> getSystemQueryListForGroupingSimilar(Integer queryId);


	/**
	 * Note: This also persists the generated parameters
	 */
	public List<SystemQueryParameter> generateSystemQueryDefaultParameterList(int queryId);
}
