package com.clifton.system.query;


import com.clifton.system.userinterface.BaseNamedSystemUserInterfaceAwareVirtualEntity;
import com.clifton.system.userinterface.SystemUserInterfaceAwareVirtual;


/**
 * The <code>SystemQueryParameter</code> class represents and optional or required query parameter definition.
 * Some system queries can allow optional or required parameters defined by this object and persisted in the database.
 * <p>
 * NOTE: Since Values are Used On The Fly on not persisted in the Database, this class
 * does not implement the used by aware implementation.  If we ever decide to save these values, should
 * implement that interface so Used by look ups will pick them up.
 *
 * @author Mary Anderson
 */
public class SystemQueryParameter extends BaseNamedSystemUserInterfaceAwareVirtualEntity<SystemUserInterfaceAwareVirtual> {

	private SystemQuery query;


	/**
	 * Used for cases where a parameter is not actually used for execution
	 * For example - Select with Freemarker can have parameters that are used to evaluate the SQL with Freemarker only
	 * but not passed to the actual SQL execution
	 */
	private boolean excludedFromExecution;

	/**
	 * Parameter Order of the values to the query execution
	 */
	private Integer parameterOrder;

	/**
	 * Denotes whether or not the user interface config should be modified by the system
	 * <p>
	 * Ex: When using dynamic fields, by default Date fields will be modified to use the Date Generation Options
	 * (Note: this particular example occurs in the BaseSystemUserInterfaceBeanPropertyProvider)
	 */
	private boolean notSystemModifiedUserInterfaceConfig;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemQuery getQuery() {
		return this.query;
	}


	public void setQuery(SystemQuery query) {
		this.query = query;
	}


	public Integer getParameterOrder() {
		return this.parameterOrder;
	}


	public void setParameterOrder(Integer parameterOrder) {
		this.parameterOrder = parameterOrder;
	}


	public boolean isExcludedFromExecution() {
		return this.excludedFromExecution;
	}


	public void setExcludedFromExecution(boolean excludedFromExecution) {
		this.excludedFromExecution = excludedFromExecution;
	}


	@Override
	public boolean isNotSystemModifiedUserInterfaceConfig() {
		return this.notSystemModifiedUserInterfaceConfig;
	}


	public void setNotSystemModifiedUserInterfaceConfig(boolean notSystemModifiedUserInterfaceConfig) {
		this.notSystemModifiedUserInterfaceConfig = notSystemModifiedUserInterfaceConfig;
	}
}
