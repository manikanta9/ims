package com.clifton.system.query;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.security.user.SecurityUser;

import java.util.Date;


/**
 * The <code>SystemQueryRunHistory</code> class represents run history for a single run.
 *
 * @author vgomelsky
 */
public class SystemQueryRunHistory extends BaseSimpleEntity<Integer> {

	private SystemQuery query;
	private SecurityUser runnerUser;

	private Date startDate;
	private Date endDate;

	/**
	 * Contains runtime parameter values and errors for failed runs.
	 */
	private String description;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(50);
		result.append("{id=");
		result.append(getId());
		result.append(", ");
		result.append(getQuery().getName());
		result.append(", ");
		result.append(getDescription());
		result.append('}');
		return result.toString();
	}


	public String getExecutionTimeFormatted() {
		if (getStartDate() == null) {
			return null;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getTimeDifferenceShort(toDate, getStartDate());
	}


	public long getExecutionTimeInSeconds() {
		if (getStartDate() == null) {
			return 0;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getSecondsDifference(toDate, getStartDate());
	}


	public long getExecutionTimeInMillis() {
		if (getStartDate() == null) {
			return 0;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getMillisecondsDifference(toDate, getStartDate());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemQuery getQuery() {
		return this.query;
	}


	public void setQuery(SystemQuery query) {
		this.query = query;
	}


	public SecurityUser getRunnerUser() {
		return this.runnerUser;
	}


	public void setRunnerUser(SecurityUser runnerUser) {
		this.runnerUser = runnerUser;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
