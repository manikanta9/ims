package com.clifton.system.query;

import com.clifton.core.beans.BaseEntity;


/**
 * @author kellyj
 */
public class SystemQueryGrouping extends BaseEntity<Integer> {

	/**
	 * The placeholder for the query grouping.  This query does not have sql defined.
	 */
	private SystemQuery parentQuery;

	/**
	 * The child query with actual sql defined.
	 */
	private SystemQuery childQuery;

	/**
	 * The order in which the child query should be ran in relation to other child queries under the parent query.
	 */
	private Integer queryOrder;

	/**
	 * A short name for the child query used for parameter mappings when one or more column names are the same across child queries.
	 */
	private String queryNameOverride;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemQuery getParentQuery() {
		return this.parentQuery;
	}


	public void setParentQuery(SystemQuery parentQuery) {
		this.parentQuery = parentQuery;
	}


	public SystemQuery getChildQuery() {
		return this.childQuery;
	}


	public void setChildQuery(SystemQuery childQuery) {
		this.childQuery = childQuery;
	}


	public Integer getQueryOrder() {
		return this.queryOrder;
	}


	public void setQueryOrder(Integer queryOrder) {
		this.queryOrder = queryOrder;
	}


	public String getQueryNameOverride() {
		return this.queryNameOverride;
	}


	public void setQueryNameOverride(String queryNameOverride) {
		this.queryNameOverride = queryNameOverride;
	}
}
