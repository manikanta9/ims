package com.clifton.system.query.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.system.hierarchy.assignment.search.BaseSystemHierarchyItemSearchForm;
import com.clifton.system.query.SystemQuery;

import java.util.Date;


public class SystemQueryRunHistorySearchForm extends BaseSystemHierarchyItemSearchForm {

	@SearchField(searchField = "query.id")
	private Integer queryId;

	@SearchField(searchFieldPath = "query.queryType", searchField = "updateStatement")
	private Boolean updateStatement;

	@SearchField(searchFieldPath = "query", searchField = "name")
	private String queryName;

	@SearchField(searchFieldPath = "query.table", searchField = "name")
	private String queryTable;

	@SearchField(searchField = "runnerUser.id")
	private Short runnerUserId;

	@SearchField(dateFieldIncludesTime = true)
	private Date startDate;

	@SearchField(dateFieldIncludesTime = true)
	private Date endDate;

	@SearchField
	private String description;

	@SearchField(searchField = "startDate,endDate", searchFieldCustomType = SearchFieldCustomTypes.DATE_DIFFERENCE_SECONDS)
	private Long executionTimeInSeconds;

	@SearchField(searchField = "startDate,endDate", searchFieldCustomType = SearchFieldCustomTypes.DATE_DIFFERENCE_MILLISECONDS)
	private Long executionTimeInMillis;

	@SearchField(searchFieldPath = "query")
	private Integer maxExecutionTimeMillis;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return SystemQuery.TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getQueryId() {
		return this.queryId;
	}


	public void setQueryId(Integer queryId) {
		this.queryId = queryId;
	}


	public Boolean getUpdateStatement() {
		return this.updateStatement;
	}


	public void setUpdateStatement(Boolean updateStatement) {
		this.updateStatement = updateStatement;
	}


	public String getQueryName() {
		return this.queryName;
	}


	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}


	public String getQueryTable() {
		return this.queryTable;
	}


	public void setQueryTable(String queryTable) {
		this.queryTable = queryTable;
	}


	public Short getRunnerUserId() {
		return this.runnerUserId;
	}


	public void setRunnerUserId(Short runnerUserId) {
		this.runnerUserId = runnerUserId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Long getExecutionTimeInSeconds() {
		return this.executionTimeInSeconds;
	}


	public void setExecutionTimeInSeconds(Long executionTimeInSeconds) {
		this.executionTimeInSeconds = executionTimeInSeconds;
	}


	public Long getExecutionTimeInMillis() {
		return this.executionTimeInMillis;
	}


	public void setExecutionTimeInMillis(Long executionTimeInMillis) {
		this.executionTimeInMillis = executionTimeInMillis;
	}


	public Integer getMaxExecutionTimeMillis() {
		return this.maxExecutionTimeMillis;
	}


	public void setMaxExecutionTimeMillis(Integer maxExecutionTimeMillis) {
		this.maxExecutionTimeMillis = maxExecutionTimeMillis;
	}
}
