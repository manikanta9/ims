package com.clifton.system.query.execute;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverterWithDynamicColumnCreation;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataColumnStyle;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.mvc.WebMethodHandler;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.command.SelectSystemQueryExecuteCommand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
public class SelectServiceCallSystemQueryExecutor<T> implements SystemQueryExecutor<DataTable, SelectSystemQueryExecuteCommand> {

	/**
	 * Hidden parameter on the query that defines the columns and format to include in the results
	 * See {@link BeanCollectionToDataTableConverter}: Columns String is passed in as columnName:beanPropertyName:style|columnName:beanPropertyName:style
	 * where style is also a specially formatted string (see {@link DataColumnStyle})
	 */
	private static final String COLUMNS_PARAMETER_NAME = "Columns";

	/**
	 * Allows for specifying a bean list custom data table converter
	 */
	private static final String CONVERTER_BEAN_NAME = "ConverterBeanName";

	/**
	 * Allows for passing hard-coded search form params defined as JSON in the description field of the param.
	 * These are values that should be bound, but not user selected.
	 */
	private static final String SERVICE_PARAMS = "ServiceParams";

	private ApplicationContextService applicationContextService;
	private SystemQueryService systemQueryService;
	private WebMethodHandler webMethodHandler;
	private JsonHandler<?> jsonHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public void executeSystemQueryCommand(SelectSystemQueryExecuteCommand queryCommand) {
		String url = queryCommand.getQuery().getSqlStatement();

		Map<String, String> params = getSystemQueryParameterValues(queryCommand);
		String columns = params.remove(COLUMNS_PARAMETER_NAME);
		String converterBeanName = params.remove(CONVERTER_BEAN_NAME);

		//For downloads we don't need a row-count, only applies to mongo based service calls right now.
		params.put("getRowCount", "false");
		Object result = getWebMethodHandler().executeServiceCall(url, params);
		DataTable dataTableResult;

		if (result instanceof List) {
			Class<T> dtoClass = (Class<T>) CollectionUtils.getFirstElementStrict((List<T>) result).getClass();
			BeanCollectionToDataTableConverter<T> converter = getBeanCollectionToDataTableConverter(columns, dtoClass, converterBeanName);
			if (!CollectionUtils.isEmpty((List<T>) result)) {
				dataTableResult = converter.convert((List<T>) result);
			}
			else {
				dataTableResult = new PagingDataTableImpl(new DataColumnImpl[]{new DataColumnImpl("No results found.", java.sql.Types.NVARCHAR)});
			}
		}
		else if (result instanceof DataTable) {
			dataTableResult = (DataTable) result;
		}
		else {
			throw new RuntimeException("Error executing URL " + url + ". Expected return type to be a List or DataTable, but was " + result.getClass());
		}
		queryCommand.setResult(dataTableResult);
	}


	@SuppressWarnings("unchecked")
	private BeanCollectionToDataTableConverter<T> getBeanCollectionToDataTableConverter(String columns, Class<T> dtoClass, String converterBeanName) {
		if (!StringUtils.isEmpty(converterBeanName)) {
			BeanCollectionToDataTableConverter<T> converter = (BeanCollectionToDataTableConverter<T>) getApplicationContextService().getContextBean(converterBeanName);
			converter.initialize(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(getApplicationContextService(), dtoClass, columns));
			return converter;
		}
		if (StringUtils.isEmpty(columns)) {
			return new BeanCollectionToDataTableConverterWithDynamicColumnCreation<>(DataTableConverterConfiguration.ofDtoClass(getApplicationContextService(), null));
		}
		else {
			return new BeanCollectionToDataTableConverter<>(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(getApplicationContextService(), dtoClass, columns));
		}
	}


	@SuppressWarnings("unchecked")
	private Map<String, String> getSystemQueryParameterValues(SelectSystemQueryExecuteCommand queryCommand) {

		List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(queryCommand.getQuery().getId());
		Map<String, String> params = new HashMap<>();

		if (!CollectionUtils.isEmpty(parameterList)) {
			for (SystemQueryParameter queryParameter : parameterList) {
				// Special Case for Columns where the parameter is hidden, and the value is in the description
				if (COLUMNS_PARAMETER_NAME.equalsIgnoreCase(queryParameter.getName())) {
					params.put(COLUMNS_PARAMETER_NAME, queryParameter.getDescription());
				}
				if (CONVERTER_BEAN_NAME.equalsIgnoreCase(queryParameter.getName())) {
					params.put(CONVERTER_BEAN_NAME, queryParameter.getDescription());
				}
				if (SERVICE_PARAMS.equalsIgnoreCase(queryParameter.getName())) {
					params.putAll((Map<String, String>) getJsonHandler().fromJson(queryParameter.getDescription(), Map.class));
				}
				else {
					SystemQueryParameterValue value = CollectionUtils.getOnlyElement(BeanUtils.filter(queryCommand.getParameterValueList(), parameterValue -> parameterValue.getParameter().getId(), queryParameter.getId()));
					boolean valueSet = (value != null && value.getValue() != null);
					if (!valueSet && queryParameter.isRequired()) {
						throw new ValidationException("Query Parameter [" + queryParameter.getName() + "] is required to execute query [" + queryCommand.getQuery().getName() + "]");
					}
					if (valueSet) {
						params.put(queryParameter.getName(), value.getValue());
						if (queryCommand.getRunDescription() != null) {
							if (queryCommand.getRunDescription().length() > 0) {
								queryCommand.getRunDescription().append("; ");
							}
							queryCommand.getRunDescription().append(queryParameter.getName());
							queryCommand.getRunDescription().append('=');
							queryCommand.getRunDescription().append(value.getValue());
						}
					}
				}
			}
		}

		if (!params.containsKey("limit")) {
			params.put("limit", Integer.toString(queryCommand.getLimit()));
		}
		return params;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public JsonHandler<?> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<?> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}


	public SystemQueryService getSystemQueryService() {
		return this.systemQueryService;
	}


	public void setSystemQueryService(SystemQueryService systemQueryService) {
		this.systemQueryService = systemQueryService;
	}


	public WebMethodHandler getWebMethodHandler() {
		return this.webMethodHandler;
	}


	public void setWebMethodHandler(WebMethodHandler webMethodHandler) {
		this.webMethodHandler = webMethodHandler;
	}
}
