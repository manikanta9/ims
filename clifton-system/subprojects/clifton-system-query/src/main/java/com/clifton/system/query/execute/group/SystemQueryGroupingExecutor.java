package com.clifton.system.query.execute.group;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.execute.SystemQueryExecutor;
import com.clifton.system.query.execute.command.SelectSystemQueryExecuteCommand;

import java.util.List;


/**
 * @author KellyJ
 */
public interface SystemQueryGroupingExecutor extends SystemQueryExecutor<DataTable, SelectSystemQueryExecuteCommand> {

	public List<SystemQueryParameter> getDefaultParameterList(SystemQuery parentQuery);
}
