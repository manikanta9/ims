package com.clifton.system.query.execute.command;

import java.io.Serializable;
import java.util.List;


/**
 * The QueryCommand is used to execute select queries (optionally with parameters).
 *
 * @author vgomelsky
 */
public class QueryCommand implements Serializable {

	private Integer queryId;
	private String queryName;

	// optional runtime parameters to pass to the query
	private List<QueryParameter> queryParameterList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getQueryId() {
		return this.queryId;
	}


	public void setQueryId(Integer queryId) {
		this.queryId = queryId;
	}


	public String getQueryName() {
		return this.queryName;
	}


	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}


	public List<QueryParameter> getQueryParameterList() {
		return this.queryParameterList;
	}


	public void setQueryParameterList(List<QueryParameter> queryParameterList) {
		this.queryParameterList = queryParameterList;
	}
}
