package com.clifton.system.query.execute.command;

import com.clifton.core.security.authorization.SecureMethodTableResolver;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryService;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;


/**
 * The <code>SystemQuerySecureMethodTableResolver</code> class looks up the system query by id, else name and returns the table associated with it.
 * Returns SystemQuery table if no table selected
 *
 * @author manderson
 */
@Component
public class SystemQueryCommandSecureMethodTableResolver implements SecureMethodTableResolver {

	private SystemQueryService systemQueryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTableName(Method method, Map<String, ?> config) {
		SystemQuery query = getSystemQuery(config);
		if (query != null && query.getTable() != null) {
			return query.getTable().getName();
		}
		// Default to SystemQuery table
		return SystemQuery.TABLE_NAME;
	}


	private SystemQuery getSystemQuery(Map<String, ?> config) {
		Integer queryId = MapUtils.getParameterAsInteger("queryId", config);
		if (queryId != null) {
			return getSystemQueryService().getSystemQuery(queryId);
		}
		String queryName = MapUtils.getParameterAsString("queryName", config);
		if (!StringUtils.isEmpty(queryName)) {
			return getSystemQueryService().getSystemQueryByName(queryName);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemQueryService getSystemQueryService() {
		return this.systemQueryService;
	}


	public void setSystemQueryService(SystemQueryService systemQueryService) {
		this.systemQueryService = systemQueryService;
	}
}
