package com.clifton.system.query.execute.command;

import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameterValue;

import java.util.List;


/**
 * The BaseSystemQueryExecuteCommand class contains common properties used for both Select and Update query commands
 *
 * @author manderson
 */
public abstract class BaseSystemQueryExecuteCommand<T> implements SystemQueryExecuteCommand<T> {

	private final SystemQuery query;

	private T result;


	/**
	 * During execution, the parameter values are appended to the runDescription to be added to the run history record
	 */
	private StringBuilder runDescription;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected BaseSystemQueryExecuteCommand(SystemQuery query) {
		this.runDescription = new StringBuilder(32);
		this.query = query;
	}


	public List<SystemQueryParameterValue> getParameterValueList() {
		return this.getQuery().getParameterValueList();
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemQuery getQuery() {
		return this.query;
	}


	@Override
	public T getResult() {
		return this.result;
	}


	@Override
	public void setResult(T result) {
		this.result = result;
	}


	public StringBuilder getRunDescription() {
		return this.runDescription;
	}
}
