package com.clifton.system.query.execute;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryRunHistory;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.command.QueryCommand;
import com.clifton.system.query.execute.command.SelectSystemQueryExecuteCommand;
import com.clifton.system.query.execute.command.SystemQueryExecuteCommand;
import com.clifton.system.query.execute.command.UpdateSystemQueryExecuteCommand;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class SystemQueryExecutionServiceImpl implements SystemQueryExecutionService {

	private SecurityUserService securityUserService;

	private SystemBeanService systemBeanService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemQueryService systemQueryService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Useful for admins when generating dynamic sql select statements to see the sql that is generated.  Can be helpful when trying to debug an issue
	 * with the statement
	 */
	@Override
	@ResponseBody
	public String getSystemQueryDynamicStatement(SystemQuery query) {
		// save the parameter value list before we replace the query
		List<SystemQueryParameterValue> parameterValueList = query.getParameterValueList();

		query = getSystemQueryService().getSystemQuery(query.getId());
		ValidationUtils.assertFalse(query.getQueryType().isUpdateStatement(), "System Query [" + query.getName() + "] is flagged as an update statement.  Cannot execute select for queries flagged as an update.");
		ValidationUtils.assertTrue(query.getQueryType().isDynamicStatement(), "System Query [" + query.getName() + "] is NOT flagged as a dynamic statement.  There is no dynamic SQL to generate.");

		query.setParameterValueList(parameterValueList);
		SelectSystemQueryExecuteCommand command = SelectSystemQueryExecuteCommand.forQueryDynamicSelectPreview(query);
		executeSystemQueryCommand(command, false);

		if (StringUtils.isEmpty(command.getDynamicSql())) {
			return "No SQL generated for selected parameters.";
		}
		return command.getDynamicSql();
	}


	/**
	 * NOTE: Special case for security - we don't usually dynamically secure reads, but query exports can be used to report
	 * on sensitive areas on the system so these must be secured at the get/read level for individual queries
	 */
	@Override
	public DataTable getSystemQueryResult(SystemQuery query) {
		return getSystemQueryResultWithLimitAndTimeout(query, SqlSelectCommand.DEFAULT_LIMIT, SqlSelectCommand.DEFAULT_QUERY_TIMEOUT);
	}


	/**
	 * NOTE: Special case for security - we don't usually dynamically secure reads, but query exports can be used to report
	 * on sensitive areas on the system so these must be secured at the get/read level for individual queries
	 */
	@Override
	public DataTable getSystemQueryResultWithTimeout(SystemQuery query, int timeout) {
		return getSystemQueryResultWithLimitAndTimeout(query, SqlSelectCommand.DEFAULT_LIMIT, timeout);
	}


	/**
	 * NOTE: Special case for security - we don't usually dynamically secure reads, but query exports can be used to report
	 * on sensitive areas on the system so these must be secured at the get/read level for individual queries
	 */
	@Override
	public DataTable getSystemQueryResultWithLimit(SystemQuery query, int limit) {
		return getSystemQueryResultWithLimitAndTimeout(query, limit, SqlSelectCommand.DEFAULT_QUERY_TIMEOUT);
	}


	/**
	 * NOTE: Special case for security - we don't usually dynamically secure reads, but query exports can be used to report
	 * on sensitive areas on the system so these must be secured at the get/read level for individual queries
	 */
	@Override
	public DataTable getSystemQueryResultWithLimitAndTimeout(SystemQuery query, int limit, int timeout) {
		// save the parameter value list input before we replace the query
		List<SystemQueryParameterValue> parameterValueList = query.getParameterValueList();

		query = getSystemQueryService().getSystemQuery(query.getId());
		ValidationUtils.assertFalse(query.getQueryType().isUpdateStatement(), "System Query [" + query.getName() + "] is flagged as an update statement.  Cannot execute select for queries flagged as an update.");

		if (query.getQueryCategory().getQueryExecuteCondition() != null) {
			EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(query.getQueryCategory().getQueryExecuteCondition(), query);
			if (result.isFalse()) {
				throw new ValidationException("Cannot execute Query because Query Category's Execute Condition evaluated to false. " + result.getMessage());
			}
		}

		// populate the SystemQueryParameters for the value list
		if (!CollectionUtils.isEmpty(parameterValueList)) {
			parameterValueList.forEach(param -> {
				SystemQueryParameter queryParameter = getSystemQueryService().getSystemQueryParameter(param.getParameter().getId());
				param.setParameter(queryParameter);
			});
		}

		// set the parameter value list on the query
		query.setParameterValueList(parameterValueList);
		SelectSystemQueryExecuteCommand command = SelectSystemQueryExecuteCommand.forQuerySelectWithTimeout(query, limit, timeout);
		executeSystemQueryCommand(command, true);
		return command.getResult();
	}


	@Override
	public DataTable getSystemQueryResultUsingCommand(QueryCommand queryCommand) {
		SystemQuery query;
		if (queryCommand.getQueryId() != null) {
			query = getSystemQueryService().getSystemQuery(queryCommand.getQueryId());
			ValidationUtils.assertNotNull(query, "Cannot find System Query with id = " + queryCommand.getQueryId());
		}
		else if (queryCommand.getQueryName() != null) {
			query = getSystemQueryService().getSystemQueryByName(queryCommand.getQueryName());
			ValidationUtils.assertNotNull(query, "Cannot find System Query with name " + queryCommand.getQueryName() + "'");
		}
		else {
			throw new ValidationException("Required argument missing: queryId or queryName");
		}

		// populate parameters if specified
		if (!CollectionUtils.isEmpty(queryCommand.getQueryParameterList())) {
			List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(query.getId());
			List<SystemQueryParameterValue> parameterValueList = new ArrayList<>();
			queryCommand.getQueryParameterList().forEach(queryParameter -> {
				SystemQueryParameter parameter = CollectionUtils.getFirstElement(BeanUtils.filter(parameterList, SystemQueryParameter::getName, queryParameter.getName()));
				ValidationUtils.assertNotNull(parameter, "Cannot find System Query parameter '" + queryParameter.getName() + "' for query: " + query);
				SystemQueryParameterValue parameterValue = new SystemQueryParameterValue();
				parameterValue.setParameter(parameter);
				parameterValue.setValue(queryParameter.getValue());
				parameterValue.setText(queryParameter.getValue());
				parameterValueList.add(parameterValue);
			});
			query.setParameterValueList(parameterValueList);
		}

		return getSystemQueryResult(query);
	}


	@Override
	public int executeSystemQueryUpdate(int id, Integer timeoutInSeconds) {
		SystemQuery query = getSystemQueryService().getSystemQuery(id);
		return executeSystemQueryUpdate(query, timeoutInSeconds);
	}


	@Override
	public int executeSystemQueryUpdate(SystemQuery query, Integer timeoutInSeconds) {
		ValidationUtils.assertTrue(query.getQueryType().isUpdateStatement(), "System Query [" + query.getName() + "] is not flagged as an update statement.  Cannot execute for queries not flagged as an update.");

		List<SystemQueryParameterValue> parameterValueList = query.getParameterValueList();
		// populate the SystemQueryParameters for the value list
		if (!CollectionUtils.isEmpty(parameterValueList)) {
			parameterValueList.forEach(param -> {
				SystemQueryParameter queryParameter = getSystemQueryService().getSystemQueryParameter(param.getParameter().getId());
				param.setParameter(queryParameter);
			});
		}
		// set the parameter value list on the query
		query.setParameterValueList(parameterValueList);
		UpdateSystemQueryExecuteCommand command = UpdateSystemQueryExecuteCommand.forQueryWithTimeout(query, timeoutInSeconds);
		executeSystemQueryCommand(command, true);
		return command.getResult();
	}


	/**
	 * Executes the SystemQueryCommand and saves history if applicable
	 *
	 * @param saveHistory - will not save history if the query has run history disable, but this will also allow disabling (used for dynamic sql preview option)
	 */
	@SuppressWarnings("unchecked")
	private void executeSystemQueryCommand(SystemQueryExecuteCommand<?> command, boolean saveHistory) {
		if (command.getQuery().getQueryType().isUpdateStatement()) {
			ValidationUtils.assertTrue(command instanceof UpdateSystemQueryExecuteCommand, "System Query: " + command.getQuery().getName() + " is an Update, and can only be used to execute updates.");
		}
		else {
			ValidationUtils.assertTrue(command instanceof SelectSystemQueryExecuteCommand, "System Query: " + command.getQuery().getName() + " is a Select, and can only be used to execute selects.");
		}

		// use the executor bean to execute the query command
		Date startDate = new Date();
		Throwable error = null;
		try {
			SystemQueryExecutor<?, SystemQueryExecuteCommand<?>> executorBean = (SystemQueryExecutor<?, SystemQueryExecuteCommand<?>>) getSystemBeanService().getBeanInstance(command.getQuery().getQueryType().getQueryExecutorBean());
			executorBean.executeSystemQueryCommand(command);
		}
		catch (Throwable e) {
			error = e;
		}

		// save history
		if (saveHistory && !command.getQuery().isRunHistoryDisabled()) {
			SystemQueryRunHistory history = new SystemQueryRunHistory();
			history.setStartDate(startDate);
			history.setEndDate(new Date());
			history.setQuery(command.getQuery());
			history.setRunnerUser(getSecurityUserService().getSecurityUserCurrent());
			if (error != null) {
				history.setDescription("ERROR: " + (StringUtils.isEmpty(command.getResultString()) ? "" : command.getResultString() + "\n") + ExceptionUtils.getDetailedMessageReversed(error));
			}
			else if (!StringUtils.isEmpty(command.getResultString())) {
				history.setDescription(command.getResultString());
			}
			history.setDescription(StringUtils.formatStringUpToNCharsWithDots(history.getDescription(), DataTypes.DESCRIPTION_LONG.getLength(), true));
			getSystemQueryService().saveSystemQueryRunHistory(history);
		}

		if (error != null) {
			throw new RuntimeException("Error executing query [" + command.getQuery() + "]: " + error.getMessage(), error);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SystemQueryService getSystemQueryService() {
		return this.systemQueryService;
	}


	public void setSystemQueryService(SystemQueryService systemQueryService) {
		this.systemQueryService = systemQueryService;
	}
}
