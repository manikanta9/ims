package com.clifton.system.query.execute.command;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.system.query.SystemQuery;


/**
 * SelectSystemQueryExecuteCommand is an SystemQueryExecuteCommand used for executing Selects, or retrieval of data from the system
 * where the result is a DataTable
 *
 * @author manderson
 */
public class SelectSystemQueryExecuteCommand extends BaseSystemQueryExecuteCommand<DataTable> {

	/**
	 * Maximum number of rows to return
	 */
	private final int limit;


	/**
	 * If query is set to generate dynamicSql that is then executed again, this property is populated with that dynamic SQL so can be
	 * easily retrieved ("Preview") feature for administrators
	 */
	private String dynamicSql;

	/**
	 * Option for dynamicSql preview to not populate the DataTable with the results, just get the dynamic sql
	 */
	private final boolean retrieveDynamicSqlOnly;

	/**
	 * Timeout in seconds, default is -1, indicating to use the JDBC driver's default
	 */
	private int queryTimeout = -1;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private SelectSystemQueryExecuteCommand(SystemQuery query, boolean retrieveDynamicSqlOnly, Integer limit) {
		super(query);

		this.retrieveDynamicSqlOnly = retrieveDynamicSqlOnly;
		this.limit = (limit == null ? SqlSelectCommand.DEFAULT_LIMIT : limit);
	}


	private SelectSystemQueryExecuteCommand(SystemQuery query, boolean retrieveDynamicSqlOnly, Integer limit, Integer queryTimeout) {
		super(query);

		this.retrieveDynamicSqlOnly = retrieveDynamicSqlOnly;
		this.limit = (limit == null ? SqlSelectCommand.DEFAULT_LIMIT : limit);
		this.queryTimeout = (queryTimeout == null ? SqlSelectCommand.DEFAULT_QUERY_TIMEOUT : queryTimeout);
	}


	public static SelectSystemQueryExecuteCommand forQuerySelect(SystemQuery query, Integer limit) {
		return new SelectSystemQueryExecuteCommand(query, false, limit);
	}


	public static SelectSystemQueryExecuteCommand forQuerySelectWithTimeout(SystemQuery query, Integer limit, Integer timeout) {
		return new SelectSystemQueryExecuteCommand(query, false, limit, timeout);
	}


	public static SelectSystemQueryExecuteCommand forQueryDynamicSelectPreview(SystemQuery query) {
		return new SelectSystemQueryExecuteCommand(query, true, null);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getResultString() {
		String resultString = getRunDescription() == null ? "" : getRunDescription().toString();
		if (getResult() != null) {
			int rowCount = getResult().getTotalRowCount();
			if (rowCount > 0) {
				return rowCount + (rowCount == 1 ? " row" : " rows") + (resultString.isEmpty() ? "" : ": " + resultString);
			}
		}
		return resultString;
	}


	public String getDynamicSql() {
		return this.dynamicSql;
	}


	public void setDynamicSql(String dynamicSql) {
		this.dynamicSql = dynamicSql;
	}


	public int getLimit() {
		return this.limit;
	}


	public boolean isRetrieveDynamicSqlOnly() {
		return this.retrieveDynamicSqlOnly;
	}


	public int getQueryTimeout() {
		return this.queryTimeout;
	}


	public void setQueryTimeout(int queryTimeout) {
		this.queryTimeout = queryTimeout;
	}
}
