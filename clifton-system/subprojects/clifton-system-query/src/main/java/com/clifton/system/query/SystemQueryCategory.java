package com.clifton.system.query;

import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemDataSource;


/**
 * The SystemQueryCategory class is used to group {@link SystemQuery} objects that may require different security
 * access for different data sources.
 *
 * @author vgomelsky
 */
public class SystemQueryCategory extends NamedEntity<Short> {

	/**
	 * Optional way to limit all queries to only this data sources.
	 */
	private SystemDataSource dataSource;

	/**
	 * Must evaluate to true in order to be able to create or update queries for this category.
	 */
	private SystemCondition queryModifyCondition;

	/**
	 * Must evaluate to true in order to be able to execute queries for this category.
	 */
	private SystemCondition queryExecuteCondition;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(SystemDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public SystemCondition getQueryModifyCondition() {
		return this.queryModifyCondition;
	}


	public void setQueryModifyCondition(SystemCondition queryModifyCondition) {
		this.queryModifyCondition = queryModifyCondition;
	}


	public SystemCondition getQueryExecuteCondition() {
		return this.queryExecuteCondition;
	}


	public void setQueryExecuteCondition(SystemCondition queryExecuteCondition) {
		this.queryExecuteCondition = queryExecuteCondition;
	}
}
