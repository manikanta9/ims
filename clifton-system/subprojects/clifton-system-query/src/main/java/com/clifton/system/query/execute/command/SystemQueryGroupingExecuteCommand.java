package com.clifton.system.query.execute.command;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.system.query.SystemQueryGrouping;


/**
 * represents command - group relationship
 *
 * @author KellyJ
 */
public class SystemQueryGroupingExecuteCommand extends BaseSystemQueryExecuteCommand<DataTable> {

	private SystemQueryExecuteCommand<?> command;
	private SystemQueryGrouping group;


	private SystemQueryGroupingExecuteCommand(SystemQueryGrouping group, SystemQueryExecuteCommand<?> command) {
		super(command.getQuery());
		this.group = group;
		this.command = command;
	}


	public static SystemQueryGroupingExecuteCommand create(SystemQueryGrouping group, SystemQueryExecuteCommand<?> command) {
		return new SystemQueryGroupingExecuteCommand(group, command);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getResultString() {
		return this.command.getResultString();
	}


	@Override
	@ValueIgnoringGetter
	public DataTable getResult() {
		return (DataTable) this.command.getResult();
	}


	public int getOrder() {
		return this.group.getQueryOrder();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemQueryExecuteCommand<?> getCommand() {
		return this.command;
	}


	public void setCommand(SystemQueryExecuteCommand<?> command) {
		this.command = command;
	}


	public SystemQueryGrouping getGroup() {
		return this.group;
	}


	public void setGroup(SystemQueryGrouping group) {
		this.group = group;
	}
}
