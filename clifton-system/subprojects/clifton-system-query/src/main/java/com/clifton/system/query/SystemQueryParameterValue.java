package com.clifton.system.query;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.system.userinterface.SystemUserInterfaceValueAware;


/**
 * The <code>SystemQueryParameterValue</code> class represent a value for a specific system query parameters.
 * Values are not persisted in the database. They are used for query execution only and can either be passed
 * from UI or programmatically.
 *
 * @author Mary Anderson
 */
@NonPersistentObject
public class SystemQueryParameterValue implements SystemUserInterfaceValueAware<SystemQueryParameter> {

	private SystemQueryParameter parameter;

	private String value;
	private String text;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemQueryParameter getParameter() {
		return this.parameter;
	}


	public void setParameter(SystemQueryParameter parameter) {
		this.parameter = parameter;
	}


	@Override
	public String getValue() {
		return this.value;
	}


	@Override
	public void setValue(String value) {
		this.value = value;
	}


	@Override
	public String getText() {
		return this.text;
	}


	@Override
	public void setText(String text) {
		this.text = text;
	}
}
