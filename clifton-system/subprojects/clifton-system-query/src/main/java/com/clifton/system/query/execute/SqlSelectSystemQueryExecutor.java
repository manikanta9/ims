package com.clifton.system.query.execute;

import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.execute.command.SelectSystemQueryExecuteCommand;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;


/**
 * @author manderson
 */
public class SqlSelectSystemQueryExecutor extends BaseSystemQueryExecutor<DataTable, SelectSystemQueryExecuteCommand> {

	private DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator;

	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * If true, the query itself returns another select statement that needs to be executed
	 */
	private boolean dynamicSQL;

	/**
	 * If true, the query itself is evaluated using Freemarker first
	 * Indicates that the SQL itself needs to be evaluated using freemarker prior to executing. Each parameter is passed to the freemarker evaluation
	 * That result is the SQL to execute, which is executed with the parameters not flagged as excluded from execution
	 */
	private boolean evaluateFreemarker;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void executeSystemQueryCommand(SelectSystemQueryExecuteCommand queryCommand) {
		String sql = queryCommand.getQuery().getSqlStatement();
		Map<SystemQueryParameter, Object> parameterValueMap = getSystemQueryParameterValueMap(queryCommand);

		if (isEvaluateFreemarker()) {
			sql = evaluateFreemarker(sql, parameterValueMap);
		}
		SqlParameterValue[] sqlParams = getSqlParameterValues(parameterValueMap);
		if (isDynamicSQL()) {
			sql = getDynamicSql(queryCommand, sql, sqlParams);
			// Clear params after this is completed since dynamic SQL enters them into the statement
			sqlParams = null;
		}
		if (queryCommand.isRetrieveDynamicSqlOnly()) {
			queryCommand.setDynamicSql(sql);
		}
		else {
			queryCommand.setResult(getQueryResult(sql, queryCommand.getQuery().getDataSourceForExecution().getName(), sqlParams, queryCommand.getLimit(), queryCommand.getQueryTimeout()));
		}
	}


	private String evaluateFreemarker(String sql, Map<SystemQueryParameter, Object> parameterValueMap) {
		TemplateConfig config = new TemplateConfig(sql);
		if (!CollectionUtils.isEmpty(parameterValueMap)) {
			for (Map.Entry<SystemQueryParameter, Object> systemQueryParameterObjectEntry : parameterValueMap.entrySet()) {
				Object value = systemQueryParameterObjectEntry.getValue();
				if (value != null) {
					config.addBeanToContext((systemQueryParameterObjectEntry.getKey()).getName(), value);
				}
			}
		}
		return getTemplateConverter().convert(config);
	}


	private String getDynamicSql(SelectSystemQueryExecuteCommand queryCommand, String sql, SqlParameterValue[] parameters) {
		DataTable dynamicSqlResult = getQueryResult(sql, queryCommand.getQuery().getDataSourceForExecution().getName(), parameters, null, null);
		if (dynamicSqlResult == null || dynamicSqlResult.getTotalRowCount() == 0 || dynamicSqlResult.getColumnCount() == 0) {
			throw new ValidationException("No SQL generated to execute");
		}
		if (dynamicSqlResult.getTotalRowCount() != 1 && dynamicSqlResult.getColumnCount() != 1) {
			throw new ValidationException("Expected SQL generated to be one String value (1 row and 1 column result).  Result was [" + dynamicSqlResult.getTotalRowCount() + "] rows and [" + dynamicSqlResult.getColumnCount() + "] columns.");
		}
		Object dynamicSql = dynamicSqlResult.getRow(0).getValue(0);
		if (dynamicSql != null) {
			ValidationUtils.assertTrue(dynamicSql instanceof String, "Expected SQL generated to be a String value, but was " + dynamicSql.getClass().getName());
		}
		//noinspection
		return (String) dynamicSql;
	}


	@Transactional(timeout = 180)
	protected DataTable getQueryResult(String sql, String dataSourceName, SqlParameterValue[] parameters, Integer limit, Integer timeout) {
		return getDataTableRetrievalHandlerLocator().locate(dataSourceName).findDataTable(sql, parameters, SqlSelectCommand.DEFAULT_START, (limit == null ? SqlSelectCommand.DEFAULT_LIMIT : limit), (timeout == null ? SqlSelectCommand.DEFAULT_QUERY_TIMEOUT : timeout));
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////         System Query Parameter Value Helper Methods         //////////
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public DataTableRetrievalHandlerLocator getDataTableRetrievalHandlerLocator() {
		return this.dataTableRetrievalHandlerLocator;
	}


	public void setDataTableRetrievalHandlerLocator(DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator) {
		this.dataTableRetrievalHandlerLocator = dataTableRetrievalHandlerLocator;
	}


	public boolean isDynamicSQL() {
		return this.dynamicSQL;
	}


	public void setDynamicSQL(boolean dynamicSQL) {
		this.dynamicSQL = dynamicSQL;
	}


	public boolean isEvaluateFreemarker() {
		return this.evaluateFreemarker;
	}


	public void setEvaluateFreemarker(boolean evaluateFreemarker) {
		this.evaluateFreemarker = evaluateFreemarker;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}
}
