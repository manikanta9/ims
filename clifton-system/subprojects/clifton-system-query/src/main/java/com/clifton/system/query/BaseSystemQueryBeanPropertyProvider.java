package com.clifton.system.query;


import com.clifton.system.userinterface.BaseSystemUserInterfaceBeanPropertyProvider;

import java.util.List;


/**
 * The <code>BaseSystemQueryBeanPropertyProvider</code> provides a base implementation of SystemUserInterfacePropertyProvider
 * used to add a system query to bean and store the query parameter values in a target property on the database.
 *
 * @author mwacker
 */
public abstract class BaseSystemQueryBeanPropertyProvider extends BaseSystemUserInterfaceBeanPropertyProvider<SystemQueryParameter, SystemQueryParameterValue> {

	private Integer systemQueryId;

	////////////////////////////////////////////////////////////////////////////

	private SystemQuery systemQuery;


	private SystemQueryService systemQueryService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public List<SystemQueryParameter> doGetPropertyTypeList() {
		return getSystemQueryService().getSystemQueryParameterListByQuery(getSystemQueryId());
	}


	@Override
	public Class<SystemQueryParameterValue> getValueClass() {
		return SystemQueryParameterValue.class;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemQuery getSystemQuery() {
		if ((this.systemQuery == null) && (getSystemQueryId() != null)) {
			this.systemQuery = getSystemQueryService().getSystemQuery(getSystemQueryId());
		}
		return this.systemQuery;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemQueryService getSystemQueryService() {
		return this.systemQueryService;
	}


	public void setSystemQueryService(SystemQueryService systemQueryService) {
		this.systemQueryService = systemQueryService;
	}


	public Integer getSystemQueryId() {
		return this.systemQueryId;
	}


	public void setSystemQueryId(Integer systemQueryId) {
		this.systemQueryId = systemQueryId;
	}
}
