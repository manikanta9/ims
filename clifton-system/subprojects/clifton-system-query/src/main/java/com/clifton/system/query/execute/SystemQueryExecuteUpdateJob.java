package com.clifton.system.query.execute;


import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.query.SystemQuery;

import java.util.Map;


/**
 * The <code>SystemQueryExecuteUpdateJob</code> executes an IsUpdateStatement {@link SystemQuery}: UPDATE or DELETE.
 *
 * @author Mary Anderson
 */
public class SystemQueryExecuteUpdateJob implements Task {

	private Integer systemQueryId;

	/**
	 * Optional query timeout in seconds: overrides system wide default
	 */
	private Integer queryTimeout;

	/**
	 * Optionally specifies the maximum number of times that the System Query will be executed unless it returns 0 for affected rows.
	 * This is helpful to limit the number of deleted or updated rows in order to avoid a time out.
	 * For example, "DELETE TOP(1000000) FROM ..."
	 */
	private Integer maxExecutionCount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private SystemQueryExecutionService systemQueryExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		ValidationUtils.assertNotNull(getSystemQueryId(), "System Query selection is required");

		int executionsCount = 0;
		int maxExecutions = calculateMaxExecutions();
		int totalRowsAffected = 0;
		while (executionsCount < maxExecutions) {
			executionsCount++;
			int rowsAffected = getSystemQueryExecutionService().executeSystemQueryUpdate(getSystemQueryId(), getQueryTimeout());
			if (rowsAffected == 0) {
				// no more effected rows: stop execution
				break;
			}
			totalRowsAffected += rowsAffected;
		}

		return Status.ofMessage(CoreMathUtils.formatNumberInteger(totalRowsAffected) + " rows affected" + (executionsCount > 1 ? " in " + executionsCount + " executions" : ""), totalRowsAffected != 0);
	}


	protected int calculateMaxExecutions() {
		int executionCount = 1;
		if (getMaxExecutionCount() != null) {
			executionCount = getMaxExecutionCount();
			// do not allow fewer than 1 or greater than 1000 executions
			if (executionCount < 1) {
				executionCount = 1;
			}
			if (executionCount > 1000) {
				executionCount = 1000;
			}
		}
		return executionCount;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSystemQueryId() {
		return this.systemQueryId;
	}


	public void setSystemQueryId(Integer systemQueryId) {
		this.systemQueryId = systemQueryId;
	}


	public Integer getMaxExecutionCount() {
		return this.maxExecutionCount;
	}


	public void setMaxExecutionCount(Integer maxExecutionCount) {
		this.maxExecutionCount = maxExecutionCount;
	}


	public SystemQueryExecutionService getSystemQueryExecutionService() {
		return this.systemQueryExecutionService;
	}


	public void setSystemQueryExecutionService(SystemQueryExecutionService systemQueryExecutionService) {
		this.systemQueryExecutionService = systemQueryExecutionService;
	}


	public Integer getQueryTimeout() {
		return this.queryTimeout;
	}


	public void setQueryTimeout(Integer queryTimeout) {
		this.queryTimeout = queryTimeout;
	}
}
