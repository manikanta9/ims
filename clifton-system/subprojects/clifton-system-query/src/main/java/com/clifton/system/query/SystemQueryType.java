package com.clifton.system.query;

import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBean;


/**
 * The class <code>SystemQueryType</code> defines various types of System Queries.  The types define how the query can be executed,
 * i.e. SQL Select, SQL Update, Service Call Select
 *
 * @author manderson
 */
public class SystemQueryType extends NamedEntity<Short> {

	/**
	 * Each type can either be an "update" to modify data in the system or a "select" to retrieve data from the system
	 * Whether or not it's an update determines how and when the query can be called
	 */
	private boolean updateStatement;

	/**
	 * Used to indicate if the sql itself is manipulated based on parameters to generate sql to execute.
	 * Examples: Select of Select - the SQL is executed first to generate sql to execute
	 * Select with Freemarker - the SQL is passed through Freemarker to generate the sql to execute
	 * When used, admins can see the Preview SQL button on the export run window based on selected parameter values
	 */
	private boolean dynamicStatement;

	/**
	 * Indicates if the SQL definition for the SystemQuery is required.
	 * Grouping types do not require sql to be defined for the grouping placeholder as each child query will have it defined.
	 */
	private boolean sqlRequired;

	/**
	 * Indicates if the query type is a grouping query in which we can define a SystemQueryGrouping.
	 */
	private boolean groupingQuery;

	/**
	 * The SystemBean of Group System Query Executor that handles the execution of the query
	 */
	private SystemBean queryExecutorBean;

	/**
	 * Most cases, the "SQL" field contains SQL, for service calls, it will contain a json URL
	 * This field dynamically changes the label on the query window so it's clear what the field contains
	 */
	private String sqlLabel;


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isUpdateStatement() {
		return this.updateStatement;
	}


	public void setUpdateStatement(boolean updateStatement) {
		this.updateStatement = updateStatement;
	}


	public SystemBean getQueryExecutorBean() {
		return this.queryExecutorBean;
	}


	public void setQueryExecutorBean(SystemBean queryExecutorBean) {
		this.queryExecutorBean = queryExecutorBean;
	}


	public String getSqlLabel() {
		return this.sqlLabel;
	}


	public void setSqlLabel(String sqlLabel) {
		this.sqlLabel = sqlLabel;
	}


	public boolean isDynamicStatement() {
		return this.dynamicStatement;
	}


	public void setDynamicStatement(boolean dynamicStatement) {
		this.dynamicStatement = dynamicStatement;
	}


	public boolean isSqlRequired() {
		return this.sqlRequired;
	}


	public void setSqlRequired(boolean sqlRequired) {
		this.sqlRequired = sqlRequired;
	}


	public boolean isGroupingQuery() {
		return this.groupingQuery;
	}


	public void setGroupingQuery(boolean groupingQuery) {
		this.groupingQuery = groupingQuery;
	}
}
