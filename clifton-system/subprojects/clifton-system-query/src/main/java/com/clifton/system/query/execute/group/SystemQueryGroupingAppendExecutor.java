package com.clifton.system.query.execute.group;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryGrouping;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author KellyJ
 */
public class SystemQueryGroupingAppendExecutor extends BaseSystemQueryGroupingExecutor {

	public boolean ignoreMismatchingQueries;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected DataTable applyGrouping(List<DataTable> queryResults) {
		DataTable result = null;
		if (!CollectionUtils.isEmpty(queryResults)) {
			if (queryResults.size() == 1) {
				return queryResults.stream().findFirst().orElse(null);
			}
			else {
				// for now, make sure every query result has the same number of columns
				if (!isIgnoreMismatchingQueries()) {
					validateResults(queryResults);
				}

				// append the results
				result = append(queryResults);
			}
			if (isIgnoreMismatchingQueries()) {
				result.setVariableColumnCountPerRow(true);
			}
		}
		return result;
	}


	/**
	 * Generates a default parameter list.
	 * This default behavior generates a combined list using the child query parameters.
	 * Parameters are considered the same if they match on: (1) name (2) is required (3) value table (4) date type
	 * The parameters can be manually overridden by using the format ParameterName[QueryNameOverride].
	 * Example:  Q1:[1-date, 2-account]  Q2:[1-date, 2-type, 3-account] -> generates 1-date, 2-account, 3-type
	 * Manual Override Example to set different account values:  1-date, 2-account[Q1Alias], 3-account[Q2Alias], 4-type
	 *
	 * @param parentQuery
	 */
	@Override
	public List<SystemQueryParameter> getDefaultParameterList(SystemQuery parentQuery) {
		final List<SystemQueryParameter> result = new ArrayList<>();
		if (parentQuery != null) {
			// retrieve a list of parameters already defined for this query
			List<SystemQueryParameter> parentParameterList = getSystemQueryService().getSystemQueryParameterListByQuery(parentQuery.getId());

			// retrieve a list of child queries
			List<SystemQueryGrouping> childQueryList = parentQuery.getQueryGroupingList();

			// create a list of combined parameters
			Map<String, SystemQueryParameter> uniqueParameters = new HashMap<>();

			// put all the already defined parent parameters in the unique parameter list
			if (!CollectionUtils.isEmpty(parentParameterList)) {
				parentParameterList.forEach(param -> uniqueParameters.put(param.getName(), param));
			}

			if (!CollectionUtils.isEmpty(childQueryList)) {
				int order = CollectionUtils.isEmpty(uniqueParameters) ? 1 : uniqueParameters.values().stream().mapToInt(this::nullSafeParameterOrder).max().getAsInt() + 1;
				for (SystemQueryGrouping group : childQueryList) {
					String childQueryAlias = group.getQueryNameOverride();

					// retrieve the list of parameters for the child query
					List<SystemQueryParameter> childParameterList = getSystemQueryService().getSystemQueryParameterListByQuery(group.getChildQuery().getId());

					if (!CollectionUtils.isEmpty(childParameterList)) {
						// check if the child query parameters are already persisted in the parent query and keep track of parameter orders
						for (SystemQueryParameter childParameter : childParameterList) {
							String childQueryParameterAlias = formatAlias(childQueryAlias, childParameter.getName());
							String key = childParameter.getName();
							// check if the unique parameters contains the original parameter name
							if (uniqueParameters.containsKey(key) || uniqueParameters.containsKey(childQueryParameterAlias)) {
								// validate the parameter
								// - if the name is already in the map, check to see if it is the same parameter
								// - if it has the same name but different attributes, error
								SystemQueryParameter parentParameter = uniqueParameters.containsKey(key) ? uniqueParameters.get(key) : uniqueParameters.get(childQueryParameterAlias);
								if (!isSameParameter(parentParameter, childParameter, childQueryAlias)) {
									ValidationUtils.fail(String.format("Parameter [%s] already exists with the same name but different attributes.  Please look at the attributes for the following [%s]: %n%s", key, group.getChildQuery().getName(), getParameterDifferences(parentParameter, childParameter, childQueryParameterAlias)));
								}
							}
							else {
								uniqueParameters.put(childParameter.getName(), newParameter(childParameter, order++, parentQuery));
							}
						}
					}
				}
			}
			uniqueParameters.forEach((k, v) -> result.add(v));
		}

		result.sort(Comparator.comparingInt(this::nullSafeParameterOrder));
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected SystemQueryParameterValue findParameterValue(Map<String, SystemQueryParameterValue> parameterValueMap, SystemQueryParameter childQueryParameter, String alias) {
		String newParameterAlias = formatAlias(alias, childQueryParameter.getName());
		SystemQueryParameterValue parameterValue = parameterValueMap.get(newParameterAlias);
		if (parameterValue == null) {
			parameterValue = parameterValueMap.get(childQueryParameter.getName());
		}
		return createParameter(parameterValue, childQueryParameter);
	}


	/**
	 * Matches on Name or Alias, isRequired, Data Type, Value Table, Value List URL, Label, and UI Config
	 */
	@Override
	protected boolean isSameParameter(SystemQueryParameter parentParameter, SystemQueryParameter childQueryParameter, String alias) {
		return (parentParameter.getName().equalsIgnoreCase(childQueryParameter.getName()) || parentParameter.getName().equalsIgnoreCase(formatAlias(alias, childQueryParameter.getName()))) &&
				parentParameter.isRequired() == childQueryParameter.isRequired() &&
				((parentParameter.getDataType() == null && childQueryParameter.getDataType() == null) || (parentParameter.getDataType() != null && childQueryParameter.getDataType() != null && parentParameter.getDataType().getId().equals(childQueryParameter.getDataType().getId()))) &&
				((parentParameter.getValueTable() == null && childQueryParameter.getValueTable() == null) || (parentParameter.getValueTable() != null && childQueryParameter.getValueTable() != null && parentParameter.getValueTable().getId().equals(childQueryParameter.getValueTable().getId()))) &&
				StringUtils.isEqualIgnoreCase(parentParameter.getValueListUrl(), childQueryParameter.getValueListUrl()) &&
				(StringUtils.isEqualIgnoreCase(parentParameter.getLabel(), childQueryParameter.getLabel()) || StringUtils.isEqualIgnoreCase(parentParameter.getLabel(), formatAlias(alias, childQueryParameter.getLabel()))) &&
				StringUtils.isEqualIgnoreCase(parentParameter.getUserInterfaceConfig(), childQueryParameter.getUserInterfaceConfig());
	}


	/**
	 * Used to give a friendly message to the user so they know what to look at.
	 *
	 * @param parentParameter
	 * @param childQueryParameter
	 * @param childQueryAlias
	 */
	protected String getParameterDifferences(SystemQueryParameter parentParameter, SystemQueryParameter childQueryParameter, String childQueryAlias) {
		StringBuilder result = new StringBuilder();
		String delimiter = ",";
		if (!(parentParameter.getName().equalsIgnoreCase(childQueryParameter.getName()) || parentParameter.getName().equalsIgnoreCase(childQueryAlias))) {
			result.append("Parameter Name").append(delimiter);
		}

		if (parentParameter.isRequired() != childQueryParameter.isRequired()) {
			result.append("Required").append(delimiter);
		}

		if (!((parentParameter.getDataType() == null && childQueryParameter.getDataType() == null) || (parentParameter.getDataType() != null && childQueryParameter.getDataType() != null && parentParameter.getDataType().getId().equals(childQueryParameter.getDataType().getId())))) {
			result.append("Data Type").append(delimiter);
		}

		if (!((parentParameter.getValueTable() == null && childQueryParameter.getValueTable() == null) || (parentParameter.getValueTable() != null && childQueryParameter.getValueTable() != null && parentParameter.getValueTable().getId().equals(childQueryParameter.getValueTable().getId())))) {
			result.append("Value List Table").append(delimiter);
		}

		if (!StringUtils.isEqualIgnoreCase(parentParameter.getValueListUrl(), childQueryParameter.getValueListUrl())) {
			result.append("Value List URL").append(delimiter);
		}

		if (!StringUtils.isEqualIgnoreCase(parentParameter.getLabel(), childQueryParameter.getLabel())) {
			result.append("Parameter Label").append(delimiter);
		}

		if (!StringUtils.isEqualIgnoreCase(parentParameter.getUserInterfaceConfig(), childQueryParameter.getUserInterfaceConfig())) {
			result.append("UI Config").append(delimiter);
		}
		int index = result.lastIndexOf(delimiter);
		return index > 0 && result.length() >= index ? result.substring(0, index) : result.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Verifies the column counts, data types and column names are the same
	 */
	protected void validateResults(List<DataTable> queryResults) {
		DataTable table = queryResults.stream().findFirst().orElse(null);
		if (table != null) {
			// (1)validate column counts
			final int columnCount = table.getColumnCount();
			boolean isSame = queryResults.stream().allMatch(dataTable -> dataTable.getColumnCount() == columnCount);
			ValidationUtils.assertTrue(isSame, "Variable column counts is not supported.");

			// (2)validate data types
			// transpose the columns
			List<List<DataColumn>> transposedLists = new ArrayList<>();
			List<List<DataColumn>> dataColumnsLists = getDataColumns(queryResults);

			for (int i = 0; i < columnCount; i++) {
				List<DataColumn> transposedList = new ArrayList<>();
				for (List<DataColumn> dataColumnList : dataColumnsLists) {
					transposedList.add(dataColumnList.get(i));
				}
				transposedLists.add(transposedList);
			}
			// now compare the lists
			StringBuilder errorMessage = new StringBuilder();
			if (!CollectionUtils.isEmpty(transposedLists)) {
				for (List<DataColumn> dataColumnList : transposedLists) {
					if (!CollectionUtils.isEmpty(dataColumnList)) {
						DataColumn columnToCompare = dataColumnList.get(0);
						String columnName = columnToCompare.getColumnName();
						int queryIndex = 1;
						for (DataColumn dataColumn : dataColumnList) {
							if (dataColumn != null) {
								if (!isCompatibleDataTypes(dataColumn, columnToCompare)) {
									errorMessage.append(String.format("[%s] DataType in query [%d] mismatch - expected [%s] but was [%s]%n", columnName, queryIndex, columnToCompare.getDataTypeName(), dataColumn.getDataTypeName()));
								}
								if (!dataColumn.getColumnName().equalsIgnoreCase(columnToCompare.getColumnName())) {
									errorMessage.append(String.format("[%s] ColumnName in query [%d] mismatch - expected [%s] but was [%s]%n", columnName, queryIndex, columnToCompare.getColumnName(), dataColumn.getColumnName()));
								}
							}
							queryIndex++;
						}
					}
				}
			}
			ValidationUtils.assertEmpty(errorMessage.toString(), "Data Types and Names of columns must match: \n" + errorMessage.toString());
		}
	}


	/**
	 * Returns true if the specified columns have the same data type or equivalent data types: VARCHAR and NVARCHAR, ...
	 */
	private boolean isCompatibleDataTypes(DataColumn column1, DataColumn column2) {
		if (column1.getDataType() == column2.getDataType()) {
			return true;
		}
		// can replace the Set with a List of Sets if it becomes necessary to add supports for additional compatible data type sets.
		if (compatibleDataTypes.contains(column1.getDataTypeName()) && compatibleDataTypes.contains(column2.getDataTypeName())) {
			return true;
		}

		return false;
	}


	private static final Set<String> compatibleDataTypes = CollectionUtils.createHashSet("VARCHAR", "NVARCHAR");


	// handles the case where we have all NULL values and SQL Server defaults the data type to int ('NULL' is in the select statement)
	// this causes data type matching failures since we are actually expecting a different data type
	private List<List<DataColumn>> getDataColumns(List<DataTable> queryResults) {
		List<List<DataColumn>> resultList = new ArrayList<>();
		Map<String, Integer> columnNameToDataType = new HashMap<>();  // map to store the column name to the appropriate data type
		Map<Integer, List<Integer>> indexesToPopulate = new HashMap<>();  // map of dataTableIndex to a list of columnIndex - used to keep track of what we need to go back and replace
		Integer dataTableIndex = 0;

		// for each query result (DataTable)
		for (DataTable t : queryResults) {
			List<DataColumn> dataColumnList = new ArrayList<>();
			int rowCount = t.getTotalRowCount();
			int dataColumnIndex = 0;

			// for each column in a query result
			for (DataColumn dataColumn : t.getColumnList()) {
				String columnName = dataColumn.getColumnName();
				boolean containsNonNullValue = false;

				// for each row in that column (for all the values in that column)
				for (int i = 0; i < rowCount; i++) {
					DataRow row = t.getRow(i);

					// find the first non-null value and use that data type for all the null values when the entire column is null
					if (row.getValue(dataColumn) != null) {
						containsNonNullValue = true;
						columnNameToDataType.putIfAbsent(columnName, dataColumn.getDataType());
						break;
					}
				}

				// we cycled through all the rows for the current column - now determine if they are all null and what we need to replace
				DataColumn resultColumn = dataColumn;
				if (!containsNonNullValue) {
					// the values are all null - replace if we can otherwise keep track of where it is so we can replace it
					if (columnNameToDataType.containsKey(columnName)) {
						int correctDataType = columnNameToDataType.get(columnName);
						if (correctDataType != dataColumn.getDataType()) {
							// we know what the data type is so create a new column with the correct data type and add it to the list
							resultColumn = new DataColumnImpl(columnName, columnNameToDataType.get(columnName), dataColumn.getDescription());
						}
					}
					else {
						// we don't know what the data type is at this point, so keep track of what we need to replace
						indexesToPopulate.computeIfAbsent(dataTableIndex, k -> new ArrayList<>()).add(dataColumnIndex);
					}
				}
				dataColumnList.add(resultColumn);
				dataColumnIndex++;
			}
			resultList.add(dataColumnList);
			dataTableIndex++;
		}

		// we cycled through all the query results, now replace anything we missed
		if (!indexesToPopulate.isEmpty()) {
			for (Map.Entry<Integer, List<Integer>> entry : indexesToPopulate.entrySet()) {
				List<DataColumn> dataColumns = resultList.get(entry.getKey());  // the resulting list of dataColumns for a DataTable

				// for each columnIndex of the current dataTableIndex, replace the column with the appropriate data type
				for (Integer columnKey : entry.getValue()) {
					DataColumn currentColumn = dataColumns.get(columnKey);
					String columnName = currentColumn.getColumnName();
					String description = currentColumn.getDescription();

					// only if the map contains the column replace it
					// the map will not have it if for every query the column as all null values
					if (columnNameToDataType.containsKey(columnName)) {
						DataColumn newColumn = new DataColumnImpl(columnName, columnNameToDataType.get(columnName), description);
						dataColumns.remove(columnKey.intValue());
						dataColumns.add(columnKey, newColumn);
					}
				}
			}
		}
		return resultList;
	}


	private int nullSafeParameterOrder(SystemQueryParameter parameter) {
		return parameter.getParameterOrder() != null ? parameter.getParameterOrder() : 1;
	}


	protected DataTable append(List<DataTable> queryResults) {
		DataTable result = null;
		for (DataTable table : queryResults) {
			if (result == null) {
				result = table;
			}
			else {
				for (int i = 0; i < table.getTotalRowCount(); i++) {
					result.addRow(table.getRow(i));
				}
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////               Getter and Setter Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isIgnoreMismatchingQueries() {
		return this.ignoreMismatchingQueries;
	}


	public void setIgnoreMismatchingQueries(boolean ignoreMismatchingQueries) {
		this.ignoreMismatchingQueries = ignoreMismatchingQueries;
	}
}
