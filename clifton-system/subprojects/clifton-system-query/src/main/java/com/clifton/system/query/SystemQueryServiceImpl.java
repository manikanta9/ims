package com.clifton.system.query;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.query.execute.group.SystemQueryGroupingExecutor;
import com.clifton.system.query.search.SystemQueryRunHistorySearchForm;
import com.clifton.system.query.search.SystemQuerySearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class SystemQueryServiceImpl implements SystemQueryService {

	private AdvancedUpdatableDAO<SystemQuery, Criteria> systemQueryDAO;
	private AdvancedUpdatableDAO<SystemQueryParameter, Criteria> systemQueryParameterDAO;
	private AdvancedUpdatableDAO<SystemQueryRunHistory, Criteria> systemQueryRunHistoryDAO;
	private AdvancedUpdatableDAO<SystemQueryCategory, Criteria> systemQueryCategoryDAO;
	private AdvancedUpdatableDAO<SystemQueryType, Criteria> systemQueryTypeDAO;
	private AdvancedUpdatableDAO<SystemQueryGrouping, Criteria> systemQueryGroupingDAO;

	private SystemBeanService systemBeanService;

	private DaoNamedEntityCache<SystemQuery> systemQueryCache;


	////////////////////////////////////////////////////////////////////////////////
	//////////          SystemQueryCategory Business Methods              //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemQueryCategory getSystemQueryCategory(short id) {
		return getSystemQueryCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemQueryCategory> getSystemQueryCategoryList() {
		return getSystemQueryCategoryDAO().findAll();
	}


	@Override
	public SystemQueryCategory saveSystemQueryCategory(SystemQueryCategory bean) {
		if (!bean.isNewBean() && bean.getDataSource() != null) {
			SystemQuerySearchForm searchForm = new SystemQuerySearchForm();
			searchForm.setQueryCategoryId(bean.getId());
			searchForm.setExcludeDataSourceId(bean.getDataSource().getId());
			List<SystemQuery> list = getSystemQueryList(searchForm);
			ValidationUtils.assertEmpty(list, CollectionUtils.toString(list, 3), "dataSource");
		}
		return getSystemQueryCategoryDAO().save(bean);
	}


	@Override
	public void deleteSystemQueryCategory(short id) {
		getSystemQueryCategoryDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////            SystemQueryType Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemQueryType getSystemQueryType(short id) {
		return getSystemQueryTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemQueryType> getSystemQueryTypeList() {
		return getSystemQueryTypeDAO().findAll();
	}


	@Override
	public SystemQueryType saveSystemQueryType(SystemQueryType bean) {
		if (bean.isUpdateStatement()) {
			ValidationUtils.assertFalse(bean.isDynamicStatement(), "Update statements cannot be dynamic statements.");
		}
		if (bean.isGroupingQuery()) {
			ValidationUtils.assertFalse(bean.isSqlRequired(), "Grouping Queries cannot have a SQL definition.");
		}
		return getSystemQueryTypeDAO().save(bean);
	}


	@Override
	public void deleteSystemQueryType(short id) {
		getSystemQueryTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////           SystemQuery Business Methods                 //////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * NOTE: Special case for security - we don't usually dynamically secure reads, but query exports can be used to report
	 * on sensitive areas on the system so these must be secured at the get/read level for individual queries
	 */
	@Override
	public SystemQuery getSystemQuery(int id) {
		SystemQuery systemQuery = getSystemQueryDAO().findByPrimaryKey(id);
		if (systemQuery != null && systemQuery.getQueryType() != null && systemQuery.getQueryType().isGroupingQuery()) {
			systemQuery.setQueryGroupingList(getSystemQueryGroupingListForParent(id));
		}
		return systemQuery;
	}


	@Override
	public SystemQuery getSystemQueryByName(String name) {
		ValidationUtils.assertNotNull(name, "Name value is required");
		return getSystemQueryCache().getBeanForKeyValueStrict(getSystemQueryDAO(), name);
	}


	@Override
	public List<SystemQuery> getSystemQueryList(SystemQuerySearchForm searchForm) {
		return getSystemQueryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<SystemQuery> getSystemQueryListByTable(final short tableId, SystemQuerySearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(Restrictions.eq("table.id", tableId));
				super.configureCriteria(criteria);
			}
		};
		return getSystemQueryDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	@Transactional
	public SystemQuery saveSystemQuery(SystemQuery query) {
		if (query != null) {
			if (query.getQueryType() != null && query.getQueryType().isSqlRequired() && StringUtils.isEmpty(query.getSqlStatement())) {
				ValidationUtils.fail("SQL is required for this Query Type");
			}

			ValidationUtils.assertMutuallyExclusive("Either Main Table or Data Source should be specified but not both.", query.getDataSource(), query.getTable());

			ValidationUtils.assertNotNull(query.getQueryCategory(), "Query Category is required.", "queryCategory");
			if (query.getQueryCategory().getDataSource() != null) {
				ValidationUtils.assertEquals(query.getQueryCategory().getDataSource(), query.getDataSourceForExecution(), "Query Category Data Source must match the specified Data Source.");
			}

			if (!StringUtils.isEmpty(query.getSqlStatement())) {
				int noCountOnIndex = query.getSqlStatement().indexOf(SqlSelectCommand.SET_NO_COUNT_ON);
				while (noCountOnIndex >= 0) {
					int noCountOffIndex = query.getSqlStatement().indexOf(SqlSelectCommand.SET_NO_COUNT_OFF, noCountOnIndex);
					if (noCountOffIndex == -1) {
						throw new ValidationException("Invalid SQL.  '" + SqlSelectCommand.SET_NO_COUNT_ON + "' found at position " + noCountOnIndex + ", but there is NOT a matching '" + SqlSelectCommand.SET_NO_COUNT_OFF + "' after that position.");
					}
					else {
						noCountOnIndex = query.getSqlStatement().indexOf(SqlSelectCommand.SET_NO_COUNT_ON, noCountOffIndex);
					}
				}
			}

			List<SystemQueryGrouping> queryGroupingList = query.getQueryGroupingList();
			query = getSystemQueryDAO().save(query);
			query.setQueryGroupingList(queryGroupingList);
			saveSystemQueryGrouping(query);
		}

		return query;
	}


	@Override
	@Transactional
	public void copySystemQuery(int id, String name) {
		SystemQuery bean = getSystemQuery(id);
		SystemQuery newBean = BeanUtils.cloneBean(bean, false, false);
		newBean.setName(name);
		saveSystemQuery(newBean);

		List<SystemQueryParameter> parameterList = getSystemQueryParameterListByQuery(id);
		if (!CollectionUtils.isEmpty(parameterList)) {
			List<SystemQueryParameter> newParameterList = new ArrayList<>();
			for (SystemQueryParameter parameter : parameterList) {
				SystemQueryParameter newParameter = BeanUtils.cloneBean(parameter, false, false);
				newParameter.setQuery(newBean);
				newParameterList.add(newParameter);
			}
			getSystemQueryParameterDAO().saveList(newParameterList);
		}
	}


	@Override
	@Transactional
	public void deleteSystemQuery(int id) {
		getSystemQueryParameterDAO().deleteList(getSystemQueryParameterListByQuery(id));
		getSystemQueryDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////         SystemQueryParameter Business Methods              //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemQueryParameter getSystemQueryParameter(int id) {
		return getSystemQueryParameterDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemQueryParameter> getSystemQueryParameterListByQuery(int queryId) {
		return getSystemQueryParameterDAO().findByField("query.id", queryId);
	}


	@Override
	@Transactional
	public List<SystemQueryParameter> generateSystemQueryDefaultParameterList(int queryId) {
		List<SystemQueryParameter> generatedList = null;
		SystemQuery query = getSystemQuery(queryId);
		if (query != null && query.getQueryType().isGroupingQuery()) {
			SystemQueryGroupingExecutor executorBean = (SystemQueryGroupingExecutor) getSystemBeanService().getBeanInstance(query.getQueryType().getQueryExecutorBean());
			generatedList = executorBean.getDefaultParameterList(query);
			if (!CollectionUtils.isEmpty(generatedList)) {
				generatedList.forEach(this::saveSystemQueryParameter);
			}
		}
		return generatedList;
	}


	@Override
	public SystemQueryParameter saveSystemQueryParameter(SystemQueryParameter query) {
		return getSystemQueryParameterDAO().save(query);
	}


	@Override
	public void deleteSystemQueryParameter(int id) {
		getSystemQueryParameterDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////         SystemQueryRunHistory Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemQueryRunHistory getSystemQueryRunHistory(int id) {
		return getSystemQueryRunHistoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemQueryRunHistory> getSystemQueryRunHistoryList(SystemQueryRunHistorySearchForm searchForm) {
		return getSystemQueryRunHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemQueryRunHistory saveSystemQueryRunHistory(SystemQueryRunHistory bean) {
		return getSystemQueryRunHistoryDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////         SystemQueryGrouping Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemQueryGrouping getSystemQueryGrouping(int id) {
		return getSystemQueryGroupingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemQueryGrouping> getSystemQueryGroupingListForParent(int parentQueryId) {
		return getSystemQueryGroupingDAO().findByField("parentQuery.id", parentQueryId);
	}


	/**
	 * Currently only non-grouping select SystemQuery definitions are returned until the filtering logic is implemented.
	 * <p>
	 * Used when maintaining SystemQueryGroupings.  Finds SystemQueries with matching SystemQueryParameter values.
	 * Compares (1) QueryParameterName, (2) SystemDataType, (3) ValueSystemDataTable, and (4) IsRequired
	 * and returns the SystemQuery if the parameter fields match for all the required parameters.
	 * If no queryId is specified all SystemQuery definitions are returned.
	 *
	 * @param queryId
	 */
	@Override
	public List<SystemQuery> getSystemQueryListForGroupingSimilar(Integer queryId) {
		// TODO add logic to filter out the query list so only similar queries are returned
		SystemQuerySearchForm form = new SystemQuerySearchForm();
		form.setGroupingQuery(false);
		form.setUpdateStatement(false);
		form.setSqlRequired(true);
		return getSystemQueryList(form);
	}


	protected void saveSystemQueryGroupingList(List<SystemQueryGrouping> newList, List<SystemQueryGrouping> oldList) {
		getSystemQueryGroupingDAO().saveList(newList, oldList);
	}


	private void saveSystemQueryGrouping(SystemQuery query) {
		if (query != null && query.getQueryType() != null && query.getQueryType().isGroupingQuery()) {
			List<SystemQueryGrouping> originalGroupingList = getSystemQueryGroupingListForParent(query.getId());
			List<SystemQueryGrouping> groupingList = query.getQueryGroupingList();
			if (!CollectionUtils.isEmpty(groupingList)) {
				groupingList.forEach(group -> group.setParentQuery(query));
			}
			saveSystemQueryGroupingList(groupingList, originalGroupingList);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////               Getter and Setter Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemQuery, Criteria> getSystemQueryDAO() {
		return this.systemQueryDAO;
	}


	public void setSystemQueryDAO(AdvancedUpdatableDAO<SystemQuery, Criteria> systemQueryDAO) {
		this.systemQueryDAO = systemQueryDAO;
	}


	public AdvancedUpdatableDAO<SystemQueryParameter, Criteria> getSystemQueryParameterDAO() {
		return this.systemQueryParameterDAO;
	}


	public void setSystemQueryParameterDAO(AdvancedUpdatableDAO<SystemQueryParameter, Criteria> systemQueryParameterDAO) {
		this.systemQueryParameterDAO = systemQueryParameterDAO;
	}


	public AdvancedUpdatableDAO<SystemQueryRunHistory, Criteria> getSystemQueryRunHistoryDAO() {
		return this.systemQueryRunHistoryDAO;
	}


	public void setSystemQueryRunHistoryDAO(AdvancedUpdatableDAO<SystemQueryRunHistory, Criteria> systemQueryRunHistoryDAO) {
		this.systemQueryRunHistoryDAO = systemQueryRunHistoryDAO;
	}


	public AdvancedUpdatableDAO<SystemQueryCategory, Criteria> getSystemQueryCategoryDAO() {
		return this.systemQueryCategoryDAO;
	}


	public void setSystemQueryCategoryDAO(AdvancedUpdatableDAO<SystemQueryCategory, Criteria> systemQueryCategoryDAO) {
		this.systemQueryCategoryDAO = systemQueryCategoryDAO;
	}


	public AdvancedUpdatableDAO<SystemQueryType, Criteria> getSystemQueryTypeDAO() {
		return this.systemQueryTypeDAO;
	}


	public void setSystemQueryTypeDAO(AdvancedUpdatableDAO<SystemQueryType, Criteria> systemQueryTypeDAO) {
		this.systemQueryTypeDAO = systemQueryTypeDAO;
	}


	public AdvancedUpdatableDAO<SystemQueryGrouping, Criteria> getSystemQueryGroupingDAO() {
		return this.systemQueryGroupingDAO;
	}


	public void setSystemQueryGroupingDAO(AdvancedUpdatableDAO<SystemQueryGrouping, Criteria> systemQueryGroupingDAO) {
		this.systemQueryGroupingDAO = systemQueryGroupingDAO;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public DaoNamedEntityCache<SystemQuery> getSystemQueryCache() {
		return this.systemQueryCache;
	}


	public void setSystemQueryCache(DaoNamedEntityCache<SystemQuery> systemQueryCache) {
		this.systemQueryCache = systemQueryCache;
	}
}
