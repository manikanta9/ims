package com.clifton.system.query.execute;

import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlHandlerLocator;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.execute.command.UpdateSystemQueryExecuteCommand;

import java.sql.PreparedStatement;
import java.util.Map;


/**
 * @author manderson
 */
public class SqlUpdateSystemQueryExecutor extends BaseSystemQueryExecutor<Integer, UpdateSystemQueryExecuteCommand> {

	private SqlHandlerLocator sqlHandlerLocator;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void executeSystemQueryCommand(UpdateSystemQueryExecuteCommand command) {
		Map<SystemQueryParameter, Object> parameterValueMap = getSystemQueryParameterValueMap(command);
		SqlParameterValue[] sqlParams = getSqlParameterValues(parameterValueMap);

		SqlHandler sqlHandler = getSqlHandlerLocator().locate(command.getQuery().getDataSourceForExecution().getName());

		// need call back in order to override default statement timeout settings from applied from the template
		int result = sqlHandler.executeUpdate(command.getQuery().getSqlStatement(), sqlParams, (PreparedStatement ps) -> {
			if (command.getTimeOutInSeconds() != null) {
				ps.setQueryTimeout(command.getTimeOutInSeconds());
			}
			return ps.executeUpdate();
		});
		command.setResult(result);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SqlHandlerLocator getSqlHandlerLocator() {
		return this.sqlHandlerLocator;
	}


	public void setSqlHandlerLocator(SqlHandlerLocator sqlHandlerLocator) {
		this.sqlHandlerLocator = sqlHandlerLocator;
	}
}
