package com.clifton.system.query.execute;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.execute.command.QueryCommand;


/**
 * @author manderson
 */
public interface SystemQueryExecutionService {


	/**
	 * Useful for admins when generating dynamic sql select statements to see the sql that is generated.  Can be helpful when trying to debug an issue
	 * with the statement
	 * <p>
	 * Executes the sql using the given parameter values to generate the real sql select to return.
	 * Query cannot be flagged as an update statement, and must be flagged as dynamicStatement
	 */
	public String getSystemQueryDynamicStatement(SystemQuery query);


	/**
	 * Executes {@link SystemQuery} with the specified id and returns results returned by the query.
	 * <p>
	 * Query cannot be flagged as an update statement.
	 */
	@SecureMethod(dynamicTableNameBeanPath = "table.name")
	public DataTable getSystemQueryResult(SystemQuery query);


	/**
	 * Same as getSystemQueryResult, but the query timeout (seconds) can be overridden
	 */
	@SecureMethod(dynamicTableNameBeanPath = "table.name", dtoClass = SystemQuery.class)
	public DataTable getSystemQueryResultWithTimeout(SystemQuery query, int timeout);


	/**
	 * Same as getSystemQueryResult, but max rows defined by limit
	 * <p>
	 * Currently not actually used for real execution, but for integration tests, we can set limit to 1 to ensure sql executes correctly
	 */
	@SecureMethod(dynamicTableNameBeanPath = "table.name", dtoClass = SystemQuery.class)
	public DataTable getSystemQueryResultWithLimit(SystemQuery query, int limit);


	/**
	 * Same as getSystemQueryResult, but max rows defined by limit and query timeout (seconds)
	 */
	@SecureMethod(dynamicTableNameBeanPath = "table.name", dtoClass = SystemQuery.class)
	public DataTable getSystemQueryResultWithLimitAndTimeout(SystemQuery query, int limit, int timeout);


	/**
	 * Executes the {@link SystemQuery} identified by the specified command and returns results produced by the query.
	 */
	@SecureMethod(tableResolverBeanName = "systemQueryCommandSecureMethodTableResolver")
	public DataTable getSystemQueryResultUsingCommand(QueryCommand queryCommand);


	/**
	 * Executes (@link SystemQuery} with the specified id and returns the number of rows affected by the update.
	 * <p>
	 * Query MUST be flagged as an update statement.
	 *
	 * @param timeoutInSeconds optional query timeout in seconds that overrides system wide default.
	 *                         NOTE: if this method is called inside of Transaction, then the timeout will be ignored
	 */
	@DoNotAddRequestMapping
	public int executeSystemQueryUpdate(int id, Integer timeoutInSeconds);


	/**
	 * Executes (@link SystemQuery} that is passed in
	 * <p>
	 * Query MUST be flagged as an update statement.
	 *
	 * @param timeoutInSeconds optional query timeout in seconds that overrides system wide default.
	 *                         NOTE: if this method is called inside of Transaction, then the timeout will be ignored
	 */
	@DoNotAddRequestMapping
	public int executeSystemQueryUpdate(SystemQuery query, Integer timeoutInSeconds);
}
