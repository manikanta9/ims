package com.clifton.system.query.execute;

import com.clifton.system.query.execute.command.SystemQueryExecuteCommand;


/**
 * The SystemQueryExecutor interface defines the methods for executing queries via a command.  Currently there are two types of commands, SelectSystemQueryExecuteCommand, and UpdateSystemQueryExecuteCommand
 *
 * @author manderson
 */
public interface SystemQueryExecutor<R, T extends SystemQueryExecuteCommand<R>> {


	/**
	 * Execute the SystemQuery and populates the command object with the results.  For Selects, this would be the DataTable that is converted to Excel or PDF file
	 * for the request, for Updates, it's an integer of the number of rows updated
	 */
	public void executeSystemQueryCommand(T queryCommand);
}
