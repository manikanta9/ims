package com.clifton.system.query.execute.command;

import com.clifton.system.query.SystemQuery;


/**
 * UpdateSystemQueryExecuteCommand is an SystemQueryExecuteCommand used for executing Updates where the result
 * property is an Integer = Row Count of Rows affected by the update
 *
 * @author manderson
 */
public class UpdateSystemQueryExecuteCommand extends BaseSystemQueryExecuteCommand<Integer> {

	private final Integer timeOutInSeconds;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private UpdateSystemQueryExecuteCommand(SystemQuery query, Integer timeOutInSeconds) {
		super(query);
		this.timeOutInSeconds = timeOutInSeconds;
	}


	public static UpdateSystemQueryExecuteCommand forQueryWithTimeout(SystemQuery query, Integer timeOutInSeconds) {
		return new UpdateSystemQueryExecuteCommand(query, timeOutInSeconds);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getResultString() {
		if (getResult() != null && getResult() > 0) {
			return getResult() + " row(s)";
		}
		return null;
	}


	public Integer getTimeOutInSeconds() {
		return this.timeOutInSeconds;
	}
}
