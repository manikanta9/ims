package com.clifton.system.query;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.ObjectUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;

import java.util.List;


/**
 * The <code>SystemQuery</code> class defines an action that can be executed to either update data, or retrieve data from the system.
 * i.e. SQL Selects, SQL Updates, Service Call (Get List or DataTable urls)
 *
 * @author vgomelsky
 */
@CacheByName
public class SystemQuery extends NamedEntity<Integer> implements SystemEntityModifyConditionAwareAdminRequired {

	public static final String TABLE_NAME = "SystemQuery";


	/**
	 * Groups similar queries that may require different security access for different data sources.
	 */
	private SystemQueryCategory queryCategory;

	/**
	 * Defines how the query is executed: Select, Update, Service Call, Select with Freemarker, Query Grouping, etc.
	 */
	private SystemQueryType queryType;

	/**
	 * Primary table that the query is based on. The table defines query's data source.
	 */
	private SystemTable table;

	/**
	 * When no table is present, use this data source.
	 */
	private SystemDataSource dataSource;

	/**
	 * For "SQL" type queries, this is SQL, for Other types, this could be a json url or other data.
	 * See the query type for additional information, which also can dynamically change the "SQL" label on screen
	 * to something more descriptive for that specific type (i.e. URL)
	 */
	private String sqlStatement;

	/**
	 * Allows to disable capture of historical executions. This will slightly improve performances.
	 */
	private boolean runHistoryDisabled;


	/**
	 * Value higher than this triggers SLA
	 */
	private Integer maxExecutionTimeMillis;


	/**
	 * Used when executing this query with parameter values set
	 * parameter values aren't saved in the database, set when running
	 */
	private List<SystemQueryParameterValue> parameterValueList;

	/**
	 * Used when this query is a representation of multiple queries that are combined and executed together.
	 * This is null when the SystemQuery is not a grouping type query.
	 */
	private List<SystemQueryGrouping> queryGroupingList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemDataSource getDataSourceForExecution() {
		return getTable() == null ? getDataSource() : ObjectUtils.coalesce(getDataSource(), getTable().getDataSource());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getQueryCategory() == null ? null : getQueryCategory().getQueryModifyCondition();
	}


	@Override
	public String getLabel() {
		if (this.table == null) {
			return super.getLabel();
		}
		return super.getLabel() + " (" + this.table.getLabel() + ")";
	}


	public String getSqlStatement() {
		return this.sqlStatement;
	}


	public void setSqlStatement(String sqlStatement) {
		this.sqlStatement = sqlStatement;
	}


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public List<SystemQueryParameterValue> getParameterValueList() {
		return this.parameterValueList;
	}


	public void setParameterValueList(List<SystemQueryParameterValue> parameterValueList) {
		this.parameterValueList = parameterValueList;
	}


	public boolean isRunHistoryDisabled() {
		return this.runHistoryDisabled;
	}


	public void setRunHistoryDisabled(boolean runHistoryDisabled) {
		this.runHistoryDisabled = runHistoryDisabled;
	}


	public Integer getMaxExecutionTimeMillis() {
		return this.maxExecutionTimeMillis;
	}


	public void setMaxExecutionTimeMillis(Integer maxExecutionTimeMillis) {
		this.maxExecutionTimeMillis = maxExecutionTimeMillis;
	}


	public SystemQueryCategory getQueryCategory() {
		return this.queryCategory;
	}


	public void setQueryCategory(SystemQueryCategory queryCategory) {
		this.queryCategory = queryCategory;
	}


	public SystemQueryType getQueryType() {
		return this.queryType;
	}


	public void setQueryType(SystemQueryType queryType) {
		this.queryType = queryType;
	}


	public List<SystemQueryGrouping> getQueryGroupingList() {
		return this.queryGroupingList;
	}


	public void setQueryGroupingList(List<SystemQueryGrouping> queryGroupingList) {
		this.queryGroupingList = queryGroupingList;
	}


	public SystemDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(SystemDataSource dataSource) {
		this.dataSource = dataSource;
	}
}
