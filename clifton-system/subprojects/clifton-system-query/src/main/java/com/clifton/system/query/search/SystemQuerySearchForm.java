package com.clifton.system.query.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;
import com.clifton.system.query.SystemQuery;


public class SystemQuerySearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField(searchField = "name,description,sqlStatement")
	private String searchPatternAdvanced;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "table.id")
	private Short tableId;

	@SearchField(searchFieldPath = "table", searchField = "name")
	private String tableName;

	@SearchField(searchFieldPath = "table.securityResource", searchField = "label")
	private String securityResourceName;

	@SearchField(searchField = "dataSource.id,table.dataSource.id", leftJoin = true)
	private Short dataSourceId;

	@SearchField(searchFieldPath = "table", searchField = "dataSource.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short excludeDataSourceId;

	@SearchField(searchFieldPath = "queryType", searchField = "updateStatement")
	private Boolean updateStatement;

	@SearchField(searchFieldPath = "queryType", searchField = "dynamicStatement")
	private Boolean dynamicStatement;

	@SearchField(searchFieldPath = "queryType", searchField = "sqlRequired")
	private Boolean sqlRequired;

	@SearchField(searchFieldPath = "queryType", searchField = "groupingQuery")
	private Boolean groupingQuery;

	@SearchField(searchField = "queryCategory.id")
	private Short queryCategoryId;

	@SearchField(searchField = "queryType.id")
	private Short queryTypeId;

	@SearchField
	private Boolean runHistoryDisabled;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return SystemQuery.TABLE_NAME;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Boolean getUpdateStatement() {
		return this.updateStatement;
	}


	public void setUpdateStatement(Boolean updateStatement) {
		this.updateStatement = updateStatement;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getSearchPatternAdvanced() {
		return this.searchPatternAdvanced;
	}


	public void setSearchPatternAdvanced(String searchPatternAdvanced) {
		this.searchPatternAdvanced = searchPatternAdvanced;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getSecurityResourceName() {
		return this.securityResourceName;
	}


	public void setSecurityResourceName(String securityResourceName) {
		this.securityResourceName = securityResourceName;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public Short getExcludeDataSourceId() {
		return this.excludeDataSourceId;
	}


	public void setExcludeDataSourceId(Short excludeDataSourceId) {
		this.excludeDataSourceId = excludeDataSourceId;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Boolean getDynamicStatement() {
		return this.dynamicStatement;
	}


	public void setDynamicStatement(Boolean dynamicStatement) {
		this.dynamicStatement = dynamicStatement;
	}


	public Boolean getSqlRequired() {
		return this.sqlRequired;
	}


	public void setSqlRequired(Boolean sqlRequired) {
		this.sqlRequired = sqlRequired;
	}


	public Boolean getGroupingQuery() {
		return this.groupingQuery;
	}


	public void setGroupingQuery(Boolean groupingQuery) {
		this.groupingQuery = groupingQuery;
	}


	public Short getQueryCategoryId() {
		return this.queryCategoryId;
	}


	public void setQueryCategoryId(Short queryCategoryId) {
		this.queryCategoryId = queryCategoryId;
	}


	public Short getQueryTypeId() {
		return this.queryTypeId;
	}


	public void setQueryTypeId(Short queryTypeId) {
		this.queryTypeId = queryTypeId;
	}


	public Boolean getRunHistoryDisabled() {
		return this.runHistoryDisabled;
	}


	public void setRunHistoryDisabled(Boolean runHistoryDisabled) {
		this.runHistoryDisabled = runHistoryDisabled;
	}
}
