package com.clifton.system.query.execute.command;

import com.clifton.system.query.SystemQuery;


/**
 * SystemQueryExecuteCommand interface defines the basic common methods for executing system queries
 *
 * @author manderson
 */
public interface SystemQueryExecuteCommand<R> {

	public SystemQuery getQuery();


	/**
	 * String representation of the result to include in the run history description
	 * Example # of rows affected
	 */
	public String getResultString();


	/**
	 * Returns the result of the query execution - For Selects, R is a DataTable, For Updates, R is an Integer (count of rows updated)
	 */
	public R getResult();


	/**
	 * Sets the result of the query execution - For Selects, R is a DataTable, For Updates, R is an Integer (count of rows updated)
	 */
	public void setResult(R result);
}
