package com.clifton.system.navigation.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class SystemNavigationScreenSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "name", searchFieldPath = "systemTable")
	private String systemTable;

	@SearchField
	private String screenClass;

	@SearchField
	private String userInterfaceConfig;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(String systemTable) {
		this.systemTable = systemTable;
	}


	public String getScreenClass() {
		return this.screenClass;
	}


	public void setScreenClass(String screenClass) {
		this.screenClass = screenClass;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getUserInterfaceConfig() {
		return this.userInterfaceConfig;
	}


	public void setUserInterfaceConfig(String userInterfaceConfig) {
		this.userInterfaceConfig = userInterfaceConfig;
	}
}
