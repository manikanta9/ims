package com.clifton.system.navigation;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.system.navigation.search.SystemNavigationScreenAliasSearchForm;
import com.clifton.system.navigation.search.SystemNavigationScreenSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SystemNavigationServiceImpl implements SystemNavigationService {

	private AdvancedUpdatableDAO<SystemNavigationScreen, Criteria> systemNavigationScreenDAO;
	private AdvancedUpdatableDAO<SystemNavigationScreenAlias, Criteria> systemNavigationScreenAliasDAO;

	////////////////////////////////////////////////////////////////////////////
	///////            System Navigation Screen Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemNavigationScreen getSystemNavigationScreen(int id) {
		return getSystemNavigationScreenDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemNavigationScreen> getSystemNavigationScreenList(SystemNavigationScreenSearchForm searchForm) {
		return getSystemNavigationScreenDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemNavigationScreen saveSystemNavigationScreen(SystemNavigationScreen bean) {
		return getSystemNavigationScreenDAO().save(bean);
	}


	@Override
	public void deleteSystemNavigationScreen(int id) {
		getSystemNavigationScreenDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	///////              System Search Field Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemNavigationScreenAlias getSystemNavigationScreenAlias(int id) {
		return getSystemNavigationScreenAliasDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemNavigationScreenAlias> getSystemNavigationScreenAliasList(SystemNavigationScreenAliasSearchForm searchForm) {
		return getSystemNavigationScreenAliasDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemNavigationScreenAlias saveSystemNavigationScreenAlias(SystemNavigationScreenAlias bean) {
		return getSystemNavigationScreenAliasDAO().save(bean);
	}


	@Override
	public void deleteSystemNavigationScreenAlias(int id) {
		getSystemNavigationScreenAliasDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemNavigationScreen, Criteria> getSystemNavigationScreenDAO() {
		return this.systemNavigationScreenDAO;
	}


	public void setSystemNavigationScreenDAO(AdvancedUpdatableDAO<SystemNavigationScreen, Criteria> systemNavigationScreenDAO) {
		this.systemNavigationScreenDAO = systemNavigationScreenDAO;
	}


	public AdvancedUpdatableDAO<SystemNavigationScreenAlias, Criteria> getSystemNavigationScreenAliasDAO() {
		return this.systemNavigationScreenAliasDAO;
	}


	public void setSystemNavigationScreenAliasDAO(AdvancedUpdatableDAO<SystemNavigationScreenAlias, Criteria> systemNavigationScreenAliasDAO) {
		this.systemNavigationScreenAliasDAO = systemNavigationScreenAliasDAO;
	}
}
