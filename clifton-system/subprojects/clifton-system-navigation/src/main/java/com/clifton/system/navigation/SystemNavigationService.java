package com.clifton.system.navigation;


import com.clifton.system.navigation.search.SystemNavigationScreenAliasSearchForm;
import com.clifton.system.navigation.search.SystemNavigationScreenSearchForm;

import java.util.List;


/**
 * The <code>SystemNavigationService</code> handles creation and use of
 * navigation screens and aliases
 *
 * @author apopp
 */
public interface SystemNavigationService {

	////////////////////////////////////////////////////////////////////////////
	///////              System Navigation Screen Business Methods     ///////// 
	////////////////////////////////////////////////////////////////////////////


	public SystemNavigationScreen getSystemNavigationScreen(int id);


	public List<SystemNavigationScreen> getSystemNavigationScreenList(SystemNavigationScreenSearchForm searchForm);


	public SystemNavigationScreen saveSystemNavigationScreen(SystemNavigationScreen bean);


	public void deleteSystemNavigationScreen(int id);


	////////////////////////////////////////////////////////////////////////////
	///////           System Navigation Screen Alias Business Methods  ///////// 
	////////////////////////////////////////////////////////////////////////////


	public SystemNavigationScreenAlias getSystemNavigationScreenAlias(int id);


	public List<SystemNavigationScreenAlias> getSystemNavigationScreenAliasList(SystemNavigationScreenAliasSearchForm searchForm);


	public SystemNavigationScreenAlias saveSystemNavigationScreenAlias(SystemNavigationScreenAlias bean);


	public void deleteSystemNavigationScreenAlias(int id);
}
