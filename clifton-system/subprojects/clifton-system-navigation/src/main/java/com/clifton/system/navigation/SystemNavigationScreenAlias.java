package com.clifton.system.navigation;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>SystemNavigationScreenAlias</code> represents another name we may
 * call a specific navigation screen by. This allows for rich searching.
 *
 * @author apopp
 */
public class SystemNavigationScreenAlias extends NamedEntity<Integer> {

	private SystemNavigationScreen navigationScreen;
	private String screenAlias;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getScreenAlias() {
		return this.screenAlias;
	}


	public void setScreenAlias(String screenAlias) {
		this.screenAlias = screenAlias;
	}


	public SystemNavigationScreen getNavigationScreen() {
		return this.navigationScreen;
	}


	public void setNavigationScreen(SystemNavigationScreen navigationScreen) {
		this.navigationScreen = navigationScreen;
	}
}
