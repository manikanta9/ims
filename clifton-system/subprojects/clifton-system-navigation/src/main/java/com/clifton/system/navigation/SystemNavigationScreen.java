package com.clifton.system.navigation;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemNavigationScreen</code> represents a window in IMS UI along with the associated
 * system table and tab within that window
 *
 * @author apopp
 */
public class SystemNavigationScreen extends NamedEntity<Integer> {

	private SystemTable systemTable;

	private String screenClass;

	/**
	 * Flexible way to control how the screen class will open.
	 * <p>
	 * Ex.
	 * UserInterfaceConfig = "{defaultActiveTabName: Trades}"
	 */
	private String userInterfaceConfig;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	public String getScreenClass() {
		return this.screenClass;
	}


	public void setScreenClass(String screenClass) {
		this.screenClass = screenClass;
	}


	public String getUserInterfaceConfig() {
		return this.userInterfaceConfig;
	}


	public void setUserInterfaceConfig(String userInterfaceConfig) {
		this.userInterfaceConfig = userInterfaceConfig;
	}
}
