package com.clifton.system.navigation.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class SystemNavigationScreenAliasSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "screenAlias,description")
	private String searchPattern;

	@SearchField
	private String description;

	@SearchField(searchField = "navigationScreen.id")
	private Integer navigationScreenId;

	@SearchField
	private String screenAlias;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getNavigationScreenId() {
		return this.navigationScreenId;
	}


	public void setNavigationScreenId(Integer navigationScreenId) {
		this.navigationScreenId = navigationScreenId;
	}


	public String getScreenAlias() {
		return this.screenAlias;
	}


	public void setScreenAlias(String screenAlias) {
		this.screenAlias = screenAlias;
	}
}
