Clifton.system.navigation.SystemNavigationScreenAliasWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Navigation Screen Alias',
	iconCls: 'window',
	width: 520,
	height: 350,

	items: [{
		xtype: 'formpanel',
		url: 'systemNavigationScreenAlias.json',
		instructions: 'Screen alias defines another name that can be used to find this screen. Each screen can use more than one synonyms to refer to it.',

		items: [
			{fieldLabel: 'Navigation Screen', name: 'navigationScreen.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.navigation.SystemNavigationScreenWindow', detailIdField: 'navigationScreen.id'},
			{fieldLabel: 'Screen Alias', name: 'screenAlias'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70}
		]
	}]
});
