Clifton.system.navigation.SystemNavigationScreenWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Navigation Screen',
	iconCls: 'window',
	width: 520,
	height: 350,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Screen',
				items: [{
					xtype: 'formpanel',
					url: 'systemNavigationScreen.json',
					instructions: 'A navigation screen defines a specific window in the system that can be search for and opened using quick search.',
					items: [
						{fieldLabel: 'Screen Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
						{fieldLabel: 'Table', name: 'systemTable.name', hiddenName: 'systemTable.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded', detailPageClass: 'Clifton.system.schema.TableWindow'},
						{fieldLabel: 'Screen Class', name: 'screenClass'},
						{fieldLabel: 'UI Config', name: 'userInterfaceConfig'}
					]
				}]
			},


			{
				title: 'Aliases',
				items: [{
					name: 'systemNavigationScreenAliasListFind',
					xtype: 'gridpanel',
					instructions: 'Screen alias defines another name that can be used to find this screen. Each screen can use more than one synonyms to refer to it.',
					columns: [
						{header: 'Screen Alias', width: 50, dataIndex: 'screenAlias'},
						{header: 'Description', width: 100, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.system.navigation.SystemNavigationScreenAliasWindow',
						getDefaultData: function(gridPanel, row) {
							return {
								navigationScreen: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {navigationScreenId: this.getWindow().getMainFormId()};
					}
				}]
			}]
	}]
});
