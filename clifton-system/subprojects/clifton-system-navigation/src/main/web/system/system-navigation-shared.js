Ext.ns('Clifton.system.navigation');

Clifton.system.navigation.NavigationcScreenGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'systemNavigationScreenListFind',
	xtype: 'gridpanel',
	topToolbarSearchParameter: 'searchPattern',
	instructions: 'A navigation screen defines a specific window in the system that can be search for and opened using quick search.',
	columns: [
		{header: 'Screen Name', width: 100, dataIndex: 'name'},
		{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
		{header: 'Table', dataIndex: 'systemTable.name', width: 100},
		{header: 'Screen Class', width: 150, dataIndex: 'screenClass'},
		{header: 'UI Config', width: 100, dataIndex: 'userInterfaceConfig'}
	],
	editor: {
		detailPageClass: 'Clifton.system.navigation.SystemNavigationScreenWindow'
	}
});
Ext.reg('system-navigation-screen-grid', Clifton.system.navigation.NavigationcScreenGridPanel);
