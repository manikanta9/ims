package com.clifton.system.navigation;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemNavigationProjectBasicTests extends BasicProjectTests {


	@Override
	public String getProjectPrefix() {
		return "system-navigation";
	}
}
