Clifton.system.search.SearchSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'systemSearchSetupWindow',
	title: 'Search Administration',
	iconCls: 'view',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.system.search.SearchSetupWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.system.search.SearchSetupWindowAdditionalTabs[i]);
				}
			}
		},
		items: [
			{
				title: 'Search Areas',
				items: [{
					name: 'systemSearchAreaListFind',
					xtype: 'gridpanel',
					instructions: 'A search area defines a table with corresponding fields that can be searched in the system. All search fields are periodically re-indexed based on corresponding configuration.',
					columns: [
						{header: 'Area Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Area Label', width: 100, dataIndex: 'label'},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Table', dataIndex: 'systemTable.name', width: 100},
						{header: 'Display Field', width: 50, dataIndex: 'displayBeanFieldName'},
						{header: 'Tooltip Field', width: 100, dataIndex: 'tooltipBeanFieldName'},
						{header: 'Rank', dataIndex: 'searchAreaRank', width: 25, type: 'int', useNull: true},
						{header: 'Filter Condition', width: 150, dataIndex: 'filterSystemCondition.name', filter: {type: 'combo', url: 'systemConditionListFind.json?optionalSystemTableName=SystemSearchArea', searchFieldName: 'filterSystemConditionId'}}
					],
					editor: {
						detailPageClass: 'Clifton.system.search.SystemSearchAreaWindow'
					},
					getTopToolbarFilters: function(toolBar) {
						toolBar.add('->');
						toolBar.add({
							text: 'Document Search:',
							tooltip: 'Search for the json document underlying a specific search entity.'
						});
						toolBar.add({
							xtype: 'system-search-combo',
							beforequery: function(queryEvent) {
								queryEvent.combo.store.baseParams = {includeDocument: true};
							}
						});
					}
				}]
			},


			{
				title: 'Search Fields',
				items: [{
					name: 'systemSearchFieldListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all search fields defined in the system grouped by search area.',
					topToolbarSearchParameter: 'searchPattern',
					groupField: 'systemSearchArea.label',
					groupTextTpl: '{values.group} - {[values.rs.length]} {[values.rs.length > 1 ? "Fields" : "Field"]}',
					columns: [
						{header: 'Search Area', dataIndex: 'systemSearchArea.label', hidden: true},
						{header: 'Field Name', width: 50, dataIndex: 'name'},
						{header: 'Many Table', dataIndex: 'systemTable.name', width: 50},
						{header: 'Many Table Property', width: 50, dataIndex: 'beanPropertyToSearchAreaTable'},
						{header: 'Description', width: 120, dataIndex: 'description'},
						{header: 'Analyzer Type', width: 50, dataIndex: 'systemSearchAnalyzer.name'},
						{header: 'Rank', dataIndex: 'searchFieldRank', width: 20, type: 'int', useNull: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.search.SystemSearchFieldWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Search Analyzers',
				items: [{
					name: 'systemSearchAnalyzerListFind',
					xtype: 'gridpanel',
					instructions: 'A search analyzer is used to index a field value into the search engine.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Analyzer Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Tokenizer', width: 200, dataIndex: 'searchTokenizer'},
						{header: 'Filter', width: 200, dataIndex: 'searchFilter'},
						{header: 'Search Time', width: 40, dataIndex: 'searchTimeAnalyzer', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.system.search.SystemSearchAnalyzerWindow'
					}
				}]
			}
		]
	}]
});
