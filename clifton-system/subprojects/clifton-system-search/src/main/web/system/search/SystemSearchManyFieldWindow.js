Clifton.system.search.SystemSearchManyFieldWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Search Field',
	width: 620,
	height: 360,

	items: [{
		xtype: 'formpanel',
		url: 'systemSearchField.json',
		instructions: 'Each search field will be included in searches for corresponding search area.',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Search Area', name: 'systemSearchArea.name', detailIdField: 'systemSearchArea.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.search.SystemSearchAreaWindow'},
			{fieldLabel: 'Bean Field Name', name: 'name'},
			{fieldLabel: 'Many Table', name: 'systemTable.name', hiddenName: 'systemTable.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded', detailPageClass: 'Clifton.system.schema.TableWindow'},
			{fieldLabel: 'Many Table Property', name: 'beanPropertyToSearchAreaTable'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
			{fieldLabel: 'Search Analyzer', name: 'systemSearchAnalyzer.name', hiddenName: 'systemSearchAnalyzer.id', xtype: 'combo', detailPageClass: 'Clifton.system.search.SystemSearchAnalyzerWindow', url: 'systemSearchAnalyzerListFind.json'},
			{fieldLabel: 'Search Rank', name: 'searchFieldRank', xtype: 'integerfield'}
		]
	}]
});
