Clifton.system.search.SystemSearchAnalyzerWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Search Analyzer',
	width: 620,
	height: 550,

	items: [{
		xtype: 'formpanel',
		url: 'systemSearchAnalyzer.json',
		instructions: 'Search analyzer is used to index field values',

		items: [
			{fieldLabel: 'Analyzer Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
			{fieldLabel: 'Tokenizer', name: 'searchTokenizer', xtype: 'textarea', height: 130},
			{fieldLabel: 'Filter', name: 'searchFilter', xtype: 'textarea', height: 130},
			{boxLabel: 'Search Time Analyzer', name: 'searchTimeAnalyzer', xtype: 'checkbox', labelSeparator: ''}
		]
	}]
});
