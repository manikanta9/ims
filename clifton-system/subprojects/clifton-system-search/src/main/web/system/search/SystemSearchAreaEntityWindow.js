Clifton.system.search.SystemSearchAreaEntityWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Search Area Entity',
	width: 620,
	height: 350,

	items: [{
		xtype: 'formpanel',
		url: 'systemSearchAreaEntity.json',
		instructions: 'Special analyzer and ranking for a specific search area entity.',

		items: [
			{fieldLabel: 'Search Area', name: 'systemSearchArea.name', detailIdField: 'systemSearchArea.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.search.SystemSearchAreaWindow'},
			{
				fieldLabel: 'Entity Name', name: 'name', detailIdField: 'fkFieldId', xtype: 'linkfield', getDetailPageClass: function(fp) {
					return TCG.getValue('systemSearchArea.systemTable.detailScreenClass', fp.getForm().formValues);
				}
			},
			{fieldLabel: 'Name', name: 'name', hidden: true},
			{fieldLabel: 'Search Value', name: 'searchValue'},
			{fieldLabel: 'Search Analyzer', name: 'systemSearchAnalyzer.name', hiddenName: 'systemSearchAnalyzer.id', xtype: 'combo', detailPageClass: 'Clifton.system.search.SystemSearchAnalyzerWindow', url: 'systemSearchAnalyzerListFind.json'},
			{fieldLabel: 'Search Rank', name: 'searchRank', xtype: 'integerfield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70}
		]
	}]
});
