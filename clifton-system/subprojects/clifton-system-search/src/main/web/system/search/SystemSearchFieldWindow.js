Clifton.system.search.SystemSearchFieldWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'systemSearchField.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.system.search.SystemSearchStandardFieldWindow';
		if (entity && entity.systemTable) {
			className = 'Clifton.system.search.SystemSearchManyFieldWindow';
		}
		return className;
	}
});
