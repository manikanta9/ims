Clifton.system.search.SystemSearchAreaWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Search Area',
	width: 650,
	height: 450,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Area',
				tbar: [
					{
						text: 'Reindex Area ',
						tooltip: 'Reindex entities for this area.',
						iconCls: 'hammer',
						handler: function() {
							const t = this.ownerCt.ownerCt.items.get(0).form.formValues;
							const f = this.ownerCt.ownerCt;
							const searchAreaId = t.id;

							const loader = new TCG.data.JsonLoader({
								waitTarget: f,
								timeout: 120000,
								waitMsg: 'Reindexing: Please Wait.',
								params: {searchAreaId: searchAreaId},
								scope: f,
								success: function(form, action) {
									const result = Ext.decode(form.responseText);
									const msgTarget = this.getMsgTarget();
									if (result.success) {
										if (this.isUseWaitMsg()) {
											msgTarget.mask('Reindexing... Success');
											const unmaskFunction = function() {
												msgTarget.unmask();
											};
											unmaskFunction.defer(600, this);
										}
									}
									else {
										this.onFailure();
										msgTarget.unmask();
										TCG.data.ErrorHandler.handleFailure(this, result);
									}
								},
								error: function(form, action) {
									Ext.Msg.alert('Failed to reindex area', action.result.data);
								}
							});
							loader.load('systemSearchAreaReindex.json');
						}
					},
					'-', '->',
					{
						text: 'Document Search:',
						tooltip: 'Search result window will contain the actual json document underlying a specific search entity as well as explanation of result scoring.'
					},
					{
						xtype: 'system-search-combo',
						emptyText: ' Search this Area',
						beforequery: function(queryEvent) {
							const t = this.ownerCt.ownerCt.items.get(0).form.formValues;
							queryEvent.combo.store.baseParams = {searchAreaId: t.id, includeDocument: true};
						}
					}
				],
				items: [{
					xtype: 'formpanel',
					url: 'systemSearchArea.json',
					instructions: 'A search area defines a table with corresponding fields that can be searched in the system. All search fields are periodically re-indexed based on corresponding configuration.',

					items: [
						{fieldLabel: 'Area Name', name: 'name'},
						{fieldLabel: 'Area Label', name: 'label'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
						{fieldLabel: 'Table', name: 'systemTable.name', hiddenName: 'systemTable.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded', detailPageClass: 'Clifton.system.schema.TableWindow'},
						{fieldLabel: 'Display Bean Field', name: 'displayBeanFieldName'},
						{fieldLabel: 'Tooltip Bean Field', name: 'tooltipBeanFieldName'},
						{fieldLabel: 'Search Rank', name: 'searchAreaRank', xtype: 'integerfield'},
						{
							fieldLabel: 'Filter Condition', name: 'filterSystemCondition.name', hiddenName: 'filterSystemCondition.id', xtype: 'system-condition-combo', qtip: 'Optionally limit entities to those that evaluate selected condition to true.',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									const o = f.findField('systemTable.name').getValueObject();
									if (o) {
										combo.store.baseParams = {optionalSystemTableName: o.name || o.label};
									}
								}
							}
						}
					]
				}]
			},


			{
				title: 'Fields',
				items: [{
					name: 'systemSearchFieldListFind',
					xtype: 'gridpanel',
					instructions: 'A list of search fields that define to this search area.',
					columns: [
						{header: 'Field Name', width: 50, dataIndex: 'name'},
						{header: 'Side Table Property', width: 50, dataIndex: 'beanPropertyToSearchAreaTable', hidden: true},
						{header: 'Table', dataIndex: 'systemTable.name', width: 100, hidden: true},
						{header: 'Description', width: 100, dataIndex: 'description'},
						{header: 'Analyzer Type', width: 50, dataIndex: 'systemSearchAnalyzer.name'},
						{header: 'Rank', dataIndex: 'searchFieldRank', width: 25, type: 'int', useNull: true}
					],
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								const t = rowItem.get('systemTable.name');
								return (t) ? 'Many Table Field' : 'Standard Field';
							},
							items: [
								{text: 'Standard Field', iconCls: 'verify', className: 'Clifton.system.search.SystemSearchFieldWindow'},
								{text: 'Many Table Field', iconCls: 'hierarchy', className: 'Clifton.system.search.SystemSearchManyFieldWindow'}
							]
						},
						getDefaultData: function(gridPanel, row) {
							return {systemSearchArea: gridPanel.getWindow().getMainForm().formValues};
						}
					},
					getLoadParams: function() {
						return {searchAreaId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Entity Ranks',
				items: [{
					name: 'systemSearchAreaEntityListFind',
					xtype: 'gridpanel',
					instructions: 'A list of search entity ranks for this search area.',
					columns: [
						{header: 'Search Area ID', width: 50, dataIndex: 'systemSearchArea.id', hidden: true},
						{header: 'FkFieldId', dataIndex: 'fkFieldId', width: 100, hidden: true},

						{header: 'Entity Name', width: 50, dataIndex: 'name'},
						{header: 'Description', width: 100, dataIndex: 'description'},
						{header: 'Search Value', dataIndex: 'searchValue', width: 70},
						{header: 'Analyzer Type', width: 50, dataIndex: 'systemSearchAnalyzer.name'},
						{header: 'Rank', dataIndex: 'searchRank', width: 25, type: 'int', useNull: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.search.SystemSearchAreaEntityWindow',
						getDefaultData: function(gridPanel, row) {
							return {systemSearchArea: gridPanel.getWindow().getMainForm().formValues};
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add({
								xtype: 'system-search-combo',
								emptyText: 'Add Entity Rank',
								beforequery: function(queryEvent) {
									const t = this.ownerCt.ownerCt.ownerCt.ownerCt.ownerCt.params;
									queryEvent.combo.store.baseParams = {searchAreaId: t.id};
								},
								selectHandler: function(combo, record, index) {
									const params = record.json;
									const defaultData = {};

									defaultData.name = combo.store.data.items[index].data.label;
									defaultData.fkFieldId = params.pkFieldId;
									defaultData.systemSearchArea = {};
									defaultData.systemSearchArea.systemTable = {};
									defaultData.systemSearchArea.systemTable.detailScreenClass = params.systemSearchArea.systemTable.detailScreenClass;
									defaultData.systemSearchArea.label = params.systemSearchArea.label;
									defaultData.systemSearchArea.name = params.systemSearchArea.name;
									defaultData.systemSearchArea.id = params.systemSearchArea.id;

									TCG.createComponent('Clifton.system.search.SystemSearchAreaEntityWindow', {
										defaultData: defaultData,
										openerCt: gridPanel
									});
								}
							});
							toolBar.add('-');
						}
					},
					getLoadParams: function() {
						return {searchAreaId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
