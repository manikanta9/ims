Ext.ns('Clifton.system.search');

Clifton.system.search.SystemSearchCombo = Ext.extend(TCG.form.ComboBox, {
	xtype: 'combo',
	id: undefined,
	errorFieldLabel: 'Search',
	url: 'systemSearchListFind.json', // needs to be set after login
	requestedProps: 'systemTable.name|systemTable.detailScreenClass|pkFieldId|systemSearchArea.label|systemSearchArea.id|label|tooltip|description|screenClass|userInterfaceConfig',
	detailPageClass: 'Clifton.business.client.ClientWindow',
	doNotTrimStrings: false,
	doNotAddContextMenu: true,
	displayField: 'label',
	hideTrigger: true,
	minChars: 1,
	emptyText: ' Search for All',
	tpl: '<tpl for="."><div class="x-combo-list-item" ext:qtip="{tooltip:htmlEncode}{description:htmlEncode}">{label}<tpl if="searchAreaLabel"> <span style="COLOR: #330027; FONT-STYLE: italic">in {searchAreaLabel}</span></tpl></div></tpl>',
	fields: ['id', {name: 'searchAreaLabel', mapping: 'systemSearchArea.label'}, 'label', 'description', 'tooltip'],
	width: 200,
	listWidth: 600,
	selectHandler: function(combo, record, index) {
		const jsonObj = Ext.decode(record.json.searchDocument);
		const jsonString = JSON.stringify(jsonObj, null, '\t');

		const title = 'Search Document Explanation - ' + record.json.label;
		const params = record.json;
		const f = this.ownerCt.ownerCt;

		const searchDocument = new Ext.FormPanel({
			bodyStyle: 'padding: 10px 0px 0',
			defaults: {
				anchor: '-5',
				xtype: 'textarea'
			},
			frame: true,
			autoScroll: true,
			labelWidth: 1,
			buttonAlign: 'center',

			params: params,
			f: f,
			items: [
				{readOnly: true, value: record.json.searchExplanation, anchor: '-5 -320'},
				{height: 300, value: jsonString, id: 'searchDoc', name: 'searchDoc', allowBlank: false}
			],
			buttons: [{
				text: 'Save',
				handler: function(btn, text) {
					const obj = TCG.getParentFormPanel(btn);
					obj.params.searchDocument = TCG.getChildByName(obj, 'searchDoc').getValue();
					const params = obj.params;

					//Not sure why we must specifiy this as a string, otherwise its an object
					// and the server cannot parse it into the needed object
					// Seems like requestedProps isn't working and everything is being returned.
					params['systemSearchArea.id'] = params.systemSearchArea.id;
					delete params['systemSearchArea'];

					const loader = new TCG.data.JsonLoader({
						waitTarget: obj.f,
						timeout: 120000,
						waitMsg: 'Reindexing: Please Wait.',
						params: params,
						scope: obj.f,
						success: function(form, action) {
							const result = Ext.decode(form.responseText);
							if (result.success) {
								if (this.isUseWaitMsg()) {
									this.getMsgTarget().mask('Reindexing... Success');
									const unmaskFunction = function() {
										this.getMsgTarget().unmask();
									};
									unmaskFunction.defer(600, this);
									win.win.closeWindow();
								}
							}
							else {
								this.onFailure();
								this.getMsgTarget().unmask();
								TCG.data.ErrorHandler.handleFailure(this, result);
							}
						},
						error: function(form, action) {
							Ext.Msg.alert('Failed to reindex area', action.result.data);
						}
					});
					loader.load('systemSearchIndexByModifiedResultSave.json');
				}
			}, {
				text: 'Close',
				handler: function() {
					win.win.closeWindow();
				}
			}]
		});
		const win = new TCG.app.Window({
			title: title,
			iconCls: 'view',
			width: 800,
			height: 650,
			openerCt: this,
			items: searchDocument
		});
	}
});
Ext.reg('system-search-combo', Clifton.system.search.SystemSearchCombo);


Clifton.system.search.SearchSetupWindowAdditionalTabs = [];

