package com.clifton.system.search.setup.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.system.search.setup.SystemSearchArea;

import java.util.List;


/**
 * @author manderson
 */
public interface SystemSearchAreaCache {


	public List<SystemSearchArea> getSystemSearchAreaList(ReadOnlyDAO<SystemSearchArea> dao);
}
