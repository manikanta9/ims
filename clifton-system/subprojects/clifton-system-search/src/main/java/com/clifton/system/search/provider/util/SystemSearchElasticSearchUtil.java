package com.clifton.system.search.provider.util;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.search.provider.search.SystemSearchResult;
import com.clifton.system.search.setup.SystemSearchAnalyzer;
import com.clifton.system.search.setup.SystemSearchArea;
import com.clifton.system.search.setup.SystemSearchAreaEntity;
import com.clifton.system.search.setup.SystemSearchField;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.springframework.beans.NullValueInNestedPathException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemSearchElasticSearchUtil</code> class contains methods used for building
 * elasticsearch queries
 *
 * @author apopp
 */
public class SystemSearchElasticSearchUtil {

	public static final String DISPLAY_BEAN_FIELD_NAME = "displayBeanFieldName";
	public static final String TOOLTIP_BEAN_FIELD_NAME = "tooltipBeanFieldName";

	public static final String VALUE_KEY = "_value";
	public static final String INDEX_KEY = "index";


	/**
	 * Creates a semi-unique string representing an index for a given application at a given environment level
	 */
	public static String getSearchIndexName(String environmentLevel, String applicationName) {
		//Index names in ElasticSearch must be lowercase
		return String.join("_", StringUtils.replace(environmentLevel, " ", "-"), StringUtils.replace(applicationName, " ", "-")).toLowerCase();
	}


	/**
	 * Build index query to create our base index with available analyzers. This is the index where
	 * search areas and documents (entities) will be placed within elasticsearch.
	 */
	public static String buildSearchIndexQuery(List<SystemSearchAnalyzer> searchAnalyzerList) {
		List<String> analyzerList = new ArrayList<>();
		List<String> tokenizerList = new ArrayList<>();
		List<String> filterList = new ArrayList<>();

		for (SystemSearchAnalyzer analyzer : CollectionUtils.getIterable(searchAnalyzerList)) {
			String tokenizer = "\"" + analyzer.getName().toLowerCase().replace(" ", "_") + "\"";
			tokenizerList.add(tokenizer + ":{" + analyzer.getSearchTokenizer() + "}");
			analyzerList.add(tokenizer + ":{ \"type\": \"custom\",\"tokenizer\": " + tokenizer + getAndNormalizeFilter(filterList, analyzer) + "}");
		}

		String tokenizers = "";
		String analyzers = "";
		String filters = "";

		if (!CollectionUtils.isEmpty(tokenizerList)) {
			tokenizers = StringUtils.collectionToCommaDelimitedString(tokenizerList);
		}

		if (!CollectionUtils.isEmpty(analyzerList)) {
			analyzers = StringUtils.collectionToCommaDelimitedString(analyzerList);
		}

		if (!CollectionUtils.isEmpty(filterList)) {
			filters = StringUtils.collectionToCommaDelimitedString(filterList);
		}

		return "{\"number_of_shards\":1,\"analysis\":{\"tokenizer\":{" + tokenizers + "},\"analyzer\":{" + analyzers + "},\"filter\":{" + filters + "}}}";
	}


	/**
	 * Convert elasticsearch search result to our known SystemSearchResult object
	 */
	@SuppressWarnings("unchecked")
	public static SystemSearchResult buildSearchResult(Map<String, Object> resultMap, SystemSearchArea searchArea, String pkFieldId, String searchDocument, String searchExplanation) {
		SystemSearchResult searchResult = new SystemSearchResult();

		searchResult.setSystemSearchArea(searchArea);
		if (resultMap.get(DISPLAY_BEAN_FIELD_NAME) != null) {
			searchResult.setLabel((String) ((Map<String, Object>) resultMap.get(DISPLAY_BEAN_FIELD_NAME)).get(VALUE_KEY));
		}
		if (resultMap.get(TOOLTIP_BEAN_FIELD_NAME) != null) {
			searchResult.setTooltip((String) ((Map<String, Object>) resultMap.get(TOOLTIP_BEAN_FIELD_NAME)).get(VALUE_KEY));
		}
		searchResult.setPkFieldId(Integer.valueOf(pkFieldId));
		searchResult.setSearchDocument(searchDocument);
		searchResult.setSearchExplanation(searchExplanation);

		String screenClass = (String) resultMap.get(searchArea.getId().toString() + "screenClass");
		String userInterfaceConfig = (String) resultMap.get(searchArea.getId().toString() + "userInterfaceConfig");

		if (screenClass == null) {
			screenClass = searchArea.getSystemTable().getDetailScreenClass();
		}

		searchResult.setScreenClass(screenClass);
		searchResult.setUserInterfaceConfig(userInterfaceConfig);

		return searchResult;
	}


	/**
	 * Build the query for the portion of the document describing this entity. We are building it based on the fields specified by the
	 * searchFieldList.
	 */
	public static XContentBuilder buildEntityIndexQuery(SystemSearchArea searchArea, List<SystemSearchField> searchFieldList, IdentityObject entity, List<SystemSearchAreaEntity> entityRankList) throws IOException {
		String searchAreaLabel = searchArea.getLabel();

		XContentBuilder contentBuilder = XContentFactory.jsonBuilder();
		contentBuilder.startObject();

		contentBuilder.startObject("systemSearchAreaLabel");
		contentBuilder.field(VALUE_KEY, searchAreaLabel);
		contentBuilder.field(INDEX_KEY, "no");
		contentBuilder.endObject();

		contentBuilder.startObject(DISPLAY_BEAN_FIELD_NAME);
		contentBuilder.field(VALUE_KEY, BeanUtils.getPropertyValue(entity, searchArea.getDisplayBeanFieldName(), true));
		contentBuilder.field(INDEX_KEY, "no");
		contentBuilder.endObject();

		String toolTip = (String) BeanUtils.getPropertyValue(entity, searchArea.getTooltipBeanFieldName(), true);

		if (toolTip != null) {
			contentBuilder.startObject(TOOLTIP_BEAN_FIELD_NAME);
			contentBuilder.field(VALUE_KEY, toolTip);
			contentBuilder.field(INDEX_KEY, "no");
			contentBuilder.endObject();
		}

		List<SystemSearchField> basicSearchFieldList = BeanUtils.filter(searchFieldList, systemSearchField -> systemSearchField.getBeanPropertyToSearchAreaTable() == null);

		for (SystemSearchField field : CollectionUtils.getIterable(basicSearchFieldList)) {
			buildFieldIndex(contentBuilder, field, entity);
		}

		for (SystemSearchAreaEntity searchAreaEntity : CollectionUtils.getIterable(entityRankList)) {
			buildAreaEntityIndex(contentBuilder, searchAreaEntity);
		}

		return contentBuilder;
	}


	/**
	 * Build SystemSearchField entity insert query.  This method builds the portion of the document
	 * that houses the property defined by the SystemSearchField for each entity
	 */
	public static void buildFieldIndex(XContentBuilder contentBuilder, SystemSearchField field, Object... entityList) {
		List<String> fieldValueList = new ArrayList<>();

		for (Object entity : entityList) {
			String fieldValue = getFieldPropertyValue((IdentityObject) entity, field.getName(), field.getSystemSearchArea().getName());
			fieldValueList.add(fieldValue);
		}

		if (!CollectionUtils.isEmpty(fieldValueList)) {
			try {
				if (CollectionUtils.getSize(fieldValueList) == 1) {
					//Make the field name unique
					contentBuilder.field(field.getSystemSearchArea().getId() + field.getName().replace(".", StringUtils.EMPTY_STRING)
									+ (field.getSystemSearchAnalyzer() != null ? field.getSystemSearchAnalyzer().getName().toLowerCase().replace(" ", "_") : StringUtils.EMPTY_STRING),
							CollectionUtils.getOnlyElement(fieldValueList));
				}
				else {
					//Make the field name unique and create field value array
					contentBuilder.field(field.getSystemSearchArea().getId() + field.getName().replace(".", StringUtils.EMPTY_STRING)
							+ (field.getSystemSearchAnalyzer() != null ? field.getSystemSearchAnalyzer().getName().toLowerCase().replace(" ", "_") : StringUtils.EMPTY_STRING), fieldValueList);
				}
			}
			catch (IOException e) {
				throw new RuntimeException("Error building content for field [" + field.getName() + "] and value [" + StringUtils.collectionToCommaDelimitedString(fieldValueList) + "].", e);
			}
		}
	}


	private static String getAndNormalizeFilter(List<String> filterList, SystemSearchAnalyzer searchAnalyzer) {
		String filter = null;

		if (searchAnalyzer.getSearchFilter() != null) {
			String[] filterAttributes = searchAnalyzer.getSearchFilter().split("~");

			for (int i = 0; i < filterAttributes.length; i++) {
				if (i == 0) {
					filter = "," + filterAttributes[i];
				}
				else {
					filterList.add(filterAttributes[i]);
				}
			}
		}
		else {
			filter = "";
		}

		return filter;
	}


	private static void buildAreaEntityIndex(XContentBuilder contentBuilder, SystemSearchAreaEntity searchAreaEntity) {
		try {
			//Make the field name unique
			String name = searchAreaEntity.getSystemSearchArea().getId() + "entity" + searchAreaEntity.getId() + searchAreaEntity.getName().toLowerCase().replace(" ", "_")
					+ (searchAreaEntity.getSystemSearchAnalyzer() != null ? searchAreaEntity.getSystemSearchAnalyzer().getName().toLowerCase().replace(" ", "_") : StringUtils.EMPTY_STRING);
			contentBuilder.field(name, searchAreaEntity.getSearchValue());
		}
		catch (Throwable e) {
			throw new RuntimeException("Error building content for area entity [" + searchAreaEntity.getName() + "] and value [" + searchAreaEntity.getSearchValue() + "].", e);
		}
	}


	private static String getFieldPropertyValue(IdentityObject entity, String fieldName, String areaName) {
		String value = null;

		try {
			Object objectValue = BeanUtils.getPropertyValue(entity, fieldName, false);
			if (objectValue != null) {
				if (objectValue instanceof String) {
					value = (String) objectValue;
				}
				else {
					// convert numeric and other values into strings
					value = objectValue.toString();
				}
			}
		}
		catch (NullValueInNestedPathException e) {
			// early part of nested property is null: this is fine
		}
		catch (Throwable e) {
			throw new RuntimeException("Field [" + fieldName + "] not found for search area [" + areaName + "] entity: " + entity, e);
		}

		return value;
	}
}
