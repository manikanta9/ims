package com.clifton.system.search;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.system.search.provider.search.SystemSearchResult;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>SystemSearchService</code> is the entry point for search requests.
 * This service also manages search area index rebuilds
 *
 * @author apopp
 */
public interface SystemSearchService {

	/**
	 * Execute the search query against the underlying provider (ElasticSearch)
	 */
	@SecureMethod(securityResource = "Search")
	public List<SystemSearchResult> getSystemSearchList(SystemSearchSearchForm searchForm);


	/**
	 * Reindex all entities under the specific search area. This is useful if new entities are added to the system
	 * or search settings have changed. A batch job also executes the underlying reindexing method.
	 */
	@RequestMapping("/systemSearchAreaReindex")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void reindexSystemSearchArea(int searchAreaId);
}
