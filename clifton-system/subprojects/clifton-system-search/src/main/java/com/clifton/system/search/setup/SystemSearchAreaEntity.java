package com.clifton.system.search.setup;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.usedby.softlink.SoftLinkField;


/**
 * The <code>SystemSearchAreaEntity</code> allows controlling rank and analyzers for specific entities within a search area.
 * <p>
 * Ex. For the company Goldman Sachs we can specify that an exact match on "goldman" will rank this specific company
 * very high.
 *
 * @author apopp
 */
public class SystemSearchAreaEntity extends NamedEntity<Integer> {

	/**
	 * Search area that this field falls under
	 */
	private SystemSearchArea systemSearchArea;

	/**
	 * fk field id of the entity
	 */
	@SoftLinkField(tableBeanPropertyName = "systemSearchArea.systemTable")
	private Integer fkFieldId;

	/**
	 * Search value that should be specially indexed for this entity
	 * <p>
	 * Ex search for "goldman" this entity will override this value
	 * such that it increase the rank to the entity underlying fkFieldId
	 */
	private String searchValue;

	/**
	 * Specify the elasticsearch index analyzer to use. This analyzer is what
	 * elasticsearch will use to parse this specific field. This is useful for different field
	 * types such as Contact Name.  This is an example where we'd like to insert the name
	 * into search but have search parse it into pieces so we can return full results on
	 * a partial query. "Vlad" will return "Vladimir"
	 */
	private SystemSearchAnalyzer systemSearchAnalyzer;

	/**
	 * The rank this field has in comparison to the other fields in this area.
	 */
	private Integer searchRank;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSearchArea getSystemSearchArea() {
		return this.systemSearchArea;
	}


	public void setSystemSearchArea(SystemSearchArea systemSearchArea) {
		this.systemSearchArea = systemSearchArea;
	}


	public SystemSearchAnalyzer getSystemSearchAnalyzer() {
		return this.systemSearchAnalyzer;
	}


	public void setSystemSearchAnalyzer(SystemSearchAnalyzer systemSearchAnalyzer) {
		this.systemSearchAnalyzer = systemSearchAnalyzer;
	}


	public String getSearchValue() {
		return this.searchValue;
	}


	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Integer getSearchRank() {
		return this.searchRank;
	}


	public void setSearchRank(Integer searchRank) {
		this.searchRank = searchRank;
	}
}
