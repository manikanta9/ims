package com.clifton.system.search.setup;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>SystemSearchAnalyzer</code> is used to parse values that we wish to index in our search engine.
 * The tokenizer and filter determine how those values are parsed.
 *
 * @author apopp
 */
public class SystemSearchAnalyzer extends NamedEntity<Integer> {

	private String searchTokenizer;
	private String searchFilter;

	private boolean searchTimeAnalyzer;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchFilter() {
		return this.searchFilter;
	}


	public void setSearchFilter(String searchFilter) {
		this.searchFilter = searchFilter;
	}


	public String getSearchTokenizer() {
		return this.searchTokenizer;
	}


	public void setSearchTokenizer(String searchTokenizer) {
		this.searchTokenizer = searchTokenizer;
	}


	public boolean isSearchTimeAnalyzer() {
		return this.searchTimeAnalyzer;
	}


	public void setSearchTimeAnalyzer(boolean searchTimeAnalyzer) {
		this.searchTimeAnalyzer = searchTimeAnalyzer;
	}
}
