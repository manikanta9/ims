package com.clifton.system.search.provider.search;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.system.search.SystemSearchSearchForm;
import com.clifton.system.search.provider.index.SystemSearchIndexProvider;
import com.clifton.system.search.provider.transport.SystemSearchTransportProvider;
import com.clifton.system.search.provider.util.SystemSearchElasticSearchUtil;
import com.clifton.system.search.setup.SystemSearchAnalyzer;
import com.clifton.system.search.setup.SystemSearchArea;
import com.clifton.system.search.setup.SystemSearchAreaEntity;
import com.clifton.system.search.setup.SystemSearchField;
import com.clifton.system.search.setup.SystemSearchSetupService;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.IndexNotFoundException;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class SystemSearchElasticSearchProvider implements SystemSearchProvider {

	private SystemSearchIndexProvider systemSearchIndexProvider;
	private SystemSearchSetupService systemSearchSetupService;
	private SystemSearchTransportProvider systemSearchTransportProvider;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemSearchResult> getSearchResultList(SystemSearchSearchForm searchForm) {
		FunctionScoreQueryBuilder searchQuery = getSearchQuery(searchForm);

		SearchRequestBuilder requestBuilder = getTransportClient().prepareSearch().setTypes().setSearchType(SearchType.DFS_QUERY_THEN_FETCH).setQuery(searchQuery)
				// Query
				// Filter
				.setFrom(searchForm.getStart())
				.setSize(searchForm.getLimit())
				.setExplain(false);

		if (!StringUtils.isEmpty(searchForm.getApplicationName())) {
			requestBuilder.setIndices(getSystemSearchIndexProvider().getSearchIndexNameForApplication(searchForm.getApplicationName()));
		}
		else {
			requestBuilder.setIndices(getSystemSearchIndexProvider().getSearchIndexName());
		}

		if (searchForm.isIncludeDocument()) {
			requestBuilder.setExplain(true);
		}

		//restrict to specific search area if specified
		if (searchForm.getSearchAreaId() != null) {
			requestBuilder.setTypes(String.valueOf(searchForm.getSearchAreaId()));
		}

		SearchResponse searchResponse = null;

		try {
			searchResponse = requestBuilder.execute().actionGet();
		}
		catch (IndexNotFoundException infe) {
			throw new RuntimeException("Index [" + (!StringUtils.isEmpty(searchForm.getApplicationName()) ? getSystemSearchIndexProvider().getSearchIndexNameForApplication(searchForm.getApplicationName()) : getSystemSearchIndexProvider().getSearchIndexName()) + "] does not exist. Ensure valid search properties are set.");
		}

		Integer totalPossibleHits = (searchResponse.getHits().getTotalHits() > searchForm.getSearchSize()) ? searchForm.getSearchSize() : (int) searchResponse.getHits().getTotalHits();

		List<SystemSearchResult> searchResultList = new ArrayList<>();
		for (SearchHit hit : searchResponse.getHits()) {
			Map<String, Object> resultMap = hit.getSource();
			SystemSearchArea searchArea = null;

			if (hit.getType() != null && MathUtils.isNumber(hit.getType())) {
				searchArea = getSystemSearchSetupService().getSystemSearchArea(Integer.valueOf(hit.getType()));
			}

			if (searchArea == null) {
				continue;
			}
			String searchDocument = null;
			String searchExplanation = null;

			if (searchForm.isIncludeDocument()) {
				searchDocument = hit.getSourceAsString();
				searchExplanation = hit.getExplanation().toString();
			}

			searchResultList.add(SystemSearchElasticSearchUtil.buildSearchResult(resultMap, searchArea, hit.getId(), searchDocument, searchExplanation));
		}

		return new PagingArrayList<>(searchResultList, searchForm.getStart(), totalPossibleHits, searchForm.getLimit());
	}


	private FunctionScoreQueryBuilder getSearchQuery(SystemSearchSearchForm searchForm) {
		SystemSearchAnalyzer searchAnalyzer = getSystemSearchSetupService().getSystemSearchAnalyzerForSearchTime();
		String searchTimeAnalyzerName = searchAnalyzer != null ? searchAnalyzer.getName().toLowerCase().replace(" ", "_") : null;

		Map<String, Float> fieldMap = getSearchRelevantFieldMap(searchForm);
		populateSearchRelevantEntityFieldMap(searchForm, fieldMap);

		MultiMatchQueryBuilder multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(searchForm.getSearchPattern())
				.fields(fieldMap)
				.type(MultiMatchQueryBuilder.Type.CROSS_FIELDS)
				.operator(Operator.AND)
				.minimumShouldMatch("15%")
				.analyzer(searchTimeAnalyzerName);

		return QueryBuilders.functionScoreQuery(multiMatchQueryBuilder, getIndexTypeFunctionScoringBuilders(searchForm));
	}


	private Map<String, Float> getSearchRelevantFieldMap(SystemSearchSearchForm searchForm) {
		Map<String, Float> fieldMap = new HashMap<>();
		/*
		 * We must search on every field we wish to include in our search. This is so elasticsearch will perform field
		 * specific analysis rather than default on a composite "_all" field.
		 */
		List<SystemSearchField> fieldList = getSystemSearchSetupService().getSystemSearchFieldByArea(searchForm.getSearchAreaId());

		for (SystemSearchField field : CollectionUtils.getIterable(fieldList)) {
			String analyzerName = (field.getSystemSearchAnalyzer() != null) ? field.getSystemSearchAnalyzer().getName().toLowerCase().replace(" ", "_") : null;
			fieldMap.put((field.getSystemSearchArea().getId() + field.getName() + (analyzerName != null ? analyzerName : StringUtils.EMPTY_STRING)).replace(".", StringUtils.EMPTY_STRING), field.getSearchFieldRank() != null ? field.getSearchFieldRank().floatValue() : 1.0F);
		}
		//These won't be pulled in from the field name query but are used globally on index documents
		fieldMap.put(SystemSearchElasticSearchUtil.DISPLAY_BEAN_FIELD_NAME, 1.0F);
		fieldMap.put(SystemSearchElasticSearchUtil.TOOLTIP_BEAN_FIELD_NAME, 1.0F);
		return fieldMap;
	}


	private void populateSearchRelevantEntityFieldMap(SystemSearchSearchForm searchForm, Map<String, Float> map) {
		for (SystemSearchAreaEntity searchAreaEntity : CollectionUtils.getIterable(getSystemSearchSetupService().getSystemSearchAreaEntityListBySearchArea(searchForm.getSearchAreaId()))) {
			//Field level score boost notation (rank multiplier)
			String name = searchAreaEntity.getSystemSearchArea().getId() + "entity" + searchAreaEntity.getId() + searchAreaEntity.getName().toLowerCase().replace(" ", "_")
					+ (searchAreaEntity.getSystemSearchAnalyzer() != null ? searchAreaEntity.getSystemSearchAnalyzer().getName().toLowerCase().replace(" ", "_") : StringUtils.EMPTY_STRING);

			map.put(name, searchAreaEntity.getSearchRank() != null ? searchAreaEntity.getSearchRank().floatValue() : 1.0F);
		}
	}


	private FunctionScoreQueryBuilder.FilterFunctionBuilder[] getIndexTypeFunctionScoringBuilders(SystemSearchSearchForm searchForm) {
		List<FunctionScoreQueryBuilder.FilterFunctionBuilder> functions = new ArrayList<>();

		if (searchForm.getSearchAreaId() != null) {
			SystemSearchArea searchArea = getSystemSearchSetupService().getSystemSearchArea(searchForm.getSearchAreaId());
			if (searchArea.getSearchAreaRank() != null) {
				functions.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(QueryBuilders.typeQuery(searchArea.getId().toString()), ScoreFunctionBuilders.fieldValueFactorFunction(searchArea.getName()).factor(searchArea.getSearchAreaRank())));
			}
		}
		else {
			List<SystemSearchArea> systemSearchAreaList = getSystemSearchSetupService().getSystemSearchAreaList(null);
			for (SystemSearchArea searchArea : CollectionUtils.getIterable(systemSearchAreaList)) {
				if (searchArea.getSearchAreaRank() == null) {
					continue;
				}
				functions.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(QueryBuilders.typeQuery(searchArea.getId().toString()), ScoreFunctionBuilders.fieldValueFactorFunction(searchArea.getName()).factor(searchArea.getSearchAreaRank())));
			}
		}

		return CollectionUtils.toArray(functions, FunctionScoreQueryBuilder.FilterFunctionBuilder.class);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSearchSetupService getSystemSearchSetupService() {
		return this.systemSearchSetupService;
	}


	public void setSystemSearchSetupService(SystemSearchSetupService systemSearchSetupService) {
		this.systemSearchSetupService = systemSearchSetupService;
	}


	public SystemSearchTransportProvider getSystemSearchTransportProvider() {
		return this.systemSearchTransportProvider;
	}


	public void setSystemSearchTransportProvider(SystemSearchTransportProvider systemSearchTransportProvider) {
		this.systemSearchTransportProvider = systemSearchTransportProvider;
	}


	public SystemSearchIndexProvider getSystemSearchIndexProvider() {
		return this.systemSearchIndexProvider;
	}


	public void setSystemSearchIndexProvider(SystemSearchIndexProvider systemSearchIndexProvider) {
		this.systemSearchIndexProvider = systemSearchIndexProvider;
	}


	private TransportClient getTransportClient() {
		return getSystemSearchTransportProvider().getTransportClient();
	}
}
