package com.clifton.system.search.setup.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.system.search.setup.SystemSearchArea;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemSearchAreaCacheImpl</code> caches a list of all of the {@list SystemSearchArea} entities
 * Whenever any SystemSearchArea object is modified, the cache is cleared
 *
 * @author manderson
 */
@Component
public class SystemSearchAreaCacheImpl extends SelfRegisteringSimpleDaoCache<SystemSearchArea, String, List<SystemSearchArea>> implements SystemSearchAreaCache {

	private static final String CACHE_KEY = "SEARCH_AREA_LIST";

	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemSearchArea> getSystemSearchAreaList(ReadOnlyDAO<SystemSearchArea> dao) {
		List<SystemSearchArea> areaList = getCacheHandler().get(getCacheName(), CACHE_KEY);
		if (areaList == null) {
			areaList = dao.findAll();
			if (areaList == null) {
				// Create an empty list, vs null so we don't try to retrieve again from DB
				areaList = new ArrayList<>();
			}
			getCacheHandler().put(getCacheName(), CACHE_KEY, areaList);
		}
		return areaList;
	}
}
