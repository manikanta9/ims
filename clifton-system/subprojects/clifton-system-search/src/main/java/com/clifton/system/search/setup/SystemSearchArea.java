package com.clifton.system.search.setup;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemSearchArea</code> defines a region that can be searched.  This typically corresponds to a system table.
 * <p>
 * Ex. Clients, Companies, Securities, Instruments, Contracts, etc
 *
 * @author apopp
 */
public class SystemSearchArea extends NamedEntity<Integer> {

	private SystemTable systemTable;

	/**
	 * Defines how this area ranks in comparison to other areas.
	 * This will be used to sort results by rank.
	 */
	private Integer searchAreaRank;

	/**
	 * If this field is set, then run each entity through the condition and ONLY put it in the search engine
	 * if condition evaluates to false.
	 */
	private SystemCondition filterSystemCondition;

	/**
	 * String to display in search results
	 */
	private String displayBeanFieldName;

	/**
	 * String to display on hover of a specific displayBeanFieldName
	 */
	private String tooltipBeanFieldName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	public Integer getSearchAreaRank() {
		return this.searchAreaRank;
	}


	public void setSearchAreaRank(Integer searchAreaRank) {
		this.searchAreaRank = searchAreaRank;
	}


	public String getDisplayBeanFieldName() {
		return this.displayBeanFieldName;
	}


	public void setDisplayBeanFieldName(String displayBeanFieldName) {
		this.displayBeanFieldName = displayBeanFieldName;
	}


	public String getTooltipBeanFieldName() {
		return this.tooltipBeanFieldName;
	}


	public void setTooltipBeanFieldName(String tooltipBeanFieldName) {
		this.tooltipBeanFieldName = tooltipBeanFieldName;
	}


	public SystemCondition getFilterSystemCondition() {
		return this.filterSystemCondition;
	}


	public void setFilterSystemCondition(SystemCondition filterSystemCondition) {
		this.filterSystemCondition = filterSystemCondition;
	}
}
