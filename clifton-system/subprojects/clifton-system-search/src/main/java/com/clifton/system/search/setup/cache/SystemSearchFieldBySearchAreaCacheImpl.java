package com.clifton.system.search.setup.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.search.setup.SystemSearchField;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * The <code>SystemSearchFieldBySearchAreaCacheImpl</code> caches a list of all of the {@list SystemSearchField} entities
 * by {@Link SystemSearchArea}
 *
 * @author apopp
 */
@Component
public class SystemSearchFieldBySearchAreaCacheImpl extends SelfRegisteringSimpleDaoCache<SystemSearchField, String, List<SystemSearchField>> implements SystemSearchFieldBySearchAreaCache {

	/**
	 * A key used to determine if the cache has ever been initialized
	 */
	private static final String INITIALIZED_KEY = "IsInitialized";

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isInitialized() {
		return getBeanList(INITIALIZED_KEY) != null;
	}


	@Override
	public List<SystemSearchField> getAllSearchFieldList() {
		Collection<String> keyList = getCacheHandler().getKeys(getCacheName());
		List<SystemSearchField> searchFieldList = new ArrayList<>();

		for (String key : CollectionUtils.getIterable(keyList)) {
			List<SystemSearchField> beanList = getBeanList(key);
			if (beanList != null) {
				searchFieldList.addAll(beanList);
			}
		}

		return searchFieldList;
	}


	@Override
	public List<SystemSearchField> getBeanListForSearchArea(Integer searchAreaId) {
		return getBeanList(getBeanKeyForSearchArea(searchAreaId));
	}


	@Override
	public void setBeanListForSearchArea(Integer searchAreaId, List<SystemSearchField> beanList) {
		if (!isInitialized()) {
			setBeanList(INITIALIZED_KEY, new ArrayList<>());
		}

		setBeanList(getBeanKeyForSearchArea(searchAreaId), beanList);
	}

	////////////////////////////////////////////////////////////////////////////////


	private String getBeanKeyForSearchArea(Integer searchAreaId) {
		return "SEARCH_AREA_" + searchAreaId;
	}


	private List<SystemSearchField> getBeanList(String key) {
		return getCacheHandler().get(getCacheName(), key);
	}


	private void setBeanList(String key, List<SystemSearchField> list) {
		getCacheHandler().put(getCacheName(), key, list != null ? list : new ArrayList<>());
	}
}
