package com.clifton.system.search.setup.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class SystemSearchAnalyzerSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private Boolean searchTimeAnalyzer;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Boolean getSearchTimeAnalyzer() {
		return this.searchTimeAnalyzer;
	}


	public void setSearchTimeAnalyzer(Boolean searchTimeAnalyzer) {
		this.searchTimeAnalyzer = searchTimeAnalyzer;
	}
}
