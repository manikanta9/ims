package com.clifton.system.search.provider.transport;


import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.threadpool.ThreadPool;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.io.InputStream;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;


public class SystemSearchElasticSearchTransportProvider implements InitializingBean, DisposableBean, SystemSearchTransportProvider {

	private TransportClient elasticSearchClient;

	private static final String ELASTICSEARCH_CONFIGURATION_RESOURCE_NAME = "elasticsearch.json";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() throws Exception {
		// initialize and set search client
		InputStream elasticsearchConfigurationResource = this.getClass().getClassLoader().getResourceAsStream(ELASTICSEARCH_CONFIGURATION_RESOURCE_NAME);
		Settings.Builder builder = Settings.builder().loadFromStream(ELASTICSEARCH_CONFIGURATION_RESOURCE_NAME, elasticsearchConfigurationResource);
		TransportClient baseClient = new PreBuiltTransportClient(builder.build());
		baseClient.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(builder.get("network.host")), 9300));
		this.elasticSearchClient = baseClient;
	}


	@Override
	public void destroy() {
		// close down the elasticsearch client and all of its associated threads on shutdown
		ThreadPool.terminate(getTransportClient().threadPool(), 5, TimeUnit.SECONDS);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TransportClient getTransportClient() {
		return this.elasticSearchClient;
	}


	public void setElasticSearchClient(TransportClient elasticSearchClient) {
		this.elasticSearchClient = elasticSearchClient;
	}
}
