package com.clifton.system.search.setup.cache;

import com.clifton.system.search.setup.SystemSearchAreaEntity;

import java.util.List;


/**
 * The <code>SystemSearchAreaEntityBySearchAreaCacheImpl</code> caches a list of all of the {@list SystemSearchAreaEntity} entities
 * by {@Link SystemSearchArea}
 *
 * @author apopp
 */
public interface SystemSearchAreaEntityBySearchAreaCache {

	public boolean isInitialized();


	public List<SystemSearchAreaEntity> getAllSearchEntitiesList();


	public List<SystemSearchAreaEntity> getBeanListForSearchArea(Integer searchAreaId);


	public void setBeanListForSearchArea(Integer searchAreaId, List<SystemSearchAreaEntity> beanList);
}
