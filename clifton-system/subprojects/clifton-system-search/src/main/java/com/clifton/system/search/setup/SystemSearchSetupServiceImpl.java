package com.clifton.system.search.setup;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.search.setup.cache.SystemSearchAnalyzerCache;
import com.clifton.system.search.setup.cache.SystemSearchAreaCache;
import com.clifton.system.search.setup.cache.SystemSearchAreaEntityBySearchAreaCache;
import com.clifton.system.search.setup.cache.SystemSearchFieldBySearchAreaCache;
import com.clifton.system.search.setup.search.SystemSearchAnalyzerSearchForm;
import com.clifton.system.search.setup.search.SystemSearchAreaEntitySearchForm;
import com.clifton.system.search.setup.search.SystemSearchAreaSearchForm;
import com.clifton.system.search.setup.search.SystemSearchFieldSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class SystemSearchSetupServiceImpl implements SystemSearchSetupService {

	private AdvancedUpdatableDAO<SystemSearchArea, Criteria> systemSearchAreaDAO;
	private AdvancedUpdatableDAO<SystemSearchField, Criteria> systemSearchFieldDAO;
	private AdvancedUpdatableDAO<SystemSearchAnalyzer, Criteria> systemSearchAnalyzerDAO;
	private AdvancedUpdatableDAO<SystemSearchAreaEntity, Criteria> systemSearchAreaEntityDAO;

	private SystemSearchAreaCache systemSearchAreaCache;
	private SystemSearchAnalyzerCache systemSearchAnalyzerCache;
	private SystemSearchFieldBySearchAreaCache systemSearchFieldBySearchAreaCache;
	private SystemSearchAreaEntityBySearchAreaCache systemSearchAreaEntityBySearchAreaCache;


	////////////////////////////////////////////////////////////////////////////
	///////              System Search Analyzer Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemSearchAnalyzer getSystemSearchAnalyzer(int id) {
		return getSystemSearchAnalyzerDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemSearchAnalyzer getSystemSearchAnalyzerForSearchTime() {
		SystemSearchAnalyzer result;
		Optional<SystemSearchAnalyzer> wrappedResult = getSystemSearchAnalyzerCache().getSystemSearchAnalyzer();
		if (wrappedResult == null) {
			result = getSystemSearchAnalyzerDAO().findOneByField("searchTimeAnalyzer", true);
			getSystemSearchAnalyzerCache().setSystemSearchAnalyzer(result);
		}
		else {
			result = wrappedResult.orElse(null);
		}
		return result;
	}


	@Override
	public List<SystemSearchAnalyzer> getSystemSearchAnalyzerList(SystemSearchAnalyzerSearchForm searchForm) {
		return getSystemSearchAnalyzerDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemSearchAnalyzer saveSystemSearchAnalyzer(SystemSearchAnalyzer bean) {
		if (bean.isSearchTimeAnalyzer()) {
			SystemSearchAnalyzerSearchForm searchForm = new SystemSearchAnalyzerSearchForm();
			searchForm.setSearchTimeAnalyzer(true);
			SystemSearchAnalyzer searchAnalyzer = CollectionUtils.getFirstElement(getSystemSearchAnalyzerList(searchForm));

			ValidationUtils.assertTrue(searchAnalyzer == null || bean.equals(searchAnalyzer), "Can only have a single search time analyzer defined.");
		}

		return getSystemSearchAnalyzerDAO().save(bean);
	}


	@Override
	public void deleteSystemSearchAnalyzer(int id) {
		getSystemSearchAnalyzerDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////              System Search Area Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemSearchArea getSystemSearchArea(int id) {
		return getSystemSearchAreaDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemSearchArea> getSystemSearchAreaList(SystemSearchAreaSearchForm searchForm) {
		if (searchForm == null) {
			// return all areas from cache
			return getSystemSearchAreaCache().getSystemSearchAreaList(getSystemSearchAreaDAO());
		}
		return getSystemSearchAreaDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemSearchArea saveSystemSearchArea(SystemSearchArea bean) {
		return getSystemSearchAreaDAO().save(bean);
	}


	@Override
	public void deleteSystemSearchArea(int id) {
		getSystemSearchAreaDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////              System Search Field Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemSearchField getSystemSearchField(int id) {
		return getSystemSearchFieldDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemSearchField> getSystemSearchFieldByArea(Integer searchAreaId) {
		if (!getSystemSearchFieldBySearchAreaCache().isInitialized()) {
			//populate empty cache
			List<SystemSearchField> searchFieldList = getSystemSearchFieldDAO().findAll();
			Map<Integer, List<SystemSearchField>> areaFieldMap = BeanUtils.getBeansMap(searchFieldList, "systemSearchArea.id");

			for (Map.Entry<Integer, List<SystemSearchField>> integerListEntry : areaFieldMap.entrySet()) {
				getSystemSearchFieldBySearchAreaCache().setBeanListForSearchArea(integerListEntry.getKey(), integerListEntry.getValue());
			}
		}

		if (searchAreaId != null) {
			return getSystemSearchFieldBySearchAreaCache().getBeanListForSearchArea(searchAreaId);
		}

		return getSystemSearchFieldBySearchAreaCache().getAllSearchFieldList();
	}


	@Override
	public List<SystemSearchField> getSystemSearchFieldList(SystemSearchFieldSearchForm searchForm) {
		return getSystemSearchFieldDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemSearchField saveSystemSearchField(SystemSearchField bean) {
		ValidationUtils.assertFalse(
				((bean.getBeanPropertyToSearchAreaTable() != null && bean.getSystemTable() == null) || (bean.getSystemTable() != null && bean.getBeanPropertyToSearchAreaTable() == null)),
				"Must specify both many side table property and many side table.");

		return getSystemSearchFieldDAO().save(bean);
	}


	@Override
	public void deleteSystemSearchField(int id) {
		getSystemSearchFieldDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////              System Search Area Entity Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemSearchAreaEntity getSystemSearchAreaEntity(int id) {
		return getSystemSearchAreaEntityDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemSearchAreaEntity> getSystemSearchAreaEntityListBySearchArea(Integer searchAreaId) {
		if (!getSystemSearchAreaEntityBySearchAreaCache().isInitialized()) {
			//populate empty cache
			SystemSearchAreaEntitySearchForm searchForm = new SystemSearchAreaEntitySearchForm();
			List<SystemSearchAreaEntity> searchFieldList = getSystemSearchAreaEntityList(searchForm);
			Map<Integer, List<SystemSearchAreaEntity>> areaFieldMap = BeanUtils.getBeansMap(searchFieldList, "systemSearchArea.id");

			for (Map.Entry<Integer, List<SystemSearchAreaEntity>> integerListEntry : areaFieldMap.entrySet()) {
				getSystemSearchAreaEntityBySearchAreaCache().setBeanListForSearchArea(integerListEntry.getKey(), integerListEntry.getValue());
			}
		}

		if (searchAreaId != null) {
			return getSystemSearchAreaEntityBySearchAreaCache().getBeanListForSearchArea(searchAreaId);
		}

		return getSystemSearchAreaEntityBySearchAreaCache().getAllSearchEntitiesList();
	}


	@Override
	public List<SystemSearchAreaEntity> getSystemSearchAreaEntityList(SystemSearchAreaEntitySearchForm searchForm) {
		return getSystemSearchAreaEntityDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemSearchAreaEntity saveSystemSearchAreaEntity(SystemSearchAreaEntity bean) {
		return getSystemSearchAreaEntityDAO().save(bean);
	}


	@Override
	public void deleteSystemSearchAreaEntity(int id) {
		getSystemSearchAreaEntityDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemSearchArea, Criteria> getSystemSearchAreaDAO() {
		return this.systemSearchAreaDAO;
	}


	public void setSystemSearchAreaDAO(AdvancedUpdatableDAO<SystemSearchArea, Criteria> systemSearchAreaDAO) {
		this.systemSearchAreaDAO = systemSearchAreaDAO;
	}


	public AdvancedUpdatableDAO<SystemSearchField, Criteria> getSystemSearchFieldDAO() {
		return this.systemSearchFieldDAO;
	}


	public void setSystemSearchFieldDAO(AdvancedUpdatableDAO<SystemSearchField, Criteria> systemSearchFieldDAO) {
		this.systemSearchFieldDAO = systemSearchFieldDAO;
	}


	public AdvancedUpdatableDAO<SystemSearchAnalyzer, Criteria> getSystemSearchAnalyzerDAO() {
		return this.systemSearchAnalyzerDAO;
	}


	public void setSystemSearchAnalyzerDAO(AdvancedUpdatableDAO<SystemSearchAnalyzer, Criteria> systemSearchAnalyzerDAO) {
		this.systemSearchAnalyzerDAO = systemSearchAnalyzerDAO;
	}


	public AdvancedUpdatableDAO<SystemSearchAreaEntity, Criteria> getSystemSearchAreaEntityDAO() {
		return this.systemSearchAreaEntityDAO;
	}


	public void setSystemSearchAreaEntityDAO(AdvancedUpdatableDAO<SystemSearchAreaEntity, Criteria> systemSearchAreaEntityDAO) {
		this.systemSearchAreaEntityDAO = systemSearchAreaEntityDAO;
	}


	public SystemSearchAreaCache getSystemSearchAreaCache() {
		return this.systemSearchAreaCache;
	}


	public void setSystemSearchAreaCache(SystemSearchAreaCache systemSearchAreaCache) {
		this.systemSearchAreaCache = systemSearchAreaCache;
	}


	public SystemSearchAnalyzerCache getSystemSearchAnalyzerCache() {
		return this.systemSearchAnalyzerCache;
	}


	public void setSystemSearchAnalyzerCache(SystemSearchAnalyzerCache systemSearchAnalyzerCache) {
		this.systemSearchAnalyzerCache = systemSearchAnalyzerCache;
	}


	public SystemSearchFieldBySearchAreaCache getSystemSearchFieldBySearchAreaCache() {
		return this.systemSearchFieldBySearchAreaCache;
	}


	public void setSystemSearchFieldBySearchAreaCache(SystemSearchFieldBySearchAreaCache systemSearchFieldBySearchAreaCache) {
		this.systemSearchFieldBySearchAreaCache = systemSearchFieldBySearchAreaCache;
	}


	public SystemSearchAreaEntityBySearchAreaCache getSystemSearchAreaEntityBySearchAreaCache() {
		return this.systemSearchAreaEntityBySearchAreaCache;
	}


	public void setSystemSearchAreaEntityBySearchAreaCache(SystemSearchAreaEntityBySearchAreaCache systemSearchAreaEntityBySearchAreaCache) {
		this.systemSearchAreaEntityBySearchAreaCache = systemSearchAreaEntityBySearchAreaCache;
	}
}
