package com.clifton.system.search.setup;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemSearchField</code> defines a field on a specific search area that we should index.
 * <p>
 * This typically defines what columns to pull in from a specific area and how to rank them amongst each other.
 *
 * @author apopp
 */
public class SystemSearchField extends NamedEntity<Integer> {

	/**
	 * Path to search on for many side system table
	 */
	private String beanPropertyToSearchAreaTable;

	/**
	 * Many side table to search on using searchAreaBeanProperty
	 */
	private SystemTable systemTable;

	/**
	 * Search area that this field falls under
	 */
	private SystemSearchArea systemSearchArea;

	/**
	 * Specify the elasticsearch index analyzer to use. This analyzer is what
	 * elasticsearch will use to parse this specific field. This is useful for different field
	 * types such as Contact Name.  This is an example where we'd like to insert the name
	 * into search but have search parse it into pieces so we can return full results on
	 * a partial query. "Vlad" will return "Vladimir"
	 */
	private SystemSearchAnalyzer systemSearchAnalyzer;

	/**
	 * The rank this field has in comparison to the other fields in this area.
	 */
	private Integer searchFieldRank;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSearchArea getSystemSearchArea() {
		return this.systemSearchArea;
	}


	public void setSystemSearchArea(SystemSearchArea systemSearchArea) {
		this.systemSearchArea = systemSearchArea;
	}


	public Integer getSearchFieldRank() {
		return this.searchFieldRank;
	}


	public void setSearchFieldRank(Integer searchFieldRank) {
		this.searchFieldRank = searchFieldRank;
	}


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	public String getBeanPropertyToSearchAreaTable() {
		return this.beanPropertyToSearchAreaTable;
	}


	public void setBeanPropertyToSearchAreaTable(String beanPropertyToSearchAreaTable) {
		this.beanPropertyToSearchAreaTable = beanPropertyToSearchAreaTable;
	}


	public SystemSearchAnalyzer getSystemSearchAnalyzer() {
		return this.systemSearchAnalyzer;
	}


	public void setSystemSearchAnalyzer(SystemSearchAnalyzer systemSearchAnalyzer) {
		this.systemSearchAnalyzer = systemSearchAnalyzer;
	}
}
