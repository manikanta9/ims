package com.clifton.system.search.provider.transport;


import org.elasticsearch.client.transport.TransportClient;


/**
 * The <code>SystemSearchTransportProvider</code> provides the underlying transport connection to the search engine
 *
 * @author apopp
 */
public interface SystemSearchTransportProvider {

	public static final int MAX_SEARCH_SIZE = 60;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Return the connection to communicate to the search engine
	 */
	public TransportClient getTransportClient();
}
