package com.clifton.system.search.provider.search;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.system.search.setup.SystemSearchArea;


/**
 * The <code>SystemSearchResult</code> represents a search result from the search engine.
 * <p>
 * It details which area it was from and the resultant display name and tooltip. It also references the primary key.
 *
 * @author apopp
 */
@NonPersistentObject
public class SystemSearchResult {

	/**
	 * The search area this result falls under
	 */
	private SystemSearchArea systemSearchArea;

	/**
	 * Label to display for result
	 */
	private String label;

	/**
	 * Tooltip to display on hover of label
	 */
	private String tooltip;

	/**
	 * Primary key of entity this result is associated to
	 */
	private int pkFieldId;

	/**
	 * Screen to open for this entity
	 */
	private String screenClass;

	/**
	 * UI config to open within the screen
	 */
	private String userInterfaceConfig;

	/**
	 * In some cases we want to return back the entire json document backing this search result.
	 * This is used administratively for debugging purposes.
	 */
	private String searchDocument;

	/**
	 * If the search document is included, also include the explanation to how this result was selected.
	 */
	private String searchExplanation;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getPkFieldId() {
		return this.pkFieldId;
	}


	public void setPkFieldId(int pkFieldId) {
		this.pkFieldId = pkFieldId;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getTooltip() {
		return this.tooltip;
	}


	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}


	public String getScreenClass() {
		return this.screenClass;
	}


	public void setScreenClass(String screenClass) {
		this.screenClass = screenClass;
	}


	public String getSearchDocument() {
		return this.searchDocument;
	}


	public void setSearchDocument(String searchDocument) {
		this.searchDocument = searchDocument;
	}


	public String getSearchExplanation() {
		return this.searchExplanation;
	}


	public void setSearchExplanation(String searchExplanation) {
		this.searchExplanation = searchExplanation;
	}


	public String getUserInterfaceConfig() {
		return this.userInterfaceConfig;
	}


	public void setUserInterfaceConfig(String userInterfaceConfig) {
		this.userInterfaceConfig = userInterfaceConfig;
	}


	public SystemSearchArea getSystemSearchArea() {
		return this.systemSearchArea;
	}


	public void setSystemSearchArea(SystemSearchArea systemSearchArea) {
		this.systemSearchArea = systemSearchArea;
	}
}
