package com.clifton.system.search.setup.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.system.search.setup.SystemSearchAnalyzer;
import org.springframework.stereotype.Component;

import java.util.Optional;


/**
 * The <code>SystemSearchAnalyzerCacheImpl</code> caches the only search analyzer we have that has time enabled.  There is already
 * validation that there can be only one (and retrieval below also enforces there is only one).
 * <p>
 * When the {@link SystemSearchAnalyzer} object is updated - if it was previously, or is now a time analyzer, the cache is cleared.
 *
 * @author manderson
 */
@Component
public class SystemSearchAnalyzerCacheImpl extends SelfRegisteringSimpleDaoCache<SystemSearchAnalyzer, String, Optional<SystemSearchAnalyzer>> implements SystemSearchAnalyzerCache {

	private static final String CACHE_KEY = "SEARCH_TIME_ANALYZER";

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Optional<SystemSearchAnalyzer> getSystemSearchAnalyzer() {
		return getCacheHandler().get(getCacheName(), CACHE_KEY);
	}


	@Override
	public void setSystemSearchAnalyzer(SystemSearchAnalyzer searchTimeAnalyzer) {
		getCacheHandler().put(getCacheName(), CACHE_KEY, Optional.ofNullable(searchTimeAnalyzer));
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<SystemSearchAnalyzer> dao, DaoEventTypes event, SystemSearchAnalyzer bean) {
		if (event.isUpdate()) { // Only needed for updates in case the key has changed - want to clear the original key so we don't leave bad objects in the cache
			// Call it so we have the original
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<SystemSearchAnalyzer> dao, DaoEventTypes event, SystemSearchAnalyzer bean, Throwable e) {
		if (e == null) {
			// If the Bean is Search Time Analyzer - Clear the Cache so next retrieval will get updated bean
			SystemSearchAnalyzer originalBean = (event.isUpdate() ? getOriginalBean(dao, bean) : null);

			//  if the bean is a SearchTimeAnalyzer() or if this is an update and it was previously a search time analyzer - clear the cache since this bean is no longer time analyzer
			if ((bean.isSearchTimeAnalyzer()) || (originalBean != null && originalBean.isSearchTimeAnalyzer())) {
				getCacheHandler().clear(getCacheName());
			}
		}
	}
}
