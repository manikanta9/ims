package com.clifton.system.search.provider.index;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.search.provider.transport.SystemSearchTransportProvider;
import com.clifton.system.search.provider.util.SystemSearchElasticSearchUtil;
import com.clifton.system.search.setup.SystemSearchAnalyzer;
import com.clifton.system.search.setup.SystemSearchArea;
import com.clifton.system.search.setup.SystemSearchAreaEntity;
import com.clifton.system.search.setup.SystemSearchField;
import com.clifton.system.search.setup.SystemSearchSetupService;
import com.clifton.system.search.setup.search.SystemSearchAnalyzerSearchForm;
import com.clifton.system.search.setup.search.SystemSearchFieldSearchForm;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.flush.FlushRequest;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.IndexNotFoundException;
import org.elasticsearch.indices.InvalidIndexNameException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Component
public class SystemSearchElasticSearchIndexProvider<T extends IdentityObject> implements SystemSearchIndexProvider {

	/**
	 * Should inject the environment level property value. Will default it to empty string.
	 * <p>
	 * A property to be injected that is NOT defined in a context will result in a null value being set.
	 * In the case of the elasticsearch index name construction, this would result in a value of null_null,
	 * which is a valid index name for ES and would result in erroneous indexes being created.
	 * <p>
	 * Because we have set the default value to an empty string here the resulting index name will just be "_"
	 * which is an invalid index name, so index creation will fail and an error will be returned to that effect.
	 */
	@Value("${elasticsearch.environment.level:}")
	private String searchEnvironmentLevel;

	/**
	 * Should inject the application name property value. Will default it to empty string.
	 * <p>
	 * A property to be injected that is NOT defined in a context will result in a null value being set.
	 * In the case of the elasticsearch index name construction, this would result in a value of null_null,
	 * which is a valid index name for ES and would result in erroneous indexes being created.
	 * <p>
	 * Because we have set the default value to an empty string here the resulting index name will just be "_"
	 * which is an invalid index name, so index creation will fail and an error will be returned to that effect.
	 */
	@Value("${elasticsearch.application.name:}")
	private String searchApplicationName;

	private DaoLocator daoLocator;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemSearchSetupService systemSearchSetupService;
	private SystemSearchTransportProvider systemSearchTransportProvider;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void createSearchIndex(String indexName) {
		List<SystemSearchAnalyzer> searchAnalyzerList = getSystemSearchSetupService().getSystemSearchAnalyzerList(new SystemSearchAnalyzerSearchForm());
		try {
			getTransportClient().admin().indices().prepareCreate(indexName).setSettings(SystemSearchElasticSearchUtil.buildSearchIndexQuery(searchAnalyzerList), XContentType.JSON).execute().actionGet();
		}
		catch (InvalidIndexNameException iine) {
			throw new RuntimeException("Failed to create index [" + indexName + "] because the requested name is invalid. Ensure that values are set in the properties: elasticsearch.environment.level and elasticsearch.application.name", iine);
		}
	}


	@Override
	public void deleteSearchIndex(String indexName) {
		try {
			getTransportClient().admin().indices().delete(new DeleteIndexRequest(indexName)).actionGet();
		}
		catch (IndexNotFoundException infe) {
			throw new RuntimeException("Failed to delete index [" + indexName + "] because it does not exist. Ensure that values are set in the properties: elasticsearch.environment.level and elasticsearch.application.name", infe);
		}
		catch (Throwable e) {
			throw new RuntimeException("Error deleting Search Index: " + indexName, e);
		}
	}


	@Override
	public void flushSearchIndex(String index) {
		getTransportClient().admin().indices().flush(new FlushRequest(index)).actionGet();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@DoNotAddRequestMapping
	public String getSearchIndexName() {
		return SystemSearchElasticSearchUtil.getSearchIndexName(getSearchEnvironmentLevel(), getSearchApplicationName());
	}


	@Override
	@DoNotAddRequestMapping
	public String getSearchIndexNameForApplication(String applicationName) {
		return SystemSearchElasticSearchUtil.getSearchIndexName(getSearchEnvironmentLevel(), (StringUtils.isEmpty(applicationName) ? getSearchApplicationName() : applicationName));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void reindexSystemSearchArea(SystemSearchArea searchArea) {
		reindexSystemSearchArea(searchArea, 0, 1000, null);
		flushSearchIndex(getSearchIndexName());
	}


	@Override
	public int reindexSystemSearchArea(SystemSearchArea searchArea, int hoursBackToReindex, int bulkInsertMax, List<String> errors) {
		List<T> updatedList = getEntityListByTable(searchArea.getSystemTable().getName(), hoursBackToReindex);

		filterEntityListByCondition(updatedList, searchArea.getFilterSystemCondition());

		SystemSearchFieldSearchForm searchForm = new SystemSearchFieldSearchForm();
		searchForm.setSearchAreaId(searchArea.getId());
		List<SystemSearchField> searchFieldList = getSystemSearchSetupService().getSystemSearchFieldList(searchForm);

		// Create or update the properties mapping on this field as to how it should be analyzed and indexed when a value
		// is inserted for it. This list of fields must be for a specific search area.
		saveSystemSearchIndexFieldPropertiesMapping(searchFieldList);

		indexEntityList(searchArea, searchFieldList, updatedList, bulkInsertMax, errors);

		return CollectionUtils.getSize(updatedList);
	}


	/**
	 * Filter the entities under this area by any specified filter condition
	 */
	private void filterEntityListByCondition(List<T> entityList, SystemCondition condition) {
		if (condition == null) {
			return;
		}

		Iterator<T> entityIterator = entityList.iterator();
		while (entityIterator.hasNext()) {
			T entity = entityIterator.next();

			boolean isFilter = getSystemConditionEvaluationHandler().isConditionTrue(condition, entity);

			if (!isFilter) {
				entityIterator.remove();
			}
		}
	}


	private void saveSystemSearchIndexFieldPropertiesMapping(List<SystemSearchField> searchFieldList) {
		SystemSearchField firstSearchField = CollectionUtils.getFirstElement(searchFieldList);

		if (firstSearchField != null) {
			//Code here used to delete a mapping for a specific type from the index mapping. Elasticsearch now recommends that there be a
			// new index for each type. TODO: Split types into separate indexes

			for (SystemSearchField systemSearchField : CollectionUtils.getIterable(searchFieldList)) {
				String analyzer = (systemSearchField.getSystemSearchAnalyzer() != null) ? systemSearchField.getSystemSearchAnalyzer().getName().toLowerCase().replace(" ", "_") : "default";
				String mapping = "{\"" + systemSearchField.getSystemSearchArea().getId() + "\":{\"properties\":{\"" + systemSearchField.getSystemSearchArea().getId()
						+ systemSearchField.getName().replace(".", "") + ""
						+ ((systemSearchField.getSystemSearchAnalyzer() != null) ? systemSearchField.getSystemSearchAnalyzer().getName().toLowerCase().replace(" ", "_") : "default")
						+ "\":{\"include_in_all\":false,\"type\":\"string\",\"analyzer\":\"" + analyzer + "\"}}}}";

				// Create new search field mapping
				getTransportClient().admin()
						.indices()
						.preparePutMapping(getSearchIndexName())
						.setType(systemSearchField.getSystemSearchArea().getId().toString())
						.setSource(mapping, XContentType.JSON)
						.execute()
						.actionGet();
			}
		}
	}


	private List<T> getEntityListByTable(String tableName, final int hoursBackTo) {
		final Date fromDate = DateUtils.addMinutes(new Date(), -hoursBackTo * 60);

		ReadOnlyDAO<T> dao = getDaoLocator().locate(tableName);
		AdvancedReadOnlyDAO<T, Criteria> searchDao = (AdvancedReadOnlyDAO<T, Criteria>) dao;

		return searchDao.findBySearchCriteria((HibernateSearchConfigurer) criteria -> {
			// 0 means get everything
			if (hoursBackTo != 0) {
				criteria.add(Restrictions.ge("updateDate", fromDate));
			}
		});
	}


	private void indexEntityList(SystemSearchArea searchArea, List<SystemSearchField> searchFieldList, List<T> entityList, int bulkInsertMax, List<String> errors) {
		BulkRequestBuilder bulkRequest = getTransportClient().prepareBulk();
		int count = 0;

		List<SystemSearchAreaEntity> entityRankList = getSystemSearchSetupService().getSystemSearchAreaEntityListBySearchArea(searchArea.getId());
		Map<Integer, List<SystemSearchAreaEntity>> entityRankMap = BeanUtils.getBeansMap(entityRankList, SystemSearchAreaEntity::getFkFieldId);

		for (T entity : CollectionUtils.getIterable(entityList)) {

			try {
				bulkRequest.add(indexEntity(searchArea, searchFieldList, entity, entityRankMap.get(BeanUtils.getIdentityAsInteger(entity))));
			}
			catch (Throwable e) {
				ExceptionUtils.addErrorOrThrowException(errors, "Error indexing entity list for " + searchArea, e);
			}

			// Bulk index every getBulkInsertMax amount
			count++;
			if (count % bulkInsertMax == 0) {
				try {
					//Bulk insert
					bulkRequest.execute().get();
				}
				catch (Throwable e) {
					if (e instanceof InterruptedException) {
						Thread.currentThread().interrupt();
					}
					ExceptionUtils.addErrorOrThrowException(errors, "Error executing bulk insert for " + searchArea, e);
				}
				bulkRequest = getTransportClient().prepareBulk();
			}
		}

		// Bulk insert, Insert any remaining less than 1000
		if (CollectionUtils.getSize(entityList) % bulkInsertMax != 0) {
			try {
				bulkRequest.execute().get();
			}
			catch (Throwable e) {
				if (e instanceof InterruptedException) {
					Thread.currentThread().interrupt();
				}
				ExceptionUtils.addErrorOrThrowException(errors, "Error executing bulk insert for " + searchArea, e);
			}
		}
	}


	private IndexRequestBuilder indexEntity(SystemSearchArea searchArea, List<SystemSearchField> searchFieldList, T entity, List<SystemSearchAreaEntity> entityRankList) throws IOException {
		Integer searchAreaId = searchArea.getId();
		Serializable id = entity.getIdentity();
		XContentBuilder contentBuilder = SystemSearchElasticSearchUtil.buildEntityIndexQuery(searchArea, searchFieldList, entity, entityRankList);

		// Build many table indices if it exists
		List<SystemSearchField> manyTableSearchFieldList = BeanUtils.filterNotNull(searchFieldList, SystemSearchField::getBeanPropertyToSearchAreaTable);
		Map<String, List<SystemSearchField>> manyTableFieldMap = BeanUtils.getBeansMap(manyTableSearchFieldList, "systemTable.name");
		Map<String, List<T>> entityListCacheMap = new HashMap<>();
		for (Map.Entry<String, List<SystemSearchField>> stringListEntry : manyTableFieldMap.entrySet()) {
			buildFieldManyTableIndex(contentBuilder, entity.getIdentity(), stringListEntry.getValue(), entityListCacheMap);
		}

		contentBuilder.endObject();
		return getTransportClient().prepareIndex(getSearchIndexName(), searchAreaId.toString(), id.toString()).setSource(contentBuilder);
	}


	/**
	 * Index fields for a given entity id. We will pull the many table relationship from the system table associated to the field list. This field list is all fields for a specific
	 * system table under this entities search area.
	 */
	private void buildFieldManyTableIndex(XContentBuilder contentBuilder, Serializable entityId, List<SystemSearchField> fieldList, Map<String, List<T>> entityListCacheMap) {
		for (SystemSearchField field : CollectionUtils.getIterable(fieldList)) {
			List<T> manyTableEntityList = getEntityListByTableField(field.getSystemTable().getName(), field.getBeanPropertyToSearchAreaTable(), entityId, entityListCacheMap);
			if (!CollectionUtils.isEmpty(manyTableEntityList)) {
				SystemSearchElasticSearchUtil.buildFieldIndex(contentBuilder, field, manyTableEntityList.toArray());
			}
		}
	}


	private List<T> getEntityListByTableField(String tableName, String beanPropertyToSearchAreaTable, Object beanPropertyValue, Map<String, List<T>> entityListCacheMap) {
		if (entityListCacheMap == null) {
			entityListCacheMap = new HashMap<>();
		}
		String key = tableName + ":" + beanPropertyToSearchAreaTable + ":" + beanPropertyValue;
		List<T> entityList = entityListCacheMap.get(key);

		if (entityList == null) {
			ReadOnlyDAO<T> dao = getDaoLocator().locate(tableName);
			entityList = dao.findByField(beanPropertyToSearchAreaTable, beanPropertyValue);
			entityListCacheMap.put(key, entityList);
		}

		return entityList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchEnvironmentLevel() {
		return this.searchEnvironmentLevel;
	}


	public void setSearchEnvironmentLevel(String searchEnvironmentLevel) {
		this.searchEnvironmentLevel = searchEnvironmentLevel;
	}


	public String getSearchApplicationName() {
		return this.searchApplicationName;
	}


	public void setSearchApplicationName(String searchApplicationName) {
		this.searchApplicationName = searchApplicationName;
	}


	public SystemSearchSetupService getSystemSearchSetupService() {
		return this.systemSearchSetupService;
	}


	public void setSystemSearchSetupService(SystemSearchSetupService systemSearchSetupService) {
		this.systemSearchSetupService = systemSearchSetupService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SystemSearchTransportProvider getSystemSearchTransportProvider() {
		return this.systemSearchTransportProvider;
	}


	public void setSystemSearchTransportProvider(SystemSearchTransportProvider systemSearchTransportProvider) {
		this.systemSearchTransportProvider = systemSearchTransportProvider;
	}


	private TransportClient getTransportClient() {
		return getSystemSearchTransportProvider().getTransportClient();
	}
}
