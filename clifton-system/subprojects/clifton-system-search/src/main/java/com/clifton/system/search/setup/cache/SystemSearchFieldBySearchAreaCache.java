package com.clifton.system.search.setup.cache;

import com.clifton.system.search.setup.SystemSearchField;

import java.util.List;


/**
 * The <code>SystemSearchFieldBySearchAreaCacheImpl</code> caches a list of all of the {@list SystemSearchField} entities
 * by {@Link SystemSearchArea}
 *
 * @author apopp
 */
public interface SystemSearchFieldBySearchAreaCache {


	public boolean isInitialized();


	public List<SystemSearchField> getAllSearchFieldList();


	public List<SystemSearchField> getBeanListForSearchArea(Integer searchAreaId);


	public void setBeanListForSearchArea(Integer searchAreaId, List<SystemSearchField> beanList);
}
