package com.clifton.system.search.setup.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.search.setup.SystemSearchAreaEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * The <code>SystemSearchAreaEntityBySearchAreaCacheImpl</code> caches a list of all of the {@list SystemSearchAreaEntity} entities
 * by {@Link SystemSearchArea}
 *
 * @author apopp
 */
@Component
public class SystemSearchAreaEntityBySearchAreaCacheImpl extends SelfRegisteringSimpleDaoCache<SystemSearchAreaEntity, String, List<SystemSearchAreaEntity>> implements SystemSearchAreaEntityBySearchAreaCache {

	/**
	 * A key used to determine if the cache has ever been initialized
	 */
	private static final String INITIALIZED_KEY = "IsInitialized";

	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isInitialized() {
		return getBeanList(INITIALIZED_KEY) != null;
	}


	@Override
	public List<SystemSearchAreaEntity> getAllSearchEntitiesList() {
		Collection<String> keyList = getCacheHandler().getKeys(getCacheName());
		List<SystemSearchAreaEntity> searchFieldList = new ArrayList<>();

		for (String key : CollectionUtils.getIterable(keyList)) {
			List<SystemSearchAreaEntity> beanList = getBeanList(key);
			if (beanList != null) {
				searchFieldList.addAll(getBeanList(key));
			}
		}

		return searchFieldList;
	}


	@Override
	public List<SystemSearchAreaEntity> getBeanListForSearchArea(Integer searchAreaId) {
		return getBeanList(getBeanKeyForSearchArea(searchAreaId));
	}


	@Override
	public void setBeanListForSearchArea(Integer searchAreaId, List<SystemSearchAreaEntity> beanList) {
		if (!isInitialized()) {
			setBeanList(INITIALIZED_KEY, new ArrayList<>());
		}

		setBeanList(getBeanKeyForSearchArea(searchAreaId), beanList);
	}

	////////////////////////////////////////////////////////////////////////////////


	private String getBeanKeyForSearchArea(Integer searchAreaId) {
		return "SEARCH_AREA_" + searchAreaId;
	}


	private List<SystemSearchAreaEntity> getBeanList(String key) {
		return getCacheHandler().get(getCacheName(), key);
	}


	private void setBeanList(String key, List<SystemSearchAreaEntity> list) {
		getCacheHandler().put(getCacheName(), key, list != null ? list : new ArrayList<>());
	}
}
