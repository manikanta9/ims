package com.clifton.system.search.indexing;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.system.search.provider.index.SystemSearchIndexProvider;
import com.clifton.system.search.setup.SystemSearchArea;
import com.clifton.system.search.setup.SystemSearchSetupService;
import com.clifton.system.search.setup.search.SystemSearchAreaSearchForm;
import org.elasticsearch.index.IndexNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemSearchIndexingJob</code> goes through each system search area and indexes it based on
 * it's properties and which system search fields are defined for it.
 *
 * @author apopp
 */
public class SystemSearchIndexingJob implements Task {

	private SystemSearchIndexProvider systemSearchIndexProvider;
	private SystemSearchSetupService systemSearchSetupService;

	private int hoursBackToReindex = 2;
	private int bulkInsertMax = 1000;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		StringBuilder message = new StringBuilder(64);
		Status status = Status.ofMessage("Starting SystemSearchIndexingJob");

		// NOTE This deletes every document under this index type (search area)
		if (getHoursBackToReindex() == 0) {
			try {
				getSystemSearchIndexProvider().deleteSearchIndex(getSystemSearchIndexProvider().getSearchIndexName());
			}
			catch (Throwable e) {
				if (e.getCause() instanceof IndexNotFoundException) {
					status.addMessage("Could not delete index [" + getSystemSearchIndexProvider().getSearchIndexName() + "] because it did not exist. Proceeding to index creation.");
				}
				else {
					status.addError(ExceptionUtils.getDetailedMessage(e));
				}
			}
			getSystemSearchIndexProvider().createSearchIndex(getSystemSearchIndexProvider().getSearchIndexName());
		}

		List<SystemSearchArea> searchAreaList = getSystemSearchSetupService().getSystemSearchAreaList(new SystemSearchAreaSearchForm());
		for (SystemSearchArea systemSearchArea : CollectionUtils.getIterable(searchAreaList)) {
			long startTime = System.currentTimeMillis();

			List<String> errors = new ArrayList<>();
			int updatedCount = getSystemSearchIndexProvider().reindexSystemSearchArea(systemSearchArea, getHoursBackToReindex(), getBulkInsertMax(), errors);
			status.addErrors(errors);

			if (message.length() > 0) {
				message.append('\n');
			}

			message.append(systemSearchArea.getName());
			message.append(": indexed records = ").append(updatedCount).append("; ");

			long endTime = System.currentTimeMillis();
			message.append(" [time = ").append((endTime - startTime) / 1000).append(" seconds]");
		}

		if (getHoursBackToReindex() == 0) {
			getSystemSearchIndexProvider().flushSearchIndex(getSystemSearchIndexProvider().getSearchIndexName());
		}

		status.setMessage(message.toString());
		return status;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public int getBulkInsertMax() {
		return this.bulkInsertMax;
	}


	public void setBulkInsertMax(int bulkInsertMax) {
		this.bulkInsertMax = bulkInsertMax;
	}


	public int getHoursBackToReindex() {
		return this.hoursBackToReindex;
	}


	public void setHoursBackToReindex(int hoursBackToReindex) {
		this.hoursBackToReindex = hoursBackToReindex;
	}


	public SystemSearchSetupService getSystemSearchSetupService() {
		return this.systemSearchSetupService;
	}


	public void setSystemSearchSetupService(SystemSearchSetupService systemSearchSetupService) {
		this.systemSearchSetupService = systemSearchSetupService;
	}


	public SystemSearchIndexProvider getSystemSearchIndexProvider() {
		return this.systemSearchIndexProvider;
	}


	public void setSystemSearchIndexProvider(SystemSearchIndexProvider systemSearchIndexProvider) {
		this.systemSearchIndexProvider = systemSearchIndexProvider;
	}
}
