package com.clifton.system.search.setup;


import com.clifton.system.search.setup.search.SystemSearchAnalyzerSearchForm;
import com.clifton.system.search.setup.search.SystemSearchAreaEntitySearchForm;
import com.clifton.system.search.setup.search.SystemSearchAreaSearchForm;
import com.clifton.system.search.setup.search.SystemSearchFieldSearchForm;

import java.util.List;


/**
 * The <code>SystemSearchSetupService</code> manages the basic DAO operations of search specific DTOs
 *
 * @author apopp
 */
public interface SystemSearchSetupService {

	////////////////////////////////////////////////////////////////////////////
	///////              System Search Analyzer Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSearchAnalyzer getSystemSearchAnalyzer(int id);


	/**
	 * Returns the SystemSearchAnalyzer that has searchTimeAnalyzer property set to true.
	 * There can be only one search time analyzer.
	 */
	public SystemSearchAnalyzer getSystemSearchAnalyzerForSearchTime();


	public List<SystemSearchAnalyzer> getSystemSearchAnalyzerList(SystemSearchAnalyzerSearchForm searchForm);


	public SystemSearchAnalyzer saveSystemSearchAnalyzer(SystemSearchAnalyzer bean);


	public void deleteSystemSearchAnalyzer(int id);

	////////////////////////////////////////////////////////////////////////////
	///////              System Search Area Business Methods           ///////// 
	////////////////////////////////////////////////////////////////////////////


	public SystemSearchArea getSystemSearchArea(int id);


	public List<SystemSearchArea> getSystemSearchAreaList(SystemSearchAreaSearchForm searchForm);


	public SystemSearchArea saveSystemSearchArea(SystemSearchArea bean);


	public void deleteSystemSearchArea(int id);


	////////////////////////////////////////////////////////////////////////////
	///////              System Search Field Business Methods          ///////// 
	////////////////////////////////////////////////////////////////////////////


	public SystemSearchField getSystemSearchField(int id);


	/**
	 * Returns the List of SystemSearchField objects for the specified search area.
	 * If the argument is null, returns all fields.
	 */
	public List<SystemSearchField> getSystemSearchFieldByArea(Integer searchAreaId);


	public List<SystemSearchField> getSystemSearchFieldList(SystemSearchFieldSearchForm searchForm);


	public SystemSearchField saveSystemSearchField(SystemSearchField bean);


	public void deleteSystemSearchField(int id);


	////////////////////////////////////////////////////////////////////////////
	///////              System Search Area Entity Business Methods    ///////// 
	////////////////////////////////////////////////////////////////////////////


	public SystemSearchAreaEntity getSystemSearchAreaEntity(int id);


	public List<SystemSearchAreaEntity> getSystemSearchAreaEntityListBySearchArea(Integer searchAreaId);


	public List<SystemSearchAreaEntity> getSystemSearchAreaEntityList(SystemSearchAreaEntitySearchForm searchForm);


	public SystemSearchAreaEntity saveSystemSearchAreaEntity(SystemSearchAreaEntity bean);


	public void deleteSystemSearchAreaEntity(int id);
}
