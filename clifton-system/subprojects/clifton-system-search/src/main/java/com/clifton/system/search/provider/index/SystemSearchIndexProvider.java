package com.clifton.system.search.provider.index;


import com.clifton.system.search.setup.SystemSearchArea;

import java.util.List;


/**
 * The <code>SystemSearchIndexProvider</code> provides methods for creating, deleting, and updating,
 * documents within the search engine
 *
 * @author apopp
 */
public interface SystemSearchIndexProvider {


	/**
	 * Create index with optimal default configuration settings
	 */
	public void createSearchIndex(String indexName);


	/**
	 * Completely erase an index. BE CAREFUL
	 * <p>
	 * You can erase everything in the search engine in under 5 milliseconds.
	 */
	public void deleteSearchIndex(String indexName);


	/**
	 * The flush process of an index basically frees memory from the index
	 * by flushing data to the index storage and clearing the internal transaction log. By default, search uses
	 * memory heuristics in order to automatically trigger flush operations as required in order to clear memory.
	 * <p>
	 * This is good to do when you change a lot of documents.
	 */
	public void flushSearchIndex(String index);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the properly formatted index name
	 */
	public String getSearchIndexName();


	/**
	 * Gets the proper index name.
	 * <p>
	 * If applicationName is specified (is not empty), it will be used instead of the default applicationName;
	 */
	public String getSearchIndexNameForApplication(String applicationName);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Entirely reindex a specific system search area
	 */
	public void reindexSystemSearchArea(SystemSearchArea systemSearchArea);


	/**
	 * Index entities under a specific search area
	 *
	 * @param systemSearchArea   - area to index
	 * @param hoursBackToReindex - hours back to check for updates (0 means ALL)
	 * @param bulkInsertMax      - How many entities to build the index for before submitting them to the search server (1000)
	 * @param errors             - optional list of error messages to collect, if null, first exception will be thrown
	 * @return - the number of entities indexed
	 */
	public int reindexSystemSearchArea(SystemSearchArea systemSearchArea, int hoursBackToReindex, int bulkInsertMax, List<String> errors);
}
