package com.clifton.system.search.provider.search;


import com.clifton.system.search.SystemSearchSearchForm;

import java.util.List;


/**
 * The <code>SystemSearchProvider</code> is used for executing a search to the search engine
 *
 * @author apopp
 */
public interface SystemSearchProvider {

	/**
	 * Returns the List of SystemSearchResult objects that match the specified search parameters.
	 */
	public List<SystemSearchResult> getSearchResultList(SystemSearchSearchForm searchForm);
}
