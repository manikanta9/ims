package com.clifton.system.search;


import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.search.form.SearchForm;

import java.io.Serializable;


/**
 * The <code>SystemSearchSearchForm</code> is a virtual search form used to determine
 * what request to build to send to the search engine.
 *
 * @author apopp
 */
@NonPersistentObject
@SearchForm(hasOrmDtoClass = false)
public class SystemSearchSearchForm implements PagingCommand, Serializable {

	/**
	 * Used for paging
	 */
	private int start = DEFAULT_START;
	private int limit = DEFAULT_LIMIT;
	private Integer maxLimit;

	/**
	 * Search size defines the max result size to search for. This resultant set will be paged through using the
	 * paging start and limit parameters.
	 */
	private Integer searchSize;

	/**
	 * The search pattern to use for performing the search query
	 */
	private String searchPattern;

	/**
	 * The search area to restrict query search under
	 */
	private Integer searchAreaId;

	/**
	 * Should this search include the json document in it's response
	 */
	private boolean includeDocument;

	/**
	 * Overrides the default application name when searching
	 * <p>
	 * I.e. enables the ability to search other applications indexes
	 */
	private String applicationName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getSearchAreaId() {
		return this.searchAreaId;
	}


	public void setSearchAreaId(Integer searchAreaId) {
		this.searchAreaId = searchAreaId;
	}


	public boolean isIncludeDocument() {
		return this.includeDocument;
	}


	public void setIncludeDocument(boolean includeDocument) {
		this.includeDocument = includeDocument;
	}


	@Override
	public int getStart() {
		return this.start;
	}


	public void setStart(int start) {
		this.start = start;
	}


	@Override
	public int getLimit() {
		return this.limit;
	}


	public void setLimit(int limit) {
		this.limit = limit;
	}


	@Override
	public Integer getMaxLimit() {
		return this.maxLimit != null ? this.maxLimit : PagingCommand.MAX_LIMIT;
	}


	public void setMaxLimit(Integer maxLimit) {
		this.maxLimit = maxLimit;
	}


	public Integer getSearchSize() {
		return this.searchSize;
	}


	public void setSearchSize(Integer searchSize) {
		this.searchSize = searchSize;
	}


	public String getApplicationName() {
		return this.applicationName;
	}


	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
}
