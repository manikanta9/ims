package com.clifton.system.search;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.search.provider.index.SystemSearchIndexProvider;
import com.clifton.system.search.provider.search.SystemSearchProvider;
import com.clifton.system.search.provider.search.SystemSearchResult;
import com.clifton.system.search.setup.SystemSearchArea;
import com.clifton.system.search.setup.SystemSearchSetupService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SystemSearchServiceImpl implements SystemSearchService {

	private static final int MAX_SEARCH_SIZE = 60;

	private SystemSearchProvider systemSearchProvider;
	private SystemSearchIndexProvider systemSearchIndexProvider;
	private SystemSearchSetupService systemSearchSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemSearchResult> getSystemSearchList(SystemSearchSearchForm searchForm) {
		if (StringUtils.isEmpty(searchForm.getSearchPattern())) {
			return new PagingArrayList<>();
		}

		if (searchForm.getSearchSize() == null || searchForm.getSearchSize() > MAX_SEARCH_SIZE) {
			searchForm.setSearchSize(MAX_SEARCH_SIZE);
		}

		return getSystemSearchProvider().getSearchResultList(searchForm);
	}


	@Override
	public void reindexSystemSearchArea(int searchAreaId) {
		SystemSearchArea searchArea = getSystemSearchSetupService().getSystemSearchArea(searchAreaId);
		ValidationUtils.assertNotNull(searchArea, "Cannot find SystemSearchArea for id = " + searchAreaId);
		getSystemSearchIndexProvider().reindexSystemSearchArea(searchArea);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSearchProvider getSystemSearchProvider() {
		return this.systemSearchProvider;
	}


	public void setSystemSearchProvider(SystemSearchProvider systemSearchProvider) {
		this.systemSearchProvider = systemSearchProvider;
	}


	public SystemSearchSetupService getSystemSearchSetupService() {
		return this.systemSearchSetupService;
	}


	public void setSystemSearchSetupService(SystemSearchSetupService systemSearchSetupService) {
		this.systemSearchSetupService = systemSearchSetupService;
	}


	public SystemSearchIndexProvider getSystemSearchIndexProvider() {
		return this.systemSearchIndexProvider;
	}


	public void setSystemSearchIndexProvider(SystemSearchIndexProvider systemSearchIndexProvider) {
		this.systemSearchIndexProvider = systemSearchIndexProvider;
	}
}
