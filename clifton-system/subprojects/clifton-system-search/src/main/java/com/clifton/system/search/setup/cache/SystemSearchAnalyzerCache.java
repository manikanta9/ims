package com.clifton.system.search.setup.cache;

import com.clifton.system.search.setup.SystemSearchAnalyzer;

import java.util.Optional;


/**
 * @author manderson
 */
public interface SystemSearchAnalyzerCache {

	public Optional<SystemSearchAnalyzer> getSystemSearchAnalyzer();


	public void setSystemSearchAnalyzer(SystemSearchAnalyzer searchTimeAnalyzer);
}
