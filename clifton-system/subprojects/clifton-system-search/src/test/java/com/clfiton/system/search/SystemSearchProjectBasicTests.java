package com.clfiton.system.search;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemSearchProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "system-search";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.net.InetAddress");
		imports.add("org.elasticsearch.");
		imports.add("org.springframework.beans.NullValueInNestedPathException");
	}
}
