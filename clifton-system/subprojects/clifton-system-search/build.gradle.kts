dependencies {

	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// used by Elasticsearch
	runtimeOnly("org.apache.lucene:lucene-analyzers-common:6.6.1")
	runtimeOnly("org.apache.lucene:lucene-join:6.6.1")
	runtimeOnly("org.apache.lucene:lucene-queryparser:6.6.1")
	runtimeOnly("org.apache.lucene:lucene-highlighter:6.6.1")
	runtimeOnly("org.apache.lucene:lucene-suggest:6.6.1")
	runtimeOnly("org.apache.lucene:lucene-queries:6.6.1")

	implementation("org.elasticsearch:elasticsearch:5.6.2") {
		isTransitive = false
	}
	implementation("org.elasticsearch.client:transport:5.6.2")

	////////////////////////////////////////////////////////////////////////////
	////////            Internal Dependencies                           ////////
	////////////////////////////////////////////////////////////////////////////

	api(project(":clifton-system"))

	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-system")))
}
