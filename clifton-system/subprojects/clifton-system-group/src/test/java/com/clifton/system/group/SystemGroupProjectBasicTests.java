package com.clifton.system.group;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;


/**
 * @author Abhinaya
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemGroupProjectBasicTests extends BasicProjectTests {


	@Override
	public String getProjectPrefix() {
		return "system-group";
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}

	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("validator");
	}
}
