package com.clifton.system.group;

import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author AbhinayaM
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class SystemGroupServiceImplTests {

	@Resource
	private SystemGroupService systemGroupService;


	@Resource
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	@Resource
	private SystemConditionService systemConditionService;


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}


	/**
	 * Test case to prove successful creating a new system Group manually
	 */
	@Test
	public void testSaveSystemGroup_Success() {
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
		saveSystemGroup((short) 1, "Test Group Names", "TestDescription", false, "Always True Condition");
	}
	//TODO: Will comment all the system defined tests for now and add it under SYSTEM-303 STORY


	/**
	 * Test case to prove that we cannot LINK GroupItem for System defined Group
	 */
	@Test
	public void testLinkSystemGroupItem_WithSystemDefined() {
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
		SystemGroup systemGroup = saveSystemGroup((short) 1, "Test Group Names", "TestDescription", false, "Always True Condition");
		this.systemGroupService.linkSystemGroupItem(systemGroup.getId(), 14);
	}


	/**
	 * Test case to prove that we cannot UNLINK GroupItem for System defined Group
	 */
	@Test
	public void testUnLinkSystemGroupItem_WithSystemDefined() {
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
		SystemGroup systemGroup = saveSystemGroup((short) 1, "Test Group Names", "TestDescription", false, "Always True Condition");
		this.systemGroupService.unlinkSystemGroupItem(systemGroup.getId(), 14);
	}


	/**
	 * Test case to prove that  it fails when we try to update system defined Group Name
	 */
	@Test
	public void testSaveSystemGroupName_WithSystemDefined_UpdateFailure() {
		TestUtils.expectException(ValidationException.class, () -> {
			SystemGroup systemGroup = saveSystemGroup((short) 1, "Test Group Names", "TestDescription", true, "Always True Condition");
			systemGroup.setName("This should not save");
			this.systemGroupService.saveSystemGroup(systemGroup);
		}, "System Defined Group Names cannot be changed.");
	}


	/**
	 * Test case to prove that  it fails when we try to delete system defined System Group
	 */
	@Test
	public void testDeleteSystemGroup_WithSystemDefined_Failure() {
		TestUtils.expectException(ValidationException.class, () -> {
			SystemGroup systemGroup = saveSystemGroup((short) 1, "Test Group Names", "TestDescription", true, "Always True Condition");
			this.systemGroupService.deleteSystemGroup(systemGroup.getId());
		}, "Deleting System Defined  Groups is not allowed.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemGroup saveSystemGroup(Short defId, String groupName, String description, boolean systemDefined, String modifyCondition) {

		SystemGroup systemGroup = new SystemGroup();
		systemGroup.setSystemGroupDefinition(this.systemGroupService.getSystemGroupDefinition(defId));
		systemGroup.setName(groupName);
		systemGroup.setDescription(description);
		systemGroup.setSystemDefined(systemDefined);
		systemGroup.setEntityModifyCondition(this.systemConditionService.getSystemConditionByName(modifyCondition));

		return this.systemGroupService.saveSystemGroup(systemGroup);
	}
}
