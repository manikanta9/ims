package com.clifton.system.group;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import com.clifton.system.group.search.SystemGroupDefinitionSearchForm;
import com.clifton.system.group.search.SystemGroupSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author AbhinayaM
 */

@Service
public class SystemGroupServiceImpl implements SystemGroupService {

	private AdvancedUpdatableDAO<SystemGroupDefinition, Criteria> systemGroupDefinitionDAO;
	private AdvancedUpdatableDAO<SystemGroup, Criteria> systemGroupDAO;
	private AdvancedUpdatableDAO<SystemGroupItem, Criteria> systemGroupItemDAO;
	private DaoNamedEntityCache<SystemGroup> systemGroupCache;

	private SystemBeanService systemBeanService;


	@Override
	public SystemGroupDefinition getSystemGroupDefinition(short id) {
		return getSystemGroupDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemGroupDefinition> getSystemGroupDefinitionList(SystemGroupDefinitionSearchForm searchForm) {
		return getSystemGroupDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemGroupDefinition saveSystemGroupDefinition(SystemGroupDefinition bean) {

		return getSystemGroupDefinitionDAO().save(bean);
	}


	@Override
	public void deleteSystemGroupDefinition(short id) {
		getSystemGroupDefinitionDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                                                            ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemGroup getSystemGroup(int id) {

		return getSystemGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemGroup getSystemGroupByName(String name) {
		return getSystemGroupCache().getBeanForKeyValue(getSystemGroupDAO(), name);
	}


	@Override
	public List<SystemGroup> getSystemGroupList(SystemGroupSearchForm searchForm) {
		return getSystemGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemGroup saveSystemGroup(SystemGroup bean) {

		return getSystemGroupDAO().save(bean);
	}


	@Transactional
	@Override
	public void deleteSystemGroup(int id) {
		List<SystemGroupItem> itemList = getSystemGroupItemListForGroup(id);
		getSystemGroupItemDAO().deleteList(itemList);
		getSystemGroupDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////          System Group Item Business Methods                ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemGroupItem> getSystemGroupItemListForGroup(int groupId) {
		return getSystemGroupItemDAO().findByField("systemGroup.id", groupId);
	}


	@Override
	public SystemGroupItem linkSystemGroupItem(int groupId, long fkFieldId) {
		SystemGroup systemGroup = getSystemGroup(groupId);
		ValidationUtils.assertFalse(systemGroup.isSystemManaged(), "System Group [" + systemGroup.getName() + "] is system managed.  You cannot manually add items.");
		SystemGroupItem groupItem = new SystemGroupItem();
		groupItem.setSystemGroup(systemGroup);
		groupItem.setFkFieldId(fkFieldId);
		return getSystemGroupItemDAO().save(groupItem);
	}


	@Override
	public void saveSystemGroupItemList(SystemGroup systemGroup, List<SystemGroupItem> newList) {
		List<SystemGroupItem> existingList = getSystemGroupItemListForGroup(systemGroup.getId());

		for (SystemGroupItem item : CollectionUtils.getIterable(newList)) {
			item.setSystemGroup(systemGroup);
			if (!CollectionUtils.isEmpty(existingList)) {
				for (SystemGroupItem existing : existingList) {
					if (MathUtils.isEqual((item.getFkFieldId()), (existing.getFkFieldId()))) {
						// Set ID on the the new list, so performs an update
						item.setId(existing.getId());
						item.setRv(existing.getRv());
					}
				}
			}
		}
		getSystemGroupItemDAO().saveList(newList, existingList);
	}


	@Override
	public Status rebuildSystemGroup(int groupId) {
		Status status = Status.ofMessage("Rebuilding System Group with an ID of " + groupId);
		rebuildSystemGroupImpl(getSystemGroup(groupId), status);
		return status;
	}


	@Override
	public void unlinkSystemGroupItem(int groupId, long fkFieldId) {
		SystemGroup systemGroup = getSystemGroup(groupId);
		ValidationUtils.assertFalse(systemGroup.isSystemManaged(), "System Group [" + systemGroup.getName() + "] is system managed.  You cannot manually remove items.");
		SystemGroupItem groupItem = getSystemGroupItemDAO().findOneByFields(new String[]{"systemGroup.id", "fkFieldId"}, new Object[]{groupId, fkFieldId});
		if (groupItem != null) {
			getSystemGroupItemDAO().delete(groupItem);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Status rebuildSystemGroupImpl(SystemGroup group, Status status) {
		ValidationUtils.assertNotNull(group, "System Group is required in order to rebuild.");
		ValidationUtils.assertTrue(group.isSystemManaged(), "System Group [" + group.getId() + "] is not system managed.  This group is manually maintained and cannot be automatically rebuilt.");
		EntityGroupRebuildAwareExecutor rebuildBean = (EntityGroupRebuildAwareExecutor) getSystemBeanService().getBeanInstance(group.getRebuildSystemBean());
		return rebuildBean.executeRebuild(group, status);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getters and setters                             ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemGroupDefinition, Criteria> getSystemGroupDefinitionDAO() {
		return this.systemGroupDefinitionDAO;
	}


	public void setSystemGroupDefinitionDAO(AdvancedUpdatableDAO<SystemGroupDefinition, Criteria> systemGroupDefinitionDAO) {
		this.systemGroupDefinitionDAO = systemGroupDefinitionDAO;
	}


	public AdvancedUpdatableDAO<SystemGroup, Criteria> getSystemGroupDAO() {
		return this.systemGroupDAO;
	}


	public void setSystemGroupDAO(AdvancedUpdatableDAO<SystemGroup, Criteria> systemGroupDAO) {
		this.systemGroupDAO = systemGroupDAO;
	}


	public DaoNamedEntityCache<SystemGroup> getSystemGroupCache() {
		return this.systemGroupCache;
	}


	public void setSystemGroupCache(DaoNamedEntityCache<SystemGroup> systemGroupCache) {
		this.systemGroupCache = systemGroupCache;
	}


	public AdvancedUpdatableDAO<SystemGroupItem, Criteria> getSystemGroupItemDAO() {
		return this.systemGroupItemDAO;
	}


	public void setSystemGroupItemDAO(AdvancedUpdatableDAO<SystemGroupItem, Criteria> systemGroupItemDAO) {
		this.systemGroupItemDAO = systemGroupItemDAO;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
