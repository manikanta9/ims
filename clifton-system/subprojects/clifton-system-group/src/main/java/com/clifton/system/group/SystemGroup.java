package com.clifton.system.group;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * @author AbhinayaM
 */
@CacheByName
public class SystemGroup extends NamedEntity<Integer> implements SystemEntityModifyConditionAware, EntityGroupRebuildAware {

	public static final String SYSTEM_GROUP_TABLE_NAME = "SystemGroup";

	////////////////////////////////////////////////////////////////////////////

	private SystemGroupDefinition systemGroupDefinition;

	private SystemBean rebuildSystemBean;

	private SystemCondition entityModifyCondition;

	private boolean systemDefined;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {

		return this.entityModifyCondition;
	}


	/**
	 * System Managed means that users cannot add items to the groups themselves
	 * it is maintained by executing the rebuildExecutor.
	 */
	public boolean isSystemManaged() {
		return getRebuildSystemBean() != null;
	}


	@Override
	public String getRebuildTableName() {
		return null;
	}


	@Override
	public SystemBean getRebuildSystemBean() {
		return this.rebuildSystemBean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemGroupDefinition getSystemGroupDefinition() {
		return this.systemGroupDefinition;
	}


	public void setSystemGroupDefinition(SystemGroupDefinition systemGroupDefinition) {
		this.systemGroupDefinition = systemGroupDefinition;
	}


	public void setRebuildSystemBean(SystemBean rebuildSystemBean) {
		this.rebuildSystemBean = rebuildSystemBean;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
}
