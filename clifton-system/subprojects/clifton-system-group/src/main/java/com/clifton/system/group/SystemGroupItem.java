package com.clifton.system.group;

import com.clifton.core.beans.BaseEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * @author AbhinayaM
 */
public class SystemGroupItem extends BaseEntity<Long> implements SystemEntityModifyConditionAware {

	private SystemGroup systemGroup;

	private Long fkFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (!this.systemGroup.isSystemManaged()) {
			return (getSystemGroup() == null) ? null : getSystemGroup().getEntityModifyCondition();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemGroup getSystemGroup() {
		return this.systemGroup;
	}


	public void setSystemGroup(SystemGroup systemGroup) {
		this.systemGroup = systemGroup;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}
}
