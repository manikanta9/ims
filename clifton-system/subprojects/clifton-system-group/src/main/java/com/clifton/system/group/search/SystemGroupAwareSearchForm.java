package com.clifton.system.group.search;

/**
 * @author AbhinayaM
 */
public interface SystemGroupAwareSearchForm {


	/**
	 * Ability for System Group Aware  search forms  to be able to filter by entities IN a specified group
	 */
	public Integer getSystemGroupId();


	/**
	 * Ability for System Group Aware  search forms  to be able to filter by entities NOT IN a specified group
	 */
	public Integer getExcludeSystemGroupId();
}
