package com.clifton.system.group.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.group.SystemGroupDefinition;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


/**
 * @author AbhinayaM
 */
public class SystemGroupDefinitionSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "name", searchFieldPath = "entityModifyCondition")
	private String entityModifyConditionName;

	@SearchField
	private String entityListUrl;

	@SearchField(searchField = "rebuildSystemBeanGroup.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean systemManaged;

	@SearchField
	private String itemsGridUserInterfaceConfig;

	////////////////////////////////////////////////////////////////////////////
	////////            getters and Setters                            ////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getEntityModifyConditionName() {
		return this.entityModifyConditionName;
	}


	public void setEntityModifyConditionName(String entityModifyConditionName) {
		this.entityModifyConditionName = entityModifyConditionName;
	}


	public String getEntityListUrl() {
		return this.entityListUrl;
	}


	public void setEntityListUrl(String entityListUrl) {
		this.entityListUrl = entityListUrl;
	}


	public Boolean getSystemManaged() {
		return this.systemManaged;
	}


	public void setSystemManaged(Boolean systemManaged) {
		this.systemManaged = systemManaged;
	}


	public String getItemsGridUserInterfaceConfig() {
		return this.itemsGridUserInterfaceConfig;
	}


	public void setItemsGridUserInterfaceConfig(String itemsGridUserInterfaceConfig) {
		this.itemsGridUserInterfaceConfig = itemsGridUserInterfaceConfig;
	}
////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return SystemGroupDefinition.SYSTEM_GROUP_DEFINITION_TABLE_NAME;
	}
}
