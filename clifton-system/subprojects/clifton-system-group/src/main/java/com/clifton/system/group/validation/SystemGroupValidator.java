package com.clifton.system.group.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.group.SystemGroup;
import org.springframework.stereotype.Component;


/**
 * @author AbhinayaM
 */
@Component
public class SystemGroupValidator extends SelfRegisteringDaoValidator<SystemGroup> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemGroup bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("Deleting System Defined  Groups is not allowed.");
			}
		}

		if (config.isUpdate()) {

			SystemGroup originalGroup = getOriginalBean(bean);
			if (originalGroup.isSystemDefined() != bean.isSystemDefined()) {
				throw new FieldValidationException("You cannot edit the System Defined field for System Groups");
			}
			if (originalGroup.isSystemDefined()) {
				ValidationUtils.assertTrue(originalGroup.getName().equals(bean.getName()), " System Defined Group Names cannot be changed.");
			}
		}
	}
}
