package com.clifton.system.group.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author AbhinayaM
 */
public class SystemGroupItemSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "systemGroup.id")
	private Integer systemGroupId;

	@SearchField(searchField = "name", searchFieldPath = "systemGroup.systemGroupDefinition.groupItemSystemTable", comparisonConditions = ComparisonConditions.EQUALS)
	private String groupItemTableName;

	@SearchField
	private Long fkFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSystemGroupId() {
		return this.systemGroupId;
	}


	public void setSystemGroupId(Integer systemGroupId) {
		this.systemGroupId = systemGroupId;
	}


	public String getGroupItemTableName() {
		return this.groupItemTableName;
	}


	public void setGroupItemTableName(String groupItemTableName) {
		this.groupItemTableName = groupItemTableName;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}
}
