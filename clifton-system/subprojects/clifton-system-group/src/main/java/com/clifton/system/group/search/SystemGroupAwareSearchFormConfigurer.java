package com.clifton.system.group.search;

import com.clifton.core.dataaccess.search.SearchFormCustomConfigurer;
import com.clifton.system.group.SystemGroupItem;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @author AbhinayaM
 */
@Component
public class SystemGroupAwareSearchFormConfigurer implements SearchFormCustomConfigurer<SystemGroupAwareSearchForm> {

	@Override
	public Class<SystemGroupAwareSearchForm> getSearchFormInterfaceClassName() {
		return SystemGroupAwareSearchForm.class;
	}


	@Override
	public void configureCriteria(Criteria criteria, SystemGroupAwareSearchForm searchForm, Map<String, Criteria> processedPathToCriteria) {

		if (searchForm.getSystemGroupId() != null) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(SystemGroupItem.class, "sg");
			detachedCriteria.setProjection(Projections.property("id"));
			detachedCriteria.add(Restrictions.eqProperty("fkFieldId", criteria.getAlias() + ".id"));
			detachedCriteria.add(Restrictions.eq("systemGroup.id", searchForm.getSystemGroupId()));
			criteria.add(Subqueries.exists(detachedCriteria));
		}
		if (searchForm.getExcludeSystemGroupId() != null) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(SystemGroupItem.class, "esg");
			detachedCriteria.setProjection(Projections.property("id"));
			detachedCriteria.add(Restrictions.eqProperty("fkFieldId", criteria.getAlias() + ".id"));
			detachedCriteria.add(Restrictions.eq("systemGroup.id", searchForm.getExcludeSystemGroupId()));
			criteria.add(Subqueries.notExists(detachedCriteria));
		}
	}
}
