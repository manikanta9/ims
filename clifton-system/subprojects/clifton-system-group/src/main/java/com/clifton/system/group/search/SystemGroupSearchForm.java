package com.clifton.system.group.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.group.SystemGroup;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


/**
 * @author AbhinayaM
 */
public class SystemGroupSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "systemGroupDefinition.id")
	private Short systemGroupDefinitionId;

	@SearchField(searchField = "name", searchFieldPath = "entityModifyCondition")
	private String entityModifyCondition;

	@SearchField(searchField = "name", searchFieldPath = "systemGroupDefinition.groupItemSystemTable", comparisonConditions = ComparisonConditions.EQUALS)
	private String groupItemTableName;

	@SearchField
	private Boolean systemDefined;

	@SearchField(searchField = "rebuildSystemBean.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean systemManaged;

	@SearchField(searchField = "groupItemList.fkFieldId", comparisonConditions = ComparisonConditions.EXISTS)
	private Long groupItemFkFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return SystemGroup.SYSTEM_GROUP_TABLE_NAME;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getSystemGroupDefinitionId() {
		return this.systemGroupDefinitionId;
	}


	public void setSystemGroupDefinitionId(Short systemGroupDefinitionId) {
		this.systemGroupDefinitionId = systemGroupDefinitionId;
	}


	public String getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(String entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getSystemManaged() {
		return this.systemManaged;
	}


	public void setSystemManaged(Boolean systemManaged) {
		this.systemManaged = systemManaged;
	}


	public String getGroupItemTableName() {
		return this.groupItemTableName;
	}


	public void setGroupItemTableName(String groupItemTableName) {
		this.groupItemTableName = groupItemTableName;
	}


	public Long getGroupItemFkFieldId() {
		return this.groupItemFkFieldId;
	}


	public void setGroupItemFkFieldId(Long groupItemFkFieldId) {
		this.groupItemFkFieldId = groupItemFkFieldId;
	}
}
