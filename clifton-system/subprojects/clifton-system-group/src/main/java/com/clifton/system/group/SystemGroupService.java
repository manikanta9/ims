package com.clifton.system.group;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.util.status.Status;
import com.clifton.system.group.search.SystemGroupDefinitionSearchForm;
import com.clifton.system.group.search.SystemGroupSearchForm;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * @author AbhinayaM
 */
public interface SystemGroupService {

	////////////////////////////////////////////////////////////////////////////
	////////            System Group Definition Business Methods         ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemGroupDefinition getSystemGroupDefinition(short id);


	public List<SystemGroupDefinition> getSystemGroupDefinitionList(SystemGroupDefinitionSearchForm searchForm);


	public SystemGroupDefinition saveSystemGroupDefinition(SystemGroupDefinition bean);


	public void deleteSystemGroupDefinition(short id);

	////////////////////////////////////////////////////////////////////////////
	////////            System Group Business Methods                   ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemGroup getSystemGroup(int id);


	public SystemGroup getSystemGroupByName(String name);


	public List<SystemGroup> getSystemGroupList(SystemGroupSearchForm searchForm);


	public SystemGroup saveSystemGroup(SystemGroup bean);


	public void deleteSystemGroup(int id);


	public Status rebuildSystemGroup(int groupId);

	////////////////////////////////////////////////////////////////////////////
	////////          System Group Item Business Methods                ////////
	////////////////////////////////////////////////////////////////////////////


	public List<SystemGroupItem> getSystemGroupItemListForGroup(int groupId);


	/**
	 * Links the specified entity to the specified group.
	 */
	public SystemGroupItem linkSystemGroupItem(int groupId, long fkFieldId);


	@DoNotAddRequestMapping
	public void unlinkSystemGroupItem(int groupId, long fkFieldId);


	@ResponseBody
	public void saveSystemGroupItemList(SystemGroup systemGroup, List<SystemGroupItem> newList);
}
