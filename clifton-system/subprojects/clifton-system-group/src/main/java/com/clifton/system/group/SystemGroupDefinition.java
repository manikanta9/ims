package com.clifton.system.group;

import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBeanGroup;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * @author AbhinayaM
 */
public class SystemGroupDefinition extends NamedEntity<Short> implements SystemEntityModifyConditionAware {

	// ADD CACHE BY NAME??

	public static final String SYSTEM_GROUP_DEFINITION_TABLE_NAME = "SystemGroupDefinition";

	private SystemBeanGroup rebuildSystemBeanGroup;

	private SystemCondition entityModifyCondition;

	private SystemTable groupItemSystemTable;
	/**
	 * Used to define a url to retrieve values for a combo box on the UI.
	 */

	private String entityListUrl;
	/**
	 * Used to define potential UI display overrides
	 */
	private String itemsGridUserInterfaceConfig;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setRebuildSystemBeanGroup(SystemBeanGroup rebuildSystemBeanGroup) {
		this.rebuildSystemBeanGroup = rebuildSystemBeanGroup;
	}


	public SystemBeanGroup getRebuildSystemBeanGroup() {
		return this.rebuildSystemBeanGroup;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public SystemTable getGroupItemSystemTable() {
		return this.groupItemSystemTable;
	}


	public void setGroupItemSystemTable(SystemTable groupItemSystemTable) {
		this.groupItemSystemTable = groupItemSystemTable;
	}


	public String getEntityListUrl() {
		return this.entityListUrl;
	}


	public void setEntityListUrl(String entityListUrl) {
		this.entityListUrl = entityListUrl;
	}


	public String getItemsGridUserInterfaceConfig() {
		return this.itemsGridUserInterfaceConfig;
	}


	public void setItemsGridUserInterfaceConfig(String itemsGridUserInterfaceConfig) {
		this.itemsGridUserInterfaceConfig = itemsGridUserInterfaceConfig;
	}
}
