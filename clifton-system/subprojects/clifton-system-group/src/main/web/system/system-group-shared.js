Ext.ns('Clifton.system.group');

Clifton.system.group.SystemGroupListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'systemGroupListFind',
	xtype: 'gridpanel',
	instructions: 'System groups are collections of a specified entity (i.e. accounts, securities) that can be used by various parts of the system.',
	topToolbarSearchParameter: 'searchPattern',
	groupItemTableName: undefined, // Allows limiting groups to those where the group item is for the given table only.
	columns: [
		{header: 'Group Definition', width: 100, dataIndex: 'systemGroupDefinition.name', filter: {searchFieldName: 'systemGroupDefinitionId', type: 'combo', disableAddNewItem: true, url: 'systemGroupDefinitionListFind.json'}},
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Group Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
		{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
		{header: 'Modify Condition', width: 110, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyCondition'}},
		{header: 'System Defined', width: 40, dataIndex: 'systemDefined', type: 'boolean'},
		{header: 'System Managed', width: 40, dataIndex: 'systemManaged', type: 'boolean'}
	],

	getTopToolbarInitialLoadParams: function(firstLoad) {
		const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
		const params = {};
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'System Group Tags';
			params.categoryHierarchyId = tag.getValue();
		}
		if (this.groupItemTableName) {
			params.groupItemTableName = this.groupItemTableName;
			const fkId = this.getGroupItemFkFieldId();
			if (fkId) {
				params.groupItemFkFieldId = fkId;
			}
		}
		return params;
	},

	// Works with the group item table to find groups for a specific entity.  i.e. groupItemTableName = OrderAccount to see all Account Groups.  groupItemFkFieldID = 5 to see all groups a specific account is in
	getGroupItemFkFieldId: function() {
		return undefined;
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=System Group Tags'},
			{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},
	editor: {
		detailPageClass: 'Clifton.system.group.GroupWindow'
	}
});
Ext.reg('system-group-list-grid', Clifton.system.group.SystemGroupListGrid);



