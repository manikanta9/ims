Clifton.system.group.GroupDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Group Definition',
	iconCls: 'grouping',
	width: 900,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Need to update',
					url: 'systemGroupDefinition.json',
					labelWidth: 160,
					items: [
						{fieldLabel: 'Definition Name', name: 'name', readOnly: true},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 35, grow: true},
						{fieldLabel: 'Modify Condition', name: 'entityModifyCondition.label', hiddenName: 'entityModifyCondition.id', displayField: 'name', xtype: 'system-condition-combo'},
						{fieldLabel: 'Group Item Table', name: 'groupItemSystemTable.name', xtype: 'linkfield', detailIdField: 'groupItemSystemTable.id', detailPageClass: 'Clifton.system.schema.TableWindow'},
						{fieldLabel: 'URL', name: 'entityListUrl', xtype: 'textarea', qtip: 'The URL list to retrieve items. Should accept a search form that implements SystemGroupAwareSearchForm'},
						{fieldLabel: 'Items Grid UI Config', name: 'itemsGridUserInterfaceConfig', xtype: 'textarea', height: 35, grow: true, qtip: 'The gridpanel UI config that displays the list of entities.'}
					]
				}]
			},

			{
				title: 'Groups',
				items: [{
					xtype: 'system-group-list-grid',
					columnOverrides: [
						{dataIndex: 'systemGroupDefinition.name', hidden: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						const params = {systemGroupDefinitionId: this.getWindow().getMainFormId()};
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'System Group Tags';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					}
				}]
			}
		]
	}]
});

