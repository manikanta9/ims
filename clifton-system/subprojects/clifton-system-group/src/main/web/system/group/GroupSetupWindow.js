Clifton.system.group.GroupSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'systemGroupSetupWindow',
	title: 'System Group Setup',
	iconCls: 'grouping',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [

			{
				title: 'Groups',
				items: [{
					xtype: 'system-group-list-grid'
				}]
			},
			{
				title: 'Group Definition',
				items: [{
					name: 'systemGroupDefinitionListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Definition Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Group Description', width: 250, dataIndex: 'description'},
						{header: 'Modify Condition', width: 110, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyCondition'}},
						{header: 'Entity List URL', width: 100, dataIndex: 'entityListUrl', hidden: true},
						{header: 'Items Grid UI Config', width: 50, dataIndex: 'itemsGridUserInterfaceConfig', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.group.GroupDefinitionWindow',
						drillDownOnly: true
					}
				}]
			}

		]
	}]
});
