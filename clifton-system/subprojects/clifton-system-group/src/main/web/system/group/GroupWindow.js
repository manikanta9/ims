Clifton.system.group.GroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Group',
	iconCls: 'grouping',
	width: 850,
	height: 500,
	enableRefreshWindow: true,

	items: [
		{
			xtype: 'tabpanel',
			requiredFormIndex: 0,
			items: [
				{
					title: 'Info',
					items: [{
						xtype: 'formpanel',
						labelWidth: 150,
						instructions: 'System groups are collections of a specified entity (i.e. accounts, securities) that can be used by various parts of the system. System Defined Groups cannot be deleted, and their names cannot be edited.',
						url: 'systemGroup.json',

						listeners: {
							afterload: function(formPanel, isClosing) {
								if (TCG.isNumber(this.getWindow().getMainFormId())) {
									this.setReadOnlyField('systemGroupDefinition.name', true);
								}
								if (!TCG.isTrue(isClosing)) {
									this.resetGroupItemTab(formPanel, formPanel.getFormValue('systemGroupDefinition'));
								}
								formPanel.resetBeanCombo(formPanel);
							}
						},
						resetBeanCombo: function(fp, beanGroupName) {
							const combo = fp.getForm().findField('rebuildSystemBean.name');
							if (TCG.isBlank(beanGroupName)) {
								beanGroupName = this.getFormValue('systemGroupDefinition.rebuildSystemBeanGroup.name');
							}
							combo.url = 'systemBeanListFind.json?groupName=' + beanGroupName;
							combo.store.proxy.setUrl(combo.url, true);
						},

						items: [
							{xtype: 'hidden', name: 'systemGroupDefinition.rebuildSystemBeanGroup.name', submitValue: false},
							{fieldLabel: 'Group Definition', name: 'systemGroupDefinition.name', hiddenName: 'systemGroupDefinition.id', xtype: 'combo', url: 'systemGroupDefinitionListFind.json', detailPageClass: 'Clifton.system.group.GroupDefinitionWindow'},
							{fieldLabel: 'System Group Name', name: 'name'},
							{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
							{xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: false},
							{
								fieldLabel: 'Rebuild Bean', name: 'rebuildSystemBean.name', hiddenName: 'rebuildSystemBean.id', xtype: 'combo', requiredFields: ['systemGroupDefinition.name'], detailPageClass: 'Clifton.system.bean.BeanWindow', url: 'systemBeanListFind.json',

								getDefaultData: function() {
									return {type: {group: {name: TCG.getParentFormPanel(this).getFormValue('systemGroupDefinition.rebuildSystemBeanGroup.name')}}};
								}

							},
							{fieldLabel: '', boxLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true},
							{
								xtype: 'system-tags-fieldset',
								title: 'Tags',
								tableName: 'SystemGroup',
								hierarchyCategoryName: 'System Group Tags'
							}
						],


						resetGroupItemTab: function(formpanel, groupDefinition) {
							const tabs = formpanel.getWindow().items.get(0);
							// Remove Group Items Tab
							const itemsTab = formpanel.getWindow().getTabPosition('Group Items');
							if (itemsTab !== 0) {
								tabs.remove(itemsTab);
							}
							if (TCG.isTrue(formpanel.getFormValue('systemManaged'))) {

								const itemsGridPanel = this.systemManagedGroupItemsGridPanel;
								itemsGridPanel.groupDefinition = groupDefinition;
								Ext.apply(itemsGridPanel, Ext.decode(groupDefinition.itemsGridUserInterfaceConfig));
								const groupItemsTab = {
									title: 'Group Items',
									items: []
								};
								groupItemsTab.items.push(itemsGridPanel);
								tabs.add(groupItemsTab);
							}
							else {
								const itemsGridPanel = this.groupItemsGridPanel;
								itemsGridPanel.groupDefinition = groupDefinition;
								Ext.apply(itemsGridPanel, Ext.decode(groupDefinition.itemsGridUserInterfaceConfig));
								const groupItemsTab = {
									title: 'Group Items',
									items: []
								};
								groupItemsTab.items.push(itemsGridPanel);
								tabs.add(groupItemsTab);
							}
						},


						groupItemsGridPanel: {
							xtype: 'gridpanel',
							instructions: 'The following items are associated with this group. To add another item to this group, select desired record from the list and click the [Add] button.',
							getTopToolbarInitialLoadParams: function() {
								return {systemGroupId: this.getWindow().getMainFormId()};
							},
							editor: {
								deleteURL: 'systemGroupItemUnlink.json',
								getDetailPageClass: function(grid, row) {
									return this.getGridPanel().groupDefinition.groupItemSystemTable.detailScreenClass;
								},
								getDeleteParams: function(selectionModel) {
									return {
										groupId: this.getWindow().getMainFormId(),
										fkFieldId: selectionModel.getSelected().id
									};
								},
								addToolbarAddButton: function(toolBar) {
									const gridPanel = this.getGridPanel();
									const grpId = gridPanel.getWindow().getMainFormId();

									const url = gridPanel.groupDefinition.entityListUrl + '?excludeSystemGroupId=' + grpId;
									toolBar.add(new TCG.form.ComboBox({name: 'fkItem', displayField: 'label', url: url, width: 150, listWidth: 230}));
									toolBar.add({
										text: 'Add',
										tooltip: 'Add the selected item to the group',
										iconCls: 'add',
										handler: function() {
											const fkId = TCG.getChildByName(toolBar, 'fkItem').getValue();
											if (fkId === '') {
												TCG.showError('You must first select an item from the list.');
											}
											else {
												const groupId = gridPanel.getWindow().getMainFormId();
												const loader = new TCG.data.JsonLoader({
													waitTarget: gridPanel,
													waitMsg: 'Linking...',
													params: {groupId: groupId, fkFieldId: fkId},
													onLoad: function(record, conf) {
														gridPanel.reload();
														TCG.getChildByName(toolBar, 'fkItem').reset();
													}
												});
												loader.load('systemGroupItemLink.json');
											}
										}
									});
									toolBar.add('-');
								}
							}
						},
						systemManagedGroupItemsGridPanel: {
							xtype: 'gridpanel',
							instructions: 'The following items are associated with this group. Click rebuild to force rebuilding of the group items.',

							getTopToolbarInitialLoadParams: function() {
								return {systemGroupId: this.getWindow().getMainFormId()};
							},
							editor: {
								deleteEnabled: false,
								getDetailPageClass: function(grid, row) {
									return this.getGridPanel().groupDefinition.groupItemSystemTable.detailScreenClass;
								},

								addToolbarAddButton: function(toolBar) {
									const gridPanel = this.getGridPanel();
									const grpId = gridPanel.getWindow().getMainFormId();
									toolBar.add({
										text: 'Rebuild',
										tooltip: 'Rebuild System Group',
										iconCls: 'run',
										handler: function() {
											TCG.data.getDataPromise('systemGroupRebuild.json', gridPanel, {
												waitMsg: 'Rebuilding...',
												params: {groupId: grpId}
											}).then(result => {
												if (result && (gridPanel.rebuildAlwaysShowStatus || (result.errorList && results.errorList.length > 0))) {
													TCG.createComponent('Clifton.core.StatusWindow', {
														title: 'System Group Rebuild Status',
														defaultData: {status: result}
													});
												}
												gridPanel.reload();
											});
										}

									});
									toolBar.add('-');
								}
							}
						}
					}]
				}
			]
		}
	]
});
