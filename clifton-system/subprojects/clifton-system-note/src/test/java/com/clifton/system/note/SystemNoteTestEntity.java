package com.clifton.system.note;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.hierarchy.definition.SystemHierarchy;


/**
 * The <code>SystemNoteTestEntity</code> is a test object to test notes
 *
 * @author manderson
 */
public class SystemNoteTestEntity extends NamedEntity<Integer> {

	// For Testing Linked Notes
	private SystemHierarchy hierarchy;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemHierarchy getHierarchy() {
		return this.hierarchy;
	}


	public void setHierarchy(SystemHierarchy hierarchy) {
		this.hierarchy = hierarchy;
	}
}
