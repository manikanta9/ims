package com.clifton.system.note;

import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.beans.MethodUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


/**
 * @author manderson
 */
public class SystemNoteSecurityMethodPermissionResolverTests {


	@Test
	public void testGetRequiredPermissions_NoteResource() {
		validatePermissions("TradeNote", SecurityPermission.PERMISSION_DELETE, SecurityPermission.PERMISSION_WRITE, SecurityPermission.PERMISSION_CREATE);
	}


	@Test
	public void testGetRequiredPermissions_TableResource() {
		validatePermissions("Trade", SecurityPermission.PERMISSION_WRITE, SecurityPermission.PERMISSION_WRITE, SecurityPermission.PERMISSION_WRITE);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void validatePermissions(String securityResourceName, int expectedDeletePermission, int expectedUpdatePermission, int expectedCreatePermission) {
		Method deleteMethod = MethodUtils.getMethod(SystemNoteService.class, "deleteSystemNote", int.class);
		Method saveMethod = MethodUtils.getMethod(SystemNoteService.class, "saveSystemNote", SystemNote.class);

		Map<String, Object> config = new HashMap<>();
		config.put("id", 10);

		SystemNoteSecureMethodPermissionResolver permissionResolver = new SystemNoteSecureMethodPermissionResolver();

		String msgFormat = "Expected %s to require %d permission for security resource %s, but instead required %d";
		int deletePermission = permissionResolver.getRequiredPermissions(deleteMethod, config, securityResourceName);
		Assertions.assertEquals(expectedDeletePermission, deletePermission, () -> String.format(msgFormat, "DELETE", expectedDeletePermission, securityResourceName, deletePermission));
		int updatePermission = permissionResolver.getRequiredPermissions(saveMethod, config, securityResourceName);
		Assertions.assertEquals(expectedUpdatePermission, updatePermission, () -> String.format(msgFormat, "UPDATE", expectedUpdatePermission, securityResourceName, updatePermission));
		int createPermission = permissionResolver.getRequiredPermissions(saveMethod, null, securityResourceName);
		Assertions.assertEquals(expectedCreatePermission, createPermission, () -> String.format(msgFormat, "CREATE", expectedCreatePermission, securityResourceName, createPermission));
	}
}
