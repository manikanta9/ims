package com.clifton.system.note.comparisons;


import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteTestEntity;
import com.clifton.system.note.search.SystemNoteSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemNoteExistsComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private SystemConditionService systemConditionService;

	@Resource
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	@Resource
	private SystemNoteService systemNoteService;

	@Resource
	private ReadOnlyDAO<SystemNote> systemNoteDAO;

	@Resource
	private ReadOnlyDAO<SystemNoteTestEntity> systemNoteTestEntityDAO;


	///////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setup() {
		Mockito.when(this.systemNoteService.getSystemNoteListForEntity(ArgumentMatchers.any(SystemNoteSearchForm.class))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			SystemNoteSearchForm searchForm = (SystemNoteSearchForm) args[0];
			if (searchForm == null || searchForm.getFkFieldId() == null) {
				return null;
			}
			return getSystemNoteDAO().findByField("fkFieldId", searchForm.getFkFieldId());
		});
	}


	private SystemCondition getCondition() {
		SystemCondition condition = new SystemCondition();
		condition.setId(1);
		condition.setName("Test Condition");
		return condition;
	}


	///////////////////////////////////////////
	///  Exists Condition Tests
	///////////////////////////////////////////


	private SystemNoteExistsComparison getNoteExistsComparison() {
		SystemNoteExistsComparison comparison = new SystemNoteExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	@Test
	public void testNoteExists() {
		SystemNoteExistsComparison comparison = getNoteExistsComparison();

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(1), context));
		Assertions.assertEquals("(3 System Notes Present)", context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(2), context));
		Assertions.assertEquals("(No System Notes Present)", context.getFalseMessage());
	}


	@Test
	public void testNoteExistsWithNoteConditionTrue() {
		SystemNoteExistsComparison comparison = getNoteExistsComparison();
		comparison.setNoteSystemConditionId(1);

		Mockito.when(this.systemConditionService.getSystemCondition(ArgumentMatchers.eq(1))).thenReturn(getCondition());
		Mockito.when(this.systemConditionEvaluationHandler.isConditionTrue(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(SystemNote.class))).thenReturn(true);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(1), context));
		Assertions.assertEquals("(3 System Notes Present that passed condition [Test Condition])", context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(2), context));
		Assertions.assertEquals("(No System Notes Present that passed condition [Test Condition])", context.getFalseMessage());
	}


	@Test
	public void testNoteExistsWithNoteConditionFalse() {
		SystemNoteExistsComparison comparison = getNoteExistsComparison();
		comparison.setNoteSystemConditionId(1);

		Mockito.when(this.systemConditionService.getSystemCondition(ArgumentMatchers.eq(1))).thenReturn(getCondition());
		Mockito.when(this.systemConditionEvaluationHandler.isConditionTrue(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(SystemNote.class))).thenReturn(false);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(2), context));
		Assertions.assertEquals("(No System Notes Present that passed condition [Test Condition])", context.getFalseMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(1), context));
		Assertions.assertEquals("(No System Notes Present that passed condition [Test Condition])", context.getFalseMessage());
	}


	///////////////////////////////////////////
	///  Not Exists Condition Tests
	///////////////////////////////////////////


	private SystemNoteNotExistsComparison getNoteNotExistsComparison() {
		SystemNoteNotExistsComparison comparison = new SystemNoteNotExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	@Test
	public void testNoteNotExists() {
		SystemNoteNotExistsComparison comparison = getNoteNotExistsComparison();
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(2), context));
		Assertions.assertEquals("(No System Notes Present)", context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(1), context));
		Assertions.assertEquals("(3 System Notes Present)", context.getFalseMessage());
	}


	@Test
	public void testNoteNotExistsWithNoteConditionTrue() {
		SystemNoteNotExistsComparison comparison = getNoteNotExistsComparison();
		comparison.setNoteSystemConditionId(1);

		Mockito.when(this.systemConditionService.getSystemCondition(ArgumentMatchers.eq(1))).thenReturn(getCondition());
		Mockito.when(this.systemConditionEvaluationHandler.isConditionTrue(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(SystemNote.class))).thenReturn(true);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(2), context));
		Assertions.assertEquals("(No System Notes Present that passed condition [Test Condition])", context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(1), context));
		Assertions.assertEquals("(3 System Notes Present that passed condition [Test Condition])", context.getFalseMessage());
	}


	@Test
	public void testNoteNotExistsWithNoteConditionFalse() {
		SystemNoteExistsComparison comparison = getNoteNotExistsComparison();
		comparison.setNoteSystemConditionId(1);

		Mockito.when(this.systemConditionService.getSystemCondition(ArgumentMatchers.eq(1))).thenReturn(getCondition());
		Mockito.when(this.systemConditionEvaluationHandler.isConditionTrue(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(SystemNote.class))).thenReturn(false);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(1), context));
		Assertions.assertEquals("(No System Notes Present that passed condition [Test Condition])", context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(this.systemNoteTestEntityDAO.findByPrimaryKey(2), context));
		Assertions.assertEquals("(No System Notes Present that passed condition [Test Condition])", context.getTrueMessage());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ReadOnlyDAO<SystemNote> getSystemNoteDAO() {
		return this.systemNoteDAO;
	}
}
