package com.clifton.system.note;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.document.DocumentAttachmentSupportedTypes;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.matcher.PropertyMatcher;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class SystemNoteServiceImplTests {

	private static final short NOTE_TYPE_WITH_DATES_AND_ORDER = 1;
	private static final short NOTE_TYPE_WITH_DATES_ONLY = 2;
	private static final short NOTE_TYPE_WITH_ORDER_ONLY = 3;
	private static final short NOTE_TYPE_WITH_SYSTEM_MANAGED_LINKS = 4;
	private static final short NOTE_TYPE_WITH_LINKED_RELATIONSHIP = 5;
	private static final short NOTE_TYPE_WITH_LINKED_TO_MULTIPLE = 6;

	private static final short NOTE_TYPE_WITH_NO_ATTACHMENT_SUPPORT = 7;
	private static final short NOTE_TYPE_WITH_ONE_ATTACHMENT_SUPPORT = 8;
	private static final short NOTE_TYPE_WITH_MULTIPLE_ATTACHMENT_SUPPORT = 9;

	// Note: Whether or not the condition passes is determined via mocking for each test
	private static final short NOTE_TYPE_WITH_ENTITY_MODIFY_CONDITION = 10;
	private static final short NOTE_TYPE_WITH_ENTITY_DELETE_CONDITION = 11;
	private static final short NOTE_TYPE_WITH_ENTITY_MODIFY_AND_DELETE_CONDITION = 12;

	private static final short NOTE_TYPE_WITH_ACTION_BEAN = 13;

	private static final int MODIFY_CONDITION_ID = 1;
	private static final int DELETE_CONDITION_ID = 2;


	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	@Resource
	private SystemNoteService systemNoteService;

	@Resource
	private XmlReadOnlyDAO<SystemNote> systemNoteDAO;

	@Resource
	private XmlReadOnlyDAO<SystemNoteTestEntity> systemNoteTestEntityDAO;


	private SystemNoteTestEntityActionHandler systemNoteTestEntityActionHandler = Mockito.mock(SystemNoteTestEntityActionHandler.class);


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);
	}


	@Test
	public void testGlobalNoteNotAllowed() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ORDER_ONLY);

			SystemNote note = new SystemNote();
			note.setText("Test Note");
			note.setNoteType(type);
			this.systemNoteService.saveSystemNote(note);
		});
		Assertions.assertEquals("Global notes are not allowed for type [Type 3 - with Order Only].", ve.getMessage());
	}


	@Test
	public void testNoteWithStartAndEndDateForDateRangeNotAllowed() {
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ORDER_ONLY);

		SystemNote note = createNote(type);
		note.setStartDate(DateUtils.toDate("01/15/2013"));
		note.setEndDate(DateUtils.toDate("01/20/2013"));
		this.systemNoteService.saveSystemNote(note);

		AssertUtils.assertNull(note.getStartDate(), "Start Date should have been cleared.");
		AssertUtils.assertNull(note.getEndDate(), "Start Date should have been cleared.");
	}


	@Test
	public void testNoteOrderWithOrderNotAllowed() {
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_DATES_ONLY);

		SystemNote note = createNote(type);
		note.setOrder(10);
		this.systemNoteService.saveSystemNote(note);

		AssertUtils.assertNull(note.getOrder(), "Order should have been cleared.");
	}


	@Test
	public void testChangeNoteTypeToNoDatesWithNotesWithDatesEntered() {
		// Because we do Start Date is NULL or End Date is NULL - need that to return a result
		this.systemNoteDAO.setLogicalEvaluatesToTrue(true);
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_DATES_ONLY);

			SystemNote note = createNote(type);
			note.setStartDate(DateUtils.toDate("01/15/2013"));
			note.setEndDate(DateUtils.toDate("01/20/2013"));
			this.systemNoteService.saveSystemNote(note);

			type.setDateRangeAllowed(false);
			this.systemNoteService.saveSystemNoteType(type);
		});
		Assertions.assertEquals("Cannot change this note type to prohibit date ranges because there are currently [1] note(s) with start and/or end dates entered.", ve.getMessage());
	}


	@Test
	public void testChangeNoteTypeToNoDatesWithNoNotesWithDatesEntered() {
		this.systemNoteDAO.setLogicalEvaluatesToTrue(false);
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_DATES_ONLY);

		SystemNote note = createNote(type);
		this.systemNoteService.saveSystemNote(note);

		type.setDateRangeAllowed(false);
		this.systemNoteService.saveSystemNoteType(type);
	}


	@Test
	public void testChangeNoteTypeToInternal() {
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_DATES_AND_ORDER);

		SystemNote note = createNote(type);
		note.setStartDate(DateUtils.toDate("01/15/2013"));
		note.setEndDate(DateUtils.toDate("01/20/2013"));
		this.systemNoteService.saveSystemNote(note);

		note = this.systemNoteService.getSystemNote(note.getId());
		AssertUtils.assertFalse(note.isPrivateNote(), "Note should not be flagged as Private");

		SystemNote note2 = createNote(type);
		this.systemNoteService.saveSystemNote(note2);

		note2 = this.systemNoteService.getSystemNote(note2.getId());
		AssertUtils.assertFalse(note.isPrivateNote(), "Note should not be flagged as Private");

		type.setInternal(true);
		this.systemNoteService.saveSystemNoteType(type);

		note = this.systemNoteService.getSystemNote(note.getId());
		AssertUtils.assertTrue(note.isPrivateNote(), "Internal Type change should have made note Private");

		note2 = this.systemNoteService.getSystemNote(note2.getId());
		AssertUtils.assertTrue(note2.isPrivateNote(), "Internal Type change should have made note Private");

		// Add a new note - should automatically be marked as private
		SystemNote note3 = createNote(type);
		// Not flagged as private before save
		AssertUtils.assertFalse(note3.isPrivateNote(), "Note should not be flagged as Private");
		this.systemNoteService.saveSystemNote(note3);
		// Observer should set the private flag
		AssertUtils.assertTrue(note3.isPrivateNote(), "Internal Note Type - should be flagged as Private");
	}


	@Test
	public void testNoteWithSystemManagedLink() {
		this.systemNoteDAO.setLogicalEvaluatesToTrue(false);
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_SYSTEM_MANAGED_LINKS);

		SystemNote note = createNote(type);
		// Make sure FK FieldID is set and Linked FK Field ID is not set before save
		AssertUtils.assertNotNull(note.getFkFieldId(), "FK Field ID should be set before saving note");
		AssertUtils.assertNull(note.getLinkedFkFieldId(), "Linked FK Field ID should be null before saving note");

		SystemNoteTestEntity testEntity = this.systemNoteTestEntityDAO.findByPrimaryKey(note.getFkFieldId());

		this.systemNoteService.saveSystemNote(note);

		// Verify that the FK Field ID is still set to the correct ID
		// And Now Linked FK Field ID is set to the Hierarchy for that entity
		Assertions.assertEquals(BeanUtils.getIdentityAsLong(testEntity), note.getFkFieldId(), "Expected System Managed Linked Note Types to keep the FK Field ID value set");
		Assertions.assertEquals(BeanUtils.getIdentityAsLong(testEntity.getHierarchy()), note.getLinkedFkFieldId(),
				"Expected System Managed Linked Note Types to set Hierarchy ID as the Linked FK Field ID on the Note");
	}


	@Test
	public void testNoteWithSystemManagedLinksAndLinkedToMultiple() {
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_SYSTEM_MANAGED_LINKS);
		type.setLinkToMultipleAllowed(true);
		this.systemNoteService.saveSystemNoteType(type);
	}


	@Test
	public void testNoteWithLinkedRelationshipAndLinkedToMultiple() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_LINKED_RELATIONSHIP);
			type.setLinkToMultipleAllowed(true);
			this.systemNoteService.saveSystemNoteType(type);
		});
		Assertions.assertEquals("Linked to Multiple option is not an available option if the note type supports linked relationship notes that aren't system managed.", ve.getMessage());
	}


	@Test
	public void testNoteWithLinkedRelationship() {
		this.systemNoteDAO.setLogicalEvaluatesToTrue(false);
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_LINKED_RELATIONSHIP);

		SystemNote note = createNote(type);
		note.setFkFieldId(null);
		note.setLinkedFkFieldId((long) 2);

		// NOTE: IF THIS FAILS WITH VALIDATION EXCEPTION THAT GLOBAL NOTES ARE NOT
		// ALLOWED THAT MEANS THAT THE LINKED RELATIONSHIP IS BEING CLEARED!
		this.systemNoteService.saveSystemNote(note);

		// Verify that the FK Field ID is still null
		// And  Linked FK Field ID is still the Hierarchy ID 2
		AssertUtils.assertNull(note.getFkFieldId(), "Expected Linked Relationship Note Type to have null FK Field ID");
		Assertions.assertEquals(2, note.getLinkedFkFieldId().intValue(), "Expected Linked Relationship Note to keep Hierarchy ID #2 as the Linked FK Field ID on the Note");
	}


	@Test
	public void testNoteWithLinkedToMultiple() {
		this.systemNoteDAO.setLogicalEvaluatesToTrue(false);
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_LINKED_TO_MULTIPLE);

		SystemNote note = createNote(type);
		note.setFkFieldId(null);
		note.setLinkedToMultiple(true);
		Long[] fkFieldIds = new Long[]{(long) 1, (long) 2, (long) 3};

		this.systemNoteService.saveSystemNoteWithLinks(note, fkFieldIds);

		// Verify that the FK Field ID is still null and linked to multiple
		AssertUtils.assertNull(note.getFkFieldId(), "Expected Linked to Multiple to have null FK Field ID");
		Assertions.assertTrue(note.isLinkedToMultiple(), "Expected Linked to Multiple flag to be set");

		Assertions.assertEquals(3, CollectionUtils.getSize(this.systemNoteService.getSystemNoteLinkedEntityListByNote(note.getId())), "Expected Note to Be Created With 3 links");
	}


	@Test
	public void testSwitchNoteTypeAttachmentSupport_NoNotesExist() {
		// All switches should be allowed if there are no notes associated with them
		// From NONE to SINGLE
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_NO_ATTACHMENT_SUPPORT);
		type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.SINGLE);
		this.systemNoteService.saveSystemNoteType(type);
		// Back to NONE
		type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.NONE);
		this.systemNoteService.saveSystemNoteType(type);
		// To MULTIPLE
		type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.MULTIPLE);
		this.systemNoteService.saveSystemNoteType(type);
		// To Single
		type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.SINGLE);
		this.systemNoteService.saveSystemNoteType(type);
		// TO MULTIPLE
		type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.MULTIPLE);
		this.systemNoteService.saveSystemNoteType(type);
		// Back to NONE
		type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.NONE);
		this.systemNoteService.saveSystemNoteType(type);
	}


	@Test
	public void testSwitchNoteTypeAttachmentSupport_FromNoneToSingle_WithNotes() {
		// Any type that doesn't support attachments can be switched to support attachments even if notes exist
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_NO_ATTACHMENT_SUPPORT);
		SystemNote note = createNote(type);
		this.systemNoteService.saveSystemNote(note);

		type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.SINGLE);
		this.systemNoteService.saveSystemNoteType(type);
	}


	@Test
	public void testSwitchNoteTypeAttachmentSupport_FromNoneToMultiple_WithNotes() {
		// Any type that doesn't support attachments can be switched to support attachments even if notes exist
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_NO_ATTACHMENT_SUPPORT);
		SystemNote note = createNote(type);
		this.systemNoteService.saveSystemNote(note);

		type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.MULTIPLE);
		this.systemNoteService.saveSystemNoteType(type);
	}


	@Test
	public void testSwitchNoteTypeAttachmentSupport_FromSingleToNone_WithNotes() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Any type that does support attachments CANNOT be switched to support attachments if notes exist
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ONE_ATTACHMENT_SUPPORT);
			SystemNote note = createNote(type);
			this.systemNoteService.saveSystemNote(note);

			type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.NONE);
			this.systemNoteService.saveSystemNoteType(type);
		});
		Assertions.assertEquals("You cannot change the attachments supported selection for this note type because notes already exist.", ve.getMessage());
	}


	@Test
	public void testSwitchNoteTypeAttachmentSupport_FromMultipleToNone_WithNotes() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Any type that does support attachments CANNOT be switched to support attachments if notes exist
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_MULTIPLE_ATTACHMENT_SUPPORT);
			SystemNote note = createNote(type);
			this.systemNoteService.saveSystemNote(note);

			type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.NONE);
			this.systemNoteService.saveSystemNoteType(type);
		});
		Assertions.assertEquals("You cannot change the attachments supported selection for this note type because notes already exist.", ve.getMessage());
	}


	@Test
	public void testSwitchNoteTypeAttachmentSupport_FromMultipleToSingle_WithNotes() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Any type that does support attachments CANNOT be switched to support attachments if notes exist
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_MULTIPLE_ATTACHMENT_SUPPORT);
			SystemNote note = createNote(type);
			this.systemNoteService.saveSystemNote(note);

			type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.SINGLE);
			this.systemNoteService.saveSystemNoteType(type);
		});
		Assertions.assertEquals("You cannot change the attachments supported selection for this note type because notes already exist.", ve.getMessage());
	}


	@Test
	public void testSwitchNoteTypeAttachmentSupport_FromSingleToMultiple_WithNotes() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Any type that does support attachments CANNOT be switched to support attachments if notes exist
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ONE_ATTACHMENT_SUPPORT);
			SystemNote note = createNote(type);
			this.systemNoteService.saveSystemNote(note);

			type.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.MULTIPLE);
			this.systemNoteService.saveSystemNoteType(type);
		});
		Assertions.assertEquals("You cannot change the attachments supported selection for this note type because notes already exist.", ve.getMessage());
	}


	@Test
	public void testSwitchNoteAttachmentSupport_FromNone() {
		// Any note associated with a type that doesn't support attachments can be switched to one that does
		SystemNoteType typeNone = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_NO_ATTACHMENT_SUPPORT);
		SystemNoteType typeOne = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ONE_ATTACHMENT_SUPPORT);
		SystemNoteType typeMultiple = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_MULTIPLE_ATTACHMENT_SUPPORT);

		SystemNote note1 = createNote(typeNone);
		this.systemNoteService.saveSystemNote(note1);
		note1.setNoteType(typeOne);
		this.systemNoteService.saveSystemNote(note1);

		SystemNote note2 = createNote(typeNone);
		this.systemNoteService.saveSystemNote(note2);
		note1.setNoteType(typeMultiple);
		this.systemNoteService.saveSystemNote(note2);
	}


	@Test
	public void testSwitchNoteAttachmentSupport_FromSingleToNone() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Any note associated with a type that does support attachments cannot be switched to one that doesn't
			SystemNoteType typeNone = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_NO_ATTACHMENT_SUPPORT);
			SystemNoteType typeOne = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ONE_ATTACHMENT_SUPPORT);

			SystemNote note1 = createNote(typeOne);
			this.systemNoteService.saveSystemNote(note1);
			note1.setNoteType(typeNone);
			this.systemNoteService.saveSystemNote(note1);
		});
		Assertions.assertEquals("Cannot switch note type from [Type 8 - Single Attachment Support] which supports one file attachment to note type [Type 7 - No Attachment Support] which doesn't support file attachments", ve.getMessage());
	}


	@Test
	public void testSwitchNoteAttachmentSupport_FromMultipleToNone() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Any note associated with a type that does support attachments cannot be switched to one that doesn't
			SystemNoteType typeNone = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_NO_ATTACHMENT_SUPPORT);
			SystemNoteType typeMultiple = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_MULTIPLE_ATTACHMENT_SUPPORT);

			SystemNote note1 = createNote(typeMultiple);
			this.systemNoteService.saveSystemNote(note1);
			note1.setNoteType(typeNone);
			this.systemNoteService.saveSystemNote(note1);
		});
		Assertions.assertEquals("Cannot switch note type from [Type 9 - Multiple Attachment Support] which supports multiple file attachments to note type [Type 7 - No Attachment Support] which doesn't support file attachments", ve.getMessage());
	}


	@Test
	public void testSwitchNoteAttachmentSupport_FromMultipleToSingle() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Any note associated with a type that does support attachments cannot be switched to one that doesn't
			SystemNoteType typeOne = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ONE_ATTACHMENT_SUPPORT);
			SystemNoteType typeMultiple = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_MULTIPLE_ATTACHMENT_SUPPORT);

			SystemNote note1 = createNote(typeMultiple);
			this.systemNoteService.saveSystemNote(note1);
			note1.setNoteType(typeOne);
			this.systemNoteService.saveSystemNote(note1);
		});
		Assertions.assertEquals("Cannot switch note type from [Type 9 - Multiple Attachment Support] which supports multiple file attachments to note type [Type 8 - Single Attachment Support] which supports one file attachment", ve.getMessage());
	}


	@Test
	public void testSaveNote_WithEntityModifyCondition_Successful() {
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ENTITY_MODIFY_CONDITION);

		// Create
		SystemNote note1 = createNote(type);
		this.systemNoteService.saveSystemNote(note1);

		// Update
		note1.setText("Update test");
		this.systemNoteService.saveSystemNote(note1);

		// Delete
		this.systemNoteService.deleteSystemNote(note1.getId());
	}


	@Test
	public void testSaveNote_WithEntityModifyCondition_CreateFailure() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("Test Condition Failure Message");
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ENTITY_MODIFY_CONDITION);

			// Create
			SystemNote note1 = createNote(type);
			this.systemNoteService.saveSystemNote(note1);
		});
		Assertions.assertEquals("You do not have permission to INSERT this SystemNote because: Test Condition Failure Message", ve.getMessage());
	}


	@Test
	public void testSaveNote_WithEntityModifyCondition_UpdateFailure() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ENTITY_MODIFY_CONDITION);

			// Create
			SystemNote note1 = createNote(type);
			this.systemNoteService.saveSystemNote(note1);

			// Update
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("Test Condition Failure Message");
			this.systemNoteService.saveSystemNote(note1);
		});
		Assertions.assertEquals("You do not have permission to UPDATE this SystemNote because: Test Condition Failure Message", ve.getMessage());
	}


	@Test
	public void testSaveNote_WithEntityModifyCondition_DeleteFailure() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ENTITY_MODIFY_CONDITION);

			// Create
			SystemNote note1 = createNote(type);
			this.systemNoteService.saveSystemNote(note1);

			// Delete
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("Test Condition Failure Message");
			this.systemNoteService.deleteSystemNote(note1.getId());
		});
		Assertions.assertEquals("You do not have permission to DELETE this SystemNote because: Test Condition Failure Message", ve.getMessage());
	}


	@Test
	public void testSaveNote_WithEntityDeleteCondition_DeleteSuccess() {
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ENTITY_DELETE_CONDITION);

		// Create
		SystemNote note1 = createNote(type);
		this.systemNoteService.saveSystemNote(note1);

		// Update
		note1.setText("Test update");
		this.systemNoteService.saveSystemNote(note1);

		// Delete
		this.systemNoteService.deleteSystemNote(note1.getId());
	}


	@Test
	public void testSaveNote_WithEntityDeleteCondition_DeleteFailure() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("Test Condition Failure Message");

			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ENTITY_DELETE_CONDITION);

			// Create - condition ok because only evaluated on the delete
			SystemNote note1 = createNote(type);
			this.systemNoteService.saveSystemNote(note1);

			// Update - condition ok because only evaluated on the delete
			note1.setText("Test update");
			this.systemNoteService.saveSystemNote(note1);

			// Delete
			this.systemNoteService.deleteSystemNote(note1.getId());
		});
		Assertions.assertEquals("You do not have permission to DELETE this SystemNote because: Test Condition Failure Message", ve.getMessage());
	}


	@Test
	public void testSaveNote_WithEntityModifyAndDeleteCondition_Success() {
		// Will be mocked for both conditions 1 & 2
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ENTITY_MODIFY_AND_DELETE_CONDITION);

		// Create
		SystemNote note1 = createNote(type);
		this.systemNoteService.saveSystemNote(note1);

		// Update
		this.systemNoteService.saveSystemNote(note1);

		// Delete
		// Change the modify condition to fail to confirm the delete condition is being used
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.argThat(PropertyMatcher.<SystemCondition>hasProperty("id", MODIFY_CONDITION_ID)), ArgumentMatchers.any())).thenReturn("I shouldn't matter because of delete condition");
		this.systemNoteService.deleteSystemNote(note1.getId());
	}


	@Test
	public void testSaveNote_WithEntityModifyAndDeleteCondition_UpdateFailure() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Modify condition
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.argThat(PropertyMatcher.<SystemCondition>hasProperty("id", MODIFY_CONDITION_ID)), ArgumentMatchers.any())).thenReturn("");
			// Delete condition
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.argThat(PropertyMatcher.<SystemCondition>hasProperty("id", DELETE_CONDITION_ID)), ArgumentMatchers.any())).thenReturn("I shouldn't matter because don't make it to delete call");

			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ENTITY_MODIFY_AND_DELETE_CONDITION);

			// Create
			SystemNote note1 = createNote(type);
			this.systemNoteService.saveSystemNote(note1);

			// Update - change modify condition to fail
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.argThat(PropertyMatcher.<SystemCondition>hasProperty("id", MODIFY_CONDITION_ID)), ArgumentMatchers.any())).thenReturn("Test Condition Failure Message");
			this.systemNoteService.saveSystemNote(note1);
		});
		Assertions.assertEquals("You do not have permission to UPDATE this SystemNote because: Test Condition Failure Message", ve.getMessage());
	}


	@Test
	public void testSaveNote_WithEntityModifyAndDeleteCondition_DeleteFailure() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Modify condition
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.argThat(PropertyMatcher.<SystemCondition>hasProperty("id", 1)), ArgumentMatchers.any())).thenReturn("");
			// Delete condition
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.argThat(PropertyMatcher.<SystemCondition>hasProperty("id", 2)), ArgumentMatchers.any())).thenReturn("Test Condition Failure Message");

			SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ENTITY_MODIFY_AND_DELETE_CONDITION);

			// Create
			SystemNote note1 = createNote(type);
			this.systemNoteService.saveSystemNote(note1);

			// Update
			this.systemNoteService.saveSystemNote(note1);

			// Delete
			this.systemNoteService.deleteSystemNote(note1.getId());
		});
		Assertions.assertEquals("You do not have permission to DELETE this SystemNote because: Test Condition Failure Message", ve.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////           Note Action Handler Tests                          /////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testNoteActionHandler_SingleNote() {
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.any())).thenReturn(this.systemNoteTestEntityActionHandler);

		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ACTION_BEAN);

		SystemNote note = createNote(type);
		note.setFkFieldId((long) 1);

		this.systemNoteService.saveSystemNote(note);

		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(1)).processSystemNoteAction(null, note, DaoEventTypes.INSERT);
		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(0)).processSystemNoteLinkAction(ArgumentMatchers.isNull(), ArgumentMatchers.any(SystemNoteLink.class), ArgumentMatchers.eq(DaoEventTypes.INSERT));
		Mockito.clearInvocations(this.systemNoteTestEntityActionHandler);

		note.setText("Update again");
		this.systemNoteService.saveSystemNote(note);

		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(1)).processSystemNoteAction(note, note, DaoEventTypes.UPDATE);
		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(0)).processSystemNoteLinkAction(ArgumentMatchers.isNull(), ArgumentMatchers.any(SystemNoteLink.class), ArgumentMatchers.any(DaoEventTypes.class));
		Mockito.clearInvocations(this.systemNoteTestEntityActionHandler);

		this.systemNoteService.deleteSystemNote(note.getId());

		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(1)).processSystemNoteAction(null, note, DaoEventTypes.DELETE);
		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(0)).processSystemNoteLinkAction(ArgumentMatchers.isNull(), ArgumentMatchers.any(SystemNoteLink.class), ArgumentMatchers.eq(DaoEventTypes.DELETE));
	}


	@Test
	public void testNoteActionHandler_LinkedToMultiple() {
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.any())).thenReturn(this.systemNoteTestEntityActionHandler);

		this.systemNoteDAO.setLogicalEvaluatesToTrue(false);
		SystemNoteType type = this.systemNoteService.getSystemNoteType(NOTE_TYPE_WITH_ACTION_BEAN);

		SystemNote note = createNote(type);
		note.setFkFieldId(null);
		note.setLinkedToMultiple(true);
		Long[] fkFieldIds = new Long[]{(long) 1, (long) 2, (long) 3};

		this.systemNoteService.saveSystemNoteWithLinks(note, fkFieldIds);

		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(1)).processSystemNoteAction(null, note, DaoEventTypes.INSERT);
		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(3)).processSystemNoteLinkAction(ArgumentMatchers.isNull(), ArgumentMatchers.any(SystemNoteLink.class), ArgumentMatchers.eq(DaoEventTypes.INSERT));
		Mockito.clearInvocations(this.systemNoteTestEntityActionHandler);

		note.setText("Update again");
		this.systemNoteService.saveSystemNote(note);

		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(1)).processSystemNoteAction(note, note, DaoEventTypes.UPDATE);
		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(0)).processSystemNoteLinkAction(ArgumentMatchers.isNull(), ArgumentMatchers.any(SystemNoteLink.class), ArgumentMatchers.any(DaoEventTypes.class));
		Mockito.clearInvocations(this.systemNoteTestEntityActionHandler);

		this.systemNoteService.deleteSystemNote(note.getId());

		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(1)).processSystemNoteAction(null, note, DaoEventTypes.DELETE);
		Mockito.verify(this.systemNoteTestEntityActionHandler, Mockito.times(3)).processSystemNoteLinkAction(ArgumentMatchers.isNull(), ArgumentMatchers.any(SystemNoteLink.class), ArgumentMatchers.eq(DaoEventTypes.DELETE));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private SystemNote createNote(SystemNoteType type) {
		return createNote(type, "Test Note");
	}


	private SystemNote createNote(SystemNoteType type, String noteText) {
		SystemNote note = new SystemNote();
		note.setText(noteText);
		note.setFkFieldId((long) 1);
		note.setNoteType(type);
		return note;
	}
}
