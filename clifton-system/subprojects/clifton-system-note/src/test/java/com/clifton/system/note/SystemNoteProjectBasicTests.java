package com.clifton.system.note;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemNoteProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "system-note";
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveSystemNoteLink");
		ignoredVoidSaveMethodSet.add("saveSystemNoteWithLinks");
		return ignoredVoidSaveMethodSet;
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
