package com.clifton.system.note;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.system.note.action.SystemNoteActionHandler;


/**
 * @author manderson
 */
public class SystemNoteTestEntityActionHandler implements SystemNoteActionHandler {


	@Override
	public void validateSystemNoteType(SystemNoteType noteType) {
		// DO NOTHING
	}


	@Override
	public void processSystemNoteAction(SystemNote originalBean, SystemNote bean, DaoEventTypes event) {
		// DO NOTHING - TEST VIA MOCK VERIFY
	}


	@Override
	public void processSystemNoteLinkAction(SystemNoteLink originalBean, SystemNoteLink bean, DaoEventTypes event) {
		// DO NOTHING - TEST VIA MOCK VERIFY
	}
}
