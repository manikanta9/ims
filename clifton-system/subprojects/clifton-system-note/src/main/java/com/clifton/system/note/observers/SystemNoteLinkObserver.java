package com.clifton.system.note.observers;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.note.SystemNoteLink;
import com.clifton.system.note.action.SystemNoteActionHandler;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemNoteLinkObserver</code> (if the note's type has an action bean set), will call that bean on the note to perform actions on inserts/updates/deletes
 *
 * @author Mary Anderson
 */
@Component
public class SystemNoteLinkObserver extends SelfRegisteringDaoObserver<SystemNoteLink> {

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<SystemNoteLink> dao, DaoEventTypes event, SystemNoteLink bean, Throwable e) {
		if (e == null) {
			if (bean.getNote().getNoteType().getNoteActionBean() != null) {
				SystemNoteLink originalBean = (event.isUpdate() ? getOriginalBean(dao, bean) : null);
				SystemNoteActionHandler actionHandler = (SystemNoteActionHandler) getSystemBeanService().getBeanInstance(bean.getNote().getNoteType().getNoteActionBean());
				if (actionHandler != null) {
					actionHandler.processSystemNoteLinkAction(originalBean, bean, event);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
