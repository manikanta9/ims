package com.clifton.system.note.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.util.Date;


/**
 * The <code>SystemNoteSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class SystemNoteSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "noteType.id", sortField = "noteType.name")
	private Short noteTypeId;

	@SearchField(searchFieldPath = "noteType", searchField = "name")
	private String noteTypeName;

	@SearchField(searchFieldPath = "noteType", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] noteTypeNames;

	@SearchField(searchFieldPath = "noteType", searchField = "order")
	private Integer noteTypeOrder;

	@SearchField
	private Integer order;

	@SearchField(searchFieldPath = "noteType", searchField = "internal")
	private Boolean internal;

	@SearchField
	private Boolean linkedToMultiple;

	@SearchField(searchFieldPath = "noteType.table", searchField = "name")
	private String tableName;

	@SearchField(searchFieldPath = "noteType", searchField = "table.id", sortField = "table.name")
	private Short tableId;

	@SearchField(searchFieldPath = "noteType", searchField = "linkSystemManaged")
	private Boolean linkSystemManaged;

	@SearchField(searchFieldPath = "noteType.linkedTable", searchField = "name")
	private String linkedTableName;

	@SearchField(searchFieldPath = "noteType", searchField = "linkedTable.id", sortField = "linkedTable.name")
	private Short linkedTableId;

	@SearchField
	private Boolean privateNote;

	@SearchField
	private String text;

	@SearchField
	private Date documentUpdateDate;

	@SearchField
	private String documentUpdateUser;

	/**
	 * USED ONLY FOR SystemNoteService.getSystemNoteListForEntity method
	 * All Custom Search Fields because they are used to "merge" various note types tied to related entities together
	 */
	private Long fkFieldId;

	private String entityTableName;

	// Custom Search Fields used for System Managed Links
	// NOTE: Search Filter(s) are ONLY applied when FKFieldID filter is also included
	// includeNotesForLinkedEntity
	// example: Trade notes - include Security Notes
	private Boolean includeNotesForLinkedEntity;

	// includeNotesAsLinkedEntity
	// example: Security notes - include Trade Notes linked to Security where LinkedFKFieldID = SecurityID
	private Boolean includeNotesAsLinkedEntity;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getNoteTypeName() {
		return this.noteTypeName;
	}


	public void setNoteTypeName(String noteTypeName) {
		this.noteTypeName = noteTypeName;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Boolean getPrivateNote() {
		return this.privateNote;
	}


	public void setPrivateNote(Boolean privateNote) {
		this.privateNote = privateNote;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public String getLinkedTableName() {
		return this.linkedTableName;
	}


	public void setLinkedTableName(String linkedTableName) {
		this.linkedTableName = linkedTableName;
	}


	public Integer getNoteTypeOrder() {
		return this.noteTypeOrder;
	}


	public void setNoteTypeOrder(Integer noteTypeOrder) {
		this.noteTypeOrder = noteTypeOrder;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}


	public Boolean getInternal() {
		return this.internal;
	}


	public void setInternal(Boolean internal) {
		this.internal = internal;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public Short getLinkedTableId() {
		return this.linkedTableId;
	}


	public void setLinkedTableId(Short linkedTableId) {
		this.linkedTableId = linkedTableId;
	}


	public Short getNoteTypeId() {
		return this.noteTypeId;
	}


	public void setNoteTypeId(Short noteTypeId) {
		this.noteTypeId = noteTypeId;
	}


	public Boolean getIncludeNotesForLinkedEntity() {
		return this.includeNotesForLinkedEntity;
	}


	public void setIncludeNotesForLinkedEntity(Boolean includeNotesForLinkedEntity) {
		this.includeNotesForLinkedEntity = includeNotesForLinkedEntity;
	}


	public Boolean getIncludeNotesAsLinkedEntity() {
		return this.includeNotesAsLinkedEntity;
	}


	public void setIncludeNotesAsLinkedEntity(Boolean includeNotesAsLinkedEntity) {
		this.includeNotesAsLinkedEntity = includeNotesAsLinkedEntity;
	}


	public Boolean getLinkSystemManaged() {
		return this.linkSystemManaged;
	}


	public void setLinkSystemManaged(Boolean linkSystemManaged) {
		this.linkSystemManaged = linkSystemManaged;
	}


	public String getEntityTableName() {
		return this.entityTableName;
	}


	public void setEntityTableName(String entityTableName) {
		this.entityTableName = entityTableName;
	}


	public Boolean getLinkedToMultiple() {
		return this.linkedToMultiple;
	}


	public void setLinkedToMultiple(Boolean linkedToMultiple) {
		this.linkedToMultiple = linkedToMultiple;
	}


	public String[] getNoteTypeNames() {
		return this.noteTypeNames;
	}


	public void setNoteTypeNames(String[] noteTypeNames) {
		this.noteTypeNames = noteTypeNames;
	}
}
