package com.clifton.system.note.search;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.PropertySubqueryUnionExpression;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteLink;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.core.util.MathUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemNoteSearchFormConfigurer</code> class configures SystemNoteSearchForm objects.
 * Too much code to be an inner class and has a lot of special handles
 * for cases where we include viewing related entities notes as well.
 *
 * @author manderson
 */
public class SystemNoteSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final SystemNoteService systemNoteService;
	private final SystemSchemaService systemSchemaService;
	private final DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////////

	// Used when getting notes tied to an entity
	// Will automatically populate all relationships where necessary
	private final String entityTableName;
	private final Long entityId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemNoteSearchFormConfigurer(SystemNoteSearchForm searchForm, SystemNoteService systemNoteService, SystemSchemaService systemSchemaService, DaoLocator daoLocator) {
		this(searchForm, null, null, systemNoteService, systemSchemaService, daoLocator);
	}


	public SystemNoteSearchFormConfigurer(SystemNoteSearchForm searchForm, String entityTableName, Long entityId, SystemNoteService systemNoteService, SystemSchemaService systemSchemaService,
	                                      DaoLocator daoLocator) {
		super(searchForm);

		this.systemNoteService = systemNoteService;
		this.systemSchemaService = systemSchemaService;
		this.daoLocator = daoLocator;

		this.entityTableName = entityTableName;
		this.entityId = entityId;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		if (getEntityId() != null) {
			SystemNoteSearchForm searchForm = (SystemNoteSearchForm) getSortableSearchForm();
			appendCriteriaForEntity(searchForm, criteria);
		}
	}


	@Override
	public boolean configureOrderBy(Criteria criteria) {
		// Note Type Order, Note Type Name, Note Order, Note Create Date, Note ID
		criteria.addOrder(Order.asc(getPathAlias("noteType", criteria) + ".order"));
		criteria.addOrder(Order.asc(getPathAlias("noteType", criteria) + ".name"));
		criteria.addOrder(Order.asc("order"));
		criteria.addOrder(Order.desc("createDate"));
		criteria.addOrder(Order.desc("id")); // When uploads are used, notes can have same create date, so when re-loading seems like order sometimes switches them around
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                 Criteria Builder Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////////


	private void appendCriteriaForEntity(SystemNoteSearchForm searchForm, Criteria criteria) {
		List<DetachedCriteria> unionCriteria = new ArrayList<>();

		Map<String, Long> linkedValueMap = new HashMap<>();
		Map<String, List<Long>> linkedEntitiesValueMap = null;
		if (BooleanUtils.isTrue(searchForm.getIncludeNotesForLinkedEntity())) {
			linkedEntitiesValueMap = new HashMap<>();
		}
		populateLinkedMaps(getEntityTableName(), getEntityId(), linkedValueMap, linkedEntitiesValueMap);

		appendCriterionForEntityImpl(unionCriteria, getEntityTableName(), getEntityId(), linkedValueMap);

		if (linkedEntitiesValueMap != null && !linkedEntitiesValueMap.isEmpty()) {
			for (Map.Entry<String, List<Long>> linkedEntitiesEntry : linkedEntitiesValueMap.entrySet()) {
				List<Long> idValues = linkedEntitiesEntry.getValue();
				for (Long id : CollectionUtils.getIterable(idValues)) {
					Map<String, Long> relatedEntityLinkedValueMap = new HashMap<>();
					populateLinkedMaps(linkedEntitiesEntry.getKey(), id, relatedEntityLinkedValueMap, null);
					appendCriterionForEntityImpl(unionCriteria, linkedEntitiesEntry.getKey(), id, relatedEntityLinkedValueMap);
				}
			}
		}

		if (searchForm.getIncludeNotesAsLinkedEntity() != null && searchForm.getIncludeNotesAsLinkedEntity()) {
			appendCriterionForEntityAsLinkedEntityImpl(unionCriteria);
		}
		criteria.add(PropertySubqueryUnionExpression.forPropertyInUnionAll("id", unionCriteria));
	}


	private void populateLinkedMaps(String tableName, Long fkFieldId, Map<String, Long> linkedValueMap, Map<String, List<Long>> linkedEntitiesValueMap) {
		List<SystemNoteType> noteTypeList = getSystemNoteService().getSystemNoteTypeListByTable(tableName);
		noteTypeList = BeanUtils.filter(noteTypeList, SystemNoteType::isLinkedRelationshipUsed);

		if (!CollectionUtils.isEmpty(noteTypeList)) {
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(tableName);
			IdentityObject bean = dao.findByPrimaryKey(fkFieldId);

			for (SystemNoteType nt : noteTypeList) {
				Long id = getPropertyValueAsLong(bean, nt.getLinkedRelationshipPath());
				if (id != null) {
					if (nt.isLinkSystemManaged()) {
						// If map is null then we don't want to include related entities
						if (linkedEntitiesValueMap != null) {
							if (!linkedEntitiesValueMap.containsKey(nt.getLinkedTable().getName())) {
								linkedEntitiesValueMap.put(nt.getLinkedTable().getName(), CollectionUtils.createList(id));
							}
							else {
								List<Long> idValues = linkedEntitiesValueMap.get(nt.getLinkedTable().getName());
								if (!idValues.contains(id)) {
									idValues.add(id);
									linkedEntitiesValueMap.put(nt.getLinkedTable().getName(), idValues);
								}
							}
						}
					}
					else {
						if (!linkedValueMap.containsKey(nt.getLinkedRelationshipPath())) {
							linkedValueMap.put(nt.getLinkedRelationshipPath(), id);
						}
					}
				}
			}
		}
	}


	private void appendCriterionForEntityImpl(List<DetachedCriteria> unionCriteria, String tableName, Long fkFieldId, Map<String, Long> linkedValueMap) {
		if (linkedValueMap != null || fkFieldId != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(SystemNote.class, "n");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("noteType", "nt");
			sub.add(Restrictions.eq("nt.table.id", getSystemSchemaService().getSystemTableByName(tableName).getId()));

			// Linked Relationship Notes
			Disjunction ors = Restrictions.disjunction();
			if (linkedValueMap != null && !CollectionUtils.isEmpty(linkedValueMap.keySet())) {
				for (Map.Entry<String, Long> stringLongEntry : linkedValueMap.entrySet()) {
					Conjunction links = Restrictions.conjunction();
					links.add(Restrictions.eq("nt.linkedRelationshipPath", stringLongEntry.getKey()));
					links.add(Restrictions.eq("nt.linkSystemManaged", false));
					links.add(Restrictions.eq("linkedFkFieldId", stringLongEntry.getValue()));
					ors.add(links);
				}
			}

			// Single Notes
			if (fkFieldId != null) {
				ors.add(Restrictions.eq("fkFieldId", fkFieldId));
			}

			// Global Notes
			Conjunction globals = Restrictions.conjunction();
			globals.add(Restrictions.isNull("fkFieldId"));
			globals.add(Restrictions.isNull("linkedFkFieldId"));
			globals.add(Restrictions.eq("linkedToMultiple", false));
			ors.add(globals);

			// Get Notes Linked through System Note Link Table
			if (fkFieldId != null) {
				DetachedCriteria linkExists = DetachedCriteria.forClass(SystemNoteLink.class, "snl");
				linkExists.setProjection(Projections.property("id"));
				linkExists.add(Restrictions.eqProperty("note.id", "n.id"));
				linkExists.add(Restrictions.eq("fkFieldId", fkFieldId));
				ors.add(Subqueries.exists(linkExists));
			}
			sub.add(ors);
			unionCriteria.add(sub);
		}
	}


	private void appendCriterionForEntityAsLinkedEntityImpl(List<DetachedCriteria> unionCriteria) {
		DetachedCriteria sub = DetachedCriteria.forClass(SystemNote.class, "n");
		sub.setProjection(Projections.property("id"));
		sub.createAlias("noteType", "nt");
		sub.add(Restrictions.eq("nt.linkedTable.id", getSystemSchemaService().getSystemTableByName(getEntityTableName()).getId()));
		sub.add(Restrictions.eq("nt.linkSystemManaged", true));

		Disjunction ors = Restrictions.disjunction();
		ors.add(Restrictions.eq("linkedFkFieldId", getEntityId()));

		// Get Notes Linked through System Note Link Table
		DetachedCriteria linkExists = DetachedCriteria.forClass(SystemNoteLink.class, "snl");
		linkExists.setProjection(Projections.property("id"));
		linkExists.add(Restrictions.eqProperty("note.id", "n.id"));
		linkExists.add(Restrictions.eq("linkedFkFieldId", getEntityId()));
		ors.add(Subqueries.exists(linkExists));

		sub.add(ors);
		unionCriteria.add(sub);
	}


	private Long getPropertyValueAsLong(IdentityObject bean, String relationshipPath) {
		return MathUtils.getNumberAsLong((Number) BeanUtils.getPropertyValue(bean, relationshipPath));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////                  Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public String getEntityTableName() {
		return this.entityTableName;
	}


	public Long getEntityId() {
		return this.entityId;
	}
}
