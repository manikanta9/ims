package com.clifton.system.note.upload;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.upload.SystemUploadHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemNoteUploadServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class SystemNoteUploadServiceImpl implements SystemNoteUploadService {

	private SystemNoteService systemNoteService;
	private SystemSchemaService systemSchemaService;
	private SystemUploadHandler systemUploadHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadSystemNoteFile(SystemNoteUploadCommand uploadCommand) {
		SystemTable fkTable = getSystemSchemaService().getSystemTableByName(uploadCommand.getFkFieldTableName());
		ValidationUtils.assertNotNull(fkTable, "Table selection is required.");
		ValidationUtils.assertNotNull(uploadCommand.getFkFieldId(), "Entity selection is required");

		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, false);
		List<SystemNote> noteList = new ArrayList<>();
		Map<String, SystemNoteType> noteTypeMap = new HashMap<>();
		for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
			SystemNote note = (SystemNote) obj;
			if (note.getNoteType() == null || StringUtils.isEmpty(note.getNoteType().getName())) {
				throw new ValidationException("Note Type is missing for note [" + note.getText() + "]");
			}
			else if (note.getNoteType().getId() == null) {
				if (noteTypeMap.containsKey(note.getNoteType().getName())) {
					note.setNoteType(noteTypeMap.get(note.getNoteType().getName()));
				}
				else {
					SystemNoteType noteType = getSystemNoteService().getSystemNoteTypeByTableAndName(fkTable.getId(), note.getNoteType().getName());
					if (noteType == null) {
						throw new ValidationException("Note Type [" + note.getNoteType().getName() + "] is not a valid type for table [" + fkTable.getName() + "]");
					}
					noteTypeMap.put(note.getNoteType().getName(), noteType);
					note.setNoteType(noteType);
				}
			}

			// If specific entity passed from the UI use it
			if (uploadCommand.getFkFieldId() != null) {
				note.setFkFieldId(MathUtils.getNumberAsLong(uploadCommand.getFkFieldId()));
			}
			noteList.add(note);
		}
		getSystemNoteService().saveSystemNoteList(noteList);
		uploadCommand.getUploadResult().addUploadResults(SystemNote.SYSTEM_NOTE_TABLE_NAME, CollectionUtils.getSize(noteList), false);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}
}
