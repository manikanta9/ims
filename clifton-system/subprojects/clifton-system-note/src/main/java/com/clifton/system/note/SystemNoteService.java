package com.clifton.system.note;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.note.observers.SystemNoteTypeObserver;
import com.clifton.system.note.search.SystemNoteSearchForm;
import com.clifton.system.note.search.SystemNoteTypeSearchForm;

import java.util.List;


/**
 * The <code>SystemNoteService</code>
 *
 * @author Mary Anderson
 */
public interface SystemNoteService {

	////////////////////////////////////////////////////////////////////////////////
	/////////              System Note Type Business Methods             ///////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemNoteType getSystemNoteType(short id);


	public SystemNoteType getSystemNoteTypeByTableAndName(short tableId, String noteTypeName);


	public List<SystemNoteType> getSystemNoteTypeList(SystemNoteTypeSearchForm searchForm);


	public List<SystemNoteType> getSystemNoteTypeListByTable(String tableName);


	@SecureMethod(dynamicTableNameBeanPath = "table.name", dynamicSecurityResourceTableNameSuffix = "Note", permissionResolverBeanName = "systemNoteSecureMethodPermissionResolver")
	public SystemNoteType saveSystemNoteType(SystemNoteType bean);


	@SecureMethod(dynamicTableNameBeanPath = "table.name", dynamicSecurityResourceTableNameSuffix = "Note", permissionResolverBeanName = "systemNoteSecureMethodPermissionResolver")
	public void deleteSystemNoteType(short id);


	////////////////////////////////////////////////////////////////////////////////
	///////////              System Note Business Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemNote getSystemNote(int id);


	public List<SystemNote> getSystemNotes(Integer[] ids);


	/**
	 * Applies special filters for notes tied to a specific entity (requires entityTableName & fkFieldId)
	 * and optionally related entity notes (includeNotesForLinkedEntity & includeNotesAsLinkedEntity)
	 */
	public List<SystemNote> getSystemNoteListForEntity(SystemNoteSearchForm searchForm);


	public List<SystemNote> getSystemNoteList(SystemNoteSearchForm searchForm);

	public void copySystemNoteList(SystemNoteSearchForm searchForm, Long toEntityId);

	@SecureMethod(dynamicTableNameBeanPath = "noteType.table.name", dynamicSecurityResourceTableNameSuffix = "Note", permissionResolverBeanName = "systemNoteSecureMethodPermissionResolver")
	public SystemNote saveSystemNote(SystemNote bean);


	/**
	 * FKFieldIds can be passed to immediately link the note to additional entities
	 */
	@SecureMethod(dynamicTableNameBeanPath = "noteType.table.name", dtoClass = SystemNote.class, dynamicSecurityResourceTableNameSuffix = "Note", permissionResolverBeanName = "systemNoteSecureMethodPermissionResolver")
	public void saveSystemNoteWithLinks(SystemNote bean, Long[] fkFieldIds);


	public void saveSystemNoteList(List<SystemNote> beanList);


	/**
	 * Sets Private Flag = true for all notes of given type
	 * Called from {@link SystemNoteTypeObserver} class when a note type is changed to private
	 */
	@DoNotAddRequestMapping
	public void saveSystemNotePrivate(short systemNoteTypeId);


	@SecureMethod(dynamicTableNameBeanPath = "noteType.table.name", dynamicSecurityResourceTableNameSuffix = "Note", permissionResolverBeanName = "systemNoteSecureMethodPermissionResolver")
	public void deleteSystemNote(int id);


	////////////////////////////////////////////////////////////////////////////////
	/////////              System Note Link Business Methods             ///////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dynamicTableNameBeanPath = "noteType.table.name", dtoClass = SystemNote.class, dynamicIdUrlParameter = "noteId", dynamicSecurityResourceTableNameSuffix = "Note", permissionResolverBeanName = "systemNoteSecureMethodPermissionResolver")
	public void saveSystemNoteLink(int noteId, long fkFieldId);


	@SecureMethod(dynamicTableNameBeanPath = "noteType.table.name", dtoClass = SystemNote.class, dynamicIdUrlParameter = "noteId", dynamicSecurityResourceTableNameSuffix = "Note", permissionResolverBeanName = "systemNoteSecureMethodPermissionResolver")
	public void deleteSystemNoteLink(int noteId, long fkFieldId);


	////////////////////////////////////////////////////////////////////////////////
	/////////              System Note Entity Business Methods           ///////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used to find entities that linked via SystemNoteLink table ONLY
	 */
	@SecureMethod(dynamicTableNameBeanPath = "noteType.table.name", dtoClass = SystemNote.class, dynamicIdUrlParameter = "noteId", dynamicSecurityResourceTableNameSuffix = "Note")
	public List<SystemNoteEntity> getSystemNoteLinkedEntityListByNote(int noteId);


	/**
	 * Used to find entities that are tied to a system note. "Used By" tab on the note.
	 * Single, and Linked to Multiple pull actual beans
	 * Linked Relationship returns the list of linked entities (i.e. Accounts for Performance summary notes)
	 * Global - Throws a validation exception so it doesn't return all entities for a table.
	 */
	@SecureMethod(dynamicTableNameBeanPath = "noteType.table.name", dtoClass = SystemNote.class, dynamicIdUrlParameter = "noteId", dynamicSecurityResourceTableNameSuffix = "Note")
	public List<SystemNoteEntity> getSystemNoteUsedByEntityListByNote(int noteId);
}
