package com.clifton.system.note;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.audit.auditor.SystemAuditDaoUtils;
import com.clifton.system.note.observers.SystemNoteObserver;
import com.clifton.system.note.observers.SystemNoteTypeObserver;
import com.clifton.system.note.search.SystemNoteSearchForm;
import com.clifton.system.note.search.SystemNoteSearchFormConfigurer;
import com.clifton.system.note.search.SystemNoteTypeSearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemNoteServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class SystemNoteServiceImpl implements SystemNoteService {

	private AdvancedUpdatableDAO<SystemNoteType, Criteria> systemNoteTypeDAO;
	private AdvancedUpdatableDAO<SystemNote, Criteria> systemNoteDAO;
	private AdvancedUpdatableDAO<SystemNoteLink, Criteria> systemNoteLinkDAO;

	private DaoLocator daoLocator;

	private DaoSingleKeyListCache<SystemNoteType, Short> systemNoteTypeCache;

	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////////
	/////////              System Note Type Business Methods             ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemNoteType getSystemNoteType(short id) {
		return getSystemNoteTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemNoteType getSystemNoteTypeByTableAndName(short tableId, String noteTypeName) {
		return getSystemNoteTypeDAO().findOneByFields(new String[]{"table.id", "name"}, new Object[]{tableId, noteTypeName});
	}


	@Override
	public List<SystemNoteType> getSystemNoteTypeList(SystemNoteTypeSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getTableName())) {
			searchForm.setTableId(getSystemSchemaService().getSystemTableByName(searchForm.getTableName()).getId());
			searchForm.setTableName(null);
		}
		if (!StringUtils.isEmpty(searchForm.getLinkedTableName())) {
			searchForm.setLinkedTableId(getSystemSchemaService().getSystemTableByName(searchForm.getLinkedTableName()).getId());
			searchForm.setLinkedTableName(null);
		}
		return getSystemNoteTypeListImpl(searchForm);
	}


	@Override
	public List<SystemNoteType> getSystemNoteTypeListByTable(String tableName) {
		return getSystemNoteTypeCache().getBeanListForKeyValue(getSystemNoteTypeDAO(), getSystemSchemaService().getSystemTableByName(tableName).getId());
	}


	private List<SystemNoteType> getSystemNoteTypeListImpl(final SystemNoteTypeSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getLinkedNoteAllowed() != null) {
					if (searchForm.getLinkedNoteAllowed()) {
						criteria.add(Restrictions.and(Restrictions.eq("linkSystemManaged", false), Restrictions.isNotNull("linkedTable.id")));
					}
					else {
						criteria.add(Restrictions.or(Restrictions.eq("linkSystemManaged", true), Restrictions.isNull("linkedTable.id")));
					}
				}
			}
		};
		return getSystemNoteTypeDAO().findBySearchCriteria(config);
	}


	/**
	 * @see SystemNoteTypeObserver
	 */
	@Override
	public SystemNoteType saveSystemNoteType(SystemNoteType bean) {
		return getSystemNoteTypeDAO().save(bean);
	}


	@Override
	public void deleteSystemNoteType(short id) {
		getSystemNoteTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////              System Note Business Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemNote getSystemNote(int id) {
		return getSystemNoteDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemNote> getSystemNotes(Integer[] ids) {
		return getSystemNoteDAO().findByPrimaryKeys(ids);
	}


	@Override
	public List<SystemNote> getSystemNoteListForEntity(SystemNoteSearchForm searchForm) {
		ValidationUtils.assertNotNull(searchForm.getFkFieldId(), "[fkFieldId] is required to look up all notes related to an entity");
		ValidationUtils.assertNotNull(searchForm.getEntityTableName(), "[entityTableName] is required to look up all notes related to an entity");
		return getSystemNoteDAO().findBySearchCriteria(
				new SystemNoteSearchFormConfigurer(searchForm, searchForm.getEntityTableName(), searchForm.getFkFieldId(), this, getSystemSchemaService(), getDaoLocator()));
	}


	@Override
	public List<SystemNote> getSystemNoteList(SystemNoteSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getTableName())) {
			searchForm.setTableId(getSystemSchemaService().getSystemTableByName(searchForm.getTableName()).getId());
			searchForm.setTableName(null);
		}
		if (!StringUtils.isEmpty(searchForm.getLinkedTableName())) {
			searchForm.setLinkedTableId(getSystemSchemaService().getSystemTableByName(searchForm.getLinkedTableName()).getId());
			searchForm.setLinkedTableName(null);
		}
		return getSystemNoteDAO().findBySearchCriteria(new SystemNoteSearchFormConfigurer(searchForm, this, getSystemSchemaService(), getDaoLocator()));
	}


	/**
	 * @see SystemNoteObserver
	 */
	@Override
	public SystemNote saveSystemNote(SystemNote bean) {
		// Observer also runs public void validateSystemNoteModifyCondition on the bean for inserts, or original bean for updates
		ValidationUtils.assertNotNull(bean.getNoteType(), "Note Type is required", "noteType.name");

		// Validated here instead of observer so can validate the note & all links in one database lookup
		validateOnePerEntityNote(bean, bean.getFkFieldId());

		// ONLY attempt to overwrite it if the note type is link system managed
		if (bean.getNoteType().isLinkSystemManaged()) {
			bean.setLinkedFkFieldId(getSystemManagedLinkedFKFieldIdValue(bean, bean.getFkFieldId()));
		}
		return getSystemNoteDAO().save(bean);
	}


	@Override
	@Transactional
	public void saveSystemNoteWithLinks(SystemNote bean, Long[] fkFieldIds) {
		boolean addLinks = (bean.isNewBean() && fkFieldIds != null && fkFieldIds.length > 0);
		saveSystemNote(bean);

		if (addLinks) {
			SystemAuditDaoUtils.executeWithAuditingDisabled(new String[]{"fkFieldId", "linkedToMultiple"}, () -> {
				saveSystemNoteLinks(bean.getId(), fkFieldIds);
				return null;
			});
		}
	}


	private void validateOnePerEntityNote(final SystemNote note, final Long fkFieldId) {
		if (fkFieldId != null && note.getNoteType().isOnePerEntity()) {
			HibernateSearchConfigurer searchConfig = criteria -> {

				if (!note.isNewBean()) {
					criteria.add(Restrictions.ne("id", note.getId()));
				}
				criteria.add(Restrictions.eq("noteType.id", note.getNoteType().getId()));

				Disjunction ors = Restrictions.disjunction();
				ors.add(Restrictions.eq("fkFieldId", fkFieldId));

				DetachedCriteria linkExists = DetachedCriteria.forClass(SystemNoteLink.class, "snl");
				linkExists.setProjection(Projections.property("id"));
				linkExists.add(Restrictions.eqProperty("note.id", criteria.getAlias() + ".id"));
				linkExists.add(Restrictions.eq("fkFieldId", fkFieldId));
				ors.add(Subqueries.exists(linkExists));

				criteria.add(ors);
			};
			List<SystemNote> noteList = getSystemNoteDAO().findBySearchCriteria(searchConfig);
			List<SystemNote> filteredNoteList = new ArrayList<>();
			if (note.getNoteType().isDateRangeAllowed()) {
				for (SystemNote dupeNote : CollectionUtils.getIterable(noteList)) {
					if (DateUtils.isOverlapInDates(note.getStartDate(), note.getEndDate(), dupeNote.getStartDate(), dupeNote.getEndDate())) {
						filteredNoteList.add(dupeNote);
					}
				}
			}
			else {
				filteredNoteList = noteList;
			}

			if (!CollectionUtils.isEmpty(filteredNoteList)) {
				throw new ValidationException("Unable to save [" + note.getNoteType().getName()
						+ "] note because it allows only one per entity and there is already an existing note is associated with this entity.");
			}
		}
	}


	private Long getSystemManagedLinkedFKFieldIdValue(SystemNote note, Long fkFieldId) {
		Long linkedValue = null;
		if (fkFieldId != null && note != null && note.getNoteType() != null && note.getNoteType().isLinkSystemManaged()) {
			SystemTable table = note.getNoteType().getTable();
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(table.getName());
			IdentityObject bean = dao.findByPrimaryKey(fkFieldId);
			Object linkedValueObj = BeanUtils.getPropertyValue(bean, note.getNoteType().getLinkedRelationshipPath());
			if (linkedValueObj != null) {
				if (linkedValueObj instanceof Integer) {
					linkedValue = ((Integer) linkedValueObj).longValue();
				}
				else if (linkedValueObj instanceof Short) {
					linkedValue = ((Short) linkedValueObj).longValue();
				}
				else if (linkedValueObj instanceof Long) {
					linkedValue = ((Long) linkedValueObj);
				}
			}
		}
		return linkedValue;
	}


	@Override
	public void saveSystemNotePrivate(short systemNoteTypeId) {
		SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
		searchForm.setNoteTypeId(systemNoteTypeId);
		searchForm.setPrivateNote(false);
		List<SystemNote> list = getSystemNoteList(searchForm);
		for (SystemNote n : CollectionUtils.getIterable(list)) {
			n.setPrivateNote(true);
		}
		getSystemNoteDAO().saveList(list);
	}


	@Override
	public void copySystemNoteList(SystemNoteSearchForm searchForm, Long toEntityId) {
		ValidationUtils.assertNotNull(toEntityId, "toEntityId must not be null");
		List<SystemNote> copyNoteList = getSystemNoteListForEntity(searchForm);
		List<SystemNote> newNoteList = new ArrayList<>();
		for (SystemNote copyNote : CollectionUtils.getIterable(copyNoteList)) {
			SystemNote newNote = new SystemNote();
			BeanUtils.copyProperties(copyNote, newNote, new String[]{"id", "fkFieldId"});
			newNote.setFkFieldId(toEntityId);
			newNoteList.add(newNote);
		}
		saveSystemNoteList(newNoteList);
	}


	@Override
	@Transactional
	public void saveSystemNoteList(List<SystemNote> beanList) {
		for (SystemNote note : CollectionUtils.getIterable(beanList)) {
			saveSystemNote(note);
		}
	}


	@Override
	@Transactional
	public void deleteSystemNote(int id) {
		SystemNote bean = getSystemNote(id);
		if (bean != null) {
			if (bean.isLinkedToMultiple()) {
				getSystemNoteLinkDAO().deleteList(getSystemNoteLinkListByNote(id));
			}
			getSystemNoteDAO().delete(id);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////              System Note Link Business Methods             ///////////
	////////////////////////////////////////////////////////////////////////////////


	private List<SystemNoteLink> getSystemNoteLinkListByNote(int noteId) {
		return getSystemNoteLinkDAO().findByField("note.id", noteId);
	}


	@Override
	public void saveSystemNoteLink(int noteId, long fkFieldId) {
		updateSystemNoteLinkList(noteId, fkFieldId, true, false);
	}


	private void saveSystemNoteLinks(int noteId, Long[] fkFieldIds) {
		if (fkFieldIds != null && fkFieldIds.length > 0) {
			for (Long fk : fkFieldIds) {
				updateSystemNoteLinkList(noteId, fk, true, true);
			}
		}
	}


	@Override
	public void deleteSystemNoteLink(int noteId, long fkFieldId) {
		updateSystemNoteLinkList(noteId, fkFieldId, false, false);
	}


	@Transactional
	protected void updateSystemNoteLinkList(int noteId, long fkFieldId, boolean add, boolean ignoreAddIfAlreadyLinked) {
		SystemNote note = getSystemNote(noteId);
		if (!note.getNoteType().isLinkToMultipleAllowed()) {
			throw new ValidationException("Note type [" + note.getNoteType().getName() + "] does not support linking to multiple entities. Unable to " + (add ? "add" : "remove") + " link.");
		}
		List<SystemNoteLink> originalList = null;
		List<SystemNoteLink> newList = new ArrayList<>();
		if (note.isLinkedToMultiple()) {
			originalList = getSystemNoteLinkListByNote(noteId);
		}
		else {
			if (add) {
				if (MathUtils.isEqual(note.getFkFieldId(), fkFieldId)) {
					if (ignoreAddIfAlreadyLinked) {
						return;
					}
					throw new ValidationException("This note is already linked to this entity.");
				}
				// Otherwise convert single note to use links
				SystemNoteLink link = new SystemNoteLink();
				link.setNote(note);
				link.setFkFieldId(note.getFkFieldId());
				link.setLinkedFkFieldId(note.getLinkedFkFieldId());

				newList.add(link);
			}
			else {
				throw new ValidationException("Note is not linked to multiple entities.  Cannot remove this link - to remove please delete the note.");
			}
		}

		for (SystemNoteLink snl : CollectionUtils.getIterable(originalList)) {
			if (MathUtils.isEqual(snl.getFkFieldId(), fkFieldId)) {
				if (add) {
					if (ignoreAddIfAlreadyLinked) {
						return;
					}
					throw new ValidationException("This note is already linked to this entity.");
				}
			}
			else {
				newList.add(snl);
			}
		}

		if (add) {
			SystemNoteLink newLink = new SystemNoteLink();
			newLink.setNote(note);
			newLink.setFkFieldId(fkFieldId);
			validateOnePerEntityNote(note, fkFieldId);
			// Could only used linked to multiple with system managed links, so OK to overwrite it always
			newLink.setLinkedFkFieldId(getSystemManagedLinkedFKFieldIdValue(note, fkFieldId));
			newList.add(newLink);
		}

		// Save the note to capture linked to multiple changes
		// Call DAO method direct to avoid extra checks that aren't needed when just copying from/to links that have already been validated
		// Only One Link Left - switch to not multiple
		if (CollectionUtils.getSize(newList) == 1) {
			SystemNoteLink link = newList.get(0);
			note.setLinkedToMultiple(false);
			note.setFkFieldId(link.getFkFieldId());
			note.setLinkedFkFieldId(link.getLinkedFkFieldId());
			newList = null;
		}
		// Otherwise flag as linked to multiple
		else {
			note.setLinkedToMultiple(true);
			note.setFkFieldId(null);
			note.setLinkedFkFieldId(null);
		}
		getSystemNoteDAO().save(note);
		getSystemNoteLinkDAO().saveList(newList, originalList);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////              System Note Entity Business Methods           ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemNoteEntity> getSystemNoteLinkedEntityListByNote(int noteId) {
		SystemNote note = getSystemNote(noteId);
		SystemTable table = note.getNoteType().getTable();

		if (!note.getNoteType().isLinkToMultipleAllowed()) {
			throw new ValidationException("Cannot load links for a note type that doesn't support linking to multiple.");
		}
		if (!note.isLinkedToMultiple()) {
			return null;
		}
		List<SystemNoteEntity> list = new ArrayList<>();
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(table.getName());

		List<IdentityObject> beanList = dao.findByPrimaryKeys(BeanUtils.getPropertyValues(getSystemNoteLinkListByNote(noteId), noteLink -> DaoUtils.convertToPrimaryKeyDataType(noteLink.getFkFieldId(), dao.getConfiguration()), Serializable.class));
		for (IdentityObject bean : CollectionUtils.getIterable(beanList)) {
			list.add(getSystemNoteEntity(note, table, bean));
		}
		return list;
	}


	@Override
	public List<SystemNoteEntity> getSystemNoteUsedByEntityListByNote(int noteId) {
		SystemNote note = getSystemNote(noteId);
		SystemTable table = note.getNoteType().getTable();
		// won't be available on the global tab, but just in case so we don't pull all
		if ("GLOBAL".equals(note.getApplyTo())) {
			throw new ValidationException("Cannot list all entities linked to this note as it applies to all entities in the [" + table.getName() + "] table.");
		}
		List<SystemNoteEntity> list = new ArrayList<>();
		if ("LINK".equals(note.getApplyTo()) || "SINGLE".equals(note.getApplyTo())) {
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(table.getName());
			if (note.getFkFieldId() != null) {
				IdentityObject bean = dao.findByPrimaryKey(note.getFkFieldId());
				list.add(getSystemNoteEntity(note, table, bean));
			}
			else if (note.isLinkedToMultiple()) {
				List<IdentityObject> beanList = dao.findByPrimaryKeys(BeanUtils.getPropertyValues(getSystemNoteLinkListByNote(noteId), "fkFieldId", Integer.class));
				for (IdentityObject bean : CollectionUtils.getIterable(beanList)) {
					list.add(getSystemNoteEntity(note, table, bean));
				}
			}
		}
		// Linked Relationship Path
		else if (note.getNoteType().getLinkedTable() != null) {
			ReadOnlyDAO<IdentityObject> linkedDao = getDaoLocator().locate(note.getNoteType().getLinkedTable().getName());
			IdentityObject bean = linkedDao.findByPrimaryKey(note.getLinkedFkFieldId());
			list.add(getSystemNoteEntity(note, note.getNoteType().getLinkedTable(), bean));
		}
		else {
			throw new ValidationException("Unable to determine entities that use [" + note.getApplyTo() + "] note for note type [" + note.getNoteType().getName() + "].");
		}
		return list;
	}


	private SystemNoteEntity getSystemNoteEntity(SystemNote note, SystemTable drillDownTable, IdentityObject bean) {
		SystemNoteEntity entity = new SystemNoteEntity();
		entity.setNote(note);
		entity.setEntity(bean);
		entity.setDrillDownTable(drillDownTable);
		if (bean != null) {
			entity.setDrillDownId(BeanUtils.getIdentityAsInteger(bean));
		}
		return entity;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                Getter & Setter Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemNoteType, Criteria> getSystemNoteTypeDAO() {
		return this.systemNoteTypeDAO;
	}


	public void setSystemNoteTypeDAO(AdvancedUpdatableDAO<SystemNoteType, Criteria> systemNoteTypeDAO) {
		this.systemNoteTypeDAO = systemNoteTypeDAO;
	}


	public AdvancedUpdatableDAO<SystemNote, Criteria> getSystemNoteDAO() {
		return this.systemNoteDAO;
	}


	public void setSystemNoteDAO(AdvancedUpdatableDAO<SystemNote, Criteria> systemNoteDAO) {
		this.systemNoteDAO = systemNoteDAO;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public AdvancedUpdatableDAO<SystemNoteLink, Criteria> getSystemNoteLinkDAO() {
		return this.systemNoteLinkDAO;
	}


	public void setSystemNoteLinkDAO(AdvancedUpdatableDAO<SystemNoteLink, Criteria> systemNoteLinkDAO) {
		this.systemNoteLinkDAO = systemNoteLinkDAO;
	}


	public DaoSingleKeyListCache<SystemNoteType, Short> getSystemNoteTypeCache() {
		return this.systemNoteTypeCache;
	}


	public void setSystemNoteTypeCache(DaoSingleKeyListCache<SystemNoteType, Short> systemNoteTypeCache) {
		this.systemNoteTypeCache = systemNoteTypeCache;
	}
}
