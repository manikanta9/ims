package com.clifton.system.note.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.system.note.SystemNoteType;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemNoteTypeCache</code> stores a list of note types for a table
 *
 * @author manderson
 */
@Component
public class SystemNoteTypeCache extends SelfRegisteringSingleKeyDaoListCache<SystemNoteType, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "table.id";
	}


	@Override
	protected Short getBeanKeyValue(SystemNoteType bean) {
		if (bean.getTable() != null) {
			return bean.getTable().getId();
		}
		return null;
	}
}
