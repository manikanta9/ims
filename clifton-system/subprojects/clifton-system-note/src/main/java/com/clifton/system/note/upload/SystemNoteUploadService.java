package com.clifton.system.note.upload;


import com.clifton.core.security.authorization.SecureMethod;


/**
 * The <code>SystemNoteUploadService</code> ...
 *
 * @author Mary Anderson
 */
public interface SystemNoteUploadService {

	/**
	 * Uploads notes into the system
	 * Performs proper default setting for fields, and validation
	 */
	@SecureMethod(dynamicTableNameUrlParameter = "fkFieldTableName", dynamicSecurityResourceTableNameSuffix = "Note", permissionResolverBeanName = "systemNoteSecureMethodPermissionResolver")
	public void uploadSystemNoteFile(SystemNoteUploadCommand uploadCommand);
}
