package com.clifton.system.note.observers;


import com.clifton.core.beans.document.DocumentAttachmentSupportedTypes;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.note.action.SystemNoteActionHandler;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>SystemNoteObserver</code> validates the note based on the note type's options
 * i.e. if blank notes are allowed, link to multiple, order, date range, etc.
 * <p>
 * Also, if the note's type has an action bean set, will call that bean on the note to perform actions on inserts/updates/deletes
 *
 * @author Mary Anderson
 */
@Component
public class SystemNoteObserver extends SelfRegisteringDaoObserver<SystemNote> {

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeTransactionMethodCallImpl(ReadOnlyDAO<SystemNote> dao, DaoEventTypes event, SystemNote bean) {
		if (event.isInsert() || event.isUpdate()) {
			// NOTE: One Per Entity Validation is done on the Service method so
			// can better handle manually linked notes and validate everything in one database lookup

			SystemNoteType type = bean.getNoteType();
			ValidationUtils.assertNotNull(type, "Note Type is required.");

			Short maxNoteLength = type.getMaxNoteLength();
			int actualNoteLength = StringUtils.length(bean.getText());
			if (maxNoteLength != null && actualNoteLength > maxNoteLength) {
				throw new ValidationException("Note length is [" + actualNoteLength + "], which exceeds the limit of [" + maxNoteLength + "] for the System Note Type ["
						+ type.getName() + "]");
			}

			if (!type.isBlankNoteAllowed()) {
				ValidationUtils.assertNotNull(bean.getText(), "Note Text is required.");
			}

			if (!type.isLinkToMultipleAllowed() && bean.isLinkedToMultiple()) {
				throw new ValidationException("Linked to Multiple entities notes are not allowed for type [" + type.getName() + "].");
			}
			if (!type.isGlobalNoteAllowed() && bean.getFkFieldId() == null && bean.getLinkedFkFieldId() == null && !bean.isLinkedToMultiple()) {
				throw new ValidationException("Global notes are not allowed for type [" + type.getName() + "].");
			}

			if (!type.isLinkedRelationshipUsed() && bean.getLinkedFkFieldId() != null) {
				throw new ValidationException("Linked relationship notes are not allowed for type [" + type.getName() + "].");
			}

			if (type.isInternal()) {
				bean.setPrivateNote(true);
			}
			if (!type.isOrderSupported()) {
				bean.setOrder(null);
			}
			if (!type.isDateRangeAllowed()) {
				bean.setStartDate(null);
				if (!type.isEndDateResolutionDate()) {
					bean.setEndDate(null);
				}
			}

			if (type.isEndDateResolutionDate() && bean.getEndDate() != null) {
				ValidationUtils.assertTrue(DateUtils.compare(bean.getEndDate(), new Date(), false) <= 0, "Resolution date cannot be in the future.  Please enter a resolution date on or before today.");
			}

			if (event.isUpdate()) {
				SystemNote originalBean = getOriginalBean(dao, bean);
				// If switching note types, need to validate documents on the note allow for that note type switch
				if (!bean.getNoteType().equals(originalBean.getNoteType()) && DocumentAttachmentSupportedTypes.NONE != originalBean.getNoteType().getAttachmentSupportedType()) {
					throw new ValidationException("Cannot switch note type from [" + originalBean.getNoteType().getName() + "] which " + getDocumentAttachmentSupportedStringForNote(originalBean)
							+ " to note type [" + bean.getNoteType().getName() + "] which " + getDocumentAttachmentSupportedStringForNote(bean));
				}
			}
		}
	}


	private String getDocumentAttachmentSupportedStringForNote(SystemNote note) {
		if (note.isSingleAttachmentAllowed()) {
			return "supports one file attachment";
		}
		if (note.isMultipleAttachmentsAllowed()) {
			return "supports multiple file attachments";
		}
		return "doesn't support file attachments";
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<SystemNote> dao, DaoEventTypes event, SystemNote bean, Throwable e) {
		if (e == null) {
			if (bean.getNoteType().getNoteActionBean() != null) {
				SystemNote originalBean = (event.isUpdate() ? getOriginalBean(dao, bean) : null);
				SystemNoteActionHandler actionHandler = (SystemNoteActionHandler) getSystemBeanService().getBeanInstance(bean.getNoteType().getNoteActionBean());
				if (actionHandler != null) {
					actionHandler.processSystemNoteAction(originalBean, bean, event);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
