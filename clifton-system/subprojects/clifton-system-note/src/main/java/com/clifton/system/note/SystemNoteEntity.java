package com.clifton.system.note;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemNoteEntity</code> is a virtual DTO object that is used for the "used by" for notes, or for when the note is linked to many, displaying a user
 * friendly list of those entities with drill down capability.
 *
 * @author manderson
 */
@NonPersistentObject
public class SystemNoteEntity implements LabeledObject {

	private SystemNote note;

	private SystemTable drillDownTable;
	private Integer drillDownId;

	private IdentityObject entity;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getEntity() instanceof LabeledObject) {
			return ((LabeledObject) getEntity()).getLabel();
		}
		if (getDrillDownTable() != null) {
			return getDrillDownTable().getName() + ": " + getDrillDownId();
		}
		return "" + getDrillDownId();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemTable getDrillDownTable() {
		return this.drillDownTable;
	}


	public void setDrillDownTable(SystemTable drillDownTable) {
		this.drillDownTable = drillDownTable;
	}


	public Integer getDrillDownId() {
		return this.drillDownId;
	}


	public void setDrillDownId(Integer drillDownId) {
		this.drillDownId = drillDownId;
	}


	public IdentityObject getEntity() {
		return this.entity;
	}


	public void setEntity(IdentityObject entity) {
		this.entity = entity;
	}


	public SystemNote getNote() {
		return this.note;
	}


	public void setNote(SystemNote note) {
		this.note = note;
	}
}
