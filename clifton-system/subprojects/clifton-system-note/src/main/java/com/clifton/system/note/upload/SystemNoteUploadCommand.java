package com.clifton.system.note.upload;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.system.note.SystemNote;
import com.clifton.system.upload.SystemUploadCommand;


/**
 * The <code>SystemNoteUploadCommand</code> extends the System Upload
 * however has some system note specific fields that will be applied
 * to the notes during the insert process.
 *
 * @author Mary Anderson
 */
public class SystemNoteUploadCommand extends SystemUploadCommand {

	/**
	 * The table the notes are applied to.  Used to generate natural key columns
	 * for the sample, or to know which natural key columns to use during the upload
	 * and also which note types are allowed
	 * <p>
	 * Also used to validate security permissions
	 */
	private String fkFieldTableName;

	/**
	 * Required - Currently only supports uploading notes for a specific entity
	 */
	private Integer fkFieldId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return SystemNote.SYSTEM_NOTE_TABLE_NAME;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isSimple() {
		return true;
	}


	// Overrides - Notes Import is all or nothing, do not insert fk beans, and always inserts, never updates


	@Override
	@ValueIgnoringGetter
	public boolean isPartialUploadAllowed() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getFkFieldTableName() {
		return this.fkFieldTableName;
	}


	public void setFkFieldTableName(String fkFieldTableName) {
		this.fkFieldTableName = fkFieldTableName;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}
}
