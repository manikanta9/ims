package com.clifton.system.note;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.security.authorization.SecureMethodPermissionResolver;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;


/**
 * The <code>SystemNoteSecureMethodPermissionResolver</code> is used to customize the security level required to access {@link SystemNote} and {@link SystemNoteType}
 * If the security resource that is determined to be used ends in the "Note" suffix, standard security CREATE / WRITE / DELETE is required for the method.
 * However, if the security resource does not end in the "Note" suffix, then WRITE access is used.
 * <p>
 * Example: TradeNote - uses CREATE, WRITE, DELETE access, BusinessClient (because no security resource for BusinessClientNote) would require only WRITE access
 * <p>
 * NOTE: Only use this for editing type methods.  All get / read level methods can use read access and do not need to be customized
 *
 * @author manderson
 */
@Component
public class SystemNoteSecureMethodPermissionResolver implements SecureMethodPermissionResolver {

	private static final String DYNAMIC_SECURITY_RESOURCE_SUFFIX = "Note";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getRequiredPermissions(Method method, Map<String, ?> config, String securityResourceName) {
		if (StringUtils.endsWith(securityResourceName, DYNAMIC_SECURITY_RESOURCE_SUFFIX)) {
			return ContextConventionUtils.getRequiredPermissionFromMethodName(method.getName(), config);
		}
		return SecurityPermission.PERMISSION_WRITE;
	}
}
