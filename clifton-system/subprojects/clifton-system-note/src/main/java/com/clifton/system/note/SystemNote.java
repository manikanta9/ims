package com.clifton.system.note;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.document.DocumentFileAware;
import com.clifton.core.beans.document.DocumentFilePropertyAware;
import com.clifton.core.beans.document.DocumentWithRecordStampAware;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.document.DocumentModifyConditionAware;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyWithDeleteConditionAware;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.util.Date;
import java.util.List;


/**
 * The <code>SystemNote</code> ...
 *
 * @author Mary Anderson
 */
public class SystemNote extends BaseEntity<Integer> implements DocumentModifyConditionAware, DocumentWithRecordStampAware, DocumentFileAware, DocumentFilePropertyAware, SystemColumnCustomValueAware, SystemEntityModifyWithDeleteConditionAware {

	public static final String SYSTEM_NOTE_TABLE_NAME = "SystemNote";

	////////////////////////////////////////////////////////////////////////////////

	private SystemNoteType noteType;

	@SoftLinkField(tableBeanPropertyName = "noteType.table")
	private Long fkFieldId;

	/**
	 * For "linked" notes where their link is determined by a field on their table
	 * linkSystemManaged = false
	 * For example, PerformanceSummary notes are for the PerformanceSummary table but
	 * are linked through the client account field
	 * linkSystemManaged = true
	 * For example, Trade Notes tied to an individual trade, but also associated with it's security
	 * These cases have BOTH fkFieldId and linkedFkFieldId populated and on saves the system populates the linkedFkFieldId automatically
	 */
	@SoftLinkField(tableBeanPropertyName = "noteType.linkedTable")
	private Long linkedFkFieldId;

	/**
	 * For cases where users manually select multiple entities to link to a note
	 * where there may or may not be a common factor.
	 * For example, Trade Confirm notes for Trades - each file could apply to 1 or more trades
	 * so users can select the trades and create one note - manually linking those trades to the one note
	 * <p>
	 * The links are defined in the SystemNoteLink table
	 */
	private boolean linkedToMultiple;

	private Date startDate;
	private Date endDate;

	private boolean privateNote;

	private String text;

	private Integer order;

	/**
	 * Document Properties
	 */
	private String documentFileType;
	private String documentName;

	/**
	 * Document Record Stamp Properties
	 */
	private Date documentUpdateDate;
	private String documentUpdateUser;

	/**
	 * Custom Columns/Values
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabelShort() {
		if (getNoteType() != null) {
			return getNoteType().getName();
		}
		return null;
	}


	public String getLabel() {
		String result = getLabelShort();
		if (result != null && getText() != null) {
			result += ": " + getText();
		}
		return result;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getNoteType() != null) {
			return getNoteType().getEntityModifyCondition();
		}
		return null;
	}


	@Override
	public SystemCondition getEntityDeleteCondition() {
		if (getNoteType() != null) {
			return getNoteType().getEntityDeleteCondition();
		}
		return null;
	}


	public String getApplyTo() {
		if (getFkFieldId() != null) {
			return "SINGLE";
		}
		if (getLinkedFkFieldId() != null) {
			return "LINK-RELATIONSHIP";
		}
		if (isLinkedToMultiple()) {
			return "LINK";
		}
		return "GLOBAL";
	}


	public String getDocumentFileName() {
		if (!StringUtils.isEmpty(getDocumentName())) {
			return getDocumentName();
		}
		return getNoteType().getName();
	}


	public boolean isSingleAttachmentAllowed() {
		if (getNoteType() != null) {
			return getNoteType().isSingleAttachmentAllowed();
		}
		return false;
	}


	public boolean isMultipleAttachmentsAllowed() {
		if (getNoteType() != null) {
			return getNoteType().isMultipleAttachmentsAllowed();
		}
		return false;
	}


	@Override
	public SystemCondition getDocumentModifyCondition() {
		if (getNoteType() != null) {
			return getNoteType().getDocumentModifyCondition();
		}
		return null;
	}


	public boolean isInactive() {
		return !DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemNoteType getNoteType() {
		return this.noteType;
	}


	public void setNoteType(SystemNoteType noteType) {
		this.noteType = noteType;
	}


	public boolean isPrivateNote() {
		return this.privateNote;
	}


	public void setPrivateNote(boolean privateNote) {
		this.privateNote = privateNote;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Long getLinkedFkFieldId() {
		return this.linkedFkFieldId;
	}


	public void setLinkedFkFieldId(Long linkedFkFieldId) {
		this.linkedFkFieldId = linkedFkFieldId;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	@Override
	public String getDocumentFileType() {
		return this.documentFileType;
	}


	@Override
	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}


	@Override
	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	@Override
	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	@Override
	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	@Override
	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}


	public String getDocumentName() {
		return this.documentName;
	}


	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isLinkedToMultiple() {
		return this.linkedToMultiple;
	}


	public void setLinkedToMultiple(boolean linkedToMultiple) {
		this.linkedToMultiple = linkedToMultiple;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}
}
