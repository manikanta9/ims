package com.clifton.system.note.action;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteLink;
import com.clifton.system.note.SystemNoteType;


/**
 * The <code>SystemNoteActionHandler</code> interface must be implemented by system note actions
 * that can be performed on SystemNote dao event and SystemNoteLink dao events
 * <p/>
 * An action can represent any code that is executed after successful insert, update, or delete of the note or link
 *
 * @author Mary Anderson
 */
public interface SystemNoteActionHandler {


	/**
	 * When selecting the bean on the note type, this can be used to validate that what is supported is configured for the note type.
	 * For example, requiring one per entity, or no system managed links, etc.
	 */
	public void validateSystemNoteType(SystemNoteType noteType);


	/**
	 * Processes an action for specified note change
	 */
	public void processSystemNoteAction(SystemNote originalBean, SystemNote bean, DaoEventTypes event);


	/**
	 * Processes an action for specified note link change
	 */
	public void processSystemNoteLinkAction(SystemNoteLink originalBean, SystemNoteLink bean, DaoEventTypes event);
}
