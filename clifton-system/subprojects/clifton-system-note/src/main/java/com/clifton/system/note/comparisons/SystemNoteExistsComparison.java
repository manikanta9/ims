package com.clifton.system.note.comparisons;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>SystemNoteExistsComparison</code> class represents a comparison that checks if the specified bean
 * has any system notes associated with it.
 * <p>
 * Can optionally define note type name to check and condition for the note to pass, i.e. Note has a Document that Isn't Checked Out
 *
 * @author manderson
 */
public class SystemNoteExistsComparison implements Comparison<IdentityObject> {

	private DaoLocator daoLocator;

	private SystemConditionService systemConditionService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Use a comma delimited list of string to check
	 * across different note types
	 */
	private String noteTypeName;

	/**
	 * If populated - will run the selected condition on each note
	 * i.e. Note must have Document attached and Not be Checked Out
	 */
	private Integer noteSystemConditionId;

	/**
	 * Includes notes for this entity that are really tied to a linked entity.
	 * For example, viewing Security Notes on a Trade
	 * Some cases the Trade information itself is a note on the security not on each additional trade
	 */
	private boolean includeNotesForLinkedEntity;

	/**
	 * Will filter notes that are active "today" (can also be used to filter resolved vs. unresolved notes)
	 */
	private boolean activeNotesOnly;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Return false to reverse the comparison: check not exists
	 */
	protected boolean isExistsComparison() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		String tableName = getDaoLocator().locate((Class<IdentityObject>) bean.getClass()).getConfiguration().getTableName();
		ValidationUtils.assertNotNull(bean, "Cannot check system notes for a bean that does not exist: " + bean, "id");

		// get all notes that exist for the bean that match criteria
		SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
		searchForm.setEntityTableName(tableName);
		searchForm.setFkFieldId(BeanUtils.getIdentityAsLong(bean)); // Notes store values as Longs
		searchForm.setIncludeNotesForLinkedEntity(isIncludeNotesForLinkedEntity());
		if (!StringUtils.isEmpty(getNoteTypeName())) {
			String[] noteTypeNames = getNoteTypeName().split(",");
			if (noteTypeNames.length > 1) {
				searchForm.setNoteTypeNames(noteTypeNames);
			}
			else if (noteTypeNames.length == 1) {
				searchForm.setNoteTypeName(noteTypeNames[0]);
			}
		}
		if (isActiveNotesOnly()) {
			searchForm.setActiveOnDate(new Date());
		}

		SystemCondition noteCondition = null;
		if (getNoteSystemConditionId() != null) {
			noteCondition = getSystemConditionService().getSystemCondition(getNoteSystemConditionId());
			ValidationUtils.assertNotNull(noteCondition, "Unable to find System Condition with ID [" + getNoteSystemConditionId() + "] to evaluate for each note.");
		}

		List<SystemNote> noteList = getSystemNoteService().getSystemNoteListForEntity(searchForm);
		int count = 0;
		if (!CollectionUtils.isEmpty(noteList)) {
			if (noteCondition == null) {
				count = CollectionUtils.getSize(noteList);
			}
			else {
				for (SystemNote note : CollectionUtils.getIterable(noteList)) {
					if (getSystemConditionEvaluationHandler().isConditionTrue(noteCondition, note)) {
						count++;
					}
				}
			}
		}

		boolean result = (count > 0);
		if (!isExistsComparison()) {
			result = !result;
		}

		// record comparison result message
		if (context != null) {
			String msg = " System Notes Present" + (noteCondition == null ? ")" : " that passed condition [" + noteCondition.getName() + "])");
			if (!StringUtils.isEmpty(getNoteTypeName())) {
				msg = " [" + getNoteTypeName() + "]" + msg;
			}
			msg = "(" + (count == 0 ? "No" : count) + msg;
			if (result) {
				context.recordTrueMessage(msg);
			}
			else {
				context.recordFalseMessage(msg);
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////               Getter and Setter Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}


	public String getNoteTypeName() {
		return this.noteTypeName;
	}


	public void setNoteTypeName(String noteTypeName) {
		this.noteTypeName = noteTypeName;
	}


	public boolean isIncludeNotesForLinkedEntity() {
		return this.includeNotesForLinkedEntity;
	}


	public void setIncludeNotesForLinkedEntity(boolean includeNotesForLinkedEntity) {
		this.includeNotesForLinkedEntity = includeNotesForLinkedEntity;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public Integer getNoteSystemConditionId() {
		return this.noteSystemConditionId;
	}


	public void setNoteSystemConditionId(Integer noteSystemConditionId) {
		this.noteSystemConditionId = noteSystemConditionId;
	}


	public boolean isActiveNotesOnly() {
		return this.activeNotesOnly;
	}


	public void setActiveNotesOnly(boolean activeNotesOnly) {
		this.activeNotesOnly = activeNotesOnly;
	}
}
