package com.clifton.system.note.observers;


import com.clifton.core.beans.document.DocumentAttachmentSupportedTypes;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.note.action.SystemNoteActionHandler;
import com.clifton.system.note.search.SystemNoteSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>SystemNoteTypeObserver</code> handles validation of changing note types based on existing notes in the system
 * as well as applying those updates to notes in the system when allowed (Example - if note type is changed to internal, all of its notes are updated and flagged as private)
 *
 * @author Mary Anderson
 */
@Component
public class SystemNoteTypeObserver extends SelfRegisteringDaoObserver<SystemNoteType> {

	private SystemBeanService systemBeanService;

	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<SystemNoteType> dao, DaoEventTypes event, SystemNoteType bean) {
		SystemNoteType originalBean = null;

		if (event.isUpdate()) {
			originalBean = getOriginalBean(dao, bean);
		}

		// Validate
		validateSystemNoteType(bean, originalBean, event);

		if (originalBean != null && event.isUpdate()) {
			// If changed from not internal to internal - set all notes of this type to private
			if (bean.isInternal()) {
				if (!originalBean.isInternal()) {
					getSystemNoteService().saveSystemNotePrivate(bean.getId());
				}
			}

			if (!bean.isDateRangeAllowed() && originalBean.isDateRangeAllowed()) {
				SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
				searchForm.setNoteTypeId(bean.getId());
				searchForm.setStartOrEndDateNotNull(true);
				List<SystemNote> noteList = getSystemNoteService().getSystemNoteList(searchForm);
				if (!CollectionUtils.isEmpty(noteList)) {
					throw new ValidationException("Cannot change this note type to prohibit date ranges because there are currently [" + CollectionUtils.getSize(noteList)
							+ "] note(s) with start and/or end dates entered.");
				}
			}

			if (!bean.isLinkToMultipleAllowed() && originalBean.isLinkToMultipleAllowed()) {
				// ENSURE NO LINKED NOTES BEFORE TURNING THIS FEATURE OFF
				SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
				searchForm.setNoteTypeId(bean.getId());
				searchForm.setLinkedToMultiple(true);
				List<SystemNote> noteList = getSystemNoteService().getSystemNoteList(searchForm);
				if (!CollectionUtils.isEmpty(noteList)) {
					throw new ValidationException("Cannot change this note type to prohibit linking to multiple because there are currently [" + CollectionUtils.getSize(noteList)
							+ "] note(s) that are linked to multiple entities.");
				}
			}
		}
	}


	private void validateSystemNoteType(SystemNoteType bean, SystemNoteType originalBean, DaoEventTypes event) {
		// System Defined Validation - Applies to all note types
		validateSystemDefined(bean, originalBean, event);

		if (bean.isLinkSystemManaged()) {
			ValidationUtils.assertNotNull(bean.getLinkedTable(), "Linked Table and Relationships is required if note type is flagged as using System Managed Links", "linkedTable.name");
		}
		if (bean.getLinkedTable() != null) {
			ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getLinkedRelationshipLabel()), "Relationship Label is required if Linked Table is populated.");
			ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getLinkedRelationshipPath()), "Relationship Path is required if Linked Table is populated.");
		}
		if (DocumentAttachmentSupportedTypes.NONE == bean.getAttachmentSupportedType()) {
			bean.setDocumentModifyCondition(null);
		}
		// At this time, you can only change attachments supported from NONE to anything.
		// Any other changes can only be made if there aren't any notes yet for that note type.
		if (originalBean != null && originalBean.getAttachmentSupportedType() != bean.getAttachmentSupportedType()
				&& DocumentAttachmentSupportedTypes.NONE != originalBean.getAttachmentSupportedType()) {
			SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
			searchForm.setNoteTypeId(bean.getId());
			List<SystemNote> noteList = getSystemNoteService().getSystemNoteList(searchForm);
			if (!CollectionUtils.isEmpty(noteList)) {
				throw new ValidationException("You cannot change the attachments supported selection for this note type because notes already exist.");
			}
		}

		if (bean.isOnePerEntity()) {
			ValidationUtils.assertFalse(bean.isGlobalNoteAllowed(), "One Per Entity option is not an available option if the note type supports global notes.");
			ValidationUtils.assertFalse(bean.isLinkedNoteAllowed(), "One Per Entity option is not an available option if the note type supports linked relationship notes that aren't system managed.");
		}

		if (bean.isDateRangeAllowed()) {
			ValidationUtils.assertFalse(bean.isEndDateResolutionDate(),
					"Note types can either support date ranges or utilize end date as the resolution date, but not both.  Please de-select one of the options.");
		}

		if (bean.isLinkToMultipleAllowed()) {
			ValidationUtils.assertFalse(bean.isLinkedNoteAllowed(),
					"Linked to Multiple option is not an available option if the note type supports linked relationship notes that aren't system managed.");
		}

		if (bean.getNoteActionBean() != null) {
			SystemNoteActionHandler actionHandler = (SystemNoteActionHandler) getSystemBeanService().getBeanInstance(bean.getNoteActionBean());
			actionHandler.validateSystemNoteType(bean);
		}
	}


	private void validateSystemDefined(SystemNoteType bean, SystemNoteType originalBean, DaoEventTypes event) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (event.isDelete()) {
				throw new ValidationException("Deleting System Defined System Note Types is not allowed.");
			}
			if (event.isInsert()) {
				throw new ValidationException("Adding new System Defined System Note Types is not allowed.");
			}
		}
		if (event.isUpdate()) {
			if (originalBean.isSystemDefined() != bean.isSystemDefined()) {
				throw new FieldValidationException("You cannot edit the System Defined field for System Note Types", "systemDefined");
			}
			if (originalBean.isSystemDefined()) {
				ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()) && originalBean.getTable().getId().equals(bean.getTable().getId()),
						"System Defined System Note Types cannot be changed.", "name");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}
}
