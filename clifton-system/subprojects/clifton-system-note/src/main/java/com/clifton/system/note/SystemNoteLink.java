package com.clifton.system.note;


import com.clifton.core.beans.BaseEntity;
import com.clifton.system.usedby.softlink.SoftLinkField;


/**
 * The <code>SystemNoteLink</code> defines the relationship from a note
 * to the multiple entities it applies to.
 * <p>
 * For cases where users manually select multiple entities to link to a note
 * where there may or may not be a common factor.
 * For example, Trade Confirm notes for Trades - each file could apply to 1 or more trades
 * so users can select the trades and create one note - manually linking those trades to the one note
 *
 * @author manderson
 */
public class SystemNoteLink extends BaseEntity<Integer> {

	private SystemNote note;

	/**
	 * The ID field of the note.noteType.table that this note is linked to.
	 */
	@SoftLinkField(tableBeanPropertyName = "note.noteType.table", ownerBeanPropertyName = "note")
	private Long fkFieldId;

	/**
	 * USED ONLY WHEN linkSystemManaged = true
	 * For example, Trade Notes tied to an individual trade, but also associated with it's security
	 * These cases have BOTH fkFieldId and linkedFkFieldId populated and on saves the system populates the linkedFkFieldId automatically
	 */
	@SoftLinkField(tableBeanPropertyName = "note.noteType.linkedTable", ownerBeanPropertyName = "note")
	private Long linkedFkFieldId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemNote getNote() {
		return this.note;
	}


	public void setNote(SystemNote note) {
		this.note = note;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Long getLinkedFkFieldId() {
		return this.linkedFkFieldId;
	}


	public void setLinkedFkFieldId(Long linkedFkFieldId) {
		this.linkedFkFieldId = linkedFkFieldId;
	}
}
