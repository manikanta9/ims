package com.clifton.system.note.observers;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.note.search.SystemNoteTypeSearchForm;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemNoteAwareObserverRegistrator</code> class is a Spring {@link ApplicationListener} that
 * registers {@link SystemNoteAwareObserver} for DAO's whose table exists
 * in the {@link SystemNoteType} table
 * <p>
 * Note: This is only done once on application start up - not necessary to register/unregister on the fly, because if a new table is added to a note type then
 * we'd have code changes that would add the notes feature to the UI.
 *
 * @author manderson
 */
public class SystemNoteAwareObserverRegistrator<T extends IdentityObject> implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	private SystemNoteService systemNoteService;
	private SystemNoteAwareObserver<T> systemNoteAwareObserver;

	private DaoLocator daoLocator;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			List<String> tableNames = new ArrayList<>();
			List<SystemNoteType> noteTypeList = getSystemNoteService().getSystemNoteTypeList(new SystemNoteTypeSearchForm());
			for (SystemNoteType noteType : CollectionUtils.getIterable(noteTypeList)) {
				if (!tableNames.contains(noteType.getTable().getName())) {
					tableNames.add(noteType.getTable().getName());
				}
			}
			registerObservers(tableNames);
		}
		catch (Throwable e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error registering System Note Aware Observers: " + event, e);
		}
	}


	@SuppressWarnings("unchecked")
	public void registerObservers(List<String> tableNameList) {
		// register observer as necessary for each table
		for (String tableName : CollectionUtils.getIterable(tableNameList)) {
			ObserverableDAO<T> dao = (ObserverableDAO<T>) getDaoLocator().locate(tableName);
			dao.registerEventObserver(DaoEventTypes.DELETE, getSystemNoteAwareObserver());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}


	public SystemNoteAwareObserver<T> getSystemNoteAwareObserver() {
		return this.systemNoteAwareObserver;
	}


	public void setSystemNoteAwareObserver(SystemNoteAwareObserver<T> systemNoteAwareObserver) {
		this.systemNoteAwareObserver = systemNoteAwareObserver;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
