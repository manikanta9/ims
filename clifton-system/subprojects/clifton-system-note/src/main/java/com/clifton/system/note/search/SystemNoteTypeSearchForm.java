package com.clifton.system.note.search;


import com.clifton.core.beans.document.DocumentAttachmentSupportedTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;
import com.clifton.system.note.SystemNoteType;


/**
 * The <code>SystemNoteTypeSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class SystemNoteTypeSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchFieldPath = "table", searchField = "name")
	private String tableName;

	@SearchField(searchField = "table.id")
	private Short tableId;

	@SearchField(searchFieldPath = "linkedTable", searchField = "name")
	private String linkedTableName;

	@SearchField(searchField = "linkedTable.id")
	private Short linkedTableId;

	@SearchField(searchField = "linkedTable.name,table.name", leftJoin = true)
	private String tableOrLinkedTableName;

	@SearchField
	private String linkedRelationshipPath;

	@SearchField(searchField = "linkedRelationshipPath", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean linkedRelationshipPathUsed;

	@SearchField
	private String linkedRelationshipLabel;

	@SearchField
	private Boolean linkSystemManaged;

	@SearchField
	private Boolean systemDefined;

	//Custom Search Field = True = isLinkSystemManaged = false and LinkedSystemTable IS NOT NULL
	// False = isLinkedSystemManaged = true OR LinkedSystemTable IS NULL
	private Boolean linkedNoteAllowed;

	@SearchField
	private Boolean blankNoteAllowed;

	@SearchField(searchFieldPath = "entityModifyCondition", searchField = "name")
	private String entityModifyCondition;

	@SearchField(searchFieldPath = "entityDeleteCondition", searchField = "name")
	private String entityDeleteCondition;

	@SearchField(searchFieldPath = "noteActionBean", searchField = "name")
	private String noteActionBean;

	@SearchField
	private DocumentAttachmentSupportedTypes attachmentSupportedType;

	@SearchField(searchField = "attachmentSupportedType", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private DocumentAttachmentSupportedTypes excludeAttachmentSupportedType;

	@SearchField
	private Boolean dateRangeAllowed;

	@SearchField
	private Boolean globalNoteAllowed;

	@SearchField
	private Boolean orderSupported;

	@SearchField
	private Boolean linkToMultipleAllowed;

	@SearchField
	private Boolean internal;

	@SearchField(searchField = "defaultNoteText", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean defaultNoteTextPopulated;

	@SearchField
	private String defaultNoteText;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	// SystemHierarchyItemSearchForm
	@Override
	public String getDaoTableName() {
		return SystemNoteType.SYSTEM_NOTE_TYPE_TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getLinkedTableName() {
		return this.linkedTableName;
	}


	public void setLinkedTableName(String linkedTableName) {
		this.linkedTableName = linkedTableName;
	}


	public String getLinkedRelationshipPath() {
		return this.linkedRelationshipPath;
	}


	public void setLinkedRelationshipPath(String linkedRelationshipPath) {
		this.linkedRelationshipPath = linkedRelationshipPath;
	}


	public String getLinkedRelationshipLabel() {
		return this.linkedRelationshipLabel;
	}


	public void setLinkedRelationshipLabel(String linkedRelationshipLabel) {
		this.linkedRelationshipLabel = linkedRelationshipLabel;
	}


	public Boolean getLinkedNoteAllowed() {
		return this.linkedNoteAllowed;
	}


	public void setLinkedNoteAllowed(Boolean linkedNoteAllowed) {
		this.linkedNoteAllowed = linkedNoteAllowed;
	}


	public Boolean getGlobalNoteAllowed() {
		return this.globalNoteAllowed;
	}


	public void setGlobalNoteAllowed(Boolean globalNoteAllowed) {
		this.globalNoteAllowed = globalNoteAllowed;
	}


	public Boolean getOrderSupported() {
		return this.orderSupported;
	}


	public void setOrderSupported(Boolean orderSupported) {
		this.orderSupported = orderSupported;
	}


	public Boolean getInternal() {
		return this.internal;
	}


	public void setInternal(Boolean internal) {
		this.internal = internal;
	}


	public Boolean getDefaultNoteTextPopulated() {
		return this.defaultNoteTextPopulated;
	}


	public void setDefaultNoteTextPopulated(Boolean defaultNoteTextPopulated) {
		this.defaultNoteTextPopulated = defaultNoteTextPopulated;
	}


	public String getDefaultNoteText() {
		return this.defaultNoteText;
	}


	public void setDefaultNoteText(String defaultNoteText) {
		this.defaultNoteText = defaultNoteText;
	}


	public Boolean getDateRangeAllowed() {
		return this.dateRangeAllowed;
	}


	public void setDateRangeAllowed(Boolean dateRangeAllowed) {
		this.dateRangeAllowed = dateRangeAllowed;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public Short getLinkedTableId() {
		return this.linkedTableId;
	}


	public void setLinkedTableId(Short linkedTableId) {
		this.linkedTableId = linkedTableId;
	}


	public String getTableOrLinkedTableName() {
		return this.tableOrLinkedTableName;
	}


	public void setTableOrLinkedTableName(String tableOrLinkedTableName) {
		this.tableOrLinkedTableName = tableOrLinkedTableName;
	}


	public Boolean getBlankNoteAllowed() {
		return this.blankNoteAllowed;
	}


	public void setBlankNoteAllowed(Boolean blankNoteAllowed) {
		this.blankNoteAllowed = blankNoteAllowed;
	}


	public Boolean getLinkSystemManaged() {
		return this.linkSystemManaged;
	}


	public void setLinkSystemManaged(Boolean linkSystemManaged) {
		this.linkSystemManaged = linkSystemManaged;
	}


	public Boolean getLinkedRelationshipPathUsed() {
		return this.linkedRelationshipPathUsed;
	}


	public void setLinkedRelationshipPathUsed(Boolean linkedRelationshipPathUsed) {
		this.linkedRelationshipPathUsed = linkedRelationshipPathUsed;
	}


	public Boolean getLinkToMultipleAllowed() {
		return this.linkToMultipleAllowed;
	}


	public void setLinkToMultipleAllowed(Boolean linkToMultipleAllowed) {
		this.linkToMultipleAllowed = linkToMultipleAllowed;
	}


	public DocumentAttachmentSupportedTypes getAttachmentSupportedType() {
		return this.attachmentSupportedType;
	}


	public void setAttachmentSupportedType(DocumentAttachmentSupportedTypes attachmentSupportedType) {
		this.attachmentSupportedType = attachmentSupportedType;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public DocumentAttachmentSupportedTypes getExcludeAttachmentSupportedType() {
		return this.excludeAttachmentSupportedType;
	}


	public void setExcludeAttachmentSupportedType(DocumentAttachmentSupportedTypes excludeAttachmentSupportedType) {
		this.excludeAttachmentSupportedType = excludeAttachmentSupportedType;
	}


	public String getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(String entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public String getEntityDeleteCondition() {
		return this.entityDeleteCondition;
	}


	public void setEntityDeleteCondition(String entityDeleteCondition) {
		this.entityDeleteCondition = entityDeleteCondition;
	}


	public String getNoteActionBean() {
		return this.noteActionBean;
	}


	public void setNoteActionBean(String noteActionBean) {
		this.noteActionBean = noteActionBean;
	}
}
