package com.clifton.system.note.comparisons;


/**
 * The <code>SystemNoteNoteExistsComparison</code> class represents a comparison that checks if the specified bean
 * has not system notes associated with it that fit specified criteria.
 * <p/>
 * Can optionally define note type name to check and condition for the note to pass, i.e. Note has a Document that Isn't Checked Out
 *
 * @author manderson
 */
public class SystemNoteNotExistsComparison extends SystemNoteExistsComparison {

	@Override
	protected boolean isExistsComparison() {
		return false;
	}
}
