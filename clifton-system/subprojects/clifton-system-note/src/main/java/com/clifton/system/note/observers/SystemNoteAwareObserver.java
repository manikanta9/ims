package com.clifton.system.note.observers;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>SystemNoteAwareObserver</code> class is a {@link DaoEventObserver} that deleting system notes
 * or links to system notes when an entity is deleted
 *
 * @param <T>
 * @author manderson
 */
@Component
public class SystemNoteAwareObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private SystemNoteService systemNoteService;


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// Note: Only Registered for Deletes
		if (e == null) {
			SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
			searchForm.setEntityTableName(dao.getConfiguration().getTableName());
			Long beanId = BeanUtils.getIdentityAsLong(bean);
			searchForm.setFkFieldId(beanId);
			List<SystemNote> noteList = getSystemNoteService().getSystemNoteListForEntity(searchForm);
			for (SystemNote note : CollectionUtils.getIterable(noteList)) {
				if (note.isLinkedToMultiple()) {
					getSystemNoteService().deleteSystemNoteLink(note.getId(), beanId);
				}
				else {
					getSystemNoteService().deleteSystemNote(note.getId());
				}
			}
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// DO NOTHING
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}
}
