package com.clifton.system.note;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.beans.document.DocumentAttachmentSupportedTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyWithDeleteConditionAware;


/**
 * The <code>SystemNoteType</code> defines note type associated with a specific table.
 *
 * @author Mary Anderson
 */
public class SystemNoteType extends NamedEntity<Short> implements SystemEntityModifyWithDeleteConditionAware {

	public static final String SYSTEM_NOTE_TYPE_TABLE_NAME = "SystemNoteType";

	////////////////////////////////////////////////////////////////////////////////

	private SystemTable table;

	/**
	 * Link Properties
	 * linkSystemManaged = false
	 * Associate notes of this type with all entities of system table that match linked values
	 * i.e. Performance Summaries - apply notes to all summaries that match Client Account
	 * linkSystemManaged = true
	 * Notes are still directly tied to an entity, i.e. Trade, but if set then the system will ALSO
	 * set the LinkedFKFieldID based on the defined relationship, i.e Security ID
	 * This enables notes of this time to be viewed both from the Trade and the Security
	 */
	private boolean linkSystemManaged;
	private SystemTable linkedTable;
	private String linkedRelationshipPath;
	private String linkedRelationshipLabel;

	private Integer order;

	/**
	 * Notes, by default, are required to have some text
	 * Some note types may be used more for attaching files, so this option allows
	 * the note itself to be blank to avoid meaningless entry
	 */
	private boolean blankNoteAllowed;

	/**
	 * If true, then notes can be manually linked to multiple entities
	 * <p>
	 * For example, Trade Confirm notes for Trades - each file could apply to 1 or more trades
	 * so users can select the trades and create one note - manually linking those trades to the one note
	 */
	private boolean linkToMultipleAllowed;

	/**
	 * Can be selected for cases where Single Notes only or Manually Linked To Multiple, i.e. notes are specifically assigned to each entity.
	 * Enforces that there can be only one note type associated with an entity active at a time
	 */
	private boolean onePerEntity;

	/**
	 * When false, hides start/end date on note window(s)
	 */
	private boolean dateRangeAllowed;

	/**
	 * When date range isn't allowed, can use this option to still display the end date
	 * as a resolution date.  For example, "Trade Break" notes can be entered, but in order
	 * for the Trade to be reconciled those notes must have been "resolved"
	 */
	private boolean endDateResolutionDate;

	/**
	 * If note type should allow documents to be attached to notes of this type, supports
	 * None, Single (one document per note), or Multiple (attachments feature)
	 */
	private DocumentAttachmentSupportedTypes attachmentSupportedType;
	private SystemCondition documentModifyCondition;

	/**
	 * Optional modify condition that specifies who can create/edit notes of this type
	 * if entityDeleteCondition is blank, will also be used for evaluating deletes
	 */
	private SystemCondition entityModifyCondition;

	/**
	 * Optional modify condition that specifies who can delete notes of this type
	 */
	private SystemCondition entityDeleteCondition;

	/**
	 * If global notes can be created for this type.
	 * A global note is just tied to the table, with not FKFieldIDs, thus it would show up
	 * for all entities in that table.  For example, a common note that applies to all clients.
	 * Used rarely
	 */
	private boolean globalNoteAllowed;

	/**
	 * Internal note types drive if the note itself is just for internal purposes.  For example, a client
	 * account PIOS processing instructions.  These should be excluded from Client facing reports.  If
	 * the note type is internal, the private field doesn't apply on the note itself.
	 */
	private boolean internal;

	/**
	 * If note type is system defined the name and table cannot be changed.  This is because
	 * a system defined note will most likely be referenced by name in code or database.
	 */
	private boolean systemDefined;

	/**
	 * When the note type is selected for a new note, if this is populated the note itself
	 * would populate with this text.
	 */
	private String defaultNoteText;

	/**
	 * If the order field on each note should be viewable
	 */
	private boolean orderSupported;

	private Short maxNoteLength;


	/**
	 * If populated, when notes or links to notes of this type are inserted, updated, or deleted, the bean action is processed.
	 * This can be used to "do something" when notes are entered.
	 * Example: Trade Reconciliation Match tables have note fields that are auto-populated on specific trade note types being entered so they can be
	 * saved on the record as well for easier use in the grids.
	 */
	private SystemBean noteActionBean;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isLinkedRelationshipUsed() {
		return getLinkedTable() != null;
	}


	public boolean isLinkedNoteAllowed() {
		return !isLinkSystemManaged() && getLinkedTable() != null;
	}


	public boolean isDefaultNoteTextPopulated() {
		return !StringUtils.isEmpty(getDefaultNoteText());
	}


	public boolean isSingleAttachmentAllowed() {
		return DocumentAttachmentSupportedTypes.SINGLE == getAttachmentSupportedType();
	}


	public boolean isMultipleAttachmentsAllowed() {
		return DocumentAttachmentSupportedTypes.MULTIPLE == getAttachmentSupportedType();
	}

	////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public SystemTable getLinkedTable() {
		return this.linkedTable;
	}


	public void setLinkedTable(SystemTable linkedTable) {
		this.linkedTable = linkedTable;
	}


	public String getLinkedRelationshipPath() {
		return this.linkedRelationshipPath;
	}


	public void setLinkedRelationshipPath(String linkedRelationshipPath) {
		this.linkedRelationshipPath = linkedRelationshipPath;
	}


	public String getLinkedRelationshipLabel() {
		return this.linkedRelationshipLabel;
	}


	public void setLinkedRelationshipLabel(String linkedRelationshipLabel) {
		this.linkedRelationshipLabel = linkedRelationshipLabel;
	}


	public boolean isGlobalNoteAllowed() {
		return this.globalNoteAllowed;
	}


	public void setGlobalNoteAllowed(boolean globalNoteAllowed) {
		this.globalNoteAllowed = globalNoteAllowed;
	}


	public boolean isInternal() {
		return this.internal;
	}


	public void setInternal(boolean internal) {
		this.internal = internal;
	}


	public String getDefaultNoteText() {
		return this.defaultNoteText;
	}


	public void setDefaultNoteText(String defaultNoteText) {
		this.defaultNoteText = defaultNoteText;
	}


	public boolean isOrderSupported() {
		return this.orderSupported;
	}


	public void setOrderSupported(boolean orderSupported) {
		this.orderSupported = orderSupported;
	}


	public SystemCondition getDocumentModifyCondition() {
		return this.documentModifyCondition;
	}


	public void setDocumentModifyCondition(SystemCondition documentModifyCondition) {
		this.documentModifyCondition = documentModifyCondition;
	}


	public boolean isLinkToMultipleAllowed() {
		return this.linkToMultipleAllowed;
	}


	public void setLinkToMultipleAllowed(boolean linkToMultipleAllowed) {
		this.linkToMultipleAllowed = linkToMultipleAllowed;
	}


	public boolean isDateRangeAllowed() {
		return this.dateRangeAllowed;
	}


	public void setDateRangeAllowed(boolean dateRangeAllowed) {
		this.dateRangeAllowed = dateRangeAllowed;
	}


	public boolean isBlankNoteAllowed() {
		return this.blankNoteAllowed;
	}


	public void setBlankNoteAllowed(boolean blankNoteAllowed) {
		this.blankNoteAllowed = blankNoteAllowed;
	}


	public boolean isLinkSystemManaged() {
		return this.linkSystemManaged;
	}


	public void setLinkSystemManaged(boolean linkSystemManaged) {
		this.linkSystemManaged = linkSystemManaged;
	}


	public boolean isOnePerEntity() {
		return this.onePerEntity;
	}


	public void setOnePerEntity(boolean onePerEntity) {
		this.onePerEntity = onePerEntity;
	}


	public boolean isEndDateResolutionDate() {
		return this.endDateResolutionDate;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public void setEndDateResolutionDate(boolean endDateResolutionDate) {
		this.endDateResolutionDate = endDateResolutionDate;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	@Override
	public SystemCondition getEntityDeleteCondition() {
		return this.entityDeleteCondition;
	}


	public void setEntityDeleteCondition(SystemCondition entityDeleteCondition) {
		this.entityDeleteCondition = entityDeleteCondition;
	}


	public Short getMaxNoteLength() {
		return this.maxNoteLength;
	}


	public void setMaxNoteLength(Short maxNoteLength) {
		this.maxNoteLength = maxNoteLength;
	}


	public DocumentAttachmentSupportedTypes getAttachmentSupportedType() {
		return this.attachmentSupportedType;
	}


	public void setAttachmentSupportedType(DocumentAttachmentSupportedTypes attachmentSupportedType) {
		this.attachmentSupportedType = attachmentSupportedType;
	}


	public SystemBean getNoteActionBean() {
		return this.noteActionBean;
	}


	public void setNoteActionBean(SystemBean noteActionBean) {
		this.noteActionBean = noteActionBean;
	}
}
