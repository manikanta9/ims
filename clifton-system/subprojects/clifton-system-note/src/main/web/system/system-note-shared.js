Ext.ns('Clifton.system.note', 'Clifton.system.note.upload');


Clifton.system.schema.SimpleUploadWindows['SystemNote'] = 'Clifton.system.note.upload.SystemNoteUploadWindow';

Clifton.system.note.SupportAttachments = false; // Enabled in document-shared if attachments are used in end project


Clifton.system.note.NoteGridPanel = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	name: 'systemNoteListForEntityFind',
	remoteSort: true,
	instructions: 'The following system notes have been recorded for this entity.',
	groupField: 'noteType.name',
	// Note: Note Type.Name field has it's own group render that customizes this
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Notes" : "Note"]})',
	showGlobalNoteMenu: false,
	showAttachmentInfo: false,
	showOrderInfo: false,
	showInternalInfo: true,
	showPrivateInfo: true,
	showGlobalColumn: false,
	showCreateDate: false,
	showDisplayFilter: true,
	viewNames: ['Default', 'Attachment Info'],
	defaultActiveFilter: true,
	showFilterActiveOnDate: false,

	// copy functionality
	allowCopyFromLikeEntities: false,
	additionalCopyEntityParams: '', // override with a '?prop=x' string to restrict entitiies available to copy from in combo
	additionalCopySystemNoteParams: {}, // override with props object to restrict Notes that will be copied

	// Set to true to see notes for related entities - i.e. on Trades also see security notes
	includeNotesForLinkedEntity: false,
	// Set to true to see notes where used as related entity - i.e. on security also see all trade notes for that security
	includeNotesAsLinkedEntity: false,

	importTableName: 'SystemNote',
	importComponentName: 'Clifton.system.note.upload.SystemNoteUploadWindow',
	getImportComponentDefaultData: function() {
		const entity = this.getEntity();
		if (entity) {
			let lbl = entity.label;
			if (!lbl) {
				lbl = entity.name;
			}
			return {tableName: this.importTableName, fkFieldTableName: this.tableName, fkFieldId: entity.id, fkFieldIdLabel: lbl};
		}
	},

	getGroup: function(v, r, groupRenderer, rowIndex, colIndex, ds) {
		const gridTableName = this.grid.ownerGridPanel.tableName;
		const column = this.cm.config[colIndex];
		let g = groupRenderer ? groupRenderer.call(column.scope, v, gridTableName, r, rowIndex, colIndex, ds) : String(v);
		if (g === '' || g === '&#160;') {
			g = column.emptyGroupText || this.emptyGroupText;
		}
		return g;
	},

	additionalPropertiesToRequest: 'id|noteType.attachmentSupportedType',

	columns: [
		{header: 'Apply To', width: 50, dataIndex: 'applyTo', hidden: true},
		{header: 'Table', width: 100, dataIndex: 'noteType.table.name', hidden: true},
		{header: 'Linked Table', width: 100, dataIndex: 'noteType.linkedTable.name', hidden: true},
		{
			header: 'Type', width: 100, dataIndex: 'noteType.name', hidden: true,
			groupRenderer: function(v, gridTableName, r, rowIndex, colIndex, ds) {
				const tbName = r.data['noteType.table.name'];
				if (tbName !== gridTableName) {
					return v + ' [' + tbName + ']';
				}
				return v;
			}
		},
		{header: 'Order', width: 30, dataIndex: 'order', type: 'int', useNull: true},
		{header: 'End Date Is Resolution', width: 35, dataIndex: 'noteType.endDateResolutionDate', type: 'boolean', hidden: true},
		{
			header: 'Note', width: 300, dataIndex: 'text',
			renderer: function(v, args, r) {
				if (r.data['noteType.endDateResolutionDate'] === true) {
					if (TCG.isNotBlank(r.data.endDate)) {
						v = '<font color="green"><b>[Resolved: ' + r.data.endDate.format('m/d/Y') + ']</b></font>&nbsp;' + v;
					}
					else {
						v = '<font color="red"><b>[Unresolved]</b></font>&nbsp;' + v;
					}
				}

				return v;
			}
		},
		{header: 'FKFieldID', width: 10, dataIndex: 'fkFieldId', hidden: true, sortable: false, filter: false},
		{header: 'LinkedFKFieldID', width: 10, dataIndex: 'linkedFkFieldId', hidden: true, sortable: false, filter: false},
		{header: 'FKFieldIDScreen', width: 45, dataIndex: 'noteType.table.detailScreenClass', hidden: true, sortable: false, filter: false},
		{header: 'LinkedFKFieldIDScreen', width: 45, dataIndex: 'noteType.linkedTable.detailScreenClass', hidden: true, sortable: false, filter: false},
		{
			header: 'View', width: 45, dataIndex: '', hidden: true, align: 'center', sortable: false, filter: false,
			renderer: function(v, args, r) {
				let clz = r.data['noteType.table.detailScreenClass'];
				let idVal = r.data.fkFieldId;
				let type = 'ShowEntity';
				let tbl = r.data['noteType.table.name'];
				// If Table is the same - show the link if available
				// Not sure if this is the best way to get back to find the table name
				if (tbl === r.store.baseParams.entityTableName) {
					if (r.data['noteType.linkedTable.name'] !== r.store.baseParams.entityTableName) {
						clz = r.data['noteType.linkedTable.detailScreenClass'];
						tbl = r.data['noteType.linkedTable.name'];
						idVal = r.data.linkedFkFieldId;
						type = 'ShowLink';
					}
				}
				if (TCG.isNotBlank(clz) && TCG.isNotBlank(idVal)) {
					return TCG.renderActionColumn('view', 'View', 'View [' + tbl + '] entity for this note.', type);
				}
				return 'N/A';
			}
		},

		{header: 'Internal', width: 35, dataIndex: 'noteType.internal', type: 'boolean', filter: {searchFieldName: 'internal'}},
		{header: 'Private', width: 35, dataIndex: 'privateNote', type: 'boolean'},
		{header: 'Start Date', width: 35, dataIndex: 'startDate', hidden: true, filter: {orNull: true}},
		{header: 'End Date', width: 35, dataIndex: 'endDate', hidden: true, filter: {orNull: true}},
		{
			header: 'Global', width: 35, dataIndex: 'global', type: 'boolean',
			renderer: function(v, args, r) {
				return TCG.renderBoolean(TCG.isEquals(r.data['applyTo'], 'GLOBAL'));
			}
		},
		{header: 'Inactive', width: 35, dataIndex: 'inactive', type: 'boolean'},
		{header: 'Linked to Multiple', width: 35, dataIndex: 'linkedToMultiple', type: 'boolean', hidden: true},
		{
			header: 'Format', width: 30, dataIndex: 'documentFileType', filter: false, align: 'center', hidden: true, viewNames: ['Attachment Info'],
			renderer: function(ft, args, r) {
				return TCG.renderActionColumn(TCG.getDocumentFileTypeIconClass(ft), '', 'View document (' + ft + ')', 'ViewFile');
			}
		},
		{header: 'Doc Updated On', width: 65, dataIndex: 'documentUpdateDate', hidden: true, viewNames: ['Attachment Info']},
		{header: 'Doc Updated By', width: 55, dataIndex: 'documentUpdateUser', hidden: true, viewNames: ['Attachment Info']}
	],
	initComponent: function() {
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);

		// Default presentation
		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('order'), (this.showOrderInfo === false));
		cm.setHidden(cm.findColumnIndex('privateNote'), (this.showPrivateInfo === false));
		cm.setHidden(cm.findColumnIndex('global'), (this.showGlobalColumn === false));
		cm.setHidden(cm.findColumnIndex('noteType.internal'), (this.showInternalInfo === false));
		cm.setHidden(cm.findColumnIndex('documentFileType'), this.showAttachmentInfo === false);
		cm.setHidden(cm.findColumnIndex('createDate'), this.showCreateDate === false);

		const showLinks = (this.includeNotesForLinkedEntity === true || this.includeNotesAsLinkedEntity === true);
		cm.setHidden(cm.findColumnIndex('View'), !showLinks);
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		const noteTypeUrl = 'systemNoteTypeListFind.json?tableOrLinkedTableName=' + this.tableName;
		filters.push({fieldLabel: 'Note Type', xtype: 'toolbar-combo', name: 'noteType', width: 150, minListWidth: 200, url: noteTypeUrl});
		if (this.showDisplayFilter) {
			filters.push({
				fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 150, minListWidth: 150, mode: 'local', emptyText: '< All Notes >', displayField: 'name', valueField: 'value',
				store: new Ext.data.ArrayStore({
					fields: ['value', 'name', 'description'],
					data: [['ALL', 'Display All Notes'], ['INTERNAL', 'Internal Notes Only', 'Display internal notes only'], ['PRIVATE', 'Private Notes Only', 'Display private notes only'], ['PUBLIC', 'Not Private and Not Internal Notes Only', 'Display not private and not internal notes only']]
				})
			});
		}
		if (this.showFilterActiveOnDate === true) {
			// Assume date returned is formatted already
			const dfDate = this.getDefaultActiveOnDate();
			filters.push({fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate', value: dfDate});
		}
		return filters;
	},
	getDefaultActiveOnDate: function() {
		return new Date().format('m/d/Y');
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad && this.defaultActiveFilter === true) {
			// default to active
			this.setFilterValue('inactive', false);
		}

		let dType = 'ALL';
		if (this.showDisplayFilter) {
			const dt = TCG.getChildByName(this.getTopToolbar(), 'displayType');
			if (dt !== null) {
				dType = dt.getValue();
			}
		}
		if (dType === 'ALL') {
			this.clearFilter('privateNote');
			this.clearFilter('noteType.internal');
		}
		if (dType === 'INTERNAL') {
			this.clearFilter('privateNote');
			this.setFilterValue('noteType.internal', true);
		}
		else if (dType === 'PRIVATE') {
			this.clearFilter('noteType.internal');
			this.setFilterValue('privateNote', true);
		}
		else if (dType === 'PUBLIC') {
			this.setFilterValue('privateNote', false);
			this.setFilterValue('noteType.internal', false);
		}

		const loadParams = {
			entityTableName: this.tableName,
			fkFieldId: this.getEntityId()
		};

		const noteTypeCombo = TCG.getChildByName(this.getTopToolbar(), 'noteType');
		if (noteTypeCombo && TCG.isNotBlank(noteTypeCombo.getValue())) {
			loadParams.noteTypeId = noteTypeCombo.getValue();
		}

		const activeDateFilter = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
		if (activeDateFilter && TCG.isNotBlank(activeDateFilter.getValue())) {
			loadParams.activeOnDate = activeDateFilter.getValue().format('m/d/Y');
		}

		loadParams.includeNotesForLinkedEntity = this.includeNotesForLinkedEntity;
		loadParams.includeNotesAsLinkedEntity = this.includeNotesAsLinkedEntity;

		return this.applyAdditionalLoadParams(firstLoad, loadParams);
	},

	// Allows overrides to add additional load params or set additional filters
	applyAdditionalLoadParams: function(firstLoad, params) {
		return params;
	},

	clearFilter: function(dataIndex) {
		const f = this.grid.filters;
		f.getFilter(dataIndex).setActive(false);
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},
	getEntity: function() {
		return this.getWindow().getMainForm().formValues;
	},
	getLinkedEntityId: function() {
		return TCG.getValue(this.linkedRelationshipPath, this.getEntity());
	},

	addToolbarButtons: function(toolBar, gridPanel) {
		if(gridPanel.allowCopyFromLikeEntities===true){
			toolBar.add({
				text: 'Copy Notes',
				tooltip: 'Copy Notes From Like Entity',
				iconCls: 'copy',
				scope: this,
				handler: function() {
					const table = TCG.data.getData('systemTableByName.json?tableName=' + gridPanel.tableName, this, 'system.table.' + gridPanel.tableName);
					const url = table.entityListUrl + this.additionalCopyEntityParams;
					TCG.createComponent('TCG.app.OKCancelWindow',
						{
						title: 'Copy Notes to ' + this.getWindow().title,
						iconCls: 'copy',
						height: 160,
						width: 600,
						modal: true,
						scope:this,
						items: [{
							xtype: 'formpanel',
							loadValidation: false,
							instructions: 'Copies all notes from the selected entity to the current entity.',
							items: [
								{fieldLabel: 'Copy From', name: 'label', xtype: 'combo', hiddenName: 'id', displayField: 'label', url: url, allowBlank: false},
							]
						}],
						saveWindow: function(closeOnSuccess, forceSubmit) {
							const params = this.scope.additionalCopySystemNoteParams;
							params.entityTableName = this.scope.tableName;
							params.fkFieldId = this.getMainFormPanel().getFormValue('id');
							params.toEntityId = this.scope.getEntityId();
							const loader = new TCG.data.JsonLoader({
								waitTarget: this,
								waitMsg: 'Saving...',
								params:params,
								onLoad: function(record, conf) {
									gridPanel.reload();
								}
							});
							loader.load('systemNoteListCopy.json');
							this.closeWindow();
						},
						openerCt: gridPanel.ownerCt
					});
				}
			});
			toolBar.add('-');
		}
		if (gridPanel.showAttachmentInfo === true) {
			toolBar.add({
				text: 'View',
				tooltip: 'View file for selected note',
				iconCls: 'view',
				scope: this,
				handler: function() {
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a note to be downloaded.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection downloads are not supported yet.  Please select one row.', 'NOT SUPPORTED');
					}
					else {
						if (sm.getSelected().json.noteType.attachmentSupportedType === 'MULTIPLE') {
							TCG.data.getDataPromise('documentFileListFind.json?definitionName=SystemNoteAttachments&fkFieldId=' + sm.getSelected().id, this)
								.then(function(fileList) {
									if (fileList) {
										if (fileList.length === 1) {
											Clifton.document.DownloadDocument('DocumentFile', grid, fileList[0].id);
										}
										else if (fileList.length === 0) {
											TCG.showError('There are no files associated with the selected note');
										}
										else {
											TCG.showError('There are multiple files associated with the selected note, please drill into the note and select what you want to download.');
										}
									}
								});
						}
						else {
							Clifton.document.DownloadDocument('SystemNote', grid, sm.getSelected().id);
						}
					}
				}
			});
			toolBar.add('-');
		}
	},
	editor: {
		deleteURL: 'systemNoteDelete.json',
		addToolbarAddButton: function(toolBar) {
			const menu = new Ext.menu.Menu();
			const gp = this.getGridPanel();
			const editor = this;

			// Single Note
			menu.add({
				scope: this,
				text: 'Single ' + gp.tableName + ' Note',
				handler: function() {
					editor.openDetailPage('Clifton.system.note.SingleNoteWindow', gp);
				}
			});

			// Loop through Linked Options
			const loader = new TCG.data.JsonLoader({
				waitTarget: gp,
				waitMsg: 'Loading linked tables...',
				params: {tableName: gp.tableName, linkedNoteAllowed: true},
				onLoad: function(records, conf) {
					const linkedAdded = {};
					for (let i = 0; i < records.length; i++) {
						const record = records[i];
						if (!linkedAdded[record.linkedRelationshipLabel]) {
							menu.add({
								scope: this,
								text: 'Linked ' + record.linkedRelationshipLabel + ' Note',
								record: record,
								handler: function() {
									gp.linkedRelationshipLabel = record.linkedRelationshipLabel;
									gp.linkedRelationshipPath = record.linkedRelationshipPath;
									editor.openDetailPage('Clifton.system.note.LinkedRelationshipNoteWindow', gp);
								}
							});
							linkedAdded[record.linkedRelationshipLabel] = record.linkedRelationshipPath;
						}
					}
				}
			});
			loader.load('systemNoteTypeListFind.json');

			// Global Note
			if (gp.showGlobalNoteMenu === true) {
				menu.add({
					scope: this,
					text: 'Global ' + gp.tableName + ' Note',
					handler: function() {
						editor.openDetailPage('Clifton.system.note.GlobalNoteWindow', gp);
					}
				});
			}

			toolBar.add({
				text: 'Add',
				tooltip: 'Add a new item',
				iconCls: 'add',
				xtype: 'splitbutton',
				menu: menu,
				scope: this,
				handler: function() {
					editor.openDetailPage('Clifton.system.note.SingleNoteWindow', gp);
				}
			});
			toolBar.add('-');
		},
		addToolbarDeleteButton: function(toolBar) {
			toolBar.add({
				text: 'Remove',
				tooltip: 'Remove selected note',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					const editor = this;
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select note to be deleted.', 'No Note Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection deletes are not supported yet.  Please select one note.', 'NOT SUPPORTED');
					}
					else {
						const e = sm.getSelected();
						const linkedMultiple = TCG.getValue('json.linkedToMultiple', e);
						const fkFieldId = this.getGridPanel().getWindow().getMainFormId();
						if (linkedMultiple === true) {
							// note linked to multiple entities
							TCG.createComponent('TCG.app.OKCancelWindow', {
								title: 'Delete Linked Note',
								iconCls: 'calendar',
								height: 220,
								width: 500,
								modal: true,

								items: [{
									xtype: 'formpanel',
									loadValidation: false,
									instructions: 'This note is linked to multiple entities.<br />Do you want to delete this note and all links to this note or just this individual link to the note?',
									items: [
										{
											xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'deleteType',
											items: [
												{boxLabel: 'Delete this link to the note.', xtype: 'radio', name: 'deleteType', inputValue: 'LINK', checked: true},
												{boxLabel: 'Delete the note completely and all links to it.', xtype: 'radio', name: 'deleteType', inputValue: 'NOTE'}
											]
										}
									]
								}],
								saveWindow: function(closeOnSuccess, forceSubmit) {
									const fp = this.getMainFormPanel();
									const dt = fp.getForm().findField('deleteType').getValue().inputValue;
									if (dt === 'NOTE') {
										const loader = new TCG.data.JsonLoader({
											waitTarget: grid.ownerCt,
											waitMsg: 'Deleting...',
											params: {id: sm.getSelected().id},
											conf: {rowId: sm.getSelected().id},
											onLoad: function(record, conf) {
												grid.ownerCt.reload();
											}
										});
										loader.load(editor.getDeleteURL());
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: grid.ownerCt,
											waitMsg: 'Deleting...',
											params: {noteId: sm.getSelected().id, fkFieldId: fkFieldId},
											conf: {rowId: sm.getSelected().id},
											onLoad: function(record, conf) {
												grid.ownerCt.reload();
											}
										});
										loader.load('systemNoteLinkDelete.json');
									}
									this.closeWindow();
								},
								openerCt: grid.ownerCt
							});
						}
						else { // non-linked note
							Ext.Msg.confirm('Delete Selected Note', 'Would you like to delete selected note?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid.ownerCt,
										waitMsg: 'Deleting...',
										params: editor.getDeleteParams(sm),
										conf: {rowId: sm.getSelected().id},
										onLoad: function(record, conf) {
											grid.getStore().remove(sm.getSelected());
											grid.ownerCt.updateCount();
										}
									});
									loader.load(editor.getDeleteURL());
								}
							});
						}
					}
				}
			});
			toolBar.add('-');
		},
		detailPageClass: {
			getItemText: function(rowItem) {
				return (rowItem.get('applyTo'));
			},
			items: [
				{text: 'SINGLE', iconCls: 'pencil', className: 'Clifton.system.note.SingleNoteWindow'},
				{text: 'LINK-RELATIONSHIP', iconCls: 'pencil', className: 'Clifton.system.note.LinkedRelationshipNoteWindow'},
				{text: 'GLOBAL', iconCls: 'pencil', className: 'Clifton.system.note.GlobalNoteWindow'},
				{text: 'LINK', iconCls: 'pencil', className: 'Clifton.system.note.SingleNoteWindow'}
			]
		},
		getDefaultData: function(gridPanel, row, className) {
			const tbl = TCG.data.getData('systemTableByName.json?tableName=' + gridPanel.tableName, gridPanel, 'system.table.' + gridPanel.tableName);
			if (className === 'Clifton.system.note.SingleNoteWindow') {
				return {
					noteType: {table: tbl},
					fkFieldId: gridPanel.getEntityId()
				};
			}
			else if (className === 'Clifton.system.note.LinkedRelationshipNoteWindow') {

				return {
					noteType: {table: tbl, linkedRelationshipPath: gridPanel.linkedRelationshipPath, linkedRelationshipLabel: gridPanel.linkedRelationshipLabel},
					linkedFkFieldId: gridPanel.getLinkedEntityId()
				};
			}
			return {
				noteType: {table: tbl}
			};
		}
	},

	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];

					if (eventName === 'ShowEntity' || eventName === 'ShowLink') {
						let clz = row.json.noteType.table.detailScreenClass;
						let id = row.json.fkFieldId;

						if (eventName === 'ShowLink') {
							clz = row.json.noteType.linkedTable.detailScreenClass;
							id = row.json.linkedFkFieldId;
						}
						const gridPanel = grid.ownerGridPanel;
						gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
					}
					if (eventName === 'ViewFile') {
						if (row.json.noteType.attachmentSupportedType === 'MULTIPLE') {
							TCG.data.getDataPromise('documentFileListFind.json?definitionName=SystemNoteAttachments&fkFieldId=' + row.json.id, grid)
								.then(function(fileList) {
									if (fileList) {
										if (fileList.length === 1) {
											Clifton.document.DownloadDocument('DocumentFile', grid, fileList[0].id);
										}
										else if (fileList.length === 0) {
											TCG.showError('There are no files associated with the selected note');
										}
										else {
											TCG.showError('There are multiple files associated with the selected note, please drill into the note and select what you want to download.');
										}
									}
								});
						}
						else {
							Clifton.document.DownloadDocument('SystemNote', grid, row.json.id);
						}
					}
				}
			}
		}
	}
});
Ext.reg('system-note-grid', Clifton.system.note.NoteGridPanel);


Clifton.system.note.NoteWindowBase = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Note',
	iconCls: 'pencil',
	height: 650,
	width: 700,
	enableRefreshWindow: true,
	url: 'systemNote.json', // Note: This needs to be here so that the enableRefreshWindow knows what URL to use to reload.

	// Defaults for Note Type add and lookup
	globalNoteAllowed: false,
	linkedNoteAllowed: false,
	linkToMultipleAllowed: false, // Set to true on LinkedNoteWindow to support adding linked to multiple notes with fkFieldIds populated immediately

	showUsedByTab: true,

	resetNoteTypeFieldsForAttachments: function(formpanel, record) {
		// Do Nothing, overridden in Document Project
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		doNotDisableAfterLoad: true,


		items: [{
			title: 'Note',
			items: [{
				xtype: 'formpanel-custom-fields',
				columnGroupName: 'System Note Custom Fields',
				loadDefaultDataAfterRender: true,

				getInstructions: function() {
					if (this.getWindow().instructions) {
						return this.getWindow().instructions + this.genericInstructions;
					}
					return this.genericInstructions;
				},
				genericInstructions: 'Private notes are considered to be "Internal" notes that should not be included in any client facing reports.  Inactive notes are those that may no longer apply, but they should not be deleted.',

				url: 'systemNote.json',
				getSaveURL: function() {
					if (this.getWindow().noteSaveUrl) {
						return this.getWindow().noteSaveUrl;
					}
					return 'systemNoteSave.json';
				},

				appendSubmitParams: function(submitParams) {
					const data = this.getWindow().defaultData;
					if (data.fkFieldIds) {
						if (!submitParams) {
							submitParams = {};
						}
						submitParams.fkFieldIds = data.fkFieldIds;
					}
					return submitParams;
				},
				labelFieldName: 'noteType.name',
				defaultDataIsReal: true,
				getWarningMessage: function(form) {
					let msg = undefined;
					if (TCG.isFalse(form.formValues.privateNote)) {
						msg = 'This note is available for viewing on client facing reports.  To restrict viewing to Parametric employees only, please check the <i>Private Note</i> checkbox.';
					}
					return msg;
				},

				initComponent: function() {
					if (this.getWindow().tabToolbar) {
						this.ownerCt.tbar = this.getWindow().tabToolbar;
					}
					this.items = this.getFormItems();
					TCG.form.FormPanel.superclass.initComponent.call(this);
				},

				getFormItems: function() {
					const currentItems = [];

					if (this.getWindow().additionalNoteItems) {
						Ext.each(this.getWindow().additionalNoteItems, function(f) {
							currentItems.push(f);
						});
					}

					Ext.each(this.noteItems, function(f) {
						currentItems.push(f);
					});

					if (this.getWindow().attachmentItems) {
						Ext.each(this.getWindow().attachmentItems, function(f) {
							currentItems.push(f);
						});
					}

					currentItems.push({xtype: 'label', html: '<hr/>'});
					return currentItems;
				},

				linksTab: {
					title: 'Links',
					items: [{
						xtype: 'gridpanel',
						name: 'systemNoteLinkedEntityListByNote',
						instructions: 'This note has been manually linked to the following entities.  If nothing appears here then the note has only been associated with one entity.',
						columns: [
							{header: 'ID', dataIndex: 'drillDownId', hidden: true},
							{header: 'Entity', dataIndex: 'label'},
							{header: 'Table Class', width: 40, dataIndex: 'drillDownTable.detailScreenClass', hidden: true}
						],
						editor: {
							deleteURL: 'systemNoteLinkDelete.json',
							getDeleteParams: function(selectionModel) {
								return {
									noteId: this.getWindow().getMainFormId(),
									fkFieldId: selectionModel.getSelected().get('drillDownId')
								};
							},

							getDetailPageClass: function(grid, row) {
								return row.data['drillDownTable.detailScreenClass'];
							},
							getDetailPageId: function(grid, row) {
								return row.data.drillDownId;
							},

							addToolbarAddButton: function(toolBar) {
								const gridPanel = this.getGridPanel();
								const noteId = gridPanel.getWindow().getMainFormId();
								const noteTypeId = TCG.getValue('noteType.id', gridPanel.getWindow().getMainForm().formValues);
								const noteType = TCG.data.getData('systemNoteType.json?id=' + noteTypeId, gridPanel, 'system.note.type.' + noteTypeId);

								const listUrl = noteType.table.entityListUrl;
								const tbl = noteType.table.name;
								if (listUrl) {
									toolBar.add(new TCG.form.ComboBox({name: 'entity', url: listUrl, displayField: 'label', emptyText: '< Select ' + tbl + ' >', width: 200, listWidth: 600}));
									toolBar.add({
										text: 'Add',
										tooltip: 'Link selected entity to this note.',
										iconCls: 'add',
										handler: function() {
											const fkFieldId = TCG.getChildByName(toolBar, 'entity').getValue();
											if (TCG.isBlank(fkFieldId)) {
												TCG.showError('You must first select desired entity from the list.');
											}
											else {
												const loader = new TCG.data.JsonLoader({
													waitTarget: gridPanel,
													waitMsg: 'Linking...',
													params: {noteId: noteId, fkFieldId: fkFieldId},
													onLoad: function(record, conf) {
														gridPanel.reload();
														TCG.getChildByName(toolBar, 'entity').reset();
													}
												});
												loader.load('systemNoteLinkSave.json');
											}
										}
									});
									toolBar.add('-');
								}
								else {
									TCG.showError('Cannot add new links - Missing list url on [' + tbl + '] table.');
								}
							}
						},
						getLoadParams: function() {
							return {noteId: this.getWindow().getMainFormId()};
						},

						gridConfig: {
							listeners: {
								'rowclick': function(grid, rowIndex, evt) {
									if (TCG.isActionColumn(evt.target)) {
										const row = grid.store.data.items[rowIndex];
										const clz = row.json.drillDownTable.detailScreenClass;
										const id = row.json.drillDownId;
										const gridPanel = grid.ownerGridPanel;
										const cmpId = TCG.getComponentId(clz, id);
										TCG.createComponent(clz, {
											id: cmpId,
											params: {id: id},
											openerCt: gridPanel
										});
									}
								}
							}
						}
					}]
				},


				noteItems: [

					{name: 'columnGroupName', xtype: 'hidden', value: 'System Note Custom Fields'}, // I think because of items dynamically being added this doesn't appear to populate, so manually setting for now
					{fieldLabel: 'Table', name: 'noteType.table.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow', detailIdField: 'noteType.table.id', submitDetailField: false},
					{
						fieldLabel: 'Note Type', name: 'noteType.name', hiddenName: 'noteType.id', xtype: 'combo', detailIdField: 'noteType.id', detailPageClass: 'Clifton.system.note.TypeWindow', url: 'systemNoteTypeListFind.json',
						requiredFields: ['noteType.table.name'],
						beforequery: function(queryEvent) {
							const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
							if (TCG.isTrue(queryEvent.combo.getWindow().globalNoteAllowed)) {
								queryEvent.combo.store.baseParams = {tableName: TCG.getValue('noteType.table.name', form.formValues), globalNoteAllowed: true};
							}
							else if (TCG.isTrue(queryEvent.combo.getWindow().linkedNoteAllowed)) {
								queryEvent.combo.store.baseParams = {tableName: TCG.getValue('noteType.table.name', form.formValues), linkedNoteAllowed: true};
							}
							else if (TCG.isTrue(queryEvent.combo.getWindow().linkToMultipleAllowed)) {
								queryEvent.combo.store.baseParams = {tableName: TCG.getValue('noteType.table.name', form.formValues), linkToMultipleAllowed: true};
							}
							else {
								queryEvent.combo.store.baseParams = {tableName: TCG.getValue('noteType.table.name', form.formValues)};
							}
						},
						getDefaultData: function(f) { // defaults table for "add new" drill down
							return {
								table: TCG.getValue('noteType.table', f.formValues),
								globalNoteAllowed: this.getWindow().globalNoteAllowed,
								linkedNoteAllowed: this.getWindow().linkedNoteAllowed
							};

						},
						// reset document related fields and toolbar based on selection
						selectHandler: function(combo, record, index) {
							this.ownerCt.resetNoteTypeFields(record.json);
						}

					},
					{fieldLabel: 'Note Text', name: 'text', xtype: 'textarea', height: 60, grow: true},

					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', allowNegative: false},
					{name: 'privateNote', xtype: 'checkbox', boxLabel: 'Private Note (intended to be viewed by internal employees only)'},
					{fieldLabel: 'Start Date', xtype: 'datefield', name: 'startDate'},
					{fieldLabel: 'End Date', xtype: 'datefield', name: 'endDate'}
				],
				listeners: {
					afterload: function(form, isClosing) {
						this.resetNoteTypeFields();
					}
				},

				resetNoteTypeFields: function(record) {
					let originalLoad = false;
					const fp = this;
					const w = fp.getWindow();
					const tabs = w.items.get(0);
					const form = fp.getForm();

					if (!record) {
						originalLoad = true;
						record = TCG.getValue('noteType', form.formValues);
					}

					if (record) {
						w.resetNoteTypeFieldsForAttachments(fp, record);

						const orderField = form.findField('order');
						const orderSupported = record.orderSupported;
						orderField.setDisabled(!orderSupported);
						orderField.setVisible(orderSupported);
						if (!orderSupported) {
							orderField.setValue('');
						}

						const privateField = form.findField('privateNote');
						if (record.internal === true) {
							privateField.setValue(true);
							privateField.setDisabled(true);
						}
						else {
							privateField.setDisabled(false);
						}

						// Show/Hide Links Tab
						const linkTab = w.getTabPosition('Links');
						if (linkTab === 0 && record.linkToMultipleAllowed) {
							tabs.insert(1, this.linksTab);
						}
						else if (linkTab !== 0 && !record.linkToMultipleAllowed) {
							tabs.remove(linkTab);
						}

						const startDateField = form.findField('startDate');
						startDateField.setDisabled(!record.dateRangeAllowed);
						startDateField.setVisible(record.dateRangeAllowed);

						const endDateField = form.findField('endDate');
						if (record.dateRangeAllowed === true || record.endDateResolutionDate === true) {
							endDateField.setDisabled(false);
							endDateField.setVisible(true);
							let lbl = 'End Date:';
							if (record.endDateResolutionDate === true) {
								lbl = 'Resolution Date: ';
							}
							endDateField.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(lbl);
						}
						else {
							endDateField.setValue('');
							endDateField.setDisabled(true);
							endDateField.setVisible(false);
						}

						const noteTextField = form.findField('text');
						if (!originalLoad) {
							if (TCG.isTrue(record.defaultNoteTextPopulated) && TCG.isBlank(noteTextField.getValue())) {
								noteTextField.setValue(record.defaultNoteText);
							}
						}
						noteTextField.allowBlank = (record.blankNoteAllowed === true);
					}
				}
			}]
		}]
	}]
});
