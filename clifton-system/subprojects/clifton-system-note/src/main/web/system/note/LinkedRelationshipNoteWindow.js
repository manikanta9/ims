TCG.use('Clifton.system.note.NoteWindowBase');

Clifton.system.note.LinkedRelationshipNoteWindow = Ext.extend(Clifton.system.note.NoteWindowBase, {

	linkedNoteAllowed: true,
	instructions: 'Linked system note that applies to all entities related by the linked table.',

	additionalNoteItems: [
		{fieldLabel: 'Linked Relationship', name: 'noteType.linkedRelationshipLabel', xtype: 'displayfield', submitValue: false},
		{fieldLabel: 'Linked Relationship Path', name: 'noteType.linkedRelationshipPath', xtype: 'hidden', submitValue: false},
		{fieldLabel: 'LinkedFKFieldID', name: 'linkedFkFieldId', xtype: 'hidden'}

	]


});

