Clifton.system.note.TypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Note Type',
	iconCls: 'pencil',
	height: 550,
	width: 700,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Type',
				items: [{
					xtype: 'formpanel',
					instructions: 'A system note type defines a specific type of note and what table it can be linked to.',
					url: 'systemNoteType.json',
					labelWidth: 160,
					items: [
						{fieldLabel: 'Note Type Name', name: 'name'},
						{fieldLabel: 'Table', name: 'table.name', hiddenName: 'table.id', xtype: 'combo', displayField: 'nameExpanded', url: 'systemTableListFind.json?defaultDataSource=true'},
						{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', allowNegative: false},
						{fieldLabel: 'Max Note Length', name: 'maxNoteLength', xtype: 'spinnerfield', allowNegative: false, minValue: 1, allowBlank: true, allowDecimals: false},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
						{boxLabel: 'Order Supported (Allows notes of this type to be ordered within the type)', name: 'orderSupported', xtype: 'checkbox'},
						{boxLabel: 'Internal Only (If checked, all notes of this type will be set as Private)', name: 'internal', xtype: 'checkbox'},
						{boxLabel: 'System Defined (If checked, cannot change name or table)', name: 'systemDefined', xtype: 'checkbox'},
						{boxLabel: 'Allow Blank Note (usually used for attachments)', name: 'blankNoteAllowed', xtype: 'checkbox'},
						{boxLabel: 'Allow Global Notes (applies to every record in the table)', name: 'globalNoteAllowed', xtype: 'checkbox'},
						{boxLabel: 'Allow linking to multiple entities', name: 'linkToMultipleAllowed', xtype: 'checkbox', qtip: 'Linking a note to multiple entities allows manual selection of which entities the note is linked to.'},
						{boxLabel: 'One per Entity', name: 'onePerEntity', xtype: 'checkbox', qtip: 'An entity can have one active note of this type at a time.'},
						{boxLabel: 'Date Range Allowed (Allows notes to apply to a specific date range)', name: 'dateRangeAllowed', xtype: 'checkbox', mutuallyExclusiveFields: ['endDateResolutionDate']},
						{boxLabel: 'End Date Is Resolution Date (End Date only can be entered and indicates that the note was resolved on that date)', name: 'endDateResolutionDate', xtype: 'checkbox', mutuallyExclusiveFields: ['dateRangeAllowed']},
						{fieldLabel: 'Action Bean', beanName: 'noteActionBean', xtype: 'system-bean-combo', groupName: 'System Note Action Handler', qtip: 'If populated, the selected bean is executed when notes of this type, or links to notes of this type are inserted, updated, or deleted.'},
						{
							xtype: 'fieldset',
							title: 'Security',
							instructions: 'Security Access is first checked for a security resource with the selected table name and <i>Note</i> suffix (i.e.InvestmentAccountNote, TradeNote).  If that security resource does not exist, security will then be verified against the selected table above. When using <i>Note</i> specific security, proper create/write/delete permissions will be required.  Otherwise, write access is sufficient. <b>ADDITIONAL</b> security can be used to further restrict editing of notes of this type by selecting a modify and/or delete condition. If delete condition is blank, the modify condition, if present, will also be used for deletes.',
							items: [
								{xtype: 'system-entity-modify-condition-combo'},
								{xtype: 'system-entity-delete-condition-combo'}
							]
						},
						{
							xtype: 'fieldset',
							title: 'Attachments',
							disabled: !Clifton.system.note.SupportAttachments,
							hidden: !Clifton.system.note.SupportAttachments,
							items: [
								{
									xtype: 'radiogroup', columns: 3, fieldLabel: 'Attachments Supported',
									items: [
										{fieldLabel: '', boxLabel: 'None', qtip: 'Do Not Allowed document attachments to notes of this type', name: 'attachmentSupportedType', inputValue: 'NONE', checked: true},
										{fieldLabel: '', boxLabel: 'Single', qtip: 'Allow one file per note', name: 'attachmentSupportedType', inputValue: 'SINGLE'},
										{fieldLabel: '', boxLabel: 'Multiple', qtip: 'Allow multiple file attachments per note', name: 'attachmentSupportedType', inputValue: 'MULTIPLE'}
									]
								},
								{fieldLabel: 'Document Modify Condition', name: 'documentModifyCondition.name', hiddenName: 'documentModifyCondition.id', displayField: 'name', xtype: 'system-condition-combo'}
							]
						},
						{
							xtype: 'fieldset-checkbox',
							title: 'Linked Relationship Notes',
							checkboxName: 'linkedRelationshipUsed',
							instructions: 'Use the following selection to allow link notes to a related entity.  System Managed Links is used to populate the Linked FK Field ID for the note automatically for the assigned FK Field ID.  This allows assigning a note to a specific entity, but the link provides access to it from the Linked entity.  i.e. Trade Notes are system linked through the Security, so Trade notes are viewable on the Security window as well and flags Security table so its notes are viewable on the Trade.  Non system managed links are those manually linked through a related entity.  For example: Performance Summary Notes are linked by the Client Account and thus apply to all summaries for that account.',
							items: [
								{boxLabel: 'Link is System Managed', name: 'linkSystemManaged', xtype: 'checkbox'},
								{fieldLabel: 'Linked Table', name: 'linkedTable.name', hiddenName: 'linkedTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true'},
								{fieldLabel: 'Relationship Label', name: 'linkedRelationshipLabel', requiredFields: ['linkedTable.id']},
								{fieldLabel: 'Relationship Path', name: 'linkedRelationshipPath', requiredFields: ['linkedTable.id']}
							]
						},
						{
							xtype: 'fieldset-checkbox',
							title: 'Default Text',
							checkboxName: 'defaultNoteTextPopulated',
							labelWidth: 1,
							instructions: 'When new notes of this type are created the note text will default to the follow text if populated.',
							items: [
								{name: 'defaultNoteText', xtype: 'textarea'}
							]
						},

						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'SystemNoteType',
							hierarchyCategoryName: 'System Note Type Tags'
						}
					]
				}]
			},


			{
				title: 'Notes',
				items: [{
					name: 'systemNoteListFind',
					xtype: 'gridpanel',
					instructions: 'The following notes are tied to this type.',
					viewNames: ['Default', 'Attachment Info', 'With Resolution Date'],
					useBufferView: false, // allow cell wrapping
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'FKFieldID', width: 15, dataIndex: 'fkFieldId', hidden: true},
						{header: 'LinkedFKFieldID', width: 15, dataIndex: 'linkedFkFieldId', hidden: true},
						{
							header: 'Note', width: 250, dataIndex: 'text',
							renderer: function(v) {
								return TCG.renderText(v, 'PADDING-LEFT: 10px');
							}
						},
						{header: 'Private', width: 40, dataIndex: 'privateNote', type: 'boolean'},
						{header: 'Inactive', width: 40, dataIndex: 'inactive', type: 'boolean', viewNames: ['Default', 'Attachment Info']},
						{header: 'Start Date', width: 40, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 40, dataIndex: 'endDate', hidden: true, viewNames: ['With Resolution Date']},
						{header: 'Order', width: 35, dataIndex: 'order', hidden: true},
						{header: 'Table Class', width: 40, dataIndex: 'noteType.table.detailScreenClass', hidden: true},
						{header: 'Linked Table Class', width: 40, dataIndex: 'noteType.linkedTable.detailScreenClass', hidden: true},
						{
							header: 'Link', width: 40, dataIndex: '', align: 'center',
							renderer: function(value, metaData, r) {
								if ((r.data['fkFieldId'] !== '' && r.data['noteType.table.detailScreenClass'] !== '') || (r.data['linkedFkFieldId'] !== '' && r.data['noteType.linkedTable.detailScreenClass'] !== '')) {
									return TCG.renderActionColumn('view', 'View', 'View entity that that is linked to this note.');
								}
								return 'N/A';
							}
						},
						{
							header: 'Format', width: 30, dataIndex: 'documentFileType', filter: false, align: 'center', hidden: true, viewNames: ['Attachment Info'],
							renderer: function(ft, args, r) {
								return TCG.renderDocumentFileTypeIcon(ft);
							}
						},
						{header: 'Doc Updated On', width: 65, dataIndex: 'documentUpdateDate', hidden: true, viewNames: ['Attachment Info']},
						{header: 'Doc Updated By', width: 55, dataIndex: 'documentUpdateUser', hidden: true, viewNames: ['Attachment Info']}
					],
					getLoadParams: function(firstLoad) {
						return {noteTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.system.note.NoteWindow',
						drillDownOnly: true
					},
					gridConfig: {
						listeners: {
							'rowclick': function(grid, rowIndex, evt) {
								if (TCG.isActionColumn(evt.target)) {
									const row = grid.store.data.items[rowIndex];
									let clz = row.json.noteType.table.detailScreenClass;
									let id = row.json.fkFieldId;
									if (!clz || !id) {
										clz = row.json.noteType.linkedTable.detailScreenClass;
										id = row.json.linkedFkFieldId;
									}
									const gridPanel = grid.ownerGridPanel;
									gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
								}
							}
						}
					}
				}]
			},


			{
				title: 'Custom Columns',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'The following custom columns are associated with notes of this type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int'},
						{header: 'Required', width: 50, dataIndex: 'required', type: 'boolean'},
						{header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getLoadParams: function() {
						return {
							linkedValue: this.getWindow().getMainFormId(),
							columnGroupName: 'System Note Custom Fields'
						};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						deleteURL: 'systemColumnDelete.json',
						getDefaultData: function(gridPanel) { // defaults group
							const grp = TCG.data.getData('systemColumnGroupByName.json?name=System Note Custom Fields', gridPanel, 'system.schema.group.System Note Custom Fields');
							const fVal = gridPanel.getWindow().getMainForm().formValues;
							const lbl = TCG.getValue('name', fVal);
							return {
								columnGroup: grp
								, linkedValue: TCG.getValue('id', fVal)
								, linkedLabel: lbl
								, table: grp.table
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'SystemNoteType'
				}]
			}
		]
	}]
});



