// not the actual window but a window selector:
//   - single, linked, or global
Clifton.system.note.NoteWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'systemNote.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.system.note.GlobalNoteWindow';
		if (entity && (entity.fkFieldId || entity.linkedToMultiple === true)) {
			className = 'Clifton.system.note.SingleNoteWindow';
		}
		if (entity && entity.linkedFkFieldId) {
			className = 'Clifton.system.note.LinkedRelationshipNoteWindow';
		}
		return className;
	}
});
