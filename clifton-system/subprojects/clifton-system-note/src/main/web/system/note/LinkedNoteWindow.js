// NOTE: USED DIRECTLY WHEN CREATED A MANUALLY LINKED NOTE WITH PASSED FkField IDS
// FOR EXISTING NOTES - USING THE SingleNoteWindow IS ENOUGH AND LINKS ARE MANAGED ON A SECOND TAB
Clifton.system.note.LinkedNoteWindow = Ext.extend(Clifton.system.note.NoteWindowBase, {

	linkToMultipleAllowed: true,

	noteSaveUrl: 'systemNoteWithLinksSave.json',

	additionalNoteItems: [
		{name: 'linkedToMultiple', xtype: 'hidden', value: true}
	]
});
