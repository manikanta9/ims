Clifton.system.note.GlobalNoteWindow = Ext.extend(Clifton.system.note.NoteWindowBase, {

	showUsedByTab: false,
	globalNoteAllowed: true,

	instructions: 'Global system note that applies to all entities for the table. '
});

