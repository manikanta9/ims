Clifton.system.note.NoteSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'systemNoteSetupWindow',
	title: 'System Notes Setup',
	iconCls: 'pencil',
	width: 1500,
	height: 700,

	items: [{
		name: 'systemNoteTypeListFind',
		xtype: 'gridpanel',
		queryExportTagName: 'System Note',
		instructions: 'Notes types allow classifying notes for a specified table into groups and defining their display order.',
		wikiPage: 'IT/System+Notes',

		groupField: 'table.name',
		groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Types" : "Type"]})',
		remoteSort: true,
		topToolbarSearchParameter: 'searchPattern',
		columnOverrides: [
			{dataIndex: 'attachmentSupportedType', hidden: !Clifton.system.note.SupportAttachments}
		],
		getTopToolbarFilters: function(toolbar) {
			return [
				{fieldLabel: 'Table', xtype: 'combo', name: 'systemTableId', width: 150, url: 'systemTableListFind.json?defaultDataSource=true', linkedFilter: 'table.name'},
				{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=System Note Type Tags'},
				{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
			];
		},
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Table', width: 75, dataIndex: 'table.name', filter: {type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', searchFieldName: 'tableId'}, hidden: true},
			{header: 'Note Type Name', width: 75, dataIndex: 'name'},
			{header: 'Linked Table', hidden: true, width: 75, dataIndex: 'linkedTable.name', filter: {type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', searchFieldName: 'linkedTableName'}},
			{header: 'Description', width: 145, dataIndex: 'description'},
			{header: 'Order', width: 25, dataIndex: 'order', type: 'int'},
			{header: 'System Defined', width: 30, dataIndex: 'systemDefined', type: 'boolean'},
			{header: 'Modify Condition', width: 75, dataIndex: 'entityModifyCondition.name', hidden: true, filter: {searchFieldName: 'entityModifyCondition'}},
			{header: 'Delete Condition', width: 75, dataIndex: 'entityDeleteCondition.name', hidden: true, tooltip: 'If blank will use the modify condition if present.', filter: {searchFieldName: 'entityDeleteCondition'}},
			{header: 'Action Bean', width: 75, dataIndex: 'noteActionBean.name', hidden: true, tooltip: 'If populated, the action bean is called after inserts/updates/deletes to notes of this type, or note links for this type.', filter: {searchFieldName: 'noteActionBean'}},
			{header: 'Internal', width: 30, dataIndex: 'internal', type: 'boolean'},
			{header: 'Allow Blank', width: 35, dataIndex: 'blankNoteAllowed', type: 'boolean'},
			{header: 'Allow Global', width: 35, dataIndex: 'globalNoteAllowed', type: 'boolean'},
			{header: 'Allow Date Range', width: 35, dataIndex: 'dateRangeAllowed', type: 'boolean'},
			{header: 'End Date Is Resolution Date', width: 35, dataIndex: 'endDateResolutionDate', type: 'boolean', hidden: true},
			{
				header: 'Link To Multiple', width: 35, dataIndex: 'linkToMultipleAllowed', type: 'boolean',
				tooltip: 'Users are allowed to manually select entities to link the note to'
			},
			{
				header: 'Linked Relationship', width: 35, dataIndex: 'linkedNoteAllowed', type: 'boolean',
				tooltip: 'Users are allowed to create a note that applies to multiple entities based on another field.  For example, Performance Summary notes can be created for the linked Client Account and thus would apply to any summary for that account.'
			},
			{
				header: 'System Managed Link', width: 35, dataIndex: 'linkSystemManaged', type: 'boolean', hidden: true,
				tooltip: 'Utilized for single entity notes, or manually linked notes to flag another property as an additional link.  Allows for the ability to view those notes from the linked entity. Example: Trade Notes with system managed link to Security allows users to see all Trade Notes for a security'
			},
			{
				header: 'One Per Entity', width: 35, dataIndex: 'onePerEntity', type: 'boolean', hidden: true,
				tooltip: 'When checked an entity can only have one active note of this type at a time.'
			},
			{
				header: 'Attachments Allowed', width: 35, dataIndex: 'attachmentSupportedType', filter: {type: 'list', options: [['NONE', 'NONE'], ['SINGLE', 'SINGLE'], ['MULTIPLE', 'MULTIPLE']]},
				renderer: function(value, metaData, r) {
					if (TCG.isEquals('SINGLE', value)) {
						return 'Single';
					}
					if (TCG.isEquals('MULTIPLE', value)) {
						metaData.css = 'amountAdjusted';
						return 'Multiple';
					}
					return '';
				}
			},
			{header: 'Has Default Text', width: 35, dataIndex: 'defaultNoteTextPopulated', type: 'boolean'}
		],

		getTopToolbarInitialLoadParams: function(firstLoad) {
			const lp = {};

			const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
			if (TCG.isNotBlank(tag.getValue())) {
				lp.categoryName = 'System Note Type Tags';
				lp.categoryHierarchyId = tag.getValue();
			}
			return lp;
		},
		editor: {
			detailPageClass: 'Clifton.system.note.TypeWindow'
		}
	}]
});
