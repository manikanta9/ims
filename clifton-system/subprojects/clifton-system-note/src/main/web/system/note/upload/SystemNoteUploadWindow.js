Clifton.system.note.upload.SystemNoteUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'systemNoteUploadWindow',
	title: 'System Note Import',
	iconCls: 'import',
	height: 400,
	hideOKButton: true,


	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File for System Note Uploads for notes associated with a selected entity',
		handler: function() {
			TCG.openFile('system/note/upload/SimpleSystemNoteUploadFileSample.xls');
		}
	}],

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		loadDefaultDataAfterRender: true,
		instructions: 'System Note Uploads allow you to import notes from files directly into the system.  Note table is populated for you from the table where you linked from.  An entity must be selected and will associate all imported notes to that entity.',

		listeners: {
			'afterrender': function() {
				const win = this.getWindow();
				const dd = this.getDefaultData(win);
				this.resetEntityListUrl(dd.fkFieldTableName);
			}
		},

		resetEntityListUrl: function(tblName) {
			let entityListUrl = undefined;
			if (tblName !== '') {
				const tbl = TCG.data.getData('systemTableByName.json?tableName=' + tblName, this, 'system.table.' + tblName);
				if (tbl) {
					entityListUrl = tbl.entityListUrl;
				}
			}
			let f = this;
			if (this.getForm) {
				f = this.getForm();
			}
			const entityField = f.findField('fkFieldId');
			if (entityListUrl) {
				const remote = true;
				entityField.url = entityListUrl;
				entityField.store = new TCG.data.JsonStore({
					root: entityField.root,
					totalProperty: (remote && !entityField.loadAll) ? 'data.total' : 'totalRowCount',
					fields: entityField.fields ? entityField.fields : entityField.urlFields,
					url: encodeURI(entityListUrl),
					autoLoad: !remote
				});


			}
			else {
				// Set url to replace me so it's flagged that we don't have a look up for it
				entityField.url = 'replaceMe';
			}
		},

		items: [
			{fieldLabel: 'Note Table', name: 'fkFieldTableName', readOnly: true},
			{
				fieldLabel: 'Entity', name: 'fkFieldIdLabel', hiddenName: 'fkFieldId', xtype: 'combo', loadAll: false, url: 'replaceMe', allowBlank: false,
				qtip: 'Select a specific entity these notes are tied to.',
				beforequery: function(queryEvent) {
					const cmb = queryEvent.combo;
					if (cmb.url === 'replaceMe') {
						TCG.showError('There is no entity look up list defined for the selected table.');
						return false;
					}
				}
			},
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'systemNoteFileUpload.json';
		}

	}]
});
