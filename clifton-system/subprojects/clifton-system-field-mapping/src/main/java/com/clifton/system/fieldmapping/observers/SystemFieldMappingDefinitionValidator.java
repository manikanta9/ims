package com.clifton.system.fieldmapping.observers;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.fieldmapping.SystemFieldMappingDefinition;
import org.springframework.stereotype.Component;


/**
 * @author AbhinayaM
 */

@Component
public class SystemFieldMappingDefinitionValidator extends SelfRegisteringDaoValidator<SystemFieldMappingDefinition> {


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemFieldMappingDefinition bean, DaoEventTypes config) throws ValidationException {

		if (config.isDelete()) {
			throw new ValidationException("You cannot delete an existing System Field Mapping Definition");
		}
		if (config.isUpdate()) {
			SystemFieldMappingDefinition original = getOriginalBean(bean);
			if (!(StringUtils.isEqual(original.getName(), bean.getName()))) {
				throw new FieldValidationException("You cannot change the Definition name of an existing System Field Mapping Definition");
			}
			if (!(CompareUtils.isEqual(original.getEntitySystemTable(), bean.getEntitySystemTable()))) {
				throw new FieldValidationException("You cannot change the table of an existing System Field Mapping Definition");
			}
			if (!(CompareUtils.isEqual(original.getDataType(), bean.getDataType()))) {
				throw new FieldValidationException("You cannot change the Data Type of an existing System Field Mapping Definition");
			}
		}
		if (config.isInsert()) {
			throw new ValidationException("You cannot insert a new System Field Mapping Definition");
		}
	}
}

