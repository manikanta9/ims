package com.clifton.system.fieldmapping.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author AbhinayaM
 */
public class SystemFieldMappingSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "fieldMappingDefinition.id", sortField = "fieldMappingDefinition.name")
	private Short fieldMappingDefinitionId;

	@SearchField(searchField = "fieldMappingDefinition.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String fieldMappingDefinitionName;

	@SearchField(searchField = "fieldMappingDefinition.entitySystemTable.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String fieldMappingDefinitionEntityTableName;

	@SearchField(searchField = "fieldMappingDefinition.entitySystemTable.id", comparisonConditions = ComparisonConditions.EQUALS)
	private Short fieldMappingDefinitionEntityTableId;

	@SearchField(searchField = "fieldMappingDefinition.valueTable.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String fieldMappingDefinitionValueTableName;

	@SearchField(searchField = "fieldMappingDefinition.valueTable.id", comparisonConditions = ComparisonConditions.EQUALS)
	private Short fieldMappingDefinitionValueTableId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getFieldMappingDefinitionId() {
		return this.fieldMappingDefinitionId;
	}


	public void setFieldMappingDefinitionId(Short fieldMappingDefinitionId) {
		this.fieldMappingDefinitionId = fieldMappingDefinitionId;
	}


	public String getFieldMappingDefinitionName() {
		return this.fieldMappingDefinitionName;
	}


	public void setFieldMappingDefinitionName(String fieldMappingDefinitionName) {
		this.fieldMappingDefinitionName = fieldMappingDefinitionName;
	}


	public String getFieldMappingDefinitionEntityTableName() {
		return this.fieldMappingDefinitionEntityTableName;
	}


	public void setFieldMappingDefinitionEntityTableName(String fieldMappingDefinitionEntityTableName) {
		this.fieldMappingDefinitionEntityTableName = fieldMappingDefinitionEntityTableName;
	}


	public Short getFieldMappingDefinitionEntityTableId() {
		return this.fieldMappingDefinitionEntityTableId;
	}


	public void setFieldMappingDefinitionEntityTableId(Short fieldMappingDefinitionEntityTableId) {
		this.fieldMappingDefinitionEntityTableId = fieldMappingDefinitionEntityTableId;
	}


	public String getFieldMappingDefinitionValueTableName() {
		return this.fieldMappingDefinitionValueTableName;
	}


	public void setFieldMappingDefinitionValueTableName(String fieldMappingDefinitionValueTableName) {
		this.fieldMappingDefinitionValueTableName = fieldMappingDefinitionValueTableName;
	}


	public Short getFieldMappingDefinitionValueTableId() {
		return this.fieldMappingDefinitionValueTableId;
	}


	public void setFieldMappingDefinitionValueTableId(Short fieldMappingDefinitionValueTableId) {
		this.fieldMappingDefinitionValueTableId = fieldMappingDefinitionValueTableId;
	}
}
