package com.clifton.system.fieldmapping;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;
import com.clifton.system.usedby.softlink.SoftLinkField;


/**
 * @author AbhinayaM
 * System Field Mapping Entries define a particular mapping between an entity and it's value.
 * <p>
 * For example, for Broker Mappings, specifically FX Connect, we map Bank of New York Mellon as company to value bnym
 */
public class SystemFieldMappingEntry extends BaseEntity<Integer> implements SystemEntityModifyConditionAwareAdminRequired, LabeledObject {


	private SystemFieldMapping systemFieldMapping;

	@SoftLinkField(tableBeanPropertyName = "systemFieldMapping.fieldMappingDefinition.entitySystemTable")
	private long fkFieldId;

	private String fkFieldLabel;

	private String mappingValue;

	private String mappingText;

	private Integer priorityOrder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getSystemFieldMapping() != null) {
			return getSystemFieldMapping().getEntityModifyCondition();
		}
		return null;
	}


	@Override
	public String getLabel() {
		return getFkFieldLabel() + " - " + getMappingValue();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public String getFkFieldLabel() {
		return this.fkFieldLabel;
	}


	public void setFkFieldLabel(String fkFieldLabel) {
		this.fkFieldLabel = fkFieldLabel;
	}


	public String getMappingValue() {
		return this.mappingValue;
	}


	public void setMappingValue(String mappingValue) {
		this.mappingValue = mappingValue;
	}


	public String getMappingText() {
		return this.mappingText != null ? this.mappingText : getMappingValue();
	}


	public void setMappingText(String mappingText) {
		this.mappingText = mappingText;
	}


	public SystemFieldMapping getSystemFieldMapping() {
		return this.systemFieldMapping;
	}


	public void setSystemFieldMapping(SystemFieldMapping systemFieldMapping) {
		this.systemFieldMapping = systemFieldMapping;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Integer getPriorityOrder() {
		return this.priorityOrder;
	}


	public void setPriorityOrder(Integer priorityOrder) {
		this.priorityOrder = priorityOrder;
	}
}
