package com.clifton.system.fieldmapping.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author AbhinayaM
 */
public class SystemFieldMappingDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "mappingEntityModifyCondition.id", sortField = "mappingEntityModifyCondition.name")
	private Integer mappingEntityModifyConditionId;

	@SearchField(searchField = "entitySystemTable.id", sortField = "entitySystemTable.name")
	private Short entitySystemTableId;

	@SearchField
	private String entityListUrl;

	@SearchField(searchField = "dataType.id", sortField = "dataType.name")
	private Short dataTypeId;

	@SearchField(searchField = "valueTable.id", sortField = "valueTable.name")
	private Short valueTableId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getMappingEntityModifyConditionId() {
		return this.mappingEntityModifyConditionId;
	}


	public void setMappingEntityModifyConditionId(Integer mappingEntityModifyConditionId) {
		this.mappingEntityModifyConditionId = mappingEntityModifyConditionId;
	}


	public Short getEntitySystemTableId() {
		return this.entitySystemTableId;
	}


	public void setEntitySystemTableId(Short entitySystemTableId) {
		this.entitySystemTableId = entitySystemTableId;
	}


	public String getEntityListUrl() {
		return this.entityListUrl;
	}


	public void setEntityListUrl(String entityListUrl) {
		this.entityListUrl = entityListUrl;
	}


	public Short getDataTypeId() {
		return this.dataTypeId;
	}


	public void setDataTypeId(Short dataTypeId) {
		this.dataTypeId = dataTypeId;
	}


	public Short getValueTableId() {
		return this.valueTableId;
	}


	public void setValueTableId(Short valueTableId) {
		this.valueTableId = valueTableId;
	}
}
