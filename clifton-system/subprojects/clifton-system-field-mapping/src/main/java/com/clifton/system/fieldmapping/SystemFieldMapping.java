package com.clifton.system.fieldmapping;

import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;


/**
 * @author AbhinayaM
 * <p>
 * System Field Mappings are a grouping for System Field Mapping Entries for a particular System Field Mapping Definition.
 * For example, we define a mapping for FX Connect that groups all of the Broker Company Mappings used when sending orders via FIX to FX Connect.
 */
public class SystemFieldMapping extends NamedEntity<Short> implements SystemEntityModifyConditionAwareAdminRequired {

	private SystemFieldMappingDefinition fieldMappingDefinition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getFieldMappingDefinition() != null) {
			return getFieldMappingDefinition().getMappingEntityModifyCondition();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemFieldMappingDefinition getFieldMappingDefinition() {
		return this.fieldMappingDefinition;
	}


	public void setFieldMappingDefinition(SystemFieldMappingDefinition fieldMappingDefinition) {
		this.fieldMappingDefinition = fieldMappingDefinition;
	}
}
