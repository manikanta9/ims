package com.clifton.system.fieldmapping.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author AbhinayaM
 */
public class SystemFieldMappingEntrySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "fkFieldLabel,mappingValue")
	private String searchPattern;

	@SearchField(searchField = "systemFieldMapping.id", sortField = "systemFieldMapping.name")
	private Short systemFieldMappingId;

	@SearchField(searchFieldPath = "systemFieldMapping", searchField = "fieldMappingDefinition.id", sortField = "fieldMappingDefinition.name")
	private Short systemFieldMappingDefinitionId;


	@SearchField
	private Long fkFieldId;

	@SearchField
	private String fkFieldLabel;

	@SearchField
	private String mappingValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getSystemFieldMappingId() {
		return this.systemFieldMappingId;
	}


	public void setSystemFieldMappingId(Short systemFieldMappingId) {
		this.systemFieldMappingId = systemFieldMappingId;
	}


	public Short getSystemFieldMappingDefinitionId() {
		return this.systemFieldMappingDefinitionId;
	}


	public void setSystemFieldMappingDefinitionId(Short systemFieldMappingDefinitionId) {
		this.systemFieldMappingDefinitionId = systemFieldMappingDefinitionId;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public String getFkFieldLabel() {
		return this.fkFieldLabel;
	}


	public void setFkFieldLabel(String fkFieldLabel) {
		this.fkFieldLabel = fkFieldLabel;
	}


	public String getMappingValue() {
		return this.mappingValue;
	}


	public void setMappingValue(String mappingValue) {
		this.mappingValue = mappingValue;
	}
}
