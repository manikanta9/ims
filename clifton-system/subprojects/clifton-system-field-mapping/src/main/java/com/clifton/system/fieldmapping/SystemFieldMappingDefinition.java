package com.clifton.system.fieldmapping;

import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.userinterface.BaseNamedSystemUserInterfaceAwareEntity;


/**
 * System Field Mapping Definitions define categories of System Field Mappings and associate them with a particular table that the mapping entries are linked to.
 * For example, Broker Mappings are used to associate Order Companies with a Broker Code value.  These mappings are associated with Order Destinations, and can be used for FIX destinations so when sending order information to an external party we can send the broker code value that external system uses for our companies
 *
 * @author AbhinayaM
 */
@CacheByName
public class SystemFieldMappingDefinition extends BaseNamedSystemUserInterfaceAwareEntity<Short> {


	/**
	 * Additional Security: Modify Condition required to pass for non-admin users when updating field mappings and/or entries under this definition.  Always passes for admin users
	 */
	private SystemCondition mappingEntityModifyCondition;

	private SystemTable entitySystemTable;

	private String entityListUrl;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getOrder() {
		return 1;
	}


	@Override
	public boolean isRequired() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemCondition getMappingEntityModifyCondition() {
		return this.mappingEntityModifyCondition;
	}


	public void setMappingEntityModifyCondition(SystemCondition mappingEntityModifyCondition) {
		this.mappingEntityModifyCondition = mappingEntityModifyCondition;
	}


	public SystemTable getEntitySystemTable() {
		return this.entitySystemTable;
	}


	public void setEntitySystemTable(SystemTable entitySystemTable) {
		this.entitySystemTable = entitySystemTable;
	}


	public String getEntityListUrl() {
		return this.entityListUrl;
	}


	public void setEntityListUrl(String entityListUrl) {
		this.entityListUrl = entityListUrl;
	}
}
