package com.clifton.system.fieldmapping;

import com.clifton.system.fieldmapping.search.SystemFieldMappingDefinitionSearchForm;
import com.clifton.system.fieldmapping.search.SystemFieldMappingEntrySearchForm;
import com.clifton.system.fieldmapping.search.SystemFieldMappingSearchForm;

import java.util.List;


/**
 * @author AbhinayaM
 */

public interface SystemFieldMappingService {

	////////////////////////////////////////////////////////////////////////////
	////////            SystemFieldMappingDefinition methods             ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemFieldMappingDefinition getSystemFieldMappingDefinition(short id);


	public SystemFieldMappingDefinition getSystemFieldMappingDefinitionByName(String name);


	public List<SystemFieldMappingDefinition> getSystemFieldMappingDefinitionList(SystemFieldMappingDefinitionSearchForm searchForm);


	public SystemFieldMappingDefinition saveSystemFieldMappingDefinition(SystemFieldMappingDefinition bean);


	public void deleteSystemFieldMappingDefinition(short id);

	////////////////////////////////////////////////////////////////////////////
	////////            SystemFieldMappingEntry methods             ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemFieldMappingEntry getSystemFieldMappingEntry(int id);


	public List<SystemFieldMappingEntry> getSystemFieldMappingEntryList(SystemFieldMappingEntrySearchForm searchForm);


	public SystemFieldMappingEntry saveSystemFieldMappingEntry(SystemFieldMappingEntry bean);


	public void deleteSystemFieldMappingEntry(int id);

	////////////////////////////////////////////////////////////////////////////
	////////            SystemFieldMapping methods             ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemFieldMapping getSystemFieldMapping(short id);


	public List<SystemFieldMapping> getSystemFieldMappingList(SystemFieldMappingSearchForm searchForm);


	public SystemFieldMapping saveSystemFieldMapping(SystemFieldMapping bean);


	public void deleteSystemFieldMapping(short id);
}
