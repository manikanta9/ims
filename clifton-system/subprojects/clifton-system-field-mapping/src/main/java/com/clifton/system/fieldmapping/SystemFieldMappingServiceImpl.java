package com.clifton.system.fieldmapping;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.system.fieldmapping.search.SystemFieldMappingDefinitionSearchForm;
import com.clifton.system.fieldmapping.search.SystemFieldMappingEntrySearchForm;
import com.clifton.system.fieldmapping.search.SystemFieldMappingSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author AbhinayaM
 */

@Service
public class SystemFieldMappingServiceImpl implements SystemFieldMappingService {

	private AdvancedUpdatableDAO<SystemFieldMappingDefinition, Criteria> systemFieldMappingDefinitionDAO;
	private AdvancedUpdatableDAO<SystemFieldMappingEntry, Criteria> systemFieldMappingEntryDAO;
	private AdvancedUpdatableDAO<SystemFieldMapping, Criteria> systemFieldMappingDAO;

	private DaoNamedEntityCache<SystemFieldMappingDefinition> systemFieldMappingDefinitionCache;


	////////////////////////////////////////////////////////////////////////////
	////////              SystemFieldMappingDefinition methods           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemFieldMappingDefinition getSystemFieldMappingDefinition(short id) {
		return getSystemFieldMappingDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemFieldMappingDefinition getSystemFieldMappingDefinitionByName(String name) {
		return getSystemFieldMappingDefinitionCache().getBeanForKeyValue(getSystemFieldMappingDefinitionDAO(), name);
	}


	@Override
	public List<SystemFieldMappingDefinition> getSystemFieldMappingDefinitionList(SystemFieldMappingDefinitionSearchForm searchForm) {
		return getSystemFieldMappingDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemFieldMappingDefinition saveSystemFieldMappingDefinition(SystemFieldMappingDefinition bean) {

		return getSystemFieldMappingDefinitionDAO().save(bean);
	}


	@Override
	public void deleteSystemFieldMappingDefinition(short id) {
		getSystemFieldMappingDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              SystemFieldMappingEntry methods           ////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public SystemFieldMappingEntry getSystemFieldMappingEntry(int id) {
		return getSystemFieldMappingEntryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemFieldMappingEntry> getSystemFieldMappingEntryList(SystemFieldMappingEntrySearchForm searchForm) {

		return getSystemFieldMappingEntryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemFieldMappingEntry saveSystemFieldMappingEntry(SystemFieldMappingEntry bean) {

		return getSystemFieldMappingEntryDAO().save(bean);
	}


	@Override
	public void deleteSystemFieldMappingEntry(int id) {

		getSystemFieldMappingEntryDAO().delete(id);
	}


	/////////////////////////////////////////////////////////////////////////////
	////////              SystemFieldMapping methods                 ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemFieldMapping getSystemFieldMapping(short id) {

		return getSystemFieldMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemFieldMapping> getSystemFieldMappingList(SystemFieldMappingSearchForm searchForm) {

		return getSystemFieldMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemFieldMapping saveSystemFieldMapping(SystemFieldMapping bean) {

		return getSystemFieldMappingDAO().save(bean);
	}


	@Override
	public void deleteSystemFieldMapping(short id) {
		getSystemFieldMappingDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                  Getter and Setter Methods                 ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemFieldMappingDefinition, Criteria> getSystemFieldMappingDefinitionDAO() {
		return this.systemFieldMappingDefinitionDAO;
	}


	public void setSystemFieldMappingDefinitionDAO(AdvancedUpdatableDAO<SystemFieldMappingDefinition, Criteria> systemFieldMappingDefinitionDAO) {
		this.systemFieldMappingDefinitionDAO = systemFieldMappingDefinitionDAO;
	}


	public AdvancedUpdatableDAO<SystemFieldMappingEntry, Criteria> getSystemFieldMappingEntryDAO() {
		return this.systemFieldMappingEntryDAO;
	}


	public void setSystemFieldMappingEntryDAO(AdvancedUpdatableDAO<SystemFieldMappingEntry, Criteria> systemFieldMappingEntryDAO) {
		this.systemFieldMappingEntryDAO = systemFieldMappingEntryDAO;
	}


	public AdvancedUpdatableDAO<SystemFieldMapping, Criteria> getSystemFieldMappingDAO() {
		return this.systemFieldMappingDAO;
	}


	public void setSystemFieldMappingDAO(AdvancedUpdatableDAO<SystemFieldMapping, Criteria> systemFieldMappingDAO) {
		this.systemFieldMappingDAO = systemFieldMappingDAO;
	}


	public DaoNamedEntityCache<SystemFieldMappingDefinition> getSystemFieldMappingDefinitionCache() {
		return this.systemFieldMappingDefinitionCache;
	}


	public void setSystemFieldMappingDefinitionCache(DaoNamedEntityCache<SystemFieldMappingDefinition> systemFieldMappingDefinitionCache) {
		this.systemFieldMappingDefinitionCache = systemFieldMappingDefinitionCache;
	}
}
