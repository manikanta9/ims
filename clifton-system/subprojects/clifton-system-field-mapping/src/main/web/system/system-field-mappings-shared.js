Ext.ns(
	'Clifton.system',
	'Clifton.system.fieldmapping'
);


Clifton.system.fieldmapping.SystemFieldMappingGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'systemFieldMappingEntryListFind',
	xtype: 'gridpanel',
	instructions: 'System Field Mapping Entries define a particular mapping between an entity and it\'s value.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Field Mapping Definition', width: 20, dataIndex: 'systemFieldMapping.fieldMappingDefinition.name', filter: {searchFieldName: 'systemFieldMapping.fieldMappingDefinitionId', type: 'combo', url: 'systemFieldMappingDefinitionListFind.json'}},
		{header: 'Field Mapping', width: 40, dataIndex: 'systemFieldMapping.name', filter: {searchFieldName: 'systemFieldMappingId', type: 'combo', url: 'systemFieldMappingListFind.json'}},
		{header: 'FKFieldID', width: 30, dataIndex: 'fkFieldId', hidden: true},
		{header: 'Linked Entity', width: 30, dataIndex: 'fkFieldLabel'},
		{header: 'Mapping Value', width: 40, dataIndex: 'mappingValue', tooltip: 'i.e. For Broker Mappings the executing broker code is the code internal to that destination that represents the executing broker.  This is often an abbrievation or numeric value.  This value, if specified is included in the FIX Message, if blank will default to the executing broker company name.'},
		{header: 'Mapping Text', width: 40, dataIndex: 'mappingText'},
		{header: 'Priority Order', width: 20, dataIndex: 'priorityOrder', type: 'int', useNull: true}

	],
	editor: {
		detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingEntryWindow'
	}

});
Ext.reg('system-field-mapping-entry-grid', Clifton.system.fieldmapping.SystemFieldMappingGrid);
