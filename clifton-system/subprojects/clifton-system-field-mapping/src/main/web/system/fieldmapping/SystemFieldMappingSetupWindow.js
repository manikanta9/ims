Clifton.system.fieldmapping.SystemFieldMappingSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'systemFieldMappingSetupWindow',
	title: 'System Field Mappings',
	iconCls: 'tools',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Field Mapping Entries',
				items: [{
					xtype: 'system-field-mapping-entry-grid'

				}]
			},

			{
				title: 'Field Mappings',
				items: [{
					name: 'systemFieldMappingListFind',
					instructions: 'System Field Mappings are a grouping for System Field Mapping Entries for a particular System Field Mapping Definition. For example, we define a mapping for FX Connect that groups all of the Broker Company Mappings used when sending orders via FIX to FX Connect.',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Mapping Definition', width: 50, dataIndex: 'fieldMappingDefinition.name', filter: {searchFieldName: 'fieldMappingDefinitionId', type: 'combo', disableAddNewItem: true, url: 'systemFieldMappingDefinitionListFind.json'}},
						{header: 'Mapping Name', width: 40, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Mapping Description', width: 100, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingWindow'
					}
				}]
			},

			{
				title: 'Field Mapping Definitions',
				items: [{
					name: 'systemFieldMappingDefinitionListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'System Field Mapping Definitions define categories of System Field Mappings and associate them with a particular table that the mapping entries are linked to. For example, Broker Mappings are used to associate Order Companies with a Broker Code value.  These mappings are associated with Order Destinations, and can be used for FIX destinations so when sending order information to an external party we can send the broker code value that external system uses for our companies',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Definition Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Entity System Table', width: 40, dataIndex: 'entitySystemTable.name', filter: {type: 'combo', searchFieldName: 'systemTableId', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
						{header: 'Mapping Modify Condition ', width: 70, dataIndex: 'mappingEntityModifyCondition.name', filter: {type: 'combo', searchFieldName: 'mappingEntityModifyConditionId', url: 'systemConditionListFind.json'}, tooltip: 'Additional Security: Modify Condition required to pass for non-admin users when updating field mappings and/or entries under this definition.  Always passes for admin users'},
						{header: 'Entity List Url', width: 90, dataIndex: 'entityListUrl', hidden: true},
						{header: 'Data Type ', width: 20, dataIndex: 'dataType.name', filter: {type: 'combo', searchFieldName: 'dataTypeId', url: 'systemDataTypeList.json', loadAll: true}},
						{header: 'Value System Table', width: 40, dataIndex: 'valueTable.name', filter: {type: 'combo', searchFieldName: 'valueTableId', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
						{header: 'Value List Url', width: 90, dataIndex: 'valueListUrl', hidden: true},
						{header: 'UI Config', width: 90, dataIndex: 'userInterfaceConfig', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingDefinitionWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]

});
