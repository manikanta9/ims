Clifton.system.fieldmapping.SystemFieldMappingDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Field Mapping Definition',
	iconCls: 'shopping-cart',
	width: 800,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'System Field Mapping Definitions define categories of System Field Mappings and associate them with a particular table that the mapping entries are linked to. For example, Broker Mappings are used to associate Order Companies with a Broker Code value.  These mappings are associated with Order Destinations, and can be used for FIX destinations so when sending order information to an external party we can send the broker code value that external system uses for our companies',
					url: 'systemFieldMappingDefinition.json',
					labelWidth: 160,
					items: [
						{fieldLabel: 'Definition Name', name: 'name', readOnly: true},
						{fieldLabel: 'Entity System Table', name: 'entitySystemTable.name', detailIdField: 'entitySystemTable.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow'},
						{
							fieldLabel: 'Mapping Modify Condition', name: 'mappingEntityModifyCondition.name', hiddenName: 'mappingEntityModifyCondition.id', xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true,
							requireConditionForNonAdminQtip: 'Additional Security: Modify Condition required to pass for non-admin users when updating field mappings and/or entries under this definition.  Always passes for admin users'
						},
						{fieldLabel: 'Entity List URL', name: 'entityListUrl', xtype: 'textfield'},
						{fieldLabel: 'Data Type', name: 'dataType.name', detailIdField: 'dataType.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.DataTypeWindow'},
						{fieldLabel: 'Value System Table', name: 'valueTable.name', hiddenName: 'valueTable.id', xtype: 'combo', url: 'systemTableListFind.json'},
						{fieldLabel: 'Value List URL', name: 'valueListUrl', xtype: 'textfield'},
						{fieldLabel: 'UI Config', name: 'userInterfaceConfig', xtype: 'textarea', height: 50}
					]
				}]
			},

			{
				title: 'Field Mappings',
				items: [{
					name: 'systemFieldMappingListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Mapping Name', width: 40, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Mapping Description', width: 100, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingWindow',
						getDefaultData: function(gridPanel) {
							return {
								fieldMappingDefinition: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {
							fieldMappingDefinitionId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Field Mapping Entries',
				xtype: 'system-field-mapping-entry-grid',
				columnOverrides: [
					{dataIndex: 'systemFieldMapping.fieldMappingDefinition.name', hidden: true}
				],
				editor: {
					detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingEntryWindow',
					getDefaultData: function(gridPanel) {
						return {
							systemFieldMapping: gridPanel.getWindow().getMainForm().formValues
						};
					}
				},
				getTopToolbarInitialLoadParams: function(firstLoad) {
					return {
						systemFieldMappingDefinitionId: this.getWindow().getMainFormId()
					};
				}
			}
		]
	}]
});
