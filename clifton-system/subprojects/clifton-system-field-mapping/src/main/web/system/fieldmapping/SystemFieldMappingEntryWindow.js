Clifton.system.fieldmapping.SystemFieldMappingEntryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Field Mapping Entry',
	iconCls: 'shopping-cart',
	width: 700,
	height: 400,

	items: [{
		xtype: 'formpanel',
		instructions: 'System Field Mapping Entries define a particular mapping between an entity and it\'s value.',
		url: 'systemFieldMappingEntry.json',

		listeners: {
			afterload: function(fp) {
				const fieldMappingDefinition = TCG.getValue('systemFieldMapping.fieldMappingDefinition', fp.getForm().formValues);
				fp.resetFieldsForDefinition(fieldMappingDefinition);
				fp.resetValueField(fieldMappingDefinition);
			}
		},
		resetValueField: function(providedFieldMappingDefinition) {
			let fieldMappingDefinition = providedFieldMappingDefinition;
			if (TCG.isNull(fieldMappingDefinition)) {
				fieldMappingDefinition = this.getFormValue('systemFieldMapping.fieldMappingDefinition');
			}
			const ff = TCG.getChildByName(this, 'valueFormFragment');
			ff.removeAll(true);

			const fieldConfig = {
				xtype: 'textfield',
				name: 'mappingValue',
				listeners: {},
				fieldLabel: 'Mapping Value',
				allowBlank: false
			};
			if (fieldMappingDefinition.description) {
				fieldConfig.qtip = fieldMappingDefinition.description;
			}
			const dt = fieldMappingDefinition.dataType.name;
			if (fieldMappingDefinition.valueListUrl) {
				fieldConfig.xtype = 'combo';
				fieldConfig.url = fieldMappingDefinition.valueListUrl;
				if (fieldMappingDefinition.valueTable && fieldMappingDefinition.valueTable.detailScreenClass) {
					fieldConfig.detailPageClass = fieldMappingDefinition.valueTable.detailScreenClass;
				}
			}
			else if (TCG.isEquals(dt, 'DATE')) {
				fieldConfig.xtype = 'datefield';
			}
			else if (TCG.isEquals(dt, 'INTEGER')) {
				fieldConfig.xtype = 'integerfield';
			}
			else if (TCG.isEquals(dt, 'DECIMAL')) {
				fieldConfig.xtype = 'currencyfield';
			}
			else if (TCG.isEquals(dt, 'BOOLEAN')) {
				fieldConfig.xtype = 'checkbox';
			}

			// apply custom configuration if any
			if (fieldMappingDefinition.userInterfaceConfig) {
				Ext.apply(fieldConfig, Ext.decode(fieldMappingDefinition.userInterfaceConfig));
			}
			// Value doesn't seem to update automatically based on field names - maybe because it's added after?
			let fieldValue = TCG.getValue('mappingValue', this.getForm().formValues);
			if (fieldConfig.xtype && fieldConfig.xtype.indexOf('combo') >= 0) {
				fieldConfig.hiddenName = 'mappingValue';
				fieldConfig.name = 'mappingText';
				fieldConfig.submitValue = true; // Combos need to submit both value and text
				fieldValue = {value: TCG.getValue('mappingValue', this.getForm().formValues), text: TCG.getValue('mappingText', this.getForm().formValues)};
			}
			else {
				ff.add({xtype: 'textfield', hidden: true, name: 'mappingText'});
			}
			const field = ff.add(fieldConfig);
			field.setValue(fieldValue);
			ff.doLayout();
		},
		prepareDefaultData: function(defaultData) {
			if (defaultData) {
				const definition = TCG.getValue('systemFieldMapping.fieldMappingDefinition', defaultData);
				this.resetFieldsForDefinition(definition);
				this.resetValueField(definition);
			}
			return defaultData;
		},

		resetFieldsForDefinition: function(fieldMappingDefinition) {
			if (fieldMappingDefinition) {
				const fp = this;
				const fkFieldCombo = fp.getForm().findField('fkFieldLabel');
				let entityListUrl = TCG.getValue('entityListUrl', fieldMappingDefinition);
				entityListUrl = entityListUrl ? entityListUrl : TCG.getValue('entitySystemTable.entityListUrl', fieldMappingDefinition);
				const entitySystemTableDetailScreenClass = TCG.getValue('entitySystemTable.detailScreenClass', fieldMappingDefinition);
				const entitySystemTableLabel = TCG.getValue('entitySystemTable.label', fieldMappingDefinition);

				if (entitySystemTableLabel) {
					fp.setFieldLabel('fkFieldLabel', entitySystemTableLabel + ': ');
				}
				fkFieldCombo.setUrl(entityListUrl);
				fkFieldCombo.resetStore();
				fkFieldCombo.detailPageClass = entitySystemTableDetailScreenClass ? entitySystemTableDetailScreenClass : false;
			}
		},

		items: [
			{name: 'systemFieldMapping.fieldMappingDefinition.entityListUrl', hidden: true, submitValue: false},
			{name: 'systemFieldMapping.fieldMappingDefinition.entitySystemTable.entityListUrl', hidden: true, submitValue: false},
			{name: 'systemFieldMapping.fieldMappingDefinition.entitySystemTable.label', hidden: true, submitValue: false},
			{name: 'systemFieldMapping.fieldMappingDefinition.entitySystemTable.detailScreenClass', hidden: true, submitValue: false},
			{name: 'systemFieldMapping.fieldMappingDefinition.dataType.name', hidden: true, submitValue: false},

			{
				fieldLabel: 'Definition', name: 'systemFieldMapping.fieldMappingDefinition.name', hiddenName: 'systemFieldMapping.fieldMappingDefinition.id', submitValue: false, displayField: 'name', xtype: 'combo', loadAll: false, url: 'systemFieldMappingDefinitionListFind.json', detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingDefinitionWindow',
				limitRequestedProperties: false,
				listeners: {
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						fp.resetFieldsForDefinition(record.json);
						fp.resetValueField(record.json);
					}
				}
			},

			{
				fieldLabel: 'Field Mapping', name: 'systemFieldMapping.name', hiddenName: 'systemFieldMapping.id', xtype: 'combo', disableAddNewItem: true, url: 'systemFieldMappingListFind.json', requiredFields: ['systemFieldMapping.fieldMappingDefinition.name'], detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingWindow',
				listeners: {
					beforequery: function(queryEvent) {
						const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
						const definitionId = form.findField('systemFieldMapping.fieldMappingDefinition.id').value;
						queryEvent.combo.store.setBaseParam('fieldMappingDefinitionId', definitionId);
					}
				}
			},
			{fieldLabel: 'Linked Entity', name: 'fkFieldLabel', hiddenName: 'fkFieldId', xtype: 'combo', detailIdField: 'fkFieldId', submitValue: true, displayField: 'label', url: 'REPLACE_ME_DYNAMICALLY', detailPageClass: 'REPLACE_ME_DYNAMICALLY', requiredFields: ['systemFieldMapping.fieldMappingDefinition.name']},
			{xtype: 'formfragment', frame: false, items: [], name: 'valueFormFragment', bodyStyle: 'padding: 0', anchor: '-15'},
			{fieldLabel: 'Priority Order', name: 'priorityOrder', xtype: 'numberfield'}
		]

	}]
});
