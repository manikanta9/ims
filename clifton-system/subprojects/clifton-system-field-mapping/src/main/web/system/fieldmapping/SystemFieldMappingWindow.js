Clifton.system.fieldmapping.SystemFieldMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Field Mapping',
	iconCls: 'shopping-cart',
	width: 800,
	height: 500,

	items: [
		{
			xtype: 'tabpanel',
			requiredFormIndex: 0,
			items: [
				{
					title: 'Info',
					items: [{
						xtype: 'formpanel',
						labelWidth: 150,
						instructions: 'System Field Mappings are a grouping for System Field Mapping Entries for a particular System Field Mapping Definition. For example, we define a mapping for FX Connect that groups all of the Broker Company Mappings used when sending orders via FIX to FX Connect.',
						url: 'systemFieldMapping.json',
						items: [
							{fieldLabel: 'Mapping Definition', name: 'fieldMappingDefinition.name', hiddenName: 'fieldMappingDefinition.id', xtype: 'combo', url: 'systemFieldMappingDefinitionListFind.json', detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingDefinitionWindow'},
							{fieldLabel: 'Mapping Name', name: 'name'},
							{fieldLabel: 'Mapping Description', name: 'description', xtype: 'textarea', height: 50}
						]
					}]
				},

				{
					title: 'Field Mapping Entries',
					xtype: 'system-field-mapping-entry-grid',
					columnOverrides: [
						{dataIndex: 'systemFieldMapping.fieldMappingDefinition.name', hidden: true},
						{dataIndex: 'systemFieldMapping.name', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingEntryWindow',
						getDefaultData: function(gridPanel) {
							return {
								systemFieldMapping: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {
							systemFieldMappingId: this.getWindow().getMainFormId()
						};
					}
				}
			]
		}

	]
});
