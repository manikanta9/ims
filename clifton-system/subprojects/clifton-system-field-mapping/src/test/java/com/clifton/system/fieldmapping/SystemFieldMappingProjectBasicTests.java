package com.clifton.system.fieldmapping;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;


/**
 * @author Abhinaya
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemFieldMappingProjectBasicTests extends BasicProjectTests {


	@Override
	public String getProjectPrefix() {
		return "system-fieldMapping";
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}

	@Override
	protected void configureDTOSkipPropertyNames(Set<String> skipPropertyNames) {
		skipPropertyNames.add("order");
		skipPropertyNames.add("required");
	}
}
