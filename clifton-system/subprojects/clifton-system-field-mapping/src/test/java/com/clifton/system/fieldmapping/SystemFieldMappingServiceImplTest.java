package com.clifton.system.fieldmapping;

import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.SystemSchemaService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author AbhinayaM
 */

@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
class SystemFieldMappingServiceImplTest {


	private static final int SYSTEM_FIELD_MAPPING_DEFINITION_MODIFY_CONDITION_ID = 14;
	private static final String SYSTEM_FIELD_MAPPING_DEFINITION_TABLE = "TestTable";
	private static final String SYSTEM_FIELD_MAPPING_DEFINITION_ENTITY_URL = "orderCompanyListFind.json?categoryName=Company Tags&amp;categoryHierarchyName=Broker";
	private static final short STRING_ID = 1;

	@Resource
	private SystemFieldMappingService systemFieldMappingService;

	@Resource
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;


	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private SystemConditionService systemConditionService;


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}


	/**
	 * Test case to prove that a new SystemFieldMappingDefinition cannot be created
	 */
	@Test
	public void testSaveDefinition_WithFieldMappingDefinition_CreateFailure() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("Test Condition Failure Message");
			SystemFieldMappingDefinition fieldMappingDefinition = new SystemFieldMappingDefinition();
			fieldMappingDefinition.setName("");
			fieldMappingDefinition.setMappingEntityModifyCondition(this.systemConditionService.getSystemCondition(8));
			fieldMappingDefinition.setEntityListUrl("");
			fieldMappingDefinition.setDataType(this.systemSchemaService.getSystemDataType(STRING_ID));
			this.systemFieldMappingService.saveSystemFieldMappingDefinition(fieldMappingDefinition);
		});
		Assertions.assertEquals("You cannot insert a new System Field Mapping Definition", ve.getMessage());
	}


	/**
	 * Test case to prove that  we cannot delete an existing  SystemFieldMappingDefinition
	 */
	@Test
	public void testSaveDefinition_WithFieldMappingDefinition_DeleteFailure() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
			SystemFieldMappingDefinition fieldMappingDefinition = this.systemFieldMappingService.getSystemFieldMappingDefinitionByName("Broker Mappings");

			// Delete
			this.systemFieldMappingService.deleteSystemFieldMappingDefinition(fieldMappingDefinition.getId());
		});
		Assertions.assertEquals("You cannot delete an existing System Field Mapping Definition", ve.getMessage());
	}


	/**
	 * Test case to prove that  it fails when we try to update Definition Name
	 */
	@Test
	public void testSaveDefinition_WithFieldMappingDefinitionName_UpdateFailure() {
		TestUtils.expectException(ValidationException.class, () -> {
			SystemFieldMappingDefinition fieldMappingDefinition = this.systemFieldMappingService.getSystemFieldMappingDefinitionByName("Broker Mappings");
			fieldMappingDefinition.setName("This should not save");
			this.systemFieldMappingService.saveSystemFieldMappingDefinition(fieldMappingDefinition);
		}, "You cannot change the Definition name of an existing System Field Mapping Definition");
	}


	/**
	 * Test case to prove that  it fails when we try to update Table
	 */
	@Test
	public void testSaveDefinition_WithFieldMappingTableName_UpdateFailure() {
		TestUtils.expectException(ValidationException.class, () -> {
			SystemFieldMappingDefinition fieldMappingDefinition = this.systemFieldMappingService.getSystemFieldMappingDefinitionByName("Broker Mappings");
			fieldMappingDefinition.setEntitySystemTable(this.systemSchemaService.getSystemTableByName(SYSTEM_FIELD_MAPPING_DEFINITION_TABLE));
			this.systemFieldMappingService.saveSystemFieldMappingDefinition(fieldMappingDefinition);
		}, "You cannot change the table of an existing System Field Mapping Definition");
	}


	/**
	 * Test case to prove that  it fails when we try to update Value Data Type
	 */
	@Test
	public void testSaveDefinition_WithFieldMappingValueDataType_UpdateFailure() {
		TestUtils.expectException(ValidationException.class, () -> {
			SystemFieldMappingDefinition fieldMappingDefinition = this.systemFieldMappingService.getSystemFieldMappingDefinitionByName("Broker Mappings");
			fieldMappingDefinition.setDataType(this.systemSchemaService.getSystemDataType(STRING_ID));
			this.systemFieldMappingService.saveSystemFieldMappingDefinition(fieldMappingDefinition);
		}, "You cannot change the Data Type of an existing System Field Mapping Definition");
	}


	/**
	 * Test case to prove successful update by updating Modify Condition/EntityURL/ValueDataType
	 */
	@Test
	public void testSaveDefinition_WithFieldMappingDefinition_UpdateSuccess() {
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any())).thenReturn("");
		SystemFieldMappingDefinition fieldMappingDefinition = this.systemFieldMappingService.getSystemFieldMappingDefinitionByName("Broker Mappings");
		// Update
		fieldMappingDefinition.setMappingEntityModifyCondition(this.systemConditionService.getSystemCondition(SYSTEM_FIELD_MAPPING_DEFINITION_MODIFY_CONDITION_ID));
		fieldMappingDefinition.setEntityListUrl(SYSTEM_FIELD_MAPPING_DEFINITION_ENTITY_URL);

		this.systemFieldMappingService.saveSystemFieldMappingDefinition(fieldMappingDefinition);
	}


	@AfterEach
	void tearDown() {
	}
}
