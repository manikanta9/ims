package com.clifton.system.statistic;

import com.clifton.core.beans.NamedSimpleEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.web.stats.SystemRequestStats;

import java.util.List;


/**
 * The SystemStatisticSource class represents a specific SouRce accessed by a user or asynchronously run system process.
 * It is used to normalize storage and simplify filtering and grouping.
 */
@CacheByName
public class SystemStatisticSource extends NamedSimpleEntity<Short> {

	/**
	 * indicates all items for this source are presented as a Single URI
	 */
	private boolean singleURI;

	// optional SLA fields: use to find unusual behavior
	private Integer minDailyRequestCount; // value lower than this triggers SLA
	private Integer maxDailyRequestCount; // value higher than this triggers SLA
	private Integer minDailyTotalTimeSeconds; // value lower than this triggers SLA
	private Integer maxDailyTotalTimeSeconds; // value higher than this triggers SLA
	private Integer minAverageRequestTimeMillis; // value higher than this triggers SLA
	private Integer maxAverageRequestTimeMillis; // value higher than this triggers SLA
	private Integer maxRequestTimeMillis; // value higher than this triggers SLA


	// All URI's associated with this source.
	private List<SystemStatisticUri> systemStatisticUriList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static SystemStatisticSource of(SystemRequestStats requestStats) {
		SystemStatisticSource statisticSource = new SystemStatisticSource();
		statisticSource.setName(requestStats.getRequestSource());
		statisticSource.setSingleURI(false);
		return statisticSource;
	}


	/**
	 * Returns true if at least one SLA field is defined.
	 */
	public boolean isSlaPresent() {
		return ObjectUtils.isNotNullPresent(getMinDailyRequestCount(), getMaxDailyRequestCount(), getMinDailyTotalTimeSeconds(), getMaxDailyTotalTimeSeconds(), getMinAverageRequestTimeMillis(), getMaxAverageRequestTimeMillis(), getMaxRequestTimeMillis());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSingleURI() {
		return this.singleURI;
	}


	public void setSingleURI(boolean singleURI) {
		this.singleURI = singleURI;
	}


	public Integer getMinDailyRequestCount() {
		return this.minDailyRequestCount;
	}


	public void setMinDailyRequestCount(Integer minDailyRequestCount) {
		this.minDailyRequestCount = minDailyRequestCount;
	}


	public Integer getMaxDailyRequestCount() {
		return this.maxDailyRequestCount;
	}


	public void setMaxDailyRequestCount(Integer maxDailyRequestCount) {
		this.maxDailyRequestCount = maxDailyRequestCount;
	}


	public Integer getMinDailyTotalTimeSeconds() {
		return this.minDailyTotalTimeSeconds;
	}


	public void setMinDailyTotalTimeSeconds(Integer minDailyTotalTimeSeconds) {
		this.minDailyTotalTimeSeconds = minDailyTotalTimeSeconds;
	}


	public Integer getMaxDailyTotalTimeSeconds() {
		return this.maxDailyTotalTimeSeconds;
	}


	public void setMaxDailyTotalTimeSeconds(Integer maxDailyTotalTimeSeconds) {
		this.maxDailyTotalTimeSeconds = maxDailyTotalTimeSeconds;
	}


	public Integer getMinAverageRequestTimeMillis() {
		return this.minAverageRequestTimeMillis;
	}


	public void setMinAverageRequestTimeMillis(Integer minAverageRequestTimeMillis) {
		this.minAverageRequestTimeMillis = minAverageRequestTimeMillis;
	}


	public Integer getMaxAverageRequestTimeMillis() {
		return this.maxAverageRequestTimeMillis;
	}


	public void setMaxAverageRequestTimeMillis(Integer maxAverageRequestTimeMillis) {
		this.maxAverageRequestTimeMillis = maxAverageRequestTimeMillis;
	}


	public Integer getMaxRequestTimeMillis() {
		return this.maxRequestTimeMillis;
	}


	public void setMaxRequestTimeMillis(Integer maxRequestTimeMillis) {
		this.maxRequestTimeMillis = maxRequestTimeMillis;
	}


	public List<SystemStatisticUri> getSystemStatisticUriList() {
		return this.systemStatisticUriList;
	}


	public void setSystemStatisticUriList(List<SystemStatisticUri> systemStatisticUriList) {
		this.systemStatisticUriList = systemStatisticUriList;
	}
}
