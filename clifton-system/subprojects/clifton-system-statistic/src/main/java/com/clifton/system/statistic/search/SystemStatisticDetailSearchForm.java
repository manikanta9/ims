package com.clifton.system.statistic.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class SystemStatisticDetailSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "parameters,systemStatistic.statisticUri.statisticSource.name,systemStatistic.statisticUri.requestURI")
	private String searchPattern;

	@SearchField(searchField = "systemStatistic.id")
	private Integer systemStatisticId;

	@SearchField(searchField = "statisticRun.id", searchFieldPath = "systemStatistic")
	private Integer statisticRunId;

	@SearchField(searchField = "statisticUri.id", searchFieldPath = "systemStatistic")
	private Integer statisticUriId;

	@SearchField
	private Date logDateAndTime;
	@SearchField
	private Long durationNano;
	@SearchField(searchField = "durationNano", formula = "CONVERT(DECIMAL(19, 6), $1 / 1000000000.0)")
	private BigDecimal durationSeconds;

	@SearchField
	private Long dbDurationNano;
	@SearchField
	private Integer dbExecuteCount;

	@SearchField
	private String parameters;
	@SearchField
	private Boolean firstHit;

	@SearchField(searchField = "name", searchFieldPath = "systemStatistic.statisticUri.statisticSource")
	private String sourceName;

	@SearchField(searchField = "statisticSource.id", searchFieldPath = "systemStatistic.statisticUri")
	private Short statisticSourceId;

	@SearchField(searchField = "requestURI", searchFieldPath = "systemStatistic.statisticUri")
	private String requestURI;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getSystemStatisticId() {
		return this.systemStatisticId;
	}


	public void setSystemStatisticId(Integer systemStatisticID) {
		this.systemStatisticId = systemStatisticID;
	}


	public Integer getStatisticRunId() {
		return this.statisticRunId;
	}


	public void setStatisticRunId(Integer statisticRunId) {
		this.statisticRunId = statisticRunId;
	}


	public Integer getStatisticUriId() {
		return this.statisticUriId;
	}


	public void setStatisticUriId(Integer statisticUriId) {
		this.statisticUriId = statisticUriId;
	}


	public Date getLogDateAndTime() {
		return this.logDateAndTime;
	}


	public void setLogDateAndTime(Date logDateAndTime) {
		this.logDateAndTime = logDateAndTime;
	}


	public Long getDurationNano() {
		return this.durationNano;
	}


	public void setDurationNano(Long durationNano) {
		this.durationNano = durationNano;
	}


	public BigDecimal getDurationSeconds() {
		return this.durationSeconds;
	}


	public void setDurationSeconds(BigDecimal durationSeconds) {
		this.durationSeconds = durationSeconds;
	}


	public Long getDbDurationNano() {
		return this.dbDurationNano;
	}


	public void setDbDurationNano(Long dbDurationNano) {
		this.dbDurationNano = dbDurationNano;
	}


	public Integer getDbExecuteCount() {
		return this.dbExecuteCount;
	}


	public void setDbExecuteCount(Integer dbExecuteCount) {
		this.dbExecuteCount = dbExecuteCount;
	}


	public String getParameters() {
		return this.parameters;
	}


	public void setParameters(String parameters) {
		this.parameters = parameters;
	}


	public Boolean getFirstHit() {
		return this.firstHit;
	}


	public void setFirstHit(Boolean firstHit) {
		this.firstHit = firstHit;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Short getStatisticSourceId() {
		return this.statisticSourceId;
	}


	public void setStatisticSourceId(Short statisticSourceId) {
		this.statisticSourceId = statisticSourceId;
	}


	public String getRequestURI() {
		return this.requestURI;
	}


	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}
}
