package com.clifton.system.statistic;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.system.statistic.processor.SystemStatisticProcessorService;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemStatisticSourceObserver</code> will merge URI records as necessary.
 * A merge is deemed necessary during Updates to the when the isSingleURI goes from false to true.
 *
 * @author RogerW
 */
@Component
public class SystemStatisticSourceObserver extends SelfRegisteringDaoObserver<SystemStatisticSource> {

	SystemStatisticProcessorService systemStatisticProcessorService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<SystemStatisticSource> dao, DaoEventTypes event, SystemStatisticSource bean) {
		if (event.isUpdate()) {
			// Call this so original bean is stored
			getOriginalBean(dao, bean);
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<SystemStatisticSource> dao, DaoEventTypes event, SystemStatisticSource bean, Throwable e) {
		// If single URI flag is changed, check if statistics need to be merged.
		if (isUriMergeNecessary(dao, event, bean)) {
			getSystemStatisticProcessorService().switchToSingleURI(bean);
		}
	}


	private boolean isUriMergeNecessary(ReadOnlyDAO<SystemStatisticSource> dao, DaoEventTypes event, SystemStatisticSource bean) {
		// only on database updates
		if (!event.isUpdate()) {
			return false;
		}

		// System Statistic Source Updates
		SystemStatisticSource original = getOriginalBean(dao, bean);

		// should stats be merged?
		return !original.isSingleURI() && bean.isSingleURI();
	}


	public SystemStatisticProcessorService getSystemStatisticProcessorService() {
		return this.systemStatisticProcessorService;
	}


	public void setSystemStatisticProcessorService(SystemStatisticProcessorService systemStatisticProcessorService) {
		this.systemStatisticProcessorService = systemStatisticProcessorService;
	}
}
