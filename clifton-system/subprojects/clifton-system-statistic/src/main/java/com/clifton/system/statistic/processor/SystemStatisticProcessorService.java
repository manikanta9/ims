package com.clifton.system.statistic.processor;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import com.clifton.system.statistic.SystemStatistic;
import com.clifton.system.statistic.SystemStatisticSource;


/**
 * The <code>SystemStatisticService</code>
 *
 * @author Roger Weber
 */
public interface SystemStatisticProcessorService {

	////////////////////////////////////////////////////////////////////////////////
	/////////          System System Statistic Business Methods          ///////////
	////////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public void switchToSingleURI(SystemStatisticSource systemStatisticSource);


	@SecureMethod(dtoClass = SystemStatistic.class, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public Status generateSystemStatisticSnapshot();
}
