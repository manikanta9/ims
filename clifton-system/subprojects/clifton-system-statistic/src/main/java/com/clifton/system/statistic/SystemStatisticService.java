package com.clifton.system.statistic;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.statistic.search.SystemStatisticDetailSearchForm;
import com.clifton.system.statistic.search.SystemStatisticRunExtendedSearchForm;
import com.clifton.system.statistic.search.SystemStatisticRunSearchForm;
import com.clifton.system.statistic.search.SystemStatisticSearchForm;
import com.clifton.system.statistic.search.SystemStatisticSourceSearchForm;
import com.clifton.system.statistic.search.SystemStatisticUriSearchForm;

import java.util.List;


/**
 * The <code>SystemStatisticService</code>
 *
 * @author Roger Weber
 */
public interface SystemStatisticService {

	////////////////////////////////////////////////////////////////////////////////
	/////////              System Statistic Business Methods             ///////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemStatistic getSystemStatistic(int id);


	public List<SystemStatistic> getSystemStatisticList(SystemStatisticSearchForm searchForm);


	public SystemStatistic saveSystemStatistic(SystemStatistic bean);


	public void deleteSystemStatistic(SystemStatistic bean);


	////////////////////////////////////////////////////////////////////////////////
	/////////          System Statistic Detail Business Methods          ///////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemStatisticDetail getSystemStatisticDetail(int id);


	public List<SystemStatisticDetail> getSystemStatisticDetailList(SystemStatisticDetailSearchForm searchForm);


	public SystemStatisticDetail saveSystemStatisticDetail(SystemStatisticDetail bean);


	////////////////////////////////////////////////////////////////////////////////
	/////////        System System Statistic Uri Business Methods        ///////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemStatisticUri getSystemStatisticUri(int id);


	public List<SystemStatisticUri> getSystemStatisticUriList(SystemStatisticUriSearchForm searchForm);


	public SystemStatisticUri saveSystemStatisticUri(SystemStatisticUri bean);


	public void deleteSystemStatisticUri(SystemStatisticUri bean);


	@DoNotAddRequestMapping
	public SystemStatisticUri getFromCacheOrCreateSystemStatisticUri(String sourceName, String requestURI);


	////////////////////////////////////////////////////////////////////////////////
	/////////        System System Statistic Source Business Methods        ///////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemStatisticSource getSystemStatisticSource(short id);


	public List<SystemStatisticSource> getSystemStatisticSourceList(SystemStatisticSourceSearchForm searchForm);


	public SystemStatisticSource getSystemStatisticSourceByName(String name);


	public SystemStatisticSource saveSystemStatisticSource(SystemStatisticSource bean);


	@DoNotAddRequestMapping
	public SystemStatisticSource getFromCacheOrCreateSystemStatisticSource(String sourceName);

	////////////////////////////////////////////////////////////////////////////////
	/////////        System System Statistic Run Business Methods        ///////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemStatisticRun getSystemStatisticRun(int id);


	public List<SystemStatisticRun> getSystemStatisticRunList(SystemStatisticRunSearchForm searchForm);


	public SystemStatisticRun saveSystemStatisticRun(SystemStatisticRun bean);


	////////////////////////////////////////////////////////////////////////////////
	/////////        System System Statistic Run Extended Business Methods        ///////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = SystemStatisticRun.class)
	public List<SystemStatisticRunExtended> getSystemStatisticRunExtendedList(SystemStatisticRunExtendedSearchForm searchForm);
}
