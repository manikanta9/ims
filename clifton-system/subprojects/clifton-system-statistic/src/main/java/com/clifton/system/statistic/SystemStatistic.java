package com.clifton.system.statistic;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.core.web.stats.SystemRequestStats;

import java.math.BigDecimal;
import java.util.List;


/**
 * The SystemStatistic class represents statistics for a specific URI and specific run.
 * Statistics can be compared for the same URI over time (multiple runs).
 */
public class SystemStatistic extends BaseSimpleEntity<Integer> implements LabeledObject {

	private SystemStatisticRun statisticRun;
	private SystemStatisticUri statisticUri;

	private int executionCount;
	private int slowExecutionCount;

	private long totalTimeNano;
	private long minTotalTimeNano;
	private long maxTotalTimeNano;

	private Integer dbExecutionCount;
	private Long totalDBTimeNano;
	private Long minDBTimeNano;
	private Long maxDBTimeNano;

	// these are the 10 slowest details at the time of capture
	private List<SystemStatisticDetail> systemStatisticDetailList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * note:  does not populate reference entities, just properties
	 */
	public static SystemStatistic of(SystemRequestStats systemRequestStats) {
		SystemStatistic systemStatistic = new SystemStatistic();
		systemStatistic.setExecutionCount(systemRequestStats.getExecuteCount());
		systemStatistic.setSlowExecutionCount(systemRequestStats.getSlowExecuteCount());
		systemStatistic.setTotalDBTimeNano(systemRequestStats.getTotalDBTimeNano());
		systemStatistic.setTotalTimeNano(systemRequestStats.getTotalTimeNano());
		systemStatistic.setDbExecutionCount(systemRequestStats.getDbExecuteCount());
		systemStatistic.setMinDBTimeNano(systemRequestStats.getMinDBTimeNano());
		systemStatistic.setMinTotalTimeNano(systemRequestStats.getMinTotalTimeNano());
		systemStatistic.setMaxDBTimeNano(systemRequestStats.getMaxDBTimeNano());
		systemStatistic.setMaxTotalTimeNano(systemRequestStats.getMaxTotalTimeNano());
		return systemStatistic;
	}


	@Override
	public String getLabel() {
		String result = (this.statisticUri != null) ? this.statisticUri.getRequestURI() : "NO URI";
		result += " (";
		result += (this.statisticRun != null) ? DateUtils.fromDate(this.statisticRun.getRunStartDateAndTime()) : "NO RUN";
		result += ')';
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Computed Getters                                ////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getTotalTimeSeconds() {
		return TimeUtils.convertNanoToSeconds(getTotalTimeNano());
	}


	public BigDecimal getMinTotalTimeSeconds() {
		return TimeUtils.convertNanoToSeconds(getMinTotalTimeNano());
	}


	public BigDecimal getMaxTotalTimeSeconds() {
		return TimeUtils.convertNanoToSeconds(getMaxTotalTimeNano());
	}


	public BigDecimal getAvgTimeNano() {
		return MathUtils.divide(getTotalTimeNano(), getExecutionCount());
	}


	public BigDecimal getAvgTimeSeconds() {
		return MathUtils.divide(getTotalTimeSeconds(), getExecutionCount());
	}


	public String getAvgTimeFormatted() {
		return getAvgTimeSeconds() == null ? "" : DateUtils.getTimeDifference(TimeUtils.convertSecondToMilli(getAvgTimeSeconds()), false);
	}


	public BigDecimal getTotalDBTimeSeconds() {
		return getTotalDBTimeNano() == null ? null : TimeUtils.convertNanoToSeconds(getTotalDBTimeNano());
	}


	public BigDecimal getAvgDBTimeSeconds() {
		// guard against divide by zero
		return getDbExecutionCount() == null || getDbExecutionCount() == 0 ? null : MathUtils.divide(getTotalDBTimeSeconds(), getDbExecutionCount());
	}


	public BigDecimal getMinDBTimeSeconds() {
		return getMinDBTimeNano() == null ? null : TimeUtils.convertNanoToSeconds(getMinDBTimeNano());
	}


	public BigDecimal getMaxDBTimeSeconds() {
		return getMaxDBTimeNano() == null ? null : TimeUtils.convertNanoToSeconds(getMaxDBTimeNano());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemStatisticRun getStatisticRun() {
		return this.statisticRun;
	}


	public void setStatisticRun(SystemStatisticRun statisticRun) {
		this.statisticRun = statisticRun;
	}


	public SystemStatisticUri getStatisticUri() {
		return this.statisticUri;
	}


	public void setStatisticUri(SystemStatisticUri statisticUri) {
		this.statisticUri = statisticUri;
	}


	public List<SystemStatisticDetail> getSystemStatisticDetailList() {
		return this.systemStatisticDetailList;
	}


	public void setSystemStatisticDetailList(List<SystemStatisticDetail> systemStatisticDetailList) {
		this.systemStatisticDetailList = systemStatisticDetailList;
	}

	// execution count


	public int getExecutionCount() {
		return this.executionCount;
	}


	public void setExecutionCount(int executionCount) {
		this.executionCount = executionCount;
	}


	public int getSlowExecutionCount() {
		return this.slowExecutionCount;
	}


	public void setSlowExecutionCount(int slowExecutionCount) {
		this.slowExecutionCount = slowExecutionCount;
	}

	// execution time


	public long getTotalTimeNano() {
		return this.totalTimeNano;
	}


	public void setTotalTimeNano(long totalTimeNano) {
		this.totalTimeNano = totalTimeNano;
	}


	public long getMinTotalTimeNano() {
		return this.minTotalTimeNano;
	}


	public void setMinTotalTimeNano(long minTotalTimeNano) {
		this.minTotalTimeNano = minTotalTimeNano;
	}


	public long getMaxTotalTimeNano() {
		return this.maxTotalTimeNano;
	}


	public void setMaxTotalTimeNano(long maxTotalTimeNano) {
		this.maxTotalTimeNano = maxTotalTimeNano;
	}

	// db execution count


	public Integer getDbExecutionCount() {
		return this.dbExecutionCount;
	}


	public void setDbExecutionCount(Integer dbExecutionCount) {
		this.dbExecutionCount = dbExecutionCount;
	}


	// db time


	public Long getTotalDBTimeNano() {
		return this.totalDBTimeNano;
	}


	public void setTotalDBTimeNano(Long totalDBTimeNano) {
		this.totalDBTimeNano = totalDBTimeNano;
	}


	public Long getMinDBTimeNano() {
		return this.minDBTimeNano;
	}


	public void setMinDBTimeNano(Long minDBTimeNano) {
		this.minDBTimeNano = minDBTimeNano;
	}


	public Long getMaxDBTimeNano() {
		return this.maxDBTimeNano;
	}


	public void setMaxDBTimeNano(Long maxDBTimeNano) {
		this.maxDBTimeNano = maxDBTimeNano;
	}
}
