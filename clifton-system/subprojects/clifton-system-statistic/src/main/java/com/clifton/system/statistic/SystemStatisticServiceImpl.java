package com.clifton.system.statistic;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.SubselectParameterExpression;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.system.statistic.cache.SystemStatisticUriBySourceAndUriCache;
import com.clifton.system.statistic.search.SystemStatisticDetailSearchForm;
import com.clifton.system.statistic.search.SystemStatisticRunExtendedSearchForm;
import com.clifton.system.statistic.search.SystemStatisticRunSearchForm;
import com.clifton.system.statistic.search.SystemStatisticSearchForm;
import com.clifton.system.statistic.search.SystemStatisticSourceSearchForm;
import com.clifton.system.statistic.search.SystemStatisticUriSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.IntegerType;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>SystemStatisticServiceImpl</code> is used to store, and retrieve system performance statistics.
 *
 * @author Roger Weber
 */
@Service
public class SystemStatisticServiceImpl implements SystemStatisticService {

	private AdvancedUpdatableDAO<SystemStatisticRun, Criteria> systemStatisticRunDAO;
	private AdvancedReadOnlyDAO<SystemStatisticRunExtended, Criteria> systemStatisticRunExtendedDAO;
	private AdvancedUpdatableDAO<SystemStatistic, Criteria> systemStatisticDAO;
	private AdvancedUpdatableDAO<SystemStatisticDetail, Criteria> systemStatisticDetailDAO;
	private AdvancedUpdatableDAO<SystemStatisticUri, Criteria> systemStatisticUriDAO;
	private AdvancedUpdatableDAO<SystemStatisticSource, Criteria> systemStatisticSourceDAO;
	private SystemStatisticUriBySourceAndUriCache systemStatisticUriBySourceAndUriCache;
	private DaoNamedEntityCache<SystemStatisticSource> systemStatisticSourceCache;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemStatistic getSystemStatistic(int id) {
		SystemStatistic systemStatistic = getSystemStatisticDAO().findByPrimaryKey(id);
		if (systemStatistic != null) {
			systemStatistic.setSystemStatisticDetailList(getSystemStatisticDetailDAO().findByField("systemStatistic.id", id));
		}
		return systemStatistic;
	}


	@Override
	public List<SystemStatistic> getSystemStatisticList(SystemStatisticSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getSelectWeekdays() != null) {
					// add weekdays restriction
					String runAlias = getPathAlias("statisticRun", criteria) + "x1_"; // NOTE: must add hard-coded suffix with custom sqlRestriction: Hibernate bug?
					criteria.add(Restrictions.sqlRestriction("DATENAME(DW, " + runAlias + ".RunStartDateAndTime) " + (BooleanUtils.isTrue(searchForm.getSelectWeekdays()) ? "NOT IN" : "IN") + " ('Saturday', 'Sunday')"));
				}
			}
		};

		return getSystemStatisticDAO().findBySearchCriteria(config);
	}


	@Override
	public SystemStatistic saveSystemStatistic(SystemStatistic bean) {
		return getSystemStatisticDAO().save(bean);
	}


	@Override
	public void deleteSystemStatistic(SystemStatistic bean) {
		getSystemStatisticDAO().delete(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemStatisticDetail getSystemStatisticDetail(int id) {
		return getSystemStatisticDetailDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemStatisticDetail> getSystemStatisticDetailList(SystemStatisticDetailSearchForm searchForm) {
		return getSystemStatisticDetailDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemStatisticDetail saveSystemStatisticDetail(SystemStatisticDetail bean) {
		return getSystemStatisticDetailDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemStatisticUri getSystemStatisticUri(int id) {
		return getSystemStatisticUriDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemStatisticUri> getSystemStatisticUriList(SystemStatisticUriSearchForm searchForm) {
		return getSystemStatisticUriDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemStatisticUri saveSystemStatisticUri(SystemStatisticUri bean) {
		return getSystemStatisticUriDAO().save(bean);
	}


	@Override
	public void deleteSystemStatisticUri(SystemStatisticUri bean) {
		getSystemStatisticUriDAO().delete(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemStatisticSource getSystemStatisticSource(short id) {
		return getSystemStatisticSourceDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemStatisticSource> getSystemStatisticSourceList(SystemStatisticSourceSearchForm searchForm) {
		return getSystemStatisticSourceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemStatisticSource getSystemStatisticSourceByName(String name) {
		return getSystemStatisticSourceCache().getBeanForKeyValue(getSystemStatisticSourceDAO(), name);
	}


	@Override
	public SystemStatisticSource saveSystemStatisticSource(SystemStatisticSource bean) {
		return getSystemStatisticSourceDAO().save(bean);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemStatisticRun getSystemStatisticRun(int id) {
		return getSystemStatisticRunDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemStatisticRun> getSystemStatisticRunList(SystemStatisticRunSearchForm searchForm) {
		return getSystemStatisticRunDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemStatisticRun saveSystemStatisticRun(SystemStatisticRun bean) {
		return getSystemStatisticRunDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemStatisticUri getFromCacheOrCreateSystemStatisticUri(String sourceName, String requestURI) {
		SystemStatisticUri statisticUri = getSystemStatisticUriBySourceAndUriCache().getBeanForKeyValues(getSystemStatisticUriDAO(), sourceName, requestURI);
		// if it does not exists, create one
		if (statisticUri == null) {
			statisticUri = new SystemStatisticUri();
			statisticUri.setRequestURI(requestURI);
			SystemStatisticSource statisticSource = getFromCacheOrCreateSystemStatisticSource(sourceName);
			statisticUri.setStatisticSource(statisticSource);
			statisticUri = saveSystemStatisticUri(statisticUri);

			getSystemStatisticUriBySourceAndUriCache().setBean(statisticUri);
		}
		return statisticUri;
	}


	@Override
	public SystemStatisticSource getFromCacheOrCreateSystemStatisticSource(String sourceName) {
		SystemStatisticSource statisticSource = getSystemStatisticSourceCache().getBeanForKeyValue(getSystemStatisticSourceDAO(), sourceName);
		// if it does not exists, create one
		if (statisticSource == null) {
			statisticSource = new SystemStatisticSource();
			statisticSource.setName(sourceName);
			statisticSource.setSingleURI(false);
			statisticSource = saveSystemStatisticSource(statisticSource);
			getSystemStatisticSourceCache().put(statisticSource.getName(), statisticSource);
		}
		return statisticSource;
	}


	@Override
	public List<SystemStatisticRunExtended> getSystemStatisticRunExtendedList(SystemStatisticRunExtendedSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(new SubselectParameterExpression(new IntegerType(), searchForm.getSourceId()));
				criteria.add(new SubselectParameterExpression(new IntegerType(), searchForm.getSourceId()));

				if (searchForm.getSelectWeekdays() != null) {
					// add weekdays restriction
					criteria.add(Restrictions.sqlRestriction("DATENAME(DW, RunStartDateAndTime) " + (BooleanUtils.isTrue(searchForm.getSelectWeekdays()) ? "NOT IN" : "IN") + " ('Saturday', 'Sunday')"));
				}

				super.configureCriteria(criteria);
			}
		};
		return getSystemStatisticRunExtendedDAO().findBySearchCriteria(config);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemStatisticRun, Criteria> getSystemStatisticRunDAO() {
		return this.systemStatisticRunDAO;
	}


	public void setSystemStatisticRunDAO(AdvancedUpdatableDAO<SystemStatisticRun, Criteria> systemStatisticRunDAO) {
		this.systemStatisticRunDAO = systemStatisticRunDAO;
	}


	public AdvancedUpdatableDAO<SystemStatistic, Criteria> getSystemStatisticDAO() {
		return this.systemStatisticDAO;
	}


	public AdvancedReadOnlyDAO<SystemStatisticRunExtended, Criteria> getSystemStatisticRunExtendedDAO() {
		return this.systemStatisticRunExtendedDAO;
	}


	public void setSystemStatisticRunExtendedDAO(AdvancedReadOnlyDAO<SystemStatisticRunExtended, Criteria> systemStatisticRunExtendedDAO) {
		this.systemStatisticRunExtendedDAO = systemStatisticRunExtendedDAO;
	}


	public void setSystemStatisticDAO(AdvancedUpdatableDAO<SystemStatistic, Criteria> systemStatisticDAO) {
		this.systemStatisticDAO = systemStatisticDAO;
	}


	public AdvancedUpdatableDAO<SystemStatisticDetail, Criteria> getSystemStatisticDetailDAO() {
		return this.systemStatisticDetailDAO;
	}


	public void setSystemStatisticDetailDAO(AdvancedUpdatableDAO<SystemStatisticDetail, Criteria> systemStatisticDetailDAO) {
		this.systemStatisticDetailDAO = systemStatisticDetailDAO;
	}


	public AdvancedUpdatableDAO<SystemStatisticUri, Criteria> getSystemStatisticUriDAO() {
		return this.systemStatisticUriDAO;
	}


	public void setSystemStatisticUriDAO(AdvancedUpdatableDAO<SystemStatisticUri, Criteria> systemStatisticUriDAO) {
		this.systemStatisticUriDAO = systemStatisticUriDAO;
	}


	public SystemStatisticUriBySourceAndUriCache getSystemStatisticUriBySourceAndUriCache() {
		return this.systemStatisticUriBySourceAndUriCache;
	}


	public void setSystemStatisticUriBySourceAndUriCache(SystemStatisticUriBySourceAndUriCache systemStatisticUriBySourceAndUriCache) {
		this.systemStatisticUriBySourceAndUriCache = systemStatisticUriBySourceAndUriCache;
	}


	public void setSystemStatisticSourceDAO(AdvancedUpdatableDAO<SystemStatisticSource, Criteria> systemStatisticSourceDAO) {
		this.systemStatisticSourceDAO = systemStatisticSourceDAO;
	}


	public AdvancedUpdatableDAO<SystemStatisticSource, Criteria> getSystemStatisticSourceDAO() {
		return this.systemStatisticSourceDAO;
	}


	public DaoNamedEntityCache<SystemStatisticSource> getSystemStatisticSourceCache() {
		return this.systemStatisticSourceCache;
	}


	public void setSystemStatisticSourceCache(DaoNamedEntityCache<SystemStatisticSource> systemStatisticSourceCache) {
		this.systemStatisticSourceCache = systemStatisticSourceCache;
	}
}
