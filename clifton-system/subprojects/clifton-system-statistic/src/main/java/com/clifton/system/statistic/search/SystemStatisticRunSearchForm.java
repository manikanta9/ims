package com.clifton.system.statistic.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * @author RogerW
 */
public class SystemStatisticRunSearchForm extends BaseEntitySearchForm {


	@SearchField(dateFieldIncludesTime = true)
	private Date runStartDateAndTime;


	@SearchField(dateFieldIncludesTime = true)
	private Date runEndDateAndTime;


	@SearchField
	private String description;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Date getRunStartDateAndTime() {
		return this.runStartDateAndTime;
	}


	public void setRunStartDateAndTime(Date runStartDateAndTime) {
		this.runStartDateAndTime = runStartDateAndTime;
	}


	public Date getRunEndDateAndTime() {
		return this.runEndDateAndTime;
	}


	public void setRunEndDateAndTime(Date runEndDateAndTime) {
		this.runEndDateAndTime = runEndDateAndTime;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
