package com.clifton.system.statistic.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


public class SystemStatisticUriSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "requestURI")
	private String searchPattern;


	@SearchField(searchField = "name", searchFieldPath = "statisticSource")
	private String sourceName;

	@SearchField(searchField = "statisticSource.id")
	private Short sourceId;

	@SearchField
	private String requestURI;


	@SearchField(searchField = "name", searchFieldPath = "systemTable")
	private String tableName;

	@SearchField(searchField = "systemTable.id")
	private Short tableId;


	@SearchField
	private Integer minDailyRequestCount;
	@SearchField
	private Integer maxDailyRequestCount;
	@SearchField
	private Integer minDailyTotalTimeSeconds;
	@SearchField
	private Integer maxDailyTotalTimeSeconds;
	@SearchField
	private Integer minAverageRequestTimeMillis;
	@SearchField
	private Integer maxAverageRequestTimeMillis;
	@SearchField
	private Integer maxRequestTimeMillis;

	// at least one SLA metric is populated
	@SearchField(searchField = "minDailyRequestCount,maxDailyRequestCount,minDailyTotalTimeSeconds,maxDailyTotalTimeSeconds,minAverageRequestTimeMillis,maxAverageRequestTimeMillis,maxRequestTimeMillis", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean slaPresent;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}


	public String getRequestURI() {
		return this.requestURI;
	}


	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public Integer getMinDailyRequestCount() {
		return this.minDailyRequestCount;
	}


	public void setMinDailyRequestCount(Integer minDailyRequestCount) {
		this.minDailyRequestCount = minDailyRequestCount;
	}


	public Integer getMaxDailyRequestCount() {
		return this.maxDailyRequestCount;
	}


	public void setMaxDailyRequestCount(Integer maxDailyRequestCount) {
		this.maxDailyRequestCount = maxDailyRequestCount;
	}


	public Integer getMinDailyTotalTimeSeconds() {
		return this.minDailyTotalTimeSeconds;
	}


	public void setMinDailyTotalTimeSeconds(Integer minDailyTotalTimeSeconds) {
		this.minDailyTotalTimeSeconds = minDailyTotalTimeSeconds;
	}


	public Integer getMaxDailyTotalTimeSeconds() {
		return this.maxDailyTotalTimeSeconds;
	}


	public void setMaxDailyTotalTimeSeconds(Integer maxDailyTotalTimeSeconds) {
		this.maxDailyTotalTimeSeconds = maxDailyTotalTimeSeconds;
	}


	public Integer getMinAverageRequestTimeMillis() {
		return this.minAverageRequestTimeMillis;
	}


	public void setMinAverageRequestTimeMillis(Integer minAverageRequestTimeMillis) {
		this.minAverageRequestTimeMillis = minAverageRequestTimeMillis;
	}


	public Integer getMaxAverageRequestTimeMillis() {
		return this.maxAverageRequestTimeMillis;
	}


	public void setMaxAverageRequestTimeMillis(Integer maxAverageRequestTimeMillis) {
		this.maxAverageRequestTimeMillis = maxAverageRequestTimeMillis;
	}


	public Integer getMaxRequestTimeMillis() {
		return this.maxRequestTimeMillis;
	}


	public void setMaxRequestTimeMillis(Integer maxRequestTimeMillis) {
		this.maxRequestTimeMillis = maxRequestTimeMillis;
	}


	public Boolean getSlaPresent() {
		return this.slaPresent;
	}


	public void setSlaPresent(Boolean slaPresent) {
		this.slaPresent = slaPresent;
	}
}
