package com.clifton.system.statistic.cache;

import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.system.statistic.SystemStatisticUri;


/**
 * @author RogerW
 */
public interface SystemStatisticUriBySourceAndUriCache extends DaoCompositeKeyCache<SystemStatisticUri, String, String> {


	public void setBean(SystemStatisticUri bean);
}
