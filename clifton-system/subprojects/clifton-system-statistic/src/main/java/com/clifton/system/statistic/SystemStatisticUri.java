package com.clifton.system.statistic;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.web.stats.SystemRequestStats;
import com.clifton.system.schema.SystemTable;

import java.util.List;


/**
 * The SystemStatisticUri class represents a specific URL accessed by a user or asynchronously run system process.
 * It is used to normalize storage and simplify filtering and grouping.
 */
public class SystemStatisticUri extends BaseSimpleEntity<Integer> {

	private SystemStatisticSource statisticSource;
	private String requestURI;
	private SystemTable systemTable; // (foreign key to the SystemTable table) not required


	// optional SLA fields: use to find unusual behavior
	private Integer minDailyRequestCount; // value lower than this triggers SLA
	private Integer maxDailyRequestCount; // value higher than this triggers SLA
	private Integer minDailyTotalTimeSeconds; // value lower than this triggers SLA
	private Integer maxDailyTotalTimeSeconds; // value higher than this triggers SLA
	private Integer minAverageRequestTimeMillis; // value higher than this triggers SLA
	private Integer maxAverageRequestTimeMillis; // value higher than this triggers SLA
	private Integer maxRequestTimeMillis; // value higher than this triggers SLA


	// All statistics associated with this URI.
	private List<SystemStatistic> systemStatisticList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * note:  does not populate reference entities, just properties
	 */
	public static SystemStatisticUri of(SystemRequestStats requestStats) {
		SystemStatisticUri statisticUri = new SystemStatisticUri();
		statisticUri.setRequestURI(requestStats.getRequestURI());
		return statisticUri;
	}


	/**
	 * Returns true if at least one SLA field is defined.
	 */
	public boolean isSlaPresent() {
		return ObjectUtils.isNotNullPresent(getMinDailyRequestCount(), getMaxDailyRequestCount(), getMinDailyTotalTimeSeconds(), getMaxDailyTotalTimeSeconds(), getMinAverageRequestTimeMillis(), getMaxAverageRequestTimeMillis(), getMaxRequestTimeMillis());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemStatisticSource getStatisticSource() {
		return this.statisticSource;
	}


	public void setStatisticSource(SystemStatisticSource statisticSource) {
		this.statisticSource = statisticSource;
	}


	public String getRequestURI() {
		return this.requestURI;
	}


	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	public List<SystemStatistic> getSystemStatisticList() {
		return this.systemStatisticList;
	}


	public void setSystemStatisticList(List<SystemStatistic> systemStatisticList) {
		this.systemStatisticList = systemStatisticList;
	}


	public Integer getMinDailyRequestCount() {
		return this.minDailyRequestCount;
	}


	public void setMinDailyRequestCount(Integer minDailyRequestCount) {
		this.minDailyRequestCount = minDailyRequestCount;
	}


	public Integer getMaxDailyRequestCount() {
		return this.maxDailyRequestCount;
	}


	public void setMaxDailyRequestCount(Integer maxDailyRequestCount) {
		this.maxDailyRequestCount = maxDailyRequestCount;
	}


	public Integer getMinDailyTotalTimeSeconds() {
		return this.minDailyTotalTimeSeconds;
	}


	public void setMinDailyTotalTimeSeconds(Integer minDailyTotalTimeSeconds) {
		this.minDailyTotalTimeSeconds = minDailyTotalTimeSeconds;
	}


	public Integer getMaxDailyTotalTimeSeconds() {
		return this.maxDailyTotalTimeSeconds;
	}


	public void setMaxDailyTotalTimeSeconds(Integer maxDailyTotalTimeSeconds) {
		this.maxDailyTotalTimeSeconds = maxDailyTotalTimeSeconds;
	}


	public Integer getMinAverageRequestTimeMillis() {
		return this.minAverageRequestTimeMillis;
	}


	public void setMinAverageRequestTimeMillis(Integer minAverageRequestTimeMillis) {
		this.minAverageRequestTimeMillis = minAverageRequestTimeMillis;
	}


	public Integer getMaxAverageRequestTimeMillis() {
		return this.maxAverageRequestTimeMillis;
	}


	public void setMaxAverageRequestTimeMillis(Integer maxAverageRequestTimeMillis) {
		this.maxAverageRequestTimeMillis = maxAverageRequestTimeMillis;
	}


	public Integer getMaxRequestTimeMillis() {
		return this.maxRequestTimeMillis;
	}


	public void setMaxRequestTimeMillis(Integer maxRequestTimeMillis) {
		this.maxRequestTimeMillis = maxRequestTimeMillis;
	}
}
