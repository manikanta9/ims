package com.clifton.system.statistic;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.core.web.stats.SystemRequestStatsDetail;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The SystemStatisticDetail class represents additional details for a specific request. Usually we capture
 * up to 10 slowest request details in order to help recreate the problem.
 */
public class SystemStatisticDetail extends BaseSimpleEntity<Integer> {

	private SystemStatistic systemStatistic;

	private Date logDateAndTime;
	private long durationNano;

	private Long dbDurationNano;
	private Integer dbExecuteCount;

	private String parameters;
	private boolean firstHit;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * note:  does not populate reference entities, just properties
	 */
	public static SystemStatisticDetail of(SystemRequestStatsDetail requestStatsDetail) {
		SystemStatisticDetail statisticDetail = new SystemStatisticDetail();
		statisticDetail.setLogDateAndTime(requestStatsDetail.getLogTimeMillis());
		statisticDetail.setDurationNano(requestStatsDetail.getDurationNano());
		statisticDetail.setDbDurationNano(requestStatsDetail.getDbDurationNano());
		statisticDetail.setDbExecuteCount(requestStatsDetail.getDbExecuteCount());
		statisticDetail.setParameters(requestStatsDetail.getParameters());
		statisticDetail.setFirstHit(requestStatsDetail.isFirstHit());
		return statisticDetail;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Long getAverageDbExecuteDurationNano() {
		return null == getDbExecuteCount() || 0 == getDbExecuteCount() || null == getDbDurationNano() ? null : (getDbDurationNano() / getDbExecuteCount());
	}


	public BigDecimal getDurationSeconds() {
		return TimeUtils.convertNanoToSeconds(getDurationNano());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemStatistic getSystemStatistic() {
		return this.systemStatistic;
	}


	public void setSystemStatistic(SystemStatistic systemStatistic) {
		this.systemStatistic = systemStatistic;
	}


	public Date getLogDateAndTime() {
		return this.logDateAndTime;
	}


	public void setLogDateAndTime(long logTimeMillis) {
		this.logDateAndTime = new Date(logTimeMillis);
	}


	public void setLogDateAndTime(Date logDateAndTime) {
		this.logDateAndTime = logDateAndTime;
	}


	public long getDurationNano() {
		return this.durationNano;
	}


	public void setDurationNano(long durationNano) {
		this.durationNano = durationNano;
	}


	public Long getDbDurationNano() {
		return this.dbDurationNano;
	}


	public void setDbDurationNano(Long dbDurationNano) {
		this.dbDurationNano = dbDurationNano;
	}


	public Integer getDbExecuteCount() {
		return this.dbExecuteCount;
	}


	public void setDbExecuteCount(Integer dbExecuteCount) {
		this.dbExecuteCount = dbExecuteCount;
	}


	public String getParameters() {
		return this.parameters;
	}


	public void setParameters(String parameters) {
		this.parameters = parameters;
	}


	public boolean isFirstHit() {
		return this.firstHit;
	}


	public void setFirstHit(boolean firstHit) {
		this.firstHit = firstHit;
	}

}
