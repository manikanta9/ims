package com.clifton.system.statistic.processor;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.web.stats.SystemRequestStats;
import com.clifton.core.web.stats.SystemRequestStatsDetail;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.security.impersonation.SecurityImpersonationHandler;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.statistic.SystemStatistic;
import com.clifton.system.statistic.SystemStatisticDetail;
import com.clifton.system.statistic.SystemStatisticRun;
import com.clifton.system.statistic.SystemStatisticService;
import com.clifton.system.statistic.SystemStatisticSource;
import com.clifton.system.statistic.SystemStatisticUri;
import com.clifton.system.statistic.search.SystemStatisticDetailSearchForm;
import com.clifton.system.statistic.search.SystemStatisticRunSearchForm;
import com.clifton.system.statistic.search.SystemStatisticSearchForm;
import com.clifton.system.statistic.search.SystemStatisticUriSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author RogerW
 */
@Service
public class SystemStatisticProcessorServiceImpl implements SystemStatisticProcessorService {

	private SystemRequestStatsService systemRequestStatsService;
	private SystemStatisticService systemStatisticService;
	private SecurityImpersonationHandler securityImpersonationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generate statistic snapshot and return the result.
	 * <p>
	 * Do not annotate with outer Transactional. Doing so will cause nested transactions to hang.
	 */
	@Override
	public Status generateSystemStatisticSnapshot() {
		return doGenerateSystemStatisticSnapshot();
	}


	/**
	 * Generate statistic snapshot on application stop. Does the same logic as {@link #generateSystemStatisticSnapshot()}
	 * using the {@link SecurityImpersonationHandler} to run as the system user for auditing purposes.
	 * <p>
	 * Do not annotate with outer Transactional. Doing so will cause nested transactions to hang.
	 */
	@PreDestroy
	public void generateSystemStatisticSnapshotOnShutdown() {
		/* Run as the system user on shutdown for audit observer */
		getSecurityImpersonationHandler().runAsSecurityUserName(SecurityUser.SYSTEM_USER, this::doGenerateSystemStatisticSnapshot);
	}


	@Override
	public void switchToSingleURI(SystemStatisticSource systemStatisticSource) {
		// only do this if the flag is set correctly
		if (!systemStatisticSource.isSingleURI()) {
			throw new RuntimeException("[switchToSingleURI] called, but [" + systemStatisticSource.getId() + "] [" + systemStatisticSource.getName() + "] is already single URI.");
		}

		List<SystemStatistic> statisticsToBeUpdated = new ArrayList<>();
		List<SystemStatistic> statisticsToBeDeleted = new ArrayList<>();
		List<SystemStatisticDetail> statisticDetailsToBeUpdated = new ArrayList<>();

		// load all of the statistics for the source by Run (id)
		Map<Integer, List<SystemStatistic>> systemStatisticMap = findStatisticMap(systemStatisticSource.getId());

		// load all of the details for the source
		Map<Integer, List<SystemStatisticDetail>> statisticDetailsMap = findStatisticDetailMap(systemStatisticSource.getId());

		// get or create a new target URI to replace the existing ones.
		SystemStatisticUri targetURI = getSystemStatisticService().getFromCacheOrCreateSystemStatisticUri(systemStatisticSource.getName(), systemStatisticSource.getName());

		// iterate over the Runs
		for (Map.Entry<Integer, List<SystemStatistic>> integerListEntry : systemStatisticMap.entrySet()) {

			SystemStatistic targetStatistic = null;
			for (SystemStatistic systemStatistic : CollectionUtils.getIterable(integerListEntry.getValue())) {
				// skip if it is already assigned to the correct URI
				if (systemStatistic.getStatisticUri().getId().equals(targetURI.getId())) {
					continue;
				}

				// load the details
				if (systemStatistic.getSystemStatisticDetailList() == null) {
					systemStatistic.setSystemStatisticDetailList(statisticDetailsMap.get(systemStatistic.getId()));
				}

				targetStatistic = mergeSystemStatistic(targetStatistic, systemStatistic, targetURI, statisticsToBeUpdated, statisticsToBeDeleted, statisticDetailsToBeUpdated);
			}
		}

		List<SystemStatisticUri> statisticUrisToBeDeleted = new ArrayList<>();
		for (SystemStatisticUri statisticUri : CollectionUtils.getIterable(findStatisticUriList(systemStatisticSource.getId()))) {
			// check if it is already assigned to the correct URI
			if (statisticUri.getId().equals(targetURI.getId())) {
				continue;
			}

			// delete the unused URI.
			statisticUrisToBeDeleted.add(statisticUri);
		}

		updateSystemStatistics(statisticsToBeUpdated, statisticsToBeDeleted, statisticDetailsToBeUpdated, statisticUrisToBeDeleted);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * This method will capture statistic snapshot when the Spring container is closed.
	 * <p>
	 * The method cannot be wrapped in an outer Transactional with the nested REQUIRES_NEW transactions. If done, the nested transactions hang indefinitely.
	 * The only thought is the nested transactions use the SystemStatisticRun as a foreign key on saved statistics stats. Since the run has not been committed,
	 * the nested saves cannot find the correct entity, and timeout rather than fail.
	 */
	private Status doGenerateSystemStatisticSnapshot() {
		Status status = new Status();

		// get a write lock....
		getSystemRequestStatsService().systemRequestExclusiveLock();

		List<SystemRequestStats> statsList = null;
		int successfulSaves = 0;
		try {
			// get the start stamp from the stats service (includes time)
			Date statsLastCleared = getSystemRequestStatsService().getSystemStatsLastCleared();
			Date currentTime = new Date();

			List<SystemStatistic> currentStats = getExistingStats(statsLastCleared, status);
			Map<String, SystemStatistic> statisticMap = BeanUtils.getBeanMapUnique(currentStats, systemStatistic -> statisticMapKey(systemStatistic.getStatisticUri()));

			// get the current details into the map
			SystemStatisticRun statisticRun;

			// create new or combine stats.
			if (CollectionUtils.isEmpty(currentStats)) {
				statisticRun = new SystemStatisticRun();
				statisticRun.setRunStartDateAndTime(statsLastCleared);
			}
			else {
				statisticRun = currentStats.get(0).getStatisticRun();
			}

			statisticRun.setRunEndDateAndTime(currentTime);
			statisticRun = getSystemStatisticService().saveSystemStatisticRun(statisticRun);

			// process each item in the list
			// get the list
			statsList = getSystemRequestStatsService().getSystemRequestStatsList(null);
			for (SystemRequestStats requestStat : CollectionUtils.getIterable(statsList)) {
				if (mergeAndSaveSystemStatistic(status, statisticMap, statisticRun, requestStat)) {
					successfulSaves++;
				}
			}
		}

		catch (Exception ex) {
			String note = String.format("{System Statistics [%s]; process continued}", ExceptionUtils.getDetailedMessage(ex));
			LogUtils.error(getClass(), note, ex);
			status.addError(note);
		}

		finally {
			try {
				// flush the stats to restart the collection (also reset the statsLastCleared).
				getSystemRequestStatsService().deleteSystemRequestStatsAll();
			}
			finally {
				getSystemRequestStatsService().systemRequestExclusiveUnlock();
			}
		}
		status.appendToMessage(String.format("%d of %d System Request Stats were successfully saved", successfulSaves, CollectionUtils.getSize(statsList)));
		return status;
	}


	private Map<Integer, List<SystemStatisticDetail>> findStatisticDetailMap(Short systemStatisticSourceId) {
		// load all of the details.
		SystemStatisticDetailSearchForm detailSearchForm = new SystemStatisticDetailSearchForm();
		detailSearchForm.setStatisticSourceId(systemStatisticSourceId);

		return BeanUtils.getBeansMap(getSystemStatisticService().getSystemStatisticDetailList(detailSearchForm), "systemStatistic.id");
	}


	private List<SystemStatisticUri> findStatisticUriList(Short systemStatisticSourceId) {
		SystemStatisticUriSearchForm searchForm = new SystemStatisticUriSearchForm();
		searchForm.setSourceId(systemStatisticSourceId);

		return getSystemStatisticService().getSystemStatisticUriList(searchForm);
	}


	private Map<Integer, List<SystemStatistic>> findStatisticMap(Short systemStatisticSourceId) {
		// load all of the statistics
		SystemStatisticSearchForm searchForm = new SystemStatisticSearchForm();
		searchForm.setStatisticSourceId(systemStatisticSourceId);

		return BeanUtils.getBeansMap(getSystemStatisticService().getSystemStatisticList(searchForm), "statisticRun.id");
	}


	@Transactional(timeout = 180)
	protected void updateSystemStatistics(List<SystemStatistic> statisticsToBeUpdated, List<SystemStatistic> statisticsToBeDeleted, List<SystemStatisticDetail> statisticDetailsToBeUpdated, List<SystemStatisticUri> statisticUrisToBeDeleted) {

		for (SystemStatisticDetail statisticDetail : CollectionUtils.getIterable(statisticDetailsToBeUpdated)) {
			getSystemStatisticService().saveSystemStatisticDetail(statisticDetail);
		}

		for (SystemStatistic systemStatistic : CollectionUtils.getIterable(statisticsToBeUpdated)) {
			getSystemStatisticService().saveSystemStatistic(systemStatistic);
		}

		for (SystemStatistic systemStatistic : CollectionUtils.getIterable(statisticsToBeDeleted)) {
			getSystemStatisticService().deleteSystemStatistic(systemStatistic);
		}

		for (SystemStatisticUri statisticUri : CollectionUtils.getIterable(statisticUrisToBeDeleted)) {
			getSystemStatisticService().deleteSystemStatisticUri(statisticUri);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private SystemStatistic mergeSystemStatistic(SystemStatistic targetStatistic, SystemStatistic systemStatistic, SystemStatisticUri targetURI, List<SystemStatistic> statisticsToBeUpdated, List<SystemStatistic> statisticsToBeDeleted, List<SystemStatisticDetail> statisticDetailsToBeUpdated) {
		if (targetStatistic == null) {
			// first time, just re-aim the stat at the new URI
			targetStatistic = systemStatistic;

			// append the current URI to any details
			appendUriToDetails(systemStatistic, statisticDetailsToBeUpdated);

			// replace the current URI with the target
			targetStatistic.setStatisticUri(targetURI);

			// add it to the list to be updated
			statisticsToBeUpdated.add(targetStatistic);
		}
		else {
			// merge the current stat into the first one.
			mergeStatistic(systemStatistic, targetStatistic);

			// move details,
			moveAndAppendUriToDetails(systemStatistic, targetStatistic, statisticDetailsToBeUpdated);

			// move the stat to the delete list
			statisticsToBeDeleted.add(systemStatistic);
		}
		return targetStatistic;
	}


	private void mergeStatistic(SystemStatistic sourceStatistic, SystemStatistic targetStatistic) {
		targetStatistic.setTotalTimeNano(targetStatistic.getTotalTimeNano() + sourceStatistic.getTotalTimeNano());

		// accumulate total execution count.
		targetStatistic.setExecutionCount(targetStatistic.getExecutionCount() + sourceStatistic.getExecutionCount());

		// accumulate slow execution count.
		targetStatistic.setSlowExecutionCount(targetStatistic.getSlowExecutionCount() + sourceStatistic.getSlowExecutionCount());

		// replace min if it is less
		if (sourceStatistic.getMinTotalTimeNano() < targetStatistic.getMinTotalTimeNano()) {
			targetStatistic.setMinTotalTimeNano(sourceStatistic.getMinTotalTimeNano());
		}

		// replace max if it is more
		if (sourceStatistic.getMaxTotalTimeNano() > targetStatistic.getMaxTotalTimeNano()) {
			targetStatistic.setMaxTotalTimeNano(sourceStatistic.getMaxTotalTimeNano());
		}

		// accumulate total DB time.
		targetStatistic.setTotalDBTimeNano(targetStatistic.getTotalDBTimeNano() + sourceStatistic.getTotalDBTimeNano());

		// accumulate total DB execution count.
		targetStatistic.setDbExecutionCount(targetStatistic.getDbExecutionCount() + sourceStatistic.getDbExecutionCount());

		// replace min DB if it is less
		if (targetStatistic.getMinDBTimeNano() > sourceStatistic.getMinDBTimeNano()) {
			targetStatistic.setMinDBTimeNano(sourceStatistic.getMinDBTimeNano());
		}

		// replace max DB if it is more
		if (targetStatistic.getMaxDBTimeNano() < sourceStatistic.getMaxDBTimeNano()) {
			targetStatistic.setMaxDBTimeNano(sourceStatistic.getMaxDBTimeNano());
		}
	}


	private void mergeStatistic(SystemRequestStats newStatistic, SystemStatistic existingStatistic) {
		// found one, merge the details in.
		// accumulate total time.
		existingStatistic.setTotalTimeNano(existingStatistic.getTotalTimeNano() + newStatistic.getTotalTimeNano());

		// accumulate total execution count.
		existingStatistic.setExecutionCount(existingStatistic.getExecutionCount() + newStatistic.getExecuteCount());

		existingStatistic.setSlowExecutionCount(existingStatistic.getSlowExecutionCount() + newStatistic.getSlowestExecuteCount());


		// replace min if it is less
		if (newStatistic.getMinTotalTimeNano() < existingStatistic.getMinTotalTimeNano()) {
			existingStatistic.setMinTotalTimeNano(newStatistic.getMinTotalTimeNano());
		}

		// replace max if it is more
		if (newStatistic.getMaxTotalTimeNano() > existingStatistic.getMaxTotalTimeNano()) {
			existingStatistic.setMaxTotalTimeNano(newStatistic.getMaxTotalTimeNano());
		}

		// accumulate slow execution count.
		existingStatistic.setSlowExecutionCount(existingStatistic.getSlowExecutionCount() + newStatistic.getSlowExecuteCount());

		// accumulate total DB time.
		existingStatistic.setTotalDBTimeNano(existingStatistic.getTotalDBTimeNano() + newStatistic.getTotalDBTimeNano());

		// accumulate total DB execution count.
		existingStatistic.setDbExecutionCount(existingStatistic.getDbExecutionCount() + newStatistic.getDbExecuteCount());

		// replace min DB if it is less
		if (existingStatistic.getMinDBTimeNano() > newStatistic.getMinDBTimeNano()) {
			existingStatistic.setMinDBTimeNano(newStatistic.getMinDBTimeNano());
		}

		// replace max DB if it is more
		if (existingStatistic.getMaxDBTimeNano() < newStatistic.getMaxDBTimeNano()) {
			existingStatistic.setMaxDBTimeNano(newStatistic.getMaxDBTimeNano());
		}
	}


	private void moveAndAppendUriToDetails(SystemStatistic sourceStatistic, SystemStatistic targetStatistic, List<SystemStatisticDetail> statisticDetailsToBeUpdated) {
		// save the old URI to the detail parameters and move detail to new stat.
		for (SystemStatisticDetail statisticDetail : CollectionUtils.getIterable(sourceStatistic.getSystemStatisticDetailList())) {

			// update the parameters
			String parameters = statisticDetail.getParameters();
			statisticDetail.setParameters((StringUtils.isEmpty(parameters) ? null : parameters + ": ") + sourceStatistic.getStatisticUri().getRequestURI());

			// assign it to the new targetStatistic
			statisticDetail.setSystemStatistic(targetStatistic);
			statisticDetailsToBeUpdated.add(statisticDetail);
		}
	}


	private void appendUriToDetails(SystemStatistic systemStatistic, List<SystemStatisticDetail> statisticDetailsToBeUpdated) {
		// save the old URI to the detail parameters
		for (SystemStatisticDetail statisticDetail : CollectionUtils.getIterable(systemStatistic.getSystemStatisticDetailList())) {
			String parameters = statisticDetail.getParameters();
			statisticDetail.setParameters((StringUtils.isEmpty(parameters) ? null : parameters + ": ") + systemStatistic.getStatisticUri().getRequestURI());

			statisticDetailsToBeUpdated.add(statisticDetail);
		}
	}


	private List<SystemStatistic> getExistingStats(Date runStartDate, Status status) {
		// get the list.  There should only be one or zero in the list.
		SystemStatisticRunSearchForm runSearchForm = new SystemStatisticRunSearchForm();
		runSearchForm.setRunStartDateAndTime(DateUtils.clearTime(runStartDate));
		List<SystemStatisticRun> runList = getSystemStatisticService().getSystemStatisticRunList(runSearchForm);

		if (CollectionUtils.getSize(runList) > 1) {
			status.addError("Expected [1] but found [" + runList.size() + "] runs for [" + runStartDate + "]");
		}

		SystemStatisticRun statisticRun = CollectionUtils.getFirstElement(runList);
		if (statisticRun == null) {
			return null;
		}

		status.appendToMessage("Merging with existing statistics");
		SystemStatisticSearchForm statisticSearchForm = new SystemStatisticSearchForm();
		statisticSearchForm.setStatisticRunId(statisticRun.getId());
		return getSystemStatisticService().getSystemStatisticList(statisticSearchForm);
	}


	/**
	 * Must start a new transaction every time so that a single failure doesn't fail everything else because this
	 * method is called inside of an outer transaction.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected boolean mergeAndSaveSystemStatistic(Status status, Map<String, SystemStatistic> statisticMap, SystemStatisticRun statisticRun, SystemRequestStats requestStat) {
		try {
			doMergeAndSaveSystemStatistic(requestStat, statisticRun, statisticMap);
		}
		catch (Exception ex) {
			String note = String.format("{[%s]: %s; process continued}", requestStat, ExceptionUtils.getDetailedMessage(ex));
			// catch the exception and log it, but do not fail the process
			LogUtils.error(getClass(), note, ex);
			status.addError(note);
			return false;
		}

		return true;
	}


	protected void doMergeAndSaveSystemStatistic(SystemRequestStats newStatistic, SystemStatisticRun statisticRun, Map<String, SystemStatistic> statisticMap) {
		// get the Source
		SystemStatisticSource statisticSource = getSystemStatisticService().getFromCacheOrCreateSystemStatisticSource(newStatistic.getRequestSource());

		// figure out what to use as the URI based on the source's isSingleURI
		String targetUri = statisticSource.isSingleURI() ? newStatistic.getRequestSource() : newStatistic.getRequestURI();

		String mapKey = statisticMapKey(newStatistic.getRequestSource(), targetUri);

		// check for it in the map
		SystemStatistic systemStatistic = CollectionUtils.getValue(statisticMap, mapKey);

		if (systemStatistic == null) {
			// check to see if the uri already exists.
			SystemStatisticUri statisticUri = getSystemStatisticService().getFromCacheOrCreateSystemStatisticUri(newStatistic.getRequestSource(), targetUri);

			// create and populate System Statistic
			systemStatistic = SystemStatistic.of(newStatistic);
			systemStatistic.setStatisticRun(statisticRun);
			// replace the URI with the one derived above
			systemStatistic.setStatisticUri(statisticUri);
			statisticMap.put(mapKey, systemStatistic);
		}
		else {
			mergeStatistic(newStatistic, systemStatistic);
		}

		// save the system statistic and get the id.
		systemStatistic = getSystemStatisticService().saveSystemStatistic(systemStatistic);

		// iterate the in-memory list and create dao objects to save to the database.
		for (SystemRequestStatsDetail detail : CollectionUtils.getIterable(newStatistic.getSlowestStats())) {
			SystemStatisticDetail statisticDetail = SystemStatisticDetail.of(detail);
			if (statisticSource.isSingleURI()) {
				// save the URI to the detail parameters
				String parameters = statisticDetail.getParameters();
				statisticDetail.setParameters((StringUtils.isEmpty(parameters) ? null : parameters + ": ") + newStatistic.getRequestURI());
			}
			statisticDetail.setSystemStatistic(systemStatistic);
			getSystemStatisticService().saveSystemStatisticDetail(statisticDetail);
		}
	}


	private static String statisticMapKey(SystemStatisticUri statisticUri) {
		return statisticMapKey(statisticUri.getStatisticSource().getName(), statisticUri.getRequestURI());
	}


	private static String statisticMapKey(String requestSource, String requestURI) {
		return requestSource + "::" + requestURI;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}


	public SystemStatisticService getSystemStatisticService() {
		return this.systemStatisticService;
	}


	public void setSystemStatisticService(SystemStatisticService systemStatisticService) {
		this.systemStatisticService = systemStatisticService;
	}


	public SecurityImpersonationHandler getSecurityImpersonationHandler() {
		return this.securityImpersonationHandler;
	}


	public void setSecurityImpersonationHandler(SecurityImpersonationHandler securityImpersonationHandler) {
		this.securityImpersonationHandler = securityImpersonationHandler;
	}
}
