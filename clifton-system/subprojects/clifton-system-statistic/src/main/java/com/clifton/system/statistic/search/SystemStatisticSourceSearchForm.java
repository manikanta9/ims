package com.clifton.system.statistic.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


public class SystemStatisticSourceSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String description;

	@SearchField
	private Boolean singleURI;


	@SearchField
	private Integer minDailyRequestCount;
	@SearchField
	private Integer maxDailyRequestCount;
	@SearchField
	private Integer minDailyTotalTimeSeconds;
	@SearchField
	private Integer maxDailyTotalTimeSeconds;
	@SearchField
	private Integer minAverageRequestTimeMillis;
	@SearchField
	private Integer maxAverageRequestTimeMillis;
	@SearchField
	private Integer maxRequestTimeMillis;

	// at least one SLA metric is populated
	@SearchField(searchField = "minDailyRequestCount,maxDailyRequestCount,minDailyTotalTimeSeconds,maxDailyTotalTimeSeconds,minAverageRequestTimeMillis,maxAverageRequestTimeMillis,maxRequestTimeMillis", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean slaPresent;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getSingleURI() {
		return this.singleURI;
	}


	public void setSingleURI(Boolean singleURI) {
		this.singleURI = singleURI;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getMinDailyRequestCount() {
		return this.minDailyRequestCount;
	}


	public void setMinDailyRequestCount(Integer minDailyRequestCount) {
		this.minDailyRequestCount = minDailyRequestCount;
	}


	public Integer getMaxDailyRequestCount() {
		return this.maxDailyRequestCount;
	}


	public void setMaxDailyRequestCount(Integer maxDailyRequestCount) {
		this.maxDailyRequestCount = maxDailyRequestCount;
	}


	public Integer getMinDailyTotalTimeSeconds() {
		return this.minDailyTotalTimeSeconds;
	}


	public void setMinDailyTotalTimeSeconds(Integer minDailyTotalTimeSeconds) {
		this.minDailyTotalTimeSeconds = minDailyTotalTimeSeconds;
	}


	public Integer getMaxDailyTotalTimeSeconds() {
		return this.maxDailyTotalTimeSeconds;
	}


	public void setMaxDailyTotalTimeSeconds(Integer maxDailyTotalTimeSeconds) {
		this.maxDailyTotalTimeSeconds = maxDailyTotalTimeSeconds;
	}


	public Integer getMinAverageRequestTimeMillis() {
		return this.minAverageRequestTimeMillis;
	}


	public void setMinAverageRequestTimeMillis(Integer minAverageRequestTimeMillis) {
		this.minAverageRequestTimeMillis = minAverageRequestTimeMillis;
	}


	public Integer getMaxAverageRequestTimeMillis() {
		return this.maxAverageRequestTimeMillis;
	}


	public void setMaxAverageRequestTimeMillis(Integer maxAverageRequestTimeMillis) {
		this.maxAverageRequestTimeMillis = maxAverageRequestTimeMillis;
	}


	public Integer getMaxRequestTimeMillis() {
		return this.maxRequestTimeMillis;
	}


	public void setMaxRequestTimeMillis(Integer maxRequestTimeMillis) {
		this.maxRequestTimeMillis = maxRequestTimeMillis;
	}


	public Boolean getSlaPresent() {
		return this.slaPresent;
	}


	public void setSlaPresent(Boolean slaPresent) {
		this.slaPresent = slaPresent;
	}
}
