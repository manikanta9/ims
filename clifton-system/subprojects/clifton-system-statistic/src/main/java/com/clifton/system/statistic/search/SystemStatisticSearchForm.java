package com.clifton.system.statistic.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class SystemStatisticSearchForm extends BaseEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "requestURI", searchFieldPath = "statisticUri")
	private String searchPattern;


	@SearchField(searchField = "statisticUri.id")
	private Integer statisticUriId;

	@SearchField(searchField = "statisticSource.id", searchFieldPath = "statisticUri")
	private Short statisticSourceId;

	@SearchField(searchField = "name", searchFieldPath = "statisticUri.statisticSource")
	private String sourceName;

	// at least one SLA metric is populated
	@SearchField(searchFieldPath = "statisticUri.statisticSource", searchField = "minDailyRequestCount,maxDailyRequestCount,minAverageRequestTimeMillis,maxAverageRequestTimeMillis,maxRequestTimeMillis", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean slaForSourcePresent;

	@SearchField(searchField = "requestURI", searchFieldPath = "statisticUri")
	private String requestURI;

	// at least one SLA metric is populated
	@SearchField(searchFieldPath = "statisticUri", searchField = "minDailyRequestCount,maxDailyRequestCount,minAverageRequestTimeMillis,maxAverageRequestTimeMillis,maxRequestTimeMillis", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean slaForUriPresent;

	@SearchField(searchField = "statisticRun.id")
	private Integer statisticRunId;

	@SearchField(searchField = "runStartDateAndTime", searchFieldPath = "statisticRun", dateFieldIncludesTime = true)
	private Date runStartDateAndTime;

	@SearchField(searchField = "runEndDateAndTime", searchFieldPath = "statisticRun", dateFieldIncludesTime = true)
	private Date runEndDateAndTime;


	@SearchField(searchField = "description", searchFieldPath = "statisticRun")
	private String runDescription;

	@SearchField
	private Integer executionCount;

	@SearchField
	private Integer slowExecutionCount;

	@SearchField
	private Long totalTimeNano;

	@SearchField(searchField = "totalTimeNano", formula = "CONVERT(DECIMAL(21, 6), $1 / 1000000000.0)")
	private BigDecimal totalTimeSeconds;

	@SearchField(searchField = "totalTimeNano,executionCount", formula = "CONVERT(DECIMAL(21, 6), $1 / $2)")
	private BigDecimal avgTimeNano;

	@SearchField(searchField = "totalTimeNano,executionCount", formula = "CONVERT(DECIMAL(21, 6), $1 / $2 / 1000000000.0)")
	private BigDecimal avgTimeSeconds;

	@SearchField
	private Long minTotalTimeNano;

	@SearchField(searchField = "minTotalTimeNano", formula = "CONVERT(DECIMAL(19, 6), $1 / 1000000000.0)")
	private BigDecimal minTotalTimeSeconds;

	@SearchField
	private Long maxTotalTimeNano;

	@SearchField(searchField = "maxTotalTimeNano", formula = "CONVERT(DECIMAL(19, 6), $1 / 1000000000.0)")
	private BigDecimal maxTotalTimeSeconds;


	@SearchField
	private Integer dbExecutionCount;

	@SearchField
	private Long totalDBTimeNano;

	@SearchField
	private Long minDBTimeNano;

	@SearchField
	private Long maxDBTimeNano;


	// Custom search filter - select which day of the week.
	private Boolean selectWeekdays;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getStatisticUriId() {
		return this.statisticUriId;
	}


	public void setStatisticUriId(Integer statisticUriId) {
		this.statisticUriId = statisticUriId;
	}


	public Short getStatisticSourceId() {
		return this.statisticSourceId;
	}


	public void setStatisticSourceId(Short statisticSourceId) {
		this.statisticSourceId = statisticSourceId;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Boolean getSlaForSourcePresent() {
		return this.slaForSourcePresent;
	}


	public void setSlaForSourcePresent(Boolean slaForSourcePresent) {
		this.slaForSourcePresent = slaForSourcePresent;
	}


	public String getRequestURI() {
		return this.requestURI;
	}


	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}


	public Boolean getSlaForUriPresent() {
		return this.slaForUriPresent;
	}


	public void setSlaForUriPresent(Boolean slaForUriPresent) {
		this.slaForUriPresent = slaForUriPresent;
	}


	public Integer getStatisticRunId() {
		return this.statisticRunId;
	}


	public void setStatisticRunId(Integer statisticRunId) {
		this.statisticRunId = statisticRunId;
	}


	public Date getRunStartDateAndTime() {
		return this.runStartDateAndTime;
	}


	public void setRunStartDateAndTime(Date runStartDateAndTime) {
		this.runStartDateAndTime = runStartDateAndTime;
	}


	public Date getRunEndDateAndTime() {
		return this.runEndDateAndTime;
	}


	public void setRunEndDateAndTime(Date runEndDateAndTime) {
		this.runEndDateAndTime = runEndDateAndTime;
	}


	public String getRunDescription() {
		return this.runDescription;
	}


	public void setRunDescription(String runDescription) {
		this.runDescription = runDescription;
	}


	public Integer getExecutionCount() {
		return this.executionCount;
	}


	public void setExecutionCount(Integer executionCount) {
		this.executionCount = executionCount;
	}


	public Integer getSlowExecutionCount() {
		return this.slowExecutionCount;
	}


	public void setSlowExecutionCount(Integer slowExecutionCount) {
		this.slowExecutionCount = slowExecutionCount;
	}


	public Long getTotalTimeNano() {
		return this.totalTimeNano;
	}


	public void setTotalTimeNano(Long totalTimeNano) {
		this.totalTimeNano = totalTimeNano;
	}


	public BigDecimal getTotalTimeSeconds() {
		return this.totalTimeSeconds;
	}


	public void setTotalTimeSeconds(BigDecimal totalTimeSeconds) {
		this.totalTimeSeconds = totalTimeSeconds;
	}


	public BigDecimal getAvgTimeNano() {
		return this.avgTimeNano;
	}


	public void setAvgTimeNano(BigDecimal avgTimeNano) {
		this.avgTimeNano = avgTimeNano;
	}


	public BigDecimal getAvgTimeSeconds() {
		return this.avgTimeSeconds;
	}


	public void setAvgTimeSeconds(BigDecimal avgTimeSeconds) {
		this.avgTimeSeconds = avgTimeSeconds;
	}


	public Long getMinTotalTimeNano() {
		return this.minTotalTimeNano;
	}


	public void setMinTotalTimeNano(Long minTotalTimeNano) {
		this.minTotalTimeNano = minTotalTimeNano;
	}


	public BigDecimal getMinTotalTimeSeconds() {
		return this.minTotalTimeSeconds;
	}


	public void setMinTotalTimeSeconds(BigDecimal minTotalTimeSeconds) {
		this.minTotalTimeSeconds = minTotalTimeSeconds;
	}


	public Long getMaxTotalTimeNano() {
		return this.maxTotalTimeNano;
	}


	public void setMaxTotalTimeNano(Long maxTotalTimeNano) {
		this.maxTotalTimeNano = maxTotalTimeNano;
	}


	public BigDecimal getMaxTotalTimeSeconds() {
		return this.maxTotalTimeSeconds;
	}


	public void setMaxTotalTimeSeconds(BigDecimal maxTotalTimeSeconds) {
		this.maxTotalTimeSeconds = maxTotalTimeSeconds;
	}


	public Integer getDbExecutionCount() {
		return this.dbExecutionCount;
	}


	public void setDbExecutionCount(Integer dbExecutionCount) {
		this.dbExecutionCount = dbExecutionCount;
	}


	public Long getTotalDBTimeNano() {
		return this.totalDBTimeNano;
	}


	public void setTotalDBTimeNano(Long totalDBTimeNano) {
		this.totalDBTimeNano = totalDBTimeNano;
	}


	public Long getMinDBTimeNano() {
		return this.minDBTimeNano;
	}


	public void setMinDBTimeNano(Long minDBTimeNano) {
		this.minDBTimeNano = minDBTimeNano;
	}


	public Long getMaxDBTimeNano() {
		return this.maxDBTimeNano;
	}


	public void setMaxDBTimeNano(Long maxDBTimeNano) {
		this.maxDBTimeNano = maxDBTimeNano;
	}


	public Boolean getSelectWeekdays() {
		return this.selectWeekdays;
	}


	public void setSelectWeekdays(Boolean selectWeekdays) {
		this.selectWeekdays = selectWeekdays;
	}
}
