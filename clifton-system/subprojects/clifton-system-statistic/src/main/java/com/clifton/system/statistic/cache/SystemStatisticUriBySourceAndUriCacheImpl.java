package com.clifton.system.statistic.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.system.statistic.SystemStatisticUri;
import org.springframework.stereotype.Component;


/**
 * @author RogerW
 */
@Component
public class SystemStatisticUriBySourceAndUriCacheImpl extends SelfRegisteringCompositeKeyDaoCache<SystemStatisticUri, String, String> implements SystemStatisticUriBySourceAndUriCache {

	@Override
	protected String getBeanKey1Property() {
		return "statisticSource.name";
	}


	@Override
	protected String getBeanKey2Property() {
		return "requestURI";
	}


	@Override
	protected String getBeanKey1Value(SystemStatisticUri bean) {
		if (bean != null && bean.getStatisticSource() != null) {
			return bean.getStatisticSource().getName();
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(SystemStatisticUri bean) {
		if (bean != null) {
			return bean.getRequestURI();
		}
		return null;
	}
}
