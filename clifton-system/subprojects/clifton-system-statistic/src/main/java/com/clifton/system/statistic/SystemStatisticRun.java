package com.clifton.system.statistic;

import com.clifton.core.beans.BaseSimpleEntity;

import java.util.Date;
import java.util.List;


/**
 * The SystemStatisticRun class represents a single snapshot run that captures all system statistics at specific point in time.
 * Usually statistics are captures from the last run.
 *
 * @author RogerW
 */
public class SystemStatisticRun extends BaseSimpleEntity<Integer> {

	private Date runStartDateAndTime;
	private Date runEndDateAndTime;
	private String description;

	private List<SystemStatistic> systemStatisticList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getRunStartDateAndTime() {
		return this.runStartDateAndTime;
	}


	public void setRunStartDateAndTime(Date runStartDateAndTime) {
		this.runStartDateAndTime = runStartDateAndTime;
	}


	public Date getRunEndDateAndTime() {
		return this.runEndDateAndTime;
	}


	public void setRunEndDateAndTime(Date runEndDateAndTime) {
		this.runEndDateAndTime = runEndDateAndTime;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public List<SystemStatistic> getSystemStatisticList() {
		return this.systemStatisticList;
	}


	public void setSystemStatisticList(List<SystemStatistic> systemStatisticList) {
		this.systemStatisticList = systemStatisticList;
	}
}
