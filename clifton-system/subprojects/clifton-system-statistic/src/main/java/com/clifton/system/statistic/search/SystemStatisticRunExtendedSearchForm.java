package com.clifton.system.statistic.search;

import com.clifton.core.dataaccess.search.SearchField;

import java.math.BigDecimal;


/**
 * @author RogerW
 */
public class SystemStatisticRunExtendedSearchForm extends SystemStatisticRunSearchForm {

	@SearchField
	private Integer executionCount;

	@SearchField
	private Integer slowExecutionCount;

	@SearchField
	private Long totalTimeNano;

	@SearchField(searchField = "totalTimeNano", formula = "CONVERT(DECIMAL(21, 6), $1 / 1000000000.0)")
	private BigDecimal totalTimeSeconds;

	@SearchField
	private Long maxTotalTimeNano;

	@SearchField(searchField = "minTotalTimeNano", formula = "CONVERT(DECIMAL(19, 6), $1 / 1000000000.0)")
	private BigDecimal minTotalTimeSeconds;

	@SearchField
	private Long minTotalTimeNano;

	@SearchField(searchField = "maxTotalTimeNano", formula = "CONVERT(DECIMAL(19, 6), $1 / 1000000000.0)")
	private BigDecimal maxTotalTimeSeconds;

	@SearchField(searchField = "totalTimeNano,executionCount", formula = "CONVERT(DECIMAL(21, 6), $1 / $2)")
	private BigDecimal avgTimeNano;

	@SearchField(searchField = "totalTimeNano,executionCount", formula = "CONVERT(DECIMAL(21, 6), $1 / $2 / 1000000000.0)")
	private BigDecimal avgTimeSeconds;

	@SearchField
	private Integer dbExecutionCount;

	@SearchField
	private Long totalDBTimeNano;

	@SearchField
	private Long minDBTimeNano;

	@SearchField
	private Long maxDBTimeNano;

	// Custom search filter - select which statistic source.
	private Integer sourceId;

	// Custom search filter - select which day of the week.
	private Boolean selectWeekdays;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getExecutionCount() {
		return this.executionCount;
	}


	public void setExecutionCount(Integer executionCount) {
		this.executionCount = executionCount;
	}


	public Integer getSlowExecutionCount() {
		return this.slowExecutionCount;
	}


	public void setSlowExecutionCount(Integer slowExecutionCount) {
		this.slowExecutionCount = slowExecutionCount;
	}


	public Long getTotalTimeNano() {
		return this.totalTimeNano;
	}


	public void setTotalTimeNano(Long totalTimeNano) {
		this.totalTimeNano = totalTimeNano;
	}


	public BigDecimal getTotalTimeSeconds() {
		return this.totalTimeSeconds;
	}


	public void setTotalTimeSeconds(BigDecimal totalTimeSeconds) {
		this.totalTimeSeconds = totalTimeSeconds;
	}


	public Long getMaxTotalTimeNano() {
		return this.maxTotalTimeNano;
	}


	public void setMaxTotalTimeNano(Long maxTotalTimeNano) {
		this.maxTotalTimeNano = maxTotalTimeNano;
	}


	public BigDecimal getMinTotalTimeSeconds() {
		return this.minTotalTimeSeconds;
	}


	public void setMinTotalTimeSeconds(BigDecimal minTotalTimeSeconds) {
		this.minTotalTimeSeconds = minTotalTimeSeconds;
	}


	public Long getMinTotalTimeNano() {
		return this.minTotalTimeNano;
	}


	public void setMinTotalTimeNano(Long minTotalTimeNano) {
		this.minTotalTimeNano = minTotalTimeNano;
	}


	public BigDecimal getMaxTotalTimeSeconds() {
		return this.maxTotalTimeSeconds;
	}


	public void setMaxTotalTimeSeconds(BigDecimal maxTotalTimeSeconds) {
		this.maxTotalTimeSeconds = maxTotalTimeSeconds;
	}


	public BigDecimal getAvgTimeNano() {
		return this.avgTimeNano;
	}


	public void setAvgTimeNano(BigDecimal avgTimeNano) {
		this.avgTimeNano = avgTimeNano;
	}


	public BigDecimal getAvgTimeSeconds() {
		return this.avgTimeSeconds;
	}


	public void setAvgTimeSeconds(BigDecimal avgTimeSeconds) {
		this.avgTimeSeconds = avgTimeSeconds;
	}


	public Integer getDbExecutionCount() {
		return this.dbExecutionCount;
	}


	public void setDbExecutionCount(Integer dbExecutionCount) {
		this.dbExecutionCount = dbExecutionCount;
	}


	public Long getTotalDBTimeNano() {
		return this.totalDBTimeNano;
	}


	public void setTotalDBTimeNano(Long totalDBTimeNano) {
		this.totalDBTimeNano = totalDBTimeNano;
	}


	public Long getMinDBTimeNano() {
		return this.minDBTimeNano;
	}


	public void setMinDBTimeNano(Long minDBTimeNano) {
		this.minDBTimeNano = minDBTimeNano;
	}


	public Long getMaxDBTimeNano() {
		return this.maxDBTimeNano;
	}


	public void setMaxDBTimeNano(Long maxDBTimeNano) {
		this.maxDBTimeNano = maxDBTimeNano;
	}


	public Integer getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}


	public Boolean getSelectWeekdays() {
		return this.selectWeekdays;
	}


	public void setSelectWeekdays(Boolean selectWeekdays) {
		this.selectWeekdays = selectWeekdays;
	}
}
