package com.clifton.system.statistic;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.TimeUtils;

import java.math.BigDecimal;


/**
 * @author RogerW
 */
public class SystemStatisticRunExtended extends SystemStatisticRun {

	private Integer executionCount;
	private Integer slowExecutionCount;
	private Long totalTimeNano;
	private Long maxTotalTimeNano;
	private Long minTotalTimeNano;
	private Integer dbExecutionCount;
	private Long totalDBTimeNano;
	private Long minDBTimeNano;
	private Long maxDBTimeNano;

	////////////////////////////////////////////////////////////////////////////
	////////            Computed Getters                                ////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getTotalTimeSeconds() {
		return TimeUtils.convertNanoToSeconds(getTotalTimeNano());
	}


	public BigDecimal getMinTotalTimeSeconds() {
		return TimeUtils.convertNanoToSeconds(getMinTotalTimeNano());
	}


	public BigDecimal getMaxTotalTimeSeconds() {
		return TimeUtils.convertNanoToSeconds(getMaxTotalTimeNano());
	}


	public BigDecimal getAvgTimeNano() {
		return MathUtils.divide(getTotalTimeNano(), getExecutionCount());
	}


	public BigDecimal getAvgTimeSeconds() {
		return MathUtils.divide(getTotalTimeSeconds(), getExecutionCount());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getExecutionCount() {
		return this.executionCount;
	}


	public void setExecutionCount(Integer executionCount) {
		this.executionCount = executionCount;
	}


	public Integer getSlowExecutionCount() {
		return this.slowExecutionCount;
	}


	public void setSlowExecutionCount(Integer slowExecutionCount) {
		this.slowExecutionCount = slowExecutionCount;
	}


	public Long getTotalTimeNano() {
		return this.totalTimeNano;
	}


	public void setTotalTimeNano(Long totalTimeNano) {
		this.totalTimeNano = totalTimeNano;
	}


	public Long getMaxTotalTimeNano() {
		return this.maxTotalTimeNano;
	}


	public void setMaxTotalTimeNano(Long maxTotalTimeNano) {
		this.maxTotalTimeNano = maxTotalTimeNano;
	}


	public Long getMinTotalTimeNano() {
		return this.minTotalTimeNano;
	}


	public void setMinTotalTimeNano(Long minTotalTimeNano) {
		this.minTotalTimeNano = minTotalTimeNano;
	}


	public Integer getDbExecutionCount() {
		return this.dbExecutionCount;
	}


	public void setDbExecutionCount(Integer dbExecutionCount) {
		this.dbExecutionCount = dbExecutionCount;
	}


	public Long getTotalDBTimeNano() {
		return this.totalDBTimeNano;
	}


	public void setTotalDBTimeNano(Long totalDBTimeNano) {
		this.totalDBTimeNano = totalDBTimeNano;
	}


	public Long getMinDBTimeNano() {
		return this.minDBTimeNano;
	}


	public void setMinDBTimeNano(Long minDBTimeNano) {
		this.minDBTimeNano = minDBTimeNano;
	}


	public Long getMaxDBTimeNano() {
		return this.maxDBTimeNano;
	}


	public void setMaxDBTimeNano(Long maxDBTimeNano) {
		this.maxDBTimeNano = maxDBTimeNano;
	}
}
