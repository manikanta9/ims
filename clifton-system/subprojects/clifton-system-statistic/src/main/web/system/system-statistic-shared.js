Ext.ns('Clifton.system.statistic');


Clifton.core.stats.createDetailStatsUriWindow = function(uri, source) {
	TCG.data.getDataPromiseUsingCaching('systemStatisticUriListFind.json?sourceName=' + source + '&requestURI=' + uri)
		.then(function(list) {
			if (list && list.length > 0) {
				const id = list[0].id;
				const className = 'Clifton.system.statistic.StatisticUriWindow';
				TCG.createComponent(className, {id: TCG.getComponentId(className, id), params: {id: id}});
			}
			else {
				TCG.showError('Cannot find System Statistic URI for source = ' + source + ' and URI = ' + uri);
			}
		});
};

// call 'Clifton.system.statistic.getSourcePromiseByName' first to avoid synchronous server call
Clifton.system.statistic.renderSourceValueForRow = function(value, row, numberFormat, componentScope, slaRenderingFunction) {
	const formattedValue = TCG.numberFormat(value, numberFormat);
	if (row.store) { // summary row has no store
		const sourceName = row.store.selectedStatisticSource;
		if (TCG.isNotBlank(sourceName)) {
			const source = TCG.data.getData('systemStatisticSourceByName.json?name=' + sourceName, componentScope, 'system.statistic.source.' + sourceName);
			return slaRenderingFunction(formattedValue, source);
		}
	}
	return formattedValue;
};

Clifton.system.statistic.getSourcePromiseByName = function(sourceName) {
	return TCG.data.getDataPromiseUsingCaching('systemStatisticSourceByName.json?name=' + sourceName, null, 'system.statistic.source.' + sourceName);
};

Clifton.system.statistic.StatisticHistoryGridPanel = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	name: 'systemStatisticListFind',
	additionalPropertiesToRequest: 'id|statisticUri.minDailyRequestCount|statisticUri.maxDailyRequestCount|statisticUri.minDailyTotalTimeSeconds|statisticUri.maxDailyTotalTimeSeconds|statisticUri.minAverageRequestTimeMillis|statisticUri.maxAverageRequestTimeMillis|statisticUri.maxRequestTimeMillis',
	instructions: 'Historical daily statistics for selected URI.',
	appendStandardColumns: false,
	pageSize: 500,
	columns: [
		{
			header: 'Start', dataIndex: 'statisticRun.runStartDateAndTime', filter: {searchFieldName: 'runStartDateAndTime'}, type: 'date', width: 50, defaultSortColumn: true, defaultSortDirection: 'DESC',
			renderer: function(v, metaData, r) {
				const note = r.data['statisticRun.description'];
				if (TCG.isNotBlank(note)) {
					const qtip = r.data['statisticRun.description'];
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return TCG.renderDateTime(v);
			}
		},
		{header: 'End', dataIndex: 'statisticRun.runEndDateAndTime', filter: {searchFieldName: 'runEndDateAndTime'}, type: 'date', width: 50},
		{header: 'Run Id', dataIndex: 'statisticRun.id', hidden: true, width: 20},
		{header: 'Run Description', dataIndex: 'statisticRun.description', filter: {searchFieldName: 'runDescription'}, hidden: true, width: 100},
		{header: 'Source', dataIndex: 'statisticUri.statisticSource.name', width: 50, filter: {searchFieldName: 'sourceName'}, hidden: true},
		{
			header: 'URI', dataIndex: 'statisticUri.requestURI', width: 160, filter: {searchFieldName: 'requestURI'}, hidden: true,
			renderer: function(v, metaData, r) {
				return Clifton.core.stats.getNormalizedURI(v, r.data.requestSource);
			}
		},
		{header: 'Source SLA', dataIndex: 'statisticUri.statisticSource.slaPresent', width: 35, filter: {searchFieldName: 'slaForSourcePresent'}, type: 'boolean', hidden: true},
		{header: 'URI SLA', dataIndex: 'statisticUri.slaPresent', width: 30, filter: {searchFieldName: 'slaForUriPresent'}, type: 'boolean', hidden: true},
		{
			header: 'Hits', dataIndex: 'executionCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Total Number of URI Hits',
			renderer: function(value, metaData, r) {
				return TCG.renderRangeSLA(TCG.numberFormat(value, '0,000'), value, TCG.getValue('statisticUri.minDailyRequestCount', r.json), TCG.getValue('statisticUri.maxDailyRequestCount', r.json), metaData);
			}
		},
		{header: 'Slow Hits', dataIndex: 'slowExecutionCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Number of URI Hits Slower than 1 Second'},
		{
			header: '% Slow', dataIndex: 'slowExecutePercent', width: 40, type: 'float', sortable: false, tooltip: 'Percent of URI Hits Slower than 1 Second',
			renderer: function(value, metaData, r) {
				const data = r.data;
				return TCG.numberFormat(100 * data['slowExecutionCount'] / data['executionCount'], '0,000.00 %');
			},
			summaryCalculation: function(v, r, field, data, col) {
				return 100 * data['slowExecutionCount'] / data['executionCount'];
			}
		},
		{header: 'Total Time Nano', dataIndex: 'totalTimeNano', width: 40, type: 'int', tooltip: 'Total Execution Time in Nano Seconds', summaryType: 'sum', hidden: true},
		{
			header: 'Total Time', dataIndex: 'totalTimeSeconds', width: 45, type: 'float', tooltip: 'Total Execution Time in Seconds', summaryType: 'sum',
			renderer: function(value, metaData, r) {
				return TCG.renderRangeSLA(TCG.numberFormat(value, '0,000.00'), value, TCG.getValue('statisticUri.minDailyTotalTimeSeconds', r.json), TCG.getValue('statisticUri.maxDailyTotalTimeSeconds', r.json), metaData);
			}
		},
		{header: 'Min Time Nano', dataIndex: 'minTotalTimeNano', type: 'int', summaryType: 'min', width: 40, tooltip: 'Minimum Execution Time in Nano Seconds', hidden: true},
		{
			header: 'Min Time', dataIndex: 'minTotalTimeSeconds', type: 'float', summaryType: 'min', width: 40, tooltip: 'Minimum Execution Time in Seconds',
			renderer: function(value, metaData, r) {
				return TCG.numberFormat(value, '0,000.0000');
			}
		},
		{header: 'Max Time Nano', dataIndex: 'maxTotalTimeNano', type: 'int', summaryType: 'max', width: 40, tooltip: 'Maximum Execution Time in Nano Seconds', hidden: true},
		{
			header: 'Max Time', dataIndex: 'maxTotalTimeSeconds', type: 'float', summaryType: 'max', width: 40, tooltip: 'Maximum Execution Time in Seconds',
			renderer: function(value, metaData, r) {
				return TCG.renderMaxSLA(TCG.numberFormat(value, '0,000.0000'), 1000 * value, TCG.getValue('statisticUri.maxRequestTimeMillis', r.json), metaData);
			}
		},
		{
			header: 'Avg Time', dataIndex: 'avgTimeSeconds', type: 'float', summaryType: 'average', width: 40, tooltip: 'Average Execution Time in Seconds',
			renderer: function(value, metaData, r) {
				return TCG.renderRangeSLA(TCG.numberFormat(value, '0,000.0000'), 1000 * value, TCG.getValue('statisticUri.minAverageRequestTimeMillis', r.json), TCG.getValue('statisticUri.maxAverageRequestTimeMillis', r.json), metaData);
			},
			summaryCalculation: function(v, r, field, data, col) {
				return data['totalTimeSeconds'] / data['executionCount'];
			}
		},
		{
			header: 'Avg Time Nano', dataIndex: 'avgTimeNano', width: 40, type: 'int', sortable: false, tooltip: 'Average Execution Time in Nano Seconds', hidden: true,
			renderer: function(value, metaData, r) {
				const data = r.data;
				return TCG.numberFormat(data['totalTimeNano'] / data['executionCount'], '0,000.000');
			},
			summaryCalculation: function(v, r, field, data, col) {
				return data['totalTimeNano'] / data['executionCount'];
			}
		},

		{header: 'DB Hits', dataIndex: 'dbExecutionCount', type: 'int', summaryType: 'sum', width: 40, tooltip: 'Total Number of DB Hits', hidden: true},
		{header: 'Total DB Time', dataIndex: 'totalDBTimeNano', width: 40, type: 'int', tooltip: 'Total DB Execution Time in Nano Seconds', summaryType: 'sum', hidden: true},
		{
			header: 'Total DB Time', dataIndex: 'totalDBTime', width: 40, type: 'float', tooltip: 'Total DB Execution Time in Seconds', summaryType: 'sum',
			hidden: true,
			renderer: function(value, metaData, r) {
				return TCG.numberFormat(value, '0,000.0000');
			}
		},
		{header: 'Min DB Time Nano', dataIndex: 'minDBTimeNano', type: 'int', summaryType: 'min', width: 40, tooltip: 'Minimum DB Execution Time in Nano Seconds', hidden: true},
		{
			header: 'Min DB Time', dataIndex: 'minDBTime', type: 'float', summaryType: 'min', width: 40, tooltip: 'Minimum DB Execution Time in Seconds',
			hidden: true,
			renderer: function(value, metaData, r) {
				return TCG.numberFormat(value, '0,000.0000');
			}
		},
		{header: 'Max DB Time Nano', dataIndex: 'maxDBTimeNano', type: 'int', summaryType: 'max', width: 40, tooltip: 'Maximum DB Execution Time in Nano Seconds', hidden: true},
		{
			header: 'Max DB Time', dataIndex: 'maxDBTime', type: 'float', summaryType: 'max', width: 40, tooltip: 'Maximum DB Execution Time in Seconds',
			hidden: true,
			renderer: function(value, metaData, r) {
				return TCG.numberFormat(value, '0,000.0000');
			}
		},
		{
			header: 'Avg DB Time Nano', dataIndex: 'averageDBTime', width: 40, type: 'float', sortable: false, tooltip: 'Average DB Execution time in Nano Seconds',
			hidden: true,
			renderer: function(value, metaData, r) {
				const data = r.data;
				return TCG.numberFormat(data['totalDBTimeNano'] / data['dbExecutionCount'], '0,000.00');
			},
			summaryCalculation: function(v, r, field, data, col) {
				return data['totalDBTimeNano'] / data['dbExecutionCount'];
			}
		},
		{
			header: 'Avg DB Time', dataIndex: 'agvDBTimeSeconds', width: 40, type: 'float', tooltip: 'Average DB Execution Time in Seconds', useNull: true,
			hidden: true,
			renderer: function(value, metaData, r) {
				return TCG.numberFormat(value, '0,000.0000');
			}
		}
	],
	plugins: {ptype: 'gridsummary'},
	getStaticLoadParams: function(firstLoad) {
		return {};
	},
	getLoadParams: function(firstLoad) {
		const params = this.getStaticLoadParams(firstLoad);

		const combo = TCG.getChildByName(this.getTopToolbar(), 'selectWeekdays');
		if (TCG.isBlank(combo.getValue())) {
			combo.setValue('WEEKDAY');
		}

		const v = combo.getValue();
		if (v === 'WEEKDAY') {
			params.selectWeekdays = true;
		}
		else if (v === 'WEEKEND') {
			params.selectWeekdays = false;
		}

		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		return [{
			fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'selectWeekdays', width: 120, minListWidth: 120, forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value',
			store: {
				xtype: 'arraystore',
				fields: ['value', 'name', 'description'],
				data: [
					['ALL', 'All Days', 'Display all dates for all days'],
					['WEEKDAY', 'Only Weekdays', 'Display only dates for weekdays (Monday through Friday)'],
					['WEEKEND', 'Only Weekends', 'Display display only Weekend days (Saturday Sunday)']
				]
			}
		}];
	},
	editor: {
		detailPageClass: 'Clifton.system.statistic.StatisticWindow',
		drillDownOnly: true
	}
});
Ext.reg('system-statistic-history-grid', Clifton.system.statistic.StatisticHistoryGridPanel);

