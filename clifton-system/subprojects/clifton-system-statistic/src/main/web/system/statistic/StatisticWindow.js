Clifton.system.statistic.StatisticWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'Request Statistics',
	iconCls: 'timer',
	width: 1100,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Request Statistics',
				items: [{
					xtype: 'formpanel',
					url: 'systemStatistic.json',
					readOnly: true,
					items: [
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'displayfield'},
							columns: [
								{
									/* left column */
									rows: [
										{xtype: 'linkfield', fieldLabel: 'Source Id', hidden: 'true', detailIdField: 'statisticUri.statisticSource.id', detailPageClass: 'Clifton.system.statistic.StatisticSourceWindow', name: 'statisticUri.statisticSource.id'},
										{xtype: 'linkfield', fieldLabel: 'Source', detailPageClass: 'Clifton.system.statistic.StatisticSourceWindow', detailIdField: 'statisticUri.statisticSource.id', name: 'statisticUri.statisticSource.name'},
										{xtype: 'linkfield', fieldLabel: 'URI Id', hidden: 'true', detailIdField: 'statisticUri.id', detailPageClass: 'Clifton.system.statistic.StatisticUriWindow', name: 'statisticUri.id'},
										{xtype: 'linkfield', fieldLabel: 'Start Time', detailPageClass: 'Clifton.system.statistic.StatisticRunWindow', detailIdField: 'statisticRun.id', name: 'statisticRun.runStartDateAndTime'},
										{fieldLabel: 'Hits', name: 'executionCount', xtype: 'floatfield'},
										{fieldLabel: 'Slow Hits', name: 'slowExecutionCount', xtype: 'floatfield'},
										{fieldLabel: 'Total Time', name: 'totalTimeSeconds', xtype: 'floatfield'},
										{fieldLabel: 'Min Time', name: 'minTotalTimeSeconds', xtype: 'floatfield'},
										{fieldLabel: 'Max Time', name: 'maxTotalTimeSeconds', xtype: 'floatfield'}
									]
								}, {
									/* right column */
									rows: [
										{xtype: 'linkfield', fieldLabel: 'URI', detailPageClass: 'Clifton.system.statistic.StatisticUriWindow', detailIdField: 'statisticUri.id', name: 'statisticUri.requestURI'},
										{xtype: 'linkfield', fieldLabel: 'Run Id', hidden: 'true', detailIdField: 'statisticRun.id', detailPageClass: 'Clifton.system.statistic.StatisticRunWindow', name: 'statisticRun.id'},
										{xtype: 'linkfield', fieldLabel: 'End Time', detailPageClass: 'Clifton.system.statistic.StatisticRunWindow', detailIdField: 'statisticRun.id', name: 'statisticRun.runEndDateAndTime'},
										{fieldLabel: 'DB Hits', name: 'dbExecutionCount', xtype: 'floatfield'},
										{fieldLabel: 'Total DB Time', name: 'totalDBTimeSeconds', xtype: 'floatfield'},
										{fieldLabel: 'Min DB Time', name: 'minDBTimeSeconds', xtype: 'floatfield'},
										{fieldLabel: 'Max DB Time', name: 'maxDBTimeSeconds', xtype: 'floatfield'}
									]
								}
							]
						},

						{
							xtype: 'formgrid-scroll',
							title: 'Slowest Requests',
							storeRoot: 'systemStatisticDetailList',
							readOnly: true,
							height: 300,
							heightResized: true,
							columnsConfig:
								[
									{header: 'Time', width: 125, dataIndex: 'logDateAndTime'},
									{header: 'Time Millis', width: 100, dataIndex: 'logTimeMillis', type: 'int', hidden: true},
									{
										header: 'Duration', width: 65, dataIndex: 'durationNano', type: 'float', tooltip: 'Execution Time in Seconds',
										renderer: function(value, metaData, r) {
											return TCG.numberFormat(value / 1000000000, '0,000.0000');
										}
									},
									{header: 'DB Duration Nano', width: 65, dataIndex: 'dbDurationNano', type: 'int', hidden: true},
									{header: 'DB Execution Count', width: 65, dataIndex: 'dbExecuteCount', type: 'int', hidden: true},
									{header: 'First Hit', width: 60, dataIndex: 'firstHit', type: 'boolean'},
									{header: 'Request Parameters', width: 1000, dataIndex: 'parameters', renderer: TCG.renderText}
								],
							markModified: Ext.emptyFn
						}
					]
				}]
			},


			{
				title: 'Historical Statistics',
				items: [{
					xtype: 'system-statistic-history-grid',
					getStaticLoadParams: function() {
						return {statisticUriId: this.getWindow().getMainFormPanel().getFormValue('statisticUri.id')};
					}
				}]
			}
		]
	}]
});
