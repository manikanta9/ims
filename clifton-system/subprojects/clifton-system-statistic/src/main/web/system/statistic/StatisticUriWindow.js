Clifton.system.statistic.StatisticUriWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Statistic URI',
	iconCls: 'timer',
	width: 1000,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'URI Statistics',
				items: [{
					xtype: 'formpanel',
					labelFieldName: 'requestURI',
					url: 'systemStatisticUri.json',
					labelWidth: 180,
					items: [
						{xtype: 'linkfield', fieldLabel: 'Source Id', hidden: 'true', detailIdField: 'statisticSource.id', detailPageClass: 'Clifton.system.statistic.StatisticSourceWindow', name: 'statisticSource.id'},
						{xtype: 'linkfield', fieldLabel: 'Source', detailPageClass: 'Clifton.system.statistic.StatisticSourceWindow', detailIdField: 'statisticSource.id', name: 'statisticSource.name'},
						{fieldLabel: 'URI', name: 'requestURI', xtype: 'displayfield'},
						{fieldLabel: 'System Table', name: 'systemTable.name', hiddenName: 'systemTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', disableAddNewItem: true},

						{xtype: 'sectionheaderfield', header: 'SLA (use to uncover unusual behavior)', fieldLabel: ''},
						{fieldLabel: 'Min Daily Request Count', name: 'minDailyRequestCount', xtype: 'integerfield'},
						{fieldLabel: 'Max Daily Request Count', name: 'maxDailyRequestCount', xtype: 'integerfield'},
						{fieldLabel: 'Min Daily Total Time (seconds)', name: 'minDailyTotalTimeSeconds', xtype: 'integerfield'},
						{fieldLabel: 'Max Daily Total Time (seconds)', name: 'maxDailyTotalTimeSeconds', xtype: 'integerfield'},
						{xtype: 'label', html: '<hr />'},
						{fieldLabel: 'Min Avg Request Time (ms)', name: 'minAverageRequestTimeMillis', xtype: 'integerfield'},
						{fieldLabel: 'Max Avg Request Time (ms)', name: 'maxAverageRequestTimeMillis', xtype: 'integerfield'},
						{fieldLabel: 'Max Request Time (ms)', name: 'maxRequestTimeMillis', xtype: 'integerfield'}
					]
				}]
			},


			{
				title: 'Historical Statistics',
				items: [{
					xtype: 'system-statistic-history-grid',
					getStaticLoadParams: function() {
						return {statisticUriId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Slow Requests',
				items: [{
					name: 'systemStatisticDetailListFind',
					xtype: 'gridpanel',
					instructions: 'The following slow statistics were collected for selected URI. Time is shown in nano seconds.',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					useBufferView: false, // enables parameter cell wrapping
					columns: [
						{header: 'Time', width: 135, dataIndex: 'logDateAndTime', type: 'date'},
						{header: 'Duration', width: 90, dataIndex: 'durationSeconds', type: 'float', tooltip: 'Execution Time in Seconds', defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'DB Duration Nano', width: 65, dataIndex: 'dbDurationNano', type: 'int', hidden: true},
						{header: 'DB Execution Count', width: 65, dataIndex: 'dbExecuteCount', type: 'int', hidden: true},
						{header: 'First Hit', width: 60, dataIndex: 'firstHit', type: 'boolean'},
						{header: 'Request Parameters', width: 600, dataIndex: 'parameters', renderer: TCG.renderText, openCellDetailWindowOnDblClick: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {statisticUriId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
