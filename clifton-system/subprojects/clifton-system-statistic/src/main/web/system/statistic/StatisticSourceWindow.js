Clifton.system.statistic.StatisticSourceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Statistic Source',
	iconCls: 'timer',
	width: 1000,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Source Statistics',
				items: [{
					xtype: 'formpanel',
					url: 'systemStatisticSource.json',
					labelFieldName: 'name',
					labelWidth: 180,
					items: [
						{fieldLabel: 'Source', name: 'name', xtype: 'displayfield'},
						{fieldLabel: 'Single URI', boxLabel: 'Checking will aggregate historical stats', name: 'singleURI', xtype: 'checkbox'},
						{fieldLabel: 'Source description', name: 'description', xtype: 'textarea', width: 500},

						{xtype: 'sectionheaderfield', header: 'SLA (use to uncover unusual behavior)', fieldLabel: ''},
						{fieldLabel: 'Min Daily Request Count', name: 'minDailyRequestCount', xtype: 'integerfield'},
						{fieldLabel: 'Max Daily Request Count', name: 'maxDailyRequestCount', xtype: 'integerfield'},
						{fieldLabel: 'Min Daily Total Time (seconds)', name: 'minDailyTotalTimeSeconds', xtype: 'integerfield'},
						{fieldLabel: 'Max Daily Total Time (seconds)', name: 'maxDailyTotalTimeSeconds', xtype: 'integerfield'},
						{xtype: 'label', html: '<hr />'},
						{fieldLabel: 'Min Avg Request Time (ms)', name: 'minAverageRequestTimeMillis', xtype: 'integerfield'},
						{fieldLabel: 'Max Avg Request Time (ms)', name: 'maxAverageRequestTimeMillis', xtype: 'integerfield'},
						{fieldLabel: 'Max Request Time (ms)', name: 'maxRequestTimeMillis', xtype: 'integerfield'}
					]
				}]
			},


			{
				title: 'Stats URIs',
				items: [{
					xtype: 'gridpanel',
					name: 'systemStatisticUriListFind', // infers url:
					appendStandardColumns: false,
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 50, hidden: true},
						{header: 'Source', dataIndex: 'statisticSource.name', width: 120, filter: {searchFieldName: 'sourceName'}},
						{header: 'URI', dataIndex: 'requestURI', width: 300, defaultSortColumn: true},
						{header: 'System Table', dataIndex: 'systemTable.name', width: 200, filter: {searchFieldName: 'tableName'}}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {sourceId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.system.statistic.StatisticUriWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
