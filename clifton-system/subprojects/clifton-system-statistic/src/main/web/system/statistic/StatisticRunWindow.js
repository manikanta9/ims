Clifton.system.statistic.StatisticRunWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Statistic Run',
	iconCls: 'timer',
	width: 1200,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Run',
				items: [{
					xtype: 'formpanel',
					labelFieldName: 'runStartDateAndTime',
					url: 'systemStatisticRun.json',
					items: [
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'displayfield'},
							columns: [
								{
									rows: [
										{fieldLabel: 'Start Time', name: 'runStartDateAndTime'}
									]
								},
								{
									rows: [
										{fieldLabel: 'End Time', name: 'runEndDateAndTime'}
									]
								}
							]
						},
						{fieldLabel: 'Run description', name: 'description', xtype: 'textarea'}
					]
				}]
			},


			{
				title: 'Run Statistics',
				items: [{
					xtype: 'system-statistic-history-grid',
					instructions: 'URI specific details for selected daily Run.',
					pageSize: 3000, // avoid paging so that summary is accurate
					getColumnOverrides: () => ([
						{dataIndex: 'statisticRun.runStartDateAndTime', hidden: true, defaultSortColumn: false},
						{dataIndex: 'statisticRun.runEndDateAndTime', hidden: true},
						{dataIndex: 'statisticUri.statisticSource.name', hidden: false},
						{dataIndex: 'statisticUri.requestURI', hidden: false},
						{dataIndex: 'totalTimeSeconds', defaultSortColumn: true, defaultSortDirection: 'DESC'}
					]),
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getLoadParams: function(firstLoad) {
						const params = {statisticRunId: this.getWindow().getMainFormId()};
						const searchPattern = TCG.getChildByName(this.getTopToolbar(), 'searchPattern');
						if (TCG.isNotBlank(searchPattern.getValue())) {
							params['searchPattern'] = searchPattern.getValue();
						}

						if (firstLoad) {
							this.setFilterValue('statisticUri.statisticSource.name', 'WEB', false, true);
							searchPattern.focus(false, 500);
						}

						return params;
					}
				}]
			},


			{
				title: 'Slow Requests',
				items: [{
					name: 'systemStatisticDetailListFind',
					xtype: 'gridpanel',
					instructions: 'The following slow statistics were collected for selected run. Time is shown in nano seconds.',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					useBufferView: false, // enables parameter cell wrapping
					columns: [
						{header: 'Time', width: 135, dataIndex: 'logDateAndTime', type: 'date'},
						{header: 'Duration', width: 90, dataIndex: 'durationSeconds', type: 'float', tooltip: 'Execution Time in Seconds', defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'DB Duration Nano', width: 65, dataIndex: 'dbDurationNano', type: 'int', hidden: true},
						{header: 'DB Execution Count', width: 65, dataIndex: 'dbExecuteCount', type: 'int', hidden: true},
						{header: 'First Hit', width: 60, dataIndex: 'firstHit', type: 'boolean'},
						{header: 'Source', width: 100, dataIndex: 'systemStatistic.statisticUri.statisticSource.name', filter: {searchFieldName: 'sourceName'}},
						{header: 'URI', width: 300, dataIndex: 'systemStatistic.statisticUri.requestURI', filter: {searchFieldName: 'requestURI'}},
						{header: 'Request Parameters', width: 600, dataIndex: 'parameters', renderer: TCG.renderText, openCellDetailWindowOnDblClick: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('systemStatistic.statisticUri.statisticSource.name', 'WEB', false, true);
						}
						return {statisticRunId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
