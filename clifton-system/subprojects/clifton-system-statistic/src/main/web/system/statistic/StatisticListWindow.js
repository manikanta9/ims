Clifton.system.statistic.StatisticListWindow = Ext.extend(TCG.app.Window, {
	id: 'systemStatisticListWindow',
	title: 'System Access Stats',
	iconCls: 'timer',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Request Stats',
				items: [{
					name: 'systemRequestStatsListFind',
					xtype: 'gridpanel',
					instructions: 'The following statistics was collected for system requests. Time is shown in seconds.',
					appendStandardColumns: false,
					limitRequestedProperties: false,
					forceLocalFiltering: true,
					isPagingEnabled: function() {
						return false;
					},
					columns: [
						{header: 'Source', dataIndex: 'requestSource', width: 50},
						{
							header: 'URI', dataIndex: 'requestURI', width: 160,
							renderer: function(v, metaData, r) {
								return Clifton.core.stats.getNormalizedURI(v, r.data.requestSource);
							}
						},
						{
							header: 'Hits', dataIndex: 'executeCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Total Number of URI Hits',
							renderer: function(value, metaData, r) {
								const source = r.data.requestSource;
								const uri = TCG.CacheUtils.get('SystemStatisticUri_NaturalKey', source + ':' + Clifton.core.stats.getNormalizedURI(r.data.requestURI, source));
								return TCG.renderRangeSLA(TCG.numberFormat(value, '0,000'), value, TCG.getValue('minDailyRequestCount', uri), TCG.getValue('maxDailyRequestCount', uri), metaData);
							}
						},
						{header: 'Slow Hits', dataIndex: 'slowExecuteCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Number of URI Hits Slower than 1 Second'},
						{
							header: '% Slow', dataIndex: 'slowExecutePercent', width: 40, type: 'float', sortable: false, tooltip: 'Percent of URI Hits Slower than 1 Second',
							renderer: function(value, metaData, r) {
								const data = r.data;
								return TCG.numberFormat(100 * data['slowExecuteCount'] / data['executeCount'], '0,000.00 %');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return 100 * data['slowExecuteCount'] / data['executeCount'];
							}
						},
						{
							header: 'Total Time', dataIndex: 'totalTimeSeconds', width: 40, type: 'float', summaryType: 'sum', defaultSortColumn: true, defaultSortDirection: 'DESC', tooltip: 'Total Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								const source = r.data.requestSource;
								const uri = TCG.CacheUtils.get('SystemStatisticUri_NaturalKey', source + ':' + Clifton.core.stats.getNormalizedURI(r.data.requestURI, source));
								return TCG.renderRangeSLA(TCG.numberFormat(value, '0,000.00'), value, TCG.getValue('minDailyTotalTimeSeconds', uri), TCG.getValue('maxDailyTotalTimeSeconds', uri), metaData);
							}
						},
						{header: 'Total Time Formatted', dataIndex: 'totalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Min Time', dataIndex: 'minTotalTimeSeconds', width: 40, type: 'float', summaryType: 'min', tooltip: 'Minimum Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.000');
							}
						},
						{header: 'Min Time Formatted', dataIndex: 'minTotalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Max Time', dataIndex: 'maxTotalTimeSeconds', width: 40, type: 'float', summaryType: 'max', tooltip: 'Maximum Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								const source = r.data.requestSource;
								const uri = TCG.CacheUtils.get('SystemStatisticUri_NaturalKey', source + ':' + Clifton.core.stats.getNormalizedURI(r.data.requestURI, source));
								return TCG.renderMaxSLA(TCG.numberFormat(value, '0,000.0000'), 1000 * value, TCG.getValue('maxRequestTimeMillis', uri), metaData);
							}
						},
						{header: 'Max Time Formatted', dataIndex: 'maxTotalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Avg Time', dataIndex: 'avgTimeSeconds', width: 40, type: 'float', summaryType: 'average', tooltip: 'Average Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								const source = r.data.requestSource;
								const uri = TCG.CacheUtils.get('SystemStatisticUri_NaturalKey', source + ':' + Clifton.core.stats.getNormalizedURI(r.data.requestURI, source));
								return TCG.renderRangeSLA(TCG.numberFormat(value, '0,000.0000'), 1000 * value, TCG.getValue('minAverageRequestTimeMillis', uri), TCG.getValue('maxAverageRequestTimeMillis', uri), metaData);
							},
							summaryCalculation: function(v, r, field, data, col) {
								return data['totalTimeSeconds'] / data['executeCount'];
							}
						},
						{header: 'Avg Time Formatted', dataIndex: 'avgTimeFormatted', width: 60, hidden: true}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('requestSource', 'WEB', true, true);
						}
					},
					editor: {
						detailPageClass: 'Clifton.core.stats.StatsWindow',
						getDefaultDataForExisting: function(gridPanel, row) {
							return row.json;
						},
						addEditButtons: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Clear',
								tooltip: 'Remove stats for selected row',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select URI to be cleared.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection clearing is not supported yet.  Please select one row.', 'NOT SUPPORTED');
									}
									else {
										Ext.Msg.confirm('Clear Selected Stats', 'Would you like to clear stats for selected URI?', function(a) {
											if (a === 'yes') {
												const data = sm.getSelected().data;
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Clearing...',
													params: {
														requestSource: data.requestSource,
														requestURI: data.requestURI
													}
												});
												loader.load('systemRequestStatsDelete.json');
											}
										});
									}
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Load SLA',
								tooltip: 'Load SLA information and use it to highlight exceptions',
								iconCls: 'flag-red',
								scope: this,
								handler: function() {
									TCG.data.getDataPromise('systemStatisticUriListFind.json?slaPresent=true', this)
										.then(function(list) {
											if (list) {
												for (let i = 0; i < list.length; i++) {
													const uri = list[i];
													const cacheKey = uri.statisticSource.name + ':' + uri.requestURI;
													TCG.CacheUtils.put('SystemStatisticUri_NaturalKey', uri, cacheKey);
												}
												gridPanel.reload();
											}
										});
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Historical Request',
				items: [{
					name: 'systemStatisticListFind',
					xtype: 'gridpanel',
					instructions: 'The following statistics was collected for system requests. Time is shown in nano seconds.',
					wikiPage: 'IT/IMS Runtime Statistics',
					wikiPageIconCls: 'chart-bar',
					wikiPageIconText: 'Dashboard',
					wikiPageTitle: 'WIKI Dashboard for System Statistic',
					wikiPageMaximized: true,
					appendStandardColumns: false,
					topToolbarSearchParameter: 'searchPattern',
					pageSize: 3000, // avoid paging so that summary is accurate
					columns: [
						{header: 'ID', dataIndex: 'id', width: 10, hidden: true},
						{header: 'Run ID', dataIndex: 'statisticRun.id', width: 65, hidden: true},
						{header: 'From Date', dataIndex: 'statisticRun.runStartDateAndTime', filter: {searchFieldName: 'runStartDateAndTime'}, type: 'date', width: 65},
						{header: 'To Date', dataIndex: 'statisticRun.runEndDateAndTime', filter: {searchFieldName: 'runEndDateAndTime'}, type: 'date', width: 65},
						{header: 'Description', dataIndex: 'statisticRun.description', filter: {searchFieldName: 'runDescription'}, width: 160, hidden: true},
						{header: 'Source', dataIndex: 'statisticUri.statisticSource.name', width: 50, filter: {searchFieldName: 'sourceName'}},
						{header: 'URI', dataIndex: 'statisticUri.requestURI', width: 160, filter: {searchFieldName: 'requestURI'}},
						{header: 'Single URI', dataIndex: 'statisticUri.statisticSource.singleURI', type: 'boolean', width: 60, hidden: true},
						{header: 'Table', dataIndex: 'statisticUri.systemTable.name', width: 40, tooltip: 'Table Datasource', hidden: true},
						{header: 'Hits', dataIndex: 'executionCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Total Number of URI Hits'},
						{header: 'Slow Hits', dataIndex: 'slowExecutionCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Number of URI Hits Slower than 1 Second'},
						{
							header: '% Slow', dataIndex: 'slowExecutePercent', width: 40, type: 'float', sortable: false, tooltip: 'Percent of URI Hits Slower than 1 Second',
							renderer: function(value, metaData, r) {
								const data = r.data;
								return TCG.numberFormat(100 * data['slowExecutionCount'] / data['executionCount'], '0,000.00 %');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return 100 * data['slowExecutionCount'] / data['executionCount'];
							}
						},
						{header: 'Total Time Nano', dataIndex: 'totalTimeNano', width: 40, type: 'int', tooltip: 'Total Execution Time in Nano Seconds', summaryType: 'sum', hidden: true},
						{
							header: 'Total Time', dataIndex: 'totalTimeSeconds', width: 45, type: 'float', tooltip: 'Total Execution Time in Seconds', summaryType: 'sum', defaultSortColumn: true, defaultSortDirection: 'DESC',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.0000');
							}
						},
						{header: 'Min Time Nano', dataIndex: 'minTotalTimeNano', type: 'int', summaryType: 'min', width: 40, tooltip: 'Minimum Execution Time in Nano Seconds', hidden: true},
						{
							header: 'Min Time', dataIndex: 'minTotalTimeSeconds', type: 'float', summaryType: 'min', width: 40, tooltip: 'Minimum Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.0000');
							}
						},
						{header: 'Max Time Nano', dataIndex: 'maxTotalTimeNano', type: 'int', summaryType: 'max', width: 40, tooltip: 'Maximum Execution Time in Nano Seconds', hidden: true},
						{
							header: 'Max Time', dataIndex: 'maxTotalTimeSeconds', type: 'float', summaryType: 'max', width: 40, tooltip: 'Maximum Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.0000');
							}
						},
						{
							header: 'Avg Time', dataIndex: 'avgTimeSeconds', type: 'float', summaryType: 'average', width: 40, tooltip: 'Average Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.000');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return data['totalTimeSeconds'] / data['executionCount'];
							}
						},
						{
							header: 'Avg Time Nano', dataIndex: 'avgTimeNano', width: 40, type: 'int', sortable: false, tooltip: 'Average Execution Time in Nano Seconds', hidden: true,
							renderer: function(value, metaData, r) {
								const data = r.data;
								return TCG.numberFormat(data['totalTimeNano'] / data['executionCount'], '0,000.000');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return data['totalTimeNano'] / data['executionCount'];
							}
						},

						{header: 'DB Hits', dataIndex: 'dbExecutionCount', type: 'int', summaryType: 'sum', width: 40, tooltip: 'Total Number of DB Hits', hidden: true},
						{header: 'Total DB Time', dataIndex: 'totalDBTimeNano', width: 40, type: 'int', tooltip: 'Total DB Execution Time in Nano Seconds', summaryType: 'sum', hidden: true},
						{
							header: 'Total DB Time', dataIndex: 'totalDBTime', width: 40, type: 'float', tooltip: 'Total DB Execution Time in Seconds', summaryType: 'sum',
							hidden: true,
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.0000');
							}
						},
						{header: 'Min DB Time Nano', dataIndex: 'minDBTimeNano', type: 'int', summaryType: 'min', width: 40, tooltip: 'Minimum DB Execution Time in Nano Seconds', hidden: true},
						{
							header: 'Min DB Time', dataIndex: 'minDBTime', type: 'float', summaryType: 'min', width: 40, tooltip: 'Minimum DB Execution Time in Seconds',
							hidden: true,
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.0000');
							}
						},
						{header: 'Max DB Time Nano', dataIndex: 'maxDBTimeNano', type: 'int', summaryType: 'max', width: 40, tooltip: 'Maximum DB Execution Time in Nano Seconds', hidden: true},
						{
							header: 'Max DB Time', dataIndex: 'maxDBTime', type: 'float', summaryType: 'max', width: 40, tooltip: 'Maximum DB Execution Time in Seconds',
							hidden: true,
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.0000');
							}
						},
						{
							header: 'Avg DB Time Nano', dataIndex: 'averageDBTime', width: 40, type: 'float', sortable: false, tooltip: 'Average DB Execution time in Nano Seconds',
							hidden: true,
							renderer: function(value, metaData, r) {
								const data = r.data;
								return TCG.numberFormat(data['totalDBTimeNano'] / data['dbExecutionCount'], '0,000.00');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return data['totalDBTimeNano'] / data['dbExecutionCount'];
							}
						},
						{
							header: 'Avg DB Time', dataIndex: 'agvDBTimeSeconds', width: 40, type: 'float', tooltip: 'Average DB Execution Time in Seconds', useNull: true,
							hidden: true,
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.0000');
							}
						}
					],
					plugins: {ptype: 'gridsummary'},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('statisticRun.runStartDateAndTime', {'after': new Date().add(Date.DAY, -1)});
							this.setFilterValue('statisticUri.statisticSource.name', 'WEB', false, true);
						}
					},
					addFirstToolbarButtons: function(toolBar) {
						toolBar.add({
								text: 'Save Stats',
								tooltip: 'Harvest System Statistics Now',
								iconCls: 'run',
								scope: this,
								handler: function() {
									const grid = this;
									const f = grid.ownerCt;
									Ext.Msg.confirm('Run Now', 'Would you like to harvest statistics now?', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitTarget: f,
												waitMsg: 'Saving...',
												onLoad: function(record, conf) {
													TCG.createComponent('Clifton.core.StatusWindow', {
														defaultData: {status: record}
													});
													grid.reload.call(grid);
												}
											});
											loader.load('systemStatisticSnapshotGenerate.json');
										}
									});
								}
							},
							'-'
						);
					},
					editor: {
						detailPageClass: 'Clifton.system.statistic.StatisticWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Historical Request Details',
				items: [{
					name: 'systemStatisticDetailListFind',
					xtype: 'gridpanel',
					instructions: 'The following statistics were collected for system requests. Time is shown in nano seconds.',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					useBufferView: false, // enables parameter cell wrapping
					columns: [
						{header: 'Time', width: 125, dataIndex: 'logDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'Duration', width: 65, dataIndex: 'durationSeconds', type: 'float', tooltip: 'Execution Time in Seconds'},
						{header: 'DB Duration Nano', width: 65, dataIndex: 'dbDurationNano', type: 'int', hidden: true},
						{header: 'DB Execution Count', width: 65, dataIndex: 'dbExecuteCount', type: 'int', hidden: true},
						{header: 'First Hit', width: 60, dataIndex: 'firstHit', type: 'boolean'},
						{header: 'Source', width: 100, dataIndex: 'systemStatistic.statisticUri.statisticSource.name', filter: {searchFieldName: 'sourceName'}},
						{header: 'URI', width: 300, dataIndex: 'systemStatistic.statisticUri.requestURI', filter: {searchFieldName: 'requestURI'}},
						{header: 'Request Parameters', width: 650, dataIndex: 'parameters', renderer: TCG.renderText, openCellDetailWindowOnDblClick: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('systemStatistic.statisticUri.statisticSource.name', 'WEB', false, true);
							this.setFilterValue('logDateAndTime', {'after': new Date().add(Date.DAY, -7)}, false, true);
						}
					}
				}]
			},


			{
				title: 'Historical Runs',
				items: [{
					name: 'systemStatisticRunExtendedListFind',
					xtype: 'gridpanel',
					instructions: 'The following statistics were collected for system runs.',
					wikiPage: 'IT/IMS Runtime Statistics',
					wikiPageIconCls: 'chart-bar',
					wikiPageIconText: 'Dashboard',
					wikiPageTitle: 'WIKI Dashboard for System Statistic',
					wikiPageMaximized: true,
					appendStandardColumns: false,
					columns: [
						{header: 'ID', dataIndex: 'id', width: 50, hidden: true},
						{
							header: 'Start Date', width: 60, dataIndex: 'runStartDateAndTime', defaultSortColumn: true, defaultSortDirection: 'desc',
							renderer: function(value, metaData, r) {
								return new Date(value).format('D M j, Y');
							}
						},
						{header: 'Start Time', width: 100, dataIndex: 'runStartDateAndTime', type: 'date', defaultSortDirection: 'desc', hidden: true},
						{header: 'End Time', width: 100, dataIndex: 'runEndDateAndTime', type: 'date', hidden: true},
						{header: 'Run Description', width: 250, dataIndex: 'description', renderer: TCG.renderText},
						{
							header: 'Hits', width: 40, dataIndex: 'executionCount', type: 'int', summaryType: 'sum', tooltip: 'Total Number of URI Hits',
							renderer: function(value, metaData, row) {
								return Clifton.system.statistic.renderSourceValueForRow(value, row, '0,000', this.scope.gridPanel, function(formattedValue, source) {
									return TCG.renderRangeSLA(formattedValue, value, TCG.getValue('minDailyRequestCount', source), TCG.getValue('maxDailyRequestCount', source), metaData);
								});
							}
						},
						{header: 'Slow Hits', dataIndex: 'slowExecutionCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Number of URI Hits Slower than 1 Second'},
						{
							header: '% Slow', dataIndex: 'slowExecutePercent', width: 40, type: 'float', sortable: false, tooltip: 'Percent of URI Hits Slower than 1 Second',
							renderer: function(value, metaData, r) {
								const data = r.data;
								return TCG.numberFormat(100 * data['slowExecutionCount'] / data['executionCount'], '0,000.00 %');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return 100 * data['slowExecutionCount'] / data['executionCount'];
							}
						},
						{
							header: 'Total Time', dataIndex: 'totalTimeSeconds', width: 40, type: 'float', summaryType: 'sum', tooltip: 'Total Execution Time in Seconds',
							renderer: function(value, metaData, row) {
								return Clifton.system.statistic.renderSourceValueForRow(value, row, '0,000.00', this.scope.gridPanel, function(formattedValue, source) {
									return TCG.renderRangeSLA(formattedValue, value, TCG.getValue('minDailyTotalTimeSeconds', source), TCG.getValue('maxDailyTotalTimeSeconds', source), metaData);
								});
							}
						},
						{header: 'Total Time Formatted', dataIndex: 'totalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Min Time', dataIndex: 'minTotalTimeSeconds', width: 40, type: 'float', summaryType: 'min', tooltip: 'Minimum Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.000');
							}
						},
						{header: 'Min Time Formatted', dataIndex: 'minTotalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Max Time', dataIndex: 'maxTotalTimeSeconds', width: 40, type: 'float', summaryType: 'max', tooltip: 'Maximum Execution Time in Seconds',
							renderer: function(value, metaData, row) {
								return Clifton.system.statistic.renderSourceValueForRow(value, row, '0,000.000', this.scope.gridPanel, function(formattedValue, source) {
									return TCG.renderMaxSLA(formattedValue, 1000 * value, TCG.getValue('maxRequestTimeMillis', source), metaData);
								});
							}
						},
						{header: 'Max Time Formatted', dataIndex: 'maxTotalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Avg Time', dataIndex: 'avgTimeSeconds', width: 40, type: 'float', summaryType: 'average', tooltip: 'Average Execution Time in Seconds',
							renderer: function(value, metaData, row) {
								return Clifton.system.statistic.renderSourceValueForRow(value, row, '0,000.000', this.scope.gridPanel, function(formattedValue, source) {
									return TCG.renderRangeSLA(formattedValue, 1000 * value, TCG.getValue('minAverageRequestTimeMillis', source), TCG.getValue('maxAverageRequestTimeMillis', source), metaData);
								});
							},
							summaryCalculation: function(v, r, field, data, col) {
								return data['totalTimeSeconds'] / data['executionCount'];
							}
						},
						{header: 'Avg Time Formatted', dataIndex: 'avgTimeFormatted', width: 60, hidden: true}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function(firstLoad) {
						const tb = this.getTopToolbar();
						const sourceCombo = TCG.getChildByName(tb, 'sourceName');

						if (firstLoad) {
							this.setFilterValue('runStartDateAndTime', {'after': new Date().add(Date.MONTH, -3)});
							if (TCG.isBlank(sourceCombo.getValue())) {
								sourceCombo.setValue('WEB');
							}
						}

						const params = {
							sourceId: undefined,
							selectWeekdays: undefined
						};

						const combo = TCG.getChildByName(tb, 'selectWeekdays');
						if (TCG.isBlank(combo.getValue())) {
							combo.setValue('WEEKDAY');
						}

						let v = combo.getValue();
						if (v === 'WEEKDAY') {
							params.selectWeekdays = true;
						}
						else if (v === 'WEEKEND') {
							params.selectWeekdays = false;
						}

						v = sourceCombo.getValue();
						// save current state on store so that it can be retrieved by row renderers
						this.grid.getStore().selectedStatisticSource = v;
						if (TCG.isBlank(v)) {
							return params;
						}

						return Clifton.system.statistic.getSourcePromiseByName(v)
							.then(function(sourceIdExtract) {
								if (sourceIdExtract) {
									params.sourceId = sourceIdExtract.id;
								}
								return params;
							});
					},
					getTopToolbarFilters: function(toolbar) {
						const filters = [];
						filters.push({fieldLabel: 'Source', xtype: 'toolbar-combo', name: 'sourceName', width: 270, url: 'systemStatisticSourceListFind.json', displayField: 'name', valueField: 'name'});
						filters.push({
							fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'selectWeekdays', width: 120, minListWidth: 120, forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value',
							store: new Ext.data.ArrayStore({
								fields: ['value', 'name', 'description'],
								data: [
									['ALL', 'All Days', 'Display all dates for all days'],
									['WEEKDAY', 'Only Weekdays', 'Display only dates for weekdays (Monday through Friday)'],
									['WEEKEND', 'Only Weekends', 'Display display only Weekend days (Saturday Sunday)']
								]
							})
						});
						return filters;
					},
					editor: {
						detailPageClass: 'Clifton.system.statistic.StatisticRunWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Stats URIs',
				items: [{
					name: 'systemStatisticUriListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all URIs in the system that the stats have been collected for.',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					columns: [
						{header: 'ID', dataIndex: 'id', width: 50, hidden: true},
						{header: 'Source', width: 100, dataIndex: 'statisticSource.name', filter: {searchFieldName: 'sourceName'}},
						{header: 'URI', width: 300, dataIndex: 'requestURI', defaultSortColumn: true},
						{header: 'System Table', width: 200, dataIndex: 'systemTable.name', filter: {searchFieldName: 'tableName'}},

						{header: 'Min Daily Count', width: 70, dataIndex: 'minDailyRequestCount', type: 'int', useNull: true},
						{header: 'Max Daily Count', width: 70, dataIndex: 'maxDailyRequestCount', type: 'int', useNull: true},
						{header: 'Min Daily Total Time', width: 70, dataIndex: 'minDailyTotalTimeSeconds', type: 'int', useNull: true, tooltip: 'Minimum Total Execution Time in Seconds for the Day'},
						{header: 'Max Daily Total Time', width: 70, dataIndex: 'maxDailyTotalTimeSeconds', type: 'int', useNull: true, tooltip: 'Maximum Total Execution Time in Seconds for the Day'},
						{header: 'Min Avg Time (ms)', width: 70, dataIndex: 'minAverageRequestTimeMillis', type: 'int', useNull: true},
						{header: 'Max Avg Time (ms)', width: 70, dataIndex: 'maxAverageRequestTimeMillis', type: 'int', useNull: true},
						{header: 'Max Time (ms)', width: 70, dataIndex: 'maxRequestTimeMillis', type: 'int', useNull: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('statisticSource.name', 'WEB', false, true);
						}
					},
					editor: {
						detailPageClass: 'Clifton.system.statistic.StatisticUriWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Stats Sources',
				items: [{
					name: 'systemStatisticSourceListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all URIs in the system that the stats have been collected for.',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					columns: [
						{header: 'ID', dataIndex: 'id', width: 50, hidden: true},
						{header: 'Source', dataIndex: 'name', width: 120, defaultSortColumn: true},
						{header: 'Description', dataIndex: 'description', width: 300},

						{header: 'Min Daily Count', width: 60, dataIndex: 'minDailyRequestCount', type: 'int', useNull: true},
						{header: 'Max Daily Count', width: 60, dataIndex: 'maxDailyRequestCount', type: 'int', useNull: true},
						{header: 'Min Avg Time (ms)', width: 70, dataIndex: 'minAverageRequestTimeMillis', type: 'int', useNull: true},
						{header: 'Max Avg Time (ms)', width: 70, dataIndex: 'maxAverageRequestTimeMillis', type: 'int', useNull: true},
						{header: 'Max Time (ms)', width: 60, dataIndex: 'maxRequestTimeMillis', type: 'int', useNull: true},

						{header: 'Single URI', dataIndex: 'singleURI', type: 'boolean', width: 40}
					],
					editor: {
						detailPageClass: 'Clifton.system.statistic.StatisticSourceWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
