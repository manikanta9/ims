package com.clifton.system.statistic;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemStatisticProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "system-statistic";
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("getSystemStatisticRunExtendedList");

		if (skipMethods.contains(method.getName())) {
			return true;
		}
		return super.isMethodSkipped(method);
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
