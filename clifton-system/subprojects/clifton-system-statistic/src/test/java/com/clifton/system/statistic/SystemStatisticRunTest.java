package com.clifton.system.statistic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 * @author RogerW
 */
public class SystemStatisticRunTest {

	private static final Date START_TIME_TEST_VALUE = new Date(151100000000L);
	private static final Date END_TIME_TEST_VALUE = new Date(151120000000L);


	@Test
	public void constructorTest() {
		SystemStatisticRun statisticRun = new SystemStatisticRun();

		Assertions.assertNull(statisticRun.getRunStartDateAndTime());
		Assertions.assertNull(statisticRun.getRunEndDateAndTime());
	}


	@Test
	public void PopulationTest() {
		SystemStatisticRun statisticRun = new SystemStatisticRun();

		statisticRun.setRunStartDateAndTime(START_TIME_TEST_VALUE);
		statisticRun.setRunEndDateAndTime(END_TIME_TEST_VALUE);

		Assertions.assertEquals(START_TIME_TEST_VALUE, statisticRun.getRunStartDateAndTime());
		Assertions.assertEquals(END_TIME_TEST_VALUE, statisticRun.getRunEndDateAndTime());
	}
}
