package com.clifton.system.statistic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 * @author RogerW
 */
public class SystemStatisticDetailTest {

	// Note:  All of these values are different to assure the set value and the get values are the same and not ambiguous
	private static final long LOG_TIME_TEST_VALUE = 151120000000L;
	private static final Date LOG_DATE_AND_TIME = new Date(LOG_TIME_TEST_VALUE);
	private static final long DB_TOTAL_TIME_TEST_VALUE = 200000000L;
	private static final long TOTAL_TIME_TEST_VALUE = 5000000000L;
	private static final int DB_EXECUTION_COUNT_TEST_VALUE = 5;
	private static final String PARAMETERS_TEST_VALUE = "Parameter Test Value";
	private static final boolean FIRST_HIT_TEST_VALUE = true;


	@Test
	public void testConstructor() {

		SystemStatisticDetail systemStatisticDetail = new SystemStatisticDetail();

		Assertions.assertNull(systemStatisticDetail.getDbDurationNano());

		Assertions.assertNull(systemStatisticDetail.getDbExecuteCount());

		Assertions.assertNull(systemStatisticDetail.getParameters());

		Assertions.assertFalse(systemStatisticDetail.isFirstHit());
	}


	@Test
	public void testPopulation() {
		SystemStatisticDetail systemStatisticDetail = new SystemStatisticDetail();

		systemStatisticDetail.setLogDateAndTime(LOG_TIME_TEST_VALUE);

		systemStatisticDetail.setDurationNano(TOTAL_TIME_TEST_VALUE);

		systemStatisticDetail.setDbDurationNano(DB_TOTAL_TIME_TEST_VALUE);

		systemStatisticDetail.setDbExecuteCount(DB_EXECUTION_COUNT_TEST_VALUE);

		systemStatisticDetail.setParameters(PARAMETERS_TEST_VALUE);

		systemStatisticDetail.setFirstHit(FIRST_HIT_TEST_VALUE);

		Assertions.assertEquals(LOG_DATE_AND_TIME, systemStatisticDetail.getLogDateAndTime());

		Assertions.assertEquals(TOTAL_TIME_TEST_VALUE, systemStatisticDetail.getDurationNano());

		Assertions.assertEquals(DB_TOTAL_TIME_TEST_VALUE, systemStatisticDetail.getDbDurationNano().longValue());

		Assertions.assertEquals(DB_EXECUTION_COUNT_TEST_VALUE, systemStatisticDetail.getDbExecuteCount().intValue());

		Assertions.assertEquals(PARAMETERS_TEST_VALUE, systemStatisticDetail.getParameters());

		Assertions.assertEquals(FIRST_HIT_TEST_VALUE, systemStatisticDetail.isFirstHit());
	}
}
