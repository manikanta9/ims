package com.clifton.system.statistic;

import com.clifton.core.web.stats.SystemRequestStats;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author RogerW
 */
public class SystemStatisticUriTest {

	private static final int SLOW_EXECUTION_COUNT_TEST_VALUE = 10;
	private static final long MIN_TOTAL_TIME_TEST_VALUE = 100000L;
	private static final String SOURCE_TEST_VALUE = "WEB - Source Test Value";
	private static final String URI_TEST_VALUE = "./uri - Uri Test Value";


	@Test
	public void testConstructor() {

		SystemStatisticUri statisticUri = new SystemStatisticUri();

		Assertions.assertNull(statisticUri.getStatisticSource());

		Assertions.assertNull(statisticUri.getRequestURI());

		Assertions.assertNull(statisticUri.getSystemTable());
	}


	@Test
	public void testOf() {
		SystemRequestStats systemRequestStats = new SystemRequestStats(SOURCE_TEST_VALUE, URI_TEST_VALUE, SLOW_EXECUTION_COUNT_TEST_VALUE, MIN_TOTAL_TIME_TEST_VALUE);
		SystemStatisticSource statisticSource = SystemStatisticSource.of(systemRequestStats);

		Assertions.assertEquals(SOURCE_TEST_VALUE, statisticSource.getName());

		Assertions.assertFalse(statisticSource.isSingleURI());
	}
}
