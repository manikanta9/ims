package com.clifton.system.statistic;

import com.clifton.core.util.MathUtils;
import com.clifton.core.web.stats.SystemRequestStats;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author RogerW
 */
public class SystemStatisticTest {

	// Note:  All of these values are different to assure the set value and the get values are the same and not ambiguous
	private static final int EXECUTION_COUNT_TEST_VALUE = 100;
	private static final int SLOW_EXECUTION_COUNT_TEST_VALUE = 10;
	private static final long DB_TOTAL_TIME_TEST_VALUE = 2000000000L;
	private static final long TOTAL_TIME_TEST_VALUE = 5000000000L;
	private static final int DB_EXECUTION_COUNT_TEST_VALUE = 5;
	private static final long MIN_DB_TIME_TEST_VALUE = 10000000L;
	private static final long MIN_TOTAL_TIME_TEST_VALUE = 100000L;
	private static final long MAX_DB_TIME_TEST_VALUE = 200000L;
	private static final long MAX_TOTAL_TIME_TEST_VALUE = 500000L;


	@Test
	public void testPopulation() {

		SystemStatistic systemStatistic = new SystemStatistic();

		systemStatistic.setExecutionCount(EXECUTION_COUNT_TEST_VALUE);
		Assertions.assertEquals(EXECUTION_COUNT_TEST_VALUE, systemStatistic.getExecutionCount());


		systemStatistic.setSlowExecutionCount(SLOW_EXECUTION_COUNT_TEST_VALUE);
		Assertions.assertEquals(SLOW_EXECUTION_COUNT_TEST_VALUE, systemStatistic.getSlowExecutionCount());


		Assertions.assertNull(systemStatistic.getTotalDBTimeNano());
		systemStatistic.setTotalDBTimeNano(DB_TOTAL_TIME_TEST_VALUE);
		Assertions.assertEquals(DB_TOTAL_TIME_TEST_VALUE, (long) systemStatistic.getTotalDBTimeNano());


		systemStatistic.setTotalTimeNano(TOTAL_TIME_TEST_VALUE);
		Assertions.assertEquals(TOTAL_TIME_TEST_VALUE, systemStatistic.getTotalTimeNano());


		Assertions.assertNull(systemStatistic.getDbExecutionCount());
		systemStatistic.setDbExecutionCount(DB_EXECUTION_COUNT_TEST_VALUE);
		Assertions.assertEquals(DB_EXECUTION_COUNT_TEST_VALUE, (long) systemStatistic.getDbExecutionCount());


		Assertions.assertNull(systemStatistic.getMinDBTimeNano());
		systemStatistic.setMinDBTimeNano(MIN_DB_TIME_TEST_VALUE);
		Assertions.assertEquals(MIN_DB_TIME_TEST_VALUE, (long) systemStatistic.getMinDBTimeNano());


		systemStatistic.setMinTotalTimeNano(MIN_TOTAL_TIME_TEST_VALUE);
		Assertions.assertEquals(MIN_TOTAL_TIME_TEST_VALUE, systemStatistic.getMinTotalTimeNano());


		Assertions.assertNull(systemStatistic.getMaxDBTimeNano());
		systemStatistic.setMaxDBTimeNano(MAX_DB_TIME_TEST_VALUE);
		Assertions.assertEquals(MAX_DB_TIME_TEST_VALUE, (long) systemStatistic.getMaxDBTimeNano());

		systemStatistic.setMaxTotalTimeNano(MAX_TOTAL_TIME_TEST_VALUE);
		Assertions.assertEquals(MAX_TOTAL_TIME_TEST_VALUE, systemStatistic.getMaxTotalTimeNano());
	}


	@Test
	public void testDerivedProperties() {
		SystemStatistic systemStatistic = new SystemStatistic();

		//verify derived property default values.
		Assertions.assertNull(systemStatistic.getTotalDBTimeSeconds());
		Assertions.assertNull(systemStatistic.getAvgDBTimeSeconds());
		Assertions.assertNull(systemStatistic.getMaxDBTimeSeconds());

		// fill in values to validate derived values.
		systemStatistic.setExecutionCount(EXECUTION_COUNT_TEST_VALUE);
		systemStatistic.setSlowExecutionCount(SLOW_EXECUTION_COUNT_TEST_VALUE);
		systemStatistic.setTotalDBTimeNano(DB_TOTAL_TIME_TEST_VALUE);
		systemStatistic.setTotalTimeNano(TOTAL_TIME_TEST_VALUE);
		systemStatistic.setDbExecutionCount(DB_EXECUTION_COUNT_TEST_VALUE);
		systemStatistic.setMinDBTimeNano(MIN_DB_TIME_TEST_VALUE);
		systemStatistic.setMinTotalTimeNano(MIN_TOTAL_TIME_TEST_VALUE);
		systemStatistic.setMaxDBTimeNano(MAX_DB_TIME_TEST_VALUE);
		systemStatistic.setMaxTotalTimeNano(MAX_TOTAL_TIME_TEST_VALUE);

		//verify derived property default values.
		assertBigDecimalEquals("TotalDBTimeSeconds", 2., systemStatistic.getTotalDBTimeSeconds());
		assertBigDecimalEquals("AvgDBTimeSeconds", .4, systemStatistic.getAvgDBTimeSeconds());
		assertBigDecimalEquals("MaxDBTimeSeconds", .0002, systemStatistic.getMaxDBTimeSeconds());
	}


	private void assertBigDecimalEquals(String propertyBeingTested, Number expectedValue, BigDecimal actualValue) {
		Assertions.assertTrue(MathUtils.isEqual(expectedValue, actualValue), String.format(".get%s() expected [%s] got [%s]", propertyBeingTested, expectedValue, actualValue));
	}


	@Test
	public void testOf() {
		SystemRequestStats systemRequestStats = new SystemRequestStats("Source", "URI", SLOW_EXECUTION_COUNT_TEST_VALUE, MIN_TOTAL_TIME_TEST_VALUE);
		systemRequestStats.addRequestExecution(MAX_TOTAL_TIME_TEST_VALUE, DB_EXECUTION_COUNT_TEST_VALUE, MAX_DB_TIME_TEST_VALUE);

		SystemStatistic systemStatistic = SystemStatistic.of(systemRequestStats);
		Assertions.assertEquals(1, systemStatistic.getSlowExecutionCount());
		Assertions.assertEquals(MAX_TOTAL_TIME_TEST_VALUE, systemStatistic.getMinTotalTimeNano());
		Assertions.assertEquals(MAX_TOTAL_TIME_TEST_VALUE, systemStatistic.getMaxTotalTimeNano());
		Assertions.assertEquals(DB_EXECUTION_COUNT_TEST_VALUE, (int) systemStatistic.getDbExecutionCount());
		Assertions.assertEquals(MAX_DB_TIME_TEST_VALUE, (long) systemStatistic.getMaxDBTimeNano());
	}
}
