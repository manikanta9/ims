package com.clifton.system.statistic;

import com.clifton.core.web.stats.SystemRequestStats;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author RogerW
 */
public class SystemStatisticSourceTest {

	private static final int SLOW_EXECUTION_COUNT_TEST_VALUE = 10;
	private static final long MIN_TOTAL_TIME_TEST_VALUE = 100000L;
	private static final String SOURCE_TEST_VALUE = "WEB - Source Test Value";
	private static final String URI_TEST_VALUE = "./uri - Uri Test Value";


	@Test
	public void testConstructor() {

		SystemStatisticSource statisticSource = new SystemStatisticSource();

		Assertions.assertNull(statisticSource.getName());

		Assertions.assertNull(statisticSource.getLabel());

		Assertions.assertNull(statisticSource.getDescription());

		Assertions.assertFalse(statisticSource.isSingleURI());
	}


	@Test
	public void testOf() {
		SystemRequestStats systemRequestStats = new SystemRequestStats(SOURCE_TEST_VALUE, URI_TEST_VALUE, SLOW_EXECUTION_COUNT_TEST_VALUE, MIN_TOTAL_TIME_TEST_VALUE);
		SystemStatisticUri statisticUri = SystemStatisticUri.of(systemRequestStats);

		Assertions.assertNull(statisticUri.getStatisticSource());

		Assertions.assertEquals(URI_TEST_VALUE, statisticUri.getRequestURI());
	}
}
