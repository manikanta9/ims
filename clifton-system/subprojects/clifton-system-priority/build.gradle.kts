dependencies {

	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	////////            Internal Dependencies                           ////////
	////////////////////////////////////////////////////////////////////////////

	api(project(":clifton-system"))

	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-system")))
}
