package com.clifton.system.priority;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemPriorityProjectBasicTests extends BasicProjectTests {


	@Override
	public String getProjectPrefix() {
		return "system-priority";
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
