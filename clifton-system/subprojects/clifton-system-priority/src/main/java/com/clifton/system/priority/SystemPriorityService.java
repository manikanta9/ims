package com.clifton.system.priority;

import com.clifton.system.priority.search.SystemPrioritySearchForm;

import java.util.List;


/**
 * @author stevenf
 */
public interface SystemPriorityService {

	public SystemPriority getSystemPriority(short id);


	public SystemPriority getSystemPriorityByName(String name);


	public List<SystemPriority> getSystemPriorityList(SystemPrioritySearchForm searchForm);


	public SystemPriority saveSystemPriority(SystemPriority bean);


	public void deleteSystemPriority(short id);
}
