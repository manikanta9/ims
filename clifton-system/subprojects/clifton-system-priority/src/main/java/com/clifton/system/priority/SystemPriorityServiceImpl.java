package com.clifton.system.priority;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.system.priority.search.SystemPrioritySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author stevenf
 */
@Service
public class SystemPriorityServiceImpl implements SystemPriorityService {

	private AdvancedUpdatableDAO<SystemPriority, Criteria> systemPriorityDAO;

	private DaoNamedEntityCache<SystemPriority> systemPriorityCache;

	////////////////////////////////////////////////////////////////////////////////
	/////////////          System Priority Business Methods             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemPriority getSystemPriority(short id) {
		return getSystemPriorityDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemPriority getSystemPriorityByName(String name) {
		return getSystemPriorityCache().getBeanForKeyValueStrict(getSystemPriorityDAO(), name);
	}


	@Override
	public List<SystemPriority> getSystemPriorityList(SystemPrioritySearchForm searchForm) {
		return getSystemPriorityDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemPriority saveSystemPriority(SystemPriority bean) {
		return getSystemPriorityDAO().save(bean);
	}


	@Override
	public void deleteSystemPriority(short id) {
		getSystemPriorityDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemPriority, Criteria> getSystemPriorityDAO() {
		return this.systemPriorityDAO;
	}


	public void setSystemPriorityDAO(AdvancedUpdatableDAO<SystemPriority, Criteria> systemPriorityDAO) {
		this.systemPriorityDAO = systemPriorityDAO;
	}


	public DaoNamedEntityCache<SystemPriority> getSystemPriorityCache() {
		return this.systemPriorityCache;
	}


	public void setSystemPriorityCache(DaoNamedEntityCache<SystemPriority> systemPriorityCache) {
		this.systemPriorityCache = systemPriorityCache;
	}
}
