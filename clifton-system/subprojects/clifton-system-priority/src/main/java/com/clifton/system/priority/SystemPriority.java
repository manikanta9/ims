package com.clifton.system.priority;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * @author stevenf
 */
@CacheByName
public class SystemPriority extends NamedEntity<Short> {

	public static final String SYSTEM_PRIORITY_IMMEDIATE = "Immediate";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	//Used for sorting the styles on the system priority screen.
	private int order;
	//used for labeling purposed on the admin interface
	private boolean defaultPriority;
	//specifies the cssStyle that will be used when an item has a given priority - allows for emphasizing or de-emphasizing text, e.g. violations.
	private String cssStyle;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (isDefaultPriority()) {
			return getName() + " (Default)";
		}
		return getName();
	}


	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public boolean isDefaultPriority() {
		return this.defaultPriority;
	}


	public void setDefaultPriority(boolean defaultPriority) {
		this.defaultPriority = defaultPriority;
	}


	public String getCssStyle() {
		return this.cssStyle;
	}


	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}
}
