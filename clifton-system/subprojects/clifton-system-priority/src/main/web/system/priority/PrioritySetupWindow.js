Clifton.system.priority.PrioritySetupWindow = Ext.extend(TCG.app.Window, {
	id: 'priorityDefinitionSetupWindow',
	title: 'Priority Setup',
	iconCls: 'priority',
	width: 1100,

	items: [{
		xtype: 'tabpanel',
		items: [{
			title: 'Priorities',
			items: [{
				name: 'systemPriorityListFind',
				xtype: 'gridpanel',
				instructions: 'Priorities are used for classification purposes and define how critical the message is.  For example, data integrity notifications should have the highest priority as there may be a problem with the data; rule definitions may be given higher priority to add visibility to violations that may result in trade errors.',
				topToolbarSearchParameter: 'searchPattern',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Priority Name', width: 50, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
					{header: 'Description', width: 200, dataIndex: 'description'},
					{
						header: 'CSS Style', width: 70, dataIndex: 'cssStyle',
						renderer: function(v, c, r) {
							if (TCG.isNotBlank(v)) {
								c.attr = 'style="' + v + '"';
							}
							return v;
						}
					},
					{header: 'Order', width: 30, dataIndex: 'order', type: 'int'},
					{header: 'Default', width: 30, dataIndex: 'defaultPriority', type: 'boolean'}
				],
				editor: {
					detailPageClass: 'Clifton.system.priority.PriorityWindow'
				}
			}]
		}]
	}]
});

