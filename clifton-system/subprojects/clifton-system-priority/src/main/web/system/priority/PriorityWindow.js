Clifton.system.priority.PriorityWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Priority',
	iconCls: 'flag-red',
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Priorities are used for classification purposes and define how critical the message is.  For example, data integrity notifications should have the highest priority as there may be a problem with the data; rule definitions may be given higher priority to add visibility to violations that may result in trade errors.',
		url: 'systemPriority.json',
		items: [
			{fieldLabel: 'Priority Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'CSS Style', name: 'cssStyle'},
			{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield'},
			{fieldLabel: 'Default', name: 'defaultPriority', xtype: 'checkbox'}
		]
	}]
});
