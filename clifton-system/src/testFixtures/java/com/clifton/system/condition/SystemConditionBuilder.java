package com.clifton.system.condition;

import com.clifton.system.schema.SystemTableBuilder;


public class SystemConditionBuilder {

	private SystemCondition systemCondition;


	private SystemConditionBuilder(SystemCondition systemCondition) {
		this.systemCondition = systemCondition;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemConditionBuilder createActiveExecTradeModify() {
		SystemCondition systemCondition = new SystemCondition();

		systemCondition.setId(148);
		systemCondition.setName("Active Execution Trade Modify Condition");
		systemCondition.setDescription("Evaluates to TRUE if all fields have not been modified for the bean. 'holdingInvestmentAccount', 'executingBrokerCompany', 'expectedUnitPrice', 'averageUnitPrice', 'accountingNotional', 'accrualAmount', 'commissionAmount', 'commissionPerUnit', 'commissionPerUnitOverride', 'feeAmount', 'settlementDate', 'tradeFillList', 'quantityIntended','exchangeRate' and 'order' are the only fields that can be modified.");
		systemCondition.setConditionEntry(SystemConditionEntryBuilder.createActiveTradeModify().toSystemConditionEntry());
		systemCondition.setSystemTable(SystemTableBuilder.createTrade().toSystemTable());

		return new SystemConditionBuilder(systemCondition);
	}


	public static SystemConditionBuilder createDocumentExists() {
		SystemCondition systemCondition = new SystemCondition();

		systemCondition.setId(3);
		systemCondition.setName("Document Exists");
		systemCondition.setDescription("Evaluates to TRUE if the entity has a document associated with it.");
		systemCondition.setConditionEntry(SystemConditionEntryBuilder.createDocumentExists().toSystemConditionEntry());

		return new SystemConditionBuilder(systemCondition);
	}


	public static SystemConditionBuilder createReadOnlyEntity() {
		SystemCondition systemCondition = new SystemCondition();

		systemCondition.setId(34);
		systemCondition.setName("Read Only Entity (Excluding Booking Date and Description)");
		systemCondition.setDescription("Evaluates to TRUE if all fields have not been modified for the bean. 'Booking Date' is the only field that can be modified.");
		systemCondition.setConditionEntry(SystemConditionEntryBuilder.createReadOnly().toSystemConditionEntry());

		return new SystemConditionBuilder(systemCondition);
	}


	public static SystemConditionBuilder createTerminateDateNull() {
		SystemCondition systemCondition = new SystemCondition();

		systemCondition.setId(11);
		systemCondition.setName("Terminate Date NULL");
		systemCondition.setDescription("Evaluates to TRUE if the Terminate Date Field is NOT populated");
		systemCondition.setConditionEntry(SystemConditionEntryBuilder.createTerminateNull().toSystemConditionEntry());

		return new SystemConditionBuilder(systemCondition);
	}


	public static SystemConditionBuilder createExecutedTradeModify() {
		SystemCondition systemCondition = new SystemCondition();

		systemCondition.setId(55);
		systemCondition.setName("Executed Trade Modify Condition");
		systemCondition.setDescription("Evaluates to TRUE if all fields have not been modified for the bean. 'commissionAmount', 'commissionPerUnit', 'feeAmount', 'description' and 'bookingDate' are the only fields that can be modified.");
		systemCondition.setConditionEntry(SystemConditionEntryBuilder.createExecuteTradeModification().toSystemConditionEntry());
		systemCondition.setSystemTable(SystemTableBuilder.createTrade().toSystemTable());

		return new SystemConditionBuilder(systemCondition);
	}


	public static SystemConditionBuilder createUserIsAdminOrGroupDataQty() {
		SystemCondition systemCondition = new SystemCondition();

		systemCondition.setId(246);
		systemCondition.setName("Current User is in Group Data Quality Admins (Or Admin)");
		systemCondition.setDescription("Current User is in Group Data Quality Admins (Or Admin)");
		systemCondition.setConditionEntry(SystemConditionEntryBuilder.createOR().toSystemConditionEntry());

		return new SystemConditionBuilder(systemCondition);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemConditionBuilder withId(int id) {
		getSystemCondition().setId(id);
		return this;
	}


	public SystemConditionBuilder withName(String name) {
		getSystemCondition().setName(name);
		return this;
	}


	public SystemConditionBuilder withDescription(String description) {
		getSystemCondition().setDescription(description);
		return this;
	}


	public SystemCondition toSystemCondition() {
		return this.systemCondition;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemCondition getSystemCondition() {
		return this.systemCondition;
	}
}
