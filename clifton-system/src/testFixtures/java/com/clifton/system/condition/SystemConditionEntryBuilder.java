package com.clifton.system.condition;

import com.clifton.system.bean.SystemBeanBuilder;
import com.clifton.system.condition.validation.ConditionEntryTypes;


public class SystemConditionEntryBuilder {

	private SystemConditionEntry systemConditionEntry;


	private SystemConditionEntryBuilder(SystemConditionEntry systemConditionEntry) {
		this.systemConditionEntry = systemConditionEntry;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemConditionEntryBuilder createActiveTradeModify() {
		SystemConditionEntry systemConditionEntry = new SystemConditionEntry();

		systemConditionEntry.setId(233);
		systemConditionEntry.setEntryType(ConditionEntryTypes.CONDITION);
		systemConditionEntry.setBean(SystemBeanBuilder.createActiveExecModifiedComparison().toSystemBean());

		return new SystemConditionEntryBuilder(systemConditionEntry);
	}


	public static SystemConditionEntryBuilder createDocumentExists() {
		SystemConditionEntry systemConditionEntry = new SystemConditionEntry();

		systemConditionEntry.setId(3);
		systemConditionEntry.setEntryType(ConditionEntryTypes.CONDITION);
		systemConditionEntry.setBean(SystemBeanBuilder.createActiveExecModifiedComparison().toSystemBean());

		return new SystemConditionEntryBuilder(systemConditionEntry);
	}


	public static SystemConditionEntryBuilder createReadOnly() {
		SystemConditionEntry systemConditionEntry = new SystemConditionEntry();

		systemConditionEntry.setId(53);
		systemConditionEntry.setEntryType(ConditionEntryTypes.CONDITION);
		systemConditionEntry.setBean(SystemBeanBuilder.createNotModified().toSystemBean());

		return new SystemConditionEntryBuilder(systemConditionEntry);
	}


	public static SystemConditionEntryBuilder createExecuteTradeModification() {
		SystemConditionEntry systemConditionEntry = new SystemConditionEntry();

		systemConditionEntry.setId(89);
		systemConditionEntry.setEntryType(ConditionEntryTypes.CONDITION);
		systemConditionEntry.setBean(SystemBeanBuilder.createExecuteTradeModification().toSystemBean());

		return new SystemConditionEntryBuilder(systemConditionEntry);
	}


	public static SystemConditionEntryBuilder createTerminateNull() {
		SystemConditionEntry systemConditionEntry = new SystemConditionEntry();

		systemConditionEntry.setId(14);
		systemConditionEntry.setEntryType(ConditionEntryTypes.CONDITION);
		systemConditionEntry.setBean(SystemBeanBuilder.createTerminateDateNull().toSystemBean());

		return new SystemConditionEntryBuilder(systemConditionEntry);
	}


	public static SystemConditionEntryBuilder createOR() {
		SystemConditionEntry systemConditionEntry = new SystemConditionEntry();

		systemConditionEntry.setId(584);
		systemConditionEntry.setEntryType(ConditionEntryTypes.OR);

		return new SystemConditionEntryBuilder(systemConditionEntry);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemConditionEntryBuilder withId(int id) {
		getSystemConditionEntry().setId(id);
		return this;
	}


	public SystemConditionEntry toSystemConditionEntry() {
		return this.systemConditionEntry;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemConditionEntry getSystemConditionEntry() {
		return this.systemConditionEntry;
	}
}
