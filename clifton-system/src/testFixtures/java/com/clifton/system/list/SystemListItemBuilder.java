package com.clifton.system.list;

public class SystemListItemBuilder {

	private SystemListItem systemListItem;


	private SystemListItemBuilder(SystemListItem systemListItem) {
		this.systemListItem = systemListItem;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemListItemBuilder createEmptySystemListItem() {
		return new SystemListItemBuilder(new SystemListItem());
	}


	public static SystemListItemBuilder createUS() {
		SystemListItem systemListItem = new SystemListItem();

		systemListItem.setId(138);
		systemListItem.setSystemList(SystemListEntityBuilder.createUS().toSystemListEntity());
		systemListItem.setValue("US");
		systemListItem.setText("United States");
		systemListItem.setTooltip("US - United States");
		systemListItem.setOrder(1);
		systemListItem.setActive(true);

		return new SystemListItemBuilder(systemListItem);
	}


	public static SystemListItemBuilder createCanada() {
		SystemListItem systemListItem = new SystemListItem();

		systemListItem.setId(139);
		systemListItem.setSystemList(SystemListEntityBuilder.createUS().toSystemListEntity());
		systemListItem.setValue("CA");
		systemListItem.setText("Canada");
		systemListItem.setTooltip("CA - Canada");
		systemListItem.setOrder(1);
		systemListItem.setActive(true);

		return new SystemListItemBuilder(systemListItem);
	}


	public static SystemListItemBuilder createGovernmentPublic() {
		SystemListItem systemListItem = new SystemListItem();

		systemListItem.setId(493);
		systemListItem.setSystemList(SystemListEntityBuilder.createClientRelationshipType().toSystemListEntity());
		systemListItem.setValue("Government (Public)");
		systemListItem.setText("Government (Public)");
		systemListItem.setActive(true);

		return new SystemListItemBuilder(systemListItem);
	}


	public static SystemListItemBuilder createInvestmentPool() {
		SystemListItem systemListItem = new SystemListItem();

		systemListItem.setId(1172);
		systemListItem.setSystemList(SystemListEntityBuilder.createBusinessClientTypes().toSystemListEntity());
		systemListItem.setValue("Investment Pool");
		systemListItem.setText("Investment Pool");
		systemListItem.setActive(true);

		return new SystemListItemBuilder(systemListItem);
	}


	public static SystemListItemBuilder createHealthServicesOrg() {
		SystemListItem systemListItem = new SystemListItem();

		systemListItem.setId(495);
		systemListItem.setSystemList(SystemListEntityBuilder.createClientRelationshipType().toSystemListEntity());
		systemListItem.setValue("Health Service Organization");
		systemListItem.setText("Health Service Organization");
		systemListItem.setActive(true);

		return new SystemListItemBuilder(systemListItem);
	}


	public static SystemListItemBuilder createNonProfit() {
		SystemListItem systemListItem = new SystemListItem();

		systemListItem.setId(521);
		systemListItem.setSystemList(SystemListEntityBuilder.createClientRelationshipSubTypes().toSystemListEntity());
		systemListItem.setValue("Non-Profit");
		systemListItem.setText("Non-Profit");
		systemListItem.setActive(true);

		return new SystemListItemBuilder(systemListItem);
	}


	public static SystemListItemBuilder createParametric() {
		SystemListItem systemListItem = new SystemListItem();

		systemListItem.setId(1037);
		systemListItem.setSystemList(SystemListEntityBuilder.createClientAccountProductCodes().toSystemListEntity());
		systemListItem.setValue("G758-PD");
		systemListItem.setText("G758-PD: Parametric Minneapolis Institutional CEM - US");
		systemListItem.setTooltip("Parametric Minneapolis Institutional CEM - US");
		systemListItem.setActive(true);

		return new SystemListItemBuilder(systemListItem);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemListItemBuilder withId(int id) {
		getSystemListItem().setId(id);
		return this;
	}


	public SystemListItemBuilder withValue(String name) {
		getSystemListItem().setValue(name);
		return this;
	}


	public SystemListItemBuilder withText(String text) {
		getSystemListItem().setText(text);
		return this;
	}


	public SystemListItemBuilder withTooltip(String tooltip) {
		getSystemListItem().setTooltip(tooltip);
		return this;
	}


	public SystemListItemBuilder withOrder(int order) {
		getSystemListItem().setOrder(order);
		return this;
	}


	public SystemListItemBuilder withActive(boolean active) {
		getSystemListItem().setActive(active);
		return this;
	}


	public SystemListItem toSystemListItem() {
		return this.systemListItem;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemListItem getSystemListItem() {
		return this.systemListItem;
	}
}
