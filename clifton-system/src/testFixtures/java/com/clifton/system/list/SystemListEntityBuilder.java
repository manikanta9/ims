package com.clifton.system.list;

import com.clifton.system.schema.SystemDataTypeBuilder;


public class SystemListEntityBuilder {

	private SystemListEntity systemListEntity;


	private SystemListEntityBuilder(SystemListEntity systemListEntity) {
		this.systemListEntity = systemListEntity;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemListEntityBuilder createUS() {
		SystemListEntity systemListEntity = new SystemListEntity();

		systemListEntity.setId(1);
		systemListEntity.setDataType(SystemDataTypeBuilder.createString().toSystemDataType());
		systemListEntity.setName("States");
		systemListEntity.setDescription("US States");
		systemListEntity.setNameSystemDefined(true);

		return new SystemListEntityBuilder(systemListEntity);
	}


	public static SystemListEntityBuilder createClientRelationshipType() {
		SystemListEntity systemListEntity = new SystemListEntity();

		systemListEntity.setId(58);
		systemListEntity.setDataType(SystemDataTypeBuilder.createString().toSystemDataType());
		systemListEntity.setName("Client Relationship Types");
		systemListEntity.setDescription("Client Relationship type selections for Client Relationships, a.k.a. Firm Category");
		systemListEntity.setNameSystemDefined(true);

		return new SystemListEntityBuilder(systemListEntity);
	}


	public static SystemListEntityBuilder createBusinessClientTypes() {
		SystemListEntity systemListEntity = new SystemListEntity();

		systemListEntity.setId(57);
		systemListEntity.setDataType(SystemDataTypeBuilder.createString().toSystemDataType());
		systemListEntity.setName("Business Client Types");
		systemListEntity.setDescription("Client type selections for client category type CLIENT");
		systemListEntity.setNameSystemDefined(true);

		return new SystemListEntityBuilder(systemListEntity);
	}


	public static SystemListEntityBuilder createClientRelationshipSubTypes() {
		SystemListEntity systemListEntity = new SystemListEntity();

		systemListEntity.setId(60);
		systemListEntity.setDataType(SystemDataTypeBuilder.createString().toSystemDataType());
		systemListEntity.setName("Client Relationship Sub-Types");
		systemListEntity.setDescription("Client Relationship sub-type selections for Client Relationships, a.k.a. Firm Sub-Category");
		systemListEntity.setNameSystemDefined(true);

		return new SystemListEntityBuilder(systemListEntity);
	}


	public static SystemListEntityBuilder createClientAccountProductCodes() {
		SystemListEntity systemListEntity = new SystemListEntity();

		systemListEntity.setId(87);
		systemListEntity.setDataType(SystemDataTypeBuilder.createString().toSystemDataType());
		systemListEntity.setName("Client Account: Product Codes");
		systemListEntity.setDescription("Client Account Eaton Vance Product Codes use to classify our accounts");
		systemListEntity.setNameSystemDefined(true);

		return new SystemListEntityBuilder(systemListEntity);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemListEntityBuilder withId(int id) {
		getSystemListEntity().setId(id);
		return this;
	}


	public SystemListEntityBuilder withName(String name) {
		getSystemListEntity().setName(name);
		return this;
	}


	public SystemListEntityBuilder withDescription(String description) {
		getSystemListEntity().setDescription(description);
		return this;
	}


	public SystemListEntity toSystemListEntity() {
		return this.systemListEntity;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemListEntity getSystemListEntity() {
		return this.systemListEntity;
	}
}
