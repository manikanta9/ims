package com.clifton.system.bean;


public class SystemBeanGroupBuilder {

	private SystemBeanGroup systemBeanGroup;


	private SystemBeanGroupBuilder(SystemBeanGroup systemBeanGroup) {
		this.systemBeanGroup = systemBeanGroup;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemBeanGroupBuilder createComparison() {
		SystemBeanGroup systemBeanGroup = new SystemBeanGroup();

		systemBeanGroup.setId((short) 1);
		systemBeanGroup.setName("Comparison");
		systemBeanGroup.setDescription("Evaluates a condition and returns either true or false based on its arguments.");
		systemBeanGroup.setAlias("Comparison");
		systemBeanGroup.setInterfaceClassName("com.clifton.core.comparison.Comparison");

		return new SystemBeanGroupBuilder(systemBeanGroup);
	}


	public static SystemBeanGroupBuilder createSpotMonthCalendar() {
		SystemBeanGroup systemBeanGroup = new SystemBeanGroup();

		systemBeanGroup.setId((short) 37);
		systemBeanGroup.setName("Investment Security Spot Month Calculator");
		systemBeanGroup.setDescription("Calculates the spot month start and end dates for a security. Spot Month is a time period until last delivery or expiration date of the the contract.  Used for Exchange Limits as we enter the Spot month our limits are generally reduced so we start trading out of that contract and into a new one.");
		systemBeanGroup.setAlias("Spot Month Calculator");
		systemBeanGroup.setInterfaceClassName("com.clifton.investment.instrument.spot.calculators.InvestmentSecuritySpotMonthCalculator");

		return new SystemBeanGroupBuilder(systemBeanGroup);
	}


	public static SystemBeanGroupBuilder createInvestmentSecurityGroupRebuildExecutor() {
		SystemBeanGroup systemBeanGroup = new SystemBeanGroup();

		systemBeanGroup.setName("Investment Security Group Rebuild Executor");
		systemBeanGroup.setDescription("Executes an algorithm to rebuild InvestmentSecurities mapped to an InvestmentSecurityGroup.");
		systemBeanGroup.setAlias("Security Group Rebuild Executor");
		systemBeanGroup.setInterfaceClassName("com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor");

		return new SystemBeanGroupBuilder(systemBeanGroup);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanGroupBuilder withId(short id) {
		getSystemBeanGroup().setId(id);
		return this;
	}


	public SystemBeanGroupBuilder withName(String name) {
		getSystemBeanGroup().setName(name);
		return this;
	}


	public SystemBeanGroupBuilder withDescription(String description) {
		getSystemBeanGroup().setDescription(description);
		return this;
	}


	public SystemBeanGroup toSystemBeanGroup() {
		return this.systemBeanGroup;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemBeanGroup getSystemBeanGroup() {
		return this.systemBeanGroup;
	}
}
