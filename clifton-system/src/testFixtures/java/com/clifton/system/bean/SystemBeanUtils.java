package com.clifton.system.bean;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.system.bean.search.SystemBeanTypeSearchForm;

import java.util.ArrayList;
import java.util.List;


/**
 * {@link SystemBeanUtils} provides utility methods for working with {@link SystemBean}s and {@link SystemBeanProperty}s.
 *
 * @author michaelm
 */
public class SystemBeanUtils {

	/**
	 * Checks for an existing {@link SystemBean} with the given beanName and returns it if one exists WITHOUT comparing properties. If there is no existing bean, then it is created
	 * using {@link #createSystemBeanUsingImplementation(SystemBeanService, String, Object)}.
	 */
	public static SystemBean getOrCreateSystemBeanUsingImplementation(SystemBeanService systemBeanService, String beanName, Object beanImplementation) {
		SystemBeanSearchForm beanSearchForm = new SystemBeanSearchForm();
		beanSearchForm.addSearchRestriction("name", ComparisonConditions.EQUALS, beanName);
		SystemBean calculatorBean = CollectionUtils.getFirstElement(systemBeanService.getSystemBeanList(beanSearchForm));
		return calculatorBean != null ? calculatorBean : SystemBeanUtils.createSystemBeanUsingImplementation(systemBeanService, beanName, beanImplementation);
	}


	/**
	 * Creates a new {@link SystemBean} and associated {@link SystemBeanProperty}s based on the provided beanImplementation.
	 */
	public static SystemBean createSystemBeanUsingImplementation(SystemBeanService systemBeanService, String beanName, Object beanImplementation) {
		SystemBeanType beanType = getSystemBeanTypeByBeanImplementationClass(systemBeanService, beanImplementation);
		SystemBean calculatorBean = new SystemBean();
		calculatorBean.setName(beanName);
		calculatorBean.setType(beanType);
		calculatorBean.setPropertyList(SystemBeanUtils.createBeanPropertyListFromBeanImplementation(systemBeanService, beanType, beanImplementation));
		return systemBeanService.saveSystemBean(calculatorBean);
	}


	public static SystemBeanType getSystemBeanTypeByBeanImplementationClass(SystemBeanService systemBeanService, Object beanImplementation) {
		ValidationUtils.assertNotNull(beanImplementation, "Bean Implementation is required in order to find Bean Type.");
		SystemBeanTypeSearchForm searchForm = new SystemBeanTypeSearchForm();
		searchForm.setClassName(beanImplementation.getClass().getName());
		return CollectionUtils.getFirstElementStrict(systemBeanService.getSystemBeanTypeList(searchForm));
	}


	public static List<SystemBeanProperty> createBeanPropertyListFromBeanImplementation(SystemBeanService systemBeanService, SystemBeanType beanType, Object beanImplementation) {
		List<String> nonNullProperties = com.clifton.core.beans.BeanUtils.getNonNullProperties(beanImplementation);
		List<SystemBeanPropertyType> propertyTypeList = systemBeanService.getSystemBeanPropertyTypeListByType(beanType.getId());
		List<SystemBeanProperty> beanPropertyList = new ArrayList<>();
		for (String nonNullProperty : CollectionUtils.getIterable(nonNullProperties)) {
			SystemBeanPropertyType propertyType = CollectionUtils.getStream(propertyTypeList).filter(propType -> propType.getSystemPropertyName().equals(nonNullProperty)).findFirst().orElse(null);
			if (propertyType != null) {
				beanPropertyList.add(createSystemBeanProperty(propertyType, BeanUtils.getPropertyValue(beanImplementation, nonNullProperty)));
			}
		}
		return beanPropertyList;
	}


	public static SystemBeanProperty createSystemBeanProperty(SystemBeanPropertyType propertyType, Object value) {
		SystemBeanProperty beanProperty = new SystemBeanProperty();
		beanProperty.setType(propertyType);

		if (propertyType.getValueGroup() != null) {
			beanProperty.setValue(BeanUtils.getFieldValue(value, "id").toString());
			setBeanPropertyText(beanProperty, value, false);
		}
		else {
			if (propertyType.isMultipleValuesAllowed()) {
				beanProperty.setValue(StringUtils.collectionToDelimitedString(CollectionUtils.getAsList(value), "::"));
				setBeanPropertyText(beanProperty, value, true);
			}
			else {
				beanProperty.setValue(String.valueOf(value));
				setBeanPropertyText(beanProperty, value, false);
			}
		}
		return beanProperty;
	}


	/**
	 * Attempts to set the text of the {@link SystemBeanProperty}. This doesn't work in all cases (e.g. Integer[] investmentSecurityIds will end up with ids as the text instead
	 * of the security label(s)). This could be enhanced to use the metadata on the {@link SystemBeanPropertyType} to look up the value and get the text from there.
	 */
	private static SystemBeanProperty setBeanPropertyText(SystemBeanProperty beanProperty, Object value, boolean multipleValuesAllowed) {
		if (multipleValuesAllowed) {
			beanProperty.setText(StringUtils.collectionToDelimitedString(CollectionUtils.getConverted(CollectionUtils.getAsList(value), BeanUtils::getLabel), "::"));
		}
		else {
			beanProperty.setText(BeanUtils.getLabel(value));
		}
		return beanProperty;
	}
}
