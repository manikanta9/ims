package com.clifton.system.bean;


public class SystemBeanTypeBuilder {

	private final SystemBeanType systemBeanType;


	private SystemBeanTypeBuilder(SystemBeanType systemBeanType) {
		this.systemBeanType = systemBeanType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemBeanTypeBuilder createWithNameAndClass(int id, String typeName, String className) {
		SystemBeanType systemBeanType = new SystemBeanType();
		systemBeanType.setId(id);
		systemBeanType.setName(typeName);
		systemBeanType.setClassName(className);
		return new SystemBeanTypeBuilder(systemBeanType);
	}


	public static SystemBeanTypeBuilder createAreFieldNotModified() {
		SystemBeanType systemBeanType = new SystemBeanType();

		systemBeanType.setId(9);
		systemBeanType.setGroup(SystemBeanGroupBuilder.createComparison().toSystemBeanGroup());
		systemBeanType.setName("Are Fields Not Modified Comparison");
		systemBeanType.setDescription("Returns true if ALL of the given fields have NOT been modified.  Returns false if ANY of the given fields have been modified.  System Managed properties are always excluded, and can optionally include custom fields");
		systemBeanType.setClassName("com.clifton.system.schema.column.comparison.SystemColumnCustomAreFieldsNotModifiedComparison");
		systemBeanType.setSingleton(true);

		return new SystemBeanTypeBuilder(systemBeanType);
	}


	public static SystemBeanTypeBuilder createDocumentExistsComparison() {
		SystemBeanType systemBeanType = new SystemBeanType();

		systemBeanType.setId(18);
		systemBeanType.setGroup(SystemBeanGroupBuilder.createComparison().toSystemBeanGroup());
		systemBeanType.setName("Document Exists Comparison");
		systemBeanType.setDescription("Returns true if the entity has a document associated with it.");
		systemBeanType.setClassName("com.clifton.document.comparisons.DocumentExistsComparison");
		systemBeanType.setSingleton(true);

		return new SystemBeanTypeBuilder(systemBeanType);
	}


	public static SystemBeanTypeBuilder createBeanFieldIsNullComparison() {
		SystemBeanType systemBeanType = new SystemBeanType();

		systemBeanType.setId(5);
		systemBeanType.setGroup(SystemBeanGroupBuilder.createComparison().toSystemBeanGroup());
		systemBeanType.setName("Bean Field Is Null Comparison");
		systemBeanType.setDescription("Compares field value of the specified bean and evaluates to true if the value is null.");
		systemBeanType.setClassName("com.clifton.core.comparison.field.BeanFieldIsNullComparison");
		systemBeanType.setSingleton(true);

		return new SystemBeanTypeBuilder(systemBeanType);
	}


	public static SystemBeanTypeBuilder createSpotMonthCalendar() {
		SystemBeanType systemBeanType = new SystemBeanType();

		systemBeanType.setId(345);
		systemBeanType.setGroup(SystemBeanGroupBuilder.createSpotMonthCalendar().toSystemBeanGroup());
		systemBeanType.setName("Spot Month Calculator From Security Date Fields");
		systemBeanType.setDescription("Spot Month Calculator for securities that can calculate start or end dates of the spot month based on business days before/after specified dates defined on the security. i.e. -2 Business Days before Delivery Month Start until Last Delivery Date, First Notice Date until Last Delivery Date, etc.");
		systemBeanType.setClassName("com.clifton.investment.instrument.spot.calculators.InvestmentSecuritySpotMonthDateFieldCalculator");
		systemBeanType.setSingleton(true);

		return new SystemBeanTypeBuilder(systemBeanType);
	}


	public static SystemBeanTypeBuilder createTradeRollDateInvestmentSecurityGroupRebuildExecutor() {
		SystemBeanType systemBeanType = new SystemBeanType();

		systemBeanType.setGroup(SystemBeanGroupBuilder.createInvestmentSecurityGroupRebuildExecutor().toSystemBeanGroup());
		systemBeanType.setName("Trade Roll Type Investment Security Group Rebuild Executor");
		systemBeanType.setDescription("Rebuilds list of InvestmentSecurities mapped to an InvestmentSecurityGroup using the Trade Roll Type that is referencing the InvestmentSecurityGroup.");
		systemBeanType.setClassName("com.clifton.trade.investment.security.group.rebuild.InvestmentSecurityGroupTradeRollTypeRebuildAwareExecutor");

		return new SystemBeanTypeBuilder(systemBeanType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanTypeBuilder withId(int id) {
		getSystemBeanType().setId(id);
		return this;
	}


	public SystemBeanTypeBuilder withName(String name) {
		getSystemBeanType().setName(name);
		return this;
	}


	public SystemBeanTypeBuilder withDescription(String description) {
		getSystemBeanType().setDescription(description);
		return this;
	}


	public SystemBeanType toSystemBeanType() {
		return this.systemBeanType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemBeanType getSystemBeanType() {
		return this.systemBeanType;
	}
}
