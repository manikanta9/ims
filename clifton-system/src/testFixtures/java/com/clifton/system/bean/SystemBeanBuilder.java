package com.clifton.system.bean;

public class SystemBeanBuilder {

	private SystemBean systemBean;


	private SystemBeanBuilder(SystemBean systemBean) {
		this.systemBean = systemBean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemBeanBuilder createEmpty() {
		return new SystemBeanBuilder(new SystemBean());
	}


	public static SystemBeanBuilder createActiveExecModifiedComparison() {
		SystemBean systemBean = new SystemBean();

		systemBean.setId(365);
		systemBean.setType(SystemBeanTypeBuilder.createAreFieldNotModified().toSystemBeanType());
		systemBean.setName("Active Execution Trade Modify Condition");
		systemBean.setDescription("Evaluates to TRUE if all fields have not been modified for the bean. 'holdingInvestmentAccount', 'executingBrokerCompany', 'expectedUnitPrice', 'averageUnitPrice', 'accountingNotional', 'accrualAmount1', 'accrualAmount2', 'commissionAmount', 'commissionPerUnit', 'commissionPerUnitOverride', 'feeAmount', 'settlementDate', 'tradeFillList', 'quantityIntended' 'description','exchangeRate' and 'order' are the only fields that can be modified.");

		return new SystemBeanBuilder(systemBean);
	}


	public static SystemBeanBuilder createDocumentExistsComparison() {
		SystemBean systemBean = new SystemBean();

		systemBean.setId(6);
		systemBean.setType(SystemBeanTypeBuilder.createDocumentExistsComparison().toSystemBeanType());
		systemBean.setName("Document Exists Comparison");
		systemBean.setDescription("Returns true if the entity has a document associated with it.");

		return new SystemBeanBuilder(systemBean);
	}


	public static SystemBeanBuilder createTerminateDateNull() {
		SystemBean systemBean = new SystemBean();

		systemBean.setId(18);
		systemBean.setType(SystemBeanTypeBuilder.createBeanFieldIsNullComparison().toSystemBeanType());
		systemBean.setName("Terminate Date NULL");
		systemBean.setDescription("Evaluates to TRUE if the Terminate Date Field IS NULL");

		return new SystemBeanBuilder(systemBean);
	}


	public static SystemBeanBuilder createExecuteTradeModification() {
		SystemBean systemBean = new SystemBean();

		systemBean.setId(118);
		systemBean.setType(SystemBeanTypeBuilder.createAreFieldNotModified().toSystemBeanType());
		systemBean.setName("Executed Trade Modify Condition");
		systemBean.setDescription("Evaluates to TRUE if all fields have not been modified for the bean. 'commissionAmount', 'commissionPerUnit', 'feeAmount', 'description' and 'bookingDate' are the only fields that can be modified.");

		return new SystemBeanBuilder(systemBean);
	}


	public static SystemBeanBuilder createDeliveryMonth() {
		SystemBean systemBean = new SystemBean();

		systemBean.setId(1716);
		systemBean.setType(SystemBeanTypeBuilder.createSpotMonthCalendar().toSystemBeanType());
		systemBean.setName("Delivery Month");
		systemBean.setDescription("Close of trading on the first day of the contract month");

		return new SystemBeanBuilder(systemBean);
	}


	public static SystemBeanBuilder createNotModified() {
		SystemBean systemBean = new SystemBean();

		systemBean.setId(61);
		systemBean.setType(SystemBeanTypeBuilder.createAreFieldNotModified().toSystemBeanType());
		systemBean.setName("Read Only Entity (Excluding Booking Date and Description)");
		systemBean.setDescription("Evaluates to TRUE if all fields have not been modified for the bean. 'Booking Date' is the only field that can be modified.\n" +
				"\n" +
				"enableCommissionOverride - not persisted field, so always ignore");

		return new SystemBeanBuilder(systemBean);
	}


	public static SystemBeanBuilder createTradeRollDateInvestmentSecurityGroupRebuildExecutor() {
		SystemBean systemBean = new SystemBean();

		systemBean.setType(SystemBeanTypeBuilder.createTradeRollDateInvestmentSecurityGroupRebuildExecutor().toSystemBeanType());
		systemBean.setName("Rebuild Security Group Linked to Trade Roll Type");
		systemBean.setDescription("Rebuilds list of InvestmentSecurities mapped to an InvestmentSecurityGroup using the Trade Roll Type that is referencing the InvestmentSecurityGroup.");

		return new SystemBeanBuilder(systemBean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanBuilder withId(int id) {
		getSystemBean().setId(id);
		return this;
	}


	public SystemBeanBuilder withName(String name) {
		getSystemBean().setName(name);
		return this;
	}


	public SystemBeanBuilder withDescription(String description) {
		getSystemBean().setDescription(description);
		return this;
	}


	public SystemBean toSystemBean() {
		return this.systemBean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemBean getSystemBean() {
		return this.systemBean;
	}
}
