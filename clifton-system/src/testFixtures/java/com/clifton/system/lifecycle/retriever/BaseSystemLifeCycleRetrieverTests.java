package com.clifton.system.lifecycle.retriever;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


/**
 * The <code>BaseSystemLifeCycleRetrieverTests</code> is a base test class that should be extended when testing a specific life cycle event retriever
 * Supports testing apply to entity true or false (always tests null is false) and stubs out 2 test methods that should be overridden when no results and with results.
 * Additional tests can be added as needed.
 *
 * @author manderson
 */
public abstract class BaseSystemLifeCycleRetrieverTests {


	/**
	 * Return the bean that we are actually testing
	 */
	protected abstract SystemLifeCycleEventRetriever getSystemLifeCycleRetriever();


	/**
	 * Return a list of entities that should always return false when isApplyToEntity is called
	 * You do not have to include null, null should always return false and is tested for all classes automatically
	 */
	protected abstract List<IdentityObject> getApplyToEntityFalseList();


	/**
	 * Return a list of entities that should always return true when isApplyToEntity is called
	 * You do not have to include null, null should always return false and is tested for all classes automatically
	 */
	protected abstract List<IdentityObject> getApplyToEntityTrueList();


	public abstract void testGetSystemLifeCycleEventListForEntity_NoResults();


	public abstract void testGetSystemLifeCycleEventListForEntity_Results();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testApplyToEntity_False() {
		// Null should always return false
		Assertions.assertFalse(getSystemLifeCycleRetriever().isApplyToEntity(null));
		CollectionUtils.asNonNullList(getApplyToEntityFalseList()).forEach((entity) -> Assertions.assertFalse(getSystemLifeCycleRetriever().isApplyToEntity(entity), "Expected Entity " + BeanUtils.getLabel(entity) + " to evaluate to false."));
	}


	@Test
	public void testApplyToEntity_True() {
		getApplyToEntityTrueList().forEach((entity) -> Assertions.assertTrue(getSystemLifeCycleRetriever().isApplyToEntity(entity), "Expected Entity " + BeanUtils.getLabel(entity) + " to evaluate to true."));
	}
}
