package com.clifton.system.lifecycle.retriever;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.executor.BaseTestExecutor;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.lifecycle.SystemLifeCycleEvent;
import com.clifton.system.lifecycle.SystemLifeCycleEventContext;
import com.clifton.system.lifecycle.SystemLifeCycleEventSearchForm;
import com.clifton.system.lifecycle.SystemLifeCycleEventService;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class SystemLifeCycleTestExecutor extends BaseTestExecutor<SystemLifeCycleEvent> {

	public static final String CURRENT_DATE_REPLACEMENT_STRING = "[CURRENT_DATE]";

	private final String mainTableName;
	private final Long mainEntityId;
	private final IdentityObject mainEntity;

	/**
	 * Can be used to be forgiving when validating the Event Date
	 * i.e. Fix connect is down, event uses current date with message but we need to pull that from the result
	 * The Expected String should use [CURRENT_DATE] and the results will be updated with that string
	 */
	private boolean eventDateIsCurrentDate;

	/**
	 * For edge case testing where we don't need the entity
	 * i.e. When FIX connection is down - just want to confirm the error is an event
	 */
	private boolean doNotUseCurrentProcessingEntity;

	private final SystemLifeCycleEventRetriever lifeCycleEventRetriever;

	private final SystemLifeCycleEventService systemLifeCycleEventService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Runs results for a specific retriever and entity
	 */
	public static SystemLifeCycleTestExecutor forTableAndEntityAndRetriever(String mainTableName, IdentityObject mainEntity, SystemLifeCycleEventRetriever lifeCycleEventRetriever) {
		return new SystemLifeCycleTestExecutor(mainTableName, BeanUtils.getIdentityAsLong(mainEntity), mainEntity, lifeCycleEventRetriever, null);
	}


	/**
	 * Runs results for a specific retriever and entityId
	 */
	public static SystemLifeCycleTestExecutor forTableAndEntityIdAndRetriever(String mainTableName, Number mainEntityId, SystemLifeCycleEventRetriever lifeCycleEventRetriever) {
		return new SystemLifeCycleTestExecutor(mainTableName, mainEntityId, null, lifeCycleEventRetriever, null);
	}


	/**
	 * Runs results for a all retrievers for an entity (by table and entity) This would include the related entity look ups if they apply
	 * i.e. Workflow Task has SystemNotes
	 */
	public static SystemLifeCycleTestExecutor forTableAndEntityId(String mainTableName, Number mainEntityId, SystemLifeCycleEventService systemLifeCycleEventService) {
		return new SystemLifeCycleTestExecutor(mainTableName, mainEntityId, null, null, systemLifeCycleEventService);
	}


	private SystemLifeCycleTestExecutor(String mainTableName, Number mainEntityId, IdentityObject mainEntity, SystemLifeCycleEventRetriever lifeCycleEventRetriever, SystemLifeCycleEventService systemLifeCycleEventService) {
		this.mainTableName = mainTableName;
		this.mainEntityId = MathUtils.getNumberAsLong(mainEntityId);
		this.mainEntity = mainEntity;
		this.lifeCycleEventRetriever = lifeCycleEventRetriever;
		this.systemLifeCycleEventService = systemLifeCycleEventService;
	}


	public SystemLifeCycleTestExecutor withEventDateIsCurrentDate(boolean eventDateIsCurrentDate) {
		this.eventDateIsCurrentDate = eventDateIsCurrentDate;
		return this;
	}


	public SystemLifeCycleTestExecutor withDoNotUseCurrentProcessingEntity(boolean doNotUseCurrentProcessingEntity) {
		this.doNotUseCurrentProcessingEntity = doNotUseCurrentProcessingEntity;
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getResultEntityTypeName() {
		return "Life Cycle Event";
	}


	@Override
	protected List<SystemLifeCycleEvent> executeTest() {
		if (this.lifeCycleEventRetriever != null) {
			SystemLifeCycleEventContext context = new SystemLifeCycleEventContext(this.mainTableName, this.mainEntityId);
			if (!this.doNotUseCurrentProcessingEntity) {
				context.setCurrentProcessingEntity(this.mainEntity, this.mainTableName);
			}
			return this.lifeCycleEventRetriever.getSystemLifeCycleEventListForEntity(context);
		}
		AssertUtils.assertNotNull(this.systemLifeCycleEventService, "System Life Cycle Event Service is required if not testing a specific retriever.");
		SystemLifeCycleEventSearchForm searchForm = new SystemLifeCycleEventSearchForm();
		searchForm.setTableName(this.mainTableName);
		searchForm.setEntityId(this.mainEntityId);
		return this.systemLifeCycleEventService.getSystemLifeCycleEventList(searchForm);
	}


	@Override
	protected String[] getStringValuesForResultEntity(SystemLifeCycleEvent resultEntity) {
		String resultEntityString = resultEntity.toStringFormatted();
		if (this.eventDateIsCurrentDate) {
			String regex = "Date: " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_ISO) + " [(0-9)]*[(0-9)]:[(0-5)][(0-9)]:[(0-9)][(0-9)]";
			resultEntityString = resultEntityString.replaceFirst(regex, "Date: " + CURRENT_DATE_REPLACEMENT_STRING);
		}
		return new String[]{resultEntityString};
	}
}
