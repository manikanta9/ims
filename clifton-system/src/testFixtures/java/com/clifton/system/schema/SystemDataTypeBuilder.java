package com.clifton.system.schema;

public class SystemDataTypeBuilder {

	private SystemDataType systemDataType;


	private SystemDataTypeBuilder(SystemDataType systemDataType) {
		this.systemDataType = systemDataType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemDataTypeBuilder createString() {
		SystemDataType systemDataType = new SystemDataType();
		systemDataType.setId((short) 3);
		systemDataType.setName(SystemDataType.STRING);
		systemDataType.setDescription("A string or a set of characters.");

		return new SystemDataTypeBuilder(systemDataType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemDataTypeBuilder withId(short id) {
		getSystemDataType().setId(id);
		return this;
	}


	public SystemDataTypeBuilder withName(String name) {
		getSystemDataType().setName(name);
		return this;
	}


	public SystemDataTypeBuilder withDescription(String description) {
		getSystemDataType().setDescription(description);
		return this;
	}


	public SystemDataType toSystemDataType() {
		return this.systemDataType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemDataType getSystemDataType() {
		return this.systemDataType;
	}
}
