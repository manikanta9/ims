package com.clifton.system.schema;

import com.clifton.core.security.authorization.SecurityResourceBuilder;
import com.clifton.system.audit.SystemAuditTypeBuilder;


public class SystemTableBuilder {

	private SystemTable systemTable;


	private SystemTableBuilder(SystemTable systemTable) {
		this.systemTable = systemTable;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemTableBuilder createInvestmentInstrumentHierarchy() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 81);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createInvestmentInstrumentHierarchy().toSecurityResource());
		systemTable.setName("InvestmentInstrumentHierarchy");
		systemTable.setDescription("Investment Instrument Hierarchy");
		systemTable.setLabel("Investment Instrument Hierarchy");
		systemTable.setDetailScreenClass("Clifton.investment.setup.InstrumentHierarchyWindow");

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createInvestmentSecurity() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 83);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createInvestmentSecurity().toSecurityResource());
		systemTable.setName("InvestmentSecurity");
		systemTable.setDescription("Investment Security");
		systemTable.setLabel("Investment Security");
		systemTable.setDetailScreenClass("Clifton.investment.instrument.SecurityWindow");
		systemTable.setUploadAllowed(true);

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createTrade() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 146);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setAuditType(SystemAuditTypeBuilder.createAuditUpdateOnly().toSystemAuditType());
		systemTable.setSecurityResource(SecurityResourceBuilder.createTrading().toSecurityResource());
		systemTable.setName("Trade");
		systemTable.setDescription("Trade");
		systemTable.setLabel("Trade");
		systemTable.setAlias("T");
		systemTable.setUploadAllowed(true);
		systemTable.setDetailScreenClass("Clifton.trade.TradeWindow");
		systemTable.setListScreenClass("Clifton.trade.TradeListWindow");
		systemTable.setEntityListUrl("tradeListFind.json?orderBy=tradeDate:DESC");
		systemTable.setScreenIconCls("shopping-cart");

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createTradeFill() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 199);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createTrading().toSecurityResource());
		systemTable.setName("TradeFill");
		systemTable.setDescription("Trade Fill");
		systemTable.setLabel("Trade Fill");
		systemTable.setAlias("TF");
		systemTable.setUploadAllowed(true);
		systemTable.setDetailScreenClass("Clifton.trade.TradeFillWindow");

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createPortfolioRun() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 167);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setAuditType(SystemAuditTypeBuilder.createNoAudit().toSystemAuditType());
		systemTable.setSecurityResource(SecurityResourceBuilder.createPortfolioRuns().toSecurityResource());
		systemTable.setName("PortfolioRun");
		systemTable.setDescription("Portfolio Run");
		systemTable.setLabel("Portfolio Run");
		systemTable.setDetailScreenClass("Clifton.portfolio.run.RunWindow");

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createAccountingPositionTransfer() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 175);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createAccountingPositionTransfer().toSecurityResource());
		systemTable.setName("AccountingPositionTransfer");
		systemTable.setDescription("Accounting Position Transfer");
		systemTable.setLabel("Accounting Position Transfer");
		systemTable.setDetailScreenClass("Clifton.accounting.positiontransfer.TransferWindow");
		systemTable.setAlias("PT");
		systemTable.setUploadAllowed(true);

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createOrderBlock() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 117);
		systemTable.setDataSource(SystemDataSourceBuilder.createDefault("OMS").toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createOrder().toSecurityResource());
		systemTable.setName("OrderBlock");
		systemTable.setDescription("Order Block");
		systemTable.setLabel("Order Block");
		systemTable.setDetailScreenClass("Clifton.order.execution.OrderBlockWindow");

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createAccountingPositionTransferDetail() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 176);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createAccountingPositionTransfer().toSecurityResource());
		systemTable.setName("AccountingPositionTransferDetail");
		systemTable.setDescription("Accounting Position Transfer Detail");
		systemTable.setLabel("Accounting Position Transfer Detail");
		systemTable.setDetailScreenClass("Clifton.accounting.positiontransfer.TransferWindow");
		systemTable.setAlias("APTD");
		systemTable.setUploadAllowed(true);

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createCollateralBalance() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 244);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createCollateral().toSecurityResource());
		systemTable.setName("CollateralBalance");
		systemTable.setDescription("Collateral Balance");
		systemTable.setLabel("Collateral Balance");
		systemTable.setAlias("CB");
		systemTable.setDetailScreenClass("Clifton.portfolio.run.RunWindow");

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createAccountingM2MDaily() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 247);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createMarkToMarket().toSecurityResource());
		systemTable.setName("AccountingM2MDaily");
		systemTable.setDescription("Accounting M2M Daily");
		systemTable.setLabel("Accounting M2M Daily");
		systemTable.setAlias("MTM");
		systemTable.setUploadAllowed(true);
		systemTable.setDetailScreenClass("Clifton.accounting.m2m.M2MDailyWindow");

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createLendingRepo() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 293);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createLendingRepo().toSecurityResource());
		systemTable.setName("LendingRepo");
		systemTable.setDescription("Lending Repo");
		systemTable.setLabel("Lending Repo");
		systemTable.setAlias("LR");
		systemTable.setDetailScreenClass("Clifton.lending.repo.RepoWindow");

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createInvestmentInstructionItem() {
		SystemTable systemTable = new SystemTable();

		systemTable.setId((short) 399);
		systemTable.setDataSource(SystemDataSourceBuilder.createIms().toSystemDataSource());
		systemTable.setSecurityResource(SecurityResourceBuilder.createInvestmentInstructions().toSecurityResource());
		systemTable.setName("InvestmentInstructionItem");
		systemTable.setDescription("Investment Instruction Item");
		systemTable.setLabel("Investment Instruction Item");
		systemTable.setAlias("II");
		systemTable.setDetailScreenClass("Clifton.investment.instruction.InstructionItemWindow");

		return new SystemTableBuilder(systemTable);
	}


	public static SystemTableBuilder createTable(short id, String name) {
		SystemTable systemTable = new SystemTable();
		systemTable.setId(id);
		systemTable.setName(name);
		return new SystemTableBuilder(systemTable);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemTableBuilder withId(short id) {
		getSystemTable().setId(id);
		return this;
	}


	public SystemTableBuilder withName(String name) {
		getSystemTable().setName(name);
		return this;
	}


	public SystemTableBuilder withDescription(String description) {
		getSystemTable().setDescription(description);
		return this;
	}


	public SystemTableBuilder withAlias(String alias) {
		getSystemTable().setAlias(alias);
		return this;
	}


	public SystemTable toSystemTable() {
		return this.systemTable;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemTable getSystemTable() {
		return this.systemTable;
	}
}
