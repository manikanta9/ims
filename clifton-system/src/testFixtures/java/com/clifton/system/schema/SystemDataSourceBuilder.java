package com.clifton.system.schema;

public class SystemDataSourceBuilder {

	private SystemDataSource systemDataSource;


	private SystemDataSourceBuilder(SystemDataSource systemDataSource) {
		this.systemDataSource = systemDataSource;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemDataSourceBuilder createIms() {
		return createDefault("IMS").withDescription("Default data source for the main database: CliftonIMS.");
	}


	public static SystemDataSourceBuilder createDefault(String alias) {
		SystemDataSource systemDataSource = new SystemDataSource();

		systemDataSource.setId((short) 1);
		systemDataSource.setName("dataSource");
		systemDataSource.setDescription("Default data source for the main database");
		systemDataSource.setAlias(alias);
		systemDataSource.setDefaultDataSource(true);

		return new SystemDataSourceBuilder(systemDataSource);
	}


	public static SystemDataSourceBuilder createNonDefault(String name, String alias) {
		SystemDataSource systemDataSource = new SystemDataSource();

		systemDataSource.setId((short) 1);
		systemDataSource.setName(name);
		systemDataSource.setDescription("Data source for the " + alias + " database");
		systemDataSource.setAlias(alias);
		systemDataSource.setDefaultDataSource(false);

		return new SystemDataSourceBuilder(systemDataSource);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemDataSourceBuilder withId(short id) {
		getSystemDataSource().setId(id);
		return this;
	}


	public SystemDataSourceBuilder withName(String name) {
		getSystemDataSource().setName(name);
		return this;
	}


	public SystemDataSourceBuilder withDescription(String description) {
		getSystemDataSource().setDescription(description);
		return this;
	}


	public SystemDataSourceBuilder withAlias(String alias) {
		getSystemDataSource().setAlias(alias);
		return this;
	}


	public SystemDataSource toSystemDataSource() {
		return this.systemDataSource;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemDataSource getSystemDataSource() {
		return this.systemDataSource;
	}
}
