package com.clifton.system;


import com.clifton.system.schema.SystemDataType;


public class SystemTestObjectFactory {

	public static final SystemDataType INTEGER;
	public static final SystemDataType DECIMAL;
	public static final SystemDataType STRING;
	public static final SystemDataType DATE;
	public static final SystemDataType BOOLEAN;
	public static final SystemDataType TEXT;


	static {
		INTEGER = new SystemDataType();
		INTEGER.setName("INTEGER");
		INTEGER.setNumeric(true);

		DECIMAL = new SystemDataType();
		DECIMAL.setName("DECIMAL");
		DECIMAL.setNumeric(true);

		STRING = new SystemDataType();
		STRING.setName("STRING");

		DATE = new SystemDataType();
		DATE.setName("DATE");

		BOOLEAN = new SystemDataType();
		BOOLEAN.setName("BOOLEAN");

		TEXT = new SystemDataType();
		TEXT.setName("TEXT");
	}
}
