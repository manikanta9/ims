package com.clifton.system.audit;

public class SystemAuditTypeBuilder {

	private SystemAuditType systemAuditType;


	private SystemAuditTypeBuilder(SystemAuditType systemAuditType) {
		this.systemAuditType = systemAuditType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemAuditTypeBuilder createNoAudit() {
		SystemAuditType systemAuditType = new SystemAuditType();

		systemAuditType.setId((short) 1);
		systemAuditType.setName("NO AUDIT");
		systemAuditType.setDescription("Do not audit anything.");

		return new SystemAuditTypeBuilder(systemAuditType);
	}


	public static SystemAuditTypeBuilder createAuditUpdateOnly() {
		SystemAuditType systemAuditType = new SystemAuditType();

		systemAuditType.setId((short) 2);
		systemAuditType.setName("AUDIT UPDATE ONLY");
		systemAuditType.setDescription("Capture only changes to field values.");
		systemAuditType.setAuditUpdate(true);

		return new SystemAuditTypeBuilder(systemAuditType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemAuditTypeBuilder withId(short id) {
		getSystemAuditType().setId(id);
		return this;
	}


	public SystemAuditTypeBuilder withName(String name) {
		getSystemAuditType().setName(name);
		return this;
	}


	public SystemAuditTypeBuilder withDescription(String description) {
		getSystemAuditType().setDescription(description);
		return this;
	}


	public SystemAuditType toSystemAuditType() {
		return this.systemAuditType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemAuditType getSystemAuditType() {
		return this.systemAuditType;
	}
}
