package com.clifton.system.bean;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author vgomelsky
 */
public class SystemBeanTests {


	@Test
	public void testEntityModifyConditionIsRequiredForNonAdmins() {
		Assertions.assertTrue(new SystemBean().isEntityModifyConditionRequiredForNonAdmins(), "EntityModifyCondition must be required for non-Admin users.");
	}
}
