package com.clifton.system.bean.property;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringDaoCache;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.schema.SystemDataType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>SystemBeanPropertyCacheTests</code> validates our caches that on failures (rollbacks) the cache is properly maintained.
 * <p>
 * 1. System Bean Cache By Name - Single bean cache - see: {@link SelfRegisteringDaoCache}
 * 2. SystemBeanPropertyCache - List cache (that on failure of updating a system bean the properties aren't inserting and the cache is properly cleared) - see {@link com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringDaoListCache}
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("SystemBeanPropertyCacheTests-context.xml")
public class SystemBeanPropertyCacheTests extends BaseInMemoryDatabaseTests {

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private TestService testService;

	@Resource
	private UpdatableDAO<SystemDataType> systemDataTypeDAO;


	@Override
	public void beforeActions() {
		// Migration Actions cannot insert data types because we don't have a save method on the service.
		setCurrentUser();

		if (this.systemDataTypeDAO.findOneByField("name", "INTEGER") == null) {
			SystemDataType integerDataType = new SystemDataType();
			integerDataType.setName("INTEGER");
			integerDataType.setDescription("I'm one of the best data types around!");
			integerDataType.setNumeric(true);
			this.systemDataTypeDAO.save(integerDataType);
		}
	}


	public void setCurrentUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 0);
		user.setUserName("testUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	@Test
	public void testSaveSystemBeanWithError_CacheBeanByNameTest() {
		SystemBeanType systemBeanType = this.systemBeanService.getSystemBeanTypeByName("Current User Is In Group Comparison");
		SystemBean newBean = new SystemBean();
		newBean.setType(systemBeanType);
		newBean.setName("Test Bean");

		boolean exception = false;
		try {
			this.testService.saveSystemBeanAndThrowException(newBean);
		}
		catch (Exception e) {
			exception = true;
		}
		Assertions.assertTrue(exception);

		// Validate the Cache is Empty
		exception = false;
		try {
			this.systemBeanService.getSystemBeanByName(newBean.getName());
		}
		catch (ValidationException e) {
			Assertions.assertEquals("Unable to find System Bean with properties [name] and value(s) [Test Bean]", ExceptionUtils.getOriginalMessage(e));
			exception = true;
		}
		Assertions.assertTrue(exception, "Expected exception for missing bean by name");

		// Save Again Without Error - Need to Clear the ID first
		// In Real Case, Above would be returned to the client and ID wouldn't be sustained
		newBean.setId(null);
		this.systemBeanService.saveSystemBean(newBean);

		// Validate the Cache is Not Empty
		Assertions.assertNotNull(this.systemBeanService.getSystemBeanByName(newBean.getName()));
	}


	@Test
	public void testSaveSystemBeanWithError_ListCacheTest() {
		SystemBeanType systemBeanType = this.systemBeanService.getSystemBeanTypeByName("Current User Is In Group Comparison");
		SystemBean systemBean = this.systemBeanService.getSystemBeanByName("Current User is a Member of Marketing Group");
		// Validate the Cache is Empty
		List<SystemBeanProperty> propertyList = this.systemBeanService.getSystemBeanPropertyListByBeanId(systemBean.getId());
		Assertions.assertTrue(CollectionUtils.isEmpty(propertyList));

		// Add Property Value and Save with Exception
		SystemBeanPropertyType groupPropertyType = CollectionUtils.getFirstElement(this.systemBeanService.getSystemBeanPropertyTypeListByType(systemBeanType.getId()));
		SystemBeanProperty property = new SystemBeanProperty();
		property.setType(groupPropertyType);
		property.setBean(systemBean);

		SecurityGroup securityGroup = this.securityUserService.getSecurityGroupByName("Marketing");
		property.setValue(securityGroup.getId() + "");
		property.setText(securityGroup.getName());

		systemBean.setPropertyList(CollectionUtils.createList(property));


		boolean exception = false;
		try {
			this.testService.saveSystemBeanAndThrowException(systemBean);
		}
		catch (Exception e) {
			exception = true;
		}
		Assertions.assertTrue(exception);

		// Validate the Cache is Still Empty
		propertyList = this.systemBeanService.getSystemBeanPropertyListByBeanId(systemBean.getId());
		// THE LIST SHOULD BE EMPTY - NOT SIZE 1 WITH NULL ELEMENT
		Assertions.assertEquals(0, CollectionUtils.getSize(propertyList));

		// Save Again Without Error - Need to Clear the Property ID first
		// In Real Case, Above would be returned to the client and ID wouldn't be sustained
		property.setId(null);
		this.systemBeanService.saveSystemBean(systemBean);
		propertyList = this.systemBeanService.getSystemBeanPropertyListByBeanId(systemBean.getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(propertyList));
	}
}
