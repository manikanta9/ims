package com.clifton.system.bean;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The <code>SystemBeanServiceImplTests</code> class tests {@link SystemBeanServiceImpl} methods.
 *
 * @author vgomelsky
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemBeanServiceImplTests {

	private static final int ALWAYS_FALSE_CONDITION_ID = 1;
	private static final int ALWAYS_TRUE_CONDITION_ID = 2;

	private static final int BEAN_TYPE_WITH_INHERITED_ENTITY_MODIFY_CONDITION_ID = 4;
	private static final int BEAN_TYPE_WITH_OVERRIDDEN_ENTITY_MODIFY_CONDITION_ID = 5;
	private static final int BEAN_TYPE_WITH_NO_ENTITY_MODIFY_CONDITION_ID = 6;
	private static final int BEAN_TYPE_WITH_NO_DUPLICATES = 7;
	private static final int BEAN_TYPE_WITH_DUPLICATES = 8;

	private static final int BEAN_WITH_INHERITED_ENTITY_MODIFY_CONDITION_ID = 6;
	private static final int BEAN_WITH_OVERRIDDEN_ENTITY_MODIFY_CONDITION_ID = 7;
	private static final int BEAN_WITH_NO_ENTITY_MODIFY_CONDITION_ID = 8;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private SystemConditionService systemConditionService;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
	}


	@Test
	public void testSaveBeanWithNoType() {
		Assertions.assertThrows(FieldValidationException.class, () -> {
			SystemBean bean = new SystemBean();
			this.systemBeanService.saveSystemBean(bean);
		});
	}


	@Test
	public void testCreateBeanWithoutRequiredProperty() {
		Assertions.assertThrows(ValidationException.class, () -> {
			SystemBean bean = new SystemBean();
			bean.setName("Test Create");
			bean.setType(this.systemBeanService.getSystemBeanType(1));
			this.systemBeanService.saveSystemBean(bean);
		});
	}


	@Test
	public void testCreateBeanWithEmptyRequiredPropertyValue() {
		Assertions.assertThrows(ValidationException.class, () -> {
			SystemBean bean = new SystemBean();
			bean.setName("Test Create");
			bean.setType(this.systemBeanService.getSystemBeanType(1));

			List<SystemBeanProperty> propertyList = new ArrayList<>();
			SystemBeanProperty property = new SystemBeanProperty();
			property.setType(this.systemBeanService.getSystemBeanPropertyType(100));
			propertyList.add(property);
			bean.setPropertyList(propertyList);

			this.systemBeanService.saveSystemBean(bean);
			Assertions.assertEquals("Test Create", this.systemBeanService.getSystemBean(bean.getId()).getName());
		});
	}


	@Test
	public void testCreateBeanWithProperties() {
		SystemBean bean = new SystemBean();
		bean.setName("Test Create");
		bean.setType(this.systemBeanService.getSystemBeanType(1));

		List<SystemBeanProperty> propertyList = new ArrayList<>();
		SystemBeanProperty property = new SystemBeanProperty();
		property.setType(this.systemBeanService.getSystemBeanPropertyType(100));
		property.setValue("testProperty");
		propertyList.add(property);
		bean.setPropertyList(propertyList);

		this.systemBeanService.saveSystemBean(bean);
		Assertions.assertEquals("Test Create", this.systemBeanService.getSystemBean(bean.getId()).getName());
	}


	@Test
	public void testSaveBeanThatClearsOnePropertyValue() {
		// create a bean with 2 properties first
		SystemBean bean = new SystemBean();
		bean.setName("Test With 2 Props");
		bean.setType(this.systemBeanService.getSystemBeanType(1));

		List<SystemBeanProperty> propertyList = new ArrayList<>();
		SystemBeanProperty property = new SystemBeanProperty();
		property.setType(this.systemBeanService.getSystemBeanPropertyType(100));
		property.setValue("testProperty");
		propertyList.add(property);

		property = new SystemBeanProperty();
		property.setType(this.systemBeanService.getSystemBeanPropertyType(101));
		property.setValue("testValue");
		propertyList.add(property);

		bean.setPropertyList(propertyList);

		this.systemBeanService.saveSystemBean(bean);
		Assertions.assertEquals("Test With 2 Props", this.systemBeanService.getSystemBean(bean.getId()).getName());
		bean = this.systemBeanService.getSystemBean(bean.getId());
		Assertions.assertEquals(2, bean.getPropertyList().size());

		// now remove the second property, save the bean again and check if only 1 property left
		bean.getPropertyList().remove(1);
		this.systemBeanService.saveSystemBean(bean);
		bean = this.systemBeanService.getSystemBean(bean.getId());
		Assertions.assertEquals(1, bean.getPropertyList().size(), "only one property should be set");
	}


	@Test
	public void testSaveBeanFailWithDuplicatesDisallowed() {
		String originalBeanName = "Duplicate bean 1";
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemBeanType type = this.systemBeanService.getSystemBeanType(BEAN_TYPE_WITH_NO_DUPLICATES);
			List<SystemBeanPropertyType> propertyTypeList = this.systemBeanService.getSystemBeanPropertyTypeListByType(BEAN_TYPE_WITH_NO_DUPLICATES);
			String value = "Duplicates disallowed value (fail)";

			// Create first bean
			{
				SystemBean bean = new SystemBean();
				bean.setName(originalBeanName);
				bean.setType(type);
				List<SystemBeanProperty> beanPropertyList = new ArrayList<>();
				for (SystemBeanPropertyType propertyType : propertyTypeList) {
					SystemBeanProperty property = new SystemBeanProperty();
					property.setType(propertyType);
					property.setValue(value);
					beanPropertyList.add(property);
				}
				bean.setPropertyList(beanPropertyList);
				this.systemBeanService.saveSystemBean(bean);
			}

			// Create duplicate bean
			{
				SystemBean bean = new SystemBean();
				bean.setName("Duplicate bean 2");
				bean.setType(type);
				List<SystemBeanProperty> beanPropertyList = new ArrayList<>();
				for (SystemBeanPropertyType propertyType : propertyTypeList) {
					SystemBeanProperty property = new SystemBeanProperty();
					property.setType(propertyType);
					property.setValue(value);
					beanPropertyList.add(property);
				}
				bean.setPropertyList(beanPropertyList);
				this.systemBeanService.saveSystemBean(bean);
			}
		});
		Assertions.assertEquals(String.format("Unable to save bean. Duplicate beans of this type are not allowed. This bean duplicates all properties of the following beans: %s", CollectionUtils.toString(Collections.singletonList(originalBeanName), 5)), ve.getMessage());
	}


	@Test
	public void testSaveBeanSucceedWithDuplicatesDisallowed() {
		SystemBeanType type = this.systemBeanService.getSystemBeanType(BEAN_TYPE_WITH_NO_DUPLICATES);
		List<SystemBeanPropertyType> propertyTypeList = this.systemBeanService.getSystemBeanPropertyTypeListByType(BEAN_TYPE_WITH_NO_DUPLICATES);
		String value = "Duplicates disallowed value (succeed)";

		// Create first bean
		{
			SystemBean bean = new SystemBean();
			bean.setName("Duplicate bean 1");
			bean.setType(type);
			List<SystemBeanProperty> beanPropertyList = new ArrayList<>();
			for (SystemBeanPropertyType propertyType : propertyTypeList) {
				SystemBeanProperty property = new SystemBeanProperty();
				property.setType(propertyType);
				property.setValue(value);
				beanPropertyList.add(property);
			}
			bean.setPropertyList(beanPropertyList);
			this.systemBeanService.saveSystemBean(bean);
		}

		// Create non-duplicate bean
		{
			SystemBean bean = new SystemBean();
			bean.setName("Duplicate bean 2");
			bean.setType(type);
			List<SystemBeanProperty> beanPropertyList = new ArrayList<>();
			for (SystemBeanPropertyType propertyType : propertyTypeList) {
				SystemBeanProperty property = new SystemBeanProperty();
				property.setType(propertyType);
				property.setValue(value + " value 2");
				beanPropertyList.add(property);
			}
			bean.setPropertyList(beanPropertyList);
			this.systemBeanService.saveSystemBean(bean);
		}
	}


	@Test
	public void testSaveBeanSucceedWithDuplicatesAllowed() {
		SystemBeanType type = this.systemBeanService.getSystemBeanType(BEAN_TYPE_WITH_DUPLICATES);
		List<SystemBeanPropertyType> propertyTypeList = this.systemBeanService.getSystemBeanPropertyTypeListByType(BEAN_TYPE_WITH_DUPLICATES);
		String value = "Duplicates allowed value";

		// Create first bean
		{
			SystemBean bean = new SystemBean();
			bean.setName("Duplicate bean 1");
			bean.setType(type);
			List<SystemBeanProperty> beanPropertyList = new ArrayList<>();
			for (SystemBeanPropertyType propertyType : propertyTypeList) {
				SystemBeanProperty property = new SystemBeanProperty();
				property.setType(propertyType);
				property.setValue(value);
				beanPropertyList.add(property);
			}
			bean.setPropertyList(beanPropertyList);
			this.systemBeanService.saveSystemBean(bean);
		}

		// Create duplicate bean
		{
			SystemBean bean = new SystemBean();
			bean.setName("Duplicate bean 2");
			bean.setType(type);
			List<SystemBeanProperty> beanPropertyList = new ArrayList<>();
			for (SystemBeanPropertyType propertyType : propertyTypeList) {
				SystemBeanProperty property = new SystemBeanProperty();
				property.setType(propertyType);
				property.setValue(value);
				beanPropertyList.add(property);
			}
			bean.setPropertyList(beanPropertyList);
			this.systemBeanService.saveSystemBean(bean);
		}
	}


	@Test
	public void testBeanTypeEntityModifyConditionInheritance() {
		SystemCondition alwaysFalseCondition = this.systemConditionService.getSystemCondition(ALWAYS_FALSE_CONDITION_ID);
		SystemBeanType beanType = this.systemBeanService.getSystemBeanType(BEAN_TYPE_WITH_INHERITED_ENTITY_MODIFY_CONDITION_ID);
		SystemCondition condition = beanType.getEntityModifyCondition();
		Assertions.assertEquals(alwaysFalseCondition, condition);
	}


	@Test
	public void testBeanTypeEntityModifyConditionOverridesInheritedValue() {
		SystemCondition alwaysTrueCondition = this.systemConditionService.getSystemCondition(ALWAYS_TRUE_CONDITION_ID);
		SystemBeanType beanType = this.systemBeanService.getSystemBeanType(BEAN_TYPE_WITH_OVERRIDDEN_ENTITY_MODIFY_CONDITION_ID);
		SystemCondition condition = beanType.getEntityModifyCondition();
		Assertions.assertEquals(alwaysTrueCondition, condition);
	}


	@Test
	public void testBeanTypeEntityModifyConditionNoValue() {
		SystemBeanType beanType = this.systemBeanService.getSystemBeanType(BEAN_TYPE_WITH_NO_ENTITY_MODIFY_CONDITION_ID);
		SystemCondition condition = beanType.getEntityModifyCondition();
		Assertions.assertEquals(null, condition);
	}


	@Test
	public void testBeanEntityModifyConditionInheritance() {
		SystemCondition alwaysFalseCondition = this.systemConditionService.getSystemCondition(ALWAYS_FALSE_CONDITION_ID);
		SystemBean bean = this.systemBeanService.getSystemBean(BEAN_WITH_INHERITED_ENTITY_MODIFY_CONDITION_ID);
		SystemCondition condition = bean.getEntityModifyCondition();
		Assertions.assertEquals(alwaysFalseCondition, condition);
	}


	@Test
	public void testBeanEntityModifyConditionOverridesInheritedValue() {
		SystemCondition alwaysFalseCondition = this.systemConditionService.getSystemCondition(ALWAYS_FALSE_CONDITION_ID);
		SystemBean bean = this.systemBeanService.getSystemBean(BEAN_WITH_OVERRIDDEN_ENTITY_MODIFY_CONDITION_ID);
		SystemCondition condition = bean.getEntityModifyCondition();
		Assertions.assertEquals(alwaysFalseCondition, condition);
	}


	@Test
	public void testBeanEntityModifyConditionNoValue() {
		SystemBean bean = this.systemBeanService.getSystemBean(BEAN_WITH_NO_ENTITY_MODIFY_CONDITION_ID);
		SystemCondition condition = bean.getEntityModifyCondition();
		Assertions.assertEquals(null, condition);
	}
}
