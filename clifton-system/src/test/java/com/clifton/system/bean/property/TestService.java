package com.clifton.system.bean.property;

import com.clifton.system.bean.SystemBean;


/**
 * @author manderson
 */
public interface TestService {

	public void saveSystemBeanAndThrowException(SystemBean systemBean);
}
