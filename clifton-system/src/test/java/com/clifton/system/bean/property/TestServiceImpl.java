package com.clifton.system.bean.property;

import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@Service
public class TestServiceImpl implements TestService {

	@Resource
	private SystemBeanService systemBeanService;


	@Override
	@Transactional
	public void saveSystemBeanAndThrowException(SystemBean systemBean) {
		this.systemBeanService.saveSystemBean(systemBean);
		// Load Saved Bean in Cache By Name
		this.systemBeanService.getSystemBeanByName(systemBean.getName());
		systemBean.setDescription("test update");
		this.systemBeanService.saveSystemBean(systemBean);
		throw new RuntimeException("Fake error thrown during system bean save to generate failure");
	}
}
