package com.clifton.system.usedby;

import com.clifton.core.beans.BaseEntity;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.system.usedby.softlink.SystemSoftLink;


/**
 * A {@link SystemSoftLink} test entity. This is modeled after <code>SystemNote</code>.
 *
 * @author MikeH
 */
public class SystemSoftLinkTestEntity extends BaseEntity<Integer> {

	private SystemSoftLinkTestEntityType type;

	@SoftLinkField(tableBeanPropertyName = "type.table")
	private Integer fkFieldId;

	@SoftLinkField(tableBeanPropertyName = "type.linkedTable")
	private Integer linkedFkFieldId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSoftLinkTestEntityType getType() {
		return this.type;
	}


	public void setType(SystemSoftLinkTestEntityType type) {
		this.type = type;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Integer getLinkedFkFieldId() {
		return this.linkedFkFieldId;
	}


	public void setLinkedFkFieldId(Integer linkedFkFieldId) {
		this.linkedFkFieldId = linkedFkFieldId;
	}
}
