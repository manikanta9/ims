package com.clifton.system.usedby;

import com.clifton.core.beans.BaseEntity;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SystemSoftLink;


/**
 * A {@link SystemSoftLink} test entity. This is modeled after <code>SystemNoteType</code>.
 *
 * @author MikeH
 */
public class SystemSoftLinkTestEntityType extends BaseEntity<Integer> {

	private SystemTable table;
	private SystemTable linkedTable;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public SystemTable getLinkedTable() {
		return this.linkedTable;
	}


	public void setLinkedTable(SystemTable linkedTable) {
		this.linkedTable = linkedTable;
	}
}
