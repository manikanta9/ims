package com.clifton.system.usedby;

import com.clifton.core.beans.BaseEntity;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.system.usedby.softlink.SystemSoftLink;


/**
 * A {@link SystemSoftLink} test entity. This is modeled after <code>SystemNoteLink</code>.
 *
 * @author MikeH
 */
public class SystemSoftLinkTestEntityLink extends BaseEntity<Integer> {

	private SystemSoftLinkTestEntity entity;

	@SoftLinkField(tableBeanPropertyName = "entity.type.table", ownerBeanPropertyName = "entity")
	private Integer fkFieldId;

	@SoftLinkField(tableBeanPropertyName = "entity.type.linkedTable", ownerBeanPropertyName = "entity")
	private Integer linkedFkFieldId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSoftLinkTestEntity getEntity() {
		return this.entity;
	}


	public void setEntity(SystemSoftLinkTestEntity entity) {
		this.entity = entity;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Integer getLinkedFkFieldId() {
		return this.linkedFkFieldId;
	}


	public void setLinkedFkFieldId(Integer linkedFkFieldId) {
		this.linkedFkFieldId = linkedFkFieldId;
	}
}
