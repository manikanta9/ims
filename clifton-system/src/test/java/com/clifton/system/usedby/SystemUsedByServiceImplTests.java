package com.clifton.system.usedby;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.usedby.softlink.SystemSoftLink;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>SystemSoftLinkServiceImplTests</code> tests the population of SystemSoftLinkFieldConfig
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemUsedByServiceImplTests {

	@Resource
	private SystemUsedByService systemUsedByService;


	@Test
	public void testOneSoftLinkWithOwner() {
		List<SystemSoftLink> linkList = this.systemUsedByService.getSystemSoftLinkList();
		// Should not return anything because Hierarchy Table is the owner
		Assertions.assertEquals(0, CollectionUtils.getSize(BeanUtils.filter(linkList, SystemSoftLink::getCoalesceOwnerTableName, "SystemHierarchyLink")));

		// Should Return Same link config for Hierarchy Table
		List<SystemSoftLink> hierarchyList = BeanUtils.filter(linkList, SystemSoftLink::getCoalesceOwnerTableName, "SystemHierarchy");
		Assertions.assertEquals(1, CollectionUtils.getSize(hierarchyList));

		SystemSoftLink hierarchyConfig = hierarchyList.get(0);
		Assertions.assertEquals("SystemHierarchyLink", hierarchyConfig.getTableName());
		Assertions.assertEquals("SystemHierarchy", hierarchyConfig.getOwnerTableName());
		Assertions.assertEquals("SystemHierarchy", hierarchyConfig.getCoalesceOwnerTableName());
		Assertions.assertEquals("assignment.table", hierarchyConfig.getTableBeanPropertyName());
		Assertions.assertEquals("fkFieldId", hierarchyConfig.getBeanPropertyName());
		Assertions.assertEquals("hierarchy", hierarchyConfig.getOwnerBeanPropertyName());
	}


	@Test
	public void testMultipleSoftLinksWithAndWithoutOwner() {
		List<SystemSoftLink> linkList = this.systemUsedByService.getSystemSoftLinkList();

		// Should not have anything because Note Table is the owner
		Assertions.assertEquals(0, CollectionUtils.getSize(BeanUtils.filter(linkList, SystemSoftLink::getCoalesceOwnerTableName, "SystemSoftListTestEntityLink")));

		// Should Return link config for SystemSoftLinkTestEntity and SystemSoftLinkTestEntityLink Tables
		List<SystemSoftLink> noteLinkList = BeanUtils.filter(linkList, SystemSoftLink::getCoalesceOwnerTableName, "SystemSoftLinkTestEntity");
		Assertions.assertEquals(4, CollectionUtils.getSize(noteLinkList));

		boolean fkFieldFound = false;
		boolean linkedFkFieldFound = false;
		boolean linkFkFieldFound = false;
		boolean linkLinkedFkFieldFound = false;

		for (SystemSoftLink config : noteLinkList) {
			Assertions.assertEquals("SystemSoftLinkTestEntity", config.getCoalesceOwnerTableName());
			if ("SystemSoftLinkTestEntityLink".equals(config.getTableName())) {
				Assertions.assertEquals("SystemSoftLinkTestEntity", config.getOwnerTableName());
				Assertions.assertEquals("entity", config.getOwnerBeanPropertyName());
				if ("fkFieldId".equals(config.getBeanPropertyName())) {
					linkFkFieldFound = true;
					Assertions.assertEquals("entity.type.table", config.getTableBeanPropertyName());
				}
				else {
					linkLinkedFkFieldFound = true;
					Assertions.assertEquals("entity.type.linkedTable", config.getTableBeanPropertyName());
				}
			}
			else if ("SystemSoftLinkTestEntity".equals(config.getTableName())) {
				Assertions.assertTrue(StringUtils.isEmpty(config.getOwnerTableName()));
				Assertions.assertTrue(StringUtils.isEmpty(config.getOwnerBeanPropertyName()));
				if ("fkFieldId".equals(config.getBeanPropertyName())) {
					fkFieldFound = true;
					Assertions.assertEquals("type.table", config.getTableBeanPropertyName());
				}
				else {
					linkedFkFieldFound = true;
					Assertions.assertEquals("type.linkedTable", config.getTableBeanPropertyName());
				}
			}
			else {
				Assertions.fail("Wrong SoftLink object returned when looking for SystemSoftLinkTestEntity Soft Links - " + config.getTableName());
			}
		}

		Assertions.assertTrue(fkFieldFound, "Did not find SoftLink for SystemSoftLinkTestEntity.fkFieldId");
		Assertions.assertTrue(linkedFkFieldFound, "Did not find SoftLink for SystemSoftLinkTestEntity.linkedFkFieldId");
		Assertions.assertTrue(linkFkFieldFound, "Did not find SoftLink for SystemSoftLinkTestEntityLink.fkFieldId");
		Assertions.assertTrue(linkLinkedFkFieldFound, "Did not find SoftLink for SystemSoftLinkTestEntityLink.linkedFkFieldId");
	}
}
