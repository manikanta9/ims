package com.clifton.system.hierarchy;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>SystemHierarchyTestEntity</code> ...
 *
 * @author manderson
 */
public class SystemHierarchyTestEntity extends NamedEntity<Integer> {

	// NOTHING HERE
}
