package com.clifton.system.hierarchy.comparison;

import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.hierarchy.SystemHierarchyTestEntity;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemHierarchyLinkExistsComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	@Resource
	private UpdatableDAO<SystemHierarchyTestEntity> systemHierarchyTestEntityDAO;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	private static final String TEST_ENTITY_TABLE_NAME = "SystemHierarchyTestEntity";
	private static final short HIERARCHY_CATEGORY_BUSINESS = 2;
	private static final short HIERARCHY_BUSINESS_MARKETING = 202;
	private static final short HIERARCHY_BUSINESS_TRADING = 203;


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
	}


	@Test
	public void testSystemHierarchyLinkExistsComparison() {
		SystemHierarchyLinkExistsComparison existsComparison = new SystemHierarchyLinkExistsComparison();
		existsComparison.setSystemHierarchyCategoryId(HIERARCHY_CATEGORY_BUSINESS);
		existsComparison.setSystemHierarchyId(HIERARCHY_BUSINESS_TRADING);
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(existsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		SystemHierarchyLinkNotExistsComparison notExistsComparison = new SystemHierarchyLinkNotExistsComparison();
		notExistsComparison.setSystemHierarchyCategoryId(HIERARCHY_CATEGORY_BUSINESS);
		notExistsComparison.setSystemHierarchyId(HIERARCHY_BUSINESS_TRADING);
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(notExistsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		SystemHierarchyTestEntity beanTrading = setupTestBean();
		this.systemHierarchyAssignmentService.saveSystemHierarchyLink(TEST_ENTITY_TABLE_NAME, beanTrading.getId(), HIERARCHY_BUSINESS_TRADING);
		validateComparison(existsComparison, beanTrading, true);
		validateComparison(notExistsComparison, beanTrading, false);

		SystemHierarchyTestEntity beanMarketing = setupTestBean();
		this.systemHierarchyAssignmentService.saveSystemHierarchyLink(TEST_ENTITY_TABLE_NAME, beanTrading.getId(), HIERARCHY_BUSINESS_MARKETING);
		validateComparison(existsComparison, beanMarketing, false);
		validateComparison(notExistsComparison, beanMarketing, true);

		SystemHierarchyTestEntity beanNone = setupTestBean();
		validateComparison(existsComparison, beanNone, false);
		validateComparison(notExistsComparison, beanNone, true);
	}


	private void validateComparison(SystemHierarchyLinkExistsComparison comparison, SystemHierarchyTestEntity bean, boolean expectTrue) {
		SimpleComparisonContext context = new SimpleComparisonContext();
		if (expectTrue) {
			Assertions.assertTrue(comparison.evaluate(bean, context));
			Assertions.assertEquals(getExpectedMessage(comparison, true), context.getTrueMessage());
		}
		else {
			Assertions.assertFalse(comparison.evaluate(bean, context));
			Assertions.assertEquals(getExpectedMessage(comparison, false), context.getFalseMessage());
		}
	}


	private String getExpectedMessage(SystemHierarchyLinkExistsComparison comparison, boolean expectTrue) {
		String msg = "(System Hierarchy Link to [Business Unit: Trading]";
		if ((comparison.isExistsComparison() && expectTrue) || (!comparison.isExistsComparison() && !expectTrue)) {
			return msg + " Exists)";
		}
		return msg + " Does Not Exist)";
	}


	////////////////////////////////////////////////////


	private SystemHierarchyTestEntity setupTestBean() {
		SystemHierarchyTestEntity bean = new SystemHierarchyTestEntity();
		bean.setName("Test Link Condition Bean");
		bean.setDescription("Test");
		this.systemHierarchyTestEntityDAO.save(bean);
		return bean;
	}

	////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
