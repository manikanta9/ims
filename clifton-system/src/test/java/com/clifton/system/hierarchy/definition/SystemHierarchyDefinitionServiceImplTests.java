package com.clifton.system.hierarchy.definition;

import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.hierarchy.SystemHierarchyTestEntity;
import com.clifton.system.hierarchy.definition.search.SystemHierarchySearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>SystemHierarchyDefinitionServiceImplTests</code> class tests {@link SystemHierarchyDefinitionServiceImpl} methods.
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemHierarchyDefinitionServiceImplTests {

	@Resource
	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	@Resource
	private UpdatableDAO<SystemHierarchyTestEntity> systemHierarchyTestEntityDAO;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
	}


	@Test
	public void testSaveSystemHierarchyLocation() {
		// Add a new Location (No restrictions on max depth)
		SystemHierarchy hierarchy = new SystemHierarchy();
		hierarchy.setParent(this.systemHierarchyDefinitionService.getSystemHierarchy(Short.parseShort("101")));
		hierarchy.setName("NY");
		hierarchy.setCategory(this.systemHierarchyDefinitionService.getSystemHierarchyCategory(MathUtils.SHORT_ONE));
		this.systemHierarchyDefinitionService.saveSystemHierarchy(hierarchy);

		hierarchy = this.systemHierarchyDefinitionService.getSystemHierarchy(hierarchy.getId());
		Assertions.assertEquals("USA / NY", hierarchy.getNameExpanded());

		SystemHierarchy childHierarchy = new SystemHierarchy();
		childHierarchy.setParent(hierarchy);
		childHierarchy.setName("NY City");
		childHierarchy.setCategory(hierarchy.getCategory());
		this.systemHierarchyDefinitionService.saveSystemHierarchy(childHierarchy);

		childHierarchy = this.systemHierarchyDefinitionService.getSystemHierarchy(childHierarchy.getId());
		Assertions.assertEquals("USA / NY / NY City", childHierarchy.getNameExpanded());

		SystemHierarchySearchForm searchForm = new SystemHierarchySearchForm();
		searchForm.setCategoryId(hierarchy.getCategory().getId());
		searchForm.setOrderBy("name");
		Assertions.assertEquals(1, CollectionUtils.getSize(this.systemHierarchyDefinitionService.getSystemHierarchyList(searchForm)));

		searchForm = new SystemHierarchySearchForm();
		searchForm.setCategoryId(hierarchy.getCategory().getId());
		searchForm.setParentId(Short.parseShort("101"));
		searchForm.setOrderBy("name");
		Assertions.assertEquals(3, CollectionUtils.getSize(this.systemHierarchyDefinitionService.getSystemHierarchyList(searchForm)));

		searchForm = new SystemHierarchySearchForm();
		searchForm.setCategoryId(hierarchy.getCategory().getId());
		searchForm.setParentId(childHierarchy.getId());
		searchForm.setOrderBy("name");
		Assertions.assertTrue(CollectionUtils.isEmpty(this.systemHierarchyDefinitionService.getSystemHierarchyList(searchForm)));
	}


	@Test
	public void testSaveSystemHierarchyProduct() {
		Assertions.assertThrows(ValidationException.class, () -> {
			// Add a new Product Interest - Valid
			SystemHierarchy hierarchy = new SystemHierarchy();
			hierarchy.setName("Transitions");
			hierarchy.setCategory(this.systemHierarchyDefinitionService.getSystemHierarchyCategory(MathUtils.SHORT_THREE));
			this.systemHierarchyDefinitionService.saveSystemHierarchy(hierarchy);

			hierarchy = this.systemHierarchyDefinitionService.getSystemHierarchy(hierarchy.getId());
			Assertions.assertEquals("Transitions", hierarchy.getLabel());

			// Try to add a child (Should fail since max depth is 1)
			SystemHierarchy childHierarchy = new SystemHierarchy();
			childHierarchy.setParent(hierarchy);
			childHierarchy.setName("Test Failure");
			childHierarchy.setCategory(hierarchy.getCategory());
			this.systemHierarchyDefinitionService.saveSystemHierarchy(childHierarchy);
		});
	}


	@Test
	public void testDeleteSystemHierarchy() {
		// Make sure hierarchies exist
		Assertions.assertNotNull(this.systemHierarchyDefinitionService.getSystemHierarchy(Short.parseShort("201")));
		Assertions.assertNotNull(this.systemHierarchyDefinitionService.getSystemHierarchy(Short.parseShort("202")));

		// First try and fail to delete the hierarchy that we are using
		deleteSystemHierarchy(Short.parseShort("201"), true);

		// Delete the entity that is using the hierarchy.
		this.systemHierarchyTestEntityDAO.delete(2);

		// Deleting the entity should have deleted corresponding SystemHierarchyLink entry
		deleteSystemHierarchy(Short.parseShort("201"), false);

		// Make sure hierarchy & child hierarchy were deleted
		Assertions.assertNull(this.systemHierarchyDefinitionService.getSystemHierarchy(Short.parseShort("201")));
		Assertions.assertNull(this.systemHierarchyDefinitionService.getSystemHierarchy(Short.parseShort("202")));
	}


	private void deleteSystemHierarchy(short id, boolean exception) {
		try {
			this.systemHierarchyDefinitionService.deleteSystemHierarchy(id);
		}
		catch (Exception e) {
			if (exception) {
				// Exception Expected.  Continue on;
				return;
			}
			Assertions.fail("Did NOT expect exception while deleting hierarchy [" + id + "]");
		}
		if (exception) {
			Assertions.fail("Expected exception while deleting hierarchy [" + id + "]");
		}
	}


	@Test
	public void testInsertSystemHierarchyCategorySystemDefined() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemHierarchyCategory category = new SystemHierarchyCategory();
			category.setSystemDefined(true);
			category.setName("Test Category");
			category.setMaxDepth(1);
			this.systemHierarchyDefinitionService.saveSystemHierarchyCategory(category);
		});
	}


	@Test
	public void testInsertUpdateDeleteSystemHierarchyCategory() {
		SystemHierarchyCategory category = new SystemHierarchyCategory();
		category.setName("Test Category");
		category.setMaxDepth(1);
		// Insert
		this.systemHierarchyDefinitionService.saveSystemHierarchyCategory(category);

		// Update
		category.setName(category.getName() + "-Updated");
		this.systemHierarchyDefinitionService.saveSystemHierarchyCategory(category);

		// Delete
		this.systemHierarchyDefinitionService.deleteSystemHierarchyCategory(category.getId());
	}


	@Test
	public void testChangeSystemHierarchyCategorySystemDefinedFlag() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemHierarchyCategory category = this.systemHierarchyDefinitionService.getSystemHierarchyCategory(MathUtils.SHORT_ONE);
			category.setSystemDefined(false);
			this.systemHierarchyDefinitionService.saveSystemHierarchyCategory(category);
		});
	}


	@Test
	public void testChangeSystemHierarchyCategorySystemDefinedName() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemHierarchyCategory category = this.systemHierarchyDefinitionService.getSystemHierarchyCategory(MathUtils.SHORT_ONE);
			category.setName(category.getName() + "-Updated");
			this.systemHierarchyDefinitionService.saveSystemHierarchyCategory(category);
		});
	}


	@Test
	public void testDeleteSystemHierarchyCategorySystemDefined() {
		Assertions.assertThrows(Exception.class, () -> {
			this.systemHierarchyDefinitionService.deleteSystemHierarchyCategory(MathUtils.SHORT_ONE);
		});
	}
}
