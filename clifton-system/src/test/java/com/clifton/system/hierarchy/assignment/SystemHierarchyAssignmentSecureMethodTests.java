package com.clifton.system.hierarchy.assignment;

import com.clifton.core.context.ContextHandler;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecureMethodSecurityEvaluator;
import com.clifton.core.security.authorization.SecureMethodSecurityResourceResolver;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.security.SecurityTestsUtils;
import com.clifton.security.user.SecurityUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


/**
 * Unit tests for {@link SystemHierarchyAssignmentSecureMethodResolver} and {@link SystemHierarchyAssignmentSecureMethodEvaluator}
 *
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemHierarchyAssignmentSecureMethodTests {

	@Resource
	private SecureMethodSecurityResourceResolver systemHierarchyAssignmentSecureMethodResolver;

	@Resource
	private SecureMethodSecurityEvaluator systemHierarchyAssignmentSecureMethodEvaluator;

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setCurrentUser(String userName) {
		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, userName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testResolverSuccessSecurityOverride() {
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "getSystemHierarchyItemList", String.class, Short.class);
		Map<String, String> config = new HashMap<>();
		config.put("hierarchyId", "1");
		config.put("tableName", "Table401");
		Assertions.assertEquals("Resource 2", this.systemHierarchyAssignmentSecureMethodResolver.getSecurityResourceName(method, config));
	}


	@Test
	public void testResolverSuccessNoOverride() {
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "getSystemHierarchyItemList", String.class, Short.class);
		Map<String, String> config = new HashMap<>();
		config.put("hierarchyId", "2");
		config.put("tableName", "Table401");
		Assertions.assertEquals("Resource 2", this.systemHierarchyAssignmentSecureMethodResolver.getSecurityResourceName(method, config));
	}


	@Test
	public void testResolverTableNameNull() {
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "saveSystemHierarchyLink", String.class, long.class, short.class);
		Map<String, String> config = new HashMap<>();
		config.put("hierarchyId", "1");
		Assertions.assertThrows(RuntimeException.class, () -> this.systemHierarchyAssignmentSecureMethodResolver.getSecurityResourceName(method, config));
	}


	@Test
	public void testResolverHierarchyIdNull() {
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "saveSystemHierarchyLink", String.class, long.class, short.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "Table401");
		Assertions.assertThrows(RuntimeException.class, () -> this.systemHierarchyAssignmentSecureMethodResolver.getSecurityResourceName(method, config));
	}


	@Test
	public void testResolverHierarchyNull() {
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "saveSystemHierarchyLink", String.class, long.class, short.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "Table401");
		config.put("hierarchyId", "10");
		Assertions.assertThrows(RuntimeException.class, () -> this.systemHierarchyAssignmentSecureMethodResolver.getSecurityResourceName(method, config));
	}


	@Test
	public void testResolverAssignmentNull() {
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "saveSystemHierarchyLink", String.class, long.class, short.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "DummyTable");
		config.put("hierarchyId", "1");
		Assertions.assertThrows(RuntimeException.class, () -> this.systemHierarchyAssignmentSecureMethodResolver.getSecurityResourceName(method, config));
	}


	@Test
	public void testEvaluatorSuccess() {
		setCurrentUser("adminUser");
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "saveSystemHierarchyLinkList", String.class, long.class, String.class, String.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "Table401");
		config.put("fkFieldId", "1");
		config.put("hierarchyIdList", "1,,null");
		config.put("hierarchyCategoryName", "CategoryName");

		Assertions.assertDoesNotThrow(() -> this.systemHierarchyAssignmentSecureMethodEvaluator.checkPermissions(null, method, config));
	}


	@Test
	public void testEvaluatorSuccessNewHierarchyIdNull() {
		setCurrentUser("adminUser");
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "moveSystemHierarchyLink", String.class, long.class, Short.class, Short.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "Table401");
		config.put("oldHierarchyId", "1");

		Assertions.assertDoesNotThrow(() -> this.systemHierarchyAssignmentSecureMethodEvaluator.checkPermissions(null, method, config));
	}


	@Test
	public void testEvaluatorSuccessOldHierarchyIdNull() {
		setCurrentUser("adminUser");
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "moveSystemHierarchyLink", String.class, long.class, Short.class, Short.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "Table401");
		config.put("newHierarchyId", "1");

		Assertions.assertDoesNotThrow(() -> this.systemHierarchyAssignmentSecureMethodEvaluator.checkPermissions(null, method, config));
	}


	@Test
	public void testEvaluatorAccessDenied() {
		setCurrentUser("regularUser");
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "deleteSystemHierarchyLinkList", String.class, long.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "Table401");
		config.put("fkFieldId", "1");

		Assertions.assertThrows(AccessDeniedException.class, () -> this.systemHierarchyAssignmentSecureMethodEvaluator.checkPermissions(null, method, config));
	}


	@Test
	public void testEvaluatorTableNameNull() {
		setCurrentUser("adminUser");
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "saveSystemHierarchyLinkList", String.class, long.class, String.class, String.class);
		Map<String, String> config = new HashMap<>();
		config.put("fkFieldId", "1");
		config.put("hierarchyIdList", "1");
		config.put("hierarchyCategoryName", "CategoryName");

		Assertions.assertThrows(RuntimeException.class, () -> this.systemHierarchyAssignmentSecureMethodEvaluator.checkPermissions(null, method, config));
	}


	@Test
	public void testEvaluatorNewAndOldHierarchyIdNull() {
		setCurrentUser("adminUser");
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "moveSystemHierarchyLink", String.class, long.class, Short.class, Short.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "Table401");

		Assertions.assertThrows(RuntimeException.class, () -> this.systemHierarchyAssignmentSecureMethodEvaluator.checkPermissions(null, method, config));
	}


	@Test
	public void testEvaluatorFkFieldIdNull() {
		setCurrentUser("adminUser");
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "deleteSystemHierarchyLinkList", String.class, long.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "Table401");

		Assertions.assertThrows(RuntimeException.class, () -> this.systemHierarchyAssignmentSecureMethodEvaluator.checkPermissions(null, method, config));
	}


	@Test
	public void testEvaluatorHierarchyNull() {
		setCurrentUser("adminUser");
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "saveSystemHierarchyLinkList", String.class, long.class, String.class, String.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "Table401");
		config.put("fkFieldId", "1");
		config.put("hierarchyIdList", "100");
		config.put("hierarchyCategoryName", "CategoryName");

		Assertions.assertThrows(RuntimeException.class, () -> this.systemHierarchyAssignmentSecureMethodEvaluator.checkPermissions(null, method, config));
	}


	@Test
	public void testEvaluatorAssignmentNull() {
		setCurrentUser("adminUser");
		Method method = MethodUtils.getMethod(SystemHierarchyAssignmentServiceImpl.class, "saveSystemHierarchyLinkList", String.class, long.class, String.class, String.class);
		Map<String, String> config = new HashMap<>();
		config.put("tableName", "DummyTable");
		config.put("fkFieldId", "1");
		config.put("hierarchyIdList", "1");
		config.put("hierarchyCategoryName", "CategoryName");

		Assertions.assertThrows(RuntimeException.class, () -> this.systemHierarchyAssignmentSecureMethodEvaluator.checkPermissions(null, method, config));
	}
}
