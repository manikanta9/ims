package com.clifton.system.hierarchy.assignment;

import com.clifton.core.util.CollectionUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionServiceImpl;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>SystemHierarchyAssignmentServiceImplTests</code> class tests {@link SystemHierarchyDefinitionServiceImpl} methods.
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemHierarchyAssignmentServiceImplTests {

	@Resource
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	@Resource
	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	private static final String TEST_ENTITY_TABLE_NAME = "SystemHierarchyTestEntity";


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
	}


	@Test
	public void testGetSystemHierarchyAssignmentList() {

		List<SystemHierarchyAssignment> list = this.systemHierarchyAssignmentService.getSystemHierarchyAssignmentListByCategory(MathUtils.SHORT_ONE);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		list = this.systemHierarchyAssignmentService.getSystemHierarchyAssignmentListByCategory(MathUtils.SHORT_TWO);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		list = this.systemHierarchyAssignmentService.getSystemHierarchyAssignmentListByCategory(MathUtils.SHORT_THREE);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		list = this.systemHierarchyAssignmentService.getSystemHierarchyAssignmentListByTable(MathUtils.SHORT_ONE);
		Assertions.assertEquals(3, CollectionUtils.getSize(list));
	}


	@Test
	public void testInsertUpdateDeleteSystemHierarchyAssignment() {
		SystemHierarchyCategory category = new SystemHierarchyCategory();
		category.setName("Test Category");
		this.systemHierarchyDefinitionService.saveSystemHierarchyCategory(category);

		SystemHierarchyAssignment assign = new SystemHierarchyAssignment();
		assign.setCategory(category);
		assign.setTable(this.systemSchemaService.getSystemTableByName(TEST_ENTITY_TABLE_NAME));
		assign.setMultipleValuesAllowed(true);

		// Insert
		this.systemHierarchyAssignmentService.saveSystemHierarchyAssignment(assign);

		// Update
		assign.setMultipleValuesAllowed(false);
		this.systemHierarchyAssignmentService.saveSystemHierarchyAssignment(assign);

		// Delete
		this.systemHierarchyAssignmentService.deleteSystemHierarchyAssignment(assign.getId());
	}


	@Test
	public void testInsertSystemHierarchyAssignmentSystemDefined() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemHierarchyAssignment assign = new SystemHierarchyAssignment();
			assign.setCategory(this.systemHierarchyDefinitionService.getSystemHierarchyCategory(MathUtils.SHORT_ONE));
			assign.setTable(this.systemSchemaService.getSystemTableByName(TEST_ENTITY_TABLE_NAME));
			this.systemHierarchyAssignmentService.saveSystemHierarchyAssignment(assign);
		});
	}


	@Test
	public void testChangeSystemHierarchyAssignmentSystemDefined() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemHierarchyAssignment assign = this.systemHierarchyAssignmentService.getSystemHierarchyAssignment(MathUtils.SHORT_ONE);
			assign.setMultipleValuesAllowed(true);
			this.systemHierarchyAssignmentService.saveSystemHierarchyAssignment(assign);
		});
	}


	@Test
	public void testDeleteSystemHierarchyAssignmentSystemDefined() {
		Assertions.assertThrows(Exception.class, () -> {
			this.systemHierarchyAssignmentService.deleteSystemHierarchyAssignment(MathUtils.SHORT_ONE);
		});
	}


	@Test
	public void testUpdateSystemHierarchyAssignmentWithLinks() {
		// Create Links
		this.systemHierarchyAssignmentService.saveSystemHierarchyLink(TEST_ENTITY_TABLE_NAME, 1, Short.parseShort("302"));
		this.systemHierarchyAssignmentService.saveSystemHierarchyLink(TEST_ENTITY_TABLE_NAME, 1, Short.parseShort("301"));

		SystemHierarchyAssignment assign = this.systemHierarchyAssignmentService.getSystemHierarchyAssignment(MathUtils.SHORT_TWO);
		assign.setCategory(this.systemHierarchyDefinitionService.getSystemHierarchyCategory(MathUtils.SHORT_TWO));
		Assertions.assertFalse(saveSystemHierarchyAssignment(assign, false), "Save expected to fail because trying to change category when links already exist.");

		assign = this.systemHierarchyAssignmentService.getSystemHierarchyAssignment(MathUtils.SHORT_TWO);
		assign.setMultipleValuesAllowed(false);
		Assertions.assertFalse(saveSystemHierarchyAssignment(assign, false), "Save expected to fail because there already exists multiple links for the same entity");

		assign = this.systemHierarchyAssignmentService.getSystemHierarchyAssignment(MathUtils.SHORT_TWO);
		assign.setSingleTableAssignment(true);
		Assertions.assertFalse(saveSystemHierarchyAssignment(assign, false), "Save expected to fail because there already exists other assignments for this table");

		assign = this.systemHierarchyAssignmentService.getSystemHierarchyAssignment(MathUtils.SHORT_TWO);
		assign.setTable(new SystemTable());
		Assertions.assertFalse(saveSystemHierarchyAssignment(assign, false), "Save expected to fail because you can't change the table");

		Assertions.assertFalse(saveSystemHierarchyAssignment(assign, true), "Delete expected to fail because there are existing links");

		// Delete the links we created so other tests aren't affected
		this.systemHierarchyAssignmentService.deleteSystemHierarchyLinkList(TEST_ENTITY_TABLE_NAME, 1);
	}


	/**
	 * Tries to save/delete the assignment bean & returns true if save was successful
	 * or false if an exception was encounter.
	 */
	private boolean saveSystemHierarchyAssignment(SystemHierarchyAssignment bean, boolean delete) {
		try {
			if (delete) {
				this.systemHierarchyAssignmentService.deleteSystemHierarchyAssignment(bean.getId());
			}
			else {
				this.systemHierarchyAssignmentService.saveSystemHierarchyAssignment(bean);
			}
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}


	@Test
	public void testGetSystemHierarchyLinkList() {
		// Test Entity 1 has no existing links
		List<SystemHierarchyLink> list = this.systemHierarchyAssignmentService.getSystemHierarchyLinkList(TEST_ENTITY_TABLE_NAME, 1, null);
		Assertions.assertTrue(CollectionUtils.isEmpty(list));

		// Test Entity 2 has 1 existing link
		list = this.systemHierarchyAssignmentService.getSystemHierarchyLinkList(TEST_ENTITY_TABLE_NAME, 2, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));
		SystemHierarchyLink hierarchyLink = list.get(0);
		Assertions.assertEquals("Business Unit: Sales", hierarchyLink.getHierarchy().getNameExpandedWithLabel());
	}


	@Test
	public void testLinkSystemHierarchy() {
		// Link test entity 1 to a location
		this.systemHierarchyAssignmentService.saveSystemHierarchyLink(TEST_ENTITY_TABLE_NAME, 1, Short.parseShort("104"));
		List<SystemHierarchyLink> list = this.systemHierarchyAssignmentService.getSystemHierarchyLinkList(TEST_ENTITY_TABLE_NAME, 1, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));
		SystemHierarchyLink hierarchyLink = list.get(0);
		Assertions.assertEquals("Location: USA / MN / Minneapolis Area", hierarchyLink.getHierarchy().getNameExpandedWithLabel());

		// Try to link test entity 1 to another location (should fail)
		try {
			this.systemHierarchyAssignmentService.saveSystemHierarchyLink(TEST_ENTITY_TABLE_NAME, 1, Short.parseShort("101"));
		}
		catch (Exception e) {
			// If it was expected Exception, continue on with test:
			// Link test entity 1 to a product interest
			this.systemHierarchyAssignmentService.saveSystemHierarchyLink(TEST_ENTITY_TABLE_NAME, 1, Short.parseShort("301"));
			list = this.systemHierarchyAssignmentService.getSystemHierarchyLinkList(TEST_ENTITY_TABLE_NAME, 1, null);
			Assertions.assertEquals(2, CollectionUtils.getSize(list));

			// Add another Product Interest Tag Value
			this.systemHierarchyAssignmentService.saveSystemHierarchyLink(TEST_ENTITY_TABLE_NAME, 1, Short.parseShort("302"));
			list = this.systemHierarchyAssignmentService.getSystemHierarchyLinkList(TEST_ENTITY_TABLE_NAME, 1, null);
			Assertions.assertEquals(3, CollectionUtils.getSize(list));
			return;
		}
		Assertions.fail("Expected exception when trying to link test entity to multiple locations.");
	}
}
