package com.clifton.system.audit.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.audit.SystemAuditLifeCycleRetriever;
import com.clifton.system.audit.SystemAuditService;
import com.clifton.system.audit.auditor.AuditTestEntity;
import com.clifton.system.lifecycle.retriever.BaseSystemLifeCycleRetrieverTests;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleEventRetriever;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleTestExecutor;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemAuditLifeCycleRetrieverTests extends BaseSystemLifeCycleRetrieverTests {

	@Resource
	private SystemAuditLifeCycleRetriever systemAuditLifeCycleRetriever;

	@Resource
	private SystemAuditService systemAuditService;

	@Resource
	private ReadOnlyDAO<AuditTestEntity> auditTestEntityDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected SystemLifeCycleEventRetriever getSystemLifeCycleRetriever() {
		return this.systemAuditLifeCycleRetriever;
	}


	@Override
	protected List<IdentityObject> getApplyToEntityFalseList() {
		// Applies to ALL identity objects
		return null;
	}


	@Override
	protected List<IdentityObject> getApplyToEntityTrueList() {
		return CollectionUtils.createList(new AuditTestEntity(), new SystemTable(), this.auditTestEntityDAO.findByPrimaryKey(1));
	}


	@Test
	@Override
	public void testGetSystemLifeCycleEventListForEntity_NoResults() {
		Mockito.when(this.systemAuditService.getSystemAuditEventList(ArgumentMatchers.any())).thenReturn(null);

		// Still includes create and last update even if no audit trail
		SystemLifeCycleTestExecutor.forTableAndEntityAndRetriever("AuditTestEntity", this.auditTestEntityDAO.findByPrimaryKey(1), getSystemLifeCycleRetriever())
				.withExpectedResults(
						"Source: AuditTestEntity [1]\tLink: AuditTestEntity [1]\tEvent Source: Entity Insert\tAction: Insert\tDate: 2021-05-05 04:15:00\tUser: Test User 1\tDetails: {id=1,label=null}",
						"Source: AuditTestEntity [1]\tLink: AuditTestEntity [1]\tEvent Source: Entity Last Update\tAction: Last Update\tDate: 2021-07-05 13:15:00\tUser: Test User 1\tDetails: {id=1,label=null}"
				)
				.execute();
	}


	@Test
	@Override
	public void testGetSystemLifeCycleEventListForEntity_Results() {
		Mockito.when(this.systemAuditService.getSystemAuditEventList(ArgumentMatchers.any())).thenReturn(getTestAuditList());
		SystemLifeCycleTestExecutor.forTableAndEntityAndRetriever("AuditTestEntity", this.auditTestEntityDAO.findByPrimaryKey(1), getSystemLifeCycleRetriever())
				.withExpectedResults(
						"Source: AuditTestEntity [1]\tLink: AuditTestEntity [1]\tEvent Source: Entity Insert\tAction: Insert\tDate: 2021-05-05 04:15:00\tUser: Test User 1\tDetails: {id=1,label=null}",
						"Source: AuditTestEntity [1]\tLink: AuditTestEntity [1]\tEvent Source: Entity Last Update\tAction: Last Update\tDate: 2021-07-05 13:15:00\tUser: Test User 1\tDetails: {id=1,label=null}",
						"Source: SystemAuditEvent [null]\tLink: AuditTestEntity [1]\tEvent Source: Audit Trail\tAction: Update\tDate: null\tUser: Unknown\tDetails: [null] updated value to [Updated Description]",
						"Source: SystemAuditEvent [null]\tLink: AuditTestEntity [1]\tEvent Source: Audit Trail\tAction: Update\tDate: null\tUser: Unknown\tDetails: [null] deleted value [Updated Description]"
				)
				.execute();
	}


	private DataTable getTestAuditList() {
		DataColumn[] columnList = new DataColumn[11];
		columnList[0] = new DataColumnImpl("ColumnID", 2);
		columnList[1] = new DataColumnImpl("MainPKFieldID", 2);
		columnList[2] = new DataColumnImpl("ParentPKFieldID", 2);
		columnList[3] = new DataColumnImpl("AuditDate", 5);
		columnList[4] = new DataColumnImpl("SecurityUserName", 1);
		columnList[5] = new DataColumnImpl("Action", 1);
		columnList[6] = new DataColumnImpl("Table", 1);
		columnList[7] = new DataColumnImpl("Field", 1);
		columnList[8] = new DataColumnImpl("Label", 1);
		columnList[9] = new DataColumnImpl("Old Value", 1);
		columnList[10] = new DataColumnImpl("New Value", 1);
		DataTable dataTable = new PagingDataTableImpl(columnList, 0, 0);

		dataTable.addRow(populateDataRow(dataTable, 103, DateUtils.toDate("2021-06-01 09:00:15", DateUtils.DATE_FORMAT_FULL), "update", "", "Updated Description"));
		dataTable.addRow(populateDataRow(dataTable, 103, DateUtils.toDate("2021-06-03 15:15:00", DateUtils.DATE_FORMAT_FULL), "update", "Updated Description", null));
		return dataTable;
	}


	private DataRow populateDataRow(DataTable dataTable, int columnId, Date auditDate, String action, String oldValue, String newValue) {
		return new DataRowImpl(dataTable, new Object[]{columnId, 1, null, auditDate, "Test User 1", action, "AuditTestEntity", null, null, oldValue, newValue});
	}
}
