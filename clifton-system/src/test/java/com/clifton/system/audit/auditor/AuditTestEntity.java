package com.clifton.system.audit.auditor;


import com.clifton.core.beans.NamedEntity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>TestAuditEntity</code> ...
 *
 * @author manderson
 */
public class AuditTestEntity extends NamedEntity<Integer> {

	private Date testDate;
	private Integer testInteger;
	private BigDecimal testDecimal;
	private BigDecimal testPercent;
	private String testString;
	private boolean testBoolean;


	public Date getTestDate() {
		return this.testDate;
	}


	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}


	public Integer getTestInteger() {
		return this.testInteger;
	}


	public void setTestInteger(Integer testInteger) {
		this.testInteger = testInteger;
	}


	public BigDecimal getTestDecimal() {
		return this.testDecimal;
	}


	public void setTestDecimal(BigDecimal testDecimal) {
		this.testDecimal = testDecimal;
	}


	public String getTestString() {
		return this.testString;
	}


	public void setTestString(String testString) {
		this.testString = testString;
	}


	public boolean isTestBoolean() {
		return this.testBoolean;
	}


	public void setTestBoolean(boolean testBoolean) {
		this.testBoolean = testBoolean;
	}


	public BigDecimal getTestPercent() {
		return this.testPercent;
	}


	public void setTestPercent(BigDecimal testPercent) {
		this.testPercent = testPercent;
	}
}
