package com.clifton.system.audit.auditor;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.User;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.audit.SystemAuditEvent;
import com.clifton.system.audit.SystemAuditService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class SystemAuditMetaDataHandlerImplTests {

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private SystemAuditMetaDataHandler systemAuditMetaDataHandler;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private SystemColumnService systemColumnService;

	@Resource
	private SystemAuditService systemAuditService;

	@Resource
	private UpdatableDAO<AuditTestEntity> auditTestEntityDAO;

	@Resource
	private ReadOnlyDAO<SystemAuditEvent> systemAuditEventDAO;

	///////////////////////////////////////////

	private static final String AUDIT_TEST_ENTITY_TABLE = "AuditTestEntity";
	private static final short NO_AUDIT = 1;
	private static final short AUDIT_UPDATE_ONLY = 2;
	private static final short AUDIT_UPDATE_AND_DELETE = 3;

	private static final short AUDIT_INSERT_AND_UPDATE = 4;
	private static final short AUDIT_DELETE_ONLY = 5;
	private static final short AUDIT_UPDATE_EXCEPT_FROM_NULL_AND_DELETE = 6;
	private static final short AUDIT_UPDATE_EXCEPT_FROM_ZERO_AND_DELETE = 7;

	/////////////////////////////////////////////


	@BeforeEach
	public void setCurrentUser() {
		// need current user for auditing
		User user = new User();
		user.setId(new Integer(7).shortValue());
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}

	/////////////////////////////////////////////

	/////////////////////////////////////////////


	@Test
	public void testGetInsertFieldInfo() {
		setAuditForTestTable(null);
		List<AuditFieldMetaData> list = this.systemAuditMetaDataHandler.getInsertFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(0, CollectionUtils.getSize(list));

		setAuditForTestTable(NO_AUDIT);
		list = this.systemAuditMetaDataHandler.getInsertFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(0, CollectionUtils.getSize(list));

		setAuditForTestTable(AUDIT_UPDATE_ONLY);
		list = this.systemAuditMetaDataHandler.getInsertFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(0, CollectionUtils.getSize(list));

		setAuditForTestTable(AUDIT_INSERT_AND_UPDATE);
		list = this.systemAuditMetaDataHandler.getInsertFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(8, CollectionUtils.getSize(list));
	}


	@Test
	public void testGetUpdateFieldInfo() {
		setAuditForTestTable(null);
		List<AuditFieldMetaData> list = this.systemAuditMetaDataHandler.getUpdateFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(0, CollectionUtils.getSize(list));

		setAuditForTestTable(NO_AUDIT);
		list = this.systemAuditMetaDataHandler.getUpdateFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(0, CollectionUtils.getSize(list));

		setAuditForTestTable(AUDIT_UPDATE_ONLY);
		list = this.systemAuditMetaDataHandler.getUpdateFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(8, CollectionUtils.getSize(list));

		setAuditForTestTable(AUDIT_DELETE_ONLY);
		list = this.systemAuditMetaDataHandler.getUpdateFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(0, CollectionUtils.getSize(list));

		setAuditForTestTable(AUDIT_INSERT_AND_UPDATE);
		list = this.systemAuditMetaDataHandler.getUpdateFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(8, CollectionUtils.getSize(list));
	}


	@Test
	public void testGetDeleteFieldInfo() {
		setAuditForTestTable(null);
		List<AuditFieldMetaData> list = this.systemAuditMetaDataHandler.getDeleteFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(0, CollectionUtils.getSize(list));

		setAuditForTestTable(NO_AUDIT);
		list = this.systemAuditMetaDataHandler.getDeleteFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(0, CollectionUtils.getSize(list));

		setAuditForTestTable(AUDIT_UPDATE_ONLY);
		list = this.systemAuditMetaDataHandler.getDeleteFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(0, CollectionUtils.getSize(list));

		setAuditForTestTable(AUDIT_DELETE_ONLY);
		list = this.systemAuditMetaDataHandler.getDeleteFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(8, CollectionUtils.getSize(list));

		setAuditForTestTable(AUDIT_UPDATE_EXCEPT_FROM_NULL_AND_DELETE);
		list = this.systemAuditMetaDataHandler.getDeleteFieldInfo(AUDIT_TEST_ENTITY_TABLE);
		Assertions.assertEquals(8, CollectionUtils.getSize(list));
	}


	@Test
	public void testAuditOnInsert() {
		setAuditForTestTable(AUDIT_INSERT_AND_UPDATE);

		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");
		bean.setTestDate(DateUtils.toDate("01/01/2013"));
		bean.setTestDecimal(BigDecimal.valueOf(5.0));
		bean.setTestString("Five");
		this.auditTestEntityDAO.save(bean);

		List<SystemAuditEvent> eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_INSERT);
		Assertions.assertEquals(7, CollectionUtils.getSize(eventList));

		// Test a few values
		SystemAuditEvent event = getEventFromList(eventList, "TestDescription");
		Assertions.assertNull(event.getOldValue());
		Assertions.assertEquals(bean.getDescription(), event.getNewValue());

		event = getEventFromList(eventList, "TestDate");
		Assertions.assertNull(event.getOldValue());
		Assertions.assertEquals("2013-01-01", event.getNewValue());

		// Integer value wasn't inserted
		event = getEventFromList(eventList, "TestInteger");
		Assertions.assertNull(event);

		// Turn Off Audit On Insert for Test Description
		setAuditForTestColumn("TestDescription", AUDIT_UPDATE_ONLY);

		AuditTestEntity bean2 = new AuditTestEntity();
		bean2.setName("Test Entity 2");
		bean2.setDescription("My description 2");
		bean2.setTestDate(DateUtils.toDate("01/02/2013"));
		bean2.setTestDecimal(BigDecimal.valueOf(7.0));
		this.auditTestEntityDAO.save(bean2);

		eventList = getEventListForBean(bean2.getId(), SystemAuditEvent.AUDIT_INSERT);
		Assertions.assertEquals(5, CollectionUtils.getSize(eventList));

		// Test a few values

		// Description Should have been skipped
		event = getEventFromList(eventList, "TestDescription");
		Assertions.assertNull(event);

		event = getEventFromList(eventList, "TestName");
		Assertions.assertNull(event.getOldValue());
		Assertions.assertEquals(bean2.getName(), event.getNewValue());
	}


	@Test
	public void testDoNotAuditOnInsert() {
		// Turn Off Audit On Insert
		setAuditForTestTable(AUDIT_UPDATE_AND_DELETE);

		AuditTestEntity bean2 = new AuditTestEntity();
		bean2.setName("Test Entity 2");
		bean2.setDescription("My description 2");
		bean2.setTestDate(DateUtils.toDate("01/02/2013"));
		bean2.setTestDecimal(BigDecimal.valueOf(7.0));
		this.auditTestEntityDAO.save(bean2);

		List<SystemAuditEvent> eventList = getEventListForBean(bean2.getId(), SystemAuditEvent.AUDIT_INSERT);
		Assertions.assertEquals(0, CollectionUtils.getSize(eventList));
	}


	@Test
	public void testAuditOnUpdate() {
		setAuditForTestTable(AUDIT_INSERT_AND_UPDATE);

		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");
		bean.setTestDate(DateUtils.toDate("01/01/2013"));
		bean.setTestDecimal(BigDecimal.valueOf(7.0));
		bean.setTestString("Seven");

		this.auditTestEntityDAO.save(bean);

		bean.setDescription("My description-Updated");
		bean.setTestDecimal(BigDecimal.valueOf(7)); // Should not count as an update because same number - just no decimal set on it
		bean.setTestString("Seven-7");
		this.auditTestEntityDAO.save(bean);

		List<SystemAuditEvent> eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(2, CollectionUtils.getSize(eventList));

		// Test a few values
		SystemAuditEvent event = getEventFromList(eventList, "TestDescription");
		Assertions.assertEquals("My description", event.getOldValue());
		Assertions.assertEquals(bean.getDescription(), event.getNewValue());

		event = getEventFromList(eventList, "TestDate");
		Assertions.assertNull(event);

		event = getEventFromList(eventList, "TestDecimal");
		Assertions.assertNull(event);

		event = getEventFromList(eventList, "TestString");
		Assertions.assertEquals("Seven", event.getOldValue());
		Assertions.assertEquals(bean.getTestString(), event.getNewValue());

		// Turn Off Audit On Update for Test Description & Test Integer
		setAuditForTestColumn("TestDescription", NO_AUDIT);
		setAuditForTestColumn("TestInteger", AUDIT_DELETE_ONLY);

		AuditTestEntity bean2 = new AuditTestEntity();
		bean2.setName("Test Entity 2");
		bean2.setDescription("My description 2");
		bean2.setTestDate(DateUtils.toDate("01/02/2013"));
		bean2.setTestDecimal(BigDecimal.valueOf(2.2));
		bean2.setTestString("Two");

		this.auditTestEntityDAO.save(bean2);

		bean2.setTestInteger(6);
		bean2.setTestDecimal(BigDecimal.valueOf(2.2003));
		bean2.setDescription("My Description - Shouldn't be audited");
		bean2.setTestString("Two-Two");
		this.auditTestEntityDAO.save(bean2);

		eventList = getEventListForBean(bean2.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(2, CollectionUtils.getSize(eventList));

		// Test a few values

		// Description Should have been skipped
		event = getEventFromList(eventList, "TestDescription");
		Assertions.assertNull(event);

		// Integer Should have been skipped
		event = getEventFromList(eventList, "TestInteger");
		Assertions.assertNull(event);

		event = getEventFromList(eventList, "TestString");
		Assertions.assertEquals("Two", event.getOldValue());
		Assertions.assertEquals(bean2.getTestString(), event.getNewValue());

		event = getEventFromList(eventList, "TestDecimal");
		Assertions.assertEquals("2.2", event.getOldValue());
		Assertions.assertEquals("2.2003", event.getNewValue());
	}


	@Test
	public void testAuditOnUpdate_ExceptFromNull() {
		setAuditForTestTable(AUDIT_UPDATE_EXCEPT_FROM_NULL_AND_DELETE);

		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");

		this.auditTestEntityDAO.save(bean);

		bean.setDescription("My description-Updated");
		bean.setTestDecimal(BigDecimal.valueOf(7)); // Should not count as an update because same number - just no decimal set on it
		bean.setTestString("Seven");
		this.auditTestEntityDAO.save(bean);

		// Description Should be the only update audit, since everything else was NULL before
		List<SystemAuditEvent> eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(1, CollectionUtils.getSize(eventList));

		// Test the one value
		SystemAuditEvent event = getEventFromList(eventList, "TestDescription");
		Assertions.assertEquals("My description", event.getOldValue());
		Assertions.assertEquals(bean.getDescription(), event.getNewValue());

		// Change a few values and make sure it's audited
		bean.setTestDecimal(BigDecimal.valueOf(7.52));
		bean.setTestString(null);
		bean.setTestInteger(7);

		this.auditTestEntityDAO.save(bean);

		// Should Have Description Update Event from Previous Update
		// And also Decimal & String updates
		eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(3, CollectionUtils.getSize(eventList));

		event = getEventFromList(eventList, "TestDate");
		Assertions.assertNull(event);

		event = getEventFromList(eventList, "TestDecimal");
		Assertions.assertEquals("7", event.getOldValue());
		Assertions.assertEquals("7.52", event.getNewValue());

		event = getEventFromList(eventList, "TestInteger");
		Assertions.assertNull(event);

		event = getEventFromList(eventList, "TestString");
		Assertions.assertEquals("Seven", event.getOldValue());
		Assertions.assertNull(event.getNewValue());
	}


	@Test
	public void testDoNotAuditOnUpdate() {
		setAuditForTestTable(AUDIT_DELETE_ONLY);

		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");
		bean.setTestDate(DateUtils.toDate("01/01/2013"));
		bean.setTestDecimal(BigDecimal.valueOf(7.0));
		bean.setTestString("Seven");

		this.auditTestEntityDAO.save(bean);

		bean.setDescription("My description-Updated");
		bean.setTestDecimal(BigDecimal.valueOf(7)); // Should not count as an update because same number - just no decimal set on it
		bean.setTestString("Seven-7");
		this.auditTestEntityDAO.save(bean);

		List<SystemAuditEvent> eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(0, CollectionUtils.getSize(eventList));
	}


	@Test
	public void testDoNotAuditOnUpdate_DecimalPrecision() {
		setAuditForTestTable(AUDIT_INSERT_AND_UPDATE);

		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");
		bean.setTestDate(DateUtils.toDate("01/01/2013"));
		// 15 Precision
		bean.setTestDecimal(new BigDecimal("7.012345678901234"));
		// 5 Precision
		bean.setTestPercent(new BigDecimal("95.12346"));
		bean.setTestString("Seven");

		this.auditTestEntityDAO.save(bean);

		bean.setTestDecimal(new BigDecimal("7.0123456789012341987")); // Should not count as an update because adding values beyond precision
		bean.setTestPercent(new BigDecimal("95.123456")); // Should not count as an update because adding values beyond precision (and value rounded to given precision matches)
		this.auditTestEntityDAO.save(bean);

		List<SystemAuditEvent> eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(0, CollectionUtils.getSize(eventList));
	}


	@Test
	public void testDoNotAuditOnDelete() {
		setAuditForTestTable(AUDIT_UPDATE_ONLY);

		// Create Both Beans First - XML DAO after delete will create second with same id and we don't want events from the first delete
		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");
		bean.setTestDate(DateUtils.toDate("01/01/2013"));
		bean.setTestDecimal(BigDecimal.valueOf(7.0));
		bean.setTestString("Seven");
		this.auditTestEntityDAO.save(bean);

		AuditTestEntity bean2 = new AuditTestEntity();
		bean2.setName("Test Entity 2");
		bean2.setDescription("My description 2");
		bean2.setTestDate(DateUtils.toDate("01/02/2013"));
		bean2.setTestDecimal(BigDecimal.valueOf(2.2));
		bean2.setTestString("Two");
		this.auditTestEntityDAO.save(bean2);

		Integer beanId = bean.getId();
		this.auditTestEntityDAO.delete(bean);

		List<SystemAuditEvent> eventList = getEventListForBean(beanId, SystemAuditEvent.AUDIT_DELETE);
		Assertions.assertEquals(0, CollectionUtils.getSize(eventList));

		beanId = bean2.getId();
		this.auditTestEntityDAO.delete(bean2);

		eventList = getEventListForBean(beanId, SystemAuditEvent.AUDIT_DELETE);
		Assertions.assertEquals(0, CollectionUtils.getSize(eventList));
	}


	@Test
	public void testAuditOnDelete() {
		setAuditForTestTable(AUDIT_DELETE_ONLY);

		// Create Both Beans First - XML DAO after delete will create second with same id and we don't want events from the first delete
		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");
		bean.setTestDate(DateUtils.toDate("01/01/2013"));
		bean.setTestDecimal(BigDecimal.valueOf(7.0));
		bean.setTestString("Seven");
		this.auditTestEntityDAO.save(bean);

		AuditTestEntity bean2 = new AuditTestEntity();
		bean2.setName("Test Entity 2");
		bean2.setDescription("My description 2");
		bean2.setTestDate(DateUtils.toDate("01/02/2013"));
		bean2.setTestDecimal(BigDecimal.valueOf(2.2));
		bean2.setTestString("Two");
		bean2.setTestInteger(2);
		this.auditTestEntityDAO.save(bean2);

		Integer beanId = bean.getId();
		this.auditTestEntityDAO.delete(bean);

		List<SystemAuditEvent> eventList = getEventListForBean(beanId, SystemAuditEvent.AUDIT_DELETE);
		Assertions.assertEquals(6, CollectionUtils.getSize(eventList));

		// Test a few values
		SystemAuditEvent event = getEventFromList(eventList, "TestDescription");
		Assertions.assertEquals("My description", event.getOldValue());
		Assertions.assertNull(event.getNewValue());

		event = getEventFromList(eventList, "TestDate");
		Assertions.assertNull(event.getNewValue());

		event = getEventFromList(eventList, "TestDecimal");
		Assertions.assertNull(event.getNewValue());

		event = getEventFromList(eventList, "TestString");
		Assertions.assertEquals("Seven", event.getOldValue());
		Assertions.assertNull(event.getNewValue());

		// Turn Off Audit On Delete for Test Description & Test Integer
		setAuditForTestColumn("TestDescription", NO_AUDIT);
		setAuditForTestColumn("TestInteger", AUDIT_UPDATE_ONLY);

		beanId = bean2.getId();
		this.auditTestEntityDAO.delete(bean2);

		eventList = getEventListForBean(beanId, SystemAuditEvent.AUDIT_DELETE);
		Assertions.assertEquals(5, CollectionUtils.getSize(eventList));

		// Test a few values

		// Description Should have been skipped
		event = getEventFromList(eventList, "TestDescription");
		Assertions.assertNull(event);

		// Integer Should have been skipped
		event = getEventFromList(eventList, "TestInteger");
		Assertions.assertNull(event);

		event = getEventFromList(eventList, "TestString");
		Assertions.assertEquals("Two", event.getOldValue());
		Assertions.assertNull(event.getNewValue());

		event = getEventFromList(eventList, "TestDecimal");
		Assertions.assertEquals("2.2", event.getOldValue());
		Assertions.assertNull(event.getNewValue());
	}


	@Test
	public void testAuditWithDisabledProperties() {
		setAuditForTestTable(AUDIT_INSERT_AND_UPDATE);

		// Insert entity
		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");
		bean.setTestDate(DateUtils.toDate("01/01/2013"));
		bean.setTestDecimal(BigDecimal.valueOf(7.0));
		bean.setTestString("Seven");
		executeWithAuditingDisabled(bean, new String[]{"testString", "testDecimal"}, false);

		List<SystemAuditEvent> eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_INSERT);
		Assertions.assertEquals(5, CollectionUtils.getSize(eventList));

		// Test a few fields are populated
		Assertions.assertNotNull(getEventFromList(eventList, "TestDescription"));
		Assertions.assertNotNull(getEventFromList(eventList, "TestDate"));
		// Test disabled fields are not populated
		Assertions.assertNull(getEventFromList(eventList, "TestString"));
		Assertions.assertNull(getEventFromList(eventList, "TestDecimal"));

		bean.setTestString("Updated String");
		bean.setTestDate(DateUtils.addDays(bean.getTestDate(), 5));
		executeWithAuditingDisabled(bean, new String[]{"testString", "testDecimal"}, false);

		eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(1, CollectionUtils.getSize(eventList));

		// Test ONLY test date was audited
		Assertions.assertNotNull(getEventFromList(eventList, "TestDate"));

		// Enable auditing on delete
		setAuditForTestTable(AUDIT_DELETE_ONLY);
		executeWithAuditingDisabled(bean, new String[]{"testDecimal"}, true);

		eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_DELETE);
		Assertions.assertEquals(5, CollectionUtils.getSize(eventList));

		// Test a few fields are populated
		Assertions.assertNotNull(getEventFromList(eventList, "TestDescription"));
		Assertions.assertNotNull(getEventFromList(eventList, "TestDate"));
		Assertions.assertNotNull(getEventFromList(eventList, "TestString"));
		// Test disabled field is not populated
		Assertions.assertNull(getEventFromList(eventList, "TestDecimal"));
	}


	@Test
	public void testAuditDeleteSkippedForNullOrFalseValues() {
		setAuditForTestTable(AUDIT_DELETE_ONLY);

		// Insert entity
		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		this.auditTestEntityDAO.save(bean);

		// Delete Entity
		// Should only have id and name field audited
		this.auditTestEntityDAO.delete(bean);

		List<SystemAuditEvent> eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_DELETE);
		Assertions.assertEquals(2, CollectionUtils.getSize(eventList));
		Assertions.assertNotNull(getEventFromList(eventList, "AuditTestEntityID"));
		Assertions.assertNotNull(getEventFromList(eventList, "TestName"));
		// Validate Boolean field not included
		Assertions.assertNull(getEventFromList(eventList, "TestBoolean"));
	}


	@Test
	public void testAuditDeleteIncludedForTrueValues() {
		setAuditForTestTable(AUDIT_DELETE_ONLY);

		// Insert entity #2
		AuditTestEntity bean2 = new AuditTestEntity();
		bean2.setName("Test Entity 2");
		bean2.setDescription("Test Entity 2 With Boolean = TRUE");
		bean2.setTestBoolean(true);

		this.auditTestEntityDAO.save(bean2);

		// Delete Entity
		// Should only have id, name, description, and test boolean field audited
		this.auditTestEntityDAO.delete(bean2);

		List<SystemAuditEvent> eventList = getEventListForBean(bean2.getId(), SystemAuditEvent.AUDIT_DELETE);
		Assertions.assertEquals(4, CollectionUtils.getSize(eventList));
		Assertions.assertNotNull(getEventFromList(eventList, "AuditTestEntityID"));
		Assertions.assertNotNull(getEventFromList(eventList, "TestName"));
		Assertions.assertNotNull(getEventFromList(eventList, "TestDescription"));
		Assertions.assertNotNull(getEventFromList(eventList, "TestBoolean"));
	}


	@Test
	public void testAuditWithDisabledProperties_Empty() {
		// Test null String[]
		setAuditForTestTable(AUDIT_INSERT_AND_UPDATE);

		// Insert entity
		AuditTestEntity bean2 = new AuditTestEntity();
		bean2.setName("Test Entity");
		bean2.setDescription("My description");
		bean2.setTestDate(DateUtils.toDate("01/01/2013"));
		bean2.setTestDecimal(BigDecimal.valueOf(7.0));
		bean2.setTestString("Seven");
		executeWithAuditingDisabled(bean2, null, false);

		List<SystemAuditEvent> eventList = getEventListForBean(bean2.getId(), SystemAuditEvent.AUDIT_INSERT);
		Assertions.assertEquals(7, CollectionUtils.getSize(eventList));

		AuditTestEntity bean3 = new AuditTestEntity();
		bean3.setName("Test Entity");
		bean3.setDescription("My description");
		bean3.setTestDate(DateUtils.toDate("01/01/2013"));
		bean3.setTestDecimal(BigDecimal.valueOf(7.0));
		bean3.setTestString("Seven");
		executeWithAuditingDisabled(bean3, new String[]{}, false);

		eventList = getEventListForBean(bean3.getId(), SystemAuditEvent.AUDIT_INSERT);
		Assertions.assertEquals(7, CollectionUtils.getSize(eventList));
	}


	@Test
	public void testAuditOnUpdate_PreciseBigDecimals() {
		setAuditForTestTable(AUDIT_INSERT_AND_UPDATE);

		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");
		bean.setTestDecimal(new BigDecimal("100.67701220093696"));

		this.auditTestEntityDAO.save(bean);

		bean.setDescription("My description-Updated");
		bean.setTestDecimal(new BigDecimal("100.67701220093697"));
		this.auditTestEntityDAO.save(bean);

		List<SystemAuditEvent> eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(2, CollectionUtils.getSize(eventList));

		// Test a few values
		SystemAuditEvent event = getEventFromList(eventList, "TestDescription");
		Assertions.assertEquals("My description", event.getOldValue());
		Assertions.assertEquals(bean.getDescription(), event.getNewValue());

		event = getEventFromList(eventList, "TestDecimal");
		Assertions.assertEquals("100.67701220093696", event.getOldValue());
		Assertions.assertEquals("100.67701220093697", event.getNewValue());
	}


	@Test
	public void testAuditOnUpdate_ExceptFromZero() {
		setAuditForTestTable(AUDIT_UPDATE_EXCEPT_FROM_ZERO_AND_DELETE);

		AuditTestEntity bean = new AuditTestEntity();
		bean.setName("Test Entity");
		bean.setDescription("My description");
		bean.setTestInteger(0);

		this.auditTestEntityDAO.save(bean);

		bean.setDescription("My description-Updated");
		bean.setTestInteger(1); // Should not count as an update because same number - just no decimal set on it
		this.auditTestEntityDAO.save(bean);

		// Description Should be the only update audit, since Integer was set to  0 before
		List<SystemAuditEvent> eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(1, CollectionUtils.getSize(eventList));

		// Test the one value
		SystemAuditEvent event = getEventFromList(eventList, "TestDescription");
		Assertions.assertEquals("My description", event.getOldValue());
		Assertions.assertEquals(bean.getDescription(), event.getNewValue());

		// Change a few values and make sure it's audited
		bean.setTestString(null);
		bean.setTestInteger(4);

		this.auditTestEntityDAO.save(bean);

		// Should Have Description Update Event from Previous Update
		// And also integer updates
		eventList = getEventListForBean(bean.getId(), SystemAuditEvent.AUDIT_UPDATE);
		Assertions.assertEquals(2, CollectionUtils.getSize(eventList));

		event = getEventFromList(eventList, "TestDate");
		Assertions.assertNull(event);

		event = getEventFromList(eventList, "TestInteger");
		Assertions.assertEquals("1", event.getOldValue());
		Assertions.assertEquals("4", event.getNewValue());
	}

	/////////////////////////////////////////////////
	//////////        Helper Methods       //////////
	/////////////////////////////////////////////////


	private SystemTable getTestTable() {
		return this.systemSchemaService.getSystemTableByName(AUDIT_TEST_ENTITY_TABLE);
	}


	private void setAuditForTestTable(Short auditTypeId) {
		SystemTable table = getTestTable();
		table.setAuditType(auditTypeId == null ? null : this.systemAuditService.getSystemAuditType(auditTypeId));
		this.systemSchemaService.saveSystemTable(table);
	}


	private SystemColumnStandard getTestColumn(String columnName) {
		return this.systemColumnService.getSystemColumnStandardByName(AUDIT_TEST_ENTITY_TABLE, columnName);
	}


	private void setAuditForTestColumn(String columnName, Short auditTypeId) {
		SystemColumnStandard column = getTestColumn(columnName);
		column.setAuditType(auditTypeId == null ? null : this.systemAuditService.getSystemAuditType(auditTypeId));
		this.systemColumnService.saveSystemColumnStandard(column);
	}


	private List<SystemAuditEvent> getEventListForBean(Integer beanId, int auditTypeId) {
		return this.systemAuditEventDAO.findByFields(new String[]{"mainPKFieldId", "auditTypeId"}, new Object[]{beanId.longValue(), auditTypeId});
	}


	private SystemAuditEvent getEventFromList(List<SystemAuditEvent> eventList, String columnName) {
		List<SystemAuditEvent> filteredList = BeanUtils.filter(eventList, SystemAuditEvent::getSystemColumnId, getTestColumn(columnName).getId());
		return CollectionUtils.getOnlyElement(filteredList);
	}


	private void executeWithAuditingDisabled(final AuditTestEntity bean, String[] properties, final boolean delete) {
		final UpdatableDAO<AuditTestEntity> dao = this.auditTestEntityDAO;
		SystemAuditDaoUtils.executeWithAuditingDisabled(properties, () -> {
			if (delete) {
				dao.delete(bean);
				return null;
			}
			return dao.save(bean);
		});
	}
}
