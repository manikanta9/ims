package com.clifton.system.userinterface;


import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

;


/**
 * The <code>SystemUserInterfaceAwareUtilsTests</code> ...
 *
 * @author manderson
 */
public class SystemUserInterfaceAwareUtilsTests {

	@Test
	public void testIsComboLookup() {
		BaseNamedSystemUserInterfaceAwareEntity bean = createNewBean(null);
		Assertions.assertFalse(SystemUserInterfaceAwareUtils.isComboLookup(bean));

		bean.setValueListUrl("systemColumnListFind.json");
		Assertions.assertTrue(SystemUserInterfaceAwareUtils.isComboLookup(bean));

		bean.setValueListUrl(null);
		bean.setUserInterfaceConfig("xtype: 'datefield'");
		Assertions.assertFalse(SystemUserInterfaceAwareUtils.isComboLookup(bean));

		bean.setUserInterfaceConfig("xtype: '" + SystemUserInterfaceAwareUtils.SYSTEM_LIST_COMBO_XTYPE + "'");
		Assertions.assertTrue(SystemUserInterfaceAwareUtils.isComboLookup(bean));
	}


	@Test
	public void testIsSystemListItemLookup() {
		BaseNamedSystemUserInterfaceAwareEntity bean = createNewBean(null);
		Assertions.assertFalse(SystemUserInterfaceAwareUtils.isSystemListItemLookup(bean));

		bean.setValueListUrl("systemColumnListFind.json");
		Assertions.assertFalse(SystemUserInterfaceAwareUtils.isSystemListItemLookup(bean));

		bean.setValueListUrl(null);
		bean.setUserInterfaceConfig("xtype: 'datefield'");
		Assertions.assertFalse(SystemUserInterfaceAwareUtils.isSystemListItemLookup(bean));

		bean.setUserInterfaceConfig("xtype: '" + SystemUserInterfaceAwareUtils.SYSTEM_LIST_COMBO_XTYPE + "'");
		Assertions.assertTrue(SystemUserInterfaceAwareUtils.isSystemListItemLookup(bean));

		bean = createNewBean(SystemUserInterfaceAwareUtils.SYSTEM_LIST_ITEM_TABLE_NAME);
		Assertions.assertTrue(SystemUserInterfaceAwareUtils.isSystemListItemLookup(bean));
	}


	@Test
	public void testGetSystemListName() {
		BaseNamedSystemUserInterfaceAwareEntity bean = createNewBean(null);
		Assertions.assertNull(SystemUserInterfaceAwareUtils.getSystemListName(bean));

		bean.setValueListUrl("systemColumnListFind.json");
		Assertions.assertNull(SystemUserInterfaceAwareUtils.getSystemListName(bean));

		bean.setValueListUrl(null);
		bean.setUserInterfaceConfig("xtype: 'datefield'");
		Assertions.assertNull(SystemUserInterfaceAwareUtils.getSystemListName(bean));

		bean.setUserInterfaceConfig("xtype: '" + SystemUserInterfaceAwareUtils.SYSTEM_LIST_COMBO_XTYPE + "' " + SystemUserInterfaceAwareUtils.SYSTEM_LIST_COMBO_LIST_NAME_PROPERTY
				+ ": 'Test List of Values'");
		Assertions.assertEquals(SystemUserInterfaceAwareUtils.getSystemListName(bean), "Test List of Values");

		bean = createNewBean(SystemUserInterfaceAwareUtils.SYSTEM_LIST_ITEM_TABLE_NAME);
		bean.setValueListUrl("systemListItemListFind.json?" + SystemUserInterfaceAwareUtils.SYSTEM_LIST_COMBO_LIST_NAME_PROPERTY + "=Test List of Values");
		Assertions.assertEquals(SystemUserInterfaceAwareUtils.getSystemListName(bean), "Test List of Values");
	}


	@Test
	public void testGetUserInterfaceConfigPropertyValue() {
		BaseNamedSystemUserInterfaceAwareEntity bean = createNewBean(null);
		Assertions.assertNull(SystemUserInterfaceAwareUtils.getUserInterfaceConfigPropertyValue(bean, "xtype"));
		Assertions.assertNull(SystemUserInterfaceAwareUtils.getUserInterfaceConfigPropertyValue(bean, "listName"));
		Assertions.assertNull(SystemUserInterfaceAwareUtils.getUserInterfaceConfigPropertyValue(bean, "valueField"));

		bean.setUserInterfaceConfig("xtype: 'datefield'");
		Assertions.assertEquals(SystemUserInterfaceAwareUtils.getUserInterfaceConfigPropertyValue(bean, "xtype"), "datefield");

		bean.setUserInterfaceConfig("xtype: 'system-list-combo', listName: 'Test List of Values'");
		Assertions.assertEquals(SystemUserInterfaceAwareUtils.getUserInterfaceConfigPropertyValue(bean, "xtype"), "system-list-combo");
		Assertions.assertEquals(SystemUserInterfaceAwareUtils.getUserInterfaceConfigPropertyValue(bean, "listName"), "Test List of Values");
	}


	////////////////////////////////////////////
	// Helper Method


	private BaseNamedSystemUserInterfaceAwareEntity createNewBean(String valueTableName) {
		BaseNamedSystemUserInterfaceAwareEntity bean = new BaseNamedSystemUserInterfaceAwareEntity();
		bean.setName("Test");

		if (valueTableName != null) {
			SystemTable table = new SystemTable();
			table.setName(valueTableName);
			bean.setValueTable(table);
		}
		return bean;
	}
}
