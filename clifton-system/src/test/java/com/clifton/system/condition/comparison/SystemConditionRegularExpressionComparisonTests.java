package com.clifton.system.condition.comparison;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author JasonS
 */
public class SystemConditionRegularExpressionComparisonTests {

	@Test
	public void testCompareValidation() {
		SystemConditionRegularExpressionComparison comparison = new SystemConditionRegularExpressionComparison();
		ValidationException e1 = Assertions.assertThrows(ValidationException.class, () -> comparison.validate());
		Assertions.assertEquals("A Bean Field Name is required.", e1.getMessage());
		comparison.setBeanFieldName("aBeanFieldName");
		ValidationException e2 = Assertions.assertThrows(ValidationException.class, () -> comparison.validate());
		Assertions.assertEquals("A Regular Expression is required.", e2.getMessage());
		comparison.setRegularExpression("{abc]");
		ValidationException e3 = Assertions.assertThrows(ValidationException.class, () -> comparison.validate());
		Assertions.assertEquals("Regular Expression {abc] is invalid.", e3.getMessage());
		comparison.setRegularExpression("[abc]");
		Assertions.assertDoesNotThrow(() -> comparison.validate());
	}


	@Test
	public void testSystemConditionRegularExpressionComparisonIsTrue() {
		SystemConditionRegularExpressionComparison comparison = new SystemConditionRegularExpressionComparison();
		comparison.setBeanFieldName("beanField");
		comparison.setRegularExpression("[abc]*");
		comparison.setIgnoreEmptyValue(false);
		comparison.setMatchEvaluatesToFalse(false);

		TestBean bean = new TestBean();
		bean.setBeanField("abc");

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("Bean Field is beanField with value of abc with Regular Expression [abc]* results in a match, with Ignore Empty Value is false with Matches Evaluates to False is false", context.getTrueMessage());

		comparison.setRegularExpression("[def]");
		comparison.setMatchEvaluatesToFalse(true);
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("Bean Field is beanField with value of abc with Regular Expression [def] does not match, with Ignore Empty Value is false with Matches Evaluates to False is true", context.getTrueMessage());

		bean.setBeanField(null);
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("Bean Field is beanField with value of null with Regular Expression [def] does not match, with Ignore Empty Value is false with Matches Evaluates to False is true", context.getTrueMessage());

		comparison.setIgnoreEmptyValue(true);
		comparison.setMatchEvaluatesToFalse(false);
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("Bean Field is beanField with value of null with Regular Expression [def] does not match, with Ignore Empty Value is true with Matches Evaluates to False is false", context.getTrueMessage());
	}


	@Test
	public void testSystemConditionRegularExpressionComparisonIsFalse() {
		SystemConditionRegularExpressionComparison comparison = new SystemConditionRegularExpressionComparison();
		comparison.setBeanFieldName("beanField");
		comparison.setRegularExpression("[abc]*");
		comparison.setIgnoreEmptyValue(false);
		comparison.setMatchEvaluatesToFalse(true);

		TestBean bean = new TestBean();
		bean.setBeanField("abc");

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("Bean Field is beanField with value of abc with Regular Expression [abc]* results in a match, with Ignore Empty Value is false with Matches Evaluates to False is true", context.getFalseMessage());

		comparison.setRegularExpression("[def]");
		comparison.setMatchEvaluatesToFalse(false);
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("Bean Field is beanField with value of abc with Regular Expression [def] does not match, with Ignore Empty Value is false with Matches Evaluates to False is false", context.getFalseMessage());

		bean.setBeanField(null);
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("Bean Field is beanField with value of null with Regular Expression [def] does not match, with Ignore Empty Value is false with Matches Evaluates to False is false", context.getFalseMessage());

		comparison.setIgnoreEmptyValue(true);
		comparison.setMatchEvaluatesToFalse(true);
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("Bean Field is beanField with value of null with Regular Expression [def] does not match, with Ignore Empty Value is true with Matches Evaluates to False is true", context.getFalseMessage());
	}


	public class TestBean extends NamedEntity<Integer> {

		private String beanField;


		public String getBeanField() {
			return this.beanField;
		}


		public void setBeanField(String beanField) {
			this.beanField = beanField;
		}
	}
}
