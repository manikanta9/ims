package com.clifton.system.condition;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.condition.validation.ConditionEntryTypes;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;

;


/**
 * The <code>SystemConditionServiceImplTests</code> class tests {@link SystemConditionServiceImpl} methods.
 *
 * @author vgomelsky
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemConditionServiceImplTests {

	// SystemCondition IDs
	private static final int MODIFIABLE_SYSTEM_CONDITION_WITH_NO_ENTRIES = 6;
	private static final int SYSTEM_DEFINED_SYSTEM_CONDITION_WITH_ONE_ENTRY = 7;

	// SystemConditionEntry IDs
	private static final int SYSTEM_CONDITION_ENTRY_OF_SYSTEM_DEFINED_SYSTEM_CONDITION = 800;

	// SystemBean IDs
	private static final int SYSTEM_BEAN_WITH_NAME_PROPERTY = 1;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	@Resource
	private SystemConditionService systemConditionService;
	@Resource
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	@Resource
	private SystemBeanService systemBeanService;


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIsConditionTrue_WithoutEntries() {
		Assertions.assertThrows(ValidationException.class, () -> {
			SystemCondition condition = this.systemConditionService.getSystemCondition(1);
			SystemTable bean = new SystemTable();
			this.systemConditionEvaluationHandler.isConditionTrue(condition, bean);
		});
	}


	@Test
	public void testIsConditionTrue_SimpleOneCondition() {
		SystemCondition condition = this.systemConditionService.getSystemCondition(2);
		SystemTable bean = new SystemTable();
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Simple condition must be false");

		bean.setName("Non Matching Name");
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Simple condition must be false - wrong name");

		bean.setName("Test Name");
		Assertions.assertTrue(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Simple condition must be true");
	}


	@Test
	public void testIsConditionTrue_AndWithTwoConditions() {
		SystemCondition condition = this.systemConditionService.getSystemCondition(3);
		SystemTable bean = new SystemTable();
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "AND with 2 conditions must be false - none set");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = null instead of 2)", this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean));

		bean.setName("Test Name");
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "AND with 2 conditions must be false - only one set");
		bean.setDataSource(new SystemDataSource());
		bean.getDataSource().setId(MathUtils.SHORT_TWO);
		Assertions.assertTrue(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "AND with 2 conditions must be true");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = 2) AND (name = Test Name)", this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean));

		bean.getDataSource().setId(MathUtils.SHORT_THREE);
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "AND with 2 conditions must be false - wrong second value");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = 3 instead of 2)", this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean));
	}


	@Test
	public void testIsConditionTrue_OrWithTwoConditions() {
		SystemCondition condition = this.systemConditionService.getSystemCondition(4);
		SystemTable bean = new SystemTable();
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "OR with 2 conditions must be false - none set");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = null instead of 2) OR (name = null instead of Test Name)", this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean));

		bean.setName("Test Name");
		Assertions.assertTrue(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "OR with 2 conditions must be true - only one set");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean)));
		Assertions.assertEquals("(name = Test Name)", this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean));

		bean.setDataSource(new SystemDataSource());
		bean.getDataSource().setId(MathUtils.SHORT_TWO);
		Assertions.assertTrue(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "OR with 2 conditions must be true");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = 2)", this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean));

		bean.getDataSource().setId(MathUtils.SHORT_THREE);
		Assertions.assertTrue(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "OR with 2 conditions must be false - wrong second value");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean)));
		Assertions.assertEquals("(name = Test Name)", this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean));

		bean.setName("Wrong Name");
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "OR with 2 conditions must be false - both set to invalid");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = 3 instead of 2) OR (name = Wrong Name instead of Test Name)", this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean));
	}


	@Test
	public void testIsConditionTrue_ComplexConditions() {
		// (createUserId = 777 AND (name = 'Test Name' OR dataSource.id = 2))
		SystemCondition condition = this.systemConditionService.getSystemCondition(5);
		SystemTable bean = new SystemTable();
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Complex condition must be false - none set");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = null instead of 2) OR (name = null instead of Test Name)", this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean));

		bean.setName("Test Name");
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Complex condition must be false - only OR set");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean)));
		Assertions.assertEquals("(createUserId = 0 instead of 777)", this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean));

		bean.setCreateUserId(new Integer(555).shortValue());
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Complex condition must be false - AND is wrong");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean)));
		Assertions.assertEquals("(createUserId = 555 instead of 777)", this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean));

		bean.setCreateUserId(new Integer(777).shortValue());
		Assertions.assertTrue(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Complex condition must be true");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean)));
		Assertions.assertEquals("(name = Test Name) AND (createUserId = 777)", this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean));

		bean.setName("Wrong Name");
		Assertions.assertFalse(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Complex condition must be false - only AND set");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = null instead of 2) OR (name = Wrong Name instead of Test Name)", this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean));

		bean.setDataSource(new SystemDataSource());
		bean.getDataSource().setId(MathUtils.SHORT_TWO);
		Assertions.assertTrue(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Complex condition must be true");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = 2) AND (createUserId = 777)", this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean));

		bean.setName("Test Name");
		Assertions.assertTrue(this.systemConditionEvaluationHandler.isConditionTrue(condition, bean), "Complex condition must be true - everything set");
		Assertions.assertTrue(StringUtils.isEmpty(this.systemConditionEvaluationHandler.getConditionFalseMessage(condition, bean)));
		Assertions.assertEquals("(dataSource.id = 2) AND (createUserId = 777)", this.systemConditionEvaluationHandler.getConditionTrueMessage(condition, bean));
	}


	@Test
	public void testModifyingNonSystemDefinedCondition() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemCondition condition = this.systemConditionService.getSystemCondition(MODIFIABLE_SYSTEM_CONDITION_WITH_NO_ENTRIES);
			Assertions.assertNotSame(condition.getDescription(), "Testing modification");
			condition.setDescription("Testing modification");
			this.systemConditionService.saveSystemCondition(condition);
			SystemCondition updatedCondition = this.systemConditionService.getSystemConditionPopulated(MODIFIABLE_SYSTEM_CONDITION_WITH_NO_ENTRIES);
			Assertions.assertEquals(0, CollectionUtils.getSize(updatedCondition.getConditionEntry().getChildEntryList()));
			Assertions.assertEquals(updatedCondition.getDescription(), "Testing modification");

			SystemBean bean = this.systemBeanService.getSystemBean(SYSTEM_BEAN_WITH_NAME_PROPERTY);
			SystemConditionEntry entry = new SystemConditionEntry();
			entry.setEntryType(ConditionEntryTypes.CONDITION);
			entry.setParent(updatedCondition.getConditionEntry());
			entry.setBean(bean);
			this.systemConditionService.saveSystemConditionEntry(entry);

			updatedCondition = this.systemConditionService.getSystemConditionPopulated(MODIFIABLE_SYSTEM_CONDITION_WITH_NO_ENTRIES);
			Assertions.assertEquals(1, CollectionUtils.getSize(updatedCondition.getConditionEntry().getChildEntryList()));

			updatedCondition.setSystemDefined(true);
			this.systemConditionService.saveSystemCondition(updatedCondition);
		});
		Assertions.assertEquals("System Defined on System Condition cannot be changed.", ve.getMessage());
	}


	@Test
	public void testModifyingSystemDefinedCondition() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemCondition condition = this.systemConditionService.getSystemCondition(SYSTEM_DEFINED_SYSTEM_CONDITION_WITH_ONE_ENTRY);
			condition.setName("Updated Name");
			this.systemConditionService.saveSystemCondition(condition);
		});
		Assertions.assertEquals("System Defined System Condition Names cannot be changed.", ve.getMessage());
	}


	@Test
	public void testDeletingSystemDefinedConditionFails() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			this.systemConditionService.deleteSystemCondition(SYSTEM_DEFINED_SYSTEM_CONDITION_WITH_ONE_ENTRY);
		});
		Assertions.assertEquals("Deleting a System Defined System Condition is not allowed.", ve.getMessage());
	}


	@Test
	public void testAddingSystemDefinedConditionFails() {
		SystemCondition condition = new SystemCondition();
		condition.setName("New system defined condition");
		condition.setDescription("Testing");
		condition.setSystemDefined(true);

		SystemConditionEntry entry = new SystemConditionEntry();
		entry.setEntryType(ConditionEntryTypes.AND);
		condition.setConditionEntry(entry);

		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			this.systemConditionService.saveSystemCondition(condition);
		});
		Assertions.assertEquals("Adding a new System Defined System Condition is not allowed.", ve.getMessage());
	}


	@Test
	public void testModifyingConditionEntryOfSystemDefinedConditionIsAllowed() {
		SystemConditionEntry conditionEntry = this.systemConditionService.getSystemConditionEntry(SYSTEM_CONDITION_ENTRY_OF_SYSTEM_DEFINED_SYSTEM_CONDITION);
		conditionEntry.setBean(this.systemBeanService.getSystemBean(2));
		this.systemConditionService.saveSystemConditionEntry(conditionEntry);
	}


	@Test
	public void testSystemConditionNoOverrideMessage() {
		SystemCondition condition = this.systemConditionService.getSystemCondition(2);
		SystemTable bean = new SystemTable();

		bean.setName("Non Matching Name");
		EvaluationResult evaluationResult = this.systemConditionEvaluationHandler.evaluateCondition(condition, bean);
		Assertions.assertEquals("(name = Non Matching Name instead of Test Name)", evaluationResult.getMessage());

		bean.setName("Test Name");
		evaluationResult = this.systemConditionEvaluationHandler.evaluateCondition(condition, bean);
		Assertions.assertEquals("(name = Test Name)", evaluationResult.getMessage());
	}


	@Test
	public void testSystemConditionOverrideMessage() {
		SystemCondition condition = this.systemConditionService.getSystemCondition(2);
		condition.setMessageOverrideForFalse("Override False Message: Condition is false");
		condition.setMessageOverrideForTrue("Override True Message: Condition is true");
		SystemTable bean = new SystemTable();

		bean.setName("Non Matching Name");
		EvaluationResult evaluationResult = this.systemConditionEvaluationHandler.evaluateCondition(condition, bean);
		Assertions.assertEquals("Override False Message: Condition is false", evaluationResult.getMessage());

		bean.setName("Test Name");
		evaluationResult = this.systemConditionEvaluationHandler.evaluateCondition(condition, bean);
		Assertions.assertEquals("Override True Message: Condition is true", evaluationResult.getMessage());
	}
}
