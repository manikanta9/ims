package com.clifton.system.schema.column.value;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.schema.column.CustomColumnTypes;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration()
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class SystemColumnValueServiceImplTests<T extends SystemColumn> {

	@Resource
	private SystemColumnValueService systemColumnValueService;

	@Resource
	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////////

	private static final int TEST_ENTITY_1 = 1;

	private static final short DATED_COLUMN_GROUP_ID = 1;
	private static final short NOT_DATED_COLUMN_GROUP_ID = 2;


	////////////////////////////////////////////////////////////////////////////////
	///////////             System Column Dated Value Tests               //////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveSystemColumnDatedValue_String() {
		SystemColumnDatedValue datedValue = new SystemColumnDatedValue();
		datedValue.setColumn(getSystemColumnCustom(TEST_ENTITY_1, DATED_COLUMN_GROUP_ID, "String Field", CustomColumnTypes.COLUMN_DATED_VALUE));
		datedValue.setStartDate(DateUtils.toDate("01/01/2014"));
		datedValue.setValue("This is a test");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);

		Assertions.assertEquals(this.systemColumnValueService.getSystemColumnDatedValueAsObject(datedValue), "This is a test");
	}


	@Test
	public void testSaveSystemColumnDatedValue_Integer() {
		SystemColumnDatedValue datedValue = new SystemColumnDatedValue();
		datedValue.setColumn(getSystemColumnCustom(TEST_ENTITY_1, DATED_COLUMN_GROUP_ID, "Integer Field", CustomColumnTypes.COLUMN_DATED_VALUE));
		datedValue.setStartDate(DateUtils.toDate("01/01/2014"));
		datedValue.setValue("5");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);

		Assertions.assertEquals(5, this.systemColumnValueService.getSystemColumnDatedValueAsObject(datedValue));

		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			datedValue.setValue("5.75");
			this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);
		});
		Assertions.assertEquals("Cannot convert value '5.75' to data type 'INTEGER' for column 'Integer Field'", ve.getMessage());
	}


	@Test
	public void testSaveSystemColumnDatedValue_Decimal() {
		SystemColumnDatedValue datedValue = new SystemColumnDatedValue();
		datedValue.setColumn(getSystemColumnCustom(TEST_ENTITY_1, DATED_COLUMN_GROUP_ID, "Decimal Field", CustomColumnTypes.COLUMN_DATED_VALUE));
		datedValue.setStartDate(DateUtils.toDate("01/01/2014"));
		datedValue.setValue("5");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);

		Assertions.assertEquals(new BigDecimal("5.0"), this.systemColumnValueService.getSystemColumnDatedValueAsObject(datedValue));

		datedValue.setValue("5.75");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);

		Assertions.assertEquals(new BigDecimal("5.75"), this.systemColumnValueService.getSystemColumnDatedValueAsObject(datedValue));

		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			datedValue.setValue("I'm not a decimal");
			this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);
		});
		Assertions.assertEquals("Cannot convert value 'I'm not a decimal' to data type 'DECIMAL' for column 'Decimal Field'", ve.getMessage());
	}


	@Test
	public void testSaveSystemColumnDatedValue_Boolean() {
		SystemColumnDatedValue datedValue = new SystemColumnDatedValue();
		datedValue.setColumn(getSystemColumnCustom(TEST_ENTITY_1, DATED_COLUMN_GROUP_ID, "Boolean Field", CustomColumnTypes.COLUMN_DATED_VALUE));
		datedValue.setStartDate(DateUtils.toDate("01/01/2014"));
		datedValue.setValue("false");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);

		Assertions.assertFalse((Boolean) this.systemColumnValueService.getSystemColumnDatedValueAsObject(datedValue));

		datedValue.setValue("no");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);
		Assertions.assertFalse((Boolean) this.systemColumnValueService.getSystemColumnDatedValueAsObject(datedValue));

		datedValue.setValue("true");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);
		Assertions.assertTrue((Boolean) this.systemColumnValueService.getSystemColumnDatedValueAsObject(datedValue));

		datedValue.setValue("yes");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);
		Assertions.assertTrue((Boolean) this.systemColumnValueService.getSystemColumnDatedValueAsObject(datedValue));

		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			datedValue.setValue("I'm not a boolean");
			this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);
		});
		Assertions.assertEquals("Cannot convert value 'I'm not a boolean' to data type 'BOOLEAN' for column 'Boolean Field'", ve.getMessage());
	}


	@Test
	public void testSaveSystemColumnDatedValue_NotDatedColumnGroup() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnDatedValue datedValue = new SystemColumnDatedValue();
			datedValue.setColumn(getSystemColumnCustom(TEST_ENTITY_1, NOT_DATED_COLUMN_GROUP_ID, "String Field", CustomColumnTypes.COLUMN_VALUE));
			datedValue.setStartDate(DateUtils.toDate("01/01/2014"));
			datedValue.setValue("This is a test for a dated value for a column that isn't dated");
			this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);
		});
		Assertions.assertEquals("Column Group: Test Group 2 Not Dated Values Column: String Field does not support dated values", ve.getMessage());
	}


	@Test
	public void testSaveSystemColumnDatedValue_OverlappingDateRange() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnDatedValue datedValue = new SystemColumnDatedValue();
			datedValue.setColumn(getSystemColumnCustom(TEST_ENTITY_1, DATED_COLUMN_GROUP_ID, "String Field", CustomColumnTypes.COLUMN_DATED_VALUE));
			datedValue.setStartDate(DateUtils.toDate("01/01/2014"));
			datedValue.setEndDate(DateUtils.toDate("01/31/2014"));
			datedValue.setValue("This is a test");
			this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);

			SystemColumnDatedValue datedValue2 = new SystemColumnDatedValue();
			datedValue2.setColumn(getSystemColumnCustom(TEST_ENTITY_1, DATED_COLUMN_GROUP_ID, "String Field", CustomColumnTypes.COLUMN_DATED_VALUE));
			datedValue2.setStartDate(DateUtils.toDate("01/15/2014"));
			datedValue2.setEndDate(DateUtils.toDate("01/31/2014"));
			datedValue2.setValue("This is an overlap");
			this.systemColumnValueService.saveSystemColumnDatedValue(datedValue2);
		});
		Assertions.assertEquals("System column dated value [String Field = This is a test  (01/01/2014 - 01/31/2014)] already exists with overlapping date range.", ve.getMessage());
	}


	@Test
	public void testSaveSystemColumnDatedValue_AutomaticallyEndPreviousValue() {
		SystemColumnDatedValue datedValue = new SystemColumnDatedValue();
		datedValue.setColumn(getSystemColumnCustom(TEST_ENTITY_1, DATED_COLUMN_GROUP_ID, "String Field", CustomColumnTypes.COLUMN_DATED_VALUE));
		datedValue.setStartDate(DateUtils.toDate("01/01/2014"));
		datedValue.setValue("This is a test");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue);

		SystemColumnDatedValue datedValue2 = new SystemColumnDatedValue();
		datedValue2.setColumn(getSystemColumnCustom(TEST_ENTITY_1, DATED_COLUMN_GROUP_ID, "String Field", CustomColumnTypes.COLUMN_DATED_VALUE));
		datedValue2.setStartDate(DateUtils.toDate("01/15/2014"));
		datedValue2.setValue("This is the next value");
		this.systemColumnValueService.saveSystemColumnDatedValue(datedValue2);

		datedValue = this.systemColumnValueService.getSystemColumnDatedValue(datedValue.getId());
		Assertions.assertEquals("01/14/2014", DateUtils.fromDateShort(datedValue.getEndDate()), "Previous Value should have been ended on 1/14/2014");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private SystemColumnCustom getSystemColumnCustom(int fkFieldId, short columnGroupId, String columnName, CustomColumnTypes customColumnType) {
		List<SystemColumnCustom> customList = this.systemColumnService.getSystemColumnCustomListForColumnGroupAndEntityId(columnGroupId, fkFieldId, customColumnType);
		return CollectionUtils.getFirstElementStrict(BeanUtils.filter(customList, SystemColumnCustom::getName, columnName));
	}
}
