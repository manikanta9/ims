package com.clifton.system.schema;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import com.clifton.system.schema.util.SystemSchemaUtilHandlerImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.Serializable;


/**
 * Test class for {@link SystemSchemaUtilHandlerImpl}.
 */
public class SystemSchemaUtilHandlerImplTest {

	private static final String TABLE_NAME = "Table Name";
	private static final String TABLE_ALIAS = "TBL";
	private static final String DATASOURCE_NAME = "Data Source Name";
	private static final String DATASOURCE_ALIAS = "DS";


	@Mock
	private SystemSchemaService systemSchemaService;
	@Mock
	private DaoLocator daoLocator;
	@Mock
	private ReadOnlyDAO<IdentityObject> readOnlyDAO;
	@Mock
	private DAOConfiguration<IdentityObject> daoConfiguration;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetEntityFromUniqueId() throws Exception {
		IdentityObject entity = generateSystemSchemaUtilHandler().getEntityFromUniqueId("ABC123DEF");
		Assertions.assertEquals(entity.getClass(), TestEntity.class);
		Assertions.assertEquals(((TestEntity) entity).getName(), "123");
		Mockito.verify(getDaoLocator(), Mockito.times(1)).locate(TABLE_NAME);
	}


	@Test
	public void testGetEntityUniqueIdComponents() throws Exception {
		String[] components = generateSystemSchemaUtilHandler().getEntityUniqueIdComponents("ABC123DEF");
		Assertions.assertEquals("ABC", components[0]);
		Assertions.assertEquals("DEF", components[1]);
		Assertions.assertEquals("123", components[2]);
	}


	@Test
	public void testGetEntityUniqueIdComponentsFailTooFewElements() throws Exception {
		Exception re = Assertions.assertThrows(RuntimeException.class, () -> {
			generateSystemSchemaUtilHandler().getEntityUniqueIdComponents("ABC123");
		});
		Assertions.assertEquals(String.format("An unexpected number of components was found in the entity unique key: [%s]", ArrayUtils.toString("ABC", 123)), re.getMessage());
	}


	@Test
	public void testGetEntityUniqueIdComponentsFailTooManyElements() throws Exception {
		Exception re = Assertions.assertThrows(RuntimeException.class, () -> {
			generateSystemSchemaUtilHandler().getEntityUniqueIdComponents("ABC123DEF123");
		});
		Assertions.assertEquals(String.format("An unexpected number of components was found in the entity unique key: [%s]", ArrayUtils.toString("ABC", 123, "DEF", 123)), re.getMessage());
	}


	@Test
	public void testGetUniqueIdForEntityParametersShort() throws Exception {
		int id = 123;
		SystemTable table = generateSystemTable();
		String uniqueId = generateSystemSchemaUtilHandler().getEntityUniqueId(table, id);
		Assertions.assertEquals(DATASOURCE_ALIAS + id + TABLE_ALIAS, uniqueId);
	}


	@Test
	public void testGetUniqueIdForEntityParametersLong() throws Exception {
		int id = 1234567890;
		SystemTable table = generateSystemTable();
		String uniqueId = generateSystemSchemaUtilHandler().getEntityUniqueId(table, id);
		Assertions.assertEquals(DATASOURCE_ALIAS + id + TABLE_ALIAS, uniqueId);
	}


	@Test
	public void testGetUniqueIdForEntityParametersInvalidLength() throws Exception {
		long id = 12345678901L;
		Exception re = Assertions.assertThrows(RuntimeException.class, () -> {
			SystemTable table = generateSystemTable();
			generateSystemSchemaUtilHandler().getEntityUniqueId(table, id);
		});
		Assertions.assertEquals(String.format("The generated unique key is longer than the maximum allowed length. Key: [%s]. Maximum length: [%d].", DATASOURCE_ALIAS + id + TABLE_ALIAS, 16), re.getMessage());
	}


	@Test
	public void testGetUniqueIdForEntityObject() throws Exception {
		TestEntity entity = new TestEntity();
		entity.setId(123);
		String uniqueIdForEntity = generateSystemSchemaUtilHandler().getEntityUniqueId(entity);
		Assertions.assertEquals(DATASOURCE_ALIAS + entity.getId() + TABLE_ALIAS, uniqueIdForEntity);
	}


	@Test
	public void testGetUniqueIdForEntityObjectInvalidIdType() throws Exception {
		Exception re = Assertions.assertThrows(RuntimeException.class, () -> {
			generateSystemSchemaUtilHandler().getEntityUniqueId(new TestEntityInvalidType());
		});
		Assertions.assertEquals(String.format("The entity ID must be of type [%s] to generate a unique key.", Number.class.getName()), re.getMessage());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemSchemaUtilHandler generateSystemSchemaUtilHandler() {
		SystemSchemaUtilHandlerImpl schemaUtilHandler = new SystemSchemaUtilHandlerImpl();
		schemaUtilHandler.setSystemSchemaService(getSystemSchemaService());
		schemaUtilHandler.setDaoLocator(getDaoLocator());

		// Setup mock methods
		SystemTable table = generateSystemTable();
		Mockito.when(getSystemSchemaService().getSystemTableByName(ArgumentMatchers.eq(TABLE_NAME))).then(invocation -> table);
		Mockito.when(getSystemSchemaService().getSystemTableList(ArgumentMatchers.any())).thenReturn(CollectionUtils.createList(table));
		Mockito.when(getDaoLocator().locate(ArgumentMatchers.anyString())).thenReturn(getReadOnlyDAO());
		Mockito.when(getDaoLocator().locate(ArgumentMatchers.any(IdentityObject.class))).thenReturn(getReadOnlyDAO());
		Mockito.when(getReadOnlyDAO().getConfiguration()).thenReturn(getDaoConfiguration());
		Mockito.when(getDaoConfiguration().getTableName()).thenReturn(TABLE_NAME);
		Mockito.when(getReadOnlyDAO().findByPrimaryKey(ArgumentMatchers.any(Serializable.class))).then(invocation -> {
			TestEntity entity = new TestEntity();
			entity.setName(String.valueOf(invocation.getArguments()[0]));
			return entity;
		});

		return schemaUtilHandler;
	}


	private SystemTable generateSystemTable() {
		SystemTable table = new SystemTable();
		SystemDataSource dataSource = new SystemDataSource();
		table.setName(TABLE_NAME);
		table.setAlias(TABLE_ALIAS);
		table.setDataSource(dataSource);
		dataSource.setName(DATASOURCE_NAME);
		dataSource.setAlias(DATASOURCE_ALIAS);
		return table;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static class TestEntity extends NamedEntity<Integer> {

	}

	private static class TestEntityInvalidType extends NamedEntity<Serializable> {

	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ReadOnlyDAO<IdentityObject> getReadOnlyDAO() {
		return this.readOnlyDAO;
	}


	public void setReadOnlyDAO(ReadOnlyDAO<IdentityObject> readOnlyDAO) {
		this.readOnlyDAO = readOnlyDAO;
	}


	public DAOConfiguration<IdentityObject> getDaoConfiguration() {
		return this.daoConfiguration;
	}


	public void setDaoConfiguration(DAOConfiguration<IdentityObject> daoConfiguration) {
		this.daoConfiguration = daoConfiguration;
	}
}
