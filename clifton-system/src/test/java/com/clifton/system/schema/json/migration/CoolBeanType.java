package com.clifton.system.schema.json.migration;

/**
 * @author stevenf
 */
public class CoolBeanType {

	private boolean greenBean;
	private boolean meanBean;


	public boolean isMeanBean() {
		return this.meanBean;
	}


	public void setMeanBean(boolean meanBean) {
		this.meanBean = meanBean;
	}


	public boolean isGreenBean() {
		return this.greenBean;
	}


	public void setGreenBean(boolean greenBean) {
		this.greenBean = greenBean;
	}
}
