package com.clifton.system.schema;

import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>SystemDataTypeConverterTests</code> class tests {@link SystemSchemaServiceImpl} methods.
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemDataTypeConverterTests {

	@Resource
	private SystemSchemaService systemSchemaService;

	private static final short STRING_ID = 1;
	private static final short INTEGER_ID = 2;
	private static final short DECIMAL_ID = 3;
	private static final short BOOLEAN_ID = 4;
	private static final short DATE_ID = 5;


	@Test
	public void testConvertValueToSystemDataTypeString() {
		SystemDataType type = this.systemSchemaService.getSystemDataType(STRING_ID);
		SystemDataTypeConverter converter = new SystemDataTypeConverter(type);

		String str = (String) converter.convert(null);
		Assertions.assertNull(str);

		str = (String) converter.convert("Test");
		Assertions.assertEquals("Test", str);

		str = (String) converter.convert("123");
		Assertions.assertEquals("123", str);

		str = (String) converter.convert("1.589");
		Assertions.assertEquals("1.589", str);

		str = (String) converter.convert("01/01/2010");
		Assertions.assertEquals("01/01/2010", str);
	}


	@Test
	public void testConvertValueToSystemDataTypeIntegerSucceed() {
		SystemDataType type = this.systemSchemaService.getSystemDataType(INTEGER_ID);
		SystemDataTypeConverter converter = new SystemDataTypeConverter(type);

		Integer obj = (Integer) converter.convert(null);
		Assertions.assertNull(obj);

		obj = (Integer) converter.convert("5");
		Assertions.assertEquals(5, obj.intValue());

		obj = (Integer) converter.convert("9876");
		Assertions.assertEquals(9876, obj.intValue());

		// Special Case - If decimal ends in .0 then OK to still convert to integer
		// This happens in uploads because of Excel formatting as number a value like 0 could be imported as 0.0, which we can still convert to 0
		obj = (Integer) converter.convert("0.0");
		Assertions.assertEquals(0, obj.intValue());

		obj = (Integer) converter.convert("0.0000");
		Assertions.assertEquals(0, obj.intValue());

		obj = (Integer) converter.convert("5.000");
		Assertions.assertEquals(5, obj.intValue());
	}


	@Test
	public void testConvertValueToSystemDataTypeIntegerFailWithString() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemDataType type = this.systemSchemaService.getSystemDataType(INTEGER_ID);
			SystemDataTypeConverter converter = new SystemDataTypeConverter(type);
			converter.convert("test");
		});
	}


	@Test
	public void testConvertValueToSystemDataTypeIntegerFailWithDouble() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemDataType type = this.systemSchemaService.getSystemDataType(INTEGER_ID);
			SystemDataTypeConverter converter = new SystemDataTypeConverter(type);
			converter.convert("10.8");
		});
	}


	@Test
	public void testConvertValueToSystemDataTypeDecimalSucceed() {
		SystemDataType type = this.systemSchemaService.getSystemDataType(DECIMAL_ID);
		SystemDataTypeConverter converter = new SystemDataTypeConverter(type);

		BigDecimal obj = (BigDecimal) converter.convert(null);
		Assertions.assertNull(obj);

		obj = (BigDecimal) converter.convert("5");
		Assertions.assertEquals(5.0, obj.doubleValue(), 0d);

		obj = (BigDecimal) converter.convert("9876");
		Assertions.assertEquals(9876.0, obj.doubleValue(), 0d);

		obj = (BigDecimal) converter.convert("10.8");
		Assertions.assertEquals(10.8, obj.doubleValue(), 0d);
	}


	@Test
	public void testConvertValueToSystemDataTypeDecimalFail() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemDataType type = this.systemSchemaService.getSystemDataType(DECIMAL_ID);
			SystemDataTypeConverter converter = new SystemDataTypeConverter(type);
			converter.convert("test");
		});
	}


	@Test
	public void testConvertValueToSystemDataTypeBoolean() {
		SystemDataType type = this.systemSchemaService.getSystemDataType(BOOLEAN_ID);
		SystemDataTypeConverter converter = new SystemDataTypeConverter(type);

		Boolean obj = (Boolean) converter.convert(null);
		Assertions.assertNull(obj);

		obj = (Boolean) converter.convert("yes");
		Assertions.assertTrue(obj);

		obj = (Boolean) converter.convert("no");
		Assertions.assertFalse(obj);

		obj = (Boolean) converter.convert("true");
		Assertions.assertTrue(obj);

		obj = (Boolean) converter.convert("TRUE");
		Assertions.assertTrue(obj);

		obj = (Boolean) converter.convert("FALSE");
		Assertions.assertFalse(obj);

		obj = (Boolean) converter.convert("tRuE");
		Assertions.assertTrue(obj);

		try {
			obj = (Boolean) converter.convert("anything");
		}
		catch (Exception e) {
			obj = Boolean.FALSE;
			Assertions.assertEquals("Invalid 'BOOLEAN' value = anything", e.getMessage());
		}
		Assertions.assertFalse(obj); // will fail unless exception is thrown
	}


	@Test
	public void testConvertValueToSystemDataTypeDateSucceed() {
		SystemDataType type = this.systemSchemaService.getSystemDataType(DATE_ID);
		SystemDataTypeConverter converter = new SystemDataTypeConverter(type);

		Date obj = (Date) converter.convert(null);
		Assertions.assertNull(obj);

		long dateTime = DateUtils.toDate("01/01/2010").getTime();

		obj = (Date) converter.convert("01/01/2010");
		Assertions.assertEquals(dateTime, obj.getTime());

		obj = (Date) converter.convert("01/01/2010 10:00 AM");
		Assertions.assertEquals(dateTime, obj.getTime());
	}


	@Test
	public void testConvertValueToSystemDataTypeDateFailWithString() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemDataType type = this.systemSchemaService.getSystemDataType(DATE_ID);
			SystemDataTypeConverter converter = new SystemDataTypeConverter(type);
			converter.convert("test");
		});
	}


	@Test
	public void testConvertValueToSystemDataTypeDateFailWithNumber() {
		Assertions.assertThrows(Exception.class, () -> {
			SystemDataType type = this.systemSchemaService.getSystemDataType(DATE_ID);
			SystemDataTypeConverter converter = new SystemDataTypeConverter(type);
			converter.convert("123456");
		});
	}
}
