package com.clifton.system.schema.column;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationService;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@ContextConfiguration()
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class SystemColumnServiceImplTests<T extends SystemColumn> {

	@Resource
	private SystemColumnService systemColumnService;

	@Resource
	private ValidationService validationService;

	@Resource
	private SystemColumnValueHandler systemColumnValueHandler;

	@Resource
	private XmlReadOnlyDAO<SystemColumnValue> systemColumnValueDAO;

	@Resource
	private XmlReadOnlyDAO<T> systemColumnDAO;

	@Resource
	private XmlReadOnlyDAO<SystemColumnTemplate> systemColumnTemplateDAO;

	@Resource
	private UpdatableDAO<SystemColumnCustomValueAwareTestEntity> systemColumnCustomValueAwareTestEntityDAO;

	@Resource
	private UpdatableDAO<SystemColumnCustomJsonValueAwareTestEntity> systemColumnCustomJsonValueAwareTestEntityDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setup() {
		this.systemColumnDAO.setLogicalEvaluatesToTrue(true);
		this.systemColumnValueDAO.setLogicalEvaluatesToTrue(true);
	}


	//This test is really looking to just not run into a null pointer exception
	@Test
	public void testSystemColumnValidateMetadata() {
		Assertions.assertEquals(this.validationService.getValidationMetaData("systemColumn").size(), 6);
	}


	@Test
	public void testGetSystemColumnListForEntityAndGroupWithNoLink() {
		// Group 1 applies to all entities - no linked filter
		SystemColumnCustomValueAwareTestEntity bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(2);
		bean.setColumnGroupName("Test Group 1");
		List<SystemColumnCustom> columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);
		Assertions.assertEquals(2, CollectionUtils.getSize(columnList));
	}


	@Test
	public void testGetSystemColumnListForEntityAndGroupWithLink() {
		// Group 2 has link filter on name

		// Bean 1 does fit link, so 2 matches total
		SystemColumnCustomValueAwareTestEntity bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(1);
		bean.setColumnGroupName("Test Group 2");
		List<SystemColumnCustom> columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);
		Assertions.assertEquals(2, CollectionUtils.getSize(columnList));

		// Bean 2 doesn't fit link, but there is 1 with a null linked value, so 1 match
		this.systemColumnDAO.setLogicalEvaluatesToTrue(false);
		bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(2);
		bean.setColumnGroupName("Test Group 2");
		columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);
		Assertions.assertEquals(1, CollectionUtils.getSize(columnList));
	}


	/**
	 * Get the list, then add a new system custom column, pull the list again - cache should be cleared and retrieve updated list
	 */
	@Test
	public void testGetSystemColumnListForEntityAndGroupWithLink_CacheTest() {
		// Group 2 has link filter on name

		// Bean 1 does fit link, so 2 matches total
		SystemColumnCustomValueAwareTestEntity bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(1);
		bean.setColumnGroupName("Test Group 2");
		List<SystemColumnCustom> columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);
		Assertions.assertEquals(2, CollectionUtils.getSize(columnList));

		// Copy Both - Stopping at each point to find if cache is cleared (validated both with linked value and without)
		SystemColumnCustom copy1 = BeanUtils.cloneBean(columnList.get(0), false, false);
		copy1.setId(null);
		copy1.setName(copy1.getName() + "NEW");
		this.systemColumnService.saveSystemColumnCustom(copy1);
		columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);
		Assertions.assertEquals(3, CollectionUtils.getSize(columnList));

		SystemColumnCustom copy2 = BeanUtils.cloneBean(columnList.get(1), false, false);
		copy2.setId(null);
		copy2.setName(copy2.getName() + "NEW");
		this.systemColumnService.saveSystemColumnCustom(copy2);
		columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);
		Assertions.assertEquals(4, CollectionUtils.getSize(columnList));
	}


	public void testGetSystemColumnValueListForEntityAndGroupWithNoLink() {
		// Bean 1 - Group 1 has no values in the database
		SystemColumnCustomValueAwareTestEntity bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(2);
		bean.setColumnGroupName("Test Group 1");
		List<SystemColumnValue> valueList = this.systemColumnValueHandler.getSystemColumnValueListForEntity(bean);
		Assertions.assertTrue(CollectionUtils.isEmpty(valueList));
	}


	@Test
	public void testSaveSystemColumnValueListForEntityRequiredValidationFail() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Bean 1 - Group 2 Has 2 columns available: String (required) & Integer (optional)
			SystemColumnCustomValueAwareTestEntity bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(1);
			bean.setColumnGroupName("Test Group 2");
			List<SystemColumnCustom> columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);
			Assertions.assertEquals(2, CollectionUtils.getSize(columnList));
			List<SystemColumnValue> valueList = new ArrayList<>();
			for (SystemColumnCustom column : columnList) {
				// Skip Required column
				if (!column.isRequired()) {
					SystemColumnValue value = new SystemColumnValue();
					value.setColumn(column);
					value.setValue("1");
					value.setText("1");
					valueList.add(value);
				}
			}
			bean.setColumnValueList(valueList);
			this.systemColumnCustomValueAwareTestEntityDAO.save(bean);
		});
		Assertions.assertEquals("Missing values for required column(s) [Special Integer].", ve.getMessage());
	}


	@Test
	public void testSaveSystemColumnJsonValueListForEntityRequiredValidationFail() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Bean 1 - Group 4 Has 2 columns available: String (required) & Integer (optional)
			// Set NO VALUES
			SystemColumnCustomJsonValueAwareTestEntity bean = this.systemColumnCustomJsonValueAwareTestEntityDAO.findByPrimaryKey(1);
			bean.setColumnGroupName("Test Group 4");
			this.systemColumnCustomJsonValueAwareTestEntityDAO.save(bean);
		});
		Assertions.assertEquals("Missing values for required column(s) [Second Name].", ve.getMessage());
	}


	@Test
	public void testSaveSystemColumnValueListForEntityDataTypeValidationFail() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Bean 1 - Group 2 Has 2 Columns available: String (required) & Integer (optional)
			SystemColumnCustomValueAwareTestEntity bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(1);
			bean.setColumnGroupName("Test Group 2");
			List<SystemColumnCustom> columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);
			Assertions.assertEquals(2, CollectionUtils.getSize(columnList));
			List<SystemColumnValue> valueList = new ArrayList<>();
			for (SystemColumnCustom column : columnList) {
				SystemColumnValue value = new SystemColumnValue();
				value.setColumn(column);
				value.setValue("abc");
				value.setText("abc");
				valueList.add(value);
			}
			bean.setColumnValueList(valueList);
			this.systemColumnCustomValueAwareTestEntityDAO.save(bean);
		});
		Assertions.assertEquals("Cannot convert value 'abc' to data type 'INTEGER' for column 'Special Integer", ve.getMessage());
	}


	@Test
	public void testSaveSystemColumnJsonValueListForEntityDataTypeValidationFail() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			// Bean 1 - Group 4 Has 2 Columns available: String (required) & Integer (optional)
			SystemColumnCustomJsonValueAwareTestEntity bean = this.systemColumnCustomJsonValueAwareTestEntityDAO.findByPrimaryKey(1);
			bean.setColumnGroupName("Test Group 4");
			bean.setCustomColumns(new CustomJsonString(""));
			CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(bean.getCustomColumns());
			bean.getCustomColumns().setColumnValueSimple("SecondName", "Test Second Name");
			bean.getCustomColumns().setColumnValueSimple("AnyInteger", "abc");

			this.systemColumnCustomJsonValueAwareTestEntityDAO.save(bean);
		});
		Assertions.assertEquals("Cannot convert value 'abc' to data type 'INTEGER' for column 'Any Integer", ve.getMessage());
	}


	// FIXME SUBCLASSES AREN'T WORKING FOR DAO LOOK UPS
	public void testSaveSystemColumnValueListForEntitySuccess() {
		// Bean 1 - Group 2 Has 2 Columns available: String (required) & Integer (optional)
		SystemColumnCustomValueAwareTestEntity bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(1);
		bean.setColumnGroupName("Test Group 2");
		List<SystemColumnCustom> columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);
		Assertions.assertEquals(2, CollectionUtils.getSize(columnList));
		List<SystemColumnValue> valueList = new ArrayList<>();
		for (SystemColumnCustom column : columnList) {
			SystemColumnValue value = new SystemColumnValue();
			value.setColumn(column);
			if ("String".equals(column.getDataType().getName())) {
				value.setValue("abc");
				value.setText("abc");
			}
			else {
				value.setValue("123");
				value.setText("123");
			}
			valueList.add(value);
		}
		bean.setColumnValueList(valueList);
		this.systemColumnCustomValueAwareTestEntityDAO.save(bean);

		// Bean 1 - Group 2 should now have 2 values in the database for Group 2
		bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(1);
		bean.setColumnGroupName("Test Group 2");
		valueList = this.systemColumnValueHandler.getSystemColumnValueListForEntity(bean);
		Assertions.assertEquals(2, CollectionUtils.getSize(valueList));

		Integer value = (Integer) this.systemColumnValueHandler.getSystemColumnValueForEntity(bean, "Test Group 2", "Special Integer", false);
		Assertions.assertTrue(MathUtils.isEqual(123, value));
	}


	@Test
	public void testSaveSystemColumnJsonValueListForEntitySuccess() {
		// Bean 1 - Group 4 Has 2 Columns available: String (required) & Integer (optional)
		SystemColumnCustomJsonValueAwareTestEntity bean = this.systemColumnCustomJsonValueAwareTestEntityDAO.findByPrimaryKey(1);
		bean.setColumnGroupName("Test Group 4");

		bean.setCustomColumns(new CustomJsonString(""));
		CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(bean.getCustomColumns());
		bean.getCustomColumns().setColumnValueSimple("SecondName", "Test Second Name");
		bean.getCustomColumns().setColumnValueSimple("AnyInteger", "123");
		this.systemColumnCustomJsonValueAwareTestEntityDAO.save(bean);

		// Bean 1 - Group 4 should now have 2 values in the database for Group 4
		bean = this.systemColumnCustomJsonValueAwareTestEntityDAO.findByPrimaryKey(1);

		Assertions.assertEquals("{\"AnyInteger\":\"123\",\"SecondName\":\"Test Second Name\"}", bean.getCustomColumns().getJsonValue());

		Assertions.assertEquals("Test Second Name", CustomJsonStringUtils.getColumnValueAsString(bean.getCustomColumns(), "SecondName"));
		// Note: This should return as an Integer - custom retrieval method?
		Assertions.assertEquals("123", CustomJsonStringUtils.getColumnValueAsString(bean.getCustomColumns(), "AnyInteger"));
		// These methods will return create data typed object
		Assertions.assertEquals(123, this.systemColumnValueHandler.getSystemColumnValueForEntity(bean, "Test Group 4", "AnyInteger", false));
		Assertions.assertEquals(123, CustomJsonStringUtils.getColumnValueAsInteger(bean.getCustomColumns(), "AnyInteger"));
		// Key Property that doesn't exist for the bean, just returns null
		Assertions.assertNull(bean.getCustomColumns().getColumnValue("DoesNotExist"));


		bean.getCustomColumns().clearColumnValue("AnyInteger");
		this.systemColumnCustomJsonValueAwareTestEntityDAO.save(bean);

		bean = this.systemColumnCustomJsonValueAwareTestEntityDAO.findByPrimaryKey(1);
		CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(bean.getCustomColumns());

		Assertions.assertEquals("{\"SecondName\":\"Test Second Name\"}", bean.getCustomColumns().getJsonValue());
		Assertions.assertEquals("Test Second Name", bean.getCustomColumns().getColumnValue("SecondName"));
	}


	// FIXME SUBCLASSES AREN'T WORKING FOR DAO LOOK UPS
	public void testDeleteSystemColumnValueListOnEntityDelete() {
		// First make sure there are existing values
		Assertions.assertFalse(CollectionUtils.isEmpty(this.systemColumnValueDAO.findByField("fkFieldId", 3)));

		// Then Delete the entity
		this.systemColumnCustomValueAwareTestEntityDAO.delete(3);

		// Then make sure there are no longer values associated with that entity
		//Assertions.assertTrue(CollectionUtils.isEmpty(this.systemColumnValueDAO.findByField("fkFieldId", 3)));
	}


	@Test
	public void testInsertSystemColumnStandard() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnStandard c = new SystemColumnStandard();
			c.setName("TEST INSERT");
			this.systemColumnService.saveSystemColumnStandard(c);
		});
		Assertions.assertEquals("Standard Columns cannot be inserted.", ve.getMessage());
	}


	@Test
	public void testDeleteSystemColumnStandard() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnStandard c = (SystemColumnStandard) this.systemColumnDAO.findByPrimaryKey(5);
			Assertions.assertFalse(c.isCustomColumn(), "Expected column with id 5 to be a standard column.");
			this.systemColumnService.deleteSystemColumn(c.getId());
		});
		Assertions.assertEquals("Standard Columns cannot be deleted.", ve.getMessage());
	}


	@Test
	public void testUpdateSystemColumnStandardName() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnStandard c = (SystemColumnStandard) this.systemColumnDAO.findByPrimaryKey(5);
			Assertions.assertFalse(c.isCustomColumn(), "Expected column with id 5 to be a standard column.");
			c.setName("Name Update");
			this.systemColumnService.saveSystemColumnStandard(c);
		});
		Assertions.assertEquals("The column's name [Name] cannot be changed", ve.getMessage());
	}


	@Test
	public void testUpdateSystemColumnStandardTable() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnStandard c = (SystemColumnStandard) this.systemColumnDAO.findByPrimaryKey(5);
			Assertions.assertFalse(c.isCustomColumn(), "Expected column with id 5 to be a standard column.");
			c.setTable(null);
			this.systemColumnService.saveSystemColumnStandard(c);
		});
		Assertions.assertEquals("The column's table [SystemColumnCustomValueAwareTestEntity] cannot be changed", ve.getMessage());
	}


	@Test
	public void testUpdateSystemColumnStandardBeanPropertyName() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnStandard c = (SystemColumnStandard) this.systemColumnDAO.findByPrimaryKey(5);
			Assertions.assertFalse(c.isCustomColumn(), "Expected column with id 5 to be a standard column.");
			c.setBeanPropertyName("Name Update");
			this.systemColumnService.saveSystemColumnStandard(c);
		});
		Assertions.assertEquals("The column's bean property name [name] cannot be changed", ve.getMessage());
	}


	@Test
	public void testUpdateSystemColumnStandardDataType() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnStandard c = (SystemColumnStandard) this.systemColumnDAO.findByPrimaryKey(5);
			Assertions.assertFalse(c.isCustomColumn(), "Expected column with id 5 to be a standard column.");
			c.setDataType(null);
			this.systemColumnService.saveSystemColumnStandard(c);
		});
		Assertions.assertEquals("The column's data type [STRING] cannot be changed", ve.getMessage());
	}


	@Test
	public void testUpdateSystemColumnStandard() {
		SystemColumnStandard c = (SystemColumnStandard) this.systemColumnDAO.findByPrimaryKey(5);
		Assertions.assertFalse(c.isCustomColumn(), "Expected column with id 5 to be a standard column.");
		c.setDescription(c.getDescription() + "_Updated");
		this.systemColumnService.saveSystemColumnStandard(c);
	}


	@Test
	public void testUpdateSystemColumnCustomName() {
		SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(1);
		Assertions.assertTrue(c.isCustomColumn(), "Expected column with id 1 to be a custom column.");
		Assertions.assertFalse(c.isSystemDefined(), "Expected column with id 1 to not be system defined");
		String originalName = c.getName();
		c.setName("Name Update");
		this.systemColumnService.saveSystemColumnCustom(c);
		// Put the name back
		c.setName(originalName);
		this.systemColumnService.saveSystemColumnCustom(c);
	}


	@Test
	public void testUpdateSystemColumnCustomName_JsonValueColumn() {
		SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(10);
		Assertions.assertTrue(c.isCustomColumn(), "Expected column with id 10 to be a custom column.");
		Assertions.assertEquals(CustomColumnTypes.COLUMN_JSON_VALUE, c.getColumnGroup().getCustomColumnType(), "Expected column with id 10 to be a JSON custom column.");
		Assertions.assertFalse(c.isSystemDefined(), "Expected column with id 10 to not be system defined");
		c.setName("Name Update");
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			this.systemColumnService.saveSystemColumnCustom(c);
		});
		Assertions.assertEquals("The column name cannot be changed for Json Custom Column Values.", ve.getMessage());
	}


	@Test
	public void testInsertSystemColumnCustom_JsonValueColumn_InvalidColumnName() {
		SystemColumnGroup columnGroup = this.systemColumnService.getSystemColumnGroupByName("Test Group 4");
		SystemColumnCustom c = new SystemColumnCustom();
		c.setColumnGroup(columnGroup);
		c.setTable(columnGroup.getTable());

		String[] badNameChecks = new String[]{"1BadName", "Bad_Name"};
		for (String badName : badNameChecks) {
			Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
				c.setName(badName);
				this.systemColumnService.saveSystemColumnCustom(c);
			});
			Assertions.assertEquals("Json Value Column Column Names must be alpha numeric only and must begin with a letter.", ve.getMessage());
		}
	}


	@Test
	public void testUpdateSystemColumnCustomSystemDefinedName() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(2);
			Assertions.assertTrue(c.isCustomColumn() && c.isSystemDefined(), "Expected column with id 2 to be a custom system defined column.");
			c.setName("Name Update");
			this.systemColumnService.saveSystemColumnCustom(c);
		});
		Assertions.assertEquals("[Name] field cannot be edited for System Defined Columns.", ve.getMessage());
	}


	@Test
	public void testEmptySystemColumnCustomGroup() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(1);
			SystemColumnCustom bean = BeanUtils.cloneBean(c, false, false);
			bean.setColumnGroup(null);
			bean.setId(null);
			this.systemColumnService.saveSystemColumnCustom(bean);
		});
		Assertions.assertEquals("Column group is required for custom columns.", ve.getMessage());
	}


	@Test
	public void testSystemColumnCustomGroupSameTable() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(1);
			SystemColumnCustom bean = BeanUtils.cloneBean(c, false, false);
			bean.setColumnGroup(this.systemColumnService.getSystemColumnGroup((short) 3));
			bean.setId(null);
			this.systemColumnService.saveSystemColumnCustom(bean);
		});
		Assertions.assertEquals("Column group table [SystemTable] must be the same as the column table [SystemColumnCustomValueAwareTestEntity].", ve.getMessage());
	}


	@Test
	public void testUpdateSystemColumnCustomSystemDefinedGroup() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(2);
			Assertions.assertTrue(c.isCustomColumn() && c.isSystemDefined(), "Expected column with id 2 to be a custom system defined column.");
			c.setColumnGroup(null);
			this.systemColumnService.saveSystemColumnCustom(c);
		});
		Assertions.assertEquals("[Column Group] field cannot be edited for System Defined Columns.", ve.getMessage());
	}


	@Test
	public void testUpdateSystemColumnCustomLinkedValue() {
		SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(1);
		Assertions.assertTrue(c.isCustomColumn(), "Expected column with id 1 to be a custom column.");
		Assertions.assertFalse(c.isSystemDefined(), "Expected column with id 1 to not be system defined");
		String value = c.getLinkedValue();
		c.setLinkedValue("");
		this.systemColumnService.saveSystemColumnCustom(c);
		// Put the value back
		c.setLinkedValue(value);
		this.systemColumnService.saveSystemColumnCustom(c);
	}


	@Test
	public void testUpdateSystemColumnCustomSystemDefinedLinkedValue() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(2);
			Assertions.assertTrue(c.isCustomColumn() && c.isSystemDefined(), "Expected column with id 2 to be a custom system defined column.");
			c.setLinkedValue("LV");
			this.systemColumnService.saveSystemColumnCustom(c);
		});
		Assertions.assertEquals("[Linked Value] field cannot be edited for System Defined Columns.", ve.getMessage());
	}


	@Test
	public void testUpdateSystemColumnCustomDataType() {
		SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(1);
		Assertions.assertTrue(c.isCustomColumn(), "Expected column with id 1 to be a custom column.");
		Assertions.assertFalse(c.isSystemDefined(), "Expected column with id 1 to not be system defined");
		SystemDataType dt = c.getDataType();
		c.setDataType(null);
		this.systemColumnService.saveSystemColumnCustom(c);
		// Put the value back
		c.setDataType(dt);
		this.systemColumnService.saveSystemColumnCustom(c);
	}


	@Test
	public void testUpdateSystemColumnCustomSystemDefinedDataType() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(2);
			Assertions.assertTrue(c.isCustomColumn() && c.isSystemDefined(), "Expected column with id 2 to be a custom system defined column.");
			c.setDataType(null);
			this.systemColumnService.saveSystemColumnCustom(c);
		});
		Assertions.assertEquals("[Data Type] field cannot be edited for System Defined Columns.", ve.getMessage());
	}


	@Test
	public void testUpdateSystemCustomColumnTemplateNameMismatch() {
		SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(1);
		SystemColumnTemplate t = this.systemColumnTemplateDAO.findByPrimaryKey(2);
		c.setTemplate(t);

		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			this.systemColumnService.saveSystemColumnCustom(c);
			c.setTemplate(null);
		});
		Assertions.assertEquals("The custom column name: " + c.getName() + " does not match the name of the selected template: " + c.getTemplate().getName(), ve.getMessage());
	}


	@Test
	public void testUpdateSystemCustomColumnTemplateNameMatch() {
		SystemColumnCustom c = (SystemColumnCustom) this.systemColumnDAO.findByPrimaryKey(1);
		SystemColumnTemplate t = this.systemColumnTemplateDAO.findByPrimaryKey(1);
		c.setTemplate(t);
		this.systemColumnService.saveSystemColumnCustom(c);
	}
}
