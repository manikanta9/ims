package com.clifton.system.schema.column;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.schema.column.value.SystemColumnValue;

import java.util.List;


public class SystemColumnCustomValueAwareTestEntity extends NamedEntity<Integer> implements SystemColumnCustomValueAware {

	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}
}
