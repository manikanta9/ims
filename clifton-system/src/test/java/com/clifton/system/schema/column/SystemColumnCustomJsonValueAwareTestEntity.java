package com.clifton.system.schema.column;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;


public class SystemColumnCustomJsonValueAwareTestEntity extends NamedEntity<Integer> implements SystemColumnCustomJsonAware {

	private String columnGroupName;

	private CustomJsonString customColumns;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public CustomJsonString getCustomColumns() {
		return this.customColumns;
	}


	public void setCustomColumns(CustomJsonString customColumns) {
		this.customColumns = customColumns;
	}
}
