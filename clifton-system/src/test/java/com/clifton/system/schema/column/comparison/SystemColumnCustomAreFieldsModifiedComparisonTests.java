package com.clifton.system.schema.column.comparison;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomValueAwareTestEntity;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>SystemColumnCustomAreFieldsModifiedComparisonTests</code> tests SystemColumnCustomAreFieldsModifiedComparison
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("../SystemColumnServiceImplTests-context.xml")
public class SystemColumnCustomAreFieldsModifiedComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private ReadOnlyDAO<SystemColumnCustomValueAwareTestEntity> systemColumnCustomValueAwareTestEntityDAO;

	@Resource
	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private SystemColumnCustomAreFieldsModifiedComparison getComparisonBean() {
		SystemColumnCustomAreFieldsModifiedComparison comparison = new SystemColumnCustomAreFieldsModifiedComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	@Test
	public void testCustomColumnModified_True() {
		SystemColumnCustomAreFieldsModifiedComparison comparison = getComparisonBean();
		comparison.setAllFields(true);
		comparison.setAllCustomFields(true);

		// New Bean
		// Group 1 applies to all entities - no linked filter
		SystemColumnCustomValueAwareTestEntity newBean = new SystemColumnCustomValueAwareTestEntity();
		newBean.setName("Test New Bean");
		newBean.setColumnGroupName("Test Group 1");
		List<SystemColumnCustom> columnList = this.systemColumnService.getSystemColumnCustomListForEntity(newBean);

		SystemColumnValue value = new SystemColumnValue();
		value.setColumn(CollectionUtils.getOnlyElement(BeanUtils.filter(columnList, SystemColumnCustom::getName, "Second Name")));
		value.setValue("test");
		value.setText("test");

		newBean.setColumnValueList(CollectionUtils.createList(value));

		Assertions.assertTrue(comparison.evaluate(newBean, null), "Comparison should evaluate to true");


		// Original Bean has No Custom Fields set
		// Group 1 applies to all entities - no linked filter
		SystemColumnCustomValueAwareTestEntity bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(2);
		bean.setColumnGroupName("Test Group 1");
		columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);

		value = new SystemColumnValue();
		value.setColumn(CollectionUtils.getOnlyElement(BeanUtils.filter(columnList, SystemColumnCustom::getName, "Second Name")));
		value.setValue("test");
		value.setText("test");

		bean.setColumnValueList(CollectionUtils.createList(value));

		Assertions.assertTrue(comparison.evaluate(bean, null), "Comparison should evaluate to true");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(bean, context);
		Assertions.assertEquals("Field [Second Name] has been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		bean.setName(bean.getName() + " Updated");
		Assertions.assertTrue(comparison.evaluate(bean, null), "Comparison should evaluate to true");

		context = new SimpleComparisonContext();
		comparison.evaluate(bean, context);
		Assertions.assertEquals("Fields [label,name,Second Name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		comparison.setAllCustomFields(false);
		comparison.setCustomFieldNames("Second Name");

		Assertions.assertTrue(comparison.evaluate(bean, null), "Comparison should evaluate to true");

		context = new SimpleComparisonContext();
		comparison.evaluate(bean, context);
		Assertions.assertEquals("Fields [label,name,Second Name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		comparison.setAllCustomFields(true);
		comparison.setExceptionCustomFieldNames("Any Integer");

		Assertions.assertTrue(comparison.evaluate(bean, null), "Comparison should evaluate to true");

		context = new SimpleComparisonContext();
		comparison.evaluate(bean, context);
		Assertions.assertEquals("Fields [label,name,Second Name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testCustomColumnModified_False() {
		SystemColumnCustomAreFieldsModifiedComparison comparison = getComparisonBean();
		comparison.setAllFields(true);
		comparison.setAllCustomFields(true);

		// Original Bean has No Custom Fields set
		// Group 1 applies to all entities - no linked filter
		SystemColumnCustomValueAwareTestEntity bean = this.systemColumnCustomValueAwareTestEntityDAO.findByPrimaryKey(2);

		Assertions.assertFalse(comparison.evaluate(bean, null), "Comparison should evaluate to false");
		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(bean, context);
		Assertions.assertEquals("None of the Fields [description,label,name] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		bean.setColumnGroupName("Test Group 1");
		Assertions.assertFalse(comparison.evaluate(bean, null), "Comparison should evaluate to false");
		context = new SimpleComparisonContext();
		comparison.evaluate(bean, context);
		Assertions.assertEquals("None of the Fields [description,label,name,Second Name,Any Integer] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));


		List<SystemColumnCustom> columnList = this.systemColumnService.getSystemColumnCustomListForEntity(bean);

		SystemColumnValue value = new SystemColumnValue();
		value.setColumn(CollectionUtils.getOnlyElement(BeanUtils.filter(columnList, SystemColumnCustom::getName, "Second Name")));
		value.setValue("test");
		value.setText("test");

		bean.setColumnValueList(CollectionUtils.createList(value));

		comparison.setExceptionCustomFieldNames("Second Name");

		context = new SimpleComparisonContext();
		comparison.evaluate(bean, context);
		Assertions.assertEquals("None of the Fields [description,label,name,Any Integer] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		comparison.setAllCustomFields(false);
		comparison.setExceptionCustomFieldNames(null);
		comparison.setCustomFieldNames("Any Integer");

		Assertions.assertFalse(comparison.evaluate(bean, null), "Comparison should evaluate to false");

		context = new SimpleComparisonContext();
		comparison.evaluate(bean, context);
		Assertions.assertEquals("None of the Fields [description,label,name,Any Integer] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
