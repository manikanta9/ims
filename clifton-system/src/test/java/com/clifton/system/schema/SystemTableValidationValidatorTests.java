package com.clifton.system.schema;

import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import com.clifton.system.schema.validation.SystemTableValidationValidator;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>SystemTableValidationValidatorTests</code> tests registering
 * the {@link SystemTableValidationValidator} and also validates the
 * conditions are properly evaluated on bean updates.
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemTableValidationValidatorTests {

	@Resource
	private XmlReadOnlyDAO<SystemTable> systemTableDAO;

	@Resource
	private UpdatableDAO<SystemHierarchyCategory> systemHierarchyCategoryDAO;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private SystemConditionService systemConditionService;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;


	@BeforeEach
	public void setup() {
		this.systemTableDAO.setLogicalEvaluatesToTrue(true);
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
	}


	@Test
	public void testInsertModifyCondition() {
		SystemTable table = this.systemTableDAO.findByPrimaryKey(MathUtils.SHORT_ONE);
		Assertions.assertEquals("SystemHierarchyCategory", table.getName());

		table.setInsertEntityCondition(this.systemConditionService.getSystemCondition(1));
		this.systemSchemaService.saveSystemTable(table);

		// Test Successful (Description = Test Description)
		SystemHierarchyCategory category = new SystemHierarchyCategory();
		category.setName("Test Succeed");
		category.setDescription("Test Description");
		this.systemHierarchyCategoryDAO.save(category);

		// Test Failure (Description != Test Description)
		SystemHierarchyCategory category2 = new SystemHierarchyCategory();
		category2.setName("Test Fail");
		category2.setDescription("Test Description FAILURE");
		try {
			this.systemHierarchyCategoryDAO.save(category2);
		}
		catch (Exception e) {
			// Expected Exception - Remove the Condition & Re-save
			table.setInsertEntityCondition(null);
			this.systemSchemaService.saveSystemTable(table);
			this.systemHierarchyCategoryDAO.save(category2);
			return;
		}
		throw new RuntimeException("Expected error on insert because condition required Description = Test Description, but it doesn't");
	}


	@Test
	public void testUpdateModifyCondition() {
		SystemTable table = this.systemTableDAO.findByPrimaryKey(MathUtils.SHORT_ONE);
		Assertions.assertEquals("SystemHierarchyCategory", table.getName());
		table.setUpdateEntityCondition(this.systemConditionService.getSystemCondition(1));
		this.systemSchemaService.saveSystemTable(table);

		SystemHierarchyCategory category = new SystemHierarchyCategory();
		category.setName("Test Category");
		category.setDescription("This is a test");
		this.systemHierarchyCategoryDAO.save(category);

		// Test Successful (Description = Test Description)
		category.setDescription("Test Description");
		this.systemHierarchyCategoryDAO.save(category);

		// Test Failure (Description != Test Description)
		category.setDescription("Test Description FAILURE");
		try {
			this.systemHierarchyCategoryDAO.save(category);
		}
		catch (Exception e) {
			// Expected Exception - Remove the Condition & Re-save
			table.setUpdateEntityCondition(null);
			this.systemSchemaService.saveSystemTable(table);
			this.systemHierarchyCategoryDAO.save(category);
			return;
		}
		throw new RuntimeException("Expected error on update because condition required Description = Test Description, but it doesn't");
	}


	@Test
	public void testDeleteModifyCondition() {
		SystemTable table = this.systemTableDAO.findByPrimaryKey(MathUtils.SHORT_ONE);
		Assertions.assertEquals("SystemHierarchyCategory", table.getName());
		table.setDeleteEntityCondition(this.systemConditionService.getSystemCondition(1));
		this.systemSchemaService.saveSystemTable(table);

		SystemHierarchyCategory category = new SystemHierarchyCategory();
		category.setName("Test Category");
		category.setDescription("Test Description");
		this.systemHierarchyCategoryDAO.save(category);

		// Test Successful (Description = Test Description)
		this.systemHierarchyCategoryDAO.delete(category.getId());

		// Test Failure (Description != Test Description)
		SystemHierarchyCategory category2 = new SystemHierarchyCategory();
		category2.setName("Test Category");
		category2.setDescription("Test Description FAILURE");
		this.systemHierarchyCategoryDAO.save(category2);

		try {
			this.systemHierarchyCategoryDAO.delete(category2.getId());
		}
		catch (Exception e) {
			// Expected Exception - Remove the Condition & Re-save
			table.setDeleteEntityCondition(null);
			this.systemSchemaService.saveSystemTable(table);
			this.systemHierarchyCategoryDAO.delete(category2.getId());
			return;
		}
		throw new RuntimeException("Expected error on delete because condition required Description = Test Description, but it doesn't");
	}
}
