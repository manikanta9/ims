package com.clifton.system.schema;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>SystemSchemaServiceImplTests</code> ...
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SystemSchemaServiceImplTests {

	@Resource
	private SystemSchemaService systemSchemaService;

	private static final String OUR_TABLE_1 = "OurTable1";
	private static final String EXTERNAL_TABLE_1 = "ExternalTable1";

	private static final String SAME_TABLE_NAME = "DuplicateTable";


	@Test
	public void testGetTableByName() {
		SystemTable tbl = this.systemSchemaService.getSystemTableByName(OUR_TABLE_1);
		Assertions.assertNotNull(tbl);
		Assertions.assertTrue(tbl.getDataSource().isDefaultDataSource());

		tbl = this.systemSchemaService.getSystemTableByName(EXTERNAL_TABLE_1);
		Assertions.assertNotNull(tbl);
		Assertions.assertFalse(tbl.getDataSource().isDefaultDataSource());
	}


	@Test
	public void testGetTableByName_SameTableName_MultipleDataSources() {
		// Same table name used across data sources (common for System, Security, Batch, Workflow, etc. type tables where those components are used across applications)
		String tableName = "SameTable";

		SystemTable table = this.systemSchemaService.getSystemTableByName(tableName);
		Assertions.assertTrue(table.getDataSource().isDefaultDataSource(), "Expected Same Table used across data sources to return the default datasource table version.");

		// Now, update the SameTable in a second data source
		SystemTable table2 = this.systemSchemaService.getSystemTableByNameAndDataSource("AnotherDataSource", tableName);
		Assertions.assertFalse(table2.getDataSource().isDefaultDataSource(), "Expected explicit retrieval by second datasource to return the non-default datasource.");
		table2.setDescription(table2.getDescription() + " Test Update");
		this.systemSchemaService.saveSystemTable(table2);

		// By name should still return the default datasource table version
		table = this.systemSchemaService.getSystemTableByName(tableName);
		Assertions.assertTrue(table.getDataSource().isDefaultDataSource(), "Expected Same Table used across data sources to return the default datasource table version.");

		// Now ADD a new table for the 3rd datasource
		SystemTable table3 = BeanUtils.cloneBean(table, false, false);
		table3.setId(null);
		table3.setDataSource(this.systemSchemaService.getSystemDataSource((short) 3));
		this.systemSchemaService.saveSystemTable(table3);

		// Again pull by name, should still get the default datasource
		table = this.systemSchemaService.getSystemTableByName(tableName);
		Assertions.assertTrue(table.getDataSource().isDefaultDataSource(), "Expected Same Table used across data sources to return the default datasource table version.");
	}


	@Test
	public void testGetTableByName_Missing() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			this.systemSchemaService.getSystemTableByName("MissingTable");
		});
		Assertions.assertEquals("Unable to find System Table with properties [name] and value(s) [MissingTable]", ve.getMessage());
	}


	@Test
	public void testGetTableByName_Multiple_FromOurAndExternal() {
		SystemTable table = this.systemSchemaService.getSystemTableByName(SAME_TABLE_NAME);
		Assertions.assertNotNull(table);
	}


	@Test
	public void testGetTableByName_Multiple_FromOurDataSource() {
		Exception ise = Assertions.assertThrows(IllegalStateException.class, () -> {
			this.systemSchemaService.getSystemTableByName("InvalidDuplicateTable");
		});
		Assertions.assertEquals("The query for SystemTable with parameters (name, dataSource.defaultDataSource) and values (InvalidDuplicateTable, true) cannot return more than 1 row; count = 2", ise.getMessage());
	}


	@Test
	public void testGetTableByName_Multiple_FromDifferentExternal() {
		Exception ise = Assertions.assertThrows(IllegalStateException.class, () -> {
			this.systemSchemaService.getSystemTableByName("RealDuplicateTable");
		});
		Assertions.assertEquals("The query for SystemTable with parameters (name) and values (RealDuplicateTable) cannot return more than 1 row; count = 2", ise.getMessage());
	}
}
