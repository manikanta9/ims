package com.clifton.system.schema;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


/**
 * This class tests the use of JSON objects for schema migrations
 *
 * @author stevenf
 */
public class SystemSchemaJsonMigrationTests {

	@Disabled
	@Test
	public void testJsonMigration() {
		//XmlMigrationHelper xmlHelper = new XmlMigrationHelper();
		//Schema schema = xmlHelper.loadFromXml("test/java/com/clifton/system/schema/json/migration/0001-json-migration-test.xml");
		//TODO - finish tests after refactoring migration code.
	}
}
