package com.clifton.system;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemProjectBasicTests extends BasicProjectTests {


	@Override
	public String getProjectPrefix() {
		return "system";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		// NOTE: refactor DB calls into CORE API?
		imports.add("java.sql.DataTruncation");
		imports.add("java.sql.SQLException");
		imports.add("java.sql.Types");
		imports.add("java.text.NumberFormat");
		imports.add("org.apache.poi.");
		imports.add("org.hibernate.exception.");
		imports.add("org.springframework.beans.factory.NoSuchBeanDefinitionException");
		imports.add("org.springframework.beans.factory.config.AutowireCapableBeanFactory");
		imports.add("org.springframework.jdbc.core.ResultSetExtractor");
		imports.add("freemarker.core.Environment");
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("sizehistory");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("saveSystemBean");
		skipMethods.add("saveSystemColumnDatedValue");
		skipMethods.add("getSystemColumnValueList");

		skipMethods.add("saveSystemCondition");
		skipMethods.add("deleteSystemCondition");
		skipMethods.add("saveSystemConditionEntry");
		skipMethods.add("deleteSystemConditionEntry");

		skipMethods.add("getSystemListItemList");

		if (skipMethods.contains(method.getName())) {
			return true;
		}
		return super.isMethodSkipped(method);
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		//TODO - add return type and secureMethod annotation no longer necessary; generic return types?
		ignoredVoidSaveMethodSet.add("saveSystemListEntity");
		ignoredVoidSaveMethodSet.add("saveSystemListQuery");
		ignoredVoidSaveMethodSet.add("saveSystemColumnCustom");
		ignoredVoidSaveMethodSet.add("saveSystemColumnStandard");
		return ignoredVoidSaveMethodSet;
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
