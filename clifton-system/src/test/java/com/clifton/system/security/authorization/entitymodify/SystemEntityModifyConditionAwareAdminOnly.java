package com.clifton.system.security.authorization.entitymodify;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;


public class SystemEntityModifyConditionAwareAdminOnly extends NamedEntity<Short> implements SystemEntityModifyConditionAwareAdminRequired {

	private SystemCondition entityModifyCondition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
