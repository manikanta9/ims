package com.clifton.system.security.authorization.entitymodify;


import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.condition.SystemConditionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>SystemEntityModifyConditionAwareTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemEntityModifyConditionAwareTests {

	private static final int CONDITION_NAME_EQUALS_TEST_NAME = 2;

	@Resource
	private UpdatableDAO<SystemEntityModifyConditionAwareAdminOnly> systemEntityModifyConditionAwareAdminOnlyDAO;

	@Resource
	private UpdatableDAO<SystemEntityModifyConditionAwareNonAdmin> systemEntityModifyConditionAwareNonAdminDAO;

	@Resource
	private SystemConditionService systemConditionService;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;


	/////////////////////////////////////////////////////////////////


	@Test
	public void testInsertUpdateDelete_NoCondition_AdminUsers() {
		setupTest(true);
		SystemEntityModifyConditionAwareAdminOnly bean = new SystemEntityModifyConditionAwareAdminOnly();
		bean.setName("Test Name");
		// Insert
		this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);

		// Update
		bean.setDescription("Updating the description.");
		this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);

		// Delete
		this.systemEntityModifyConditionAwareAdminOnlyDAO.delete(bean.getId());
	}


	@Test
	public void testInsertUpdateDelete_NoCondition_NonAdminAllowed() {
		setupTest(false);
		SystemEntityModifyConditionAwareNonAdmin bean = new SystemEntityModifyConditionAwareNonAdmin();
		bean.setName("Test Name");
		// Insert
		this.systemEntityModifyConditionAwareNonAdminDAO.save(bean);

		// Update
		bean.setDescription("Updating the description.");
		this.systemEntityModifyConditionAwareNonAdminDAO.save(bean);

		// Delete
		this.systemEntityModifyConditionAwareNonAdminDAO.delete(bean.getId());
	}


	@Test
	public void testInsert_NoCondition_NonAdminNotAllowed() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			setupTest(false);
			SystemEntityModifyConditionAwareAdminOnly bean = new SystemEntityModifyConditionAwareAdminOnly();
			bean.setName("Test");
			this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);
		});
		Assertions.assertEquals("You must be an admin user to INSERT this SystemEntityModifyConditionAwareAdminOnly.  Modify Condition has not been selected to validate additional security, but is required for Non Admin Users.", ve.getMessage());
	}


	@Test
	public void testUpdate_NoCondition_NonAdminNotAllowed() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true, false);
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemEntityModifyConditionAwareAdminOnly bean = new SystemEntityModifyConditionAwareAdminOnly();
			bean.setName("Test Name");
			// Insert OK
			this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);

			// Update - Fails because not an admin and no condition
			bean.setName("Test Name Doesn't Equal");
			this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);
		});
		Assertions.assertEquals("You must be an admin user to UPDATE this SystemEntityModifyConditionAwareAdminOnly.  Modify Condition has not been selected to validate additional security, but is required for Non Admin Users.", ve.getMessage());
	}


	@Test
	public void testDelete_NoCondition_NonAdminNotAllowed() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true, false);
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemEntityModifyConditionAwareAdminOnly bean = new SystemEntityModifyConditionAwareAdminOnly();
			bean.setName("Test ");
			// Insert OK because an admin
			this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);

			// Delete - Fails because not an admin and no condition
			bean.setName("Test Name Doesn't Equal");
			this.systemEntityModifyConditionAwareAdminOnlyDAO.delete(bean.getId());
		});
		Assertions.assertEquals("You must be an admin user to DELETE this SystemEntityModifyConditionAwareAdminOnly.  Modify Condition has not been selected to validate additional security, but is required for Non Admin Users.", ve.getMessage());
	}


	/////////////////////////////////////////////////////////////////


	@Test
	public void testInsertUpdateDelete_WithCondition_True() {
		setupTest(false);
		SystemEntityModifyConditionAwareAdminOnly bean = new SystemEntityModifyConditionAwareAdminOnly();
		bean.setEntityModifyCondition(this.systemConditionService.getSystemCondition(CONDITION_NAME_EQUALS_TEST_NAME));
		bean.setName("Test Name");
		// Insert
		this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);

		// Update
		bean.setDescription("Updating the description.");
		this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);

		// Delete
		this.systemEntityModifyConditionAwareAdminOnlyDAO.delete(bean.getId());
	}


	@Test
	public void testInsert_WithCondition_False() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			setupTest(false);
			SystemEntityModifyConditionAwareAdminOnly bean = new SystemEntityModifyConditionAwareAdminOnly();
			bean.setEntityModifyCondition(this.systemConditionService.getSystemCondition(CONDITION_NAME_EQUALS_TEST_NAME));
			bean.setName("Test Name Doesn't Equal");
			this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);
		});
		Assertions.assertEquals("You do not have permission to INSERT this SystemEntityModifyConditionAwareAdminOnly because: (name = Test Name Doesn't Equal instead of Test Name)", ve.getMessage());
	}


	@Test
	public void testUpdate_WithCondition_False() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			setupTest(false);
			SystemEntityModifyConditionAwareAdminOnly bean = new SystemEntityModifyConditionAwareAdminOnly();
			bean.setEntityModifyCondition(this.systemConditionService.getSystemCondition(CONDITION_NAME_EQUALS_TEST_NAME));
			bean.setName("Test Name");
			// Insert OK
			this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);

			// Update - Fails
			bean.setName("Test Name Doesn't Equal");
			this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);
		});
		Assertions.assertEquals("You do not have permission to UPDATE this SystemEntityModifyConditionAwareAdminOnly because: (name = Test Name Doesn't Equal instead of Test Name)", ve.getMessage());
	}


	@Test
	public void testDelete_WithCondition_False() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true, false);
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemEntityModifyConditionAwareAdminOnly bean = new SystemEntityModifyConditionAwareAdminOnly();
			bean.setEntityModifyCondition(this.systemConditionService.getSystemCondition(CONDITION_NAME_EQUALS_TEST_NAME));
			bean.setName("Test Name Doesn't Equal");
			// Insert OK because an admin
			this.systemEntityModifyConditionAwareAdminOnlyDAO.save(bean);

			// Update - Fails because not an admin
			bean.setName("Test Name Doesn't Equal");
			this.systemEntityModifyConditionAwareAdminOnlyDAO.delete(bean.getId());
		});
		Assertions.assertEquals("You do not have permission to DELETE this SystemEntityModifyConditionAwareAdminOnly because: (name = Test Name Doesn't Equal instead of Test Name)", ve.getMessage());
	}


	////////////////////////////////////////////////////


	private void setupTest(boolean adminUser) {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(adminUser);
	}
}
