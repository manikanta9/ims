package com.clifton.system.validation;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.DaoException;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.audit.auditor.AuditTestEntity;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.hibernate.exception.DataException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.DataTruncation;
import java.sql.SQLException;

;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class DataAccessExceptionConverterTests {

	@Resource
	private DataAccessExceptionConverter dataAccessExceptionConverter;


	@Test
	public void testConvertDuplicateKey() {
		SQLException from = new SQLException("Cannot insert duplicate key row in object 'dbo.SystemAuditType' with unique index 'ux_SystemAuditType_SystemAuditTypeName'.", "",
				DataAccessExceptionConverter.ERROR_CODE_DUPLICATE_KEY);
		RuntimeException to = this.dataAccessExceptionConverter.convert(new RuntimeException(from));
		Assertions.assertTrue(to instanceof ValidationException);
		Assertions.assertEquals("The field 'System Audit Type Name' does not allow duplicate values.", to.getMessage());
	}


	@Test
	public void testConvertDuplicateCompositeKey() {
		SQLException from = new SQLException("Cannot insert duplicate key row in object 'dbo.SystemColumn' with unique index 'ux_SystemColumn_SystemColumnName_SystemTableID'.", "",
				DataAccessExceptionConverter.ERROR_CODE_DUPLICATE_KEY);
		RuntimeException to = this.dataAccessExceptionConverter.convert(new RuntimeException(from));
		Assertions.assertTrue(to instanceof ValidationException);
		Assertions.assertEquals("The field combination 'System Column Name', 'System Table ID' does not allow duplicate values.", to.getMessage());
	}


	@Test
	public void testConvertReferenceConstraint() {
		SQLException from = new SQLException(
				"The DELETE statement conflicted with the REFERENCE constraint \"FK_SystemTable_SystemAuditType_SystemAuditTypeID\". The conflict occurred in database \"CliftonIMS\", table \"dbo.SystemTable\", column 'SystemAuditTypeID'.",
				"", DataAccessExceptionConverter.ERROR_CODE_REFERENCE_CONSTRAINT);
		RuntimeException to = this.dataAccessExceptionConverter.convert(new RuntimeException(from));
		Assertions.assertTrue(to instanceof ValidationException);
		Assertions.assertEquals("Cannot delete selected System Audit Type row because it is referenced by 'System Table' row(s).", to.getMessage());
	}


	@Test
	public void testConvertNutNull() {
		SQLException cause = new SQLException("Cannot insert the value NULL into column 'BeanPropertyName', table 'CliftonIMS.dbo.SystemColumn'; column does not allow nulls. INSERT fails.", "",
				DataAccessExceptionConverter.ERROR_CODE_NOT_NULL);
		SystemColumnStandard dto = new SystemColumnStandard();
		dto.setName("my test column");
		RuntimeException from = new DaoException("Error updating something", dto, cause);

		RuntimeException to = this.dataAccessExceptionConverter.convert(from);
		Assertions.assertTrue(to instanceof FieldValidationException, "converted exception must be an instance of FieldValidationException");
		Assertions.assertEquals(((FieldValidationException) to).getFieldName(), "beanPropertyName");
		Assertions.assertEquals("The field 'beanPropertyName' for 'my test column' is required.", to.getMessage());
	}


	@Test
	public void testConvertOther() {
		RuntimeException to = this.dataAccessExceptionConverter.convert(new RuntimeException("Test Exception"));
		Assertions.assertEquals("Test Exception", to.getMessage());

		SQLException from = new SQLException("SQL ERROR", "", -1111);
		to = this.dataAccessExceptionConverter.convert(new RuntimeException("Wrapper", from));
		Assertions.assertEquals(RuntimeException.class, to.getClass());
		Assertions.assertEquals("Unhandled SQL error code: -1111", to.getMessage());
	}


	@Test
	public void testConvertDataTruncationFromRuntimeException() {
		DataTruncation dataTruncation = new DataTruncation(0, true, false, 10, 10);
		RuntimeException to = this.dataAccessExceptionConverter.convert(new RuntimeException("Error updating SystemHierarchy", dataTruncation));
		// Expected to return as ValidationException (Original Error is manually logged)
		Assertions.assertEquals(ValidationException.class, to.getClass());
		Assertions.assertEquals("Error updating SystemHierarchy: Data truncation Unable to determine specific field that exceeds max length.", to.getMessage());
	}


	@Test
	public void testConvertDataTruncationFromDataException() {
		DataTruncation dataTruncation = new DataTruncation(0, true, false, 10, 10);
		RuntimeException to = this.dataAccessExceptionConverter.convert(new DataException("Error updating SystemHierarchy", dataTruncation,
				"UPDATE SystemHierarchy SET SystemHierarchyName=?,SystemHierarchyDescription=?,..."));
		// Expected to return as ValidationException (Original Error is manually logged)
		Assertions.assertEquals(ValidationException.class, to.getClass());
		Assertions.assertEquals("Error updating SystemHierarchy: Data truncation Unable to determine specific field that exceeds max length.", to.getMessage());
	}


	@Test
	public void testConvertDataTruncationFromDaoException_NotFound() {
		SystemHierarchyCategory cat = new SystemHierarchyCategory();
		cat.setId(MathUtils.SHORT_ONE);
		cat.setName("Category 1");
		SystemHierarchy hierarchy = new SystemHierarchy();
		hierarchy.setName("Hierarchy 1");
		hierarchy.setCategory(cat);
		hierarchy.setDescription("This is a test hierarchy that doesn't have anything that can be found for data truncation exception");

		RuntimeException to = this.dataAccessExceptionConverter.convert(createFakeExceptionForDataTruncation(hierarchy));
		// Expected to return as ValidationException (Original Error is manually logged)
		Assertions.assertEquals(ValidationException.class, to.getClass());
		Assertions.assertEquals("Error saving SystemHierarchy: Data truncation Unable to find a specific field that exceeds max length.", to.getMessage());
	}


	@Test
	public void testConvertDataTruncationFromDaoException_String() {
		SystemHierarchyCategory cat = new SystemHierarchyCategory();
		cat.setId(MathUtils.SHORT_ONE);
		cat.setName("Category 1");
		SystemHierarchy hierarchy = new SystemHierarchy();
		hierarchy.setName("First Hierarchy Level 1 - Test This Name Is Way Too Long!!!! And now must exceed 100 characters!!!!!!!");
		hierarchy.setCategory(cat);
		hierarchy.setDescription("This is a test hierarchy that has name that exceeds max length of 100");

		RuntimeException to = this.dataAccessExceptionConverter.convert(createFakeExceptionForDataTruncation(hierarchy));
		// Expected to return as ValidationException (Original Error is manually logged)
		Assertions.assertEquals(ValidationException.class, to.getClass());
		Assertions.assertEquals(
				to.getMessage(), "Error saving SystemHierarchy: Data truncation SystemHierarchyName has max length of 100. Value length 102 is too long. [First Hierarchy Level 1 - Test This Name Is Way Too Long!!!! And now must exceed 100 characters!!!!!!!]"
						+ StringUtils.NEW_LINE);
	}


	@Test
	public void testConvertDataTruncationFromDaoException_Decimal() {
		AuditTestEntity testEntity = new AuditTestEntity();
		testEntity.setName("Test Name");
		// 19,2
		testEntity.setTestDecimal(BigDecimal.valueOf(12345678901234567892.02145));
		// 9, 5
		testEntity.setTestPercent(BigDecimal.valueOf(99999.25));

		RuntimeException to = this.dataAccessExceptionConverter.convert(createFakeExceptionForDataTruncation(testEntity));
		// Expected to return as ValidationException (Original Error is manually logged)
		Assertions.assertEquals(ValidationException.class, to.getClass());
		Assertions.assertEquals(
				to.getMessage(),
				"Error saving AuditTestEntity: Data truncation TestDecimal integer portion has max length of 13.  Value with integer portion length 20 is too large. [12345678901234567000]"
						+ StringUtils.NEW_LINE + "TestPercent integer portion has max length of 4.  Value with integer portion length 5 is too large. [99999.25]" + StringUtils.NEW_LINE);
	}


	@Test
	public void testConvertDataTruncationFromDaoException_StringAndDecimal() {
		AuditTestEntity testEntity = new AuditTestEntity();
		testEntity.setName("Test Name 1 - This Name is Too Long for the Field.......");
		// 19,2
		testEntity.setTestDecimal(BigDecimal.valueOf(12345678901234567892.02145));
		// 9, 5
		testEntity.setTestPercent(BigDecimal.valueOf(99999.25));

		RuntimeException to = this.dataAccessExceptionConverter.convert(createFakeExceptionForDataTruncation(testEntity));
		// Expected to return as ValidationException (Original Error is manually logged)
		Assertions.assertEquals(ValidationException.class, to.getClass());
		Assertions.assertEquals(to.getMessage(), "Error saving AuditTestEntity: Data truncation TestName has max length of 50. Value length 56 is too long. [Test Name 1 - This Name is Too Long for the Field.......]"
				+ StringUtils.NEW_LINE + "TestDecimal integer portion has max length of 13.  Value with integer portion length 20 is too large. [12345678901234567000]" + StringUtils.NEW_LINE
				+ "TestPercent integer portion has max length of 4.  Value with integer portion length 5 is too large. [99999.25]" + StringUtils.NEW_LINE);
	}


	private DaoException createFakeExceptionForDataTruncation(IdentityObject dto) {
		// Don't care about parameters - mean nothing for finding what's wrong
		DataTruncation dataTruncation = new DataTruncation(0, true, false, 10, 10);
		return new DaoException("Error saving " + dto.getClass().getSimpleName(), dto, dataTruncation);
	}
}
