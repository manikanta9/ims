package com.clifton.system.list;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DataTableToExcelFileConverter;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.list.search.SystemListItemSearchForm;
import com.clifton.system.list.search.SystemListSearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.upload.SystemUploadCommand;
import com.clifton.system.upload.SystemUploadHandler;
import com.clifton.system.upload.SystemUploadService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.sql.Types;
import java.util.List;
import java.util.Set;


/**
 * The <code>SystemListServiceImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemListServiceImplTests<T extends SystemList> {

	@Resource
	private SystemListService<T> systemListService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator;

	@Resource
	private DataTableRetrievalHandler dataTableRetrievalHandler;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	@Resource
	private SystemUploadHandler systemUploadHandler;

	@Resource
	private SystemUploadService systemUploadService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
	}


	@Test
	public void testGetSystemListList() {
		List<T> list = this.systemListService.getSystemListList(new SystemListSearchForm());
		Assertions.assertEquals(4, CollectionUtils.getSize(list));
	}


	@Test
	public void testSaveSystemListBean() {
		// Try to edit name of nameSystemDefined methods
		SystemList listEntity = this.systemListService.getSystemList(1);
		Assertions.assertEquals("States", listEntity.getName());
		Assertions.assertTrue(listEntity.isNameSystemDefined());
		listEntity.setName("States - Updated");
		saveSystemList(listEntity, true);

		listEntity = this.systemListService.getSystemList(3);
		Assertions.assertEquals("Users", listEntity.getName());
		Assertions.assertTrue(listEntity.isNameSystemDefined());
		listEntity.setName("Users - Updated");
		saveSystemList(listEntity, true);

		// Try to edit the query of a list system defined list
		listEntity.setName("Users");
		Assertions.assertTrue(listEntity.isListSystemDefined());
		((SystemListQuery) listEntity).setSqlStatement("SELECT SecurityUserName, SecurityUserName FROM SecurityUser");
		saveSystemList(listEntity, true);

		// Try to create a SystemListQuery object with no table
		SystemListQuery list = new SystemListQuery();
		list.setName("Test");
		saveSystemList(list, true);

		// Set the table, but the sql query is still required and not set so should fail
		list.setTable(this.systemSchemaService.getSystemTable(MathUtils.SHORT_ONE));
		saveSystemList(list, true);
	}


	private void saveSystemList(SystemList bean, boolean expectException) {
		try {
			if (bean instanceof SystemListEntity) {
				this.systemListService.saveSystemListEntity((SystemListEntity) bean);
			}
			else {
				this.systemListService.saveSystemListQuery((SystemListQuery) bean);
			}
		}
		catch (Exception e) {
			Assertions.assertTrue(expectException, "Did NOT expect an exception to be thrown during save, but there was.");
			return;
		}
		Assertions.assertFalse(expectException, "Expected an exception to be thrown during save, but there was not.");
	}


	@Test
	public void testGetSystemListItems() {
		Mockito.when(this.dataTableRetrievalHandlerLocator.locate("dataSource")).thenReturn(this.dataTableRetrievalHandler);
		Mockito.when(this.dataTableRetrievalHandler.findDataTable(ArgumentMatchers.argThat(new SqlSelectCommandArgumentMatcher("SELECT SecurityUserID, SecurityUserName FROM SecurityUser")))).thenReturn(getUsersDataTableResults());

		List<SystemListItem> itemList = this.systemListService.getSystemListItemListByList(1);
		Assertions.assertEquals(5, CollectionUtils.getSize(itemList));

		itemList = this.systemListService.getSystemListItemListByList(2);
		Assertions.assertEquals(3, CollectionUtils.getSize(itemList));

		itemList = this.systemListService.getSystemListItemListByList(3);
		Assertions.assertEquals(3, CollectionUtils.getSize(itemList));
	}


	@Test
	public void testFindSystemListItems() {
		Mockito.when(this.dataTableRetrievalHandlerLocator.locate("dataSource")).thenReturn(this.dataTableRetrievalHandler);
		Mockito.when(this.dataTableRetrievalHandler.findDataTable(ArgumentMatchers.argThat(new SqlSelectCommandArgumentMatcher("SELECT SecurityUserID, SecurityUserName FROM SecurityUser")))).thenReturn(getUsersDataTableResults());

		SystemListItemSearchForm form = new SystemListItemSearchForm();
		form.setSearchPattern("1");
		form.setActive(true);
		form.setListName("Users");
		List<SystemListItem> itemList = this.systemListService.getSystemListItemList(form);
		Assertions.assertEquals(1, CollectionUtils.getSize(itemList));

		form.setSearchPattern("User");
		itemList = this.systemListService.getSystemListItemList(form);
		Assertions.assertEquals(3, CollectionUtils.getSize(itemList));

		form.setActive(null);
		itemList = this.systemListService.getSystemListItemList(form);
		Assertions.assertEquals(3, CollectionUtils.getSize(itemList));
	}


	private DataTable getUsersDataTableResults() {
		DataColumn[] columnList = new DataColumn[]{new DataColumnImpl("ID", Types.INTEGER), new DataColumnImpl("Name", Types.NVARCHAR)};
		DataTable dataTable = new PagingDataTableImpl(columnList);
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{1, "User1"}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{2, "User2"}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{3, "User3"}));
		return dataTable;
	}


	private DataTable getStringUsersDataTableResults() {
		DataColumn[] columnList = new DataColumn[]{new DataColumnImpl("ID", Types.NVARCHAR), new DataColumnImpl("Name", Types.NVARCHAR)};
		DataTable dataTable = new PagingDataTableImpl(columnList);
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"User1", "User1"}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"User2", "User2"}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"User3", "User3"}));
		return dataTable;
	}


	private DataTable getOneColumnUsersDataTableResults() {
		DataColumn[] columnList = new DataColumn[]{new DataColumnImpl("ID", Types.NVARCHAR)};
		DataTable dataTable = new PagingDataTableImpl(columnList);
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"User1"}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"User2"}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"User3"}));
		return dataTable;
	}


	private DataTable getFourColumnUsersDataTableResults() {
		DataColumn[] columnList = new DataColumn[]{new DataColumnImpl("ID", Types.NVARCHAR), new DataColumnImpl("Name", Types.NVARCHAR), new DataColumnImpl("Name-3", Types.NVARCHAR),
				new DataColumnImpl("Name-4", Types.NVARCHAR)};
		DataTable dataTable = new PagingDataTableImpl(columnList);
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"User1", "User1", "User1", "User1"}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"User2", "User2", "User2", "User2"}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"User3", "User3", "User3", "User3"}));
		return dataTable;
	}


	@Test
	public void testInsertSystemListQueryWithInvalidDataType() {
		// Try to insert a sql query list with data type integer, but sql that returns a
		// string as the value field.
		Mockito.when(this.dataTableRetrievalHandlerLocator.locate("dataSource")).thenReturn(this.dataTableRetrievalHandler);
		String sql = "SELECT SecurityUserName, SecurityUserName FROM SecurityUser";
		Mockito.when(this.dataTableRetrievalHandler.findDataTable(ArgumentMatchers.argThat(new SqlSelectCommandArgumentMatcher(sql)))).thenReturn(getStringUsersDataTableResults());

		SystemListQuery queryList = new SystemListQuery();
		queryList.setDataType(this.systemSchemaService.getSystemDataType(MathUtils.SHORT_TWO));
		queryList.setName("Test Invalid");
		queryList.setTable(this.systemSchemaService.getSystemTable(MathUtils.SHORT_ONE));
		queryList.setSqlStatement(sql);
		this.saveSystemList(queryList, true);

		// Change Data Type to String so it is now a value query for this list
		queryList.setDataType(this.systemSchemaService.getSystemDataType(MathUtils.SHORT_ONE));

		// Try to set system defined fields
		queryList.setNameSystemDefined(true);
		this.saveSystemList(queryList, true);

		queryList.setNameSystemDefined(false);
		queryList.setListSystemDefined(true);
		this.saveSystemList(queryList, true);

		queryList.setListSystemDefined(false);
		this.saveSystemList(queryList, false);

		// Now that the bean is validated and saved, try to change the DataType to an invalid data type
		queryList.setDataType(this.systemSchemaService.getSystemDataType(MathUtils.SHORT_TWO));
		this.saveSystemList(queryList, true);
		queryList = (SystemListQuery) this.systemListService.getSystemList(queryList.getId());

		// This item is not list system defined - try to change the sql (then change it back)
		queryList.setSqlStatement(sql + " ORDER BY SecurityUserName");
		this.saveSystemList(queryList, false);

		// Put the original sql back and
		// try to change the name.  Should be successful
		// since name is not system defined.
		queryList.setSqlStatement(sql);
		queryList.setName(queryList.getName() + " - Updated");
		this.saveSystemList(queryList, false);

		// Try adding an item for a query list
		SystemListItem item = new SystemListItem();
		item.setSystemList(queryList);
		item.setValue("USER_TEST");
		item.setText("USER_TEST");
		this.saveSystemListItem(item, true);

		// Now let's just delete the list
		this.systemListService.deleteSystemList(queryList.getId());
	}


	@Test
	public void testInsertSystemListQueryWithInvalidDataTableResults() {
		// Try to insert a sql query list that returns 1 column and then try again with 4 columns
		Mockito.when(this.dataTableRetrievalHandlerLocator.locate("dataSource")).thenReturn(this.dataTableRetrievalHandler);
		Mockito.when(this.dataTableRetrievalHandler.findDataTable(ArgumentMatchers.argThat(new SqlSelectCommandArgumentMatcher("SELECT SecurityUserName FROM SecurityUser")))).thenReturn(getOneColumnUsersDataTableResults());
		Mockito.when(this.dataTableRetrievalHandler.findDataTable(ArgumentMatchers.argThat(new SqlSelectCommandArgumentMatcher("SELECT SecurityUserName, SecurityUserName, SecurityUserName, SecurityUserName FROM SecurityUser")))).thenReturn(
				getFourColumnUsersDataTableResults());

		SystemListQuery queryList = new SystemListQuery();
		queryList.setDataType(this.systemSchemaService.getSystemDataType(MathUtils.SHORT_ONE));
		queryList.setName("Test Invalid");
		queryList.setTable(this.systemSchemaService.getSystemTable(MathUtils.SHORT_ONE));
		queryList.setSqlStatement("SELECT SecurityUserName FROM SecurityUser");
		this.saveSystemList(queryList, true);

		queryList.setSqlStatement("SELECT SecurityUserName, SecurityUserName, SecurityUserName, SecurityUserName FROM SecurityUser");
		this.saveSystemList(queryList, true);
	}


	@Test
	public void testDeleteSystemDefinedList() {
		Assertions.assertThrows(Exception.class, () ->
				this.systemListService.deleteSystemList(3)
		);
	}


	@Test
	public void testDeleteSystemDefinedName() {
		Assertions.assertThrows(Exception.class, () ->
				this.systemListService.deleteSystemList(2)
		);
	}


	@Test
	public void testEditSystemDefinedListItems() {
		// Cannot edit/add/delete any states
		SystemListItem item = new SystemListItem();
		item.setSystemList(this.systemListService.getSystemList(1));
		item.setValue("Test New State");
		item.setText("Test New State");
		this.saveSystemListItem(item, true);

		// CAN edit/add/delete Countries
		item = new SystemListItem();
		item.setSystemList(this.systemListService.getSystemList(2));
		item.setValue("Test New Country");
		item.setText("Test New Country");
		this.saveSystemListItem(item, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            SystemListItemUploadTests                       ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Test is to ensure there isn't an extra column for "SystemList" that is used only to instantiate a new SystemListEntity object
	 */
	@Test
	public void testDownloadSystemListItems_UploadSampleFile() {
		SystemUploadCommand uploadCommand = new SystemUploadCommand();
		uploadCommand.setTableName("SystemListItem");

		DataTable dataTable = this.systemUploadService.downloadSystemUploadFileSample(uploadCommand, false);
		Set<String> expectedColumns = CollectionUtils.createHashSet("SystemList-Name", "Value", "Text", "Tooltip", "Order", "Active");

		for (DataColumn column : dataTable.getColumnList()) {
			Assertions.assertTrue(expectedColumns.contains(column.getColumnName()), "Unexpected column " + column.getColumnName() + " found in the upload sample file.");
		}
		Assertions.assertEquals(expectedColumns.size(), dataTable.getColumnCount());
	}


	@Test
	public void testUploadSystemListItems_Successful() {
		DataTable dataTable = getDataTableForSystemListItemUpload();
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"Upload Test", "NJ", "New Jersey Update"}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"Upload Test", "Three", "Three"}));

		SystemUploadCommand uploadCommand = getSystemUploadCommandForSystemListItemDataTable(dataTable);
		this.systemUploadHandler.uploadSystemUploadFile(uploadCommand);
		System.out.println(uploadCommand.getUploadResultsString());
		Assertions.assertEquals("[SystemListItem]: 2 Records Inserted." + StringUtils.NEW_LINE + StringUtils.NEW_LINE +
				"There were no errors with the upload file.", uploadCommand.getUploadResultsString().trim());
	}


	@Test
	public void testUploadSystemListItems_MissingList() {
		DataTable dataTable = getDataTableForSystemListItemUpload();
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"Upload Test Do Not Exist", "Three", "Three"}));

		SystemUploadCommand uploadCommand = getSystemUploadCommandForSystemListItemDataTable(dataTable);
		this.systemUploadHandler.uploadSystemUploadFile(uploadCommand);
		System.out.println(uploadCommand.getUploadResultsString());
		Assertions.assertEquals("No rows were saved in the database." + StringUtils.NEW_LINE +
				"The following errors were encountered with the upload file: " + StringUtils.NEW_LINE +
				"[Bean_0]: Unable to lookup [SystemList] by natural key(s) [name] values [Upload Test Do Not Exist].", uploadCommand.getUploadResultsString().trim());
	}


	@Test
	public void testUploadSystemListItems_SystemDefinedList() {
		DataTable dataTable = getDataTableForSystemListItemUpload();
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{"Users", "Mary", "Mary"}));

		SystemUploadCommand uploadCommand = getSystemUploadCommandForSystemListItemDataTable(dataTable);

		TestUtils.expectException(ValidationException.class, () -> {
			this.systemUploadHandler.uploadSystemUploadFile(uploadCommand);
		}, "Cannot edit list items for list [Users] as it is marked as list system defined.");
	}


	private DataTable getDataTableForSystemListItemUpload() {
		DataColumn[] columnList = new DataColumn[]{new DataColumnImpl("SystemList-Name", Types.NVARCHAR), new DataColumnImpl("Value", java.sql.Types.NVARCHAR), new DataColumnImpl("Text", Types.VARCHAR)};
		return new PagingDataTableImpl(columnList);
	}


	private SystemUploadCommand getSystemUploadCommandForSystemListItemDataTable(DataTable dataTable) {
		SystemUploadCommand uploadCommand = new SystemUploadCommand();
		uploadCommand.setTableName("SystemListItem");
		DataTableToExcelFileConverter dataFileConverter = new DataTableToExcelFileConverter("xls");
		DataTableFileConfig fileConfig = new DataTableFileConfig(dataTable);
		try {
			uploadCommand.setFile(new MultipartFileImpl(dataFileConverter.convert(fileConfig)));
		}
		catch (Throwable e) {
			throw new RuntimeException("Error setting upload file: " + e.getMessage(), e);
		}
		return uploadCommand;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void saveSystemListItem(SystemListItem bean, boolean expectException) {
		try {
			this.systemListService.saveSystemListItem(bean);
		}
		catch (Exception e) {
			Assertions.assertTrue(expectException, "Did NOT expect an exception to be thrown during save, but there was.");
			return;
		}
		Assertions.assertFalse(expectException, "Expected an exception to be thrown during save, but there was not.");
	}


	static class SqlSelectCommandArgumentMatcher implements ArgumentMatcher<SqlSelectCommand> {

		private final String sql;


		SqlSelectCommandArgumentMatcher(String sql) {
			this.sql = sql;
		}


		@Override
		public boolean matches(SqlSelectCommand command) {
			if (command == null) {
				return false;
			}
			return this.sql.equals(command.getSql());
		}
	}
}
