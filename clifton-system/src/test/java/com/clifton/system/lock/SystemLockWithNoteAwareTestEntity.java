package com.clifton.system.lock;

import com.clifton.core.beans.NamedEntity;
import com.clifton.security.user.SecurityUser;


/**
 * @author manderson
 */
public class SystemLockWithNoteAwareTestEntity extends NamedEntity<Integer> implements SystemLockWithNoteAware {

	private SecurityUser lockedByUser;

	private String lockNote;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityUser getLockedByUser() {
		return this.lockedByUser;
	}


	@Override
	public void setLockedByUser(SecurityUser lockedByUser) {
		this.lockedByUser = lockedByUser;
	}


	@Override
	public String getLockNote() {
		return this.lockNote;
	}


	@Override
	public void setLockNote(String lockNote) {
		this.lockNote = lockNote;
	}
}
