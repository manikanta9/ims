package com.clifton.system.lock;

import com.clifton.core.beans.NamedEntity;
import com.clifton.security.user.SecurityUser;


/**
 * @author manderson
 */
public class SystemLockAwareTestEntity extends NamedEntity<Integer> implements SystemLockAware {


	private SecurityUser lockedByUser;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityUser getLockedByUser() {
		return this.lockedByUser;
	}


	@Override
	public void setLockedByUser(SecurityUser lockedByUser) {
		this.lockedByUser = lockedByUser;
	}
}
