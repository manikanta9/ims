package com.clifton.system.lock;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.xml.XmlUpdatableDAO;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.function.Function;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SystemLockServiceImplTests {

	private static final String TEST_ENTITY_TABLE_NAME = "SystemLockAwareTestEntity";
	private static final String TEST_ENTITY_WITH_NOTE_TABLE_NAME = "SystemLockWithNoteAwareTestEntity";

	private static final Short TEST_USER_1 = 1;
	private static final Short TEST_USER_2 = 2;

	private static final Integer TEST_ENTITY_1 = 1;
	private static final Integer TEST_ENTITY_2 = 2;
	private static final Integer TEST_ENTITY_3 = 3;

	private static final Integer TEST_ENTITY_WITH_NOTE_1 = 1;
	private static final Integer TEST_ENTITY_WITH_NOTE_2 = 2;
	private static final Integer TEST_ENTITY_WITH_NOTE_3 = 3;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private ReadOnlyDAO<SecurityUser> securityUserDAO;

	@Resource
	private SystemLockService systemLockService;

	@Resource
	private XmlUpdatableDAO<SystemLockAwareTestEntity> systemLockAwareTestEntityDAO;

	@Resource
	private XmlUpdatableDAO<SystemLockWithNoteAwareTestEntity> systemLockWithNoteAwareTestEntityDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testLockUnlockEntity_SameUser() {
		SystemLockAwareTestEntity entity = lockEntity(TEST_ENTITY_1, true, false, TEST_USER_1, null);
		// Update the entity (by the Locked By User)
		entity.setDescription("Test Update Description");
		this.systemLockAwareTestEntityDAO.save(entity);

		// Unlock the entity
		this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_TABLE_NAME, TEST_ENTITY_1);
	}


	@Test
	public void testLockUnlockEntity_RequireNote_SameUser() {
		SystemLockWithNoteAwareTestEntity entity = lockEntityWithNote(TEST_ENTITY_WITH_NOTE_1, true, false, TEST_USER_1, null, "I want to lock it!");
		// Update the entity (by the Locked By User)
		entity.setDescription("Test Update Description");
		this.systemLockWithNoteAwareTestEntityDAO.save(entity);

		// Unlock the entity
		this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_WITH_NOTE_TABLE_NAME, TEST_ENTITY_WITH_NOTE_1);

		// Validate Lock Note is empty
		entity = getSystemLockWithNoteAwareTestEntity(TEST_ENTITY_WITH_NOTE_1);
		Assertions.assertNull(entity.getLockNote(), "Lock note should be cleared when entity is unlocked.");
	}


	@Test
	public void testLockEntity_RequireNote_MissingNote() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			this.systemLockService.lockSystemLockAwareEntity(TEST_ENTITY_WITH_NOTE_TABLE_NAME, TEST_ENTITY_WITH_NOTE_1, null);
		});
		Assertions.assertEquals("A lock note is required when locking this SystemLockWithNoteAwareTestEntity", ve.getMessage());
		// Not locked - nothing to unlock
	}


	@Test
	public void testLockEntity_RequireNote_UpdateNote() {
		lockEntityWithNote(TEST_ENTITY_WITH_NOTE_1, true, false, TEST_USER_1, null, "I want to lock it!");
		// Same user can re-call lock and update the lock note
		lockEntityWithNote(TEST_ENTITY_WITH_NOTE_1, false, false, TEST_USER_1, null, "I want to lock it! UPDATE ME!");

		SystemLockWithNoteAwareTestEntity entity = getSystemLockWithNoteAwareTestEntity(TEST_ENTITY_WITH_NOTE_1);
		Assertions.assertEquals(TEST_USER_1, entity.getLockedByUser().getId());
		Assertions.assertEquals("I want to lock it! UPDATE ME!", entity.getLockNote());

		// Unlock the entity
		this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_WITH_NOTE_TABLE_NAME, TEST_ENTITY_WITH_NOTE_1);

		// Validate Lock Note is empty
		entity = getSystemLockWithNoteAwareTestEntity(TEST_ENTITY_WITH_NOTE_1);
		Assertions.assertNull(entity.getLockNote(), "Lock note should be cleared when entity is unlocked.");
	}


	@Test
	public void testLockAlreadyLockedEntity() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			lockEntity(TEST_ENTITY_2, true, false, TEST_USER_1, TEST_USER_2);
			this.systemLockService.lockSystemLockAwareEntity(TEST_ENTITY_TABLE_NAME, TEST_ENTITY_2, null);
		});
		Assertions.assertEquals("Cannot lock [Test Entity #2] because: it is currently locked by Test User #1.", ve.getMessage());

		// Clean up for test Unlock the entity
		setCurrentUser(TEST_USER_1);
		this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_TABLE_NAME, TEST_ENTITY_2);
	}


	@Test
	public void testUpdateLockedEntity() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemLockAwareTestEntity entity = lockEntity(TEST_ENTITY_1, true, false, TEST_USER_1, TEST_USER_2);

			entity.setDescription("Test Update Description by someone else");
			this.systemLockAwareTestEntityDAO.save(entity);
		});
		Assertions.assertEquals("Cannot update [Test Entity #1] because: it is currently locked by Test User #1.", ve.getMessage());

		// Clean up for test Unlock the entity
		setCurrentUser(TEST_USER_1);
		this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_TABLE_NAME, TEST_ENTITY_1);
	}


	@Test
	public void testDeleteLockedEntity() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemLockAwareTestEntity entity = lockEntity(TEST_ENTITY_3, true, false, TEST_USER_1, TEST_USER_2);
			this.systemLockAwareTestEntityDAO.delete(entity.getId());
		});
		Assertions.assertEquals("Cannot delete [Test Entity #3] because: it is currently locked by Test User #1.", ve.getMessage());

		// Clean up for test Unlock the entity
		setCurrentUser(TEST_USER_1);
		this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_TABLE_NAME, TEST_ENTITY_3);
	}


	@Test
	public void testUnlockLockedEntity_DifferentUser() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			lockEntity(TEST_ENTITY_1, true, false, TEST_USER_1, TEST_USER_2);
			// Try to unlock - fails because second user is not an admin
			this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_TABLE_NAME, TEST_ENTITY_1);
		});
		Assertions.assertEquals("Cannot unlock [Test Entity #1] because: it is currently locked by Test User #1.", ve.getMessage());

		// Clean up for test Unlock the entity
		setCurrentUser(TEST_USER_1);
		this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_TABLE_NAME, TEST_ENTITY_1);
	}


	@Test
	public void testUnlockLockedEntity_AdminUser() {
		lockEntity(TEST_ENTITY_1, true, true, TEST_USER_1, TEST_USER_2);
		// Try to unlock - Should be OK because Admins can unlock other user's entities
		this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_TABLE_NAME, TEST_ENTITY_1);
	}


	@Test
	public void testUpdateLockedEntity_AdminUser() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			SystemLockAwareTestEntity entity = lockEntity(TEST_ENTITY_1, true, true, TEST_USER_1, TEST_USER_2);

			// Try to Update - Should fail because even admins can't update locked entity
			entity.setDescription("Test Update Description by someone else");
			this.systemLockAwareTestEntityDAO.save(entity);
		});
		Assertions.assertEquals("Cannot update [Test Entity #1] because: it is currently locked by Test User #1.", ve.getMessage());

		// Clean up for test Unlock the entity
		setCurrentUser(TEST_USER_1);
		this.systemLockService.unlockSystemLockAwareEntity(TEST_ENTITY_TABLE_NAME, TEST_ENTITY_1);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Helper method for testing that performs initial locking (and set current user for locking) and also setting up mock return results for admin.  After locking is performed, current user can be changed by passing afterLockUserId
	 */
	private SystemLockAwareTestEntity lockEntity(Integer id, boolean verify, boolean admin, Short lockUserId, Short afterLockUserId) {
		return lockEntityImpl(TEST_ENTITY_TABLE_NAME, id, this::getSystemLockAwareTestEntity, verify, admin, lockUserId, afterLockUserId, null);
	}


	/**
	 * Helper method for testing that performs initial locking (and set current user for locking) and also setting up mock return results for admin.  After locking is performed, current user can be changed by passing afterLockUserId
	 */
	private SystemLockWithNoteAwareTestEntity lockEntityWithNote(Integer id, boolean verify, boolean admin, Short lockUserId, Short afterLockUserId, String lockNote) {
		return lockEntityImpl(TEST_ENTITY_WITH_NOTE_TABLE_NAME, id, this::getSystemLockWithNoteAwareTestEntity, verify, admin, lockUserId, afterLockUserId, lockNote);
	}


	private <T extends SystemLockAware> T lockEntityImpl(String tableName, Integer id, Function<Integer, T> lockAwareEntityRetriever, boolean verify, boolean admin, Short lockUserId, Short afterLockUserId, String lockNote) {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(admin);

		// Set Current User
		setCurrentUser(lockUserId);

		if (verify) {
			T entity = lockAwareEntityRetriever.apply(id);
			AssertUtils.assertNull(entity.getLockedByUser(), "Locked By User should be null");
		}
		// Lock The Entity
		this.systemLockService.lockSystemLockAwareEntity(tableName, id, lockNote);
		T entity = lockAwareEntityRetriever.apply(id);
		if (verify) {
			AssertUtils.assertNotNull(entity.getLockedByUser(), "Locked By User should not be null after locking.");
			if (entity instanceof SystemLockWithNoteAware) {
				AssertUtils.assertNotNull(((SystemLockWithNoteAware) entity).getLockNote(), "Locked Note should not be null after locking.");
			}
		}

		// After Locking Set Current User
		if (afterLockUserId != null) {
			setCurrentUser(afterLockUserId);
		}

		return entity;
	}


	private SystemLockAwareTestEntity getSystemLockAwareTestEntity(int id) {
		return this.systemLockAwareTestEntityDAO.findByPrimaryKey(id);
	}


	private SystemLockWithNoteAwareTestEntity getSystemLockWithNoteAwareTestEntity(int id) {
		return this.systemLockWithNoteAwareTestEntityDAO.findByPrimaryKey(id);
	}


	private void setCurrentUser(short userId) {
		// Set Current User
		Mockito.when(this.securityUserService.getSecurityUserCurrent()).thenReturn(this.securityUserDAO.findByPrimaryKey(userId));
	}
}
