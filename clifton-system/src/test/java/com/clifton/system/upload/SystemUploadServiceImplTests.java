package com.clifton.system.upload;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataColumnStyle;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import com.clifton.system.hierarchy.definition.search.SystemHierarchySearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.upload.config.SystemUploadConfigPopulatorImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
@ContextConfiguration
public class SystemUploadServiceImplTests<E extends SystemColumnCustomValueAware> {

	@Resource
	private SystemUploadConfigPopulatorImpl systemUploadConfigPopulator;

	@Resource
	private SystemUploadServiceImpl systemUploadService;
	@Resource
	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;
	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	private static final String REQUIRED_STYLE = "fontColor=" + DataColumnStyle.FONT_COLOR_RED;
	private static final DataColumn CATEGORY_NAME = new DataColumnImpl("Category-Name", Types.NVARCHAR);
	private static final DataColumn NAME = new DataColumnImpl("Name", Types.NVARCHAR);
	private static final DataColumn CONDITION_NAME = new DataColumnImpl("Condition-Name", Types.NVARCHAR);


	static {
		CATEGORY_NAME.setStyle(REQUIRED_STYLE);
		NAME.setStyle(REQUIRED_STYLE);
	}


	private static final List<DataColumn> DATA_COLUMNS_CATEGORY_UPDATE = new ArrayList<>();


	static {
		DATA_COLUMNS_CATEGORY_UPDATE.add(CATEGORY_NAME);
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Category-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Category-SystemDefined", Types.BOOLEAN));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Category-MaxDepth", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Category-Inactive", Types.BOOLEAN));

		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Category-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Category-SystemDefined", Types.BOOLEAN));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Category-MaxDepth", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Category-Inactive", Types.BOOLEAN));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-SystemDefined", Types.BIT));

		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Category-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Category-SystemDefined", Types.BOOLEAN));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Category-MaxDepth", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Category-Inactive", Types.BOOLEAN));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-SystemDefined", Types.BIT));

		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-Category-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-Category-SystemDefined", Types.BOOLEAN));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-Category-MaxDepth", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-Category-Inactive", Types.BOOLEAN));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Parent-Parent-Parent-SystemDefined", Types.BIT));

		DATA_COLUMNS_CATEGORY_UPDATE.add(NAME);
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE.add(new DataColumnImpl("SystemDefined", Types.BIT));
	}


	private static final List<DataColumn> DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS = new ArrayList<>();


	static {
		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(CATEGORY_NAME);

		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("Parent-Name", Types.NVARCHAR));

		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("Parent-Parent-Name", Types.NVARCHAR));

		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("Parent-Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("Parent-Parent-Parent-Name", Types.NVARCHAR));

		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(NAME);
		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.add(new DataColumnImpl("SystemDefined", Types.BIT));
	}


	private static final List<DataColumn> DATA_COLUMNS_CATEGORY_READ = new ArrayList<>();


	static {
		DATA_COLUMNS_CATEGORY_READ.add(CATEGORY_NAME);
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-SystemDefined", Types.BIT));

		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-SystemDefined", Types.BIT));

		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-SystemDefined", Types.BIT));

		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-SystemDefined", Types.BIT));

		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Parent-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Parent-Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Parent-Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Parent-Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Parent-SystemDefined", Types.BIT));

		DATA_COLUMNS_CATEGORY_READ.add(NAME);
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ.add(new DataColumnImpl("SystemDefined", Types.BIT));
	}


	private static final List<DataColumn> DATA_COLUMNS_CATEGORY_READ_NO_FKS = new ArrayList<>();


	static {
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(CATEGORY_NAME);
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Name", Types.NVARCHAR));

		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Parent-Name", Types.NVARCHAR));

		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Parent-Parent-Name", Types.NVARCHAR));

		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Name", Types.NVARCHAR));

		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Parent-Category-Name", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Parent-Parent-Parent-Parent-Parent-Name", Types.NVARCHAR));

		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(NAME);
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Value", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Order", Types.INTEGER));
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("Description", Types.NVARCHAR));
		DATA_COLUMNS_CATEGORY_READ_NO_FKS.add(new DataColumnImpl("SystemDefined", Types.BIT));
	}


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
	}


	@Test
	public void testDownloadSampleFile_WithInsertMissingFKBeansOn() {
		this.systemUploadConfigPopulator.setMaxLevelsDeep(3);
		SystemUploadCommand uploadBean = new SystemUploadCommand();
		uploadBean.setTableName("SystemHierarchy");
		uploadBean.setInsertMissingFKBeans(true);

		DataTable dataTable = this.systemUploadService.downloadSystemUploadFileSample(uploadBean, false);
		validateDataTable(dataTable, DATA_COLUMNS_CATEGORY_UPDATE);

		// Change SystemHierarchyCategory table to NOT allow uploads
		SystemTable table = this.systemSchemaService.getSystemTableByName("SystemHierarchyCategory");
		table.setUploadAllowed(false);
		this.systemSchemaService.saveSystemTable(table);

		// Verify Upload Sample DataTable
		this.systemUploadConfigPopulator.setMaxLevelsDeep(5);
		dataTable = this.systemUploadService.downloadSystemUploadFileSample(uploadBean, false);
		validateDataTable(dataTable, DATA_COLUMNS_CATEGORY_READ);
	}


	@Test
	public void testDownloadSampleFile_WithInsertMissingFKBeansOff() {
		this.systemUploadConfigPopulator.setMaxLevelsDeep(3);

		SystemUploadCommand uploadBean = new SystemUploadCommand();
		uploadBean.setTableName("SystemHierarchy");
		uploadBean.setInsertMissingFKBeans(false);

		DataTable dataTable = this.systemUploadService.downloadSystemUploadFileSample(uploadBean, false);
		validateDataTable(dataTable, DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS);

		// Change SystemHierarchyCategory table to NOT allow uploads - SHOULDN'T MATTER BECAUSE WE AREN'T ALLOWING FK INSERTS
		SystemTable table = this.systemSchemaService.getSystemTableByName("SystemHierarchyCategory");
		table.setUploadAllowed(false);
		this.systemSchemaService.saveSystemTable(table);

		// Verify Upload Sample DataTable
		this.systemUploadConfigPopulator.setMaxLevelsDeep(5);
		dataTable = this.systemUploadService.downloadSystemUploadFileSample(uploadBean, false);
		validateDataTable(dataTable, DATA_COLUMNS_CATEGORY_READ_NO_FKS);
	}


	@Test
	public void testDownloadSampleFile_BeanPropertiesToExclude() {
		this.systemUploadConfigPopulator.setMaxLevelsDeep(3);

		SystemUploadCommand uploadBean = new SystemUploadCommand();
		uploadBean.setTableName("SystemHierarchy");
		uploadBean.setInsertMissingFKBeans(false);
		uploadBean.setBeanPropertiesToExclude(new String[]{"parent.parent.parent.name"});

		DataTable dataTable = this.systemUploadService.downloadSystemUploadFileSample(uploadBean, false);
		validateDataTable(dataTable, DATA_COLUMNS_CATEGORY_UPDATE_NO_FKS.stream().filter(dataColumn -> !dataColumn.getColumnName().equals("Parent-Parent-Parent-Name")).collect(Collectors.toList()));
	}


	private void validateDataTable(DataTable dataTable, List<DataColumn> verifyList) {
		Assertions.assertNotNull(dataTable);

		DataColumn[] columnList = dataTable.getColumnList();
		Assertions.assertEquals(verifyList.size(), columnList.length, "Expected Column List to be size [" + verifyList.size() + "] but was [" + columnList.length + "]");
		for (DataColumn column : verifyList) {
			boolean found = false;
			for (DataColumn fileColumn : columnList) {
				if (column.getColumnName().equals(fileColumn.getColumnName())) {
					DataColumnStyle columnStyle = new DataColumnStyle(column.getStyle());
					DataColumnStyle fileColumnStyle = new DataColumnStyle(fileColumn.getStyle());
					Assertions.assertEquals(columnStyle.getFontColor(), fileColumnStyle.getFontColor(), "Column [" + column.getColumnName() + "] expected font color to be [" + columnStyle.getFontColor() + "] but was [" + fileColumnStyle.getFontColor() + "]");
					found = true;
					break;
				}
			}
			Assertions.assertTrue(found, "Could not find Column [" + column.getColumnName() + "] in the resulting DataTable");
		}
	}


	@Test
	public void testUploadSystemHierarchyExcelFileMissingColumns() {
		Assertions.assertThrows(ValidationException.class, () -> {
			SystemUploadCommand upload = new SystemUploadCommand();
			upload.setTableName("SystemHierarchy");
			upload.setFile(new MultipartFileImpl("src/test/java/com/clifton/system/upload/SystemUploadHierarchySample_MissingColumns.xls"));
			this.systemUploadService.uploadSystemUploadFile(upload);
		});
	}


	@Test
	public void testUploadSystemHierarchyExcelFile() {
		// Change SystemHierarchyCategory table to NOT allow uploads
		SystemTable table = this.systemSchemaService.getSystemTableByName("SystemHierarchyCategory");
		table.setUploadAllowed(false);
		this.systemSchemaService.saveSystemTable(table);

		SystemUploadCommand upload = new SystemUploadCommand();
		upload.setTableName("SystemHierarchy");
		upload.setFile(new MultipartFileImpl("src/test/java/com/clifton/system/upload/SystemUploadHierarchySample.xls"));
		this.systemUploadService.uploadSystemUploadFile(upload);
		// Should fail because SystemHierarchyCategory table doesn't allow inserts via uploads
		// and also because insertMissingFKBeans is set to false.
		Assertions.assertEquals(
				"No rows were saved in the database."
						+ StringUtils.NEW_LINE
						+ "The following errors were encountered with the upload file: "
						+ StringUtils.NEW_LINE
						+ "[Bean_0]: Unable to lookup [SystemHierarchyCategory] by natural key(s) [name] values [Test Category]."
						+ StringUtils.NEW_LINE
						+ StringUtils.NEW_LINE
						+ "[Bean_1]: Unable to lookup [SystemHierarchyCategory] by natural key(s) [name] values [Test Category]."
						+ StringUtils.NEW_LINE
						+ "Unable to lookup [SystemHierarchy] by natural key(s) [category,parent,name] values [{id=null,label=Test Category},NULL,Level One].If you are referencing an object from within this file, please make sure it's created in a row before being used as a reference."
						+ StringUtils.NEW_LINE
						+ StringUtils.NEW_LINE
						+ "[Bean_2]: Unable to lookup [SystemHierarchyCategory] by natural key(s) [name] values [Test Category]."
						+ StringUtils.NEW_LINE
						+ "Unable to lookup [SystemHierarchy] by natural key(s) [category,parent,name] values [{id=null,label=Test Category},NULL,Level One].If you are referencing an object from within this file, please make sure it's created in a row before being used as a reference."
						+ StringUtils.NEW_LINE + StringUtils.NEW_LINE, upload.getUploadResultsString());

		// Perform the upload again, this time inserting missing FK Fields
		upload.setInsertMissingFKBeans(true);
		this.systemUploadService.uploadSystemUploadFile(upload);
		// Should still fail because SystemHierarchyCategory table doesn't allow uploads
		Assertions.assertEquals(
				"No rows were saved in the database."
						+ StringUtils.NEW_LINE
						+ "The following errors were encountered with the upload file: "
						+ StringUtils.NEW_LINE
						+ "[Bean_0]: Unable to lookup [SystemHierarchyCategory] by natural key(s) [name] values [Test Category]."
						+ StringUtils.NEW_LINE
						+ StringUtils.NEW_LINE
						+ "[Bean_1]: Unable to lookup [SystemHierarchy] by natural key(s) [category,parent,name] values [{id=null,label=Test Category},NULL,Level One].If you are referencing an object from within this file, please make sure it's created in a row before being used as a reference."
						+ StringUtils.NEW_LINE
						+ StringUtils.NEW_LINE
						+ "[Bean_2]: Unable to lookup [SystemHierarchy] by natural key(s) [category,parent,name] values [{id=null,label=Test Category},NULL,Level One].If you are referencing an object from within this file, please make sure it's created in a row before being used as a reference."
						+ StringUtils.NEW_LINE + StringUtils.NEW_LINE, upload.getUploadResultsString());

		// Change SystemHierarchyCategory table to allow uploads
		table.setUploadAllowed(true);
		this.systemSchemaService.saveSystemTable(table);

		// This time should succeed since SystemHierarchyCategory table now allows uploads
		this.systemUploadService.uploadSystemUploadFile(upload);
		Assertions.assertEquals("[SystemHierarchy]: 3 Records Inserted." + StringUtils.NEW_LINE + "[SystemHierarchyCategory]: 1 Records Inserted." + StringUtils.NEW_LINE + StringUtils.NEW_LINE
				+ "There were no errors with the upload file.", upload.getUploadResultsString());

		// Verify Results
		// Category Saved By Upload should have ID of 1
		SystemHierarchyCategory category = this.systemHierarchyDefinitionService.getSystemHierarchyCategoryByName("Test Category");
		Assertions.assertEquals("Test Category Description", category.getDescription());
		SystemHierarchySearchForm searchForm = new SystemHierarchySearchForm();
		searchForm.setCategoryId(category.getId());
		searchForm.setIgnoreNullParent(true);
		searchForm.setOrderBy("name");
		List<SystemHierarchy> hierarchyList = this.systemHierarchyDefinitionService.getSystemHierarchyList(searchForm);
		Assertions.assertEquals(3, hierarchyList.size());

		for (SystemHierarchy hierarchy : hierarchyList) {
			Assertions.assertEquals(category, hierarchy.getCategory());
			if ("Level One".equals(hierarchy.getName())) {
				Assertions.assertNull(hierarchy.getParent(), "Level One should not have a parent hierarchy");
			}
			else {
				Assertions.assertEquals("Level One", hierarchy.getParent().getName(), "Level Two-A and Level Two-B should have Level One as their parent.");
			}
		}

		// Try to Save again - shouldn't overwrite anything.
		upload.setExistingBeans(FileUploadExistingBeanActions.SKIP);
		this.systemUploadService.uploadSystemUploadFile(upload);

		Assertions.assertEquals("No rows were saved in the database." + StringUtils.NEW_LINE + "There were no errors with the upload file.", upload.getUploadResultsString());

		// Set Overwrite existing - should update all
		upload.setExistingBeans(FileUploadExistingBeanActions.UPDATE);
		this.systemUploadService.uploadSystemUploadFile(upload);
		Assertions.assertEquals("[SystemHierarchy]: 3 Records Inserted/Updated." + StringUtils.NEW_LINE + StringUtils.NEW_LINE + "There were no errors with the upload file.",
				upload.getUploadResultsString());

		// Set Inserts Only - Should throw a Validation Exception
		/*  TODO XML DAO'S DON'T CHECK UX CONSTRAINTS???
		 upload.setExistingBeans(SystemUploadCommand.EXISTING_BEANS_INSERT);
		 boolean exceptionOccurred = false;
		 try {
		 this.systemUploadService.uploadSystemUploadFile(upload);
		 }
		 catch (ValidationException e) {
		 // Do nothing expected an exception
		 exceptionOccurred = true;
		 System.out.println(e.getMessage());
		 }
		 Assertions.assertTrue("Expected an UX Constraint Violation Validation Exception to be thrown by upload but it wasn't, instead the upload succeed with the following message ["
		 + upload.getUploadResultsString() + "]", exceptionOccurred);
		 */
	}


	@Test
	public void testPartialUpdateUploads() {
		// Setup Existing Data
		SystemHierarchyCategory c1 = new SystemHierarchyCategory();
		c1.setName("Test Partial Update Category");
		this.systemHierarchyDefinitionService.saveSystemHierarchyCategory(c1);

		SystemHierarchy h1 = new SystemHierarchy();
		h1.setName("Hierarchy 1");
		h1.setCategory(c1);
		h1.setDescription("Test Description for hierarchy 1 - Do not overwrite");
		this.systemHierarchyDefinitionService.saveSystemHierarchy(h1);

		SystemHierarchy h2 = new SystemHierarchy();
		h2.setParent(h1);
		h2.setCategory(c1);
		h2.setName("Hierarchy 2");
		h2.setDescription("Test Description for hierarchy 2 - Do not overwrite");
		this.systemHierarchyDefinitionService.saveSystemHierarchy(h2);

		SystemUploadCommand upload = new SystemUploadCommand();
		upload.setTableName("SystemHierarchy");
		upload.setFile(new MultipartFileImpl("src/test/java/com/clifton/system/upload/SystemUploadHierarchyPartialUpdate.xls"));
		upload.setExistingBeans(FileUploadExistingBeanActions.UPDATE);
		this.systemUploadService.uploadSystemUploadFile(upload);

		// Upload should add Hierarchy 2b, but not change the descriptions of 1 & 2
		SystemHierarchySearchForm searchForm = new SystemHierarchySearchForm();
		searchForm.setIgnoreNullParent(true);
		searchForm.setCategoryId(c1.getId());
		searchForm.setOrderBy("name");

		List<SystemHierarchy> hList = this.systemHierarchyDefinitionService.getSystemHierarchyList(searchForm);
		Assertions.assertEquals(3, hList.size());
		for (SystemHierarchy h : hList) {
			if ("Hierarchy 1".equals(h.getName())) {
				Assertions.assertEquals("Test Description for hierarchy 1 - Do not overwrite", h.getDescription());
				Assertions.assertNull(h.getParent());
			}
			else {
				Assertions.assertEquals("Hierarchy 1", h.getParent().getName());

				if ("Hierarchy 2".equals(h.getName())) {
					Assertions.assertEquals("Test Description for hierarchy 2 - Do not overwrite", h.getDescription());
				}
				else {
					Assertions.assertTrue(StringUtils.isEmpty(h.getDescription()));
				}
			}
		}
	}


	@Test
	public void testPartialUpdateUploadsWithFKs() {
		// Verify existing data
		SystemTable t1 = this.systemSchemaService.getSystemTableByName("SystemTable");
		Assertions.assertEquals("Test Condition - Does nothing", t1.getDeleteEntityCondition().getName());
		Assertions.assertNull(t1.getDetailScreenClass());

		SystemTable t2 = this.systemSchemaService.getSystemTableByName("SystemBean");
		Assertions.assertNull(t2.getDeleteEntityCondition());
		Assertions.assertNull(t2.getDetailScreenClass());

		SystemTable t3 = this.systemSchemaService.getSystemTableByName("SystemBeanType");
		Assertions.assertEquals("Clifton.system.bean.BeanTypeWindow", t3.getDetailScreenClass());
		Assertions.assertNull(t3.getDeleteEntityCondition());

		SystemUploadCommand upload = new SystemUploadCommand();
		upload.setTableName("SystemTable");
		upload.setFile(new MultipartFileImpl("src/test/java/com/clifton/system/upload/SystemUploadSystemTablePartialUpdate.xls"));
		upload.setExistingBeans(FileUploadExistingBeanActions.UPDATE);
		this.systemUploadService.uploadSystemUploadFile(upload);

		// Verify data was updated and other data wasn't overwritten
		t1 = this.systemSchemaService.getSystemTableByName("SystemTable");
		Assertions.assertNotNull(t1.getDataSource());
		Assertions.assertEquals("Test Condition - Does nothing", t1.getDeleteEntityCondition().getName());
		Assertions.assertNull(t1.getDetailScreenClass());

		t2 = this.systemSchemaService.getSystemTableByName("SystemBean");
		Assertions.assertNotNull(t2.getDataSource());
		Assertions.assertNull(t2.getDeleteEntityCondition());
		Assertions.assertNull(t2.getDetailScreenClass());

		t3 = this.systemSchemaService.getSystemTableByName("SystemBeanType");
		Assertions.assertEquals("Clifton.system.bean.BeanTypeWindow", t3.getDetailScreenClass());
		Assertions.assertNotNull(t3.getDataSource());
		Assertions.assertNull(t3.getDeleteEntityCondition());
	}
}
