<?xml version="1.0" encoding="UTF-8" ?>
<migrations xmlns="https://nexus.paraport.com/repository/schema/migrations"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="https://nexus.paraport.com/repository/schema/migrations https://nexus.paraport.com/repository/schema/migrations/clifton-migrations.xsd"
			conditionalSql="SELECT 1 WHERE NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemConditionEntry' AND COLUMN_NAME = 'ParentSystemConditionEntryID')">


	<sql runWith="DDL">
		<statement>

			ALTER TABLE SystemConditionEntry ALTER COLUMN SystemBeanID INT NULL;


			ALTER TABLE SystemConditionEntry ADD ConditionEntryType NVARCHAR(50) NULL;

			ALTER TABLE SystemConditionEntry ADD ParentSystemConditionEntryID INT NULL;

			ALTER TABLE SystemConditionEntry ADD CONSTRAINT [FK_SystemConditionEntry_SystemConditionEntry_ParentSystemConditionEntryID] FOREIGN KEY (ParentSystemConditionEntryID)
				REFERENCES SystemConditionEntry(SystemConditionEntryID);

			CREATE NONCLUSTERED INDEX ix_SystemConditionEntry_ParentSystemConditionEntryID ON SystemConditionEntry(ParentSystemConditionEntryID);

			ALTER TABLE SystemCondition ADD SystemConditionEntryID INT NULL;

			ALTER TABLE SystemCondition ADD CONSTRAINT [FK_SystemCondition_SystemConditionEntry_SystemConditionEntryID] FOREIGN KEY (SystemConditionEntryID)
				REFERENCES SystemConditionEntry(SystemConditionEntryID);

     	</statement>
     </sql>

	<sql runWith="META_DATA">
     	<statement>

			INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemConditionEntry'), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING'), 'ConditionEntryType', 'Condition Entry Type', 'Condition Entry Type', 'entryType', 1, 0, getdate(), 0, getdate();

			INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemConditionEntry'), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'INTEGER'), 'ParentSystemConditionEntryID', 'Parent Entry', 'Parent System Condition Entry ID', 'parent', 1, 0, getdate(), 0, getdate();

			INSERT INTO SystemRelationship(SystemColumnID, ParentSystemColumnID, RelationshipName, RelationshipLabel, RelationshipDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'ParentSystemConditionEntryID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemConditionEntry')), (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'SystemConditionEntryID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemConditionEntry')), 'FK_SystemConditionEntry_SystemConditionEntry_ParentSystemConditionEntryID', NULL, NULL, 0, getdate(), 0, getdate();


			INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemCondition'), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING'), 'SystemConditionEntryID', 'Condition Entry', 'System Condition Entry ID', 'conditionEntry', 1, 0, getdate(), 0, getdate();

			INSERT INTO SystemRelationship(SystemColumnID, ParentSystemColumnID, RelationshipName, RelationshipLabel, RelationshipDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'SystemConditionEntryID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemCondition')), (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'SystemConditionEntryID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemConditionEntry')), 'FK_SystemCondition_SystemConditionEntry_SystemConditionEntryID', NULL, NULL, 0, getdate(), 0, getdate();

		</statement>
	</sql>


	<sql runWith="META_DATA">
		<statement><![CDATA[
			-- MIGRATE OLD DATA TO NEW FORMAT AND DROP OLD COLUMNS

			-- 0. Correct duplicate names
			UPDATE SystemCondition SET SystemConditionName = CAST(SystemConditionID AS NVARCHAR) + ' - ' + SystemConditionName
				FROM SystemCondition c
				WHERE EXISTS (
					SELECT * FROM SystemCondition c2
					WHERe c.SystemConditionName = c2.SystemConditionName AND c.SystemConditionID <> c2.SystemConditionID
				)
				AND c.ParentSystemConditionID IS NOT NULL


			-- 1. Convert simple single entry conditions (119 from 258)
			UPDATE SystemCondition SET SystemConditionEntryID = e.SystemConditionEntryID
			FROM SystemCondition c
				INNER JOIN SystemConditionEntry e ON e.SystemConditionID = c.SystemConditionID
			WHERE
				NOT EXISTS ( -- no parent conditions
					SELECT * FROM SystemCondition c2
					WHERE c2.ParentSystemConditionID = c.SystemConditionID
				)
				AND ( -- only one child entry
					SELECT COUNT(*) FROM SystemConditionEntry e2
					WHERE e2.SystemConditionID = c.SystemConditionID
				) = 1;

			UPDATE SystemConditionEntry SET ConditionEntryType = 'CONDITION'
			FROM SystemConditionEntry e
				INNER JOIN SystemCondition c ON e.SystemConditionID = c.SystemConditionID
			WHERE c.SystemConditionEntryID IS NOT NULL;


			-- 2. Convert all entries for conditions with multiple entries (no sub-conditions)

			INSERT INTO SystemConditionEntry(SystemConditionID, ConditionEntryType, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT SystemConditionID, CASE WHEN IsOrCondition = 1 THEN 'OR' ELSE 'AND' END, CreateUserID, CreateDate, UpdateUserID, UpdateDate FROM SystemCondition c
				WHERE SystemConditionEntryID IS NULL

			UPDATE SystemCondition SET SystemConditionEntryID = e.SystemConditionEntryID
				FROM SystemCondition c
					INNER JOIN SystemConditionEntry e ON c.SystemConditionID = e.SystemConditionID
				WHERE c.SystemConditionEntryID IS NULL AND e.ConditionEntryType IN ('AND', 'OR')

			UPDATE e SET ParentSystemConditionEntryID = e2.SystemConditionEntryID, ConditionEntryType = 'CONDITION'
				FROM SystemConditionEntry e
					INNER JOIN SystemConditionEntry e2 ON e.SystemConditionID = e2.SystemConditionID
				WHERE e.ConditionEntryType IS NULL AND e2.ConditionEntryType IN ('AND', 'OR')


			-- 3. Copy grand-child entries into child entries
			INSERT INTO SystemConditionEntry(SystemConditionID, ParentSystemConditionEntryID, ConditionEntryType, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT c.SystemConditionID, e.SystemConditionEntryID, e2.ConditionEntryType, e2.CreateUserID, e2.CreateDate, e2.UpdateUserID, e2.UpdateDate
				FROM SystemCondition c
					INNER JOIN SystemConditionEntry e ON c.SystemConditionEntryID = e.SystemConditionEntryID
					INNER JOIN SystemCondition c2 ON c2.ParentSystemConditionID = c.SystemConditionID
					INNER JOIN SystemConditionEntry e2 ON c2.SystemConditionEntryID = e2.SystemConditionEntryID
				WHERE c.ParentSystemConditionID IS NOT NULL
					AND EXISTS (
						SELECT * FROM SystemCondition c2
						WHERE c2.ParentSystemConditionID = c.SystemConditionID
					)

			INSERT INTO SystemConditionEntry(SystemConditionID, ParentSystemConditionEntryID, ConditionEntryType, SystemBeanID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT c.SystemConditionID, newE.SystemConditionEntryID, e3.ConditionEntryType, e3.SystemBeanID, e2.CreateUserID, e2.CreateDate, e2.UpdateUserID, e2.UpdateDate
				FROM SystemCondition c
					INNER JOIN SystemConditionEntry e ON c.SystemConditionEntryID = e.SystemConditionEntryID
					INNER JOIN SystemConditionEntry newE ON newE.ParentSystemConditionEntryID = e.SystemConditionEntryID
					INNER JOIN SystemCondition c2 ON c2.ParentSystemConditionID = c.SystemConditionID
					INNER JOIN SystemConditionEntry e2 ON c2.SystemConditionEntryID = e2.SystemConditionEntryID
					INNER JOIN SystemConditionEntry e3 ON e3.ParentSystemConditionEntryID = e2.SystemConditionEntryID
				WHERE c.ParentSystemConditionID IS NOT NULL
					AND EXISTS (
						SELECT * FROM SystemCondition c2
						WHERE c2.ParentSystemConditionID = c.SystemConditionID
					)
					AND newE.ConditionEntryType IN ('AND', 'OR')

			UPDATE c2 SET ParentSystemConditionID = NULL
				FROM SystemCondition c
					INNER JOIN SystemCondition c2 ON c2.ParentSystemConditionID = c.SystemConditionID
				WHERE c.ParentSystemConditionID IS NOT NULL
					AND EXISTS (
						SELECT * FROM SystemCondition c2
						WHERE c2.ParentSystemConditionID = c.SystemConditionID
					)


			-- 4. Copy child entries into parent (former parent into grandparent)
			INSERT INTO SystemConditionEntry(SystemConditionID, ParentSystemConditionEntryID, ConditionEntryType, SystemBeanID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT c2.SystemConditionID, e.SystemConditionEntryID, e2.ConditionEntryType, CASE WHEN e2.ConditionEntryType = 'CONDITION' THEN e2.SystemBeanID ELSE NULL END, e2.CreateUserID, e2.CreateDate, e2.UpdateUserID, e2.UpdateDate
				FROM SystemCondition c
					INNER JOIN SystemConditionEntry e ON c.SystemConditionEntryID = e.SystemConditionEntryID
					INNER JOIN SystemCondition c2 ON c2.ParentSystemConditionID = c.SystemConditionID
					INNER JOIN SystemConditionEntry e2 ON c2.SystemConditionEntryID = e2.SystemConditionEntryID

			INSERT INTO SystemConditionEntry(SystemConditionID, ParentSystemConditionEntryID, ConditionEntryType, SystemBeanID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT c.SystemConditionID, p2.SystemConditionEntryID, e2.ConditionEntryType, CASE WHEN e2.ConditionEntryType = 'CONDITION' THEN e2.SystemBeanID ELSE NULL END, e2.CreateUserID, e2.CreateDate, e2.UpdateUserID, e2.UpdateDate
				FROM SystemCondition c
					INNER JOIN SystemConditionEntry p ON c.SystemConditionEntryID = p.SystemConditionEntryID
					INNER JOIN SystemConditionEntry p2 ON p2.ParentSystemConditionEntryID = p.SystemConditionEntryID
					INNER JOIN SystemCondition c2 ON c2.ParentSystemConditionID = c.SystemConditionID AND p2.SystemConditionID = c2.SystemConditionID
					INNER JOIN SystemConditionEntry e ON c2.SystemConditionEntryID = e.SystemConditionEntryID
					INNER JOIN SystemConditionEntry e2 ON e2.ParentSystemConditionEntryID = e.SystemConditionEntryID
				WHERE p2.ConditionEntryType IN ('AND', 'OR')

			INSERT INTO SystemConditionEntry(SystemConditionID, ParentSystemConditionEntryID, ConditionEntryType, SystemBeanID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
				SELECT c.SystemConditionID, p3.SystemConditionEntryID, e3.ConditionEntryType, CASE WHEN e3.ConditionEntryType = 'CONDITION' THEN e3.SystemBeanID ELSE NULL END, e3.CreateUserID, e3.CreateDate, e3.UpdateUserID, e3.UpdateDate
				FROM SystemCondition c
					INNER JOIN SystemConditionEntry p ON c.SystemConditionEntryID = p.SystemConditionEntryID
					INNER JOIN SystemConditionEntry p2 ON p2.ParentSystemConditionEntryID = p.SystemConditionEntryID
					INNER JOIN SystemConditionEntry p3 ON p3.ParentSystemConditionEntryID = p2.SystemConditionEntryID
					INNER JOIN SystemCondition c2 ON c2.ParentSystemConditionID = c.SystemConditionID AND p2.SystemConditionID = c2.SystemConditionID
					INNER JOIN SystemConditionEntry e ON c2.SystemConditionEntryID = e.SystemConditionEntryID
					INNER JOIN SystemConditionEntry e2 ON e2.ParentSystemConditionEntryID = e.SystemConditionEntryID
					INNER JOIN SystemConditionEntry e3 ON e3.ParentSystemConditionEntryID = e2.SystemConditionEntryID
				WHERE p2.ConditionEntryType IN ('AND', 'OR') AND p3.ConditionEntryType IN ('AND', 'OR') AND p3.ConditionEntryType = e2.ConditionEntryType


			-- 5. Drop no longer used columns and make fields not null
			DROP INDEX ix_SystemConditionEntry_SystemConditionID ON SystemConditionEntry;
			DROP INDEX ix_SystemConditionEntry_SystemBeanID ON SystemConditionEntry;
			ALTER TABLE SystemConditionEntry DROP CONSTRAINT FK_SystemConditionEntry_SystemCondition_SystemConditionID;
			ALTER TABLE SystemConditionEntry DROP COLUMN SystemConditionID;

			ALTER TABLE SystemCondition DROP CONSTRAINT DF_SystemCondition_IsOrCondition;
			ALTER TABLE SystemCondition DROP COLUMN IsOrCondition;

			DELETE FROM SystemRelationship
				WHERE SystemColumnID = (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'SystemConditionID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemConditionEntry'))
					AND ParentSystemColumnID = (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'SystemConditionID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemCondition'));

			DELETE FROM SystemColumn
				WHERE ColumnName = 'SystemConditionID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemConditionEntry');

			ALTER TABLE SystemConditionEntry ALTER COLUMN ConditionEntryType NVARCHAR(50) NOT NULL;


			DROP INDEX ux_SystemCondition_ParentSystemConditionID_SystemConditionName ON SystemCondition;
			DROP INDEX ux_SystemCondition_SystemConditionName_ParentSystemConditionID ON SystemCondition;
			ALTER TABLE SystemCondition DROP CONSTRAINT FK_SystemCondition_SystemCondition_ParentSystemConditionID;
			ALTER TABLE SystemCondition DROP COLUMN ParentSystemConditionID;

			DELETE FROM SystemRelationship
				WHERE SystemColumnID = (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'ParentSystemConditionID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemCondition'))
					AND ParentSystemColumnID = (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'SystemConditionID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemCondition'));

			DELETE FROM SystemAuditEvent WHERE SystemColumnID = (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'ParentSystemConditionID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemCondition'));

			DELETE FROM SystemColumn
				WHERE ColumnName = 'ParentSystemConditionID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemCondition');


			DELETE FROM SystemAuditEvent WHERE SystemColumnID = (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'IsOrCondition' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemCondition'));

			DELETE FROM SystemColumn
				WHERE ColumnName = 'IsOrCondition' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemCondition');


			ALTER TABLE SystemCondition ALTER COLUMN SystemConditionEntryID INT NOT NULL;


			CREATE UNIQUE NONCLUSTERED INDEX ux_SystemCondition_SystemConditionName ON SystemCondition(SystemConditionName);

     	]]></statement>
     </sql>

</migrations>
