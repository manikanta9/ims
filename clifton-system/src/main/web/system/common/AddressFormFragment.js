Clifton.system.common.AddressFormFragment = Ext.extend(TCG.form.FormPanelFragment, {

	// If true then will put address fields in a field set
	useFieldSet: false,
	fieldSetTitle: 'Address',
	fieldSetCollapsed: false,
	fieldSetCollapsible: false,
	bodyStyle: 'padding: 0',

	itemsBefore: undefined,
	itemsAfter: undefined,
	addressFieldsPrefix: undefined, // CAN BE USED FOR company.addressLine1 or registrationAddressLine1 - if prefix is not empty and doesn't end in a . will capitalize the existing property names

	initComponent: function() {
		const currentItems = [];
		if (this.itemsBefore) {
			Ext.each(this.itemsBefore, function(f) {
				currentItems.push(f);
			});
		}
		this.addressFieldSet = null;

		if (this.useFieldSet) {
			const fs = new TCG.form.FieldSet({
				collapsible: this.fieldSetCollapsible,
				collapsed: this.fieldSetCollapsed,
				title: this.fieldSetTitle,
				items: this.getAddressItems()
			});
			currentItems.push(fs);
			this.addressFieldSet = fs;
		}
		else {
			const thisAddressItems = this.getAddressItems();
			Ext.each(thisAddressItems, function(f) {
				currentItems.push(f);
			});
		}
		if (this.itemsAfter) {
			Ext.each(this.itemsAfter, function(f) {
				currentItems.push(f);
			});
		}
		this.items = currentItems;
		Clifton.system.common.AddressFormFragment.superclass.initComponent.call(this);
	},

	getAddressItems: function() {
		const ff = this;
		const addressPrefix = this.addressFieldsPrefix;
		if (TCG.isBlank(addressPrefix)) {
			return this.addressItems;
		}
		const thisAddressItems = [];
		Ext.each(this.addressItems, function(fItem) {
			const f = TCG.clone(fItem);
			if (f.columns) {
				Ext.each(f.columns, function(column) {
					Ext.each(column.rows, function(cf) {
						ff.applyAddressFieldAdjustments(addressPrefix, cf);
					});
				});
			}
			else {
				ff.applyAddressFieldAdjustments(addressPrefix, f);
			}
			thisAddressItems.push(f);
		});
		return thisAddressItems;
	},

	applyAddressFieldAdjustments: function(addressPrefix, field) {
		field.name = this.getAddressFieldName(addressPrefix, field.name);
		if (TCG.isNotBlank(field.hiddenName)) {
			field.hiddenName = this.getAddressFieldName(addressPrefix, field.hiddenName);
		}
		if (TCG.isNotBlank(field.countryFieldName)) {
			field.countryFieldName = this.getAddressFieldName(addressPrefix, field.countryFieldName);
		}
	},
	getAddressFieldName: function(addressPrefix, fieldName) {
		if (TCG.endsWith(addressPrefix, '.')) {
			fieldName = addressPrefix + fieldName;
		}
		else {
			fieldName = addressPrefix + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length);
		}
		return fieldName;
	},

	instructions: 'Please enter address related information below.',

	addressItems:
		[
			// Address Fields
			{fieldLabel: 'Address 1', name: 'addressLine1'},
			{fieldLabel: 'Address 2', name: 'addressLine2'},
			{fieldLabel: 'Address 3', name: 'addressLine3'},
			{
				xtype: 'columnpanel',
				defaults: {xtype: 'textfield', anchor: '-20'},
				columns: [
					{
						rows: [
							{fieldLabel: 'City', name: 'city'}
						],
						config: {columnWidth: 0.4}
					},
					{
						rows: [
							{
								fieldLabel: 'State', name: 'state', xtype: 'combo', valueField: 'value', displayField: 'value', tooltipField: 'text', url: 'systemListItemListFind.json',
								countryFieldName: 'country',
								beforequery: function(queryEvent) {
									const f = queryEvent.combo.getParentForm().getWindow().getMainForm();
									const country = f.findField(queryEvent.combo.countryFieldName).value;
									if (country === 'Canada') {
										queryEvent.combo.store.setBaseParam('listName', 'Canadian Provinces');
									}
									else {
										queryEvent.combo.store.setBaseParam('listName', 'States');
									}
								},
								listeners: {
									select: function(field, value) {
										if (TCG.isNotBlank(value)) {
											const f = field.getParentForm().getWindow().getMainForm();
											const country = f.findField(field.countryFieldName);
											if (TCG.isBlank(country.getValue())) {
												country.setValue('United States');
												country.setRawValue('United States');
											}
										}
									}
								}
							}
						],
						config: {columnWidth: 0.3, labelWidth: 50}
					},
					{
						rows: [
							{fieldLabel: 'Postal Code', name: 'postalCode'}
						],
						config: {columnWidth: 0.3, labelWidth: 80}
					}
				]
			},
			{fieldLabel: 'Country', name: 'country', xtype: 'combo', displayField: 'text', valueField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'}
		]
});
Ext.reg('address-formfragment', Clifton.system.common.AddressFormFragment);
