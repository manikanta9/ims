Clifton.system.list.ListSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'systemListSetupWindow',
	title: 'Lists and Queries',
	iconCls: 'list',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.system.list.ListSetupWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.system.list.ListSetupWindowAdditionalTabs[i]);
				}
			}
		},
		items: [
			{
				title: 'Lists',
				items: [{
					xtype: 'gridpanel',
					name: 'systemListListFind',
					instructions: 'This section manages lists that can be used across the system for look ups.  Lists can be manually created/edited by adding specific items to the list.  Or, lists can be calculated by executing a SQL query.',
					wikiPage: 'IT/System+Lists',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'List Name', width: 80, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Modify Condition', width: 100, dataIndex: 'entityModifyCondition.name', filter: false},
						{header: 'Data Type', width: 25, dataIndex: 'dataType.name'},
						{header: 'SQL', width: 15, dataIndex: 'sqlList', type: 'boolean'},
						{header: 'Name System Defined', width: 35, dataIndex: 'nameSystemDefined', type: 'boolean'},
						{header: 'List System Defined', width: 35, dataIndex: 'listSystemDefined', type: 'boolean'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'queryTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=System List Tags'},
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const tag = TCG.getChildByName(this.getTopToolbar(), 'queryTagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							return {
								categoryName: 'System List Tags',
								categoryHierarchyId: tag.getValue()
							};
						}
					},
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								return TCG.isTrue(rowItem.get('sqlList')) ? 'Query' : 'List';
							},
							items: [
								{text: 'List', iconCls: 'list', className: 'Clifton.system.list.ListWindow'},
								{text: 'Query', iconCls: 'run', className: 'Clifton.system.list.ListQueryWindow'}
							]
						},
						deleteURL: 'systemListDelete.json'
					}
				}]
			}
		]
	}]
});

