Clifton.system.list.ListWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'List',
	iconCls: 'list',
	width: 800,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'List',
				items: [{
					xtype: 'formpanel',
					instructions: 'A manual list is populated by adding/editing/deleting items to it. Names must be unique and if the name is flagged as system defined it cannot be edited.  The data type selected is the type of values that can be entered for each item (Integer, String, etc)',
					url: 'systemList.json',
					items: [
						{fieldLabel: 'List Name', name: 'name'},
						{fieldLabel: 'Data Type', name: 'dataType.name', hiddenName: 'dataType.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'systemDataTypeList.json', detailPageClass: 'Clifton.system.schema.DataTypeWindow'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true},
						{boxLabel: 'List Name is system defined (cannot be cleared: referenced by name in code or via meta data)', name: 'nameSystemDefined', xtype: 'checkbox'},
						{boxLabel: 'List is system defined (list items cannot be changed, added or deleted)', name: 'listSystemDefined', xtype: 'checkbox'},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'SystemList',
							hierarchyCategoryName: 'System List Tags'
						}
					],
					getSaveURL: function() {
						return 'systemListEntitySave.json';
					}
				}]
			},


			{
				title: 'Items',
				items: [{
					xtype: 'system-list-item-grid',
					instructions: 'The following items are associated with this list.  If the list is not a System Defined List then items can be added/edited/removed.'
				}]
			}
		]
	}]
});
