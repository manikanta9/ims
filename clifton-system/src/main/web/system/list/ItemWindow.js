Clifton.system.list.ItemWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'List Item',
	iconCls: 'list',

	items: [{
		xtype: 'formpanel',
		instructions: 'A list item has a value field, text field and optional tooltip field.',
		url: 'systemListItem.json',
		labelFieldName: 'text',
		items: [
			{fieldLabel: 'System List', name: 'systemList.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.list.ListWindow', detailIdField: 'systemList.id'},
			{fieldLabel: 'Value', name: 'value'},
			{fieldLabel: 'Text', name: 'text'},
			{fieldLabel: 'Tooltip', name: 'tooltip', xtype: 'textarea'},
			{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield'},
			{fieldLabel: 'Active', name: 'active', xtype: 'checkbox', checked: true}
		]
	}]
});
