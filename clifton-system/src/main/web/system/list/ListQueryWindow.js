Clifton.system.list.ListQueryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'List',
	iconCls: 'list',
	height: 600,
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'List Query',
				items: [{
					xtype: 'formpanel',
					instructions: 'A list query populates its items by executing a specified query.  The query can return 2 or 3 columns, the first column is the value field (verified against the selected data type), the second column is the text field, and the third column is the tooltip field which is optional.',
					url: 'systemList.json',
					items: [
						{fieldLabel: 'List Name', name: 'name'},
						{fieldLabel: 'Data Type', name: 'dataType.name', hiddenName: 'dataType.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'systemDataTypeList.json', detailPageClass: 'Clifton.system.schema.DataTypeWindow'},
						{fieldLabel: 'Main Table', name: 'table.name', hiddenName: 'table.id', displayField: 'nameExpanded', xtype: 'combo', url: 'systemTableListFind.json'},
						{fieldLabel: 'Data Source', name: 'dataSource.name', hiddenName: 'dataSource.id', xtype: 'combo', url: 'systemDataSourceListFind.json', displayField: 'name', disableAddNewItem: true, detailPageClass: 'Clifton.system.schema.DataSourceWindow'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
						{xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true},
						{boxLabel: 'List Name is system defined (cannot be cleared: referenced by name in code or via meta data)', name: 'nameSystemDefined', xtype: 'checkbox'},
						{boxLabel: 'List is system defined (list items cannot be changed, added or deleted)', name: 'listSystemDefined', xtype: 'checkbox'},
						{fieldLabel: 'SQL', name: 'sqlStatement', xtype: 'textarea', allowBlank: false, anchor: '-35 -350', qtip: 'The query can optionally use a search pattern by including " = ?" parameter.  For example, "DECLARE @SearchPattern NVARCHAR(100) = ?" and then "WHERE @SearchPattern IS NULL OR AccountGroupName LIKE \'%\' + @SearchPattern + \'%\'"'},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'SystemList',
							hierarchyCategoryName: 'System List Tags'
						}
					],
					getSaveURL: function() {
						return 'systemListQuerySave.json';
					}
				}]
			},


			{
				title: 'Items',
				items: [{
					name: 'systemListItemListByList',
					xtype: 'gridpanel',
					instructions: 'The following items are associated with this list.  These items cannot be edited directly, but were determined from the SQL query executed for this list.',
					columns: [
						{header: 'Value', width: 100, dataIndex: 'value'},
						{header: 'Text', width: 100, dataIndex: 'text'},
						{header: 'Tooltip', width: 100, dataIndex: 'tooltip'}
					],
					getLoadParams: function() {
						return {'systemListId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
