Clifton.system.condition.ConditionWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'systemConditionPopulated.json',

	getClassName: function(config, entity) {
		const obj = entity || config.defaultData;
		if (obj) {
			if (obj.conditionEntry.entryType === 'CONDITION') {
				return 'Clifton.system.condition.SimpleConditionWindow';
			}
			return 'Clifton.system.condition.ComplexConditionWindow';
		}
		TCG.showError('Cannot determine condition entry type because requirement parameter was not specified.');
		return false;
	}

});
