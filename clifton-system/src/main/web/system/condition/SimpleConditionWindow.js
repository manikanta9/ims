Clifton.system.condition.SimpleConditionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Condition',
	height: 500,
	width: 800,
	iconCls: 'question',
	allowOpenFromModal: true,
	defaultDataIsReal: true,
	enableRefreshWindow: true,

	items: [
		{
			xtype: 'formpanel',
			url: 'systemConditionPopulated.json',
			table: 'SystemCondition',
			labelWidth: 140,
			getValidationBeanName: function() {
				return 'SystemCondition';
			},
			getSaveURL: function() {
				return 'systemConditionSave.json';
			},
			getSubmitParams: function() {
				if (TCG.isNull(this.getIdFieldValue())) { // new condition
					return {'conditionEntry.entryType': this.getDefaultData(this.getWindow()).conditionEntry.entryType};
				}
			},

			items: [
				{fieldLabel: 'Condition Name', name: 'name'},
				{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 80},
				{
					fieldLabel: 'Condition Bean', name: 'conditionEntry.bean.name', hiddenName: 'conditionEntry.bean.id', requestedProps: 'description', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Comparison', allowBlank: false,
					detailPageClass: 'Clifton.system.bean.BeanWindow',
					getDefaultData: function() {
						return {type: {group: {name: 'Comparison'}}};
					}
				},
				{name: 'conditionEntry.bean.description', xtype: 'textarea', height: 80, readOnly: true, submitValue: false},
				{fieldLabel: 'System Table', name: 'systemTable.name', hiddenName: 'systemTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow', qtip: 'Optional SystemTable that this condition is linked to. Used for grouping/filtering to better organize/find and associate conditions and where they apply.'},
				{boxLabel: 'This Condition is System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true, qtip: 'System Defined conditions cannot be created, deleted or have their name changed via UI'},
				{fieldLabel: 'True Override Message', name: 'messageOverrideForTrue', xtype: 'textarea', qtip: 'A message that can be set to replace the default message if the system condition is true.', height: 60},
				{fieldLabel: 'False Override Message', name: 'messageOverrideForFalse', xtype: 'textarea', qtip: 'A message that can be set to replace the default message if the system condition is false.', height: 60}
			]
		}
	]
});
