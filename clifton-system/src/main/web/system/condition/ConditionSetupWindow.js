Clifton.system.condition.ConditionSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'systemConditionSetupWindow',
	title: 'System Conditions',
	iconCls: 'question',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Conditions',
				items: [{
					xtype: 'gridpanel',
					name: 'systemConditionListFind',
					instructions: 'A system condition represents a logical condition that evaluates to either true or false for the specified bean.',
					wikiPage: 'IT/System+Conditions',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Condition Name', dataIndex: 'name', width: 200, defaultSortColumn: true},
						{header: 'Description', dataIndex: 'description', width: 300},
						{header: 'System Table', dataIndex: 'systemTable.name', width: 100, filter: {type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', searchFieldName: 'systemTableId'}},
						{header: 'Simple', dataIndex: 'simpleCondition', type: 'boolean', width: 30, qtip: 'Simple vs Grouping (AND or OR) condition'},
						{header: 'System', dataIndex: 'systemDefined', type: 'boolean', width: 30},
						{header: 'True Message Override', dataIndex: 'messageOverrideForTrue', hidden: true, tooltip: 'Message that replaces the default message if the system condition is true.', width: 300},
						{header: 'False Message Override', dataIndex: 'messageOverrideForFalse', hidden: true, tooltip: 'Message that replaces the default message if the system condition is false.', width: 300}
					],
					editor: {
						detailPageClass: 'Clifton.system.condition.ConditionWindow',
						getDefaultData: function(gridPanel) {
							return this.savedDefaultData;
						},
						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Add',
								tooltip: 'Create a new Condition of selected type',
								iconCls: 'add',
								menu: new Ext.menu.Menu({
									items: [
										{
											text: 'Simple Condition',
											tooltip: 'A simple condition that evaluates a single Comparison bean',
											iconCls: 'question',
											scope: this,
											handler: function() {
												this.openNewConditionWindow('CONDITION', gridPanel);
											}
										}, '-',
										{
											text: 'AND Condition',
											tooltip: 'Logical grouping of multiple entries using AND',
											iconCls: 'question',
											scope: this,
											handler: function() {
												this.openNewConditionWindow('AND', gridPanel);
											}
										},
										{
											text: 'OR Condition',
											tooltip: 'Logical grouping of multiple entries using OR',
											iconCls: 'question',
											scope: this,
											handler: function() {
												this.openNewConditionWindow('OR', gridPanel);
											}
										}
									]
								})
							});
							toolBar.add('-');
						},
						openNewConditionWindow: function(conditionType, gridPanel) {
							this.savedDefaultData = {
								conditionEntry: {
									entryType: conditionType
								}
							};
							this.openDetailPage(this.getDetailPageClass(), gridPanel);
						}
					}
				}]
			},


			{
				title: 'Condition Beans',
				items: [{
					xtype: 'gridpanel',
					name: 'systemBeanListFind',
					instructions: 'The following comparison beans are available for condition entries.',
					topToolbarSearchParameter: 'searchPattern',
					groupField: 'type.name',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Condition Type', width: 200, dataIndex: 'type.name', filter: {searchFieldName: 'typeId', type: 'combo', url: 'systemBeanTypeListFind.json?groupName=Comparison'}, hidden: true},
						{header: 'Bean Name', width: 200, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Modify Condition', width: 200, dataIndex: 'entityModifyCondition.name', filter: false, sortable: false}
					],
					getTopToolbarFilters: function(toolbar) {
						const filters = TCG.grid.GridPanel.prototype.getTopToolbarFilters.call(this, arguments);
						filters.push({fieldLabel: 'Condition Type', xtype: 'combo', name: 'beanTypeId', width: 250, listWidth: 400, url: 'systemBeanTypeListFind.json?groupName=Comparison', linkedFilter: 'type.name'});
						return filters;
					},
					getTopToolbarInitialLoadParams: function() {
						return {groupName: 'Comparison'};
					},
					editor: {
						detailPageClass: 'Clifton.system.bean.BeanWindow'
					}
				}]
			},


			{
				title: 'Condition Bean Types',
				items: [{
					xtype: 'gridpanel',
					name: 'systemBeanTypeListFind',
					instructions: 'The following comparison bean types are available for condition entries.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', dataIndex: 'name', width: 200, defaultSortColumn: true},
						{header: 'Class Name', dataIndex: 'className', width: 300},
						{header: 'Description', dataIndex: 'description', hidden: true, width: 500},
						{header: 'Modify Condition', dataIndex: 'entityModifyCondition.name', width: 150},
						{header: 'Singleton', dataIndex: 'singleton', type: 'boolean', width: 35},
						{header: 'Allow Dupes', dataIndex: 'duplicateBeansAllowed', type: 'boolean', width: 40}
					],
					getTopToolbarInitialLoadParams: function() {
						return {groupName: 'Comparison'};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.bean.TypeWindow'
					}
				}]
			}
		]
	}]
});
