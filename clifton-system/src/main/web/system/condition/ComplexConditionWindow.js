Clifton.system.condition.ComplexConditionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Condition',
	height: 570,
	width: 750,
	iconCls: 'question',
	layout: 'border',
	allowOpenFromModal: true,
	defaultDataIsReal: true,
	enableRefreshWindow: true,

	items: [
		{
			region: 'north',
			xtype: 'formpanel',
			labelWidth: 140,
			height: 300,
			url: 'systemConditionPopulated.json',
			table: 'SystemCondition',
			getValidationBeanName: function() {
				return 'SystemCondition';
			},
			getSaveURL: function() {
				return 'systemConditionSave.json';
			},
			getSubmitParams: function() {
				if (TCG.isNull(this.getIdFieldValue())) { // new condition
					return {'conditionEntry.entryType': this.getDefaultData(this.getWindow()).conditionEntry.entryType};
				}
			},

			listeners: {
				afterload: function() {
					if (TCG.isNotNull(this.getIdFieldValue())) { // existing condition
						const root = {};
						this.populateTreePanelData(root, this.getFormValue('conditionEntry'));

						const tree = this.ownerCt.items.get(1);
						tree.setRootNode(root);

						tree.animate = false;
						tree.expandAll();
						tree.animate = true;
					}
				}
			},

			populateTreePanelData: function(root, entry) {
				root.id = entry.id;
				root.nodeData = entry;
				if (entry.entryType === 'CONDITION') {
					root.text = entry.bean.name;
					root.leaf = true;
				}
				else {
					root.text = entry.entryType;
					const childList = entry.childEntryList;
					if (childList) {
						root.children = [];
						for (let i = 0; i < childList.length; i++) {
							const child = {};
							root.children.push(child);
							this.populateTreePanelData(child, childList[i]);
						}
					}
				}
			},

			items: [
				{fieldLabel: 'Condition Name', name: 'name'},
				{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 65},
				{fieldLabel: 'System Table', name: 'systemTable.name', hiddenName: 'systemTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow', qtip: 'Optional SystemTable that this condition is linked to. Used for grouping/filtering to better organize/find and associate conditions and where they apply.'},
				{boxLabel: 'This Condition is System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true, qtip: 'System Defined conditions cannot be created, deleted or have their name changed via UI'},
				{fieldLabel: 'True Override Message', name: 'messageOverrideForTrue', xtype: 'textarea', qtip: 'A message that can be set to replace the default message if the system condition is true.', height: 60},
				{fieldLabel: 'False Override Message', name: 'messageOverrideForFalse', xtype: 'textarea', qtip: 'A message that can be set to replace the default message if the system condition is false.', height: 60}
			]
		},


		{
			region: 'center',
			xtype: 'treepanel-advanced',
			rootVisible: true,
			root: {  // to be loaded later
				leaf: true
			},
			openDetailWindowAsModal: true,
			detailPageClass: 'Clifton.system.condition.ConditionEntryWindow',
			deleteURL: 'systemConditionEntryDelete.json',
			openDetailOnDblClick: function(node, e) {
				return (TCG.isNotNull(TCG.getValue('nodeData.bean', node.attributes)));
			},
			reload: function() {
				const w = this.getWindow();
				if (w.isModified()) {
					TCG.showInfo('Please save your changes to condition in order to see changes to entries.', 'Save Changes');
				}
				else {
					const fp = w.getMainFormPanel();
					fp.doLoad(undefined, {id: fp.getIdFieldValue()});
				}
			},
			onContextMenu: function(node, e) {
				const menu = TCG.isNotNull(TCG.getValue('nodeData.bean', node.attributes)) ? this.getSimpleEntryContextMenu() : this.getGroupingEntryContextMenu();
				if (menu) {
					menu.contextNode = node;
					menu.showAt(e.getXY());
				}
			},
			onContextMenuSelect: function(item) {
				const node = item.parentMenu.contextNode;
				const tree = node.getOwnerTree();
				tree.contextNode = node; // save so that can reload after changes
				let className = tree.detailPageClass;
				const id = Ext.isNumber(node.id) ? node.id : null;
				if (item.value === 'rf-open') {
					TCG.createComponent(className, {
						id: TCG.getComponentId(className, id),
						params: {id: id},
						modal: tree.openDetailWindowAsModal,
						openerCt: tree
					});
				}
				else if (item.value === 'rf-add-simple') {
					TCG.createComponent(className, {
						id: TCG.getComponentId(className),
						defaultData: {
							parent: {id: id},
							systemCondition: tree.getWindow().getMainForm().formValues
						},
						modal: tree.openDetailWindowAsModal,
						openerCt: tree
					});
				}
				else if (item.value === 'rf-add-simple-existing') {
					className = 'Clifton.system.condition.ConditionEntrySelectorWindow';
					TCG.createComponent(className, {
						id: TCG.getComponentId(className),
						defaultData: {
							parent: {id: id}
						},
						modal: tree.openDetailWindowAsModal,
						openerCt: tree
					});
				}
				else if (item.value === 'rf-add-and') {
					const loader = new TCG.data.JsonLoader({
						waitTarget: tree,
						params: {
							'parent.id': id,
							entryType: 'AND'
						},
						onLoad: function(record, conf) {
							tree.reload();
						}
					});
					loader.load('systemConditionEntrySave.json');
				}
				else if (item.value === 'rf-add-or') {
					const loader = new TCG.data.JsonLoader({
						waitTarget: tree,
						params: {
							'parent.id': id,
							entryType: 'OR'
						},
						onLoad: function(record, conf) {
							tree.reload();
						}
					});
					loader.load('systemConditionEntrySave.json');
				}
				else if (item.value === 'rf-remove') {
					Ext.Msg.show({
						title: 'Delete Entry?',
						msg: 'Would you like to delete "' + node.text + '" entry?',
						buttons: Ext.Msg.YESNO,
						fn: function(buttonId) {
							if (buttonId === 'yes') {
								const loader = new TCG.data.JsonLoader({
									waitTarget: tree,
									params: {id: id},
									onLoad: function(record, conf) {
										node.remove(true);
									}
								});
								loader.load(tree.deleteURL);
							}
						}
					});
				}
			},
			getGroupingEntryContextMenu: function() {
				if (!(this.groupingEntryContextMenu instanceof Ext.Component)) { // lazy initialization
					this.groupingEntryContextMenu = this.createComponent({
						items: [
							{value: 'rf-add-simple', text: 'Add Simple Entry - New', iconCls: 'add'},
							{value: 'rf-add-simple-existing', text: 'Add Simple Entry - Existing', iconCls: 'add'},
							{value: 'rf-add-and', text: 'Add Grouping Entry - AND', iconCls: 'add'},
							{value: 'rf-add-or', text: 'Add Grouping Entry - OR', iconCls: 'add'},
							{value: 'rf-remove', text: 'Delete Selected Entry', iconCls: 'remove'}
						],
						listeners: {
							itemclick: this.onContextMenuSelect
						}
					}, 'menu');
				}
				return this.groupingEntryContextMenu;
			},
			getSimpleEntryContextMenu: function() {
				if (!(this.simpleEntryContextMenu instanceof Ext.Component)) { // lazy initialization
					this.simpleEntryContextMenu = this.createComponent({
						items: [
							{value: 'rf-open', text: 'View Selected Entry', iconCls: 'info'},
							{value: 'rf-remove', text: 'Delete Selected Entry', iconCls: 'remove'}
						],
						listeners: {
							itemclick: this.onContextMenuSelect
						}
					}, 'menu');
				}
				return this.simpleEntryContextMenu;
			}
		}
	]
});
