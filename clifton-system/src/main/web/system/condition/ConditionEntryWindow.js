Clifton.system.condition.ConditionEntryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Condition Entry',
	iconCls: 'question',
	width: 800,
	height: 500,
	allowOpenFromModal: true,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formwithdynamicfields',
		url: 'systemConditionEntry.json',
		labelFieldName: 'bean.name',
		labelWidth: 170,

		dynamicTriggerFieldName: 'bean.type.name',
		dynamicTriggerValueFieldName: 'bean.type.id',
		dynamicFieldTypePropertyName: 'parameter',
		dynamicFieldListPropertyName: 'bean.propertyList',
		dynamicFieldsUrl: 'systemBeanPropertyTypeListByType.json',
		dynamicFieldsUrlParameterName: 'typeId',

		getWarningMessage: function() {
			return 'WARNING: Changes to the Bean will also affect all other places that use this Bean.';
		},
		getSaveURL: function() {
			return 'systemConditionEntryAndBeanSave.json';
		},
		getValidationBeanName: function() {
			return 'SystemConditionEntry';
		},

		items: [
			{xtype: 'hidden', name: 'entryType', value: 'CONDITION'},
			{xtype: 'hidden', name: 'parent.id'},
			{xtype: 'hidden', name: 'rootParent.id', submitValue: false},
			{xtype: 'linkfield', fieldLabel: 'Parent Condition', name: 'systemCondition.name', detailIdField: 'systemCondition.id', detailPageClass: 'Clifton.system.condition.ConditionWindow', submitDetailField: false, qtip: 'The parent system condition for this specific condition entry.'},
			{
				fieldLabel: 'Condition Type', name: 'bean.type.name', hiddenName: 'bean.type.id', xtype: 'combo',
				url: 'systemBeanTypeListFind.json?groupName=Comparison',
				detailPageClass: 'Clifton.system.bean.TypeWindow',
				disableAddNewItem: true,
				listeners: {
					// reset object bean property fields on changes to bean type (triggerField)
					select: function(combo, record, index) {
						combo.ownerCt.resetDynamicFields(combo, false);
					}
				}
			},
			{fieldLabel: 'Bean Name', name: 'bean.name'},
			{fieldLabel: 'Description', name: 'bean.description', xtype: 'textarea'},
			{xtype: 'label', html: '<hr/>'},
			{xtype: 'linkfield', fieldLabel: 'Bean Link', name: 'bean.name', detailIdField: 'bean.id', detailPageClass: 'Clifton.system.bean.BeanWindow', qtip: 'The link to the actual backing bean for this entry which is modified using the fields below. This can be used to modify the bean directly or to see other properties on the bean, such as other uses of the backing bean.'}
		],

		listeners: {
			afterload: function(form, isClosing) {
				form.setupParentConditionLink();
				form.setupBeanTypeLink();
			}
		},

		setupParentConditionLink: async function() {
			// Set up the parent condition field from loaded properties
			const rootParentField = this.getForm().findField('rootParent.id');
			if (rootParentField.getValue()) {
				const systemConditions = await TCG.data.getDataPromise('systemConditionListFind.json', this, {params: {conditionEntryId: rootParentField.getValue()}});
				// Guard-clause: Unexpected number of results; display none
				if (systemConditions.length !== 1) {
					console.warn('A non-one-to-one relationship was found for System Conditions to System Condition Entries.', systemConditions);
					return;
				}
				const systemCondition = systemConditions[0];
				this.setFormValue('systemCondition.name', systemCondition.name, true);
				this.setFormValue('systemCondition.id', systemCondition.id, true);
			}
		},

		setupBeanTypeLink: function() {
			// for created objects replace combo with linkfield: can't edit
			const triggerField = this.getForm().findField('bean.type.name');
			if (this.getIdFieldValue() > 0 && triggerField.xtype !== 'linkfield') {
				this.remove(triggerField, true);

				const formValues = this.getForm().formValues;
				this.add({name: 'bean.type.id', xtype: 'hidden', value: TCG.getValue('bean.type.id', formValues)});
				this.insert(5, {
					fieldLabel: triggerField.fieldLabel,
					name: 'bean.type.name',
					submitValue: false,
					xtype: 'linkfield',
					detailPageClass: 'Clifton.system.bean.TypeWindow',
					detailIdField: 'bean.type.id',
					value: TCG.getValue('bean.type.name', formValues)
				});
				try {
					this.doLayout();
				}
				catch (e) {
					// strange error due to removal of elements with allowBlank = false
				}
			}
		}
	}]
});
