Clifton.system.condition.ConditionEntrySelectorWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Condition Entry',
	iconCls: 'question',
	width: 600,
	height: 300,

	items: [{
		xtype: 'formpanel',
		url: 'systemConditionEntry.json',
		instructions: 'Please select existing Condition bean to use for this entry.',
		items: [
			{xtype: 'hidden', name: 'entryType', value: 'CONDITION'},
			{xtype: 'hidden', name: 'parent.id'},
			{
				fieldLabel: 'Condition Bean', name: 'bean.name', hiddenName: 'bean.id', displayField: 'name', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Comparison', allowBlank: false,
				detailPageClass: 'Clifton.system.bean.BeanWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Comparison'}}};
				}
			}
		]
	}]
});
