Ext.ns('Clifton.system', 'Clifton.system.audit', 'Clifton.system.bean', 'Clifton.system.common', 'Clifton.system.condition', 'Clifton.system.hierarchy', 'Clifton.system.lifecycle', 'Clifton.system.json.migration', 'Clifton.system.list', 'Clifton.system.schema', 'Clifton.system.upload', 'Clifton.system.warning', 'Clifton.system.usedby', 'Clifton.system.security');

TCG.use('Clifton.system.lifecycle.LifeCycleEventListGrid');

//use to link to simple upload window from generic upload window
Clifton.system.schema.SimpleUploadWindows = {};

Clifton.system.downloadSampleUploadFile = function(tableName, allRows, componentScope, params) {
	if (!params) {
		params = {};
	}
	if (tableName) {
		params.tableName = tableName;
	}
	if (allRows) {
		params.allRows = allRows;
	}
	if (!params.outputFormat) {
		params.outputFormat = 'xls';
	}
	TCG.downloadFile('systemUploadFileSampleDownload.json', params, componentScope);
};

Clifton.system.LinkedEntitySampleFileDownlaodToolbarItems = [
	{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File For Selected Table (Max 10 Rows of Data). The data in the sample will include the required columns for Linked Entities.',
		handler: function() {
			const formPanel = this.ownerCt.ownerCt.items.get(0);
			const params = {
				dtoClassForBinding: formPanel.dtoClassForBinding,
				enableValidatingBinding: true,
				disableValidatingBindingValidation: true,
				...formPanel.getSampleFileDownloadParams()
			};
			Clifton.system.downloadSampleUploadFile(formPanel.tableName, false, this, params);
		}
	},
	{
		text: 'Sample File (All Data)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File For Selected Table (Contains All (up to 500 rows) Existing Data). The data in the sample will include the required columns for Linked Entities.',
		handler: function() {
			const formPanel = this.ownerCt.ownerCt.items.get(0);
			const params = {
				dtoClassForBinding: formPanel.dtoClassForBinding,
				enableValidatingBinding: true,
				disableValidatingBindingValidation: true,
				...formPanel.getSampleFileDownloadParams()
			};
			Clifton.system.downloadSampleUploadFile(formPanel.tableName, true, this, params);
		}
	}
];

Clifton.system.upload.InvalidDataEncounteredItems = [
	{xtype: 'sectionheaderfield', header: 'If invalid data is encountered'},
	{
		xtype: 'radiogroup', columns: 1,
		items: [
			{boxLabel: 'Fail this upload and don\'t load any data.', name: 'partialUploadAllowed', inputValue: 'false', checked: true},
			{boxLabel: 'Skip rows with invalid data but upload valid rows.', name: 'partialUploadAllowed', inputValue: 'true'}
		]
	}
];

Clifton.system.upload.ExistingBeansItems = [
	{xtype: 'sectionheaderfield', header: 'If uploaded row already exists in the system'},
	{
		xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'existingBeans',
		items: [
			{fieldLabel: '', boxLabel: 'Skip upload for rows that already exist.', name: 'existingBeans', inputValue: 'SKIP', checked: true},
			{fieldLabel: '', boxLabel: 'Overwrite existing data with uploaded information. (Columns not included in the file will NOT overwrite existing data in the database)', name: 'existingBeans', inputValue: 'UPDATE'},
			{fieldLabel: '', boxLabel: 'Insert all data to improve upload performance (Fails if a constraint is violated).', name: 'existingBeans', inputValue: 'INSERT'}
		]
	}
];

Clifton.system.LinkedEntityFileUploadFormPanel = Ext.extend(TCG.form.FormPanel, {
	xtype: 'formpanel',
	fileUpload: true,
	labelWidth: 200,
	dtoClassForBinding: 'com.clifton.system.upload.SystemUploadCommand',
	defaultBeanPropertiesToExclude: [],

	getSampleFileDownloadParams: function() {
		const params = this.getFormValuesFormatted();
		delete params['file'];
		return params;
	},

	initComponent: function() {
		const items = this.items || [];
		items.push(this.fileItems);
		items.push(this.uploadCommandItems);
		items.push(this.linkedEntityItems);
		this.items = items;
		TCG.callSuper(this, 'initComponent', arguments);
	},

	fileItems: [
		{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false}
	],

	uploadCommandItems: [], // used for form values specific to the command being used

	linkedEntityItems: [
		{
			xtype: 'fieldset', title: 'Bean Properties to Exclude', collapsible: true, collapsed: true, labelWidth: 125,
			items: [
				{
					xtype: 'label', html: 'These properties will not be included in sample files and will not be processed if included in the upload file. These are useful for file uploads that have dynamically generated columns based on a linked entity. ' +
						'For example, Business Contact Assignments linked to Investment Accounts exclude the Issuing Company despite it being a natural key because the Investment Account Number is the desired way to identify the Investment Account.<br><br>'
				},
				{
					fieldLabel: 'Bean Property', xtype: 'listfield', allowBlank: true, name: 'beanPropertiesToExclude',
					controlConfig: {
						xtype: 'textfield', dataIndex: 'beanProperty', height: '20'
					},
					initComponent: function() {
						this.defaultItems = TCG.getParentFormPanel(this).defaultBeanPropertiesToExclude;
						TCG.callParent(this, 'initComponent', arguments);
					}
				}
			]
		},
		{
			xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
			items: [
				{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
			]
		}
	],

	getSaveParams: function() {
		return {
			dtoClassForBinding: this.dtoClassForBinding,
			enableValidatingBinding: true,
			disableValidatingBindingValidation: true
		};
	},

	getSaveURL: function() {
		return 'systemUploadFileUpload.json';
	}
});
Ext.reg('system-linked-entity-file-upload-formpanel', Clifton.system.LinkedEntityFileUploadFormPanel);

Clifton.system.schema.TableWindowAdditionalTabs = [];

Clifton.system.schema.ColumnWindowAdditionalTabs = [{
	title: 'Audit Log',
	items: [{
		xtype: 'gridpanel',
		name: 'systemAuditEventListFind',
		instructions: 'Audit Log contains information about field level changes (insert/update/delete) for fields that have auditing enabled. The following is the audit trail history for this column.',
		dataTable: true,
		columns: [
			// Used for Drill Down Lookup Support
			{header: 'ColumnID', dataIndex: 'systemColumnId', type: 'int', hidden: true, sortable: false},
			{header: 'MainPKFieldID', dataIndex: 'mainPKFieldId', type: 'int', hidden: true, filter: {searchFieldName: 'entityId'}, sortable: false},
			{header: 'ParentPKFieldID', dataIndex: 'parentPKFieldId', type: 'int', hidden: true, filter: {searchFieldName: 'parentEntityId'}, sortable: false, useNull: true},

			{header: 'Date', dataIndex: 'auditDate', width: 150},
			{header: 'User', dataIndex: 'securityUserId', filter: {type: 'combo', displayField: 'userName', url: 'securityUserListFind.json'}, width: 100},
			{header: 'Action', dataIndex: 'auditTypeId', filter: {type: 'list', options: [[1, 'insert'], [2, 'update'], [3, 'delete']]}, width: 100},
			{header: 'Table', dataIndex: 'systemTableId', hidden: true, filter: {type: 'combo', url: 'systemTableListFind.json', displayField: 'labelExpanded'}, width: 200},
			{header: 'Field', dataIndex: 'columnName', hidden: true, width: 200},
			{header: 'Label', dataIndex: 'label', width: 250},
			{header: 'Old Value', filter: false, sortable: false, width: 200},
			{header: 'New Value', filter: false, sortable: false, width: 200}
		],
		getLoadParams: function(firstLoad) {
			if (firstLoad) {
				// default to last 10 days
				this.setFilterValue('auditDate', {'after': new Date().add(Date.DAY, -10)});
			}
			return {
				readUncommittedRequested: true,
				systemColumnId: this.getWindow().getMainFormId()
			};
		},
		editor: {ptype: 'system-audit-grid-editor'}
	}]
}];

// Depending on the Table of the FKFieldID - may be passing a Short or and Integer
// Need to call the correct method so it maps it to the correct object
Clifton.system.GetCustomColumnValue = function(fkFieldId, tableName, columnGroupName, columnName, returnGlobalDefaultIfNull) {
	if (!fkFieldId) {
		return undefined;
	}
	const url = 'systemColumnValueForEntityByTable.json';
	const cacheKey = tableName + '.' + columnGroupName + '.' + columnName + '.' + fkFieldId;
	let result = TCG.CacheUtils.get('SYSTEM_COLUMN_VALUE', cacheKey);
	if (!result) {
		result = TCG.getResponseText(url + '?tableName=' + tableName + '&entityId=' + fkFieldId + '&columnGroupName=' + columnGroupName + '&columnName=' + columnName + '&returnGlobalDefaultIfNull=' + returnGlobalDefaultIfNull);
		TCG.CacheUtils.put('SYSTEM_COLUMN_VALUE', result, cacheKey);
	}
	return result;
};

/**
 * Returns a Promise for the result of the custom column value arguments.
 *
 * @param fkFieldId the entity ID the value is assigned
 * @param tableName the table name of the entity
 * @param columnGroupName the column group name
 * @param columnName the custom column name
 * @param returnGlobalDefaultIfNull true to return the global default if no value is defined
 * @returns {*} a promise that will resolve the custom value
 */
Clifton.system.GetCustomColumnValuePromise = function(fkFieldId, tableName, columnGroupName, columnName, returnGlobalDefaultIfNull) {
	if (!fkFieldId) {
		return Promise.resolve(void 0);
	}
	const cacheKey = tableName + '.' + columnGroupName + '.' + columnName + '.' + fkFieldId;
	const result = TCG.CacheUtils.get('SYSTEM_COLUMN_VALUE', cacheKey);
	if (result) {
		return Promise.resolve(result);
	}
	const params = {
		tableName: tableName,
		entityId: fkFieldId,
		columnGroupName: columnGroupName,
		columnName: columnName,
		returnGlobalDefaultIfNull: returnGlobalDefaultIfNull
	};
	return TCG.data.getDataValuePromise('systemColumnValueForEntityByTable.json', this, null, params)
		.then(result => {
			TCG.CacheUtils.put('SYSTEM_COLUMN_VALUE', result, cacheKey);
			return result;
		});
};

Clifton.system.SystemColumnDatedValueGrid = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	name: 'systemColumnDatedValueListFind',
	instructions: 'The following custom data fields are associated with this entity and can have their values change over time using Start/End Date fields.',
	additionalPropertiesToRequest: 'column.columnGroup.table.id|column.columnGroup.table.name|column.columnGroup.id',
	groupField: 'column.columnGroup.label',
	groupTextTpl: '{values.group}',
	remoteSort: true,
	includeTopToolbarActiveOnDateFilter: true,
	getTopToolbarFilters: function(toolbar) {
		if (this.includeTopToolbarActiveOnDateFilter) {
			return [{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}];
		}
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Column Group', width: 120, dataIndex: 'column.columnGroup.label', hidden: true},
		{header: 'Column', width: 100, dataIndex: 'column.label'},
		{header: 'Data Type', width: 100, dataIndex: 'column.dataType.name', hidden: true},
		{
			header: 'Value', width: 200, dataIndex: 'text', filter: {searchFieldName: 'valueOrText'},
			renderer: function(v, c, r) {
				if (r.data['column.dataType.name'] === 'BOOLEAN') {
					if (TCG.isNull(v)) {
						return '';
					}
					if (TCG.isTrue(v) || v === 'Yes') {
						return 'Yes';
					}
					return 'No';
				}
				if (r.data['column.dataType.name'] === 'DECIMAL' || r.data['column.dataType.name'] === 'INTEGER') {
					return TCG.numberFormat(v, '0,000', true);
				}
				return v;
			}
		},
		{header: 'Start Date', width: 65, dataIndex: 'startDate'},
		{header: 'End Date', width: 65, dataIndex: 'endDate'}
	],
	editor: {
		detailPageClass: 'Clifton.system.schema.ColumnDatedValueWindow',
		getDefaultData: function(gridPanel) {
			const grid = this.grid;
			const sm = grid.getSelectionModel();
			let table;
			const columnGroup = {};

			if (sm.getCount() === 1) {
				table = sm.getSelected().json.column.columnGroup.table;
				columnGroup.id = sm.getSelected().json.column.columnGroup.id;
				columnGroup.label = sm.getSelected().json.column.columnGroup.label;
			}
			else {
				table = TCG.data.getData('systemTableByName.json?tableName=' + gridPanel.tableName, gridPanel);
			}

			return {
				fkFieldId: this.getWindow().getMainFormId(),
				column: {
					table: table,
					columnGroup: columnGroup
				}
			};
		}
	},
	getLoadParams: function(firstLoad) {
		let dateValue;
		const t = this.getTopToolbar();
		const bd = TCG.getChildByName(t, 'activeOnDate');
		if (this.includeTopToolbarActiveOnDateFilter && firstLoad) {
			// default to today
			if (bd.getValue() === '') {
				dateValue = (new Date()).format('m/d/Y');
				bd.setValue(dateValue);
			}
		}
		const result = {
			tableName: this.tableName,
			fkFieldId: this.getWindow().getMainFormId()
		};
		if (this.includeTopToolbarActiveOnDateFilter && bd.getValue() !== '') {
			dateValue = (bd.getValue()).format('m/d/Y');
			result.activeOnDate = dateValue;
		}
		return result;
	}
});
Ext.reg('system-column-dated-value-grid', Clifton.system.SystemColumnDatedValueGrid);

Clifton.system.audit.AuditGridPanel = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	parentTable: false, // specifies whether lookup should be made by parent entity id or by entity id (default)
	childTables: undefined, // optional list of comma delimited child table names to be included in results by parent id
	name: 'systemAuditEventListFind',
	dataTable: true,
	instructions: 'Audit Log contains information about field level changes (insert/update/delete) for this entity.',
	useBufferView: false,
	rowSelectionModel: 'multiple',
	columns: [
		// Used for Drill Down Lookup Support
		{header: 'ColumnID', dataIndex: 'columnId', width: 50, type: 'int', hidden: true, filter: false, sortable: false},
		{header: 'MainPKFieldID', dataIndex: 'mainPKFieldId', width: 50, type: 'int', hidden: true, filter: {searchFieldName: 'entityId'}, sortable: false},
		{header: 'ParentPKFieldID', dataIndex: 'parentPKFieldId', width: 50, type: 'int', hidden: true, filter: {searchFieldName: 'parentEntityId'}, sortable: false, useNull: true},
		{header: 'Date', dataIndex: 'auditDate', width: 75, defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'User', dataIndex: 'securityUserId', width: 75, filter: {type: 'combo', displayField: 'userName', url: 'securityUserListFind.json'}},
		{header: 'Action', dataIndex: 'auditTypeId', width: 50, filter: {type: 'list', options: [[1, 'insert'], [2, 'update'], [3, 'delete']]}},
		{header: 'Table', dataIndex: 'systemTableId', width: 75, hidden: true, filter: {type: 'combo', url: 'systemTableListFind.json', displayField: 'labelExpanded'}},
		{header: 'Field', dataIndex: 'columnName', width: 75},
		{
			header: 'Label', dataIndex: 'label', hidden: true, width: 150,
			renderer: function(v, metaData, r) {
				return TCG.renderText(v);
			}
		},
		{
			header: 'Old Value', filter: false, sortable: false, width: 150,
			renderer: function(v, metaData, r) {
				if (TCG.isNotNull(v)) {
					return TCG.renderText(v);
				}
			}
		},
		{
			header: 'New Value', filter: false, sortable: false, width: 150,
			renderer: function(v, metaData, r) {
				if (TCG.isNotNull(v)) {
					return TCG.renderText(v);
				}
			}
		}
	],

	initComponent: function() {
		Clifton.system.audit.AuditGridPanel.superclass.initComponent.call(this, arguments);
		// show table and label fields if including child tables
		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('systemTableId'), !(this.parentTable === true || this.childTables !== undefined));
		cm.setHidden(cm.findColumnIndex('label'), !(this.parentTable === true || this.childTables !== undefined));
	},
	getLoadParams: function(firstLoad) {
		// always filter by table and entity id
		const params = {tableName: this.tableName};
		params[this.parentTable ? 'parentEntityId' : 'entityId'] = this.getEntityId();
		if (this.childTables) {
			params.childTables = this.childTables;
		}
		return params;
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},
	editor: {ptype: 'system-audit-grid-editor'}
});
Ext.reg('system-audit-grid', Clifton.system.audit.AuditGridPanel);


Clifton.system.audit.AuditGridPanelEditor = Ext.extend(TCG.grid.GridEditor, {
	drillDownOnly: true,
	startEditing: async function(grid, rowIndex, evt) {
		if (evt && TCG.isActionColumn(evt.target)) {
			return; // skip because this is action column
		}

		// Get system column
		const gridPanel = this.getGridPanel();
		const row = grid.store.data.items[rowIndex];
		const column = await TCG.data.getDataPromiseUsingCaching(`systemColumn.json?id=${row.data.columnId}`, grid, `system.column.${row.data.columnId}`);
		if (!column) {
			TCG.showError(`Cannot find SystemColumn with id = ${row.data.columnId}`);
			return;
		}

		// Identify and open instance of detail screen
		if (TCG.isNotBlank(column.table.detailScreenClass)) {
			// Use detail screen class directly from table
			gridPanel.editor.openDetailPage(column.table.detailScreenClass, gridPanel, row.data.mainPKFieldId, row);
		}
		else if (row.data.parentPKFieldId) {
			// Use detail screen class from parent table
			const parentTableName = await TCG.data.getDataValuePromise(`migrationParentTableName.json?tableName=${column.table.name}`, grid);
			if (!parentTableName) {
				TCG.showError(`Detail screen class is not defined for table "${column.table.name}". Drill down is unavailable.`, 'Drill Down Unavailable.');
				return;
			}
			const parentTable = await TCG.data.getDataPromise(`systemTableByName.json?tableName=${parentTableName}`, grid);
			if (TCG.isBlank(parentTable.detailScreenClass)) {
				TCG.showError(`Detail screen class is not defined for table "${column.table.name}" and its parent. Drill down is unavailable.`, 'Drill Down Unavailable.');
				return;
			}
			gridPanel.editor.openDetailPage(parentTable.detailScreenClass, gridPanel, row.data.parentPKFieldId, row);
		}
		else {
			// No detail screen available
			TCG.showError(`Detail screen class is not defined for table "${column.table.name}". Drill down is unavailable.`, 'Drill Down Unavailable.');
		}
	},
	addEditButtons: function(toolBar, gridPanel) {
		toolBar.add({
			text: 'View Differences',
			tooltip: 'View differences between old value and new value. Select 2 rows to view differences between Old Value of older row and New Value of newer row.',
			iconCls: 'diff',
			scope: this,
			handler: function() {
				const sm = gridPanel.grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('You must select a row to view differences.', 'No Row Selected.');
				}
				else if (sm.getCount() > 2) {
					TCG.showError('You can only select 1 or 2 rows to view differences.', 'Too Many Rows Selected.');
				}
				else {
					let olderRow = sm.getSelections()[0];
					let newerRow = olderRow;
					if (sm.getCount() === 2) {
						newerRow = sm.getSelections()[1];
						if (olderRow.data['auditDate'] > newerRow.data['auditDate']) {
							olderRow = sm.getSelections()[1];
							newerRow = sm.getSelections()[0];
						}
					}
					TCG.showDifferences(olderRow.data['Old Value'], newerRow.data['New Value']);
				}
			}
		});
		toolBar.add('-');
	}
});
Ext.preg('system-audit-grid-editor', Clifton.system.audit.AuditGridPanelEditor);


Clifton.system.usedby.UsedByGridPanel = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	name: 'systemUsedByEntityList',
	instructions: 'This entity is used by the following entities in the system.  Note: Some references are purposely excluded.',

	groupField: 'label',
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Entities" : "Entity"]})',

	columns: [
		{header: 'Relationship Label', dataIndex: 'label', hidden: true},
		{header: 'Table', dataIndex: 'column.table.label', hidden: true},
		{header: 'ColumnID', dataIndex: 'column.id', hidden: true},
		{header: 'Column', dataIndex: 'column.label', hidden: true},
		{header: 'Entity', dataIndex: 'usedByLabel', width: 200},
		{header: 'ID', dataIndex: 'usedById', hidden: true},
		{header: 'Table Class', width: 40, dataIndex: 'column.table.detailScreenClass', hidden: true},
		{
			header: 'View', width: 40, dataIndex: '', align: 'center',
			renderer: function(value, metaData, r) {
				if (r.data['usedById'] !== '' && r.data['column.table.detailScreenClass'] !== '') {
					return TCG.renderActionColumn('view', 'View', 'View this entity.');
				}
				else if (r.data['usedById'] === '' && r.data['column.id'] !== '') {
					return TCG.renderActionColumn('list', 'View All', 'View full list of used by entities for this relationship');
				}
				return 'N/A';
			}
		}
	],
	getLoadParams: function() {
		return {
			tableName: this.tableName,
			entityId: this.getEntityId()
		};
	},

	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const row = grid.store.data.items[rowIndex];
					const gridPanel = grid.ownerGridPanel;
					if (row.json.usedById && row.json.usedById !== '') {
						const clz = row.json.column.table.detailScreenClass;
						const id = row.json.usedById;
						const cmpId = TCG.getComponentId(clz, id);

						TCG.createComponent(clz, {
							id: cmpId,
							params: {id: id},
							openerCt: gridPanel
						});
					}
					else if (row.json.column.id !== '') {
						const clz = 'Clifton.system.usedby.UsedByGridPanelColumnWindow';
						const columnId = row.json.column.id;
						const cmpId = TCG.getComponentId(clz, columnId);

						TCG.createComponent(clz, {
							id: cmpId,
							params: {columnId: columnId, entityId: gridPanel.getEntityId()},
							openerCt: gridPanel
						});

					}

				}
			}
		}
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	}
});
Ext.reg('system-usedby-grid', Clifton.system.usedby.UsedByGridPanel);


Clifton.system.usedby.UsedByGridPanelColumnWindow = Ext.extend(TCG.app.Window, {
	title: 'Entity Used in Column',
	iconCls: 'grid',
	width: 700,

	items: [{
		xtype: 'gridpanel',
		name: 'systemUsedByEntityListForColumn',
		instructions: 'This entity is used by the following entities in the system for the selected column.',
		columns: [
			{header: 'Relationship Label', dataIndex: 'label', hidden: true},
			{header: 'Table', dataIndex: 'column.table.label', hidden: true},
			{header: 'Column', dataIndex: 'column.label', hidden: true},
			{header: 'Entity', dataIndex: 'usedByLabel', width: 200},
			{header: 'ID', dataIndex: 'usedById', hidden: true},
			{header: 'Table Class', width: 40, dataIndex: 'column.table.detailScreenClass', hidden: true},
			{
				header: 'View', width: 40, dataIndex: '', align: 'center',
				renderer: function(value, metaData, r) {
					if (r.data['usedById'] !== '' && r.data['column.table.detailScreenClass'] !== '') {
						return TCG.renderActionColumn('view', 'View', 'View this entity.');
					}
					return 'N/A';
				}
			}
		],
		getLoadParams: function() {
			return {
				columnId: this.getWindow().params.columnId,
				entityId: this.getWindow().params.entityId
			};
		},

		gridConfig: {
			listeners: {
				'rowclick': function(grid, rowIndex, evt) {
					if (TCG.isActionColumn(evt.target)) {
						const row = grid.store.data.items[rowIndex];
						const clz = row.json.column.table.detailScreenClass;
						const id = row.json.usedById;
						const gridPanel = grid.ownerGridPanel;
						const cmpId = TCG.getComponentId(clz, id);
						TCG.createComponent(clz, {
							id: cmpId,
							params: {id: id},
							openerCt: gridPanel
						});
					}
				}
			}
		},
		getEntityId: function() {
			return this.getWindow().getMainFormId();
		}

	}]
});
Ext.reg('system-usedby-column-grid', Clifton.system.usedby.UsedByGridPanelColumnWindow);


Clifton.system.hierarchy.FolderTreePanel = Ext.extend(TCG.tree.AdvancedTreePanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	hierarchyCategoryName: undefined, // Optional to limit to a specific hierarchy category
	ddGroup: 'SAME-AS-DD-SOURCE',
	itemComponentId: 'REQUIRED-ID', // component that has the items managed by these folders: gridpanel, etc.

	title: 'Folders',
	rootNodeText: 'Folders',
	rootVisible: true,
	enableDrop: true,
	detailPageClass: 'Clifton.system.hierarchy.HierarchyWindow',
	deleteURL: 'systemHierarchyDelete.json',

	configureDefaultDataForAdd: function(dd) {
		if (this.hierarchyCategoryName) {
			delete dd.tableName;
			dd.categoryName = this.hierarchyCategoryName;
		}
	},

	initComponent: function() {
		let url = 'systemHierarchyListFind.json?tableName=' + this.tableName;
		if (this.hierarchyCategoryName) {
			url += '&categoryName=' + this.hierarchyCategoryName;
		}
		this.loader = new TCG.tree.JsonTreeLoader({
			dataUrl: url
		});
		this.root = {
			nodeType: 'async',
			text: this.rootNodeText
		};

		Clifton.system.hierarchy.FolderTreePanel.superclass.initComponent.call(this);
	},

	// component that has the items managed by these folders: gridpanel, etc.
	getItemComponent: function() {
		return Ext.getCmp(this.itemComponentId);
	},

	onNodeSelected: function(id) {
		const grid = this.getItemComponent();
		grid.getLoadParams = function() {
			return TCG.isNull(id) ? {} : {hierarchyId: id};
		};
		grid.reload();
	},

	listeners: {
		beforenodedrop: function(dropEvent) {
			const tree = this;
			let oldFolderId = this.getSelectionModel().getSelectedNode();
			if (TCG.isNotNull(oldFolderId)) {
				oldFolderId = Ext.isNumber(oldFolderId.id) ? oldFolderId.id : null;
			}

			let newFolderId = dropEvent.target.id;
			if (!Ext.isNumber(newFolderId)) {
				newFolderId = null;
			}
			const items = dropEvent.data.selections;
			Ext.Msg.show({
				title: 'Move Item(s)?',
				msg: 'Would you like to move selected item(s) to "' + dropEvent.target.attributes.text + '"folder?',
				buttons: Ext.Msg.YESNO,
				fn: function(buttonId) {
					if (buttonId === 'yes') {
						let movedCount = 0;
						for (let i = 0; i < items.length; i++) {
							const loader = new TCG.data.JsonLoader({
								waitTarget: tree,
								params: {
									tableName: tree.tableName,
									fkFieldId: items[i].id,
									oldHierarchyId: oldFolderId,
									newHierarchyId: newFolderId
								},
								onLoad: function(record, conf) {
									movedCount++;
									if (movedCount === items.length) { // refresh after all items were moved
										const grid = tree.getItemComponent();
										grid.reload();
									}
								}
							});
							loader.load('systemHierarchyLinkMove.json');

						}
					}
				},
				scope: this
			});
		}
	}
});
Ext.reg('system-hierarchy-folders', Clifton.system.hierarchy.FolderTreePanel);


Clifton.system.hierarchy.HierarchyGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'systemHierarchyListFind',
	xtype: 'gridpanel',
	instructions: 'The following hierarchies are associated with this hierarchy category.',
	topToolbarSearchParameter: 'searchPattern',
	additionalPropertiesToRequest: 'level',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Value', width: 30, dataIndex: 'value', hidden: true},
		{
			header: 'Name', width: 100, dataIndex: 'name',
			renderer: function(v, metaData, r) {
				if (r.json.level === 1) {
					return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
				}
				if (r.json.level === 2) {
					return '<div style="FONT-WEIGHT: bold; COLOR: #777777; PADDING-LEFT: 15px">' + v + '</div>';
				}
				const padding = (r.json.level - 1) * 15;
				if ((r.json.level % 2) === 0) {
					return '<div style="PADDING-LEFT: ' + padding + 'px">' + v + '</div>';
				}
				return '<div style="COLOR: #330027; FONT-STYLE: italic; PADDING-LEFT: ' + padding + 'px">' + v + '</div>';
			}

		},
		{header: 'Description', width: 250, dataIndex: 'description'},
		{header: 'Order', width: 30, dataIndex: 'order', type: 'int', useNull: true, hidden: true}
	],
	editor: {
		detailPageClass: 'Clifton.system.hierarchy.HierarchyWindow',
		deleteURL: 'systemHierarchyDelete.json',
		getDefaultData: function(gridPanel) { // defaults category id for the detail page
			return {
				category: gridPanel.getHierarchyCategory()
			};
		}
	},
	getHierarchyCategory: function() {
		return this.getWindow().getMainForm().formValues;
	},
	getTopToolbarInitialLoadParams: function() {
		return {
			'categoryId': TCG.getValue('id', this.getHierarchyCategory()),
			ignoreNullParent: true
		};
	}
});
Ext.reg('system-hierarchy-grid', Clifton.system.hierarchy.HierarchyGridPanel);


Clifton.system.schema.CustomColumnTypes = [
	['COLUMN_VALUE', 'COLUMN_VALUE', 'Standard Custom Columns whose values are stored in SystemColumnValue table'],
	['COLUMN_DATED_VALUE', 'COLUMN_DATED_VALUE', 'Dated Custom Columns whose values are stored in SystemColumnDatedValue table.  Each value applies to a specific date range.'],
	['COLUMN_JSON_VALUE', 'COLUMN_JSON_VALUE', 'Json Custom Columns are like standard custom columns except their values are stored in a single standard column on the table as one JSON string value instead of the separate system column value table.']
];

Clifton.system.schema.FormPanelWithCustomFields = Ext.extend(TCG.form.FormPanelWithDynamicFields, {
	columnGroupName: 'OVERRIDE_ME',
	dynamicFieldTypePropertyName: 'column',
	dynamicFieldListPropertyName: 'columnValueList',
	dynamicFieldsUrl: 'systemColumnCustomConfig.json?requestedMaxDepth=4',
	dynamicFieldsUrlParameterName: 'linkedValue',
	dynamicTriggerFieldValueOverride: undefined,
	group: undefined,

	initComponent: function() {
		Clifton.system.schema.FormPanelWithCustomFields.superclass.initComponent.apply(this, arguments);

		const field = this.getForm().findField('columnGroupName', true);
		if (TCG.isNull(field)) {
			this.add({name: 'columnGroupName', xtype: 'hidden', value: this.columnGroupName});
		}
	},

	onRender: function() {
		if (this.columnGroupName === 'OVERRIDE_ME') {
			const field = this.getForm().findField('columnGroupName', true);
			if (TCG.isNotNull(field)) {
				if (field.getValue()) {
					this.columnGroupName = field.getValue();
				}
				else {
					this.columnGroupName = this.getWindow().params['columnGroupName'];
					field.setValue(this.columnGroupName);
				}
			}
			else {
				TCG.showError('Unable to determine column group name for this form');
			}
		}
		this.group = TCG.data.getData('systemColumnGroupByName.json?name=' + this.columnGroupName, this, 'system.schema.group.' + this.columnGroupName);

		// No group = no custom fields
		let triggerField = undefined;
		if (this.group) {
			this.dynamicTriggerValueFieldName = this.group.linkedBeanProperty;
			this.dynamicTriggerFieldName = this.group.linkedBeanProperty;

			if (this.dynamicTriggerFieldName) {
				triggerField = this.getForm().findField(this.dynamicTriggerFieldName);
			}

			this.on('afterload', function(form, isClosing) {
				// on first bean load we get bean type and need to reset corresponding bean properties
				if (!isClosing) {
					this.resetDynamicFields(triggerField, true);
				}
			}, this);
		}

		if (triggerField) {
			if (TCG.isEquals(triggerField.xtype, 'combo')) {
				triggerField.addListener('select', function(field, value) {
					const p = TCG.getParentFormPanel(field);
					p.resetDynamicFields(triggerField, false);
				});
			}
			else {
				triggerField.addListener('change', function(field, value) {
					const p = TCG.getParentFormPanel(field);
					p.resetDynamicFields(triggerField, false);
				});
			}
		}

		TCG.form.FormPanelWithDynamicFields.superclass.onRender.apply(this, arguments);
	},

	afterRenderPromise: function(fp) {
		let triggerField = undefined;
		if (fp.group) {
			fp.dynamicTriggerValueFieldName = fp.group.linkedBeanProperty;
			fp.dynamicTriggerFieldName = fp.group.linkedBeanProperty;

			if (this.dynamicTriggerFieldName) {
				triggerField = fp.getForm().findField(fp.dynamicTriggerFieldName);
			}

			// new entity with defaulted type (triggerField): reset corresponding properties
			if (!triggerField || triggerField.getValue() !== '') {
				fp.resetDynamicFields(triggerField, true);
			}
		}
	},

	getDynamicTriggerFieldValue: function() {
		if (TCG.isNotBlank(this.dynamicTriggerFieldValueOverride)) {
			return this.dynamicTriggerFieldValueOverride;
		}
		const params = this.getWindow().params;
		if (params && TCG.isNotBlank(params.dynamicTriggerFieldValueOverride)) {
			return params.dynamicTriggerFieldValueOverride;
		}
		if (this.dynamicTriggerValueFieldName) {
			const f = this.getForm().findField(this.dynamicTriggerValueFieldName);
			return f ? f.getValue() : TCG.getValue(this.dynamicTriggerValueFieldName, this.getForm().formValues);
		}
		return undefined;
	},


	setDynamicFieldsValues: function() {
		// if loaded, then trigger field value is the same as corresponding loaded value
		const form = this.getForm();
		for (let i = 0; i < this.dynamicFields.length; i++) {
			let field = this.dynamicFields[i];
			if (TCG.isNotNull(field) && field.isFormField) {
				if (field.name.indexOf('_') === -1) {
					// dynamic field that points to real field name (label, etc.)
					let v = this.getFormValue(field.name);
					if (TCG.isNotNull(v)) {
						field.setValue(v);
						if (form && form.trackResetOnLoad) {
							field.originalValue = field.getValue(); // in case setValue reformatted the field
						}
						if (field.detailIdField) {
							v = this.getFormValue(field.detailIdField);
							if (TCG.isNotNull(v)) {
								field = this.getForm().findField(field.detailIdField);
								if (TCG.isNotNull(field)) {
									field.setValue(v);
									if (form && form.trackResetOnLoad) {
										field.originalValue = field.getValue(); // in case setValue reformatted the field
									}
								}
							}
						}
					}
				}
				else {
					const fieldValue = this.getDynamicPropertyFromField(field);
					let value;
					if (TCG.isNull(fieldValue)) {
						value = '';
					}
					else if (field.xtype === 'combo') {
						value = fieldValue;
					}
					else {
						value = fieldValue.value;
					}
					field.setValue(value);
					if (form && form.trackResetOnLoad) {
						field.originalValue = field.getValue(); // mark as non-dirty
					}
				}
			}
		}
	},

	getDynamicPropertyFromField: function(field) {
		const typeId = field.name.substring(field.name.indexOf('_') + 1);
		let fieldValue = null;
		const savedProps = this.fieldValueList;
		if (savedProps) {
			for (let j = 0; j < savedProps.length; j++) {
				const p = savedProps[j];
				if (TCG.isEquals(TCG.getValue(this.dynamicFieldTypePropertyName + '.id', p), typeId)) {
					fieldValue = p;
					break;
				}
			}
		}
		if (TCG.isNull(fieldValue)) {
			if (field.custom_defaultValue) {
				fieldValue = {'value': field.custom_defaultValue, 'text': field.custom_defaultText};
			}
		}
		return fieldValue;
	},

	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},


	// call-back because get is asynchronous
	getDynamicFields: function(triggerField, callback) {
		const fp = this;
		const params = {};
		params['linkedValue'] = this.getDynamicTriggerFieldValue();
		params['columnGroupName'] = fp.columnGroupName;
		params['entityId'] = fp.getEntityId();
		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			waitMsg: 'Loading fields...',
			params: params,
			onLoad: function(record, conf) {
				fp.fieldValueList = record.columnValueList;
				const fieldRecords = record.columnList;
				if (fieldRecords) {
					const fields = [];
					for (let i = 0; i < fieldRecords.length; i++) {
						const field = fp.getDynamicFieldFromRecord(fp, fieldRecords[i], i);
						if (field) {
							fields.push(field);
						}
					}

					callback.apply(fp, [fields]);
				}
			}
		});
		loader.load(this.dynamicFieldsUrl);
	},

	// returns form field for the specified field type
	getDynamicFieldFromRecord: function(fp, fieldType, index) {
		const field = {
			xtype: 'textfield',
			name: this.getFieldNameFromType(fieldType),
			fieldLabel: fieldType.label,
			submitValue: false,
			allowBlank: (fieldType.required !== true),
			fieldTypeId: fieldType.id, // used during bean property creation to identify field type
			qtip: fieldType.description
		};
		const dt = fieldType.dataType.name;
		if (fieldType.valueListUrl) {
			field.xtype = 'combo';
			field.url = fieldType.valueListUrl;
			if (fieldType.valueTable && fieldType.valueTable.detailScreenClass) {
				Ext.apply(field, Ext.decode('{ detailPageClass: \'' + fieldType.valueTable.detailScreenClass + '\' }'));
			}
		}
		else if (dt === 'DATE') {
			field.xtype = 'datefield';
		}
		else if (dt === 'INTEGER') {
			field.xtype = 'integerfield';
		}
		else if (dt === 'DECIMAL') {
			field.xtype = 'currencyfield';
		}
		else if (dt === 'BOOLEAN') {
			field.xtype = 'checkbox';
		}
		else if (dt === 'TIME') {
			field.xtype = 'timefield';
		}
		else if (dt === 'TABLE') {
			field.xtype = 'grid-field';
		}

		if (fieldType.defaultValue) {
			field.custom_defaultValue = fieldType.defaultValue;
			field.custom_defaultText = fieldType.defaultText;
		}

		// Depended Fields
		if (fieldType.dependedColumn) {
			field.requiredFields = [this.getFieldNameFromType(fieldType.dependedColumn)];
		}

		// apply custom configuration if any
		if (fieldType.userInterfaceConfig) {
			Ext.apply(field, Ext.decode(fieldType.userInterfaceConfig));
		}
		return field;
	}

});
Ext.reg('formpanel-custom-fields', Clifton.system.schema.FormPanelWithCustomFields);


Clifton.system.schema.FormPanelWithTabsWithCustomFields = Ext.extend(Clifton.system.schema.FormPanelWithCustomFields, {
	layout: 'fit',
	frame: false,
	bodyStyle: 'padding: 0px 0px 0',

	initComponent: function() {
		Clifton.system.schema.FormPanelWithTabsWithCustomFields.superclass.initComponent.apply(this, arguments);

		const f = this.getForm();

		f.formPanelWithTabsWithCustomColumnFields = this;
		f.isDirty = function(componentToCheckForDirtiness) {
			let dirty = false;
			const componentToCheck = componentToCheckForDirtiness || f.formPanelWithTabsWithCustomColumnFields;
			if (componentToCheck.items) {
				componentToCheck.items.each(function(o) {
					if (((o.isDirty && o.isDirty()) || o.getForm && (o.controlWindowModified === true && o.getForm().isDirty())) || (o.items && f.isDirty(o))) {
						dirty = true;
						return false;
					}
				});
			}
			return dirty;
		};
	}
});
Ext.reg('formwithtabs-custom-fields', Clifton.system.schema.FormPanelWithTabsWithCustomFields);


Clifton.system.schema.FormPanelWithCustomJsonFields = Ext.extend(Clifton.system.schema.FormPanelWithCustomFields, {
	jsonValueProperty: undefined, // loaded in getDynamicFields

	// call-back because get is asynchronous
	getDynamicFields: function(triggerField, callback) {
		const fp = this;
		const params = {};
		params['linkedValue'] = this.getDynamicTriggerFieldValue();
		params['columnGroupName'] = fp.columnGroupName;
		// Not getting actual values, so we don't need to pass the entity id.  Also, will cache this on the client.
		const cacheKey = 'JSON_COLUMN_GROUP_' + fp.columnGroupName + '_' + this.getDynamicTriggerFieldValue();

		TCG.data.getDataPromiseUsingCaching(this.dynamicFieldsUrl, this, cacheKey, {params: params})
			.then(function(data) {
				fp.jsonValueProperty = data.jsonValueBeanPropertyName;
				const fieldRecords = data.columnList;
				if (fieldRecords) {
					const fields = [];
					for (let i = 0; i < fieldRecords.length; i++) {
						const field = fp.getDynamicFieldFromRecord(fp, fieldRecords[i], i);
						if (field) {
							if (field.name) {
								field.originalName = field.name;
								field.name = fp.jsonValueProperty + '.' + field.name;
							}
							fields.push(field);

						}
					}
					callback.apply(fp, [fields]);
				}
			});
	},

	getFieldNameFromType: function(fieldType) {
		return fieldType.name;
	},


	setDynamicFieldsValues: function() {
		// if loaded, then trigger field value is the same as corresponding loaded value
		const form = this.getForm();
		for (let i = 0; i < this.dynamicFields.length; i++) {
			let field = this.dynamicFields[i];
			if (TCG.isNotNull(field) && field.isFormField) {
				// dynamic field that points to real field name (label, etc.)
				let v = this.getFormValue(field.name);
				if (TCG.isNotNull(v)) {
					field.setValue(v);
					if (form && form.trackResetOnLoad) {
						field.originalValue = field.getValue(); // in case setValue reformatted the field
					}
					if (field.detailIdField) {
						v = this.getFormValue(field.detailIdField);
						if (TCG.isNotNull(v)) {
							field = this.getForm().findField(field.detailIdField);
							if (TCG.isNotNull(field)) {
								field.setValue(v);
								if (form && form.trackResetOnLoad) {
									field.originalValue = field.getValue(); // in case setValue reformatted the field
								}
							}
						}
					}
				}
			}
		}
	},


	// add dynamic field values separately, need to do this because of value and text properties and submit as the map values
	getSubmitParams: function() {
		let result = {};
		if (this.dynamicFields) {
			const jsonValue = {};
			for (let i = 0; i < this.dynamicFields.length; i++) {
				const field = this.dynamicFields[i];
				// do not submit fields with no values
				if (TCG.isNotNull(field) && field.isFormField && field.getValue() !== '' && field.submitDetailField !== false) {
					const value = this.getValueFromField(field);
					const text = this.getTextFromField(field, value);
					if (!TCG.isEquals(value, text)) {
						jsonValue[field.originalName] = {value: value, text: text};
					}
					else {
						jsonValue[field.originalName] = value;
					}

				}
			}
			if (!TCG.isEmptyObject(jsonValue)) {
				result[this.jsonValueProperty] = Ext.encode(jsonValue);
			}
		}
		if (this.appendSubmitParams) {
			result = this.appendSubmitParams(result);
		}
		return result;
	}
});
Ext.reg('formpanel-custom-json-fields', Clifton.system.schema.FormPanelWithCustomJsonFields);


Clifton.system.schema.GridPanelWithCustomJsonFields = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'OVERRIDE_ME', // the table name to look up custom columns for
	columnGroupName: undefined, // if you need to limit it to just one column group - otherwise would include all (most tables just use one)
	defaultHidden: true, // By default hide all Custom Columns - set to false to display by default
	nonCustomColumns: undefined, // Override with the standard columns to display.

	gridFiltersConfig: {
		local: false // force remote load - combo custom field values don't filter well in client
	},

	initComponent: function() {
		const customColumns = this.loadCustomColumns();
		const cols = [];
		Ext.each(this.nonCustomColumns, function(f) {
			cols.push(f);
		});
		Ext.each(customColumns, function(f) {
			cols.push(f);
		});

		this.columns = cols;
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);
	},


	getLinkedValueFilter: function() {
		// Optionally for detail windows with grid as sub tabs, can limit the results to a specific linked value or null
		return undefined;
	},

	// If the grid itself is not the object where the custom values are, but are nested
	// Use this to override what that property is.  Remember to suffix it with .
	// Example Table A has a property for Table B.  The custom fields are on Table B, but the list is of Table A objects.  return here tableAPropertyName.
	getJsonValueBeanPath: function() {
		return '';
	},


	loadCustomColumns: function() {
		let cacheKey = 'CUSTOM_JSON_COLUMNS_' + this.tableName;
		let url = 'systemColumnCustomListFind.json?customColumnType=COLUMN_JSON_VALUE&tableName=' + this.tableName;
		if (TCG.isNotNull(this.columnGroupName)) {
			cacheKey += '_' + this.columnGroupName;
			url += '&columnGroupName=' + this.columnGroupName;
		}
		// Ability to additionally filter for a specific linked value
		const linkedValue = this.getLinkedValueFilter();
		if (linkedValue) {
			url += '&includeNullLinkedValue=true&linkedValue=' + linkedValue;
			cacheKey += '_' + linkedValue;
		}
		url += '&orderBy=columnGroupName:ASC#columnOrder:ASC';
		url += '&requestedPropertiesRoot=data&requestedProperties=|columnGroup.jsonValueColumn.beanPropertyName|name|label|description|dataType.name|valueTable.name|valueListUrl';

		const result = TCG.data.getData(url, this, cacheKey);
		const customCols = [];
		const checkedList = []; // sometimes the same column is used separately by different linked values, let's merge them into one

		const jsonValueBeanPath = this.getJsonValueBeanPath();
		const defaultHidden = this.defaultHidden;

		Ext.each(result, function(f) {
			const jsonColumnName = jsonValueBeanPath + f.columnGroup.jsonValueColumn.beanPropertyName;
			const columnName = f.name;
			const propertyName = jsonColumnName + '.' + columnName;

			if (checkedList.indexOf(propertyName) < 0) {
				checkedList.push(propertyName);

				// SORTING IS CURRENTLY DISABLED
				let columnConfig = {header: f.label, width: 50, hidden: defaultHidden, dataIndex: propertyName, renderer: Clifton.system.schema.renderCustomJsonFieldValue, sortable: false, exportDataIndex: 'CUSTOM_JSON_STRING_' + jsonColumnName + '.' + columnName, type: 'auto', exportDataType: 'NAME', tooltip: f.description, dataTypeName: f.dataType.name, filter: {dataTypeName: f.dataType.name, doNotTruncateBeanPath: true, searchFieldName: propertyName}};
				if (f.dataType.name === 'DATE') {
					// Doesn't render properly as type date, so need to manually configure that, and then also set the type on the filter so filtering works correctly
					columnConfig = Ext.apply(columnConfig, {align: 'center', exportDataType: 'DATE', filter: {type: 'date', dataTypeName: f.dataType.name, doNotTruncateBeanPath: true, searchFieldName: propertyName}});
				}
				else if (f.dataType.name === 'INTEGER') {
					if (TCG.isNotBlank(f.valueListUrl)) {
						columnConfig = Ext.apply(columnConfig, {idDataIndex: propertyName + '.value', filter: {doNotTruncateBeanPath: true, searchFieldName: propertyName, type: 'combo', url: f.valueListUrl}});
					}
					else if (TCG.isNotBlank(f.valueTable) && TCG.isNotBlank(f.gridFilterUserInterfaceConfig)) {
						// likely a system-list-combo applied via grid filter override applied below
						columnConfig = Ext.apply(columnConfig, {idDataIndex: propertyName + '.value', filter: {doNotTruncateBeanPath: true, searchFieldName: propertyName, type: 'combo'}});
					}
					else {
						columnConfig = Ext.apply(columnConfig, {type: 'int', useNull: true, exportDataType: 'INT'});
					}
				}
				else if (f.dataType.name === 'BOOLEAN') {
					columnConfig = Ext.apply(columnConfig, {type: 'boolean', exportDataType: 'BIT'});
				}
				else if (f.dataType.name === 'DECIMAL') {
					columnConfig = Ext.apply(columnConfig, {type: 'float', useNull: true, exportDataType: 'DECIMAL_PRECISE'});
				}

				if (TCG.isNotBlank(f.gridFilterUserInterfaceConfig)) {
					if (!columnConfig.filter) {
						columnConfig.filter = {};
					}
					Ext.apply(columnConfig.filter, Ext.decode(f.gridFilterUserInterfaceConfig));
				}


				customCols.push(columnConfig);
			}
		});
		return customCols;
	}
});
Ext.reg('gridpanel-custom-json-fields', Clifton.system.schema.GridPanelWithCustomJsonFields);

// Used to render custom json values in the grids.  Some fields are saved as value, text others just the actual value (no nested properties) i.e. combo would store id/name, but text would just store the text value once
// Expects dataTypeName to be a property on the column (see above grid)
Clifton.system.schema.renderCustomJsonFieldValue = function(val, metaData, r) {
	if (TCG.isNotBlank(val)) {
		let value = val;
		if (val.text) {
			value = val.text;
		}
		if (this.dataTypeName === 'BOOLEAN') {
			return TCG.renderBoolean(value);
		}
		if (this.dataTypeName === 'DATE') {
			return TCG.renderDate(TCG.parseDate(value, 'm/d/Y'));
		}
		return value;
	}
	return val;
};

Clifton.system.hierarchy.TagFieldSet = Ext.extend(TCG.form.FieldSet, {
	title: 'Tags',
	tableName: 'OVERRIDE-ME',
	hierarchyCategoryName: undefined, // Optionally allow only one hierarchy category for selection in the tag fieldset
	// If true, won't replicate field
	singleTableAssignment: false,
	listeners: {
		afterRender: function() {
			const fp = TCG.getParentFormPanel(this);
			fp.on('afterload', function() {
				if (fp.getWindow().savedSinceOpen === true) {
					this.saveTags();
				}
				else {
					this.loadTags();
				}
			}, this);
		}
	},
	getFkFieldId: function(fp) {
		return fp.getWindow().getMainFormId();
	},

	tagsLoaded: false,
	originalValue: '', // Used to track if changes are actually made to any of the tag values
	loadTags: function() {
		const fp = TCG.getParentFormPanel(this);
		const params = {};
		params['tableName'] = this.tableName;
		params['fkFieldId'] = this.getFkFieldId(fp);
		if (this.hierarchyCategoryName) {
			params['hierarchyCategoryName'] = this.hierarchyCategoryName;
		}

		const fs = this;
		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			waitMsg: 'Loading tags...',
			params: params,
			onLoad: function(records, conf) {
				if (records) {
					for (let i = 0; i < records.length; i++) {
						const record = records[i];
						const field = fs.items.get(i);
						const val = {value: record.hierarchy.id, text: record.hierarchy.nameExpandedWithLabel};
						field.originalValue = val.value;
						field.setValue(val);
						field.fireEvent('change', field, record.hierarchy.id, '');
					}
					// When tags are loaded - reset original value
					fs.originalValue = fs.getTagValues();

					// Can be used to do something after tags are loaded, for example disabling the field set.
					fs.fireEvent('afterLoadTags', fs);
				}
				fs.tagsLoaded = true;
			}
		});
		loader.load('systemHierarchyLinkList.json');
	},
	saveTags: function() {
		const fs = this;
		const fp = TCG.getParentFormPanel(this);
		const params = {};
		params['tableName'] = this.tableName;
		params['fkFieldId'] = this.getFkFieldId(fp);
		if (this.hierarchyCategoryName) {
			params['hierarchyCategoryName'] = this.hierarchyCategoryName;
		}

		params['hierarchyIdList'] = this.getTagValues();
		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			waitMsg: 'Saving tags...',
			params: params,
			onLoad: function(record, conf) {
				// Reset "Original Value" for the tags after they are saved
				fs.originalValue = fs.getTagValues();
			}
		});
		loader.load('systemHierarchyLinkListSave.json');
	},

	getTagValues: function() {
		let hVal = '';
		for (let i = 0; i < this.items.length; i++) {
			if (this.items.get(i).getValue() !== '') {
				hVal = hVal + this.items.get(i).getValue() + ',';
			}
		}
		return hVal;
	},

	isDirty: function() {
		// Track original value vs current value for dirty across all tags
		return TCG.isTrue(this.tagsLoaded) && TCG.isNotEquals(this.originalValue, this.getTagValues());
	},
	initComponent: function() {
		if (this.singleTableAssignment === true) {
			this.items = this.singleAssignmentItems;
		}
		else {
			this.items = this.multipleAssignmentItems;
		}
		TCG.form.FieldSet.superclass.initComponent.call(this);
	},
	singleAssignmentItems: [
		{
			xtype: 'combo', name: 'systemHierarchyList', displayField: 'nameExpandedWithLabel', url: 'systemHierarchyListFind.json',
			detailPageClass: 'Clifton.system.hierarchy.HierarchyWindow',
			disableAddNewItem: true,
			beforequery: function(queryEvent) {
				queryEvent.combo.store.baseParams = {tableName: queryEvent.combo.ownerCt.tableName, ignoreNullParent: true};
			}
		}
	],
	multipleAssignmentItems: [
		{
			xtype: 'combo', name: 'systemHierarchyList', displayField: 'nameExpandedWithLabel', url: 'systemHierarchyListFind.json',
			detailPageClass: 'Clifton.system.hierarchy.HierarchyWindow',
			disableAddNewItem: true,
			beforequery: function(queryEvent) {
				if (queryEvent.combo.ownerCt.hierarchyCategoryName) {
					queryEvent.combo.store.baseParams = {tableName: queryEvent.combo.ownerCt.tableName, ignoreNullParent: true, categoryName: queryEvent.combo.ownerCt.hierarchyCategoryName};
				}
				else {
					queryEvent.combo.store.baseParams = {tableName: queryEvent.combo.ownerCt.tableName, ignoreNullParent: true};
				}
			},
			listeners: {
				'change': function(field) {
					// Ensure Window's Apply Button Gets Triggered if an actual change
					const win = TCG.getParentFormPanel(field).getWindow();
					// Window is modified will check if any of the fields are "dirty" so we call that to enforce the Apply button gets enabled
					const modified = TCG.isTrue(win.isModified());
					win.setModified(modified);
				}
			},
			isDirty: function() {
				// Because fields are destroyed when values are removed, track "dirty" at the field set level
				if (this.ownerCt && this.ownerCt.isDirty) {
					return this.ownerCt.isDirty();
				}
				return false;
			},
			// If singleTableAssignment is False then use this:
			plugins: [Ext.ux.FieldReplicator]
		}
	]
});
Ext.reg('system-tags-fieldset', Clifton.system.hierarchy.TagFieldSet);


// singleTableAssignment = true
Clifton.system.hierarchy.HierarchyCombo = Ext.extend(TCG.form.ComboBox, {
	fieldLabel: 'OVERRIDE-ME',
	tableName: 'OVERRIDE-ME',
	hierarchyCategoryName: 'OVERRIDE-ME',
	name: 'systemHierarchyList', displayField: 'nameExpanded', url: 'systemHierarchyListFind.json',
	detailPageClass: 'Clifton.system.hierarchy.HierarchyWindow',
	disableAddNewItem: true,
	submitValue: false,
	beforequery: function(queryEvent) {
		queryEvent.combo.store.baseParams = {
			categoryName: queryEvent.combo.hierarchyCategoryName,
			tableName: queryEvent.combo.tableName,
			ignoreNullParent: true,
			requestedPropertiesRoot: 'data.rows',
			requestedProperties: 'id|nameExpanded'
		};
	},
	listeners: {
		afterRender: function() {
			if (this.hiddenField) {
				this.hiddenField.doNotSubmit = true;
			}
			const fp = TCG.getParentFormPanel(this);
			fp.on('afterload', function() {
				if (fp.getWindow().savedSinceOpen === true) {
					this.saveTag();
				}
				else {
					this.loadTag();
				}
			}, this);
		}
	},
	getFkFieldId: function(fp) {
		return fp.getWindow().getMainFormId();
	},
	loadTag: function() {
		const fp = TCG.getParentFormPanel(this);
		const params = {};
		params['tableName'] = this.tableName;
		params['fkFieldId'] = this.getFkFieldId(fp);
		if (this.hierarchyCategoryName) {
			params['hierarchyCategoryName'] = this.hierarchyCategoryName;
		}
		params['requestedPropertiesRoot'] = 'data';
		params['requestedProperties'] = 'hierarchy.id|hierarchy.nameExpanded';


		const field = this;
		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			waitMsg: 'Loading tags...',
			params: params,
			onLoad: function(records, conf) {
				if (records && records.length > 0) {
					const record = records[0];
					const val = {value: record.hierarchy.id, text: record.hierarchy.nameExpanded};
					field.originalValue = val.value;
					field.setValue(val);
					field.fireEvent('change', field, record.hierarchy.id, '');
				}
			}
		});
		loader.load('systemHierarchyLinkList.json');
	},
	saveTag: function() {
		const fp = TCG.getParentFormPanel(this);
		const params = {};
		params['tableName'] = this.tableName;
		params['fkFieldId'] = this.getFkFieldId(fp);
		params['hierarchyCategoryName'] = this.hierarchyCategoryName;

		params['hierarchyIdList'] = this.getValue();
		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			waitMsg: 'Saving tags...',
			params: params
		});
		loader.load('systemHierarchyLinkListSave.json');
	}
});
Ext.reg('system-hierarchy-combo', Clifton.system.hierarchy.HierarchyCombo);

/**
 * Combo box to use for system bean selections for a specific system bean group
 * i.e. {fieldLabel: 'Journal Generator Bean', name: 'journalGeneratorBean.name', hiddenName: 'journalGeneratorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Accounting Task Journal Generator',
				detailPageClass: 'Clifton.system.bean.BeanWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Accounting Task Journal Generator'}}};
				}
			}
 */
Clifton.system.bean.BeanCombo = Ext.extend(TCG.form.ComboBox, {
	fieldLabel: 'Bean',
	groupName: 'REPLACE_ME', // i.e. Accounting Task Journal Generator
	beanName: 'REPLACE_ME', // i.e. journalGeneratorBean used to set name: beanName.name and hiddenName: beanName.id
	displayField: 'labelShort', // coalesce of alias and name
	detailPageClass: 'Clifton.system.bean.BeanWindow',
	minListWidth: 300,

	initComponent: function() {
		this.name = this.beanName + '.labelShort';
		this.hiddenName = this.beanName + '.id';
		this.url = 'systemBeanListFind.json?groupName=' + this.groupName;

		Clifton.system.bean.BeanCombo.superclass.initComponent.apply(this, arguments);
	},

	getDefaultData: function() {
		return {type: {group: {name: this.groupName}}};
	}

});
Ext.reg('system-bean-combo', Clifton.system.bean.BeanCombo);


Clifton.system.SystemConditionCombo = Ext.extend(TCG.form.ComboBox, {
	fieldLabel: 'System Condition',
	url: 'systemConditionListFind.json',
	detailPageClass: 'Clifton.system.condition.ConditionWindow',

	afterRender: function() {
		Clifton.system.SystemConditionCombo.superclass.afterRender.apply(this, arguments);
		this.drillDownMenu = new Ext.menu.Menu({
			items: [
				{
					text: 'Open Detail Window', iconCls: 'info', scope: this,
					handler: function() {
						this.createDetailClass(false);
					}
				},
				{
					text: 'Add New Simple Condition', iconCls: 'add', scope: this,
					handler: function() {
						this.savedConditionType = 'CONDITION';
						this.createDetailClass(true);
					}
				},
				{
					text: 'Add New AND Condition', iconCls: 'add', scope: this,
					handler: function() {
						this.savedConditionType = 'AND';
						this.createDetailClass(true);
					}
				},
				{
					text: 'Add New OR Condition', iconCls: 'add', scope: this,
					handler: function() {
						this.savedConditionType = 'OR';
						this.createDetailClass(true);
					}
				}
			]
		});
	},

	getDefaultData: function(form) {
		return {
			conditionEntry: {
				entryType: this.savedConditionType
			}
		};
	}
});
Ext.reg('system-condition-combo', Clifton.system.SystemConditionCombo);

/**
 * Combo box to use for modify condition selections that are used to additionally security entities:
 * i.e. SystemHierarchyCategory, InvestmentGroup, InvestmentSecurityGroup
 */
Clifton.system.EntityModifyConditionCombo = Ext.extend(Clifton.system.SystemConditionCombo, {
	fieldLabel: 'Modify Condition',
	name: 'entityModifyCondition.name',
	hiddenName: 'entityModifyCondition.id',
	// Returns Root Conditions that are available globally - i.e. not defined for a specific table
	url: 'systemConditionListFind.json?emptySystemTableOnly=true',

	/**
	 * If <tt>true</tt>, then an empty Entity Modify Condition implies that only admins may make modifications. Otherwise, an empty Entity Modify Conditions implies that all users with appropriate standard permissions may make modifications.
	 *
	 * If <tt>true</tt>, the corresponding interface method on the bean should be overridden approrpriately.
	 */
	requireConditionForNonAdmin: false, // Updates instructions, should set this = isEntityModifyConditionRequiredForNonAdmin on the bean class
	requireConditionForNonAdminQtip: 'Additional Security: Modify Condition required to pass for non-admin users.  Always passes for admin users.',
	optionalConditionForNonAdminQtip: 'Optional Additional Security: If selected this Modify Condition is required to pass for non-admin users.  Always passes for admin users.',

	setRequireConditionForNonAdmin: function(required) {
		const newValue = required && required === true;
		if (newValue !== this.requireConditionForNonAdmin) {
			this.requireConditionForNonAdmin = newValue;
			this.updateRequireConditionForNonAdminQtip();
		}
	},

	updateRequireConditionForNonAdminQtip: function() {
		if (this.requireConditionForNonAdmin) {
			this.qtip = this.requireConditionForNonAdminQtip;
		}
		else {
			this.qtip = this.optionalConditionForNonAdminQtip;
		}
		if (this.el) {
			// has been intialized and is being updated
			this.afterRender();
		}
	},

	initComponent: function() {
		this.updateRequireConditionForNonAdminQtip();
		Clifton.system.EntityModifyConditionCombo.superclass.initComponent.apply(this, arguments);
	}

});
Ext.reg('system-entity-modify-condition-combo', Clifton.system.EntityModifyConditionCombo);


/**
 * Combo box to use for delete condition selections that are used to additionally secure deletion of entities:
 * See SystemNoteType for current use case
 */
Clifton.system.EntityDeleteConditionCombo = Ext.extend(Clifton.system.EntityModifyConditionCombo, {
	fieldLabel: 'Delete Condition',
	name: 'entityDeleteCondition.name',
	hiddenName: 'entityDeleteCondition.id',

	requireConditionForNonAdminQtip: 'Additional Security for Deletes Only: Delete Condition required to pass for non-admin users.  Always passes for admin users. If not set, the modify condition, if present, will be used.',
	optionalConditionForNonAdminQtip: 'Optional Additional Security: If selected this Delete Condition is required to pass for non-admin users.  Always passes for admin users. If not set, the modify condition, if present will be used.'
});
Ext.reg('system-entity-delete-condition-combo', Clifton.system.EntityDeleteConditionCombo);

/**
 * Special combo box to be used when options are pulled from SystemList(s)
 * Supports drill down into the list so users can add items where possible
 * EXAMPLE: { fieldLabel: 'Category', name: 'categoryName', xtype: 'system-list-combo', listName: 'Contract Type Categories' }
 */
Clifton.system.ListCombo = Ext.extend(TCG.form.ComboBox, {
	listName: 'REQUIRED-LIST-NAME',
	// OPTIONAL hiddenName: 'FIELD-HIDDEN-NAME',
	name: 'REQUIRED-FIELD-NAME',
	url: 'systemListItemListFind.json',
	displayField: 'text',
	tooltipField: 'tooltip',
	valueField: 'value',

	disableAddNewItem: true,

	beforequery: function(queryEvent) {
		queryEvent.combo.store.baseParams = {
			listName: this.listName
		};
	},

	afterRender: function() {
		Clifton.system.ListCombo.superclass.afterRender.apply(this, arguments);
		if (!this.doNotAddContextMenu) {
			const el = this.getEl();
			el.dom.setAttribute('ext:qtip', 'Right click to display the context menu with additional features.');
			el.on('contextmenu', function(e, target) {
				e.preventDefault();
				if (!this.drillDownMenu) {
					this.drillDownMenu = new Ext.menu.Menu({
						items: [
							{
								text: 'Open List to View/Edit Items', iconCls: 'info',
								handler: function() {
									this.createDetailClass(false);
								}, scope: this
							}
						]
					});
				}
				// disable detail menu if no selection was made
				this.drillDownMenu.showAt(e.getXY());
			}, this);
		}
	},

	createDetailClass: function(newInstance) {
		const fp = TCG.getParentFormPanel(this);
		const w = TCG.isNull(fp) ? {} : fp.getWindow();

		const listObj = TCG.data.getData('systemListByName.json?name=' + this.listName, this, 'system.list.' + this.listName);


		let className = 'Clifton.system.list.ListWindow';
		if (listObj.sqlList === true) {
			className = 'Clifton.system.list.ListQueryWindow';
		}
		let id = listObj.id;
		if (id === '' || newInstance) {
			id = undefined;
		}
		const params = id ? {id: id} : undefined;
		const cmpId = TCG.getComponentId(className, id);
		TCG.createComponent(className, {
			modal: TCG.isNull(id),
			id: cmpId,
			defaultData: listObj,
			defaultDataIsReal: true,
			params: params,
			openerCt: this, // for modal new item window will call reload() on close
			defaultIconCls: w.iconCls
		});
	}
});
Ext.reg('system-list-combo', Clifton.system.ListCombo);

// Extension of System List Combo with Listeners to Reload Grid on Selections
Clifton.system.ListToolbarCombo = Ext.extend(Clifton.system.ListCombo, {
	linkedFilterCancelReload: true,

	reloadGridPanel: field => {
		const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
		if (gridPanel && gridPanel.reload) {
			gridPanel.reload();
		}
	},

	listeners: {
		select: function(field) {
			this.reloadGridPanel(field);
		},
		specialkey: function(field, e) {
			if (e.getKey() === e.ENTER) {
				this.reloadGridPanel(field);
			}
		}
	}
});
Ext.reg('toolbar-system-list-combo', Clifton.system.ListToolbarCombo);


Clifton.system.list.ListItemGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'systemListItemListFind',
	xtype: 'gridpanel',
	instructions: 'The following items are associated with this list.  If the list is not a System Defined List then items can be added/edited/removed.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Value', width: 100, dataIndex: 'value'},
		{header: 'Text', width: 100, dataIndex: 'text'},
		{header: 'Tooltip', width: 100, dataIndex: 'tooltip'},
		{header: 'Order', width: 50, dataIndex: 'order', type: 'int', useNull: true},
		{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'}
	],
	editor: {
		detailPageClass: 'Clifton.system.list.ItemWindow',
		deleteURL: 'systemListItemDelete.json',
		getDefaultData: function(gridPanel) { // defaults systemList id for the detail page
			return {
				systemList: gridPanel.getSystemList()
			};
		}
	},
	getSystemList: function() {
		return this.getWindow().getMainForm().formValues;
	},
	getTopToolbarInitialLoadParams: function() {
		return {'systemListId': TCG.getValue('id', this.getSystemList())};
	}
});
Ext.reg('system-list-item-grid', Clifton.system.list.ListItemGridPanel);


Clifton.system.list.ListSetupWindowAdditionalTabs = [];


Clifton.system.InfoWindow_AdditionalTabs = [];
Clifton.system.InfoWindow = Ext.override(TCG.app.InfoWindow, {
	title: 'Info Window',
	iconCls: 'info',
	modal: true,
	allowOpenFromModal: true,
	minimizable: false,
	maximizable: true,
	enableShowInfo: false,
	width: 1000,
	height: 450,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.system.InfoWindow_AdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel'
				}]
			},
			{
				title: 'Audit Trail',
				items: [{
					xtype: 'system-audit-grid',
					tableName: 'UNKNOWN-TABLE',
					getEntityId: function() {
						return this.getWindow().idFieldValue;
					},
					getLoadParams: function(firstLoad) {
						const params = {
							tableName: this.tableName,
							entityId: this.getEntityId()
						};
						if (this.childTables) {
							params.childTables = this.childTables;
						}
						return params;
					}
				}]
			},
			{
				title: 'Used By',
				items: [{
					xtype: 'system-usedby-grid',
					tableName: 'UNKNOWN-TABLE',
					getEntityId: function() {
						return this.getWindow().idFieldValue;
					}
				}]
			},
			{
				title: 'Object Data',
				items: [{
					xtype: 'json-migration-form',
					getTableName: function() {
						return this.getWindow().table.name;
					},
					getEntityId: function() {
						return this.getWindow().idFieldValue;
					}
				}]
			}
		]
	}],

	params: {},
	table: undefined,
	init: function() {
		const d = this.data;
		this.params.id = this.data.windowId;
		let f = this.items[0].items[0].items[0];
		f.items = [];
		f = f.items;
		f.push({fieldLabel: 'Window ID', xtype: 'displayfield', value: d.windowId});
		f.push({fieldLabel: 'URL', xtype: 'displayfield', value: d.url});
		const applicationScope = this.getApplicationScope();
		if (applicationScope) {
			f.push({fieldLabel: 'Source Application', xtype: 'displayfield', value: TCG.camelCaseToTitle(applicationScope)});
		}
		// show table info
		let table = '';
		if (d.table) {
			table = d.table;
		}
		else if (d.url) {
			table = d.url.substring(0, d.url.indexOf('.'));
			table = table.charAt(0).toUpperCase() + table.substr(1);
		}

		// Verify a Real Table:
		const t = TCG.data.getData(`systemTableByName.json?tableName=${table}`, this);
		this.table = t;
		if (t) {
			// Set Audit Trail Tab Table Name
			const auditTrail = this.items[0].items[1].items[0];
			auditTrail.tableName = table;
			if (d.getInfoWindowAuditTrailLoadParams) { // support optional override
				if (!auditTrail.getLoadParamsCopy) {
					auditTrail.getLoadParamsCopy = auditTrail.getLoadParams;
				}
				auditTrail.getLoadParams = d.getInfoWindowAuditTrailLoadParams;
			}
			else if (auditTrail.getLoadParamsCopy) {
				auditTrail.getLoadParams = auditTrail.getLoadParamsCopy;
			}
			// Set Used By Tab Table Name
			this.items[0].items[2].items[0].tableName = table;
			f.push({xtype: 'hidden', name: 'tableId', value: t.id});
			f.push({fieldLabel: 'Table Name', xtype: 'linkfield', value: table, detailIdField: 'tableId', detailPageClass: 'Clifton.system.schema.TableWindow'});

			if (t.securityResource) {
				f.push({xtype: 'hidden', name: 'securityResourceId', value: t.securityResource.id});
				f.push({fieldLabel: 'Security Resource', xtype: 'linkfield', value: t.securityResource.labelExpanded, detailIdField: 'securityResourceId', detailPageClass: 'Clifton.security.authorization.ResourceWindow'});
			}
			if (t.auditType) {
				f.push({fieldLabel: 'Audit Type', xtype: 'displayfield', value: t.auditType.name});
			}
			if (t.insertEntityCondition) {
				f.push({xtype: 'hidden', name: 'insertEntityConditionId', value: t.insertEntityCondition.id});
				f.push({fieldLabel: 'Insert Condition', xtype: 'linkfield', value: t.insertEntityCondition.label, detailIdField: 'insertEntityConditionId', detailPageClass: 'Clifton.system.condition.ConditionWindow'});
			}
			if (t.updateEntityCondition) {
				f.push({xtype: 'hidden', name: 'updateEntityConditionId', value: t.updateEntityCondition.id});
				f.push({fieldLabel: 'Update Condition', xtype: 'linkfield', value: t.updateEntityCondition.label, detailIdField: 'updateEntityConditionId', detailPageClass: 'Clifton.system.condition.ConditionWindow'});
			}
			if (t.deleteEntityCondition) {
				f.push({xtype: 'hidden', name: 'deleteEntityConditionId', value: t.deleteEntityCondition.id});
				f.push({fieldLabel: 'Delete Condition', xtype: 'linkfield', value: t.deleteEntityCondition.label, detailIdField: 'deleteEntityConditionId', detailPageClass: 'Clifton.system.condition.ConditionWindow'});
			}

			// This needs to be set each time, even if undefined so it will clear any previous values
			this.items[0].items[1].items[0].childTables = d.childTables;
		}
		else {
			// Disable Audit Trail, Used By
			this.items[0].items[1].disabled = true;
			this.items[0].items[2].disabled = true;
		}

		// show entity info
		if (d.data) {
			f.push({xtype: 'label', html: '<hr />'});
			this.idFieldValue = d.data.id;
			if (d.data.label) {
				f.push({fieldLabel: 'Entity Label', xtype: 'displayfield', value: d.data.label});
			}
			f.push({fieldLabel: 'Created By', xtype: 'displayfield', value: this.getUserLabel(d.data.createUserId)});
			f.push({fieldLabel: 'Create Date', xtype: 'displayfield', value: d.data.createDate, type: 'date'});
			f.push({fieldLabel: 'Updated By', xtype: 'displayfield', value: this.getUserLabel(d.data.updateUserId)});
			f.push({fieldLabel: 'Update Date', xtype: 'displayfield', value: d.data.updateDate, type: 'date'});
		}

		const label = TCG.getValue('data.label', this.data);
		this.title = `Info Window for ${table} ${this.idFieldValue}` + (TCG.isNull(label) ? '' : ' (' + label + ')');
		TCG.app.InfoWindow.superclass.init.apply(this, arguments);
	},

	getUserLabel: function(userId) {
		return Clifton.security.renderSecurityUser(userId, this);
	}
});


Clifton.system.InfoWindow_AdditionalTabs[Clifton.system.InfoWindow_AdditionalTabs.length] = {
	title: 'Security Access',
	items: [{
		xtype: 'security-resource-access-grid',
		getSecurityResourceName: function() {
			const tbl = this.getWindow().table;
			if (!tbl || !tbl.securityResource) {
				return false;
			}
			return tbl.securityResource.name;
		}
	}]
};


Clifton.system.InfoWindow_AdditionalTabs[Clifton.system.InfoWindow_AdditionalTabs.length] = {
	title: 'Entity Life Cycle',
	items: [{
		xtype: 'system-lifecycle-grid',
		getTableName: function() {
			return this.getWindow().table.name;
		},
		getEntityId: function() {
			return this.getWindow().idFieldValue;
		}
	}]
};


Clifton.system.renderSystemLockIconWithTooltip = function(locked, entityType, lockedByUser, lockNote) {
	if (TCG.isTrue(locked)) {
		return TCG.renderIconWithTooltip('lock', 'This ' + entityType + ' is currently locked by ' + lockedByUser + (TCG.isNotBlank(lockNote) ? '. Lock Note: ' + lockNote : ''));
	}
	return '';
};

/**
 * Security User and Group Methods that supports Tag Look ups
 * Defaults to using Security Group Tags as the hierarchy category name, but can be overridden
 */

Clifton.system.security.getUserSecurityGroupForTag = async function(componentScope, groupTagCategoryName, groupTagName) {
	const hierarchy = await TCG.data.getDataPromiseUsingCaching('systemHierarchyByCategoryAndNameExpanded.json?categoryName=' + groupTagCategoryName + '&name=' + groupTagName, componentScope, 'system.hierarchy.' + groupTagCategoryName + '.' + groupTagName);
	if (TCG.isNull(hierarchy)) {
		return null;
	}
	const groupList = await TCG.data.getDataPromiseUsingCaching('securityGroupListFind.json?securityGroupHierarchyId=' + hierarchy.id, componentScope, 'security.groupList.' + groupTagName);
	if (TCG.isNull(groupList)) {
		return null;
	}
	const userGroupList = await TCG.data.getDataPromiseUsingCaching('securityGroupListForCurrentUser.json', componentScope, 'security.currentUser.groupList');
	if (TCG.isNull(userGroupList)) {
		return null;
	}

	let finalGroup = null;
	for (const userGroup of userGroupList) {
		for (const group of groupList) {
			if (TCG.isEquals(userGroup.id, group.id)) {
				if (TCG.isBlank(finalGroup)) {
					finalGroup = group;
					continue;
				}
				else {
					// Multiple Groups Found
					return null;
				}
			}
		}
	}
	return finalGroup;
};


Clifton.system.security.SecurityGroupWithTagCombo = Ext.extend(TCG.form.ComboBox, {
	groupTagCategoryName: 'Security Group Tags',
	groupTagName: 'REQUIRED-TAG-NAME',
	url: 'securityGroupListFind.json',
	disableAddNewItem: true,

	beforequery: function(queryEvent) {
		const s = queryEvent.combo.store;
		const hierarchy = this.getSecurityGroupHierarchyId();
		if (TCG.isNotNull(hierarchy)) {
			s.setBaseParam('securityGroupHierarchyId', hierarchy);
		}
	},

	getSecurityGroupHierarchyId: function() {
		const hierarchy = TCG.data.getData('systemHierarchyByCategoryAndNameExpanded.json?categoryName=' + this.groupTagCategoryName + '&name=' + this.groupTagName, this, 'system.hierarchy.' + this.groupTagCategoryName + '.' + this.groupTagName);
		if (TCG.isNotNull(hierarchy)) {
			return hierarchy.id;
		}
		return null;
	}
});
Ext.reg('system-security-group-combo', Clifton.system.security.SecurityGroupWithTagCombo);


// Extension of Security Group combo with Listeners to Reload Grid on Selections AND option to default to current users team if in only 1
Clifton.system.security.SecurityGroupWithTagToolbarCombo = Ext.extend(Clifton.system.security.SecurityGroupWithTagCombo, {
	linkedFilterCancelReload: true,

	reloadGridPanel: field => {
		const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
		if (gridPanel && gridPanel.reload) {
			gridPanel.reload();
		}
	},

	listeners: {
		select: function(field) {
			this.reloadGridPanel(field);
		},
		specialkey: function(field, e) {
			if (e.getKey() === e.ENTER) {
				this.reloadGridPanel(field);
			}
		}
	}
});

Clifton.system.AUDIT_UPDATE_TYPES = [
	['Skip Updates From Null', 'DO_NOT_AUDIT_UPDATE_FROM_NULL', 'skip auditing updates to a column if set from previously NULL '],
	['Skip Updates From Zero', 'DO_NOT_AUDIT_UPDATE_FROM_ZERO', 'skip auditing updates to a column if set from previously ZERO']
];
Ext.reg('toolbar-system-security-group-combo', Clifton.system.security.SecurityGroupWithTagToolbarCombo);


// Extension of Security Group combo but looks up the user list
Clifton.system.security.SecurityUserWithGroupTagCombo = Ext.extend(Clifton.system.security.SecurityGroupWithTagCombo, {
	url: 'securityUserListFind.json',
	displayField: 'displayName'
});
Ext.reg('system-security-user-group-combo', Clifton.system.security.SecurityUserWithGroupTagCombo);
