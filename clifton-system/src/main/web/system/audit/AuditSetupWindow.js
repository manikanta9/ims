Clifton.system.audit.AuditSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'auditSetupWindow',
	title: 'Audit Trail',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Audit Log',
				items: [{
					xtype: 'gridpanel',
					name: 'systemAuditEventListFind',
					instructions: 'Audit Log contains information about field level changes (insert/update/delete) for fields that have auditing enabled.',
					dataTable: true,
					useBufferView: false,
					rowSelectionModel: 'multiple',
					columns: [
						// Used for Drill Down Lookup Support
						{header: 'ColumnID', dataIndex: 'columnId', type: 'int', hidden: true, filter: false, sortable: false},
						{header: 'MainPKFieldID', dataIndex: 'mainPKFieldId', type: 'int', hidden: true, filter: {searchFieldName: 'entityId'}, sortable: false},
						{header: 'ParentPKFieldID', dataIndex: 'parentPKFieldId', type: 'int', hidden: true, filter: {searchFieldName: 'parentEntityId'}, sortable: false, useNull: true},

						{header: 'Date', dataIndex: 'auditDate', width: 150},
						{header: 'User', dataIndex: 'securityUserId', filter: {type: 'combo', displayField: 'userName', url: 'securityUserListFind.json'}, width: 100},
						{header: 'Action', dataIndex: 'auditTypeId', filter: {type: 'list', options: [[1, 'insert'], [2, 'update'], [3, 'delete']]}, width: 100},
						{header: 'Table', dataIndex: 'systemTableId', filter: {type: 'combo', url: 'systemTableListFind.json', displayField: 'labelExpanded'}, width: 200},
						// TODO: {table.name}.{name} - unfortunately doesn't look like combo supports nested properties similar to grid's renderer
						{header: 'Field', dataIndex: 'columnName', width: 200},
						{header: 'Label', dataIndex: 'label', width: 250},
						{
							header: 'Old Value', filter: false, sortable: false, width: 200,
							renderer: function(v, metaData, r) {
								if (TCG.isNotNull(v)) {
									return TCG.renderText(v);
								}
								return null;
							}
						},
						{
							header: 'New Value', filter: false, sortable: false, width: 200,
							renderer: function(v, metaData, r) {
								if (TCG.isNotNull(v)) {
									return TCG.renderText(v);
								}
								return null;
							}
						}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 10 days
							this.setFilterValue('auditDate', {'after': new Date().add(Date.DAY, -10)});
						}
						return {
							readUncommittedRequested: true
						};
					},
					editor: {ptype: 'system-audit-grid-editor'}
				}]
			},


			{
				title: 'Audit Types',
				items: [{
					xtype: 'gridpanel',
					name: 'systemAuditTypeList',
					instructions: 'Audit types define <i>actions</i> that should be audited when a database field value is modified. When auditing is enabled for a field or a table, the system stores old/new values, date/time when the field was modified and the user that did the change.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Created On', width: 50, dataIndex: 'createDate'}
					],
					editor: {
						detailPageClass: 'Clifton.system.audit.TypeWindow'
					}
				}]
			}
		]
	}]
});
