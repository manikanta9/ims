Clifton.system.audit.TypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Audit Type',

	items: [{
		xtype: 'formpanel',
		instructions: 'Audit types define <i>actions</i> that should be audited when a database field value is modified.',
		url: 'systemAuditType.json',
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{
				fieldLabel: 'Audit', xtype: 'checkboxgroup',
				items: [
					{boxLabel: 'Insert', name: 'auditInsert'},
					{boxLabel: 'Update', name: 'auditUpdate'},
					{boxLabel: 'Delete', name: 'auditDelete'}
				]
			},
			{
				fieldLabel: 'Audit Update Types', name: 'auditUpdateType', displayField: 'name', valueField: 'auditUpdateType', mode: 'local', xtype: 'combo',
				store: {
					xtype: 'arraystore',
					fields: ['name', 'auditUpdateType', 'description'],
					data: Clifton.system.AUDIT_UPDATE_TYPES
				}, requiredFields: ['auditUpdate']
			},
			{boxLabel: 'Capture full state on insert', name: 'captureFullStateOnInsert', xtype: 'checkbox', labelSeparator: ''},
			{boxLabel: 'Capture full state on delete', name: 'captureFullStateOnDelete', xtype: 'checkbox', labelSeparator: ''},
			{fieldLabel: 'Max Field Size', name: 'maxFieldSize', xtype: 'integerfield'}
		]
	}]
});
