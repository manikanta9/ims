Clifton.system.schema.ColumnWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'systemColumn.json',

	getClassName: function(config, entity) {
		if ((entity && TCG.isTrue(entity.customColumn)) || TCG.isNull(entity)) {
			return 'Clifton.system.schema.ColumnCustomWindow';
		}
		return 'Clifton.system.schema.ColumnStandardWindow';
	}
});
