Clifton.system.schema.DataTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Data Type',
	height: 300,
	iconCls: 'objects',

	items: [{
		xtype: 'formpanel',
		instructions: 'Data types define property value types for various fields across the system.',
		url: 'systemDataType.json',
		readOnly: true,
		items: [
			{fieldLabel: 'Data Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
			{fieldLabel: 'Numeric', name: 'numeric', xtype: 'checkbox'}
		]
	}]
});
