Clifton.system.schema.ColumnDatedValueWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Data Value',
	width: 600,
	height: 425,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		labelWidth: 150,
		url: 'systemColumnDatedValue.json',
		instructions: 'Select a column group/column then enter the associated value and start/end dates that value applies to. Values cannot exist for the same column for overlapping date ranges.  Setting start date on a new entry will attempt to find an existing value as of start date and end the previous value on the day before',
		prepareDefaultData: function(defaultData) {
			if (typeof defaultData.column.columnGroup.id === 'undefined') {
				defaultData.column.columnGroup = {};
				const records = TCG.data.getData('systemColumnGroupListByTableAndCustomColumnType.json?tableId=' + defaultData.column.table.id + '&customColumnType=COLUMN_DATED_VALUE', this);

				if (records && records.length === 1) {
					const r = records[0];
					defaultData.column.columnGroup.id = r.id;
					defaultData.column.columnGroup.label = r.label;
				}
			}
			defaultData.startDate = Clifton.calendar.getBusinessDayFrom(0).format('Y-m-d 00:00:00');
			return defaultData;
		},
		listeners: {
			afterload: function() {
				this.resetValueField();
			}
		},
		resetValueField: function(column) {
			if (TCG.isNull(column)) {
				column = this.getFormValue('column');
			}
			const ff = TCG.getChildByName(this, 'valueFormFragment');
			ff.removeAll(true);

			const fieldConfig = {
				xtype: 'textfield',
				name: 'value',
				listeners: {},
				fieldLabel: column.label || column.name,
				allowBlank: false
			};
			if (column.description) {
				fieldConfig.qtip = column.description;
			}
			const dt = column.dataType.name;
			if (column.valueListUrl) {
				fieldConfig.xtype = 'combo';
				fieldConfig.url = column.valueListUrl;
				if (column.valueTable && column.valueTable.detailScreenClass) {
					fieldConfig.detailPageClass = column.valueTable.detailScreenClass;
				}
			}
			else if (TCG.isEquals(dt, 'DATE')) {
				fieldConfig.xtype = 'datefield';
			}
			else if (TCG.isEquals(dt, 'INTEGER')) {
				fieldConfig.xtype = 'integerfield';
			}
			else if (TCG.isEquals(dt, 'DECIMAL')) {
				fieldConfig.xtype = 'currencyfield';
			}
			else if (TCG.isEquals(dt, 'BOOLEAN')) {
				fieldConfig.xtype = 'checkbox';
			}

			// apply custom configuration if any
			if (column.userInterfaceConfig) {
				Ext.apply(fieldConfig, Ext.decode(column.userInterfaceConfig));
			}
			// Value doesn't seem to update automatically based on field names - maybe because it's added after?
			let fieldValue = TCG.getValue('value', this.getForm().formValues);
			if (fieldConfig.xtype && fieldConfig.xtype.indexOf('combo') >= 0) {
				fieldConfig.hiddenName = 'value';
				fieldConfig.name = 'text';
				fieldConfig.submitValue = true; // Combos need to submit both value and text
				fieldValue = {value: TCG.getValue('value', this.getForm().formValues), text: TCG.getValue('text', this.getForm().formValues)};
			}
			else {
				ff.add({xtype: 'textfield', hidden: true, name: 'text'});
			}
			const field = ff.add(fieldConfig);
			field.setValue(fieldValue);
			ff.doLayout();
		},
		items: [
			{fieldLabel: 'FKFieldID', name: 'fkFieldId', hidden: true},
			{fieldLabel: 'Table', name: 'column.table.name', detailIdField: 'column.table.id', xtype: 'linkfield', submitDetailField: false, detailPageClass: 'Clifton.system.schema.TableWindow'},
			{
				fieldLabel: 'Column Group', name: 'column.columnGroup.label', hiddenName: 'column.columnGroup.id', displayField: 'label', xtype: 'combo', url: 'systemColumnGroupListByTableAndCustomColumnType.json', root: 'data', allowBlank: false, detailPageClass: 'Clifton.system.schema.ColumnGroupWindow',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {tableId: f.formValues.column.table.id, customColumnType: 'COLUMN_DATED_VALUE'};
				}
			},
			{
				fieldLabel: 'Column', name: 'column.label', hiddenName: 'column.id', displayField: 'label', xtype: 'combo', url: 'systemColumnCustomListForColumnGroupAndEntityId.json', root: 'data', allowBlank: false, detailPageClass: 'Clifton.system.schema.ColumnCustomWindow', requiredFields: ['column.columnGroup.label'],
				listeners: {
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.baseParams = {columnGroupId: f.findField('column.columnGroup.label').getValue(), entityId: f.findField('fkFieldId').getValue(), customColumnType: 'COLUMN_DATED_VALUE'};
					},
					select: function(combo, record, index) {
						const dataTypeName = TCG.getChildByName(combo.getParentForm(), 'column.dataType.name');
						dataTypeName.setValue(record.json.dataType.name);

						const columnDescription = TCG.getChildByName(combo.getParentForm(), 'column.description');
						columnDescription.setValue(record.json.description);

						combo.getParentForm().resetValueField(record.json);
					}
				}
			},
			{fieldLabel: 'Data Type', name: 'column.dataType.name', xtype: 'displayfield'},
			{name: 'column.description', xtype: 'textarea', height: 70, readOnly: true},
			{xtype: 'formfragment', frame: false, labelWidth: 150, items: [], name: 'valueFormFragment', bodyStyle: 'padding: 0'},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', width: 125},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', width: 125}
		]
	}]
});
