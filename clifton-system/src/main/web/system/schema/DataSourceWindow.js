Clifton.system.schema.DataSourceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Data Source',
	width: 800,
	height: 400,
	iconCls: 'grid',
	allowOpenFromModal: true,
	items: [{
		xtype: 'formpanel',
		url: 'systemDataSource.json',
		readOnly: true,
		instructions: 'A System Data Source represents a named data source (database connection) or an application (if external datasource)',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Data Source Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Alias', name: 'alias'},
			{fieldLabel: 'Database Name', name: 'databaseName'},
			{fieldLabel: 'Connection URL', name: 'connectionUrl'},
			{fieldLabel: 'Application URL', name: 'applicationUrl'},
			{fieldLabel: 'External', name: 'external', type: 'boolean', tooltip: 'If true, this data source is serviced by an external API.'},
			{fieldLabel: 'Default', name: 'defaultDataSource', type: 'boolean'}
		]
	}]
});
