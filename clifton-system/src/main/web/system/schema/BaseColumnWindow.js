Clifton.system.schema.BaseColumnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Custom Column',
	height: 600,
	width: 750,
	iconCls: 'grid',
	enableRefreshWindow: true,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.system.schema.ColumnWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	}
});
