Clifton.system.schema.RelationshipWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Relationship',
	height: 400,
	iconCls: 'grid',
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		url: 'systemRelationship.json',
		labelWidth: 90,
		items: [
			{fieldLabel: 'FK Name', name: 'name', xtype: 'displayfield'},
			{
				xtype: 'columnpanel',
				columns: [{
					rows: [
						{fieldLabel: 'From Table', name: 'column.table.label', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow', detailIdField: 'column.table.id'},
						{fieldLabel: 'From Column', name: 'column.label', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.ColumnWindow', detailIdField: 'column.id'}
					]
				}, {
					rows: [
						{fieldLabel: 'To Table', name: 'parentColumn.table.label', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow', detailIdField: 'parentColumn.table.id'},
						{fieldLabel: 'To Column', name: 'parentColumn.label', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.ColumnWindow', detailIdField: 'parentColumn.id'}
					]
				}]
			},

			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'FK Label', name: 'label', xtype: 'textarea', height: 50},
			{fieldLabel: 'Description', xtype: 'textarea'},
			{boxLabel: 'Exclude relationship look ups for Used by tab on Detail Windows.', xtype: 'checkbox', name: 'excludeFromUsedBy'}
		]
	}]
});
