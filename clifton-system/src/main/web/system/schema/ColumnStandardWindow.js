TCG.use('Clifton.system.schema.BaseColumnWindow');

Clifton.system.schema.ColumnStandardWindow = Ext.extend(Clifton.system.schema.BaseColumnWindow, {
	titlePrefix: 'Table Column',
	iconCls: 'grid',
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [{
			title: 'Column',

			items: [{
				xtype: 'formpanel',
				url: 'systemColumn.json',
				getSaveURL: function() {
					return 'systemColumnStandardSave.json';
				},
				items: [
					{fieldLabel: 'Table', name: 'table.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow', detailIdField: 'table.id'},
					{fieldLabel: 'Column Name', name: 'name', disabled: true},
					{fieldLabel: 'Bean Property', name: 'beanPropertyName', disabled: true},
					{fieldLabel: 'Label', name: 'label'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
					{fieldLabel: 'Data Type', name: 'dataType.name', hiddenName: 'dataType.id', xtype: 'combo', url: 'systemDataTypeList.json', detailPageClass: 'Clifton.system.schema.DataTypeWindow', loadAll: true, disabled: true},
					{fieldLabel: 'Audit Type', name: 'auditType.name', hiddenName: 'auditType.id', xtype: 'combo', loadAll: true, url: 'systemAuditTypeList.json'},
					{fieldLabel: 'Value List URL', name: 'valueListUrl'}
				]
			}]
		}]
	}]
});
