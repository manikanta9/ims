TCG.use('Clifton.system.schema.BaseColumnWindow');

Clifton.system.schema.ColumnCustomWindow = Ext.extend(Clifton.system.schema.BaseColumnWindow, {
	titlePrefix: 'Custom Column',
	height: 600,
	iconCls: 'grid',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Column',
				items: [
					{
						xtype: 'formpanel',
						instructions: 'Custom column applies to a specific group/table and can be applied to a subset entities defined by the group\'s link field and the field\'s value for that link.  If no link, then the field applies to all entities for that table.  If this column is System Defined, you cannot change its name, data type, group, or link value.',
						url: 'systemColumn.json',
						labelWidth: 130,
						getSaveURL: function() {
							return 'systemColumnCustomSave.json';
						},

						listeners: {
							afterload: function(panel) {
								const customColumnType = panel.getFormValue('columnGroup.customColumnType');

								const w = this.getWindow();
								const tabs = w.items.get(0);

								const valuesTab = w.getTabPosition('Values');
								if (valuesTab === 0 && customColumnType === 'COLUMN_VALUE') {
									tabs.insert(1, this.valuesTab);
								}
								else if (valuesTab !== 0 && customColumnType !== 'COLUMN_VALUE') {
									tabs.remove(valuesTab);
								}

								const datedValuesTab = w.getTabPosition('Dated Values');
								if (datedValuesTab === 0 && customColumnType === 'COLUMN_DATED_VALUE') {
									tabs.insert(1, this.datedValuesTab);
								}
								else if (datedValuesTab !== 0 && customColumnType !== 'COLUMN_DATED_VALUE') {
									tabs.remove(datedValuesTab);
								}

								const jsonValuesTab = w.getTabPosition('Json Values');
								if (jsonValuesTab === 0 && customColumnType === 'COLUMN_JSON_VALUE') {
									tabs.insert(1, this.jsonValuesTab);
								}
								else if (jsonValuesTab !== 0 && customColumnType !== 'COLUMN_JSON_VALUE') {
									tabs.remove(jsonValuesTab);
								}
							}
						},
						items: [
							{
								xtype: 'fieldset',
								title: 'Table/Group/Link',
								items: [
									{fieldLabel: 'Table', name: 'table.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow', detailIdField: 'table.id'},
									{fieldLabel: 'Group', name: 'columnGroup.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.ColumnGroupWindow', detailIdField: 'columnGroup.id'},
									{fieldLabel: 'Custom Column Type', name: 'columnGroup.customColumnType', xtype: 'displayfield', submitField: false},
									{fieldLabel: 'Link Field', name: 'columnGroup.linkedBeanProperty', xtype: 'displayfield', submitField: false},
									{fieldLabel: 'Link Value', name: 'linkedValue', requiredFields: ['columnGroup.linkedBeanProperty']},
									{fieldLabel: 'Link Label', name: 'linkedLabel', requiredFields: ['linkedValue']}
								]
							},
							{
								fieldLabel: 'Depends On', name: 'dependedColumn.name', hiddenName: 'dependedColumn.id', xtype: 'combo', url: 'systemColumnListFind.json',
								beforequery: function(queryEvent) {
									const f = queryEvent.combo.getParentForm().getForm();
									const groupId = TCG.getValue('columnGroup.id', f.formValues);
									const linkValue = TCG.getValue('linkedValue', f.formValues);
									queryEvent.combo.store.baseParams = {columnGroupId: groupId, linkedValue: linkValue, includeNullLinkedValue: true, customOnly: true};
								}
							},
							{
								fieldLabel: 'Template Column', name: 'template.name', hiddenName: 'template.id', xtype: 'combo', url: 'systemColumnTemplateListFind.json', detailPageClass: 'Clifton.system.schema.TemplateColumnWindow', detailIdField: 'template.id',
								beforequery: function(queryEvent) {
									const f = queryEvent.combo.getParentForm().getForm();
									const groupId = TCG.getValue('columnGroup.id', f.formValues);
									queryEvent.combo.store.baseParams = {systemColumnGroupId: groupId};
								}
							},
							{fieldLabel: 'Column Name', name: 'name'},
							{boxLabel: 'Use the following Label in UI instead of the Column Name', name: 'labelUsed', xtype: 'checkbox'},
							{fieldLabel: 'Label', name: 'labelOverride', requiredFields: ['labelUsed']},
							{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 35, grow: true},
							{fieldLabel: 'Data Type', name: 'dataType.name', hiddenName: 'dataType.id', xtype: 'combo', url: 'systemDataTypeList.json', detailPageClass: 'Clifton.system.schema.DataTypeWindow', loadAll: true},
							{fieldLabel: 'Value List Table', name: 'valueTable.name', hiddenName: 'valueTable.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'},
							{fieldLabel: 'Value List URL', name: 'valueListUrl'},
							{fieldLabel: 'UI Config', name: 'userInterfaceConfig', xtype: 'textarea', height: 35, grow: true, qtip: 'Optional User Interface configuration that will customize this field.'},
							{fieldLabel: 'Grid Filter UI Config', name: 'gridFilterUserInterfaceConfig', xtype: 'textarea', height: 35, grow: true, qtip: 'Optional User Interface configuration that will customize this field in grid filters. Used by Json Custom Columns that are supported by grids.'},
							{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', minValue: 0, allowBlank: false},
							{fieldLabel: 'Required', name: 'required', xtype: 'checkbox'},
							{fieldLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true},

							{
								xtype: 'fieldset-checkbox',
								title: 'Default Values',
								checkboxName: 'defaultUsed',
								instructions: 'The following value/text is used to default field values for this field on screen.  If a field is not defined for an entity, the global default value/text will be used.',
								items: [
									// Entity Modify Fields
									{fieldLabel: 'Value', name: 'defaultValue'},
									{fieldLabel: 'Text', name: 'defaultText'},
									{fieldLabel: 'Global Value', name: 'globalDefaultValue'},
									{fieldLabel: 'Global Text', name: 'globalDefaultText'}
								]
							}
						],


						valuesTab: {
							title: 'Values',
							items:
								[{
									name: 'systemColumnValueListFind',
									xtype: 'gridpanel',
									instructions: 'A list of all values for selected custom System Column.',
									columns: [
										{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
										{header: 'FK Field ID', width: 50, dataIndex: 'fkFieldId', type: 'int', defaultSortColumn: true},
										{header: 'Value', width: 150, dataIndex: 'value'},
										{header: 'Text', width: 150, dataIndex: 'text'},
										{
											header: 'View', width: 35, dataIndex: 'column.table.detailScreenClass', align: 'center', filter: false, sortable: false,
											renderer: function(clz, args, r) {
												return TCG.renderActionColumn('view', 'View', 'View entity that has this value', 'view');
											}
										}
									],
									getLoadParams: function() {
										return {
											columnId: this.getWindow().getMainFormId()
										};
									},
									gridConfig: {
										listeners: {
											'rowclick': function(grid, rowIndex, evt) {
												if (TCG.isActionColumn(evt.target)) {
													const row = grid.store.data.items[rowIndex];
													const clz = row.json.column.table.detailScreenClass;
													const id = row.json.fkFieldId;
													const cmpId = TCG.getComponentId(clz, id);
													TCG.createComponent(clz, {
														id: cmpId,
														params: {id: id},
														openerCt: grid.ownerGridPanel
													});
												}
											}
										}
									}
								}]
						},


						jsonValuesTab: {
							title: 'Json Values',
							items:
								[{
									xtype: 'label',
									html: 'This feature is not supported at this time.'
								}]
						},


						datedValuesTab: {
							title: 'Dated Values',
							items: [{
								name: 'systemColumnDatedValueListFind',
								xtype: 'gridpanel',
								instructions: 'A list of all dated values for selected custom System Column.',
								columns: [
									{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
									{header: 'FK Field ID', width: 50, dataIndex: 'fkFieldId', type: 'int', defaultSortColumn: true},
									{header: 'Value', width: 100, dataIndex: 'value'},
									{header: 'Text', width: 100, dataIndex: 'text'},
									{header: 'Start Date', width: 35, dataIndex: 'startDate'},
									{header: 'End Date', width: 35, dataIndex: 'endDate'},
									{
										header: 'View', width: 35, dataIndex: 'column.table.detailScreenClass', align: 'center', filter: false, sortable: false,
										renderer: function(clz, args, r) {
											return TCG.renderActionColumn('view', 'View', 'View entity that has this value', 'view');
										}
									}
								],
								getLoadParams: function() {
									return {
										columnId: this.getWindow().getMainFormId()
									};
								},
								editor: {
									detailPageClass: 'Clifton.system.schema.ColumnDatedValueWindow',
									drillDownOnly: true
								},
								gridConfig: {
									listeners: {
										'rowclick': function(grid, rowIndex, evt) {
											if (TCG.isActionColumn(evt.target)) {
												const row = grid.store.data.items[rowIndex];
												const clz = row.json.column.table.detailScreenClass;
												const id = row.json.fkFieldId;
												const cmpId = TCG.getComponentId(clz, id);
												TCG.createComponent(clz, {
													id: cmpId,
													params: {id: id},
													openerCt: grid.ownerGridPanel
												});
											}
										}
									}
								}
							}]
						}
					}
				]
			}
		]
	}]
});
