Clifton.system.schema.SchemaSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'systemSchemaSetupWindow',
	title: 'Schema',
	iconCls: 'grid',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Tables',
				items: [{
					name: 'systemTableListFind',
					xtype: 'gridpanel',
					instructions: 'A list of system database tables. Double click on a table to view/edit its properties: audit type, column labels, validation, etc.',
					viewNames: ['Default View', 'UI Config', 'Modify Conditions'],
					topToolbarSearchParameter: 'searchPattern',
					rowSelectionModel: 'multiple',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Data Source', width: 50, dataIndex: 'dataSource.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'systemDataSourceList.json', loadAll: true}},
						{header: 'Table Name', width: 100, dataIndex: 'name'},
						{header: 'Table Label', width: 100, dataIndex: 'label', hidden: true},
						{header: 'Table Alias', width: 30, dataIndex: 'alias', hidden: true},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
						{
							header: 'Audit Type', width: 60, dataIndex: 'auditType.name', filter: {type: 'combo', searchFieldName: 'auditTypeId', url: 'systemAuditTypeList.json', loadAll: true},
							viewNames: ['Default View']
						},
						{
							header: 'Security Resource', width: 100, dataIndex: 'securityResource.labelExpanded', filter: {searchFieldName: 'securityResourceName'},
							viewNames: ['Default View']
						},
						{
							header: 'Upload Allowed', width: 30, dataIndex: 'uploadAllowed', type: 'boolean',
							viewNames: ['Default View']
						},
						{
							header: 'Insert Condition', width: 100, dataIndex: 'insertEntityCondition.name', hidden: true, filter: {searchFieldName: 'insertEntityConditionName'},
							qtip: 'Condition is evaluated before entities can be created.  If the condition returns true, then the action is performed; if false, then the user is prohibited from performing that action.',
							viewNames: ['Modify Conditions']
						},
						{
							header: 'Update Condition', width: 100, dataIndex: 'updateEntityCondition.name', hidden: true, filter: {searchFieldName: 'updateEntityConditionName'},
							qtip: 'Condition is evaluated before entities can be updated.  If the condition returns true, then the action is performed; if false, then the user is prohibited from performing that action.',
							viewNames: ['Modify Conditions']
						},
						{
							header: 'Delete Condition', width: 100, dataIndex: 'deleteEntityCondition.name', hidden: true, filter: {searchFieldName: 'deleteEntityConditionName'},
							qtip: 'Condition is evaluated before entities can be deleted.  If the condition returns true, then the action is performed; if false, then the user is prohibited from performing that action.',
							viewNames: ['Modify Conditions']
						},
						{
							header: 'Detail Screen Class', width: 100, dataIndex: 'detailScreenClass', hidden: true,
							viewNames: ['UI Config']
						},
						{
							header: 'List Screen Class', width: 100, dataIndex: 'listScreenClass', hidden: true,
							viewNames: ['UI Config']
						},
						{
							header: 'Entity List URL', width: 100, dataIndex: 'entityListUrl', hidden: true,
							viewNames: ['UI Config']
						},
						{
							header: 'Screen Icon Cls', width: 50, dataIndex: 'screenIconCls', hidden: true,
							viewNames: ['UI Config']
						},
						{header: 'Max Query Size Limit', width: 30, dataIndex: 'maxQuerySizeLimit', type: 'int', hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.schema.TableWindow',
						addEditButtons: function(t, gridPanel) {
							t.add({
								text: 'Copy...',
								tooltip: 'Copies fields from selected Table(s) and Columns into corresponding Tables/Columns from a different Data Source.',
								iconCls: 'copy',
								handler: function() {
									const selectionModel = gridPanel.grid.getSelectionModel();
									if (selectionModel.getCount() < 1) {
										TCG.showError('Please select at least one table to copy from.', 'No Rows Selected');
									}
									else {
										const selections = selectionModel.getSelections();
										const fromDataSource = selections[0].json.dataSource.name;
										let tables = selections[0].json.name;
										for (let i = 1; i < selections.length; i++) {
											if (fromDataSource !== selections[i].json.dataSource.name) {
												TCG.showError('All selected Tables must belong to the same Data Source.', 'Invalid Selection');
												tables = null;
												break;
											}
											tables += ',' + selections[i].json.name;
										}
										if (tables !== null) {
											TCG.createComponent('Clifton.system.schema.CopySchemaFieldsWindow', {
												defaultData: {
													fromDataSource: fromDataSource,
													tables: tables
												},
												openerCt: gridPanel
											});
										}
									}
								}
							});
							t.add('-');
						}
					}
				}]
			},


			{
				title: 'Columns',
				items: [{
					name: 'systemColumnListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all database columns (both standard and custom) for all tables in the system.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Data Source', width: 70, dataIndex: 'table.dataSource.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'systemDataSourceList.json', loadAll: true}},
						{header: 'Table Name', width: 100, dataIndex: 'table.name', filter: {searchFieldName: 'tableName'}},
						{header: 'Column Name', width: 100, dataIndex: 'name'},
						{header: 'Bean Property', width: 100, dataIndex: 'beanPropertyName', hidden: true},
						{header: 'Column Label', width: 100, dataIndex: 'label'},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 60, dataIndex: 'dataType.name', filter: {searchFieldName: 'dataTypeName'}},
						{header: 'Audit Type', width: 100, dataIndex: 'auditType.name', filter: {searchFieldName: 'auditTypeName'}},
						{header: 'Value List URL', width: 100, dataIndex: 'valueListUrl', hidden: true},
						{header: 'Template Column', width: 60, dataIndex: 'template.name', hidden: true},
						{header: 'System', width: 40, dataIndex: 'systemDefined', type: 'boolean'},
						{header: 'Custom', width: 40, dataIndex: 'customColumn', type: 'boolean'},
						{
							header: 'Custom Type', width: 60, dataIndex: 'columnGroup.customColumnType', tooltip: 'Defines how and where the custom values are stored.',
							filter: {
								searchFieldName: 'customColumnType',
								type: 'combo', mode: 'local',
								store: {
									xtype: 'arraystore',
									data: Clifton.system.schema.CustomColumnTypes
								}
							}
						},
						{header: 'Column Group', width: 80, dataIndex: 'columnGroup.name', filter: {searchFieldName: 'columnGroupName'}},
						{header: 'Linked Value', width: 50, dataIndex: 'linkedValue', hidden: true},
						{header: 'Linked Label', width: 100, dataIndex: 'linkedLabel'},
						{header: 'Dependent Column', width: 50, dataIndex: 'dependedColumn.name', filter: {searchFieldName: 'dependentColumnName'}, hidden: true},
						{header: 'Value Table', width: 100, dataIndex: 'valueTable.name', filter: {searchFieldName: 'valueTableName'}, hidden: true},
						{header: 'UI Config', width: 50, dataIndex: 'userInterfaceConfig', hidden: true},
						{header: 'Grid Filter UI Config', width: 50, dataIndex: 'gridFilterUserInterfaceConfig', hidden: true, tooltip: 'Optional User Interface configuration that will customize this field in grid filters. Used by Json Custom Columns that are supported by grids.'},
						{header: 'Order', width: 30, dataIndex: 'order', hidden: true, type: 'int', useNull: true},
						{header: 'Required', width: 50, dataIndex: 'required', hidden: true, type: 'boolean'},
						{header: 'Default Value', width: 50, dataIndex: 'defaultValue', hidden: true},
						{header: 'Default Text', width: 50, dataIndex: 'defaultText', hidden: true},
						{header: 'Global Default Value', width: 50, dataIndex: 'globalDefaultValue', hidden: true},
						{header: 'Global Default Text', width: 50, dataIndex: 'globalDefaultText', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Data Types',
				items: [{
					name: 'systemDataTypeList',
					xtype: 'gridpanel',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Data Type', width: 50, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Numeric', width: 30, dataIndex: 'numeric', type: 'boolean'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.schema.DataTypeWindow'
					}
				}]
			},


			{
				title: 'Data Sources',
				items: [{
					name: 'systemDataSourceListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all data sources used by the system.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Data Source', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Database Name', width: 50, dataIndex: 'databaseName'},
						{header: 'Alias', width: 30, dataIndex: 'alias'},
						{header: 'Connection URL', width: 100, dataIndex: 'connectionUrl'},
						{header: 'Application URL', width: 100, dataIndex: 'applicationUrl'},
						{header: 'External', width: 20, dataIndex: 'external', type: 'boolean', tooltip: 'If true, this data source is serviced by an external API.'},
						{header: 'Default', width: 20, dataIndex: 'defaultDataSource', type: 'boolean'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.schema.DataSourceWindow'
					}
				}]
			},


			{
				title: 'Soft Links',
				items: [{
					name: 'systemSoftLinkList',
					xtype: 'gridpanel',
					groupField: 'coalesceOwnerTableName',
					groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Links" : "Link"]})',
					remoteSort: true,
					columns: [
						{header: 'Owner/Table', width: 50, dataIndex: 'coalesceOwnerTableName', hidden: true},
						{header: 'Table', width: 50, dataIndex: 'tableName', hidden: true},
						{header: 'Label', width: 50, dataIndex: 'label'},
						{header: 'Owner Table', width: 50, dataIndex: 'ownerTableName', hidden: true},
						{header: 'Owner Bean Property', width: 40, dataIndex: 'ownerBeanPropertyName'},
						{header: 'Bean Property', width: 50, dataIndex: 'beanPropertyName'},
						{header: 'Bean Data Type', width: 35, dataIndex: 'beanDataType'},
						{header: 'Link Table Bean Property', width: 50, dataIndex: 'tableBeanPropertyName'},
						{
							header: 'Second Link Table Bean Property', width: 50, dataIndex: 'secondTableBeanPropertyName',
							tooltip: 'For some cases we have table selected in the setup, but actual cases can also set the specific table for the link.  For example, Warning Types can have a cause table defined, but some warnings that are generated by the system will create warnings and override or set the table.  In these cases, the first table is checked and if it is null then the second table is used.'
						},
						{header: 'Exclude from Used By', width: 35, dataIndex: 'excludeFromUsedBy', type: 'boolean'}
					],
					editor: {
						drillDownOnly: true
					}
				}]
			},

			{
				title: 'Audit Management',
				items: [{
					name: 'systemAuditTableConfigList',
					xtype: 'gridpanel',
					instructions: 'The following can be used to validate audit observers are properly registered based on the table/column configuration.  Audit Observers can be re-registered for all or specific tables. The auditing level is a merged view of the table and its columns.  So, even if the table does not audit inserts, if at least one column in the table does, then it is flagged to audit inserts and should be registered.',
					columns: [
						{header: 'Table Name', width: 70, dataIndex: 'tableName', nonPersistentField: true},

						{header: 'Audit Insert', width: 40, type: 'boolean', dataIndex: 'auditType.auditInsert', nonPersistentField: true},
						{header: 'Insert Observer Registered', width: 40, type: 'boolean', dataIndex: 'insertObserverRegistered', nonPersistentField: true},

						{header: 'Audit Update', width: 40, type: 'boolean', dataIndex: 'auditType.auditUpdate', nonPersistentField: true},
						{header: 'Update Observer Registered', width: 40, type: 'boolean', dataIndex: 'updateObserverRegistered', nonPersistentField: true},

						{header: 'Audit Delete', width: 40, type: 'boolean', dataIndex: 'auditType.auditDelete', nonPersistentField: true},
						{header: 'Delete Observer Registered', width: 40, type: 'boolean', dataIndex: 'deleteObserverRegistered', nonPersistentField: true},

						{header: 'Invalid', width: 40, type: 'boolean', dataIndex: 'invalid', nonPersistenField: true, tooltip: 'Invalid means that there is at least one mismatch for the registered auditors'}
					],
					getLoadParams: function(firstLoad) {
						return {invalidOnly: TCG.getChildByName(this.getTopToolbar(), 'invalidOnly').checked, requestedMaxDepth: 3};
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							'-',
							{fieldLabel: '', boxLabel: 'Show Invalid Only&nbsp;', xtype: 'toolbar-checkbox', name: 'invalidOnly', qtip: 'If checked will only display those that are invalid (mismatch auditing type and observers)', checked: true}
						];
					},
					addFirstToolbarButtons: function(toolBar) {
						const gp = this;
						toolBar.add({
							iconCls: 'run',
							text: 'Register Audit Observers',
							tooltip: 'Re-register audit observers for selected table (or all if no table is selected)',
							scope: this,
							handler: function() {
								const grid = this.grid;
								const sm = grid.getSelectionModel();
								if (sm.getCount() === 0) {
									Ext.Msg.confirm('Re-Register Auditors for All Tables?', 'Are you sure you want to re-register auditors for all tables?  If not, please cancel and select a specific row to re-register for that table only.', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitTarget: grid.ownerCt,
												waitMsg: 'Registering...',
												onLoad: function(record, conf) {
													gp.reload();
												}
											});
											loader.load('systemAuditorsRegister.json');
										}
									});
								}
								else if (sm.getCount() !== 1) {
									TCG.showError('Multi-selection clearing is not supported yet.  Please select one row.', 'NOT SUPPORTED');
								}
								else {
									const data = sm.getSelected().data;
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid.ownerCt,
										waitMsg: 'Registering...',
										params: {
											tableName: data.tableName
										},
										onLoad: function(record, conf) {
											gp.reload();
										}
									});
									loader.load('systemAuditorsForTableNameRegister.json');
								}
							}
						});
						toolBar.add('-');
					}
				}]
			},


			{
				title: 'Bean Definitions',
				items: [{
					name: 'contextBeanDefinitionList',
					xtype: 'gridpanel',
					instructions: 'A list of all application context bean definitions as well as descriptors for singleton beans registered directly in the context that do not have a definition.',
					columns: [
						{header: 'Bean Name', width: 80, dataIndex: 'beanName', nonPersistentField: true},
						{header: 'Bean Class', width: 120, dataIndex: 'beanClassName', nonPersistentField: true},
						{header: 'Definition Source', width: 80, dataIndex: 'definitionSource', tooltip: 'The source from which this bean was defined. Examples of definition sources include context XML files, <tt>@Bean</tt>-annotated methods, <tt>@Component</tt>-annotated classes, and programmatic generation (such as via <tt>BeanFactoryPostProcessor</tt> beans).', nonPersistentField: true},
						{header: 'Resource Description', width: 120, dataIndex: 'resourceDescription', nonPersistentField: true, hidden: true},
						{header: 'Singleton', width: 20, dataIndex: 'singleton', type: 'boolean', nonPersistentField: true},
						{header: 'Autowire', width: 20, dataIndex: 'autowireCandidate', type: 'boolean', nonPersistentField: true},
						{header: 'Lazy Init', width: 20, dataIndex: 'lazyInit', type: 'boolean', nonPersistentField: true},
						{header: 'No Definition', width: 20, dataIndex: 'withoutDefinition', type: 'boolean', nonPersistentField: true}
					],
					editor: {
						drillDownOnly: true,
						init: function(grid) {
							this.grid = grid;
							this.grid.addListener('rowdblclick', function(grid, rowIndex, evt) {
								const row = grid.store.data.items[rowIndex];
								TCG.data.getDataValuePromise('contextBeanDetails.json?beanName=' + row.data.beanName, grid)
									.then(function(result) {
										if (result) {
											new TCG.app.CloseWindow({
												title: 'Bean Details - ' + row.data.beanName,
												width: 1000,
												height: 600,
												modal: true,
												openerCt: grid,
												items: [{
													xtype: 'formpanel',
													items: [],
													listeners: {
														afterrender: function(fp) {
															const div = document.createElement('div');
															div.setAttribute('style', 'height: 100%; overflow-y:auto;');
															fp.body.appendChild(div);
															this.jsonEditor = new JSONEditor(div, {}, Ext.decode(result));
															fp.doLayout();
														}
													}
												}]
											});
										}
									});
							}, this);
						}
					}
				}]
			},


			{
				title: 'URL Mappings',
				items: [{
					name: 'contextUrlMappingDefinitionList',
					xtype: 'gridpanel',
					instructions: 'A list of all URL for services exposed by the system. Each URL maps to corresponding method of system managed bean.  See hidden columns for details.',
					columns: [
						{header: 'URL', width: 65, dataIndex: 'url', nonPersistentField: true},
						{
							header: 'Return Type', width: 45, dataIndex: 'methodReturnTypeShort', nonPersistentField: true,
							renderer: function(v, metaData, r) {
								metaData.attr = TCG.renderQtip(r.json.methodReturnType);
								return v;
							}
						},
						{header: 'Return Type Detailed', width: 100, dataIndex: 'methodReturnType', hidden: true, nonPersistentField: true},
						{
							header: 'Parameters', width: 120, dataIndex: 'methodParametersShort', nonPersistentField: true,
							renderer: function(v, metaData, r) {
								metaData.attr = TCG.renderQtip(r.json.methodParameters);
								return v;
							}
						},
						{header: 'Parameters (Detailed)', width: 150, dataIndex: 'methodParameters', hidden: true, nonPersistentField: true},
						{header: 'Bean Name', width: 40, dataIndex: 'beanName', hidden: true, nonPersistentField: true},
						{header: 'Bean Class', width: 100, dataIndex: 'beanClassName', hidden: true, nonPersistentField: true},
						{header: 'Method Name', width: 70, dataIndex: 'methodName', hidden: true, nonPersistentField: true},
						{header: 'Method Signature', width: 200, dataIndex: 'methodSignature', hidden: true, nonPersistentField: true},
						{header: 'Request Method', width: 20, dataIndex: 'requestMethod', nonPersistentField: true}
					],
					editor: {
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Migration Tables',
				items: [{
					name: 'tableListFind',
					instructions: 'Migration Table information as defined in Hibernate migration schemas files.',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					limitRequestedProperties: true,
					isPagingEnabled: function() {
						return false;
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {versionSupported: TCG.getChildByName(this.getTopToolbar(), 'versionSupported').checked, requestedMaxDepth: 2};
					},
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							'-',
							{fieldLabel: '', boxLabel: 'Versioning Allowed&nbsp;', xtype: 'toolbar-checkbox', name: 'versionSupported', qtip: 'If checked will include any tables that have an rv column and therefore are available to add versioning to'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					columns: [
						{header: 'Table Name', width: 150, dataIndex: 'name', defaultSortColumn: true, defaultSortDirection: 'ASC'},
						{header: 'Data Source', width: 100, dataIndex: 'dataSource'},
						{header: 'Operation Type', width: 100, dataIndex: 'operationType', hidden: true},
						{header: 'Audit Type', width: 100, dataIndex: 'auditType'},
						{header: 'Cache Type', width: 70, dataIndex: 'cacheType'},
						{header: 'Table Type', width: 100, dataIndex: 'tableType'},
						{header: 'Dao Name', width: 100, dataIndex: 'daoName', hidden: true},
						{header: 'DTO Class', width: 150, dataIndex: 'dtoClass', hidden: true},
						{header: 'DTO Proxy', width: 150, dataIndex: 'dtoProxy', hidden: true},
						{header: 'Encoding', width: 40, dataIndex: 'defaultDataTypeEncodingType'},
						{header: 'Upload Allowed', width: 60, dataIndex: 'uploadAllowed', type: 'boolean', hidden: true},
						{header: 'Upload Service Bean', width: 100, dataIndex: 'uploadServiceBeanName', hidden: true},
						{header: 'Upload Service Method', width: 100, dataIndex: 'uploadServiceMethodName'},
						{header: 'Discriminator Formula', width: 100, dataIndex: 'discriminatorFormula', hidden: true},
						{header: 'Discriminator Value', width: 100, dataIndex: 'discriminatorValue', hidden: true},
						{header: 'Exclude From SQL', width: 60, dataIndex: 'excludeFromSql', type: 'boolean', hidden: true},
						{header: 'Version', width: 50, dataIndex: 'version', type: 'boolean'}
					]
				}]
			},


			{
				title: 'Migration Columns',
				items: [{
					name: 'columnListFind',
					instructions: 'Migration Column information for this table as defined in Hibernate migration schemas files.',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					limitRequestedProperties: true,
					topToolbarSearchParameter: 'searchPattern',
					isPagingEnabled: function() {
						return false;
					},
					columns: [
						{header: 'Table Name', width: 150, dataIndex: 'tableName', filter: {searchFieldName: 'tableNameContains'}, defaultSortColumn: true, defaultSortDirection: 'ASC'},
						{header: 'Column Name', width: 150, dataIndex: 'name'},
						{header: 'Bean Property', width: 150, dataIndex: 'beanPropertyName', hidden: true},
						{header: 'Data Type', width: 80, dataIndex: 'dataType'},
						{header: 'Required', width: 60, dataIndex: 'required', type: 'boolean'},
						{header: 'Required for Validation', width: 60, dataIndex: 'required', type: 'boolean', hidden: true},
						{header: 'Default Value', width: 100, dataIndex: 'defaultValue', hidden: true},
						{header: 'Sort Order', width: 50, dataIndex: 'sortOrder', type: 'int', useNull: true},
						{header: 'Sort Direction', width: 60, dataIndex: 'sortDirection'},
						{header: 'Upload Converter', width: 100, dataIndex: 'uploadConverterName', hidden: true},
						{header: 'Upload Lookup Service Bean', width: 100, dataIndex: 'uploadLookupServiceBeanName', hidden: true},
						{header: 'Upload Lookup Service Method', width: 100, dataIndex: 'uploadLookupServiceMethodName', hidden: true},
						{header: 'Parent', width: 60, dataIndex: 'parent', type: 'boolean', hidden: true},
						{header: 'Indexed', width: 60, dataIndex: 'indexed', type: 'boolean'},
						{header: 'Unique Index', width: 60, dataIndex: 'uniqueIndex', type: 'boolean'},
						{header: 'FK Table', width: 100, dataIndex: 'fkTable'},
						{header: 'FK Field', width: 100, dataIndex: 'fkField', hidden: true},
						{header: 'Lazy', width: 100, dataIndex: 'lazy', hidden: true},
						{header: 'Natural Key', width: 60, dataIndex: 'naturalKey', type: 'boolean'},
						{header: 'Ignore Upload', width: 60, dataIndex: 'ignoreUpload', type: 'boolean', hidden: true},
						{header: 'Delayed', width: 60, dataIndex: 'delayed', type: 'boolean', hidden: true},
						{header: 'Force Simple Property', width: 60, dataIndex: 'forceSimpleProperty', type: 'boolean', hidden: true},
						{header: 'Exclude From Inserts', width: 60, dataIndex: 'excludedFromInserts', type: 'boolean', hidden: true},
						{header: 'Exclude From Updates', width: 60, dataIndex: 'excludedFromUpdates', type: 'boolean', hidden: true},
						{header: 'Type Definition Name', width: 100, dataIndex: 'typeDefinitionName', hidden: true, renderer: v => TCG.renderPre(v)},
						{header: 'Type Definition Class Name', width: 100, dataIndex: 'typeDefinitionClassName', hidden: true, renderer: v => TCG.renderPre(v)},
						{header: 'Type Definition Converter Class', width: 100, dataIndex: 'typeDefinitionConverterClass', hidden: true, renderer: v => TCG.renderPre(v)},
						{header: 'Audit Type', width: 100, dataIndex: 'auditType', hidden: true},
						{header: 'Calculation', width: 100, dataIndex: 'calculation', hidden: true}
					]
				}]
			},


			{
				title: 'Migration Metadata',
				items: [{
					name: 'migrationModuleVersionListFind',
					instructions: 'Metadata from env.MigrationModuleVersion table regarding each migration step that was executed.',
					xtype: 'gridpanel',
					limitRequestedProperties: true,
					groupField: 'migrationModuleName',
					groupTextTpl: '{values.group} - {[values.rs.length]} {[values.rs.length > 1 ? "DB Migrations" : "DB Migration"]}',
					topToolbarSearchParameter: 'searchPattern',
					isPagingEnabled: function() {
						return true;
					},
					columns: [
						{header: 'Module Name', width: 10, dataIndex: 'migrationModuleName', hidden: true},
						{header: 'Migration File Path', width: 28, dataIndex: 'migrationPath'},
						{header: 'Version', width: 5, dataIndex: 'versionNumber', type: 'int', doNotFormat: true},
						{header: 'Pre DDL Completed', width: 10, dataIndex: 'preDdlStepCompleted', type: 'boolean'},
						{header: 'DDL Completed', width: 8, dataIndex: 'ddlStepCompleted', type: 'boolean'},
						{header: 'Meta Data Completed', width: 10, dataIndex: 'metaDataStepCompleted', type: 'boolean'},
						{header: 'Data Completed', width: 8, dataIndex: 'dataStepCompleted', type: 'boolean'},
						{header: 'Action Completed', width: 9, dataIndex: 'actionStepCompleted', type: 'boolean'},
						{header: 'Data Action Completed', width: 12, dataIndex: 'dataActionStepCompleted', type: 'boolean'},
						{header: 'SQL Completed', width: 8, dataIndex: 'sqlStepCompleted', type: 'boolean'},
						{header: 'Skipped', width: 5, dataIndex: 'skippedDueToConditionalSql', type: 'boolean'},
						{header: 'Initial Execution', width: 10, dataIndex: 'initialExecutionDate', defaultSortColumn: true, defaultSortDirection: 'DESC', alwaysIncludeTime: true},
						{header: 'Last Execution', width: 10, dataIndex: 'lastExecutionDate', alwaysIncludeTime: true}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('initialExecutionDate', {'after': new Date().add(Date.DAY, -62)});
						}
						const params = {};
						const t = this.getTopToolbar();
						const search = TCG.getChildByName(t, 'searchPattern');

						if (TCG.isNotBlank(search.getValue())) {
							params.searchPattern = search.getValue();
						}
						return params;
					},
					addFirstToolbarButtons: function(toolBar) {
						const gp = this;
						toolBar.add({
							iconCls: 'expand-all',
							tooltip: 'Expand or Collapse Rule Assignments for all Client Accounts',
							scope: this,
							handler: function() {
								gp.grid.collapsed = !gp.grid.collapsed;
								gp.grid.view.toggleAllGroups(!gp.grid.collapsed);
							}
						});
						toolBar.add('-');
					}
				}]
			},


			{
				title: 'Table Size History',
				items: [{
					name: 'systemTableSizeHistoryListFind',
					instructions: 'Historical table size information for various databases.',
					topToolbarSearchParameter: 'searchPattern',
					xtype: 'gridpanel',
					additionalPropertiesToRequest: 'systemTable.id',
					columns: [
						{header: 'ID', width: 20, dataIndex: 'id', hidden: true, tooltip: 'ID of Database Row'},
						{header: 'Database Name', width: 30, dataIndex: 'systemTable.dataSource.databaseName', filter: {searchFieldName: 'databaseName'}},
						{header: 'Table Name', width: 55, dataIndex: 'systemTable.name', filter: {searchFieldName: 'systemTableName'}, defaultSortDirection: 'ASC'},
						{header: 'Snapshot Date', width: 30, dataIndex: 'snapshotDate'},
						{header: 'Number of Rows', width: 30, dataIndex: 'numberOfRows', type: 'int', summaryType: 'sum'},
						{header: 'Data Size (bytes)', width: 30, dataIndex: 'dataSizeInBytes', type: 'int', summaryType: 'sum'},
						{header: 'Index Size (bytes)', width: 30, dataIndex: 'indexSizeInBytes', type: 'int', summaryType: 'sum'},
						{header: 'Unused Size (bytes)', width: 30, dataIndex: 'unusedSizeInBytes', type: 'int', summaryType: 'sum'},
						{header: 'Total Size (bytes)', width: 30, dataIndex: 'totalSizeInBytes', type: 'int', summaryType: 'sum', defaultSortColumn: true, defaultSortDirection: 'desc'}
					],
					plugins: {ptype: 'gridsummary'},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.schema.TableWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.systemTable.id;
						}
					},
					getLoadParams: function(firstLoad) {
						const params = this.getTopToolbarInitialLoadParams(firstLoad) || {};
						const searchPattern = TCG.getChildByName(this.getTopToolbar(), 'searchPattern');
						if (firstLoad) {
							searchPattern.focus(false, 500);
							this.setFilterValue('snapshotDate', {'after': new Date().add(Date.DAY, -10)});
						}
						if (TCG.isNotBlank(searchPattern.getValue())) {
							params[this.topToolbarSearchParameter] = searchPattern.getValue();
						}
						return params;
					}
				}]
			}
		]
	}]
});


Clifton.system.schema.CopySchemaFieldsWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Copy Schema Fields',
	iconCls: 'copy',
	height: 400,
	width: 750,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Copies the fields specified below for selected Table(s) and their corresponding Columns from selected Data Source to the target Data Source.',
		items: [
			{fieldLabel: 'From Data Source', name: 'fromDataSource', readOnly: true},
			{fieldLabel: 'To Data Source', xtype: 'combo', name: 'toDataSource', url: 'systemDataSourceList.json', loadAll: true, allowBlank: false},
			{fieldLabel: 'Tables', name: 'tables', xtype: 'textarea', height: 80, readOnly: true},

			{boxLabel: 'Copy Table label and description', name: 'copyTableLabelAndDescription', xtype: 'checkbox', checked: true},
			{boxLabel: 'Copy Audit Type', name: 'copyAuditType', xtype: 'checkbox', checked: true},
			{boxLabel: 'Copy Security Resource', name: 'copySecurityResource', xtype: 'checkbox', checked: true},
			{boxLabel: 'Copy User Interface Configuration', name: 'copyUserInterfaceConfiguration', xtype: 'checkbox', checked: true},
			{boxLabel: 'Copy Column Information (label, description, audit type, value list URL)', name: 'copyColumnInformation', xtype: 'checkbox', checked: true},
			{boxLabel: 'Overwrite existing values (uncheck to skip fields if the value already exists', name: 'overwriteExistingValues', xtype: 'checkbox'}
		],
		getSaveURL: function() {
			return 'systemSchemaFieldsCopy.json';
		}
	}],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Copying...',
			success: function(form, action) {
				const result = action.result.status;
				TCG.createComponent('Clifton.core.StatusWindow', {
					title: 'Copy Schema Fields Status',
					defaultData: {status: result}
				});
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	}
});
