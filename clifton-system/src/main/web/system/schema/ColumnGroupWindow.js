Clifton.system.schema.ColumnGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Column Group',
	iconCls: 'grid',
	height: 450,
	width: 700,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Column Group',
				items: [{
					xtype: 'formpanel',
					url: 'systemColumnGroup.json',
					instructions: 'Column Group information is listed below.  You can override the table security for this column group in the Security Override field.',
					labelWidth: 150,
					items: [
						{fieldLabel: 'Table', name: 'table.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow', detailIdField: 'table.id'},
						{fieldLabel: 'Group Name', name: 'name', disabled: true},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', disabled: true},
						{fieldLabel: 'Linked Bean Property', name: 'linkedBeanProperty', disabled: true},
						{
							fieldLabel: 'Custom Column Type', xtype: 'combo', qtip: 'Defines how and where the custom column values are stored. If values are entered for a specific date range or stored as separate system column values', name: 'customColumnType', mode: 'local',
							store: {
								xtype: 'arraystore',
								data: Clifton.system.schema.CustomColumnTypes
							},
							disabled: true
						},
						{fieldLabel: 'JSON Value Column', name: 'jsonValueColumn.label', disabled: true, detailPageClass: 'Clifton.system.schema.ColumnWindow', detailIdField: 'jsonValueColumn.id'},
						{xtype: 'label', html: '<hr />'},
						{fieldLabel: 'Table Security Resource', name: 'table.securityResource.labelExpanded', xtype: 'displayfield'},
						{fieldLabel: 'Security Resource', name: 'securityResource.labelExpanded', hiddenName: 'securityResource.id', displayField: 'labelExpanded', queryParam: 'labelExpanded', xtype: 'combo', url: 'securityResourceListFind.json', qtip: 'If not set, Table\'s Security Resource will be used. Requires FULL CONTROL access to modify custom Columns and WRITE to modify column values.'}
					]
				}]
			},


			{
				title: 'Template Columns',
				items: [{
					name: 'systemColumnTemplateListFind',
					xtype: 'gridpanel',
					instructions: 'The following Template Columns are associated with this Column Group. Template Columns are used to group multiple Custom Columns that represent the same concept.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 100, dataIndex: 'name'},
						{header: 'Label', width: 100, dataIndex: 'label', hidden: true},
						{header: 'Description', width: 150, dataIndex: 'description'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {systemColumnGroupId: this.getWindow().params.id};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.TemplateColumnWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
