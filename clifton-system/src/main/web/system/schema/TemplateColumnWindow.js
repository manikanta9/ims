Clifton.system.schema.TemplateColumnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Template Column',
	width: 750,
	iconCls: 'grid',
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Template Column',
				items: [{
					xtype: 'formpanel',
					url: 'systemColumnTemplate.json',
					tbar: [{
						text: 'Assign Columns To Template',
						tooltip: 'Assign existing Columns with the same name from the same Column Group to this Template Column.',
						iconCls: 'run',
						handler: function() {
							const formPanel = TCG.getParentFormPanel(this);
							const templateId = formPanel.getIdFieldValue();
							if (templateId) {
								Ext.Msg.confirm('Assign Columns', 'Do you want to assign all existing Columns with the same name from the same Column Group to this Template Column?', function(a) {

									if (a === 'yes') {
										const loader = new TCG.data.JsonLoader({
											waitTarget: formPanel,
											params: {templateId: templateId}
										});
										loader.load('systemColumnsAssignToTemplate.json');
									}
								});
							}
							else {
								TCG.showError('The template must be saved before columns can be assigned to it.');
							}
						}
					}],
					items: [
						{fieldLabel: 'Column Group', name: 'systemColumnGroup.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.ColumnGroupWindow', detailIdField: 'systemColumnGroup.id'},
						{fieldLabel: 'Column Name', name: 'name'},
						{fieldLabel: 'Label', name: 'label'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
					]
				}]
			},


			{
				title: 'Custom Columns',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'The following custom columns utilize this template column',
					topToolbarSearchParameter: 'searchPatternCustomColumn',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group', width: 50, dataIndex: 'groupLinkedLabel', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'columnGroup.name', hidden: true, filter: {xtype: 'combo', searchFieldName: 'columnGroupId', url: 'systemColumnGroupListByTable.json'}},
						{header: 'Link Value', width: 50, dataIndex: 'linkedValue', hidden: true},
						{header: 'Link Label', width: 175, dataIndex: 'linkedLabel'},
						{header: 'Column Name', width: 75, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Data Type', width: 45, dataIndex: 'dataType.name'},
						{header: 'Order', width: 45, dataIndex: 'order', type: 'int'},
						{header: 'Required', width: 45, dataIndex: 'required', type: 'boolean'},
						{header: 'System', width: 45, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function() {
						return {templateId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
