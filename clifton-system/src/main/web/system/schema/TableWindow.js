Clifton.system.schema.TableWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Table Details',
	width: 900,
	height: 600,
	iconCls: 'grid',
	enableRefreshWindow: true,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.system.schema.TableWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Table',
				items: [{
					xtype: 'formpanel',
					url: 'systemTable.json',
					labelFieldName: 'nameExpanded',
					items: [
						{fieldLabel: 'Data Source', name: 'dataSource.name', disabled: true},
						{fieldLabel: 'Table Name', name: 'name', disabled: true},
						{fieldLabel: 'Table Label', name: 'label'},
						{fieldLabel: 'Alias', name: 'alias', maxLength: 3, qtip: 'The unique short-name identifier for the table.', disabled: true},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
						{fieldLabel: 'Audit Type', name: 'auditType.name', hiddenName: 'auditType.id', xtype: 'combo', loadAll: true, url: 'systemAuditTypeList.json'},
						{fieldLabel: 'Security Resource', name: 'securityResource.labelExpanded', hiddenName: 'securityResource.id', displayField: 'labelExpanded', xtype: 'combo', url: 'securityResourceListFind.json', queryParam: 'labelExpanded', detailPageClass: 'Clifton.security.authorization.ResourceWindow'},
						{boxLabel: 'Allow Uploads to be performed on data in this table', name: 'uploadAllowed', xtype: 'checkbox', readOnly: true},
						{
							xtype: 'fieldset-checkbox',
							checkboxName: 'hasEntityValidation',
							title: 'Entity Validation',
							instructions: 'The following fields are used to validate entities during specified actions.  If the condition returns true, then the action is performed; if false, then the user is prohibited from performing that action.',
							items: [
								{fieldLabel: 'Insert Condition', name: 'insertEntityCondition.label', hiddenName: 'insertEntityCondition.id', displayField: 'name', xtype: 'system-condition-combo'},
								{fieldLabel: 'Update Condition', name: 'updateEntityCondition.label', hiddenName: 'updateEntityCondition.id', displayField: 'name', xtype: 'system-condition-combo'},
								{fieldLabel: 'Delete Condition', name: 'deleteEntityCondition.label', hiddenName: 'deleteEntityCondition.id', displayField: 'name', xtype: 'system-condition-combo'}
							]
						},
						{
							xtype: 'fieldset',
							title: 'User Interface Configuration',
							instructions: 'The following fields can be used to define UI configuration for this table such as drill downs and drop down selections.',
							items: [
								{
									fieldLabel: 'Detail Screen Class', name: 'detailScreenClass',
									qtip: 'UI Screen Class for the Detail Page'
								},
								{
									fieldLabel: 'List Screen Class', name: 'listScreenClass',
									qtip: 'UI Screen Class for the List Page'
								},
								{
									fieldLabel: 'Icon Cls', name: 'screenIconCls',
									qtip: 'Icon Cls is the CSS Class for the icon: the image that represents this entity'
								},
								{
									fieldLabel: 'Entity List URL', name: 'entityListUrl',
									qtip: 'URL to lookup entities in this table'
								}
							]
						},
						{fieldLabel: 'Max Query Size Limit', name: 'maxQuerySizeLimit', type: 'integerField', qtip: 'Override the system default for the maximum number of records to return from queries.'}
					]
				}]
			},


			{
				title: 'Columns',
				items: [{
					name: 'systemColumnStandardListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarInitialLoadParams: function() {
						return {
							'tableId': this.getWindow().params.id
						};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 100, dataIndex: 'name'},
						{header: 'Bean Property', width: 100, dataIndex: 'beanPropertyName'},
						{header: 'Label', width: 100, dataIndex: 'label'},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 60, dataIndex: 'dataType.name'},
						{header: 'Audit Type', width: 100, dataIndex: 'auditType.name'}
					],
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Custom Columns',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'Custom columns can be assigned to tables and are classified by groups.  Groups are system defined.  Columns can apply to groups using a linked bean property where the field would only apply to the entity if the linked bean property value equals the value assigned to that column.',
					wikiPage: 'IT/System+-+Custom+Columns',
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const params = {tableId: this.getWindow().getMainFormId()};

						const t = this.getTopToolbar();
						const cg = TCG.getChildByName(t, 'columnGroupId');
						if (TCG.isNotBlank(cg.getValue())) {
							params.columnGroupId = cg.getValue();
						}
						return params;
					},
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						const tableId = this.getWindow().getMainFormId();
						return [
							{
								fieldLabel: 'Column Group', name: 'columnGroupId', width: 180, xtype: 'toolbar-combo', url: 'systemColumnGroupListByTable.json', root: 'data',
								beforequery: function(queryEvent) {
									queryEvent.combo.store.setBaseParam('tableId', tableId);
								}
							},
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					groupField: 'groupLinkedLabel',
					groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Columns" : "Column"]})',
					remoteSort: true,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Group', width: 50, dataIndex: 'groupLinkedLabel', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'columnGroup.name', hidden: true, filter: {xtype: 'combo', searchFieldName: 'columnGroupId', url: 'systemColumnGroupListByTable.json'}},
						{
							header: 'Custom Column Type', width: 50, dataIndex: 'columnGroup.customColumnType', hidden: true, filter: {
								type: 'combo', searchFieldName: 'customColumnType', mode: 'local',
								store: {
									xtype: 'arraystore',
									data: Clifton.system.schema.CustomColumnTypes
								}
							}
						},
						{header: 'Link Field', width: 60, dataIndex: 'columnGroup.linkedBeanProperty', hidden: true},
						{header: 'Link Value', width: 40, dataIndex: 'linkedValue', hidden: true},
						{header: 'Link Label', width: 60, dataIndex: 'linkedLabel', hidden: true},
						{header: 'Column Name', width: 100, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Template Column', width: 100, dataIndex: 'template.name', hidden: true},
						{header: 'Depends On', width: 100, dataIndex: 'dependedColumn.name', hidden: true, filter: {searchFieldName: 'dependedColumnName'}},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Order', width: 40, dataIndex: 'order', type: 'int'},
						{header: 'Required', width: 40, dataIndex: 'required', type: 'boolean'},
						{header: 'System', width: 40, dataIndex: 'systemDefined', type: 'boolean'},
						{header: 'Value Table', width: 100, dataIndex: 'valueTable.name', hidden: true, filter: {searchFieldName: 'valueTableId', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
						{header: 'Value List Url', width: 100, dataIndex: 'valueListUrl', hidden: true, filter: {searchFieldName: 'valueListUrl'}},
						{header: 'UI Config', width: 200, dataIndex: 'userInterfaceConfig', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						deleteURL: 'systemColumnDelete.json',
						getDefaultData: function(gridPanel) { // defaults group & table
							const t = gridPanel.getTopToolbar();
							const g = TCG.getChildByName(t, 'columnGroupId');
							if (TCG.isBlank(g.getValue())) {
								TCG.showError('Please first select a column group for the field you are creating.');
								return false;
							}
							const grp = TCG.data.getData('systemColumnGroup.json?id=' + g.getValue(), gridPanel, 'system.schema.group.' + g.getRawValue());
							return {
								columnGroup: grp,
								table: gridPanel.getWindow().getMainForm().formValues
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			},


			{
				title: 'Template Columns',
				items: [{
					name: 'systemColumnTemplateListFind',
					xtype: 'gridpanel',
					instructions: 'Template Columns are used to group multiple Custom Columns that represent the same concept.',
					wikiPage: 'IT/System+-+Custom+Columns',
					groupField: 'systemColumnGroup.name',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Group', width: 15, dataIndex: 'systemColumnGroup.name', hidden: true},
						{header: 'Column Name', width: 50, dataIndex: 'name'},
						{header: 'Label', width: 50, dataIndex: 'label'},
						{header: 'Description', width: 100, dataIndex: 'description'}
					],
					getLoadParams: function() {
						const groups = TCG.data.getData('systemColumnGroupListByTable.json?tableId=' + this.getWindow().params.id, this);
						const colIds = [];
						if (groups.length > 0) {
							for (let i = 0; i < groups.length; i++) {
								colIds[i] = groups[i].id;
							}
						}
						else {
							colIds[0] = 0;
						}

						return {
							'systemColumnGroupIds': colIds
						};
					},
					getTopToolbarFilters: function(toolbar) {
						const grid = this;
						const tableId = grid.getWindow().params.id;
						return [
							{
								fieldLabel: 'Column Group', name: 'columnGroupId', width: 180, xtype: 'combo', url: 'systemColumnGroupListByTable.json', root: 'data',
								beforequery: function(queryEvent) {
									queryEvent.combo.store.baseParams = {tableId: tableId};
								}
							}
						];
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.TemplateColumnWindow',
						deleteEnabled: false,
						getDefaultData: function(gridPanel) { // defaults group & table
							const t = gridPanel.getTopToolbar();
							const g = TCG.getChildByName(t, 'columnGroupId');
							if (TCG.isBlank(g.getValue())) {
								TCG.showError('Please first select a column group for the template you are creating.');
								return false;
							}
							const grp = TCG.data.getData('systemColumnGroup.json?id=' + g.getValue(), gridPanel, 'system.schema.group.' + g.getRawValue());
							return {
								systemColumnGroup: grp
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			},


			{
				title: 'Column Groups',
				items: [{
					name: 'systemColumnGroupListByTable',
					xtype: 'gridpanel',
					instructions: 'Related custom columns can be grouped together and security can be defined at group level.',
					wikiPage: 'IT/System+-+Custom+Columns',
					getLoadParams: function() {
						return {
							tableId: this.getWindow().params.id
						};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
						{header: 'Linked Bean Property', width: 70, dataIndex: 'linkedBeanProperty'},
						{header: 'Custom Column Type', width: 60, dataIndex: 'customColumnType', tooltip: 'Defines how and where the custom column values are stored.'},
						{header: 'JSON Value Column', width: 50, dataIndex: 'jsonValueColumn.name'},
						{
							header: 'Security Resource', width: 80, dataIndex: 'securityResourceLabel',
							renderer: function(v, metaData, r) {
								if (TCG.isTrue(r.json.securityOverridden)) {
									metaData.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Security Override', width: 40, dataIndex: 'securityOverridden', type: 'boolean', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnGroupWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Relationships',
				items: [{
					name: 'systemRelationshipListFind',
					xtype: 'gridpanel',
					instructions: 'This table has the following relationships. A "From Relationship" ' +
						'is a relationship from this table to another.  A "To Relationship" is a relationship from another table to this one.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'FK Name', width: 100, dataIndex: 'name', hidden: true},
						{header: 'Relationship', width: 100, dataIndex: 'label'},
						{header: 'Table', width: 75, dataIndex: 'column.table.label', hidden: true},
						{header: 'Column', width: 75, dataIndex: 'column.label'},
						{header: 'Referenced Table', width: 75, dataIndex: 'parentColumn.table.label', hidden: true},
						{
							header: 'Referenced Column', width: 100, dataIndex: 'parentColumn.label',
							renderer: function(v, c, r) {
								return '[' + r.data['parentColumn.table.label'] + '].[' + v + ']';
							}
						},
						{header: 'Exclude from Used By', width: 50, dataIndex: 'excludeFromUsedBy', type: 'boolean', qtip: 'Exclude relationship look ups for Used by tab on Detail Windows.'},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.system.schema.RelationshipWindow',
						drillDownOnly: true
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 150, minListWidth: 150, mode: 'local', value: 'FROM', displayField: 'name', valueField: 'value',
								store: new Ext.data.ArrayStore({
									fields: ['value', 'name'],
									data: [['FROM', 'From Relationships'], ['TO', 'To Relationships']]
								})
							}
						];
					},
					getLoadParams: function(firstLoad) {
						if (!firstLoad) {
							const displayType = TCG.getChildByName(this.getTopToolbar(), 'displayType').getValue();
							if (displayType === 'TO') {
								return {parentTableId: this.getWindow().params.id};
							}
						}
						return {tableId: this.getWindow().params.id};
					}
				}]
			},


			{
				title: 'Migration Columns',
				items: [{
					name: 'columnListFind',
					instructions: 'Migration Column information for this table as defined in Hibernate migration schemas files.',
					xtype: 'gridpanel',
					limitRequestedProperties: false,
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarInitialLoadParams: function() {
						return {
							tableName: TCG.getValue('name', this.getWindow().getMainForm().formValues)
						};
					},
					isPagingEnabled: function() {
						return false;
					},
					columns: [
						{header: 'Column Name', width: 150, dataIndex: 'name'},
						// Not Used???
						{header: 'Label', width: 100, dataIndex: 'label', hidden: true},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Bean Property', width: 150, dataIndex: 'beanPropertyName', hidden: true},
						{header: 'Data Type', width: 60, dataIndex: 'dataType'},
						{header: 'Required', width: 60, dataIndex: 'required', type: 'boolean'},
						{header: 'Required for Validation', width: 60, dataIndex: 'required', type: 'boolean', hidden: true},
						{header: 'Default Value', width: 100, dataIndex: 'defaultValue', hidden: true},
						{header: 'Sort Order', width: 65, dataIndex: 'sortOrder', type: 'int', useNull: true},
						{header: 'Sort Direction', width: 70, dataIndex: 'sortDirection'},
						{header: 'Upload Converter', width: 100, dataIndex: 'uploadConverterName', hidden: true},
						{header: 'Upload Lookup Service Bean', width: 100, dataIndex: 'uploadLookupServiceBeanName', hidden: true},
						{header: 'Upload Lookup Service Method', width: 100, dataIndex: 'uploadLookupServiceMethodName', hidden: true},
						{header: 'Parent', width: 60, dataIndex: 'parent', type: 'boolean', hidden: true},
						{header: 'Indexed', width: 60, dataIndex: 'indexed', type: 'boolean'},
						{header: 'Unique Index', width: 60, dataIndex: 'uniqueIndex', type: 'boolean'},
						{header: 'FK Table', width: 100, dataIndex: 'fkTable'},
						{header: 'FK Field', width: 100, dataIndex: 'fkField', hidden: true},
						{header: 'Lazy', width: 100, dataIndex: 'lazy', hidden: true},
						{header: 'Natural Key', width: 60, dataIndex: 'naturalKey', type: 'boolean'},
						{header: 'Ignore Upload', width: 60, dataIndex: 'ignoreUpload', type: 'boolean', hidden: true},
						{header: 'Delayed', width: 60, dataIndex: 'delayed', type: 'boolean', hidden: true},
						{header: 'Force Simple Property', width: 60, dataIndex: 'forceSimpleProperty', type: 'boolean', hidden: true},
						{header: 'Exclude From Inserts', width: 60, dataIndex: 'excludedFromInserts', type: 'boolean', hidden: true},
						{header: 'Exclude From Updates', width: 60, dataIndex: 'excludedFromUpdates', type: 'boolean', hidden: true},
						{header: 'Type Definition Name', width: 100, dataIndex: 'typeDefinitionName', hidden: true, renderer: v => TCG.renderPre(v)},
						{header: 'Type Definition Class Name', width: 100, dataIndex: 'typeDefinitionClassName', hidden: true, renderer: v => TCG.renderPre(v)},
						{header: 'Type Definition Converter Class', width: 100, dataIndex: 'typeDefinitionConverterClass', hidden: true, renderer: v => TCG.renderPre(v)},
						{header: 'Audit Type', width: 100, dataIndex: 'auditType', hidden: true},
						{header: 'Calculation', width: 100, dataIndex: 'calculation', hidden: true}
					]
				}]
			},


			{
				title: 'Table Size History',
				items: [{
					name: 'systemTableSizeHistoryListFind',
					instructions: 'Historical table size information for selected table.',
					xtype: 'gridpanel',
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true, tooltip: 'ID of Database Row'},
						{header: 'Data Source', width: 50, dataIndex: 'systemTable.dataSource.name', hidden: true},
						{header: 'Snapshot Date', width: 30, dataIndex: 'snapshotDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Number of Rows', width: 40, dataIndex: 'numberOfRows', type: 'int'},
						{header: 'Data Size (bytes)', width: 40, dataIndex: 'dataSizeInBytes', type: 'int'},
						{header: 'Index Size (bytes)', width: 40, dataIndex: 'indexSizeInBytes', type: 'int'},
						{header: 'Unused Size (bytes)', width: 40, dataIndex: 'unusedSizeInBytes', type: 'int'},
						{header: 'Total Size (bytes)', width: 30, dataIndex: 'totalSizeInBytes', type: 'int'}
					],
					getLoadParams: function() {
						return {
							'systemTableId': this.getWindow().params.id
						};
					}
				}]
			}
		]
	}]
});
