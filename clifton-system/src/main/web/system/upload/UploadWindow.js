Clifton.system.upload.UploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'systemUploadWindow',
	title: 'System Upload',
	iconCls: 'import',
	height: 600,
	width: 800,
	saveTimeout: 240,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File For Selected Table (Max 10 Rows of Data)',
		handler: function() {
			const f = this.ownerCt.ownerCt.items.get(0);
			f.getWindow().getMainFormPanel().downloadSampleFile(false);
		}
	}, '-', {
		text: 'Sample File (All Data)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File For Selected Table (Contains All (up to 500 rows) Existing Data)',
		handler: function() {
			const f = this.ownerCt.ownerCt.items.get(0);
			f.getWindow().getMainFormPanel().downloadSampleFile(true);
		}
	}, {
		xtype: 'tbfill'
	}, {
		text: 'Go To Simple Upload',
		iconCls: 'import',
		tooltip: 'Go to Simple Upload window for selected table. Simple uploads are specialized uploads to make importing specific data easier, or include additional validation/functionality.<br/><br/><b>Not Supported for all Tables</b>',
		handler: function() {
			let f = this.ownerCt.ownerCt.items.get(0);
			const w = f.getWindow();
			f = w.getMainForm();
			const tableName = f.findField('tableName').value;
			if (TCG.isBlank(tableName)) {
				TCG.showError('A table is required in order to lookup/go to the simple upload window for that table.');
				return;
			}
			let simpleWindowClass = undefined;
			if (Clifton.system.schema.SimpleUploadWindows) {
				simpleWindowClass = Clifton.system.schema.SimpleUploadWindows[tableName];
			}

			if (simpleWindowClass) {
				TCG.createComponent(simpleWindowClass);
			}
			else {
				TCG.showError('Table ' + tableName + ' does not support simple uploads.');
			}
		}
	}

	],

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'System Uploads allow you to upload data from files directly into the system.  Use the options below to customize how to upload your data.  To view a sample file, please first select a table from the list.  Columns in the sample file that are required are in red.',


		getDefaultData: function(win) {
			if (win.defaultData && win.defaultData.tableName) {
				this.loadNaturalKeys(win.defaultData.tableName);
			}
			return win.defaultData;
		},
		loadNaturalKeys: function(tableName) {
			let naturalKey = '';
			if (TCG.isNotBlank(tableName)) {
				naturalKey = TCG.getResponseText('systemUploadTableDefaultNaturalKey.json?tableName=' + tableName, this);
			}
			this.getForm().findField('systemUploadNaturalKey').setValue(naturalKey);
		},
		items: [
			{xtype: 'hidden', name: 'enableOpenSessionInView', value: 'true'},
			{
				fieldLabel: 'Table', name: 'tableName', xtype: 'combo', displayField: 'name', valueField: 'name', url: 'systemTableListFind.json?defaultDataSource=true&uploadAllowed=true', allowBlank: false,
				detailPageClass: 'Clifton.system.schema.TableWindow',
				disableAddNewItem: true,
				createDetailClass: function(newInstance) {
					const fp = TCG.getParentFormPanel(this);
					const w = fp.getWindow();
					const className = this.detailPageClass;
					const name = this.getValue();
					let id = undefined;
					if (TCG.isNotBlank(name)) {
						id = TCG.data.getData('systemTableByName.json?tableName=' + name, this, 'system.table.' + name).id;
					}
					const params = id ? {id: id} : undefined;
					const defaultData = this.getDefaultData ? this.getDefaultData(fp.getForm()) : {};
					const cmpId = TCG.getComponentId(className, id);
					TCG.createComponent(className, {
						modal: TCG.isNull(id),
						id: cmpId,
						defaultData: defaultData,
						params: params,
						openerCt: this, // for modal new item window will call reload() on close
						defaultIconCls: w.iconCls
					});
				},
				listeners: {
					'select': function(combo) {
						const fp = TCG.getParentFormPanel(combo);
						const tableName = combo.getValue();
						fp.loadNaturalKeys(tableName);
					}
				}

			},
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{
				boxLabel: 'Create entities referenced by uploaded rows if they don\'t already exist.', name: 'insertMissingFKBeans', xtype: 'checkbox', labelSeparator: '',
				qtip: 'Note: Sample files use this selection to generate appropriate file.  When checked, additional columns will be available to populate information on the missing fk beans.  Otherwise, only fields used to lookup an existing entity will be included.'
			},
			Clifton.system.upload.InvalidDataEncounteredItems,
			Clifton.system.upload.ExistingBeansItems,
			{
				xtype: 'fieldset', title: 'Natural Key Overrides', collapsible: true, collapsed: true,
				items: [

					{xtype: 'label', html: 'For the uploaded table, you can chose to override the natural key used to find unique rows. Note: This is not common and should be used rarely.'},
					{fieldLabel: 'Default Natural Key', name: 'systemUploadNaturalKey', submitValue: false, readOnly: true},
					{
						xtype: 'listfield',
						allowBlank: true,
						name: 'overrideTableNaturalKeyBeanProperties',
						requiredFields: ['tableName'],
						controlConfig: {
							fieldLabel: 'Column', name: 'naturalKeyColumn.label', hiddenName: 'naturalKeyColumn.beanPropertyName', displayField: 'label', valueField: 'beanPropertyName', xtype: 'combo',
							url: 'systemColumnStandardListFind.json?excludeRecordStampColumns=true',
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								if (TCG.isBlank(f.findField('tableName').getValue())) {
									return false;
								}
								combo.store.baseParams = {tableName: f.findField('tableName').getValue()};
							}
						}
					}
				]
			},
			{
				xtype: 'fieldset', title: 'Custom Column Values', collapsed: true,
				instructions: 'If custom columns values are to be inserted/updated, please select a group from the list below.  If a group is selected the values in the upload file will overwrite the existing custom values in the database.  Values will ONLY be overwritten in the database if there is at least one value in the file to set.  To further restrict which custom fields to update, you can select a linked value (determined by the group).  For example, Securities fields are linked to custom columns by their instrument\'s hierarchy.',
				items: [
					{
						fieldLabel: 'Group', name: 'columnGroupName', xtype: 'combo', loadAll: false, displayField: 'name', valueField: 'name', url: 'systemColumnGroupListByTableName.json', requiredFields: ['tableName'], root: 'data',
						emptyText: '',
						beforequery: function(queryEvent) {
							const tbl = queryEvent.combo.getParentForm().getForm().findField('tableName').value;
							if (TCG.isNotBlank(tbl)) {
								queryEvent.combo.store.baseParams = {
									tableName: tbl
								};
							}
						}
					},
					{fieldLabel: 'Linked Value', name: 'linkedValue', requiredFields: ['columnGroupName']},
					{boxLabel: 'Update custom column values, even if skipping updates of existing beans for the upload', name: 'updateCustomFieldsAlways', xtype: 'checkbox', checked: true}
				]
			},

			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
		],

		getSaveURL: function() {
			return 'systemUploadFileUpload.json';
		},

		downloadSampleFile: function(allRows) {
			const f = this.getForm();
			const tableName = f.findField('tableName').value;
			if (TCG.isBlank(tableName)) {
				TCG.showError('A table is required in order to generate a sample upload file.');
				return;
			}
			const params = this.getFormValuesFormatted(true);
			// Note: Value not used for sample downloads and returns component not value because its a radio group
			if (params.existingBeans) {
				params.existingBeans = '';
			}
			Clifton.system.downloadSampleUploadFile(tableName, allRows, this, params);
		}
	}]
});
