Clifton.system.hierarchy.HierarchyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Hierarchy',
	iconCls: 'hierarchy',
	height: 450,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Hierarchy',
				items: [{
					xtype: 'formpanel',
					instructions: 'A hierarchy defines a specific level or tag label within a hierarchy category.',
					url: 'systemHierarchy.json',
					items: [
						{fieldLabel: 'Hierarchy Category', name: 'category.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.hierarchy.CategoryWindow', detailIdField: 'category.id'},
						{fieldLabel: 'Expanded Name', name: 'nameExpanded', disabled: true},
						{
							fieldLabel: 'Parent Hierarchy', name: 'parent.name', hiddenName: 'parent.id', displayField: 'nameExpanded', xtype: 'combo', loadAll: false, url: 'systemHierarchyListFind.json',
							beforequery: function(queryEvent) {
								queryEvent.combo.store.baseParams = {
									categoryId: queryEvent.combo.getParentForm().getForm().findField('category.id').getValue(),
									ignoreNullParent: true
								};
							}
						},
						{fieldLabel: 'Hierarchy Name', name: 'name'},
						{fieldLabel: 'Hierarchy Value', name: 'value', qtip: 'Optional value field, not current used except to store additional data like Master Identifier of the source.'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Order', name: 'order', xtype: 'integerfield'},
						{fieldLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true, boxLabel: ' (only "Description" and "Order" fields can be updated)', qtip: 'System Defined hierarchies have limited editable properties. These hierarchies may provide critical functionality or be otherwise relied upon by the system.'}
					],

					listeners: {
						afterload: function(formPanel) {
							if (formPanel.getFormValue('systemDefined')) {
								formPanel.disableField('parent.name');
								formPanel.disableField('name');
								formPanel.disableField('value');
							}
							if (formPanel.getFormValue('category.maxDepth') === 1) { // tags have no parent
								formPanel.hideField('nameExpanded');
								formPanel.hideField('parent.name');
							}
						}
					},

					afterRenderPromise: function(fp) {
						// if category is not specified, try to get it from other fields
						const data = fp.getWindow().defaultData;
						if (Ext.isNumber(fp.getFormValue('category.id')) === false) {
							const parentId = fp.getForm().findField('parent.id').getValue();
							if (data && data.category && data.category.id) {
								// already have category specified as default data
							}
							else if (Ext.isNumber(parentId)) {
								// get parent's category: same as this
								fp.loadCategory('systemHierarchy.json', {id: parentId}, 'category');
							}
							else if (data && data.categoryName) {
								fp.loadCategory('systemHierarchyCategoryByName.json', {name: data.categoryName});
							}
							else if (data && data.tableName) {
								fp.loadCategory('systemHierarchyCategoryByTable.json', {tableName: data.tableName});
							}
							else if (this.getLoadURL()) {
								// Form will attempt to load category from given parameters (most likely an entity ID)
							}
							else {
								TCG.showError('Required Hierarchy Category is missing and cannot be derived from parent or table.');
							}
						}
					},
					loadCategory: function(uri, parameters, categoryPath) {
						const formPanel = this;
						const loader = new TCG.data.JsonLoader({
							waitTarget: formPanel,
							params: parameters,
							onLoad: function(record, conf) {
								const category = categoryPath ? TCG.getValue(categoryPath, record) : record;
								formPanel.setFormValue('category.id', category.id);
								formPanel.setFormValue('category.name', category.name);
							}
						});
						loader.load(uri);
					}
				}]
			},


			{
				title: 'Links',
				items: [{
					name: 'systemHierarchyLinkListFind',
					xtype: 'gridpanel',
					instructions: 'The following links are associated with this hierarchy category.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Table', width: 100, dataIndex: 'assignment.table.name', filter: {searchFieldName: 'tableName'}},
						{header: 'FK Field ID', width: 50, dataIndex: 'fkFieldId', type: 'int'},
						{
							header: 'Entity', width: 35, dataIndex: 'assignment.table.detailScreenClass', filter: false, sortable: false,
							renderer: function(clz, args, r) {
								if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.fkFieldId)) {
									return TCG.renderActionColumn('view', 'View', 'View Entity that is associated with this link');
								}
								return 'N/A';
							},
							eventListeners: {
								click: function(column, grid, rowIndex, event) {
									const row = grid.getStore().getAt(rowIndex);
									const gridPanel = grid.ownerGridPanel;
									gridPanel.editor.openDetailPage(row.json.assignment.table.detailScreenClass, gridPanel, row.json.fkFieldId, row);
								}
							}
						}
					],
					editor: {
						addEnabled: false
					},
					getLoadParams: function() {
						return {'hierarchyId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
