Clifton.system.hierarchy.HierarchySetupWindow = Ext.extend(TCG.app.Window, {
	id: 'systemHierarchySetupWindow',
	title: 'Hierarchy Setup',
	iconCls: 'hierarchy',
	width: 1200,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Categories',
				items: [{
					name: 'systemHierarchyCategoryListFind',
					xtype: 'gridpanel',
					instructions: 'A list of system hierarchy categories. Hierarchy Categories are used to group hierarchy structures, i.e. Geographical, Business Units, etc.  To use hierarchies as tags, the max depth value should be 1. System Defined Categories and Assignments tied to them cannot be edited.',
					wikiPage: 'IT/System+-+Hierarchy+-+Folder+Organization+and+Tagging',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
						{header: 'Modify Condition', width: 150, dataIndex: 'entityModifyCondition.name', filter: false},
						{header: 'Max Depth', width: 30, dataIndex: 'maxDepth', type: 'int', useNull: true},
						{header: 'System Defined', width: 40, dataIndex: 'systemDefined', type: 'boolean'},
						{header: 'Inactive', width: 30, dataIndex: 'inactive', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.system.hierarchy.CategoryWindow'
					}
				}]
			},


			{
				title: 'Assignments',
				items: [{
					name: 'systemHierarchyAssignmentList',
					xtype: 'gridpanel',
					instructions: 'A list of system hierarchy assignments. Hierarchy Assignments link hierarchy categories to tables.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Table', width: 100, dataIndex: 'table.name'},
						{
							header: 'Security Resource', width: 100, dataIndex: 'securityResourceLabel',
							renderer: function(v, metaData, r) {
								if (TCG.isTrue(r.json.securityOverridden)) {
									metaData.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Security Override', width: 40, dataIndex: 'securityOverridden', type: 'boolean', hidden: true},
						{header: 'Category', width: 100, dataIndex: 'category.name'},
						{header: 'Single Table Assignment', width: 60, dataIndex: 'singleTableAssignment', type: 'boolean'},
						{header: 'Multiple Values Allowed', width: 60, dataIndex: 'multipleValuesAllowed', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.system.hierarchy.AssignmentWindow'
					}
				}]
			}
		]
	}]
});
