Clifton.system.hierarchy.AssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Hierarchy Assignment',
	iconCls: 'hierarchy',
	width: 700,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Assignment',
				items: [{
					xtype: 'formpanel',
					instructions: 'A hierarchy assignment defines valid associations between tables and hierarchy categories.  If the associated category is system defined, the assignment cannot be changed.',
					url: 'systemHierarchyAssignment.json',
					labelFieldName: 'category.name',
					items: [
						{fieldLabel: 'Hierarchy Category', name: 'category.name', hiddenName: 'category.id', displayField: 'name', xtype: 'combo', url: 'systemHierarchyCategoryListFind.json', disableAddNewItem: true, detailPageClass: 'Clifton.system.hierarchy.CategoryWindow'},
						{fieldLabel: 'Table', name: 'table.name', hiddenName: 'table.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', disableAddNewItem: true, detailPageClass: 'Clifton.system.schema.TableWindow'},
						{fieldLabel: 'Security Resource', name: 'securityResource.labelExpanded', hiddenName: 'securityResource.id', displayField: 'labelExpanded', queryParam: 'labelExpanded', xtype: 'combo', url: 'securityResourceListFind.json', qtip: 'The resource that will be used to control access for adding/removing/moving hierarchy links for this assignment. If not set, Table\'s Security Resource will be used. Requires WRITE access to modify hierarchy links.'},
						{name: 'singleTableAssignment', xtype: 'checkbox', boxLabel: 'Allow only one hierarchy category for selected table'},
						{name: 'multipleValuesAllowed', xtype: 'checkbox', boxLabel: 'Allow the same entity to be assigned to more than one hierarchy within the same category'}
					]
				}]
			},


			{
				title: 'Links',
				items: [{
					name: 'systemHierarchyLinkListFind',
					xtype: 'gridpanel',
					instructions: 'The following links are associated with this hierarchy assignment.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Hierarchy', width: 200, dataIndex: 'hierarchy.nameExpanded', filter: {searchFieldName: 'hierarchyNameExpanded'}},
						{header: 'FK Field ID', width: 50, dataIndex: 'fkFieldId', type: 'int'},
						{
							header: 'Entity', width: 35, dataIndex: 'assignment.table.detailScreenClass', filter: false, sortable: false,
							renderer: function(clz, args, r) {
								if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.fkFieldId)) {
									return TCG.renderActionColumn('view', 'View', 'View Entity that is associated with this link');
								}
								return 'N/A';
							},
							eventListeners: {
								click: function(column, grid, rowIndex, event) {
									const row = grid.getStore().getAt(rowIndex);
									const gridPanel = grid.ownerGridPanel;
									gridPanel.editor.openDetailPage(row.json.assignment.table.detailScreenClass, gridPanel, row.json.fkFieldId, row);
								}
							}
						}
					],
					editor: {
						addEnabled: false
					},
					getLoadParams: function() {
						return {'assignmentId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
