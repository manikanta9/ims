Clifton.system.hierarchy.CategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Hierarchy Category',
	iconCls: 'hierarchy',
	width: 900,
	height: 500,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Category',
				items: [{
					xtype: 'formpanel',
					instructions: 'A category defines a hierarchy structure.  A category with a max depth of 1 is also known as a tag.',
					url: 'systemHierarchyCategory.json',
					items: [
						{fieldLabel: 'Category Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true},
						{fieldLabel: 'Max Depth', name: 'maxDepth', xtype: 'spinnerfield', maxValue: 10},
						{fieldLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true, boxLabel: ' (only "Description" and "Modify Condition" fields can be updated)'},
						{fieldLabel: 'Inactive', name: 'inactive', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Hierarchies',
				items: [{
					name: 'systemHierarchyListFind',
					xtype: 'treegrid',
					instructions: 'The following hierarchies are tied to this category.',
					appendStandardColumns: false, // to avoid UI glitch
					columns: [
						{header: 'Hierarchy Name', width: 300, dataIndex: 'name'},
						{header: 'Hierarchy Value', width: 120, dataIndex: 'value'}, // Put second to prevent treegrid from displaying incorrectly
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Order', width: 60, dataIndex: 'order', type: 'int', doNotFormat: true, useNull: true},
						{header: 'System', width: 60, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.system.hierarchy.HierarchyWindow',
						deleteURL: 'systemHierarchyDelete.json',
						getDefaultData: function(treePanel) { // defaults category for the detail page
							return {
								category: treePanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {'categoryId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Assignments',
				items: [{
					name: 'systemHierarchyAssignmentListByCategory',
					xtype: 'gridpanel',
					instructions: 'The following tables are assigned to this hierarchy category.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Table', width: 150, dataIndex: 'table.name'},
						{
							header: 'Security Resource', width: 120, dataIndex: 'securityResourceLabel',
							renderer: function(v, metaData, r) {
								if (TCG.isTrue(r.json.securityOverridden)) {
									metaData.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Security Override', width: 40, dataIndex: 'securityOverridden', type: 'boolean', hidden: true},
						{header: 'Single Table Assignment', width: 50, dataIndex: 'singleTableAssignment', type: 'boolean'},
						{header: 'Multiple Values Allowed', width: 50, dataIndex: 'multipleValuesAllowed', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.system.hierarchy.AssignmentWindow',
						deleteURL: 'systemHierarchyAssignmentDelete.json',
						getDefaultData: function(gridPanel) { // defaults category id for the detail page
							return {
								category: {
									id: gridPanel.getWindow().getMainFormId(),
									name: gridPanel.getWindow().getMainForm().findField('name').value
								}
							};
						}
					},
					getLoadParams: function() {
						return {'categoryId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Links',
				items: [{
					name: 'systemHierarchyLinkListFind',
					xtype: 'gridpanel',
					instructions: 'The following links are associated with this hierarchy category.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Table', width: 100, dataIndex: 'assignment.table.name', filter: {searchFieldName: 'tableName'}},
						{header: 'Hierarchy', width: 200, dataIndex: 'hierarchy.nameExpanded', filter: {searchFieldName: 'hierarchyNameExpanded'}},
						{header: 'FK Field ID', width: 50, dataIndex: 'fkFieldId', type: 'int'},
						{
							header: 'Entity', width: 35, dataIndex: 'assignment.table.detailScreenClass', filter: false, sortable: false,
							renderer: function(clz, args, r) {
								if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.fkFieldId)) {
									return TCG.renderActionColumn('view', 'View', 'View Entity that is associated with this link');
								}
								return 'N/A';
							},
							eventListeners: {
								click: function(column, grid, rowIndex, event) {
									const row = grid.getStore().getAt(rowIndex);
									const gridPanel = grid.ownerGridPanel;
									gridPanel.editor.openDetailPage(row.json.assignment.table.detailScreenClass, gridPanel, row.json.fkFieldId, row);
								}
							}
						}
					],
					editor: {
						addEnabled: false
					},
					getLoadParams: function() {
						return {'categoryId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'SystemHierarchyCategory'
				}]
			}
		]
	}]
});
