Clifton.system.hierarchy.HierarchyLinkWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'systemHierarchyLink.json',

	getClassName: function(config, entity) {
		return 'Clifton.system.hierarchy.HierarchyWindow';
	},

	prepareEntity: function(entity) {
		return entity ? entity.hierarchy : entity;
	}
});
