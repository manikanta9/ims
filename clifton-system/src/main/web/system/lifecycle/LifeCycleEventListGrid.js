Clifton.system.lifecycle.LifeCycleEventListGrid = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	name: 'systemLifeCycleEventList',
	instructions: 'System Life cycle includes various events that occured over the life of the entity.  Optionally include related entities.  This can include audit logs, workflow history, or other events.',
	wikiPage: 'IT/System+Life+Cycle',
	useBufferView: false,
	additionalPropertiesToRequest: 'sourceSystemTable.detailScreenClass|linkedSystemTable.detailScreenClass',
	viewNames: ['Normalized View', 'Expanded Action', 'Expanded Details'],
	defaultViewName: 'Expanded Details',
	columns: [
		{header: 'Date', dataIndex: 'eventDate', width: 14, allViews: true},
		{header: 'Source Table', dataIndex: 'sourceSystemTable.name', width: 50, hidden: true, filter: false, sortable: false},
		{header: 'Source FK Field ID', dataIndex: 'sourceFkFieldId', width: 35, type: 'int', hidden: true, filter: false, sortable: false, useNull: true},
		{header: 'Linked Table', dataIndex: 'linkedSystemTable.name', width: 50, hidden: true, filter: false, sortable: false},
		{header: 'Linked FK Field ID', dataIndex: 'linkedFkFieldId', width: 35, type: 'int', hidden: true, filter: false, sortable: false, useNull: true},
		{header: 'Source', dataIndex: 'eventSource', width: 45, allViews: true},
		{header: 'User', dataIndex: 'eventUser.displayName', width: 40, allViews: true},
		{
			header: 'Action', dataIndex: 'eventAction', width: 50, hidden: true, viewNames: ['Normalized View', 'Expanded Details'],
			renderer: function(v, metaData, r) {
				if (TCG.getValue('eventSource', r.json) === 'Workflow Transition') {
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(TCG.getValue('eventUser.displayName', r.json) + ' executed "' + v + '" workflow transition on "' + TCG.getValue('linkedSystemTable.label', r.json) + '" ' + (r.json.eventDetails || ''));
				}
				else if (v === 'Delete') {
					metaData.css = 'amountNegative';
				}
				return v;
			}
		},
		{
			header: 'Expanded Action', dataIndex: 'eventActionExpanded', width: 75, hidden: true, viewNames: ['Expanded Action'],
			renderer: function(v, metaData, r) {
				if (TCG.getValue('eventSource', r.json) === 'Workflow Transition') {
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(TCG.getValue('eventUser.displayName', r.json) + ' executed "' + TCG.getValue('eventAction', r.json) + '" workflow transition on "' + TCG.getValue('linkedSystemTable.label', r.json) + '" ' + (r.json.eventDetails || ''));
				}
				else if (TCG.getValue('eventAction', r.json) === 'Delete') {
					metaData.css = 'amountNegative';
				}
				return TCG.getValue('linkedSystemTable.label', r.json) + ': ' + r.json.eventAction;
			}
		},
		{header: 'Scope', dataIndex: 'linkedSystemTable.label', width: 50, hidden: true, filter: false, sortable: false, viewNames: ['Normalized View']},
		{header: 'Details', dataIndex: 'eventDetails', width: 140, hidden: true, viewNames: ['Normalized View', 'Expanded Action'], renderer: TCG.renderValueWithTooltip},
		{
			header: 'Expanded Details', dataIndex: 'eventDetailsExpanded', width: 160, hidden: true, viewNames: ['Expanded Details'],
			renderer: function(v, metaData, r) {
				const value = TCG.getValue('linkedSystemTable.label', r.json) + (r.json.eventDetails ? ': ' + r.json.eventDetails : '');
				return TCG.renderValueWithTooltip(value, metaData);
			}
		}
	],

	editor: {
		drillDownOnly: true,
		getDetailPageClass: function(grid, row) {
			return row.json.sourceFkFieldId ? row.json.sourceSystemTable.detailScreenClass : row.json.linkedSystemTable.detailScreenClass;
		},
		getDetailPageId: function(gridPanel, row) {
			return row.json.sourceFkFieldId || row.json.linkedFkFieldId;
		}
	},


	getLoadParams: function(firstLoad) {
		if (firstLoad && this.defaultViewName) {
			this.setDefaultView(this.defaultViewName);
		}
		// always filter by table and entity id
		const params = {tableName: this.getTableName()};
		params['entityId'] = this.getEntityId();
		return params;
	},
	getTableName: function() {
		return this.tableName;
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	}
});
Ext.reg('system-lifecycle-grid', Clifton.system.lifecycle.LifeCycleEventListGrid);
