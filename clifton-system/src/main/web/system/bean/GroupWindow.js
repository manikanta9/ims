Clifton.system.bean.GroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Bean Group',
	iconCls: 'objects',
	width: 900,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'A bean Group represents a system interface used for a particular purpose that can have multiple bean Type implementations. Group objects are system defined and only description and modify condition can be modified by a user.',
					url: 'systemBeanGroup.json',
					items: [
						{fieldLabel: 'Group Name', name: 'name', readOnly: true},
						{fieldLabel: 'Alias', name: 'alias'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Interface', name: 'interfaceClassName', readOnly: true},
						{xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true}
					]
				}]
			},


			{
				title: 'Bean Types',
				items: [{
					name: 'systemBeanTypeListFind',
					xtype: 'gridpanel',
					instructions: 'The following system bean types belong to this group.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Class', dataIndex: 'className', width: 150, hidden: true},
						{header: 'Modify Condition', width: 100, dataIndex: 'entityModifyCondition.name', filter: false, sortable: false},
						{header: 'Singleton', width: 30, dataIndex: 'singleton', type: 'boolean'},
						{header: 'Allow Dupes', width: 30, dataIndex: 'duplicateBeansAllowed', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function() {
						return {groupId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.bean.TypeWindow'
					}
				}]
			},


			{
				title: 'Beans',
				items: [{
					name: 'systemBeanListFind',
					xtype: 'gridpanel',
					instructions: 'The following system beans belong to this group.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Bean Name', width: 100, dataIndex: 'name'},
						{header: 'Bean Alias', width: 200, dataIndex: 'alias', hidden: true, tooltip: 'Alias is an optional short version of the bean name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Bean Type', width: 100, dataIndex: 'type.name', filter: {sortFieldName: 'typeName'}},
						{header: 'Modify Condition', width: 100, dataIndex: 'entityModifyCondition.name', filter: false, sortable: false}
					],
					getTopToolbarInitialLoadParams: function() {
						return {groupId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.bean.BeanWindow'
					}
				}]
			}
		]
	}]
});
