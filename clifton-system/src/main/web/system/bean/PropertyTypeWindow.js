Clifton.system.bean.PropertyTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Bean Property Definition',
	iconCls: 'objects',
	height: 500,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Bean property definition describes a single bean property for the specified system bean type.',
		url: 'systemBeanPropertyType.json',
		items: [
			{fieldLabel: 'Bean Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.bean.TypeWindow'},
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'System Name', name: 'systemPropertyName', readOnly: true},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
			{fieldLabel: 'Data Type', name: 'dataType.name', detailIdField: 'dataType.id', xtype: 'linkfield'},
			{fieldLabel: 'Value Bean Group', name: 'valueGroup.name', detailIdField: 'valueGroup.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.bean.GroupWindow', qtip: 'Optional: if specified, value for this property type must be a bean that belongs to this group.'},
			{fieldLabel: 'Value List Table', name: 'valueTable.name', hiddenName: 'valueTable.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded', detailPageClass: 'Clifton.system.schema.TableWindow', disableAddNewItem: true},
			{fieldLabel: 'Value List URL', name: 'valueListUrl'},
			{fieldLabel: 'UI Config', name: 'userInterfaceConfig', xtype: 'script-textarea', height: 35, grow: true},
			{fieldLabel: 'Order', name: 'order'},
			{boxLabel: 'Allow multiple values to be selected for this property', name: 'multipleValuesAllowed', xtype: 'checkbox', disabled: true},
			{fieldLabel: 'Required', name: 'required', xtype: 'checkbox', disabled: true}
		]
	}]
});

