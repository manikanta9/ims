Clifton.system.bean.BeanWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'System Bean',
	iconCls: 'objects',
	width: 800,
	height: 600,
	allowOpenFromModal: true,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formwithdynamicfields',
		instructions: 'A system bean represents a specific instance of corresponding system bean type.',
		url: 'systemBean.json',
		labelWidth: 170,
		childTables: 'SystemBeanProperty', // used by audit trail

		dynamicTriggerFieldName: 'type.label',
		dynamicTriggerValueFieldName: 'type.id',
		dynamicFieldTypePropertyName: 'type',
		dynamicFieldListPropertyName: 'propertyList',
		dynamicFieldsUrl: 'systemBeanPropertyTypeListByBean.json',
		dynamicFieldsUrlParameterName: 'type.id',

		dynamicFieldsUrlIdFieldName: 'id',
		dynamicFieldsUrlListParameterName: 'propertyList',

		items: [
			{fieldLabel: 'Bean Group (Interface)', name: 'type.group.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.bean.GroupWindow', detailIdField: 'type.group.id'},
			{
				fieldLabel: 'Bean Type (Class)', name: 'type.label', hiddenName: 'type.id', displayField: 'label', xtype: 'combo',
				url: 'systemBeanTypeListFind.json',
				detailPageClass: 'Clifton.system.bean.TypeWindow',
				disableAddNewItem: true,
				listeners: {
					// reset object bean property fields on changes to bean type (triggerField)
					select: function(combo, record, index) {
						combo.ownerCt.resetDynamicFields(combo, false);
					},
					beforequery: function(queryEvent) {
						const form = queryEvent.combo.getParentForm().getForm();
						const groupId = form.findField('type.group.id').getValue();
						const groupName = form.findField('type.group.name').getValue();
						const queryParams = {};
						if (groupName) {
							queryParams.groupName = groupName;
						}
						if (groupId) {
							queryParams.groupId = groupId;
						}
						queryEvent.combo.store.baseParams = queryParams;
					}
				}
			},
			{fieldLabel: 'Bean Name', name: 'name'},
			{fieldLabel: 'Bean Alias', name: 'alias', qtip: 'Alias is an optional short version of the bean name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{
				fieldLabel: 'Modify Condition', name: 'entityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'entityModifyCondition.id', submitDetailField: false,
				qtip: 'This Field represents either the overridden modify condition or the inherited modify condition. The overridden takes precedent. In other words: this is the applicable condition'
			},
			{fieldLabel: 'Override Modify Condition', name: 'overrideEntityModifyCondition.name', hiddenName: 'overrideEntityModifyCondition.id', xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true},
			{xtype: 'label', html: '<hr/>'}
		],

		customizeBeanUI: function(alias, addColons) {
			if (alias) {
				this.setFieldLabel('type.label', alias + ' Type' + (addColons ? ':' : ''));
				this.setFieldLabel('name', alias + (addColons ? ':' : ''));
				this.hideField('type.group.name');
				this.getWindow().titlePrefix = alias;
				this.updateTitle();
			}
		},
		prepareDefaultData: function(dd) {
			const alias = TCG.getValue('type.group.alias', dd);
			this.customizeBeanUI(alias);
			return dd;
		},
		listeners: {
			afterload: function(form, isClosing) {
				// for created objects replace combo with linkfield: can't edit
				const triggerField = this.getForm().findField('type.label');
				if (this.getIdFieldValue() > 0 && triggerField.xtype !== 'linkfield') {
					this.remove(triggerField, true);

					const formValues = this.getForm().formValues;
					this.add({name: 'type.id', xtype: 'hidden', value: TCG.getValue('type.id', formValues)});
					this.insert(3, {
						fieldLabel: triggerField.fieldLabel,
						name: 'type.label',
						submitValue: false,
						xtype: 'linkfield',
						detailPageClass: 'Clifton.system.bean.TypeWindow',
						detailIdField: 'type.id',
						value: TCG.getValue('type.label', formValues)
					});
					try {
						this.doLayout();
					}
					catch (e) {
						// strange error due to removal of elements with allowBlank = false
					}
				}
				// customize if alias is set
				const alias = this.getFormValue('type.group.alias');
				this.customizeBeanUI(alias, true);
			}
		}
	}]
});
