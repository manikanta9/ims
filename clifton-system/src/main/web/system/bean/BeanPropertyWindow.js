Clifton.system.bean.BeanPropertyWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'systemBeanProperty.json',

	getClassName: function(config, entity) {
		return 'Clifton.system.bean.BeanWindow';
	},

	prepareEntity: function(entity) {
		if (entity && entity.bean && entity.bean.id) {
			// need to get the bean with corresponding properties populated
			return TCG.data.getDataPromise('systemBean.json', this, {params: {id: entity.bean.id}});
		}
		return entity;
	}
});
