Clifton.system.bean.BeanSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'systemBeanSetupWindow',
	title: 'System Beans',
	iconCls: 'objects',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Beans',
				items: [{
					xtype: 'gridpanel',
					name: 'systemBeanListFind',
					instructions: 'A bean is an instance of corresponding bean Type populated with property values specific to this instance. System beans implement specific business functionality and can be used by various parts of the system. Using Object Oriented terminology: Bean == Object, Type == Class, Group == Interface.',
					wikiPage: 'IT/System+Beans',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Bean Group', xtype: 'combo', name: 'groupId', width: 250, listWidth: 400, url: 'systemBeanGroupListFind.json', linkedFilter: 'type.group.name'},
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Bean Name', width: 200, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}, defaultSortColumn: true},
						{header: 'Bean Alias', width: 200, dataIndex: 'alias', hidden: true, tooltip: 'Alias is an optional short version of the bean name'},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'Bean Type', width: 200, dataIndex: 'type.name', filter: {searchFieldName: 'typeId', type: 'combo', url: 'systemBeanTypeListFind.json'}},
						{header: 'Bean Group', width: 150, dataIndex: 'type.group.name', filter: {searchFieldName: 'groupId', type: 'combo', url: 'systemBeanGroupListFind.json'}},
						{header: 'Modify Condition', width: 200, dataIndex: 'entityModifyCondition.name', filter: false, sortable: false}
					],
					editor: {
						detailPageClass: 'Clifton.system.bean.BeanWindow',
						copyURL: 'systemBeanCopy.json'
					}
				}]
			},


			{
				title: 'Bean Types',
				items: [{
					xtype: 'gridpanel',
					name: 'systemBeanTypeListFind',
					instructions: 'A bean type represents a specific class that implements corresponding bean Group. Type objects are system defined and cannot be modified by a user. Using Object Oriented terminology: Bean == Object, Type == Class, Group == Interface.',
					groupField: 'group.name',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Bean Group', xtype: 'combo', name: 'groupId', width: 250, listWidth: 400, url: 'systemBeanGroupListFind.json', linkedFilter: 'group.name'},
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Bean Group', dataIndex: 'group.name', width: 100, filter: {searchFieldName: 'groupId', type: 'combo', url: 'systemBeanGroupListFind.json'}, hidden: true},
						{header: 'Type Name', dataIndex: 'name', width: 120},
						{header: 'Description', dataIndex: 'description', hidden: true},
						{header: 'Class', dataIndex: 'className', width: 150},
						{header: 'Modify Condition', width: 100, dataIndex: 'entityModifyCondition.name', filter: false, sortable: false},
						{header: 'Singleton', width: 30, dataIndex: 'singleton', type: 'boolean'},
						{header: 'Allow Dupes', width: 30, dataIndex: 'duplicateBeansAllowed', type: 'boolean'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.bean.TypeWindow'
					}
				}]
			},


			{
				title: 'Bean Groups',
				items: [{
					xtype: 'gridpanel',
					name: 'systemBeanGroupListFind',
					instructions: 'A bean Group represents a system interface used for a particular purpose that can have multiple bean Type implementations. Group objects are system defined and cannot be modified by a user. Using Object Oriented terminology: Bean == Object, Type == Class, Group == Interface.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Alias', width: 50, dataIndex: 'alias'},
						{header: 'Interface', dataIndex: 'interfaceClassName'},
						{header: 'Modify Condition', width: 100, dataIndex: 'entityModifyCondition.name', filter: false, sortable: false},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.bean.GroupWindow'
					}
				}]
			}
		]
	}]
});
