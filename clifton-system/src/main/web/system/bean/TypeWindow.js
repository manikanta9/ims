Clifton.system.bean.TypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Bean Type',
	iconCls: 'objects',
	width: 900,
	height: 500,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'A bean Type represents a specific class that implements corresponding bean group. Type objects are system defined and cannot be modified by a user.',
					url: 'systemBeanType.json',
					childTables: 'SystemBeanPropertyType', // used by audit trail
					labelWidth: 150,
					items: [
						{fieldLabel: 'Group (Interface)', name: 'group.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.bean.GroupWindow', detailIdField: 'group.id'},
						{fieldLabel: 'Class', name: 'className', readOnly: true},
						{fieldLabel: 'Type Name', name: 'name', readOnly: true},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Modify Condition', name: 'entityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'entityModifyCondition.id', submitDetailField: false,
							qtip: 'This Field represents either the overridden modify condition or the inherited modify condition. The overridden takes precedent. In other words: this is the applicable condition.'
						},
						{fieldLabel: 'Override Modify Condition', name: 'overrideEntityModifyCondition.name', hiddenName: 'overrideEntityModifyCondition.id', xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true},
						{fieldLabel: 'Singleton', name: 'singleton', xtype: 'checkbox', disabled: true},
						{fieldLabel: 'Allow Duplicate Beans', name: 'duplicateBeansAllowed', xtype: 'checkbox', disabled: true}
					]
				}]
			},


			{
				title: 'Property Definitions',
				items: [{
					name: 'systemBeanPropertyTypeListFind',
					xtype: 'gridpanel',
					instructions: 'The following properties are available and can be used to customize this bean type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Property Name', width: 100, dataIndex: 'name'},
						{header: 'System Name', width: 80, dataIndex: 'systemPropertyName'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 45, dataIndex: 'dataType.name'},
						{header: 'Value List URL', width: 80, dataIndex: 'valueListUrl', hidden: true},
						{header: 'UI Config', width: 80, dataIndex: 'userInterfaceConfig', hidden: true},
						{header: 'Order', width: 30, dataIndex: 'order', type: 'int', useNull: true},
						{header: 'Allow Multiple', width: 35, dataIndex: 'multipleValuesAllowed', type: 'boolean'},
						{header: 'Required', width: 30, dataIndex: 'required', type: 'boolean'}
					],
					getLoadParams: function() {
						return {typeId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.system.bean.PropertyTypeWindow'
					}
				}]
			},


			{
				title: 'Beans',
				items: [{
					name: 'systemBeanListFind',
					xtype: 'gridpanel',
					instructions: 'The following system beans are of this type.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Bean Name', width: 100, dataIndex: 'name'},
						{header: 'Bean Alias', width: 200, dataIndex: 'alias', hidden: true, tooltip: 'Alias is an optional short version of the bean name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Modify Condition', width: 100, dataIndex: 'entityModifyCondition.name', filter: false, sortable: false}
					],
					getTopToolbarInitialLoadParams: function() {
						return {typeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.system.bean.BeanWindow',
						copyURL: 'systemBeanCopy.json',
						getDefaultData: function(grid) {
							return {
								type: grid.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			}
		]
	}]
});
