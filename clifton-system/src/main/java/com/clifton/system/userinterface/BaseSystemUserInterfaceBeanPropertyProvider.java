package com.clifton.system.userinterface;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.json.JsonStringToMapConverter;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseSystemUserInterfaceBeanPropertyProvider</code> provides a base implementation of SystemUserInterfacePropertyProvider
 * used to add a extra parameters to a parameter list and store the parameter values in a target property on the database.
 * <p>
 * NOTE:  Currently this is only used by the SystemBeanService.
 *
 * @author mwacker
 */
public abstract class BaseSystemUserInterfaceBeanPropertyProvider<T extends SystemUserInterfaceAwareVirtual, V extends SystemUserInterfaceValueAware<T>> implements
		SystemUserInterfacePropertyProvider<T, V> {

	/**
	 * The system bean property where the json representation of the sub properties provided by this class
	 * will be stored.
	 */
	private String jsonParameterValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract List<T> doGetPropertyTypeList();


	/**
	 * Provides the class that store the property values.
	 */
	public abstract Class<V> getValueClass();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<V> getPropertyList(SystemUserInterfaceAware targetProperty) {
		List<V> parameterValueList = null;
		Map<String, Object> parameterValueMap = getParameterValueMap();
		if (parameterValueMap != null) {
			Map<Integer, T> parameterMap = BeanUtils.getBeanMap(getPropertyTypeList(targetProperty), bean -> (Integer) bean.getIdentity());
			parameterValueList = new ArrayList<>();
			for (Map.Entry<Integer, T> integerTEntry : parameterMap.entrySet()) {
				if (parameterValueMap.containsKey((integerTEntry.getKey()).toString())) {
					V parameterValue = BeanUtils.newInstance(getValueClass());
					BeanUtils.setPropertyValue(parameterValue, "parameter", integerTEntry.getValue(), true);
					Map<String, Object> valueMap = (new JsonStringToMapConverter()).convert(parameterValueMap.get((integerTEntry.getKey()).toString()).toString());
					// If the actual property value had characters (like double quotes) that were escaped, we need to remove those escapes
					// Example: Compliance: Positions vs. Exchange Limits Query Export, Restriction List Value is a Json String: [{"comparison":"GREATER_THAN","value":80,"field":"percentOfLimit"}]
					parameterValue.setValue(StringUtils.removeAll(valueMap.get("value").toString(), "\\\\"));
					parameterValue.setText(StringUtils.removeAll(valueMap.get("text").toString(), "\\\\"));
					parameterValueList.add(parameterValue);
				}
			}
		}
		return parameterValueList;
	}


	@Override
	public List<T> getPropertyTypeList(SystemUserInterfaceAware targetProperty) {
		List<T> parameterList = doGetPropertyTypeList();
		if (!CollectionUtils.isEmpty(parameterList)) {
			for (T parameter : CollectionUtils.getIterable(parameterList)) {
				configureDateUI(parameter);
				BeanUtils.setPropertyValue(parameter, "targetSystemUserInterfaceAware", targetProperty, true);
			}
			return parameterList;
		}
		return null;
	}


	public Map<String, Object> getParameterValueMap() {
		return (new JsonStringToMapConverter()).convert(getJsonParameterValue());
	}


	public void configureDateUI(SystemUserInterfaceAware parameter) {
		if (DataTypeNames.DATE == DataTypeNames.valueOf(parameter.getDataType().getName()) && !parameter.isNotSystemModifiedUserInterfaceConfig()) {
			String userInterfaceConfig;
			if (StringUtils.isEmpty(parameter.getUserInterfaceConfig())) {
				userInterfaceConfig = "{displayField: 'name',valueField: 'value', xtype:'combo', mode:'local', store: {xtype: 'arraystore',fields:['name', 'value', 'description'],data: Clifton.calendar.date.COMMON_DATE_GENERATION_OPTIONS}}";
			}
			else {
				// Remove the last character } append ,  and the date options and then re-end it with }
				userInterfaceConfig = parameter.getUserInterfaceConfig().trim();
				userInterfaceConfig = userInterfaceConfig.substring(0, userInterfaceConfig.length() - 1) + ", displayField: 'name',valueField: 'value', xtype:'combo', mode:'local', store: {xtype: 'arraystore',fields:['name', 'value', 'description'],data: Clifton.calendar.date.COMMON_DATE_GENERATION_OPTIONS}" + "}";
			}
			BeanUtils.setPropertyValue(parameter, "userInterfaceConfig", userInterfaceConfig, true);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJsonParameterValue() {
		return this.jsonParameterValue;
	}


	public void setJsonParameterValue(String jsonParameterValue) {
		this.jsonParameterValue = jsonParameterValue;
	}
}
