package com.clifton.system.userinterface;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemTable;

import java.io.Serializable;


/**
 * The <code>BasedNamedSystemUserInterfaceAwareEntity</code> ...
 *
 * @author manderson
 */
public class BaseNamedSystemUserInterfaceAwareEntity<T extends Serializable> extends NamedEntity<T> implements SystemUserInterfaceAware {

	private Integer order;
	private boolean required;

	private SystemDataType dataType;

	private SystemTable valueTable;
	private String valueListUrl;

	private String userInterfaceConfig;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemTable getValueTable() {
		return this.valueTable;
	}


	public void setValueTable(SystemTable valueTable) {
		this.valueTable = valueTable;
	}


	@Override
	public String getValueListUrl() {
		return this.valueListUrl;
	}


	public void setValueListUrl(String valueListUrl) {
		this.valueListUrl = valueListUrl;
	}


	@Override
	public String getUserInterfaceConfig() {
		return this.userInterfaceConfig;
	}


	public void setUserInterfaceConfig(String userInterfaceConfig) {
		this.userInterfaceConfig = userInterfaceConfig;
	}


	@Override
	public boolean isRequired() {
		return this.required;
	}


	public void setRequired(boolean required) {
		this.required = required;
	}


	@Override
	public SystemDataType getDataType() {
		return this.dataType;
	}


	public void setDataType(SystemDataType dataType) {
		this.dataType = dataType;
	}


	@Override
	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}
}
