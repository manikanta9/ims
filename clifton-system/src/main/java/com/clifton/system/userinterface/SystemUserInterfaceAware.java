package com.clifton.system.userinterface;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemUserInterfaceAware</code> should be implemented by DTO objects that
 * have custom UI representation (i.e. Custom Fields, System Bean Property Types, etc);
 *
 * @author manderson
 */
public interface SystemUserInterfaceAware extends IdentityObject, LabeledObject {

	/**
	 * Name of the "field"
	 */
	public String getName();


	/**
	 * Optional description - can be used to populate qtip
	 */
	public String getDescription();


	/**
	 * Order of the "fields"
	 */
	public Integer getOrder();


	/**
	 * Is the field required
	 */
	public boolean isRequired();


	/**
	 * DataType for the values
	 */
	public SystemDataType getDataType();


	/**
	 * Fake FK relationships: optional table that field values are pulled from
	 */
	public SystemTable getValueTable();


	/**
	 * optional JSON URL that returns value list available options
	 */
	public String getValueListUrl();


	/**
	 * Optional user interface element configuration (for the element that corresponds to the data type.
	 * Example: when valueListUrl = "systemListItemListFind.json?listName=ANY LIST", then userInterfaceConfig = "{displayField: 'text', valueField: 'value', tooltipField: 'tooltip'}"
	 */
	public String getUserInterfaceConfig();


	/**
	 * Denotes whether the user interface configuration should NOT be modified by the system
	 */
	default boolean isNotSystemModifiedUserInterfaceConfig() {
		return false;
	}
}
