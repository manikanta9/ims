package com.clifton.system.userinterface;


import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.Serializable;


/**
 * The <code>SystemUserInterfaceValueConfig</code> defines the meta data to support custom used by look ups
 * for the ui aware bean and it's saved values.
 * <p>
 * NOTE: Although they could, Custom Columns are not currently using this feature as they have special
 * cases with dynamic owners (i.e. fkFieldId) so they are really definitions of two fk relationships so
 * they have their own used by implementation
 *
 * @author manderson
 */
public class SystemUserInterfaceValueUsedByConfig implements Serializable {

	/**
	 * Table Name the Values are stored in
	 * SystemBeanPropertyType stores values in SystemBeanProperty
	 */
	private final String valueTableName;

	/**
	 * This is the field where the value is stored in the valueTableName
	 * usually this is just "value"
	 */
	private final String valueBeanPropertyName;

	/**
	 * From the value table, this is the bean property where the reference to the {@link SystemUserInterfaceAware} object is.
	 * For SystemBeanProperty, is is the type field
	 */
	private final String valueUIAwareBeanPropertyName;

	/**
	 * For cases where we have an actual object for the owner, i.e.
	 * SystemBeanProperty has SystemBean, which is the "owner" as a property
	 */
	private final String valueOwnerBeanPropertyName;

	/**
	 * For drill down support
	 */
	private final String valueOwnerTableName;

	/**
	 * Go with valueOwnerTableName, this is the property on the value table that links it to it's owner "used by"
	 * For SystemColumnValue, this is the fkFieldId
	 */
	private final String valueOwnerFkFieldIdPropertyName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemUserInterfaceValueUsedByConfig(String valueTableName, String valueBeanPropertyName, String valueUIAwareBeanPropertyName, String valueOwnerBeanPropertyName,
	                                            String valueOwnerTableName, String valueOwnerFkFieldIdPropertyName) {
		ValidationUtils.assertNotNull(valueTableName, "Value Table Name is Required");
		ValidationUtils.assertNotNull(valueBeanPropertyName, "Value Bean Property Name is Required");
		ValidationUtils.assertNotNull(valueUIAwareBeanPropertyName, "Value UI Aware Bean Property Name is Required");
		ValidationUtils.assertNotNull(valueOwnerTableName, "Value Owner Table Name is Required");
		ValidationUtils.assertMutuallyExclusive("Value Owner Bean Property Name or Value Owner Fk Field ID Property Name is Required, but not both.", valueOwnerBeanPropertyName,
				valueOwnerFkFieldIdPropertyName);

		this.valueTableName = valueTableName;
		this.valueBeanPropertyName = valueBeanPropertyName;
		this.valueUIAwareBeanPropertyName = valueUIAwareBeanPropertyName;
		this.valueOwnerBeanPropertyName = valueOwnerBeanPropertyName;
		this.valueOwnerTableName = valueOwnerTableName;
		this.valueOwnerFkFieldIdPropertyName = valueOwnerFkFieldIdPropertyName;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || !o.getClass().equals(getClass())) {
			return false;
		}
		return CoreCompareUtils.getNoEqualProperties(this, o, true).isEmpty();
	}


	@Override
	public int hashCode() {
		return new StringBuilder(this.valueTableName).append(this.valueBeanPropertyName)
				.append(this.valueUIAwareBeanPropertyName)
				.append(this.valueOwnerBeanPropertyName)
				.append(this.valueOwnerTableName)
				.append(this.valueOwnerFkFieldIdPropertyName)
				.toString()
				.hashCode();
	}


	public String getValueTableName() {
		return this.valueTableName;
	}


	public String getValueBeanPropertyName() {
		return this.valueBeanPropertyName;
	}


	public String getValueUIAwareBeanPropertyName() {
		return this.valueUIAwareBeanPropertyName;
	}


	public String getValueOwnerBeanPropertyName() {
		return this.valueOwnerBeanPropertyName;
	}


	public String getValueOwnerTableName() {
		return this.valueOwnerTableName;
	}


	public String getValueOwnerFkFieldIdPropertyName() {
		return this.valueOwnerFkFieldIdPropertyName;
	}
}
