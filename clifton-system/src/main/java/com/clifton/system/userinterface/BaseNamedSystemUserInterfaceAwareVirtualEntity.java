package com.clifton.system.userinterface;


/**
 * The <code>BaseNamedSystemUserInterfaceAwareVirtualEntity</code> type defines a generic instance of {@link SystemUserInterfaceAwareVirtual} where the
 * implementor can define a specific type for the targetSystemUserInterfaceAware property.  For example, this is used by <code>SystemBeanPropertyType</code> to
 * implement targetSystemUserInterfaceAware as SystemBeanPropertyType.
 * <p>
 * NOTE:  When implementing, T should be SystemUserInterfaceAwareVirtual unless you will be binding to this object from the UI.
 *
 * @param <T>
 * @author mwacker
 */
public class BaseNamedSystemUserInterfaceAwareVirtualEntity<T extends SystemUserInterfaceAwareVirtual> extends BaseNamedSystemUserInterfaceAwareEntity<Integer> implements SystemUserInterfaceAwareVirtual {

	private boolean resetDynamicFieldsOnChange;
	private Integer virtualTypeId;
	private T targetSystemUserInterfaceAware;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isVirtualProperty() {
		return getTargetSystemUserInterfaceAware() != null;
	}


	@Override
	public boolean isResetDynamicFieldsOnChange() {
		return this.resetDynamicFieldsOnChange;
	}


	public void setResetDynamicFieldsOnChange(boolean resetDynamicFieldsOnChange) {
		this.resetDynamicFieldsOnChange = resetDynamicFieldsOnChange;
	}


	@Override
	public Integer getVirtualTypeId() {
		return this.virtualTypeId;
	}


	public void setVirtualTypeId(Integer virtualTypeId) {
		this.virtualTypeId = virtualTypeId;
	}


	@Override
	public T getTargetSystemUserInterfaceAware() {
		return this.targetSystemUserInterfaceAware;
	}


	public void setTargetSystemUserInterfaceAware(T targetSystemUserInterfaceAware) {
		this.targetSystemUserInterfaceAware = targetSystemUserInterfaceAware;
	}
}
