package com.clifton.system.userinterface;


import java.util.List;


/**
 * The <code>SystemBeanPropertyTypeProvider</code> defines methods that will extra ui properties for objects that implement it.
 * <p>
 * NOTE:  This is currently used by SystemBean's to provide extra properties based on the bean properties.
 *
 * @author mwacker
 */
public interface SystemUserInterfacePropertyProvider<T extends SystemUserInterfaceAwareVirtual, V extends SystemUserInterfaceValueAware<T>> {

	/**
	 * Provides a list of property values.
	 */
	public List<V> getPropertyList(SystemUserInterfaceAware targetProperty);


	/**
	 * Will return a list of addition SystemUserInterfaceAwareVirtual objects that will be sent to the UI.
	 *
	 * @param targetProperty -- The property that will store the data for extra properties returned be this method
	 */
	public List<T> getPropertyTypeList(SystemUserInterfaceAware targetProperty);


	/**
	 * The getter for the bean property where the json representation of the extra parameters is stored.
	 */
	public String getJsonParameterValue();
}
