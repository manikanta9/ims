package com.clifton.system.userinterface;


/**
 * The <code>SystemUserInterfaceValueAware</code> should be implemented by DTO objects that
 * have values for a custom UI representation (i.e. Custom Fields, System Bean Property Types, etc);
 *
 * @author mwacker
 */
public interface SystemUserInterfaceValueAware<T extends SystemUserInterfaceAware> {

	public String getValue();


	public void setValue(String value);


	public String getText();


	public void setText(String text);


	/**
	 * The parameter (or SystemUserInterfaceAware) that this value is for.
	 */
	public T getParameter();
}
