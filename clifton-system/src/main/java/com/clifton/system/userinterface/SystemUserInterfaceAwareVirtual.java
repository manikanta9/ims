package com.clifton.system.userinterface;


/**
 * The <code>SystemUserInterfaceAwareVirtual</code> should be implemented by DTO objects that
 * have custom UI representation and can support dynamic fields (i.e. Custom Fields, System Bean Property Types, etc);
 *
 * @author mwacker
 */
public interface SystemUserInterfaceAwareVirtual extends SystemUserInterfaceAware {

	/**
	 * Does the field cause the dynamic fields to reload when changed
	 */
	public boolean isResetDynamicFieldsOnChange();


	/**
	 * The target parameter where the data for this parameter will be stored.  All properties with the same targetSystemUserInterfaceAware should be serialized to json and stored
	 * on that property.
	 * <p>
	 * NOTE: Serializing the values to the target property is the responsibility of the service, see SystemBeanService.saveSystemBean where it calls buildSystemBeanPropertyList.
	 * <p>
	 * TODO: add example.
	 */
	public SystemUserInterfaceAwareVirtual getTargetSystemUserInterfaceAware();


	/**
	 * Indicates that this property is auto-generated/non-persisted field and should not be saved.
	 */
	public boolean isVirtualProperty();


	/**
	 * The id of the that created type.
	 */
	public Integer getVirtualTypeId();
}
