package com.clifton.system.userinterface;


/**
 * The <code>SystemUserInterfaceValueUsedByAware</code> defines methods needed for
 * custom Used By support for tables that contain definitions/for fake fk relationships
 * <p>
 * If this interface is implemented, then used by will automatically pick it up and populate
 * used by based on the {@link SystemUserInterfaceValueUsedByConfig}
 *
 * @author manderson
 */
public interface SystemUserInterfaceUsedByAware extends SystemUserInterfaceAware {

	/**
	 * This stores the information in order to lookup values and essentially
	 * determine relationships for "used by".
	 */
	public SystemUserInterfaceValueUsedByConfig getSystemUserInterfaceValueConfig();


	/**
	 * This stores the used by label for the ui field that is used to group related used by
	 * for example SystemBeanPropertyType returns the BeanType name + . + Property Name
	 */
	public String getUsedByLabel();
}
