package com.clifton.system.userinterface;


import com.clifton.core.util.StringUtils;
import com.clifton.system.list.SystemListItem;


/**
 * The <code>SystemUserInterfaceAwareUtils</code> contains utility methods for working with
 * objects that implement {@link SystemUserInterfaceAware}
 * <p>
 * These methods are currently used for SystemUploads that use Custom Columns to enhance support for looking up values in the system and not
 * requiring users to defined value and text for custom values.
 *
 * @author manderson
 */
public class SystemUserInterfaceAwareUtils {

	public static final String XTYPE_PROPERTY = "xtype";

	public static final String XTYPE_COMBO = "combo";
	public static final String XTYPE_LINK_FIELD = "linkfield";
	public static final String XTYPE_DISPLAY_FIELD = "displayfield";
	public static final String XTYPE_LABEL_FIELD = "label";

	public static final String COMBO_VALUE_FIELD_PROPERTY = "valueField";
	public static final String COMBO_VALUE_FIELD_PROPERTY_DEFAULT = "id";
	public static final String COMBO_DISPLAYFIELD_PROPERTY = "displayField";
	public static final String COMBO_DISPLAYFIELD_PROPERTY_DEFAULT = "name";

	public static final String COMBO_SEARCH_FIELD_PROPERTY = "queryParam";
	public static final String COMBO_SEARCH_FIELD_PROPERTY_DEFAULT = "searchPattern";

	public static final String SYSTEM_LIST_ITEM_TABLE_NAME = "SystemListItem";
	public static final String SYSTEM_LIST_COMBO_XTYPE = "system-list-combo";
	public static final String SYSTEM_LIST_COMBO_LIST_NAME_PROPERTY = "listName";
	public static final String SYSTEM_LIST_COMBO_VALUE_FIELD_PROPERTY_DEFAULT = "value";
	public static final String SYSTEM_LIST_COMBO_DISPLAYFIELD_PROPERTY_DEFAULT = "text";

	private static final String SYSTEM_LIST_LOOKUP_URL = "systemListItemListFind.json?listName=%s";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the UI aware bean results in a field that is not just there for display
	 * Currently these are: label, displayfield, and linkfield
	 */
	public static boolean isInputField(SystemUserInterfaceAware bean) {
		String xtype = getUserInterfaceConfigPropertyValue(bean, XTYPE_PROPERTY);
		if (XTYPE_LINK_FIELD.equals(xtype)) {
			return false;
		}
		if (XTYPE_LABEL_FIELD.equals(xtype)) {
			return false;
		}
		if (XTYPE_DISPLAY_FIELD.equals(xtype)) {
			return false;
		}
		return true;
	}


	/**
	 * Returns true if the UI aware bean results in a combo dropdown. This is determined via a valueListUrl being defined
	 * or specific properties found in the UI config
	 */
	public static boolean isComboLookup(SystemUserInterfaceAware bean) {
		if (bean != null) {
			if (!StringUtils.isEmpty(bean.getValueListUrl())) {
				return true;
			}
			if (isSystemListItemLookup(bean)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns the value list URL on the bean. If that is not set and the combo lookup uses a system list item lookup, then
	 * the system list item lookup URL is returned instead. Otherwise, returns null.
	 */
	public static String getComboLookupURL(SystemUserInterfaceAware bean) {
		String url = bean.getValueListUrl();
		if (url == null && isSystemListItemLookup(bean)) {
			String systemListName = getSystemListName(bean);
			if (systemListName != null) {
				url = String.format(SYSTEM_LIST_LOOKUP_URL, systemListName);
			}
		}

		return url;
	}


	/**
	 * Returns true for combo look ups that use the {@link SystemListItem} table for values
	 * These often need special handling for mimicking the lookup in code rather than using the URL
	 * or Value Table's natural key to lookup the specific item
	 */
	public static boolean isSystemListItemLookup(SystemUserInterfaceAware bean) {
		if (bean != null) {
			if (bean.getValueTable() != null) {
				return SYSTEM_LIST_ITEM_TABLE_NAME.equals(bean.getValueTable().getName());
			}
			if (bean.getUserInterfaceConfig() != null) {
				return SYSTEM_LIST_COMBO_XTYPE.equals(getUserInterfaceConfigPropertyValue(bean, XTYPE_PROPERTY));
			}
		}
		return false;
	}


	public static String getSystemListName(SystemUserInterfaceAware bean) {
		String listName = null;
		if (isSystemListItemLookup(bean)) {
			if (bean.getValueListUrl() != null) {
				listName = StringUtils.getParameterValueFromUrl(bean.getValueListUrl(), SYSTEM_LIST_COMBO_LIST_NAME_PROPERTY);
			}
			else if (bean.getUserInterfaceConfig() != null) {
				listName = getUserInterfaceConfigPropertyValue(bean, SYSTEM_LIST_COMBO_LIST_NAME_PROPERTY);
			}
		}
		return listName;
	}


	public static String getComboSearchField(SystemUserInterfaceAware bean) {
		String fieldName = null;
		if (isComboLookup(bean)) {
			fieldName = getUserInterfaceConfigPropertyValue(bean, COMBO_SEARCH_FIELD_PROPERTY);
			if (StringUtils.isEmpty(fieldName)) {
				fieldName = COMBO_SEARCH_FIELD_PROPERTY_DEFAULT;
			}
		}
		return fieldName;
	}


	public static String getComboValueField(SystemUserInterfaceAware bean) {
		String fieldName = null;
		if (isComboLookup(bean)) {
			fieldName = getUserInterfaceConfigPropertyValue(bean, COMBO_VALUE_FIELD_PROPERTY);
			if (StringUtils.isEmpty(fieldName)) {
				if (isSystemListItemLookup(bean)) {
					fieldName = SYSTEM_LIST_COMBO_VALUE_FIELD_PROPERTY_DEFAULT;
				}
				else {
					fieldName = COMBO_VALUE_FIELD_PROPERTY_DEFAULT;
				}
			}
		}
		return fieldName;
	}


	public static String getComboDisplayField(SystemUserInterfaceAware bean) {
		String fieldName = null;
		if (isComboLookup(bean)) {
			fieldName = getUserInterfaceConfigPropertyValue(bean, COMBO_DISPLAYFIELD_PROPERTY);
			if (StringUtils.isEmpty(fieldName)) {
				if (isSystemListItemLookup(bean)) {
					fieldName = SYSTEM_LIST_COMBO_DISPLAYFIELD_PROPERTY_DEFAULT;
				}
				else {
					fieldName = COMBO_DISPLAYFIELD_PROPERTY_DEFAULT;
				}
			}
		}
		return fieldName;
	}


	/**
	 * Returns the UI Config property value for the given property name, i.e. valueField: 'id', displayField: 'name', xtype: 'combo'
	 */
	public static String getUserInterfaceConfigPropertyValue(SystemUserInterfaceAware bean, String propertyName) {
		String value = null;
		if (bean != null && !StringUtils.isEmpty(bean.getUserInterfaceConfig()) && !StringUtils.isEmpty(propertyName)) {
			int index = bean.getUserInterfaceConfig().indexOf(propertyName + ":");
			if (index >= 0) {
				value = bean.getUserInterfaceConfig().substring(index);
				index = value.indexOf("'");
				value = value.substring(index + 1);
				value = value.substring(0, value.indexOf("'"));
			}
		}
		return value;
	}
}
