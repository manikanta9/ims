package com.clifton.system.userinterface;


/**
 * {@link SystemUserInterfaceMultiValueAware} represents a DTO type which provides a custom UI representation (see {@link SystemUserInterfaceValueAware}) and a
 * single value.
 * <p>
 * DTOs implementing this interface may represent multiple values if {@link #isMultipleValuesAllowed()} returns {@code true}. If the DTO represents multiple
 * values, these individual values shall be delimited within the result of {@link #getValue()} via the delimiter {@link #MULTI_VALUE_LIST_DELIMITER}.
 *
 * @author MikeH
 */
public interface SystemUserInterfaceMultiValueAware<T extends SystemUserInterfaceAware> extends SystemUserInterfaceValueAware<T> {

	/**
	 * The string delimiting individual values within the value represented by the DTO.
	 */
	public static final String MULTI_VALUE_LIST_DELIMITER = "::";


	/**
	 * Indicates whether this object represents a single value (when {@code false}) or multiple values (when {@code true}) via the {@link
	 * #MULTI_VALUE_LIST_DELIMITER} delimiter.
	 *
	 * @return {@code true} if this object represents multiple values, or {@code false} otherwise
	 */
	public boolean isMultipleValuesAllowed();
}
