package com.clifton.system.upload;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.schema.SystemTable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * The <code>SystemUploadService</code> interface defines the methods necessary for
 * working with uploads.
 *
 * @author manderson
 */
public interface SystemUploadService {

	////////////////////////////////////////////////////////////////////////////
	////////             SystemUploadCommand Business Methods               ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a sample DataTable for the upload for the given tableName
	 * If allRows is true, exports ALL data into the excel file, otherwise
	 * just a subset (currently set to 10) rows of data.
	 * <p>
	 * Note: Upload Bean: insertMissingFkBeans properties - Most cases we don't insert fk beans, just look them up, when that is the case will include natural key only, not all properties on the bean
	 * Makes sample files easier to read with less unnecessary data
	 */
	@SecureMethod(dynamicTableNameUrlParameter = "tableName")
	public DataTable downloadSystemUploadFileSample(SystemUploadCommand uploadCommand, boolean allRows);


	/**
	 * Can be called from other service methods that have a more customized/simple upload and additional filters are useful in narrowing the results
	 * down to what user select
	 */
	@DoNotAddRequestMapping
	public DataTable downloadSystemUploadFileSampleWithAdditionalFilters(SystemUploadCommand uploadCommand, final boolean allRows, Map<String, Object> additionalFilterMap);


	/**
	 * Uploads data in DataTable argument using configuration defined by
	 * SystemTable with given table name's metadata and other properties defined
	 * by the SystemUploadCommand bean
	 * <p>
	 * All results are stored in the {@link SystemUploadResult} object including
	 * what saves, if any, or what errors, if any occurred.
	 */
	@SecureMethod(dynamicTableNameUrlParameter = "tableName")
	public void uploadSystemUploadFile(SystemUploadCommand uploadCommand);


	/**
	 * Performs multiple uploads of each sheet in the file where the sheetName
	 * matches a key in the map and uploads that sheet to the table value.
	 */
	@DoNotAddRequestMapping
	public void uploadSystemUploadFileSheets(SystemUploadCommand uploadCommand, Map<String, String> sheetNameToTableNameMap);


	/**
	 * Checks Migration Table Column information and returns as a String the Natural Key for the given table
	 */
	@ResponseBody
	@SecureMethod(dtoClass = SystemTable.class)
	public String getSystemUploadTableDefaultNaturalKey(String tableName);
}
