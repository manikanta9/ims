package com.clifton.system.upload;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.mvc.WebMethodHandler;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import com.clifton.system.list.search.SystemListItemSearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.system.upload.config.SystemUploadConfig;
import com.clifton.system.upload.config.SystemUploadConfigPopulator;
import com.clifton.system.upload.converter.SystemUploadBeanListConverter;
import com.clifton.system.userinterface.SystemUserInterfaceAwareUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>SystemUploadHandlerImpl</code> ...
 *
 * @author manderson
 */
@Component
public class SystemUploadHandlerImpl<L extends SystemList> implements SystemUploadHandler {

	private ApplicationContextService applicationContextService;

	private DaoLocator daoLocator;

	private SystemColumnService systemColumnService;
	private SystemColumnValueHandler systemColumnValueHandler;

	private SystemListService<L> systemListService;

	private SystemSchemaService systemSchemaService;

	private SystemUploadConfigPopulator systemUploadConfigPopulator;
	private SystemUploadBeanListConverter<IdentityObject> systemUploadBeanListConverter;

	private WebMethodHandler webMethodHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<IdentityObject> convertSystemUploadFileToBeanList(SystemUploadCommand uploadCommand, boolean performDaoLookup) {
		if (performDaoLookup) {
			Map<String, List<IdentityObject>> result = convertSystemUploadFileToBeanMap(uploadCommand, false);
			return result.get(uploadCommand.getTableName());
		}
		return convertSystemUploadFileToBeanListImpl(uploadCommand);
	}



	@Override
	public void uploadSystemUploadFile(SystemUploadCommand uploadCommand) {
		try {
			convertSystemUploadFileToBeanMap(uploadCommand, true);
		}
		catch (Throwable e) {
			if (CoreExceptionUtils.isValidationException(e)) {
				throw e;
			}
			throw new RuntimeException("Error in uploadSystemUploadFile for " + uploadCommand, e);
		}
	}


	@Override
	public void processSystemUploadCustomColumnValueList(List<IdentityObject> beanList) {
		Map<String, List<SystemListItem>> systemListItemMap = new HashMap<>();
		Map<String, IdentityObject> comboLookupResultMap = new HashMap<>();
		for (IdentityObject bean : CollectionUtils.getIterable(beanList)) {
			SystemColumnCustomValueAware awareBean = (SystemColumnCustomValueAware) bean;
			List<SystemColumnValue> valueList = awareBean.getColumnValueList();
			Map<String, List<SystemColumnValue>> columnNameValueListMap = BeanUtils.getBeansMap(valueList, systemColumnValue -> systemColumnValue.getColumn().getName());
			for (Map.Entry<String, List<SystemColumnValue>> columnNameEntry : columnNameValueListMap.entrySet()) {
				// If only one - process it
				if (CollectionUtils.getSize(columnNameEntry.getValue()) == 1) {
					processSystemColumnValue(columnNameEntry.getValue().get(0), systemListItemMap, comboLookupResultMap);
				}
				// If more than one, then confirm all attributes are similar (i.e. same look up lists)
				// Otherwise throw an exception that a specific linked value on the upload is required
				else if (CollectionUtils.getSize(columnNameEntry.getValue()) > 1) {
					if (!isAllColumnsForValueListSimilar(columnNameEntry.getValue())) {
						throw new ValidationException("Custom columns are not filtered and there were multiple found with name [" + columnNameEntry.getKey() + "] that use different attributes for retrieval.  Please enter the correct Linked Value on the upload to determine which columns apply.");
					}
					else {
						// Otherwise - process the first one and apply the same to all
						SystemColumnValue firstValue = columnNameEntry.getValue().get(0);
						processSystemColumnValue(firstValue, systemListItemMap, comboLookupResultMap);
						columnNameEntry.getValue().forEach(columnValue -> {
							columnValue.setValue(firstValue.getValue());
							columnValue.setText(firstValue.getText());
						});
					}
				}
			}
		}
	}


	private boolean isAllColumnsForValueListSimilar(List<SystemColumnValue> valueList) {
		Set<String> uniqueRetrievalStringList = new HashSet<>();
		for (SystemColumnValue columnValue : CollectionUtils.getIterable(valueList)) {
			if (SystemUserInterfaceAwareUtils.isComboLookup(columnValue.getColumn())) {
				String key;
				if (SystemUserInterfaceAwareUtils.isSystemListItemLookup(columnValue.getColumn())) {
					key = SystemUserInterfaceAwareUtils.getSystemListName(columnValue.getColumn());
				}
				else {
					key = ((columnValue.getColumn().getValueTable() != null) ? columnValue.getColumn().getValueTable().getName() : columnValue.getColumn().getValueListUrl());
				}
				if (!uniqueRetrievalStringList.contains(key)) {
					uniqueRetrievalStringList.add(key);
				}
				if (uniqueRetrievalStringList.size() > 1) {
					return false;
				}
			}
		}
		return true;
	}


	private void processSystemColumnValue(SystemColumnValue columnValue, Map<String, List<SystemListItem>> systemListItemMap, Map<String, IdentityObject> comboLookupResultMap) {
		if (SystemUserInterfaceAwareUtils.isComboLookup(columnValue.getColumn())) {
			// List Item look ups - checks text if available, else checks value
			if (SystemUserInterfaceAwareUtils.isSystemListItemLookup(columnValue.getColumn())) {
				populateSystemColumnValueFromSystemListItemLookup(columnValue, systemListItemMap);
			}
			// Regular look ups only perform if text is populated
			else if (!StringUtils.isEmpty(columnValue.getText())) {
				populateSystemColumnValueFromComboLookup(columnValue, comboLookupResultMap);
			}
		}
		else { // Copy Value to Text or Text to Value (If either not explicitly set)
			if (StringUtils.isEmpty(columnValue.getText())) {
				columnValue.setText(columnValue.getValue());
			}
			else if (StringUtils.isEmpty(columnValue.getValue())) {
				columnValue.setValue(columnValue.getText());
			}
		}
	}


	private List<IdentityObject> convertSystemUploadFileToBeanListImpl(SystemUploadCommand uploadCommand) {
		return convertSystemUploadFileToBeanListImpl(getSystemUploadConfigPopulator().populateSystemUploadConfig(uploadCommand));
	}


	private List<IdentityObject> convertSystemUploadFileToBeanListImpl(SystemUploadConfig config) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		if (uploadCommand.getFile() == null && uploadCommand.getWorkbook() == null) {
			throw new FieldValidationException("File selection is required to process an upload.", "file");
		}
		if (StringUtils.isEmpty(uploadCommand.getTableName())) {
			throw new FieldValidationException("Table Name is required to process an upload file.", "tableName");
		}

		// Clear any results from previous attempts
		uploadCommand.clearResults();

		SystemUploadBeanListConverter<IdentityObject> converter = getSystemUploadConverter(uploadCommand);
		List<IdentityObject> beanList = converter.convertSystemUploadToBeanList(config);

		// Clean up potential empty object references
		if (!config.getCheckEmptyObjectMap().isEmpty()) {
			for (IdentityObject bean : CollectionUtils.getIterable(beanList)) {
				for (Map.Entry<String, Object> emptyObjectMapEntry : config.getCheckEmptyObjectMap().entrySet()) {
					clearEmptyObject(bean, emptyObjectMapEntry.getKey(), config.getCheckEmptyObjectMap());
				}
			}
		}

		// If Custom Column Group has been selected - process the values
		if (!StringUtils.isEmpty(uploadCommand.getColumnGroupName())) {
			processSystemUploadCustomColumnValueList(beanList);
		}
		return beanList;
	}


	private void clearEmptyObject(IdentityObject bean, String beanPropertyName, Map<String, Object> emptyObjectMap) {
		Object currentValue = BeanUtils.getPropertyValue(bean, beanPropertyName);
		if (currentValue != null) {
			Object emptyValue = emptyObjectMap.getOrDefault(beanPropertyName, ClassUtils.newInstance(currentValue.getClass()));
			if (CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualPropertiesWithSetters(currentValue, emptyValue, false))) {
				BeanUtils.setPropertyValue(bean, beanPropertyName, null);

				// Nested Path - continue up the chain
				if (beanPropertyName.contains(".")) {
					String parentBeanPropertyName = StringUtils.substringBeforeLast(beanPropertyName, ".");
					clearEmptyObject(bean, parentBeanPropertyName, emptyObjectMap);
				}
			}
		}
	}


	private Map<String, List<IdentityObject>> convertSystemUploadFileToBeanMap(SystemUploadCommand uploadCommand, boolean executeSaves) {
		SystemUploadConfig config = getSystemUploadConfigPopulator().populateSystemUploadConfig(uploadCommand);
		List<IdentityObject> beanList = convertSystemUploadFileToBeanListImpl(config);
		// If we are going to insert missing FK Beans, then we need to store them in a map so
		// when they are inserted, the reference remains intact.
		Map<String, List<IdentityObject>> beanMap = new HashMap<>();
		// Stores all beans that we access in the DAO but not necessarily will insert/update them
		Map<String, List<IdentityObject>> lookupBeanMap = new HashMap<>();

		if (uploadCommand.isPartialUploadAllowed() || !uploadCommand.isError()) {
			ReadOnlyDAO<IdentityObject> dao = config.getTableDAOMap().get(uploadCommand.getTableName());
			// Lookup FK Beans in Database
			int beanIndex = 0;
			for (IdentityObject daoBean : beanList) {
				for (Column column : dao.getConfiguration().getColumnList()) {
					// Column wasn't included in the file - continue on to the next column
					// Used for Partial Bean Updates when some column values shouldn't be overwritten
					if (uploadCommand.getUploadResult().isSkipColumn(column)) {
						continue;
					}
					if (BooleanUtils.isTrue(column.getIgnoreUpload())) {
						uploadCommand.getUploadResult().addSkipBeanProperty(column.getBeanPropertyName());
						continue;
					}
					if (column.getFkTable() != null) {
						IdentityObject fkBean = (IdentityObject) BeanUtils.getPropertyValue(daoBean, column.getBeanPropertyName());
						if (fkBean == null && !column.getColumnRequired()) {
							// No FK Bean to Lookup and Field isn't required
							continue;
						}
						// Else look it up in the Database or in the list of previous beans
						boolean errorIfNull = !uploadCommand.isInsertMissingFKBeans() && !uploadCommand.isSimple();
						if (column.getFkTable().equals(uploadCommand.getTableName())) {
							// If we are referencing the bean within the file, it should be previously created
							errorIfNull = true;
						}
						IdentityObject existingFKBean = getDAOObjectByNaturalKeys(config, beanIndex, fkBean, lookupBeanMap, errorIfNull);
						if (existingFKBean == null && !errorIfNull) {
							if (!uploadCommand.isSimple()) {
								lookupBeanMap.put(column.getFkTable(), addBeanToList(lookupBeanMap.get(column.getFkTable()), fkBean));
								beanMap.put(column.getFkTable(), addBeanToList(beanMap.get(column.getFkTable()), fkBean));
							}
						}
						else {
							fkBean = existingFKBean;
						}
						BeanUtils.setPropertyValue(daoBean, column.getBeanPropertyName(), fkBean);
					}
				}
				// Only continue on if the row isn't already marked with an error
				if (!uploadCommand.getUploadResult().isBeanError(beanIndex)) {
					if (FileUploadExistingBeanActions.INSERT != uploadCommand.getExistingBeans()) {
						IdentityObject existingBean = getDAOObjectByNaturalKeys(config, beanIndex, daoBean, lookupBeanMap, false);
						if (existingBean != null && !existingBean.isNewBean() && FileUploadExistingBeanActions.SKIP == uploadCommand.getExistingBeans()) {
							// Only update if there are any custom fields.
							if (!StringUtils.isEmpty(uploadCommand.getColumnGroupName()) && uploadCommand.isUpdateCustomFieldsAlways() && daoBean instanceof SystemColumnCustomValueAware && !CollectionUtils.isEmpty(((SystemColumnCustomValueAware) daoBean).getColumnValueList())) {
								List<SystemColumnValue> valueList = ((SystemColumnCustomValueAware) daoBean).getColumnValueList();
								daoBean = existingBean;
								((SystemColumnCustomValueAware) daoBean).setColumnValueList(valueList);
								((SystemColumnCustomValueAware) daoBean).setColumnGroupName(uploadCommand.getColumnGroupName());
								// Add to special map so just custom fields are updated
								beanMap.put(uploadCommand.getTableName() + "_CUSTOM", addBeanToList(beanMap.get(uploadCommand.getTableName() + "_CUSTOM"), daoBean));
							}
							// Skip this bean if it already exists
							continue;
						}
						if (existingBean != null) {
							BeanUtils.setPropertyValue(daoBean, "id", existingBean.getIdentity());
							if (existingBean instanceof BaseEntity) {
								// Need to copy the rv for BaseEntity objects because that is how the DAO determines
								// if it is an insert or update.
								BeanUtils.setPropertyValue(daoBean, "rv", ((BaseEntity<?>) existingBean).getRv());
							}
							// If Columns are skipped in the upload, copy them back to the bean
							for (String beanPropertyName : CollectionUtils.getIterable(uploadCommand.getUploadResult().getSkipBeanPropertyList())) {
								// Only set if not a nested property
								// For example parent.parent may be skipped, but if parent is null already and we try to set parent.parent null
								// now the first level parent is an empty object which causes a transient exception
								if (!beanPropertyName.contains(".")) {
									Object resetValue = BeanUtils.getPropertyValue(existingBean, beanPropertyName);
									BeanUtils.setPropertyValue(daoBean, beanPropertyName, resetValue);
								}
							}
						}
					}
					// Only add to the list to save if no errors
					if (!uploadCommand.getUploadResult().isBeanError(beanIndex)) {
						beanMap.put(uploadCommand.getTableName(), addBeanToList(beanMap.get(uploadCommand.getTableName()), daoBean));
						lookupBeanMap.put(uploadCommand.getTableName(), addBeanToList(lookupBeanMap.get(uploadCommand.getTableName()), daoBean));
					}
				}
				beanIndex++;
			}
		}

		// Clean Up Custom Fields that matched on name but don't apply based on Linked Value.
		// Need to do this here, because now all FK field properties have been set, so linked values
		// based on ids are set.  New entities that are inserting the fk field, but linked on id wouldn't apply
		// anyway because the id would be new and wouldn't match up.
		// Ensure if linked property is set and linked value is set - it matches
		if (config.getColumnGroup() != null) {
			String linkedProperty = config.getColumnGroup().getLinkedBeanProperty();
			if (!StringUtils.isEmpty(linkedProperty)) {
				for (IdentityObject daoBean : CollectionUtils.getIterable(beanMap.get(uploadCommand.getTableName()))) {
					SystemColumnCustomValueAware customAwareBean = (SystemColumnCustomValueAware) daoBean;
					Object linkedValue = BeanUtils.getPropertyValue(customAwareBean, linkedProperty, true);
					List<SystemColumnValue> newCvList = new ArrayList<>();
					for (SystemColumnValue cv : CollectionUtils.getIterable(customAwareBean.getColumnValueList())) {
						// Column has no linked value = applies to all - keep it in the list
						if (StringUtils.isEmpty(cv.getColumn().getLinkedValue())) {
							newCvList.add(cv);
						}
						// Otherwise only add it if the linked values match up
						if (linkedValue != null) {
							if (CompareUtils.isEqual(linkedValue.toString(), cv.getColumn().getLinkedValue())) {
								newCvList.add(cv);
							}
						}
					}
					customAwareBean.setColumnValueList(newCvList);
				}
			}
		}

		if (executeSaves) {
			executeDaoSaves(config, beanMap, lookupBeanMap);
		}
		return beanMap;
	}


	@SuppressWarnings("unchecked")
	private SystemUploadBeanListConverter<IdentityObject> getSystemUploadConverter(SystemUploadCommand uploadCommand) {
		if (!StringUtils.isEmpty(uploadCommand.getSystemUploadBeanListConverterBeanName())) {
			return getApplicationContextService().getContextBean(uploadCommand.getSystemUploadBeanListConverterBeanName(), SystemUploadBeanListConverter.class);
		}
		return getSystemUploadBeanListConverter();
	}


	/**
	 * Returns bean in the database found by searching against it's natural keys.  If it doesn't exist
	 * will then look it up in the bean map given. If it references another IdentityObject, will first attempt
	 * to look up the reference.
	 */
	private IdentityObject getDAOObjectByNaturalKeys(SystemUploadConfig config, int beanIndex, IdentityObject bean, Map<String, List<IdentityObject>> lookupBeanMap, boolean errorIfNull) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		if (bean == null) {
			return null;
		}
		String tableName = config.getClassTableMap().get(bean.getClass());

		String[] beanFields = config.getTableNaturalKeyBeanFields().get(tableName);
		if (beanFields == null || beanFields.length == 0) {
			return null;
		}

		boolean dbLookup = true;
		Object[] values = new Object[beanFields.length];
		for (int i = 0; i < beanFields.length; i++) {
			values[i] = BeanUtils.getPropertyValue(bean, beanFields[i]);
			// If it's an IdentityObject - look it up
			if (values[i] instanceof IdentityObject) {
				IdentityObject obj = (IdentityObject) values[i];
				if (obj.isNewBean()) {
					IdentityObject daoObj = getDAOObjectByNaturalKeys(config, beanIndex, obj, lookupBeanMap, !uploadCommand.isInsertMissingFKBeans());
					if (daoObj != null) {
						BeanUtils.setPropertyValue(bean, beanFields[i], daoObj);
						values[i] = daoObj;
						if (daoObj.isNewBean()) {
							dbLookup = false;
						}
					}
					else {
						// Don't bother looking it up in the database, because based on it's natural key
						// not existing, it doesn't exist
						dbLookup = false;
					}
				}
			}
		}

		// Check what will be inserted from this upload file before looking up in the database
		List<IdentityObject> subList = lookupBeanMap.get(tableName);
		if (!CollectionUtils.isEmpty(subList)) {
			for (int i = 0; i < beanFields.length; i++) {
				subList = BeanUtils.filterByPropertyName(subList, beanFields[i], new Object[]{values[i]}, false, true);
			}
			if (CollectionUtils.isSingleElement(subList)) {
				return subList.get(0);
			}
		}

		ReadOnlyDAO<IdentityObject> dao = config.getTableDAOMap().get(tableName);
		if (dbLookup) {
			IdentityObject existingBean = getDAOObjectByNaturalKeysInDatabase(dao, beanFields, values);
			if (existingBean != null) {
				lookupBeanMap.put(tableName, addBeanToList(lookupBeanMap.get(tableName), existingBean));
				return existingBean;
			}
		}

		// Check if there is an uploadLookupServiceBeanName/uploadLookupServiceMethodName set on any column with a value and lookup that way
		for (Column column : CollectionUtils.getIterable(dao.getConfiguration().getColumnList())) {
			if (column.getUploadLookupServiceBeanName() != null && column.getUploadLookupServiceMethodName() != null) {
				IdentityObject existingBean = getDAOObjectByUploadLookupMethod(BeanUtils.getPropertyValue(bean, column.getBeanPropertyName()), column.getUploadLookupServiceBeanName(),
						column.getUploadLookupServiceMethodName());
				if (existingBean != null) {
					lookupBeanMap.put(tableName, addBeanToList(lookupBeanMap.get(tableName), existingBean));
					return existingBean;
				}
			}
		}

		// Override errorIfNull parameter if the table we are looking for data from
		// doesn't allow inserts via uploads - only look it up if the bean actually is
		// null - unless it's a simple upload in which case calling method may have an override to set the value
		if (!uploadCommand.isSimple()) {
			SystemTable table = getSystemSchemaService().getSystemTableByName(tableName);
			if (!table.isUploadAllowed()) {
				errorIfNull = true;
			}

			SystemUploadResult result = uploadCommand.getUploadResult();
			// Add Error Messages that can't find bean by natural keys
			if (errorIfNull) {
				if (tableName.equals(config.getTableName())) {
					result.addMissingSelfReferencingFKPropertyError(beanIndex, uploadCommand.getTableName(), config.getTableNaturalKeyBeanFields().get(tableName), values);
				}
				else {
					result.addMissingFKPropertyError(beanIndex, tableName, config.getTableNaturalKeyBeanFields().get(tableName), values);
				}
			}
			// Otherwise will be inserting bean
			else {
				// Verify Required Fields for FK DAO Bean Inserts
				for (Column column : dao.getConfiguration().getColumnList()) {
					if ("id".equals(column.getBeanPropertyName()) || BeanUtils.isSystemManagedField(column.getBeanPropertyName())) {
						continue;
					}
					if (BooleanUtils.isTrue(column.getIgnoreUpload())) {
						result.addSkipBeanProperty(column.getBeanPropertyName());
						continue;
					}
					if (column.getColumnRequired() && BeanUtils.getPropertyValue(bean, column.getBeanPropertyName()) == null) {
						result.addMissingFKRequiredProperty(beanIndex, tableName, beanFields, values, column.getName());
					}
				}
			}
		}
		return null;
	}


	private IdentityObject getDAOObjectByNaturalKeysInDatabase(ReadOnlyDAO<IdentityObject> dao, String[] beanFields, Object[] values) {
		// Try natural key first
		IdentityObject result;
		try {
			result = dao.findOneByFields(beanFields, values);
		}
		// If more than one - throw an exception
		catch (IllegalStateException e) {
			throw new ValidationException("Natural Key Lookup Error: [" + e.getMessage() + "]", e);
		}

		// If none, try simple natural key lookup to avoid "NULL" fields values that were actually skipped
		// Composite Natural Keys Only:
		// If none by natural key see if there are null values and if so skip those in lookup (simple natural key lookup)
		if (result == null && beanFields != null && beanFields.length > 1) {
			String[] simpleBeanFields = new String[0];
			Object[] simpleValues = new Object[0];
			boolean attemptSimple = false;
			for (int i = 0; i < beanFields.length; i++) {
				if (values[i] == null) {
					// As long as we find one null value, try simple lookup
					attemptSimple = true;
					continue;
				}
				simpleBeanFields = ArrayUtils.add(simpleBeanFields, beanFields[i]);
				simpleValues = ArrayUtils.add(simpleValues, values[i]);
			}
			if (attemptSimple && simpleBeanFields.length > 0) {
				try {
					result = dao.findOneByFields(simpleBeanFields, simpleValues);
				}
				catch (IllegalStateException e) {
					throw new ValidationException("Simple Natural Key Lookup Error: [" + e.getMessage() + "]. Full natural key (" + StringUtils.join(beanFields, ",")
							+ ") is required and must be fully populated in the upload.", e);
				}
			}
		}
		return result;
	}


	private IdentityObject getDAOObjectByUploadLookupMethod(Object value, String uploadLookupServiceBeanName, String uploadLookupServiceMethodName) {
		if (value != null) {
			Object lookupServiceBean = getApplicationContextService().getContextBean(uploadLookupServiceBeanName);
			if (lookupServiceBean == null) {
				throw new RuntimeException("Cannot find bean with name = '" + uploadLookupServiceBeanName + "' in application context.");
			}
			Method method;
			try {
				method = lookupServiceBean.getClass().getMethod(uploadLookupServiceMethodName, value.getClass());
			}
			catch (Throwable e) {
				throw new RuntimeException("Cannot find method '" + uploadLookupServiceMethodName + "' on bean '" + uploadLookupServiceBeanName + "': " + e.getMessage(), e);
			}

			try {
				Object result = method.invoke(lookupServiceBean, value);
				if (result instanceof IdentityObject) {
					return (IdentityObject) result;
				}
			}
			catch (Throwable e) {
				throw new RuntimeException("Error executing " + uploadLookupServiceBeanName + "." + uploadLookupServiceMethodName + "(): " + e.getMessage(), e);
			}
		}
		return null;
	}


	/**
	 * Executes the actual DAO saves for all of the beans in the upload.
	 */
	@Transactional(timeout = 180)
	protected void executeDaoSaves(SystemUploadConfig config, Map<String, List<IdentityObject>> beanMap, Map<String, List<IdentityObject>> existingBeanListMap) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		if (beanMap.isEmpty()) {
			return;
		}
		if (uploadCommand.isPartialUploadAllowed() || !uploadCommand.isError()) {
			if (uploadCommand.isInsertMissingFKBeans()) {
				for (Map.Entry<String, List<IdentityObject>> stringListEntry : beanMap.entrySet()) {
					if ((stringListEntry.getKey()).equals(uploadCommand.getTableName())) {
						// Perform save for final table last
						continue;
					}
					int saveCount = saveBeanList(config, stringListEntry.getKey(), stringListEntry.getValue(), existingBeanListMap);
					uploadCommand.getUploadResult().addUploadResults(stringListEntry.getKey(), saveCount, false);
				}
			}

			int saveCount = saveBeanList(config, uploadCommand.getTableName(), beanMap.get(uploadCommand.getTableName()), existingBeanListMap);

			if (beanMap.get(uploadCommand.getTableName() + "_CUSTOM") != null) {
				for (IdentityObject entity : CollectionUtils.getIterable(beanMap.get(uploadCommand.getTableName() + "_CUSTOM"))) {
					try {
						SystemColumnCustomValueAware awareBean = (SystemColumnCustomValueAware) entity;

						// If can have different columns with same name for different linked values, make sure we are pointing to the correct column
						if (config.getColumnGroup() != null && !StringUtils.isEmpty(config.getColumnGroup().getLinkedBeanProperty())) {
							List<SystemColumnCustom> columnList = getSystemColumnService().getSystemColumnCustomListForEntity(awareBean);
							List<SystemColumnValue> valueList = new ArrayList<>();
							for (SystemColumnValue v : CollectionUtils.getIterable(awareBean.getColumnValueList())) {
								for (SystemColumnCustom custom : CollectionUtils.getIterable(columnList)) {
									if (v.getColumn().getName().equals(custom.getName())) {
										v.setColumn(custom);
										valueList.add(v);
										columnList.remove(custom);
										break;
									}
								}
							}
							awareBean.setColumnValueList(valueList);
						}

						getSystemColumnValueHandler().saveSystemColumnValueListForEntity(awareBean);
					}
					catch (Exception e) {
						throw new ValidationException("Error saving custom fields for entity [" + BeanUtils.getLabel(entity) + "]: " + e.getMessage(), e);
					}
				}
				uploadCommand.getUploadResult().addUploadResults(uploadCommand.getTableName() + (" - Custom Column Data Only"), CollectionUtils.getSize(beanMap.get(uploadCommand.getTableName() + "_CUSTOM")), true);
			}

			uploadCommand.getUploadResult().addUploadResults(uploadCommand.getTableName(), saveCount, FileUploadExistingBeanActions.UPDATE == uploadCommand.getExistingBeans());
		}
	}


	/**
	 * Shortcut for adding beans to the list of beans.  If the list is null
	 * will create a new list
	 */
	private List<IdentityObject> addBeanToList(List<IdentityObject> list, IdentityObject bean) {
		if (list == null) {
			list = new ArrayList<>();
		}
		list.add(bean);
		return list;
	}


	private int saveBeanList(SystemUploadConfig config, String tableName, List<IdentityObject> beanList, Map<String, List<IdentityObject>> existingBeanListMap) {
		List<IdentityObject> saveList = beanList;
		if (config.getUploadCommand().isSmartSave()) {
			List<IdentityObject> existingList = BeanUtils.filter(existingBeanListMap.get(tableName), bean -> !bean.isNewBean());
			saveList = BeanUtils.filter(saveList, saveBean -> {
				if (saveBean.isNewBean()) {
					return true;
				}
				IdentityObject existingBean = CollectionUtils.getFirstElement(BeanUtils.filter(existingList, bean -> CompareUtils.isEqual(bean, saveBean)));
				if (existingBean == null || !CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualPropertiesWithSetters(existingBean, saveBean, false))) {
					return true;
				}
				return false;
			});
			int skipCount = CollectionUtils.getSize(beanList) - CollectionUtils.getSize(saveList);
			if (skipCount > 0) {
				config.getUploadCommand().getUploadResult().addUploadResultsSkipped(tableName, skipCount, "");
			}
		}

		Object bean = config.getTableSaveServiceBeanMap().get(tableName);
		Method method = config.getTableSaveMethodMap().get(tableName);
		if (method != null) {
			try {
				for (IdentityObject dto : CollectionUtils.getIterable(saveList)) {
					method.invoke(bean, dto);
				}
			}
			catch (Throwable e) {
				throw new RuntimeException("Error executing " + bean.getClass().getName() + "." + method.getName() + "(IdentityObject dto) for System Upload saves for table [" + tableName + "]: "
						+ e.getMessage(), e);
			}
		}
		else {
			UpdatableDAO<IdentityObject> dao = (UpdatableDAO<IdentityObject>) config.getTableDAOMap().get(tableName);
			dao.saveList(saveList);
		}

		return CollectionUtils.getSize(saveList);
	}


	////////////////////////////////////////////////////////////////////////////
	///////             SystemColumnCustom Helper Methods               ////////
	////////////////////////////////////////////////////////////////////////////


	private void populateSystemColumnValueFromSystemListItemLookup(SystemColumnValue columnValue, Map<String, List<SystemListItem>> systemListItemMap) {
		String systemListName = SystemUserInterfaceAwareUtils.getSystemListName(columnValue.getColumn());
		if (systemListName == null) {
			throw new ValidationException("Unable to determine System List Name to look up values for Custom Column [" + columnValue.getColumn().getName() + "]");
		}
		List<SystemListItem> itemList = systemListItemMap.get(systemListName);
		if (itemList == null) {
			SystemListItemSearchForm searchForm = new SystemListItemSearchForm();
			searchForm.setListName(systemListName);
			itemList = getSystemListService().getSystemListItemList(searchForm);
			systemListItemMap.put(systemListName, itemList);
		}
		if (CollectionUtils.isEmpty(itemList)) {
			throw new ValidationException("Unable to find any available values for System List [" + systemListName + "] needed for Custom Column [" + columnValue.getColumn().getName() + "]");
		}
		// Users would be more likely to correctly enter the text value since that is what is visible to them...use this in lookup if populated
		if (!StringUtils.isEmpty(columnValue.getText())) {
			// Compare Text values as both capitalized so it isn't case sensitive
			List<SystemListItem> filteredItemList = BeanUtils.filter(itemList, (SystemListItem systemListItem) -> BeanUtils.getPropertyValue(systemListItem, SystemUserInterfaceAwareUtils.getComboDisplayField(columnValue.getColumn())), columnValue.getText());
			if (CollectionUtils.isEmpty(filteredItemList)) {
				throw new ValidationException("Cannot find item [" + columnValue.getText() + "] for System List [" + systemListName + "] needed for Custom Column [" + columnValue.getColumn().getName() + "]");
			}
			// Name Equals would result in max of one
			setSystemColumnValueProperties(columnValue, filteredItemList.get(0));
		}
		// Otherwise check the value field
		else if (!StringUtils.isEmpty(columnValue.getValue())) {
			List<SystemListItem> filteredItemList = BeanUtils.filter(itemList, (SystemListItem systemListItem) -> BeanUtils.getPropertyValue(systemListItem, SystemUserInterfaceAwareUtils.getComboValueField(columnValue.getColumn())), columnValue.getValue());
			if (CollectionUtils.isEmpty(filteredItemList)) {
				throw new ValidationException("Cannot find item with value [" + columnValue.getValue() + "] for System List [" + systemListName + "] needed for Custom Column [" + columnValue.getColumn().getName()
						+ "]");
			}
			// Value Equals would result in max of one
			setSystemColumnValueProperties(columnValue, filteredItemList.get(0));
		}
	}


	private void populateSystemColumnValueFromComboLookup(SystemColumnValue columnValue, Map<String, IdentityObject> comboLookupResultMap) {
		IdentityObject valueObject = null;

		// If already found & stored in map - use the result
		String key = ((columnValue.getColumn().getValueTable() != null) ? columnValue.getColumn().getValueTable().getName() : columnValue.getColumn().getValueListUrl()) + "_" + columnValue.getText();
		if (comboLookupResultMap.containsKey(key)) {
			valueObject = comboLookupResultMap.get(key);
		}

		// Nothing stored - look it up
		if (valueObject == null) {
			// If there is a table, attempt a natural key lookup if there is only one natural key column
			if (columnValue.getColumn().getValueTable() != null) {
				valueObject = getSystemColumnValueComboResultForSingleNaturalKey(columnValue);
			}

			// If nothing found via natural key lookup (or couldn't perform a natural key lookup) try url lookup
			if (valueObject == null && columnValue.getColumn().getValueListUrl() != null) {
				valueObject = getSystemColumnValueComboResultForUrlLookup(columnValue);
			}

			// If a result was found - put it in the map to re-use match (often uploads have the same data throughout)
			if (valueObject != null) {
				comboLookupResultMap.put(key, valueObject);
			}
			else {
				throw new ValidationException("Cannot find result for [" + columnValue.getText() + "] needed for Custom Column [" + columnValue.getColumn().getName() + "]");
			}
		}

		setSystemColumnValueProperties(columnValue, valueObject);
	}


	/**
	 * If table dao has only one natural key field attempts lookup on that field
	 * If none, more than one natural key will return null
	 */
	private IdentityObject getSystemColumnValueComboResultForSingleNaturalKey(SystemColumnValue columnValue) {
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(columnValue.getColumn().getValueTable().getName());
		Column naturalKeyColumn = null;
		for (Column daoColumn : dao.getConfiguration().getColumnList()) {
			if (daoColumn.isNaturalKey()) {
				// First one found - set it
				if (naturalKeyColumn == null) {
					naturalKeyColumn = daoColumn;
				}
				// More than one found - clear natural key column and break out of the loop
				else {
					naturalKeyColumn = null;
					break;
				}
			}
		}
		// If only one natural key found - try to look it up
		if (naturalKeyColumn != null) {
			return dao.findOneByField(naturalKeyColumn.getBeanPropertyName(), columnValue.getText());
		}
		return null;
	}


	private IdentityObject getSystemColumnValueComboResultForUrlLookup(SystemColumnValue columnValue) {
		Map<String, String> params = new HashMap<>();
		params.put(SystemUserInterfaceAwareUtils.getComboSearchField(columnValue.getColumn()), columnValue.getText());
		List<IdentityObject> result = getWebMethodHandler().executeServiceCall(columnValue.getColumn().getValueListUrl(), params);
		if (CollectionUtils.isEmpty(result)) {
			throw new ValidationException("Cannot find [" + columnValue.getText() + "] needed for Custom Column [" + columnValue.getColumn().getName() + "]");
		}
		// One Result - Return it
		if (CollectionUtils.getSize(result) == 1) {
			return result.get(0);
		}

		// If more than one - see if you can find one exact match (i.e. search pattern usually does a "like" search, so see if can narrow down by one to the displayfield
		String displayField = SystemUserInterfaceAwareUtils.getComboDisplayField(columnValue.getColumn());
		List<IdentityObject> filteredResult = BeanUtils.filterByPropertyName(result, displayField, new Object[]{columnValue.getText()}, false, true);

		// One match after filtering
		if (CollectionUtils.isSingleElement(filteredResult)) {
			return filteredResult.get(0);
		}

		// Couldn't filter to one exact match
		throw new ValidationException("Found multiple matches to [" + columnValue.getText() + "] needed for Custom Column [" + columnValue.getColumn().getName() + "]: "
				+ BeanUtils.getPropertyValues(result, displayField, ","));
	}


	private void setSystemColumnValueProperties(SystemColumnValue columnValue, IdentityObject result) {
		// Set Value and Also Set Text with Real Display Field so it matches what in the dropdown
		ObjectToStringConverter converter = new ObjectToStringConverter();
		columnValue.setValue(converter.convert(BeanUtils.getPropertyValue(result, SystemUserInterfaceAwareUtils.getComboValueField(columnValue.getColumn()))));
		columnValue.setText(converter.convert(BeanUtils.getPropertyValue(result, SystemUserInterfaceAwareUtils.getComboDisplayField(columnValue.getColumn()))));
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemUploadConfigPopulator getSystemUploadConfigPopulator() {
		return this.systemUploadConfigPopulator;
	}


	public void setSystemUploadConfigPopulator(SystemUploadConfigPopulator systemUploadConfigPopulator) {
		this.systemUploadConfigPopulator = systemUploadConfigPopulator;
	}


	public WebMethodHandler getWebMethodHandler() {
		return this.webMethodHandler;
	}


	public void setWebMethodHandler(WebMethodHandler webMethodHandler) {
		this.webMethodHandler = webMethodHandler;
	}


	public SystemListService<L> getSystemListService() {
		return this.systemListService;
	}


	public void setSystemListService(SystemListService<L> systemListService) {
		this.systemListService = systemListService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemUploadBeanListConverter<IdentityObject> getSystemUploadBeanListConverter() {
		return this.systemUploadBeanListConverter;
	}


	public void setSystemUploadBeanListConverter(SystemUploadBeanListConverter<IdentityObject> systemUploadBeanListConverter) {
		this.systemUploadBeanListConverter = systemUploadBeanListConverter;
	}
}
