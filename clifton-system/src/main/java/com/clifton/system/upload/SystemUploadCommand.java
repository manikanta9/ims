package com.clifton.system.upload;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;


/**
 * The <code>SystemUploadCommand</code> defines the options for uploading data into the system for a specified table and file
 *
 * @author manderson
 */
@NonPersistentObject
public class SystemUploadCommand {


	private String tableName;

	/**
	 * For the uploaded table only - allows users to override the natural key used in the lookup
	 * Example: InvestmentInterestRateIndex table is unique on name and symbol
	 * Natural key is Symbol, but user could say they want to upload using name as the lookup property
	 */
	private String[] overrideTableNaturalKeyBeanProperties;

	/**
	 * Can be used in special cases where we want to explicitly include a column in the file to process
	 * Example: InvestmentSecurity Uploads for One to One Securities - Since we are also adding the Instrument, but Instruments don't actually support uploads the only available fields are those that are part of the natural key.
	 * We want in these custom cases, the ability to specify the instrument trading currency to include for each security
	 * Should be very careful when using this - useful for sample downloads - but should be thoroughly tested when needed
	 */
	private String[] additionalBeanPropertiesToInclude;

	/**
	 * Can be used in special cases where we want to explicitly exclude a column in the file to process.
	 * E.g. BusinessContactAssignment Simple Uploads dynamically generate columns for the FkFieldId column. One use case for this is linking a PM to an InvestmentAccount, which has natural keys for Issuing Company and Account Number.
	 * Only Account Number is desired in the upload so the UI excludes investmentAccount.issuingCompany.
	 */
	private String[] beanPropertiesToExclude;

	/**
	 * If inserting/updating custom fields, this should be set
	 */
	private String columnGroupName;
	private String linkedValue; // Narrows down which custom columns to update
	private boolean updateCustomFieldsAlways; // If SKIP = true, can use this to leave standard values alone and only overwrite custom field values


	/**
	 * Path to a linked {@link com.clifton.system.schema.SystemTable}.
	 * This is used to dynamically find and include Sample File columns for linked entities.
	 * <p>
	 * E.g. BusinessContactAssignments are linked to entities based on the {@link com.clifton.system.schema.SystemTable} associated with the
	 * BusinessContactAssignmentCategory.
	 */
	private String linkedEntityTableNameBeanProperty;
	/**
	 * Path to the bean property of the linked entity id for the DTO associated with this {@link SystemUploadCommand}.
	 * This is used to dynamically find and include Sample File columns for linked entities.
	 * <p>
	 * E.g. BusinessContactAssignments are linked to entities based on the fkFieldId property.
	 */
	private String linkedEntityIdBeanProperty;
	/**
	 * Path to the bean property of the linked entity label for the DTO associated with this {@link SystemUploadCommand}.
	 * This is used to dynamically find and include Sample File columns for linked entities.
	 * <p>
	 * E.g. BusinessContactAssignments are linked to entities and the label of the linked entity is saved as the entitySystemLabel bean property.
	 */
	private String linkedEntityLabelBeanProperty;


	/**
	 * True indicates that if not all rows can be uploaded,
	 * upload the successful rows and ignore the unsuccessful rows
	 * else don't upload any rows
	 */
	private boolean partialUploadAllowed;


	/**
	 * Used when empty cells in the file should be captured as mapped values so that a converter can convert them. Example in PortfolioCashFlowMultipleUploadConverter.
	 */
	private boolean captureNullUnmappedValues = false;

	/**
	 * Used to determine how to handle existing beans in the database
	 * 1.  SKIP - Will lookup beans by Natural Keys and if it exists, will skip that
	 * row in the upload.
	 * 2.  UPDATE - Will lookup bean by Natural Keys and if it exists, will update that
	 * record. (Columns not included in the upload file will not be overwritten in the database)
	 * 3.  INSERT - Will NOT lookup beans in the database (FK look ups are still performed)
	 * and will assume all rows are inserts.  If there is an existing bean it should be caught
	 * by a UX constraint violation during the saves (Used for Improved Performance)
	 */
	private FileUploadExistingBeanActions existingBeans;

	/**
	 * If FK bean lookup by Natural Keys results in a null object
	 * then if this is true, will insert the missing object
	 * else will return with an error.
	 */
	private boolean insertMissingFKBeans;

	/**
	 * The result object is used to generate a string to return to the user
	 * with either error messages/and or what was inserted/updated
	 */
	private SystemUploadResult uploadResult;

	/**
	 * Map Helper that allows storing previously retrieved {@link IdentityObject}s for other rows in the upload.
	 * Note "clearResults" method has been overridden and when called (just prior to processing the file) sets up this map for use.
	 */
	private final Map<String, IdentityObject> linkedEntityMap = new HashMap<>();

	/**
	 * The file is automatically bound by Spring
	 * Workbook is created once so it can be reused for
	 * uploads that upload multiple sheets at once so we
	 * don't have to keep creating a new Workbook each time (slow process)
	 */
	private MultipartFile file;
	private Workbook workbook;

	/**
	 * If working with Excel files with multiple sheets - options
	 * to set the sheet index that contains the data to be uploaded.
	 * NOTE: This is not on UI yet, but implemented for Excel Based test cases
	 */
	private int sheetIndex = 0;

	/**
	 * If true, has special handling that expects the system to determine a lot of the data
	 * for the file based upon presumptions, i.e. for trades one security for the symbol, looking up the appropriate
	 * holding account for the trade, etc.
	 * <p>
	 * In System Upload generic functionality - doesn't validate required column headers since many will
	 * be unnecessary
	 */
	private boolean simple;

	/**
	 * Can be used to prevent updates if the properties of the bean haven't actually changed
	 */
	private boolean smartSave;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(128);
		result.append("{tableName=").append(this.tableName).append(", ")
				.append("uploadResult=").append(this.uploadResult).append('}');
		return result.toString();
	}


	public String getSystemUploadBeanListConverterBeanName() {
		// Uses the default
		return null;
	}


	@SuppressWarnings("unused")
	public void applyUploadSpecificProperties(IdentityObject bean, DataRow row) {
		// DO NOTHING - CAN BE OVERRIDDEN FOR CUSTOMIZATIONS
		// For example, {@link InvestmentSecurityUpload} sets hierarchy from UI, not in upload file, but we need it to look up existing instruments/securities correctly
	}


	public String getUploadResultsString() {
		if (this.uploadResult != null) {
			return this.uploadResult.toString();
		}
		return "";
	}


	public void clearResults() {
		getLinkedEntityMap().clear();
		this.uploadResult = new SystemUploadResult();
	}


	public boolean isError() {
		if (this.uploadResult != null) {
			return this.uploadResult.isError();
		}
		return false;
	}


	public String getLinkedEntityTableName() {
		return (String) BeanUtils.getPropertyValue(this, getLinkedEntityTableNameBeanProperty());
	}


	////////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String[] getOverrideTableNaturalKeyBeanProperties() {
		return this.overrideTableNaturalKeyBeanProperties;
	}


	public void setOverrideTableNaturalKeyBeanProperties(String[] overrideTableNaturalKeyBeanProperties) {
		this.overrideTableNaturalKeyBeanProperties = overrideTableNaturalKeyBeanProperties;
	}


	public String[] getAdditionalBeanPropertiesToInclude() {
		return this.additionalBeanPropertiesToInclude;
	}


	public void setAdditionalBeanPropertiesToInclude(String... additionalBeanPropertiesToInclude) {
		this.additionalBeanPropertiesToInclude = additionalBeanPropertiesToInclude;
	}


	public String[] getBeanPropertiesToExclude() {
		return this.beanPropertiesToExclude;
	}


	public void setBeanPropertiesToExclude(String[] beanPropertiesToExclude) {
		this.beanPropertiesToExclude = beanPropertiesToExclude;
	}


	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public String getLinkedValue() {
		return this.linkedValue;
	}


	public void setLinkedValue(String linkedValue) {
		this.linkedValue = linkedValue;
	}


	public boolean isUpdateCustomFieldsAlways() {
		return this.updateCustomFieldsAlways;
	}


	public void setUpdateCustomFieldsAlways(boolean updateCustomFieldsAlways) {
		this.updateCustomFieldsAlways = updateCustomFieldsAlways;
	}


	public String getLinkedEntityTableNameBeanProperty() {
		return this.linkedEntityTableNameBeanProperty;
	}


	public void setLinkedEntityTableNameBeanProperty(String linkedEntityTableNameBeanProperty) {
		this.linkedEntityTableNameBeanProperty = linkedEntityTableNameBeanProperty;
	}


	public String getLinkedEntityIdBeanProperty() {
		return this.linkedEntityIdBeanProperty;
	}


	public void setLinkedEntityIdBeanProperty(String linkedEntityIdBeanProperty) {
		this.linkedEntityIdBeanProperty = linkedEntityIdBeanProperty;
	}


	public String getLinkedEntityLabelBeanProperty() {
		return this.linkedEntityLabelBeanProperty;
	}


	public void setLinkedEntityLabelBeanProperty(String linkedEntityLabelBeanProperty) {
		this.linkedEntityLabelBeanProperty = linkedEntityLabelBeanProperty;
	}


	public boolean isPartialUploadAllowed() {
		return this.partialUploadAllowed;
	}


	public void setPartialUploadAllowed(boolean partialUploadAllowed) {
		this.partialUploadAllowed = partialUploadAllowed;
	}


	public boolean isCaptureNullUnmappedValues() {
		return this.captureNullUnmappedValues;
	}


	public void setCaptureNullUnmappedValues(boolean captureNullUnmappedValues) {
		this.captureNullUnmappedValues = captureNullUnmappedValues;
	}


	public FileUploadExistingBeanActions getExistingBeans() {
		return this.existingBeans;
	}


	public void setExistingBeans(FileUploadExistingBeanActions existingBeans) {
		this.existingBeans = existingBeans;
	}


	public boolean isInsertMissingFKBeans() {
		return this.insertMissingFKBeans;
	}


	public void setInsertMissingFKBeans(boolean insertMissingFKBeans) {
		this.insertMissingFKBeans = insertMissingFKBeans;
	}


	public SystemUploadResult getUploadResult() {
		return this.uploadResult;
	}


	public void setUploadResult(SystemUploadResult uploadResult) {
		this.uploadResult = uploadResult;
	}


	public Map<String, IdentityObject> getLinkedEntityMap() {
		return this.linkedEntityMap;
	}


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	public Workbook getWorkbook() {
		return this.workbook;
	}


	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}


	public int getSheetIndex() {
		return this.sheetIndex;
	}


	public void setSheetIndex(int sheetIndex) {
		this.sheetIndex = sheetIndex;
	}


	public boolean isSimple() {
		return this.simple;
	}


	public void setSimple(boolean simple) {
		this.simple = simple;
	}


	public boolean isSmartSave() {
		return this.smartSave;
	}


	public void setSmartSave(boolean smartSave) {
		this.smartSave = smartSave;
	}
}
