package com.clifton.system.upload.config;


import com.clifton.system.upload.SystemUploadCommand;


/**
 * The <code>SystemUploadConfigPopulator</code> ...
 *
 * @author manderson
 */
public interface SystemUploadConfigPopulator {

	////////////////////////////////////////////////////////////////////////////
	//////             SystemUploadConfig Populator Method             ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If simpleUpload is true, doesn't enforce that the table must have the upload allowed option.  This enables explicit simple upload support without
	 * allowing a generic upload support (i.e. SystemNotes - simple uploads only, no generic otherwise would end up with all notes uploaded as global notes)
	 */
	public SystemUploadConfig populateSystemUploadConfig(SystemUploadCommand uploadCommand);
}
