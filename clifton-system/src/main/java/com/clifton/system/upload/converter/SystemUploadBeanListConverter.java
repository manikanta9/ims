package com.clifton.system.upload.converter;


import com.clifton.core.beans.IdentityObject;
import com.clifton.system.upload.config.SystemUploadConfig;

import java.util.List;


/**
 * The <code>SystemUploadBeanListConverter</code> ...
 *
 * @author manderson
 */
public interface SystemUploadBeanListConverter<T extends IdentityObject> {

	public List<T> convertSystemUploadToBeanList(SystemUploadConfig config);
}
