package com.clifton.system.upload.converter;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataColumnStyle;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.mvc.WebMethodHandler;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.upload.config.SystemUploadConfig;
import com.clifton.system.userinterface.SystemUserInterfaceAwareUtils;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemUploadSampleDataTableConverter</code> ...
 *
 * @author manderson
 */
public class SystemUploadSampleDataTableConverter implements Converter<List<IdentityObject>, DataTable> {

	/**
	 * The maximum number of options to validate on. If this limit is exceeded, then validation is not used for that column.
	 */
	private static final int MAX_VALIDATION_OPTIONS = 500;

	private static final String REQUIRED_STYLE = ("fontColor=" + DataColumnStyle.FONT_COLOR_RED);
	private static final String DATA_TYPE_STYLE = "dataType=";

	private final SystemUploadConfig config;
	private final WebMethodHandler webMethodHandler;
	private final DaoLocator daoLocator;


	public SystemUploadSampleDataTableConverter(SystemUploadConfig config, WebMethodHandler webMethodHandler, DaoLocator daoLocator) {
		this.config = config;
		this.webMethodHandler = webMethodHandler;
		this.daoLocator = daoLocator;
	}


	@Override
	public DataTable convert(List<IdentityObject> beanList) {
		DataColumn[] dataColumnArray = ArrayUtils.addAll(getStandardColumnArray(), getCustomColumnArray(), getLinkedEntityColumnArray());
		return constructDataTable(beanList, dataColumnArray);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DataColumn[] getStandardColumnArray() {
		return convertColumnListToDataColumnArray(this.config.getColumnList());
	}


	private DataColumn[] convertColumnListToDataColumnArray(List<Column> columnList) {
		int columnListSize = CollectionUtils.getSize(columnList);
		DataColumn[] dataColumnArray = new DataColumn[columnListSize];
		for (int i = 0; i < columnListSize; i++) {
			Column column = columnList.get(i);
			DataColumn dataColumn = new DataColumnImpl(column.getName(), Types.NVARCHAR, column.getDescription());
			if (column.getColumnRequired()) {
				dataColumn.setStyle(DATA_TYPE_STYLE + column.getDataType().name() + "," + REQUIRED_STYLE);
			}
			else {
				dataColumn.setStyle(DATA_TYPE_STYLE + column.getDataType().name());
			}
			dataColumnArray[i] = dataColumn;
		}
		return dataColumnArray;
	}


	private DataColumn[] getCustomColumnArray() {
		int customColumnListSize = CollectionUtils.getSize(this.config.getCustomColumnList());
		DataColumn[] customDataColumnArray = new DataColumn[customColumnListSize];
		List<String> processedColumnNames = new ArrayList<>();
		for (int i = 0; i < customColumnListSize; i++) {
			SystemColumnCustom column = this.config.getCustomColumnList().get(i);
			if (SystemUserInterfaceAwareUtils.isInputField(column)) {
				if (!processedColumnNames.contains(column.getName())) {
					processedColumnNames.add(column.getName());
					// Only show the "Text" field for combo look ups, but will still support uploading both
					if (SystemUserInterfaceAwareUtils.isComboLookup(column)) {
						DataColumn dataColumnText = new DataColumnImpl(column.getName() + SystemUploadConfig.CUSTOM_COMBO_LOOKUP_TEXT_COLUMN_NAME_SUFFIX, Types.NVARCHAR, column.getDescription());

						// For columns which have a URL to retrieve the list of options, generate a list of valid options for validation
						String listUrl = SystemUserInterfaceAwareUtils.getComboLookupURL(column);
						if (listUrl != null) {
							Object result = this.webMethodHandler.executeServiceCall(listUrl, null);

							if (result instanceof Collection) {
								Collection<?> list = (Collection<?>) result;

								if (!CollectionUtils.isEmpty(list) && CollectionUtils.getSize(list) <= MAX_VALIDATION_OPTIONS) {
									List<String> columnOptions = new ArrayList<>();

									for (Object resultItem : CollectionUtils.getIterable(list)) {
										String columnDisplayField = SystemUserInterfaceAwareUtils.getComboDisplayField(column);

										String columnOption = null;
										if (columnDisplayField != null) {
											columnOption = BeanUtils.getPropertyValue(resultItem, columnDisplayField).toString();
										}

										if (!StringUtils.isEmpty(columnOption)) {
											columnOptions.add(columnOption);
										}
									}

									if (!CollectionUtils.isEmpty(columnOptions)) {
										//Convert the options to an array and set it on the column info
										String[] columnOptionsArray = new String[columnOptions.size()];
										columnOptionsArray = columnOptions.toArray(columnOptionsArray);
										dataColumnText.setValidOptions(columnOptionsArray);
									}
								}
							}
						}

						if (column.isRequired()) {
							dataColumnText.setStyle(REQUIRED_STYLE);
						}
						customDataColumnArray[i] = dataColumnText;
					}
					else {
						DataColumn dataColumn = new DataColumnImpl(column.getName(), Types.NVARCHAR, column.getDescription());
						if (column.isRequired()) {
							dataColumn.setStyle(REQUIRED_STYLE);
						}
						customDataColumnArray[i] = dataColumn;
					}
				}
			}
		}
		return customDataColumnArray;
	}


	private DataColumn[] getLinkedEntityColumnArray() {
		return convertColumnListToDataColumnArray(this.config.getLinkedEntityColumnList());
	}


	private DataTable constructDataTable(List<IdentityObject> beanList, DataColumn[] dataColumnArray) {
		int standardColumnListSize = CollectionUtils.getSize(this.config.getColumnList());
		int customColumnListSize = CollectionUtils.getSize(this.config.getCustomColumnList());
		DataTable table = new PagingDataTableImpl(dataColumnArray);
		ReadOnlyDAO<IdentityObject> linkedEntityDao = null;
		Map<Integer, IdentityObject> linkedEntityMap = new HashMap<>();
		for (int j = 0; j < CollectionUtils.getSize(beanList); j++) {
			IdentityObject bean = beanList.get(j);
			Object[] data = new Object[dataColumnArray.length];
			List<SystemColumnValue> valueList = (this.config.getColumnGroup() == null ? null : ((SystemColumnCustomValueAware) bean).getColumnValueList());
			for (int i = 0; i < dataColumnArray.length; i++) {
				if (i < standardColumnListSize) {
					Column column = this.config.getColumnList().get(i);
					Object value = BeanUtils.getPropertyValue(bean, column.getBeanPropertyName(), true);
					if (this.config.getColumnValueConverter(column) != null) {
						data[i] = this.config.getColumnValueConverter(column).reverseConvert(value);
					}
					else {
						data[i] = value;
					}
				}
				else if (i < standardColumnListSize + customColumnListSize) {
					if (!CollectionUtils.isEmpty(valueList)) {
						DataColumn column = dataColumnArray[i];
						String colName = column.getColumnName();
						boolean text = false;
						if (colName.endsWith(SystemUploadConfig.CUSTOM_COMBO_LOOKUP_TEXT_COLUMN_NAME_SUFFIX)) {
							text = true;
							colName = colName.replaceAll(SystemUploadConfig.CUSTOM_COMBO_LOOKUP_TEXT_COLUMN_NAME_SUFFIX, "");
						}
						SystemColumnValue value = CollectionUtils.getOnlyElement(BeanUtils.filter(valueList, columnValue -> columnValue.getColumn().getName(), colName));
						if (value != null) {
							if (text) {
								data[i] = value.getText();
							}
							else {
								data[i] = value.getValue();
							}
						}
					}
				}
				else {
					Column column = config.getLinkedEntityColumnList().get(i - standardColumnListSize - customColumnListSize);
					if (linkedEntityDao == null) {
						linkedEntityDao = this.daoLocator.locate(column.getTableName());
					}
					ReadOnlyDAO<IdentityObject> finalLinkedEntityDao = linkedEntityDao;
					IdentityObject linkedEntity = linkedEntityMap.computeIfAbsent((int) BeanUtils.getPropertyValue(bean, this.config.getLinkedEntityIdBeanProperty()),
							linkedEntityId -> finalLinkedEntityDao.findOneByField("id", linkedEntityId));
					Object value = BeanUtils.getPropertyValue(linkedEntity, StringUtils.substringAfterFirst(column.getBeanPropertyName(), "."));
					ValidationUtils.assertNotNull(value, String.format("Unable to determine value for field [%s] linked to entity [%s]", column.getName(), bean));
					data[i] = value;
				}
			}
			DataRow row = new DataRowImpl(table, data);
			table.addRow(row);
		}
		return table;
	}
}
