package com.clifton.system.upload;


import com.clifton.core.beans.IdentityObject;

import java.util.List;


public interface SystemUploadHandler {

	/**
	 * Used for Simple Uploads where we need to perform some additional logic on the list
	 * The option to perform dao look ups is for foreign keys - in many cases simple uploads
	 * do their own look ups with some specific assumptions so we don't want to do this but
	 * we still want the file converted to a list of beans where the columns set all the properties on those beans
	 * <p>
	 * THIS DOES NOT SAVE ANY DATA TO THE DATABASE
	 */
	public List<IdentityObject> convertSystemUploadFileToBeanList(SystemUploadCommand uploadCommand, boolean performDaoLookup);


	/**
	 * Full standard upload for the file, executes dao saves for the table (and FK tables if opted to insert missing)
	 */
	public void uploadSystemUploadFile(SystemUploadCommand uploadCommand);


	/**
	 * Can be used for custom uploads that need to also process custom column values.  The upload itself
	 * generally just sets the column and value or text, this will make sure the value is fully populated
	 * i.e. populate value from text and SystemListItem or Combo look up
	 */
	public void processSystemUploadCustomColumnValueList(List<IdentityObject> identityObjectList);
}
