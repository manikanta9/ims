package com.clifton.system.upload;


import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemUploadResult</code> ...
 *
 * @author manderson
 */
public class SystemUploadResult implements Serializable {

	private final Map<Object, String> uploadErrorMap = new HashMap<>();
	private final Map<String, String> uploadInsertsMap = new HashMap<>();
	private final List<String> skipBeanPropertyList = new ArrayList<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(20);
		if (this.uploadInsertsMap.isEmpty()) {
			sb.append("No rows were saved in the database.");
		}
		else {
			for (Map.Entry<String, String> stringStringEntry : this.uploadInsertsMap.entrySet()) {
				sb.append(stringStringEntry.getValue());
				sb.append(StringUtils.NEW_LINE);
			}
		}
		sb.append(StringUtils.NEW_LINE);

		if (this.uploadErrorMap.isEmpty()) {
			sb.append("There were no errors with the upload file.");
		}
		else {
			sb.append("The following errors were encountered with the upload file: " + StringUtils.NEW_LINE);
			for (Map.Entry<Object, String> objectStringEntry : this.uploadErrorMap.entrySet()) {
				sb.append("[").append(objectStringEntry.getKey()).append("]: ").append(objectStringEntry.getValue()).append(StringUtils.NEW_LINE);
			}
		}

		return sb.toString();
	}


	public boolean isError() {
		return !this.uploadErrorMap.isEmpty();
	}


	public boolean isRowError(Integer row) {
		return this.uploadErrorMap.containsKey(row);
	}


	public boolean isBeanError(int beanIndex) {
		return this.uploadErrorMap.containsKey("Bean_" + beanIndex);
	}


	public void addUploadResults(String tableName, int rows, boolean update) {
		this.uploadInsertsMap.put(tableName, "[" + tableName + "]: " + rows + " Records Inserted" + (update ? "/Updated" : "") + ".");
	}


	public void addUploadResultsUpdateOnly(String tableName, int rows) {
		this.uploadInsertsMap.put(tableName, "[" + tableName + "]: " + rows + " Records Updated.");
	}


	public void addUploadResults(String resultKey, String uploadMessage) {
		this.uploadInsertsMap.put(resultKey, "[" + resultKey + "]: " + uploadMessage + ".");
	}


	public void addUploadResultsSkipped(String tableName, int rows, String additionalInfo) {
		this.uploadInsertsMap.put(tableName + "_SKIP", "[" + tableName + "]: " + rows + " Records Skipped" + (!StringUtils.isEmpty(additionalInfo) ? additionalInfo : "") + ".");
	}


	public void addRequiredFieldError(Integer rowIndex, String columnName) {
		String error = this.uploadErrorMap.get(rowIndex);
		if (error == null) {
			error = "";
		}
		error += "Missing Required Field Value [" + columnName + "].";
		this.uploadErrorMap.put(rowIndex, error);
	}


	public void addMissingFKRequiredProperty(int beanIndex, String fkTable, String[] naturalKeyBeanFields, Object[] values, String columnName) {
		String error = this.uploadErrorMap.get("Bean_" + beanIndex);

		String errorMessage = "Unable to insert [" + fkTable + "] with natural key(s) [" + StringUtils.join(naturalKeyBeanFields, ",") + "] values [" + getValueArrayAsString(values) + "]: Column [" + columnName
				+ "] is required for inserts." + StringUtils.NEW_LINE;

		if (error == null) {
			error = errorMessage;
		}
		else if (!error.contains(errorMessage)) {
			error += errorMessage;
		}

		this.uploadErrorMap.put("Bean_" + beanIndex, error);
	}


	private String getValueArrayAsString(Object[] values) {
		StringBuilder valueString = new StringBuilder(16);
		for (int i = 0; i < values.length; i++) {
			valueString.append(values[i] == null ? "NULL" : values[i].toString());
			if (i < values.length - 1) {
				valueString.append(",");
			}
		}
		return valueString.toString();
	}


	public void addMissingFKPropertyError(int beanIndex, String fkTable, String[] naturalKeyBeanFields, Object[] values) {
		addMissingFKPropertyError(beanIndex, fkTable, naturalKeyBeanFields, values, null);
	}


	public void addMissingFKPropertyError(int beanIndex, String fkTable, String[] naturalKeyBeanFields, Object[] values, String additionalMessage) {
		String error = this.uploadErrorMap.get("Bean_" + beanIndex);

		String errorMessage = "Unable to lookup [" + fkTable + "] by natural key(s) [" + StringUtils.join(naturalKeyBeanFields, ",") + "] values [" + getValueArrayAsString(values) + "]."
				+ StringUtils.coalesce(true, additionalMessage) + StringUtils.NEW_LINE;

		if (error == null) {
			error = errorMessage;
		}
		else if (!error.contains(errorMessage)) {
			error += errorMessage;
		}

		this.uploadErrorMap.put("Bean_" + beanIndex, error);
	}


	public void addBeanError(int beanIndex, String errorMessage) {
		String error = this.uploadErrorMap.get("Bean_" + beanIndex);
		if (error == null) {
			error = errorMessage;
		}
		else if (!error.contains(errorMessage)) {
			error += errorMessage;
		}
		this.uploadErrorMap.put("Bean_" + beanIndex, error);
	}


	public void addMissingSelfReferencingFKPropertyError(int beanIndex, String tableName, String[] naturalKeyBeanFields, Object[] values) {
		addMissingFKPropertyError(beanIndex, tableName, naturalKeyBeanFields, values,
				"If you are referencing an object from within this file, please make sure it's created in a row before being used as a reference.");
	}


	public void addError(Object key, String message) {
		String error = this.uploadErrorMap.get("Error_" + key);
		if (error == null) {
			error = message;
		}
		else {
			error += message;
		}

		this.uploadErrorMap.put("Error_" + key, error);
	}


	public void addSkipBeanProperty(String beanPropertyName) {
		for (int i = 0; i < this.skipBeanPropertyList.size(); i++) {
			String p = this.skipBeanPropertyList.get(i);
			if (p.startsWith(beanPropertyName + ".")) {
				this.skipBeanPropertyList.set(i, beanPropertyName);
				return;
			}
			else if (beanPropertyName.startsWith(p + ".")) {
				return;
			}
		}
		this.skipBeanPropertyList.add(beanPropertyName);
	}


	public boolean isSkipColumn(Column column) {
		return this.skipBeanPropertyList.contains(column.getBeanPropertyName());
	}


	public List<String> getSkipBeanPropertyList() {
		return this.skipBeanPropertyList;
	}
}
