package com.clifton.system.upload.config;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumnGroup;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.upload.SystemUploadCommand;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The <code>SystemUploadConfigPopulatorImpl</code> creates and populates {@link SystemUploadConfig} object
 * based on the table, column group, and other options passed.
 * <p>
 * Data is populated from migration data defined for the table(s)
 *
 * @author manderson
 */
@Component
public class SystemUploadConfigPopulatorImpl implements SystemUploadConfigPopulator {

	private ApplicationContextService applicationContextService;

	private DaoLocator daoLocator;

	private SystemColumnService systemColumnService;
	private SystemSchemaService systemSchemaService;

	/**
	 * Used for self referencing natural key tables, so we don't run into an infinite loop
	 */
	private int maxLevelsDeep = 5;


	////////////////////////////////////////////////////////////////////////////
	//////             SystemUploadConfig Populator Method               ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If simpleUpload is true, doesn't enforce that the table must have the upload allowed option.  This enables explicit simple upload support without
	 * allowing a generic upload support (i.e. SystemNotes - simple uploads only, no generic otherwise would end up with all notes uploaded as global notes)
	 */
	@Override
	public SystemUploadConfig populateSystemUploadConfig(SystemUploadCommand uploadCommand) {
		SystemTable table = getSystemSchemaService().getSystemTableByName(uploadCommand.getTableName());
		if (table == null) {
			throw new FieldValidationException("Unable to locate Table with Name [" + uploadCommand.getTableName() + "]", "tableName");
		}
		if (!table.isUploadAllowed() && !uploadCommand.isSimple()) {
			throw new FieldValidationException("Uploads are not supported for Table [" + uploadCommand.getTableName() + "]", "tableName");
		}

		SystemUploadConfig config = new SystemUploadConfig(uploadCommand);

		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(uploadCommand.getTableName());
		config.addTableDAO(uploadCommand.getTableName(), dao);
		populateUploadServiceMethod(config, dao);

		populateColumnList(config);
		populateColumnCustomList(config);
		populateLinkedEntityColumnList(config);

		return config;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * In most cases, the upload will call the dao save method directly.  Some cases we call a save method on the service
	 * which can perform some additional functionality prior to saving.  This is defined via migrations.
	 */
	private void populateUploadServiceMethod(SystemUploadConfig config, ReadOnlyDAO<IdentityObject> dao) {
		Table table = dao.getConfiguration().getTable();
		String serviceBeanName = ObjectUtils.coalesce(table.getUploadServiceBeanName(), table.getServiceBeanName());

		if (!StringUtils.isEmpty(serviceBeanName)) {
			String methodName = ObjectUtils.coalesce(table.getUploadServiceMethodName(), table.getServiceSaveMethodName());
			if (!StringUtils.isEmpty(methodName)) {
				Object bean = getApplicationContextService().getContextBean(serviceBeanName);
				if (bean == null) {
					throw new RuntimeException("Cannot find bean with name = '" + serviceBeanName + "' in application context for uploads to table [" + config.getTableName() + "].");
				}
				Method method;
				try {
					method = bean.getClass().getMethod(methodName, dao.getConfiguration().getBeanClass());
				}
				catch (Throwable e) {
					throw new RuntimeException("Cannot find method '" + methodName + "' on bean '" + serviceBeanName + "' for uploads to table [" + config.getTableName() + ": " + e.getMessage(), e);
				}
				config.addTableSaveMethod(config.getTableName(), bean, method);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void populateColumnList(SystemUploadConfig config) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		String[] overrideNaturalKeyBeanProperties = uploadCommand.getOverrideTableNaturalKeyBeanProperties();
		boolean naturalKeyOverrides = overrideNaturalKeyBeanProperties != null && overrideNaturalKeyBeanProperties.length > 0;
		if (naturalKeyOverrides) {
			for (String override : overrideNaturalKeyBeanProperties) {
				config.addTableNaturalKeyBeanField(config.getTableName(), override, true);
			}
		}
		List<Column> columnList = getUploadColumnList(config, naturalKeyOverrides);
		config.setColumnList(columnList);
	}


	private List<Column> getUploadColumnList(SystemUploadConfig config, boolean naturalKeyOverrides) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		// Setup the actual Columns that will be used for this upload
		String[] overrideNaturalKeyBeanProperties = uploadCommand.getOverrideTableNaturalKeyBeanProperties();
		List<Column> columnList = new ArrayList<>();
		ValidationUtils.assertNotEmpty(config.getTableName(), "Table name is required to build upload column list.");
		SystemTable systemTable = getSystemSchemaService().getSystemTableByName(config.getTableName());
		ValidationUtils.assertNotNull(systemTable, String.format("Unable to locate System Table with name [%s] to build upload column list.", config.getTableName()));
		List<SystemColumnStandard> tableColumns = getSystemColumnService().getSystemColumnStandardListByTable(systemTable.getId());
		for (Column daoColumn : config.getDaoConfiguration().getColumnList()) {
			if (isSkipColumn(daoColumn) && (!naturalKeyOverrides || !ArrayUtils.contains(overrideNaturalKeyBeanProperties, daoColumn.getBeanPropertyName()))) {
				continue;
			}
			boolean forceExclude = ArrayUtils.contains(uploadCommand.getBeanPropertiesToExclude(), daoColumn.getBeanPropertyName());
			if (BooleanUtils.isTrue(daoColumn.getIgnoreUpload()) || forceExclude) {
				continue;
			}
			// Need to copy and create a new object so we don't change the original Column definition for Hibernate
			Column column = new Column();
			BeanUtils.copyProperties(daoColumn, column);
			// Properly Support FK Field Lookup
			if (column.getFkTable() != null) {
				// If there is an upload value class name on the FK field itself, then we need it to create a new instance of the give class
				// Used for FK references to a table that uses subclasses.  Generally for uploads we would only support a specific class here
				// For example SystemListItem -> SystemList, we need to use SystemListEntity class
				if (!StringUtils.isEmpty(column.getUploadValueClassName())) {
					Column emptyFkColumn = new Column();
					BeanUtils.copyProperties(daoColumn, emptyFkColumn);
					emptyFkColumn.setRequired(false);
					emptyFkColumn.setName(getColumnNameFromBeanFieldName(emptyFkColumn.getBeanPropertyName()));

					// Add it to the check empty object map - in case it's actually not used we'll set it back to null
					if (!config.getCheckEmptyObjectMap().containsKey(emptyFkColumn.getBeanPropertyName())) {
						config.addBeanPropertyToCheckEmptyObjectSet(emptyFkColumn.getBeanPropertyName(), ClassUtils.newInstance(CoreClassUtils.getClass(emptyFkColumn.getUploadValueClassName())));
					}
					columnList.add(emptyFkColumn);
				}
				if (config.isNaturalKey(column)) {
					config.addTableNaturalKeyBeanField(config.getTableName(), column.getBeanPropertyName(), false);
				}
				columnList.addAll(getTableColumns(config, column));
			}
			else {
				column.setRequired(DataTypes.BIT != column.getDataType() && column.getColumnRequired());
				column.setName(getColumnNameFromBeanFieldName(column.getBeanPropertyName()));
				column.setDescription(getColumnDescription(column, tableColumns));
				columnList.add(column);
				if (config.isNaturalKey(column)) {
					config.addTableNaturalKeyBeanField(config.getTableName(), column.getBeanPropertyName(), false);
				}
			}
			if (!StringUtils.isEmpty(column.getUploadValueConverterName())) {
				// Verify converter wasn't already set
				if (config.getColumnValueConverter(column) == null) {
					@SuppressWarnings("unchecked")
					ReversableConverter<Object, Object> converter = (ReversableConverter<Object, Object>) getApplicationContextService().getContextBean(column.getUploadValueConverterName());
					config.addColumnValueConverter(column.getUploadValueConverterName(), converter);
				}
			}
		}
		return columnList;
	}


	private List<Column> getLinkedEntityColumns(SystemUploadConfig config) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		if (!StringUtils.isEmpty(uploadCommand.getLinkedEntityTableNameBeanProperty())) {
			String linkedEntityTableName = uploadCommand.getLinkedEntityTableName();
			ValidationUtils.assertNotEmpty(linkedEntityTableName, String.format("Unable to find Linked Table Name using Bean Property [%s].", uploadCommand.getLinkedEntityTableNameBeanProperty()));
			if (!StringUtils.isEmpty(uploadCommand.getLinkedEntityIdBeanProperty())) {
				config.addTableNaturalKeyBeanField(uploadCommand.getTableName(), uploadCommand.getLinkedEntityIdBeanProperty(), false);
			}
			return getTableColumns(config, StringUtils.deCapitalize(uploadCommand.getLinkedEntityTableName()), uploadCommand.getLinkedEntityTableName(), true, 1, false);
		}
		return Collections.emptyList();
	}


	private List<Column> getTableColumns(SystemUploadConfig config, Column column) {
		return getTableColumns(config, column.getBeanPropertyName(), column.getFkTable(), column.getColumnRequired(), config.getUploadCommand().isInsertMissingFKBeans());
	}


	private List<Column> getTableColumns(SystemUploadConfig config, String beanPropertyName, String tableName, boolean required, boolean insertMissingFKBeans) {
		return getTableColumns(config, beanPropertyName, tableName, required, 1, insertMissingFKBeans);
	}


	private List<Column> getTableColumns(SystemUploadConfig config, String beanPropertyName, String tableName, boolean required, int levelsDeep, boolean insertMissingFKBeans) {
		List<Column> columnList = new ArrayList<>();
		if (levelsDeep > getMaxLevelsDeep()) {
			return columnList;
		}

		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(tableName);
		config.addTableDAO(tableName, dao);
		SystemTable table = getSystemSchemaService().getSystemTableByName(tableName);
		// Only include all fields if: Table allows uploads and We are Inserting Missing FK Beans
		boolean updatable = table.isUploadAllowed() && insertMissingFKBeans;
		List<SystemColumnStandard> tableColumns = getSystemColumnService().getSystemColumnStandardListByTable(table.getId());

		for (Column column : dao.getConfiguration().getColumnList()) {
			String columnBeanPropertyName = beanPropertyName + "." + column.getBeanPropertyName();
			boolean forceInclude = ArrayUtils.contains(config.getUploadCommand().getAdditionalBeanPropertiesToInclude(), columnBeanPropertyName);
			boolean forceExclude = ArrayUtils.contains(config.getUploadCommand().getBeanPropertiesToExclude(), columnBeanPropertyName);
			ValidationUtils.assertFalse(forceInclude && forceExclude, String.format("Upload is configured to both include and exclude property [%s].", columnBeanPropertyName));
			if (forceExclude) {
				continue;
			}
			// For tables that aren't updatable via uploads, only concerned with the
			// natural key columns for lookup purposes.
			if ((!updatable || BooleanUtils.isTrue(column.getIgnoreUpload())) && !config.isNaturalKey(column) && !forceInclude) {
				continue;
			}

			if (column.getFkTable() != null) {
				// If there is an upload value class name on the FK field itself, then we need it to create a new instance of the give class
				// Used for FK references to a table that uses subclasses.  Generally for uploads we would only support a specific class here
				// For example SystemListItem -> SystemList, we need to use SystemListEntity class
				if (!StringUtils.isEmpty(column.getUploadValueClassName())) {
					Column fkColumn = new Column();
					// Copy Properties so we keep column info, i.e. enum class names, etc.
					BeanUtils.copyProperties(column, fkColumn);
					fkColumn.setBeanPropertyName(columnBeanPropertyName);
					fkColumn.setName(getColumnNameFromBeanFieldName(fkColumn.getBeanPropertyName()));
					fkColumn.setDescription(getColumnDescription(column, tableColumns));
					fkColumn.setRequired(false);

					if (!config.getCheckEmptyObjectMap().containsKey(fkColumn.getBeanPropertyName())) {
						config.addBeanPropertyToCheckEmptyObjectSet(fkColumn.getBeanPropertyName(), ClassUtils.newInstance(CoreClassUtils.getClass(fkColumn.getUploadValueClassName())));
					}
					columnList.add(fkColumn);
				}
				if (config.isNaturalKey(column)) {
					config.addTableNaturalKeyBeanField(tableName, column.getBeanPropertyName(), false);
					// If at any point we run into a Natural Key field that isn't required, from that point on, pass false as required.
					if (required && !column.getColumnRequired()) {
						required = false;
					}
				}
				if (config.isNaturalKey(column) || (column.getColumnRequired() && updatable) || forceInclude) {
					// If Self-Referencing Natural Key... we are going only max levels deep and do not require the parent field
					if (tableName.equals(column.getFkTable())) {
						columnList.addAll(getTableColumns(config, columnBeanPropertyName, column.getFkTable(), false, ++levelsDeep, insertMissingFKBeans));
					}
					else {
						columnList.addAll(getTableColumns(config, columnBeanPropertyName, column.getFkTable(), (config.isNaturalKey(column) && required),
								insertMissingFKBeans));
					}
				}
			}
			else {
				if (isSkipColumn(column)) {
					continue;
				}
				if (!config.isNaturalKey(column) && BooleanUtils.isTrue(column.getIgnoreUpload()) && !forceInclude) {
					continue;
				}

				Column fkColumn = new Column();
				// Copy Properties so we keep column info, i.e. enum class names, etc.
				BeanUtils.copyProperties(column, fkColumn);
				fkColumn.setBeanPropertyName(columnBeanPropertyName);
				fkColumn.setName(getColumnNameFromBeanFieldName(fkColumn.getBeanPropertyName()));
				fkColumn.setDescription(getColumnDescription(column, tableColumns));

				//fkColumn.setDataType(column.getDataType());
				if (config.isNaturalKey(column)) {
					config.addTableNaturalKeyBeanField(tableName, column.getBeanPropertyName(), false);
					// Even if it's a natural key for a field that is required, the
					// total natural key can have null values within it, so don't require it
					fkColumn.setRequired(required ? column.getColumnRequired() : required);
					fkColumn.setNaturalKey(true);
				}
				else {
					fkColumn.setRequired(false);
				}
				columnList.add(fkColumn);
			}
		}
		if (CollectionUtils.isEmpty(columnList) && required) {
			throw new ValidationException("Error performing Upload. Required FK field lookup table [" + tableName + "] either has no natural key defined or all natural keys have been excluded.");
		}
		// Put Required Columns First
		columnList = BeanUtils.sortWithFunction(columnList, Column::getRequired, false);
		return columnList;
	}


	private String getColumnNameFromBeanFieldName(String beanFieldName) {
		String[] beanFields = beanFieldName.split("\\.");
		StringBuilder columnName = new StringBuilder();
		for (int i = 0; i < beanFields.length; i++) {
			columnName.append(StringUtils.capitalize(beanFields[i]));
			if (i < beanFields.length - 1) {
				columnName.append("-");
			}
		}
		return columnName.toString();
	}


	private String getColumnDescription(Column column, List<SystemColumnStandard> tableColumns) {
		for (SystemColumnStandard s : CollectionUtils.getIterable(tableColumns)) {
			// NOTE: CANNOT MATCH ON NAME BECAUSE FOR UPLOAD PURPOSES WITH FK REFERENCES THE NAME IS UPDATED
			// FOR UPLOADS SO WE KNOW THE FULL REFERENCE PATH
			if (column.getBeanPropertyName().equals(s.getBeanPropertyName())) {
				// Need to verify Column Name vs. Description here, because Column Name populated on the DataColumn
				// is the new bean name, so it's always coming up as different
				// Also, need to use label, not name because the name has no spaces, but description does.  Default
				// logic sets the label and the description the same
				if (!StringUtils.isEqual(s.getLabel(), s.getDescription())) {
					return s.getDescription();
				}
				return null;
			}
		}
		return null;
	}


	/**
	 * Returns true if the {@link Column} should be skipped for uploads.
	 * Primary Key ID fields and system managed fields are skipped.
	 */
	private boolean isSkipColumn(Column column) {
		// Skip PK id fields
		if ("id".equals(column.getBeanPropertyName())) {
			return true;
		}
		// Skip System Managed Fields
		if (BeanUtils.isSystemManagedField(column.getBeanPropertyName())) {
			return true;
		}

		return false;
	}


	private void populateColumnCustomList(SystemUploadConfig config) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		if (!StringUtils.isEmpty(uploadCommand.getColumnGroupName())) {
			SystemColumnGroup group = getSystemColumnService().getSystemColumnGroupByName(uploadCommand.getColumnGroupName());
			config.setColumnGroup(group);
			SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
			searchForm.setColumnGroupId(group.getId());
			if (!StringUtils.isEmpty(uploadCommand.getLinkedValue())) {
				searchForm.setLinkedValue(uploadCommand.getLinkedValue());
				searchForm.setIncludeNullLinkedValue(true);
			}
			config.setCustomColumnList(getSystemColumnService().getSystemColumnCustomList(searchForm));
		}
	}


	private void populateLinkedEntityColumnList(SystemUploadConfig config) {
		config.setLinkedEntityIdBeanProperty(config.getUploadCommand().getLinkedEntityIdBeanProperty());
		config.setLinkedEntityColumnList(getLinkedEntityColumns(config));
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public int getMaxLevelsDeep() {
		return this.maxLevelsDeep;
	}


	public void setMaxLevelsDeep(int maxLevelsDeep) {
		this.maxLevelsDeep = maxLevelsDeep;
	}
}
