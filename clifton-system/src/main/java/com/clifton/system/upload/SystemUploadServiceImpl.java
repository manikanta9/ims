package com.clifton.system.upload;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.string.StringToBigDecimalConverter;
import com.clifton.core.converter.string.StringToDateConverter;
import com.clifton.core.converter.string.StringToIntegerConverter;
import com.clifton.core.converter.string.StringToShortConverter;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.MigrationSchemaService;
import com.clifton.core.dataaccess.migrate.schema.search.ColumnSearchForm;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.mvc.WebMethodHandler;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.system.upload.config.SystemUploadConfig;
import com.clifton.system.upload.config.SystemUploadConfigPopulator;
import com.clifton.system.upload.converter.SystemUploadSampleDataTableConverter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemUploadServiceImpl</code> provides a basic implementation of the {@link SystemUploadService}
 * interface.
 *
 * @author manderson
 */
@Service
public class SystemUploadServiceImpl implements SystemUploadService {

	private MigrationSchemaService migrationSchemaService;

	private SystemColumnValueHandler systemColumnValueHandler;

	private SystemUploadConfigPopulator systemUploadConfigPopulator;
	private SystemUploadHandler systemUploadHandler;

	private int maxSampleUploadRows = 10;

	/**
	 * If someone clicks All Rows, still need to limit results in case the table itself (i.e. MarketDataValue) has too much rows of data
	 */
	private int allRowsMaxSampleUploadRows = 500;

	private WebMethodHandler webMethodHandler;
	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	////////          SystemUploadCommand Business Methods           ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Securing Read Access here because some sensitive tables could be available for uploads, so want to make sure user has at least read access to download
	 * samples
	 */
	@Override
	public DataTable downloadSystemUploadFileSample(SystemUploadCommand uploadCommand, final boolean allRows) {
		return downloadSystemUploadFileSampleWithAdditionalFilters(uploadCommand, allRows, null);
	}


	@Override
	public DataTable downloadSystemUploadFileSampleWithAdditionalFilters(SystemUploadCommand uploadCommand, final boolean allRows, Map<String, Object> additionalFilterMap) {
		SystemUploadConfig config = getSystemUploadConfigPopulator().populateSystemUploadConfig(uploadCommand);
		if (!CollectionUtils.isEmpty(config.getCheckEmptyObjectMap())) {
			config.getColumnList().removeIf(column -> config.getCheckEmptyObjectMap().containsKey(column.getBeanPropertyName()));
		}
		final AdvancedReadOnlyDAO<IdentityObject, Criteria> dao = (AdvancedReadOnlyDAO<IdentityObject, Criteria>) config.getTableDAOMap().get(uploadCommand.getTableName());

		if (additionalFilterMap == null) {
			additionalFilterMap = new HashMap<>();
		}

		// If filtering by custom column group with linked value filter - add it to the additionalFilterMap
		if (config.getColumnGroup() != null && !StringUtils.isEmpty(uploadCommand.getLinkedValue())) {
			String propertyName = config.getColumnGroup().getLinkedBeanProperty();
			Class<?> clazz = BeanUtils.getPropertyType(dao.getConfiguration().getBeanClass(), propertyName);
			Object linked = getLinkedValueAsObject(clazz, uploadCommand.getLinkedValue());
			additionalFilterMap.put(propertyName, linked);
		}

		List<IdentityObject> beanList = getBeanListImpl(dao, additionalFilterMap, allRows);

		if (config.getColumnGroup() != null) {
			for (IdentityObject bean : CollectionUtils.getIterable(beanList)) {
				SystemColumnCustomValueAware awareBean = (SystemColumnCustomValueAware) bean;
				awareBean.setColumnGroupName(config.getColumnGroup().getName());
				awareBean.setColumnValueList(getSystemColumnValueHandler().getSystemColumnValueListForEntity(awareBean));
			}
		}

		SystemUploadSampleDataTableConverter converter = new SystemUploadSampleDataTableConverter(config, getWebMethodHandler(), getDaoLocator());
		return converter.convert(beanList);
	}


	private List<IdentityObject> getBeanListImpl(AdvancedReadOnlyDAO<IdentityObject, Criteria> dao, final Map<String, Object> additionalFilterMap, final boolean allRows) {
		return dao.findBySearchCriteria(new HibernateSearchFormConfigurer(new AuditableEntitySearchForm()) {

			@Override
			public void configureCriteria(Criteria criteria) {
				// NOTE: depending on table size, max restriction may not help as much if there is order by clause
				// it's nice to have order by for queries that return relatively small number of rows
				// but for something like MarketDataValue which has millions of rows, it may take a long time
				criteria.setMaxResults(allRows ? getAllRowsMaxSampleUploadRows() : getMaxSampleUploadRows());

				for (Map.Entry<String, Object> stringObjectEntry : additionalFilterMap.entrySet()) {
					Object propertyValue = stringObjectEntry.getValue();

					String pathAlias = "";

					String propertyName = stringObjectEntry.getKey();
					int index = (stringObjectEntry.getKey()).lastIndexOf('.');
					if (index != -1) {
						pathAlias = getPathAlias((stringObjectEntry.getKey()).substring(0, index), criteria) + ".";
						propertyName = propertyName.substring(index + 1);
					}

					criteria.add(Restrictions.eq(pathAlias + propertyName, propertyValue));
				}
			}
		});
	}


	private Object getLinkedValueAsObject(Class<?> clazz, String linkedValue) {

		if (linkedValue == null) {
			return null;
		}

		if (BigDecimal.class.isAssignableFrom(clazz)) {
			return new StringToBigDecimalConverter().convert(linkedValue);
		}
		if (Integer.class.isAssignableFrom(clazz) || clazz == int.class) {
			return new StringToIntegerConverter().convert(linkedValue);
		}
		if (Short.class.isAssignableFrom(clazz) || clazz == short.class) {
			return new StringToShortConverter().convert(linkedValue);
		}
		if (Date.class.isAssignableFrom(clazz)) {
			return new StringToDateConverter().convert(linkedValue);
		}
		return linkedValue;
	}


	@Override
	public void uploadSystemUploadFileSheets(SystemUploadCommand uploadCommand, Map<String, String> sheetNameToTableNameMap) {
		if (uploadCommand.getFile() == null && uploadCommand.getWorkbook() == null) {
			throw new FieldValidationException("File or Workbook is required to process an upload.", "file");
		}
		if (uploadCommand.getWorkbook() == null) {
			uploadCommand.setWorkbook(ExcelFileUtils.createWorkbook(uploadCommand.getFile()));
		}
		Workbook workbook = uploadCommand.getWorkbook();
		for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			Sheet sheet = workbook.getSheetAt(i);
			if (sheet != null) {
				String sheetName = sheet.getSheetName();
				for (Map.Entry<String, String> sheetNameTableEntry : sheetNameToTableNameMap.entrySet()) {
					if (sheetNameTableEntry.getKey().equals(sheetName)) {
						uploadCommand.setTableName(sheetNameTableEntry.getValue());
						uploadCommand.setSheetIndex(i);
						try {
							uploadSystemUploadFile(uploadCommand);
						}
						catch (Exception e) {
							throw new RuntimeException("Error uploading data from sheet [" + sheetName + "]: " + e.getMessage(), e);
						}
						if (uploadCommand.getUploadResult().isError()) {
							throw new RuntimeException("Error uploading data from sheet [" + sheetName + "]: " + uploadCommand.getUploadResultsString());
						}
					}
				}
			}
		}
		uploadCommand.setWorkbook(null);
	}


	@Override
	public void uploadSystemUploadFile(SystemUploadCommand uploadCommand) {
		getSystemUploadHandler().uploadSystemUploadFile(uploadCommand);
	}


	@Override
	public String getSystemUploadTableDefaultNaturalKey(String tableName) {
		ValidationUtils.assertNotNull(tableName, "Table Name is Required to Lookup Natural Keys");
		ColumnSearchForm searchForm = new ColumnSearchForm();
		searchForm.setTableName(tableName);
		searchForm.setNaturalKey(true);
		Collection<Column> columnList = getMigrationSchemaService().getColumnList(searchForm);
		if (CollectionUtils.isEmpty(columnList)) {
			return "None";
		}
		StringBuilder sb = new StringBuilder(16);
		for (Column column : columnList) {
			sb.append(column.getName());
			sb.append(", ");
		}
		return sb.substring(0, sb.length() - 2);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public int getMaxSampleUploadRows() {
		return this.maxSampleUploadRows;
	}


	public void setMaxSampleUploadRows(int maxSampleUploadRows) {
		this.maxSampleUploadRows = maxSampleUploadRows;
	}


	public int getAllRowsMaxSampleUploadRows() {
		return this.allRowsMaxSampleUploadRows;
	}


	public void setAllRowsMaxSampleUploadRows(int allRowsMaxSampleUploadRows) {
		this.allRowsMaxSampleUploadRows = allRowsMaxSampleUploadRows;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public SystemUploadConfigPopulator getSystemUploadConfigPopulator() {
		return this.systemUploadConfigPopulator;
	}


	public void setSystemUploadConfigPopulator(SystemUploadConfigPopulator systemUploadConfigPopulator) {
		this.systemUploadConfigPopulator = systemUploadConfigPopulator;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}


	public WebMethodHandler getWebMethodHandler() {
		return this.webMethodHandler;
	}


	public void setWebMethodHandler(WebMethodHandler webMethodHandler) {
		this.webMethodHandler = webMethodHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public MigrationSchemaService getMigrationSchemaService() {
		return this.migrationSchemaService;
	}


	public void setMigrationSchemaService(MigrationSchemaService migrationSchemaService) {
		this.migrationSchemaService = migrationSchemaService;
	}
}
