package com.clifton.system.upload.config;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Subclass;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnGroup;
import com.clifton.system.upload.SystemUploadCommand;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemUploadConfig</code> contains the Upload configuration
 * for a specific table.  This information is populated by the {@link SystemUploadConfigPopulatorImpl} based
 * on migration xml data.
 *
 * @author manderson
 */
@NonPersistentObject
public class SystemUploadConfig {

	public static final String CUSTOM_COMBO_LOOKUP_TEXT_COLUMN_NAME_SUFFIX = "_TEXT";

	private final SystemUploadCommand uploadCommand;

	private List<Column> columnList;

	private SystemColumnGroup columnGroup;
	private List<SystemColumnCustom> customColumnList;

	private String linkedEntityIdBeanProperty;
	private List<Column> linkedEntityColumnList;

	private final Map<Class<?>, String> classTableMap = new HashMap<>();
	private final Map<String, ReadOnlyDAO<IdentityObject>> tableDAOMap = new HashMap<>();
	private final Map<String, Object> tableSaveServiceBeanMap = new HashMap<>();
	private final Map<String, Method> tableSaveMethodMap = new HashMap<>();
	private final Map<String, String[]> tableNaturalKeyBeanFields = new HashMap<>();

	// When Special Converters are used, i.e. Trade Upload uses B/S instead of true/false for IsBuy column
	private Map<String, ReversableConverter<Object, Object>> columnConverterMap;

	/**
	 * When we use a converter for a FK Field (i.e. SystemListItem -> SystemList) in order to instantiate the class properly
	 * it's possible the the object is not actually used.  Use this to check object matches new instance then set it to null
	 * prevents transient  hibernate errors.  Key = bean property name, value = new instance
	 */
	private final Map<String, Object> checkEmptyObjectMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemUploadConfig(SystemUploadCommand uploadCommand) {
		this.uploadCommand = uploadCommand;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SystemUploadConfig)) {
			return false;
		}
		return CompareUtils.isEqual(getTableName(), ((SystemUploadConfig) obj).getTableName());
	}


	@Override
	public int hashCode() {
		return (getTableName() == null) ? 0 : getTableName().hashCode();
	}


	public String getTableName() {
		return getUploadCommand().getTableName();
	}


	public ReadOnlyDAO<IdentityObject> getDao() {
		return getTableDAOMap().get(getTableName());
	}


	public DAOConfiguration<IdentityObject> getDaoConfiguration() {
		return getDao().getConfiguration();
	}


	public void addTableDAO(String table, ReadOnlyDAO<IdentityObject> dao) {
		this.tableDAOMap.put(table, dao);
		this.classTableMap.put(dao.getConfiguration().getBeanClass(), table);

		// Map all of the subclasses so we can find correct DAO
		if (!CollectionUtils.isEmpty(dao.getConfiguration().getTable().getSubclassList())) {
			for (Subclass subclass : dao.getConfiguration().getTable().getSubclassList()) {
				this.classTableMap.put(CoreClassUtils.getClass(subclass.getDtoClass()), table);
			}
		}
	}


	public void addTableSaveMethod(String table, Object bean, Method method) {
		this.tableSaveServiceBeanMap.put(table, bean);
		this.tableSaveMethodMap.put(table, method);
	}


	public boolean isNaturalKey(Column column) {
		if (StringUtils.isEqual(getTableName(), column.getTableName())) {
			if (!ArrayUtils.isEmpty(getUploadCommand().getOverrideTableNaturalKeyBeanProperties())) {
				return ArrayUtils.contains(getUploadCommand().getOverrideTableNaturalKeyBeanProperties(), column.getBeanPropertyName());
			}
		}
		return column.isNaturalKey();
	}


	public void addTableNaturalKeyBeanField(String table, String beanField, boolean override) {
		// If adding for the upload table and there are overrides and we aren't actually adding the overrides - do nothing
		if (!override && StringUtils.isEqual(table, getUploadCommand().getTableName()) && !ArrayUtils.isEmpty(getUploadCommand().getOverrideTableNaturalKeyBeanProperties())) {
			return;
		}
		String[] beanFields = this.tableNaturalKeyBeanFields.get(table);
		if (beanFields == null || beanFields.length == 0) {
			beanFields = new String[]{beanField};
		}

		else {
			// Only add the beanField if it doesn't already exist
			boolean found = false;
			for (String str : beanFields) {
				if (beanField.equals(str)) {
					found = true;
					break;
				}
			}
			if (!found) {
				beanFields = ArrayUtils.add(beanFields, beanField);
			}
		}
		this.tableNaturalKeyBeanFields.put(table, beanFields);
	}


	public void addColumnValueConverter(String converterName, ReversableConverter<Object, Object> converter) {
		if (this.columnConverterMap == null) {
			this.columnConverterMap = new HashMap<>();
		}
		this.columnConverterMap.put(converterName, converter);
	}


	public ReversableConverter<Object, Object> getColumnValueConverter(Column column) {
		if (!StringUtils.isEmpty(column.getUploadValueConverterName()) && !CollectionUtils.isEmpty(this.columnConverterMap)) {
			return this.columnConverterMap.get(column.getUploadValueConverterName());
		}
		return null;
	}


	public void addBeanPropertyToCheckEmptyObjectSet(String beanPropertyName, Object newInstance) {
		this.checkEmptyObjectMap.putIfAbsent(beanPropertyName, newInstance);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemUploadCommand getUploadCommand() {
		return this.uploadCommand;
	}


	public List<Column> getColumnList() {
		return this.columnList;
	}


	public void setColumnList(List<Column> columnList) {
		this.columnList = columnList;
	}


	public SystemColumnGroup getColumnGroup() {
		return this.columnGroup;
	}


	public void setColumnGroup(SystemColumnGroup columnGroup) {
		this.columnGroup = columnGroup;
	}


	public List<SystemColumnCustom> getCustomColumnList() {
		return this.customColumnList;
	}


	public void setCustomColumnList(List<SystemColumnCustom> customColumnList) {
		this.customColumnList = customColumnList;
	}


	public String getLinkedEntityIdBeanProperty() {
		return this.linkedEntityIdBeanProperty;
	}


	public void setLinkedEntityIdBeanProperty(String linkedEntityIdBeanProperty) {
		this.linkedEntityIdBeanProperty = linkedEntityIdBeanProperty;
	}


	public List<Column> getLinkedEntityColumnList() {
		return this.linkedEntityColumnList;
	}


	public void setLinkedEntityColumnList(List<Column> linkedEntityColumnList) {
		this.linkedEntityColumnList = linkedEntityColumnList;
	}


	public Map<Class<?>, String> getClassTableMap() {
		return this.classTableMap;
	}


	public Map<String, ReadOnlyDAO<IdentityObject>> getTableDAOMap() {
		return this.tableDAOMap;
	}


	public Map<String, Object> getTableSaveServiceBeanMap() {
		return this.tableSaveServiceBeanMap;
	}


	public Map<String, Method> getTableSaveMethodMap() {
		return this.tableSaveMethodMap;
	}


	public Map<String, String[]> getTableNaturalKeyBeanFields() {
		return this.tableNaturalKeyBeanFields;
	}


	public Map<String, Object> getCheckEmptyObjectMap() {
		return this.checkEmptyObjectMap;
	}
}
