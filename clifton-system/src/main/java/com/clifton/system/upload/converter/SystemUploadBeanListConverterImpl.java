package com.clifton.system.upload.converter;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.ExcelFileToDataTableConverter;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.upload.SystemUploadCommand;
import com.clifton.system.upload.config.SystemUploadConfig;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemUploadBeanListConverterImpl</code> ...
 *
 * @author manderson
 */
@Component
public class SystemUploadBeanListConverterImpl<T extends IdentityObject> implements SystemUploadBeanListConverter<T> {

	private DaoLocator daoLocator;


	@Override
	public List<T> convertSystemUploadToBeanList(SystemUploadConfig config) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		// First: Read Excel file or pre existing Workbook and convert it into a DataTable
		ExcelFileToDataTableConverter excelConverter = new ExcelFileToDataTableConverter();
		excelConverter.setSheetIndex(uploadCommand.getSheetIndex());
		DataTable dataTable = excelConverter.convert((uploadCommand.getWorkbook() != null ? uploadCommand.getWorkbook() : uploadCommand.getFile()));

		// Setup Column Maps Based on DataTable and Configuration
		Map<String, Integer> columnIndexMap = new HashMap<>();
		Map<String, Integer> unmappedColumnIndexMap = new HashMap<>();
		setupColumnIndexMaps(config, dataTable, columnIndexMap, unmappedColumnIndexMap);

		List<T> beanList = new ArrayList<>();
		for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
			List<T> rowBeanList = new ArrayList<>();
			try {
				rowBeanList = getBeanListForRow(config, i, dataTable.getRow(i), columnIndexMap, unmappedColumnIndexMap);
			}
			catch (Throwable e) {
				uploadCommand.getUploadResult().addBeanError(i, (StringUtils.isEmpty(e.getMessage()) ? ExceptionUtils.getOriginalMessage(e) : e.getMessage()));
				LogUtils.errorOrInfo(LogCommand.ofThrowableAndMessage(getClass(), e, () -> (StringUtils.isEmpty(e.getMessage()) ? ExceptionUtils.getOriginalMessage(e) : e.getMessage())));
			}
			// If no error found for that row - add it to the list
			if (!uploadCommand.getUploadResult().isRowError(i) && !CollectionUtils.isEmpty(rowBeanList)) {
				beanList.addAll(rowBeanList);
			}
		}
		return beanList;
	}


	private void setupColumnIndexMaps(SystemUploadConfig config, DataTable dataTable, Map<String, Integer> columnIndexMap, Map<String, Integer> unmappedColumnIndexMap) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		DataColumn[] columnList = dataTable.getColumnList();
		for (int i = 0; i < columnList.length; i++) {
			DataColumn column = columnList[i];
			if (!StringUtils.isEmpty(column.getColumnName())) {
				// We check columns without case sensitivity, but when the column is found, we reset the name to what was expected to prevent issues with future retrievals from the map
				String columnName = column.getColumnName();

				boolean found = false;
				for (Column col : CollectionUtils.getIterable(config.getColumnList())) {
					if (StringUtils.isEqualIgnoreCase(column.getColumnName(), col.getName())) {
						columnName = col.getName();
						found = true;
						break;
					}
				}
				if (!found) {
					for (SystemColumnCustom col : CollectionUtils.getIterable(config.getCustomColumnList())) {
						if (StringUtils.isEqualIgnoreCase(column.getColumnName(), col.getName())) {
							columnName = col.getName();
							found = true;
							break;
						}
						if (StringUtils.isEqualIgnoreCase(column.getColumnName(), col.getName() + SystemUploadConfig.CUSTOM_COMBO_LOOKUP_TEXT_COLUMN_NAME_SUFFIX)) {
							columnName = col.getName() + SystemUploadConfig.CUSTOM_COMBO_LOOKUP_TEXT_COLUMN_NAME_SUFFIX;
							found = true;
							break;
						}
					}
				}
				if (!found) {
					for (Column col : CollectionUtils.getIterable(config.getLinkedEntityColumnList())) {
						if (StringUtils.isEqualIgnoreCase(column.getColumnName(), col.getName())) {
							columnName = col.getName();
							found = true;
							break;
						}
					}
				}

				columnIndexMap.put(columnName, i);

				if (!found) {
					unmappedColumnIndexMap.put(columnName, i);
				}
			}
		}

		// Verify All Required Fields are Present UNLESS upload is to update existing beans then beans can be partially updated
		List<String> missingRequiredFields = new ArrayList<>();
		for (Column column : config.getColumnList()) {
			if (!columnIndexMap.containsKey(column.getName())) {
				if (config.getCheckEmptyObjectMap().containsKey(column.getBeanPropertyName())) {
					continue;
				}
				if (FileUploadExistingBeanActions.UPDATE == uploadCommand.getExistingBeans()) {
					if (config.isNaturalKey(column) && column.getBeanPropertyName().contains(".")) {
						String skipBeanPropertyName = column.getBeanPropertyName().substring(0, column.getBeanPropertyName().lastIndexOf("."));
						uploadCommand.getUploadResult().addSkipBeanProperty(skipBeanPropertyName);

						// See if we need to skip an additional level up - this happens whens the natural key of a table is comprised ONLY of FKs
						int levelsDeep = CollectionUtils.getSize(StringUtils.delimitedStringToList(column.getBeanPropertyName(), "\\."));
						// Only applies if we are more than 2 levels deep - at 2 levels we are already skipping level 1
						if (levelsDeep > 2) {
							String parentSkipBeanPropertyName = skipBeanPropertyName.substring(0, skipBeanPropertyName.lastIndexOf("."));
							// To check we see if there is a column defined at one level higher than this one.  If there isn't add it to skip
							if (CollectionUtils.isEmpty(BeanUtils.filter(config.getColumnList(), col -> col.getBeanPropertyName().startsWith(parentSkipBeanPropertyName) && CollectionUtils.getSize(StringUtils.delimitedStringToList(col.getBeanPropertyName(), "\\.")) == (levelsDeep - 1)))) {
								uploadCommand.getUploadResult().addSkipBeanProperty(parentSkipBeanPropertyName);
							}
						}
					}
					else if (!column.getBeanPropertyName().contains(".")) {
						uploadCommand.getUploadResult().addSkipBeanProperty(column.getBeanPropertyName());
					}
				}
				else if (column.getColumnRequiredForValidation()) {
					missingRequiredFields.add(column.getName());
				}
			}
		}
		if (!CollectionUtils.isEmpty(missingRequiredFields) && !uploadCommand.isSimple()) {
			throw new ValidationException("Upload File is missing required columns [" + StringUtils.collectionToCommaDelimitedString(missingRequiredFields) + "]");
		}
	}


	@SuppressWarnings("unchecked")
	private List<T> getBeanListForRow(SystemUploadConfig config, int rowIndex, DataRow row, Map<String, Integer> columnIndexMap, Map<String, Integer> unmappedColumnIndexMap) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		T bean = (T) BeanUtils.newInstance(config.getDaoConfiguration().getBeanClass());
		Map<String, Object> unmappedPropertyMap = new HashMap<>();

		bean = populateBeanStandardColumns(bean, config, rowIndex, row, columnIndexMap);
		populateSystemColumnValueObjects(bean, config, row);
		bean = populateBeanLinkedEntityId(bean, config, rowIndex, row, columnIndexMap);

		for (String colName : unmappedColumnIndexMap.keySet()) {
			Object value = row.getValue(colName);
			if (value != null || uploadCommand.isCaptureNullUnmappedValues()) {
				unmappedPropertyMap.put(colName, value);
			}
		}

		uploadCommand.applyUploadSpecificProperties(bean, row);
		return populateBeans(bean, unmappedPropertyMap);
	}


	/**
	 * Can be used for additional functionality, or to convert one row into multiple records
	 * For example, See MarketDataValueMultipleConverter that sets unmapped column name as new data fields in additional records
	 */
	protected List<T> populateBeans(T bean, @SuppressWarnings("unused") Map<String, Object> unmappedPropertyMap) {
		return CollectionUtils.createList(bean);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private T populateBeanStandardColumns(T bean, SystemUploadConfig config, int rowIndex, DataRow row, Map<String, Integer> columnIndexMap) {
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		// This needs to be sorted in order so that the upload value class name is processed before trying to set properties
		for (Column column : CollectionUtils.getIterable(BeanUtils.sortWithFunction(config.getColumnList(), Column::getName, true))) {
			Integer index = columnIndexMap.get(column.getName());

			if (index == null) {
				// FK Entity that we can use to instantiate a new class
				if (column.getUploadValueClassName() != null && !StringUtils.isEmpty(column.getFkTable())) {
					BeanUtils.setPropertyValue(bean, column.getBeanPropertyName(), ClassUtils.newInstance(CoreClassUtils.getClass(column.getUploadValueClassName())));
				}
				continue;
			}
			Object value = row.getValue(index);
			// Consider empty String to be null value
			if (value instanceof String && StringUtils.isEmpty((String) value)) {
				value = null;
			}
			if (config.getColumnValueConverter(column) != null) {
				value = config.getColumnValueConverter(column).convert(value);
			}
			if (value == null && column.getColumnRequired() && !uploadCommand.isSimple()) {
				if (!"FK Field Lookup".equals(column.getDescription()) || ("FK Field Lookup".equals(column.getDescription()) && uploadCommand.isInsertMissingFKBeans())) {
					uploadCommand.getUploadResult().addRequiredFieldError(rowIndex, column.getName());
				}
			}
			else if (value != null) {
				value = convertValueToColumnDataType(value, column);
				BeanUtils.setPropertyValue(bean, column.getBeanPropertyName(), value);
			}
		}
		return bean;
	}


	private T populateBeanLinkedEntityId(T bean, SystemUploadConfig config, int rowIndex, DataRow row, Map<String, Integer> columnIndexMap) {
		if (CollectionUtils.isEmpty(config.getLinkedEntityColumnList())) {
			return bean;
		}
		SystemUploadCommand uploadCommand = config.getUploadCommand();
		List<String> linkedEntityBeanPropertyFieldList = new ArrayList<>();
		List<Object> linkedEntityBeanPropertyValueList = new ArrayList<>();
		for (Column column : CollectionUtils.getIterable(config.getLinkedEntityColumnList())) {
			Integer index = columnIndexMap.get(column.getName());
			if (index == null) {
				continue;
			}
			Object value = row.getValue(index);
			// Consider empty String to be null value
			if (value instanceof String && StringUtils.isEmpty((String) value)) {
				value = null;
			}
			if (config.getColumnValueConverter(column) != null) {
				value = config.getColumnValueConverter(column).convert(value);
			}
			if (value == null && column.getColumnRequired() && !uploadCommand.isSimple()) {
				if (!"FK Field Lookup".equals(column.getDescription()) || ("FK Field Lookup".equals(column.getDescription()) && uploadCommand.isInsertMissingFKBeans())) {
					uploadCommand.getUploadResult().addRequiredFieldError(rowIndex, column.getName());
				}
			}
			else if (value != null) {
				value = convertValueToColumnDataType(value, column);
			}
			linkedEntityBeanPropertyFieldList.add(StringUtils.substringAfterFirst(column.getBeanPropertyName(), "."));
			linkedEntityBeanPropertyValueList.add(value);
		}
		ReadOnlyDAO<IdentityObject> linkedEntityDao = getDaoLocator().locate(uploadCommand.getLinkedEntityTableName());
		String[] linkedEntityBeanPropertyFieldArray = linkedEntityBeanPropertyFieldList.toArray(new String[0]);
		Object[] linkedEntityBeanPropertyValueArray = linkedEntityBeanPropertyValueList.toArray();
		IdentityObject linkedEntity = uploadCommand.getLinkedEntityMap().computeIfAbsent(ArrayUtils.toString(linkedEntityBeanPropertyValueList),
				linkedEntityObject -> linkedEntityDao.findOneByFields(linkedEntityBeanPropertyFieldArray, linkedEntityBeanPropertyValueArray));
		ValidationUtils.assertNotNull(linkedEntity, String.format("Unable to find Linked Entity using properties [%s] for [%s].", formatFieldsWithValues(linkedEntityBeanPropertyFieldArray, linkedEntityBeanPropertyValueArray), bean.toString()));
		Integer linkedEntityId = (Integer) linkedEntity.getIdentity();
		BeanUtils.setPropertyValue(bean, config.getLinkedEntityIdBeanProperty(), linkedEntityId);
		if (!StringUtils.isEmpty(uploadCommand.getLinkedEntityIdBeanProperty())) {
			BeanUtils.setPropertyValue(bean, uploadCommand.getLinkedEntityLabelBeanProperty(), BeanUtils.getLabel(linkedEntity));
		}
		return bean;
	}


	private String formatFieldsWithValues(String[] fieldNameArray, Object[] valueArray) {
		StringBuilder resultSb = new StringBuilder();
		for (int i = 0; i < ArrayUtils.getLength(fieldNameArray); i++) {
			resultSb.append(fieldNameArray[i]).append(": ").append(valueArray[i]);
			resultSb.append(i < ArrayUtils.getLength(fieldNameArray) - 1 ? ", " : StringUtils.EMPTY_STRING);
		}
		return resultSb.toString();
	}


	private Object convertValueToColumnDataType(Object value, Column column) {
		if (value instanceof String && DataTypes.INT == column.getDataType()) {
			value = new Double((String) value).intValue();
		}
		//TODO - need a case for COMPRESSED TEXT?
		if (value instanceof String && DataTypes.ENUM == column.getDataType()) {
			try {
				//Needed to adjust this statement for Java 8.
				@SuppressWarnings("rawtypes")
				Class typeDefinitionClass = Class.forName(column.getTypeDefinitionClassName());//.asSubclass(Enum.class);
				value = Enum.valueOf(typeDefinitionClass, (String) value);
				//	value = Enum.valueOf(Class.forName(column.getEnumClassName()).asSubclass(Enum.class), (String) value);
			}
			catch (Exception e) {
				throw new RuntimeException("Unable to set Enum value for column [" + column.getName() + "] with value [" + value + "]", e);
			}
		}
		if (value instanceof Date && DataTypes.TIME == column.getDataType()) {
			value = new Time((Date) value);
		}
		if (value instanceof String && DataTypes.TIME == column.getDataType()) {
			value = Time.parse((String) value);
		}
		if (value instanceof BigDecimal && DataTypeNames.STRING == column.getDataType().getTypeName()) {
			value = ((BigDecimal) value).toPlainString();
		}
		return value;
	}


	/**
	 * The following just copies the values over to the {@link SystemColumnValue} objects
	 * All combo/{@link com.clifton.system.list.SystemListItem} look ups are performed later in the {@link com.clifton.system.upload.SystemUploadService}
	 */
	private void populateSystemColumnValueObjects(T bean, SystemUploadConfig config, DataRow row) {
		if (config.getColumnGroup() != null) {

			List<SystemColumnValue> valueList = new ArrayList<>();
			for (SystemColumnCustom custom : config.getCustomColumnList()) {
				Object value = row.getValue(custom.getName());
				Object text = row.getValue(custom.getName() + SystemUploadConfig.CUSTOM_COMBO_LOOKUP_TEXT_COLUMN_NAME_SUFFIX);
				if (value != null || text != null) {
					SystemColumnValue valueObject = new SystemColumnValue();
					valueObject.setColumn(custom);
					valueObject.setValue(new ObjectToStringConverter().convert(value));
					valueObject.setText(new ObjectToStringConverter().convert(text));
					valueList.add(valueObject);
				}
				if (!CollectionUtils.isEmpty(valueList)) {
					((SystemColumnCustomValueAware) bean).setColumnGroupName(config.getColumnGroup().getName());
					((SystemColumnCustomValueAware) bean).setColumnValueList(valueList);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
