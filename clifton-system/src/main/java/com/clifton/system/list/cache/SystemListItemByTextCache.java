package com.clifton.system.list.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.system.list.SystemListItem;
import org.springframework.stereotype.Component;


/**
 * @author vgomelsky
 */
@Component
public class SystemListItemByTextCache extends SelfRegisteringCompositeKeyDaoCache<SystemListItem, Integer, String> {


	@Override
	protected String getBeanKey1Property() {
		return "systemList.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "text";
	}


	@Override
	protected Integer getBeanKey1Value(SystemListItem bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getSystemList());
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(SystemListItem bean) {
		return bean.getText();
	}
}
