package com.clifton.system.list.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListQuery;
import com.clifton.system.schema.SystemDataTypeConverter;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemListItemValidator</code> contains all methods necessary to validate
 * a {@link SystemListItem} objects prior to adding/editing/deleting from the database.
 *
 * @author manderson
 */
@Component
public class SystemListItemValidator extends SelfRegisteringDaoValidator<SystemListItem> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemListItem bean, DaoEventTypes config) throws ValidationException {
		SystemList listBean = bean.getSystemList();
		if (listBean.isListSystemDefined()) {
			throw new ValidationException("Cannot edit list items for list [" + listBean.getName() + "] as it is marked as list system defined.");
		}

		if (config.isInsert() || config.isUpdate()) {
			if (listBean instanceof SystemListQuery) {
				throw new ValidationException("Cannot add specific items to a list that executes a query for the items.");
			}

			// Validate the Data Type
			validateSystemListItemDataType(bean);
		}
	}


	private void validateSystemListItemDataType(SystemListItem item) {
		try {
			SystemDataTypeConverter converter = new SystemDataTypeConverter(item.getSystemList().getDataType());
			converter.convert(item.getValue());
		}
		catch (Exception e) {
			throw new FieldValidationException("Selected Data Type for the List does not match all items.  There is an item in the list with value [" + item.getValue()
					+ "] that cannot be converted to type [" + item.getSystemList().getDataType().getName() + "].", "value");
		}
	}
}
