package com.clifton.system.list;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


/**
 * The <code>SystemList</code> class represents a list definition of items
 * that can be used in various parts of the system.
 *
 * @author vgomelsky
 */
@CacheByName
@JsonDeserialize(as = SystemListEntity.class)
public abstract class SystemList extends NamedEntity<Integer> implements SystemEntityModifyConditionAwareAdminRequired {

	/**
	 * This is the dataType of the list item's value field.
	 */
	private SystemDataType dataType;

	/**
	 * If true, then the List's name cannot be
	 * edited (nor the list deleted)
	 */
	private boolean nameSystemDefined;

	/**
	 * If true than the List items cannot be added/edited/deleted.
	 * For SQL Lists, than the SQL Query can't be edited.
	 */
	private boolean listSystemDefined;

	/**
	 * Optional condition to secure lists - if not set, user must be an admin to edit
	 */
	private SystemCondition entityModifyCondition;


	/**
	 * Overridden by child class to differentiate between a regular list and a calculated
	 * list (list items determined by executing SQL query)
	 */
	public abstract boolean isSqlList();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemDataType getDataType() {
		return this.dataType;
	}


	public void setDataType(SystemDataType dataType) {
		this.dataType = dataType;
	}


	public boolean isNameSystemDefined() {
		return this.nameSystemDefined;
	}


	public void setNameSystemDefined(boolean nameSystemDefined) {
		this.nameSystemDefined = nameSystemDefined;
	}


	public boolean isListSystemDefined() {
		return this.listSystemDefined;
	}


	public void setListSystemDefined(boolean listSystemDefined) {
		this.listSystemDefined = listSystemDefined;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
