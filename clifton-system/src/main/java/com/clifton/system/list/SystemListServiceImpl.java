package com.clifton.system.list;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.list.search.SystemListItemSearchForm;
import com.clifton.system.list.search.SystemListSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>SystemListServiceImpl</code> class provides basic implementation of {@link SystemListService} interface.
 *
 * @author vgomelsky
 */
@Service
public class SystemListServiceImpl<T extends SystemList> implements SystemListService<T> {

	private AdvancedUpdatableDAO<T, Criteria> systemListDAO;
	private AdvancedUpdatableDAO<SystemListItem, Criteria> systemListItemDAO;

	private DaoNamedEntityCache<T> systemListCache;
	private DaoCompositeKeyCache<SystemListItem, Integer, String> systemListItemByTextCache;
	private DaoCompositeKeyCache<SystemListItem, Integer, String> systemListItemByValueCache;

	private DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator;


	////////////////////////////////////////////////////////////////////////////
	///////////           System List Business Methods              ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public T getSystemList(int id) {
		return getSystemListDAO().findByPrimaryKey(id);
	}


	@Override
	public T getSystemListByName(String name) {
		return getSystemListCache().getBeanForKeyValue(getSystemListDAO(), name);
	}


	@Override
	public List<T> getSystemListList(SystemListSearchForm searchForm) {
		return getSystemListDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public void deleteSystemList(int id) {
		deleteSystemListItemListByList(id);
		getSystemListDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////         System List - List Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public void saveSystemListEntity(SystemListEntity bean) {
		getSystemListDAO().save((T) bean);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////         System List - Query Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public void saveSystemListQuery(SystemListQuery bean) {
		getSystemListDAO().save((T) bean);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////          SystemListItem Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemListItem getSystemListItem(int id) {
		return getSystemListItemDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemListItem getSystemListItemByListAndText(String listName, String text) {
		T list = getSystemListByName(listName);
		if (list == null) {
			throw new ValidationException("Cannot find system list with name " + listName);
		}
		return getSystemListItemByTextCache().getBeanForKeyValues(getSystemListItemDAO(), list.getId(), text);
	}


	@Override
	public SystemListItem getSystemListItemByListAndValue(String listName, String value) {
		T list = getSystemListByName(listName);
		if (list == null) {
			throw new ValidationException("Cannot find system list with name " + listName);
		}
		return getSystemListItemByValueCache().getBeanForKeyValues(getSystemListItemDAO(), list.getId(), value);
	}


	@Override
	public List<SystemListItem> getSystemListItemListByList(int systemListId) {
		SystemList listEntity = getSystemList(systemListId);
		return getSystemListItemListByListBean(listEntity);
	}


	@Override
	public List<SystemListItem> getSystemListItemList(SystemListItemSearchForm searchForm) {
		SystemList systemList;
		if (searchForm.getSystemListId() == null) {
			systemList = getSystemListByName(searchForm.getListName());
			if (systemList != null) {
				searchForm.setSystemListId(systemList.getId());
			}
		}
		else {
			systemList = getSystemList(searchForm.getSystemListId());
		}
		AssertUtils.assertNotNull(systemList, "There are no lists in the system with the name [%s].  Cannot retrieve item list.", searchForm.getListName());

		if (systemList instanceof SystemListQuery) {
			SystemListQuery queryList = (SystemListQuery) systemList;
			List<SystemListItem> itemList = getSystemListItemsForQuery(queryList, searchForm.getSearchPattern());
			if (CollectionUtils.isEmpty(itemList)) {
				return itemList;
			}
			// TODO: add proper support for paging for SQL based lists
			return new PagingArrayList<>(itemList, 0, itemList.size());
		}

		return getSystemListItemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<SystemListItem> getSystemListItemListByListBean(SystemList listEntity) {
		if (!(listEntity instanceof SystemListQuery)) {
			if (listEntity.isNewBean()) {
				return null; // New Bean with manually defined list items - there are none.
			}
			return getSystemListItemDAO().findByField("systemList.id", listEntity.getId());
		}

		return getSystemListItemsForQuery((SystemListQuery) listEntity, null);
	}


	private List<SystemListItem> getSystemListItemsForQuery(SystemListQuery queryList, String searchPattern) {
		String sql = queryList.getSqlStatement();
		SqlSelectCommand command = new SqlSelectCommand(sql);
		if (StringUtils.contains(sql, " = ?")) {
			// TODO: what is a cleaner way to determine if the SQL does or does not support search parameter (SQL or Java filter)?
			// add List Type (Static Items, SQL, SQL with Search, ...) similar to System Query Type?
			command.addStringParameterValue(searchPattern);
			searchPattern = null; // no need to search again
		}
		DataTable results = getDataTableRetrievalHandlerLocator().locate(queryList.getDataSourceForExecution().getName()).findDataTable(command);
		return convertDataTableToSystemListItemList(queryList, results, searchPattern);
	}


	private List<SystemListItem> convertDataTableToSystemListItemList(SystemList systemList, DataTable results, String searchPattern) {
		if (results == null || results.getTotalRowCount() == 0) {
			return null;
		}
		int columnCount = results.getColumnCount();
		if (columnCount < 2 || columnCount > 3) {
			throw new ValidationException("The SQL query determining the list of items is returning an invalid number of columns.  Queries should return at least 2, but no more than 3 columns.  The first column is the value (required), the second is the text (required), and the third is the tooltip (optional)");
		}
		List<SystemListItem> list = new ArrayList<>();
		for (int i = 0; i < results.getTotalRowCount(); i++) {
			DataRow row = results.getRow(i);
			String text = convertObjectToString(row.getValue(1));
			if (text == null) {
				throw new IllegalStateException("List item value cannot be null. List: " + systemList);
			}
			if (!StringUtils.isEmpty(searchPattern)) {
				if (!text.contains(searchPattern)) {
					continue;
				}
			}
			SystemListItem item = new SystemListItem();
			item.setSystemList(systemList);
			item.setValue(convertObjectToString(row.getValue(0)));
			item.setText(text);
			if (columnCount == 3) {
				item.setTooltip(convertObjectToString(row.getValue(2)));
			}
			list.add(item);
		}
		return list;
	}


	private String convertObjectToString(Object value) {
		if (value == null) {
			return null;
		}
		if (value instanceof Date) {
			return DateUtils.fromDate(((Date) value));
		}
		return value.toString();
	}


	// see SystemListItemValidator for validation
	@Override
	public SystemListItem saveSystemListItem(SystemListItem bean) {
		return getSystemListItemDAO().save(bean);
	}


	@Override
	public void deleteSystemListItem(int id) {
		getSystemListItemDAO().delete(id);
	}


	private void deleteSystemListItemListByList(int listId) {
		List<SystemListItem> itemList = getSystemListItemListByList(listId);
		getSystemListItemDAO().deleteList(itemList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemListItem, Criteria> getSystemListItemDAO() {
		return this.systemListItemDAO;
	}


	public void setSystemListItemDAO(AdvancedUpdatableDAO<SystemListItem, Criteria> systemListItemDAO) {
		this.systemListItemDAO = systemListItemDAO;
	}


	public AdvancedUpdatableDAO<T, Criteria> getSystemListDAO() {
		return this.systemListDAO;
	}


	public void setSystemListDAO(AdvancedUpdatableDAO<T, Criteria> systemListDAO) {
		this.systemListDAO = systemListDAO;
	}


	public DataTableRetrievalHandlerLocator getDataTableRetrievalHandlerLocator() {
		return this.dataTableRetrievalHandlerLocator;
	}


	public void setDataTableRetrievalHandlerLocator(DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator) {
		this.dataTableRetrievalHandlerLocator = dataTableRetrievalHandlerLocator;
	}


	public DaoNamedEntityCache<T> getSystemListCache() {
		return this.systemListCache;
	}


	public void setSystemListCache(DaoNamedEntityCache<T> systemListCache) {
		this.systemListCache = systemListCache;
	}


	public DaoCompositeKeyCache<SystemListItem, Integer, String> getSystemListItemByTextCache() {
		return this.systemListItemByTextCache;
	}


	public void setSystemListItemByTextCache(DaoCompositeKeyCache<SystemListItem, Integer, String> systemListItemByTextCache) {
		this.systemListItemByTextCache = systemListItemByTextCache;
	}


	public DaoCompositeKeyCache<SystemListItem, Integer, String> getSystemListItemByValueCache() {
		return this.systemListItemByValueCache;
	}


	public void setSystemListItemByValueCache(DaoCompositeKeyCache<SystemListItem, Integer, String> systemListItemByValueCache) {
		this.systemListItemByValueCache = systemListItemByValueCache;
	}
}
