package com.clifton.system.list.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


/**
 * @author vgomelsky
 */
public class SystemListSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;


	@SearchField(searchField = "name,description,sqlStatement")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "dataType.id")
	private Short dataTypeId;

	@SearchField
	private String sqlStatement;

	@SearchField(searchField = "sqlStatement", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean sqlList;

	@SearchField
	private Boolean nameSystemDefined;

	@SearchField
	private Boolean listSystemDefined;

	@SearchField(searchField = "entityModifyCondition.id")
	private Integer entityModifyConditionId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "SystemList";
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getDataTypeId() {
		return this.dataTypeId;
	}


	public void setDataTypeId(Short dataTypeId) {
		this.dataTypeId = dataTypeId;
	}


	public String getSqlStatement() {
		return this.sqlStatement;
	}


	public void setSqlStatement(String sqlStatement) {
		this.sqlStatement = sqlStatement;
	}


	public Boolean getSqlList() {
		return this.sqlList;
	}


	public void setSqlList(Boolean sqlList) {
		this.sqlList = sqlList;
	}


	public Boolean getNameSystemDefined() {
		return this.nameSystemDefined;
	}


	public void setNameSystemDefined(Boolean nameSystemDefined) {
		this.nameSystemDefined = nameSystemDefined;
	}


	public Boolean getListSystemDefined() {
		return this.listSystemDefined;
	}


	public void setListSystemDefined(Boolean listSystemDefined) {
		this.listSystemDefined = listSystemDefined;
	}


	public Integer getEntityModifyConditionId() {
		return this.entityModifyConditionId;
	}


	public void setEntityModifyConditionId(Integer entityModifyConditionId) {
		this.entityModifyConditionId = entityModifyConditionId;
	}
}
