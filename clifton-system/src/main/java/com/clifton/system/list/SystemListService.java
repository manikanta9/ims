package com.clifton.system.list;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.list.search.SystemListItemSearchForm;
import com.clifton.system.list.search.SystemListSearchForm;

import java.util.List;


/**
 * The <code>SystemListService</code> interface defines methods for working with system lists.
 *
 * @author vgomelsky
 */
public interface SystemListService<T extends SystemList> {

	////////////////////////////////////////////////////////////////////////////
	/////////          SystemList Business Methods                    //////////
	////////////////////////////////////////////////////////////////////////////


	public T getSystemList(int id);


	public T getSystemListByName(String name);


	@SecureMethod(dtoClass = SystemList.class)
	public List<T> getSystemListList(SystemListSearchForm searchForm);


	@SecureMethod(dtoClass = SystemList.class)
	public void deleteSystemList(int id);


	////////////////////////////////////////////////////////////////////////////
	/////////         System List - List Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = SystemList.class)
	public void saveSystemListEntity(SystemListEntity bean);


	////////////////////////////////////////////////////////////////////////////
	/////////         System List - Query Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = SystemList.class)
	public void saveSystemListQuery(SystemListQuery bean);


	////////////////////////////////////////////////////////////////////////////
	//////////          SystemListItem Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	public SystemListItem getSystemListItem(int id);


	/**
	 * Uses cache and retrieves unique SystemListItem object for selected list name and text
	 */
	public SystemListItem getSystemListItemByListAndText(String listName, String text);


	/**
	 * Uses cache and retrieves unique SystemListItem object for selected list name and value
	 */
	public SystemListItem getSystemListItemByListAndValue(String listName, String value);


	public List<SystemListItem> getSystemListItemListByList(int systemListId);


	/**
	 * Finds {@link SystemListItem}s by search fields.
	 * <p>
	 * listName is required.
	 * searchPattern is checked against the text field.
	 * active is a Boolean object that is only used if it is not null
	 */
	public List<SystemListItem> getSystemListItemList(final SystemListItemSearchForm searchForm);


	/**
	 * Returns a list of {@link SystemListItem} objects for the given List Entity bean.
	 * Accepts the bean instead of just an id for new beans that don't have an ID yet.
	 */
	public List<SystemListItem> getSystemListItemListByListBean(SystemList listEntity);


	public SystemListItem saveSystemListItem(SystemListItem bean);


	public void deleteSystemListItem(int id);
}
