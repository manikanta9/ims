package com.clifton.system.list.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * @author manderson
 */
public class SystemListItemSearchForm extends BaseEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	private String listName;

	@SearchField(searchField = "systemList.id")
	private Integer systemListId;


	@SearchField(searchField = "text,value,tooltip")
	private String searchPattern;

	@SearchField
	private String value;
	@SearchField(searchField = "value", comparisonConditions = ComparisonConditions.EQUALS)
	private String valueEquals;
	@SearchField
	private String text;
	@SearchField
	private String tooltip;

	@SearchField
	private Integer order;

	@SearchField
	private Boolean active;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getListName() {
		return this.listName;
	}


	public void setListName(String listName) {
		this.listName = listName;
	}


	public Integer getSystemListId() {
		return this.systemListId;
	}


	public void setSystemListId(Integer systemListId) {
		this.systemListId = systemListId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getValueEquals() {
		return this.valueEquals;
	}


	public void setValueEquals(String valueEquals) {
		this.valueEquals = valueEquals;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public String getTooltip() {
		return this.tooltip;
	}


	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}
}
