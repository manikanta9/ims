package com.clifton.system.list;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;


/**
 * The <code>SystemListItem</code> class represents a single list item.
 *
 * @author vgomelsky
 */
public class SystemListItem extends BaseEntity<Integer> implements LabeledObject, SystemEntityModifyConditionAwareAdminRequired {

	private SystemList systemList;
	private String value;
	private String text;
	private String tooltip;
	private Integer order;
	private boolean active;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getText();
	}


	public String getLabelLong() {
		return getText() + " (" + getValue() + ")";
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getSystemList() != null) {
			return getSystemList().getEntityModifyCondition();
		}
		return null;
	}


	@Override
	public boolean isEntityModifyConditionRequiredForNonAdmins() {
		if (getSystemList() != null) {
			return getSystemList().isEntityModifyConditionRequiredForNonAdmins();
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public SystemList getSystemList() {
		return this.systemList;
	}


	public void setSystemList(SystemList systemList) {
		this.systemList = systemList;
	}


	public String getTooltip() {
		return this.tooltip;
	}


	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}
}
