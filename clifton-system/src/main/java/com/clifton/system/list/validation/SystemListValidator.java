package com.clifton.system.list.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListQuery;
import com.clifton.system.list.SystemListService;
import com.clifton.system.schema.SystemDataTypeConverter;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>SystemListValidator</code> contains all methods necessary to validate
 * a {@link SystemList} objects prior to adding/editing/deleting from the database.
 *
 * @author manderson
 */
@Component
public class SystemListValidator<T extends SystemList> extends SelfRegisteringDaoValidator<SystemList> {

	private SystemListService<T> systemListService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemList bean, DaoEventTypes config) throws ValidationException {
		// Validate Delete - Cannot delete list if its name is system defined
		if (config.isDelete()) {
			if (bean.isNameSystemDefined()) {
				throw new ValidationException("Cannot delete a list that is marked as name system defined.");
			}
			return;
		}

		// SQL Lists - Validate Required Fields
		if (bean instanceof SystemListQuery) {
			SystemListQuery beanQueryList = (SystemListQuery) bean;
			ValidationUtils.assertMutuallyExclusive("Either Table or Data Source should be specified but not both.", beanQueryList.getDataSource(), beanQueryList.getTable());
			if (StringUtils.isEmpty(beanQueryList.getSqlStatement())) {
				throw new FieldValidationException("A sql query is required for SQL Lists.", "sql");
			}
		}

		if (config.isUpdate()) {
			SystemList originalBean = getOriginalBean(bean);
			// Validate Class Names for original bean and this bean are the same.
			if (!originalBean.getClass().equals(bean.getClass())) {
				throw new ValidationException("Cannot update a System List Entity from/to Regulate vs. SQL based");
			}

			// Validate Change In System Defined Fields
			if ((originalBean.isListSystemDefined() != bean.isListSystemDefined())) {
				throw new ValidationException("System Defined field cannot be edited via the User Interface.");
			}
			if (originalBean.isNameSystemDefined() != bean.isNameSystemDefined() && originalBean.isNameSystemDefined()) {
				throw new ValidationException("Name System Defined field cannot be cleared via the User Interface.");
			}

			// Validate Change In Name
			if (originalBean.isNameSystemDefined() && (!originalBean.getName().equals(bean.getName()))) {
				throw new ValidationException("Cannot change the name of a list that is marked as name system defined.");
			}

			// Validate Change SQL for list system defined.
			if (bean instanceof SystemListQuery && bean.isListSystemDefined()) {
				if (!((SystemListQuery) originalBean).getSqlStatement().equals(((SystemListQuery) bean).getSqlStatement())) {
					throw new ValidationException("The sql defined for this list cannot be edited as the list is marked as list system defined.");
				}
			}

			// If Changing Data Type for existing - validate Data Type
			if (!originalBean.getDataType().equals(bean.getDataType())) {
				validateDataType(bean);
			}
		}

		if (config.isInsert()) {
			// Validate System Defined Fields
			if (bean.isListSystemDefined() || bean.isNameSystemDefined()) {
				throw new ValidationException("System Defined fields cannot be defined via the User Interface.");
			}

			// Validate Data Types
			validateDataType(bean);
		}
	}


	private void validateDataType(SystemList bean) {
		List<SystemListItem> itemList = getSystemListService().getSystemListItemListByListBean(bean);
		SystemDataTypeConverter converter = new SystemDataTypeConverter(bean.getDataType());
		for (SystemListItem item : CollectionUtils.getIterable(itemList)) {
			try {
				converter.convert(item.getValue());
			}
			catch (Exception e) {
				throw new FieldValidationException("Selected Data Type for the List does not match all items.  There is an item in the list with value [" + item.getValue()
						+ "] that cannot be converted to type [" + item.getSystemList().getDataType().getName() + "].", "value");
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemListService<T> getSystemListService() {
		return this.systemListService;
	}


	public void setSystemListService(SystemListService<T> systemListService) {
		this.systemListService = systemListService;
	}
}
