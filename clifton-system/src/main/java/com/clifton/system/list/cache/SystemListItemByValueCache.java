package com.clifton.system.list.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.system.list.SystemListItem;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemListItemByValueCache</code> caches SystemListItems for ListID_Value
 *
 * @author manderson
 */
@Component
public class SystemListItemByValueCache extends SelfRegisteringCompositeKeyDaoCache<SystemListItem, Integer, String> {


	@Override
	protected String getBeanKey1Property() {
		return "systemList.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "value";
	}


	@Override
	protected Integer getBeanKey1Value(SystemListItem bean) {
		if (bean.getSystemList() != null) {
			return bean.getSystemList().getId();
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(SystemListItem bean) {
		return bean.getValue();
	}
}
