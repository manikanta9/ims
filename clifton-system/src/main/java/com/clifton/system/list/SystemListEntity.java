package com.clifton.system.list;


import javax.persistence.Table;


/**
 * The <code>SystemListEntity</code> defines a list definition that contains manually entered list items.
 *
 * @author manderson
 */
@Table(name = "SystemList")
public class SystemListEntity extends SystemList {

	@Override
	public boolean isSqlList() {
		return false;
	}
}
