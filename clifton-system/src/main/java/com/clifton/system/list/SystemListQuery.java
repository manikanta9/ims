package com.clifton.system.list;


import com.clifton.core.util.ObjectUtils;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemTable;

import javax.persistence.Table;


/**
 * The <code>SystemListQuery</code> is a special case of the {@link SystemList} object
 * which contains a SQL query that determines the list of items.
 * <p>
 * listSystemDefined field for Query Lists means that the SQL query cannot be edited.
 *
 * @author manderson
 */
@Table(name = "SystemList")
public class SystemListQuery extends SystemList {

	/**
	 * Primary table that the query is based on. The table defines query's data source.
	 */
	private SystemTable table;

	/**
	 * When no table is present, use this data source.
	 */
	private SystemDataSource dataSource;

	/**
	 * The sql should return 2 or 3 columns.  The first columns is the value field, which will
	 * be validated against the data type.  The second column is the text field, and the
	 * third column is the tooltip field.
	 */
	private String sqlStatement;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	public SystemDataSource getDataSourceForExecution() {
		return getTable() == null ? getDataSource() : ObjectUtils.coalesce(getDataSource(), getTable().getDataSource());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isSqlList() {
		return true;
	}


	public String getSqlStatement() {
		return this.sqlStatement;
	}


	public void setSqlStatement(String sqlStatement) {
		this.sqlStatement = sqlStatement;
	}


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public SystemDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(SystemDataSource dataSource) {
		this.dataSource = dataSource;
	}
}
