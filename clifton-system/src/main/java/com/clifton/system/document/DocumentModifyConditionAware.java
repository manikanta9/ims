package com.clifton.system.document;


import com.clifton.core.beans.document.DocumentAware;
import com.clifton.system.condition.SystemCondition;


/**
 * The <code>DocumentModifyConditionAware</code> interface should be implemented by {@link DocumentAware} beans
 * that support their own modify conditions that are validated during uploads and check outs of documents.
 * <p/>
 * Example:  SystemNoteTypes where IsAttachmentAllowed allow setting a specific modify condition that's specific to that note type.
 * The notes themselves implement this interface and thus we can create a specific modify condition for Trade Notes as opposed
 * to Company Notes.  If no condition is returned will use the default condition defined in Document Management Section.
 *
 * @author Mary Anderson
 */
public interface DocumentModifyConditionAware extends DocumentAware {

	public SystemCondition getDocumentModifyCondition();
}
