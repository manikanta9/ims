package com.clifton.system.action;

import com.clifton.core.converter.json.JsonStringToMapConverter;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.bean.search.SystemBeanPropertyTypeSearchForm;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author TerryS
 */
@Component
public class FtpFileFilterMigrationAction {

	private SystemBeanService systemBeanService;


	public void doMigration() {
		JsonStringToMapConverter converter = new JsonStringToMapConverter();

		SystemBeanSearchForm searchForm = new SystemBeanSearchForm();
		searchForm.setIncludeTypeIds(getSystemBeanTypes());
		List<SystemBean> systemBeanList = getSystemBeanService().getSystemBeanList(searchForm);
		for (SystemBean systemBean : CollectionUtils.getIterable(systemBeanList)) {
			List<SystemBeanProperty> beanPropertyList = getSystemBeanService().getSystemBeanPropertyListByBeanId(systemBean.getId());
			List<String> existingTypeMap = CollectionUtils.getStream(beanPropertyList)
					.map(SystemBeanProperty::getType)
					.map(SystemBeanPropertyType::getSystemPropertyName)
					.collect(Collectors.toList());
			boolean update = false;
			List<SystemBeanProperty> newPropertyList = new ArrayList<>();
			for (SystemBeanProperty systemBeanProperty : CollectionUtils.getIterable(beanPropertyList)) {
				SystemBeanPropertyType propertyType = systemBeanProperty.getParameter();
				if (StringUtils.isEqual(propertyType.getSystemPropertyName(), "importConfigurationList")) {
					String value = systemBeanProperty.getValue();
					if (!StringUtils.isEmpty(value)) {
						Map<String, Object> configurationMap = converter.convert(value);
						if (configurationMap != null && configurationMap.containsKey("ROOT_ARRAY") && configurationMap.get("ROOT_ARRAY") instanceof List) {
							@SuppressWarnings("unchecked")
							List<Map<String, Object>> listOfMaps = (List<Map<String, Object>>) configurationMap.get("ROOT_ARRAY");
							if (!CollectionUtils.isEmpty(listOfMaps)) {
								AssertUtils.assertEquals(1, listOfMaps.size(), "Only a single filter per definition is supported.");
								configurationMap = listOfMaps.get(0);
								for (Map.Entry<String, Object> entry : configurationMap.entrySet()) {
									if (entry.getValue() != null) {
										String propertyName;
										Object propertyValue = entry.getValue();
										switch (entry.getKey()) {
											case "ftpServerFolder":
												propertyName = "ftpServerFolder";
												break;
											case "filterRegex":
												propertyName = "filterRegex";
												break;
											case "dateFormat":
												propertyName = "dateFormat";
												break;
											case "targetPath":
												propertyName = "targetPath";
												break;
											case "writeExtension":
												propertyValue = true;
												propertyName = "writeExtension";
												break;
											default:
												throw new RuntimeException("Filter configuration field not known " + entry.getKey());
										}
										if (!StringUtils.isEmpty(propertyName)) {
											AssertUtils.assertFalse(existingTypeMap.contains(propertyName), "System Bean Type Already defined " + propertyName);
											newPropertyList.add(convertFilterProperty(systemBean, propertyName, propertyValue));
											existingTypeMap.add(propertyName);
											update = true;
										}
									}
								}
							}
						}
					}
				}
			}
			if (update) {
				newPropertyList.addAll(beanPropertyList);
				systemBean.setPropertyList(newPropertyList.stream()
						.filter(p -> !("importConfigurationList".equals(p.getType().getSystemPropertyName())))
						.collect(Collectors.toList()));
				getSystemBeanService().saveSystemBean(systemBean);
			}
		}
	}


	private SystemBeanProperty convertFilterProperty(SystemBean systemBean, String propertyName, Object value) {
		SystemBeanProperty systemBeanProperty = new SystemBeanProperty();

		SystemBeanPropertyTypeSearchForm systemBeanPropertyTypeSearchForm = new SystemBeanPropertyTypeSearchForm();
		systemBeanPropertyTypeSearchForm.setSystemPropertyName(propertyName);
		systemBeanPropertyTypeSearchForm.setTypeId(systemBean.getType().getId());
		List<SystemBeanPropertyType> typeList = getSystemBeanService().getSystemBeanPropertyTypeList(systemBeanPropertyTypeSearchForm);
		AssertUtils.assertNotEmpty(typeList, "Could not get property type " + propertyName);
		AssertUtils.assertEquals(typeList.size(), 1, "Could not get property type " + propertyName + " " + typeList.size());

		systemBeanProperty.setBean(systemBean);
		systemBeanProperty.setValue(value.toString());
		systemBeanProperty.setText(value.toString());
		systemBeanProperty.setType(typeList.get(0));

		return systemBeanProperty;
	}


	private Integer[] getSystemBeanTypes() {
		List<Integer> systemTypeIds = new ArrayList<>();
		SystemBeanType systemBeanType = getSystemBeanService().getSystemBeanTypeByName("FTP Import Job");
		ObjectUtils.doIfPresent(systemBeanType, t -> systemTypeIds.add(t.getId()));
		systemBeanType = getSystemBeanService().getSystemBeanTypeByName("DES Encrypted FTP Import Job");
		ObjectUtils.doIfPresent(systemBeanType, t -> systemTypeIds.add(t.getId()));
		return systemTypeIds.toArray(new Integer[0]);
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
