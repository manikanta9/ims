package com.clifton.system.hierarchy.assignment.cache;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>SystemHierarchyLinkByEntityCache</code> class provides caching and retrieval from cache of SystemHierarchyLink objects
 * by table and fkFieldId.
 *
 * @author apopp
 */
@Component
public class SystemHierarchyLinkByEntityCache extends SelfRegisteringCompositeKeyDaoListCache<SystemHierarchyLink, Short, Long> {

	@Override
	protected String getBeanKey1Property() {
		return "assignment.table.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "fkFieldId";
	}


	@Override
	protected Short getBeanKey1Value(SystemHierarchyLink bean) {
		if (bean != null && bean.getAssignment() != null) {
			return BeanUtils.getBeanIdentity(bean.getAssignment().getTable());
		}
		return null;
	}


	@Override
	protected Long getBeanKey2Value(SystemHierarchyLink bean) {
		if (bean != null) {
			return bean.getFkFieldId();
		}
		return null;
	}


	/**
	 * We must override lookupBeanList because we need to go deep enough to derive the table id through the assignment
	 */
	@Override
	protected List<SystemHierarchyLink> lookupBeanList(ReadOnlyDAO<SystemHierarchyLink> dao, boolean throwExceptionIfNotFound, final Object... keyProperties) {
		AdvancedReadOnlyDAO<SystemHierarchyLink, Criteria> advancedDao = (AdvancedReadOnlyDAO<SystemHierarchyLink, Criteria>) dao;
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			criteria.add(Restrictions.eq("fkFieldId", keyProperties[1]));
			criteria.createCriteria("assignment", "a").add(Restrictions.eq("a.table.id", keyProperties[0]));
		};

		return advancedDao.findBySearchCriteria(searchConfigurer);
	}
}
