package com.clifton.system.hierarchy.assignment;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;


/**
 * The <code>SystemHierarchyAssignment</code> defines which hierarchy
 * categories can be defined an an entity.
 * <p>
 * i.e. Company can be tied to a Geographical Hierarchy
 * <p>
 * Additional security condition is defined on the {@link SystemHierarchyCategory}
 *
 * @author manderson
 */
public class SystemHierarchyAssignment extends BaseEntity<Short> implements SystemEntityModifyConditionAwareAdminRequired {

	private SystemHierarchyCategory category;
	private SystemTable table;

	/**
	 * IsSingleTableAssignment:
	 * 1.  There is and only can be one assignment for the assigned table.
	 * (Otherwise could have multiple assignments for the same table)
	 * 2.  Hierarchy Category for IsSingleTableAssignment = true can only be edited, not removed.
	 * (Allowing edits would also depend on the Category system defined flag)
	 */
	private boolean singleTableAssignment;

	/**
	 * IsMultipleValuesAllowed:
	 * 1.  Multiple Hierarchies can be assigned to the same entity for this category/table assignment.
	 * Example of Multiple vs. Single values allowed: a Company can be assigned to
	 * multiple Business Units, but only one Location.
	 */
	private boolean multipleValuesAllowed;

	/**
	 * Optional security override.  If not set, uses the security defined for the table.
	 */
	private SecurityResource securityResource;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSecurityResourceLabel() {
		if (getSecurityResource() != null) {
			return getSecurityResource().getLabelExpanded();
		}
		else if (getTable() != null && getTable().getSecurityResource() != null) {
			return getTable().getSecurityResource().getLabelExpanded();
		}
		return null;
	}


	public boolean isSecurityOverridden() {
		return getSecurityResource() != null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return (getCategory() == null) ? null : getCategory().getEntityModifyCondition();
	}


	@Override
	public boolean isEntityModifyConditionRequiredForNonAdmins() {
		return getCategory() != null && getCategory().isEntityModifyConditionRequiredForNonAdmins();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyCategory getCategory() {
		return this.category;
	}


	public void setCategory(SystemHierarchyCategory category) {
		this.category = category;
	}


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public boolean isMultipleValuesAllowed() {
		return this.multipleValuesAllowed;
	}


	public void setMultipleValuesAllowed(boolean multipleValuesAllowed) {
		this.multipleValuesAllowed = multipleValuesAllowed;
	}


	public boolean isSingleTableAssignment() {
		return this.singleTableAssignment;
	}


	public void setSingleTableAssignment(boolean singleTableAssignment) {
		this.singleTableAssignment = singleTableAssignment;
	}


	public SecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(SecurityResource securityResource) {
		this.securityResource = securityResource;
	}
}
