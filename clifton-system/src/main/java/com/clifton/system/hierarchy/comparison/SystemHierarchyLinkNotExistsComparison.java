package com.clifton.system.hierarchy.comparison;

/**
 * The SystemHierarchyLinkNotExistsComparison extends is exists comparison but returns true if the link does not exist.
 *
 * @author manderson
 */
public class SystemHierarchyLinkNotExistsComparison extends SystemHierarchyLinkExistsComparison {


	@Override
	protected boolean isExistsComparison() {
		return false;
	}
}
