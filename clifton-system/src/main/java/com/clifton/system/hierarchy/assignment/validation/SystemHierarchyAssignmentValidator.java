package com.clifton.system.hierarchy.assignment.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignment;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemHierarchyAssignmentValidator</code> contains all methods necessary to validate
 * a {@link SystemHierarchyAssignment} prior to adding/editing/deleting from the database.
 *
 * @author manderson
 */
@Component
public class SystemHierarchyAssignmentValidator extends SelfRegisteringDaoValidator<SystemHierarchyAssignment> {

	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemHierarchyAssignment bean, DaoEventTypes config) throws ValidationException {
		List<SystemHierarchyAssignment> existingList = getSystemHierarchyAssignmentService().getSystemHierarchyAssignmentListByTable(bean.getTable().getId());
		if (config.isInsert()) {
			// First validate for System Defined Categories
			if (bean.getCategory().isSystemDefined()) {
				throw new ValidationException("Category [" + bean.getCategory().getName() + "] is system-defined. Creating assignments for this category is not permitted.");
			}
			// Then Validate for IsSingleTable Assignment
			for (SystemHierarchyAssignment assign : CollectionUtils.getIterable(existingList)) {
				if (assign.isSingleTableAssignment()) {
					throw new ValidationException("There already exists an assignment for this table that limits assignment count to 1.");
				}
			}
		}
		else if (config.isUpdate()) {
			SystemHierarchyAssignment originalBean = getOriginalBean(bean);
			// Validate System Defined Category
			if (originalBean.getCategory().isSystemDefined()) {
				throw new ValidationException("This assignment was tied to a system-defined category [" + originalBean.getCategory().getName()
						+ "].  You cannot change the category for this assignment.");
			}
			// Validate Table Change
			if (!originalBean.getTable().equals(bean.getTable())) {
				throw new ValidationException("You cannot change the table from [" + originalBean.getTable().getName() + "] for this assignment.");
			}
			// Validate Category or IsMultipleValuesAllowed Change
			List<SystemHierarchyLink> linkList = getSystemHierarchyAssignmentService().getSystemHierarchyLinkListByAssignment(bean.getId());
			if (!originalBean.getCategory().equals(bean.getCategory()) && !CollectionUtils.isEmpty(linkList)) {
				throw new ValidationException("There are links that are currently tied to this assignment. Cannot change the category for this assignment.");
			}
			if (originalBean.isMultipleValuesAllowed() && !bean.isMultipleValuesAllowed()) {
				List<Long> fkFieldIdList = new ArrayList<>();
				for (SystemHierarchyLink link : CollectionUtils.getIterable(linkList)) {
					if (fkFieldIdList.contains(link.getFkFieldId())) {
						throw new ValidationException("Cannot change this assignment to restrict against multiple values as there exists links that are currently tying multiple values.");
					}
					fkFieldIdList.add(link.getFkFieldId());
				}
			}
			// Validate for IsSingleTable Assignment
			for (SystemHierarchyAssignment assign : CollectionUtils.getIterable(existingList)) {
				if (!assign.equals(bean) && bean.isSingleTableAssignment()) {
					throw new ValidationException("There already exists an assignment for table [" + bean.getTable().getName() + "].  You cannot change this assignment to a single table assignment.");
				}
			}
		}
		else if (config.isDelete()) {
			// Validate System Defined Category
			if (bean.getCategory().isSystemDefined()) {
				throw new ValidationException("Category [" + bean.getCategory().getName() + "] is system-defined. Deleting assignments for this category is not permitted.");
			}
			// Validate there are no existing links for this assignment
			if (!CollectionUtils.isEmpty(getSystemHierarchyAssignmentService().getSystemHierarchyLinkListByAssignment(bean.getId()))) {
				throw new ValidationException("There are existing links tied to this assignment.  Delete not permitted.");
			}

			// Validate Not deleting the last hierarchy entry for the table.
			boolean found = false;
			for (SystemHierarchyAssignment assign : CollectionUtils.getIterable(existingList)) {
				if (!assign.equals(bean)) {
					found = true;
					break;
				}
			}
			if (!found) {
				throw new ValidationException("This is the only assignment for this table.  Delete is not permitted.");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}
}
