package com.clifton.system.hierarchy.assignment.search;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * The <code>BaseSystemHierarchyItemSearchForm</code> can be extended by hierarchical entities and is used to find entities that have various hierarchy
 * associations or missing hierarchy associations.
 * <p>
 * Supports combination of up to three hierarchy category links. Example: System Queries are organized into folders (one hierarchy category), but also linked
 * through tags (another hierarchy category).
 * <p>
 * This search form can be used on it's own or can be extended by the entity search form
 * Any search form that implements the SystemHierarchyItemSearchForm interface will automatically be configured with the {@link SystemHierarchyItemSearchFormConfigurer}
 *
 * @author manderson
 */
public abstract class BaseSystemHierarchyItemSearchForm extends BaseEntitySearchForm implements SystemHierarchyItemSearchForm {

	private String categoryName;
	//////////////////////////////////////////////////////////////
	private String categoryTableName; // Optional override for getDaoTableName for this specific category filter - i.e. Reports need to filter on Report folders and Report Template tags
	// Can use id or name to filter
	private Short categoryHierarchyId;
	private Short[] categoryHierarchyIds;
	private String categoryHierarchyName;
	private String[] categoryHierarchyNames;
	private String categoryLinkFieldPath; // If not searching specifically for this entity, but through related entity. i.e. For Report search on Report Template Tags the filter needs to be applied to the hierarchy link to the report's template
	/**
	 * If true will return all regardless what hierarchy in category 1 the entity is linked to
	 * If false will return only those not assigned to a hierarchy in category 1
	 */
	private Boolean categoryIncludeAllIfNull;
	private String category2Name;
	private String category2TableName; // Optional override for getDaoTableName for this specific category filter - i.e. Reports need to filter on Report folders and Report Template tags
	private Short category2HierarchyId;
	private Short[] category2HierarchyIds;
	private String category2HierarchyName;
	private String[] category2HierarchyNames;
	private String category2LinkFieldPath; // If not searching specifically for this entity, but through related entity. i.e. For Report search on Report Template Tags the filter needs to be applied to the hierarchy link to the report's template
	/**
	 * If true will return all regardless what hierarchy in category 2 the entity is linked to
	 * If false will return only those not assigned to a hierarchy in category 1
	 */
	private Boolean category2IncludeAllIfNull;
	private String category3Name;
	private String category3TableName; // Optional override for getDaoTableName for this specific category filter - i.e. Reports need to filter on Report folders and Report Template tags
	private Short category3HierarchyId;
	private Short[] category3HierarchyIds;
	private String category3HierarchyName;
	private String[] category3HierarchyNames;
	private String category3LinkFieldPath; // If not searching specifically for this entity, but through related entity. i.e. For Report search on Report Template Tags the filter needs to be applied to the hierarchy link to the report's template
	/**
	 * If true will return all regardless what hierarchy in category 3 the entity is linked to
	 * If false will return only those not assigned to a hierarchy in category 3
	 */
	private Boolean category3IncludeAllIfNull;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return this.categoryName;
	}


	@Override
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	@Override
	public String getCategoryTableName() {
		return this.categoryTableName;
	}


	@Override
	public void setCategoryTableName(String categoryTableName) {
		this.categoryTableName = categoryTableName;
	}


	@Override
	public Short getCategoryHierarchyId() {
		return this.categoryHierarchyId;
	}


	@Override
	public void setCategoryHierarchyId(Short categoryHierarchyId) {
		this.categoryHierarchyId = categoryHierarchyId;
	}


	@Override
	public Short[] getCategoryHierarchyIds() {
		return this.categoryHierarchyIds;
	}


	@Override
	public void setCategoryHierarchyIds(Short[] categoryHierarchyIds) {
		this.categoryHierarchyIds = categoryHierarchyIds;
	}


	@Override
	public String getCategoryHierarchyName() {
		return this.categoryHierarchyName;
	}


	@Override
	public void setCategoryHierarchyName(String categoryHierarchyName) {
		this.categoryHierarchyName = categoryHierarchyName;
	}


	@Override
	public String[] getCategoryHierarchyNames() {
		return this.categoryHierarchyNames;
	}


	@Override
	public void setCategoryHierarchyNames(String[] categoryHierarchyNames) {
		this.categoryHierarchyNames = categoryHierarchyNames;
	}


	@Override
	public String getCategoryLinkFieldPath() {
		return this.categoryLinkFieldPath;
	}


	@Override
	public void setCategoryLinkFieldPath(String categoryLinkFieldPath) {
		this.categoryLinkFieldPath = categoryLinkFieldPath;
	}


	@Override
	public Boolean getCategoryIncludeAllIfNull() {
		return this.categoryIncludeAllIfNull;
	}


	@Override
	public void setCategoryIncludeAllIfNull(Boolean categoryIncludeAllIfNull) {
		this.categoryIncludeAllIfNull = categoryIncludeAllIfNull;
	}


	@Override
	public String getCategory2Name() {
		return this.category2Name;
	}


	@Override
	public void setCategory2Name(String category2Name) {
		this.category2Name = category2Name;
	}


	@Override
	public String getCategory2TableName() {
		return this.category2TableName;
	}


	@Override
	public void setCategory2TableName(String category2TableName) {
		this.category2TableName = category2TableName;
	}


	@Override
	public Short getCategory2HierarchyId() {
		return this.category2HierarchyId;
	}


	@Override
	public void setCategory2HierarchyId(Short category2HierarchyId) {
		this.category2HierarchyId = category2HierarchyId;
	}


	@Override
	public Short[] getCategory2HierarchyIds() {
		return this.category2HierarchyIds;
	}


	@Override
	public void setCategory2HierarchyIds(Short[] category2HierarchyIds) {
		this.category2HierarchyIds = category2HierarchyIds;
	}


	@Override
	public String getCategory2HierarchyName() {
		return this.category2HierarchyName;
	}


	@Override
	public void setCategory2HierarchyName(String category2HierarchyName) {
		this.category2HierarchyName = category2HierarchyName;
	}


	@Override
	public String[] getCategory2HierarchyNames() {
		return this.category2HierarchyNames;
	}


	@Override
	public void setCategory2HierarchyNames(String[] category2HierarchyNames) {
		this.category2HierarchyNames = category2HierarchyNames;
	}


	@Override
	public String getCategory2LinkFieldPath() {
		return this.category2LinkFieldPath;
	}


	@Override
	public void setCategory2LinkFieldPath(String category2LinkFieldPath) {
		this.category2LinkFieldPath = category2LinkFieldPath;
	}


	@Override
	public Boolean getCategory2IncludeAllIfNull() {
		return this.category2IncludeAllIfNull;
	}


	@Override
	public void setCategory2IncludeAllIfNull(Boolean category2IncludeAllIfNull) {
		this.category2IncludeAllIfNull = category2IncludeAllIfNull;
	}


	@Override
	public String getCategory3Name() {
		return this.category3Name;
	}


	@Override
	public void setCategory3Name(String category3Name) {
		this.category3Name = category3Name;
	}


	@Override
	public String getCategory3TableName() {
		return this.category3TableName;
	}


	@Override
	public void setCategory3TableName(String category3TableName) {
		this.category3TableName = category3TableName;
	}


	@Override
	public Short getCategory3HierarchyId() {
		return this.category3HierarchyId;
	}


	@Override
	public void setCategory3HierarchyId(Short category3HierarchyId) {
		this.category3HierarchyId = category3HierarchyId;
	}


	@Override
	public Short[] getCategory3HierarchyIds() {
		return this.category3HierarchyIds;
	}


	@Override
	public void setCategory3HierarchyIds(Short[] category3HierarchyIds) {
		this.category3HierarchyIds = category3HierarchyIds;
	}


	@Override
	public String getCategory3HierarchyName() {
		return this.category3HierarchyName;
	}


	@Override
	public void setCategory3HierarchyName(String category3HierarchyName) {
		this.category3HierarchyName = category3HierarchyName;
	}


	@Override
	public String[] getCategory3HierarchyNames() {
		return this.category3HierarchyNames;
	}


	@Override
	public void setCategory3HierarchyNames(String[] category3HierarchyNames) {
		this.category3HierarchyNames = category3HierarchyNames;
	}


	@Override
	public String getCategory3LinkFieldPath() {
		return this.category3LinkFieldPath;
	}


	@Override
	public void setCategory3LinkFieldPath(String category3LinkFieldPath) {
		this.category3LinkFieldPath = category3LinkFieldPath;
	}


	@Override
	public Boolean getCategory3IncludeAllIfNull() {
		return this.category3IncludeAllIfNull;
	}


	@Override
	public void setCategory3IncludeAllIfNull(Boolean category3IncludeAllIfNull) {
		this.category3IncludeAllIfNull = category3IncludeAllIfNull;
	}
}
