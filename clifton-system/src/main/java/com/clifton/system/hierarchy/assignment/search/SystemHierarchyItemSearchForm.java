package com.clifton.system.hierarchy.assignment.search;

import com.clifton.core.dataaccess.search.form.RestrictiveSearchForm;


/**
 * The <code>SystemHierarchyItemSearchForm</code> ...
 *
 * @author manderson
 */
public interface SystemHierarchyItemSearchForm extends RestrictiveSearchForm {

	/**
	 * Table name of the entity we are looking for - when extending this search form, can override getDaoTableName method
	 * NOTE: uses name "daoTableName" to avoid conflicts with other search forms that link to a table and use tableName
	 * For specific cases, the category filters can be overridden with their own table name if necessary
	 */
	public String getDaoTableName();


	public String getCategoryName();


	public void setCategoryName(String categoryName);


	public String getCategoryTableName();


	public void setCategoryTableName(String categoryTableName);


	public Short getCategoryHierarchyId();


	public void setCategoryHierarchyId(Short categoryHierarchyId);


	public Short[] getCategoryHierarchyIds();


	public void setCategoryHierarchyIds(Short[] categoryHierarchyIds);


	public String getCategoryHierarchyName();


	public void setCategoryHierarchyName(String categoryHierarchyName);


	public String[] getCategoryHierarchyNames();


	public void setCategoryHierarchyNames(String[] categoryHierarchyNames);


	public String getCategoryLinkFieldPath();


	public void setCategoryLinkFieldPath(String categoryLinkFieldPath);


	public Boolean getCategoryIncludeAllIfNull();


	public void setCategoryIncludeAllIfNull(Boolean categoryIncludeAllIfNull);


	public String getCategory2Name();


	public void setCategory2Name(String category2Name);


	public String getCategory2TableName();


	public void setCategory2TableName(String category2TableName);


	public Short getCategory2HierarchyId();


	public void setCategory2HierarchyId(Short category2HierarchyId);


	public Short[] getCategory2HierarchyIds();


	public void setCategory2HierarchyIds(Short[] category2HierarchyIds);


	public String getCategory2HierarchyName();


	public void setCategory2HierarchyName(String category2HierarchyName);


	public String[] getCategory2HierarchyNames();


	public void setCategory2HierarchyNames(String[] category2HierarchyNames);


	public String getCategory2LinkFieldPath();


	public void setCategory2LinkFieldPath(String category2LinkFieldPath);


	public Boolean getCategory2IncludeAllIfNull();


	public void setCategory2IncludeAllIfNull(Boolean category2IncludeAllIfNull);


	public String getCategory3Name();


	public void setCategory3Name(String category3Name);


	public String getCategory3TableName();


	public void setCategory3TableName(String category3TableName);


	public Short getCategory3HierarchyId();


	public void setCategory3HierarchyId(Short category3HierarchyId);


	public Short[] getCategory3HierarchyIds();


	public void setCategory3HierarchyIds(Short[] category3HierarchyIds);


	public String getCategory3HierarchyName();


	public void setCategory3HierarchyName(String category3HierarchyName);


	public String[] getCategory3HierarchyNames();


	public void setCategory3HierarchyNames(String[] category3HierarchyNames);


	public String getCategory3LinkFieldPath();


	public void setCategory3LinkFieldPath(String category3LinkFieldPath);


	public Boolean getCategory3IncludeAllIfNull();


	public void setCategory3IncludeAllIfNull(Boolean category3IncludeAllIfNull);
}
