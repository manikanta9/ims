package com.clifton.system.hierarchy.definition;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignment;
import com.clifton.system.hierarchy.definition.search.SystemHierarchyCategorySearchForm;
import com.clifton.system.hierarchy.definition.search.SystemHierarchySearchForm;
import com.clifton.system.hierarchy.definition.validation.SystemHierarchyCategoryValidator;
import com.clifton.system.hierarchy.definition.validation.SystemHierarchyValidator;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The <code>SystemHierarchyDefinitionServiceImpl</code> provides a basic implementation for the
 * {@link SystemHierarchyDefinitionService} interface.
 *
 * @author manderson
 */
@Service
public class SystemHierarchyDefinitionServiceImpl implements SystemHierarchyDefinitionService {

	private AdvancedUpdatableDAO<SystemHierarchyCategory, Criteria> systemHierarchyCategoryDAO;
	private AdvancedUpdatableDAO<SystemHierarchy, Criteria> systemHierarchyDAO;

	private DaoNamedEntityCache<SystemHierarchyCategory> systemHierarchyCategoryCache;


	////////////////////////////////////////////////////////////////////////////
	//////       System Hierarchy Category Business Methods            /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemHierarchyCategory getSystemHierarchyCategory(short id) {
		return getSystemHierarchyCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemHierarchyCategory getSystemHierarchyCategoryByName(String name) {
		return getSystemHierarchyCategoryCache().getBeanForKeyValueStrict(getSystemHierarchyCategoryDAO(), name);
	}


	@Override
	public SystemHierarchyCategory getSystemHierarchyCategoryByTable(final String tableName) {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createCriteria("assignmentList").createAlias("table", "t").add(Restrictions.eq("t.name", tableName));

		return CollectionUtils.getOnlyElement(getSystemHierarchyCategoryDAO().findBySearchCriteria(searchConfigurer));
	}


	@Override
	public List<SystemHierarchyCategory> getSystemHierarchyCategoryList(SystemHierarchyCategorySearchForm searchForm) {
		return getSystemHierarchyCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see SystemHierarchyCategoryValidator
	 */
	@Override
	public SystemHierarchyCategory saveSystemHierarchyCategory(SystemHierarchyCategory bean) {
		return getSystemHierarchyCategoryDAO().save(bean);
	}


	/**
	 * @see SystemHierarchyCategoryValidator
	 */
	@Override
	@Transactional
	public void deleteSystemHierarchyCategory(short id) {
		// First delete hierarchies tied to this category
		getSystemHierarchyDAO().deleteList(getSystemHierarchyListByCategory(id));
		getSystemHierarchyCategoryDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////         System Hierarchy Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemHierarchy getSystemHierarchy(short id) {
		return getSystemHierarchyDAO().findByPrimaryKey(id);
	}


	/**
	 * Note: Assumes one unique name in the category - convenient for one level deep look ups
	 * for those that support multiple levels deep and have the same name at different levels this will throw an exception
	 */
	@Override
	public SystemHierarchy getSystemHierarchyByCategoryAndNameExpanded(String categoryName, String name) {
		SystemHierarchySearchForm searchForm = new SystemHierarchySearchForm();
		searchForm.setCategoryName(categoryName);
		searchForm.setSearchPattern(name);
		return CollectionUtils.getOnlyElement(getSystemHierarchyList(searchForm));
	}


	private List<SystemHierarchy> getSystemHierarchyListByCategory(short categoryId) {
		SystemHierarchySearchForm searchForm = new SystemHierarchySearchForm();
		searchForm.setCategoryId(categoryId);
		return getSystemHierarchyList(searchForm);
	}


	@Override
	public List<SystemHierarchy> getSystemHierarchyList(final SystemHierarchySearchForm searchForm) {
		// Default Sorting if none defined
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("nameExpanded");
		}
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				if (searchForm.getParentId() != null || !searchForm.isIgnoreNullParent()) {
					if (searchForm.getParentId() == null) {
						criteria.add(Restrictions.isNull("parent.id"));
					}
					else {
						criteria.add(Restrictions.eq("parent.id", searchForm.getParentId()));
					}
				}

				if (!StringUtils.isEmpty(searchForm.getTableName())) {
					DetachedCriteria sub = DetachedCriteria.forClass(SystemHierarchyAssignment.class, "a");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("table", "t").add(Restrictions.eq("t.name", searchForm.getTableName()));
					sub.add(Restrictions.eqProperty("a.category.id", criteria.getAlias() + ".category.id"));
					criteria.add(Subqueries.exists(sub));
				}
				super.configureCriteria(criteria);
			}
		};
		return getSystemHierarchyDAO().findBySearchCriteria(searchConfigurer);
	}


	private List<SystemHierarchy> getSystemHierarchyListByParent(short parentId) {
		return getSystemHierarchyDAO().findByField("parent.id", parentId);
	}


	/**
	 * @see SystemHierarchyValidator
	 */
	@Override
	public SystemHierarchy saveSystemHierarchy(SystemHierarchy bean) {
		return getSystemHierarchyDAO().save(bean);
	}


	/**
	 * @see SystemHierarchyValidator
	 */
	@Override
	@Transactional
	public void deleteSystemHierarchy(short id) {
		// First Delete all children & grand children, etc.
		for (SystemHierarchy hierarchy : CollectionUtils.getIterable(getSystemHierarchyListByParent(id))) {
			deleteSystemHierarchy(hierarchy.getId());
		}
		getSystemHierarchyDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemHierarchyCategory, Criteria> getSystemHierarchyCategoryDAO() {
		return this.systemHierarchyCategoryDAO;
	}


	public void setSystemHierarchyCategoryDAO(AdvancedUpdatableDAO<SystemHierarchyCategory, Criteria> systemHierarchyCategoryDAO) {
		this.systemHierarchyCategoryDAO = systemHierarchyCategoryDAO;
	}


	public AdvancedUpdatableDAO<SystemHierarchy, Criteria> getSystemHierarchyDAO() {
		return this.systemHierarchyDAO;
	}


	public void setSystemHierarchyDAO(AdvancedUpdatableDAO<SystemHierarchy, Criteria> systemHierarchyDAO) {
		this.systemHierarchyDAO = systemHierarchyDAO;
	}


	public DaoNamedEntityCache<SystemHierarchyCategory> getSystemHierarchyCategoryCache() {
		return this.systemHierarchyCategoryCache;
	}


	public void setSystemHierarchyCategoryCache(DaoNamedEntityCache<SystemHierarchyCategory> systemHierarchyCategoryCache) {
		this.systemHierarchyCategoryCache = systemHierarchyCategoryCache;
	}
}
