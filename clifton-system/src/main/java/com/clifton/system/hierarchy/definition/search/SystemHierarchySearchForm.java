package com.clifton.system.hierarchy.definition.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>SystemHierarchySearchForm</code> ...
 *
 * @author manderson
 */
public class SystemHierarchySearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "name")
	private String searchPattern;

	// Max Levels Deep is Currently 6
	@SearchField(searchField = "parent.parent.parent.parent.parent.name,parent.parent.parent.parent.name,parent.parent.parent.name,parent.parent.name,parent.name,name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String nameExpanded;

	@SearchField
	private String value;

	private String tableName;

	@SearchField(searchField = "category.id")
	private Short categoryId;

	@SearchField(searchField = "name", searchFieldPath = "category")
	private String categoryName;

	private Short parentId;

	/**
	 * If true, null parent id will be ignored from the search,
	 * otherwise search will use parent.id IS NULL parameter
	 */
	private boolean ignoreNullParent;

	@SearchField
	private Integer order;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Short getParentId() {
		return this.parentId;
	}


	public void setParentId(Short parentId) {
		this.parentId = parentId;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public String getNameExpanded() {
		return this.nameExpanded;
	}


	public void setNameExpanded(String nameExpanded) {
		this.nameExpanded = nameExpanded;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public boolean isIgnoreNullParent() {
		return this.ignoreNullParent;
	}


	public void setIgnoreNullParent(boolean ignoreNullParent) {
		this.ignoreNullParent = ignoreNullParent;
	}


	public String getCategoryName() {
		return this.categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
