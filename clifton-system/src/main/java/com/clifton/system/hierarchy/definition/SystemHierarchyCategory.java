package com.clifton.system.hierarchy.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;


/**
 * The <code>SystemHierarchyCategory</code> groups hierarchy structures into categories
 * i.e. Geographical, Business Units, etc.
 * <p>
 * Tags are a special case for hierarchies where the max depth would be 1
 *
 * @author manderson
 */
@CacheByName
public class SystemHierarchyCategory extends NamedEntity<Short> implements SystemEntityModifyConditionAwareAdminRequired {

	/**
	 * System Defined Categories:
	 * 1.  CategoryName cannot be edited.
	 * 2.  Category cannot be deleted.
	 * 3.  Category assignments can not be added/edited/removed.
	 * 4.  Hierarchy values can still be edited/added/removed by users.
	 */
	private boolean systemDefined;

	/**
	 * MaxDepth determines how many levels a hierarchy can be.
	 * if null then will not restrict on the hierarchy depth.
	 */
	private Integer maxDepth;

	/**
	 * Specifies whether this category is inactive: removes corresponding observer.
	 */
	private boolean inactive;

	/**
	 * For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit this category, its hierarchies, or assignments
	 */
	private SystemCondition entityModifyCondition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getMaxDepth() {
		return this.maxDepth;
	}


	public void setMaxDepth(Integer maxDepth) {
		this.maxDepth = maxDepth;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
