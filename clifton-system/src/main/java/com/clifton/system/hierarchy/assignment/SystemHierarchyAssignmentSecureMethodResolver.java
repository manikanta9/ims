package com.clifton.system.hierarchy.assignment;

import com.clifton.core.security.authorization.SecureMethodSecurityResourceResolver;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The {@link SystemHierarchyAssignmentSecureMethodResolver} returns security resource for systemHierarchyAssignment if set - otherwise security resource
 * associated with the table associated with the hierarchy assignment
 *
 * @author lnaylor
 */
@Component
public class SystemHierarchyAssignmentSecureMethodResolver implements SecureMethodSecurityResourceResolver {

	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getSecurityResourceName(@SuppressWarnings("unused") Method method, Map<String, ?> config) {
		SystemHierarchyAssignment assignment = getSystemHierarchyAssignment(method, config);

		AssertUtils.assertNotNull(assignment, "Cannot get SecurityResource. No SystemHierarchyAssignment found when invoking [%s] with arguments: [%s]", method.getName(),
				CollectionUtils.getStream(config.keySet())
						.map(k -> k + "=" + MapUtils.getParameterAsString(k, config))
						.collect(Collectors.joining("\n", "\n", "")));
		if (assignment.getSecurityResource() != null) {
			return assignment.getSecurityResource().getName();
		}
		return assignment.getTable().getSecurityResource().getName();
	}


	private SystemHierarchyAssignment getSystemHierarchyAssignment(Method method, Map<String, ?> config) {
		String methodName = method.getName();
		String tableName = MapUtils.getParameterAsString("tableName", config);
		AssertUtils.assertNotEmpty(tableName, "Cannot get Security Resource. Invocation of [%s] has no associated tableName", methodName);

		Short hierarchyId;
		switch (methodName) {
			case "saveSystemHierarchyLink":
			case "getSystemHierarchyItemList":
				hierarchyId = MapUtils.getParameterAsShort("hierarchyId", config, 0);
				break;
			default:
				throw new UnsupportedOperationException(String.format("Cannot get SecurityResource. Method [%s] is not supported by SystemHierarchyAssignmentSecureMethodResolver", methodName));
		}
		AssertUtils.assertNotNull(hierarchyId, "Cannot get Security Resource. No hierarchyId in request to [%s]", methodName);
		SystemHierarchy hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchy(hierarchyId);
		AssertUtils.assertNotNull(hierarchy, "Cannot get Security Resource. No SystemHierarchy found with hierarchyId [%d]", hierarchyId);

		SystemHierarchyCategory category = getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName(hierarchy.getCategory().getName());
		AssertUtils.assertNotNull(category, "Cannot get Security Resource. No SystemHierarchyCategory found with name [%s]", hierarchy.getCategory().getName());

		return getSystemHierarchyAssignmentService().getSystemHierarchyAssignmentByTableAndCategory(tableName, category.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public SystemHierarchyDefinitionService getSystemHierarchyDefinitionService() {
		return this.systemHierarchyDefinitionService;
	}


	public void setSystemHierarchyDefinitionService(SystemHierarchyDefinitionService systemHierarchyDefinitionService) {
		this.systemHierarchyDefinitionService = systemHierarchyDefinitionService;
	}
}
