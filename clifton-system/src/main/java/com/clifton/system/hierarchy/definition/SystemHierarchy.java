package com.clifton.system.hierarchy.definition;


import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;


/**
 * The <code>SystemHierarchy</code> defines the structure of the hierarchy for a category.
 * <p>
 * i.e. SystemHierarchyCategory: Geographical
 * Hierarchies:
 * USA
 * USA/MN
 * USA/IA
 * USA/MN/Minneapolis
 * <p>
 * Additional security condition is defined on the {@link SystemHierarchyCategory}
 *
 * @author manderson
 */
public class SystemHierarchy extends NamedHierarchicalEntity<SystemHierarchy, Short> implements SystemEntityModifyConditionAwareAdminRequired {

	private SystemHierarchyCategory category;

	/**
	 * Optional value field for a "short" representation.
	 * For new OMS will use this similar to SystemListItem "value" field to store the master identifier of the source data
	 */
	private String value;

	/**
	 * Optional order of his hierarchy node. Can be used for meaningful numeric 'order' codes.
	 */
	private Integer order;
	/**
	 * <i>System defined</i> entities are used by the system and have limited modifiable properties.
	 */
	private boolean systemDefined;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getMaxDepth() {
		return (getCategory() == null) ? null : getCategory().getMaxDepth();
	}


	public String getNameExpandedWithLabel() {
		if (getCategory() == null) {
			return null;
		}
		return getCategory().getName() + ": " + getNameExpanded();
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return (getCategory() == null) ? null : getCategory().getEntityModifyCondition();
	}


	@Override
	public boolean isEntityModifyConditionRequiredForNonAdmins() {
		return getCategory() != null && getCategory().isEntityModifyConditionRequiredForNonAdmins();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyCategory getCategory() {
		return this.category;
	}


	public void setCategory(SystemHierarchyCategory category) {
		this.category = category;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
}
