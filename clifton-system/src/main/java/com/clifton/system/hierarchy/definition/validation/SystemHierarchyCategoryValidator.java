package com.clifton.system.hierarchy.definition.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>SystemHierarchyCategoryValidator</code> contains all methods necessary to validate
 * a {@link SystemHierarchyCategory} prior to adding/editing/deleting from the database.
 *
 * @author manderson
 */
@Component
public class SystemHierarchyCategoryValidator extends SelfRegisteringDaoValidator<SystemHierarchyCategory> {

	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemHierarchyCategory category, DaoEventTypes config) throws ValidationException {
		// Cannot create a system defined category via the UI
		if (config.isInsert()) {
			ValidationUtils.assertFalse(category.isSystemDefined(), "Creating system defined categories is not permitted.");
		}
		// Cannot edit a system defined category
		else if (config.isUpdate()) {
			SystemHierarchyCategory originalCategory = getOriginalBean(category);
			List<String> changedFields = CoreCompareUtils.getNoEqualProperties(category, originalCategory, false, "description", "entityModifyCondition");
			if (!CollectionUtils.isEmpty(changedFields)) {
				if (category.isSystemDefined() || originalCategory.isSystemDefined()) {
					throw new ValidationException("Cannot update system defined Hierarchy Category. Only 'Description' and 'Modify Condition' fields can be updated.");
				}
			}
		}
		else if (config.isDelete()) {
			ValidationUtils.assertFalse(category.isSystemDefined(), "Deleting system defined categories is not permitted.");
			// Make sure Hierarchy Category isn't used prior to deleting
			if (!CollectionUtils.isEmpty(getSystemHierarchyAssignmentService().getSystemHierarchyAssignmentListByCategory(category.getId()))) {
				throw new ValidationException("This hierarchy category is currently assigned.  Cannot delete.");
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}
}
