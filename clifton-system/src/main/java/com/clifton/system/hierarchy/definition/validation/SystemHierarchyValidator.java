package com.clifton.system.hierarchy.definition.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.CoreValidationUtils;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemHierarchyValidator</code> contains all methods necessary to validate
 * a {@link SystemHierarchy} prior to adding/editing/deleting from the database.
 *
 * @author manderson
 */
@Component
public class SystemHierarchyValidator extends SelfRegisteringDaoValidator<SystemHierarchy> {

	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemHierarchy bean, DaoEventTypes config) throws ValidationException {
		validateSystemDefined(bean, config);
		validateHierarchyCategory(bean, config);
		validateHierarchyUsage(bean, config);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateSystemDefined(SystemHierarchy bean, DaoEventTypes config) {
		// Apply system-defined constraints
		if (config.isInsert()) {
			ValidationUtils.assertFalse(bean.isSystemDefined(), "Manual creation of \"System Defined\" entities is not allowed.", "systemDefined");
		}
		if (config.isUpdate()) {
			SystemHierarchy originalBean = getOriginalBean(bean);
			if (originalBean.isSystemDefined()) {
				CoreValidationUtils.assertDisallowedModifiedFields(originalBean, bean, "category", "name", "value", "parent", "systemDefined");
			}
			else {
				ValidationUtils.assertFalse(bean.isSystemDefined(), "Modifications to the \"System Defined\" field are not allowed.", "systemDefined");
			}
		}
		if (config.isDelete()) {
			ValidationUtils.assertFalse(bean.isSystemDefined(), "Deletion of \"System Defined\" entities is not allowed");
		}
	}


	private void validateHierarchyCategory(SystemHierarchy bean, DaoEventTypes config) {
		if (config.isInsert() || config.isUpdate()) {
			// make sure that parent belongs to the same category as this hierarchy
			SystemHierarchy parent = bean.getParent();
			if (parent != null && parent.getCategory() != null) {
				if (!bean.getCategory().equals(parent.getCategory())) {
					throw new FieldValidationException("Parent hierarchy category [" + parent.getCategory().getName() + "] and hierarchy category [" + bean.getCategory().getName()
							+ "] are different.  Hierarchies and their parents must belong to the same category.", "parent.id");
				}
			}

			// Validate hierarchy depth
			SystemHierarchyCategory cat = bean.getCategory();
			if (cat.getMaxDepth() != null) {
				ValidationUtils.assertTrue(bean.getLevel() <= cat.getMaxDepth(), () -> String.format("Hierarchies for category [%s] cannot be greater than [%d] levels deep.  This hierarchy is at level [%d]", cat.getName(), cat.getMaxDepth(), bean.getLevel()));
			}
		}
	}


	private void validateHierarchyUsage(SystemHierarchy bean, DaoEventTypes config) {
		if (config.isDelete()) {
			// Make sure Hierarchy isn't used prior to deleting
			if (!CollectionUtils.isEmpty(getSystemHierarchyAssignmentService().getSystemHierarchyLinkListByHierarchy(bean.getId()))) {
				throw new ValidationException("This hierarchy is currently linked to an entity.  Cannot delete.");
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}
}
