package com.clifton.system.hierarchy.assignment;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.system.hierarchy.assignment.search.SystemHierarchyLinkSearchForm;
import com.clifton.system.hierarchy.definition.SystemHierarchy;

import java.util.List;


/**
 * The <code>SystemHierarchyAssignmentService</code> defines methods for assigning and linking
 * Hierarchies to entities.
 *
 * @author manderson
 */
public interface SystemHierarchyAssignmentService {

	////////////////////////////////////////////////////////////////////////////
	//////      System Hierarchy Assignment Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignment getSystemHierarchyAssignment(short id);


	public SystemHierarchyAssignment getSystemHierarchyAssignmentByTableAndCategory(final String tableName, final short categoryId);


	public List<SystemHierarchyAssignment> getSystemHierarchyAssignmentList();


	public List<SystemHierarchyAssignment> getSystemHierarchyAssignmentListByCategory(short categoryId);


	public List<SystemHierarchyAssignment> getSystemHierarchyAssignmentListByTable(short tableId);


	public SystemHierarchyAssignment saveSystemHierarchyAssignment(SystemHierarchyAssignment bean);


	public void deleteSystemHierarchyAssignment(short id);

	////////////////////////////////////////////////////////////////////////////
	////////        System Hierarchy Link Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	public List<SystemHierarchyLink> getSystemHierarchyLinkList(SystemHierarchyLinkSearchForm searchForm);


	public List<SystemHierarchyLink> getSystemHierarchyLinkListByHierarchy(short hierarchyId);


	public List<SystemHierarchyLink> getSystemHierarchyLinkListByAssignment(short assignmentId);


	public List<SystemHierarchyLink> getSystemHierarchyLinkListByTableAndCategory(String tableName, String categoryName);


	/**
	 * Returns a list of all {@link SystemHierarchyLink} items associated with given table and hierarchy id.
	 * i.e. For Market Data Value look ups for Data Fields "tagged" by a specific hierarchy, we want the list of data field ids
	 * first, to pass those ids then to the value lookup.  Instead of looking through data fields that does a where exists or extra joins
	 * since we only need ids
	 */
	public List<SystemHierarchyLink> getSystemHierarchyLinkListByTableAndHierarchy(String tableName, short hierarchyId);


	/**
	 * Returns SystemHierarchy object associated with the specified entity for the specified category. Returns null if one is not found.
	 * This method can only be used for hierarchy assignments that have multipleValuesAllowed = false.
	 */
	public SystemHierarchy getSystemHierarchyByLink(String tableName, long fkFieldId, String hierarchyCategoryName);


	/**
	 * @param hierarchyCategoryName - optional to get links tied to a specific category
	 */
	public List<SystemHierarchyLink> getSystemHierarchyLinkList(final String tableName, final long fkFieldId, final String hierarchyCategoryName);


	/**
	 * @param hierarchyCategoryName - optional to save links tied to a specific category
	 */
	@SecureMethod(securityEvaluatorBeanName = "systemHierarchyAssignmentSecureMethodEvaluator", dynamicTableNameUrlParameter = "tableName", permissions = SecurityPermission.PERMISSION_WRITE)
	public void saveSystemHierarchyLinkList(final String tableName, final long fkFieldId, String hierarchyIdList, String hierarchyCategoryName);


	/**
	 * Moves the link from the specified old to new hierarchy.  Updates an existing link or inserts a new link.
	 */
	@SecureMethod(securityEvaluatorBeanName = "systemHierarchyAssignmentSecureMethodEvaluator", dynamicTableNameUrlParameter = "tableName")
	public void moveSystemHierarchyLink(String tableName, long fkFieldId, Short oldHierarchyId, Short newHierarchyId);


	@SecureMethod(securityResourceResolverBeanName = "systemHierarchyAssignmentSecureMethodResolver", dynamicTableNameUrlParameter = "tableName", permissions = SecurityPermission.PERMISSION_WRITE)
	public SystemHierarchyLink saveSystemHierarchyLink(String tableName, long fkFieldId, short hierarchyId);


	public void deleteSystemHierarchyLink(int id);


	@SecureMethod(securityEvaluatorBeanName = "systemHierarchyAssignmentSecureMethodEvaluator", dynamicTableNameUrlParameter = "tableName", permissions = SecurityPermission.PERMISSION_WRITE)
	public void deleteSystemHierarchyLinkList(String tableName, long fkFieldId);

	////////////////////////////////////////////////////////////////////////////
	////////        System Hierarchy Item Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a List of entities of the specified tableName type that are linked to the specified hierarchy.
	 * If the specified hierarchyId is null, then returns all entities of the specified type that don't
	 * belong to any hierarchy.
	 */
	@SecureMethod(securityResourceResolverBeanName = "systemHierarchyAssignmentSecureMethodResolver", dynamicTableNameUrlParameter = "tableName")
	public List<IdentityObject> getSystemHierarchyItemList(String tableName, Short hierarchyId);
}
