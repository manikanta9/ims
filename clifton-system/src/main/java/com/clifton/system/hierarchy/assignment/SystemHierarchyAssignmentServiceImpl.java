package com.clifton.system.hierarchy.assignment;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyListCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.hierarchy.assignment.search.SystemHierarchyLinkSearchForm;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import com.clifton.system.schema.SystemSchemaService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemHierarchyAssignmentServiceImpl</code> provides a basic implementation for the
 * {@link SystemHierarchyAssignmentService} interface.
 *
 * @author manderson
 */
@Service
public class SystemHierarchyAssignmentServiceImpl implements SystemHierarchyAssignmentService {

	private AdvancedUpdatableDAO<SystemHierarchyAssignment, Criteria> systemHierarchyAssignmentDAO;
	private AdvancedUpdatableDAO<SystemHierarchyLink, Criteria> systemHierarchyLinkDAO;

	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;
	private DaoCompositeKeyListCache<SystemHierarchyLink, Short, Long> systemHierarchyLinkByEntityCache;
	private DaoCompositeKeyCache<SystemHierarchyAssignment, String, Short> systemHierarchyAssignmentByTableAndCategoryCache;
	private SystemSchemaService systemSchemaService;

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	//////      System Hierarchy Assignment Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemHierarchyAssignment getSystemHierarchyAssignment(short id) {
		return getSystemHierarchyAssignmentDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemHierarchyAssignment getSystemHierarchyAssignmentByTableAndCategory(final String tableName, final short categoryId) {
		return getSystemHierarchyAssignmentByTableAndCategoryCache().getBeanForKeyValues(getSystemHierarchyAssignmentDAO(), tableName, categoryId);
	}


	@Override
	public List<SystemHierarchyAssignment> getSystemHierarchyAssignmentList() {
		return getSystemHierarchyAssignmentDAO().findAll();
	}


	@Override
	public List<SystemHierarchyAssignment> getSystemHierarchyAssignmentListByCategory(short categoryId) {
		return getSystemHierarchyAssignmentDAO().findByField("category.id", categoryId);
	}


	@Override
	public List<SystemHierarchyAssignment> getSystemHierarchyAssignmentListByTable(short tableId) {
		return getSystemHierarchyAssignmentDAO().findByField("table.id", tableId);
	}


	@Override
	public SystemHierarchyAssignment saveSystemHierarchyAssignment(SystemHierarchyAssignment bean) {
		return getSystemHierarchyAssignmentDAO().save(bean);
	}


	@Override
	public void deleteSystemHierarchyAssignment(short id) {
		getSystemHierarchyAssignmentDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////        System Hierarchy Link Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemHierarchyLink> getSystemHierarchyLinkList(SystemHierarchyLinkSearchForm searchForm) {
		return getSystemHierarchyLinkDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<SystemHierarchyLink> getSystemHierarchyLinkListByHierarchy(short hierarchyId) {
		return getSystemHierarchyLinkDAO().findByField("hierarchy.id", hierarchyId);
	}


	@Override
	public List<SystemHierarchyLink> getSystemHierarchyLinkListByAssignment(short assignmentId) {
		return getSystemHierarchyLinkDAO().findByField("assignment.id", assignmentId);
	}


	@Override
	public List<SystemHierarchyLink> getSystemHierarchyLinkListByTableAndCategory(String tableName, String categoryName) {
		SystemHierarchyAssignment assignment = getSystemHierarchyAssignmentByTableAndCategory(tableName, getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName(categoryName).getId());
		ValidationUtils.assertNotNull(assignment, "Cannot find System Hierarchy Assignment for Table [" + tableName + "] and category [" + categoryName + "]");
		return getSystemHierarchyLinkListByAssignment(assignment.getId());
	}


	@Override
	public List<SystemHierarchyLink> getSystemHierarchyLinkListByTableAndHierarchy(final String tableName, final short hierarchyId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			HibernateUtils.addEqualsRestriction("hierarchy.id", hierarchyId, criteria);
			HibernateUtils.addEqualsRestriction("assignment.table.name", tableName, criteria);
		};
		return getSystemHierarchyLinkDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public SystemHierarchy getSystemHierarchyByLink(String tableName, long fkFieldId, String hierarchyCategoryName) {
		List<SystemHierarchyLink> links = getSystemHierarchyLinkList(tableName, fkFieldId, hierarchyCategoryName);
		SystemHierarchyLink link = CollectionUtils.getOnlyElement(links);
		return (link == null) ? null : link.getHierarchy();
	}


	@Override
	public List<SystemHierarchyLink> getSystemHierarchyLinkList(final String tableName, final long fkFieldId, String hierarchyCategoryName) {
		List<SystemHierarchyLink> systemHierarchyLinkList = getSystemHierarchyLinkByEntityCache().getBeanListForKeyValues(getSystemHierarchyLinkDAO(), getSystemSchemaService().getSystemTableByName(tableName).getId(),
				fkFieldId);

		if (!StringUtils.isEmpty(hierarchyCategoryName)) {
			systemHierarchyLinkList = BeanUtils.filter(systemHierarchyLinkList, systemHierarchyLink -> systemHierarchyLink.getAssignment() == null ? null : systemHierarchyLink.getAssignment().getCategory().getName(), hierarchyCategoryName);
		}

		return systemHierarchyLinkList;
	}


	private List<SystemHierarchyLink> getSystemHierarchyLinkListByCategory(final String tableName, final long fkFieldId, final short categoryId) {
		List<SystemHierarchyLink> systemHierarchyLinkList = getSystemHierarchyLinkByEntityCache().getBeanListForKeyValues(getSystemHierarchyLinkDAO(), getSystemSchemaService().getSystemTableByName(tableName).getId(),
				fkFieldId);
		return BeanUtils.filter(systemHierarchyLinkList, systemHierarchyLink -> systemHierarchyLink.getAssignment() == null ? null : systemHierarchyLink.getAssignment().getCategory().getId(), categoryId);
	}


	@Override
	public void saveSystemHierarchyLinkList(final String tableName, final long fkFieldId, String hierarchyIdList, String hierarchyCategoryName) {

		List<String> hierarchyIds = StringUtils.delimitedStringToList(hierarchyIdList, ",");
		List<SystemHierarchyLink> existingList = getSystemHierarchyLinkList(tableName, fkFieldId, hierarchyCategoryName);
		List<SystemHierarchyLink> newList = new ArrayList<>();

		for (String hierarchyId : CollectionUtils.getIterable(hierarchyIds)) {
			if (StringUtils.isEmpty(hierarchyId) || StringUtils.isEqualIgnoreCase(hierarchyId, "null")) {
				continue;
			}
			SystemHierarchyLink existing = null;
			for (SystemHierarchyLink l : CollectionUtils.getIterable(existingList)) {
				if (l.getHierarchy().getId().equals(new Short(hierarchyId))) {
					existing = l;
					break;
				}
			}
			if (existing != null) {
				newList.add(existing);
			}
			else {
				SystemHierarchyLink link = new SystemHierarchyLink();
				link.setFkFieldId(fkFieldId);
				link.setHierarchy(getSystemHierarchyDefinitionService().getSystemHierarchy(new Integer(hierarchyId).shortValue()));
				link.setAssignment(getSystemHierarchyAssignmentByTableAndCategory(tableName, link.getHierarchy().getCategory().getId()));
				newList.add(link);
			}
		}
		getSystemHierarchyLinkDAO().saveList(newList, existingList);
	}


	@Override
	public SystemHierarchyLink saveSystemHierarchyLink(String tableName, long fkFieldId, short hierarchyId) {
		SystemHierarchy hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchy(hierarchyId);
		SystemHierarchyAssignment assign = getSystemHierarchyAssignmentByTableAndCategory(tableName, hierarchy.getCategory().getId());
		if (!assign.isMultipleValuesAllowed()) {
			if (!CollectionUtils.isEmpty(getSystemHierarchyLinkListByCategory(tableName, fkFieldId, hierarchy.getCategory().getId()))) {
				throw new ValidationException("There is already a value selected for category [" + hierarchy.getCategory().getName() + "].  Assignment rules allow only one value for this category.");
			}
		}

		SystemHierarchyLink link = new SystemHierarchyLink();
		link.setAssignment(assign);
		link.setFkFieldId(fkFieldId);
		link.setHierarchy(hierarchy);
		getSystemHierarchyLinkDAO().save(link);
		return link;
	}


	@Override
	@Transactional
	public void moveSystemHierarchyLink(String tableName, long fkFieldId, Short oldHierarchyId, Short newHierarchyId) {
		SystemHierarchy newHierarchy = (newHierarchyId == null) ? null : getSystemHierarchyDefinitionService().getSystemHierarchy(newHierarchyId);
		if (oldHierarchyId != null) {
			// if not moving from the root, then update the hierarchy of the old link
			SystemHierarchy oldHierarchy = getSystemHierarchyDefinitionService().getSystemHierarchy(oldHierarchyId);
			List<SystemHierarchyLink> links = getSystemHierarchyLinkListByCategory(tableName, fkFieldId, oldHierarchy.getCategory().getId());
			ValidationUtils.assertNotEmpty(links, "Cannot find hierarchy link for table '" + tableName + "', hierarchyId = " + oldHierarchyId + " and FK Field = " + fkFieldId);
			if (CollectionUtils.getSize(links) > 1) {
				throw new ValidationException("Expected one hierarchy link for table '" + tableName + "', hierarchyId = " + oldHierarchyId + " and FK Field = " + fkFieldId + " but found "
						+ CollectionUtils.getSize(links));
			}

			SystemHierarchyLink link = links.get(0);
			if (newHierarchy == null) {
				// moving to the root: delete existing link
				getSystemHierarchyLinkDAO().delete(link.getId());
			}
			else {
				// make sure the move is not across different categories
				if (!oldHierarchy.getCategory().equals(newHierarchy.getCategory())) {
					throw new ValidationException("Cannot move an item across two different hierarchy categories: from " + oldHierarchy.getCategory().getLabel() + " to "
							+ newHierarchy.getCategory().getLabel());
				}

				link.setHierarchy(newHierarchy);
				getSystemHierarchyLinkDAO().save(link);
			}
		}
		else if (newHierarchy != null) {
			// insert the first link for the entity (move from the root)
			SystemHierarchyAssignment assignment = getSystemHierarchyAssignmentByTableAndCategory(tableName, newHierarchy.getCategory().getId());

			SystemHierarchyLink link = new SystemHierarchyLink();
			link.setHierarchy(newHierarchy);
			link.setAssignment(assignment);
			link.setFkFieldId(fkFieldId);
			getSystemHierarchyLinkDAO().save(link);
		}
	}


	@Override
	public void deleteSystemHierarchyLink(int id) {
		getSystemHierarchyLinkDAO().delete(id);
	}


	@Override
	public void deleteSystemHierarchyLinkList(String tableName, long fkFieldId) {
		getSystemHierarchyLinkDAO().deleteList(getSystemHierarchyLinkList(tableName, fkFieldId, null));
	}

	////////////////////////////////////////////////////////////////////////////
	////////        System Hierarchy Item Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * NOTE CAN BE RE-FACTORED/REMOVED SO ALL INSTANCES USE THE SEARCH FORM AND SEARCH LIST ON
	 * IT'S OWN (SEE systemQueryListFind) for an example, but since this can be used for most of the generic cases, will leave as is
	 */
	@Override
	public List<IdentityObject> getSystemHierarchyItemList(final String tableName, final Short hierarchyId) {
		AdvancedReadOnlyDAO<IdentityObject, Criteria> dao = (AdvancedReadOnlyDAO<IdentityObject, Criteria>) getDaoLocator().locate(tableName);
		HibernateSearchConfigurer searchConfigurer;
		if (hierarchyId == null) {
			// get all entities that haven't been assigned to the specified table
			searchConfigurer = criteria -> {
				DetachedCriteria sub = DetachedCriteria.forClass(SystemHierarchyLink.class, "l");
				sub.setProjection(Projections.property("id"));
				sub.createCriteria("assignment", "a").createAlias("table", "t").add(Restrictions.eq("t.name", tableName));
				sub.add(Restrictions.eqProperty("l.fkFieldId", criteria.getAlias() + ".id"));

				criteria.add(Subqueries.notExists(sub));
			};
		}
		else {
			// get all entities that are assigned to the specified hierarchy
			searchConfigurer = criteria -> {
				DetachedCriteria hierarchyItemIds = DetachedCriteria.forClass(SystemHierarchyLink.class, "l");
				hierarchyItemIds.setProjection(Projections.property("fkFieldId"));
				hierarchyItemIds.add(Restrictions.eq("l.hierarchy.id", hierarchyId));
				hierarchyItemIds.createCriteria("assignment", "a").createAlias("table", "t").add(Restrictions.eq("t.name", tableName));

				criteria.add(Subqueries.propertyIn("id", hierarchyItemIds));
			};
		}
		return dao.findBySearchCriteria(searchConfigurer);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemHierarchyAssignment, Criteria> getSystemHierarchyAssignmentDAO() {
		return this.systemHierarchyAssignmentDAO;
	}


	public void setSystemHierarchyAssignmentDAO(AdvancedUpdatableDAO<SystemHierarchyAssignment, Criteria> systemHierarchyAssignmentDAO) {
		this.systemHierarchyAssignmentDAO = systemHierarchyAssignmentDAO;
	}


	public AdvancedUpdatableDAO<SystemHierarchyLink, Criteria> getSystemHierarchyLinkDAO() {
		return this.systemHierarchyLinkDAO;
	}


	public void setSystemHierarchyLinkDAO(AdvancedUpdatableDAO<SystemHierarchyLink, Criteria> systemHierarchyLinkDAO) {
		this.systemHierarchyLinkDAO = systemHierarchyLinkDAO;
	}


	public SystemHierarchyDefinitionService getSystemHierarchyDefinitionService() {
		return this.systemHierarchyDefinitionService;
	}


	public void setSystemHierarchyDefinitionService(SystemHierarchyDefinitionService systemHierarchyDefinitionService) {
		this.systemHierarchyDefinitionService = systemHierarchyDefinitionService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public DaoCompositeKeyListCache<SystemHierarchyLink, Short, Long> getSystemHierarchyLinkByEntityCache() {
		return this.systemHierarchyLinkByEntityCache;
	}


	public void setSystemHierarchyLinkByEntityCache(DaoCompositeKeyListCache<SystemHierarchyLink, Short, Long> systemHierarchyLinkByEntityCache) {
		this.systemHierarchyLinkByEntityCache = systemHierarchyLinkByEntityCache;
	}


	public DaoCompositeKeyCache<SystemHierarchyAssignment, String, Short> getSystemHierarchyAssignmentByTableAndCategoryCache() {
		return this.systemHierarchyAssignmentByTableAndCategoryCache;
	}


	public void setSystemHierarchyAssignmentByTableAndCategoryCache(DaoCompositeKeyCache<SystemHierarchyAssignment, String, Short> systemHierarchyAssignmentByTableAndCategoryCache) {
		this.systemHierarchyAssignmentByTableAndCategoryCache = systemHierarchyAssignmentByTableAndCategoryCache;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
