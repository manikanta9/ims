package com.clifton.system.hierarchy.definition.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class SystemHierarchyCategorySearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Integer maxDepth;

	@SearchField
	private Boolean inactive;

	@SearchField(searchField = "entityModifyCondition.id")
	private Integer entityModifyConditionId;

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Integer getMaxDepth() {
		return this.maxDepth;
	}


	public void setMaxDepth(Integer maxDepth) {
		this.maxDepth = maxDepth;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public Integer getEntityModifyConditionId() {
		return this.entityModifyConditionId;
	}


	public void setEntityModifyConditionId(Integer entityModifyConditionId) {
		this.entityModifyConditionId = entityModifyConditionId;
	}
}
