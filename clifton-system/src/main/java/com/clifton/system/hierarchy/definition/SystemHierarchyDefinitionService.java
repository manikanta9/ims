package com.clifton.system.hierarchy.definition;


import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import com.clifton.system.hierarchy.definition.search.SystemHierarchyCategorySearchForm;
import com.clifton.system.hierarchy.definition.search.SystemHierarchySearchForm;

import java.util.List;


/**
 * The <code>SystemHierarchyDefinitionService</code> defines the methods necessary to create/delete and retrieve
 * {@link SystemHierarchyCategory} and {@link SystemHierarchy} objects
 *
 * @author manderson
 */
public interface SystemHierarchyDefinitionService {

	//////////////////////////////////////////////////////////////////////////// 
	//////       System Hierarchy Category Business Methods            ///////// 
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyCategory getSystemHierarchyCategory(short id);


	/**
	 * Note: Throws an exception if not found
	 */
	public SystemHierarchyCategory getSystemHierarchyCategoryByName(String name);


	/**
	 * Returns the SystemHierarchyCategory assigned to the specified table.
	 * Throws exception if more than one category is returned.
	 */
	public SystemHierarchyCategory getSystemHierarchyCategoryByTable(final String tableName);


	public List<SystemHierarchyCategory> getSystemHierarchyCategoryList(SystemHierarchyCategorySearchForm searchForm);


	public SystemHierarchyCategory saveSystemHierarchyCategory(SystemHierarchyCategory bean);


	/**
	 * Deletes the specified category and all hierarchies tied to this category.
	 * <p>
	 * Note: Delete will fail if category is being used by any entities in {@link SystemHierarchyLink}
	 */
	public void deleteSystemHierarchyCategory(short id);


	//////////////////////////////////////////////////////////////////////////// 
	////////         System Hierarchy Business Methods                ////////// 
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchy getSystemHierarchy(short id);


	public SystemHierarchy getSystemHierarchyByCategoryAndNameExpanded(String categoryName, String name);


	public List<SystemHierarchy> getSystemHierarchyList(SystemHierarchySearchForm searchForm);


	public SystemHierarchy saveSystemHierarchy(SystemHierarchy bean);


	/**
	 * Deletes the specified system hierarchy and all children, grandchildren, etc.
	 * <p>
	 * Note: Delete will fail if hierarchy or any of its children are tied to any entities in {@link SystemHierarchyLink}
	 */
	public void deleteSystemHierarchy(short id);
}
