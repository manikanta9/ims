package com.clifton.system.hierarchy;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;


/**
 * The <code>SystemHierarchyObserver</code> class is a {@link DaoEventObserver} that triggers
 * updating {@link SystemHierarchyLink} information for a given bean.
 *
 * @param <T>
 * @author manderson
 */
public class SystemHierarchyObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;
	private final String tableName;


	public SystemHierarchyObserver(String tableName) {
		AssertUtils.assertNotNull(tableName, "Required argument 'tableName' cannot be null.");
		this.tableName = tableName;
	}


	/**
	 * To check if two SystemHierarchyObservers are equal, check if the tablenames are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SystemHierarchyObserver<?>)) {
			return false;
		}
		return this.tableName.equals(((SystemHierarchyObserver<?>) obj).tableName);
	}


	@Override
	public int hashCode() {
		return this.tableName.hashCode();
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			if (event.isDelete()) {
				getSystemHierarchyAssignmentService().deleteSystemHierarchyLinkList(dao.getConfiguration().getTableName(), BeanUtils.getIdentityAsInteger(bean));
			}
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// NOTHING HERE FOR NOW
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}
}
