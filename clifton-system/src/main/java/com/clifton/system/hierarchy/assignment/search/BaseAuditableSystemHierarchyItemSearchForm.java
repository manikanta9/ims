package com.clifton.system.hierarchy.assignment.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.EntityAuditFieldSearchForm;

import java.util.Date;


/**
 * The <code>BaseAuditableSystemHierarchyItemSearchForm</code> class extends the {@link BaseSystemHierarchyItemSearchForm} to include the standard audit fields.
 *
 * @author MikeH
 */
public abstract class BaseAuditableSystemHierarchyItemSearchForm extends BaseSystemHierarchyItemSearchForm implements EntityAuditFieldSearchForm {

	@SearchField(dateFieldIncludesTime = true)
	private Date createDate;

	@SearchField
	private Short createUserId;

	@SearchField(dateFieldIncludesTime = true)
	private Date updateDate;

	@SearchField
	private Short updateUserId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getCreateDate() {
		return this.createDate;
	}


	@Override
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	@Override
	public Short getCreateUserId() {
		return this.createUserId;
	}


	@Override
	public void setCreateUserId(Short createUserId) {
		this.createUserId = createUserId;
	}


	@Override
	public Date getUpdateDate() {
		return this.updateDate;
	}


	@Override
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	@Override
	public Short getUpdateUserId() {
		return this.updateUserId;
	}


	@Override
	public void setUpdateUserId(Short updateUserId) {
		this.updateUserId = updateUserId;
	}
}

