package com.clifton.system.hierarchy.assignment.search;


import com.clifton.core.dataaccess.search.SearchFormCustomConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * The <code>SystemHierarchyItemSearchFormConfigurer</code> provides a custom filtering used by search forms that implement
 * {@link SystemHierarchyItemSearchForm} to append proper filters.  These are used for cases that use folders are or tags (hierarchies)
 * <p>
 * NOTE: This search form configurer is AUTOMATICALLY registered for any search form that implements {@link SystemHierarchyItemSearchForm}
 *
 * @author manderson
 */
@Component
public class SystemHierarchyItemSearchFormConfigurer implements SearchFormCustomConfigurer<SystemHierarchyItemSearchForm> {

	@Override
	public Class<SystemHierarchyItemSearchForm> getSearchFormInterfaceClassName() {
		return SystemHierarchyItemSearchForm.class;
	}


	@Override
	public void configureCriteria(Criteria criteria, SystemHierarchyItemSearchForm searchForm, Map<String, Criteria> processedPathToCriteria) {
		addCategorySearchCriteria(criteria, processedPathToCriteria,
				StringUtils.coalesce(false, searchForm.getCategoryTableName(), searchForm.getDaoTableName()),
				searchForm.getCategoryName(),
				ArrayUtils.wrapCoalescing(searchForm.getCategoryHierarchyIds(), searchForm.getCategoryHierarchyId(), Short.class),
				ArrayUtils.wrapCoalescing(searchForm.getCategoryHierarchyNames(), searchForm.getCategoryHierarchyName(), String.class),
				searchForm.getCategoryIncludeAllIfNull(),
				searchForm.getCategoryLinkFieldPath());
		addCategorySearchCriteria(criteria, processedPathToCriteria,
				StringUtils.coalesce(false, searchForm.getCategory2TableName(), searchForm.getDaoTableName()),
				searchForm.getCategory2Name(),
				ArrayUtils.wrapCoalescing(searchForm.getCategory2HierarchyIds(), searchForm.getCategory2HierarchyId(), Short.class),
				ArrayUtils.wrapCoalescing(searchForm.getCategory2HierarchyNames(), searchForm.getCategory2HierarchyName(), String.class),
				searchForm.getCategory2IncludeAllIfNull(),
				searchForm.getCategory2LinkFieldPath());
		addCategorySearchCriteria(criteria, processedPathToCriteria,
				StringUtils.coalesce(false, searchForm.getCategory3TableName(), searchForm.getDaoTableName()),
				searchForm.getCategory3Name(),
				ArrayUtils.wrapCoalescing(searchForm.getCategory3HierarchyIds(), searchForm.getCategory3HierarchyId(), Short.class),
				ArrayUtils.wrapCoalescing(searchForm.getCategory3HierarchyNames(), searchForm.getCategory3HierarchyName(), String.class),
				searchForm.getCategory3IncludeAllIfNull(),
				searchForm.getCategory3LinkFieldPath());
	}


	private void addCategorySearchCriteria(Criteria criteria, Map<String, Criteria> processedPathToCriteria, String tableName, String categoryName, Short[] hierarchyIds, String[] hierarchyNames, Boolean includeAllIfNull, String linkFieldPath) {
		// If hierarchyId is null
		if (ArrayUtils.isEmpty(hierarchyIds) && ArrayUtils.isEmpty(hierarchyNames)) {
			// and include all if null or no category passed - no filtering needed
			if ((includeAllIfNull != null && includeAllIfNull) || StringUtils.isEmpty(categoryName)) {
				return;
			}
			// Otherwise - include all unassigned to the category
			DetachedCriteria sub = DetachedCriteria.forClass(SystemHierarchyLink.class, "l");
			sub.setProjection(Projections.property("id"));
			sub.createCriteria("assignment", "a").createAlias("table", "t").add(Restrictions.eq("t.name", tableName));
			sub.createCriteria("a.category", "c").add(Restrictions.eq("c.name", categoryName));

			String fkAlias = criteria.getAlias();
			if (!StringUtils.isEmpty(linkFieldPath)) {
				fkAlias = HibernateUtils.getPathAlias(linkFieldPath, criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, JoinType.INNER_JOIN, processedPathToCriteria);
			}

			sub.add(Restrictions.eqProperty("l.fkFieldId", fkAlias + ".id"));
			criteria.add(Subqueries.notExists(sub));
		}
		else {
			// get all entities that are assigned to the specified hierarchy
			DetachedCriteria hierarchyItemIds = DetachedCriteria.forClass(SystemHierarchyLink.class, "l");
			hierarchyItemIds.setProjection(Projections.property("fkFieldId"));
			hierarchyItemIds.createCriteria("assignment", "a").createAlias("table", "t").add(Restrictions.eq("t.name", tableName));
			if (!ArrayUtils.isEmpty(hierarchyIds)) {
				hierarchyItemIds.add(Restrictions.in("l.hierarchy.id", hierarchyIds));
			}
			else {
				hierarchyItemIds.createCriteria("l.hierarchy", "h").add(Restrictions.in("h.name", hierarchyNames));
				hierarchyItemIds.createCriteria("a.category", "c").add(Restrictions.eq("c.name", categoryName));
			}

			String fkAlias = criteria.getAlias();
			if (!StringUtils.isEmpty(linkFieldPath)) {
				fkAlias = HibernateUtils.getPathAlias(linkFieldPath, criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, JoinType.INNER_JOIN, processedPathToCriteria);
			}
			criteria.add(Subqueries.propertyIn(fkAlias + ".id", hierarchyItemIds));
		}
	}
}
