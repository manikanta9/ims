package com.clifton.system.hierarchy;


import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignment;
import com.clifton.system.hierarchy.definition.SystemHierarchy;


/**
 * The <code>SystemHierarchyRegistrator</code> interface defines methods that help register/unregister observers
 * to corresponding DAO's based on {@link SystemHierarchy} data.
 *
 * @author manderson
 */
public interface SystemHierarchyRegistrator {

	/**
	 * Registers observers enabled by {@link SystemHierarchyAssignment} for the specified table's DAO.
	 */
	public void registerObservers(String tableName);
}
