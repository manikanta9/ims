package com.clifton.system.hierarchy;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignment;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.List;


/**
 * The <code>SystemHierarchyRegistratorImpl</code> class is a Spring {@link ApplicationListener} that
 * registers {@link SystemHierarchyObserver} for DAO's whose table exists
 * in the {@link SystemHierarchyAssignment} table.
 *
 * @author manderson
 */
public class SystemHierarchyRegistratorImpl<T extends IdentityObject> implements SystemHierarchyRegistrator, CurrentContextApplicationListener<ContextRefreshedEvent> {

	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	private DaoLocator daoLocator;


	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			List<SystemHierarchyAssignment> list = getSystemHierarchyAssignmentService().getSystemHierarchyAssignmentList();
			for (SystemHierarchyAssignment assign : CollectionUtils.getIterable(list)) {
				// skip inactive  categories
				if (!assign.getCategory().isInactive()) {
					registerObservers(assign.getTable().getName());
				}
			}
		}
		catch (Throwable e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error registering Hierarchy observers: " + event, e);
		}
	}


	@Override
	@SuppressWarnings("unchecked")
	public void registerObservers(String tableName) {
		// get DAO that's being observed
		ObserverableDAO<T> dao = (ObserverableDAO<T>) getDaoLocator().locate(tableName);
		DaoEventObserver<T> observer = new SystemHierarchyObserver<>(tableName);
		getApplicationContext().getAutowireCapableBeanFactory().autowireBeanProperties(observer, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		// register observers as necessary
		dao.registerEventObserver(DaoEventTypes.DELETE, observer);
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}
}
