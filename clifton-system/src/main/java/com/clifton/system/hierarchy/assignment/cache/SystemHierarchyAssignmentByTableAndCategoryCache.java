package com.clifton.system.hierarchy.assignment.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignment;
import org.springframework.stereotype.Component;


/**
 * The {@link SystemHierarchyAssignmentByTableAndCategoryCache} class provides caching and retrieval from cache of {@link SystemHierarchyAssignment}
 * objects by tableName and SystemHierarchyCategoryId.
 *
 * @author lnaylor
 */
@Component
public class SystemHierarchyAssignmentByTableAndCategoryCache extends SelfRegisteringCompositeKeyDaoCache<SystemHierarchyAssignment, String, Short> {

	@Override
	protected String getBeanKey1Property() {
		return "table.name";
	}


	@Override
	protected String getBeanKey2Property() {
		return "category.id";
	}


	@Override
	protected String getBeanKey1Value(SystemHierarchyAssignment bean) {
		if (bean != null && bean.getTable() != null) {
			return bean.getTable().getName();
		}
		return null;
	}


	@Override
	protected Short getBeanKey2Value(SystemHierarchyAssignment bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getCategory());
		}
		return null;
	}
}
