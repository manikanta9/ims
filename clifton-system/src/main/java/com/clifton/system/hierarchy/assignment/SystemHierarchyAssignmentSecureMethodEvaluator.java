package com.clifton.system.hierarchy.assignment;

import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecureMethodSecurityEvaluator;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.web.MethodSecurityHandler;
import com.clifton.security.web.mvc.SecurityMethodToResourceCache;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The {@link SystemHierarchyAssignmentSecureMethodEvaluator} is used to check permissions for {@link SystemHierarchyAssignmentService} methods that
 * need to check against multiple {@link com.clifton.core.security.authorization.SecurityResource} objects.
 *
 * @author lnaylor
 */
@Component
public class SystemHierarchyAssignmentSecureMethodEvaluator implements SecureMethodSecurityEvaluator {

	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	private MethodSecurityHandler methodSecurityHandler;

	private SecurityAuthorizationService securityAuthorizationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void checkPermissions(SecureMethod secureAnnotation, Method method, Map<String, ?> config) {

		List<String> securityResourceList = getSecurityResourceList(method, config);

		for (String securityResource : securityResourceList) {
			int requiredPermissions = getMethodSecurityHandler().getRequiredPermissions(method, method.getName(), secureAnnotation, config, securityResource);

			// check if current user is allowed to access the specified resource
			if (SecurityMethodToResourceCache.RESOURCE_NOT_MAPPED.equals(securityResource)) {
				throw new AccessDeniedException("UNMAPPED RESOURCE for method: " + method.getName(), requiredPermissions);
			}
			if (!getSecurityAuthorizationService().isSecurityAccessAllowed(securityResource, requiredPermissions)) {
				throw new AccessDeniedException(securityResource, requiredPermissions);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<String> getSecurityResourceList(Method method, Map<String, ?> config) {
		List<String> securityResourceList = new ArrayList<>();
		String methodName = method.getName();
		String tableName = MapUtils.getParameterAsString("tableName", config);
		AssertUtils.assertNotEmpty(tableName, "Cannot get Security Resource List. No tableName in request to [%s]", methodName);

		switch (methodName) {
			case "moveSystemHierarchyLink":
				Short newHierarchyId = MapUtils.getParameterAsShort("newHierarchyId", config, 0);
				if (newHierarchyId != null) {
					String resourceName = getSecurityResourceName(getSystemHierarchyAssignmentFromHierarchyId(newHierarchyId, tableName));
					securityResourceList.add(resourceName);
				}
				Short oldHierarchyId = MapUtils.getParameterAsShort("oldHierarchyId", config, 0);
				if (oldHierarchyId != null) {
					String resourceName = getSecurityResourceName(getSystemHierarchyAssignmentFromHierarchyId(oldHierarchyId, tableName));
					securityResourceList.add(resourceName);
				}
				AssertUtils.assertNotEmpty(securityResourceList, "Cannot get Security Resource List. Both newHierarchyId and oldHierarchyId missing in request to [%s]", methodName);
				break;
			case "deleteSystemHierarchyLinkList":
				Long fkFieldId = MapUtils.getParameterAsLong("fkFieldId", config);
				AssertUtils.assertNotNull(fkFieldId, "Cannot get Security Resource List. No fkFieldId in request to [%s]", methodName);
				List<SystemHierarchyLink> systemHierarchyLinkList = getSystemHierarchyAssignmentService().getSystemHierarchyLinkList(tableName, fkFieldId, null);
				for (SystemHierarchyLink link : CollectionUtils.getIterable(systemHierarchyLinkList)) {
					securityResourceList.add(getSecurityResourceName(link.getAssignment()));
				}
				break;
			case "saveSystemHierarchyLinkList":
				String hierarchyIdListString = MapUtils.getParameterAsString("hierarchyIdList", config);
				List<String> hierarchyIdList = StringUtils.delimitedStringToList(hierarchyIdListString, ",");
				for (String hierarchyId : CollectionUtils.getIterable(hierarchyIdList)) {
					if (StringUtils.isEmpty(hierarchyId) || StringUtils.isEqualIgnoreCase(hierarchyId, "null")) {
						continue;
					}
					String resourceName = getSecurityResourceName(getSystemHierarchyAssignmentFromHierarchyId(new Short(hierarchyId), tableName));
					securityResourceList.add(resourceName);
				}
				break;
			default:
				throw new UnsupportedOperationException(String.format("Cannot get SecurityResource. Method [%s] is not supported by [%s]", methodName, getClass().getSimpleName()));
		}

		return CollectionUtils.getDistinct(securityResourceList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemHierarchyAssignment getSystemHierarchyAssignmentFromHierarchyId(Short hierarchyId, String tableName) {
		SystemHierarchy hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchy(hierarchyId);
		AssertUtils.assertNotNull(hierarchy, "Cannot get Security Resource. No SystemHierarchy found with id [%s]", hierarchyId);
		SystemHierarchyAssignment assignment = getSystemHierarchyAssignmentService().getSystemHierarchyAssignmentByTableAndCategory(tableName, hierarchy.getCategory().getId());
		AssertUtils.assertNotNull(assignment, "Cannot get Security Resource. No SystemHierarchyAssignment found for tableName [%s] and categoryId [%d]", tableName, hierarchy.getCategory().getId());
		return assignment;
	}


	private String getSecurityResourceName(SystemHierarchyAssignment assignment) {
		if (assignment.getSecurityResource() != null) {
			return assignment.getSecurityResource().getName();
		}
		return assignment.getTable().getSecurityResource().getName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public SystemHierarchyDefinitionService getSystemHierarchyDefinitionService() {
		return this.systemHierarchyDefinitionService;
	}


	public void setSystemHierarchyDefinitionService(SystemHierarchyDefinitionService systemHierarchyDefinitionService) {
		this.systemHierarchyDefinitionService = systemHierarchyDefinitionService;
	}


	public MethodSecurityHandler getMethodSecurityHandler() {
		return this.methodSecurityHandler;
	}


	public void setMethodSecurityHandler(MethodSecurityHandler methodSecurityHandler) {
		this.methodSecurityHandler = methodSecurityHandler;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
