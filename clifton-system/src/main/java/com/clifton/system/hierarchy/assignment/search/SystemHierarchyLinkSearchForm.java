package com.clifton.system.hierarchy.assignment.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class SystemHierarchyLinkSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;


	@SearchField(searchField = "assignment.id")
	private Short assignmentId;

	@SearchField(searchField = "assignment.category.id")
	private Short categoryId;

	@SearchField(searchField = "assignment.table.id")
	private Short tableId;

	@SearchField(searchField = "name", searchFieldPath = "assignment.table", comparisonConditions = ComparisonConditions.EQUALS)
	private String tableName;


	@SearchField(searchField = "hierarchy.id")
	private Short hierarchyId;

	// Max Levels Deep is Currently 6
	@SearchField(searchField = "hierarchy.parent.parent.parent.parent.parent.name,hierarchy.parent.parent.parent.parent.name,hierarchy.parent.parent.parent.name,hierarchy.parent.parent.name,hierarchy.parent.name,hierarchy.name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String hierarchyNameExpanded;

	@SearchField
	private Long fkFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getAssignmentId() {
		return this.assignmentId;
	}


	public void setAssignmentId(Short assignmentId) {
		this.assignmentId = assignmentId;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Short getHierarchyId() {
		return this.hierarchyId;
	}


	public void setHierarchyId(Short hierarchyId) {
		this.hierarchyId = hierarchyId;
	}


	public String getHierarchyNameExpanded() {
		return this.hierarchyNameExpanded;
	}


	public void setHierarchyNameExpanded(String hierarchyNameExpanded) {
		this.hierarchyNameExpanded = hierarchyNameExpanded;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}
}
