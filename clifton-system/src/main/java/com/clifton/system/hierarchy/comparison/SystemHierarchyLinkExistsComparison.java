package com.clifton.system.hierarchy.comparison;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;

import java.util.List;


/**
 * The IsSystemHierarchyLinkExists Comparison checks for a selected category and hierarchy if that hierarchy is assigned to the given entity {@link com.clifton.system.hierarchy.assignment.SystemHierarchyLink}
 *
 * @author manderson
 */
public class SystemHierarchyLinkExistsComparison implements Comparison<IdentityObject> {

	private DaoLocator daoLocator;
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;
	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Category for the selected hierarchy.  Not used for evaluation, but for UI screen selection to filter hierarchy selections, so required.
	 */
	private Short systemHierarchyCategoryId;
	private Short systemHierarchyId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected boolean isExistsComparison() {
		return true;
	}


	@SuppressWarnings("unchecked")
	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		ValidationUtils.assertNotNull(getSystemHierarchyId(), "System Hierarchy selection is required.");
		SystemHierarchy hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchy(getSystemHierarchyId());

		Integer beanId = (Integer) bean.getIdentity();
		ValidationUtils.assertNotNull(beanId, "Cannot check system hierarchy links for a bean that does not exist: " + bean, "id");
		String tableName = getDaoLocator().locate((Class<IdentityObject>) bean.getClass()).getConfiguration().getTableName();

		List<SystemHierarchyLink> linkList = getSystemHierarchyAssignmentService().getSystemHierarchyLinkList(tableName, beanId, hierarchy.getCategory().getName());
		boolean result = false;
		for (SystemHierarchyLink link : CollectionUtils.getIterable(linkList)) {
			if (link.getHierarchy().equals(hierarchy)) {
				result = true;
				break;
			}
		}

		if (!isExistsComparison()) {
			result = !result;
		}

		// record comparison result message
		if (context != null) {
			String msgPrefix = "(System Hierarchy Link to [" + hierarchy.getNameExpandedWithLabel() + "]";
			if (result) {
				context.recordTrueMessage(msgPrefix + (isExistsComparison() ? " Exists)" : " Does Not Exist)"));
			}
			else {
				context.recordFalseMessage(msgPrefix + (isExistsComparison() ? " Does Not Exist)" : " Exists)"));
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public SystemHierarchyDefinitionService getSystemHierarchyDefinitionService() {
		return this.systemHierarchyDefinitionService;
	}


	public void setSystemHierarchyDefinitionService(SystemHierarchyDefinitionService systemHierarchyDefinitionService) {
		this.systemHierarchyDefinitionService = systemHierarchyDefinitionService;
	}


	public Short getSystemHierarchyCategoryId() {
		return this.systemHierarchyCategoryId;
	}


	public void setSystemHierarchyCategoryId(Short systemHierarchyCategoryId) {
		this.systemHierarchyCategoryId = systemHierarchyCategoryId;
	}


	public Short getSystemHierarchyId() {
		return this.systemHierarchyId;
	}


	public void setSystemHierarchyId(Short systemHierarchyId) {
		this.systemHierarchyId = systemHierarchyId;
	}
}
