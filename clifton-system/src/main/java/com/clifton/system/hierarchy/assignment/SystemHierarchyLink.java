package com.clifton.system.hierarchy.assignment;


import com.clifton.core.beans.BaseEntity;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.usedby.softlink.SoftLinkField;


/**
 * The <code>SystemHierarchyLink</code> ties a specified entity to a hierarchy.
 * <p>
 * i.e. Clifton Group Company is tied to Hierarchy:
 * Category: Geographical
 * Value: USA/Mn/Minneapolis Area/Edina
 *
 * @author manderson
 */
public class SystemHierarchyLink extends BaseEntity<Integer> {

	private SystemHierarchyAssignment assignment;
	private SystemHierarchy hierarchy;

	@SoftLinkField(ownerBeanPropertyName = "hierarchy", tableBeanPropertyName = "assignment.table")
	private long fkFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignment getAssignment() {
		return this.assignment;
	}


	public void setAssignment(SystemHierarchyAssignment assignment) {
		this.assignment = assignment;
	}


	public SystemHierarchy getHierarchy() {
		return this.hierarchy;
	}


	public void setHierarchy(SystemHierarchy hierarchy) {
		this.hierarchy = hierarchy;
	}


	public long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}
}
