package com.clifton.system.condition;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.condition.search.SystemConditionSearchForm;

import java.util.List;


/**
 * The <code>SystemConditionService</code> interface defines methods for working with system conditions.
 *
 * @author vgomelsky
 */
public interface SystemConditionService {

	////////////////////////////////////////////////////////////////////////////
	////////          System Condition Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemCondition getSystemCondition(int id);


	/**
	 * Returns SystemCondition for the specified id which is recursively populated with all of its entries.
	 */
	@SecureMethod(dtoClass = SystemConditionEntry.class)
	public SystemCondition getSystemConditionPopulated(int id);


	public SystemCondition getSystemConditionByName(String name);


	public List<SystemCondition> getSystemConditionList(SystemConditionSearchForm searchForm);


	/**
	 * The method requires that the specified SystemCondition has SystemConditionEntry set.
	 * It will recursively create the entries (but not beans on those CONDITION entries).
	 */
	public SystemCondition saveSystemCondition(SystemCondition condition);


	/**
	 * Deletes SystemCondition with the specified id.
	 * Also deletes all child entries recursively.
	 */
	public void deleteSystemCondition(int id);


	////////////////////////////////////////////////////////////////////////////
	////////       System Condition Entry Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns fully populated SystemConditionEntry for the specified id.
	 * Returned object will include fully populated bean with all of its properties.
	 */
	public SystemConditionEntry getSystemConditionEntry(int id);


	public List<SystemConditionEntry> getSystemConditionEntryListByParent(int conditionEntryId);


	public SystemConditionEntry saveSystemConditionEntry(SystemConditionEntry conditionEntry);


	/**
	 * Saves the specified entry as well as the bean that it points to along with all properties.
	 */
	public SystemConditionEntry saveSystemConditionEntryAndBean(SystemConditionEntry conditionEntry);


	/**
	 * Deletes SystemConditionEntry with the specified id and all if its child entries recursively.
	 */
	public void deleteSystemConditionEntry(int id);
}
