package com.clifton.system.condition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemCondition</code> class represents a reusable logical condition that points to corresponding
 * root-level {@link SystemConditionEntry} that can represent any complex logical condition with multiple AND and OR statements.
 *
 * @author vgomelsky
 */
public class SystemCondition extends NamedEntity<Integer> {

	/**
	 * Optional SystemTable that this condition is linked to.
	 * Used for grouping/filtering to better organize/find and associate conditions and where they apply.
	 */
	private SystemTable systemTable;

	/**
	 * If true, names cannot be edited.
	 */
	private boolean systemDefined;

	private SystemConditionEntry conditionEntry;

	private String messageOverrideForTrue;

	private String messageOverrideForFalse;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns false if this is a grouping AND or OR condition or true if it's a simple (single bean) condition.
	 */
	public boolean isSimpleCondition() {
		if (this.conditionEntry != null && this.conditionEntry.getBean() != null) {
			return true;
		}
		return false;
	}


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public SystemConditionEntry getConditionEntry() {
		return this.conditionEntry;
	}


	public void setConditionEntry(SystemConditionEntry conditionEntry) {
		this.conditionEntry = conditionEntry;
	}


	public String getMessageOverrideForTrue() {
		return this.messageOverrideForTrue;
	}


	public void setMessageOverrideForTrue(String messageOverrideForTrue) {
		this.messageOverrideForTrue = messageOverrideForTrue;
	}


	public String getMessageOverrideForFalse() {
		return this.messageOverrideForFalse;
	}


	public void setMessageOverrideForFalse(String messageOverrideForFalse) {
		this.messageOverrideForFalse = messageOverrideForFalse;
	}
}
