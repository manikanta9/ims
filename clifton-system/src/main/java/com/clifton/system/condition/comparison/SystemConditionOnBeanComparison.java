package com.clifton.system.condition.comparison;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;
import org.springframework.core.annotation.AnnotationUtils;

import java.io.Serializable;
import java.lang.reflect.Field;


/**
 * The SystemConditionOnBeanComparison class implements a Comparison based on another {@linnk SystemCondition}
 * <p>
 * For example, for SystemNote linked to WorkflowTaskDefinition we can define:
 * fkFieldPropertyPath = "fkFieldId"
 * systemConditionPropertyPath = "category.definitionEntityModifyCondition"
 * <p>
 * The logic will retrieve WorkflowTaskDefinition for the note, then SystemCondition from the task definition,
 * will evaluate that system condition and return its evaluation result.
 *
 * @author vgomelsky
 */
public class SystemConditionOnBeanComparison implements Comparison<IdentityObject>, ValidationAware {

	/**
	 * Optionally specifies another bean that the {@link SystemCondition} will be retrieved from.
	 * The value defines the primary key of that bean while corresponding {@link SoftLinkField}
	 * annotation defines the table.
	 */
	private String fkFieldPropertyPath;

	/**
	 * If the fkFieldPropertyPath is not a SoftLinkField and we need to know the table it belongs to (uses teh get service method as defined in migrations)
	 * Example: Trade.orderIdentifier belongs to TradeOrder table, however Trade doesn't know about TradeOrder so it's not a real link
	 */
	private String fkFieldTableName;


	/**
	 * If there is no value, instead of failing because of missing FK Field value allow returning true or false (missingFkFieldValueResult)
	 * example, Trade -> Lookup Trade Order Status - if no order yet, then return false
	 */
	private boolean doNotThrowExceptionForMissingFkFieldValue;


	/**
	 * If FKFieldValue is missing and doNotThrowExceptionForMissingFkFieldValue is true, then the condition will return this result
	 */
	private boolean missingFkFieldValueResult;


	/**
	 * Will retrieve and evaluate {@link SystemCondition} property value of the bean.
	 * If {@link #fkFieldPropertyPath} is specified, will use it to retrieve the corresponding bean
	 * and then will retrieve the {@link SystemCondition} from that bean.
	 * If the System Condition is not set for the property, returns false.
	 */
	private String systemConditionPropertyPath;

	/**
	 * If {@link #systemConditionPropertyPath} is not defined, then one can explicitly specify what condition should be evaluated.
	 * This can only be used with {@link #fkFieldPropertyPath} to change the bean that evaluation will be done for.
	 */
	private Integer systemConditionId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private DaoLocator daoLocator;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemConditionService systemConditionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		if (!StringUtils.isEmpty(getFkFieldPropertyPath())) {
			ValidationUtils.assertMutuallyExclusive("Either systemConditionId or systemConditionPropertyPath must be defined when fkFieldPropertyPath is defined", getSystemConditionPropertyPath(), getSystemConditionId());
		}
		else if (getSystemConditionId() == null) {
			ValidationUtils.assertNotEmpty(getSystemConditionPropertyPath(), "systemConditionPropertyPath is required when fkFieldPropertyPath and systemConditionId are not defined");
		}
	}


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		if (!StringUtils.isEmpty(getFkFieldPropertyPath())) {
			Serializable fkFieldId = (Serializable) BeanUtils.getPropertyValue(bean, getFkFieldPropertyPath());
			if (fkFieldId == null) {
				if (!isDoNotThrowExceptionForMissingFkFieldValue()) {
					throw new ValidationException("Cannot retrieve FK Field Bean because '" + getFkFieldPropertyPath() + "' property value is null for " + bean);
				}
				else if (isMissingFkFieldValueResult()) {
					context.recordTrueMessage("FKFieldValue is missing and Default is to Return true.");
				}
				else {
					context.recordFalseMessage("FKFieldValue is missing and Default is to Return false.");
				}
				return isMissingFkFieldValueResult();
			}

			String fkTableName = getFkFieldTableName();
			if (StringUtils.isEmpty(fkTableName)) {
				Field fkField = ClassUtils.getClassField(bean.getClass(), getFkFieldPropertyPath(), true, true);
				SoftLinkField fkSoftLink = AnnotationUtils.findAnnotation(fkField, SoftLinkField.class);
				ValidationUtils.assertNotNull(fkSoftLink, "Could not find SoftLinkField annotation for " + fkField);
				ValidationUtils.assertNotNull(fkFieldId, "Cannot retrieve FK Field Bean because '" + getFkFieldPropertyPath() + "' property SoftLinkField annotation is missing.");
				SystemTable table = (SystemTable) BeanUtils.getPropertyValue(bean, fkSoftLink.tableBeanPropertyName());
				ValidationUtils.assertNotNull(table, "SoftLinkField table is not set for " + bean);
				fkTableName = table.getName();
			}

			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(fkTableName);
			Serializable entityId = dao.convertToPrimaryKeyDataType(fkFieldId);
			bean = dao.findByPrimaryKey(entityId);
			ValidationUtils.assertNotNull(bean, "Cannot find FK Field Bean for id = " + fkFieldId);
		}

		SystemCondition condition;
		if (StringUtils.isEmpty(getSystemConditionPropertyPath())) {
			condition = getSystemConditionService().getSystemCondition(getSystemConditionId());
			ValidationUtils.assertNotNull(condition, "Cannot find System Condition with id = " + getSystemConditionId());
		}
		else {
			condition = (SystemCondition) BeanUtils.getPropertyValue(bean, getSystemConditionPropertyPath());
			if (condition == null) {
				// condition is not defined: evaluate to false
				context.recordFalseMessage("System Condition is not defined for '" + getSystemConditionPropertyPath() + "' property.");
				return false;
			}
		}

		return getSystemConditionEvaluationHandler().isConditionTrue(condition, bean, context);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFkFieldPropertyPath() {
		return this.fkFieldPropertyPath;
	}


	public void setFkFieldPropertyPath(String fkFieldPropertyPath) {
		this.fkFieldPropertyPath = fkFieldPropertyPath;
	}


	public String getFkFieldTableName() {
		return this.fkFieldTableName;
	}


	public void setFkFieldTableName(String fkFieldTableName) {
		this.fkFieldTableName = fkFieldTableName;
	}


	public boolean isDoNotThrowExceptionForMissingFkFieldValue() {
		return this.doNotThrowExceptionForMissingFkFieldValue;
	}


	public void setDoNotThrowExceptionForMissingFkFieldValue(boolean doNotThrowExceptionForMissingFkFieldValue) {
		this.doNotThrowExceptionForMissingFkFieldValue = doNotThrowExceptionForMissingFkFieldValue;
	}


	public boolean isMissingFkFieldValueResult() {
		return this.missingFkFieldValueResult;
	}


	public void setMissingFkFieldValueResult(boolean missingFkFieldValueResult) {
		this.missingFkFieldValueResult = missingFkFieldValueResult;
	}


	public String getSystemConditionPropertyPath() {
		return this.systemConditionPropertyPath;
	}


	public void setSystemConditionPropertyPath(String systemConditionPropertyPath) {
		this.systemConditionPropertyPath = systemConditionPropertyPath;
	}


	public Integer getSystemConditionId() {
		return this.systemConditionId;
	}


	public void setSystemConditionId(Integer systemConditionId) {
		this.systemConditionId = systemConditionId;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}
}
