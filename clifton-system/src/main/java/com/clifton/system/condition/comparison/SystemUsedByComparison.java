package com.clifton.system.condition.comparison;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.SystemUsedByEntity;
import com.clifton.system.usedby.SystemUsedByService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Comparison implementing {@link SystemUsedByService#getSystemUsedByEntityList(String, int)}  to look up entities using the configured target entity.
 * The results are then compared against configured exclude and include lists. If no includedTables or excludedTables are configured the comparison
 * returns false if the entity is used by any other entities. If excludedTables are configured the comparison returns if the target entity is used by
 * any entity besides the ones configured. If includedTables are configured the comparison returns true if any of the used by entities match the
 * configured ones and false otherwise.
 *
 * @author jonathanr
 */
public class SystemUsedByComparison implements Comparison<IdentityObject>, ValidationAware {

	private SystemUsedByService systemUsedByService;

	private String systemTableName;
	private List<Short> includeUsedByTableIdList;
	private List<Short> excludeUsedByTableIdList;
	private String fkFieldIdPropertyPath;
	private String fkSystemTablePropertyPath;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean calculateResult(IdentityObject bean) {
		String tableName = getSystemTableName();
		if (StringUtils.isEmpty(tableName)) {
			Object systemTable = BeanUtils.getPropertyValue(bean, getFkSystemTablePropertyPath());
			ValidationUtils.assertTrue(systemTable instanceof SystemTable, "The configured path \"" + getFkSystemTablePropertyPath() + "\" is not a valid path. Please make sure the configured path is valid and of type [SystemTable].");
			tableName = ((SystemTable) systemTable).getName();
		}
		Object numberObject = BeanUtils.getPropertyValue(bean, getFkFieldIdPropertyPath());
		ValidationUtils.assertTrue(numberObject instanceof Number, "The Configured path \"" + getFkFieldIdPropertyPath() + "\" is not a valid path. Please make sure the configured path is valid and of type [Number].");
		Integer entityId = ((Number) numberObject).intValue();
		List<SystemUsedByEntity> usedByEntityList = getSystemUsedByService().getSystemUsedByEntityList(tableName, entityId);
		if (CollectionUtils.isEmpty(usedByEntityList)) {
			return false;
		}

		Set<Short> usedByTableIdSet = new HashSet<>();
		for (SystemUsedByEntity usedByEntity : CollectionUtils.getIterable(usedByEntityList)) {
			usedByTableIdSet.add(usedByEntity.getColumn().getTable().getId());
		}
		boolean include = !CollectionUtils.isEmpty(getIncludeUsedByTableIdList());

		if (include) {
			return usedByTableIdSet.stream().anyMatch(tableId -> getIncludeUsedByTableIdList().contains(tableId));
		}
		else {
			return !CollectionUtils.getFiltered(usedByTableIdSet, tableId -> !getExcludeUsedByTableIdList().contains(tableId)).isEmpty();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////              Override  methods                         //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {

		boolean result = calculateResult(bean) ^ isReverse();
		if (context != null) {
			if (result) {
				context.recordTrueMessage(getTrueMessage());
			}
			else {
				context.recordFalseMessage(getFalseMessage());
			}
		}
		return result;
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertMutuallyExclusive("System Table and System Table Path are mutually exclusive, only one may be configured.", getSystemTableName(), getFkSystemTablePropertyPath());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected String getTrueMessage() {
		return "( The target entity is being used by other entities)";
	}


	protected String getFalseMessage() {
		return "( The target entity is not being used by other entities )";
	}


	protected boolean isReverse() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                 Getters And Setters                    //////////
	////////////////////////////////////////////////////////////////////////////


	public SystemUsedByService getSystemUsedByService() {
		return this.systemUsedByService;
	}


	public void setSystemUsedByService(SystemUsedByService systemUsedByService) {
		this.systemUsedByService = systemUsedByService;
	}


	public String getSystemTableName() {
		return this.systemTableName;
	}


	public void setSystemTableName(String systemTableName) {
		this.systemTableName = systemTableName;
	}


	public List<Short> getIncludeUsedByTableIdList() {
		return this.includeUsedByTableIdList;
	}


	public void setIncludeUsedByTableIdList(List<Short> includeUsedByTableIdList) {
		this.includeUsedByTableIdList = includeUsedByTableIdList;
	}


	public List<Short> getExcludeUsedByTableIdList() {
		return this.excludeUsedByTableIdList;
	}


	public void setExcludeUsedByTableIdList(List<Short> excludeUsedByTableIdList) {
		this.excludeUsedByTableIdList = excludeUsedByTableIdList;
	}


	public String getFkFieldIdPropertyPath() {
		return this.fkFieldIdPropertyPath;
	}


	public void setFkFieldIdPropertyPath(String fkFieldIdPropertyPath) {
		this.fkFieldIdPropertyPath = fkFieldIdPropertyPath;
	}


	public String getFkSystemTablePropertyPath() {
		return this.fkSystemTablePropertyPath;
	}


	public void setFkSystemTablePropertyPath(String fkSystemTablePropertyPath) {
		this.fkSystemTablePropertyPath = fkSystemTablePropertyPath;
	}
}
