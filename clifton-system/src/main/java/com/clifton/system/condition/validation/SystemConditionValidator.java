package com.clifton.system.condition.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.condition.SystemCondition;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemConditionValidator</code> class prevents updates and deletes of system defined conditions.
 * It also does not allow changing the name of a system defined condition.
 *
 * @author NickK
 */
@Component
public class SystemConditionValidator extends SelfRegisteringDaoValidator<SystemCondition> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemCondition bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isInsert()) {
				throw new ValidationException("Adding a new System Defined System Condition is not allowed.");
			}
			else if (config.isDelete()) {
				throw new ValidationException("Deleting a System Defined System Condition is not allowed.");
			}
		}
		if (config.isUpdate()) {
			SystemCondition original = getOriginalBean(bean);
			if (original.isSystemDefined() != bean.isSystemDefined()) {
				throw new ValidationException("System Defined on System Condition cannot be changed.");
			}
			if (original.isSystemDefined()) {
				ValidationUtils.assertTrue(original.getName().equals(bean.getName()), "System Defined System Condition Names cannot be changed.", "name");
			}

			ValidationUtils.assertEquals(original.getConditionEntry().getId(), bean.getConditionEntry().getId(), "Condition Entry cannot be changed for an existing System Condition", "conditionEntry");
		}
	}
}
