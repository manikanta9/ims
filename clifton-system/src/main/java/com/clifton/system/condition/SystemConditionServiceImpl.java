package com.clifton.system.condition;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.search.SystemConditionSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The <code>SystemConditionServiceImpl</code> class is a basic implementation of {@link SystemConditionService} interface.
 *
 * @author vgomelsky
 */
@Service
public class SystemConditionServiceImpl implements SystemConditionService {

	private AdvancedUpdatableDAO<SystemCondition, Criteria> systemConditionDAO;
	private UpdatableDAO<SystemConditionEntry> systemConditionEntryDAO;

	private DaoSingleKeyListCache<SystemConditionEntry, Integer> systemConditionEntryByParentCache;

	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////
	////////          System Condition Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getSystemCondition(int id) {
		return getSystemConditionDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemCondition getSystemConditionPopulated(int id) {
		SystemCondition result = getSystemCondition(id);
		if (result != null && result.getConditionEntry() != null) {
			populateChildEntriesRecursively(result.getConditionEntry());
		}
		return result;
	}


	@Override
	public SystemCondition getSystemConditionByName(String name) {
		return getSystemConditionDAO().findOneByField("name", name);
	}


	@Override
	public List<SystemCondition> getSystemConditionList(final SystemConditionSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// Optional table name restriction only makes sense if not explicitly table restrictions
				if (searchForm.getSystemTableId() == null && !StringUtils.isEmpty(searchForm.getOptionalSystemTableName())) {
					criteria.createAlias("systemTable", "st", JoinType.LEFT_OUTER_JOIN);
					criteria.add(Restrictions.or(Restrictions.isNull("systemTable.id"), Restrictions.eq("st.name", searchForm.getOptionalSystemTableName())));
				}
			}
		};
		return getSystemConditionDAO().findBySearchCriteria(config);
	}


	/**
	 * @see com.clifton.system.condition.validation.SystemConditionValidator
	 */
	@Override
	@Transactional
	public SystemCondition saveSystemCondition(SystemCondition condition) {
		SystemConditionEntry conditionEntry = condition.getConditionEntry();
		ValidationUtils.assertNotNull(conditionEntry, "System Condition Entry is required.");

		if (condition.isNewBean()) {
			// create entry first (and children recursively if necessary)
			conditionEntry = createSystemConditionEntriesRecursively(conditionEntry);
			condition.setConditionEntry(conditionEntry);
		}
		else if (condition.isSimpleCondition()) {
			// the bean may be changed for a simple condition entry
			saveSystemConditionEntry(conditionEntry);
		}

		return getSystemConditionDAO().save(condition);
	}


	@Override
	@Transactional
	public void deleteSystemCondition(int id) {
		SystemCondition condition = getSystemCondition(id);
		ValidationUtils.assertNotNull(condition, "Cannot find SystemCondition to delete for id = " + id);

		// also delete all child entries
		getSystemConditionDAO().delete(condition);
		deleteSystemConditionEntryRecursively(condition.getConditionEntry());
	}


	////////////////////////////////////////////////////////////////////////////
	////////       System Condition Entry Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemConditionEntry getSystemConditionEntry(int id) {
		SystemConditionEntry conditionEntry = getSystemConditionEntryDAO().findByPrimaryKey(id);
		if (conditionEntry != null && conditionEntry.getBean() != null) {
			// set fully populated bean with all properties
			conditionEntry.setBean(getSystemBeanService().getSystemBean(conditionEntry.getBean().getId()));
		}
		return conditionEntry;
	}


	private void populateChildEntriesRecursively(SystemConditionEntry conditionEntry) {
		if (conditionEntry.getEntryType().isGroupingEntry()) {
			List<SystemConditionEntry> childEntryList = getSystemConditionEntryListByParent(conditionEntry.getId());
			if (!CollectionUtils.isEmpty(childEntryList)) {
				conditionEntry.setChildEntryList(childEntryList);
				for (SystemConditionEntry childEntry : childEntryList) {
					populateChildEntriesRecursively(childEntry);
				}
			}
		}
	}


	@Override
	public List<SystemConditionEntry> getSystemConditionEntryListByParent(int conditionEntryId) {
		return getSystemConditionEntryByParentCache().getBeanListForKeyValue(getSystemConditionEntryDAO(), conditionEntryId);
	}


	@Override
	public SystemConditionEntry saveSystemConditionEntry(SystemConditionEntry conditionEntry) {
		validateSystemConditionEntry(conditionEntry);
		return getSystemConditionEntryDAO().save(conditionEntry);
	}


	@Override
	@Transactional
	public SystemConditionEntry saveSystemConditionEntryAndBean(SystemConditionEntry conditionEntry) {
		validateSystemConditionEntry(conditionEntry);

		if (conditionEntry.getBean() != null) {
			// all so save the bean with all of its properties
			getSystemBeanService().saveSystemBean(conditionEntry.getBean());
		}

		return getSystemConditionEntryDAO().save(conditionEntry);
	}


	private void validateSystemConditionEntry(SystemConditionEntry conditionEntry) {
		ValidationUtils.assertNotNull(conditionEntry.getEntryType(), "Entry Type is required for " + conditionEntry);
		if (conditionEntry.getEntryType().isGroupingEntry()) {
			ValidationUtils.assertNull(conditionEntry.getBean(), "Condition bean is not allowed for Grouping Entries.");
		}
		else {
			ValidationUtils.assertNotNull(conditionEntry.getBean(), "Condition bean is required for Conditional Entries.");
		}
	}


	@Transactional
	protected SystemConditionEntry createSystemConditionEntriesRecursively(SystemConditionEntry conditionEntry) {
		// create parent first
		conditionEntry = saveSystemConditionEntry(conditionEntry);

		// create children if any
		for (SystemConditionEntry childEntry : CollectionUtils.getIterable(conditionEntry.getChildEntryList())) {
			childEntry.setParent(conditionEntry);
			createSystemConditionEntriesRecursively(childEntry);
		}

		return conditionEntry;
	}


	@Override
	@Transactional
	public void deleteSystemConditionEntry(int id) {
		SystemConditionEntry conditionEntry = getSystemConditionEntry(id);
		ValidationUtils.assertNotNull(conditionEntry, "Cannot find SystemConditionEntry to delete for id = " + id);

		deleteSystemConditionEntryRecursively(conditionEntry);
	}


	@Transactional
	protected void deleteSystemConditionEntryRecursively(SystemConditionEntry conditionEntry) {
		// delete children first (recursively), if any
		if (conditionEntry.getEntryType().isGroupingEntry()) {
			List<SystemConditionEntry> childEntries = getSystemConditionEntryListByParent(conditionEntry.getId());
			for (SystemConditionEntry childEntry : CollectionUtils.getIterable(childEntries)) {
				deleteSystemConditionEntryRecursively(childEntry);
			}
		}

		getSystemConditionEntryDAO().delete(conditionEntry);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemCondition, Criteria> getSystemConditionDAO() {
		return this.systemConditionDAO;
	}


	public void setSystemConditionDAO(AdvancedUpdatableDAO<SystemCondition, Criteria> systemConditionDAO) {
		this.systemConditionDAO = systemConditionDAO;
	}


	public UpdatableDAO<SystemConditionEntry> getSystemConditionEntryDAO() {
		return this.systemConditionEntryDAO;
	}


	public void setSystemConditionEntryDAO(UpdatableDAO<SystemConditionEntry> systemConditionEntryDAO) {
		this.systemConditionEntryDAO = systemConditionEntryDAO;
	}


	public DaoSingleKeyListCache<SystemConditionEntry, Integer> getSystemConditionEntryByParentCache() {
		return this.systemConditionEntryByParentCache;
	}


	public void setSystemConditionEntryByParentCache(DaoSingleKeyListCache<SystemConditionEntry, Integer> systemConditionEntryByParentCache) {
		this.systemConditionEntryByParentCache = systemConditionEntryByParentCache;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
