package com.clifton.system.condition.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.condition.SystemConditionEntry;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;


/**
 * The <code>SystemConditionEntryByParentCache</code> caches the list of condition entries that are children an entry.
 *
 * @author vgomelsky
 */
@Component
public class SystemConditionEntryByParentCache extends SelfRegisteringSingleKeyDaoListCache<SystemConditionEntry, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "parent.id";
	}


	@Override
	protected Integer getBeanKeyValue(SystemConditionEntry bean) {
		if (bean.getParent() != null) {
			return bean.getParent().getId();
		}
		return null;
	}


	@Override
	protected List<SystemConditionEntry> lookupBeanList(ReadOnlyDAO<SystemConditionEntry> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		List<SystemConditionEntry> result = super.lookupBeanList(dao, throwExceptionIfNotFound, keyProperties);

		// sort by type then bean name
		if (CollectionUtils.getSize(result) > 1) {
			result.sort(Comparator.comparing(SystemConditionEntry::getEntryType).thenComparing(SystemConditionEntry::getLabel));
		}

		return result;
	}
}
