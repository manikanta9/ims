package com.clifton.system.condition;


import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.HierarchicalEntity;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.validation.ConditionEntryTypes;

import java.util.List;


/**
 * The <code>SystemConditionEntry</code> class represents a conditional entry that evaluates to true or false.
 * Entries are hierarchical and can be grouped into any logical condition including any combination of AND and OR grouping conditions.
 *
 * @author vgomelsky
 */
public class SystemConditionEntry extends HierarchicalEntity<SystemConditionEntry, Integer> implements LabeledObject {

	/**
	 * AND, OR, boolean condition
	 */
	private ConditionEntryTypes entryType;

	/**
	 * Bean definition for a bean that implements {@link Comparison}.
	 */
	private SystemBean bean;


	@OneToManyEntity(serviceBeanName = "systemConditionService", serviceMethodName = "getSystemConditionEntryListByParent", delayed = false)
	private List<SystemConditionEntry> childEntryList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return (this.bean == null) ? (this.entryType == null ? "UNDEFINED ENTRY TYPE" : this.entryType.name()) : this.bean.getName();
	}


	@Override
	public String toString() {
		if (isNewBean()) {
			return new StringBuilder(20).append("{id=").append(getId())
					.append(", label=").append(getLabel())
					.append(", childEntryList=").append(getChildEntryList())
					.append('}').toString();
		}
		return super.toString();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public ConditionEntryTypes getEntryType() {
		return this.entryType;
	}


	public void setEntryType(ConditionEntryTypes entryType) {
		this.entryType = entryType;
	}


	public SystemBean getBean() {
		return this.bean;
	}


	public void setBean(SystemBean bean) {
		this.bean = bean;
	}


	public List<SystemConditionEntry> getChildEntryList() {
		return this.childEntryList;
	}


	public void setChildEntryList(List<SystemConditionEntry> childEntryList) {
		this.childEntryList = childEntryList;
	}
}
