package com.clifton.system.condition.comparison;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.web.mvc.WebMethodHandler;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * This comparison bean queries a database table to see if any entities exist that match the provided conditions.
 * <p>
 * It accepts a system table, which is queried using the table's entityListUrl (so that field must be populated on the provided table).
 * <p>
 * Each freemarker template provided in the freemarkerParameters field is processed to extract a field from the bean being compared, and the value for that field is appended to the query string.
 * <p>
 * The evaluator will return true if there are any entities in the table that match using the specified fields.
 *
 * @author MitchellF
 */
public class EntityExistsComparison<T extends IdentityObject> implements Comparison<T>, ValidationAware {

	/**
	 * Id of the table to be queried.  Must have EntityListURL field populated
	 */
	private Short systemTableId;

	/**
	 * List of string FreeMarker templates to be processed and appended to the query string
	 */
	private List<String> freeMarkerParameters;

	private SystemSchemaService systemSchemaService;

	private TemplateConverter templateConverter;

	private WebMethodHandler webMethodHandler;


	@Override
	public boolean evaluate(T bean, ComparisonContext context) {
		StringBuilder queryString = getSystemTableUrlBuilder();

		List<String> queryParameters = getFreeMarkerQueryParameters(bean);
		Map<Boolean, List<String>> queryParameterRestrictionMap = BeanUtils.getBeansMap(queryParameters, str -> StringUtils.contains(str, "comparison"));

		String restrictionListString = CollectionUtils.getStream(queryParameterRestrictionMap.get(Boolean.TRUE))
				.filter(str -> !StringUtils.isEmpty(str))
				.map(str -> StringUtils.replace(StringUtils.replace(str, "restrictionList=[", ""), "]", ""))
				.filter(str -> !StringUtils.isEmpty(str))
				.collect(Collectors.joining(","));
		if (!StringUtils.isEmpty(restrictionListString)) {
			queryString.append("&restrictionList=[").append(restrictionListString).append(']');
		}

		String queryParameterString = CollectionUtils.getStream(queryParameterRestrictionMap.get(Boolean.FALSE))
				.filter(str -> !StringUtils.isEmpty(str))
				.collect(Collectors.joining("&"));
		if (!StringUtils.isEmpty(queryParameterString)) {
			queryString.append('&').append(queryParameterString);
		}

		List<T> result = getWebMethodHandler().executeServiceCall(queryString.toString(), null);

		return isReverse(!CollectionUtils.isEmpty(result));
	}


	protected boolean isReverse(boolean result) {
		return result;
	}


	@Override
	public void validate() throws ValidationException {
		getAndValidateSystemTable();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private StringBuilder getSystemTableUrlBuilder() {
		SystemTable table = getAndValidateSystemTable();

		StringBuilder queryString = new StringBuilder(table.getEntityListUrl());
		if (!queryString.toString().contains("?")) {
			queryString.append('?');
		}
		if (!queryString.toString().endsWith("?")) {
			queryString.append('&');
		}
		// append default limit of 1 for performance and memory constraints
		return queryString.append("limit=1");
	}


	private SystemTable getAndValidateSystemTable() {
		SystemTable table = getSystemSchemaService().getSystemTable(getSystemTableId());
		if (table == null) {
			throw new RuntimeException("Unable to find System Table with ID: " + getSystemTableId());
		}
		if (table.getEntityListUrl() == null) {
			throw new RuntimeException("Cannot provide table with EntityListUrl not populated. No EntityListUrl for table: " + table);
		}
		return table;
	}


	private List<String> getFreeMarkerQueryParameters(T bean) {
		List<String> queryParameters = new ArrayList<>();
		for (String template : getFreeMarkerParameters()) {
			// Evaluate template
			TemplateConfig templateConfig = new TemplateConfig(template);
			templateConfig.addBeanToContext("bean", bean);
			String freeMarkerResult = getTemplateConverter().convert(templateConfig);
			if (!StringUtils.isEmpty(freeMarkerResult)) {
				queryParameters.add(freeMarkerResult);
			}
		}
		return queryParameters;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getSystemTableId() {
		return this.systemTableId;
	}


	public void setSystemTableId(Short systemTableId) {
		this.systemTableId = systemTableId;
	}


	public List<String> getFreeMarkerParameters() {
		return this.freeMarkerParameters;
	}


	public void setFreeMarkerParameters(List<String> freeMarkerParameters) {
		this.freeMarkerParameters = freeMarkerParameters;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public WebMethodHandler getWebMethodHandler() {
		return this.webMethodHandler;
	}


	public void setWebMethodHandler(WebMethodHandler webMethodHandler) {
		this.webMethodHandler = webMethodHandler;
	}
}
