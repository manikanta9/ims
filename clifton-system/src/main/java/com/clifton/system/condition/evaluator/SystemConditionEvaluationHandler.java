package com.clifton.system.condition.evaluator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.context.Context;
import com.clifton.system.condition.SystemCondition;


/**
 * The SystemConditionEvaluationHandler interface defines methods for evaluating {@link SystemCondition} objects.
 *
 * @author vgomelsky
 */
public interface SystemConditionEvaluationHandler {


	/**
	 * Returns true if the specified {@link SystemCondition} evaluates to true for the specified bean.
	 */
	public <T extends IdentityObject> boolean isConditionTrue(SystemCondition condition, T bean);


	/**
	 * Returns true if the specified {@link SystemCondition} evaluates to true for the specified bean.
	 */
	public <T extends IdentityObject> boolean isConditionTrue(SystemCondition condition, T bean, ComparisonContext context);


	/**
	 * Returns the trueMessage if the specified condition evaluates to true for the specified bean.
	 * Returns null otherwise (condition evaluated to false).
	 */
	public <T extends IdentityObject> String getConditionTrueMessage(SystemCondition condition, T bean);


	/**
	 * Returns the falseMessage if the specified condition evaluates to false for the specified bean.
	 * Returns null otherwise (condition evaluated to true).
	 */
	public <T extends IdentityObject> String getConditionFalseMessage(SystemCondition condition, T bean);


	/**
	 * Returns an {@link EvaluationResult} object that contains the result of the {@link SystemCondition}
	 * evaluation for the given bean.
	 */
	public <T extends IdentityObject> EvaluationResult evaluateCondition(int conditionId, T bean);


	/**
	 * Returns an {@link EvaluationResult} object that contains the result of the {@link SystemCondition}
	 * evaluation for the given bean.
	 */
	public <T extends IdentityObject> EvaluationResult evaluateCondition(SystemCondition condition, T bean);


	/**
	 * Returns an {@link EvaluationResult} object that contains the result of the {@link SystemCondition}
	 * evaluation for the given bean. Using the provided context throughout the process
	 */
	public <T extends IdentityObject> EvaluationResult evaluateCondition(SystemCondition condition, T bean, Context context);
}
