package com.clifton.system.condition.comparison;

/**
 * <code>SystemNotUsedByComparison</code> is the is the opposite comparison of {@link SystemUsedByComparison}.
 * Put simply returns it implements the same logic and then returns the opposite boolean value.
 *
 * @author jonathanr
 */
public class SystemNotUsedByComparison extends SystemUsedByComparison {


	@Override
	protected String getTrueMessage() {
		return "( The target entity has no unexpected dependencies )";
	}


	@Override
	protected String getFalseMessage() {
		return "( The target entity has unexpected dependencies )";
	}


	@Override
	protected boolean isReverse() {
		return true;
	}
}
