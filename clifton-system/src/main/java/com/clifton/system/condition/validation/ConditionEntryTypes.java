package com.clifton.system.condition.validation;

import com.clifton.system.condition.SystemConditionEntry;


/**
 * The ConditionEntryTypes enum defines a type of {@link SystemConditionEntry}.
 * Grouping entries (AND and OR) must always have child entries (more than one).
 *
 * @author vgomelsky
 */
public enum ConditionEntryTypes {

	OR(true), AND(true), CONDITION(false);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	ConditionEntryTypes(boolean groupingEntry) {
		this.groupingEntry = groupingEntry;
	}


	/**
	 * Specifies whether this entry is a specific condition or a grouping of child conditions and groupings.
	 */
	private boolean groupingEntry;


	public boolean isGroupingEntry() {
		return this.groupingEntry;
	}
}
