package com.clifton.system.condition.evaluator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.comparison.GroupingComparison;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.comparison.grouping.AndComparison;
import com.clifton.core.comparison.grouping.OrComparison;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionEntry;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.validation.ConditionEntryTypes;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class SystemConditionEvaluationHandlerImpl implements SystemConditionEvaluationHandler {

	private SystemBeanService systemBeanService;
	private SystemConditionService systemConditionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends IdentityObject> boolean isConditionTrue(SystemCondition condition, T bean) {
		return isConditionTrue(condition, bean, null);
	}


	@Override
	public <T extends IdentityObject> boolean isConditionTrue(SystemCondition condition, T bean, ComparisonContext context) {
		Comparison<T> comparison = convertSystemConditionToComparison(condition);
		return comparison.evaluate(bean, context);
	}


	@Override
	public <T extends IdentityObject> String getConditionTrueMessage(SystemCondition condition, T bean) {
		EvaluationResult result = evaluateCondition(condition, bean);
		if (result.isResult()) {
			return result.getMessage();
		}
		return null;
	}


	@Override
	public <T extends IdentityObject> String getConditionFalseMessage(SystemCondition condition, T bean) {
		EvaluationResult result = evaluateCondition(condition, bean);
		if (!result.isResult()) {
			return result.getMessage();
		}
		return null;
	}


	@Override
	public <T extends IdentityObject> EvaluationResult evaluateCondition(int conditionId, T bean) {
		SystemCondition condition = getSystemConditionService().getSystemCondition(conditionId);
		ValidationUtils.assertNotNull(condition, "Cannot find System Condition With ID [" + conditionId + "]");
		return evaluateCondition(condition, bean);
	}


	@Override
	public <T extends IdentityObject> EvaluationResult evaluateCondition(SystemCondition condition, T bean) {
		return evaluateCondition(condition, bean, null);
	}


	@Override
	public <T extends IdentityObject> EvaluationResult evaluateCondition(SystemCondition condition, T bean, Context context) {
		Comparison<T> comparison = convertSystemConditionToComparison(condition);
		SimpleComparisonContext simpleContext = new SimpleComparisonContext(context);
		boolean result = comparison.evaluate(bean, simpleContext);

		String message = result ?
				(StringUtils.isEmpty(condition.getMessageOverrideForTrue()) ? simpleContext.getTrueMessage() : condition.getMessageOverrideForTrue())
				:
				(StringUtils.isEmpty(condition.getMessageOverrideForFalse()) ? simpleContext.getFalseMessage() : condition.getMessageOverrideForFalse());

		return new EvaluationResult(result, message, condition, bean);
	}


	private <T extends IdentityObject> Comparison<T> convertSystemConditionToComparison(SystemCondition condition) {
		Comparison<T> result;
		SystemConditionEntry entry = condition.getConditionEntry();
		if (entry.getEntryType().isGroupingEntry()) {
			// create complex grouping comparison structure
			result = ConditionEntryTypes.AND == entry.getEntryType() ? new AndComparison<>() : new OrComparison<>();

			condition = getSystemConditionService().getSystemConditionPopulated(condition.getId());
			populateComparisonWithChildren((GroupingComparison) result, condition.getConditionEntry().getChildEntryList());
		}
		else {
			// create simple condition
			@SuppressWarnings("unchecked")
			Comparison<T> comparisonBean = (Comparison<T>) getSystemBeanService().getBeanInstance(entry.getBean());
			result = comparisonBean;
		}

		return result;
	}


	private <T extends IdentityObject> void populateComparisonWithChildren(GroupingComparison<T> comparison, List<SystemConditionEntry> childList) {
		for (SystemConditionEntry entry : CollectionUtils.getIterable(childList)) {
			if (entry.getEntryType().isGroupingEntry()) {
				GroupingComparison<T> child = ConditionEntryTypes.AND == entry.getEntryType() ? new AndComparison<>() : new OrComparison<>();
				comparison.addChildComparison(child);
				populateComparisonWithChildren(child, entry.getChildEntryList());
			}
			else {
				@SuppressWarnings("unchecked")
				Comparison<T> comparisonBean = (Comparison<T>) getSystemBeanService().getBeanInstance(entry.getBean());
				comparison.addChildComparison(comparisonBean);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}
}
