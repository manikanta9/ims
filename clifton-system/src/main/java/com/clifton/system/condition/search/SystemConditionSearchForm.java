package com.clifton.system.condition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


/**
 * The <code>SystemConditionSearchForm</code> class defines search configuration for SystemCondition objects.
 *
 * @author vgomelsky
 */
public class SystemConditionSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "systemTable.id", sortField = "systemTable.name")
	private Short systemTableId;

	@SearchField(searchFieldPath = "systemTable", searchField = "name")
	private String systemTableName;

	// Custom Search Field - Returns conditions directly tied to the given table or those without an association to a specific table
	private String optionalSystemTableName;

	@SearchField(searchField = "systemTable.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean emptySystemTableOnly;

	@SearchField
	private Boolean systemDefined;

	@SearchField(searchField = "conditionEntry.id")
	private Integer conditionEntryId;

	@SearchField(searchField = "bean", searchFieldPath = "conditionEntry", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean simpleCondition;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getSystemTableId() {
		return this.systemTableId;
	}


	public void setSystemTableId(Short systemTableId) {
		this.systemTableId = systemTableId;
	}


	public String getSystemTableName() {
		return this.systemTableName;
	}


	public void setSystemTableName(String systemTableName) {
		this.systemTableName = systemTableName;
	}


	public String getOptionalSystemTableName() {
		return this.optionalSystemTableName;
	}


	public void setOptionalSystemTableName(String optionalSystemTableName) {
		this.optionalSystemTableName = optionalSystemTableName;
	}


	public Boolean getEmptySystemTableOnly() {
		return this.emptySystemTableOnly;
	}


	public void setEmptySystemTableOnly(Boolean emptySystemTableOnly) {
		this.emptySystemTableOnly = emptySystemTableOnly;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Integer getConditionEntryId() {
		return this.conditionEntryId;
	}


	public void setConditionEntryId(Integer conditionEntryId) {
		this.conditionEntryId = conditionEntryId;
	}


	public Boolean getSimpleCondition() {
		return this.simpleCondition;
	}


	public void setSimpleCondition(Boolean simpleCondition) {
		this.simpleCondition = simpleCondition;
	}
}

