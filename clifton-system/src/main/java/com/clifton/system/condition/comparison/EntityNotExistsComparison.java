package com.clifton.system.condition.comparison;

import com.clifton.core.beans.IdentityObject;


/**
 * Functions the same as {@link EntityExistsComparison}, except it will return true if there are NO entities in the database that match on the provided fields.
 *
 * @author MitchellF
 */
public class EntityNotExistsComparison<T extends IdentityObject> extends EntityExistsComparison<T> {

	@Override
	protected boolean isReverse(boolean result) {
		return !result;
	}
}
