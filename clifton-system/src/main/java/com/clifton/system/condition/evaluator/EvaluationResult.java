package com.clifton.system.condition.evaluator;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.condition.SystemCondition;


/**
 * The <code>EvaluationResult</code> stores the result from a {@link SystemCondition} evaluation.
 * If the result is true, the message is the true message; if false, false message
 *
 * @author manderson
 */
public class EvaluationResult {

	private final boolean result;
	private final String message;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public EvaluationResult(boolean result, String message, SystemCondition condition, IdentityObject bean) {
		ValidationUtils.assertNotNull(message, "Condition " + condition + " that evaluated to TRUE for bean " + bean + " cannot have trueMessage = null");
		this.message = message;
		this.result = result;
	}


	public boolean isResult() {
		return this.result;
	}


	public boolean isTrue() {
		return isResult();
	}


	public boolean isFalse() {
		return !isResult();
	}


	public String getMessage() {
		return this.message;
	}
}
