package com.clifton.system.condition.comparison;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


/**
 * The SystemConditionRegularExpressionComparison class implements a Comparison based on a bean field and
 * a regular expression
 * <p>
 * For example, a bean field of username and a regular expression of [ake] can be used to identify whenever a
 * bean with a username field containing "ake" is created. Then depending on the Matches Evaluates to False flag,
 * the action will be allowed or rejected. Additionally, you can use the Ignore Empty Value flag to allow or discourage
 * empty values in the username field.
 * </p>
 * Multiple versions of this comparison can be used to cover multiple fields, and the same comparison can be
 * leveraged across tables as long as the bean field name is the same.
 *
 * @author JasonS
 */
public class SystemConditionRegularExpressionComparison implements Comparison<IdentityObject>, ValidationAware {

	private String beanFieldName;

	private String regularExpression;

	private boolean ignoreEmptyValue;

	private boolean matchEvaluatesToFalse;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getBeanFieldName(), "A Bean Field Name is required.");
		ValidationUtils.assertNotEmpty(getRegularExpression(), "A Regular Expression is required.");
		try {
			Pattern.compile(getRegularExpression());
		}
		catch (PatternSyntaxException e) {
			throw new ValidationException("Regular Expression " + getRegularExpression() + " is invalid.", e);
		}
	}


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		ObjectToStringConverter converter = new ObjectToStringConverter();
		String fieldValue = converter.convert(BeanUtils.getFieldValue(bean, getBeanFieldName()));
		boolean isMatched = false;
		boolean result = false;

		if (StringUtils.isEmpty(fieldValue)) {
			result = isIgnoreEmptyValue();
		}
		else {
			result = Pattern.matches(getRegularExpression(), fieldValue);
			isMatched = result;
		}

		if (isMatchEvaluatesToFalse()) {
			result = !result;
		}

		if (context != null) {
			StringBuilder msg = new StringBuilder();
			msg.append("Bean Field is ").append(getBeanFieldName());
			msg.append(" with value of ").append(fieldValue);
			msg.append(" with Regular Expression ").append(getRegularExpression());
			if (isMatched) {
				msg.append(" results in a match,");
			}
			else {
				msg.append(" does not match,");
			}
			msg.append(" with Ignore Empty Value is ").append(isIgnoreEmptyValue());
			msg.append(" with Matches Evaluates to False is ").append(isMatchEvaluatesToFalse());
			if (result) {
				context.recordTrueMessage(msg.toString());
			}
			else {
				context.recordFalseMessage(msg.toString());
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBeanFieldName() {
		return this.beanFieldName;
	}


	public void setBeanFieldName(String beanFieldName) {
		this.beanFieldName = beanFieldName;
	}


	public String getRegularExpression() {
		return this.regularExpression;
	}


	public void setRegularExpression(String regularExpression) {
		this.regularExpression = regularExpression;
	}


	public boolean isIgnoreEmptyValue() {
		return this.ignoreEmptyValue;
	}


	public void setIgnoreEmptyValue(boolean ignoreEmptyValue) {
		this.ignoreEmptyValue = ignoreEmptyValue;
	}


	public boolean isMatchEvaluatesToFalse() {
		return this.matchEvaluatesToFalse;
	}


	public void setMatchEvaluatesToFalse(boolean matchEvaluatesToFalse) {
		this.matchEvaluatesToFalse = matchEvaluatesToFalse;
	}
}
