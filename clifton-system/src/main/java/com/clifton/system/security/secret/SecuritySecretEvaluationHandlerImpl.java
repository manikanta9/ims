package com.clifton.system.security.secret;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretEvaluationHandler;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @author theodorez
 */
@Component
public class SecuritySecretEvaluationHandlerImpl implements SecuritySecretEvaluationHandler {

	private SystemBeanService systemBeanService;
	private SystemSchemaService systemSchemaService;
	private SecurityAuthorizationService securityAuthorizationService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@DoNotAddRequestMapping
	public int getEntitySecretIdFromFieldPath(IdentityObject entity, String fieldPath) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(fieldPath), "Field Path cannot be empty");
		int secretId;
		String[] pathArray = fieldPath.split("\\.");
		try {
			//Loop the entries in the field path, describing each level and getting the next entry until the end is reached
			Map<String, Object> properties;
			if (entity instanceof SystemBean) {
				properties = BeanUtils.describe(getSystemBeanService().getBeanInstance((SystemBean) entity));
			}
			else {
				properties = BeanUtils.describe(entity);
			}

			Object property = null;
			for (String path : pathArray) {
				property = properties.get(path);
				if (property instanceof SystemBean) {
					property = getSystemBeanService().getBeanInstance((SystemBean) property);
				}
				properties = BeanUtils.describe(property);
			}
			ValidationUtils.assertTrue(property instanceof SecuritySecret, "The object found at the end of the field path is not a Security Secret");
			secretId = (Integer) properties.get("id");
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred while getting the entity secret ID", e);
		}
		return secretId;
	}


	@Override
	@DoNotAddRequestMapping
	public void evaluateEntitySystemCondition(IdentityObject entity, String systemTableName) {
		if (getSecurityAuthorizationService().isSecurityUserAdmin()) {
			return;
		}

		if (entity instanceof SystemEntityModifyConditionAware) {
			SystemCondition condition = ((SystemEntityModifyConditionAware) entity).getEntityModifyCondition();
			if (condition != null) {
				String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(condition, entity);
				if (!StringUtils.isEmpty(error)) {
					throw new ValidationException("Error decrypting secret. You do not have permission to decrypt secrets because: " + error);
				}
				return;
			}
		}

		SystemTable table = getSystemSchemaService().getSystemTableByName(systemTableName);
		if (table.getSecurityResource() == null) {
			throw new ValidationException("There is no security resource assigned to table [" + table.getName() + "].  Unable to determine if the user may decrypt secrets.");
		}

		if (!getSecurityAuthorizationService().isSecurityAccessAllowed(table.getSecurityResource().getName(), SecurityPermission.PERMISSION_READ)) {
			throw new ValidationException("Unable to decrypt the secret because: No Read Access to Table [" + table.getName() + "] using Security Resource [" + table.getSecurityResource().getName()
					+ "].");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
