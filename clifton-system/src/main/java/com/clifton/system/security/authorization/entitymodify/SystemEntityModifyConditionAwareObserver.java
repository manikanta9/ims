package com.clifton.system.security.authorization.entitymodify;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemEntityModifyConditionAwareObserver</code> provides additional verification prior
 * to inserts/updates/deletes for a bean based on the {@link SystemCondition} returned by the getEntityModifyCondition method implementation.
 * <p>
 * <ul>
 * <li>If the user is an admin, then they have unrestricted access and are able to perform any action - the condition logic is bypassed.</li>
 * <li>If no condition is returned and isEntityModifyConditionRequiredForNonAdmins = true, then only admins are able to modify entities</li>
 * <li>If no condition is returned and sEntityModifyConditionRequiredForNonAdmins = false, then anyone can modify entities</li>
 * </ul>
 * <p>
 * This observer is automatically registered for entities that extends the {@link SystemEntityModifyConditionAware} interface
 *
 * @author manderson
 */
@Component
public class SystemEntityModifyConditionAwareObserver<T extends SystemEntityModifyConditionAware> extends BaseDaoEventObserver<T> {

	private SecurityAuthorizationService securityAuthorizationService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Run this first
	 * NOTE: Set to -200, because other overrides use -100 and this should run before those
	 */
	public SystemEntityModifyConditionAwareObserver() {
		setOrder(-200);
	}


	@Override
	protected void beforeTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// do not check security if current user is an admin - always allowed
		if (getSecurityAuthorizationService().isSecurityUserAdmin()) {
			return;
		}

		SystemCondition condition = getModifyCondition(bean, event);

		// Only when updating, and original bean had a condition defined that is different
		// than what it is now, evaluate the condition to verify the user has access to change it
		if (event.isUpdate()) {
			T originalBean = getOriginalBean(dao, bean);
			SystemCondition originalCondition = originalBean.getEntityModifyCondition();
			if (originalCondition != null && !originalCondition.equals(condition)) {
				String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(originalCondition, originalBean);
				if (!StringUtils.isEmpty(error)) {
					throw new ValidationException("Update Not Allowed. You do not have permission to update this " + dao.getConfiguration().getTableName() + " because: " + error);
				}
			}
		}

		if (condition == null) {
			// Not Allowed
			if (bean instanceof SystemEntityModifyConditionAwareAdminRequired && ((SystemEntityModifyConditionAwareAdminRequired) bean).isEntityModifyConditionRequiredForNonAdmins()) {
				throw new ValidationException("You must be an admin user to " + event.name() + " this " + dao.getConfiguration().getTableName()
						+ ".  Modify Condition has not been selected to validate additional security, but is required for Non Admin Users.");
			}
			// Otherwise - Allowed
			return;
		}

		String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(condition, bean);
		if (!StringUtils.isEmpty(error)) {
			throw new ValidationException("You do not have permission to " + event.name() + " this " + dao.getConfiguration().getTableName() + " because: " + error);
		}
	}


	private SystemCondition getModifyCondition(T bean, DaoEventTypes eventType) {
		if (eventType.isDelete() && bean instanceof SystemEntityModifyWithDeleteConditionAware) {
			return ObjectUtils.coalesce(((SystemEntityModifyWithDeleteConditionAware) bean).getEntityDeleteCondition(), bean.getEntityModifyCondition());
		}
		return bean.getEntityModifyCondition();
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
