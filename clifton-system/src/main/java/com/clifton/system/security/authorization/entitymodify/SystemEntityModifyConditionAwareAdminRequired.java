package com.clifton.system.security.authorization.entitymodify;

/**
 * The {@link SystemEntityModifyConditionAwareAdminRequired} interface extends {@link SystemEntityModifyConditionAware}. This interface allows restricting modification to
 * administrator-only access when no {@link #getEntityModifyCondition() entity modify condition} is assigned. The {@link #isEntityModifyConditionRequiredForNonAdmins()} method may
 * be overridden to allow other preconditions for administrator-only access.
 * <p>
 * <b>Note: Administrator-only access should be limited to secure functions which do not require modifications under normal circumstances, such as sensitive system operations.
 * Administrator access is reserved for unusual or critical scenarios and may be protected by a break-glass protocol in production environments.</b>
 *
 * @author ignacioa
 */
public interface SystemEntityModifyConditionAwareAdminRequired extends SystemEntityModifyConditionAware {

	/**
	 * Returns <code>true</code> if administrator access should be required for modifications when no {@link #getEntityModifyCondition() entity modify condition} is assigned.
	 */
	public default boolean isEntityModifyConditionRequiredForNonAdmins() {
		return true;
	}
}
