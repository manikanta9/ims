package com.clifton.system.security.authorization.entitymodify;


import com.clifton.core.beans.IdentityObject;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignment;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;


/**
 * The <code>SystemEntityModifyConditionAware</code> is an interface that can be implemented
 * by a DTO and used to provide ADDITIONAL security restrictions for inserts/updates/deletes
 * <p>
 * NOTE: This extra security check is done at the DAO level, and users must also have required permissions to get through the service method
 * <p>
 * Entities that implement this interface are automatically registered with the {@link SystemEntityModifyConditionAwareObserver}
 * <p>
 * If no {@link #getEntityModifyCondition() entity modify condition} is assigned, then the entity modify condition is ignored. The {@link
 * SystemEntityModifyConditionAwareAdminRequired} interface may be used to restrict access to administrators in these scenarios.
 * <p>
 * Example: {@link SystemHierarchyCategory} has an option to select the modify condition,
 * and {@link SystemHierarchyCategory}, {@link SystemHierarchy}, {@link SystemHierarchyAssignment} all use that modify condition on edits
 *
 * @author manderson
 * @see SystemEntityModifyConditionAwareAdminRequired
 */
public interface SystemEntityModifyConditionAware extends IdentityObject {

	/**
	 * Returns the {@link SystemCondition} to evaluate
	 */
	public SystemCondition getEntityModifyCondition();
}
