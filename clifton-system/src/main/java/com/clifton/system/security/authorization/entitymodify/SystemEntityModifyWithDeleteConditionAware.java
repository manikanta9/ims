package com.clifton.system.security.authorization.entitymodify;


import com.clifton.system.condition.SystemCondition;


/**
 * The <code>SystemEntityModifyWithDeleteConditionAware</code> is an interface that can be implemented
 * by a DTO and used to provide ADDITIONAL security restrictions for inserts/updates/deletes.  It supports a separate condition for deletes to provide 2 types of conditions
 * <p>
 * NOTE: This extra security check is done at the DAO level, and users must also have required permissions to get through the service method
 * <p>
 * Entities that implement this interface are automatically registered with the {@link SystemEntityModifyConditionAwareObserver}
 * <p>
 * If a delete condition is not present, and the action is a delete, will defer to the entity modify condition present.
 * If a modify condition is not present, isEntityModifyConditionRequiredForNonAdmins would prohibit anyone except for admins to edit when a modify condition is not present.
 * <p>
 * Example - System Notes - user can create/edit notes for specific note type, however for some note types, we want to be able to disable who can delete the notes
 *
 * @author manderson
 */
public interface SystemEntityModifyWithDeleteConditionAware extends SystemEntityModifyConditionAware {

	/**
	 * Returns the {@link SystemCondition} to evaluate during deletes
	 */
	public SystemCondition getEntityDeleteCondition();
}
