package com.clifton.system.security.authorization.setup;


import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SecurityAuthorizationSetupServiceImpl</code> extends security's
 * {@link SecurityAuthorizationSetupServiceImpl} with an override to get resource by a table name
 * <p>
 * Make sure this is registered in spring context so the override is applied
 *
 * @author manderson
 */
public class SecurityAuthorizationSetupServiceImpl extends com.clifton.security.authorization.setup.SecurityAuthorizationSetupServiceImpl {

	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityResource getSecurityResourceByTable(String tableName) {
		SystemTable table = getSystemSchemaService().getSystemTableByName(tableName);
		// NOTE: Table Method is Never Null as above method throws an exception if it can't be found
		return table.getSecurityResource();
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
