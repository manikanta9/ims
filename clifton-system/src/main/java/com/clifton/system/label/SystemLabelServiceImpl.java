package com.clifton.system.label;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>SystemLabelServiceImpl</code> class provides basic implementation for the SystemLabelService interface.
 *
 * @author vgomelsky
 */
@Service
public class SystemLabelServiceImpl implements SystemLabelService {

	private AdvancedUpdatableDAO<SystemLabel, Criteria> systemLabelDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemLabel getSystemLabelByTableAndPK(short tableId, Number pkFieldId) {
		ValidationUtils.assertNotNull(pkFieldId, "PK Field ID is required to lookup System Label By Table and PK Field");
		// Even if the pkFieldId is a short, in SystemLabel table it's stored as an integer to support
		// both, so need to convert it to an int
		return getSystemLabelDAO().findOneByFields(new String[]{"systemTable.id", "pkFieldId"}, new Object[]{tableId, pkFieldId.intValue()});
	}


	@Override
	public List<SystemLabel> getSystemLabelList(SystemLabelSearchForm searchForm) {
		return getSystemLabelDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<SystemLabel> getSystemLabelListByConfigurer(SearchConfigurer<Criteria> searchConfigurer) {
		return getSystemLabelDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public SystemLabel saveSystemLabel(SystemLabel bean) {
		if (bean != null) {
			// the database can hold up to 500 characters: does not make to have a label longer than that
			bean.setLabel(StringUtils.formatStringUpToNCharsWithDots(bean.getLabel(), DataTypes.DESCRIPTION.getLength(), true));
		}
		return getSystemLabelDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemLabel, Criteria> getSystemLabelDAO() {
		return this.systemLabelDAO;
	}


	public void setSystemLabelDAO(AdvancedUpdatableDAO<SystemLabel, Criteria> systemLabelDAO) {
		this.systemLabelDAO = systemLabelDAO;
	}
}
