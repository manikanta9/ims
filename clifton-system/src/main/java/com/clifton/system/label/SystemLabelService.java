package com.clifton.system.label;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import org.hibernate.Criteria;

import java.util.List;


/**
 * The <code>SystemLabelService</code> interface defines methods for working with SystemLabel objects.
 *
 * @author vgomelsky
 */
public interface SystemLabelService {

	public SystemLabel getSystemLabelByTableAndPK(short tableId, Number pkFieldId);


	@DoNotAddRequestMapping
	public List<SystemLabel> getSystemLabelListByConfigurer(SearchConfigurer<Criteria> searchConfigurer);


	public List<SystemLabel> getSystemLabelList(SystemLabelSearchForm searchForm);


	@DoNotAddRequestMapping
	public SystemLabel saveSystemLabel(SystemLabel bean);
}
