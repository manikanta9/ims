package com.clifton.system.label;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemLabel</code> class represents a user friendly label for a specific database row
 * identified by the system table and corresponding primary key field.
 *
 * @author vgomelsky
 */
public class SystemLabel extends BaseSimpleEntity<Integer> {

	private SystemTable systemTable;
	/**
	 * Id of the primary key field that the label is for and that belongs to this SystemTable.
	 */
	private int pkFieldId;
	private String label;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(128);
		result.append("{id=");
		result.append(getId());
		if (getSystemTable() != null) {
			result.append(", table=");
			result.append(getSystemTable().getName());
		}
		result.append(", pkFieldId=");
		result.append(getPkFieldId());
		result.append(", label=");
		result.append(StringUtils.formatStringUpToNCharsWithDots(getLabel(), 200));
		result.append('}');
		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	public int getPkFieldId() {
		return this.pkFieldId;
	}


	public void setPkFieldId(int pkFieldId) {
		this.pkFieldId = pkFieldId;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}
}
