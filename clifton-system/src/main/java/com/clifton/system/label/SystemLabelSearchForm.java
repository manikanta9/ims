package com.clifton.system.label;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


public class SystemLabelSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "label")
	private String searchPattern;

	@SearchField
	private String label;

	@SearchField(searchField = "systemTable.id")
	private Short tableId;

	@SearchField(searchField = "systemTable.id")
	private Short[] tableIds;

	@SearchField(searchFieldPath = "systemTable", searchField = "name")
	private String tableName;

	@SearchField
	private Integer pkFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Integer getPkFieldId() {
		return this.pkFieldId;
	}


	public void setPkFieldId(Integer pkFieldId) {
		this.pkFieldId = pkFieldId;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public Short[] getTableIds() {
		return this.tableIds;
	}


	public void setTableIds(Short[] tableIds) {
		this.tableIds = tableIds;
	}
}
