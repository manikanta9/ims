package com.clifton.system.label;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.config.NoTableDAOConfig;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>SystemLabelUpdaterJob</code> class rebuilds (inserts/updates) system labels.
 * Finds all DTO's that implement LabeledObject interface. Allows configuration options to
 * further restrict the list of objects to generate labels for.
 *
 * @author vgomelsky
 */
public class SystemLabelUpdaterJob implements Task {

	/**
	 * Number of minutes from now to look back for changes.
	 * If set to "0", then will rebuild labels for every single row.
	 */
	private int minutesOfUpdates = 90;
	/**
	 * If set to true, limits to tables that have auditing enabled.
	 */
	private boolean rebuildAuditedTablesOnly;
	/**
	 * Comma delimited list of table names for tables NEVER to generate labels for.
	 */
	private String tablesToExclude;
	/**
	 * Comma delimited list of table names for tables ALWAYS to generate labels for: regardless of rebuildAuditedTablesOnly setting.
	 * Rebuilds labels for all entities: event when create date == update date.
	 */
	private String tablesToInclude;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private DaoLocator daoLocator;
	private SystemLabelService systemLabelService;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		return doRun();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends IdentityObject & LabeledObject> Status doRun() {
		StringBuilder result = new StringBuilder();
		final Date fromDate = DateUtils.addMinutes(new Date(), -getMinutesOfUpdates());

		Set<String> excludeTables = new HashSet<>();
		if (!StringUtils.isEmpty(getTablesToExclude())) {
			excludeTables.addAll(CollectionUtils.createList(getTablesToExclude().split(",")));
		}

		final Set<String> includeTables = new HashSet<>();
		if (!StringUtils.isEmpty(getTablesToInclude())) {
			includeTables.addAll(CollectionUtils.createList(getTablesToInclude().split(",")));
		}

		List<SystemTable> auditedTables = isRebuildAuditedTablesOnly() ? getSystemSchemaService().getSystemTableListAudited() : null;

		// 1. get a list of DAO's that support labels
		Collection<ReadOnlyDAO<? extends LabeledObject>> daoList = getDaoLocator().locateAll(LabeledObject.class);
		for (ReadOnlyDAO<? extends LabeledObject> dao : daoList) {
			long startTime = System.currentTimeMillis();
			final DAOConfiguration<? extends LabeledObject> config = dao.getConfiguration();
			// Skip DAO Configs with no Tables
			if (config instanceof NoTableDAOConfig<?>) {
				continue;
			}

			if (excludeTables.contains(config.getTableName())) {
				continue;
			}

			// skip DAO's that are not being audited but INCLUDE those that are ALWAYS included
			SystemTable auditedTable = CollectionUtils.getFirstElement(BeanUtils.filter(auditedTables, SystemTable::getName, config.getTableName()));
			if (!includeTables.contains(config.getTableName())) {
				if (isRebuildAuditedTablesOnly() && auditedTable == null) {
					continue;
				}
			}

			// improve performance by creating labels only for modified entities
			final boolean updatedEntitiesOnly = (isRebuildAuditedTablesOnly() && auditedTable != null && auditedTable.getAuditType() != null && !auditedTable.getAuditType().isAuditInsert());

			// 2. look for records that were updates since the specified time
			if (config.isValidBeanField("updateDate")) {
				@SuppressWarnings("unchecked")
				AdvancedReadOnlyDAO<T, Criteria> searchDao = (AdvancedReadOnlyDAO<T, Criteria>) dao;
				List<T> updatedList = searchDao.findBySearchCriteria((HibernateSearchConfigurer) criteria -> {
					// 0 means get everything
					if (getMinutesOfUpdates() != 0) {
						criteria.add(Restrictions.ge("updateDate", fromDate));
					}
					if (!includeTables.contains(config.getTableName())) {
						if (updatedEntitiesOnly && config.isValidBeanField("createDate")) {
							criteria.add(Restrictions.neProperty("updateDate", "createDate"));
						}
					}
				});
				int count = CollectionUtils.getSize(updatedList);
				if (count > 0) {
					// 3. update labels for the table
					String resultMessage = updateLabels(updatedList, config);
					if (resultMessage != null) {
						if (result.length() > 0) {
							result.append('\n');
						}
						result.append(config.getTableName());
						result.append(": modified records = ");
						result.append(count);
						result.append("; ");
						result.append(resultMessage);

						long endTime = System.currentTimeMillis();
						result.append(" [time = ");
						result.append((endTime - startTime) / 1000);
						result.append(" seconds]");
					}
				}
			}
		}

		return Status.ofMessage(result.toString());
	}


	/**
	 * Inserts or updates SystemLabel objects as necessary for the specified DTO's.
	 */
	private <T extends IdentityObject & LabeledObject> String updateLabels(List<T> updatedDTOs, DAOConfiguration<? extends LabeledObject> config) {
		SystemTable table = getSystemSchemaService().getSystemTableByName(config.getTableName());
		int updateCount = 0;
		int insertCount = 0;
		// TODO: performance improvements: minimize DB calls (how?); batch updates?
		for (T dto : updatedDTOs) {
			SystemLabel label = getSystemLabelService().getSystemLabelByTableAndPK(table.getId(), (Number) dto.getIdentity());
			if (label == null) {
				label = new SystemLabel();
				label.setSystemTable(table);
				label.setPkFieldId(((Number) dto.getIdentity()).intValue());
				label.setLabel(dto.getLabel());
				if (label.getLabel() != null) {
					getSystemLabelService().saveSystemLabel(label);
					insertCount++;
				}
				else {
					LogUtils.warn(getClass(), "The label for existing DTO is null: class = " + dto.getClass().getName() + ", id = " + dto.getIdentity());
				}
			}
			else if (!label.getLabel().equals(dto.getLabel())) {
				if (dto.getLabel() != null) {
					label.setLabel(dto.getLabel());
					getSystemLabelService().saveSystemLabel(label);
					updateCount++;
				}
				else {
					LogUtils.warn(getClass(), "New label for existing DTO is null: class = " + dto.getClass().getName() + ", id = " + dto.getIdentity());
				}
			}
		}

		if (insertCount > 0 || updateCount > 0) {
			return "inserted = " + insertCount + "; updated = " + updateCount;
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isRebuildAuditedTablesOnly() {
		return this.rebuildAuditedTablesOnly;
	}


	public void setRebuildAuditedTablesOnly(boolean rebuildAuditedTablesOnly) {
		this.rebuildAuditedTablesOnly = rebuildAuditedTablesOnly;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public int getMinutesOfUpdates() {
		return this.minutesOfUpdates;
	}


	public void setMinutesOfUpdates(int minutesOfUpdates) {
		this.minutesOfUpdates = minutesOfUpdates;
	}


	public SystemLabelService getSystemLabelService() {
		return this.systemLabelService;
	}


	public void setSystemLabelService(SystemLabelService systemLabelService) {
		this.systemLabelService = systemLabelService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public String getTablesToExclude() {
		return this.tablesToExclude;
	}


	public void setTablesToExclude(String tablesToExclude) {
		this.tablesToExclude = tablesToExclude;
	}


	public String getTablesToInclude() {
		return this.tablesToInclude;
	}


	public void setTablesToInclude(String tablesToInclude) {
		this.tablesToInclude = tablesToInclude;
	}
}
