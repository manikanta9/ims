package com.clifton.system.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.DaoException;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.BaseDataAccessExceptionConverter;
import com.clifton.core.web.handler.WebMethodCallException;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.schema.SystemRelationship;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.search.SystemRelationshipSearchForm;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;


/**
 * The <code>DataAccessExceptionConverter</code> class converts database exceptions to corresponding {$link ValidationException}
 * instances with user friendly error messages.
 * <p>
 * This class implements MS SQL Server 2005 specific exception conversion.
 *
 * @author vgomelsky
 */
public class DataAccessExceptionConverter extends BaseDataAccessExceptionConverter {

	/**
	 * The maximum length for object names in SQL Server. Object names may be truncated upon creation or modification if they are longer than this.
	 */
	private static final int MAXIMUM_SYSNAME_LENGTH = 128;

	private SystemSchemaService systemSchemaService;
	private SystemColumnService systemColumnService;

	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LabeledObject getSecurityUser(short id) {
		return getSecurityUserService().getSecurityUser(id);
	}


	private SystemRelationship getSystemRelationshipByName(String fkName) {
		// lookup using default data source first
		SystemRelationshipSearchForm searchForm = new SystemRelationshipSearchForm();
		searchForm.setNameEquals(fkName);
		searchForm.setDefaultDataSource(true);
		List<SystemRelationship> relationshipList = getSystemSchemaService().getSystemRelationshipList(searchForm);

		if (CollectionUtils.isEmpty(relationshipList)) {
			// not found: lookup in any data source
			searchForm = new SystemRelationshipSearchForm();
			searchForm.setNameEquals(fkName);
			relationshipList = getSystemSchemaService().getSystemRelationshipList(searchForm);
		}

		return CollectionUtils.getOnlyElement(relationshipList);
	}


	@Override
	protected RuntimeException convertFromDuplicateKeyException(RuntimeException exception, SQLException sqlException) {
		String message = sqlException.getMessage();
		// MESSAGE EXAMPLE: Cannot insert duplicate key row in object 'dbo.SystemAuditType' with unique index 'ux_SystemAuditType_AuditTypeName'.
		int end = message.lastIndexOf('\'');
		int start = message.lastIndexOf('\'', end - 1);
		String indexName = message.substring(start, end);
		String[] indexElements = indexName.split("_");
		if (indexElements.length < 2) {
			return new RuntimeException("Duplicate Key error but cannot determine details because constraint does not follow our naming conventions. Message: '" + message + "'", exception);
		}
		String tableName = indexElements[1];
		String firstColumn = null;
		StringBuilder msg = new StringBuilder("The field ");
		for (int i = 2; i < indexElements.length; i++) {
			String columnName = indexElements[i];
			SystemColumnStandard column = getSystemColumnService().getSystemColumnStandardByDataSourceAndName(getDataSourceNameFromException(exception), tableName, columnName);
			if (column == null) {
				if (i == indexElements.length - 1 && indexName.length() >= MAXIMUM_SYSNAME_LENGTH) {
					// If the final field is invalid, assume index name has been truncated and indicate that further fields exist for unique constraint
					msg.append("(additional columns truncated)");
					break;
				}
				else {
					return new IllegalStateException("Cannot find the following table column: " + tableName + "." + columnName, exception);
				}
			}
			if (i == 2) {
				firstColumn = column.getBeanPropertyName();
				if (indexElements.length > 3) {
					msg.append("combination ");
				}
			}
			msg.append('\'');
			msg.append(column.getLabel());
			msg.append('\'');
			if (i < (indexElements.length - 1)) {
				msg.append(", ");
			}
		}
		msg.append(" does not allow duplicate values.");
		if (exception instanceof DaoException) {
			IdentityObject dto = ((DaoException) exception).getCauseEntity();
			msg.append(" Entity: ");
			msg.append(BeanUtils.getLabel(dto));
		}
		return new FieldValidationException(msg.toString(), firstColumn);
	}


	@Override
	@SuppressWarnings({"unchecked", "UnnecessaryUnboxing"})
	protected RuntimeException convertFromReferenceConstraintException(RuntimeException exception, SQLException sqlException) {
		String message = sqlException.getMessage();
		// MESSAGE EXAMPLE: The DELETE statement conflicted with the REFERENCE constraint "FK_SystemTable_SystemAuditType_SystemAuditTypeID". The conflict occurred in database "CliftonIMS", table "dbo.SystemTable", column 'SystemAuditTypeID'.
		// Means You can't delete SystemAuditType because it's being used by rows in System Table
		int start = message.indexOf('\"');
		int end = message.indexOf('\"', start + 1);
		String fkName = message.substring(start + 1, end);
		// Try first to just find the relationship by name
		SystemRelationship relationship = getSystemRelationshipByName(fkName);
		SystemTable parentTable;
		SystemTable table;
		String columnName;

		if (relationship != null) {
			parentTable = relationship.getParentColumn().getTable(); // SystemAuditType
			table = relationship.getColumn().getTable(); // SystemTable
			columnName = relationship.getColumn().getName();
		}
		else {
			String[] fkNames = fkName.split("_");
			// Try to figure out from FK name, if relationship doesn't exist
			parentTable = getSystemSchemaService().getSystemTableByNameAndDataSource(getDataSourceNameFromException(exception), fkNames[2]);
			AssertUtils.assertNotNull(parentTable, "Cannot find the following table: %1s", fkNames[2]);
			table = getSystemSchemaService().getSystemTableByNameAndDataSource(getDataSourceNameFromException(exception), fkNames[1]);
			AssertUtils.assertNotNull(table, "Cannot find the following table: %1s", fkNames[1]);
			columnName = fkNames[3];
		}

		// Try to Get id of the field that violated constraint
		Serializable fkFieldValue = 0;
		if (exception instanceof DaoException) {
			// DaoException has the DTO object and the ID
			// however, because Hibernate may delay database updates, original exception maybe coming from a different DTO
			IdentityObject dto = ((DaoException) exception).getCauseEntity();
			ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate((Class<IdentityObject>) dto.getClass());
			if (dao.getConfiguration().getTableName().equals(parentTable.getName())) {
				fkFieldValue = dto.getIdentity();
				if (fkFieldValue == null) {
					return new ValidationException("Cannot insert '" + BeanUtils.getLabel(dto) + "' entity because of database constraint error: " + message);
				}
			}
		}
		// Try to see from the request/handler if we can determine the id value
		else if (exception instanceof WebMethodCallException) {
			WebMethodCallException webException = (WebMethodCallException) exception;
			// First check if request table matches the table we think it should
			String webTableName = webException.getTableNameFromMethod();
			if (StringUtils.isEqual(webTableName, parentTable.getName())) {
				fkFieldValue = webException.getRequestId();
			}
		}
		if (StringUtils.isEmpty(columnName) || "0".equals(fkFieldValue.toString())) {
			// Give a more generic message since we don't have access to the actual bean that threw the error
			// This happens because DaoException may not be thrown by HibernateUpdatableDAO since SQL statements are not always executed immediately so error is thrown later
			return new ValidationException("Cannot delete selected " + parentTable.getLabel() + " row because it is referenced by '" + table.getLabel() + "' row(s).");
		}
		// get row(s) that reference the row being deleted: making the error message more informative
		ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(table.getName());
		String fkFieldName = dao.getConfiguration().getBeanFieldName(columnName);
		String unchangedFkFieldName = fkFieldName;
		if (!fkFieldName.endsWith("Id")) {
			fkFieldName += ".id";
		}
		Class<?> fkType = BeanUtils.getPropertyType(dao.getConfiguration().getBeanClass(), fkFieldName);
		if (!fkFieldValue.getClass().equals(fkType)) {
			// need to do casting if the class is different
			if (fkType.equals(Integer.class) || fkType.equals(int.class)) {
				fkFieldValue = Integer.valueOf(fkFieldValue.toString());
			}
			else if (fkType.equals(Long.class) || fkType.equals(long.class)) {
				fkFieldValue = Long.valueOf(fkFieldValue.toString());
			}
			else if (fkType.equals(Short.class) || fkType.equals(short.class)) {
				fkFieldValue = Short.valueOf(fkFieldValue.toString());
			}
			else {
				return new RuntimeException("Cannot cast " + fkFieldValue + " value to " + fkType + " for " + fkFieldName + " field for " + dao.getConfiguration().getBeanClass());
			}
		}
		if (!dao.getConfiguration().isValidBeanField(unchangedFkFieldName)) {
			return new RuntimeException("Error in exception translation: cannot find '" + unchangedFkFieldName + "' DAO field for table '" + table.getName() + "'", exception);
		}
		List<? extends IdentityObject> referencingList = dao.findByField(fkFieldName, fkFieldValue);
		if (CollectionUtils.isEmpty(referencingList)) {
			String err = "Error during delete.  Cannot find referencing beans in table " + table.getName() + " where " + fkFieldName + " = " + fkFieldValue + "; Original Message: " + message;
			return new RuntimeException(err, exception);
		}
		//Not sure what to default this to, or why the sortOrder field would be the default label of the row
		String labelField = (referencingList.get(0) instanceof LabeledObject) ? "label" : dao.getConfiguration().getOrderByList().get(0).getName();
		String labels = BeanUtils.getPropertyValues(referencingList, labelField, ", ", "'", "'", ", ...", 5);
		return new ValidationException("Cannot delete selected " + parentTable.getLabel() + " row because it is referenced by the following '" + table.getLabel() + "' row(s): " + labels + ".");
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
