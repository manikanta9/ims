package com.clifton.system.schema.sizehistory;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlHandlerLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * A batch job that, when run, will calculate table / index size data for specified tables from specified data sources
 * and save that data to the SystemTableSizeHistory table.
 *
 * @author davidi
 */
public class SystemTableSizeHistoryJob implements Task {

	private static final int COL_INDEX_SYSTEM_TABLE_NAME = 1;
	private static final int COL_INDEX_SNAPSHOT_DATE = 2;
	private static final int COL_INDEX_TOTAL_ROWS = 3;
	private static final int COL_INDEX_DATA_SIZE = 4;
	private static final int COL_INDEX_INDEX_SIZE = 5;
	private static final int COL_INDEX_UNUSED_SIZE = 6;

	private final static String SQL_TABLESIZE_QUERY =
			"SET NOCOUNT ON\n" +
					"DECLARE @TableSizes Table (\n" +
					"\tName Varchar(200),\n" +
					"\tRows int,\n" +
					"\tReserved Varchar(200),\n" +
					"\tDataSize Varchar(200),\n" +
					"\tIndexSize Varchar(200),\n" +
					"\tUnused Varchar(200))\n" +
					"\n" +
					"DECLARE @TableName Varchar(100),\n" +
					"\t@SchemaName Varchar(100)\n" +
					"SET @SchemaName = 'dbo'\n" +
					"\n" +
					"\tDECLARE TableAnalysis CURSOR FOR\n" +
					"\t\tSELECT ss.name + '.' + t.Name\n" +
					"\t\tFROM sys.tables t\n" +
					"\t\t\tinner join sys.Schemas ss on t.Schema_ID = ss.Schema_ID\n" +
					"\t\tWHERE t.Type = 'U'\n" +
					"\n" +
					"\tOPEN TableAnalysis\n" +
					"\n" +
					"\tFETCH NEXT FROM TableAnalysis into @TableName\n" +
					"\n" +
					"\tWHILE @@Fetch_Status = 0\n" +
					"\tBEGIN\n" +
					"\t\tprint @TableName\n" +
					"\t\tInsert Into @TableSizes\n" +
					"\t\tEXEC sp_spaceused @TableName\n" +
					"\n" +
					"\tFETCH NEXT FROM TableAnalysis into @TableName\n" +
					"\n" +
					"\tEND\n" +
					"\n" +
					"\tCLOSE TableAnalysis\n" +
					"\tDEALLOCATE TableAnalysis\n" +
					"\n" +
					"\n" +
					"-- All sizes are now in bytes, not KB \n" +
					"SELECT\n" +
					"\tSystemTableName = Name,\n" +
					"\tSnapshotDate = GETDATE(),\n" +
					"\tTotalRows = Rows,\n" +
					"\tDataSizeInBytes = Convert(bigint, Replace(Datasize, 'KB', '')) * 1024,\n" +
					"\tIndexSizeInBytes = Convert(bigint, Replace(IndexSize, 'KB', '')) * 1024,\n" +
					"\tUnusedSizeInBytes = Convert(bigint, Replace(Unused, 'KB', '')) * 1024\n" +
					"FROM @TableSizes\n" +
					"ORDER by name\n" +
					"SET NOCOUNT OFF";


	////////////////////////////////////////////////////////////////////////////


	private Short[] dataSourceIdArray;
	private String[] includesExpressions;
	private String[] excludesExpressions;


	////////////////////////////////////////////////////////////////////////////


	private SystemTableSizeHistoryService systemTableSizeHistoryService;
	private SystemSchemaService systemSchemaService;

	private SqlHandlerLocator sqlHandlerLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = new Status();

		List<Pattern> includeRegexList = createRegularExpressionList(getIncludesExpressions());
		List<Pattern> excludeRegexList = createRegularExpressionList(getExcludesExpressions());

		Map<String, Short> statusDetails = new HashMap<>();

		for (SystemDataSource dataSource : CollectionUtils.getIterable(getDataSources())) {
			try {
				for (SystemTableSizeHistory systemTableSizeHistory : CollectionUtils.getIterable(getDatabaseTableSizeStats(dataSource))) {
					if (isTableIncluded(systemTableSizeHistory.getSystemTable().getName(), includeRegexList, excludeRegexList)) {
						getSystemTableSizeHistoryService().saveSystemTableSizeHistory(systemTableSizeHistory);
						String databaseName = systemTableSizeHistory.getSystemTable().getDataSource().getDatabaseName();
						statusDetails.compute(databaseName, (key, currentValue) -> currentValue == null ? 1 : ++currentValue);
					}
				}
			}
			catch (Exception exc) {
				status.addDetail(StatusDetail.CATEGORY_ERROR, String.format("Job completed with errors: %s. Failed to get table size stats for tables in datasource '%s'.",
						exc.getMessage(), dataSource.getName()));
				LogUtils.error(getClass(), String.format("Failed to get table size stats for tables in datasource %s.", dataSource.getName()), exc);
			}
		}

		StringBuilder detailMessage = new StringBuilder("Tables updated:\n\n");
		statusDetails.forEach((databaseName, updateCount) -> detailMessage
				.append(databaseName)
				.append(": ")
				.append(updateCount)
				.append('\n'));

		status.setMessage((status.getErrorCount() == 0 ? "" : "Job completed with errors\n") + detailMessage);

		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<SystemDataSource> getDataSources() {
		if (ArrayUtils.isEmpty(getDataSourceIdArray())) {
			// exclude external data sources (do not have a DB connection to them)
			return BeanUtils.filter(getSystemSchemaService().getSystemDataSourceList(), d -> !d.isExternal());
		}

		List<SystemDataSource> systemDataSourceList = new ArrayList<>();

		for (Short dataSourceId : getDataSourceIdArray()) {
			systemDataSourceList.add(getSystemSchemaService().getSystemDataSource(dataSourceId));
		}
		return systemDataSourceList;
	}


	private List<SystemTableSizeHistory> getDatabaseTableSizeStats(SystemDataSource dataSource) {
		SqlHandler sqlHandler = getSqlHandlerLocator().locate(dataSource.getName());

		List<SystemTableSizeHistory> systemTableSizeHistoryList = new ArrayList<>();
		List<SystemTableSizeHistory> queryResultList = sqlHandler.executeSelect(SQL_TABLESIZE_QUERY, getSystemTableSizeHistoryResultSetExtractor(dataSource));

		for (SystemTableSizeHistory systemTableSizeHistory : CollectionUtils.getIterable(queryResultList)) {
			if (systemTableSizeHistory != null) {
				systemTableSizeHistoryList.add(systemTableSizeHistory);
			}
		}

		return systemTableSizeHistoryList;
	}


	private boolean isTableIncluded(String tableName, List<Pattern> includePatternList, List<Pattern> excludePatternList) {

		// Rule:  empty include regex list automatically includes a table (it may however be excluded in the exclusionRegex).
		boolean isTableIncluded = CollectionUtils.isEmpty(includePatternList);

		for (Pattern inclusionRegex : CollectionUtils.getIterable(includePatternList)) {
			if (inclusionRegex.matcher(tableName).matches()) {
				isTableIncluded = true;
				break;
			}
		}

		if (!isTableIncluded) {
			return isTableIncluded;
		}

		for (Pattern exclusionRegex : CollectionUtils.getIterable(excludePatternList)) {
			if (exclusionRegex.matcher(tableName).matches()) {
				isTableIncluded = false;
				break;
			}
		}

		return isTableIncluded;
	}


	private List<Pattern> createRegularExpressionList(String[] expressionList) {

		List<Pattern> regularExpressionList = new ArrayList<>();

		if (ArrayUtils.isEmpty(expressionList)) {
			return regularExpressionList;
		}

		for (String regexExpressionString : expressionList) {
			if (!StringUtils.isEmpty(regexExpressionString)) {
				regularExpressionList.add(Pattern.compile(regexExpressionString));
			}
		}

		return regularExpressionList;
	}


	private ResultSetExtractor<List<SystemTableSizeHistory>> getSystemTableSizeHistoryResultSetExtractor(SystemDataSource dataSource) {
		return resultSet -> {
			List<SystemTableSizeHistory> result = new ArrayList<>();
			while (resultSet.next()) {
				String systemTableName = resultSet.getString(COL_INDEX_SYSTEM_TABLE_NAME);

				if (systemTableName != null) {
					// strip off schema prefix (e.g. "dbo.")
					if (systemTableName.contains(".")) {
						systemTableName = StringUtils.substringAfterLast(systemTableName, ".");
					}

					SystemTable systemTable = getSystemSchemaService().getSystemTableByNameAndDataSource(dataSource.getName(), systemTableName);

					if (systemTable != null) {
						SystemTableSizeHistory sizeHistory = new SystemTableSizeHistory();
						sizeHistory.setSystemTable(systemTable);
						sizeHistory.setSnapshotDate(resultSet.getDate(COL_INDEX_SNAPSHOT_DATE));
						sizeHistory.setNumberOfRows(resultSet.getLong(COL_INDEX_TOTAL_ROWS));
						sizeHistory.setDataSizeInBytes(resultSet.getLong(COL_INDEX_DATA_SIZE));
						sizeHistory.setIndexSizeInBytes(resultSet.getLong(COL_INDEX_INDEX_SIZE));
						sizeHistory.setUnusedSizeInBytes(resultSet.getLong(COL_INDEX_UNUSED_SIZE));

						result.add(sizeHistory);
					}
				}
			}
			return result;
		};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short[] getDataSourceIdArray() {
		return this.dataSourceIdArray;
	}


	public void setDataSourceIdArray(Short[] dataSourceIdList) {
		this.dataSourceIdArray = dataSourceIdList;
	}


	public String[] getIncludesExpressions() {
		return this.includesExpressions;
	}


	public void setIncludesExpressions(String[] includesExpressions) {
		this.includesExpressions = includesExpressions;
	}


	public String[] getExcludesExpressions() {
		return this.excludesExpressions;
	}


	public void setExcludesExpressions(String[] excludesExpressions) {
		this.excludesExpressions = excludesExpressions;
	}


	public SystemTableSizeHistoryService getSystemTableSizeHistoryService() {
		return this.systemTableSizeHistoryService;
	}


	public void setSystemTableSizeHistoryService(SystemTableSizeHistoryService systemTableSizeHistoryService) {
		this.systemTableSizeHistoryService = systemTableSizeHistoryService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SqlHandlerLocator getSqlHandlerLocator() {
		return this.sqlHandlerLocator;
	}


	public void setSqlHandlerLocator(SqlHandlerLocator sqlHandlerLocator) {
		this.sqlHandlerLocator = sqlHandlerLocator;
	}
}
