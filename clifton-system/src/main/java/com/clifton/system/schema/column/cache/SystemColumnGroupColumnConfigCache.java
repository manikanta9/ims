package com.clifton.system.schema.column.cache;

/**
 * @author manderson
 */
public interface SystemColumnGroupColumnConfigCache {

	public SystemColumnGroupColumnConfig getSystemColumnGroupColumnConfig(String columnGroupName);


	public void setSystemColumnGroupColumnConfig(SystemColumnGroupColumnConfig config);
}
