package com.clifton.system.schema.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.system.schema.SystemDataSource;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemDataSourceListByDefaultCache</code> caches the list of data sources by the defaultDataSource flag.  Note: used only to find non-default data sources as there is a specific cache for the defaultDataSource=true cache
 *
 * @author manderson
 */
@Component
public class SystemDataSourceListByDefaultCache extends SelfRegisteringSingleKeyDaoListCache<SystemDataSource, Boolean> {


	@Override
	protected String getBeanKeyProperty() {
		return "defaultDataSource";
	}


	@Override
	protected Boolean getBeanKeyValue(SystemDataSource bean) {
		if (bean != null) {
			return bean.isDefaultDataSource();
		}
		return false;
	}
}
