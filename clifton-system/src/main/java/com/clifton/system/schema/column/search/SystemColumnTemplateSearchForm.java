package com.clifton.system.schema.column.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class SystemColumnTemplateSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField(searchField = "systemColumnGroup.id", comparisonConditions = ComparisonConditions.EQUALS)
	private Short systemColumnGroupId;

	@SearchField(searchField = "systemColumnGroup.id")
	private Short[] systemColumnGroupIds;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String name;

	@SearchField(searchField = "description")
	private String description;

	@SearchField(searchField = "label")
	private String label;

	@SearchField(searchField = "table.id", searchFieldPath = "systemColumnGroup")
	private Short tableId;

	@SearchField(searchField = "name", searchFieldPath = "systemColumnGroup.table", comparisonConditions = ComparisonConditions.EQUALS)
	private String tableName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getSystemColumnGroupId() {
		return this.systemColumnGroupId;
	}


	public void setSystemColumnGroupId(Short systemColumnGroupId) {
		this.systemColumnGroupId = systemColumnGroupId;
	}


	public Short[] getSystemColumnGroupIds() {
		return this.systemColumnGroupIds;
	}


	public void setSystemColumnGroupIds(Short[] systemColumnGroupIds) {
		this.systemColumnGroupIds = systemColumnGroupIds;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
}
