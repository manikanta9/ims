package com.clifton.system.schema.jobs;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.web.api.client.locator.ExternalServiceLocator;
import com.clifton.security.authorization.setup.SecurityAuthorizationSetupService;
import com.clifton.system.audit.SystemAuditService;
import com.clifton.system.audit.SystemAuditType;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.schema.column.validation.SystemColumnValidator;
import com.clifton.system.schema.search.SystemTableSearchForm;
import com.clifton.system.schema.sizehistory.SystemTableSizeHistory;
import com.clifton.system.schema.sizehistory.SystemTableSizeHistorySearchForm;
import com.clifton.system.schema.sizehistory.SystemTableSizeHistoryService;

import java.util.List;
import java.util.Map;


/**
 * The SystemSchemaReplicationJob batch job replicates system schema and related information from source to target application.
 * It has a number of parameters to allow flexible configuration and advanced use cases.
 * <p>
 * For example:
 * - lock system table modification in Reconciliation application by limiting them to 'systemuser' only via SystemTable insert/update/delete conditions.
 * - create a batch job in IMS that will only insert into IMS SystemTable new tables from Reconciliation.
 * - create a second job in IMS that will push all IMS updates to Reconciliation.
 * - both jobs can limit the fields that will be managed by IMS. You can even enhance conditions to allow some fields to be managed by Reconciliation.
 * - IMS jobs will be configured to run as systemuser so that no changes can be performed by any individual.
 * - because changes to batch jobs are controlled via Workflow Tasks in IMS, we will keep centralized control in IMS for other systems.
 *
 * @author vgomelsky
 */
public class SystemSchemaReplicationJob implements Task, ValidationAware, StatusHolderObjectAware<Status> {

	private short sourceDataSourceId;
	private short targetDataSourceId;

	private boolean replicateTableInserts;
	private boolean replicateTableUpdates;
	private String[] tableFieldsToReplicate;

	private boolean replicateColumnInserts;
	private boolean replicateColumnUpdates;
	private boolean replicateColumnDeletes;
	private String[] columnFieldsToReplicate;

	private boolean replicateTableSizeHistory;


	private StatusHolderObject<Status> statusHolder;


	private ExternalServiceLocator externalServiceLocator;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEquals(getSourceDataSourceId(), getTargetDataSourceId(), "Source and Target data sources cannot be the same.");

		// validate that dependent services exist in both source and target data sources
		SystemDataSource sourceDataSource = getSystemSchemaService().getSystemDataSource(getSourceDataSourceId());
		SystemDataSource targetDataSource = getSystemSchemaService().getSystemDataSource(getTargetDataSourceId());

		getExternalServiceLocator().getExternalService(SystemSchemaService.class, sourceDataSource.getName());
		getExternalServiceLocator().getExternalService(SystemSchemaService.class, targetDataSource.getName());

		if (isReplicateTableUpdates()) {
			getExternalServiceLocator().getExternalService(SystemTableSizeHistoryService.class, sourceDataSource.getName());
			getExternalServiceLocator().getExternalService(SystemTableSizeHistoryService.class, targetDataSource.getName());
		}

		// validate that all properties on SystemTable object are valid
		ValidationUtils.assertFalse(ArrayUtils.isEmpty(getTableFieldsToReplicate()), "Required job property is missing: tableFieldsToReplicate");
		for (String propertyName : getTableFieldsToReplicate()) {
			if (!BeanUtils.isPropertyPresent(SystemTable.class, propertyName)) {
				throw new ValidationException("Invalid SystemTable bean property: " + propertyName);
			}
		}

		if (isReplicateColumnInserts() || isReplicateColumnUpdates()) {
			ValidationUtils.assertFalse(ArrayUtils.isEmpty(getColumnFieldsToReplicate()), "SystemColumn fields to replicate must be specified when replicating column Inserts or Updates.");
		}
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolder.getStatus();
		status.setMessage(null); // details will be appended for each replication

		SystemDataSource sourceDataSource = getSystemSchemaService().getSystemDataSource(getSourceDataSourceId());
		SystemSchemaService sourceSystemSchemaService = getExternalServiceLocator().getExternalService(SystemSchemaService.class, sourceDataSource.getName());

		SystemDataSource targetDataSource = getSystemSchemaService().getSystemDataSource(getTargetDataSourceId());
		SystemSchemaService targetSystemSchemaService = getExternalServiceLocator().getExternalService(SystemSchemaService.class, targetDataSource.getName());

		List<SystemTable> sourceTableList = getSystemTableList(sourceSystemSchemaService, targetDataSource);
		List<SystemTable> targetTableList = getSystemTableList(targetSystemSchemaService, sourceDataSource);
		Map<String, SystemTable> targetMap = BeanUtils.getBeanMapUnique(targetTableList, SystemTable::getName);

		if (isReplicateTableInserts()) {
			String message = null;
			for (SystemTable sourceTable : CollectionUtils.getIterable(sourceTableList)) {
				if (!targetMap.containsKey(sourceTable.getName())) {
					// insert SystemTable from Source to Target data source
					SystemTable targetTable = new SystemTable();
					if (populateAndSaveTargetTable(targetTable, sourceTable, targetSystemSchemaService, targetDataSource, sourceDataSource, status)) {
						message = ((message == null) ? "" : message + ", ") + sourceTable.getName();
						targetMap.put(targetTable.getName(), targetTable); // in order to enable dependent replications
					}
				}
			}
			status.appendToMessage("Inserted: " + (message != null ? message : "0 out of " + CollectionUtils.getSize(sourceTableList)));
		}

		if (isReplicateTableUpdates()) {
			String message = null;
			for (SystemTable sourceTable : CollectionUtils.getIterable(sourceTableList)) {
				SystemTable targetTable = targetMap.get(sourceTable.getName());
				if (targetTable != null) {
					// update SystemTable from Source to Target data source if changes detected
					if (populateAndSaveTargetTable(targetTable, sourceTable, targetSystemSchemaService, targetDataSource, sourceDataSource, status)) {
						message = ((message == null) ? "" : message + ", ") + sourceTable.getName();
					}
				}
			}
			status.appendToMessage("Updated: " + (message != null ? message : "0 out of " + CollectionUtils.getSize(sourceTableList)));
		}

		if (isReplicateColumnUpdates() || isReplicateColumnInserts() || isReplicateColumnDeletes()) {
			SystemColumnService sourceSystemColumnService = getExternalServiceLocator().getExternalService(SystemColumnService.class, sourceDataSource.getName());
			SystemColumnService targetSystemColumnService = getExternalServiceLocator().getExternalService(SystemColumnService.class, targetDataSource.getName());
			for (SystemTable sourceTable : CollectionUtils.getIterable(sourceTableList)) {
				SystemTable targetTable = targetMap.get(sourceTable.getName());
				if (targetTable != null) {
					replicateTableColumns(targetTable, sourceTable, targetSystemColumnService, sourceSystemColumnService, targetDataSource, status);
				}
				else {
					status.addSkipped(sourceTable.getName() + " source table is not defined in " + targetDataSource.getDatabaseName());
				}
			}
		}

		if (isReplicateTableSizeHistory()) {
			String message = null;
			SystemTableSizeHistoryService sourceHistoryService = getExternalServiceLocator().getExternalService(SystemTableSizeHistoryService.class, sourceDataSource.getName());
			SystemTableSizeHistoryService targetHistoryService = getExternalServiceLocator().getExternalService(SystemTableSizeHistoryService.class, targetDataSource.getName());
			for (SystemTable sourceTable : CollectionUtils.getIterable(sourceTableList)) {
				SystemTable targetTable = targetMap.get(sourceTable.getName());
				if (targetTable != null) {
					int count = replicateTableSizeHistory(targetTable, sourceTable, targetHistoryService, sourceHistoryService, status);
					if (count != 0) {
						message = ((message == null) ? "" : message + ", ") + sourceTable.getName() + ": " + count;
					}
				}
				else {
					status.addSkipped(sourceTable.getName() + " source table is not defined in " + targetDataSource.getDatabaseName());
				}
			}
			status.appendToMessage("History: " + (message != null ? message : "0 out of " + CollectionUtils.getSize(sourceTableList)));
		}

		return status;
	}


	/**
	 * For REMOTE service calls, returns tables for the Default Data Source.
	 * For LOCAL service calls, remaps the other side data source based on the database name to the local data source id.
	 */
	private List<SystemTable> getSystemTableList(SystemSchemaService systemSchemaService, SystemDataSource otherSideDataSource) {
		SystemTableSearchForm searchForm = new SystemTableSearchForm();
		if (getExternalServiceLocator().isExternalService(systemSchemaService)) {
			// external service call: get tables for the default data source from the external location
			searchForm.setDefaultDataSource(true);
		}
		else {
			// local call: use data source id (multiple apps could use the same DB name)
			searchForm.setDataSourceId(otherSideDataSource.getId());
		}

		return systemSchemaService.getSystemTableList(searchForm);
	}


	/**
	 * Populates the specified target table object from the specified source table.  Only populates require properties and remaps dependent objects as necessary.
	 * Saves only if no changes were made.
	 *
	 * @return returns true if the target table was updated or false if no properties were changed
	 */
	private boolean populateAndSaveTargetTable(SystemTable targetTable, SystemTable sourceTable, SystemSchemaService targetSystemSchemaService, SystemDataSource targetDataSource, SystemDataSource sourceDataSource, Status status) {
		boolean result = targetTable.isNewBean();
		try {
			for (String fieldName : getTableFieldsToReplicate()) {
				Object sourceValue = BeanUtils.getPropertyValue(sourceTable, fieldName);
				if (sourceValue != null) {
					// need to remap the value into corresponding application entity by natural key
					if (sourceValue instanceof SystemDataSource) {
						sourceValue = getExternalServiceLocator().isExternalService(targetSystemSchemaService) ? targetSystemSchemaService.getSystemDataSourceDefault() : sourceDataSource;
					}
					else if (sourceValue instanceof SystemAuditType) {
						sourceValue = getExternalServiceLocator().getExternalService(SystemAuditService.class, targetDataSource.getName()).getSystemAuditTypeByName(sourceTable.getAuditType().getName());
					}
					else if (sourceValue instanceof SecurityResource) {
						sourceValue = getExternalServiceLocator().getExternalService(SecurityAuthorizationSetupService.class, targetDataSource.getName()).getSecurityResourceByName(sourceTable.getSecurityResource().getName());
					}
					else if (sourceValue instanceof SystemCondition) {
						sourceValue = getExternalServiceLocator().getExternalService(SystemConditionService.class, targetDataSource.getName()).getSystemConditionByName(((SystemCondition) sourceValue).getName());
					}

					if (sourceValue == null) {
						// do not throw an exception to allow some fields to update even when there was an error
						status.addError("Cannot find SystemTable Foreign Key for property '" + fieldName + "' with value '" + BeanUtils.getPropertyValue(sourceTable, fieldName) + "' in target database: " + targetDataSource.getDatabaseName());
						result = false;
						break;
					}
				}

				Object targetValue = BeanUtils.getPropertyValue(targetTable, fieldName);
				if (!isEqualUsingNaturalKey(sourceValue, targetValue)) {
					BeanUtils.setPropertyValue(targetTable, fieldName, sourceValue);
					result = true;
				}
			}

			if (result) {
				targetSystemSchemaService.saveSystemTable(targetTable);
			}
		}
		catch (Throwable e) {
			status.addError("Error saving " + sourceTable.getName() + ": " + ExceptionUtils.getDetailedMessage(e));
			LogUtils.error(getClass(), "Error saving " + sourceTable.getName(), e);
			result = false;
		}

		return result;
	}


	/**
	 * Copies only new historic records from source to target system. If changed, updates the very last target history measurements.
	 *
	 * @return returns the number of history records replicated.
	 */
	private int replicateTableSizeHistory(SystemTable targetTable, SystemTable sourceTable, SystemTableSizeHistoryService targetHistoryService, SystemTableSizeHistoryService sourceHistoryService, Status status) {
		int replicationCount = 0;
		try {
			// find the last history that was replicated
			SystemTableSizeHistorySearchForm searchForm = new SystemTableSizeHistorySearchForm();
			searchForm.setSystemTableId(targetTable.getId());
			searchForm.setOrderBy("snapshotDate:desc");
			searchForm.setLimit(1);
			SystemTableSizeHistory lastTargetHistory = CollectionUtils.getFirstElement(targetHistoryService.getSystemTableSizeHistoryList(searchForm));

			// find all entries to replicate
			searchForm = new SystemTableSizeHistorySearchForm();
			searchForm.setSystemTableId(sourceTable.getId());
			if (lastTargetHistory != null) {
				searchForm.setMinSnapshotDate(lastTargetHistory.getSnapshotDate());
			}
			List<SystemTableSizeHistory> sourceHistoryList = sourceHistoryService.getSystemTableSizeHistoryList(searchForm);
			for (SystemTableSizeHistory sourceHistory : CollectionUtils.getIterable(sourceHistoryList)) {
				SystemTableSizeHistory targetHistory = sourceHistory.cloneForTable(targetTable);
				if (lastTargetHistory != null && DateUtils.isEqualWithoutTime(lastTargetHistory.getSnapshotDate(), targetHistory.getSnapshotDate())) {
					// check if the last record needs to be updated
					if (lastTargetHistory.isSameNumericMetrics(sourceHistory)) {
						continue;
					}
					targetHistory.populateNumericMetricsFrom(sourceHistory);
				}
				targetHistoryService.saveSystemTableSizeHistory(targetHistory);
				replicationCount++;
			}
		}
		catch (Throwable e) {
			status.addError("Error saving history for " + sourceTable.getName() + ": " + ExceptionUtils.getDetailedMessage(e));
			LogUtils.error(getClass(), "Error saving history for " + sourceTable.getName(), e);
		}

		return replicationCount;
	}


	/**
	 * Insert/update/delete columns from the specified to the specified target table.
	 */
	private void replicateTableColumns(SystemTable targetTable, SystemTable sourceTable, SystemColumnService targetSystemColumnService, SystemColumnService sourceSystemColumnService, SystemDataSource targetDataSource, Status status) {
		try {
			SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
			searchForm.setTableId(sourceTable.getId());
			List<SystemColumnStandard> sourceColumnList = sourceSystemColumnService.getSystemColumnStandardList(searchForm);

			searchForm = new SystemColumnSearchForm();
			searchForm.setTableId(targetTable.getId());
			List<SystemColumnStandard> targetColumnList = targetSystemColumnService.getSystemColumnStandardList(searchForm);
			Map<String, SystemColumnStandard> targetColumnMap = BeanUtils.getBeanMapUnique(targetColumnList, SystemColumn::getName);


			String insertMessage = null;
			String updateMessage = null;
			String deleteMessage = null;
			for (SystemColumnStandard sourceColumn : CollectionUtils.getIterable(sourceColumnList)) {
				SystemColumnStandard targetColumn = targetColumnMap.remove(sourceColumn.getName());
				if (targetColumn == null) {
					if (isReplicateColumnInserts()) {
						targetColumn = new SystemColumnStandard();
						targetColumn.setTable(targetTable);
						if (populateAndSaveTargetColumn(targetColumn, sourceColumn, targetSystemColumnService, targetDataSource, status)) {
							insertMessage = ((insertMessage == null) ? "" : insertMessage + ", ") + sourceColumn.getName();
						}
					}
				}
				else if (isReplicateColumnUpdates()) {
					if (populateAndSaveTargetColumn(targetColumn, sourceColumn, targetSystemColumnService, targetDataSource, status)) {
						updateMessage = ((updateMessage == null) ? "" : updateMessage + ", ") + sourceColumn.getName();
					}
				}
			}

			if (isReplicateColumnDeletes()) {
				for (SystemColumn targetColumn : targetColumnMap.values()) {
					// need to disable validation: deletes to standard columns are now allowed
					DaoUtils.executeWithSpecificObserversDisabled(() -> targetSystemColumnService.deleteSystemColumn(targetColumn.getId()), SystemColumnValidator.class);
					deleteMessage = ((deleteMessage == null) ? "" : deleteMessage + ", ") + targetColumn.getName();
				}
			}

			if (insertMessage != null || updateMessage != null || deleteMessage != null) {
				status.appendToMessage(targetTable.getName() + (insertMessage == null ? "" : " Inserted: " + insertMessage) + (updateMessage == null ? "" : " Updated: " + updateMessage) + (deleteMessage == null ? "" : " Deleted: " + deleteMessage));
			}
		}
		catch (Throwable e) {
			status.addError("Error replicating columns for " + sourceTable.getName() + ": " + ExceptionUtils.getDetailedMessage(e));
			LogUtils.error(getClass(), "Error replicating columns for " + sourceTable.getName(), e);
		}
	}


	/**
	 * Populates the specified target column object from the specified source column.  Only populates require properties and remaps dependent objects as necessary.
	 * Saves only if no changes were made.
	 *
	 * @return returns true if the target column was updated or false if no properties were changed
	 */
	private boolean populateAndSaveTargetColumn(SystemColumnStandard targetColumn, SystemColumnStandard sourceColumn, SystemColumnService targetSystemColumnService, SystemDataSource targetDataSource, Status status) {
		boolean result = targetColumn.isNewBean();
		try {
			for (String fieldName : getColumnFieldsToReplicate()) {
				Object sourceValue = BeanUtils.getPropertyValue(sourceColumn, fieldName);
				if (sourceValue != null) {
					// need to remap the value into corresponding application entity by natural key
					if (sourceValue instanceof SystemTable) {
						sourceValue = targetColumn.getTable();
					}
					else if (sourceValue instanceof SystemAuditType) {
						sourceValue = getExternalServiceLocator().getExternalService(SystemAuditService.class, targetDataSource.getName()).getSystemAuditTypeByName(sourceColumn.getAuditType().getName());
					}
					else if (sourceValue instanceof SystemDataType) {
						sourceValue = getExternalServiceLocator().getExternalService(SystemSchemaService.class, targetDataSource.getName()).getSystemDataTypeByName(sourceColumn.getDataType().getName());
					}

					if (sourceValue == null) {
						// do not throw an exception to allow some fields to update even when there was an error
						status.addError("Cannot find SystemColumn Foreign Key for property '" + fieldName + "' with value '" + BeanUtils.getPropertyValue(sourceColumn, fieldName) + "' in target database: " + targetDataSource.getDatabaseName());
						result = false;
						break;
					}
				}

				Object targetValue = BeanUtils.getPropertyValue(targetColumn, fieldName);
				if (!isEqualUsingNaturalKey(sourceValue, targetValue)) {
					BeanUtils.setPropertyValue(targetColumn, fieldName, sourceValue);
					result = true;
				}
			}

			if (result) {
				// need to disable validation: updates to standard columns are now allowed
				DaoUtils.executeWithSpecificObserversDisabled(() -> targetSystemColumnService.saveSystemColumnStandard(targetColumn), SystemColumnValidator.class);
			}
		}
		catch (Throwable e) {
			status.addError("Error saving " + sourceColumn.getName() + ": " + ExceptionUtils.getDetailedMessage(e));
			LogUtils.error(getClass(), "Error saving " + sourceColumn.getName(), e);
			result = false;
		}

		return result;
	}


	/**
	 * Assumes that all DTO's have name as the natural key.
	 */
	private boolean isEqualUsingNaturalKey(Object source, Object target) {
		if (source instanceof NamedObject) {
			source = ((NamedObject) source).getName();
			if (target != null) {
				target = ((NamedObject) target).getName();
			}
		}
		return CompareUtils.isEqual(source, target);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public short getSourceDataSourceId() {
		return this.sourceDataSourceId;
	}


	public void setSourceDataSourceId(short sourceDataSourceId) {
		this.sourceDataSourceId = sourceDataSourceId;
	}


	public short getTargetDataSourceId() {
		return this.targetDataSourceId;
	}


	public void setTargetDataSourceId(short targetDataSourceId) {
		this.targetDataSourceId = targetDataSourceId;
	}


	public boolean isReplicateTableInserts() {
		return this.replicateTableInserts;
	}


	public void setReplicateTableInserts(boolean replicateTableInserts) {
		this.replicateTableInserts = replicateTableInserts;
	}


	public boolean isReplicateTableUpdates() {
		return this.replicateTableUpdates;
	}


	public void setReplicateTableUpdates(boolean replicateTableUpdates) {
		this.replicateTableUpdates = replicateTableUpdates;
	}


	public String[] getTableFieldsToReplicate() {
		return this.tableFieldsToReplicate;
	}


	public void setTableFieldsToReplicate(String[] tableFieldsToReplicate) {
		this.tableFieldsToReplicate = tableFieldsToReplicate;
	}


	public boolean isReplicateColumnInserts() {
		return this.replicateColumnInserts;
	}


	public void setReplicateColumnInserts(boolean replicateColumnInserts) {
		this.replicateColumnInserts = replicateColumnInserts;
	}


	public boolean isReplicateColumnUpdates() {
		return this.replicateColumnUpdates;
	}


	public void setReplicateColumnUpdates(boolean replicateColumnUpdates) {
		this.replicateColumnUpdates = replicateColumnUpdates;
	}


	public boolean isReplicateColumnDeletes() {
		return this.replicateColumnDeletes;
	}


	public void setReplicateColumnDeletes(boolean replicateColumnDeletes) {
		this.replicateColumnDeletes = replicateColumnDeletes;
	}


	public String[] getColumnFieldsToReplicate() {
		return this.columnFieldsToReplicate;
	}


	public void setColumnFieldsToReplicate(String[] columnFieldsToReplicate) {
		this.columnFieldsToReplicate = columnFieldsToReplicate;
	}


	public boolean isReplicateTableSizeHistory() {
		return this.replicateTableSizeHistory;
	}


	public void setReplicateTableSizeHistory(boolean replicateTableSizeHistory) {
		this.replicateTableSizeHistory = replicateTableSizeHistory;
	}


	public ExternalServiceLocator getExternalServiceLocator() {
		return this.externalServiceLocator;
	}


	public void setExternalServiceLocator(ExternalServiceLocator externalServiceLocator) {
		this.externalServiceLocator = externalServiceLocator;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
