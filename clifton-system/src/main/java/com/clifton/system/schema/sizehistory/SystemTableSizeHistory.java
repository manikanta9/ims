package com.clifton.system.schema.sizehistory;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.system.schema.SystemTable;

import java.util.Date;


/**
 * A DTO Object to track table size characteristics from various data sources.
 *
 * @author David Irizarry
 */
public class SystemTableSizeHistory extends BaseSimpleEntity<Integer> {

	private SystemTable systemTable;
	private Date snapshotDate;

	private long numberOfRows;
	private long dataSizeInBytes;
	private long indexSizeInBytes;
	private long unusedSizeInBytes;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a clone of this history object (except for id and record stamp fields).
	 * Uses the specified SystemTable for the table. Can be used to replicate history to a different system.
	 */
	public SystemTableSizeHistory cloneForTable(SystemTable systemTable) {
		SystemTableSizeHistory clone = new SystemTableSizeHistory();
		clone.setSystemTable(systemTable);
		clone.setSnapshotDate(getSnapshotDate());
		clone.populateNumericMetricsFrom(this);
		return clone;
	}


	/**
	 * Copies all numeric measurement properties from the specified object to this object.
	 */
	public void populateNumericMetricsFrom(SystemTableSizeHistory source) {
		setNumberOfRows(source.getNumberOfRows());
		setDataSizeInBytes(source.getDataSizeInBytes());
		setIndexSizeInBytes(source.getIndexSizeInBytes());
		setUnusedSizeInBytes(source.getUnusedSizeInBytes());
	}


	/**
	 * Returns true of all numeric measurement properties of the specified object are equal to these of this object.
	 */
	public boolean isSameNumericMetrics(SystemTableSizeHistory sizeHistory) {
		if (getNumberOfRows() != sizeHistory.getNumberOfRows()) {
			return false;
		}
		if (getDataSizeInBytes() != sizeHistory.getDataSizeInBytes()) {
			return false;
		}
		if (getIndexSizeInBytes() != sizeHistory.getIndexSizeInBytes()) {
			return false;
		}
		if (getUnusedSizeInBytes() != sizeHistory.getUnusedSizeInBytes()) {
			return false;
		}
		return true;
	}


	public long getTotalSizeInBytes() {
		return this.getDataSizeInBytes() + this.getIndexSizeInBytes() + this.getUnusedSizeInBytes();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date date) {
		this.snapshotDate = date;
	}


	public long getNumberOfRows() {
		return this.numberOfRows;
	}


	public void setNumberOfRows(long totalRows) {
		this.numberOfRows = totalRows;
	}


	public long getDataSizeInBytes() {
		return this.dataSizeInBytes;
	}


	public void setDataSizeInBytes(long dataSizeInBytes) {
		this.dataSizeInBytes = dataSizeInBytes;
	}


	public long getIndexSizeInBytes() {
		return this.indexSizeInBytes;
	}


	public void setIndexSizeInBytes(long indexSizeInBytes) {
		this.indexSizeInBytes = indexSizeInBytes;
	}


	public long getUnusedSizeInBytes() {
		return this.unusedSizeInBytes;
	}


	public void setUnusedSizeInBytes(long unusedSizeInBytes) {
		this.unusedSizeInBytes = unusedSizeInBytes;
	}
}
