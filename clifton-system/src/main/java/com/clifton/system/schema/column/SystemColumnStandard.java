package com.clifton.system.schema.column;


import javax.persistence.Table;


/**
 * The <code>SystemColumnStandard</code> class represents a database table column.
 *
 * @author vgomelsky
 */
@Table(name = "SystemColumn")
public class SystemColumnStandard extends SystemColumn {

	private String beanPropertyName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isCustomColumn() {
		return false;
	}


	@Override
	public String getCustomLabel() {
		return getLabel();
	}


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}


	public void setBeanPropertyName(String beanPropertyName) {
		this.beanPropertyName = beanPropertyName;
	}
}
