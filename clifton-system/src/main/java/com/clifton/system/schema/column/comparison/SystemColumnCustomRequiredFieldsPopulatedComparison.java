package com.clifton.system.schema.column.comparison;


import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.util.List;


/**
 * The <code>SystemColumnCustomRequiredFieldsPopulatedComparison</code> class is a {@link Comparison} that evaluates to true
 * if all required custom columns on the bean have been populated
 * <p>
 * Note: This is only currently implemented for standard system column value aware entities.
 *
 * @author manderson
 */
public class SystemColumnCustomRequiredFieldsPopulatedComparison<T extends SystemColumnCustomValueAware> implements Comparison<T> {

	private SystemColumnValueHandler systemColumnValueHandler;

	/**
	 * Column group to verify required fields have been populated for
	 * for example: Contracts verify "Contract Clauses" group before being published
	 */
	private String columnGroupName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(T bean, ComparisonContext context) {
		ValidationUtils.assertTrue(SystemColumnCustomValueAware.class.isAssignableFrom(bean.getClass()), "Invalid condition for bean class [" + bean.getClass().getName()
				+ "] as it doesn't implement SystemColumnCustomAware interface.");

		// Set group name on the entity so we know which to look for - keep original so we don't clear what was there
		String originalGroupName = bean.getColumnGroupName();
		bean.setColumnGroupName(getColumnGroupName());

		List<String> missingValueColumns = getSystemColumnValueHandler().validateRequiredSystemColumnCustomValuesForEntity(bean);
		// Change column group name back to what it was
		bean.setColumnGroupName(originalGroupName);
		boolean result = CollectionUtils.isEmpty(missingValueColumns);
		if (result) {
			context.recordTrueMessage("There are no missing required custom column values.");
		}
		else {
			context.recordFalseMessage("The following required custom columns are missing values: " + StringUtils.collectionToCommaDelimitedString(missingValueColumns));
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
