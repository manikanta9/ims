package com.clifton.system.schema.column.cache;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.StringUtils;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemColumnGroupColumnCache</code> ...
 *
 * @author manderson
 */
@Component
public class SystemColumnGroupColumnConfigCacheImpl extends SelfRegisteringDaoObserver<SystemColumn> implements CustomCache<String, SystemColumnGroupColumnConfig>, SystemColumnGroupColumnConfigCache {

	private CacheHandler<String, SystemColumnGroupColumnConfig> cacheHandler;


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public SystemColumnGroupColumnConfig getSystemColumnGroupColumnConfig(String columnGroupName) {
		return getCacheHandler().get(getCacheName(), columnGroupName);
	}


	@Override
	public void setSystemColumnGroupColumnConfig(SystemColumnGroupColumnConfig config) {
		if (config != null) {
			getCacheHandler().put(getCacheName(), config.getGroupName(), config);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<SystemColumn> dao, DaoEventTypes event, SystemColumn bean, Throwable e) {
		if (e == null && bean instanceof SystemColumnCustom) {
			String key = ((SystemColumnCustom) bean).getColumnGroup().getName();
			if (event.isUpdate()) {
				SystemColumn originalBean = getOriginalBean(dao, bean);
				String originalKey = ((SystemColumnCustom) originalBean).getColumnGroup().getName();
				if (!StringUtils.isEqual(key, originalKey)) {
					if (!StringUtils.isEmpty(originalKey)) {
						getCacheHandler().remove(getCacheName(), originalKey);
					}
				}
			}
			if (!StringUtils.isEmpty(key)) {
				getCacheHandler().remove(getCacheName(), key);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////                Getter and Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<String, SystemColumnGroupColumnConfig> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, SystemColumnGroupColumnConfig> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
