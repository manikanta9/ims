package com.clifton.system.schema.validation;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.system.schema.SystemTable;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemTableValidatorRegisteringObserver</code> class is a {@link DaoEventObserver} that registers/unregisters
 * the {@link SystemTableValidationValidator} observers to DAOs based upon
 * changes to {@link SystemTable} Entity Modify Conditions
 *
 * @author manderson
 */
@Component
public class SystemTableValidatorRegisteringObserver extends BaseDaoEventObserver<SystemTable> {

	private SystemTableValidationRegistrator systemTableValidationRegistrator;


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<SystemTable> dao, DaoEventTypes event, SystemTable bean) {
		// Get the Original Bean, so it's set for after call
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<SystemTable> dao, DaoEventTypes event, SystemTable bean, @SuppressWarnings("unused") Throwable e) {
		// If any changes to EntityModifyConditions - Re-register DAO SystemTableValidationValidator for that table
		try {
			// Register Observer for Inserts
			if (event.isInsert()) {
				if (isEntityModifiedConditionDefined(bean)) {
					getSystemTableValidationRegistrator().registerObservers(bean);
				}
			}
			// Unregister Observers for Deletes
			else if (event.isDelete()) {
				if (isEntityModifiedConditionDefined(bean)) {
					getSystemTableValidationRegistrator().unregisterObservers(bean);
				}
			}
			else if (event.isUpdate()) {
				// Updates
				SystemTable original = getOriginalBean(dao, bean);

				// Check if any conditions changed
				if (!CompareUtils.isEqual(original.getInsertEntityCondition(), bean.getInsertEntityCondition())
						|| !CompareUtils.isEqual(original.getUpdateEntityCondition(), bean.getUpdateEntityCondition())
						|| !CompareUtils.isEqual(original.getDeleteEntityCondition(), bean.getDeleteEntityCondition())) {

					// Unregister Original Observers 
					getSystemTableValidationRegistrator().unregisterObservers(bean);

					// Register New Observers
					getSystemTableValidationRegistrator().registerObservers(bean);
				}
			}
		}
		catch (NoSuchBeanDefinitionException e2) {
			if (!bean.getDataSource().isDefaultDataSource()) {
				// Do Nothing - No DAO for non default datasource is OK 
				return;
			}
			throw (e2);
		}
	}


	private boolean isEntityModifiedConditionDefined(SystemTable table) {
		return table.getInsertEntityCondition() != null || table.getUpdateEntityCondition() != null || table.getDeleteEntityCondition() != null;
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public SystemTableValidationRegistrator getSystemTableValidationRegistrator() {
		return this.systemTableValidationRegistrator;
	}


	public void setSystemTableValidationRegistrator(SystemTableValidationRegistrator systemTableValidationRegistrator) {
		this.systemTableValidationRegistrator = systemTableValidationRegistrator;
	}
}
