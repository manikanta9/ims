package com.clifton.system.schema.column.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.column.SystemColumnGroup;
import org.springframework.stereotype.Component;


@Component
public class SystemColumnGroupValidator extends SelfRegisteringDaoValidator<SystemColumnGroup> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemColumnGroup bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert()) {
			throw new ValidationException("System Column Groups cannot be added via UI");
			// Note: Since these are inserted via data migration, should add a notification to check specific properties for Json Value Custom Column Types:
			// 1. Json Value Column MUST be a Standard Column
			// 2. Json Value Column Bean Property Class should be CustomJsonString class
			// 3. Json Value Column is UNIQUE per Column Groups
		}
		if (config.isDelete()) {
			throw new ValidationException("System Column Groups cannot be deleted via UI");
		}

		// NOTE: THE ONLY FIELD THAT CAN BE UPDATED IS THE SECURITY RESOURCE, SO USE THE ORIGINAL BEAN AND COPY
		// THE SECURITY RESOURCE SELECTED.  IF THE SECURITY RESOURCE IS THE SAME AS THE TABLE SECURITY - CLEAR THE VALUE
		SystemColumnGroup originalBean = getOriginalBean(bean);
		ValidationUtils.assertTrue(StringUtils.isEqual(originalBean.getName(), bean.getName()), "Column group names cannot be changed");
		ValidationUtils.assertTrue(StringUtils.isEqual(originalBean.getLinkedBeanProperty(), bean.getLinkedBeanProperty()), "Column linked bean properties cannot be changed");
		ValidationUtils.assertTrue(originalBean.getTable().equals(bean.getTable()), "Column group tables cannot be changed.");
		ValidationUtils.assertTrue(originalBean.getCustomColumnType() == bean.getCustomColumnType(), "Column column type cannot be changed.");

		if (originalBean.getJsonValueColumn() != null) {
			ValidationUtils.assertTrue(originalBean.getJsonValueColumn().equals(bean.getJsonValueColumn()), "Json Value Column cannot be changed.");
		}

		SecurityResource res = bean.getSecurityResource();
		if (res != null && originalBean.getTable().getSecurityResource() != null && res.equals(originalBean.getTable().getSecurityResource())) {
			bean.setSecurityResource(null);
		}
	}
}
