package com.clifton.system.schema;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.audit.SystemAuditEvent;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.schema.search.SystemDataSourceSearchForm;
import com.clifton.system.schema.search.SystemRelationshipSearchForm;
import com.clifton.system.schema.search.SystemTableSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemSchemaServiceImpl</code> class provides basic implementation for <code>SystemSchemaService</code>.
 *
 * @author vgomelsky
 */
@Service
public class SystemSchemaServiceImpl implements SystemSchemaService {

	public static final String CACHE_DEFAULT_DATASOURCE = "SYSTEM_DATASOURCE_DEFAULT";

	private ReadOnlyDAO<SystemDataType> systemDataTypeDAO;
	private AdvancedReadOnlyDAO<SystemDataSource, Criteria> systemDataSourceDAO;
	private AdvancedUpdatableDAO<SystemTable, Criteria> systemTableDAO;
	private AdvancedUpdatableDAO<SystemRelationship, Criteria> systemRelationshipDAO;

	private SystemColumnService systemColumnService;

	private DaoSingleKeyCache<SystemTable, String> systemTableByNameCache;
	private DaoNamedEntityCache<SystemDataSource> systemDataSourceCache;
	private DaoSingleKeyListCache<SystemDataSource, Boolean> systemDataSourceListByDefaultCache;
	private DaoNamedEntityCache<SystemDataType> systemDataTypeCache;
	private CacheHandler<Object, SystemDataSource> cacheHandler;

	////////////////////////////////////////////////////////////////////////////
	////////          SystemDataType Business Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemDataType getSystemDataType(short id) {
		return getSystemDataTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemDataType getSystemDataTypeByName(String name) {
		return getSystemDataTypeCache().getBeanForKeyValueStrict(getSystemDataTypeDAO(), name);
	}


	@Override
	public List<SystemDataType> getSystemDataTypeList() {
		return getSystemDataTypeDAO().findAll();
	}

	////////////////////////////////////////////////////////////////////////////
	////////         SystemDataSource Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemDataSource getSystemDataSource(short id) {
		return getSystemDataSourceDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemDataSource getSystemDataSourceDefault() {
		SystemDataSource df = getSystemDataSourceDefaultFromCache();
		if (df == null) {
			df = getSystemDataSourceDAO().findOneByField("defaultDataSource", true);
			setSystemDataSourceDefaultInCache(df);
		}
		return df;
	}


	@Override
	public SystemDataSource getSystemDataSourceByName(String dataSourceName) {
		return getSystemDataSourceCache().getBeanForKeyValue(getSystemDataSourceDAO(), dataSourceName);
	}


	@Override
	public List<SystemDataSource> getSystemDataSourceNotDefaultList() {
		return getSystemDataSourceListByDefaultCache().getBeanListForKeyValue(getSystemDataSourceDAO(), false);
	}


	@Override
	public List<SystemDataSource> getSystemDataSourceList() {
		return getSystemDataSourceDAO().findAll();
	}


	@Override
	public List<SystemDataSource> getSystemDataSourceList(SystemDataSourceSearchForm searchForm) {
		return getSystemDataSourceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	/////////                  Cache Helper Methods                   //////////
	////////////////////////////////////////////////////////////////////////////


	private SystemDataSource getSystemDataSourceDefaultFromCache() {
		return getCacheHandler().get(CACHE_DEFAULT_DATASOURCE, true);
	}


	private void setSystemDataSourceDefaultInCache(SystemDataSource dataSource) {
		getCacheHandler().put(CACHE_DEFAULT_DATASOURCE, true, dataSource);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            SystemTable Business Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemTable getSystemTable(short id) {
		return getSystemTableDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemTable getSystemTableByName(String tableName) {
		return getSystemTableByNameCache().getBeanForKeyValueStrict(getSystemTableDAO(), tableName);
	}


	@Override
	public SystemTable getSystemTableByNameAndDataSource(final String dataSourceName, final String tableName) {
		if (StringUtils.isEmpty(dataSourceName)) {
			return getSystemTableByName(tableName);
		}
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			criteria.add(Restrictions.eq("name", tableName));
			criteria.createAlias("dataSource", "d").add(Restrictions.eq("d.name", dataSourceName));
		};
		return CollectionUtils.getOnlyElement(getSystemTableDAO().findBySearchCriteria(searchConfigurer));
	}


	@Override
	public List<SystemTable> getSystemTableList(SystemTableSearchForm searchForm) {
		return getSystemTableDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<SystemTable> getSystemTableListWithEntityModifyCondition() {
		HibernateSearchConfigurer searchConfig = criteria -> {
			Criterion insert = Restrictions.isNotNull("insertEntityCondition");
			Criterion update = Restrictions.isNotNull("updateEntityCondition");
			Criterion delete = Restrictions.isNotNull("deleteEntityCondition");

			LogicalExpression or1 = Restrictions.or(insert, update);
			LogicalExpression or2 = Restrictions.or(or1, delete);

			criteria.add(or2);
		};
		return getSystemTableDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public List<SystemTable> getSystemTableListAudited() {
		// 1. get audited tables
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.add(Restrictions.isNotNull("auditType"));
		List<SystemTable> result = getSystemTableDAO().findBySearchCriteria(searchConfigurer);
		if (result == null) {
			result = new ArrayList<>();
		}

		// 2. get audited columns
		SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
		searchForm.setAuditOnly(true);
		searchForm.setStandardOnly(true);
		List<SystemColumnStandard> columnList = getSystemColumnService().getSystemColumnList(searchForm);

		// 3. merge
		for (SystemColumnStandard column : CollectionUtils.getIterable(columnList)) {
			int index = result.indexOf(column.getTable());
			if (index != -1) {
				result.get(index).addColumn(column);
			}
			else {
				SystemTable table = column.getTable();
				table.addColumn(column);
				result.add(table);
			}
		}

		return result;
	}


	@Override
	public SystemTable saveSystemTable(SystemTable table) {
		// Validate table alias
		if (table.getAlias() != null) {
			ValidationUtils.assertFalse(table.getAlias().length() > 3,
					String.format("The table alias exceeds the maximum length (3 characters): [%s]", table.getAlias()), "tableAlias");
			ValidationUtils.assertTrue(table.getAlias().matches("^[a-zA-Z]*$"),
					String.format("The table alias must contain letter characters only: [%s]", table.getAlias()), "tableAlias");
		}
		if (table.getAuditType() != null && table.getName().equals(SystemAuditEvent.class.getSimpleName())) {
			throw new FieldValidationException("Auditing of 'SystemAuditEvent' table is not allowed", "auditType");
		}

		return getSystemTableDAO().save(table);
	}

	////////////////////////////////////////////////////////////////////////////
	////////        SystemRelationship Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemRelationship getSystemRelationship(int id) {
		return getSystemRelationshipDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemRelationship> getSystemRelationshipList(SystemRelationshipSearchForm searchForm) {
		return getSystemRelationshipDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see com.clifton.system.schema.validation.SystemRelationshipDaoValidator
	 */
	@Override
	public SystemRelationship saveSystemRelationship(SystemRelationship relationship) {
		return getSystemRelationshipDAO().save(relationship);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReadOnlyDAO<SystemDataType> getSystemDataTypeDAO() {
		return this.systemDataTypeDAO;
	}


	public void setSystemDataTypeDAO(ReadOnlyDAO<SystemDataType> systemDataTypeDAO) {
		this.systemDataTypeDAO = systemDataTypeDAO;
	}


	public AdvancedReadOnlyDAO<SystemDataSource, Criteria> getSystemDataSourceDAO() {
		return this.systemDataSourceDAO;
	}


	public void setSystemDataSourceDAO(AdvancedReadOnlyDAO<SystemDataSource, Criteria> systemDataSourceDAO) {
		this.systemDataSourceDAO = systemDataSourceDAO;
	}


	public AdvancedUpdatableDAO<SystemTable, Criteria> getSystemTableDAO() {
		return this.systemTableDAO;
	}


	public void setSystemTableDAO(AdvancedUpdatableDAO<SystemTable, Criteria> systemTableDAO) {
		this.systemTableDAO = systemTableDAO;
	}


	public AdvancedUpdatableDAO<SystemRelationship, Criteria> getSystemRelationshipDAO() {
		return this.systemRelationshipDAO;
	}


	public void setSystemRelationshipDAO(AdvancedUpdatableDAO<SystemRelationship, Criteria> systemRelationshipDAO) {
		this.systemRelationshipDAO = systemRelationshipDAO;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public DaoSingleKeyCache<SystemTable, String> getSystemTableByNameCache() {
		return this.systemTableByNameCache;
	}


	public void setSystemTableByNameCache(DaoSingleKeyCache<SystemTable, String> systemTableByNameCache) {
		this.systemTableByNameCache = systemTableByNameCache;
	}


	public DaoNamedEntityCache<SystemDataSource> getSystemDataSourceCache() {
		return this.systemDataSourceCache;
	}


	public void setSystemDataSourceCache(DaoNamedEntityCache<SystemDataSource> systemDataSourceCache) {
		this.systemDataSourceCache = systemDataSourceCache;
	}


	public DaoSingleKeyListCache<SystemDataSource, Boolean> getSystemDataSourceListByDefaultCache() {
		return this.systemDataSourceListByDefaultCache;
	}


	public void setSystemDataSourceListByDefaultCache(DaoSingleKeyListCache<SystemDataSource, Boolean> systemDataSourceListByDefaultCache) {
		this.systemDataSourceListByDefaultCache = systemDataSourceListByDefaultCache;
	}


	public DaoNamedEntityCache<SystemDataType> getSystemDataTypeCache() {
		return this.systemDataTypeCache;
	}


	public void setSystemDataTypeCache(DaoNamedEntityCache<SystemDataType> systemDataTypeCache) {
		this.systemDataTypeCache = systemDataTypeCache;
	}


	public CacheHandler<Object, SystemDataSource> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Object, SystemDataSource> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
