package com.clifton.system.schema.column.json;

import com.clifton.system.schema.column.SystemColumnCustomAware;


/**
 * The <code>SystemColumnCustomJsonAware</code> is used to define classes that utilize JSON custom column values.  Each column group is stored in a separate value on the source table
 * <p>
 * <p>
 * This interface is used to register observer that validates/converts/saves the values correctly as json
 *
 * @author manderson
 */
public interface SystemColumnCustomJsonAware extends SystemColumnCustomAware {


	// NOTHING HERE USES JUST COLUMN GROUP NAME FROM {@link SystemColumnCustomAware}
}
