package com.clifton.system.schema.column.search;

import java.util.Date;


/**
 * The <code>SystemColumnDatedValueAwareSearchForm</code> interface can be implemented by search forms that need to be able to search for a {@link com.clifton.system.schema.column.value.SystemColumnDatedValue} that exists with a given value
 * <p>
 * See: {@link SystemColumnDatedValueAwareSearchFormUtils} for adding the search criteria
 *
 * @author manderson
 */
public interface SystemColumnDatedValueAwareSearchForm {


	/**
	 * Returns the table name the dated value column is associated with -i.e. InvestmentAccount
	 */
	public String getSystemColumnDatedValueTableName();


	/**
	 * Returns the dated value column name searching for - i.e. Risk Designation
	 */
	public String getSystemColumnDatedValueColumnName();


	/**
	 * Returns the value for the dated column value you are searching for
	 */
	public String getSystemColumnDatedValue();


	/**
	 * If true, does a value = search
	 * else, does a like search on value or text
	 */
	public Boolean getDatedValueEquals();


	/**
	 * If true, returns only active dated values (defaults to today, but can use getDatedValueActiveOnDate for a different date)
	 * Note: This filter is not applied if false or null.  Can't think of a reason to find only inactive values?
	 */
	public Boolean getDatedValueActive();


	/**
	 * Optional Used with getDatedValueActive to specify active on date if not using default of today
	 */
	public Date getDatedValueActiveOnDate();
}
