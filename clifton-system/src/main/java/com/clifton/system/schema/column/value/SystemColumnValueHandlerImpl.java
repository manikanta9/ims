package com.clifton.system.schema.column.value;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ImmutableObjectWrapper;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.audit.auditor.SystemAuditDaoUtils;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemDataTypeConverter;
import com.clifton.system.schema.column.CustomColumnTypes;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomAware;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.SystemColumnGroup;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.cache.SystemColumnGroupColumnConfig;
import com.clifton.system.schema.column.cache.SystemColumnValueCache;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;


@Component
public class SystemColumnValueHandlerImpl implements SystemColumnValueHandler {

	private static final ImmutableObjectWrapper<SystemColumnValueHolder> NULL_ENTITY = new ImmutableObjectWrapper<>(null);

	private AdvancedUpdatableDAO<SystemColumnValue, Criteria> systemColumnValueDAO;

	private SystemColumnValueCache systemColumnValueCache;
	private SystemColumnService systemColumnService;

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////              System Column Value Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <E extends SystemColumnCustomValueAware> List<SystemColumnValue> getSystemColumnValueListForEntity(E entity) {
		if (!StringUtils.isEmpty(entity.getColumnGroupName())) {
			SystemColumnGroup columnGroup = getSystemColumnService().getSystemColumnGroupByName(entity.getColumnGroupName());
			return getSystemColumnValueListImpl(entity, columnGroup);
		}
		return null;
	}


	@Override
	public <E extends SystemColumnCustomValueAware> void saveSystemColumnValueListForEntity(E entity) {
		if (!StringUtils.isEmpty(entity.getColumnGroupName())) {
			// only applies to regular system custom columns
			SystemColumnGroup columnGroup = getSystemColumnService().getSystemColumnGroupByName(entity.getColumnGroupName());
			if (columnGroup.getCustomColumnType() != CustomColumnTypes.COLUMN_VALUE) {
				return;
			}
			// validate: all required Columns are set
			List<SystemColumnValue> valueList = entity.getColumnValueList();
			List<SystemColumnValue> newValueList = new ArrayList<>();

			// Remove anything from the list whose value is the same as the global default
			for (SystemColumnValue v : CollectionUtils.getIterable(valueList)) {
				if (!StringUtils.isEmpty(v.getColumn().getGlobalDefaultValue())) {
					if (v.getColumn().getGlobalDefaultValue().equals(v.getValue())) {
						continue;
					}
				}
				newValueList.add(v);
			}

			List<String> missingRequiredColumns = validateRequiredSystemColumnCustomValuesForEntityImpl(entity, newValueList);
			if (!CollectionUtils.isEmpty(missingRequiredColumns)) {
				throw new ValidationException("Missing values for required column(s) [" + StringUtils.collectionToCommaDelimitedString(missingRequiredColumns) + "].");
			}

			// validate value data types
			for (SystemColumnValue v : CollectionUtils.getIterable(newValueList)) {
				SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(v.getColumn().getDataType());
				try {
					Object value = dataTypeConverter.convert(v.getValue());
					v.setValue(new ObjectToStringConverter().convert(value));
				}
				catch (Throwable e) {
					throw new ValidationException("Cannot convert value '" + v.getValue() + "' to data type '" + v.getColumn().getDataType().getName() + "' for column '" + v.getColumn().getName());
				}
			}

			// update the old list with the new list
			List<SystemColumnValue> oldValueList = getSystemColumnValueListForEntity(entity);
			for (SystemColumnValue value : CollectionUtils.getIterable(newValueList)) {
				for (SystemColumnValue oldValue : CollectionUtils.getIterable(oldValueList)) {
					if (value.getColumn().equals(oldValue.getColumn())) {
						value.setId(oldValue.getId());
						value.setRv(oldValue.getRv());
					}
				}
				if (entity.getIdentity() instanceof Short) {
					value.setFkFieldId(((Short) entity.getIdentity()).intValue());
				}
				else {
					value.setFkFieldId((Integer) entity.getIdentity());
				}
			}
			getSystemColumnValueDAO().saveList(newValueList, oldValueList);
		}
	}


	@Override
	public <E extends SystemColumnCustomValueAware> List<String> validateRequiredSystemColumnCustomValuesForEntity(E entity) {
		return validateRequiredSystemColumnCustomValuesForEntityImpl(entity, getSystemColumnValueListForEntity(entity));
	}


	/**
	 * Returns a list of required columns that are missing values for the given entity. Value list is passed so can validate against a new list
	 * of values just before saving.
	 * <p>
	 * Properly verifies dependent fields are populated before enforcing a field being required
	 * Also, for those with global defaults doesn't enforce requirement as it just uses that value if not populated
	 */
	private <E extends SystemColumnCustomValueAware> List<String> validateRequiredSystemColumnCustomValuesForEntityImpl(E entity, List<SystemColumnValue> valueList) {
		List<String> missing = new ArrayList<>();
		List<SystemColumnCustom> columnList = getSystemColumnService().getSystemColumnCustomListForEntity(entity);
		for (SystemColumnCustom column : CollectionUtils.getIterable(columnList)) {
			// If there is a global default, we aren't saving each instance, so it just uses that value
			if (column.isRequired() && StringUtils.isEmpty(column.getGlobalDefaultValue())) {
				if (column.getDependedColumn() != null) {
					// Only required if depended system Column was populated, otherwise, can't enter values
					SystemColumnValue dependedValue = null;
					SystemColumnCustom dependedColumn = column.getDependedColumn();
					for (SystemColumnValue dv : CollectionUtils.getIterable(valueList)) {
						ValidationUtils.assertNotNull(dv.getColumn(), "Column type is not specified for value: " + dv);
						if (dv.getColumn().equals(dependedColumn)) {
							dependedValue = dv;
							break;
						}
					}
					if (dependedValue == null || StringUtils.isEmpty(dependedValue.getValue()) ||
							(SystemDataType.BOOLEAN.equals(dependedColumn.getDataType().getName()) && "false".equals(dependedValue.getValue()))) {
						continue;
					}
				}

				SystemColumnValue value = null;
				for (SystemColumnValue v : CollectionUtils.getIterable(valueList)) {
					ValidationUtils.assertNotNull(v.getColumn(), "Column type is not specified for value: " + v);
					if (v.getColumn().equals(column)) {
						value = v;
						break;
					}
				}
				if (value == null || StringUtils.isEmpty(value.getValue())) {
					missing.add(column.getName());
				}
			}
		}
		return missing;
	}


	@Override
	public <E extends SystemColumnCustomValueAware> List<SystemColumnValue> getSystemColumnValueListImpl(E entity, SystemColumnGroup columnGroup) {
		ValidationUtils.assertNotNull(entity, "Cannot retrieve custom Column values for null entity.");

		SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
		searchForm.setFkFieldId(BeanUtils.getIdentityAsInteger(entity));

		// If not getting all columnGroup is required (Otherwise looks up by table name across groups)
		if (columnGroup == null) {
			searchForm.setTableName(getTableNameForEntity(entity));
		}
		else {
			searchForm.setColumnGroupId(columnGroup.getId());
		}
		return getSystemColumnValueList(searchForm);
	}


	public List<SystemColumnValue> getSystemColumnValueList(SystemColumnValueSearchForm searchForm) {
		// try performance optimization by avoiding an extra join
		if (searchForm.getColumnGroupName() != null) {
			if (searchForm.getColumnGroupId() == null) {
				searchForm.setColumnGroupId(getSystemColumnService().getSystemColumnGroupByName(searchForm.getColumnGroupName()).getId());
				searchForm.setColumnGroupName(null);
			}
		}
		return getSystemColumnValueDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public <E extends SystemColumnCustomValueAware> void copySystemColumnValueList(E fromEntity, E toEntity, boolean deleteExisting) {
		// Clear existing column values
		if (deleteExisting) {
			deleteSystemColumnValueListForEntity(toEntity);
		}

		List<SystemColumnValue> valueList = getSystemColumnValueListForEntity(fromEntity);
		List<SystemColumnValue> newValueList = new ArrayList<>();
		for (SystemColumnValue v : CollectionUtils.getIterable(valueList)) {
			SystemColumnValue nv = BeanUtils.cloneBean(v, false, false);
			nv.setFkFieldId(((Number) toEntity.getIdentity()).intValue());
			newValueList.add(nv);
		}

		getSystemColumnValueDAO().saveList(newValueList);
	}


	@Override
	@Transactional
	public void moveSystemColumnValueListForEntityList(String columnGroupName, List<Integer> entityIdList, String fromLinkedValue, String toLinkedValue, boolean bypassCustomColumnValidation) {
		Map<String, SystemColumnCustom> columnCustomMap = getSystemColumnService().validateSystemColumnCustomListSameForLinkedValues(columnGroupName, fromLinkedValue, toLinkedValue, null, bypassCustomColumnValidation);

		SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
		searchForm.setColumnGroupName(columnGroupName);
		searchForm.setLinkedValue(fromLinkedValue);
		if (entityIdList.size() == 1) {
			searchForm.setFkFieldId(entityIdList.get(0));
		}
		else {
			searchForm.setFkFieldIds(entityIdList.toArray(new Integer[0]));
		}
		List<SystemColumnValue> valueList = getSystemColumnValueList(searchForm);

		if (!CollectionUtils.isEmpty(valueList)) {
			for (SystemColumnValue value : CollectionUtils.getIterable(valueList)) {
				if (columnCustomMap.containsKey(value.getColumn().getName())) {
					value.setColumn(columnCustomMap.get(value.getColumn().getName()));
					updateSystemColumnValueColumn(value);
				}
				else {
					getSystemColumnValueDAO().delete(value.getId());
				}
			}
		}
	}


	private void updateSystemColumnValueColumn(final SystemColumnValue value) {
		final UpdatableDAO<SystemColumnValue> dao = this.systemColumnValueDAO;
		SystemAuditDaoUtils.executeWithAuditingDisabled(new String[]{"column"}, () -> dao.save(value));
	}


	@Override
	public <E extends SystemColumnCustomValueAware> void deleteSystemColumnValueListForEntity(E entity) {
		getSystemColumnValueDAO().deleteList(getSystemColumnValueListImpl(entity, null));
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public <E extends SystemColumnCustomValueAware> void loadSystemColumnValueCacheForEntityList(List<E> entityList, String columnGroupName) {
		String tableName = null;
		SystemColumnGroup group = getSystemColumnService().getSystemColumnGroupByName(columnGroupName);
		if (group.getCustomColumnType() == CustomColumnTypes.COLUMN_VALUE) {
			String linkedPropertyName = group.getLinkedBeanProperty();
			boolean useLinkedProperty = !StringUtils.isEmpty(linkedPropertyName);
			Map<Object, List<SystemColumnCustom>> columnMap = new HashMap<>();
			for (E entity : CollectionUtils.getIterable(entityList)) {
				entity.setColumnGroupName(columnGroupName);
				if (tableName == null) {
					tableName = getTableNameForEntity(entity);
				}
				Object value = null;
				if (useLinkedProperty) {
					value = BeanUtils.getPropertyValue(entity, linkedPropertyName);
				}
				if (value == null) {
					value = "NULL";
				}
				List<SystemColumnCustom> columnList;
				if (columnMap.containsKey(value)) {
					columnList = columnMap.get(value);
				}
				else {
					columnList = getSystemColumnService().getSystemColumnCustomListForEntity(entity);
					columnMap.put(value, columnList);
				}
				loadSystemColumnValueCacheForEntityImpl(entity, tableName, columnGroupName, columnList);
			}
		}
	}


	private <C extends SystemColumnCustomAware> void loadSystemColumnValueCacheForEntity(C entity, String columnGroupName) {
		String tableName = getTableNameForEntity(entity);
		entity.setColumnGroupName(columnGroupName);
		List<SystemColumnCustom> columnList = getSystemColumnService().getSystemColumnCustomListForEntity(entity);
		SystemColumnGroup columnGroup = getSystemColumnService().getSystemColumnGroupByName(columnGroupName);
		if (columnGroup.getCustomColumnType() == CustomColumnTypes.COLUMN_JSON_VALUE) {
			// Only Use the Cache for the Global Defaults
			if (!CollectionUtils.isEmpty(columnList)) {
				for (SystemColumnCustom column : CollectionUtils.getIterable(columnList)) {
					SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(column.getDataType());

					// Check if global default has been cached - if not - cache it
					if (getSystemColumnValueCache().getSystemColumnValueGlobalDefault(tableName, columnGroupName, column.getName(), column.getLinkedValue()) == null) {
						Object globalResult = dataTypeConverter.convert(column.getGlobalDefaultValue());
						ObjectWrapper<SystemColumnValueHolder> wrapper = new ObjectWrapper<>(new SystemColumnValueHolder(globalResult, column.getGlobalDefaultText()));
						getSystemColumnValueCache().setSystemColumnValueGlobalDefault(tableName, columnGroupName, column.getName(), column.getLinkedValue(), wrapper);
					}
				}
			}
		}
		else {
			if (entity instanceof SystemColumnCustomValueAware) {
				loadSystemColumnValueCacheForEntityImpl((SystemColumnCustomValueAware) entity, tableName, columnGroupName, columnList);
			}
		}
	}


	private <E extends SystemColumnCustomValueAware> void loadSystemColumnValueCacheForEntityImpl(E entity, String tableName, String columnGroupName, List<SystemColumnCustom> columnList) {
		if (!CollectionUtils.isEmpty(columnList)) {
			List<SystemColumnValue> valueList = getSystemColumnValueListForEntity(entity);

			for (SystemColumnCustom column : CollectionUtils.getIterable(columnList)) {
				List<SystemColumnValue> columnValueList = BeanUtils.filter(valueList, value -> value.getColumn().getName(), column.getName());
				if (CollectionUtils.getSize(columnValueList) > 1) {
					throw new IllegalStateException("Cannot have more than 1 value for column '" + column.getLabel() + "' from column group '" + columnGroupName + "' for entity with id = "
							+ entity.getIdentity());
				}

				SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(column.getDataType());

				// If no value, set in cache to null
				if (CollectionUtils.isEmpty(columnValueList)) {
					getSystemColumnValueCache().setSystemColumnValueForEntity(tableName, entity.getIdentity(), columnGroupName, column.getName(), NULL_ENTITY);
				}
				else if (CollectionUtils.getSize(columnValueList) == 1) {
					SystemColumnValue value = columnValueList.get(0);
					Object result = dataTypeConverter.convert(value.getValue());
					ObjectWrapper<SystemColumnValueHolder> wrapper = new ObjectWrapper<>(new SystemColumnValueHolder(result, value.getText()));
					getSystemColumnValueCache().setSystemColumnValueForEntity(tableName, entity.getIdentity(), columnGroupName, column.getName(), wrapper);
				}

				// Check if global default has been cached - if not - cache it
				if (getSystemColumnValueCache().getSystemColumnValueGlobalDefault(tableName, columnGroupName, column.getName(), column.getLinkedValue()) == null) {
					Object globalResult = dataTypeConverter.convert(column.getGlobalDefaultValue());
					ObjectWrapper<SystemColumnValueHolder> wrapper = new ObjectWrapper<>(new SystemColumnValueHolder(globalResult, column.getGlobalDefaultText()));
					getSystemColumnValueCache().setSystemColumnValueGlobalDefault(tableName, columnGroupName, column.getName(), column.getLinkedValue(), wrapper);
				}
			}
		}
	}


	@Override
	public <C extends SystemColumnCustomAware> Object getSystemColumnValueForEntity(C entity, String columnGroupName, String columnName, boolean returnGlobalDefaultIfNull) {
		SystemColumnValueHolder resultHolder = getSystemColumnValueHolderForEntity(entity, columnGroupName, columnName, returnGlobalDefaultIfNull);
		if (resultHolder != null) {
			return resultHolder.getValue();
		}
		return null;
	}


	@Override
	public <C extends SystemColumnCustomAware> Object getSystemColumnValueForEntityWithLinkedValue(C entity, String columnGroupName, String columnName, String linkedValue, boolean returnGlobalDefaultIfNull) {
		SystemColumnValueHolder resultHolder = getSystemColumnValueHolderForEntityWithLinkedValue(entity, columnGroupName, columnName, linkedValue, returnGlobalDefaultIfNull);
		if (resultHolder != null) {
			return resultHolder.getValue();
		}
		return null;
	}


	@Override
	public <C extends SystemColumnCustomAware> SystemColumnValueHolder getSystemColumnValueHolderForEntity(C entity, String columnGroupName, String columnName, boolean returnGlobalDefaultIfNull) {
		return getSystemColumnValueHolderForEntityWithLinkedValue(entity, columnGroupName, columnName, null, returnGlobalDefaultIfNull);
	}


	private <C extends SystemColumnCustomAware> SystemColumnValueHolder getSystemColumnValueHolderForEntityWithLinkedValue(C entity, String columnGroupName, String columnName, String linkedValue, boolean returnGlobalDefaultIfNull) {
		SystemColumnGroupColumnConfig config = getSystemColumnService().getSystemColumnGroupColumnConfig(columnGroupName);
		if (!config.isColumnNameApplyToLinkedValue(columnName, entity)) {
			return null;
		}
		String tableName = getTableNameForEntity(entity);
		SystemColumnValueHolder result = null;
		if (config.getCustomColumnType() == CustomColumnTypes.COLUMN_JSON_VALUE) {
			CustomJsonString customJsonString = (CustomJsonString) BeanUtils.getPropertyValue(entity, config.getJsonValueBeanProperty());
			if (customJsonString != null) {
				Object value = CustomJsonStringUtils.getColumnValue(customJsonString, columnName);

				if (value instanceof String) {
					SystemColumnCustom customColumn = CollectionUtils.getFirstElement(BeanUtils.filter(getSystemColumnService().getSystemColumnCustomListForEntityAndColumnGroup(entity, getSystemColumnService().getSystemColumnGroupByName(columnGroupName), config.getCustomColumnType()), SystemColumnCustom::getName, columnName));
					if (customColumn != null) {
						SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(customColumn.getDataType());
						value = dataTypeConverter.convert((String) value);
					}
				}

				String text = (new ObjectToStringConverter()).convert(customJsonString.getColumnText(columnName));
				result = new SystemColumnValueHolder(value, text);
			}
		}
		else {
			// lookup in cache first: use wrapper to support null caching
			result = getSystemColumnValueFromCache(entity, columnGroupName, () -> getSystemColumnValueCache().getSystemColumnValueForEntity(tableName, entity.getIdentity(), columnGroupName, columnName));
		}

		if (result == null && returnGlobalDefaultIfNull) {
			String linkedValueString = ObjectUtils.coalesce(linkedValue, config.getEntityLinkedValue(entity));
			// If result contains null object, and returnGlobalDefaultIfNull = true - pull Global Default
			result = getSystemColumnValueFromCache(entity, columnGroupName, () -> getSystemColumnValueCache().getSystemColumnValueGlobalDefault(tableName, columnGroupName, columnName, linkedValueString));
		}
		return result;
	}


	private <C extends SystemColumnCustomAware> SystemColumnValueHolder getSystemColumnValueFromCache(C entity, String columnGroupName, Supplier<ObjectWrapper<SystemColumnValueHolder>> functionLookupFromCache) {
		ObjectWrapper<SystemColumnValueHolder> wrappedResult = functionLookupFromCache.get();
		if (wrappedResult == null) {
			loadSystemColumnValueCacheForEntity(entity, columnGroupName);
			// Look it up again after resetting the cache
			wrappedResult = functionLookupFromCache.get();
		}
		return wrappedResult.getObject();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	private String getTableNameForEntity(IdentityObject entity) {
		return getDaoLocator().locate((Class<IdentityObject>) entity.getClass()).getConfiguration().getTableName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemColumnValue, Criteria> getSystemColumnValueDAO() {
		return this.systemColumnValueDAO;
	}


	public void setSystemColumnValueDAO(AdvancedUpdatableDAO<SystemColumnValue, Criteria> systemColumnValueDAO) {
		this.systemColumnValueDAO = systemColumnValueDAO;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemColumnValueCache getSystemColumnValueCache() {
		return this.systemColumnValueCache;
	}


	public void setSystemColumnValueCache(SystemColumnValueCache systemColumnValueCache) {
		this.systemColumnValueCache = systemColumnValueCache;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}
}
