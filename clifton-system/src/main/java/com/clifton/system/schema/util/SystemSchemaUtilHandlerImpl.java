package com.clifton.system.schema.util;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.search.SystemTableSearchForm;
import org.springframework.stereotype.Component;


/**
 * Implementor for {@link SystemSchemaUtilHandler}.
 */
@Component
public class SystemSchemaUtilHandlerImpl implements SystemSchemaUtilHandler {

	private SystemSchemaService systemSchemaService;
	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends IdentityObject> T getEntityFromUniqueId(String uniqueId) {
		String[] components = getEntityUniqueIdComponents(uniqueId);
		String tableAlias = components[1];
		String entityId = components[2];

		SystemTableSearchForm searchForm = new SystemTableSearchForm();
		searchForm.setAliasEquals(tableAlias);
		SystemTable table = CollectionUtils.getFirstElementStrict(getSystemSchemaService().getSystemTableList(searchForm));

		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(table.getName());
		AssertUtils.assertNotNull(dao, "Unable to retrieve table with alias [%s] for entity unique key [%s].", tableAlias, uniqueId);

		@SuppressWarnings("unchecked")
		T entity = (T) dao.findByPrimaryKey(Long.parseLong(entityId));
		return entity;
	}


	@Override
	public String[] getEntityUniqueIdComponents(String uniqueId) {
		// Split on all letter/number boundaries
		String[] components = uniqueId.split("(?<=[a-zA-Z])(?=\\d)|(?<=\\d)(?=[a-zA-Z])");
		AssertUtils.assertTrue(components.length == 3, () ->
				String.format("An unexpected number of components was found in the entity unique key: [%s]", ArrayUtils.toString(components)));

		// Remap component order
		String[] remappedComponents = new String[3];
		remappedComponents[0] = components[0]; // Data source alias
		remappedComponents[1] = components[2]; // Table alias
		remappedComponents[2] = components[1]; // Entity ID

		return remappedComponents;
	}


	@Override
	public String getEntityUniqueId(SystemTable table, Number entityId) {
		AssertUtils.assertNotNull(table.getDataSource().getAlias(), "The alias for the data source [%s] must not be null.", table.getDataSource().getName());
		AssertUtils.assertNotNull(table.getAlias(), "The alias for the table [%s] must not be null.", table.getName());
		String uniqueKey = table.getDataSource().getAlias() + Long.toString(entityId.longValue()) + table.getAlias();
		AssertUtils.assertTrue(uniqueKey.length() < 16, "The generated unique key is longer than the maximum allowed length. Key: [%s]. Maximum length: [%d].", uniqueKey, ENTITY_UNIQUE_ID_MAXIMUM_LENGTH);
		return uniqueKey;
	}


	@Override
	public String getEntityUniqueId(IdentityObject entity) {
		AssertUtils.assertTrue(entity.getIdentity() instanceof Number, "The entity ID must be of type [%s] to generate a unique key.", Number.class.getName());
		ReadOnlyDAO<IdentityObject> entityDao = getDaoLocator().locate(entity);
		String tableName = entityDao.getConfiguration().getTableName();
		SystemTable table = getSystemSchemaService().getSystemTableByName(tableName);
		return getEntityUniqueId(table, (Number) entity.getIdentity());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
