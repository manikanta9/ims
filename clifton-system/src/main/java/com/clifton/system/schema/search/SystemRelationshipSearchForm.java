package com.clifton.system.schema.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class SystemRelationshipSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField(searchField = "column.id")
	private Integer columnId;

	@SearchField(searchField = "parentColumn.id")
	private Integer parentColumnId;

	@SearchField(searchField = "table.id", searchFieldPath = "column")
	private Short tableId;

	@SearchField(searchField = "table.id", searchFieldPath = "parentColumn")
	private Short parentTableId;

	@SearchField
	private Boolean excludeFromUsedBy;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String label;

	@SearchField
	private String description;

	@SearchField(searchFieldPath = "column.table.dataSource")
	private Boolean defaultDataSource;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getColumnId() {
		return this.columnId;
	}


	public void setColumnId(Integer columnId) {
		this.columnId = columnId;
	}


	public Integer getParentColumnId() {
		return this.parentColumnId;
	}


	public void setParentColumnId(Integer parentColumnId) {
		this.parentColumnId = parentColumnId;
	}


	public Boolean getExcludeFromUsedBy() {
		return this.excludeFromUsedBy;
	}


	public void setExcludeFromUsedBy(Boolean excludeFromUsedBy) {
		this.excludeFromUsedBy = excludeFromUsedBy;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public Short getParentTableId() {
		return this.parentTableId;
	}


	public void setParentTableId(Short parentTableId) {
		this.parentTableId = parentTableId;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public Boolean getDefaultDataSource() {
		return this.defaultDataSource;
	}


	public void setDefaultDataSource(Boolean defaultDataSource) {
		this.defaultDataSource = defaultDataSource;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
