package com.clifton.system.schema.column;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemColumnGroup</code> ...
 *
 * @author Mary Anderson
 */
@CacheByName
public class SystemColumnGroup extends NamedEntity<Short> {

	/**
	 * The table this group is for.
	 * i.e. Security Custom Fields group is tied to InvestmentSecurity table.
	 */
	private SystemTable table;

	/**
	 * Optional security override.  If not set, uses the security defined for the table.
	 */
	private SecurityResource securityResource;

	/**
	 * Used for applying fields for this group to specific entities.
	 * i.e. Security Custom Fields group may have different fields based upon the security's instrument.hierarchy.type.id
	 */
	private String linkedBeanProperty;


	/**
	 * The type of custom column, defines how and where the values are stored
	 */
	private CustomColumnTypes customColumnType;


	/**
	 * Used ONLY when customColumnType = CustomColumnTypes.COLUMN_JSON_VALUE
	 * It represents the standard column that holds the json for the column values in the group for the entity
	 */
	private SystemColumn jsonValueColumn;


	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////


	public String getSecurityResourceLabel() {
		if (getSecurityResource() != null) {
			return getSecurityResource().getLabelExpanded();
		}
		else if (getTable() != null && getTable().getSecurityResource() != null) {
			return getTable().getSecurityResource().getLabelExpanded();
		}
		return null;
	}


	public boolean isSecurityOverridden() {
		return getSecurityResource() != null;
	}


	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public String getLinkedBeanProperty() {
		return this.linkedBeanProperty;
	}


	public void setLinkedBeanProperty(String linkedBeanProperty) {
		this.linkedBeanProperty = linkedBeanProperty;
	}


	public SecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(SecurityResource securityResource) {
		this.securityResource = securityResource;
	}


	public CustomColumnTypes getCustomColumnType() {
		return this.customColumnType;
	}


	public void setCustomColumnType(CustomColumnTypes customColumnType) {
		this.customColumnType = customColumnType;
	}


	public SystemColumn getJsonValueColumn() {
		return this.jsonValueColumn;
	}


	public void setJsonValueColumn(SystemColumn jsonValueColumn) {
		this.jsonValueColumn = jsonValueColumn;
	}
}
