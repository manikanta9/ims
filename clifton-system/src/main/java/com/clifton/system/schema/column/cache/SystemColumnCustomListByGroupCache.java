package com.clifton.system.schema.column.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringDaoListCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnGroup;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;


/**
 * The <code>SystemColumnCustomListByGroupCache</code> caches a list of custom system columns for a {@link SystemColumnGroup} id
 * <p>
 * This cache cannot utilize the {@link SelfRegisteringSingleKeyDaoListCache}
 * because of the subclasses for SystemColumn on the dao - this dao is actually registered for both custom and standard and when invoked for standard columns it throws ClassCastException
 * on getBeanKeyValue - using super method that uses reflection doesn't throw that exception and getBeanList with columnGroupId populated will ALWAYS return SystemColumnCustom objects
 *
 * @author manderson
 */
@Component
public class SystemColumnCustomListByGroupCache extends SelfRegisteringDaoListCache<SystemColumnCustom> implements DaoSingleKeyListCache<SystemColumnCustom, Short> {


	@Override
	protected String[] getBeanKeyProperties() {
		return new String[]{"columnGroup.id"};
	}


	@Override
	public List<SystemColumnCustom> getBeanListForKeyValue(ReadOnlyDAO<SystemColumnCustom> dao, Short columnGroupId) {
		return getBeanListImpl(dao, false, columnGroupId);
	}


	@Override
	public List<SystemColumnCustom> getBeanListForKeyValueStrict(ReadOnlyDAO<SystemColumnCustom> dao, Short columnGroupId) {
		return getBeanListImpl(dao, true, columnGroupId);
	}


	@Override
	public Serializable[] getBeanIdListForKeyValue(ReadOnlyDAO<SystemColumnCustom> dao, Short columnGroupId) {
		return getBeanIdListImpl(dao, false, columnGroupId);
	}


	@Override
	public void clearBeanListForKeyValue(Short keyPropertyValue) {
		clearBeanListImpl(keyPropertyValue);
	}
}
