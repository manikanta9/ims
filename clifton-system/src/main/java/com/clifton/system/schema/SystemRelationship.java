package com.clifton.system.schema;


import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.system.schema.column.SystemColumnStandard;


/**
 * The <code>SystemRelationship</code> class represents a relationship between two database tables.
 *
 * @author vgomelsky
 */
public class SystemRelationship extends NamedHierarchicalEntity<SystemRelationship, Integer> {

	private SystemColumnStandard column;
	private SystemColumnStandard parentColumn;

	private boolean excludeFromUsedBy;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnStandard getColumn() {
		return this.column;
	}


	public void setColumn(SystemColumnStandard column) {
		this.column = column;
	}


	public SystemColumnStandard getParentColumn() {
		return this.parentColumn;
	}


	public void setParentColumn(SystemColumnStandard parentColumn) {
		this.parentColumn = parentColumn;
	}


	public boolean isExcludeFromUsedBy() {
		return this.excludeFromUsedBy;
	}


	public void setExcludeFromUsedBy(boolean excludeFromUsedBy) {
		this.excludeFromUsedBy = excludeFromUsedBy;
	}
}
