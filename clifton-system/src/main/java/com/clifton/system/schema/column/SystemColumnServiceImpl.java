package com.clifton.system.schema.column;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.audit.SystemAuditEvent;
import com.clifton.system.schema.column.cache.SystemColumnGroupColumnConfig;
import com.clifton.system.schema.column.cache.SystemColumnGroupColumnConfigCache;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.schema.column.search.SystemColumnTemplateSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * The <code>SystemColumnServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class SystemColumnServiceImpl<C extends SystemColumn> implements SystemColumnService {

	private AdvancedUpdatableDAO<C, Criteria> systemColumnDAO;

	private AdvancedUpdatableDAO<SystemColumnGroup, Criteria> systemColumnGroupDAO;

	private DaoLocator daoLocator;

	private DaoSingleKeyListCache<SystemColumnCustom, Short> systemColumnCustomListByGroupCache;
	private DaoNamedEntityCache<SystemColumnGroup> systemColumnGroupCache;
	private DaoSingleKeyCache<SystemColumnGroup, Integer> systemColumnGroupByJsonValueColumnCache;
	private SystemColumnGroupColumnConfigCache systemColumnGroupColumnConfigCache;


	private AdvancedUpdatableDAO<SystemColumnTemplate, Criteria> systemColumnTemplateDAO;


	////////////////////////////////////////////////////////////////////////////
	////////              System Column Group Methods                ////////////
	////////////////////////////////////////////////////////////////////////////

	// NOT NECESSARY TO SECURE READING GROUPS AT THIS TIME, THEY ARE USED
	// IN CONJUNCTION WITH OTHER ITEMS (COLUMNS, COLUMN VALUES) SO EXTRA SECURITY CHECK ISN'T NECESSARY


	@Override
	public SystemColumnGroup getSystemColumnGroup(short id) {
		return getSystemColumnGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemColumnGroup getSystemColumnGroupByName(String name) {
		return getSystemColumnGroupCache().getBeanForKeyValueStrict(getSystemColumnGroupDAO(), name);
	}


	@Override
	public SystemColumnGroup getSystemColumnGroupByJsonValueColumn(int systemColumnId) {
		return getSystemColumnGroupByJsonValueColumnCache().getBeanForKeyValue(getSystemColumnGroupDAO(), systemColumnId);
	}


	@Override
	public List<SystemColumnGroup> getSystemColumnGroupListByTable(short tableId) {
		return getSystemColumnGroupDAO().findByField("table.id", tableId);
	}


	@Override
	public List<SystemColumnGroup> getSystemColumnGroupListByTableAndCustomColumnType(final short tableId, final CustomColumnTypes customColumnType) {
		ValidationUtils.assertNotNull(customColumnType, "Custom Column Type is required");
		HibernateSearchConfigurer config = criteria -> {
			criteria.add(Restrictions.eq("table.id", tableId));
			criteria.add(Restrictions.eq("customColumnType", customColumnType));
		};
		return getSystemColumnGroupDAO().findBySearchCriteria(config);
	}


	@Override
	public List<SystemColumnGroup> getSystemColumnGroupListByTableName(final String tableName) {
		HibernateSearchConfigurer config = criteria -> criteria.createAlias("table", "t").add(Restrictions.eq("t.name", tableName));
		return getSystemColumnGroupDAO().findBySearchCriteria(config);
	}


	@Override
	public SystemColumnGroup saveSystemColumnGroup(SystemColumnGroup bean) {
		return getSystemColumnGroupDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                System Column Methods                  ////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public C getSystemColumn(int id) {
		return getSystemColumnDAO().findByPrimaryKey(id);
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<C> getSystemColumnList(final SystemColumnSearchForm searchForm) {
		// If linked value is null and explicitly not including null linked values, return nothing - nothing will match so don't bother going to the database
		if (searchForm.getLinkedValue() == null && BooleanUtils.isTrue(searchForm.getIncludeNullLinkedValue())) {
			return null;
		}

		if (BooleanUtils.isTrue(searchForm.getExcludeRecordStampColumns())) {
			searchForm.setExcludeNames(BeanUtils.getSystemManagedFields());
		}

		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getLinkedValue() != null) {
					if (BooleanUtils.isTrue(searchForm.getIncludeNullLinkedValue())) {
						criteria.add(Restrictions.or(Restrictions.isNull("linkedValue"), Restrictions.eq("linkedValue", searchForm.getLinkedValue())));
					}
					else {
						criteria.add(Restrictions.eq("linkedValue", searchForm.getLinkedValue()));
					}
				}
			}
		};
		return getSystemColumnDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public void deleteSystemColumn(int id) {
		getSystemColumnDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////         STANDARD SystemColumn Specific Business Methods        //////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public SystemColumnStandard getSystemColumnStandardByName(final String tableName, final String columnName) {
		return getSystemColumnStandardByDataSourceAndName(null, tableName, columnName);
	}


	@Override
	public SystemColumnStandard getSystemColumnStandardByDataSourceAndName(final String dataSourceName, final String tableName, final String columnName) {
		SystemColumnSearchForm searchForm = new SystemColumnSearchForm();

		searchForm.setTableName(tableName);
		searchForm.setName(columnName);
		searchForm.setStandardOnly(true);

		if (StringUtils.isEmpty(dataSourceName)) {
			searchForm.setDefaultDataSource(true);
		}
		else {
			searchForm.setDataSourceNameEquals(dataSourceName);
		}

		return (SystemColumnStandard) CollectionUtils.getOnlyElement(getSystemColumnList(searchForm));
	}


	@Override
	public List<SystemColumnStandard> getSystemColumnStandardListByTable(short tableId) {
		SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
		searchForm.setTableId(tableId);
		return getSystemColumnStandardList(searchForm);
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<SystemColumnStandard> getSystemColumnStandardList(SystemColumnSearchForm searchForm) {
		searchForm.setStandardOnly(true);
		return (List<SystemColumnStandard>) getSystemColumnList(searchForm);
	}


	@Override
	@SuppressWarnings("unchecked")
	public void saveSystemColumnStandard(SystemColumnStandard column) {
		if (column.getAuditType() != null && column.getTable().getName().equals(SystemAuditEvent.class.getSimpleName())) {
			throw new FieldValidationException("Auditing of 'SystemAuditEvent' table is not allowed", "auditType");
		}
		getSystemColumnDAO().save((C) column);
	}

	////////////////////////////////////////////////////////////////////////////
	//////          CUSTOM SystemColumn Specific Business Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <E extends SystemColumnCustomAware> List<SystemColumnCustom> getSystemColumnCustomListForEntity(E entity) {
		if (StringUtils.isEmpty(entity.getColumnGroupName())) {
			return null;
		}
		SystemColumnGroup columnGroup = getSystemColumnGroupByName(entity.getColumnGroupName());
		return getSystemColumnCustomListForEntityAndColumnGroup(entity, columnGroup, columnGroup.getCustomColumnType());
	}


	@Override
	public <E extends SystemColumnCustomAware> List<SystemColumnCustom> getSystemColumnCustomListForEntityAndColumnGroup(E entity, SystemColumnGroup columnGroup, CustomColumnTypes customColumnType) {
		if (columnGroup.getCustomColumnType() != customColumnType) {
			return null;
		}

		String linkedValue = null;
		// If linked property defined = get current bean's value
		if (!StringUtils.isEmpty(columnGroup.getLinkedBeanProperty())) {
			String linkedValueProperty = columnGroup.getLinkedBeanProperty();
			Object value = BeanUtils.getPropertyValue(entity, linkedValueProperty);
			linkedValue = (value == null || StringUtils.isEmpty(value.toString()) ? null : value.toString());
		}
		return getSystemColumnCustomListForGroupAndLinkedValue(columnGroup.getName(), linkedValue, true);
	}


	@Override
	public List<SystemColumnCustom> getSystemColumnCustomListForColumnGroupAndEntityId(short columnGroupId, Integer entityId, CustomColumnTypes customColumnType) {
		SystemColumnGroup group = getSystemColumnGroup(columnGroupId);

		ReadOnlyDAO<SystemColumnCustomAware> dao = getDaoLocator().locate(group.getTable().getName());
		SystemColumnCustomAware entity = dao.findByPrimaryKey(entityId);
		return getSystemColumnCustomListForEntityAndColumnGroup(entity, group, customColumnType);
	}


	@Override
	public List<SystemColumnCustom> getSystemColumnCustomListForGroupAndLinkedValue(String columnGroupName, String linkedValue, boolean includeNullLinkedValue) {
		// If linked value is null and explicitly not including null linked values, return nothing - nothing will match so don't bother going to the database
		if (StringUtils.isEmpty(linkedValue) && !includeNullLinkedValue) {
			return null;
		}

		List<SystemColumnCustom> list = getSystemColumnCustomListForGroup(columnGroupName);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}

		Predicate<SystemColumnCustom> filterCondition;
		if (!StringUtils.isEmpty(linkedValue)) {
			if (includeNullLinkedValue) {
				filterCondition = (column -> (column.getLinkedValue() == null || linkedValue.equals(column.getLinkedValue())));
			}
			else {
				filterCondition = (column -> linkedValue.equals(column.getLinkedValue()));
			}
		}
		else {
			filterCondition = (column -> column.getLinkedValue() == null);
		}
		return list.stream().filter(filterCondition).collect(Collectors.toList());
	}


	@Override
	public List<SystemColumnCustom> getSystemColumnCustomList(SystemColumnSearchForm searchForm) {
		searchForm.setCustomOnly(true);
		// noinspection unchecked
		return (List<SystemColumnCustom>) getSystemColumnList(searchForm);
	}


	/**
	 * Returns all custom columns for the given group.  Does not do any filtering on linked value, so there could be results returned
	 * with the same column name that apply.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<SystemColumnCustom> getSystemColumnCustomListForGroup(String columnGroupName) {
		SystemColumnGroup group = getSystemColumnGroupByName(columnGroupName);
		return getSystemColumnCustomListByGroupCache().getBeanListForKeyValue((ReadOnlyDAO<SystemColumnCustom>) getSystemColumnDAO(), group.getId());
	}


	@Override
	public SystemColumnGroupColumnConfig getSystemColumnGroupColumnConfig(String groupName) {
		SystemColumnGroupColumnConfig config = getSystemColumnGroupColumnConfigCache().getSystemColumnGroupColumnConfig(groupName);
		if (config == null) {
			SystemColumnGroup columnGroup = getSystemColumnGroupByName(groupName);
			List<SystemColumnCustom> customColumnList = getSystemColumnCustomListForGroup(groupName);
			config = new SystemColumnGroupColumnConfig(groupName, columnGroup.getCustomColumnType(), (columnGroup.getJsonValueColumn() != null ? ((SystemColumnStandard) columnGroup.getJsonValueColumn()).getBeanPropertyName() : null), customColumnList);
			getSystemColumnGroupColumnConfigCache().setSystemColumnGroupColumnConfig(config);
		}
		return config;
	}


	@Override
	@SuppressWarnings("unchecked")
	public void saveSystemColumnCustom(SystemColumnCustom bean) {
		// Moved from Validator to here because when copying custom column lists, if something was system defined, then we want to keep it that way
		if (bean.isNewBean()) {
			ValidationUtils.assertFalse(bean.isSystemDefined(), "System Defined Columns cannot be created via the UI", "systemDefined");
		}

		//If a column template is selected, the name of the custom column must be the same as the template name
		if (bean.getTemplate() != null) {
			ValidationUtils.assertEquals(bean.getTemplate().getName(), bean.getName(), "The custom column name: " + bean.getName() + " does not match the name of the selected template: " + bean.getTemplate().getName());
		}
		getSystemColumnDAO().save((C) bean);
	}


	@Override
	@Transactional
	public void copySystemColumnCustomListForLinkedValue(String columnGroupName, String fromLinkedValue, String toLinkedValue, String toLinkedLabel) {
		ValidationUtils.assertNotNull(columnGroupName, "Missing column group name to copy custom columns for linked value.");
		ValidationUtils.assertNotNull(fromLinkedValue, "Missing from linked value to copy custom columns for.");
		ValidationUtils.assertNotNull(toLinkedValue, "Missing to linked value to copy custom columns for.");

		List<SystemColumnCustom> fromColumns = getSystemColumnCustomListForGroupAndLinkedValue(columnGroupName, fromLinkedValue, false);
		if (!CollectionUtils.isEmpty(fromColumns)) {
			Map<String, SystemColumnCustom> newColumnMap = new HashMap<>();
			List<SystemColumnCustom> newColumns = new ArrayList<>();
			for (SystemColumnCustom col : CollectionUtils.getIterable(fromColumns)) {
				SystemColumnCustom newCol = BeanUtils.cloneBean(col, false, false);
				newCol.setLinkedValue(toLinkedValue);
				newCol.setLinkedLabel(StringUtils.coalesce(true, toLinkedLabel, toLinkedValue));
				newCol.setDependedColumn(null);
				newColumnMap.put(col.getName(), newCol);
				newColumns.add(newCol);
			}
			// Fix Dependency Columns
			for (SystemColumnCustom col : CollectionUtils.getIterable(fromColumns)) {
				if (col.getDependedColumn() != null) {
					SystemColumnCustom newCol = newColumnMap.get(col.getName());
					if (!StringUtils.isEmpty((col.getDependedColumn().getLinkedValue()))) {
						newCol.setDependedColumn(newColumnMap.get(col.getDependedColumn().getName()));
					}
					else {
						newCol.setDependedColumn(col.getDependedColumn());
					}
				}
			}
			// noinspection unchecked
			getSystemColumnDAO().saveList((List<C>) newColumns);
		}
	}


	@Override
	public Map<String, SystemColumnCustom> validateSystemColumnCustomListSameForLinkedValues(String columnGroupName, String firstLinkedValue, String secondLinkedValue, String messagePrefix, boolean bypassCustomColumnValidation) {
		Map<String, SystemColumnCustom> columnCustomMap = new HashMap<>();
		List<SystemColumnCustom> fromColumnList = getSystemColumnCustomListForGroupAndLinkedValue(columnGroupName, firstLinkedValue, false);
		List<SystemColumnCustom> toColumnList = getSystemColumnCustomListForGroupAndLinkedValue(columnGroupName, secondLinkedValue, false);
		Set<String> missingFields = new HashSet<>();
		for (SystemColumnCustom fromColumn : CollectionUtils.getIterable(fromColumnList)) {
			boolean found = false;

			for (SystemColumnCustom toColumn : CollectionUtils.getIterable(toColumnList)) {
				if (fromColumn.getName().equals(toColumn.getName())) {
					if (!fromColumn.getDataType().equals(toColumn.getDataType())) {
						throw new ValidationException(messagePrefix + "Custom Column [" + fromColumn.getName() + "] uses different data types");
					}
					columnCustomMap.put(toColumn.getName(), toColumn);
					found = true;
					break;
				}
			}
			if (!found && !bypassCustomColumnValidation) {
				missingFields.add(fromColumn.getName());
			}
		}

		if (!bypassCustomColumnValidation) {
			ValidationUtils.assertEmpty(missingFields, messagePrefix + "The following custom columns are missing from the to list and cannot be mapped: " + CollectionUtils.toString(missingFields, 10));

			// Check only for toColumn REQUIRED fields that may be missing
			Set<String> missingRequiredFields = new HashSet<>();
			for (SystemColumnCustom toColumn : CollectionUtils.getIterable(toColumnList)) {
				if (toColumn.isRequired()) {
					boolean found = false;

					for (SystemColumnCustom fromColumn : CollectionUtils.getIterable(fromColumnList)) {
						if (fromColumn.getName().equals(toColumn.getName())) {
							found = true;
							break;
						}
					}
					if (!found) {
						missingRequiredFields.add(toColumn.getName());
					}
				}
			}
			ValidationUtils.assertEmpty(missingRequiredFields, messagePrefix + "The following custom columns are required but missing from the existing list: " + CollectionUtils.toString(missingRequiredFields, 10));
		}
		return columnCustomMap;
	}


	////////////////////////////////////////////////////////////////////////////
	//////                     SystemColumnTemplate Methods               //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemColumnTemplate getSystemColumnTemplate(int id) {
		return getSystemColumnTemplateDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemColumnTemplate> getSystemColumnTemplateList(SystemColumnTemplateSearchForm searchForm) {
		return getSystemColumnTemplateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemColumnTemplate saveSystemColumnTemplate(SystemColumnTemplate bean) {
		return getSystemColumnTemplateDAO().save(bean);
	}


	@Override
	@Transactional
	public void assignSystemColumnsToTemplate(int templateId) {
		SystemColumnTemplate bean = getSystemColumnTemplate(templateId);
		ValidationUtils.assertNotNull(bean, "The template must be saved before System Columns can be assigned to it.");
		SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
		searchForm.setName(bean.getName());
		searchForm.setColumnGroupId(bean.getSystemColumnGroup().getId());
		searchForm.setTableId(bean.getSystemColumnGroup().getTable().getId());
		searchForm.setTemplateAssigned(Boolean.FALSE);

		List<SystemColumnCustom> customColumns = getSystemColumnCustomList(searchForm);
		for (SystemColumnCustom columnCustom : CollectionUtils.getIterable(customColumns)) {
			columnCustom.setTemplate(bean);
			saveSystemColumnCustom(columnCustom);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemColumnGroup, Criteria> getSystemColumnGroupDAO() {
		return this.systemColumnGroupDAO;
	}


	public void setSystemColumnGroupDAO(AdvancedUpdatableDAO<SystemColumnGroup, Criteria> systemColumnGroupDAO) {
		this.systemColumnGroupDAO = systemColumnGroupDAO;
	}


	public AdvancedUpdatableDAO<C, Criteria> getSystemColumnDAO() {
		return this.systemColumnDAO;
	}


	public void setSystemColumnDAO(AdvancedUpdatableDAO<C, Criteria> systemColumnDAO) {
		this.systemColumnDAO = systemColumnDAO;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public DaoNamedEntityCache<SystemColumnGroup> getSystemColumnGroupCache() {
		return this.systemColumnGroupCache;
	}


	public void setSystemColumnGroupCache(DaoNamedEntityCache<SystemColumnGroup> systemColumnGroupCache) {
		this.systemColumnGroupCache = systemColumnGroupCache;
	}


	public SystemColumnGroupColumnConfigCache getSystemColumnGroupColumnConfigCache() {
		return this.systemColumnGroupColumnConfigCache;
	}


	public void setSystemColumnGroupColumnConfigCache(SystemColumnGroupColumnConfigCache systemColumnGroupColumnConfigCache) {
		this.systemColumnGroupColumnConfigCache = systemColumnGroupColumnConfigCache;
	}


	public DaoSingleKeyListCache<SystemColumnCustom, Short> getSystemColumnCustomListByGroupCache() {
		return this.systemColumnCustomListByGroupCache;
	}


	public void setSystemColumnCustomListByGroupCache(DaoSingleKeyListCache<SystemColumnCustom, Short> systemColumnCustomListByGroupCache) {
		this.systemColumnCustomListByGroupCache = systemColumnCustomListByGroupCache;
	}


	public AdvancedUpdatableDAO<SystemColumnTemplate, Criteria> getSystemColumnTemplateDAO() {
		return this.systemColumnTemplateDAO;
	}


	public void setSystemColumnTemplateDAO(AdvancedUpdatableDAO<SystemColumnTemplate, Criteria> systemColumnTemplateDAO) {
		this.systemColumnTemplateDAO = systemColumnTemplateDAO;
	}


	public DaoSingleKeyCache<SystemColumnGroup, Integer> getSystemColumnGroupByJsonValueColumnCache() {
		return this.systemColumnGroupByJsonValueColumnCache;
	}


	public void setSystemColumnGroupByJsonValueColumnCache(DaoSingleKeyCache<SystemColumnGroup, Integer> systemColumnGroupByJsonValueColumnCache) {
		this.systemColumnGroupByJsonValueColumnCache = systemColumnGroupByJsonValueColumnCache;
	}
}
