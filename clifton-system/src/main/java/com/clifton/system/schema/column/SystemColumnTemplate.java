package com.clifton.system.schema.column;

import com.clifton.core.beans.NamedEntity;


/**
 * The SystemColumnTemplate class represents an optional template column. There are many instances when we have
 * multiple custom columns (for different investment security hierarchies) that have exactly the same meaning.
 * These columns can be "grouped" together by linking them to the same template column.
 * <p>
 * For example, "Strike Price" for all options.
 *
 * @author theodorez
 */
public class SystemColumnTemplate extends NamedEntity<Integer> {

	private SystemColumnGroup systemColumnGroup;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnGroup getSystemColumnGroup() {
		return this.systemColumnGroup;
	}


	public void setSystemColumnGroup(SystemColumnGroup systemColumnGroup) {
		this.systemColumnGroup = systemColumnGroup;
	}
}
