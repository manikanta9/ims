package com.clifton.system.schema.column.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


public class SystemColumnDatedValueSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchFieldPath = "column", searchField = "columnGroup.id")
	private Short columnGroupId;

	@SearchField(searchFieldPath = "column.columnGroup", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String columnGroupName;

	@SearchField(searchFieldPath = "column.columnGroup.table", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String tableName;

	@SearchField(searchFieldPath = "column", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String columnName;

	@SearchField
	private Integer fkFieldId;

	@SearchField(searchField = "column.id")
	private Integer columnId;

	@SearchField
	private String value;

	@SearchField(searchField = "value", comparisonConditions = ComparisonConditions.EQUALS)
	private String valueEquals;

	@SearchField
	private String text;

	@SearchField(searchField = "value,text", searchFieldCustomType = SearchFieldCustomTypes.OR)
	private String valueOrText;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getColumnGroupId() {
		return this.columnGroupId;
	}


	public void setColumnGroupId(Short columnGroupId) {
		this.columnGroupId = columnGroupId;
	}


	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getColumnName() {
		return this.columnName;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Integer getColumnId() {
		return this.columnId;
	}


	public void setColumnId(Integer columnId) {
		this.columnId = columnId;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getValueEquals() {
		return this.valueEquals;
	}


	public void setValueEquals(String valueEquals) {
		this.valueEquals = valueEquals;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public String getValueOrText() {
		return this.valueOrText;
	}


	public void setValueOrText(String valueOrText) {
		this.valueOrText = valueOrText;
	}
}
