package com.clifton.system.schema.column.search;

import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.schema.column.value.SystemColumnDatedValue;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;


/**
 * The <code>SystemColumnDatedValueAwareSearchFormUtils</code> is a utility class that be used to add exists clause to criteria for
 * {@link SystemColumnDatedValueAwareSearchForm}
 *
 * @author manderson
 */
public class SystemColumnDatedValueAwareSearchFormUtils {


	/**
	 * If table name, column name, and value are populated will append a where exists for the SystemColumnDatedValue table
	 */
	public static void configureCriteriaForSystemColumnDatedValueAwareSearchForm(SystemColumnDatedValueAwareSearchForm searchForm, Criteria criteria) {
		if (isSystemColumnDatedValueSearchCriteriaApplied(searchForm)) {
			DetachedCriteria subCriteria = DetachedCriteria.forClass(SystemColumnDatedValue.class, "dv");
			subCriteria.setProjection(Projections.property("id"));
			subCriteria.createAlias("column", "c");
			subCriteria.createAlias("c.table", "t");
			subCriteria.add(Restrictions.eq("t.name", searchForm.getSystemColumnDatedValueTableName()));
			subCriteria.add(Restrictions.eq("c.name", searchForm.getSystemColumnDatedValueColumnName()));
			// Applies only if Active = true - Can't think of why/how inactive search would work?
			if (BooleanUtils.isTrue(searchForm.getDatedValueActive())) {
				subCriteria.add(ActiveExpressionForDates.forActiveOnDate(true, searchForm.getDatedValueActiveOnDate()));
			}
			if (BooleanUtils.isTrue(searchForm.getDatedValueEquals())) {
				subCriteria.add(Restrictions.eq("value", searchForm.getSystemColumnDatedValue()));
			}
			else {
				subCriteria.add(Restrictions.or(Restrictions.like("value", searchForm.getSystemColumnDatedValue()), Restrictions.like("text", searchForm.getSystemColumnDatedValue())));
			}
			subCriteria.add(Restrictions.eqProperty("fkFieldId", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.exists(subCriteria));
		}
	}


	private static boolean isSystemColumnDatedValueSearchCriteriaApplied(SystemColumnDatedValueAwareSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getSystemColumnDatedValueTableName())) {
			if (!StringUtils.isEmpty(searchForm.getSystemColumnDatedValueColumnName())) {
				return !StringUtils.isEmpty(searchForm.getSystemColumnDatedValue());
			}
		}
		return false;
	}
}
