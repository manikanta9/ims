package com.clifton.system.schema.column.value;


import com.clifton.system.schema.column.SystemColumnCustomAware;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.SystemColumnGroup;

import java.util.List;


/**
 * The <code>SystemColumnValueHandler</code> defines the methods for working with {@link SystemColumnDatedValue} and {@link SystemColumnValue} Objects.
 *
 * @author apopp
 */
public interface SystemColumnValueHandler {


	////////////////////////////////////////////////////////////////////////////
	////////             System Column Value Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	public <E extends SystemColumnCustomValueAware> List<SystemColumnValue> getSystemColumnValueListForEntity(E entity);


	public <E extends SystemColumnCustomValueAware> void saveSystemColumnValueListForEntity(E entity);


	public <E extends SystemColumnCustomValueAware> List<SystemColumnValue> getSystemColumnValueListImpl(E entity, SystemColumnGroup columnGroup);


	/**
	 * Validates for an entity that all required columns have been fully populated.
	 * Properly verifies dependent fields are populated before enforcing a field being required
	 * Also, for those with global defaults doesn't enforce requirement as it just uses that value if not populated
	 * <p>
	 * NOTE: Entity must have column group name set so the system knows which columns to validate
	 */
	public <E extends SystemColumnCustomValueAware> List<String> validateRequiredSystemColumnCustomValuesForEntity(E entity);


	/**
	 * Internally consumed method for copying system column values between entities.
	 *
	 * @param deleteExisting - In most cases the toEntity is a new bean, but in some (ex. Business Contracts overwriting clauses)
	 *                       we want to delete what is there first.
	 */
	public <E extends SystemColumnCustomValueAware> void copySystemColumnValueList(E fromEntity, E toEntity, boolean deleteExisting);


	/**
	 * When moving an entity (like instruments, or securities to new hierarchy) this will allow updating the {@link SystemColumnValue} entries for the entity (or list of entities)
	 * to the correct corresponding {@link com.clifton.system.schema.column.SystemColumnCustom} columns based on the same column name.
	 * <p>
	 * Note: Unless choosing to bypass - Validation will allow this ONLY when all columns in the from list exist in the to list, and any additional columns in to the to list are allowed if not required
	 */
	public void moveSystemColumnValueListForEntityList(String columnGroupName, List<Integer> entityIdList, String fromLinkedValue, String toLinkedValue, boolean bypassCustomColumnValidation);


	public <E extends SystemColumnCustomValueAware> void deleteSystemColumnValueListForEntity(E entity);


	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Loads the cache for a specific entity list and column group name.  Sets values and globalDefaultIfNull values in cache
	 * Loads all values for columns so when individual values are retrieved they can use the cache and minimize db look ups
	 * <p>
	 * This method is used to pre-load the cache for a list of objects to reduce the number of individual column look ups
	 * Only applies to SystemColumnCustomValueAware as these are the ones with separately stored values.
	 */
	public <E extends SystemColumnCustomValueAware> void loadSystemColumnValueCacheForEntityList(List<E> entityList, String columnGroupName);


	/**
	 * Returns the custom value for the given entity, column group name, and column name in it's correct data type object.
	 * Works for both standard system column values (values stored in SystemColumnValue table) and Json Custom Values (values stored in a JSON String on the object itself)
	 */
	public <C extends SystemColumnCustomAware> Object getSystemColumnValueForEntity(C entity, String columnGroupName, String columnName, boolean returnGlobalDefaultIfNull);


	/**
	 * Returns the custom value for the given entity, column group name, column name and linked value in it's correct data type object. This allows the caller to override the
	 * linked value that is on the entity. It is useful in cases when a historical linked value is desired (e.g. viewing a Portfolio Run report for a run whose account has been
	 * switched to a different processing type).
	 * Works for both standard system column values (values stored in SystemColumnValue table) and Json Custom Values (values stored in a JSON String on the object itself).
	 */
	public <C extends SystemColumnCustomAware> Object getSystemColumnValueForEntityWithLinkedValue(C entity, String columnGroupName, String columnName, String linkedValue, boolean returnGlobalDefaultIfNull);


	/**
	 * Returns the custom value for the given entity, column group name, and column name in it's correct data type object.
	 * Works for both standard system column values (values stored in SystemColumnValue table) and Json Custom Values (values stored in a JSON String on the object itself)
	 */
	public <C extends SystemColumnCustomAware> SystemColumnValueHolder getSystemColumnValueHolderForEntity(C entity, String columnGroupName, String columnName, boolean returnGlobalDefaultIfNull);
}
