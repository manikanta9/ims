package com.clifton.system.schema.column.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.system.schema.column.SystemColumnGroup;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemColumnGroupByJsonValueColumnCache</code> class caches the system column group based on the json value column id
 *
 * @author manderson
 */
@Component
public class SystemColumnGroupByJsonValueColumnCache extends SelfRegisteringSingleKeyDaoCache<SystemColumnGroup, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "jsonValueColumn.id";
	}


	@Override
	protected Integer getBeanKeyValue(SystemColumnGroup bean) {
		if (bean == null) {
			return null;
		}
		if (bean.getJsonValueColumn() == null) {
			return null;
		}
		return bean.getJsonValueColumn().getId();
	}
}
