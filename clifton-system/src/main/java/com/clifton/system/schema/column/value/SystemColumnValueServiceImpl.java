package com.clifton.system.schema.column.value;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.SystemDataTypeConverter;
import com.clifton.system.schema.column.CustomColumnTypes;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomAware;
import com.clifton.system.schema.column.SystemColumnCustomConfig;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.SystemColumnGroup;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.column.search.SystemColumnDatedValueSearchForm;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class SystemColumnValueServiceImpl implements SystemColumnValueService {

	private AdvancedUpdatableDAO<SystemColumnValue, Criteria> systemColumnValueDAO;
	private AdvancedUpdatableDAO<SystemColumnDatedValue, Criteria> systemColumnDatedValueDAO;

	private SystemColumnService systemColumnService;
	private SystemColumnValueHandler systemColumnValueHandler;

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////////
	//////////             System Column Dated Value Methods             ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemColumnDatedValue getSystemColumnDatedValue(int id) {
		return getSystemColumnDatedValueDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemColumnDatedValue> getSystemColumnDatedValueList(SystemColumnDatedValueSearchForm searchForm) {
		return getSystemColumnDatedValueDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/*
	 * See {@link SystemColumnDatedValueObserver} is invoked for this method. It performs validation checking that ensures
	 * the date ranges do not overlap. This is only if it is a new bean or the dates have changed.
	 */
	@Override
	@Transactional
	public SystemColumnDatedValue saveSystemColumnDatedValue(SystemColumnDatedValue bean) {
		ValidationUtils.assertTrue(CustomColumnTypes.COLUMN_DATED_VALUE == bean.getColumn().getColumnGroup().getCustomColumnType(), "Column Group: " + bean.getColumn().getColumnGroup().getName() + " Column: " + bean.getColumn().getLabel() + " does not support dated values");
		ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getValue()), "Value is required for system column dated values.");

		if (bean.isNewBean() && bean.getStartDate() != null) {
			// Find currently active value
			SystemColumnDatedValueSearchForm existingSearchForm = new SystemColumnDatedValueSearchForm();
			existingSearchForm.setColumnId(bean.getColumn().getId());
			existingSearchForm.setFkFieldId(bean.getFkFieldId());
			existingSearchForm.setActiveOnDate(bean.getStartDate());
			SystemColumnDatedValue existing = CollectionUtils.getFirstElement(getSystemColumnDatedValueList(existingSearchForm));
			if (existing != null && existing.getEndDate() == null) {
				existing.setEndDate(DateUtils.addDays(bean.getStartDate(), -1));
				getSystemColumnDatedValueDAO().save(existing);
			}
		}

		// Validate Value Data Type
		SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(bean.getColumn().getDataType());
		try {
			Object value = dataTypeConverter.convert(bean.getValue());
			bean.setValue(new ObjectToStringConverter().convert(value));
			if (StringUtils.isEmpty(bean.getText())) {
				bean.setText(bean.getValue());
			}
		}
		catch (Throwable e) {
			throw new ValidationException("Cannot convert value '" + bean.getValue() + "' to data type '" + bean.getColumn().getDataType().getName() + "' for column '" + bean.getColumn().getName() + "'");
		}
		return getSystemColumnDatedValueDAO().save(bean);
	}


	@Override
	public void deleteSystemColumnDatedValue(int id) {
		getSystemColumnDatedValueDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////          System Column Dated Value Object Methods          //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For the given {@link SystemColumnDatedValue} returns the field value as an object - i.e. Boolean, BigDecimal, String, etc.
	 */
	@Override
	public Object getSystemColumnDatedValueAsObject(SystemColumnDatedValue bean) {
		if (bean != null) {
			SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(bean.getColumn().getDataType());
			return dataTypeConverter.convert(bean.getValue());
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               System Column Value Methods               ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemColumnValue> getSystemColumnValueList(SystemColumnValueSearchForm searchForm) {
		SearchFormUtils.convertSearchRestrictionList(searchForm);

		// try performance optimization by avoiding an extra join
		if (searchForm.getColumnGroupName() != null) {
			if (searchForm.getColumnGroupId() == null) {
				searchForm.setColumnGroupId(getSystemColumnService().getSystemColumnGroupByName(searchForm.getColumnGroupName()).getId());
				searchForm.setColumnGroupName(null);
			}
		}

		searchForm.validate();
		return getSystemColumnValueDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public void saveSystemColumnValueList(SystemColumnCustomConfig config) {
		if (!StringUtils.isEmpty(config.getColumnGroupName())) {
			SystemColumnGroup group = getSystemColumnService().getSystemColumnGroupByName(config.getColumnGroupName());
			SystemColumnCustomValueAware entity = locateEntity(group.getTable().getName(), config.getEntityId());
			if (entity == null) {
				// Note: In one case where this happened, UI was submitting ID value twice so it was concatenating it.
				throw new IllegalStateException("Cannot find [" + group.getTable().getName() + "] Entity with ID [" + config.getEntityId() + "] in the database.  Unable to save custom column values.");
			}
			entity.setColumnGroupName(config.getColumnGroupName());

			List<SystemColumnValue> columnValueList = new ArrayList<>();
			// I think because the config isn't a real bean, these objects aren't populated
			for (SystemColumnValue columnValue : CollectionUtils.getIterable(config.getColumnValueList())) {
				// If there's an ID, and no column, get the column from the original bean
				if (columnValue.getId() != null && columnValue.getColumn() == null) {
					SystemColumnValue originalValue = getSystemColumnValueDAO().findByPrimaryKey(columnValue.getId());
					ValidationUtils.assertNotNull(originalValue, "Cannot find System Column Value in the database for " + columnValue);
					columnValue.setColumn(originalValue.getColumn());
				}
				// Otherwise if there is a column id, need to populate the column object
				if (StringUtils.isEmpty(columnValue.getColumn().getName())) {
					columnValue.setColumn((SystemColumnCustom) getSystemColumnService().getSystemColumn(columnValue.getColumn().getId()));
				}
				columnValueList.add(columnValue);
			}
			entity.setColumnValueList(columnValueList);
			getSystemColumnValueHandler().saveSystemColumnValueListForEntity(entity);
		}
	}


	@Override
	public void copySystemColumnValueList(String tableName, String columnGroupName, int fromEntityId, int toEntityId, boolean deleteExisting) {
		SystemColumnCustomValueAware fromEntity = locateEntity(tableName, fromEntityId);
		SystemColumnCustomValueAware toEntity = locateEntity(tableName, toEntityId);

		ValidationUtils.assertNotNull(fromEntity, "Cannot find from entity in the database for id [" + fromEntityId + "] on table [" + tableName + "]");
		ValidationUtils.assertNotNull(toEntity, "Cannot find to entity in the database for id [" + toEntityId + "] on table [" + tableName + "]");

		fromEntity.setColumnGroupName(columnGroupName);
		getSystemColumnValueHandler().copySystemColumnValueList(fromEntity, toEntity, deleteExisting);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////            System Column Value Object Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Object getSystemColumnValueForEntityByTable(String tableName, Integer entityId, String columnGroupName, String columnName, boolean returnGlobalDefaultIfNull) {
		SystemColumnCustomAware entity = locateEntity(tableName, entityId);
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(entity, columnGroupName, columnName, returnGlobalDefaultIfNull);
	}


	@Override
	public Object getSystemColumnCustomValue(int entityId, String columnGroupName, String columnName) {
		String tableName = getSystemColumnService().getSystemColumnGroupByName(columnGroupName).getTable().getName();
		return getSystemColumnValueForEntityByTable(tableName, entityId, columnGroupName, columnName, true);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////                  Config Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemColumnCustomConfig getSystemColumnCustomConfig(SystemColumnCustomConfig config) {
		if (StringUtils.isEmpty(config.getColumnGroupName())) {
			return null;
		}
		SystemColumnGroup columnGroup = getSystemColumnService().getSystemColumnGroupByName(config.getColumnGroupName());
		if (columnGroup.getCustomColumnType() == CustomColumnTypes.COLUMN_JSON_VALUE) {
			ValidationUtils.assertNotNull(columnGroup.getJsonValueColumn(), "Custom Column Group " + columnGroup.getName() + " uses json column value for values, however the column is not defined on the column group.");
			ValidationUtils.assertFalse(columnGroup.getJsonValueColumn().isCustomColumn(), "Json Value Column " + columnGroup.getJsonValueColumn().getName() + " for column group " + columnGroup.getName() + " is invalid.  Only standard columns can hold json values.");
			config.setJsonValueBeanPropertyName(((SystemColumnStandard) columnGroup.getJsonValueColumn()).getBeanPropertyName());
		}

		if (config.getEntityId() != null) {
			SystemColumnCustomAware entity = locateEntity(columnGroup.getTable().getName(), config.getEntityId());
			ValidationUtils.assertNotNull(entity, "Missing " + columnGroup.getTable().getName() + " with ID " + config.getEntityId() + " in the database.");
			entity.setColumnGroupName(config.getColumnGroupName());
			if (columnGroup.getCustomColumnType() == CustomColumnTypes.COLUMN_VALUE) {
				config.setColumnValueList(getSystemColumnValueHandler().getSystemColumnValueListImpl((SystemColumnCustomValueAware) entity, columnGroup));
			}
			if (config.getLinkedValue() == null) {
				config.setColumnList(getSystemColumnService().getSystemColumnCustomListForEntityAndColumnGroup(entity, columnGroup, CustomColumnTypes.COLUMN_VALUE));
			}
		}
		// Pull from value passed even if the entity exists
		if (config.getEntityId() == null || config.getLinkedValue() != null) {
			config.setColumnList(getSystemColumnService().getSystemColumnCustomListForGroupAndLinkedValue(config.getColumnGroupName(), config.getLinkedValue(), true));
		}
		return config;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private <E extends SystemColumnCustomAware> E locateEntity(String tableName, Number entityId) {
		ReadOnlyDAO<E> dao = getDaoLocator().locate(tableName);
		return dao.findByPrimaryKey(entityId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemColumnValue, Criteria> getSystemColumnValueDAO() {
		return this.systemColumnValueDAO;
	}


	public void setSystemColumnValueDAO(AdvancedUpdatableDAO<SystemColumnValue, Criteria> systemColumnValueDAO) {
		this.systemColumnValueDAO = systemColumnValueDAO;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public AdvancedUpdatableDAO<SystemColumnDatedValue, Criteria> getSystemColumnDatedValueDAO() {
		return this.systemColumnDatedValueDAO;
	}


	public void setSystemColumnDatedValueDAO(AdvancedUpdatableDAO<SystemColumnDatedValue, Criteria> systemColumnDatedValueDAO) {
		this.systemColumnDatedValueDAO = systemColumnDatedValueDAO;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
