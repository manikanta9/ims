package com.clifton.system.schema.column;


import com.clifton.core.security.authorization.SecureMethodSecurityResourceResolver;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;


/**
 * The <code>SystemColumnGroupNameSecureMethodResolver</code> returns security resource for columnGroupName if set - otherwise security resource associated with the table associated with the column group
 * <p>
 * Used for security for requests for custom column values where the bean itself only has reference to the columnGroupName - no id properties to use for dynamic look ups
 *
 * @author Mary Anderson
 */
@Component
public class SystemColumnGroupNameSecureMethodResolver implements SecureMethodSecurityResourceResolver {

	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getSecurityResourceName(@SuppressWarnings("unused") Method method, Map<String, ?> config) {
		SystemColumnGroup group = getSystemColumnGroup(config);
		if (group != null) {
			if (group.getSecurityResource() != null) {
				return group.getSecurityResource().getName();
			}
			if (group.getTable().getSecurityResource() != null) {
				return group.getTable().getSecurityResource().getName();
			}
		}
		return null;
	}


	private SystemColumnGroup getSystemColumnGroup(Map<String, ?> config) {
		String columnGroupName = MapUtils.getParameterAsString("columnGroupName", config);
		if (!StringUtils.isEmpty(columnGroupName)) {
			return getSystemColumnService().getSystemColumnGroupByName(columnGroupName);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}
}
