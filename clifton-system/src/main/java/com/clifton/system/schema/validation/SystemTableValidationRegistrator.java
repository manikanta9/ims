package com.clifton.system.schema.validation;


import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemTableValidationRegistrator</code> interface defines methods that help register/unregister validators
 * to corresponding DAO's based on {@link SystemTable} data.
 *
 * @author manderson
 */
public interface SystemTableValidationRegistrator {

	/**
	 * Registers observers enabled by {@link SystemTable} entity modify conditions for the specified table's DAO.
	 */
	public void registerObservers(SystemTable table);


	/**
	 * Unregisters observers enabled by {@link SystemTable} entity modify conditions for the specified table's DAO.
	 */
	public void unregisterObservers(SystemTable table);
}
