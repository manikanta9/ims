package com.clifton.system.schema.column;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * The <code>CustomColumnTypes</code> enum defines the type of custom columns this group uses.  The type of custom columns defines how and where the values are stored.
 *
 * @author manderson
 */
public enum CustomColumnTypes {

	COLUMN_VALUE, // Standard custom column that uses SystemColumnValue table

	COLUMN_DATED_VALUE, // Custom column that uses SystemColumnDatedValue table (values apply to a date range)

	COLUMN_JSON_VALUE {  // All column Values for the group are stored in a single standard column on the table as one JSON string value


		@Override
		public void validateSystemColumnCustom(SystemColumnCustom systemColumnCustom, SystemColumnCustom originalSystemColumnCustom, DaoEventTypes eventType) {
			// Note: For not prohibiting this, but we could at least 1. check if it's used or 2. delete uses (would require retrieval on the entire linked table filtered to just those referenced by linked property value)
			ValidationUtils.assertFalse(eventType.isDelete(), "Deletes are not currently supported for custom columns that use Json Values");

			// Updates - Cannot change the Column Name (since it's used in the json property)
			// Note: again could allow this if we then modified the json property value in all usages of it.  Would need to check the entire linked table filtered to just those referenced by linked property value)
			if (eventType.isUpdate() && originalSystemColumnCustom != null) {
				ValidationUtils.assertTrue(StringUtils.isEqual(systemColumnCustom.getName(), originalSystemColumnCustom.getName()), "The column name cannot be changed for Json Custom Column Values.");
			}

			// Confirm Column Names are Alpha Numeric only and no spaces
			ValidationUtils.assertTrue(systemColumnCustom.getName().matches(StringUtils.ALPHA_NUMERIC_START_WITH_ALPHA_ONLY_REGEX), "Json Value Column Column Names must be alpha numeric only and must begin with a letter.");
			// Note: Since most are inserted via data migration, should add a notification to check these as well
		}
	};


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Called from the SystemColumnValidator to perform CustomColumnType specific logic on Inserts / Updates / Deletes
	 *
	 * @param originalSystemColumnCustom - only populated for UPDATES
	 */
	public void validateSystemColumnCustom(SystemColumnCustom systemColumnCustom, SystemColumnCustom originalSystemColumnCustom, DaoEventTypes eventType) {
		// DEFAULT - NO ADDITIONAL VALIDATION
	}
}
