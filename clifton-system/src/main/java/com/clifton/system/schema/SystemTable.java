package com.clifton.system.schema;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.system.audit.SystemAuditType;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.column.SystemColumnStandard;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemTable</code> class represents a database table.
 *
 * @author vgomelsky
 */
public class SystemTable extends NamedEntity<Short> {

	private SystemDataSource dataSource;
	private SystemAuditType auditType;
	private SecurityResource securityResource;

	/**
	 * The unique short-name identifier for the table.
	 */
	private String alias;

	private boolean uploadAllowed;

	/**
	 * Used to validate entities on insert/update/delete
	 * dao events.
	 */
	private SystemCondition insertEntityCondition;
	private SystemCondition updateEntityCondition;
	private SystemCondition deleteEntityCondition;

	/**
	 * User Interface Configuration
	 * detailScreenClass = UI Class for detail page (Example Clifton.investment.account.AccountWindow)
	 * listScreenClass = UI Class for list page (Example Clifton.investment.account.AccountSetupWindow)
	 * entityListUrl = URL for dropdown to select an entity from this table (Example investmentAccountListFind.json)
	 * screenIconClass = Icon Cls (Example account)
	 */

	private String detailScreenClass;
	private String listScreenClass;
	private String entityListUrl;
	private String screenIconCls;

	/**
	 * Allows the system defined query limit to be overridden.
	 */
	private Integer maxQuerySizeLimit;

	private List<SystemColumnStandard> columnList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isHasModifyCondition() {
		return getInsertEntityCondition() != null || getUpdateEntityCondition() != null || getDeleteEntityCondition() != null;
	}


	public void addColumn(SystemColumnStandard column) {
		if (this.columnList == null) {
			this.columnList = new ArrayList<>();
		}
		this.columnList.add(column);
	}


	/**
	 * Includes data source name for non-default data sources.
	 */
	public String getNameExpanded() {
		return (getDataSource() != null && !getDataSource().isDefaultDataSource()) ? getName() + " (" + getDataSource().getName() + ")" : getName();
	}


	/**
	 * Includes data source name for non-default data sources.
	 */
	public String getLabelExpanded() {
		return (getDataSource() != null && !getDataSource().isDefaultDataSource()) ? getLabel() + " (" + getDataSource().getName() + ")" : getLabel();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(SystemDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public SystemAuditType getAuditType() {
		return this.auditType;
	}


	public void setAuditType(SystemAuditType auditType) {
		this.auditType = auditType;
	}


	public SecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(SecurityResource securityResource) {
		this.securityResource = securityResource;
	}


	public List<SystemColumnStandard> getColumnList() {
		return this.columnList;
	}


	public void setColumnList(List<SystemColumnStandard> columnList) {
		this.columnList = columnList;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public boolean isUploadAllowed() {
		return this.uploadAllowed;
	}


	public void setUploadAllowed(boolean uploadAllowed) {
		this.uploadAllowed = uploadAllowed;
	}


	public SystemCondition getInsertEntityCondition() {
		return this.insertEntityCondition;
	}


	public void setInsertEntityCondition(SystemCondition insertEntityCondition) {
		this.insertEntityCondition = insertEntityCondition;
	}


	public SystemCondition getUpdateEntityCondition() {
		return this.updateEntityCondition;
	}


	public void setUpdateEntityCondition(SystemCondition updateEntityCondition) {
		this.updateEntityCondition = updateEntityCondition;
	}


	public SystemCondition getDeleteEntityCondition() {
		return this.deleteEntityCondition;
	}


	public void setDeleteEntityCondition(SystemCondition deleteEntityCondition) {
		this.deleteEntityCondition = deleteEntityCondition;
	}


	public String getDetailScreenClass() {
		return this.detailScreenClass;
	}


	public void setDetailScreenClass(String detailScreenClass) {
		this.detailScreenClass = detailScreenClass;
	}


	public String getListScreenClass() {
		return this.listScreenClass;
	}


	public void setListScreenClass(String listScreenClass) {
		this.listScreenClass = listScreenClass;
	}


	public String getEntityListUrl() {
		return this.entityListUrl;
	}


	public void setEntityListUrl(String entityListUrl) {
		this.entityListUrl = entityListUrl;
	}


	public String getScreenIconCls() {
		return this.screenIconCls;
	}


	public void setScreenIconCls(String screenIconCls) {
		this.screenIconCls = screenIconCls;
	}


	public Integer getMaxQuerySizeLimit() {
		return this.maxQuerySizeLimit;
	}


	public void setMaxQuerySizeLimit(Integer maxQuerySizeLimit) {
		this.maxQuerySizeLimit = maxQuerySizeLimit;
	}
}
