package com.clifton.system.schema.column.comparison;


import com.clifton.system.schema.column.SystemColumnCustomAware;


/**
 * The <code>SystemColumnCustomFieldValueDoesNotEqualComparison</code> ...
 *
 * @author manderson
 */
public class SystemColumnCustomFieldNotEqualsToValueComparison<T extends SystemColumnCustomAware> extends SystemColumnCustomFieldEqualsToValueComparison<T> {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
