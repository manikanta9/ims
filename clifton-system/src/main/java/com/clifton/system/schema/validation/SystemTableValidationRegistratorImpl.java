package com.clifton.system.schema.validation;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.List;


/**
 * The <code>SystemTableValidationRegistratorImpl</code> class is a Spring {@link ApplicationListener} that
 * registers {@link SystemTableValidationValidator}s for DAO's whose tables have any entity modify conditions defined
 * in the {@link SystemTable} table.
 *
 * @author manderson
 */
public class SystemTableValidationRegistratorImpl<T extends IdentityObject> implements SystemTableValidationRegistrator, CurrentContextApplicationListener<ContextRefreshedEvent> {

	private SystemSchemaService systemSchemaService;

	private DaoLocator daoLocator;


	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			List<SystemTable> tableList = getSystemSchemaService().getSystemTableListWithEntityModifyCondition();
			for (SystemTable table : CollectionUtils.getIterable(tableList)) {
				registerObservers(table);
			}
		}
		catch (Throwable e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error registering DAO validators: " + event, e);
		}
	}


	@Override
	@SuppressWarnings("unchecked")
	public void registerObservers(SystemTable table) {
		// do not register observers for externally managed tables: reside in other applications
		if (!table.getDataSource().isExternal()) {
			// get DAO that's being observed
			ObserverableDAO<T> dao = (ObserverableDAO<T>) getDaoLocator().locate(table.getName());
			DaoEventObserver<T> observer = new SystemTableValidationValidator<>(table);
			getApplicationContext().getAutowireCapableBeanFactory().autowireBeanProperties(observer, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

			// register observers as necessary

			if (table.getInsertEntityCondition() != null) {
				dao.registerEventObserver(DaoEventTypes.INSERT, observer);
			}

			if (table.getUpdateEntityCondition() != null) {
				dao.registerEventObserver(DaoEventTypes.UPDATE, observer);
			}

			if (table.getDeleteEntityCondition() != null) {
				dao.registerEventObserver(DaoEventTypes.DELETE, observer);
			}
		}
	}


	@Override
	@SuppressWarnings("unchecked")
	public void unregisterObservers(SystemTable table) {
		// do not unregister observers for externally managed tables: reside in other applications
		if (!table.getDataSource().isExternal()) {
			ObserverableDAO<T> dao = (ObserverableDAO<T>) getDaoLocator().locate(table.getName());

			DaoEventObserver<T> observer = new SystemTableValidationValidator<>(table);

			// unregister observers
			dao.unregisterEventObserver(DaoEventTypes.INSERT, observer);
			dao.unregisterEventObserver(DaoEventTypes.UPDATE, observer);
			dao.unregisterEventObserver(DaoEventTypes.DELETE, observer);
		}
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
