package com.clifton.system.schema.copy;

import com.clifton.core.util.status.Status;

import java.io.Serializable;


/**
 * @author vgomelsky
 */
public class SchemaFieldsCopyCommand implements Serializable {

	private String fromDataSource;
	private String toDataSource;
	private String tables;

	private boolean copyTableLabelAndDescription;
	private boolean copyAuditType;
	private boolean copySecurityResource;
	private boolean copyUserInterfaceConfiguration;
	private boolean copyColumnInformation;
	private boolean overwriteExistingValues;


	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFromDataSource() {
		return this.fromDataSource;
	}


	public void setFromDataSource(String fromDataSource) {
		this.fromDataSource = fromDataSource;
	}


	public String getToDataSource() {
		return this.toDataSource;
	}


	public void setToDataSource(String toDataSource) {
		this.toDataSource = toDataSource;
	}


	public String getTables() {
		return this.tables;
	}


	public void setTables(String tables) {
		this.tables = tables;
	}


	public boolean isCopyTableLabelAndDescription() {
		return this.copyTableLabelAndDescription;
	}


	public void setCopyTableLabelAndDescription(boolean copyTableLabelAndDescription) {
		this.copyTableLabelAndDescription = copyTableLabelAndDescription;
	}


	public boolean isCopyAuditType() {
		return this.copyAuditType;
	}


	public void setCopyAuditType(boolean copyAuditType) {
		this.copyAuditType = copyAuditType;
	}


	public boolean isCopySecurityResource() {
		return this.copySecurityResource;
	}


	public void setCopySecurityResource(boolean copySecurityResource) {
		this.copySecurityResource = copySecurityResource;
	}


	public boolean isCopyUserInterfaceConfiguration() {
		return this.copyUserInterfaceConfiguration;
	}


	public void setCopyUserInterfaceConfiguration(boolean copyUserInterfaceConfiguration) {
		this.copyUserInterfaceConfiguration = copyUserInterfaceConfiguration;
	}


	public boolean isCopyColumnInformation() {
		return this.copyColumnInformation;
	}


	public void setCopyColumnInformation(boolean copyColumnInformation) {
		this.copyColumnInformation = copyColumnInformation;
	}


	public boolean isOverwriteExistingValues() {
		return this.overwriteExistingValues;
	}


	public void setOverwriteExistingValues(boolean overwriteExistingValues) {
		this.overwriteExistingValues = overwriteExistingValues;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
