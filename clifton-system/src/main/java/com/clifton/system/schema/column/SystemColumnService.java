package com.clifton.system.schema.column;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.system.schema.column.cache.SystemColumnGroupColumnConfig;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.schema.column.search.SystemColumnTemplateSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;


/**
 * The <code>SystemColumnService</code> defines the methods for retrieving/updating
 * SystemColumn & SystemColumnCustom entities
 *
 * @author Mary Anderson
 */
public interface SystemColumnService {

	////////////////////////////////////////////////////////////////////////////
	////////             System Column Group Methods                ////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(disableSecurity = true)
	public SystemColumnGroup getSystemColumnGroup(short id);


	@SecureMethod(disableSecurity = true)
	public SystemColumnGroup getSystemColumnGroupByName(String name);


	@DoNotAddRequestMapping
	public SystemColumnGroup getSystemColumnGroupByJsonValueColumn(int systemColumnId);


	@SecureMethod(disableSecurity = true)
	public List<SystemColumnGroup> getSystemColumnGroupListByTable(short tableId);


	@SecureMethod(disableSecurity = true)
	public List<SystemColumnGroup> getSystemColumnGroupListByTableAndCustomColumnType(short tableId, CustomColumnTypes customColumnType);


	@SecureMethod(disableSecurity = true)
	public List<SystemColumnGroup> getSystemColumnGroupListByTableName(String tableName);


	public SystemColumnGroup saveSystemColumnGroup(SystemColumnGroup bean);


	//////////////////////////////////////////////////////////////////////////// 
	//////////            BaseSystemColumn Business Methods            ///////// 
	////////////////////////////////////////////////////////////////////////////


	public <C extends SystemColumn> C getSystemColumn(int id);


	public <C extends SystemColumn> List<C> getSystemColumnList(SystemColumnSearchForm searchForm);


	/**
	 * Only custom columns can be deleted. An exception will be thrown if an attempt is made to delete a standard column.
	 */
	@SecureMethod(dynamicSecurityResourceBeanPath = "columnGroup.securityResource.name", dynamicTableNameBeanPath = "columnGroup.table.name", dtoClass = SystemColumnCustom.class, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void deleteSystemColumn(int id);


	//////////////////////////////////////////////////////////////////////////// 
	//////         STANDARD SystemColumn Specific Business Methods        ////// 
	////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public SystemColumnStandard getSystemColumnStandardByName(final String tableName, final String columnName);


	@DoNotAddRequestMapping
	public SystemColumnStandard getSystemColumnStandardByDataSourceAndName(final String dataSourceName, final String tableName, final String columnName);


	public List<SystemColumnStandard> getSystemColumnStandardListByTable(short tableId);


	public List<SystemColumnStandard> getSystemColumnStandardList(SystemColumnSearchForm searchForm);


	@SecureMethod(dynamicTableNameBeanPath = "table.name", permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void saveSystemColumnStandard(SystemColumnStandard column);


	//////////////////////////////////////////////////////////////////////////// 
	//////         CUSTOM SystemColumn Specific Business Methods        ////// 
	////////////////////////////////////////////////////////////////////////////


	public <E extends SystemColumnCustomAware> List<SystemColumnCustom> getSystemColumnCustomListForEntity(E entity);


	@DoNotAddRequestMapping
	public <E extends SystemColumnCustomAware> List<SystemColumnCustom> getSystemColumnCustomListForEntityAndColumnGroup(E entity, SystemColumnGroup columnGroup, CustomColumnTypes customColumnType);


	@SecureMethod(dynamicIdUrlParameter = "columnGroupId", dtoClass = SystemColumnGroup.class, dynamicSecurityResourceBeanPath = "securityResource.name", dynamicTableNameBeanPath = "table.name")
	public List<SystemColumnCustom> getSystemColumnCustomListForColumnGroupAndEntityId(short columnGroupId, Integer entityId, CustomColumnTypes customColumnType);


	public List<SystemColumnCustom> getSystemColumnCustomListForGroupAndLinkedValue(String columnGroupName, String linkedValue, boolean includeNullLinkedValue);


	public List<SystemColumnCustom> getSystemColumnCustomList(SystemColumnSearchForm searchForm);


	/**
	 * Returns all custom columns for the given group.  Does not do any filtering on linked value, so there could be results returned
	 * with the same column name that apply.
	 */
	@DoNotAddRequestMapping
	public List<SystemColumnCustom> getSystemColumnCustomListForGroup(String columnGroupName);


	@SecureMethod(dynamicSecurityResourceBeanPath = "columnGroup.securityResource.name", dynamicTableNameBeanPath = "columnGroup.table.name", permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void saveSystemColumnCustom(SystemColumnCustom bean);


	public void copySystemColumnCustomListForLinkedValue(String columnGroupName, String fromLinkedValue, String toLinkedValue, String toLinkedLabel);


	/**
	 * Validation method that checks for the given column group name and linked values that all column names in the from list exist in the to list (to list can have more)
	 * Used when we want to be able to "move" an entity but ensure custom columns will map over by name
	 * <p>
	 * i.e. Move Instrument to new Hierarchy - need to map instrument and securities to reference new columns, but before getting there, need to validate they are the same
	 */
	@DoNotAddRequestMapping
	public Map<String, SystemColumnCustom> validateSystemColumnCustomListSameForLinkedValues(String columnGroupName, String firstLinkedValue, String secondLinkedValue, String messagePrefix, boolean bypassCustomColumnValidation);


	@DoNotAddRequestMapping
	public SystemColumnGroupColumnConfig getSystemColumnGroupColumnConfig(String groupName);


	////////////////////////////////////////////////////////////////////////////
	//////                     SystemColumnTemplate Methods               //////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnTemplate getSystemColumnTemplate(int id);


	public List<SystemColumnTemplate> getSystemColumnTemplateList(SystemColumnTemplateSearchForm searchForm);


	public SystemColumnTemplate saveSystemColumnTemplate(SystemColumnTemplate bean);


	/**
	 * Finds all existing custom columns using the template name, column group, and table that have not been assigned to a template.
	 * Assigns them to the given template.
	 */
	@SecureMethod(dtoClass = SystemColumn.class)
	@RequestMapping("systemColumnsAssignToTemplate")
	public void assignSystemColumnsToTemplate(int templateId);
}
