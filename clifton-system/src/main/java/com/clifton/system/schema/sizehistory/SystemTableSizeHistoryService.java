package com.clifton.system.schema.sizehistory;

import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.List;


/**
 * @author davidi
 */
public interface SystemTableSizeHistoryService {

	public SystemTableSizeHistory getSystemTableSizeHistory(Integer id);


	public List<SystemTableSizeHistory> getSystemTableSizeHistoryList(SystemTableSizeHistorySearchForm searchForm);


	@DoNotAddRequestMapping
	public SystemTableSizeHistory saveSystemTableSizeHistory(SystemTableSizeHistory systemTableSizeHistory);
}
