package com.clifton.system.schema.column.value;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.util.Date;


/**
 * The <code>SystemColumnDatedValue</code> is similar in structure to SystemColumnValue.  The main difference is that class supports
 * date range.  Therefore you can have a value that is active for a specific range of dates.  This is useful if the values change
 * over time but you want to maintain a history.
 * <p>
 * Ex. A futures instruments data field tab. Click add which will allow a new dated value entry.
 *
 * Values stored here are for custom columns under a column group of CustomColumnTypes.COLUMN_DATED_VALUE
 *
 * @author apopp
 */
public class SystemColumnDatedValue extends BaseEntity<Integer> implements LabeledObject {

	private SystemColumnCustom column;

	// The entity this field value applies to
	@SoftLinkField(tableBeanPropertyName = "column.columnGroup.table")
	private Integer fkFieldId;

	private String value;

	/**
	 * Text field stores display text when applicable.
	 * For example, for drop downs, value usually stores 'id' and text stores 'label' of corresponding entity.
	 * Otherwise stores the value
	 */
	private String text;

	private Date startDate;
	private Date endDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		result.append(",column=");
		result.append(getColumn());
		result.append(",value=");
		result.append(getValue());
		result.append(",text=");
		result.append(getText());
		result.append('}');
		return result.toString();
	}


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		result.append(this.column == null ? "UNKNOWN-COLUMN" : this.column.getLabel());
		result.append(" = ");
		result.append(this.text);
		return result.toString();
	}


	public String getLabelWithDates() {
		return getLabel() + "  " + DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public SystemColumnCustom getColumn() {
		return this.column;
	}


	public void setColumn(SystemColumnCustom column) {
		this.column = column;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
