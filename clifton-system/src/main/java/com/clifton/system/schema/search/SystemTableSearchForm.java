package com.clifton.system.schema.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class SystemTableSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "name")
	private String[] tableNames;

	@SearchField(searchField = "dataSource.id")
	private Short dataSourceId;

	@SearchField(searchFieldPath = "dataSource")
	private Boolean defaultDataSource;

	@SearchField(searchField = "auditType.id")
	private Short auditTypeId;

	@SearchField(searchField = "securityResource.id")
	private Integer securityResourceId;

	@SearchField(searchFieldPath = "securityResource", searchField = "name")
	private String securityResourceName;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String label;

	@SearchField
	private String alias;

	@SearchField(searchField = "alias", comparisonConditions = ComparisonConditions.EQUALS)
	private String aliasEquals;

	@SearchField
	private Boolean uploadAllowed;

	@SearchField(searchFieldPath = "insertEntityCondition", searchField = "name")
	private String insertEntityConditionName;

	@SearchField(searchFieldPath = "updateEntityCondition", searchField = "name")
	private String updateEntityConditionName;

	@SearchField(searchFieldPath = "deleteEntityCondition", searchField = "name")
	private String deleteEntityConditionName;

	@SearchField
	private String detailScreenClass;

	@SearchField
	private String listScreenClass;

	@SearchField
	private String entityListUrl;

	@SearchField
	private String screenIconCls;

	@SearchField
	private Integer maxQuerySizeLimit;

	@SearchField(searchField = "maxQuerySizeLimit", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean maxQuerySizeLimitNotNull;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String[] getTableNames() {
		return this.tableNames;
	}


	public void setTableNames(String[] tableNames) {
		this.tableNames = tableNames;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public Boolean getDefaultDataSource() {
		return this.defaultDataSource;
	}


	public void setDefaultDataSource(Boolean defaultDataSource) {
		this.defaultDataSource = defaultDataSource;
	}


	public Short getAuditTypeId() {
		return this.auditTypeId;
	}


	public void setAuditTypeId(Short auditTypeId) {
		this.auditTypeId = auditTypeId;
	}


	public Integer getSecurityResourceId() {
		return this.securityResourceId;
	}


	public void setSecurityResourceId(Integer securityResourceId) {
		this.securityResourceId = securityResourceId;
	}


	public String getSecurityResourceName() {
		return this.securityResourceName;
	}


	public void setSecurityResourceName(String securityResourceName) {
		this.securityResourceName = securityResourceName;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getAliasEquals() {
		return this.aliasEquals;
	}


	public void setAliasEquals(String aliasEquals) {
		this.aliasEquals = aliasEquals;
	}


	public Boolean getUploadAllowed() {
		return this.uploadAllowed;
	}


	public void setUploadAllowed(Boolean uploadAllowed) {
		this.uploadAllowed = uploadAllowed;
	}


	public String getInsertEntityConditionName() {
		return this.insertEntityConditionName;
	}


	public void setInsertEntityConditionName(String insertEntityConditionName) {
		this.insertEntityConditionName = insertEntityConditionName;
	}


	public String getUpdateEntityConditionName() {
		return this.updateEntityConditionName;
	}


	public void setUpdateEntityConditionName(String updateEntityConditionName) {
		this.updateEntityConditionName = updateEntityConditionName;
	}


	public String getDeleteEntityConditionName() {
		return this.deleteEntityConditionName;
	}


	public void setDeleteEntityConditionName(String deleteEntityConditionName) {
		this.deleteEntityConditionName = deleteEntityConditionName;
	}


	public String getDetailScreenClass() {
		return this.detailScreenClass;
	}


	public void setDetailScreenClass(String detailScreenClass) {
		this.detailScreenClass = detailScreenClass;
	}


	public String getListScreenClass() {
		return this.listScreenClass;
	}


	public void setListScreenClass(String listScreenClass) {
		this.listScreenClass = listScreenClass;
	}


	public String getEntityListUrl() {
		return this.entityListUrl;
	}


	public void setEntityListUrl(String entityListUrl) {
		this.entityListUrl = entityListUrl;
	}


	public String getScreenIconCls() {
		return this.screenIconCls;
	}


	public void setScreenIconCls(String screenIconCls) {
		this.screenIconCls = screenIconCls;
	}


	public Integer getMaxQuerySizeLimit() {
		return this.maxQuerySizeLimit;
	}


	public void setMaxQuerySizeLimit(Integer maxQuerySizeLimit) {
		this.maxQuerySizeLimit = maxQuerySizeLimit;
	}


	public Boolean getMaxQuerySizeLimitNotNull() {
		return this.maxQuerySizeLimitNotNull;
	}


	public void setMaxQuerySizeLimitNotNull(Boolean maxQuerySizeLimitNotNull) {
		this.maxQuerySizeLimitNotNull = maxQuerySizeLimitNotNull;
	}
}
