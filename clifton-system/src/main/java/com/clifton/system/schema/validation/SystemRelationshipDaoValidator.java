package com.clifton.system.schema.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.SystemRelationship;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemRelationshipDaoValidator</code> validated Updates are allowed only
 * and the only modifiable field is IsExcludeFromUsedBy
 *
 * @author manderson
 */
@Component
public class SystemRelationshipDaoValidator extends SelfRegisteringDaoValidator<SystemRelationship> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemRelationship bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert() || config.isDelete()) {
			throw new ValidationException("Relationships cannot be inserted or deleted from the UI.");
		}

		SystemRelationship originalBean = getOriginalBean(bean);
		ValidationUtils.assertEquals(originalBean.getName(), bean.getName(), "FK Name field cannot be edited");
		ValidationUtils.assertEquals(originalBean.getColumn(), bean.getColumn(), "Columns cannot be changed for Relationships.");
		ValidationUtils.assertEquals(originalBean.getParentColumn(), bean.getParentColumn(), "Columns cannot be changed for Relationships.");
	}
}
