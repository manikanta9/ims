package com.clifton.system.schema;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.system.audit.SystemAuditType;
import com.clifton.system.schema.search.SystemDataSourceSearchForm;
import com.clifton.system.schema.search.SystemRelationshipSearchForm;
import com.clifton.system.schema.search.SystemTableSearchForm;

import java.util.List;


/**
 * The <code>SystemSchemaService</code> interface defines methods for working with the database schema.
 *
 * @author vgomelsky
 */
public interface SystemSchemaService {

	////////////////////////////////////////////////////////////////////////////
	////////          SystemDataType Business Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemDataType getSystemDataType(short id);


	public SystemDataType getSystemDataTypeByName(String name);


	public List<SystemDataType> getSystemDataTypeList();

	////////////////////////////////////////////////////////////////////////////
	////////         SystemDataSource Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemDataSource getSystemDataSource(short id);


	public SystemDataSource getSystemDataSourceDefault();


	public SystemDataSource getSystemDataSourceByName(String dataSourceName);


	@DoNotAddRequestMapping
	public List<SystemDataSource> getSystemDataSourceNotDefaultList();


	public List<SystemDataSource> getSystemDataSourceList();


	public List<SystemDataSource> getSystemDataSourceList(SystemDataSourceSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////            SystemTable Business Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemTable getSystemTable(short id);


	/**
	 * Performs lookup for table by name and default datasource.
	 * If more than one result is found during second global lookup, will throw an exception.
	 * Throws ValidationException if the table cannot be found.
	 * The result is cached for future retrievals
	 */
	public SystemTable getSystemTableByName(String tableName);


	public SystemTable getSystemTableByNameAndDataSource(String dataSourceName, String tableName);


	/**
	 * Returns any {@link SystemTable} with at least one of the Insert/Update/Delete
	 * EntityModifyCondition fields populated.
	 */
	public List<SystemTable> getSystemTableListWithEntityModifyCondition();


	public List<SystemTable> getSystemTableList(SystemTableSearchForm searchForm);


	/**
	 * Returns a List of {@link SystemTable} objects that either have {@link SystemAuditType} populated or
	 * have at least one column that has {@link SystemAuditType} populated.
	 * Each table will also have a list of columns that have {@link SystemAuditType} populated.
	 */
	public List<SystemTable> getSystemTableListAudited();


	public SystemTable saveSystemTable(SystemTable table);

	////////////////////////////////////////////////////////////////////////////
	////////        SystemRelationship Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemRelationship getSystemRelationship(int id);


	/**
	 * Returns a List of {@link SystemRelationship} objects for the specified table.
	 */
	public List<SystemRelationship> getSystemRelationshipList(SystemRelationshipSearchForm searchForm);


	public SystemRelationship saveSystemRelationship(SystemRelationship relationship);
}
