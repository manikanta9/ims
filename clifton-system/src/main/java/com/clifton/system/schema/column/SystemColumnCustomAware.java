package com.clifton.system.schema.column;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>SystemColumnCustomAware</code> should be extended by entities that utilize custom fields.  Applies to both JSON and standard SystemColumnValue types
 * Actual DTO objects should implement the appropriate sub-class based on what types of custom columns it supports. i.e. {@link SystemColumnCustomValueAware}
 *
 * @author Mary Anderson
 */
public interface SystemColumnCustomAware extends IdentityObject {

	/**
	 * Not persisted in the DB, but used during saves to determine if custom field values
	 * should be saved for a specific group for this entity.
	 */
	public void setColumnGroupName(String columnGroupName);


	public String getColumnGroupName();
}
