package com.clifton.system.schema.column.cache;

import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.system.schema.column.value.SystemColumnValueHolder;

import java.io.Serializable;


/**
 * The <code>SystemColumnValueCacheImpl</code> class provides caching and retrieval from cache of custom column value objects.
 * <p>
 * The class also implements {@link DaoEventObserver} and clears the cache whenever update methods to related DTO's happen.
 * Make sure to register this class as an observer to corresponding SystemColumnValue and SystemColumn (default values) DAO's.
 * <p>
 * Cache contains a map with:
 * <p>
 * key: TableName_ColumnGroupName_ColumnName
 * value: Map with key: ID -> ObjectWrapper<Object>
 * and "GLOBAL_DEFAULT" -> ObjectWrapper<Object>
 * <p>
 * When the any of the following fields are updated
 *
 * @author vgomelsky
 */
public interface SystemColumnValueCache {

	public ObjectWrapper<SystemColumnValueHolder> getSystemColumnValueGlobalDefault(String tableName, String columnGroupName, String columnName, String linkedValue);


	public ObjectWrapper<SystemColumnValueHolder> getSystemColumnValueForEntity(String tableName, Serializable entityId, String columnGroupName, String columnName);


	public void setSystemColumnValueForEntity(String tableName, Serializable entityId, String columnGroupName, String columnName, ObjectWrapper<SystemColumnValueHolder> value);


	public void setSystemColumnValueGlobalDefault(String tableName, String columnGroupName, String columnName, String linkedValue, ObjectWrapper<SystemColumnValueHolder> value);
}
