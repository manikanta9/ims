package com.clifton.system.schema;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.shared.dataaccess.DataTypeNames;


/**
 * The <code>SystemDataType</code> represents a DB data type.
 *
 * @author vgomelsky
 */
@CacheByName
public class SystemDataType extends NamedEntity<Short> {

	public static final String INTEGER = DataTypeNames.INTEGER.name();
	public static final String DECIMAL = DataTypeNames.DECIMAL.name();
	public static final String STRING = DataTypeNames.STRING.name();
	public static final String DATE = DataTypeNames.DATE.name();
	public static final String TIME = DataTypeNames.TIME.name();
	public static final String BOOLEAN = DataTypeNames.BOOLEAN.name();
	public static final String TEXT = DataTypeNames.TEXT.name();
	public static final String SECRET = DataTypeNames.SECRET.name();
	public static final String BINARY = DataTypeNames.BINARY.name();
	public static final String TABLE = DataTypeNames.TABLE.name();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private boolean numeric;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Convenience method to return the equivalent {@link DataTypeNames} for this data type.
	 * <p>
	 * The method name does not follow our standard getter syntax to avoid serialization.
	 */
	public DataTypeNames asDataTypeNames() {
		return DataTypeNames.valueOf(getName());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isNumeric() {
		return this.numeric;
	}


	public void setNumeric(boolean numeric) {
		this.numeric = numeric;
	}
}
