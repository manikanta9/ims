package com.clifton.system.schema.copy;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * @author vgomelsky
 */
@Service
public class SystemSchemaCopyServiceImpl implements SystemSchemaCopyService {

	private SystemSchemaService systemSchemaService;
	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public Status copySystemSchemaFields(SchemaFieldsCopyCommand command) {
		SystemDataSource fromDataSource = getSystemSchemaService().getSystemDataSourceByName(command.getFromDataSource());
		ValidationUtils.assertNotNull(fromDataSource, "Cannot find FROM data source with name: " + command.getFromDataSource());

		SystemDataSource toDataSource = getSystemSchemaService().getSystemDataSourceByName(command.getToDataSource());
		ValidationUtils.assertNotNull(toDataSource, "Cannot find TO data source with name: " + command.getToDataSource());

		ValidationUtils.assertNotEmpty(command.getTables(), "Required 'tables' property is missing.");
		Status status = command.getStatus() == null ? new Status() : command.getStatus();
		status.setMessage("Copying Schema Fields from '" + fromDataSource.getName() + "' to '" + toDataSource.getName() + "'");
		for (String tableName : command.getTables().split(",")) {
			SystemTable fromTable = getSystemSchemaService().getSystemTableByNameAndDataSource(fromDataSource.getName(), tableName);
			ValidationUtils.assertNotNull(fromTable, "Cannot find FROM table with name: " + tableName);

			SystemTable toTable = getSystemSchemaService().getSystemTableByNameAndDataSource(toDataSource.getName(), tableName);
			ValidationUtils.assertNotNull(toTable, "Cannot find TO table with name: " + tableName);

			if (copyTable(fromTable, toTable, command)) {
				status.addMessage("Copied fields for " + tableName);
			}
			else {
				status.addSkipped("Skipped fields for " + tableName);
			}
		}
		status.setMessageWithErrors("Completed Copying Schema Fields from '" + fromDataSource.getName() + "' to '" + toDataSource.getName() + "' for " + CollectionUtils.getSize(status.getDetailListForCategory(StatusDetail.CATEGORY_MESSAGE)) + " tables." + (status.getSkippedCount() != 0 ? " Skipped: " + status.getSkippedCount() : ""), 5);
		return status;
	}


	private boolean copyTable(SystemTable fromTable, SystemTable toTable, SchemaFieldsCopyCommand command) {
		boolean updated = false;

		if (command.isCopyTableLabelAndDescription()) {
			updated = BeanUtils.copyProperty(fromTable, toTable, SystemTable::getLabel, SystemTable::setLabel, command.isOverwriteExistingValues());
			updated = updated || BeanUtils.copyProperty(fromTable, toTable, SystemTable::getDescription, SystemTable::setDescription, command.isOverwriteExistingValues());
		}
		if (command.isCopyAuditType()) {
			updated = updated || BeanUtils.copyProperty(fromTable, toTable, SystemTable::getAuditType, SystemTable::setAuditType, command.isOverwriteExistingValues());
		}
		if (command.isCopySecurityResource()) {
			updated = updated || BeanUtils.copyProperty(fromTable, toTable, SystemTable::getSecurityResource, SystemTable::setSecurityResource, command.isOverwriteExistingValues());
		}
		if (command.isCopyUserInterfaceConfiguration()) {
			updated = updated || BeanUtils.copyProperty(fromTable, toTable, SystemTable::getDetailScreenClass, SystemTable::setDetailScreenClass, command.isOverwriteExistingValues());
			updated = updated || BeanUtils.copyProperty(fromTable, toTable, SystemTable::getListScreenClass, SystemTable::setListScreenClass, command.isOverwriteExistingValues());
			updated = updated || BeanUtils.copyProperty(fromTable, toTable, SystemTable::getScreenIconCls, SystemTable::setScreenIconCls, command.isOverwriteExistingValues());
			updated = updated || BeanUtils.copyProperty(fromTable, toTable, SystemTable::getEntityListUrl, SystemTable::setEntityListUrl, command.isOverwriteExistingValues());
		}
		if (command.isCopyColumnInformation()) {
			List<SystemColumnStandard> fromColumnList = getSystemColumnService().getSystemColumnStandardListByTable(fromTable.getId());
			Map<String, SystemColumnStandard> fromColumnMap = BeanUtils.getBeanMap(fromColumnList, SystemColumnStandard::getName);
			List<SystemColumnStandard> toColumnList = getSystemColumnService().getSystemColumnStandardListByTable(toTable.getId());
			for (SystemColumnStandard toColumn : CollectionUtils.getIterable(toColumnList)) {
				SystemColumnStandard fromColumn = fromColumnMap.get(toColumn.getName());
				if (fromColumn != null) {
					boolean updatedColumn = BeanUtils.copyProperty(fromColumn, toColumn, SystemColumn::getLabel, SystemColumn::setLabel, command.isOverwriteExistingValues());
					updatedColumn = updatedColumn || BeanUtils.copyProperty(fromColumn, toColumn, SystemColumn::getDescription, SystemColumn::setDescription, command.isOverwriteExistingValues());
					updatedColumn = updatedColumn || BeanUtils.copyProperty(fromColumn, toColumn, SystemColumn::getAuditType, SystemColumn::setAuditType, command.isOverwriteExistingValues());
					updatedColumn = updatedColumn || BeanUtils.copyProperty(fromColumn, toColumn, SystemColumn::getValueListUrl, SystemColumn::setValueListUrl, command.isOverwriteExistingValues());
					if (updatedColumn) {
						getSystemColumnService().saveSystemColumnStandard(toColumn);
						updated = true;
					}
				}
			}
		}

		if (updated) {
			getSystemSchemaService().saveSystemTable(toTable);
		}
		return updated;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}
}
