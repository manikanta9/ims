package com.clifton.system.schema.column;


import com.clifton.core.util.StringUtils;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.userinterface.SystemUserInterfaceAware;
import com.clifton.system.userinterface.SystemUserInterfaceUsedByAware;

import javax.persistence.Table;


/**
 * The <code>SystemColumnCustom</code> ...
 * <p>
 * NOTE: Because of the special dynamic relationships for columns, Used By was implemented separately for custom columns
 * although could be supported by the {@link SystemUserInterfaceUsedByAware} interface in the future if needed
 *
 * @author manderson
 */
@Table(name = "SystemColumn")
public class SystemColumnCustom extends SystemColumn implements SystemUserInterfaceAware {

	/**
	 * The group this custom column belongs to.
	 */
	private SystemColumnGroup columnGroup;

	/**
	 * If the group has a linkedBeanProperty defined, and this is set, this column
	 * applies to a subset of entities where the linked property = this value.
	 * <p>
	 * Most cases would probably be an id field, i.e. Applies to Securities of a specific instrument.hierarchy.type.id
	 * Could also be a boolean, i.e. Applies to accounts with accountType.ourAccount = true
	 */
	private String linkedValue;
	// User friendly version of linkedValue, i.e. Clifton Account instead of just "true"
	private String linkedLabel;

	/**
	 * Value can only be entered if dependedSystemField is defined.
	 * In UI, puts the dependedSystemField in the requiredFields for this field.
	 */
	private SystemColumnCustom dependedColumn;

	/**
	 * SystemUserInterfaceAware Properties
	 */

	private SystemTable valueTable;
	private int order;
	private boolean required;

	/**
	 * For detail windows, can be used to customize the display of the field.
	 */
	private String userInterfaceConfig;

	/**
	 * For JSON Custom columns that support visibility in the grids, in some cases we need to apply ui config to the filters
	 * For Example, a combo box where we need to override displayField, or using xtype: 'system-list-combo'
	 */
	private String gridFilterUserInterfaceConfig;

	/**
	 * Default value in UI for new entities
	 */
	private String defaultValue;
	private String defaultText;

	/**
	 * Value used if an entity doesn't have a value defined.
	 */
	private String globalDefaultValue;
	private String globalDefaultText;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isCustomColumn() {
		return true;
	}


	@Override
	public String getCustomLabel() {
		return getName() + " (" + getLinkedLabel() + ")";
	}


	/**
	 * Returns true if linkedValue field is not populated: applies to all rows.
	 */
	public boolean isLinkedToAllRows() {
		return this.linkedValue == null;
	}


	public boolean isLabelUsed() {
		return StringUtils.compare(getName(), getLabel()) != 0;
	}


	public boolean isDefaultUsed() {
		return !StringUtils.isEmpty(StringUtils.coalesce(false, getDefaultValue(), getDefaultText(), getGlobalDefaultValue(), getGlobalDefaultText()));
	}


	public String getGroupLinkedLabel() {
		String label = "";
		if (getColumnGroup() != null) {
			label = getColumnGroup().getName();
			if (!StringUtils.isEmpty(getLinkedLabel())) {
				label += " (Link: " + getLinkedLabel() + ")";
			}
		}
		return label;
	}


	public String getLabelOverride() {
		if (isLabelUsed()) {
			return getLabel();
		}
		return null;
	}


	public void setLabelOverride(String labelOverride) {
		if (!StringUtils.isEmpty(labelOverride)) {
			setLabel(labelOverride);
		}
		else {
			setLabel(getName());
		}
	}


	////////////////////////////////////////////////////////////////////////////


	public SystemColumnGroup getColumnGroup() {
		return this.columnGroup;
	}


	public void setColumnGroup(SystemColumnGroup columnGroup) {
		this.columnGroup = columnGroup;
	}


	public String getLinkedValue() {
		return this.linkedValue;
	}


	public void setLinkedValue(String linkedValue) {
		this.linkedValue = linkedValue;
	}


	public String getLinkedLabel() {
		return this.linkedLabel;
	}


	public void setLinkedLabel(String linkedLabel) {
		this.linkedLabel = linkedLabel;
	}


	public SystemColumnCustom getDependedColumn() {
		return this.dependedColumn;
	}


	public void setDependedColumn(SystemColumnCustom dependedColumn) {
		this.dependedColumn = dependedColumn;
	}


	@Override
	public SystemTable getValueTable() {
		return this.valueTable;
	}


	public void setValueTable(SystemTable valueTable) {
		this.valueTable = valueTable;
	}


	@Override
	public String getUserInterfaceConfig() {
		return this.userInterfaceConfig;
	}


	public void setUserInterfaceConfig(String userInterfaceConfig) {
		this.userInterfaceConfig = userInterfaceConfig;
	}


	public String getGridFilterUserInterfaceConfig() {
		return this.gridFilterUserInterfaceConfig;
	}


	public void setGridFilterUserInterfaceConfig(String gridFilterUserInterfaceConfig) {
		this.gridFilterUserInterfaceConfig = gridFilterUserInterfaceConfig;
	}


	@Override
	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	@Override
	public boolean isRequired() {
		return this.required;
	}


	public void setRequired(boolean required) {
		this.required = required;
	}


	public String getDefaultValue() {
		return this.defaultValue;
	}


	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}


	public String getDefaultText() {
		return this.defaultText;
	}


	public void setDefaultText(String defaultText) {
		this.defaultText = defaultText;
	}


	public String getGlobalDefaultValue() {
		return this.globalDefaultValue;
	}


	public void setGlobalDefaultValue(String globalDefaultValue) {
		this.globalDefaultValue = globalDefaultValue;
	}


	public String getGlobalDefaultText() {
		return this.globalDefaultText;
	}


	public void setGlobalDefaultText(String globalDefaultText) {
		this.globalDefaultText = globalDefaultText;
	}
}
