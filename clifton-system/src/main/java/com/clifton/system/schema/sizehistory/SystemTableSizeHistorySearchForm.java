package com.clifton.system.schema.sizehistory;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * @author davidi
 */
public class SystemTableSizeHistorySearchForm extends BaseEntitySearchForm {

	@SearchField(searchFieldPath = "systemTable", searchField = "name,dataSource.databaseName")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField(searchField = "systemTable.id")
	private Short systemTableId;

	@SearchField(searchFieldPath = "systemTable", searchField = "name")
	private String systemTableName;

	@SearchField(searchFieldPath = "systemTable.dataSource", searchField = "name")
	private String dataSourceName;

	@SearchField(searchFieldPath = "systemTable.dataSource", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String dataSourceNameEquals;

	@SearchField(searchFieldPath = "systemTable.dataSource", searchField = "databaseName")
	private String databaseName;

	@SearchField
	private Date snapshotDate;

	@SearchField(searchField = "snapshotDate", comparisonConditions = {ComparisonConditions.GREATER_THAN_OR_EQUALS})
	private Date minSnapshotDate;

	@SearchField
	private Long numberOfRows;

	@SearchField
	private Long dataSizeInBytes;

	@SearchField
	private Long indexSizeInBytes;

	@SearchField
	private Long unusedSizeInBytes;

	@SearchField(searchField = "dataSizeInBytes,indexSizeInBytes,unusedSizeInBytes", searchFieldCustomType = SearchFieldCustomTypes.MATH_ADD)
	private Long totalSizeInBytes;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getSystemTableId() {
		return this.systemTableId;
	}


	public void setSystemTableId(Short systemTableId) {
		this.systemTableId = systemTableId;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSystemTableName() {
		return this.systemTableName;
	}


	public void setSystemTableName(String tableName) {
		this.systemTableName = tableName;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public String getDataSourceNameEquals() {
		return this.dataSourceNameEquals;
	}


	public void setDataSourceNameEquals(String dataSourceNameEquals) {
		this.dataSourceNameEquals = dataSourceNameEquals;
	}


	public String getDatabaseName() {
		return this.databaseName;
	}


	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date date) {
		this.snapshotDate = date;
	}


	public Date getMinSnapshotDate() {
		return this.minSnapshotDate;
	}


	public void setMinSnapshotDate(Date minSnapshotDate) {
		this.minSnapshotDate = minSnapshotDate;
	}


	public Long getNumberOfRows() {
		return this.numberOfRows;
	}


	public void setNumberOfRows(Long numberOfRows) {
		this.numberOfRows = numberOfRows;
	}


	public Long getDataSizeInBytes() {
		return this.dataSizeInBytes;
	}


	public void setDataSizeInBytes(Long dataSizeInBytes) {
		this.dataSizeInBytes = dataSizeInBytes;
	}


	public Long getIndexSizeInBytes() {
		return this.indexSizeInBytes;
	}


	public void setIndexSizeInBytes(Long indexSizeInBytes) {
		this.indexSizeInBytes = indexSizeInBytes;
	}


	public Long getUnusedSizeInBytes() {
		return this.unusedSizeInBytes;
	}


	public void setUnusedSizeInBytes(Long unusedSizeInBytes) {
		this.unusedSizeInBytes = unusedSizeInBytes;
	}


	public Long getTotalSizeInBytes() {
		return this.totalSizeInBytes;
	}


	public void setTotalSizeInBytes(Long totalSizeInBytes) {
		this.totalSizeInBytes = totalSizeInBytes;
	}
}
