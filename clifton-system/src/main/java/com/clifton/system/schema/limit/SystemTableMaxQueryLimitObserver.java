package com.clifton.system.schema.limit;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.schema.SystemTable;
import org.springframework.stereotype.Component;

import java.text.NumberFormat;


/**
 * <code>SystemTableMaxQueryLimitObserver</code> Validates the {@link SystemTable#maxQuerySizeLimit} is greater than
 * zero and updates the new query size on the associated {@link com.clifton.core.dataaccess.dao.config.DAOConfiguration}
 *
 * @author TerryS
 */
@Component
public class SystemTableMaxQueryLimitObserver<T extends IdentityObject> extends SelfRegisteringDaoValidator<SystemTable> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<SystemTable> dao, DaoEventTypes event, SystemTable bean, @SuppressWarnings("unused") Throwable e) {
		// do not allow overrides from external system tables
		if (bean.getDataSource().isDefaultDataSource()) {
			// null will reset to system default.
			int maxQueryLimit = bean.getMaxQuerySizeLimit() == null ? PagingCommand.MAX_LIMIT : bean.getMaxQuerySizeLimit();
			ReadOnlyDAO<T> tableDao = getDaoLocator().locate(bean.getName());
			tableDao.getConfiguration().setMaxQuerySizeLimit(maxQueryLimit);
		}
	}


	@Override
	public void validate(SystemTable bean, DaoEventTypes event) throws ValidationException {
		if (bean.getMaxQuerySizeLimit() != null && !MathUtils.isBetween(bean.getMaxQuerySizeLimit(), 1, PagingCommand.MAX_LIMIT - 1)) {
			throw new ValidationException("The Maximum Query Limit Must be greater than zero and less than " + NumberFormat.getIntegerInstance().format(PagingCommand.MAX_LIMIT));
		}
	}
}
