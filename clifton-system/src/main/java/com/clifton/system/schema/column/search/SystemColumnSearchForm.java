package com.clifton.system.schema.column.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.system.schema.column.CustomColumnTypes;


public class SystemColumnSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,label,description")
	private String searchPattern;

	@SearchField(searchField = "name,label,description,linkedValue,linkedLabel")
	private String searchPatternCustomColumn;

	@SearchField(comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.LIKE})
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.NOT_IN)
	private String[] excludeNames;

	@SearchField
	private String label;

	@SearchField
	private String description;

	@SearchField(searchField = "table.id")
	private Short tableId;

	@SearchField(searchFieldPath = "table", searchField = "name", comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.LIKE})
	private String tableName;

	@SearchField(searchField = "auditType.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean auditOnly;

	@SearchField(searchField = "auditType.id")
	private Short auditTypeId;

	@SearchField(searchField = "name", searchFieldPath = "auditType")
	private String auditTypeName;

	@SearchField(searchFieldPath = "table", searchField = "dataSource.id")
	private Short dataSourceId;

	@SearchField(searchFieldPath = "table.dataSource", searchField = "name")
	private String dataSourceName;

	@SearchField(searchFieldPath = "table.dataSource", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String dataSourceNameEquals;

	@SearchField(searchFieldPath = "table.dataSource", searchField = "defaultDataSource")
	private Boolean defaultDataSource;

	@SearchField(searchField = "columnGroup.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean standardOnly;

	@SearchField(searchField = "beanPropertyName", comparisonConditions = ComparisonConditions.EQUALS)
	private String beanPropertyNameEquals;

	@SearchField(searchField = "beanPropertyName", comparisonConditions = ComparisonConditions.NOT_IN)
	private String[] excludeBeanPropertyNames;

	@SearchField(searchField = "dataType.id")
	private Short dataTypeId;

	@SearchField(searchField = "name", searchFieldPath = "dataType")
	private String dataTypeName;

	@SearchField
	private Integer order;

	@SearchField
	private Boolean required;

	@SearchField
	private Boolean systemDefined;

	@SearchField(searchField = "columnGroup.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean customColumn;

	/**
	 * When populated this will <b>include</b> all custom columns for given type AND all standard columns.  So, to get
	 * only custom columns you must specify customOnly = true. (Unless EQUALS search is explicitly selected - like from UI)
	 */
	@SearchField(searchFieldPath = "columnGroup", searchField = "customColumnType", leftJoin = true, comparisonConditions = {ComparisonConditions.EQUALS_OR_IS_NULL, ComparisonConditions.EQUALS})
	private CustomColumnTypes customColumnType;

	/**
	 * When populated this will <b>exclude</b> all custom columns for given type
	 * Standard Columns will be included by default, so to get only custom columns you must specify customOnly = true.
	 */
	@SearchField(searchFieldPath = "columnGroup", searchField = "customColumnType", leftJoin = true, comparisonConditions = ComparisonConditions.NOT_EQUALS_OR_IS_NULL)
	private CustomColumnTypes excludeCustomColumnType;

	@SearchField(searchField = "valueTable.id")
	private Short valueTableId;

	@SearchField(searchField = "name", searchFieldPath = "valueTable")
	private String valueTableName;

////////////////////////////////////////////////////////////////////////////

	@SearchField(searchField = "columnGroup.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean customOnly;

	@SearchField(searchField = "columnGroup.id")
	private Short columnGroupId;

	@SearchField(searchFieldPath = "columnGroup", searchField = "name")
	private String columnGroupName;

	@SearchField(searchField = "linkedValue", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean linkedToAllRows;

	// Custom Search Fields
	private String linkedValue;

	@SearchField(searchField = "linkedValue", comparisonConditions = ComparisonConditions.IN)
	private String[] linkedValues;

	// include null can be used when a linked value is supplied, but
// also want all fields that have no linked value.
	private Boolean includeNullLinkedValue;

	// Custom Filter that will exclude by name CreateUserID, CreateDate, UpdateUserID, UpdateDate, and rv
	private Boolean excludeRecordStampColumns;

	@SearchField(searchField = "template.id")
	private Integer templateId;

	@SearchField(searchFieldPath = "template", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String templateName;

	@SearchField(searchField = "template", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean templateAssigned;

	@SearchField
	private String userInterfaceConfig;

	@SearchField
	private String gridFilterUserInterfaceConfig;

	@SearchField(searchFieldPath = "dependedColumn", searchField = "name")
	private String dependedColumnName;

	@SearchField
	private String valueListUrl;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getSearchPatternCustomColumn() {
		return this.searchPatternCustomColumn;
	}


	public void setSearchPatternCustomColumn(String searchPatternCustomColumn) {
		this.searchPatternCustomColumn = searchPatternCustomColumn;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String[] getExcludeNames() {
		return this.excludeNames;
	}


	public void setExcludeNames(String[] excludeNames) {
		this.excludeNames = excludeNames;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Boolean getAuditOnly() {
		return this.auditOnly;
	}


	public void setAuditOnly(Boolean auditOnly) {
		this.auditOnly = auditOnly;
	}


	public Short getAuditTypeId() {
		return this.auditTypeId;
	}


	public void setAuditTypeId(Short auditTypeId) {
		this.auditTypeId = auditTypeId;
	}


	public String getAuditTypeName() {
		return this.auditTypeName;
	}


	public void setAuditTypeName(String auditTypeName) {
		this.auditTypeName = auditTypeName;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public String getDataSourceNameEquals() {
		return this.dataSourceNameEquals;
	}


	public void setDataSourceNameEquals(String dataSourceNameEquals) {
		this.dataSourceNameEquals = dataSourceNameEquals;
	}


	public Boolean getDefaultDataSource() {
		return this.defaultDataSource;
	}


	public void setDefaultDataSource(Boolean defaultDataSource) {
		this.defaultDataSource = defaultDataSource;
	}


	public Boolean getStandardOnly() {
		return this.standardOnly;
	}


	public void setStandardOnly(Boolean standardOnly) {
		this.standardOnly = standardOnly;
	}


	public String getBeanPropertyNameEquals() {
		return this.beanPropertyNameEquals;
	}


	public void setBeanPropertyNameEquals(String beanPropertyNameEquals) {
		this.beanPropertyNameEquals = beanPropertyNameEquals;
	}


	public String[] getExcludeBeanPropertyNames() {
		return this.excludeBeanPropertyNames;
	}


	public void setExcludeBeanPropertyNames(String[] excludeBeanPropertyNames) {
		this.excludeBeanPropertyNames = excludeBeanPropertyNames;
	}


	public Short getDataTypeId() {
		return this.dataTypeId;
	}


	public void setDataTypeId(Short dataTypeId) {
		this.dataTypeId = dataTypeId;
	}


	public String getDataTypeName() {
		return this.dataTypeName;
	}


	public void setDataTypeName(String dataTypeName) {
		this.dataTypeName = dataTypeName;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Boolean getRequired() {
		return this.required;
	}


	public void setRequired(Boolean required) {
		this.required = required;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getCustomColumn() {
		return this.customColumn;
	}


	public void setCustomColumn(Boolean customColumn) {
		this.customColumn = customColumn;
	}


	public CustomColumnTypes getCustomColumnType() {
		return this.customColumnType;
	}


	public void setCustomColumnType(CustomColumnTypes customColumnType) {
		this.customColumnType = customColumnType;
	}


	public CustomColumnTypes getExcludeCustomColumnType() {
		return this.excludeCustomColumnType;
	}


	public void setExcludeCustomColumnType(CustomColumnTypes excludeCustomColumnType) {
		this.excludeCustomColumnType = excludeCustomColumnType;
	}


	public Short getValueTableId() {
		return this.valueTableId;
	}


	public void setValueTableId(Short valueTableId) {
		this.valueTableId = valueTableId;
	}


	public String getValueTableName() {
		return this.valueTableName;
	}


	public void setValueTableName(String valueTableName) {
		this.valueTableName = valueTableName;
	}


	public Boolean getCustomOnly() {
		return this.customOnly;
	}


	public void setCustomOnly(Boolean customOnly) {
		this.customOnly = customOnly;
	}


	public Short getColumnGroupId() {
		return this.columnGroupId;
	}


	public void setColumnGroupId(Short columnGroupId) {
		this.columnGroupId = columnGroupId;
	}


	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public Boolean getLinkedToAllRows() {
		return this.linkedToAllRows;
	}


	public void setLinkedToAllRows(Boolean linkedToAllRows) {
		this.linkedToAllRows = linkedToAllRows;
	}


	public String getLinkedValue() {
		return this.linkedValue;
	}


	public void setLinkedValue(String linkedValue) {
		this.linkedValue = linkedValue;
	}


	public String[] getLinkedValues() {
		return this.linkedValues;
	}


	public void setLinkedValues(String[] linkedValues) {
		this.linkedValues = linkedValues;
	}


	public Boolean getIncludeNullLinkedValue() {
		return this.includeNullLinkedValue;
	}


	public void setIncludeNullLinkedValue(Boolean includeNullLinkedValue) {
		this.includeNullLinkedValue = includeNullLinkedValue;
	}


	public Boolean getExcludeRecordStampColumns() {
		return this.excludeRecordStampColumns;
	}


	public void setExcludeRecordStampColumns(Boolean excludeRecordStampColumns) {
		this.excludeRecordStampColumns = excludeRecordStampColumns;
	}


	public Integer getTemplateId() {
		return this.templateId;
	}


	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}


	public String getTemplateName() {
		return this.templateName;
	}


	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}


	public Boolean getTemplateAssigned() {
		return this.templateAssigned;
	}


	public void setTemplateAssigned(Boolean templateAssigned) {
		this.templateAssigned = templateAssigned;
	}


	public String getUserInterfaceConfig() {
		return this.userInterfaceConfig;
	}


	public void setUserInterfaceConfig(String userInterfaceConfig) {
		this.userInterfaceConfig = userInterfaceConfig;
	}


	public String getGridFilterUserInterfaceConfig() {
		return this.gridFilterUserInterfaceConfig;
	}


	public void setGridFilterUserInterfaceConfig(String gridFilterUserInterfaceConfig) {
		this.gridFilterUserInterfaceConfig = gridFilterUserInterfaceConfig;
	}


	public String getDependedColumnName() {
		return this.dependedColumnName;
	}


	public void setDependedColumnName(String dependedColumnName) {
		this.dependedColumnName = dependedColumnName;
	}


	public String getValueListUrl() {
		return this.valueListUrl;
	}


	public void setValueListUrl(String valueListUrl) {
		this.valueListUrl = valueListUrl;
	}
}
