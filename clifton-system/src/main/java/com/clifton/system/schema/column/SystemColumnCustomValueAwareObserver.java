package com.clifton.system.schema.column;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemColumnCustomAwareObserver</code> class should be registered for MODIFY events of all SystemColumnCustomAware DAO's
 * It deletes custom columns on corresponding entity delete, updates custom columns and validates their values before updates.
 *
 * @author Mary Anderson
 */
@Component
public class SystemColumnCustomValueAwareObserver<E extends SystemColumnCustomValueAware> extends BaseDaoEventObserver<E> {

	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void configurePersistedInstance(E persistedInstance, E originalInstance) {
		// if there was an error, then persistedInstance is null
		if (persistedInstance != null) {
			// keep custom value properties on persisted instance so that they can be saved in the after method
			persistedInstance.setColumnGroupName(originalInstance.getColumnGroupName());
			persistedInstance.setColumnValueList(originalInstance.getColumnValueList());
		}
	}


	@Override
	protected void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<E> dao, DaoEventTypes event, E bean, Throwable e) {
		// no need to update if there was an exception
		if (e != null) {
			return;
		}

		if (event.isInsert() || event.isUpdate()) {
			getSystemColumnValueHandler().saveSystemColumnValueListForEntity(bean);
		}
		else if (event.isDelete()) {
			getSystemColumnValueHandler().deleteSystemColumnValueListForEntity(bean);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
