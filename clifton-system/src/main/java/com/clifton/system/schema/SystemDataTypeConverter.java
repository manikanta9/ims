package com.clifton.system.schema;


import com.clifton.core.json.custom.CustomJsonObjectList;
import com.clifton.core.json.custom.util.CustomJsonObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.security.secret.SecuritySecret;

import java.math.BigDecimal;


/**
 * The <code>SystemDataTypeConverter</code> handles converting a String object
 * to an object associated with the given {@link SystemDataType}
 *
 * @author manderson
 */
public class SystemDataTypeConverter implements Converter<String, Object> {

	private final SystemDataType systemDataType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemDataTypeConverter(SystemDataType systemDataType) {
		this.systemDataType = systemDataType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Object convert(String from) {
		if (from == null) {
			return null;
		}

		if (isString()) {
			return from;
		}

		if (isNumber()) {
			// Remove formatting
			from = from.replaceAll(",", "");
			if (SystemDataType.INTEGER.equals(this.systemDataType.getName())) {
				// Replace cases with decimal and all trailing zeros - can still convert to a decimal
				// Occurs from Excel imports - excel value is 0.0 but we can convert that to integer value of 0
				if (from.contains(".")) {
					from = from.replaceAll("\\.0*$", "");
				}
				return new Integer(from);
			}
			// Not an Integer, then it's a Decimal
			return BigDecimal.valueOf(new Double(from));
		}

		if (SystemDataType.DATE.equals(this.systemDataType.getName())) {
			return DateUtils.toDate(from);
		}

		if (SystemDataType.TIME.equals(this.systemDataType.getName())) {
			return TimeUtils.toTime(from);
		}

		if (SystemDataType.BOOLEAN.equals(this.systemDataType.getName())) {
			if ("true".equalsIgnoreCase(from) || "yes".equalsIgnoreCase(from) || "1".equals(from) || "Y".equals(from)) {
				return Boolean.TRUE;
			}
			if ("false".equalsIgnoreCase(from) || "no".equalsIgnoreCase(from) || "0".equals(from) || "N".equals(from)) {
				return Boolean.FALSE;
			}
			throw new RuntimeException("Invalid 'BOOLEAN' value = " + from);
		}

		if (SystemDataType.SECRET.equals(this.systemDataType.getName())) {
			SecuritySecret secret = new SecuritySecret();
			secret.setId(new Integer(from));
			return secret;
		}

		if (SystemDataType.TABLE.equals(this.systemDataType.getName())) {
			if (!StringUtils.isEmpty(from)) {
				CustomJsonObjectList jsonList = new CustomJsonObjectList(from);
				CustomJsonObjectUtils.initializeObjectFromJson(jsonList);
				return jsonList;
			}
		}

		throw new RuntimeException("Conversion for System Data Type with Name [" + this.systemDataType.getName() + "] is not supported.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isString() {
		return SystemDataType.STRING.equals(this.systemDataType.getName()) || SystemDataType.TEXT.equals(this.systemDataType.getName());
	}


	public boolean isNumber() {
		return SystemDataType.INTEGER.equals(this.systemDataType.getName()) || SystemDataType.DECIMAL.equals(this.systemDataType.getName());
	}
}
