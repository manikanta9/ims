package com.clifton.system.schema.column.comparison;


/**
 * The <code>SystemColumnCustomAreFieldsNotModifiedComparison</code> takes an unsaved updated bean and compares it to the
 * original bean retrieved from the database.
 * <p>
 * NOTE: Custom Columns are optionally included in comparison.
 * <p>
 * Returns TRUE if ALL of the given fields have NOT been modified.
 * Returns FALSE if ANY of the given fields have been modified.
 * NOTE: System Managed Fields (Record Stamp) are always excluded from comparison
 *
 * @author manderson
 */
public class SystemColumnCustomAreFieldsNotModifiedComparison extends SystemColumnCustomAreFieldsModifiedComparison {


	@Override
	protected boolean isReverse() {
		return true;
	}
}
