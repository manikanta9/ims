package com.clifton.system.schema.column;


import com.clifton.system.schema.column.value.SystemColumnValue;

import java.util.List;


/**
 * The <code>SystemColumnCustomConfig</code> container object that just holds
 * custom columns and values for a specific entity & group
 *
 * @author Mary Anderson
 */
public class SystemColumnCustomConfig {

	private String columnGroupName;
	private Integer entityId;
	private String linkedValue;

	private List<SystemColumnCustom> columnList;


	// If using CustomColumnType.COLUMN_VALUE
	private List<SystemColumnValue> columnValueList;

	// If using CustomColumnType.COLUMN_JSON_VALUE
	private String jsonValueBeanPropertyName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setId(Integer id) {
		setEntityId(id);
	}


	public Integer getId() {
		return getEntityId();
	}


	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public Integer getEntityId() {
		return this.entityId;
	}


	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}


	public String getLinkedValue() {
		return this.linkedValue;
	}


	public void setLinkedValue(String linkedValue) {
		this.linkedValue = linkedValue;
	}


	public List<SystemColumnCustom> getColumnList() {
		return this.columnList;
	}


	public void setColumnList(List<SystemColumnCustom> columnList) {
		this.columnList = columnList;
	}


	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	public String getJsonValueBeanPropertyName() {
		return this.jsonValueBeanPropertyName;
	}


	public void setJsonValueBeanPropertyName(String jsonValueBeanPropertyName) {
		this.jsonValueBeanPropertyName = jsonValueBeanPropertyName;
	}
}
