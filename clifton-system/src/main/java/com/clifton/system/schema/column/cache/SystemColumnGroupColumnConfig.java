package com.clifton.system.schema.column.cache;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.schema.column.CustomColumnTypes;
import com.clifton.system.schema.column.SystemColumnCustom;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>SystemColumnGroupColumnConfig</code> is used to easily find for a specific column group and column if it applies to the given entity
 * This is used to prevent unnecessarily trying to lookup custom column value for an entity that it doesn't apply to.
 * <p>
 * For example: Some Securities use Index Ratio for various calculations.  When performing calculations on securities, we look up the column value, but if
 * the column doesn't apply to that security, then we don't want to waste extra look ups for a field that doesn't apply.
 *
 * @author manderson
 */
public class SystemColumnGroupColumnConfig {

	private final String groupName;

	private final CustomColumnTypes customColumnType;

	private final String jsonValueBeanProperty;

	private String linkedBeanProperty;

	private Map<String, Set<String>> columnNameLinkedValueMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnGroupColumnConfig(String groupName, CustomColumnTypes customColumnType, String jsonValueBeanProperty, List<SystemColumnCustom> customColumnList) {
		this.groupName = groupName;
		this.customColumnType = customColumnType;
		this.jsonValueBeanProperty = jsonValueBeanProperty;
		this.columnNameLinkedValueMap = new ConcurrentHashMap<>();

		boolean global = false;
		for (int i = 0; i < CollectionUtils.getSize(customColumnList); i++) {
			SystemColumnCustom c = customColumnList.get(i);
			if (i == 0) {
				this.linkedBeanProperty = c.getColumnGroup().getLinkedBeanProperty();
				global = StringUtils.isEmpty(this.linkedBeanProperty);
			}
			if (global) {
				// Concurrent Hash Maps do not allow null values
				this.columnNameLinkedValueMap.put(c.getName(), new HashSet<>());
			}
			else {
				if (!this.columnNameLinkedValueMap.containsKey(c.getName())) {
					this.columnNameLinkedValueMap.put(c.getName(), CollectionUtils.newConcurrentHashSet());
				}
				this.columnNameLinkedValueMap.get(c.getName()).add(c.getLinkedValue());
			}
		}
	}


	/////////////////////////////////////////////////////////////////////


	public boolean isGlobalColumnGroup() {
		return StringUtils.isEmpty(getLinkedBeanProperty());
	}


	public boolean isColumnNameApplyToLinkedValue(String columnName, Object entity) {
		// Global Column Group - As long as column name is in the map, OK
		if (isGlobalColumnGroup()) {
			return this.columnNameLinkedValueMap.containsKey(columnName);
		}
		Set<String> values = this.columnNameLinkedValueMap.get(columnName);
		String valueStr = getEntityLinkedValue(entity);
		// Prevent Null Pointer: Values cannot contain null, so if the value is null = return false as it doesn't apply
		if (valueStr == null) {
			return false;
		}
		return (!CollectionUtils.isEmpty(values) && values.contains(valueStr));
	}


	public String getEntityLinkedValue(Object entity) {
		if (isGlobalColumnGroup()) {
			return null;
		}
		Object entityValue = BeanUtils.getPropertyValue(entity, getLinkedBeanProperty());
		return (entityValue != null ? entityValue.toString() : null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getGroupName() {
		return this.groupName;
	}


	public CustomColumnTypes getCustomColumnType() {
		return this.customColumnType;
	}


	public String getJsonValueBeanProperty() {
		return this.jsonValueBeanProperty;
	}


	public String getLinkedBeanProperty() {
		return this.linkedBeanProperty;
	}


	public void setLinkedBeanProperty(String linkedBeanProperty) {
		this.linkedBeanProperty = linkedBeanProperty;
	}


	public Map<String, Set<String>> getColumnNameLinkedValueMap() {
		return this.columnNameLinkedValueMap;
	}


	public void setColumnNameLinkedValueMap(Map<String, Set<String>> columnNameLinkedValueMap) {
		this.columnNameLinkedValueMap = columnNameLinkedValueMap;
	}
}
