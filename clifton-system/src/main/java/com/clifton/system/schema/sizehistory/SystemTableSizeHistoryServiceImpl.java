package com.clifton.system.schema.sizehistory;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author davidi
 */
@Service
public class SystemTableSizeHistoryServiceImpl implements SystemTableSizeHistoryService {

	private AdvancedUpdatableDAO<SystemTableSizeHistory, Criteria> systemTableSizeHistoryDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemTableSizeHistory getSystemTableSizeHistory(Integer id) {
		return getSystemTableSizeHistoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemTableSizeHistory> getSystemTableSizeHistoryList(SystemTableSizeHistorySearchForm searchForm) {
		return getSystemTableSizeHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SystemTableSizeHistory saveSystemTableSizeHistory(SystemTableSizeHistory systemTableSizeHistory) {
		if (systemTableSizeHistory == null) {
			return systemTableSizeHistory;
		}

		SystemTableSizeHistory existingEntry = findExistingEntry(systemTableSizeHistory);
		if (existingEntry != null) {
			BeanUtils.copyProperties(systemTableSizeHistory, existingEntry, new String[]{"id"});
			systemTableSizeHistory = existingEntry;
		}

		return getSystemTableSizeHistoryDAO().save(systemTableSizeHistory);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks to determine if an entry with the same SystemTable ID and SnapshotDate already exists in the
	 * SystemTableSizeHistory table.  Returns the entity if it exists, otherwise returns null.
	 */
	private SystemTableSizeHistory findExistingEntry(SystemTableSizeHistory systemTableSizeHistory) {
		if (systemTableSizeHistory == null) {
			return null;
		}

		SystemTableSizeHistorySearchForm searchForm = new SystemTableSizeHistorySearchForm();
		searchForm.setSystemTableId(systemTableSizeHistory.getSystemTable().getId());
		searchForm.setSnapshotDate(systemTableSizeHistory.getSnapshotDate());

		List<SystemTableSizeHistory> foundEntries = getSystemTableSizeHistoryList(searchForm);

		return CollectionUtils.getFirstElement(foundEntries);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setSystemTableSizeHistoryDAO(AdvancedUpdatableDAO<SystemTableSizeHistory, Criteria> systemTableSizeHistoryDAO) {
		this.systemTableSizeHistoryDAO = systemTableSizeHistoryDAO;
	}


	public AdvancedUpdatableDAO<SystemTableSizeHistory, Criteria> getSystemTableSizeHistoryDAO() {
		return this.systemTableSizeHistoryDAO;
	}
}
