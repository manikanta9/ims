package com.clifton.system.schema.column.comparison;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemColumnComparisonUtils</code> class has shared utilities that help compare custom fields.
 *
 * @author vgomelsky
 */
public class SystemColumnComparisonUtils {

	/**
	 * Compares the specified field on both beans and returns the field name if they are not equal.
	 * Returns null if field values are the same.
	 * <p>
	 * Note, does smart comparison for custom column for SystemColumnCustomAware beans.
	 */
	@SuppressWarnings("unchecked")
	public static String getFieldNameIfNotEqual(IdentityObject bean1, IdentityObject bean2, String fieldName) {
		Object originalValue = BeanUtils.getPropertyValue(bean2, fieldName);
		Object newValue = BeanUtils.getPropertyValue(bean1, fieldName);
		if ("columnValueList".equals(fieldName) && bean1 instanceof SystemColumnCustomValueAware) {
			return getFieldNameIfNotEqual((List<SystemColumnValue>) originalValue, (List<SystemColumnValue>) newValue);
		}
		// use intelligent comparison that equates "2.5" to "2.5000"
		if (CompareUtils.compare(originalValue, newValue) == 0) {
			return null;
		}
		return fieldName;
	}


	/**
	 * Returns the name of the custom column that is not equal to corresponding custom column in the other list.
	 * Returns null if both lists have identical column values.
	 */
	public static String getFieldNameIfNotEqual(List<SystemColumnValue> list1, List<SystemColumnValue> list2) {
		Map<Object, SystemColumnValue> map2 = BeanUtils.getBeanMap(list2, SystemColumnValue::getId);
		for (SystemColumnValue cv1 : CollectionUtils.getIterable(list1)) {
			SystemColumnValue cv2 = map2.remove(cv1.getId());
			if (cv2 == null) {
				// found a value in first list that is not in the second list
				return cv1.getColumn().getLabel();
			}
			Object v1 = cv1.getValue();
			Object v2 = cv2.getValue();
			// perform data conversion if necessary to make sure that "5.0" is equal to "5"
			if (cv1.getColumn().getDataType().isNumeric()) {
				if (MathUtils.compare(new BigDecimal((String) v1), new BigDecimal((String) v2)) != 0) {
					return cv1.getColumn().getLabel();
				}
			}
			else if (!CompareUtils.isEqual(v1, v2)) {
				// values are different
				return cv1.getColumn().getLabel();
			}
		}
		if (!map2.isEmpty()) {
			// second list has more element(s), return first
			return map2.get(map2.keySet().iterator().next()).getColumn().getLabel();
		}

		return null;
	}
}
