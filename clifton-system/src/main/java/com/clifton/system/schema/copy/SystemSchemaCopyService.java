package com.clifton.system.schema.copy;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.system.schema.SystemTable;


/**
 * @author vgomelsky
 */
public interface SystemSchemaCopyService {


	@SecureMethod(dtoClass = SystemTable.class)
	public Status copySystemSchemaFields(SchemaFieldsCopyCommand command);
}
