package com.clifton.system.schema;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.StringUtils;


/**
 * The <code>SystemDataSource</code> represents a named data source (a database connection).
 *
 * @author vgomelsky
 */
@CacheByName
public class SystemDataSource extends NamedEntity<Short> {

	private boolean defaultDataSource;

	/**
	 * The unique short-name identifier for the data source.
	 */
	private String alias;

	/**
	 * The name of the database that the data source should be using
	 */
	private String databaseName;

	/**
	 * The URL that the data source should be using to connect to its database.
	 * <p>
	 * This is used for direct JDBC connections.
	 */
	private String connectionUrl;

	/**
	 * For external data sources this will be the application that hosts the API used as the datasource.
	 * <p>
	 * If this field is populated that indicates there should be an OAuth connection setup for it.
	 */
	private String applicationUrl;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if there is an OAuth connection for the given datasource.
	 */
	public boolean isExternal() {
		return !StringUtils.isEmpty(getApplicationUrl());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		result.append(",name=");
		result.append(getName());
		result.append(",db=");
		result.append(getDatabaseName());
		result.append('}');
		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isDefaultDataSource() {
		return this.defaultDataSource;
	}


	public void setDefaultDataSource(boolean defaultDataSource) {
		this.defaultDataSource = defaultDataSource;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getDatabaseName() {
		return this.databaseName;
	}


	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}


	public String getConnectionUrl() {
		return this.connectionUrl;
	}


	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}


	public String getApplicationUrl() {
		return this.applicationUrl;
	}


	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}
}
