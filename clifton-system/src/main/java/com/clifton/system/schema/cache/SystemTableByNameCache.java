package com.clifton.system.schema.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.core.util.ArrayUtils;
import com.clifton.system.schema.SystemTable;
import org.springframework.stereotype.Component;


/**
 * The SystemTableByNameCache class caches SystemTable objects by table name. Because there could be multiple data sources that have
 * the same table, it first looks up using the default data source.  If the table is not found in the default data source, then it
 * will lookup the table name globally.  It will fail if more than one SystemTable is returned.
 *
 * @author vgomelsky
 */
@Component
public class SystemTableByNameCache extends SelfRegisteringSingleKeyDaoCache<SystemTable, String> {

	@Override
	protected String getBeanKeyProperty() {
		return "name";
	}


	@Override
	protected String getBeanKeyValue(SystemTable bean) {
		return bean.getName();
	}


	@Override
	protected SystemTable lookupBean(ReadOnlyDAO<SystemTable> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		// check default data source only (the same tables could be present in multiple databases)
		SystemTable bean = dao.findOneByFields(new String[]{"name", "dataSource.defaultDataSource"}, ArrayUtils.add(keyProperties, true));
		if (bean == null) {
			// not present in default data source: use default logic to check any
			bean = super.lookupBean(dao, throwExceptionIfNotFound, keyProperties);
		}
		return bean;
	}


	/**
	 * If a new table is inserted, just clear the cache for that table name,
	 * we don't want to overwrite the default datasource table that might be cached
	 * because the lookup is customized to handle various cases.
	 */
	@Override
	protected boolean isClearOnAllKeyChanges() {
		return true;
	}
}
