package com.clifton.system.schema.column.value;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.usedby.softlink.SoftLinkField;


/**
 * The <code>SystemColumnValue</code> stores the value for a custom column and entity.
 * Values stored here are for custom columns under a column group of CustomColumnTypes.COLUMN_VALUE
 */
public class SystemColumnValue extends BaseEntity<Integer> implements LabeledObject {

	private SystemColumnCustom column;

	// The entity this field value applies to
	@SoftLinkField(tableBeanPropertyName = "column.columnGroup.table")
	private Integer fkFieldId;

	private String value;

	/**
	 * Text field stores display text when applicable.
	 * For example, for drop downs, value usually stores 'id' and text stores 'label' of corresponding entity.
	 */
	private String text;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		result.append(",column=");
		result.append(getColumn());
		result.append(",value=");
		result.append(getValue());
		result.append(",text=");
		result.append(getText());
		result.append('}');
		return result.toString();
	}


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		result.append(this.column == null ? "UNKNOWN-COLUMN" : this.column.getName());
		result.append(" = ");
		result.append(this.value);
		return result.toString();
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public SystemColumnCustom getColumn() {
		return this.column;
	}


	public void setColumn(SystemColumnCustom column) {
		this.column = column;
	}
}
