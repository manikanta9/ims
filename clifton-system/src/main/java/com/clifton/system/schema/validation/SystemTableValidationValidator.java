package com.clifton.system.schema.validation;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.Validator;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemTableValidationValidator</code> class is a {@link DaoEventObserver} that implements the
 * {@link Validator} interface and performs any EntityModifyConditions defined on the
 * {@link SystemTable} for a given bean.
 * <p>
 * TODO: It is possible for changes to the condition without changes to the table could happen.  In this case
 * the old condition would be used.
 *
 * @param <T>
 * @author manderson
 */
public class SystemTableValidationValidator<T extends IdentityObject> extends SelfRegisteringDaoValidator<T> {

	private final SystemTable table;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	public SystemTableValidationValidator(SystemTable table) {
		AssertUtils.assertNotNull(table, "Required argument 'table' cannot be null.");
		this.table = table;
	}


	@Override
	public void validate(T bean, DaoEventTypes config) throws ValidationException {
		SystemCondition validateCondition = null;

		if (DaoEventTypes.INSERT == config) {
			validateCondition = this.table.getInsertEntityCondition();
		}
		else if (DaoEventTypes.UPDATE == config) {
			validateCondition = this.table.getUpdateEntityCondition();
		}
		else if (DaoEventTypes.DELETE == config && this.table.getDeleteEntityCondition() != null) {
			validateCondition = this.table.getDeleteEntityCondition();
		}
		if (validateCondition != null) {
			validateCondition(bean, config.name(), validateCondition);
		}
	}


	private void validateCondition(T bean, String action, SystemCondition condition) {
		String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(condition, bean);
		if (!StringUtils.isEmpty(error)) {
			throw new ValidationException("Unable to " + action + " entity because:<br />" + error);
		}
	}


	/**
	 * To check if two SystemTableValidationValidators are equal, check if the SystemTables are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SystemTableValidationValidator<?>)) {
			return false;
		}
		return this.table.equals(((SystemTableValidationValidator<?>) obj).table);
	}


	@Override
	public int hashCode() {
		return this.table.hashCode();
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
