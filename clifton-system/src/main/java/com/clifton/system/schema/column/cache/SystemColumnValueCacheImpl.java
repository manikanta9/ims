package com.clifton.system.schema.column.cache;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueHolder;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>SystemColumnValueCacheImpl</code> class provides caching and retrieval from cache of custom column value objects.
 * <p>
 * The class also implements {@link DaoEventObserver} and clears the cache whenever update methods to related DTOs happen.
 * Make sure to register this class as an observer to corresponding SystemColumnValue and SystemColumn (default values) DAOs.
 * <p>
 * Cache contains a map with:
 * <ul>
 * <li>key: <tt>TableName_ColumnGroupName_ColumnName</tt>
 * <li>value: Map with key: <tt>ID</tt> -> <tt>ObjectWrapper&lt;Object&gt;</tt>
 * <li>and <tt>"{@value #GLOBAL_DEFAULT_KEY}"</tt> -> <tt>ObjectWrapper&lt;Object&gt;</tt>
 * </ul>
 * <p>
 * When the any of the following fields are updated
 *
 * @author vgomelsky
 */
@Component
public class SystemColumnValueCacheImpl<T extends IdentityObject> extends BaseDaoEventObserver<T> implements CustomCache<String, Map<String, ObjectWrapper<SystemColumnValueHolder>>>, SystemColumnValueCache {

	private static final String GLOBAL_DEFAULT_KEY = "GLOBAL_DEFAULT";

	private CacheHandler<String, Map<String, ObjectWrapper<SystemColumnValueHolder>>> cacheHandler;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public ObjectWrapper<SystemColumnValueHolder> getSystemColumnValueGlobalDefault(String tableName, String columnGroupName, String columnName, String linkedValue) {
		Map<String, ObjectWrapper<SystemColumnValueHolder>> valueMap = getCacheHandler().get(getCacheName(), getKey(tableName, columnGroupName, columnName, linkedValue));
		if (valueMap != null) {
			return valueMap.get(GLOBAL_DEFAULT_KEY);
		}
		return null;
	}


	@Override
	public ObjectWrapper<SystemColumnValueHolder> getSystemColumnValueForEntity(String tableName, Serializable entityId, String columnGroupName, String columnName) {
		// For Specific Entities - Linked Value Isn't Necessary
		Map<String, ObjectWrapper<SystemColumnValueHolder>> valueMap = getCacheHandler().get(getCacheName(), getKey(tableName, columnGroupName, columnName, null));
		if (valueMap != null) {
			return valueMap.get(entityId + "");
		}
		return null;
	}


	@Override
	public void setSystemColumnValueForEntity(String tableName, Serializable entityId, String columnGroupName, String columnName, ObjectWrapper<SystemColumnValueHolder> value) {
		// For Specific Entities - Linked Value Isn't Necessary
		Map<String, ObjectWrapper<SystemColumnValueHolder>> valueMap = getCacheHandler().get(getCacheName(), getKey(tableName, columnGroupName, columnName, null));
		if (valueMap == null) {
			valueMap = new ConcurrentHashMap<>();
		}
		valueMap.put(entityId + "", value);
		getCacheHandler().put(getCacheName(), getKey(tableName, columnGroupName, columnName, null), valueMap);
	}


	@Override
	public void setSystemColumnValueGlobalDefault(String tableName, String columnGroupName, String columnName, String linkedValue, ObjectWrapper<SystemColumnValueHolder> value) {
		Map<String, ObjectWrapper<SystemColumnValueHolder>> valueMap = getCacheHandler().get(getCacheName(), getKey(tableName, columnGroupName, columnName, linkedValue));
		if (valueMap == null) {
			valueMap = new ConcurrentHashMap<>();
		}
		valueMap.put(GLOBAL_DEFAULT_KEY, value);
		getCacheHandler().put(getCacheName(), getKey(tableName, columnGroupName, columnName, linkedValue), valueMap);
	}


	/**
	 * Global key means to include the linked value.  Otherwise that is excluded (used for entity specific keys)
	 */
	private String getKey(T bean, boolean global) {
		SystemColumnCustom column = (bean instanceof SystemColumnCustom) ? (SystemColumnCustom) bean : ((SystemColumnValue) bean).getColumn();
		return getKey(column.getTable().getName(), column.getColumnGroup().getName(), column.getName(), (global ? column.getLinkedValue() : null));
	}


	private String getKey(String tableName, String columnGroupName, String columnName, String linkedValue) {
		StringBuilder key = new StringBuilder(80);
		key.append(tableName);
		key.append('_');
		key.append(columnGroupName);
		key.append('_');
		key.append(columnName);
		if (!StringUtils.isEmpty(linkedValue)) {
			key.append('_');
			key.append(linkedValue);
		}
		return key.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (!((bean instanceof SystemColumnCustom) || bean instanceof SystemColumnValue)) {
			return;
		}

		if (e == null) {
			String globalKey = getKey(bean, true);
			String entitySpecificKey = getKey(bean, false);

			// Custom Columns: Updates or Deletes - Skip Inserts since no values would exist for them yet
			if (bean instanceof SystemColumnCustom) {
				SystemColumnCustom column = (SystemColumnCustom) bean;
				if (event.isUpdate()) {
					// Not likely to happen, but if the key changed...clear old and new
					T originalBean = getOriginalBean(dao, bean);
					if (originalBean != null) {
						String originalGlobalKey = getKey(originalBean, true);
						String originalEntitySpecificKey = getKey(originalBean, false);
						if (!StringUtils.isEqual(originalGlobalKey, globalKey) || !StringUtils.isEqual(originalEntitySpecificKey, entitySpecificKey)) {
							getCacheHandler().remove(getCacheName(), originalGlobalKey);
							getCacheHandler().remove(getCacheName(), globalKey);

							getCacheHandler().remove(getCacheName(), originalEntitySpecificKey);
							getCacheHandler().remove(getCacheName(), entitySpecificKey);
						}
						// Otherwise, if DataType changed  clear key
						else if (!CompareUtils.isEqual(((SystemColumnCustom) originalBean).getDataType(), column.getDataType())) {
							getCacheHandler().remove(getCacheName(), globalKey);
							getCacheHandler().remove(getCacheName(), entitySpecificKey);
						}
						// Otherwise, if Global Default changed - remove Global Default Value
						else if (!StringUtils.isEqual(((SystemColumnCustom) originalBean).getGlobalDefaultValue(), column.getGlobalDefaultValue())) {
							removeKeyFromValueCacheMap(globalKey, GLOBAL_DEFAULT_KEY);
						}
					}
				}
				else if (event.isDelete()) {
					getCacheHandler().remove(getCacheName(), globalKey);
					getCacheHandler().remove(getCacheName(), entitySpecificKey);
				}
			}
			else {
				Integer fkFieldId = ((SystemColumnValue) bean).getFkFieldId();
				// Specific value change - clear that specific entry from the map
				removeKeyFromValueCacheMap(entitySpecificKey, fkFieldId + "");
			}
		}
	}


	private void removeKeyFromValueCacheMap(String cacheKey, String valueKey) {
		Map<String, ObjectWrapper<SystemColumnValueHolder>> map = getCacheHandler().get(getCacheName(), cacheKey);
		if (map != null) {
			map.remove(valueKey);
			getCacheHandler().put(getCacheName(), cacheKey, map);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	public void setCacheHandler(CacheHandler<String, Map<String, ObjectWrapper<SystemColumnValueHolder>>> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	@Override
	public CacheHandler<String, Map<String, ObjectWrapper<SystemColumnValueHolder>>> getCacheHandler() {
		return this.cacheHandler;
	}
}
