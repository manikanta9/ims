package com.clifton.system.schema.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author danielh
 */
public class SystemDataSourceSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField
	private String alias;

	@SearchField
	private String databaseName;

	@SearchField
	private Boolean defaultDataSource;

	@SearchField
	private String connectionUrl;

	@SearchField
	private String applicationUrl;

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getDatabaseName() {
		return this.databaseName;
	}


	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}


	public Boolean getDefaultDataSource() {
		return this.defaultDataSource;
	}


	public void setDefaultDataSource(Boolean defaultDataSource) {
		this.defaultDataSource = defaultDataSource;
	}


	public String getConnectionUrl() {
		return this.connectionUrl;
	}


	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}


	public String getApplicationUrl() {
		return this.applicationUrl;
	}


	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}
}
