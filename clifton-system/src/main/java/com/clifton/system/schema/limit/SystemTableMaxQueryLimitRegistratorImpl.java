package com.clifton.system.schema.limit;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.search.SystemTableSearchForm;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.List;


/**
 * <code>SystemTableMaxQueryLimitRegistratorImpl</code> update the maximum query limit on DAO configurations from system table values upon spring startup.
 *
 * @author TerryS
 */
public class SystemTableMaxQueryLimitRegistratorImpl<T extends IdentityObject> implements SystemTableMaxQueryLimitRegistrator, CurrentContextApplicationListener<ContextRefreshedEvent> {

	private SystemSchemaService systemSchemaService;

	private DaoLocator daoLocator;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			SystemTableSearchForm systemTableSearchForm = new SystemTableSearchForm();
			systemTableSearchForm.setMaxQuerySizeLimitNotNull(true);
			systemTableSearchForm.setDefaultDataSource(true);
			List<SystemTable> tableList = getSystemSchemaService().getSystemTableList(systemTableSearchForm);
			for (SystemTable table : CollectionUtils.getIterable(tableList)) {
				updateMaximumQueryLimit(table);
			}
		}
		catch (Throwable e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error updating maximum query limits from system table: " + event, e);
		}
	}


	@Override
	public void updateMaximumQueryLimit(SystemTable table) {
		// null will result in the system default.
		int maxQueryLimit = table.getMaxQuerySizeLimit() == null ? PagingCommand.MAX_LIMIT : table.getMaxQuerySizeLimit();
		ReadOnlyDAO<T> dao = getDaoLocator().locate(table.getName());
		dao.getConfiguration().setMaxQuerySizeLimit(maxQueryLimit);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
