package com.clifton.system.schema.column.json;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.schema.SystemDataTypeConverter;
import com.clifton.system.schema.column.CustomColumnTypes;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnGroup;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemColumnCustomAwareObserver</code> class should be registered for MODIFY events of all SystemColumnCustomAware DAO's
 * It deletes custom columns on corresponding entity delete, updates custom columns and validates their values before updates.
 *
 * @author Mary Anderson
 */
@Component
public class SystemColumnCustomJsonAwareObserver<E extends SystemColumnCustomJsonAware> extends BaseDaoEventObserver<E> {


	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<E> dao, @SuppressWarnings("unused") DaoEventTypes event, @SuppressWarnings("unused") E bean) {
		if (event.isInsert() || event.isUpdate()) {
			// Only validate if:
			// 1. Column Group Name is Populated
			if (!StringUtils.isEmpty(bean.getColumnGroupName())) {
				// 2. The column group values being edited are saved as a Json String
				SystemColumnGroup columnGroup = getSystemColumnService().getSystemColumnGroupByName(bean.getColumnGroupName());
				if (columnGroup != null && columnGroup.getCustomColumnType() == CustomColumnTypes.COLUMN_JSON_VALUE) {
					validateCustomFields(bean, columnGroup);
				}
			}
		}
	}


	protected void validateCustomFields(E bean, SystemColumnGroup columnGroup) {
		SystemColumnStandard jsonValueColumn = (SystemColumnStandard) columnGroup.getJsonValueColumn();
		CustomJsonString customJsonString = (CustomJsonString) BeanUtils.getPropertyValue(bean, jsonValueColumn.getBeanPropertyName());

		// Get the List of Columns:
		List<SystemColumnCustom> customColumnList = getSystemColumnService().getSystemColumnCustomListForEntity(bean);

		List<String> missingRequiredColumns = new ArrayList<>();
		for (SystemColumnCustom columnCustom : CollectionUtils.getIterable(customColumnList)) {
			Object value = CustomJsonStringUtils.getColumnValue(customJsonString, columnCustom.getName());
			String valueString = new ObjectToStringConverter().convert(value);

			if (StringUtils.isEmpty(columnCustom.getGlobalDefaultValue())) {
				if (StringUtils.isEmpty(valueString) && columnCustom.isRequired()) {
					missingRequiredColumns.add(columnCustom.getLabel());
					continue;
				}
			}
			else if (columnCustom.getGlobalDefaultValue().equals(valueString)) {
				customJsonString.clearColumnValue(columnCustom.getName());
				continue;
			}

			// Validate Value Data Types (if not null)
			if (value != null) {
				SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(columnCustom.getDataType());
				try {
					Object valueObject = dataTypeConverter.convert(valueString);
					valueString = (new ObjectToStringConverter().convert(valueObject));
					customJsonString.replaceColumnValue(columnCustom.getName(), valueString);
				}
				catch (Throwable e) {
					throw new ValidationException("Cannot convert value '" + valueString + "' to data type '" + columnCustom.getDataType().getName() + "' for column '" + columnCustom.getLabel());
				}
			}
		}

		if (!CollectionUtils.isEmpty(missingRequiredColumns)) {
			throw new ValidationException("Missing values for required column(s) [" + StringUtils.collectionToCommaDelimitedString(missingRequiredColumns) + "].");
		}

		// Reset the Json Value Based on Updated Map
		CustomJsonStringUtils.resetCustomJsonStringFromJsonValueMap(customJsonString);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}
}
