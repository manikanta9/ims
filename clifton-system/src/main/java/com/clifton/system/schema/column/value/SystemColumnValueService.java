package com.clifton.system.schema.column.value;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.schema.column.SystemColumnCustomConfig;
import com.clifton.system.schema.column.search.SystemColumnDatedValueSearchForm;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>SystemColumnValueService</code> defines the methods for working with {@link SystemColumnDatedValue} and {@link SystemColumnValue} Objects.
 *
 * @author apopp
 */
public interface SystemColumnValueService {

	////////////////////////////////////////////////////////////////////////////////
	//////////             System Column Dated Value Methods             ///////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dynamicSecurityResourceBeanPath = "column.columnGroup.securityResource.name", dynamicTableNameBeanPath = "column.columnGroup.table.name")
	public SystemColumnDatedValue getSystemColumnDatedValue(int id);


	public List<SystemColumnDatedValue> getSystemColumnDatedValueList(SystemColumnDatedValueSearchForm searchForm);


	@SecureMethod(dynamicSecurityResourceBeanPath = "column.columnGroup.securityResource.name", dynamicTableNameBeanPath = "column.columnGroup.table.name")
	public SystemColumnDatedValue saveSystemColumnDatedValue(SystemColumnDatedValue bean);


	@SecureMethod(dynamicSecurityResourceBeanPath = "column.columnGroup.securityResource.name", dynamicTableNameBeanPath = "column.columnGroup.table.name")
	public void deleteSystemColumnDatedValue(int id);


	////////////////////////////////////////////////////////////////////////////////
	//////////          System Column Dated Value Object Methods          //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For the given {@link SystemColumnDatedValue} returns the field value as an object - i.e. Boolean, BigDecimal, String, etc. based on the columns data type
	 */
	@DoNotAddRequestMapping
	public Object getSystemColumnDatedValueAsObject(SystemColumnDatedValue bean);


	////////////////////////////////////////////////////////////////////////////////
	////////////               System Column Value Methods               ///////////
	////////////////////////////////////////////////////////////////////////////////


	public List<SystemColumnValue> getSystemColumnValueList(SystemColumnValueSearchForm searchForm);


	@SecureMethod(securityResourceResolverBeanName = "systemColumnGroupNameSecureMethodResolver")
	public void saveSystemColumnValueList(SystemColumnCustomConfig config);


	/**
	 * UI exposed copy system column value list method. See @link copySystemColumnValueList internal method.
	 */
	public void copySystemColumnValueList(String tableName, String columnGroupName, int fromEntityId, int toEntityId, boolean deleteExisting);

	////////////////////////////////////////////////////////////////////////////////
	///////////            System Column Value Object Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	@ResponseBody
	@SecureMethod(securityResourceResolverBeanName = "systemColumnGroupNameSecureMethodResolver")
	public Object getSystemColumnValueForEntityByTable(String tableName, Integer entityId, String columnGroupName, String columnName, boolean returnGlobalDefaultIfNull);


	/**
	 * Returns the value for the specified custom column that belongs to the specified entity.
	 * Returns null if the values is not defined and no global value is defined.
	 */
	@ResponseBody
	@SecureMethod(securityResourceResolverBeanName = "systemColumnGroupNameSecureMethodResolver")
	public Object getSystemColumnCustomValue(int entityId, String columnGroupName, String columnName);

	////////////////////////////////////////////////////////////////////////////////
	/////////////                  Config Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Sets ColumnCustomList and optionally columnValueList based upon config properties
	 * and returns the populated config object.
	 */
	@SecureMethod(securityResourceResolverBeanName = "systemColumnGroupNameSecureMethodResolver")
	public SystemColumnCustomConfig getSystemColumnCustomConfig(SystemColumnCustomConfig config);
}
