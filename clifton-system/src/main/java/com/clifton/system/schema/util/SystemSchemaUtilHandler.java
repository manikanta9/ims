package com.clifton.system.schema.util;


import com.clifton.core.beans.IdentityObject;
import com.clifton.system.schema.SystemTable;


/**
 * The handler component for system schema operations.
 */
public interface SystemSchemaUtilHandler {

	/**
	 * The maximum number of characters allowed for unique schema keys generated for an entity.
	 */
	public static final short ENTITY_UNIQUE_ID_MAXIMUM_LENGTH = 16;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the entity corresponding to the given unique entity identifier.
	 *
	 * @param uniqueId the unique identifier, as generated via {@link #getEntityUniqueId(IdentityObject)}
	 * @param <T>      the entity type
	 */
	public <T extends IdentityObject> T getEntityFromUniqueId(String uniqueId);


	/**
	 * Splits the given unique entity identifier into its components.
	 * <p>
	 * Resulting array components:
	 * <ol>
	 * <li>Data source alias
	 * <li>Entity ID
	 * <li>Table alias
	 * </ol>
	 *
	 * @param uniqueId the unique identifier, as generated via {@link #getEntityUniqueId(IdentityObject)}
	 * @return the array of entity identifier components
	 */
	public String[] getEntityUniqueIdComponents(String uniqueId);


	/**
	 * Gets the unique identifier for the given table and entity ID. This message may be separated via {@link #getEntityUniqueIdComponents(String)}.
	 * <p>
	 * The unique identifier is limited to {@value #ENTITY_UNIQUE_ID_MAXIMUM_LENGTH} characters in length and is provided in the following format with no
	 * delimiters:
	 * <pre>
	 * {@code <Data Source Alias><Entity ID><Table Alias>}
	 * </pre>
	 *
	 * @param table    the table for the unique identifier
	 * @param entityId the entity ID for the unique identifier
	 * @return the generated unique identifier
	 */
	public String getEntityUniqueId(SystemTable table, Number entityId);


	/**
	 * Gets the unique identifier for the given entity. This method uses the entity to determine the table object and entity ID which shall be used to construct
	 * the unique identifier.
	 *
	 * @param entity the entity for which to create the unique identifier
	 * @return the generated unique identifier
	 * @see #getEntityUniqueId(SystemTable, Number)
	 */
	public String getEntityUniqueId(IdentityObject entity);
}
