package com.clifton.system.schema.column.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemDataTypeConverter;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemColumnValidator</code> class throws corresponding ValidationException if one of
 * validation rules for {@link SystemColumnCustom} is violated during insert, update, or delete.
 * <p/>
 * For example, system defined columns cannot be updated
 * If the Group doesn't have a linkedBeanProperty, Linked Value cannot be populated.
 * Sets linkedLabel if not set, but a value is set
 * Depended Columns must also be custom and cannot depend on itself and if linked values are populated, must be the same value
 * <p/>
 * Also validated {@link SystemColumnStandard} updates - (Inserts or Deletes are not allowed) and restricts changing the table, data type, or column name
 *
 * @author Mary Anderson
 */
@Component
public class SystemColumnValidator extends SelfRegisteringDaoValidator<SystemColumn> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemColumn bean, DaoEventTypes config) throws ValidationException {
		if (bean.isCustomColumn()) {
			validateCustomColumn((SystemColumnCustom) bean, config);
		}
		else {
			validateStandardColumn((SystemColumnStandard) bean, config);
		}
	}


	private void validateCustomColumn(SystemColumnCustom bean, DaoEventTypes config) {
		SystemColumnCustom originalBean = null;

		if (config.isUpdate()) {
			originalBean = (SystemColumnCustom) getOriginalBean(bean);
			validateCustomSystemDefinedFields(originalBean, bean);
		}

		if (config.isInsert() || config.isUpdate()) {
			ValidationUtils.assertNotNull(bean.getColumnGroup(), "Column group is required for custom columns.");
			ValidationUtils.assertTrue(bean.getColumnGroup().getTable().equals(bean.getTable()), "Column group table [" + bean.getColumnGroup().getTable().getName()
					+ "] must be the same as the column table [" + bean.getTable().getName() + "].");
			if (!StringUtils.isEmpty(bean.getLinkedValue())) {
				ValidationUtils.assertTrue(!StringUtils.isEmpty(bean.getColumnGroup().getLinkedBeanProperty()), "Linked Values can only be entered for Groups with a Linked Bean Property defined.",
						"linkedValue");
				if (StringUtils.isEmpty(bean.getLinkedLabel())) {
					bean.setLinkedLabel(bean.getLinkedValue());
				}
			}
			if (bean.getDependedColumn() != null) {
				ValidationUtils.assertTrue(bean.getDependedColumn().isCustomColumn(), "Depended Columns must be custom columns.", "dependedColumn.id");
				ValidationUtils.assertFalse(bean.equals(bean.getDependedColumn()), "Invalid selection for Depended Column.  A column cannot depend on itself.", "dependedColumn.id");

				ValidationUtils.assertTrue(
						StringUtils.compare(bean.getLinkedValue(), bean.getDependedColumn().getLinkedValue()) == 0,
						"Invalid selection for Depended Column.  Depend column has a link value of [" + bean.getDependedColumn().getLinkedValue() + "] but this column has a link value of ["
								+ bean.getLinkedValue() + "].");
			}


			if (!StringUtils.isEmpty(bean.getDefaultValue())) {
				validateCustomDefaultData(bean.getDataType(), bean.getDefaultValue());
			}

			if (!StringUtils.isEmpty(bean.getGlobalDefaultValue())) {
				validateCustomDefaultData(bean.getDataType(), bean.getGlobalDefaultValue());
			}
		}

		// Custom Column Type Specific Validation
		bean.getColumnGroup().getCustomColumnType().validateSystemColumnCustom(bean, originalBean, config);
	}


	private void validateStandardColumn(SystemColumnStandard bean, DaoEventTypes config) {
		if (config.isInsert() || config.isDelete()) {
			throw new ValidationException("Standard Columns cannot be " + (config.isInsert() ? "inserted." : "deleted."));
		}
		SystemColumnStandard originalBean = (SystemColumnStandard) getOriginalBean(bean);
		ValidationUtils.assertTrue(originalBean.getTable().equals(bean.getTable()), "The column's table [" + originalBean.getTable().getName() + "] cannot be changed");
		ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "The column's name [" + originalBean.getName() + "] cannot be changed");
		ValidationUtils.assertTrue(originalBean.getBeanPropertyName().equals(bean.getBeanPropertyName()), "The column's bean property name [" + originalBean.getBeanPropertyName()
				+ "] cannot be changed");
		ValidationUtils.assertTrue(originalBean.getDataType().equals(bean.getDataType()), "The column's data type [" + originalBean.getDataType().getName() + "] cannot be changed");
		bean.setSystemDefined(true);
	}


	private void validateCustomDefaultData(SystemDataType dataType, String value) {
		SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(dataType);
		try {
			Object valueObj = dataTypeConverter.convert(value);
			new ObjectToStringConverter().convert(valueObj);
		}
		catch (Throwable e) {
			throw new ValidationException("Cannot convert default value '" + value + "' to selected data type '" + dataType.getName() + "'");
		}
	}


	private void validateCustomSystemDefinedFields(SystemColumnCustom original, SystemColumnCustom bean) {
		if (original.isSystemDefined()) {
			String message = "] field cannot be edited for System Defined Columns.";
			ValidationUtils.assertTrue(original.getName().equals(bean.getName()), "[Name" + message, "name");
			ValidationUtils.assertTrue(original.getColumnGroup().equals(bean.getColumnGroup()), "[Column Group" + message, "columnGroup.id");
			ValidationUtils.assertTrue(original.getDataType().equals(bean.getDataType()), "[Data Type" + message, "dataType.id");
			ValidationUtils.assertTrue(StringUtils.compare(original.getLinkedValue(), bean.getLinkedValue()) == 0, "[Linked Value" + message, "linkedValue");
		}
	}
}
