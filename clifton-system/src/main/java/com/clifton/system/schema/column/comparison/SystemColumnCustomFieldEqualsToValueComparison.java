package com.clifton.system.schema.column.comparison;


import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.comparison.field.BeanFieldEqualsToValueComparison;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.column.SystemColumnCustomAware;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;


/**
 * The <code>SystemColumnCustomFieldValueEqualsComparison</code> ...
 *
 * @author manderson
 */
public class SystemColumnCustomFieldEqualsToValueComparison<T extends SystemColumnCustomAware> extends BeanFieldEqualsToValueComparison<T> {

	private SystemColumnValueHandler systemColumnValueHandler;
	private String columnGroupName;
	private String columnName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(T bean, ComparisonContext context) {
		ValidationUtils.assertTrue(SystemColumnCustomValueAware.class.isAssignableFrom(bean.getClass()), "Invalid condition for bean class [" + bean.getClass().getName()
				+ "] as it doesn't implement SystemColumnCustomAware interface.");

		// Set group name on the entity so we know which to look for - keep original so we don't clear what was there
		String originalGroupName = bean.getColumnGroupName();
		bean.setColumnGroupName(getColumnGroupName());

		Object value = getSystemColumnValueHandler().getSystemColumnValueForEntity(bean, getColumnGroupName(), getColumnName(), true);

		bean.setColumnGroupName(originalGroupName);

		String actualValue = getActualValue(bean);
		boolean result = compareValues(value, getActualValue(bean));

		// record comparison result message
		if (context != null) {
			String msg;

			if (isReverse() ? !result : result) {
				msg = ("(" + getColumnName() + " = " + actualValue + ")");
			}
			else {
				msg = ("(" + getColumnName() + " = " + value + " instead of " + actualValue + ")");
			}
			if (result) {
				context.recordTrueMessage(msg);
			}
			else {
				context.recordFalseMessage(msg);
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public String getColumnName() {
		return this.columnName;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
