package com.clifton.system.schema.column.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * The <code>SystemFieldValueSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class SystemColumnValueSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchFieldPath = "column", searchField = "columnGroup.id")
	private Short columnGroupId;

	@SearchField(searchFieldPath = "column.columnGroup", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String columnGroupName;

	@SearchField(searchFieldPath = "column", searchField = "linkedValue", comparisonConditions = ComparisonConditions.EQUALS)
	private String linkedValue;

	@SearchField(searchFieldPath = "column.columnGroup.table", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String tableName;

	@SearchField(searchFieldPath = "column", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String columnName;

	@SearchField
	private Integer fkFieldId;

	@SearchField(searchField = "fkFieldId")
	private Integer[] fkFieldIds;

	@SearchField(searchField = "column.id")
	private Integer columnId;

	@SearchField
	private String value;

	@SearchField(searchField = "value", comparisonConditions = ComparisonConditions.EQUALS)
	private String valueEquals;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertFalse(getColumnGroupId() == null && getColumnGroupName() == null && getLinkedValue() == null && getTableName() == null && getColumnName() == null && getFkFieldId() == null && ArrayUtils.isEmpty(getFkFieldIds()) && getColumnId() == null && getValue() == null && getValueEquals() == null,
				"At least one of the following restrictions must be present: Column Group, Linked Value, Table, Column, FK Field, Value.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public String getColumnName() {
		return this.columnName;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	public Integer getColumnId() {
		return this.columnId;
	}


	public void setColumnId(Integer columnId) {
		this.columnId = columnId;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getValueEquals() {
		return this.valueEquals;
	}


	public void setValueEquals(String valueEquals) {
		this.valueEquals = valueEquals;
	}


	public Short getColumnGroupId() {
		return this.columnGroupId;
	}


	public void setColumnGroupId(Short columnGroupId) {
		this.columnGroupId = columnGroupId;
	}


	public String getLinkedValue() {
		return this.linkedValue;
	}


	public void setLinkedValue(String linkedValue) {
		this.linkedValue = linkedValue;
	}


	public Integer[] getFkFieldIds() {
		return this.fkFieldIds;
	}


	public void setFkFieldIds(Integer[] fkFieldIds) {
		this.fkFieldIds = fkFieldIds;
	}
}
