package com.clifton.system.schema.column.value;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.column.search.SystemColumnDatedValueSearchForm;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemColumnDatedValueObserver</code> performs efficient validation and numeric value setter logic.
 *
 * @author apopp
 */
@Component
public class SystemColumnDatedValueObserver extends SelfRegisteringDaoValidator<SystemColumnDatedValue> {

	private SystemColumnValueService systemColumnValueService;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemColumnDatedValue bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			SystemColumnDatedValueSearchForm searchForm = new SystemColumnDatedValueSearchForm();

			// Ensure the value is set
			ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getValue()), "Value is required for system column dated values.");

			//Only perform date validation for new values or a pre-existing value if it's date has changed
			if (bean.isNewBean() || DateUtils.compare(bean.getStartDate(), getOriginalBean(bean).getStartDate(), true) != 0
					|| DateUtils.compare(bean.getStartDate(), getOriginalBean(bean).getEndDate(), true) != 0) {
				/*
				 * Loop through all related dated values and make sure they don't overlap in dates
				 */
				searchForm.setFkFieldId(bean.getFkFieldId());
				searchForm.setColumnId(bean.getColumn().getId());

				for (SystemColumnDatedValue systemColumnDatedValue : CollectionUtils.getIterable(getSystemColumnValueService().getSystemColumnDatedValueList(searchForm))) {
					ValidationUtils.assertFalse(
							!bean.equals(systemColumnDatedValue)
									&& DateUtils.isOverlapInDates(systemColumnDatedValue.getStartDate(), systemColumnDatedValue.getEndDate(), bean.getStartDate(), bean.getEndDate()),
							"System column dated value [" + systemColumnDatedValue.getLabelWithDates() + "] already exists with overlapping date range.");
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////////            Getter and Setter Methods           ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}
}
