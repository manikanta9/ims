package com.clifton.system.schema.column;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.audit.SystemAuditType;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemColumn</code> ...
 *
 * @author Mary Anderson
 */
public abstract class SystemColumn extends NamedEntity<Integer> {

	private SystemTable table;
	private SystemDataType dataType;
	private SystemAuditType auditType;

	private String valueListUrl;

	/**
	 * All standard columns are system defined = true, these columns can't be edited because they must match the database.
	 * Custom columns that are system defined prohibit some fields from being edited (name, data type)
	 * because they are used throughout the system in code, so changing the name and/or data type would break those areas that lookup the field values
	 * See {@link com.clifton.system.schema.column.validation.SystemColumnValidator#validateCustomSystemDefinedFields)
	 */
	private boolean systemDefined;

	private SystemColumnTemplate template;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract boolean isCustomColumn();


	public abstract String getCustomLabel();


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public SystemDataType getDataType() {
		return this.dataType;
	}


	public void setDataType(SystemDataType dataType) {
		this.dataType = dataType;
	}


	public SystemAuditType getAuditType() {
		return this.auditType;
	}


	public void setAuditType(SystemAuditType auditType) {
		this.auditType = auditType;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public String getValueListUrl() {
		return this.valueListUrl;
	}


	public void setValueListUrl(String valueListUrl) {
		this.valueListUrl = valueListUrl;
	}


	public SystemColumnTemplate getTemplate() {
		return this.template;
	}


	public void setTemplate(SystemColumnTemplate template) {
		this.template = template;
	}
}
