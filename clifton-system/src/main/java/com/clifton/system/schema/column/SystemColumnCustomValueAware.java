package com.clifton.system.schema.column;


import com.clifton.system.schema.column.value.SystemColumnValue;

import java.util.List;


/**
 * The <code>SystemColumnCustomValueAware</code> should be extended by entities that utilize standard custom fields.
 * Values are stored in {@link SystemColumnValue} table
 *
 * @author Mary Anderson
 */
public interface SystemColumnCustomValueAware extends SystemColumnCustomAware {

	/**
	 * Custom Fields values for this entity
	 */
	public List<SystemColumnValue> getColumnValueList();


	public void setColumnValueList(List<SystemColumnValue> columnValueList);
}
