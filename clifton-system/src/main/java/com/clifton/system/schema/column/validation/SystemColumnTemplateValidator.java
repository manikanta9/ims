package com.clifton.system.schema.column.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.column.SystemColumnTemplate;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemColumnTemplateValidator</code> validates on updates that
 * the name of the template has not changed
 *
 * @author theodorez
 */
@Component
public class SystemColumnTemplateValidator extends SelfRegisteringDaoValidator<SystemColumnTemplate> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	public void validate(SystemColumnTemplate bean, DaoEventTypes config) throws ValidationException {
		SystemColumnTemplate originalBean = getOriginalBean(bean);
		if (!bean.isNewBean()) {
			ValidationUtils.assertTrue(bean.getName().equals(originalBean.getName()), "The name of an existing Template cannot be changed.");
		}
	}
}
