package com.clifton.system.schema.jobs;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemTableCacheClearJob</code> can pass a comma delimited list of table names to clear the cache for,
 * and/or specific cache name(s).
 *
 * @author Mary Anderson
 */
public class SystemTableCacheClearJob implements Task {

	/**
	 * Comma Delimited list of String table names
	 */
	private String tableNames;

	/**
	 * Comma Delimited list of String cache names
	 */
	private String cacheNames;

	private DaoLocator daoLocator;
	private CacheHandler<?, ?> cacheHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		List<String> tableNameList = StringUtils.delimitedStringToList(getTableNames(), ",");
		List<String> cacheNameList = StringUtils.delimitedStringToList(getCacheNames(), ",");
		if (cacheNameList == null) {
			cacheNameList = new ArrayList<>();
		}
		for (String tableName : CollectionUtils.getIterable(tableNameList)) {
			if (!StringUtils.isEmpty(tableName) && !"null".equalsIgnoreCase(tableName)) {
				ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(tableName);
				cacheNameList.add(dao.getConfiguration().getCacheName());
			}
		}

		for (String cacheName : CollectionUtils.getIterable(cacheNameList)) {
			if (!StringUtils.isEmpty(cacheName) && !"null".equalsIgnoreCase(cacheName)) {
				getCacheHandler().clear(cacheName);
			}
		}

		return Status.ofMessage("Cleared Cache for the following: " + StringUtils.collectionToCommaDelimitedString(cacheNameList));
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableNames() {
		return this.tableNames;
	}


	public void setTableNames(String tableNames) {
		this.tableNames = tableNames;
	}


	public String getCacheNames() {
		return this.cacheNames;
	}


	public void setCacheNames(String cacheNames) {
		this.cacheNames = cacheNames;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public CacheHandler<?, ?> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<?, ?> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
