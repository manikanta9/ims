package com.clifton.system.schema.column.comparison;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.comparison.field.AreFieldsModifiedComparison;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemColumnCustomAreFieldsModifiedComparison</code> takes an unsaved updated bean and compares it to the
 * original bean retrieved from the database.
 * <p/>
 * NOTE: Custom Columns are optionally included in comparison.
 * <p>
 * Returns TRUE if ANY of the given fields have been modified.
 * Returns FALSE if NONE of the given fields have been modified.
 * NOTE: System Managed Fields (Record Stamp) are always excluded from comparison
 *
 * @author manderson
 */
public class SystemColumnCustomAreFieldsModifiedComparison extends AreFieldsModifiedComparison {

	private SystemColumnService systemColumnService;
	private SystemColumnValueService systemColumnValueService;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Define specific custom fields to check
	 */
	private String customFieldNames;


	/**
	 * Optionally check all custom fields
	 */
	private boolean allCustomFields;


	/**
	 * When checking all custom fields define a list of fields to exclude
	 */
	private String exceptionCustomFieldNames;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected IdentityObject getOriginalBean(final IdentityObject bean) {
		IdentityObject originalBean = super.getOriginalBean(bean);
		if (originalBean != null && bean instanceof SystemColumnCustomValueAware) {
			SystemColumnCustomValueAware beanWithCustomColumns = (SystemColumnCustomValueAware) bean;
			if (!StringUtils.isEmpty(beanWithCustomColumns.getColumnGroupName())) {
				// populate original custom columns
				SystemColumnCustomValueAware originalBeanWithCustomColumns = (SystemColumnCustomValueAware) originalBean;
				originalBeanWithCustomColumns.setColumnGroupName(beanWithCustomColumns.getColumnGroupName());
				SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
				searchForm.setColumnGroupName(beanWithCustomColumns.getColumnGroupName());
				searchForm.setFkFieldId(((Number) originalBean.getIdentity()).intValue());
				originalBeanWithCustomColumns.setColumnValueList(getSystemColumnValueService().getSystemColumnValueList(searchForm));
			}
		}
		return originalBean;
	}


	@Override
	protected List<String> getNotEqualFields(List<String> beanFields, IdentityObject bean, IdentityObject originalBean) {
		List<String> notEqualFields = super.getNotEqualFields(beanFields, bean, originalBean);
		if (notEqualFields == null) {
			notEqualFields = new ArrayList<>();
		}

		if (bean instanceof SystemColumnCustomValueAware && !StringUtils.isEmpty(((SystemColumnCustomValueAware) bean).getColumnGroupName())) {
			List<String> customFields = getCustomColumnFieldNames((SystemColumnCustomValueAware) bean);
			if (!CollectionUtils.isEmpty(customFields)) {
				beanFields.addAll(customFields);

				List<SystemColumnValue> valueList = ((SystemColumnCustomValueAware) bean).getColumnValueList();
				List<SystemColumnValue> originalValueList = originalBean == null ? null : ((SystemColumnCustomValueAware) originalBean).getColumnValueList();

				for (String customFieldName : customFields) {
					SystemColumnValue originalValue = CollectionUtils.getOnlyElement(BeanUtils.filter(originalValueList, systemColumnValue -> systemColumnValue.getColumn().getName(), customFieldName));
					SystemColumnValue newValue = CollectionUtils.getOnlyElement(BeanUtils.filter(valueList, systemColumnValue -> systemColumnValue.getColumn().getName(), customFieldName));
					// use intelligent comparison that equates "2.5" to "2.5000"
					if (CompareUtils.compare(originalValue, newValue) != 0) {
						if (originalValue != null && newValue != null) {
							if (CompareUtils.compare(originalValue.getValue(), newValue.getValue()) != 0) {
								notEqualFields.add(customFieldName);
							}
						}
						else {
							notEqualFields.add(customFieldName);
						}
					}
				}
			}
		}
		return notEqualFields;
	}


	private List<String> getCustomColumnFieldNames(SystemColumnCustomValueAware bean) {
		// If all custom fields, set String beanFieldNames automatically, and filter out any exceptions
		if (isAllCustomFields()) {
			Iterable<String> exceptions = StringUtils.delimitedStringToList(getExceptionCustomFieldNames(), ",");
			List<String> customFields = new ArrayList<>();

			List<SystemColumnCustom> allCustomFieldList = getSystemColumnService().getSystemColumnCustomListForEntity(bean);

			for (SystemColumnCustom customColumn : CollectionUtils.getIterable(allCustomFieldList)) {
				boolean add = true;
				for (String exception : CollectionUtils.getIterable(exceptions)) {
					if (StringUtils.isEqualIgnoreCase(customColumn.getName(), exception)) {
						add = false;
						break;
					}
				}
				if (add) {
					customFields.add(customColumn.getName());
				}
			}
			return customFields;
		}
		return StringUtils.delimitedStringToList(getCustomFieldNames(), ",");
	}


	@Override
	@ValueIgnoringGetter
	public String getExceptionBeanFieldNames() {
		String exceptions = super.getExceptionBeanFieldNames();
		if (StringUtils.isEmpty(exceptions)) {
			return "columnValueList,columnGroupName";
		}
		return exceptions + ",columnValueList,columnGroupName";
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public String getCustomFieldNames() {
		return this.customFieldNames;
	}


	public void setCustomFieldNames(String customFieldNames) {
		this.customFieldNames = customFieldNames;
	}


	public boolean isAllCustomFields() {
		return this.allCustomFields;
	}


	public void setAllCustomFields(boolean allCustomFields) {
		this.allCustomFields = allCustomFields;
	}


	public String getExceptionCustomFieldNames() {
		return this.exceptionCustomFieldNames;
	}


	public void setExceptionCustomFieldNames(String exceptionCustomFieldNames) {
		this.exceptionCustomFieldNames = exceptionCustomFieldNames;
	}
}
