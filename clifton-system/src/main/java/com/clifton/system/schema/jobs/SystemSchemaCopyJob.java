package com.clifton.system.schema.jobs;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.copy.SchemaFieldsCopyCommand;
import com.clifton.system.schema.copy.SystemSchemaCopyService;
import com.clifton.system.schema.search.SystemTableSearchForm;

import java.util.List;
import java.util.Map;


/**
 * The <code>SystemSchemaCopyJob</code> can be used to copy table/column information from the tables with the same name from one datasource to another
 *
 * @author manderson
 */
public class SystemSchemaCopyJob implements Task, ValidationAware, StatusHolderObjectAware<Status> {

	private SystemSchemaCopyService systemSchemaCopyService;

	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String fromDataSource;
	private String toDataSource;

	/**
	 * Optionally specify the comma delimited list of tables, OR copy all common tables
	 */
	private String tables;
	private boolean copyAllTables;

	private boolean copyTableLabelAndDescription;
	private boolean copyAuditType;
	private boolean copySecurityResource;
	private boolean copyUserInterfaceConfiguration;
	private boolean copyColumnInformation;
	private boolean overwriteExistingValues;

	private StatusHolderObject<Status> statusHolder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getFromDataSource(), "From Data Source is required.");
		ValidationUtils.assertNotEmpty(getToDataSource(), "To Data Source is required.");
		ValidationUtils.assertFalse(StringUtils.isEqualIgnoreCase(getFromDataSource(), getToDataSource()), "From and To Data Source cannot be the same.");
		if (!isCopyAllTables()) {
			ValidationUtils.assertNotEmpty(getTables(), "Specific Table(s) are required when not copying all tables");
		}
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolder.getStatus();
		status.setMessage(null);

		String copyTables = getCopyTables();

		SchemaFieldsCopyCommand copyCommand = new SchemaFieldsCopyCommand();
		copyCommand.setStatus(status);
		copyCommand.setFromDataSource(getFromDataSource());
		copyCommand.setToDataSource(getToDataSource());
		copyCommand.setTables(copyTables);

		copyCommand.setCopyTableLabelAndDescription(isCopyTableLabelAndDescription());
		copyCommand.setCopyAuditType(isCopyAuditType());
		copyCommand.setCopySecurityResource(isCopySecurityResource());
		copyCommand.setCopyUserInterfaceConfiguration(isCopyUserInterfaceConfiguration());
		copyCommand.setCopyColumnInformation(isCopyColumnInformation());
		copyCommand.setOverwriteExistingValues(isOverwriteExistingValues());


		getSystemSchemaCopyService().copySystemSchemaFields(copyCommand);
		return copyCommand.getStatus();
	}


	private String getCopyTables() {
		if (isCopyAllTables()) {
			SystemTableSearchForm toDataSourceSearchForm = new SystemTableSearchForm();
			toDataSourceSearchForm.setDataSourceId(getSystemSchemaService().getSystemDataSourceByName(getToDataSource()).getId());
			List<SystemTable> toTables = getSystemSchemaService().getSystemTableList(toDataSourceSearchForm);

			SystemTableSearchForm fromDataSourceSearchForm = new SystemTableSearchForm();
			fromDataSourceSearchForm.setDataSourceId(getSystemSchemaService().getSystemDataSourceByName(getFromDataSource()).getId());
			fromDataSourceSearchForm.setTableNames(BeanUtils.getPropertyValues(toTables, SystemTable::getName, String.class));
			List<SystemTable> fromTables = getSystemSchemaService().getSystemTableList(fromDataSourceSearchForm);

			if (CollectionUtils.isEmpty(fromTables)) {
				throw new ValidationException("No tables available to copy");
			}

			// Remove all whitespace
			return StringUtils.replace(AssertUtils.assertNotNull(StringUtils.collectionToCommaDelimitedString(fromTables, SystemTable::getName), "List of tables to copy is null."), " ", "");
		}
		return getTables();
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaCopyService getSystemSchemaCopyService() {
		return this.systemSchemaCopyService;
	}


	public void setSystemSchemaCopyService(SystemSchemaCopyService systemSchemaCopyService) {
		this.systemSchemaCopyService = systemSchemaCopyService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public String getFromDataSource() {
		return this.fromDataSource;
	}


	public void setFromDataSource(String fromDataSource) {
		this.fromDataSource = fromDataSource;
	}


	public String getToDataSource() {
		return this.toDataSource;
	}


	public void setToDataSource(String toDataSource) {
		this.toDataSource = toDataSource;
	}


	public String getTables() {
		return this.tables;
	}


	public void setTables(String tables) {
		this.tables = tables;
	}


	public boolean isCopyAllTables() {
		return this.copyAllTables;
	}


	public void setCopyAllTables(boolean copyAllTables) {
		this.copyAllTables = copyAllTables;
	}


	public boolean isCopyTableLabelAndDescription() {
		return this.copyTableLabelAndDescription;
	}


	public void setCopyTableLabelAndDescription(boolean copyTableLabelAndDescription) {
		this.copyTableLabelAndDescription = copyTableLabelAndDescription;
	}


	public boolean isCopyAuditType() {
		return this.copyAuditType;
	}


	public void setCopyAuditType(boolean copyAuditType) {
		this.copyAuditType = copyAuditType;
	}


	public boolean isCopySecurityResource() {
		return this.copySecurityResource;
	}


	public void setCopySecurityResource(boolean copySecurityResource) {
		this.copySecurityResource = copySecurityResource;
	}


	public boolean isCopyUserInterfaceConfiguration() {
		return this.copyUserInterfaceConfiguration;
	}


	public void setCopyUserInterfaceConfiguration(boolean copyUserInterfaceConfiguration) {
		this.copyUserInterfaceConfiguration = copyUserInterfaceConfiguration;
	}


	public boolean isCopyColumnInformation() {
		return this.copyColumnInformation;
	}


	public void setCopyColumnInformation(boolean copyColumnInformation) {
		this.copyColumnInformation = copyColumnInformation;
	}


	public boolean isOverwriteExistingValues() {
		return this.overwriteExistingValues;
	}


	public void setOverwriteExistingValues(boolean overwriteExistingValues) {
		this.overwriteExistingValues = overwriteExistingValues;
	}
}
