package com.clifton.system.schema.column.value;

/**
 * Lightweight object to hold both value and text representation for system column custom values
 * For use in {@link com.clifton.system.schema.column.cache.SystemColumnValueCache}
 *
 * @author MitchellF
 */
public class SystemColumnValueHolder {

	private Object value;
	private String text;


	public SystemColumnValueHolder() {
	}


	public SystemColumnValueHolder(Object value, String text) {
		this.value = value;
		this.text = text;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Object getValue() {
		return this.value;
	}


	public void setValue(Object value) {
		this.value = value;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}
}
