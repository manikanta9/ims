package com.clifton.system.schema.limit;

import com.clifton.system.schema.SystemTable;


/**
 * <code>SystemTableMaxQueryLimitRegistrator</code> update the maximum query limit on the DAO configurations from the system table upon startup.
 *
 * @author TerryS
 */
public interface SystemTableMaxQueryLimitRegistrator {

	public void updateMaximumQueryLimit(SystemTable table);
}
