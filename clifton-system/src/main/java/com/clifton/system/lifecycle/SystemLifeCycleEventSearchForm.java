package com.clifton.system.lifecycle;

/**
 * @author manderson
 */
public class SystemLifeCycleEventSearchForm {

	private String tableName;
	private Long entityId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Long getEntityId() {
		return this.entityId;
	}


	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
}
