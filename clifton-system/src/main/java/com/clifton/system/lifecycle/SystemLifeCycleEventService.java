package com.clifton.system.lifecycle;

import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * @author manderson
 */
public interface SystemLifeCycleEventService {


	@ModelAttribute("data")
	@SecureMethod(dynamicTableNameUrlParameter = "tableName")
	public List<SystemLifeCycleEvent> getSystemLifeCycleEventList(SystemLifeCycleEventSearchForm searchForm);
}
