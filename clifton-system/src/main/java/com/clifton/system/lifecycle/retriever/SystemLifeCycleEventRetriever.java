package com.clifton.system.lifecycle.retriever;

import com.clifton.core.beans.IdentityObject;
import com.clifton.system.lifecycle.SystemLifeCycleEvent;
import com.clifton.system.lifecycle.SystemLifeCycleEventContext;

import java.util.List;


/**
 * The <code>SystemLifeCycleEventRetriever</code> interface should be implemented by any area that applies to life cycle events to return the life of events for an entity
 *
 * @author manderson
 */
public interface SystemLifeCycleEventRetriever {


	/**
	 * Used to filter which retrievers apply to a specific entity
	 */
	public boolean isApplyToEntity(IdentityObject entity);


	public List<SystemLifeCycleEvent> getSystemLifeCycleEventListForEntity(SystemLifeCycleEventContext lifeCycleEventContext);
}
