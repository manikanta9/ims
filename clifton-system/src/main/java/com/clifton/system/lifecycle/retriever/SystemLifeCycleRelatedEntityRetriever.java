package com.clifton.system.lifecycle.retriever;

import com.clifton.core.beans.IdentityObject;

import java.util.List;


/**
 * The <code>SystemLifeCycleRelatedEntityRetriever</code> interface should be implemented by any area that applies to life cycle events and we need to include related entity information
 * i.e. Order Allocations include the OrderBlock (if available), the Order Block would include Placements (if availble) etc. in order to show an aggregate life cycle view
 *
 * @author manderson
 */
public interface SystemLifeCycleRelatedEntityRetriever {


	/**
	 * Used to filter which retrievers apply to a specific entity
	 */
	public boolean isApplyToEntity(IdentityObject entity);


	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity);
}
