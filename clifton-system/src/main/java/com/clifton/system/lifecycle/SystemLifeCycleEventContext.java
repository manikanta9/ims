package com.clifton.system.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * The <code>SystemLifeCycleEventContext</code> is used during system life cycle event retrievals to store common look ups
 * For example, looking up tables by name, or users by user name - these are often made by a small subset of values so we map them to speed up retrievals
 *
 * @author manderson
 */
public class SystemLifeCycleEventContext {

	/**
	 * The main entity we are getting the life cycle for
	 * Can be used to determine related entities and which we need to add or not
	 */
	private final String mainTableName;
	private final Long mainEntityId;

	private final Map<String, SecurityUser> securityUserByUserNameMap = new HashMap<>();
	private final Map<Short, SecurityUser> securityUserByIdMap = new HashMap<>();
	private final Map<String, SystemTable> systemTableByNameMap = new HashMap<>();

	private final Set<IdentityObject> retrievedEntitySet = new HashSet<>();


	// These are updated for each entity as they are being processed.  Some methods can take the object itself, others need the Table name
	private IdentityObject currentProcessingEntity;
	private String currentProcessingEntityTableName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemLifeCycleEventContext(String mainTableName, Long mainEntityId) {
		this.mainTableName = mainTableName;
		this.mainEntityId = mainEntityId;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isRetrieveEntity(IdentityObject entity) {
		return this.retrievedEntitySet.add(entity);
	}


	public void setCurrentProcessingEntity(IdentityObject currentProcessingEntity, String currentProcessingEntityTableName) {
		this.currentProcessingEntity = currentProcessingEntity;
		this.currentProcessingEntityTableName = currentProcessingEntityTableName;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUser getSecurityUserByName(SecurityUserService securityUserService, String userName) {
		if (StringUtils.isEmpty(userName)) {
			return null;
		}
		return this.securityUserByUserNameMap.computeIfAbsent(userName, key -> {
			SecurityUser securityUser = securityUserService.getSecurityUserByName(userName);
			// Add to ID map so we have it
			if (securityUser != null) {
				this.securityUserByIdMap.put(securityUser.getId(), securityUser);
			}
			return securityUser;
		});
	}


	public SecurityUser getSecurityUserById(SecurityUserService securityUserService, Short securityUserId) {
		if (MathUtils.isNullOrZero(securityUserId)) {
			return null;
		}
		return this.securityUserByIdMap.computeIfAbsent(securityUserId, key -> {
			SecurityUser securityUser = securityUserService.getSecurityUser(securityUserId);
			// Add to user name map so we have it
			if (securityUser != null) {
				this.securityUserByUserNameMap.put(securityUser.getUserName(), securityUser);
			}
			return securityUser;
		});
	}


	public SystemTable getSystemTableByName(SystemSchemaService systemSchemaService, String tableName) {
		if (StringUtils.isEmpty(tableName)) {
			return null;
		}
		return this.systemTableByNameMap.computeIfAbsent(tableName, key -> systemSchemaService.getSystemTableByName(tableName));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getMainTableName() {
		return this.mainTableName;
	}


	public Long getMainEntityId() {
		return this.mainEntityId;
	}


	public IdentityObject getCurrentProcessingEntity() {
		return this.currentProcessingEntity;
	}


	public String getCurrentProcessingEntityTableName() {
		return this.currentProcessingEntityTableName;
	}
}
