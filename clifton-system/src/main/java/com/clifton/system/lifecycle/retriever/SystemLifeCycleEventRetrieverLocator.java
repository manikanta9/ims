package com.clifton.system.lifecycle.retriever;

import com.clifton.core.beans.IdentityObject;

import java.util.Set;


/**
 * <code>SystemLifeCycleEventRetrieverLocator</code> Loads {@link SystemLifeCycleEventRetriever} implementations and is used to return a list of retrievers that apply for a given entity
 */
public interface SystemLifeCycleEventRetrieverLocator {


	public Set<SystemLifeCycleEventRetriever> locate(IdentityObject entity);
}
