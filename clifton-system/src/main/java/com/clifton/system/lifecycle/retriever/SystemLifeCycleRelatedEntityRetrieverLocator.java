package com.clifton.system.lifecycle.retriever;

import com.clifton.core.beans.IdentityObject;

import java.util.Set;


/**
 * <code>SystemLifeCycleRelatedEntityRetrieverLocator</code> Loads {@link SystemLifeCycleRelatedEntityRetriever} implementations and is used to return a list of related entities to include in the life cycle view
 */
public interface SystemLifeCycleRelatedEntityRetrieverLocator {


	public Set<SystemLifeCycleRelatedEntityRetriever> locate(IdentityObject entity);
}
