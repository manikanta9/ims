package com.clifton.system.lifecycle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleEventRetriever;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleEventRetrieverLocator;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetrieverLocator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
@Service
public class SystemLifeCycleEventServiceImpl implements SystemLifeCycleEventService {

	private DaoLocator daoLocator;

	private SystemLifeCycleEventRetrieverLocator systemLifeCycleEventRetrieverLocator;

	private SystemLifeCycleRelatedEntityRetrieverLocator systemLifeCycleRelatedEntityRetrieverLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemLifeCycleEvent> getSystemLifeCycleEventList(SystemLifeCycleEventSearchForm searchForm) {
		ValidationUtils.assertNotEmpty(searchForm.getTableName(), "Table Name is required");
		ValidationUtils.assertNotNull(searchForm.getEntityId(), "FKField ID is required");
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(searchForm.getTableName());
		IdentityObject entity = dao.findByPrimaryKey(dao.convertToPrimaryKeyDataType(searchForm.getEntityId()));
		ValidationUtils.assertNotNull(entity, "Cannot find entity for table " + searchForm.getTableName() + " and id " + searchForm.getEntityId());

		SystemLifeCycleEventContext lifeCycleEventContext = new SystemLifeCycleEventContext(searchForm.getTableName(), searchForm.getEntityId());

		List<SystemLifeCycleEvent> lifeCycleEventList = getSystemLifeCycleEventListForEntity(entity, lifeCycleEventContext);
		// second order by Event Source so that Workflow Transition for the insert is placed after Entity Insert
		return BeanUtils.sortWithFunctions(lifeCycleEventList, CollectionUtils.createList(SystemLifeCycleEvent::getEventDate, SystemLifeCycleEvent::getEventSource, SystemLifeCycleEvent::getSourceFkFieldId), CollectionUtils.createList(false, false, false));
	}


	private List<SystemLifeCycleEvent> getSystemLifeCycleEventListForEntity(IdentityObject entity, SystemLifeCycleEventContext lifeCycleEventContext) {
		List<SystemLifeCycleEvent> lifeCycleEventList = new ArrayList<>();

		if (lifeCycleEventContext.isRetrieveEntity(entity)) {
			lifeCycleEventContext.setCurrentProcessingEntity(entity, getDaoLocator().locate(entity).getConfiguration().getTableName());
			lifeCycleEventList.addAll(getSystemLifeCycleEventListForEntityImpl(lifeCycleEventContext));

			Set<SystemLifeCycleRelatedEntityRetriever> linkedEntityRetrieverSet = getSystemLifeCycleRelatedEntityRetrieverLocator().locate(entity);
			for (SystemLifeCycleRelatedEntityRetriever retriever : CollectionUtils.getIterable(linkedEntityRetrieverSet)) {
				for (IdentityObject relatedEntity : CollectionUtils.getIterable(retriever.getRelatedEntityListForEntity(entity))) {
					lifeCycleEventList.addAll(getSystemLifeCycleEventListForEntity(relatedEntity, lifeCycleEventContext));
				}
			}
		}
		return lifeCycleEventList;
	}


	private List<SystemLifeCycleEvent> getSystemLifeCycleEventListForEntityImpl(SystemLifeCycleEventContext lifeCycleEventContext) {
		List<SystemLifeCycleEvent> lifeCycleEventList = new ArrayList<>();
		Set<SystemLifeCycleEventRetriever> retrieverSet = getSystemLifeCycleEventRetrieverLocator().locate(lifeCycleEventContext.getCurrentProcessingEntity());
		for (SystemLifeCycleEventRetriever retriever : CollectionUtils.getIterable(retrieverSet)) {
			lifeCycleEventList.addAll(retriever.getSystemLifeCycleEventListForEntity(lifeCycleEventContext));
		}
		return lifeCycleEventList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemLifeCycleEventRetrieverLocator getSystemLifeCycleEventRetrieverLocator() {
		return this.systemLifeCycleEventRetrieverLocator;
	}


	public void setSystemLifeCycleEventRetrieverLocator(SystemLifeCycleEventRetrieverLocator systemLifeCycleEventRetrieverLocator) {
		this.systemLifeCycleEventRetrieverLocator = systemLifeCycleEventRetrieverLocator;
	}


	public SystemLifeCycleRelatedEntityRetrieverLocator getSystemLifeCycleRelatedEntityRetrieverLocator() {
		return this.systemLifeCycleRelatedEntityRetrieverLocator;
	}


	public void setSystemLifeCycleRelatedEntityRetrieverLocator(SystemLifeCycleRelatedEntityRetrieverLocator systemLifeCycleRelatedEntityRetrieverLocator) {
		this.systemLifeCycleRelatedEntityRetrieverLocator = systemLifeCycleRelatedEntityRetrieverLocator;
	}
}
