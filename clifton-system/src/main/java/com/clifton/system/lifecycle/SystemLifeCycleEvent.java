package com.clifton.system.lifecycle;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.schema.SystemTable;

import java.util.Date;


/**
 * A <code>SystemLifeCycleEvent</code> is a generic representation of an event that occured and can be attributed to an entity's life cycle.  For example audit events and workflow history
 *
 * @author manderson
 */
public class SystemLifeCycleEvent {

	/**
	 * Identifying information of the source of the record, i.e. Workflow History.WorkflowHistoryID, SystemAuditEvent.SystemAuditEventID
	 */
	private SystemTable sourceSystemTable;
	private Long sourceFkFieldId;

	/**
	 * The identifying information of the linked entity for the source.  i.e. The linked entity the audit trail history is for
	 */
	private SystemTable linkedSystemTable;
	private Long linkedFkFieldId;


	/**
	 * Event Source: Audit Trail, Workflow
	 */
	private String eventSource;

	/**
	 * Concise name of the action for the event, i.e. Entity Created, Updated
	 */
	private String eventAction;

	/**
	 * User friendly description of the event.
	 */
	private String eventDetails;

	/**
	 * When the event occurred, should include time and is used to sort all events into a timeline
	 */
	private Date eventDate;

	/**
	 * The user that is responsible for the event (create user, update user, etc.)
	 */
	private SecurityUser eventUser;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String toStringFormatted() {
		StringBuilder result = new StringBuilder(20);
		result.append("Source: ");
		if (getSourceSystemTable() != null) {
			result.append(getSourceSystemTable().getName());
			result.append(" [").append(getSourceFkFieldId()).append("]");
		}
		else {
			result.append("N/A");
		}
		result.append(StringUtils.TAB);
		result.append("Link: ");
		if (getLinkedSystemTable() != null) {
			result.append(getLinkedSystemTable().getName());
			result.append(" [").append(getLinkedFkFieldId()).append("]");
		}
		else {
			result.append("N/A");
		}
		result.append(StringUtils.TAB).append("Event Source: ").append(getEventSource());
		result.append(StringUtils.TAB).append("Action: ").append(getEventAction());
		result.append(StringUtils.TAB).append("Date: ").append(DateUtils.fromDate(getEventDate()));
		result.append(StringUtils.TAB).append("User: ").append(getEventUser() == null ? "Unknown" : getEventUser().getDisplayName());
		result.append(StringUtils.TAB).append("Details: ").append(getEventDetails());
		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemTable getSourceSystemTable() {
		return this.sourceSystemTable;
	}


	public void setSourceSystemTable(SystemTable sourceSystemTable) {
		this.sourceSystemTable = sourceSystemTable;
	}


	public Long getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Long sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public SystemTable getLinkedSystemTable() {
		return this.linkedSystemTable;
	}


	public void setLinkedSystemTable(SystemTable linkedSystemTable) {
		this.linkedSystemTable = linkedSystemTable;
	}


	public Long getLinkedFkFieldId() {
		return this.linkedFkFieldId;
	}


	public void setLinkedFkFieldId(Long linkedFkFieldId) {
		this.linkedFkFieldId = linkedFkFieldId;
	}


	public String getEventSource() {
		return this.eventSource;
	}


	public void setEventSource(String eventSource) {
		this.eventSource = eventSource;
	}


	public String getEventAction() {
		return this.eventAction;
	}


	public void setEventAction(String eventAction) {
		this.eventAction = eventAction;
	}


	public String getEventDetails() {
		return this.eventDetails;
	}


	public void setEventDetails(String eventDetails) {
		this.eventDetails = eventDetails;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public SecurityUser getEventUser() {
		return this.eventUser;
	}


	public void setEventUser(SecurityUser eventUser) {
		this.eventUser = eventUser;
	}
}
