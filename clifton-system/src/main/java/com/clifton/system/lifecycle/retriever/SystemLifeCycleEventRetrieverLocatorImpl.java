package com.clifton.system.lifecycle.retriever;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Component
public class SystemLifeCycleEventRetrieverLocatorImpl implements SystemLifeCycleEventRetrieverLocator, InitializingBean, ApplicationContextAware {


	private Set<SystemLifeCycleEventRetriever> retrieverSet = new HashSet<>();

	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		Map<String, SystemLifeCycleEventRetriever> beanMap = getApplicationContext().getBeansOfType(SystemLifeCycleEventRetriever.class);
		if (!CollectionUtils.isEmpty(beanMap)) {
			setRetrieverSet(new HashSet<>(beanMap.values()));
		}
	}


	@Override
	public Set<SystemLifeCycleEventRetriever> locate(IdentityObject entity) {
		return CollectionUtils.getStream(getRetrieverSet()).filter(retriever -> retriever.isApplyToEntity(entity)).collect(Collectors.toSet());
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Set<SystemLifeCycleEventRetriever> getRetrieverSet() {
		return this.retrieverSet;
	}


	public void setRetrieverSet(Set<SystemLifeCycleEventRetriever> retrieverSet) {
		this.retrieverSet = retrieverSet;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
