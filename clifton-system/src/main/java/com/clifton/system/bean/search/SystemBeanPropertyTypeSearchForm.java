package com.clifton.system.bean.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>SystemBeanPropertyTypeSearchForm</code> class defines search for SystemBeanPropertyType objects.
 *
 * @author vgomelsky
 */
public class SystemBeanPropertyTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField(searchField = "type.id")
	private Integer typeId;

	@SearchField(searchField = "beanList.id", searchFieldPath = "type", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer beanId;

	@SearchField(searchField = "systemPropertyName")
	private String systemPropertyName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public Integer getBeanId() {
		return this.beanId;
	}


	public void setBeanId(Integer beanId) {
		this.beanId = beanId;
	}


	public String getSystemPropertyName() {
		return this.systemPropertyName;
	}


	public void setSystemPropertyName(String systemPropertyName) {
		this.systemPropertyName = systemPropertyName;
	}
}
