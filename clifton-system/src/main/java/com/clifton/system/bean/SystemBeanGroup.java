package com.clifton.system.bean;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;


/**
 * The <code>SystemBeanGroup</code> class represents an interface used for a particular purpose that can have
 * multiple {@link SystemBeanType} implementations.
 * <p>
 * SystemBeanGroup objects should be system defined and cannot be modified via UI.  The name is usually referenced by
 * other parts of the system and cannot change.
 *
 * @author vgomelsky
 */
@CacheByName
public class SystemBeanGroup extends NamedEntity<Short> implements SystemEntityModifyConditionAwareAdminRequired {

	private String interfaceClassName;

	/**
	 * Optional alias that can be given to beans in this group.  If defined, beans will be called by this alias instead of beans.
	 * Can be used to make UI more user friendly.
	 * For example, "Batch Job" alias will result in "Batch Job" and "Batch Job Type" instead of "Bean" and "Bean Type".
	 */
	private String alias;

	/**
	 * For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to create/edit SystemBeans in this group
	 */
	private SystemCondition entityModifyCondition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return (getAlias() == null) ? getName() : getAlias();
	}


	public String getInterfaceClassName() {
		return this.interfaceClassName;
	}


	public void setInterfaceClassName(String interfaceClassName) {
		this.interfaceClassName = interfaceClassName;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}
}
