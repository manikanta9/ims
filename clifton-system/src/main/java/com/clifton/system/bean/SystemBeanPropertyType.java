package com.clifton.system.bean;


import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;
import com.clifton.system.userinterface.BaseNamedSystemUserInterfaceAwareVirtualEntity;
import com.clifton.system.userinterface.SystemUserInterfaceUsedByAware;
import com.clifton.system.userinterface.SystemUserInterfaceValueUsedByConfig;


/**
 * The <code>SystemBeanPropertyType</code> class describes a single bean property for the specified {@link SystemBeanType}.
 *
 * @author vgomelsky
 */
public class SystemBeanPropertyType extends BaseNamedSystemUserInterfaceAwareVirtualEntity<SystemBeanPropertyType> implements SystemUserInterfaceUsedByAware, SystemEntityModifyConditionAwareAdminRequired {

	private SystemBeanType type;

	/**
	 * Optional: if specified, value for this property type must be a bean that belongs to this group.
	 */
	private SystemBeanGroup valueGroup;

	/**
	 * The field name of the {@link SystemBeanType#className bean class} for this property for the associated {@link #type}.
	 */
	private String systemPropertyName;

	/**
	 * for arrays and lists
	 */
	private boolean multipleValuesAllowed;

	/**
	 * The property type that will trigger the look up of additional fields that can store values in this field.
	 * <p>
	 * For example, a bean that implements BaseSystemQueryBeanPropertyProvider this property link to
	 * the systemQueryId property that would trigger the look up of the system query properties.
	 */
	private SystemBeanPropertyType triggerPropertyType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getType() != null) {
			return getType().getEntityModifyCondition();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////  Used By Support  /////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemUserInterfaceValueUsedByConfig getSystemUserInterfaceValueConfig() {
		return new SystemUserInterfaceValueUsedByConfig("SystemBeanProperty", "value", "type", "bean", "SystemBean", null);
	}


	@Override
	public String getUsedByLabel() {
		if (getType() != null) {
			return "Bean Property Type: " + getType().getLabel() + "." + getLabel();
		}
		return "Bean Property Type: [UNKNOWN BEAN TYPE] " + getLabel();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanType getType() {
		return this.type;
	}


	public void setType(SystemBeanType type) {
		this.type = type;
	}


	public String getSystemPropertyName() {
		return this.systemPropertyName;
	}


	public void setSystemPropertyName(String systemPropertyName) {
		this.systemPropertyName = systemPropertyName;
	}


	public SystemBeanGroup getValueGroup() {
		return this.valueGroup;
	}


	public void setValueGroup(SystemBeanGroup valueGroup) {
		this.valueGroup = valueGroup;
	}


	public boolean isMultipleValuesAllowed() {
		return this.multipleValuesAllowed;
	}


	public void setMultipleValuesAllowed(boolean multipleValuesAllowed) {
		this.multipleValuesAllowed = multipleValuesAllowed;
	}


	public SystemBeanPropertyType getTriggerPropertyType() {
		return this.triggerPropertyType;
	}


	public void setTriggerPropertyType(SystemBeanPropertyType triggerPropertyType) {
		this.triggerPropertyType = triggerPropertyType;
	}
}
