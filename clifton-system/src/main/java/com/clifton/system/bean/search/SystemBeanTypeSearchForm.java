package com.clifton.system.bean.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>SystemBeanTypeSearchForm</code> class defines search configuration for SystemBeanType objects.
 *
 * @author vgomelsky
 */
public class SystemBeanTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,group.name,className")
	private String searchPattern;

	@SearchField(searchField = "group.id", comparisonConditions = {ComparisonConditions.EQUALS})
	private Short groupId;

	@SearchField(searchField = "name", searchFieldPath = "group", comparisonConditions = {ComparisonConditions.EQUALS})
	private String groupName;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String className;

	@SearchField
	private Boolean singleton;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public String getGroupName() {
		return this.groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getClassName() {
		return this.className;
	}


	public void setClassName(String className) {
		this.className = className;
	}


	public Boolean getSingleton() {
		return this.singleton;
	}


	public void setSingleton(Boolean singleton) {
		this.singleton = singleton;
	}
}
