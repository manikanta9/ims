package com.clifton.system.bean.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class SystemBeanGroupSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Short id;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


}
