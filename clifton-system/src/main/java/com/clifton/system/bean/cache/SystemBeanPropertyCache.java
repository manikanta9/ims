package com.clifton.system.bean.cache;


import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.system.bean.SystemBeanProperty;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemBeanPropertyCache</code> class provides caching and retrieval from cache of the list of properties for
 * SystemBean objects by id.
 * <p/>
 * The class also implements {@link DaoEventObserver} and clears the cache whenever update methods to related DTO's happen.
 * Make sure to register this class as an observer to corresponding SystemBean DAO.
 *
 * @author manderson
 */
@Component
public class SystemBeanPropertyCache extends SelfRegisteringSingleKeyDaoListCache<SystemBeanProperty, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "bean.id";
	}


	@Override
	protected Integer getBeanKeyValue(SystemBeanProperty bean) {
		if (bean.getBean() != null) {
			return bean.getBean().getId();
		}
		return null;
	}
}
