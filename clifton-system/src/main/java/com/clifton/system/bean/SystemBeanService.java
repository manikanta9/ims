package com.clifton.system.bean;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.system.bean.search.SystemBeanGroupSearchForm;
import com.clifton.system.bean.search.SystemBeanPropertyTypeSearchForm;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.system.bean.search.SystemBeanTypeSearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The <code>SystemBeanService</code> class defines methods for working with system beans.
 * This is a schema that allows simple and context managed beans to be defined in the database.
 *
 * @author vgomelsky
 */
public interface SystemBeanService {

	/**
	 * Returns a fully populated instance of the specified bean.
	 * Bean's class is determined from bean's schema. Corresponding property values are populated from the data source.
	 */
	@DoNotAddRequestMapping
	public Object getBeanInstance(SystemBean bean);


	/**
	 * Returns fully populated instance of the specified bean.
	 * Property values are populated from the specified list.
	 */
	@DoNotAddRequestMapping
	public Object getBeanInstance(SystemBean bean, List<SystemBeanProperty> beanPropertyValues);


	@DoNotAddRequestMapping
	public Object getBeanInstanceWithMergedConfiguration(Integer... systemBeanIds);

	////////////////////////////////////////////////////////////////////////////
	////////             SystemBean Business Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a SystemBean for the specified id populated with its properties.
	 */
	public SystemBean getSystemBean(int id);


	/**
	 * Returns SystemBean for the specified name
	 * Note: doesn't populate the bean with properties
	 */
	public SystemBean getSystemBeanByName(String name);


	public List<SystemBean> getSystemBeanList(SystemBeanSearchForm searchForm);


	/**
	 * Saves the specified SystemBean and its properties.  Validates property values to make sure that
	 * all required properties are populated, etc.
	 */
	public SystemBean saveSystemBean(SystemBean bean);


	/**
	 * Saves the specified SystemBean and its properties.
	 * Ignores validation - used for json migrations
	 */
	@DoNotAddRequestMapping
	public SystemBean saveSystemBeanNoValidation(SystemBean bean);


	/**
	 * Ability to just rename the system bean
	 * Example: For Investment Instructions - the name is dependent on the Investment Instruction Definition ID so is
	 * updated after being created.  Don't need to re-save entire system bean with properties, nor validate everything again, so just
	 * update the name
	 */
	@DoNotAddRequestMapping
	public void renameSystemBean(int id, String name);


	/**
	 * Makes a copy of the SystemBean with a new name and optional descriptionOverride
	 */
	public SystemBean copySystemBean(int id, String name, String descriptionOverride);


	public void deleteSystemBean(int id);


	@DoNotAddRequestMapping
	public List<SystemBean> getDuplicateBeans(SystemBean bean, List<SystemBean> comparingBeanList);

	////////////////////////////////////////////////////////////////////////////
	////////           SystemBeanType Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanType getSystemBeanType(int id);


	public SystemBeanType getSystemBeanTypeByName(String name);


	public List<SystemBeanType> getSystemBeanTypeList(SystemBeanTypeSearchForm searchForm);


	public SystemBeanType saveSystemBeanType(SystemBeanType bean);


	////////////////////////////////////////////////////////////////////////////
	////////           SystemBeanGroup Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanGroup getSystemBeanGroup(short id);


	public SystemBeanGroup getSystemBeanGroupByName(String name);


	public List<SystemBeanGroup> getSystemBeanGroupList(SystemBeanGroupSearchForm searchForm);


	public SystemBeanGroup saveSystemBeanGroup(SystemBeanGroup bean);


	////////////////////////////////////////////////////////////////////////////
	////////           SystemBeanProperty Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanProperty getSystemBeanProperty(int id);


	@ModelAttribute("data")
	public List<SystemBeanProperty> getSystemBeanPropertyListByBeanId(int beanId);


	/**
	 * Gets a list of <code>SystemBeanProperty</code> objects for the specific bean.
	 * <p>
	 * NOTE: This list will include properties returned by SystemBeanPropertyProviderBean.getPropertyList call
	 * if the bean implements that interface.
	 */
	public List<SystemBeanProperty> getSystemBeanPropertyListByBean(SystemBean bean);


	////////////////////////////////////////////////////////////////////////////
	////////           SystemBeanPropertyType Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanPropertyType getSystemBeanPropertyType(int id);


	public List<SystemBeanPropertyType> getSystemBeanPropertyTypeListByType(int typeId);


	/**
	 * Gets a list of <code>SystemBeanPropertyType</code> objects for the specific bean.
	 * <p>
	 * NOTE: This list will include properties returned by SystemBeanPropertyProviderBean.getPropertyTypeList call
	 * if the bean implements that interface.
	 */
	@ModelAttribute("data")
	public List<SystemBeanPropertyType> getSystemBeanPropertyTypeListByBean(SystemBean bean);


	public List<SystemBeanPropertyType> getSystemBeanPropertyTypeList(SystemBeanPropertyTypeSearchForm searchForm);


	public SystemBeanPropertyType saveSystemBeanPropertyType(SystemBeanPropertyType bean);


	/**
	 * Allows saving the system bean property type while bypassing some validation ( regarding value list table and value list URL)
	 */
	public SystemBeanPropertyType saveSystemBeanPropertyTypeNoValidation(SystemBeanPropertyType bean);
}
