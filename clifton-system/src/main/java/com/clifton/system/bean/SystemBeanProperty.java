package com.clifton.system.bean;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.ManyToOneEntity;
import com.clifton.security.secret.SecretAware;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;
import com.clifton.system.userinterface.SystemUserInterfaceMultiValueAware;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The <code>SystemBeanProperty</code> class defines a specific instance (value) of the specified property for the specified bean.
 *
 * @author vgomelsky
 */
public class SystemBeanProperty extends BaseEntity<Integer> implements SystemEntityModifyConditionAwareAdminRequired, SystemUserInterfaceMultiValueAware<SystemBeanPropertyType>, LabeledObject, SecretAware {

	@ManyToOneEntity
	private SystemBean bean;
	private SystemBeanPropertyType type;
	// the value could be either a simple value (String, Number, Date, etc.) or another bean
	private String value;
	// optional text is used to store text (label), usually when combos are used for input
	private String text;
	private SystemBean beanValue;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemBeanPropertyType getParameter() {
		return getType();
	}


	public void setParameter(SystemBeanPropertyType parameter) {
		setType(parameter);
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getBean() != null) {
			return getBean().getEntityModifyCondition();
		}
		return null;
	}


	@Override
	public String getLabel() {
		StringBuilder lbl = new StringBuilder(16);
		if (getBean() != null) {
			lbl.append(getBean().getName());
		}
		if (getType() != null) {
			lbl.append(" - ").append(getType().getName());
		}
		return lbl.toString();
	}


	@Override
	public boolean isMultipleValuesAllowed() {
		return getType().isMultipleValuesAllowed();
	}


	@Override
	public List<Integer> getSecuritySecretIds() {
		if (getValue() != null && getType().getVirtualTypeId() == null && getType().getDataType() != null && getType().getDataType().getName().equals(SystemDataType.SECRET)) {
			return new ArrayList<>(Collections.singletonList(Integer.valueOf(getValue())));
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBean getBean() {
		return this.bean;
	}


	public void setBean(SystemBean bean) {
		this.bean = bean;
	}


	public SystemBeanPropertyType getType() {
		return this.type;
	}


	public void setType(SystemBeanPropertyType type) {
		this.type = type;
	}


	@Override
	public String getValue() {
		return this.value;
	}


	@Override
	public void setValue(String value) {
		this.value = value;
	}


	public SystemBean getBeanValue() {
		return this.beanValue;
	}


	public void setBeanValue(SystemBean beanValue) {
		this.beanValue = beanValue;
	}


	@Override
	public String getText() {
		return this.text;
	}


	@Override
	public void setText(String text) {
		this.text = text;
	}
}
