package com.clifton.system.bean;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;
import com.clifton.system.userinterface.SystemUserInterfacePropertyProvider;

import java.util.List;


/**
 * The <code>SystemBeanType</code> class represents a specific implementation class that belongs to a {@link SystemBeanGroup}.
 * The class must implement the interface of linked {@link SystemBeanGroup}.
 *
 * @author vgomelsky
 */
public class SystemBeanType extends NamedEntity<Integer> implements SystemEntityModifyConditionAwareAdminRequired {

	private SystemBeanGroup group;
	private String className;
	/**
	 * Identifies whether instances of this bean are of "singleton" scope:
	 * thread safe, can be shared and cached for improved performance and memory utilization.
	 */
	private boolean singleton;

	/**
	 * Specified whether or not multiple beans of this type with identical properties may be saved.
	 */
	private boolean duplicateBeansAllowed;

	/**
	 * For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to create/edit
	 */
	private SystemCondition overrideEntityModifyCondition;

	@OneToManyEntity(serviceBeanName = "systemBeanService", serviceMethodName = "getSystemBeanPropertyTypeListByType")
	private List<SystemBeanPropertyType> propertyTypeList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (this.overrideEntityModifyCondition != null) {
			return this.overrideEntityModifyCondition;
		}
		if (getGroup() != null) {
			return getGroup().getEntityModifyCondition();
		}
		return null;
	}


	public boolean isBeanClassPropertyProvider() {
		if (!StringUtils.isEmpty(getClassName())) {
			Class<?> beanClass = CoreClassUtils.getClass(getClassName());
			return SystemUserInterfacePropertyProvider.class.isAssignableFrom(beanClass);
		}
		return false;
	}


	@Override
	public String getLabel() {
		if (this.group == null) {
			return getName();
		}
		return getName() + " (" + this.group.getName() + ')';
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanGroup getGroup() {
		return this.group;
	}


	public void setGroup(SystemBeanGroup group) {
		this.group = group;
	}


	public String getClassName() {
		return this.className;
	}


	public void setClassName(String className) {
		this.className = className;
	}


	public boolean isSingleton() {
		return this.singleton;
	}


	public void setSingleton(boolean singleton) {
		this.singleton = singleton;
	}


	public boolean isDuplicateBeansAllowed() {
		return this.duplicateBeansAllowed;
	}


	public void setDuplicateBeansAllowed(boolean duplicateBeansAllowed) {
		this.duplicateBeansAllowed = duplicateBeansAllowed;
	}


	public List<SystemBeanPropertyType> getPropertyTypeList() {
		return this.propertyTypeList;
	}


	public void setPropertyTypeList(List<SystemBeanPropertyType> propertyTypeList) {
		this.propertyTypeList = propertyTypeList;
	}


	public SystemCondition getOverrideEntityModifyCondition() {
		return this.overrideEntityModifyCondition;
	}


	public void setOverrideEntityModifyCondition(SystemCondition overrideEntityModifyCondition) {
		this.overrideEntityModifyCondition = overrideEntityModifyCondition;
	}
}
