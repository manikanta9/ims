package com.clifton.system.bean.processor;

import com.clifton.system.bean.SystemBeanProperty;


/**
 * Handles the processing of system bean properties
 *
 * @author theodorez
 */
public interface SystemBeanPropertyProcessor {

	public SystemBeanProperty processProperty(SystemBeanProperty property);
}
