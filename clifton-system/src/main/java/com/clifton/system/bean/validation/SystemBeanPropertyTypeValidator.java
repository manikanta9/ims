package com.clifton.system.bean.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.bean.SystemBeanPropertyType;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>SystemBeanPropertyTypeValidator</code> class validates UPDATES to only those fields that
 * are allowed: name, description, order, valueTable, valueListUrl, userInterfaceConfig
 *
 * @author vgomelsky
 */
@Component
public class SystemBeanPropertyTypeValidator extends SelfRegisteringDaoValidator<SystemBeanPropertyType> {

	private static final String[] ALLOWED_PROPERTIES = new String[]{"usedByLabel", "label", "name", "description", "order", "valueTable", "valueListUrl", "userInterfaceConfig"};


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemBeanPropertyType bean, DaoEventTypes config) throws ValidationException {
		if (config.isUpdate()) {
			SystemBeanPropertyType originalBean = getOriginalBean(bean);
			// also exclude calculated getters: usedByLabel, label
			List<String> diffProps = CoreCompareUtils.getNoEqualProperties(bean, originalBean, false, ALLOWED_PROPERTIES);
			if (!CollectionUtils.isEmpty(diffProps)) {
				throw new ValidationException("Cannot update " + diffProps + " fields for bean property type. Changes to these fields are not allowed.");
			}
		}
		else {
			throw new ValidationException("Inserts and deletes are not allowed for System Bean Property Type objects.");
		}
	}
}
