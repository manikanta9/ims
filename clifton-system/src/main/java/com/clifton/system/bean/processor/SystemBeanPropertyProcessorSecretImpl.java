package com.clifton.system.bean.processor;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.encryption.EncryptionUtilsAES;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretService;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemSchemaService;


/**
 * Handles processing of properties with a secret data type
 *
 * @author theodorez
 */
public class SystemBeanPropertyProcessorSecretImpl implements SystemBeanPropertyProcessor {


	private SecuritySecretService securitySecretService;
	private SystemSchemaService systemSchemaService;


	/**
	 * On save the UI submits the value as the SecretID and the Text is the plaintext password.
	 * <p>
	 * Create or update a secret as
	 */
	@Override
	public SystemBeanProperty processProperty(SystemBeanProperty property) {
		if (property.getType().getDataType().getName().equals(SystemDataType.SECRET) && (!StringUtils.isEmpty(property.getValue()) || !StringUtils.isEmpty(property.getText()))) {
			SecuritySecret secret;
			//If value is empty, must be a new entry
			if (StringUtils.isEmpty(property.getValue())) {
				secret = new SecuritySecret();
			}
			else {
				try {
					secret = getSecuritySecretService().getSecuritySecret(new Integer(property.getValue()));
				}
				catch (Exception e) {
					throw new RuntimeException("Error occurred getting security secret: ", e);
				}
			}
			secret.setSecretString(property.getText());
			getSecuritySecretService().saveSecuritySecret(secret);
			property.setValue(secret.getId().toString());
			property.setText(EncryptionUtilsAES.ENCRYPTED_STRING_PLACEHOLDER);
		}

		return property;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
