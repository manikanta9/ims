package com.clifton.system.bean.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.bean.SystemBeanGroup;

import java.util.List;


/**
 * The <code>SystemBeanGroupValidator</code> validates that:
 * <p>
 * 1. Inserts are not allowed
 * 2. Deletes are not allowed
 * 3. Updates can modify description and entityModifyCondition only
 *
 * @author manderson
 */
public class SystemBeanGroupValidator extends SelfRegisteringDaoValidator<SystemBeanGroup> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemBeanGroup bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert()) {
			throw new ValidationException("Creating new system bean groups is not allowed.");
		}
		if (config.isDelete()) {
			throw new ValidationException("Deleting system bean groups is not allowed.");
		}

		// Otherwise an update - only two fields that can be modified are description and entity modify condition
		SystemBeanGroup originalBean = getOriginalBean(bean);
		List<String> diffs = CoreCompareUtils.getNoEqualProperties(bean, originalBean, false, "description", "entityModifyCondition");
		if (diffs != null) {
			if (!diffs.isEmpty()) {
				throw new FieldValidationException("Cannot update field '" + diffs.get(0) + "' for system bean group [" + bean.getName() + "]", diffs.get(0));
			}
		}
	}
}
