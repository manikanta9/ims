package com.clifton.system.bean.jobs;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.web.mvc.WebMethodHandler;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.userinterface.SystemUserInterfaceAwareUtils;
import com.clifton.system.userinterface.SystemUserInterfaceMultiValueAware;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * The <code>SystemBeanPropertyLabelUpdaterJob</code> is used to update the display fields for SystemBeanProperties, since they are
 * not automatically updated when a target entity changes. Target entities may be updated to reflect new/different functionality, and
 * this should also be reflected in the SystemBeanProperty display labels. For example, Compliance Rules often update System Bean properties
 * such as "Opening Date Filtering: On or After (01/14/2019)" to "Opening Date Filtering: On or After (01/31/2019)".
 *
 * @author lnaylor
 */
public class SystemBeanPropertyLabelUpdaterJob implements Task, ValidationAware {

	private static final String STATUS_CATEGORY_UPDATED = "Updated";

	private List<Short> includeBeanGroupIdList;

	private List<Integer> includeBeanTypeIdList;

	private List<Integer> excludeBeanTypeIdList;

	private SystemBeanService systemBeanService;

	private SystemSchemaService systemSchemaService;

	private ApplicationContextService applicationContextService;

	private DaoLocator daoLocator;

	private WebMethodHandler webMethodHandler;

	/**
	 * If set to true, the job will not update any displays; it will only show which displays would be updated if the job were run with dryRun set to false
	 */
	private boolean dryRun;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {

		Status status = Status.ofEmptyMessage();
		List<SystemBean> systemBeanList = getFilteredSystemBeanList();

		for (SystemBean bean : systemBeanList) {
			try {
				processSystemBean(bean, status);
			}
			catch (Exception e) {
				status.addError(String.format("[%s] (%d): Error processing: %s", StringUtils.formatStringUpToNCharsWithDots(bean.getNameWithoutAutoGeneratedPrefix(), 50), bean.getId(), e));
				status.incrementStatusCount(StatusDetail.CATEGORY_SKIPPED);
			}
		}
		String dryRunMessage = isDryRun() ? "(DRY RUN). " : "";
		String messagePrefix = String.format("%sProcessing complete. [%d] beans processed. [%d] beans updated. [%d] beans unchanged. [%d] errors. See system audit trail for more details.%n%nUpdated properties (bean ID):%n%n",
				dryRunMessage, CollectionUtils.getSize(systemBeanList), status.getDetailListForCategory(STATUS_CATEGORY_UPDATED).size(), status.getStatusCount(StatusDetail.CATEGORY_SKIPPED), status.getErrorCount());
		status.setMessage(status.getDetailList().stream()
				.filter(detail -> detail.getCategory().equals(STATUS_CATEGORY_UPDATED))
				.map(StatusDetail::getNote).distinct()
				.collect(Collectors.joining("\n\n", messagePrefix, "\n\nErrors:")));
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private void processSystemBean(SystemBean bean, Status status) {
		boolean beanValueChanged = false;
		List<String> updatedPropertyNames = new ArrayList<>();

		List<SystemBeanProperty> propertyList = getSystemBeanService().getSystemBeanPropertyListByBeanId(bean.getId());

		for (SystemBeanProperty property : CollectionUtils.getIterable(propertyList)) {
			if (!isPropertyEligibleForUpdate(property)) {
				continue;
			}

			String updatedDisplayText = getUpToDateDisplayValue(property, status);
			if (!StringUtils.isEqual(property.getText(), updatedDisplayText)) {
				if (!isDryRun()) {
					property.setText(updatedDisplayText);
				}
				updatedPropertyNames.add(property.getType().getName());
				beanValueChanged = true;
			}
		}


		if (beanValueChanged) {
			String beanNameTrimmed = StringUtils.formatStringUpToNCharsWithDots(bean.getNameWithoutAutoGeneratedPrefix(), DataTypes.NAME.getLength(), true);
			String updateMsgPrefix = String.format("[%s] (%d): ", beanNameTrimmed, bean.getId());
			String updateMsg = updatedPropertyNames.stream()
					.map(propertyName -> "[" + propertyName + "]")
					.collect(Collectors.joining(", ", updateMsgPrefix, ""));
			try {
				if (!isDryRun()) {
					bean.setPropertyList(propertyList);
					getSystemBeanService().saveSystemBean(bean);
				}
				status.addDetail(STATUS_CATEGORY_UPDATED, updateMsg);
			}
			catch (Exception e) {
				status.addError(String.format("%s: Error saving: %s", updateMsg, ExceptionUtils.getOriginalException(e)));
				status.incrementStatusCount(StatusDetail.CATEGORY_SKIPPED);
			}
		}

		if (!beanValueChanged) {
			status.incrementStatusCount(StatusDetail.CATEGORY_SKIPPED);
		}
	}


	private String getUpToDateDisplayValue(SystemBeanProperty property, Status status) {
		if (StringUtils.isEmpty(property.getValue())) {
			return property.getValue();
		}

		List<String> valueList = CollectionUtils.createList(property.getValue());
		List<String> textList = CollectionUtils.createList(property.getText());

		if (property.isMultipleValuesAllowed()) {
			valueList = StringUtils.delimitedStringToList(property.getValue(), SystemUserInterfaceMultiValueAware.MULTI_VALUE_LIST_DELIMITER);
			textList = StringUtils.delimitedStringToList(property.getText(), SystemUserInterfaceMultiValueAware.MULTI_VALUE_LIST_DELIMITER);
		}

		SystemTable valueTable = ObjectUtils.coalesce(property.getType().getValueTable(), getSystemSchemaService().getSystemTableByName(SystemBean.class.getSimpleName()));
		String valueField = StringUtils.coalesce(SystemUserInterfaceAwareUtils.getComboValueField(property.getType()), "id");
		String displayField = StringUtils.coalesce(SystemUserInterfaceAwareUtils.getComboDisplayField(property.getType()), "name");

		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(valueTable.getName());
		Class<?> valueClass = getValueClass(dao, valueField);

		List<String> updatedTextList = new ArrayList<>();

		for (int i = 0; i < CollectionUtils.getSize(valueList); i++) {
			IdentityObject identityObject = getEntity(dao, valueClass, valueField, Objects.requireNonNull(valueList).get(i), property, status);
			if (identityObject != null) {
				try {
					updatedTextList.add(BeanUtils.getPropertyValue(identityObject, displayField).toString());
				}
				catch (Exception e) {
					status.addError(String.format("[%s] (%d): Error getting current value of entity for [%s] with [%s=%s]", StringUtils.formatStringUpToNCharsWithDots(property.getBean().getNameWithoutAutoGeneratedPrefix(), DataTypes.NAME.getLength()), property.getBean().getId(), property.getType().getName(), valueField, valueList.get(i)));
					if (CollectionUtils.getSize(textList) > i) {
						updatedTextList.add(Objects.requireNonNull(textList).get(i));
					}
				}
			}
			else if (CollectionUtils.getSize(textList) > i) {
					updatedTextList.add(Objects.requireNonNull(textList).get(i));
			}

		}
		return StringUtils.collectionToDelimitedString(updatedTextList, SystemUserInterfaceMultiValueAware.MULTI_VALUE_LIST_DELIMITER);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private IdentityObject getEntity(ReadOnlyDAO<IdentityObject> dao, Class<?> valueClass, String valueField, String value, SystemBeanProperty property, Status status) {
		List<IdentityObject> identityObjectList = getIdentityObjectList(dao, valueField, value, valueClass, property, status);

		if (CollectionUtils.getSize(identityObjectList) > 1) {
			identityObjectList = getIdentityObjectListFromValueListUrl(property.getType(), valueField, value);

			if (CollectionUtils.getSize(identityObjectList) > 1) {
				status.addError(String.format("[%s] (%d): Multiple entities found for [%s] with [%s=%s]", StringUtils.formatStringUpToNCharsWithDots(property.getBean().getNameWithoutAutoGeneratedPrefix(), DataTypes.NAME.getLength()), property.getBean().getId(), property.getType().getName(), valueField, value));
				return null;
			}
		}

		if (CollectionUtils.isEmpty(identityObjectList)) {
			status.addError(String.format("[%s] (%d): No entities found for [%s] with [%s=%s]", StringUtils.formatStringUpToNCharsWithDots(property.getBean().getNameWithoutAutoGeneratedPrefix(), DataTypes.NAME.getLength()), property.getBean().getId(), property.getType().getName(), valueField, value));
			return null;
		}

		return CollectionUtils.getOnlyElement(identityObjectList);
	}


	private List<IdentityObject> getIdentityObjectList(ReadOnlyDAO<IdentityObject> dao, String valueField, String value, Class<?> valueClass, SystemBeanProperty property, Status status) {
		List<IdentityObject> identityObjectList;
		try {
			if (valueField.contains(".") || valueClass == null || dao == null) {
				identityObjectList = getIdentityObjectListFromValueListUrl(property.getType(), valueField, value);
			}
			else if (valueClass.isAssignableFrom(Short.class)) {
				identityObjectList = dao.findByField(valueField, Short.valueOf(value));
			}
			else if (valueClass.isAssignableFrom(Integer.class)) {
				identityObjectList = dao.findByField(valueField, Integer.valueOf(value));
			}
			else if (valueClass.isAssignableFrom(Long.class)) {
				identityObjectList = dao.findByField(valueField, Long.valueOf(value));
			}
			else {
				identityObjectList = dao.findByField(valueField, value);
			}
		}
		catch (Exception e) {
			status.addError(String.format("[%s] (%d): Error looking up entity for [%s]  with [%s=%s]", StringUtils.formatStringUpToNCharsWithDots(property.getBean().getNameWithoutAutoGeneratedPrefix(), DataTypes.NAME.getLength()), property.getBean().getId(), property.getType().getName(), valueField, value));
			return new ArrayList<>();
		}
		return identityObjectList;
	}


	private List<IdentityObject> getIdentityObjectListFromValueListUrl(SystemBeanPropertyType propertyType, String valueField, Object value) {
		String url = SystemUserInterfaceAwareUtils.getComboLookupURL(propertyType);
		if (StringUtils.isEmpty(url)) {
			return new ArrayList<>();
		}
		List<IdentityObject> identityObjectList = getWebMethodHandler().executeServiceCall(url, null);
		if (CollectionUtils.isEmpty(identityObjectList)) {
			return new ArrayList<>();
		}

		return CollectionUtils.getFiltered(identityObjectList, identityObject -> String.valueOf(BeanUtils.getPropertyValue(identityObject, valueField)).equals(value));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<SystemBean> getFilteredSystemBeanList() {
		SystemBeanSearchForm systemBeanSearchForm = new SystemBeanSearchForm();

		systemBeanSearchForm.setIncludeGroupIds(CollectionUtils.toArrayOrNull(getIncludeBeanGroupIdList(), Short.class));
		systemBeanSearchForm.setIncludeTypeIds(CollectionUtils.toArrayOrNull(getIncludeBeanTypeIdList(), Integer.class));
		systemBeanSearchForm.setExcludeTypeIds(CollectionUtils.toArrayOrNull(getExcludeBeanTypeIdList(), Integer.class));

		return getSystemBeanService().getSystemBeanList(systemBeanSearchForm);
	}


	private Class<?> getValueClass(ReadOnlyDAO<IdentityObject> dao, String valueField) {
		Class<?> valueClass;
		try {
			valueClass = BeanUtils.getPropertyType(dao.newBean(), valueField);
		}
		catch (Exception e) {
			valueClass = null; //Entity will be looked up using valueUrl instead of dao
		}
		return valueClass;
	}

	private boolean isPropertyEligibleForUpdate(SystemBeanProperty property) {
		return (property.getType().getValueTable() != null || property.getType().getValueGroup() != null || SystemUserInterfaceAwareUtils.isSystemListItemLookup(property.getType()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		if (getIncludeBeanGroupIdList() != null || getIncludeBeanTypeIdList() != null) {
			ValidationUtils.assertMutuallyExclusive("Only one of 'Include Bean Group(s)' and 'Include Bean Type(s)' may be specified.", getIncludeBeanGroupIdList(), getIncludeBeanTypeIdList());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<Short> getIncludeBeanGroupIdList() {
		return this.includeBeanGroupIdList;
	}


	public void setIncludeBeanGroupIdList(List<Short> includeBeanGroupIdList) {
		this.includeBeanGroupIdList = includeBeanGroupIdList;
	}


	public List<Integer> getIncludeBeanTypeIdList() {
		return this.includeBeanTypeIdList;
	}


	public void setIncludeBeanTypeIdList(List<Integer> includeBeanTypeIdList) {
		this.includeBeanTypeIdList = includeBeanTypeIdList;
	}


	public List<Integer> getExcludeBeanTypeIdList() {
		return this.excludeBeanTypeIdList;
	}


	public void setExcludeBeanTypeIdList(List<Integer> excludeBeanTypeIdList) {
		this.excludeBeanTypeIdList = excludeBeanTypeIdList;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public WebMethodHandler getWebMethodHandler() {
		return this.webMethodHandler;
	}


	public void setWebMethodHandler(WebMethodHandler webMethodHandler) {
		this.webMethodHandler = webMethodHandler;
	}


	public boolean isDryRun() {
		return this.dryRun;
	}


	public void setDryRun(boolean dryRun) {
		this.dryRun = dryRun;
	}
}
