package com.clifton.system.bean.rebuild;

import com.clifton.core.util.status.Status;
import com.clifton.system.bean.SystemBeanGroup;


/**
 * The <code>EntityGroupRebuildAwareExecutor</code> is a {@link SystemBeanGroup} that defines beans that
 * can be used to rebuild associated entities mapped to a <code>EntityGroupRebuildAware</code> entity.
 *
 * @author NickK
 * @see EntityGroupRebuildAware
 */
public interface EntityGroupRebuildAwareExecutor {

	/**
	 * Execute the rebuild for the specified group entity.
	 *
	 * @param groupEntity group entity to rebuild associated entity mappings
	 * @param status statuses will be added to this object and returned, if status is null a new one will be created and returned
	 */
	public Status executeRebuild(EntityGroupRebuildAware groupEntity, Status status);


	/**
	 * Validates the entity group that will be affected by the bean during rebuild.
	 *
	 * @param groupEntity group entity that will be affected by rebuild.
	 */
	public void validate(EntityGroupRebuildAware groupEntity);
}
