package com.clifton.system.bean;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.converter.json.jackson.JacksonObjectToJsonStringConverter;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.core.validation.ValidationAware;
import com.clifton.system.audit.auditor.SystemAuditDaoUtils;
import com.clifton.system.bean.processor.SystemBeanPropertyProcessor;
import com.clifton.system.bean.search.SystemBeanGroupSearchForm;
import com.clifton.system.bean.search.SystemBeanPropertyTypeSearchForm;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.system.bean.search.SystemBeanTypeSearchForm;
import com.clifton.system.schema.SystemDataTypeConverter;
import com.clifton.system.userinterface.SystemUserInterfaceAware;
import com.clifton.system.userinterface.SystemUserInterfaceAwareVirtual;
import com.clifton.system.userinterface.SystemUserInterfaceMultiValueAware;
import com.clifton.system.userinterface.SystemUserInterfacePropertyProvider;
import com.clifton.system.userinterface.SystemUserInterfaceValueAware;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.type.StringNVarcharType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * The <code>SystemBeanServiceImpl</code> class provides basic implementation of the {@link SystemBeanService} interface.
 *
 * @author vgomelsky
 */
@Service
public class SystemBeanServiceImpl implements SystemBeanService {

	public static final String SINGLETON_BEAN_INSTANCE_CACHE = "com.clifton.system.bean.SINGLETON_BEAN_INSTANCE_CACHE";

	private AdvancedUpdatableDAO<SystemBean, Criteria> systemBeanDAO;
	private AdvancedUpdatableDAO<SystemBeanType, Criteria> systemBeanTypeDAO;
	private AdvancedUpdatableDAO<SystemBeanGroup, Criteria> systemBeanGroupDAO;
	private AdvancedUpdatableDAO<SystemBeanProperty, Criteria> systemBeanPropertyDAO;
	private AdvancedUpdatableDAO<SystemBeanPropertyType, Criteria> systemBeanPropertyTypeDAO;

	private ApplicationContextService applicationContextService;
	private CacheHandler<Integer, Object> cacheHandler;

	private DaoNamedEntityCache<SystemBean> systemBeanCache;
	private DaoNamedEntityCache<SystemBeanGroup> systemBeanGroupCache;
	private DaoSingleKeyListCache<SystemBeanProperty, Integer> systemBeanPropertyCache;

	private Map<String, SystemBeanPropertyProcessor> systemBeanPropertyProcessorMap;

	////////////////////////////////////////////////////////////////////////////
	////////            Bean Creation Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Object getBeanInstance(SystemBean bean) {
		AssertUtils.assertNotNull(bean, "Argument bean cannot be null");
		Object result;

		// check cache first for singleton beans
		boolean cache = bean.getType().isSingleton() && !bean.isNewBean();
		if (cache) {
			result = getCacheHandler().get(SINGLETON_BEAN_INSTANCE_CACHE, bean.getId());
			if (result != null) {
				return result;
			}
		}

		// get properties
		List<SystemBeanProperty> beanPropertyValues = getSystemBeanPropertyListByBeanId(bean.getId());
		result = getBeanInstance(bean, beanPropertyValues);

		if (cache) {
			getCacheHandler().put(SINGLETON_BEAN_INSTANCE_CACHE, bean.getId(), result);
		}

		return result;
	}


	@Override
	public Object getBeanInstance(SystemBean bean, List<SystemBeanProperty> beanPropertyValues) {
		AssertUtils.assertNotNull(bean, "Argument bean cannot be null");

		Class<?> beanClass = CoreClassUtils.getClass(bean.getType().getClassName());
		Object result = BeanUtils.newInstance(beanClass);

		// convert properties
		for (SystemBeanProperty propertyValue : CollectionUtils.getIterable(beanPropertyValues)) {
			if (!propertyValue.getType().isVirtualProperty()) {
				SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(propertyValue.getType().getDataType());
				Object value;

				try {
					if (propertyValue.getType().isMultipleValuesAllowed()) {
						value = convertBeanPropertyList(dataTypeConverter, propertyValue);
					}
					else {
						value = convertBeanProperty(dataTypeConverter, propertyValue.getType(), propertyValue.getValue());
					}

					BeanUtils.setPropertyValue(result, propertyValue.getType().getSystemPropertyName(), value);
				}
				catch (Throwable e) {
					throw new ValidationException("Cannot convert value '" + propertyValue.getValue() + "' to data type '" + propertyValue.getType().getDataType().getName() + "'", e);
				}
			}
		}

		getApplicationContextService().autowireBean(result);

		return result;
	}


	/**
	 * Retrieves the systemBeanProperty list for all bean ids passed in and merges non-null values in the order they were passed.
	 * This means the bean with the most specific configuration that should take precedence should be passed as the LAST id.
	 * e.g. Most generic bean defines 2 values, second bean defines two more and overrides 1, last bean overrides 2 more properties.
	 * After the properties from all beans are merged, an instance of the last bean with those properties is returned.
	 */
	@Override
	public Object getBeanInstanceWithMergedConfiguration(Integer... systemBeanIds) {
		Integer beanInstanceId = null;
		Map<String, SystemBeanProperty> mergedBeanPropertyMap = new HashMap<>();
		for (Integer systemBeanId : systemBeanIds) {
			if (systemBeanId != null) {
				List<SystemBeanProperty> beanPropertyList = getSystemBeanPropertyListByBeanId(systemBeanId);
				for (SystemBeanProperty beanProperty : CollectionUtils.getIterable(beanPropertyList)) {
					if (beanProperty.getValue() != null) {
						mergedBeanPropertyMap.put(beanProperty.getType().getSystemPropertyName(), beanProperty);
					}
				}
				beanInstanceId = systemBeanId;
			}
		}
		if (beanInstanceId != null) {
			return getBeanInstance(getSystemBean(beanInstanceId), new ArrayList<>(mergedBeanPropertyMap.values()));
		}
		throw new ValidationException("Unable to retrieve bean instance; no valid bean id was found.");
	}


	private Object convertBeanPropertyList(SystemDataTypeConverter dataTypeConverter, SystemBeanProperty propertyValue) {
		List<String> valueStringList = StringUtils.delimitedStringToList(propertyValue.getValue(), SystemUserInterfaceMultiValueAware.MULTI_VALUE_LIST_DELIMITER);
		List<Object> resultList = new ArrayList<>();

		for (String valueString : CollectionUtils.getIterable(valueStringList)) {
			Object value = convertBeanProperty(dataTypeConverter, propertyValue.getType(), valueString);

			if (value != null) {
				resultList.add(value);
			}
		}

		return resultList;
	}


	private Object convertBeanProperty(SystemDataTypeConverter dataTypeConverter, SystemBeanPropertyType propertyValueType, String valueString) {
		Object value;

		if (propertyValueType.getValueGroup() != null) {
			Integer beanId = (Integer) dataTypeConverter.convert(valueString);
			value = getSystemBean(beanId);
		}
		else {
			value = dataTypeConverter.convert(valueString);
		}

		return value;
	}


	////////////////////////////////////////////////////////////////////////////
	////////             SystemBean Business Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemBean getSystemBean(int id) {
		return doGetSystemBean(id, true);
	}


	private SystemBean doGetSystemBean(int id, boolean includeDynamicProperties) {
		SystemBean bean = getSystemBeanDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setPropertyList(getSystemBeanPropertyListByBeanId(bean, includeDynamicProperties));
		}
		return bean;
	}


	@Override
	public SystemBean getSystemBeanByName(String name) {
		return getSystemBeanCache().getBeanForKeyValueStrict(getSystemBeanDAO(), name);
	}


	@Override
	public List<SystemBean> getSystemBeanList(final SystemBeanSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getPropertyValue())) {
			ValidationUtils.assertFalse(StringUtils.isEmpty(searchForm.getSystemPropertyName()), "In order to filter on beans with property value of [" + searchForm.getPropertyValue() + "], systemPropertyName is required search filter.");
		}
		HibernateSearchFormConfigurer searchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (!StringUtils.isEmpty(searchForm.getSystemPropertyName())) {
					DetachedCriteria sub = DetachedCriteria.forClass(SystemBeanProperty.class, "prop");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("type", "propType");
					sub.add(Restrictions.eqProperty("bean.id", criteria.getAlias() + ".id"));
					sub.add(Restrictions.eq("propType.systemPropertyName", searchForm.getSystemPropertyName()));

					if (!StringUtils.isEmpty(searchForm.getPropertyValue())) {
						Criterion stringValueEquals = Restrictions.and(Restrictions.eq("propType.multipleValuesAllowed", false), Restrictions.eq("value", searchForm.getPropertyValue()));
						Criterion multipleValuesContainsRestriction = Restrictions.sqlRestriction(String.format("CONCAT('%1$s', prop_.Value, '%1$s') LIKE CONCAT('%%%1$s', ?, '%1$s%%')", SystemUserInterfaceMultiValueAware.MULTI_VALUE_LIST_DELIMITER), searchForm.getPropertyValue(), new StringNVarcharType());
						Criterion multipleValuesContains = Restrictions.and(Restrictions.eq("propType.multipleValuesAllowed", true), multipleValuesContainsRestriction);
						sub.add(Restrictions.or(stringValueEquals, multipleValuesContains));
					}
					criteria.add(Subqueries.exists(sub));
				}
			}
		};
		return getSystemBeanDAO().findBySearchCriteria(searchFormConfigurer);
	}


	@Override
	@Transactional
	public SystemBean saveSystemBean(SystemBean bean) {
		return saveSystemBeanImpl(bean, true);
	}


	@Override
	@Transactional
	public SystemBean saveSystemBeanNoValidation(SystemBean bean) {
		return saveSystemBeanImpl(bean, false);
	}


	private SystemBean saveSystemBeanImpl(SystemBean bean, boolean validate) {
		// validate required properties, etc.
		SystemBeanType type = bean.getType();
		ValidationUtils.assertNotNull(type, "Bean type is required for saving the bean", "type");
		List<SystemBeanPropertyType> propertyTypeList = getSystemBeanPropertyTypeListByType(type.getId());
		List<SystemBeanProperty> propertyList = buildSystemBeanPropertyList(bean.getPropertyList());
		for (SystemBeanPropertyType propertyType : CollectionUtils.getIterable(propertyTypeList)) {
			SystemBeanPropertyProcessor propertyProcessor = getSystemBeanPropertyProcessorMap().get(propertyType.getDataType().getName());
			if (propertyProcessor != null || propertyType.isRequired()) {
				Optional<SystemBeanProperty> propertyOptional = CollectionUtils.getStream(propertyList)
						.filter(p -> p.getType().equals(propertyType))
						.findFirst();

				if (propertyProcessor != null && propertyOptional.isPresent()) {
					propertyProcessor.processProperty(propertyOptional.get());
				}
				if (propertyType.isRequired()) {
					SystemBeanProperty property = propertyOptional.orElseThrow(() -> new ValidationException("Required bean property '" + propertyType.getName() + "' is not set."));
					ValidationUtils.assertFalse(StringUtils.isEmpty(property.getValue()), "Required bean property '" + propertyType.getName() + "' value cannot be empty.");
				}
			}
		}

		// Validate non-violation of duplicate beans if rule flag is enabled on bean type
		// Ignore this validation for json migrations.
		if (validate && !type.isDuplicateBeansAllowed()) {
			// Get all beans of same type
			SystemBeanSearchForm searchForm = new SystemBeanSearchForm();
			searchForm.setTypeId(bean.getType().getId());
			List<SystemBean> comparingBeanList = getSystemBeanList(searchForm);
			// Search for any duplicate beans
			List<SystemBean> duplicateBeans = getDuplicateBeans(bean, comparingBeanList);
			List<String> duplicateBeanNames = CollectionUtils.getStream(duplicateBeans)
					.map(SystemBean::getName)
					.collect(Collectors.toList());
			ValidationUtils.assertEmpty(duplicateBeans, String.format("Unable to save bean. Duplicate beans of this type are not allowed. This bean duplicates all properties of the following beans: %s", CollectionUtils.toString(duplicateBeanNames, 5)));
		}

		// delete the properties that are no longer present on the bean
		if (!bean.isNewBean()) {
			List<SystemBeanProperty> oldPropertyList = getSystemBeanPropertyListByBeanId(bean, false);
			for (SystemBeanProperty oldProperty : CollectionUtils.getIterable(oldPropertyList)) {
				if (!propertyList.contains(oldProperty)) {
					getSystemBeanPropertyDAO().delete(oldProperty.getId());
				}
			}
		}
		// save the bean and its new properties
		SystemBean savedBean = getSystemBeanDAO().save(bean);
		for (SystemBeanProperty property : CollectionUtils.getIterable(propertyList)) {
			if (!property.getType().isVirtualProperty()) {
				property.setBean(savedBean);
				getSystemBeanPropertyDAO().save(property);
			}
		}
		// Set the cache and clear bean instance cache
		getSystemBeanPropertyCache().clearBeanListForKeyValue(savedBean.getId());
		getCacheHandler().remove(SINGLETON_BEAN_INSTANCE_CACHE, savedBean.getId());
		//Disable validation during migrations.  This is because some beans may have references to
		//projects that are not dependencies (e.g. a trade with a Dietz calculator from the performance
		//project will fail to instantiate with a classNotFoundException
		if (validate) {
			try {
				// finally, make sure the bean can be instantiated
				Object beanInstance = getBeanInstance(savedBean);

				// validate the bean if validation is supported
				if (beanInstance instanceof ValidationAware) {
					((ValidationAware) beanInstance).validate();
				}
			}
			catch (Exception e) {
				// If can't instantiate clear the cache that was set
				getSystemBeanPropertyCache().clearBeanListForKeyValue(savedBean.getId());
				getCacheHandler().remove(SINGLETON_BEAN_INSTANCE_CACHE, savedBean.getId());
				throw new RuntimeException(e);
			}
		}
		savedBean.setPropertyList(propertyList);
		return savedBean;
	}


	@Override
	public void renameSystemBean(int id, String name) {
		final SystemBean bean = getSystemBeanDAO().findByPrimaryKey(id);
		bean.setName(name);

		final UpdatableDAO<SystemBean> dao = getSystemBeanDAO();
		SystemAuditDaoUtils.executeWithAuditingDisabled(new String[]{"name"}, () -> dao.save(bean));
	}


	@Override
	public SystemBean copySystemBean(int id, String name, String descriptionOverride) {
		SystemBean bean = getSystemBean(id);
		SystemBean newBean = BeanUtils.cloneBean(bean, false, false);
		newBean.setId(null);
		newBean.setName(name);
		if (!StringUtils.isEmpty(descriptionOverride)) {
			newBean.setDescription(descriptionOverride);
		}
		List<SystemBeanProperty> newPropertyList = new ArrayList<>();
		for (SystemBeanProperty property : CollectionUtils.getIterable(bean.getPropertyList())) {
			SystemBeanProperty newProperty = BeanUtils.cloneBean(property, false, false);
			newProperty.setId(null);
			newProperty.setBean(newBean);
			newPropertyList.add(newProperty);
		}
		newBean.setPropertyList(newPropertyList);
		// When copying a system bean, we don't need to 1. Validate the values - should already be valid properties
		// 2.  Validate duplicate beans.  Usually the point of copying is to copy something, and then edit the one or two properties that needs to be different
		saveSystemBeanNoValidation(newBean);
		return newBean;
	}


	@Override
	@Transactional
	public void deleteSystemBean(int id) {
		SystemBean bean = getSystemBean(id);
		if (bean != null) {
			getSystemBeanPropertyDAO().deleteList(bean.getPropertyList());
		}
		getSystemBeanDAO().delete(id);
		getCacheHandler().remove(SINGLETON_BEAN_INSTANCE_CACHE, id);
	}


	/**
	 * Builds the correct list of properties and removes any virtual properties.  Also, builds the json value for
	 * any virtual properties and stores the resulting string in the target field.
	 */
	private List<SystemBeanProperty> buildSystemBeanPropertyList(List<SystemBeanProperty> systemBeanPropertyList) {
		List<SystemBeanProperty> result = new ArrayList<>();
		MultiValueMap<Integer, SystemBeanProperty> multiValueMap = BeanUtils.getMultiValueMapFromProperty(systemBeanPropertyList, "type.targetSystemUserInterfaceAware.id");

		JacksonObjectToJsonStringConverter converter = new JacksonObjectToJsonStringConverter();
		for (Map.Entry<Integer, Collection<SystemBeanProperty>> beanEntry : multiValueMap.entrySet()) {
			if (beanEntry.getKey() != null) {
				Map<String, Object> mapValue = new HashMap<>();
				for (SystemBeanProperty property : CollectionUtils.getIterable(beanEntry.getValue())) {
					Map<String, Object> propertyMap = new HashMap<>();
					propertyMap.put("value", property.getValue());
					propertyMap.put("text", property.getText());
					mapValue.put(property.getType().getVirtualTypeId().toString(), converter.convert(propertyMap));
				}
				SystemBeanProperty beanProperty = new SystemBeanProperty();
				beanProperty.setType(getSystemBeanPropertyType(beanEntry.getKey()));
				beanProperty.setValue(converter.convert(mapValue));
				beanProperty.setText(converter.convert(mapValue));
				result.add(beanProperty);
			}
			else {
				result.addAll(beanEntry.getValue());
			}
		}

		return result;
	}


	/**
	 * Finds any beans in the given list which duplicate the given bean. Beans are considered to be <i>duplicate</i> if they are of the same type and all of
	 * their properties are equal.
	 *
	 * @param bean              the source bean
	 * @param comparingBeanList the list of beans to seek for duplicates
	 * @return a list of beans in the given list which duplicate the source bean
	 */
	@Override
	public List<SystemBean> getDuplicateBeans(SystemBean bean, List<SystemBean> comparingBeanList) {
		List<SystemBean> matchingBeans = new ArrayList<>();
		List<SystemBeanProperty> beanPropertyList = bean.getPropertyList() != null || bean.isNewBean() ? bean.getPropertyList() : getSystemBeanPropertyListByBeanId(bean, false);
		for (SystemBean comparingBean : CollectionUtils.getIterable(comparingBeanList)) {
			// Skip same bean
			if (CompareUtils.isEqual(bean.getId(), comparingBean.getId())) {
				continue;
			}

			// Check for duplicate bean properties
			List<SystemBeanProperty> comparingBeanPropertyList = getSystemBeanPropertyListByBeanId(comparingBean, false);
			if (CompareUtils.isEqual(CollectionUtils.getSize(beanPropertyList), CollectionUtils.getSize(comparingBeanPropertyList))) {
				// Check for duplication: If for all properties there is at least one matching value on the comparing bean, then the beans are duplicates
				boolean isDuplicateBean = CollectionUtils.getStream(beanPropertyList)
						.allMatch(beanProperty -> CollectionUtils.getStream(comparingBeanPropertyList)
								.anyMatch(comparingBeanProperty -> CompareUtils.isEqual(beanProperty.getType().getId(), comparingBeanProperty.getType().getId())
										&& CompareUtils.isEqual(beanProperty.getValue(), comparingBeanProperty.getValue())));
				if (isDuplicateBean) {
					matchingBeans.add(comparingBean);
				}
			}
		}
		return matchingBeans;
	}


	////////////////////////////////////////////////////////////////////////////
	////////           SystemBeanType Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemBeanType getSystemBeanType(int id) {
		return getSystemBeanTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemBeanType getSystemBeanTypeByName(String name) {
		return getSystemBeanTypeDAO().findOneByField("name", name);
	}


	@Override
	public List<SystemBeanType> getSystemBeanTypeList(SystemBeanTypeSearchForm searchForm) {
		return getSystemBeanTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see com.clifton.system.bean.validation.SystemBeanTypeValidator
	 */
	@Override
	public SystemBeanType saveSystemBeanType(SystemBeanType bean) {
		return getSystemBeanTypeDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////          SystemBeanGroup Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemBeanGroup getSystemBeanGroup(short id) {
		return getSystemBeanGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemBeanGroup getSystemBeanGroupByName(String name) {
		return getSystemBeanGroupCache().getBeanForKeyValueStrict(getSystemBeanGroupDAO(), name);
	}


	@Override
	public List<SystemBeanGroup> getSystemBeanGroupList(SystemBeanGroupSearchForm searchForm) {
		return getSystemBeanGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see com.clifton.system.bean.validation.SystemBeanGroupValidator
	 */
	@Override
	public SystemBeanGroup saveSystemBeanGroup(SystemBeanGroup bean) {
		return getSystemBeanGroupDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////        SystemBeanProperty Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemBeanProperty getSystemBeanProperty(int id) {
		return getSystemBeanPropertyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemBeanProperty> getSystemBeanPropertyListByBeanId(int beanId) {
		return getSystemBeanPropertyCache().getBeanListForKeyValue(getSystemBeanPropertyDAO(), beanId);
	}


	@Override
	public List<SystemBeanProperty> getSystemBeanPropertyListByBean(SystemBean bean) {
		return getSystemBeanPropertyListByBeanId(bean, true);
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	private List<SystemBeanProperty> getSystemBeanPropertyListByBeanId(SystemBean bean, boolean includeDynamicProperties) {
		if (bean != null && !bean.isNewBean()) {
			List<SystemBeanProperty> beanPropertyList = getSystemBeanPropertyListByBeanId(bean.getId());
			if (!includeDynamicProperties || bean.getType() == null || !bean.getType().isBeanClassPropertyProvider()) {
				return beanPropertyList;
			}
			Object beanInstance = getBeanInstance(bean, beanPropertyList);

			if (beanInstance instanceof SystemUserInterfacePropertyProvider) {
				for (SystemBeanPropertyType propertyType : CollectionUtils.getIterable(getSystemBeanPropertyTypeListByType(bean.getType().getId()))) {
					if (propertyType.getTriggerPropertyType() != null) {
						List<SystemUserInterfaceValueAware> subPropertyList = ((SystemUserInterfacePropertyProvider) beanInstance).getPropertyList(propertyType);

						// copy the user interface aware objects to system bean property objects.
						for (SystemUserInterfaceValueAware value : CollectionUtils.getIterable(subPropertyList)) {
							SystemBeanProperty property;
							if (value instanceof SystemBeanProperty) {
								property = (SystemBeanProperty) value;
							}
							else {
								property = new SystemBeanProperty();
								property.setText(value.getText());
								property.setValue(value.getValue());
								property.setType(copyToSystemBeanPropertyType(value.getParameter()));
							}
							beanPropertyList.add(property);
						}
					}
				}
			}
			return beanPropertyList;
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////           SystemBeanPropertyType Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemBeanPropertyType getSystemBeanPropertyType(int id) {
		return getSystemBeanPropertyTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SystemBeanPropertyType> getSystemBeanPropertyTypeListByType(int typeId) {
		return getSystemBeanPropertyTypeDAO().findByField("type.id", typeId);
	}


	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public List<SystemBeanPropertyType> getSystemBeanPropertyTypeListByBean(SystemBean bean) {
		ValidationUtils.assertNotNull(bean.getType(), "");
		ValidationUtils.assertNotNull(bean.getType().getId(), "");

		List<SystemBeanPropertyType> propertyTypeList = getSystemBeanPropertyTypeListByType(bean.getType().getId());
		List<SystemBeanPropertyType> result = new ArrayList<>(propertyTypeList);

		SystemBean populatedBean = bean;
		if ((populatedBean.getId() != null) && CollectionUtils.isEmpty(populatedBean.getPropertyList())) {
			populatedBean = doGetSystemBean(populatedBean.getId(), false);
		}
		else {
			SystemBeanType beanType = CollectionUtils.isEmpty(result) ? populatedBean.getType() : result.get(0).getType();
			if (beanType.isNewBean()) {
				beanType = getSystemBeanType(beanType.getId());
			}
			populatedBean.setType(beanType);
		}
		if (!CollectionUtils.isEmpty(populatedBean.getPropertyList()) && populatedBean.getType().isBeanClassPropertyProvider()) {
			Object beanInstance = null;
			for (SystemBeanPropertyType propertyType : CollectionUtils.getIterable(propertyTypeList)) {
				if (propertyType.getTriggerPropertyType() != null) {
					if (beanInstance == null) {
						if (populatedBean.isNewBean()) {
							for (SystemBeanProperty property : CollectionUtils.getIterable(populatedBean.getPropertyList())) {
								property.setType(getSystemBeanPropertyType(property.getType().getId()));
							}
						}
						beanInstance = getBeanInstance(populatedBean, populatedBean.getPropertyList());
					}
					if (beanInstance instanceof SystemUserInterfacePropertyProvider) {
						List<SystemUserInterfaceAwareVirtual> subPropertyList = ((SystemUserInterfacePropertyProvider) beanInstance).getPropertyTypeList(propertyType);

						for (SystemUserInterfaceAwareVirtual subProperty : CollectionUtils.getIterable(subPropertyList)) {
							result.add(copyToSystemBeanPropertyType(subProperty));
						}

						propertyType.getTriggerPropertyType().setResetDynamicFieldsOnChange(true);
					}
				}
			}
		}
		else {
			for (SystemBeanPropertyType property : CollectionUtils.getIterable(result)) {
				if (property.getTriggerPropertyType() != null) {
					property.getTriggerPropertyType().setResetDynamicFieldsOnChange(true);
				}
			}
		}

		return result;
	}


	@Override
	public List<SystemBeanPropertyType> getSystemBeanPropertyTypeList(SystemBeanPropertyTypeSearchForm searchForm) {
		return getSystemBeanPropertyTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see com.clifton.system.bean.validation.SystemBeanPropertyTypeValidator
	 */
	@Override
	public SystemBeanPropertyType saveSystemBeanPropertyType(SystemBeanPropertyType bean) {
		// This validation is here to allow overriding it for cases like portal where there isn't a table/url association
		if ((bean.getValueTable() == null) != StringUtils.isEmpty(bean.getValueListUrl())) {
			throw new UserIgnorableValidationException(getClass(), "saveSystemBeanPropertyTypeNoValidation", "Value List URL and Value Table should either both be populated or both be blank except in rare cases where URLs use an external service call.");
		}
		return saveSystemBeanPropertyTypeNoValidation(bean);
	}


	@Override
	public SystemBeanPropertyType saveSystemBeanPropertyTypeNoValidation(SystemBeanPropertyType bean) {
		return getSystemBeanPropertyTypeDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Copies an instance of SystemUserInterfaceAware to a new SystemBeanPropertyType.
	 */
	private SystemBeanPropertyType copyToSystemBeanPropertyType(SystemUserInterfaceAware property) {
		if (property instanceof SystemBeanPropertyType) {
			return (SystemBeanPropertyType) property;
		}
		SystemBeanPropertyType type = null;
		if (property != null) {
			type = new SystemBeanPropertyType();
			//			type.setId((Integer) property.getIdentity());
			type.setVirtualTypeId((Integer) property.getIdentity());
			type.setName(property.getName());
			type.setLabel(property.getLabel());
			type.setDescription(property.getDescription());
			type.setOrder(property.getOrder());
			type.setRequired(property.isRequired());
			type.setDataType(property.getDataType());
			type.setValueTable(property.getValueTable());
			type.setValueListUrl(property.getValueListUrl());
			type.setUserInterfaceConfig(property.getUserInterfaceConfig());
			type.setValueListUrl(property.getValueListUrl());
			if (property instanceof SystemUserInterfaceAwareVirtual) {
				// Fixes Required and Dependent fields to use the correct field names
				if (!StringUtils.isEmpty(type.getUserInterfaceConfig())) {
					type.setUserInterfaceConfig(type.getUserInterfaceConfig().replaceAll("field_", "field_virtual_"));
				}
				type.setTargetSystemUserInterfaceAware(copyToSystemBeanPropertyType(((SystemUserInterfaceAwareVirtual) property).getTargetSystemUserInterfaceAware()));
			}
		}
		return type;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SystemBean, Criteria> getSystemBeanDAO() {
		return this.systemBeanDAO;
	}


	public void setSystemBeanDAO(AdvancedUpdatableDAO<SystemBean, Criteria> systemBeanDAO) {
		this.systemBeanDAO = systemBeanDAO;
	}


	public AdvancedUpdatableDAO<SystemBeanType, Criteria> getSystemBeanTypeDAO() {
		return this.systemBeanTypeDAO;
	}


	public void setSystemBeanTypeDAO(AdvancedUpdatableDAO<SystemBeanType, Criteria> systemBeanTypeDAO) {
		this.systemBeanTypeDAO = systemBeanTypeDAO;
	}


	public AdvancedUpdatableDAO<SystemBeanGroup, Criteria> getSystemBeanGroupDAO() {
		return this.systemBeanGroupDAO;
	}


	public void setSystemBeanGroupDAO(AdvancedUpdatableDAO<SystemBeanGroup, Criteria> systemBeanGroupDAO) {
		this.systemBeanGroupDAO = systemBeanGroupDAO;
	}


	public AdvancedUpdatableDAO<SystemBeanProperty, Criteria> getSystemBeanPropertyDAO() {
		return this.systemBeanPropertyDAO;
	}


	public void setSystemBeanPropertyDAO(AdvancedUpdatableDAO<SystemBeanProperty, Criteria> systemBeanPropertyDAO) {
		this.systemBeanPropertyDAO = systemBeanPropertyDAO;
	}


	public AdvancedUpdatableDAO<SystemBeanPropertyType, Criteria> getSystemBeanPropertyTypeDAO() {
		return this.systemBeanPropertyTypeDAO;
	}


	public void setSystemBeanPropertyTypeDAO(AdvancedUpdatableDAO<SystemBeanPropertyType, Criteria> systemBeanPropertyTypeDAO) {
		this.systemBeanPropertyTypeDAO = systemBeanPropertyTypeDAO;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public DaoSingleKeyListCache<SystemBeanProperty, Integer> getSystemBeanPropertyCache() {
		return this.systemBeanPropertyCache;
	}


	public void setSystemBeanPropertyCache(DaoSingleKeyListCache<SystemBeanProperty, Integer> systemBeanPropertyCache) {
		this.systemBeanPropertyCache = systemBeanPropertyCache;
	}


	public DaoNamedEntityCache<SystemBean> getSystemBeanCache() {
		return this.systemBeanCache;
	}


	public void setSystemBeanCache(DaoNamedEntityCache<SystemBean> systemBeanCache) {
		this.systemBeanCache = systemBeanCache;
	}


	public DaoNamedEntityCache<SystemBeanGroup> getSystemBeanGroupCache() {
		return this.systemBeanGroupCache;
	}


	public void setSystemBeanGroupCache(DaoNamedEntityCache<SystemBeanGroup> systemBeanGroupCache) {
		this.systemBeanGroupCache = systemBeanGroupCache;
	}


	public CacheHandler<Integer, Object> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Integer, Object> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public Map<String, SystemBeanPropertyProcessor> getSystemBeanPropertyProcessorMap() {
		return this.systemBeanPropertyProcessorMap;
	}


	public void setSystemBeanPropertyProcessorMap(Map<String, SystemBeanPropertyProcessor> systemBeanPropertyProcessorMap) {
		this.systemBeanPropertyProcessorMap = systemBeanPropertyProcessorMap;
	}
}
