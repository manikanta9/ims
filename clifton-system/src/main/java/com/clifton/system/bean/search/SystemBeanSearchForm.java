package com.clifton.system.bean.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>SystemBeanSearchForm</code> class defines search configuration for SystemBean objects.
 *
 * @author vgomelsky
 */
public class SystemBeanSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,alias,description", sortField = "name")
	private String searchPattern;

	@SearchField(searchField = "alias,name", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String labelShort;

	@SearchField
	private String name;

	@SearchField
	private String alias;

	@SearchField
	private String description;

	@SearchField(searchField = "type.id", sortField = "type.name", comparisonConditions = {ComparisonConditions.EQUALS})
	private Integer typeId;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = {ComparisonConditions.EQUALS})
	private String typeName;

	@SearchField(searchField = "type.id", comparisonConditions = {ComparisonConditions.IN})
	private Integer[] includeTypeIds;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private String excludeTypeName;

	@SearchField(searchField = "type.id", comparisonConditions = {ComparisonConditions.NOT_IN})
	private Integer[] excludeTypeIds;

	@SearchField(searchField = "group.id", searchFieldPath = "type")
	private Short groupId;

	@SearchField(searchField = "name", searchFieldPath = "type.group", comparisonConditions = {ComparisonConditions.EQUALS})
	private String groupName;

	@SearchField(searchField = "type.group.id", comparisonConditions = {ComparisonConditions.IN})
	private Short[] includeGroupIds;


	/**
	 * Custom Search Field(s) - Note: Property Value Requires Both to Be Populated but systemPropertyName can be used alone
	 * Does a Where Exists where the Bean has a property of given name with given value
	 * Properly handles multiple value properties that are saved in the DV as comma separated values
	 */
	private String systemPropertyName;
	private String propertyValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getGroupName() {
		return this.groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getSystemPropertyName() {
		return this.systemPropertyName;
	}


	public void setSystemPropertyName(String systemPropertyName) {
		this.systemPropertyName = systemPropertyName;
	}


	public String getPropertyValue() {
		return this.propertyValue;
	}


	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}


	public String getExcludeTypeName() {
		return this.excludeTypeName;
	}


	public void setExcludeTypeName(String excludeTypeName) {
		this.excludeTypeName = excludeTypeName;
	}


	public String getLabelShort() {
		return this.labelShort;
	}


	public void setLabelShort(String labelShort) {
		this.labelShort = labelShort;
	}


	public Integer[] getIncludeTypeIds() {
		return this.includeTypeIds;
	}


	public void setIncludeTypeIds(Integer[] includeTypeIds) {
		this.includeTypeIds = includeTypeIds;
	}


	public Integer[] getExcludeTypeIds() {
		return this.excludeTypeIds;
	}


	public void setExcludeTypeIds(Integer[] excludeTypeIds) {
		this.excludeTypeIds = excludeTypeIds;
	}


	public Short[] getIncludeGroupIds() {
		return this.includeGroupIds;
	}


	public void setIncludeGroupIds(Short[] includeGroupIds) {
		this.includeGroupIds = includeGroupIds;
	}
}
