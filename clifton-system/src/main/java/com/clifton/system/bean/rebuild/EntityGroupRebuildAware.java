package com.clifton.system.bean.rebuild;

import com.clifton.system.bean.SystemBean;


/**
 * The <code>EntityGroupRebuildAware</code> can be implemented to define an entity group
 * that is capable of having it's mapped group entities rebuilt by the system.
 * <p>
 * Candidates for use include InvestmentSecurityGroup and InvestmentAccountGroup
 *
 * @author NickK
 */
public interface EntityGroupRebuildAware {


	/**
	 * Returns the name of the table that will be affected by a rebuild.
	 */
	public String getRebuildTableName();


	/**
	 * Returns the bean to be executed to rebuild the grouped entities.
	 */
	public SystemBean getRebuildSystemBean();
}
