package com.clifton.system.bean.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.bean.SystemBeanType;

import java.util.List;


/**
 * The <code>SystemBeanTypeValidator</code> validates that:
 * <p>
 * 1. Inserts are not allowed
 * 2. Deletes are not allowed
 * 3. Updates can modify description only
 *
 * @author manderson
 */
public class SystemBeanTypeValidator extends SelfRegisteringDaoValidator<SystemBeanType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(SystemBeanType bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert()) {
			throw new ValidationException("Creating new system bean type is not allowed.");
		}
		if (config.isDelete()) {
			throw new ValidationException("Deleting system bean types is not allowed.");
		}

		// Otherwise an update - only field that can be modified is description
		SystemBeanType originalBean = getOriginalBean(bean);
		List<String> diffs = CoreCompareUtils.getNoEqualProperties(bean, originalBean, false, "description");
		if (diffs != null) {
			if (!diffs.isEmpty()) {
				throw new FieldValidationException("Cannot update field '" + diffs.get(0) + "' for system bean type [" + bean.getName() + "]", diffs.get(0));
			}
		}
	}
}
