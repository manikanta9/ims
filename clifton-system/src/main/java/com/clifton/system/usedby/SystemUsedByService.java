package com.clifton.system.usedby;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.schema.SystemRelationship;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.usedby.softlink.SystemSoftLink;

import java.util.List;


/**
 * The <code>SystemUsedByService</code> ...
 *
 * @author manderson
 */
public interface SystemUsedByService {

	/**
	 * Returns a list of {@link SystemUsedByEntity} objects that the given entity with tableName and entityId is used by
	 * <p>
	 * Uses {@link SystemRelationship}s to determine used by where parent column for the relationship = tableName.primaryKey column and child column value for the relationship = entityId
	 * Also uses {@link SystemColumnCustom} where valueTable = given table name where {@link SystemColumnValue} for that column = entityId
	 */
	@SecureMethod(dtoClass = SystemTable.class)
	public List<SystemUsedByEntity> getSystemUsedByEntityList(String tableName, int entityId);


	/**
	 * Returns a list of {@link SystemUsedByEntity} objects that the given entity with entityId is used by
	 * for a specific column
	 * <p>
	 * Supports Standard Columns, i.e. Database Columns, and Custom Columns (Values in SystemColumnValue)
	 */
	@SecureMethod(dtoClass = SystemTable.class)
	public List<SystemUsedByEntity> getSystemUsedByEntityListForColumn(int entityId, int columnId);


	////////////////////////////////////////////////////////////////////////////
	////////////////             System Soft Links          ////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = SystemTable.class)
	public List<SystemSoftLink> getSystemSoftLinkList();
}
