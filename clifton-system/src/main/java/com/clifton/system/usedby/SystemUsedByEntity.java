package com.clifton.system.usedby;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.StringUtils;
import com.clifton.system.schema.SystemRelationship;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;

import java.util.Date;


/**
 * The <code>SystemUsedByEntity</code> represents a link to another entity in the system that an entity is "used by"
 * <p>
 * For example: BusinessContracts are used by: Contract Parties, Other Contracts, Investment Accounts, Billing Definitions
 *
 * @author manderson
 */
@NonPersistentObject
public class SystemUsedByEntity implements LabeledObject {

	/**
	 * When populated from a {@link SystemColumnCustom} this is the "Custom Field: " + custom column name
	 * When populated from a {@link SystemRelationship} this is the "child" column in that relationship
	 */
	private SystemColumn column;
	private String label;

	/**
	 * ID of the entity in column's table that is referenced
	 */
	private Integer usedById;

	/**
	 * Label of the entity with usedById in column's table that represents it
	 * If entity implements {@link LabeledObject} then uses getLabel()
	 * Otherwise uses TableName: ID as a default
	 */
	private String usedByLabel;


	private Short createUserId;
	private Date createDate;


	private Integer totalRecords;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemUsedByEntity(SystemColumn column, IdentityObject entity) {
		this.column = column;
		if (entity != null) {
			this.usedById = ((Number) entity.getIdentity()).intValue();
			if (entity instanceof LabeledObject) {
				this.usedByLabel = ((LabeledObject) entity).getLabel();
			}
			else {
				if (column != null && column.getTable() != null) {
					this.usedByLabel = column.getTable().getLabel() + ": " + this.usedById;
				}
				else {
					this.usedByLabel = "Unknown Table: " + this.usedById;
				}
			}
		}
	}


	public SystemUsedByEntity(String label, SystemColumn column, IdentityObject entity) {
		this.label = label;
		this.column = column;
		if (entity != null) {
			this.usedById = ((Number) entity.getIdentity()).intValue();
			if (entity instanceof LabeledObject) {
				this.usedByLabel = ((LabeledObject) entity).getLabel();
			}
			else {
				this.usedByLabel = label + ": " + this.usedById;
			}
		}
	}


	public SystemUsedByEntity(SystemColumn column, Integer totalRecords) {
		this.column = column;
		this.usedByLabel = "Total: " + totalRecords + " (Not individually displayed)";
		this.totalRecords = totalRecords;
	}


	public SystemUsedByEntity(String label, Integer totalRecords) {
		this.label = label;
		this.usedByLabel = "Total: " + totalRecords + " (Not individually displayed)";
		this.totalRecords = totalRecords;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String lbl = this.label;
		if (StringUtils.isEmpty(lbl)) {
			if (getColumn() != null) {
				if (getColumn().getTable() != null) {
					lbl = getColumn().getTable().getLabel() + ".";
				}
				lbl += getColumn().getLabel();
			}
		}
		return lbl;
	}


	@Override
	public String toString() {
		return "SystemUsedByEntity{" +
				"column=" + this.column +
				", label='" + this.label + '\'' +
				", usedById=" + this.usedById +
				", usedByLabel='" + this.usedByLabel + '\'' +
				", totalRecords=" + this.totalRecords +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumn getColumn() {
		return this.column;
	}


	public void setColumn(SystemColumn column) {
		this.column = column;
	}


	public Integer getUsedById() {
		return this.usedById;
	}


	public void setUsedById(Integer usedById) {
		this.usedById = usedById;
	}


	public String getUsedByLabel() {
		return this.usedByLabel;
	}


	public void setUsedByLabel(String usedByLabel) {
		this.usedByLabel = usedByLabel;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public Short getCreateUserId() {
		return this.createUserId;
	}


	public void setCreateUserId(Short createUserId) {
		this.createUserId = createUserId;
	}


	public Date getCreateDate() {
		return this.createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Integer getTotalRecords() {
		return this.totalRecords;
	}


	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}
}
