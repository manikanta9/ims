package com.clifton.system.usedby.softlink;


import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;

import java.lang.reflect.Field;


/**
 * The <code>SystemSoftLink</code> holds the meta data for classes and their fields that utilize the {@link SoftLinkField} annotation.
 * It is populated the first time it is called
 * <p>
 * It's primary use is to manage Used By link generation for specific entities so we can include
 * these dynamic links that aren't managed by a real FK Database relationship.
 *
 * @author manderson
 */
public class SystemSoftLink {

	private final String tableName;

	/**
	 * For cases where the table is defined in multiple places will check tableBeanPropertyName first if not null
	 * else will check secondTableBeanPropertyName
	 */
	private final String tableBeanPropertyName;
	private final String secondTableBeanPropertyName;

	private final String beanPropertyName;

	private final DataTypes beanDataType;

	/**
	 * Used for cases where the soft link is held in a table where we want to show the used
	 * by for it's owner.  For example, SystemHierarchyLink table holds the soft link, but
	 * for users, we want to see the hierarchy that applies, not the link
	 */
	private String ownerTableName;
	private String ownerBeanPropertyName;

	/**
	 * Table.Column Name of the Soft Link used as the label for Used by.  This usually pulled from the
	 * column on the Used by entity, but because of owners, and reverse lookup, need to explicitly set this
	 * so the column can be properly used for drill down support.
	 */
	private final String label;

	/**
	 * Optionally can exclude from used by
	 * Only applies to the used by when looking at entities that reference this field,
	 * i.e. All Warnings tied to a security, trade, etc.  For the warning itself would still show it's own personal
	 * soft link references
	 * * NOTE: Not currently used, and without examples of usage of SoftLinks in Used by not sure if we'll need this
	 */
	private final boolean excludeFromUsedBy;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSoftLink(String label, String tableName, Field field, DataTypes beanDataType, SoftLinkField softLinkField, String ownerTableName) {
		this.label = label;
		this.tableName = tableName;
		this.tableBeanPropertyName = softLinkField.tableBeanPropertyName();
		this.secondTableBeanPropertyName = softLinkField.secondTableBeanPropertyName();
		this.beanPropertyName = field.getName();
		this.excludeFromUsedBy = softLinkField.excludeFromUsedBy();
		this.beanDataType = beanDataType;

		if (ownerTableName != null) {
			this.ownerTableName = ownerTableName;
			this.ownerBeanPropertyName = softLinkField.ownerBeanPropertyName();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCoalesceOwnerTableName() {
		return StringUtils.coalesce(false, getOwnerTableName(), getTableName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public String getOwnerTableName() {
		return this.ownerTableName;
	}


	public String getTableBeanPropertyName() {
		return this.tableBeanPropertyName;
	}


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}


	public String getOwnerBeanPropertyName() {
		return this.ownerBeanPropertyName;
	}


	public String getLabel() {
		return this.label;
	}


	public boolean isExcludeFromUsedBy() {
		return this.excludeFromUsedBy;
	}


	public DataTypes getBeanDataType() {
		return this.beanDataType;
	}


	public String getSecondTableBeanPropertyName() {
		return this.secondTableBeanPropertyName;
	}
}
