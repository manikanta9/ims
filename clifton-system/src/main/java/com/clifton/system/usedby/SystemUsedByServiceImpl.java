package com.clifton.system.usedby;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.UpdatableEntity;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.CustomJsonStringExpression;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.SystemRelationship;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.CustomColumnTypes;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.column.search.SystemColumnDatedValueSearchForm;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnDatedValue;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.system.schema.search.SystemRelationshipSearchForm;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.system.usedby.softlink.SystemSoftLink;
import com.clifton.system.userinterface.SystemUserInterfaceUsedByAware;
import com.clifton.system.userinterface.SystemUserInterfaceValueUsedByConfig;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemUsedByServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class SystemUsedByServiceImpl implements SystemUsedByService {

	private SystemColumnService systemColumnService;
	private SystemColumnValueService systemColumnValueService;

	private SystemSchemaService systemSchemaService;

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private List<SystemSoftLink> softLinkList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemUsedByEntity> getSystemUsedByEntityList(String tableName, int entityId) {
		ValidationUtils.assertNotNull(tableName, "Table name is required to view Used By for an entity.");

		// Verify this is a real table
		ValidationUtils.assertNotNull(getSystemSchemaService().getSystemTableByName(tableName), "Unable to find System Table with Name [" + tableName + "]");

		Map<String, SystemColumn> primaryKeyColumnMap = new HashMap<>();
		SystemColumn primaryKeyColumn = getPrimaryKeyColumnForTable(tableName, primaryKeyColumnMap);

		List<SystemUsedByEntity> usedByList = new ArrayList<>();

		// REAL FK RELATIONSHIPS
		Serializable convertedEntityId = getDaoLocator().locate(tableName).convertToPrimaryKeyDataType(entityId);
		appendUsedByForSystemRelationships(primaryKeyColumn, convertedEntityId, usedByList);

		// FAKE FK RELATIONSHIPS VIA CUSTOM COLUMNS
		appendUsedByForSystemColumnCustomValues(primaryKeyColumn, convertedEntityId, usedByList);

		// FAKE FK RELATIONSHIPS VIA SYSTEM USER INTERFACE USED BY AWARE
		// TODO - CAN THESE BE REFACTORED INTO SOFT LINKS?
		appendUsedByForSystemUserInterfaceUsedByAware(primaryKeyColumn, convertedEntityId, usedByList, primaryKeyColumnMap);

		// Annotation Driven Soft Links - Notifications/System Notes/ where FK relationship is driven by meta data for Table name and some kind of fkFieldId
		appendUsedByForSoftLinks(tableName, convertedEntityId, usedByList, primaryKeyColumnMap);

		return usedByList;
	}


	@Override
	public List<SystemUsedByEntity> getSystemUsedByEntityListForColumn(int entityId, int columnId) {
		SystemColumn column = getSystemColumnService().getSystemColumn(columnId);
		ValidationUtils.assertNotNull(column, "Cannot find system column in the database with id [" + columnId + "].");

		// Custom Columns - Don't worry about type because uses Text = in search form
		// Otherwise we do need to determine entity id type
		Serializable entityIdNumber = entityId;
		if (!column.isCustomColumn()) {
			// Get column reference
			ReadOnlyDAO<IdentityObject> beanDao = getDaoLocator().locate(column.getTable().getName());
			Column daoColumn = CollectionUtils.getFirstElement(BeanUtils.filter(beanDao.getConfiguration().getColumnList(), Column::getName, column.getName()));
			ValidationUtils.assertNotNull(daoColumn, "Cannot find DAO configuration for column: " + column);

			// Convert to target type
			DataTypes columnDataType = daoColumn.getDataType();
			entityIdNumber = DaoUtils.convertToDataType(entityIdNumber, columnDataType);
		}

		return getSystemUsedByEntityListForColumnImpl(entityIdNumber, column, false);
	}


	/**
	 * Returns all entities related to given entity by it's reference in the given column value.
	 *
	 * @param usePlaceHolderForLargeRecords - If more than 500 records and viewing all - returns a place holder with drill down.  Specific column drill down then exposes all records
	 */
	private List<SystemUsedByEntity> getSystemUsedByEntityListForColumnImpl(Serializable entityId, SystemColumn column, boolean usePlaceHolderForLargeRecords) {
		ValidationUtils.assertNotNull(column, "Column is required to find used by");
		List<SystemUsedByEntity> usedByList = new ArrayList<>();

		// Check Long PK Field - Applies to either Standard Or Custom - Checking the Table it belongs to...
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(column.getTable().getName());
		// Special Exclude, sometimes we need to use Long instead of Integers for PK fields...when this happens it's usually because the table is very large
		// And in these cases we wouldn't want Used By to include them, even if the relationship hasn't been explicitly excluded.
		if (DataTypes.IDENTITY_LONG == dao.getConfiguration().getPrimaryKeyColumn().getDataType()) {
			return usedByList;
		}

		// Actual Database Column
		if (column instanceof SystemColumnStandard) {
			List<IdentityObject> list = dao.findByField(((SystemColumnStandard) column).getBeanPropertyName() + ".id", entityId);
			if (CollectionUtils.getSize(list) > 500 && usePlaceHolderForLargeRecords) {
				return CollectionUtils.createList(new SystemUsedByEntity(column, CollectionUtils.getSize(list)));
			}

			// Special Handling for Hierarchies:
			for (IdentityObject bean : CollectionUtils.getIterable(list)) {
				SystemUsedByEntity systemUsedByEntity = new SystemUsedByEntity(column, bean);
				populateModificationInformation(systemUsedByEntity, bean);
				usedByList.add(systemUsedByEntity);
			}
		}
		// Custom Columns
		else {
			SystemColumnCustom customColumn = (SystemColumnCustom) column;
			// Standard Custom Column Values
			if (customColumn.getColumnGroup().getCustomColumnType() == CustomColumnTypes.COLUMN_VALUE) {
				SystemColumnValueSearchForm valueSearchForm = new SystemColumnValueSearchForm();
				valueSearchForm.setColumnId(column.getId());
				valueSearchForm.setValueEquals("" + entityId);
				List<SystemColumnValue> valueList = getSystemColumnValueService().getSystemColumnValueList(valueSearchForm);

				if (CollectionUtils.getSize(valueList) > 500 && usePlaceHolderForLargeRecords) {
					usedByList.add(new SystemUsedByEntity(column, CollectionUtils.getSize(valueList)));
				}
				else {
					for (SystemColumnValue val : CollectionUtils.getIterable(valueList)) {
						SystemUsedByEntity systemUsedByEntity = new SystemUsedByEntity(column, dao.findByPrimaryKey(val.getFkFieldId()));
						populateModificationInformation(systemUsedByEntity, val);
						usedByList.add(systemUsedByEntity);
					}
				}
			}
			// Json Custom Column Values
			else if (customColumn.getColumnGroup().getCustomColumnType() == CustomColumnTypes.COLUMN_JSON_VALUE) {
				AdvancedReadOnlyDAO<IdentityObject, Criteria> beanDao = (AdvancedReadOnlyDAO<IdentityObject, Criteria>) getDaoLocator().locate(column.getTable().getName());
				List<IdentityObject> list = beanDao.findBySearchCriteria((HibernateSearchConfigurer) criteria -> criteria.add(new CustomJsonStringExpression("=", ((SystemColumnStandard) customColumn.getColumnGroup().getJsonValueColumn()).getBeanPropertyName(), customColumn.getName(), entityId)));
				if (CollectionUtils.getSize(list) > 500 && usePlaceHolderForLargeRecords) {
					usedByList.add(new SystemUsedByEntity(column, CollectionUtils.getSize(list)));
				}
				else {
					for (IdentityObject bean : CollectionUtils.getIterable(list)) {
						SystemUsedByEntity systemUsedByEntity = new SystemUsedByEntity(column, bean);
						populateModificationInformation(systemUsedByEntity, bean);
						usedByList.add(systemUsedByEntity);
					}
				}
			}
			else {
				// Dated Custom Column Values
				SystemColumnDatedValueSearchForm valueSearchForm = new SystemColumnDatedValueSearchForm();
				valueSearchForm.setColumnId(column.getId());
				valueSearchForm.setValueEquals("" + entityId);
				List<SystemColumnDatedValue> valueList = getSystemColumnValueService().getSystemColumnDatedValueList(valueSearchForm);

				if (CollectionUtils.getSize(valueList) > 500 && usePlaceHolderForLargeRecords) {
					usedByList.add(new SystemUsedByEntity(column, CollectionUtils.getSize(valueList)));
				}
				else {
					for (SystemColumnDatedValue val : CollectionUtils.getIterable(valueList)) {
						SystemUsedByEntity systemUsedByEntity = new SystemUsedByEntity(column, dao.findByPrimaryKey(val.getFkFieldId()));
						populateModificationInformation(systemUsedByEntity, val);
						usedByList.add(systemUsedByEntity);
					}
				}
			}
		}
		return usedByList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////             System Relationships Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	private void appendUsedByForSystemRelationships(SystemColumn primaryKeyColumn, Serializable entityId, List<SystemUsedByEntity> usedByList) {
		// Find all relationships where this is the parent column
		SystemRelationshipSearchForm searchForm = new SystemRelationshipSearchForm();
		searchForm.setParentColumnId(primaryKeyColumn.getId());
		searchForm.setExcludeFromUsedBy(false);
		List<SystemRelationship> relationshipList = getSystemSchemaService().getSystemRelationshipList(searchForm);

		for (SystemRelationship rel : CollectionUtils.getIterable(relationshipList)) {
			// Filtered above in the lookup, but just in case better to ensure it's excluded in case the search filters break
			if (rel.isExcludeFromUsedBy() || rel.getColumn() == null) {
				continue;
			}
			usedByList.addAll(getSystemUsedByEntityListForColumnImpl(entityId, rel.getColumn(), true));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////             System Column Custom Values               /////////////
	////////////////////////////////////////////////////////////////////////////


	private void appendUsedByForSystemColumnCustomValues(SystemColumn primaryKeyColumn, Serializable entityId, List<SystemUsedByEntity> usedByList) {
		// Find any Custom Columns that may reference this entity
		SystemColumnSearchForm columnSearchForm = new SystemColumnSearchForm();
		columnSearchForm.setValueTableId(primaryKeyColumn.getTable().getId());
		columnSearchForm.setCustomOnly(true);
		List<SystemColumnCustom> customColumnsUsedByList = getSystemColumnService().getSystemColumnCustomList(columnSearchForm);

		for (SystemColumnCustom custom : CollectionUtils.getIterable(customColumnsUsedByList)) {
			usedByList.addAll(getSystemUsedByEntityListForColumnImpl(entityId, custom, true));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////         System User Interface Used By Aware           /////////////
	////////////////////////////////////////////////////////////////////////////


	private void appendUsedByForSystemUserInterfaceUsedByAware(SystemColumn primaryKeyColumn, Serializable
			entityId, List<SystemUsedByEntity> usedByList, Map<String, SystemColumn> primaryKeyColumnMap) {
		// Find any SystemUserInterfaceUsedByAware objects that may reference this entity
		Collection<ReadOnlyDAO<? extends SystemUserInterfaceUsedByAware>> daoList = getDaoLocator().locateAll(SystemUserInterfaceUsedByAware.class);
		for (ReadOnlyDAO<? extends SystemUserInterfaceUsedByAware> dao : CollectionUtils.getIterable(daoList)) {
			List<? extends SystemUserInterfaceUsedByAware> list = dao.findByField("valueTable.id", primaryKeyColumn.getTable().getId());
			for (SystemUserInterfaceUsedByAware uiAwareBean : CollectionUtils.getIterable(list)) {
				SystemUserInterfaceValueUsedByConfig uiValueConfig = uiAwareBean.getSystemUserInterfaceValueConfig();
				if (uiValueConfig == null) {
					continue;
				}
				usedByList.addAll(getSystemUsedByEntityListForUsedByAwareBean(entityId, uiAwareBean, uiValueConfig, true, primaryKeyColumnMap));
			}
		}
	}


	/**
	 * Returns all entities related to given entity by it's reference in the given UI Aware bean.
	 *
	 * @param usePlaceHolderForLargeRecords - If more than 500 records and viewing all - returns a place holder with drill down.  Specific column drill down then exposes all records
	 */
	private List<SystemUsedByEntity> getSystemUsedByEntityListForUsedByAwareBean(Serializable entityId, SystemUserInterfaceUsedByAware
			uiAwareBean, SystemUserInterfaceValueUsedByConfig uiValueConfig, boolean usePlaceHolderForLargeRecords, Map<String, SystemColumn> primaryKeyColumnMap) {
		List<SystemUsedByEntity> usedByList = new ArrayList<>();

		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(uiValueConfig.getValueTableName());

		// Special Exclude, sometimes we need to use Long instead of Integers for PK fields...when this happens it's usually because the table is very large
		// And in these cases we wouldn't want Used By to include them, even if the relationship hasn't been explicitly excluded.
		// Can't think of a case where this would apply for custom ui aware, but just in case
		if (DataTypes.IDENTITY_LONG == dao.getConfiguration().getPrimaryKeyColumn().getDataType()) {
			return usedByList;
		}

		List<IdentityObject> list = dao.findByFields(new String[]{uiValueConfig.getValueUIAwareBeanPropertyName() + ".id", uiValueConfig.getValueBeanPropertyName()},
				new Object[]{uiAwareBean.getIdentity(), "" + entityId}); // EntityID needs to be passed as a String

		ReadOnlyDAO<IdentityObject> usedByDao = getDaoLocator().locate(uiValueConfig.getValueOwnerTableName());
		SystemColumn pkColumn = getPrimaryKeyColumnForTable(uiValueConfig.getValueOwnerTableName(), primaryKeyColumnMap);

		if (CollectionUtils.getSize(list) > 500 && usePlaceHolderForLargeRecords) {
			usedByList.add(new SystemUsedByEntity(uiAwareBean.getUsedByLabel(), CollectionUtils.getSize(list)));
		}
		else {
			for (IdentityObject val : CollectionUtils.getIterable(list)) {
				IdentityObject usedByEntity = null;
				if (!StringUtils.isEmpty(uiValueConfig.getValueOwnerBeanPropertyName())) {
					usedByEntity = (IdentityObject) BeanUtils.getPropertyValue(val, uiValueConfig.getValueOwnerBeanPropertyName());
				}
				else if (usedByDao != null) {
					Serializable usedByEntityId = (Serializable) BeanUtils.getPropertyValue(val, uiValueConfig.getValueOwnerFkFieldIdPropertyName());
					usedByEntityId = dao.convertToPrimaryKeyDataType(usedByEntityId);
					usedByEntity = usedByDao.findByPrimaryKey(usedByEntityId);
				}
				if (usedByEntity != null) {
					SystemUsedByEntity systemUsedByEntity = new SystemUsedByEntity(uiAwareBean.getUsedByLabel(), pkColumn, usedByEntity);
					populateModificationInformation(systemUsedByEntity, usedByEntity);
					usedByList.add(systemUsedByEntity);
				}
			}
		}
		return usedByList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////               System Soft Link Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	private List<SystemSoftLink> getSoftLinkList() {
		if (this.softLinkList == null) {
			populateSoftLinkList();
		}
		return this.softLinkList;
	}


	@Override
	public List<SystemSoftLink> getSystemSoftLinkList() {
		return BeanUtils.sortWithFunction(getSoftLinkList(), SystemSoftLink::getCoalesceOwnerTableName, true);
	}


	private void appendUsedByForSoftLinks(String tableName, Serializable entityId, List<SystemUsedByEntity> usedByList, Map<String, SystemColumn> primaryKeyColumnMap) {
		ReadOnlyDAO<IdentityObject> beanDao = getDaoLocator().locate(tableName);
		IdentityObject bean = beanDao.findByPrimaryKey(entityId);

		for (SystemSoftLink softLink : CollectionUtils.getIterable(getSoftLinkList())) {
			if (!softLink.isExcludeFromUsedBy()) {
				// All Soft Links that Reference Me
				usedByList.addAll(getUsedByForSoftLinkImpl(getPrimaryKeyColumnForTable(tableName, primaryKeyColumnMap), entityId, softLink, primaryKeyColumnMap, true));
			}

			// If I'm the Soft Link or it's owner - show the entities that it's linked to - i.e. As a Note - Click Used By I want to see the Account, Trade, Security, etc. that I'm used by
			// Even if used by is excluded - these are generally one entity look ups and can be useful to see what the entity
			// is linked to.  i.e. I might not want to see all Notes under Used By, but for a specific note I still would want to see what account, Trade, etc. I'm linked to.
			usedByList.addAll(getUsedByAsSoftLinkImpl(tableName, bean, softLink, primaryKeyColumnMap));
		}
	}


	/**
	 * Returns the SoftLink Entities that reference the entity
	 * Trade -> Returns SystemNotes
	 */
	private List<SystemUsedByEntity> getUsedByForSoftLinkImpl(SystemColumn primaryKeyColumn, Serializable entityId, SystemSoftLink
			softLink, Map<String, SystemColumn> primaryKeyColumnMap,
	                                                          boolean usePlaceHolderForLargeRecords) {
		List<SystemUsedByEntity> usedByList = new ArrayList<>();

		List<IdentityObject> list = getSoftLinkEntityListReferencedByTableAndValue(softLink, primaryKeyColumn.getTable(), entityId);

		if (CollectionUtils.getSize(list) > 500 && usePlaceHolderForLargeRecords) {
			usedByList.add(new SystemUsedByEntity(softLink.getLabel(), CollectionUtils.getSize(list)));
		}
		else {
			for (IdentityObject bean : CollectionUtils.getIterable(list)) {
				if (softLink.getOwnerBeanPropertyName() != null) {
					IdentityObject owner = (IdentityObject) BeanUtils.getPropertyValue(bean, softLink.getOwnerBeanPropertyName());
					SystemUsedByEntity ube = new SystemUsedByEntity(getPrimaryKeyColumnForTable(softLink.getOwnerTableName(), primaryKeyColumnMap), owner);
					ube.setLabel(softLink.getLabel());
					populateModificationInformation(ube, owner);
					usedByList.add(ube);
				}
				else {
					SystemUsedByEntity ube = new SystemUsedByEntity(getPrimaryKeyColumnForTable(softLink.getTableName(), primaryKeyColumnMap), bean);
					ube.setLabel(softLink.getLabel());
					populateModificationInformation(ube, bean);
					usedByList.add(ube);
				}
			}
		}
		return usedByList;
	}


	/**
	 * Returns a list of entities in the system that are referenced by a specific SoftLinkEntity and SoftLink
	 * Many times will return one object unless it's using an owner in which case there could be multiple
	 * <p>
	 * Example:
	 * 1. For a Trade SystemNote - and SoftLink for fkField - Would return the Trade referenced
	 * 2. For a Trade SystemNote - and SoftLink for SystemNoteLink fkField - Would return all Trades associated with the note through the SystemNoteLink table.
	 */
	private List<SystemUsedByEntity> getUsedByAsSoftLinkImpl(String tableName, IdentityObject softLinkBean, SystemSoftLink
			softLink, Map<String, SystemColumn> primaryKeyColumnMap) {
		List<SystemUsedByEntity> usedByList = new ArrayList<>();
		if (tableName.equals(softLink.getTableName())) {
			SystemUsedByEntity e = getUsedByEntityReferencedBySoftLinkImpl(softLinkBean, softLink, primaryKeyColumnMap);
			if (e != null) {
				usedByList.add(e);
			}
		}
		// If I'm the Owner of a Soft link - find those entities and include them
		else if (tableName.equals(softLink.getOwnerTableName())) {
			List<IdentityObject> children = getChildrenForSoftLinkOwner(softLink, (Number) softLinkBean.getIdentity());

			for (IdentityObject child : CollectionUtils.getIterable(children)) {
				SystemUsedByEntity e = getUsedByEntityReferencedBySoftLinkImpl(child, softLink, primaryKeyColumnMap);
				if (e != null) {
					usedByList.add(e);
				}
			}
		}
		return usedByList;
	}


	/**
	 * Returns the SystemUsedByEntity representing the single entity referenced by one SoftLinkEntity value
	 */
	private SystemUsedByEntity getUsedByEntityReferencedBySoftLinkImpl(IdentityObject softLinkBean, SystemSoftLink softLink, Map<String, SystemColumn> primaryKeyColumnMap) {
		Number fkFieldId = (Number) BeanUtils.getPropertyValue(softLinkBean, softLink.getBeanPropertyName());
		if (fkFieldId != null) {
			SystemTable valueTable = (SystemTable) BeanUtils.getPropertyValue(softLinkBean, softLink.getTableBeanPropertyName());
			if (valueTable == null && !StringUtils.isEmpty(softLink.getSecondTableBeanPropertyName())) {
				valueTable = (SystemTable) BeanUtils.getPropertyValue(softLinkBean, softLink.getSecondTableBeanPropertyName());
			}
			if (valueTable != null) {
				ReadOnlyDAO<IdentityObject> fkBeanDao = getDaoLocator().locate(valueTable.getName());
				IdentityObject fkBean = fkBeanDao.findByPrimaryKey(fkFieldId);
				if (fkBean != null) {
					SystemUsedByEntity usedByEntity = new SystemUsedByEntity(getPrimaryKeyColumnForTable(valueTable.getName(), primaryKeyColumnMap), fkBean);
					populateModificationInformation(usedByEntity, fkBean);
					usedByEntity.setLabel(valueTable.getLabel() + ": " + softLink.getLabel());
					return usedByEntity;
				}
			}
		}
		return null;
	}


	/**
	 * Returns a list of soft link entities in the system that reference a specific entity
	 * <p>
	 * Example:
	 * 1. For a Trade - SystemNote - and SoftLink for fkField - Would return all notes where noteType.table = "Trade" and fkFieldId = TradeID
	 * 2. For a Trade - SystemNote - and SoftLink for SystemNoteLink fkField - Would return all notes where SystemNoteLink note.noteType.table = "Trade" and fkFieldId = TradeID
	 */
	private List<IdentityObject> getSoftLinkEntityListReferencedByTableAndValue(final SystemSoftLink softLink, final SystemTable table, final Serializable entityId) {
		ReadOnlyDAO<IdentityObject> linkAwareDao = getDaoLocator().locate(softLink.getTableName());

		// Support for nested properties - Copied for Owner Lookup Too - can it be merged together?
		HibernateSearchConfigurer searchConfig = new HibernateSearchConfigurer() {

			@Override
			public void configureCriteria(Criteria criteria) {
				if (!StringUtils.isEmpty(softLink.getSecondTableBeanPropertyName())) {
					String firstTableAlias = getTablePropertyAlias(criteria, softLink.getTableBeanPropertyName(), "x");
					String secondTableAlias = getTablePropertyAlias(criteria, softLink.getSecondTableBeanPropertyName(), "y");
					criteria.add(Restrictions.or(Restrictions.eq(firstTableAlias, table.getId()),
							Restrictions.and(Restrictions.isNull(firstTableAlias), Restrictions.eq(secondTableAlias, table.getId()))));
				}
				else {
					criteria.add(Restrictions.eq(getTablePropertyAlias(criteria, softLink.getTableBeanPropertyName(), "x"), table.getId()));
				}
				criteria.add(Restrictions.eq(softLink.getBeanPropertyName(), DaoUtils.convertToDataType(entityId, softLink.getBeanDataType(), softLink.getClass())));
			}


			private String getTablePropertyAlias(Criteria criteria, String tableBeanPropertyName, String aliasPrefix) {
				int dotIndex = tableBeanPropertyName.indexOf('.');
				String alias = "";
				String path = tableBeanPropertyName;
				int count = 0;
				while (dotIndex != -1) {
					count++;
					criteria.createAlias(alias + path.substring(0, dotIndex), aliasPrefix + count, JoinType.LEFT_OUTER_JOIN);
					alias = (aliasPrefix + count) + ".";
					path = path.substring(dotIndex + 1);
					dotIndex = path.indexOf('.');
				}
				alias += (path + ".id");
				return alias;
			}
		};
		return ((AdvancedReadOnlyDAO<IdentityObject, Criteria>) linkAwareDao).findBySearchCriteria(searchConfig);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////


	private void populateSoftLinkList() {
		List<SystemSoftLink> list = new ArrayList<>();
		PropertyDataTypeConverter converter = new PropertyDataTypeConverter();
		Collection<ReadOnlyDAO<?>> daoList = getDaoLocator().locateAll();
		for (ReadOnlyDAO<?> dao : CollectionUtils.getIterable(daoList)) {
			Field[] fields = ClassUtils.getClassFields(dao.getConfiguration().getBeanClass(), true, true);
			for (Field field : fields) {
				SoftLinkField softLinkAnnotation = AnnotationUtils.findAnnotation(field, SoftLinkField.class);
				if (softLinkAnnotation != null) {
					String ownerTableName = null;
					if (!StringUtils.isEmpty(softLinkAnnotation.ownerBeanPropertyName())) {
						Class<?> ownerDtoClass = BeanUtils.getPropertyType(dao.getConfiguration().getBeanClass(), softLinkAnnotation.ownerBeanPropertyName());
						if (!IdentityObject.class.isAssignableFrom(ownerDtoClass)) {
							throw new IllegalArgumentException("Invalid [ownerBeanPropertyName] selected for SoftLinkField annotation for field [" + dao.getConfiguration().getBeanClass().getName()
									+ "." + field.getName() + "].  Owner property must be an Identity Object, but selected property [" + softLinkAnnotation.ownerBeanPropertyName() + "] is ["
									+ ownerDtoClass.getName() + "]");
						}
						@SuppressWarnings("unchecked")
						ReadOnlyDAO<IdentityObject> ownerDao = getDaoLocator().locate((Class<IdentityObject>) ownerDtoClass);
						ownerTableName = ownerDao.getConfiguration().getTableName();
					}

					// The column that is set on the Used By Entity is used for drill down support, so we want to "label" these with the actual
					// field that the reference is from, but make sure we use the right column for drill down support
					String label = dao.getConfiguration().getTableName() + "." + dao.getConfiguration().getDBFieldName(field.getName());
					SystemSoftLink softLink;
					if (softLinkAnnotation.propertyDataType() == DataTypes.NONE) {
						softLink = new SystemSoftLink(label, dao.getConfiguration().getTableName(), field, converter.convert(BeanUtils.getPropertyDescriptor(dao.getConfiguration()
								.getBeanClass(), field.getName())), softLinkAnnotation, ownerTableName);
					}
					else {
						softLink = new SystemSoftLink(label, dao.getConfiguration().getTableName(), field, softLinkAnnotation.propertyDataType(), softLinkAnnotation, ownerTableName);
					}

					list.add(softLink);
				}
			}
		}
		this.softLinkList = list;
	}


	/**
	 * Returns all the children associated with the owner
	 * i.e. Returns a SystemNoteLinks for a SystemNote, Returns all SystemHierarchyLinks for a SystemHierarchy
	 */
	private List<IdentityObject> getChildrenForSoftLinkOwner(final SystemSoftLink softLink, final Number entityId) {
		ReadOnlyDAO<IdentityObject> childDao = getDaoLocator().locate(softLink.getTableName());

		// Support for nested properties - Copied for Owner Lookup Too - can it be merged together?
		HibernateSearchConfigurer searchConfig = criteria -> {
			int dotIndex = softLink.getOwnerBeanPropertyName().indexOf('.');
			String alias = "";
			String path = softLink.getOwnerBeanPropertyName();
			int count = 0;
			while (dotIndex != -1) {
				count++;
				criteria.createAlias(alias + path.substring(0, dotIndex), "x" + count);
				alias = ("x" + count) + ".";
				path = path.substring(dotIndex + 1);
				dotIndex = path.indexOf('.');
			}
			alias += (path + ".id");
			criteria.add(Restrictions.eq(alias, entityId));
		};
		return ((AdvancedReadOnlyDAO<IdentityObject, Criteria>) childDao).findBySearchCriteria(searchConfig);
	}


	/**
	 * Find the Primary Key Column - i.e. bean property name = id
	 * Store it in a map so don't repeat the same look ups for the same table during a Used By Lookup
	 */
	private SystemColumn getPrimaryKeyColumnForTable(String tableName, Map<String, SystemColumn> primaryKeyColumnMap) {
		if (!primaryKeyColumnMap.containsKey(tableName)) {
			SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
			searchForm.setTableName(tableName);
			searchForm.setBeanPropertyNameEquals("id");
			searchForm.setStandardOnly(true);
			searchForm.setDefaultDataSource(true); // same table might be present in multiple data sources
			List<SystemColumnStandard> columnList = getSystemColumnService().getSystemColumnList(searchForm);
			ValidationUtils.assertTrue(CollectionUtils.getSize(columnList) == 1,
					"Expected one column in table [" + tableName + "] with bean property name [id] but found [" + CollectionUtils.getSize(columnList)
							+ "].  Unable to generate Used by list for this entity.");
			primaryKeyColumnMap.put(tableName, columnList.get(0));
		}
		return primaryKeyColumnMap.get(tableName);
	}


	/**
	 * Update the provided {@link SystemUsedByEntity} with created date and user if the {@link IdentityObject} parameter
	 * is an {@link UpdatableEntity}.
	 */
	private void populateModificationInformation(SystemUsedByEntity systemUsedByEntity, IdentityObject identityObject) {
		if (identityObject != null && UpdatableEntity.class.isAssignableFrom(identityObject.getClass())) {
			UpdatableEntity updatableEntity = (UpdatableEntity) identityObject;
			systemUsedByEntity.setCreateUserId(updatableEntity.getCreateUserId());
			systemUsedByEntity.setCreateDate(updatableEntity.getCreateDate());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}
}
