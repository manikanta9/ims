package com.clifton.system.usedby.softlink;


import com.clifton.core.shared.dataaccess.DataTypes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>SoftLinkField</code> annotation marks a field as holding the value for a SoftLink,
 * generally these fields are fkFieldId, linkedFkFieldId, etc.  These are helpful for "used by" feature to look up where an entity is used.
 * <p>
 * Example:  When looking to see where a specific replication is used (which could include custom fields), we annotate the custom field
 * FKFieldID value so when retrieving usages for a specific replication, we can filter by soft link references with
 * table name = InvestmentReplication and fkFieldId value = id of the replication we are looking up.
 *
 * @author manderson
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SoftLinkField {

	/**
	 * Specifies the bean property name where the table is defined for this "SoftLink"
	 * For cases where the table is defined in multiple places, this should be the first table to check and if it's null
	 * will use the secondTableBeanPropertyName (For example see SystemWarning where the cause table can be directly set, otherwise it uses the cause table from the warning type)
	 */
	String tableBeanPropertyName();


	String secondTableBeanPropertyName() default "";


	/**
	 * The "owner" bean which is used to display what really is using the entity.
	 * For example, SystemHierarchyLink is "owned" by SystemHierarchy so when
	 * showing used by we want to see the actual hierarchies
	 * <p>
	 * SystemNotes don't use an owner, but SystemNoteLinks do, so in both cases the entity using the note
	 * would show the note, not the link
	 */
	String ownerBeanPropertyName() default "";


	/**
	 * Optionally can exclude from used by
	 * Only applies to the used by when looking at entities that reference this field,
	 * i.e. All Warnings tied to a security, trade, etc.  For the warning itself would still show it's own personal
	 * soft link references
	 * <p>
	 * NOTE: Not currently used, and without examples of usage of SoftLinks in Used by not sure if we'll need this
	 */
	boolean excludeFromUsedBy() default false;


	/**
	 * Optionally used to explicitly set the DataType for the soft link reference.
	 * If the data type is set here, then that data type should be used instead of inferring the type.
	 */
	DataTypes propertyDataType() default DataTypes.NONE;
}
