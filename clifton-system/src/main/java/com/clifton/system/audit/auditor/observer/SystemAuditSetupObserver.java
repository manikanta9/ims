package com.clifton.system.audit.auditor.observer;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.audit.SystemAuditService;
import com.clifton.system.audit.SystemAuditType;
import com.clifton.system.audit.auditor.SystemAuditorRegistrator;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemAuditSetupObserver</code> class monitors updates to {@link SystemTable} and {@link SystemColumn}
 * objects and registers/unregisters corresponding field-level auditors.
 *
 * @param <T>
 * @author vgomelsky
 */
@Component
public class SystemAuditSetupObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private SystemAuditorRegistrator systemAuditorRegistrator;
	private SystemAuditService systemAuditService;

	private SystemColumnService systemColumnService;


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			// get bean's table with all columns populated
			SystemTable table;
			if (bean instanceof SystemTable) {
				table = (SystemTable) bean;
				SystemAuditType auditType = table.getAuditType();
				// ORIGINAL BEAN WAS ALREADY UPDATED - NEED TO GET IT BEFORE THE METHOD CALL
				//SystemTable originalTable = getSystemSchemaService().getSystemTable(table.getId());
				//if (CompareUtils.isEqual(auditType, originalTable.getAuditType())) {
				// no changes to audit type: skip registration
				//return;
				//}
				if (auditType != null && !auditType.isNewBean()) {
					table.setAuditType(getSystemAuditService().getSystemAuditType(auditType.getId()));
				}
				table.setColumnList(getSystemColumnService().getSystemColumnStandardListByTable(table.getId()));
			}
			else if (bean instanceof SystemColumnStandard) {
				// Do nothing for deletes?  those should only happen when replicating from an external system and a column was deleted
				if (event.isDelete()) {
					return;
				}
				SystemColumnStandard column = (SystemColumnStandard) bean;
				SystemColumn originalColumn = getSystemColumnService().getSystemColumn(column.getId());
				ValidationUtils.assertNotNull(originalColumn, "Cannot find original SystemColumn using id for: " + column);
				table = originalColumn.getTable();
				SystemAuditType auditType = column.getAuditType();
				//if (CompareUtils.isEqual(auditType, originalColumn.getAuditType())) {
				// no changes to audit type: skip registration
				//return;
				//}
				if (auditType != null && !auditType.isNewBean()) {
					column.setAuditType(getSystemAuditService().getSystemAuditType(auditType.getId()));
				}
				table.setColumnList(getSystemColumnService().getSystemColumnStandardListByTable(table.getId()));
				int index = table.getColumnList().indexOf(column);
				if (index != -1) {
					table.getColumnList().set(index, column);
				}
			}
			else if (bean instanceof SystemColumnCustom) {
				return;
			}
			else {
				throw new IllegalArgumentException("The argument bean must be a SystemTable or SystemColumn not: " + bean.getClass());
			}

			// register/unregister corresponding auditors
			SystemAuditType auditType = getSystemAuditorRegistrator().getSystemAuditTypeMerged(table);
			try {
				getSystemAuditorRegistrator().registerAuditors(table.getName(), auditType, true);
			}
			catch (NoSuchBeanDefinitionException e2) {
				if (!table.getDataSource().isDefaultDataSource()) {
					// Do Nothing - No DAO for non default datasource is OK
					return;
				}
				throw (e2);
			}
		}
	}


	@Override
	public void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, @SuppressWarnings("unused") T bean) {
		// do nothing
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemAuditorRegistrator getSystemAuditorRegistrator() {
		return this.systemAuditorRegistrator;
	}


	public void setSystemAuditorRegistrator(SystemAuditorRegistrator systemAuditorRegistrator) {
		this.systemAuditorRegistrator = systemAuditorRegistrator;
	}


	public SystemAuditService getSystemAuditService() {
		return this.systemAuditService;
	}


	public void setSystemAuditService(SystemAuditService systemAuditService) {
		this.systemAuditService = systemAuditService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}
}
