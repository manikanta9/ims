package com.clifton.system.audit.auditor.observer;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.system.audit.SystemAuditEvent;
import com.clifton.system.audit.auditor.AuditFieldMetaData;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemDataTypeConverter;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemDaoAuditUpdateObserver</code> class is a DAO observer that should be registered for DAO's
 * that weed to provide field level auditing for UPDATE operations.
 *
 * @author vgomelsky
 */
@Component
public class SystemDaoAuditUpdateObserver<T extends IdentityObject> extends SystemDaoAuditDeleteObserver<T> {

	public static final String BEAN_CONTEXT_KEY = SystemDaoAuditUpdateObserver.class.getName();


	public SystemDaoAuditUpdateObserver() {
		super();
		setOrder(-1000); // make sure that audit observer is always the first one to run
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// audit in the afterTransactionMethodCallImpl so that updates to a bean via after method workflow actions are picked up by audit
	}


	@Override
	public void afterTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// don't audit if there was an exception thrown (nothing was saved)
		if (e == null) {
			AssertUtils.assertTrue(DaoEventTypes.UPDATE == event, "Unexpected event type. Expected UPDATE but was: ", event);

			boolean customData = (bean instanceof SystemColumnValue);
			boolean textAndValueAreSame = customData && ((SystemColumnValue) bean).getValue().equals(((SystemColumnValue) bean).getText());

			Date auditDate = new Date();
			short securityUserId = (Short) ((IdentityObject) getContextHandler().getBean(Context.USER_BEAN_NAME)).getIdentity();
			Map<String, Object> propertyValueMap = BeanUtils.describe(bean);
			Long parentPKFieldId = getParentPKFieldId(propertyValueMap, dao);
			T originalBean = super.getOriginalBean(dao, bean);
			Map<String, Object> originalPropertyValueMap = BeanUtils.describe(originalBean);

			List<AuditFieldMetaData> fieldList = filterAuditFieldList(getSystemAuditMetaDataHandler().getUpdateFieldInfo(dao.getConfiguration().getTableName()));
			for (AuditFieldMetaData field : CollectionUtils.getIterable(fieldList)) {
				Object value = propertyValueMap.get(field.getPropertyName());
				Object originalValue = originalPropertyValueMap.get(field.getPropertyName());
				// If Do Not Audit Updates From Null and Original Value is Null continue on
				if ((field.getAuditUpdateType() != null && field.getAuditUpdateType().isSkipAudit(originalValue)) || (textAndValueAreSame && "text".equals(field.getPropertyName()))) {
					continue;
				}
				// "smart" comparison: 1.00 is same as 1 for BigDecimal, etc.
				if (isDifferentValue(value, originalValue, bean, field)) {
					if (bean instanceof SystemColumnCustomJsonAware && value instanceof CustomJsonString) {
						generateAuditEventsForCustomJsonValue(field.getColumnId(), (SystemColumnCustomJsonAware) bean, (SystemColumnCustomJsonAware) originalBean, (CustomJsonString) value, (CustomJsonString) originalValue, auditDate, securityUserId, parentPKFieldId);
					}
					else {
						SystemAuditEvent auditEvent = new SystemAuditEvent();
						auditEvent.setSystemColumnId((customData ? ((SystemColumnValue) bean).getColumn().getId() : field.getColumnId()));
						auditEvent.setMainPKFieldId((customData ? ((SystemColumnValue) bean).getFkFieldId() : ((Number) bean.getIdentity())).longValue());
						auditEvent.setAuditDate(auditDate);
						auditEvent.setSecurityUserId(securityUserId);
						auditEvent.setAuditTypeId(SystemAuditEvent.AUDIT_UPDATE);
						auditEvent.setParentPKFieldId(parentPKFieldId);
						auditEvent.setOldValue(truncateValue(originalValue, field.getMaxFieldSize()));
						auditEvent.setNewValue(truncateValue(value, field.getMaxFieldSize()));
						getSystemAuditService().saveSystemAuditEvent(auditEvent);
					}
				}
			}
		}
	}


	private boolean isDifferentValue(Object value, Object originalValue, T bean, AuditFieldMetaData fieldMetaData) {
		// "smart" comparison: 1.00 is same as 1 for BigDecimal, etc.
		if (value instanceof String && originalValue instanceof String && bean instanceof SystemColumnValue) {
			// for DECIMAL custom columns try formatting them first so that "2045" is considered equal to "2,045"
			String stringValue = (String) value;
			String stringOriginalValue = (String) originalValue;
			if (!StringUtils.isEmpty(stringValue) && !StringUtils.isEmpty(stringOriginalValue)) {
				SystemColumnCustom column = ((SystemColumnValue) bean).getColumn();
				SystemDataType valueType = column.getDataType();
				// Do Not Convert "Numeric" Values for Text field If they use a Value Table (a select list is used where value is the id and text is a string label)
				if (!("text".equals(fieldMetaData.getPropertyName()) && valueType.isNumeric() && column.getValueTable() != null)) {
					SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(valueType);
					value = dataTypeConverter.convert(stringValue);
					originalValue = dataTypeConverter.convert(stringOriginalValue);
				}
			}
		}
		// Decimal Precision would only be set for DECIMAL data types, which should only apply to BigDecimal object types
		if (fieldMetaData.getDecimalPrecision() != null && value instanceof BigDecimal && originalValue instanceof BigDecimal) {
			return (CompareUtils.compare(MathUtils.round((BigDecimal) value, fieldMetaData.getDecimalPrecision()), MathUtils.round((BigDecimal) originalValue, fieldMetaData.getDecimalPrecision())) != 0);
		}
		/*
		 * Note: The below comparison method performs case-insensitive comparisons for strings. String case comparisons will not show up in audit trail records. This reduces
		 * clutter in the audit trail. If we desire to include audit trail records for case changes at some point in the future, we may add a special condition for these updates
		 * here.
		 */
		return (CompareUtils.compare(value, originalValue) != 0);
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		AssertUtils.assertTrue(DaoEventTypes.UPDATE == event, "Unexpected event type. Expected UPDATE but was: ", event);
		// get original bean so that it can be used for comparison after update
		super.getOriginalBean(dao, bean);
	}
}
