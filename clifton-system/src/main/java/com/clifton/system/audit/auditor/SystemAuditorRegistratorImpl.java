package com.clifton.system.audit.auditor;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.audit.SystemAuditType;
import com.clifton.system.audit.auditor.observer.SystemDaoAuditDeleteObserver;
import com.clifton.system.audit.auditor.observer.SystemDaoAuditInsertObserver;
import com.clifton.system.audit.auditor.observer.SystemDaoAuditUpdateObserver;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>SystemAuditorRegistratorImpl</code> class implements the {@link SystemAuditorRegistrator} interface.
 *
 * @author vgomelsky
 */
@Component
public class SystemAuditorRegistratorImpl<T extends IdentityObject> implements SystemAuditorRegistrator, CurrentContextApplicationListener<ContextRefreshedEvent> {

	/**
	 * Used to limit observer registration to the current application's dataSource
	 */
	private String defaultDataSourceDatabaseName;

	private DaoLocator daoLocator;

	private SystemDaoAuditInsertObserver<T> systemDaoAuditInsertObserver;
	private SystemDaoAuditUpdateObserver<T> systemDaoAuditUpdateObserver;
	private SystemDaoAuditDeleteObserver<T> systemDaoAuditDeleteObserver;

	private SystemSchemaService systemSchemaService;

	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			// get the dataSource databaseName - will filter tables based on it
			String dataSourceDatabaseName = getDefaultDataSourceDatabaseName();
			// get only audited tables (include audited columns)
			List<SystemTable> auditedTableList = getSystemSchemaService().getSystemTableListAudited();
			for (SystemTable table : CollectionUtils.getIterable(auditedTableList)) {
				if (dataSourceDatabaseName == null || dataSourceDatabaseName.equalsIgnoreCase(table.getDataSource().getDatabaseName())) {
					SystemAuditType auditType = getSystemAuditTypeMerged(table);
					registerAuditors(table.getName(), auditType, false);
				}
			}
		}
		catch (Throwable e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error registering DAO auditors: " + event, e);
		}
	}


	@Override
	@SuppressWarnings("unchecked")
	public void registerAuditors(String tableName, SystemAuditType auditType, boolean unregisterIfFalse) {
		// get DAO that's being audited
		ObserverableDAO<T> dao = (ObserverableDAO<T>) getDaoLocator().locate(tableName);

		// register observers as necessary
		if (auditType.isAuditInsert()) {
			dao.registerEventObserver(DaoEventTypes.INSERT, getSystemDaoAuditInsertObserver());
		}
		else if (unregisterIfFalse) {
			dao.unregisterEventObserver(DaoEventTypes.INSERT, getSystemDaoAuditInsertObserver());
		}

		if (auditType.isAuditUpdate()) {
			dao.registerEventObserver(DaoEventTypes.UPDATE, getSystemDaoAuditUpdateObserver());
		}
		else if (unregisterIfFalse) {
			dao.unregisterEventObserver(DaoEventTypes.UPDATE, getSystemDaoAuditUpdateObserver());
		}

		if (auditType.isAuditDelete()) {
			dao.registerEventObserver(DaoEventTypes.DELETE, getSystemDaoAuditDeleteObserver());
		}
		else if (unregisterIfFalse) {
			dao.unregisterEventObserver(DaoEventTypes.DELETE, getSystemDaoAuditDeleteObserver());
		}
	}


	@Override
	public SystemAuditType getSystemAuditTypeMerged(SystemTable table) {
		SystemAuditType result = new SystemAuditType();
		// determine what audit types are enabled for this DAO
		SystemAuditType auditType = table.getAuditType();
		if (auditType != null) {
			result.setAuditInsert(auditType.isAuditInsert());
			result.setAuditUpdate(auditType.isAuditUpdate());
			result.setAuditDelete(auditType.isAuditDelete());
		}
		for (SystemColumnStandard column : CollectionUtils.getIterable(table.getColumnList())) {
			SystemAuditType columnAuditType = column.getAuditType();
			if (columnAuditType != null) {
				if (columnAuditType.isAuditInsert()) {
					result.setAuditInsert(true);
				}
				if (columnAuditType.isAuditUpdate()) {
					result.setAuditUpdate(true);
				}
				if (columnAuditType.isAuditDelete()) {
					result.setAuditDelete(true);
				}
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDefaultDataSourceDatabaseName() {
		return this.defaultDataSourceDatabaseName;
	}


	public void setDefaultDataSourceDatabaseName(String defaultDataSourceDatabaseName) {
		this.defaultDataSourceDatabaseName = defaultDataSourceDatabaseName;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemDaoAuditInsertObserver<T> getSystemDaoAuditInsertObserver() {
		return this.systemDaoAuditInsertObserver;
	}


	public void setSystemDaoAuditInsertObserver(SystemDaoAuditInsertObserver<T> systemDaoAuditInsertObserver) {
		this.systemDaoAuditInsertObserver = systemDaoAuditInsertObserver;
	}


	public SystemDaoAuditUpdateObserver<T> getSystemDaoAuditUpdateObserver() {
		return this.systemDaoAuditUpdateObserver;
	}


	public void setSystemDaoAuditUpdateObserver(SystemDaoAuditUpdateObserver<T> systemDaoAuditUpdateObserver) {
		this.systemDaoAuditUpdateObserver = systemDaoAuditUpdateObserver;
	}


	public SystemDaoAuditDeleteObserver<T> getSystemDaoAuditDeleteObserver() {
		return this.systemDaoAuditDeleteObserver;
	}


	public void setSystemDaoAuditDeleteObserver(SystemDaoAuditDeleteObserver<T> systemDaoAuditDeleteObserver) {
		this.systemDaoAuditDeleteObserver = systemDaoAuditDeleteObserver;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
