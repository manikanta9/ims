package com.clifton.system.audit;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


public class SystemAuditEventSearchForm extends BaseEntitySearchForm {

	// custom search filters that can be used instead of setting them on restrictionList
	private String tableName;
	private String childTables; // comma delimited list of child tables: generates SQL UNION
	private Long entityId;
	private Long[] entityIds;
	private Long parentEntityId;
	private Integer systemColumnId;

	private Integer auditTypeId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getChildTables() {
		return this.childTables;
	}


	public void setChildTables(String childTables) {
		this.childTables = childTables;
	}


	public Long getEntityId() {
		return this.entityId;
	}


	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}


	public Long[] getEntityIds() {
		return this.entityIds;
	}


	public void setEntityIds(Long[] entityIds) {
		this.entityIds = entityIds;
	}


	public Long getParentEntityId() {
		return this.parentEntityId;
	}


	public void setParentEntityId(Long parentEntityId) {
		this.parentEntityId = parentEntityId;
	}


	public Integer getSystemColumnId() {
		return this.systemColumnId;
	}


	public void setSystemColumnId(Integer systemColumnId) {
		this.systemColumnId = systemColumnId;
	}


	public Integer getAuditTypeId() {
		return this.auditTypeId;
	}


	public void setAuditTypeId(Integer auditTypeId) {
		this.auditTypeId = auditTypeId;
	}
}
