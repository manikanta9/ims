package com.clifton.system.audit;


import com.clifton.core.beans.BaseSimpleEntity;

import java.util.Date;


/**
 * The <code>SystemAuditEvent</code> class represents a change to a single database field value.
 * It contains both old and new values for the field as well as info about who/when update it.
 *
 * @author vgomelsky
 */
public class SystemAuditEvent extends BaseSimpleEntity<Integer> {

	public static final int AUDIT_INSERT = 1;
	public static final int AUDIT_UPDATE = 2;
	public static final int AUDIT_DELETE = 3;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private Date auditDate;
	private int securityUserId;
	// INSERT/UPDATE/DELETE: see constants for details
	private int auditTypeId;

	// identifies the column and corresponding table that was changed
	private int systemColumnId;
	// identifies the row (Primary Key) that was changed)
	private long mainPKFieldId;
	// optionally identifies parent row (if DTO has parentField specified in DB Migrations). Used for grouping of related audits.
	private Long parentPKFieldId;

	private String oldValue;
	private String newValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getSecurityUserId() {
		return this.securityUserId;
	}


	public void setSecurityUserId(int securityUserId) {
		this.securityUserId = securityUserId;
	}


	public int getSystemColumnId() {
		return this.systemColumnId;
	}


	public void setSystemColumnId(int systemColumnId) {
		this.systemColumnId = systemColumnId;
	}


	public Date getAuditDate() {
		return this.auditDate;
	}


	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}


	public int getAuditTypeId() {
		return this.auditTypeId;
	}


	public void setAuditTypeId(int auditTypeId) {
		this.auditTypeId = auditTypeId;
	}


	public long getMainPKFieldId() {
		return this.mainPKFieldId;
	}


	public void setMainPKFieldId(long mainPKFieldId) {
		this.mainPKFieldId = mainPKFieldId;
	}


	public Long getParentPKFieldId() {
		return this.parentPKFieldId;
	}


	public void setParentPKFieldId(Long parentPKFieldId) {
		this.parentPKFieldId = parentPKFieldId;
	}


	public String getOldValue() {
		return this.oldValue;
	}


	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}


	public String getNewValue() {
		return this.newValue;
	}


	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
}
