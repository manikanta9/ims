package com.clifton.system.audit.auditor.observer;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.system.audit.SystemAuditEvent;
import com.clifton.system.audit.SystemAuditService;
import com.clifton.system.audit.auditor.AuditFieldMetaData;
import com.clifton.system.audit.auditor.SystemAuditDaoUtils;
import com.clifton.system.audit.auditor.SystemAuditMetaDataHandler;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnGroup;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>SystemDaoAuditDeleteObserver</code> class is a DAO observer that should be registered for DAO's
 * that weed to provide field level auditing for DELETE operations.
 *
 * @param <T>
 * @author vgomelsky
 */
@Component
public class SystemDaoAuditDeleteObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	/**
	 * Maximum number of characters in audited field.  If audited field (string) is longer, then its value will be truncated.
	 */
	public static final int MAX_FIELD_SIZE = 4000;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemAuditService systemAuditService;
	private SystemAuditMetaDataHandler systemAuditMetaDataHandler;

	private SystemColumnService systemColumnService;


	public SystemDaoAuditDeleteObserver() {
		super();
		setOrder(-1000); // make sure that audit observer is always the first one to run
	}


	////////////////////////////////////////////////////////////////////////////
	////////               DaoEventObserver Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// don't audit if there was an exception thrown (nothing was saved)
		if (e == null) {
			AssertUtils.assertTrue(DaoEventTypes.DELETE == event, "Unexpected event type. Expected DELETE but was: ", event);

			boolean customData = (bean instanceof SystemColumnValue);
			boolean textAndValueAreSame = customData && ((SystemColumnValue) bean).getValue().equals(((SystemColumnValue) bean).getText());

			Date auditDate = new Date();
			short securityUserId = (Short) ((IdentityObject) getContextHandler().getBean(Context.USER_BEAN_NAME)).getIdentity();
			Map<String, Object> propertyValueMap = BeanUtils.describe(bean);
			Long parentPKFieldId = getParentPKFieldId(propertyValueMap, dao);
			List<AuditFieldMetaData> fieldList = filterAuditFieldList(getSystemAuditMetaDataHandler().getDeleteFieldInfo(dao.getConfiguration().getTableName()));
			for (AuditFieldMetaData field : CollectionUtils.getIterable(fieldList)) {
				Object value = propertyValueMap.get(field.getPropertyName());
				// No need to audit deletes of field values that were null
				// Or boolean values that were false - boolean are only audited on deletes if true
				if (value == null || (value instanceof Boolean && Boolean.FALSE.equals(value)) || (textAndValueAreSame && "text".equals(field.getPropertyName()))) {
					continue;
				}
				if (bean instanceof SystemColumnCustomJsonAware && value instanceof CustomJsonString) {
					generateAuditEventsForCustomJsonValue(field.getColumnId(), null, (SystemColumnCustomJsonAware) bean, null, (CustomJsonString) value, auditDate, securityUserId, parentPKFieldId);
				}
				else {
					SystemAuditEvent auditEvent = new SystemAuditEvent();
					auditEvent.setSystemColumnId((customData ? ((SystemColumnValue) bean).getColumn().getId() : field.getColumnId()));
					auditEvent.setMainPKFieldId((customData ? ((SystemColumnValue) bean).getFkFieldId() : ((Number) bean.getIdentity())).longValue());
					auditEvent.setAuditDate(auditDate);
					auditEvent.setSecurityUserId(securityUserId);
					auditEvent.setAuditTypeId(SystemAuditEvent.AUDIT_DELETE);
					auditEvent.setParentPKFieldId(parentPKFieldId);
					auditEvent.setOldValue(truncateValue(value, field.getMaxFieldSize()));
					auditEvent.setNewValue(null);
					getSystemAuditService().saveSystemAuditEvent(auditEvent);
				}
			}
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// nothing here; all work is done in the end
	}


	/**
	 * If auditing is optionally turned off for specific fields - filters those fields out and returns only fields
	 * that need to be audited
	 */
	protected List<AuditFieldMetaData> filterAuditFieldList(List<AuditFieldMetaData> fieldList) {
		List<String> disabledList = SystemAuditDaoUtils.getAuditingDisabledPropertyList();
		// Nothing disabled or no fields in the list
		if (CollectionUtils.isEmpty(disabledList) || CollectionUtils.isEmpty(fieldList)) {
			return fieldList;
		}

		// Otherwise - filter out the disabled fields
		List<AuditFieldMetaData> list = new ArrayList<>();
		for (AuditFieldMetaData field : fieldList) {
			if (!disabledList.contains(field.getPropertyName())) {
				list.add(field);
			}
		}
		return list;
	}


	/**
	 * Truncates the value to match the maxFieldSize if it exceeds it.
	 */
	protected String truncateValue(Object value, int maxFieldSize) {
		String formattedValue = toFormattedObject(value);
		if (StringUtils.length(formattedValue) > maxFieldSize) {
			formattedValue = formattedValue.substring(0, maxFieldSize - 3) + "...";
		}
		return formattedValue;
	}


	/**
	 * Formats the value based on the data type using default formatter or toString if
	 * data type specific formatting is not available.
	 */
	private String toFormattedObject(Object value) {
		String result = null;
		if (value != null) {
			if (value instanceof BigDecimal) {
				result = CoreMathUtils.formatNumberDecimalMax((BigDecimal) value);
			}
			else if (value instanceof Date) {
				result = DateUtils.fromDateSmart((Date) value);
			}
			else {
				result = value.toString();
			}
		}
		return result;
	}


	/**
	 * Returns the id of the parent field or null if one is not defined.
	 */
	protected Long getParentPKFieldId(Map<String, Object> propertyValueMap, ReadOnlyDAO<T> dao) {
		Long parentPKFieldId = null;
		String parentPKField = dao.getConfiguration().getParentFieldName();
		if (parentPKField != null) {
			Object value = propertyValueMap.get(parentPKField);
			if (value instanceof IdentityObject) {
				value = ((IdentityObject) value).getIdentity();
				if (value instanceof Integer) {
					parentPKFieldId = (long) (Integer) value;
				}
				else if (value instanceof Long) {
					parentPKFieldId = (Long) value;
				}
				else if (value instanceof Short) {
					parentPKFieldId = (long) (Short) value;
				}
				else {
					throw new RuntimeException("Invalid identity data type for parentPKField '" + parentPKField + "'. Identity value is: " + value);
				}
			}
			else {
				throw new RuntimeException("Invalid parentPKField '" + parentPKField + "'. It must be an instance of IdentityObject.");
			}
		}
		return parentPKFieldId;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void generateAuditEventsForCustomJsonValue(int systemColumnId, SystemColumnCustomJsonAware bean, SystemColumnCustomJsonAware originalBean, CustomJsonString value, CustomJsonString originalValue, Date auditDate, int securityUserId, Long parentPKFieldId) {
		SystemColumnGroup systemColumnGroup = getSystemColumnService().getSystemColumnGroupByJsonValueColumn(systemColumnId);
		if (systemColumnGroup != null) {
			List<SystemColumnCustom> columnList = getSystemColumnService().getSystemColumnCustomListForEntity(bean == null ? originalBean : bean);
			List<SystemColumnCustom> originalColumnList = bean == null ? columnList : null;
			Map<String, Object> valueMap = CustomJsonStringUtils.getCustomJsonStringAsMap(value);
			Map<String, Object> originalValueMap = CustomJsonStringUtils.getCustomJsonStringAsMap(originalValue);

			Set<String> checkedKeys = new HashSet<>();
			for (Map.Entry<String, Object> valueEntry : valueMap.entrySet()) {
				checkedKeys.add(valueEntry.getKey());
				Object columnValue = valueEntry.getValue();
				Object originalColumnValue = originalValueMap.get(valueEntry.getKey());

				SystemColumnCustom columnCustom = CollectionUtils.getFirstElement(BeanUtils.filter(columnList, SystemColumnCustom::getName, valueEntry.getKey()));
				if (columnCustom != null) {
					// Might need to make this a "smarter" comparison
					if (CompareUtils.compare(columnValue, originalColumnValue) != 0) {
						AssertUtils.assertNotNull(bean, "Cannot save custom system column " + columnCustom.getName() + " because the bean is null");
						saveSystemColumnJsonAuditEvent(columnCustom, bean, columnValue, originalColumnValue, auditDate, securityUserId, parentPKFieldId);
					}
				}
			}
			// Now check original map for any keys we don't have
			for (Map.Entry<String, Object> originalValueEntry : originalValueMap.entrySet()) {
				if (!checkedKeys.contains(originalValueEntry.getKey())) {
					// If a change is made that affects the columns that are now valid the existing column that is being removed may no longer apply to the bean, so we need the list for the original bean
					if (originalColumnList == null) {
						originalColumnList = getSystemColumnService().getSystemColumnCustomListForEntity(originalBean);
					}
					SystemColumnCustom columnCustom = CollectionUtils.getFirstElement(BeanUtils.filter(originalColumnList, SystemColumnCustom::getName, originalValueEntry.getKey()));
					if (columnCustom != null) {
						AssertUtils.assertNotNull(bean, "Cannot save custom system column " + columnCustom.getName() + " because the bean is null");
						saveSystemColumnJsonAuditEvent(columnCustom, bean, null, originalValueEntry.getValue(), auditDate, securityUserId, parentPKFieldId);
					}
				}
			}
		}
	}


	private void saveSystemColumnJsonAuditEvent(SystemColumnCustom columnCustom, SystemColumnCustomJsonAware bean, Object columnValue, Object originalColumnValue, Date auditDate, int securityUserId, Long parentPKFieldId) {
		SystemAuditEvent auditEvent = new SystemAuditEvent();
		auditEvent.setSystemColumnId(columnCustom.getId());
		auditEvent.setMainPKFieldId(((Number) bean.getIdentity()).longValue());
		auditEvent.setAuditDate(auditDate);
		auditEvent.setSecurityUserId(securityUserId);
		if (originalColumnValue == null) {
			auditEvent.setAuditTypeId(SystemAuditEvent.AUDIT_INSERT);
		}
		else if (columnValue == null) {
			auditEvent.setAuditTypeId(SystemAuditEvent.AUDIT_DELETE);
		}
		else {
			auditEvent.setAuditTypeId(SystemAuditEvent.AUDIT_UPDATE);
		}
		auditEvent.setParentPKFieldId(parentPKFieldId);
		auditEvent.setOldValue(new ObjectToStringConverter().convert(originalColumnValue));
		auditEvent.setNewValue(new ObjectToStringConverter().convert(columnValue));
		getSystemAuditService().saveSystemAuditEvent(auditEvent);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemAuditMetaDataHandler getSystemAuditMetaDataHandler() {
		return this.systemAuditMetaDataHandler;
	}


	public void setSystemAuditMetaDataHandler(SystemAuditMetaDataHandler systemAuditMetaDataHandler) {
		this.systemAuditMetaDataHandler = systemAuditMetaDataHandler;
	}


	public SystemAuditService getSystemAuditService() {
		return this.systemAuditService;
	}


	public void setSystemAuditService(SystemAuditService systemAuditService) {
		this.systemAuditService = systemAuditService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}
}
