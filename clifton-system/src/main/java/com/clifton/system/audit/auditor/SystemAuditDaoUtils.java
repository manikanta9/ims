package com.clifton.system.audit.auditor;


import com.clifton.core.dataaccess.DaoUtils.DaoCallback;
import com.clifton.core.util.CollectionUtils;

import java.util.List;


/**
 * The <code>SystemAuditDaoUtils</code> class contains utility methods for auditing events.
 * <p/>
 * Ability to update a table but turn auditing off for specific fields - Can be used for
 * system updates where we don't want to audit a specific change by the system, only if the user
 * manually updates the value
 * <p/>
 * Example: Investment Manager Account - Use system managed account # - inserts unique value, then immediately
 * updates the account number to something more reasonable using the generated ID of the record.  Don't need to
 * audit the update from the initial UUID value to the real value.
 *
 * @author manderson
 */
public class SystemAuditDaoUtils {

	private static final ThreadLocal<List<String>> auditingDisabledPropertyList = new ThreadLocal<>();


	/**
	 * Returns List of bean property names that auditing has been disabled for
	 * By default not used = returns null
	 */
	public static List<String> getAuditingDisabledPropertyList() {
		return auditingDisabledPropertyList.get();
	}


	public static boolean isAuditingPropertiesDisabled() {
		if (!CollectionUtils.isEmpty(auditingDisabledPropertyList.get())) {
			return true;
		}
		return false;
	}


	public static boolean isAuditingDisabledForProperty(String propertyName) {
		if (isAuditingPropertiesDisabled()) {
			return auditingDisabledPropertyList.get().contains(propertyName);
		}
		return false;
	}


	private static void setAuditingDisabledPropertyList(String[] properties) {
		List<String> list = CollectionUtils.createList(properties);
		auditingDisabledPropertyList.set(list);
	}


	private static void clearAuditingDisabledPropertyList() {
		auditingDisabledPropertyList.remove();
	}


	/**
	 * Executes the specified callback in the mode that allows disabling fields for auditing.
	 * Guarantees to allow dao insert/update/delete before callback execution and clearing property list after.
	 *
	 * @param auditingDisabledProperties - passed as an array for easier use
	 * @param callback                   - action to execute
	 */
	public static <T> T executeWithAuditingDisabled(String[] auditingDisabledProperties, DaoCallback<T> callback) {
		try {
			setAuditingDisabledPropertyList(auditingDisabledProperties);
			return callback.execute();
		}
		finally {
			clearAuditingDisabledPropertyList();
		}
	}
}
