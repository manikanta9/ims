package com.clifton.system.audit;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>SystemAuditType</code> class defines the type of audit that should be enabled for a database entity.
 * <p>
 * For example: AUDIT DELETE ONLY, AUDIT INSERT AND UPDATE, AUDIT UPDATE (Except From Null) AND DELETE, AUDIT UPDATE AND DELETE,
 * AUDIT UPDATE ONLY, AUDIT UPDATE ONLY (Except From Null), NO AUDIT
 *
 * @author vgomelsky
 */
@CacheByName
public class SystemAuditType extends NamedEntity<Short> {

	private int maxFieldSize;

	private boolean auditInsert;
	private boolean auditUpdate;
	private boolean auditDelete;

	/**
	 * Option used to skip auditing updates to a column if set from previously NULL value
	 * This is often used for columns that aren't set on the insert of the bean, but when set
	 * it shouldn't change, so we do not need to audit the initial setting of the value only when changed
	 * from a previously populated value.
	 */
	private AuditUpdateTypes auditUpdateType;
	private boolean captureFullStateOnInsert;
	private boolean captureFullStateOnDelete;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getMaxFieldSize() {
		return this.maxFieldSize;
	}


	public void setMaxFieldSize(int maxFieldSize) {
		this.maxFieldSize = maxFieldSize;
	}


	public boolean isAuditInsert() {
		return this.auditInsert;
	}


	public void setAuditInsert(boolean auditInsert) {
		this.auditInsert = auditInsert;
	}


	public boolean isAuditUpdate() {
		return this.auditUpdate;
	}


	public void setAuditUpdate(boolean auditUpdate) {
		this.auditUpdate = auditUpdate;
	}


	public boolean isAuditDelete() {
		return this.auditDelete;
	}


	public void setAuditDelete(boolean auditDelete) {
		this.auditDelete = auditDelete;
	}


	public boolean isCaptureFullStateOnInsert() {
		return this.captureFullStateOnInsert;
	}


	public void setCaptureFullStateOnInsert(boolean captureFullStateOnInsert) {
		this.captureFullStateOnInsert = captureFullStateOnInsert;
	}


	public boolean isCaptureFullStateOnDelete() {
		return this.captureFullStateOnDelete;
	}


	public void setCaptureFullStateOnDelete(boolean captureFullStateOnDelete) {
		this.captureFullStateOnDelete = captureFullStateOnDelete;
	}


	public AuditUpdateTypes getAuditUpdateType() {
		return this.auditUpdateType;
	}


	public void setAuditUpdateType(AuditUpdateTypes auditUpdateType) {
		this.auditUpdateType = auditUpdateType;
	}
}
