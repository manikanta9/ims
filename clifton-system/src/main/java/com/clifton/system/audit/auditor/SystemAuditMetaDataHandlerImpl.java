package com.clifton.system.audit.auditor;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.audit.SystemAuditType;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemAuditMetaDataHandlerImpl</code> class caches {@link AuditFieldMetaData} info for audited columns
 * and clears it on column/table changes.
 *
 * @author vgomelsky
 */
@Component
public class SystemAuditMetaDataHandlerImpl<T extends IdentityObject> extends BaseDaoEventObserver<T> implements SystemAuditMetaDataHandler {

	public static final String CACHE_NAME_INSERT = SystemAuditMetaDataHandlerImpl.class.getName() + "_INSERT";
	public static final String CACHE_NAME_UPDATE = SystemAuditMetaDataHandlerImpl.class.getName() + "_UPDATE";
	public static final String CACHE_NAME_DELETE = SystemAuditMetaDataHandlerImpl.class.getName() + "_DELETE";

	private CacheHandler<String, List<AuditFieldMetaData>> cacheHandler;

	private DaoLocator daoLocator;
	private SystemSchemaService systemSchemaService;
	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AuditFieldMetaData> getDeleteFieldInfo(String tableName) {
		List<AuditFieldMetaData> result = getCacheHandler().get(CACHE_NAME_DELETE, tableName);
		if (result == null) {
			populateMetaData(tableName);
			result = getCacheHandler().get(CACHE_NAME_DELETE, tableName);
			AssertUtils.assertNotNull(result, "Cannot find delete cache for table %s", tableName);
		}
		return result;
	}


	@Override
	public List<AuditFieldMetaData> getInsertFieldInfo(String tableName) {
		List<AuditFieldMetaData> result = getCacheHandler().get(CACHE_NAME_INSERT, tableName);
		if (result == null) {
			populateMetaData(tableName);
			result = getCacheHandler().get(CACHE_NAME_INSERT, tableName);
			AssertUtils.assertNotNull(result, "Cannot find insert cache for table %s", tableName);
		}
		return result;
	}


	@Override
	public List<AuditFieldMetaData> getUpdateFieldInfo(String tableName) {
		List<AuditFieldMetaData> result = getCacheHandler().get(CACHE_NAME_UPDATE, tableName);
		if (result == null) {
			populateMetaData(tableName);
			result = getCacheHandler().get(CACHE_NAME_UPDATE, tableName);
			AssertUtils.assertNotNull(result, "Cannot find update cache for table %s", tableName);
		}
		return result;
	}


	@SuppressWarnings("unchecked")
	private void populateMetaData(String tableName) {
		List<AuditFieldMetaData> insertList = new ArrayList<>();
		List<AuditFieldMetaData> updateList = new ArrayList<>();
		List<AuditFieldMetaData> deleteList = new ArrayList<>();

		SystemTable table = getSystemSchemaService().getSystemTableByName(tableName);
		DAOConfiguration<T> daoConfiguration = (DAOConfiguration<T>) getDaoLocator().locate(tableName).getConfiguration();
		SystemAuditType tableAuditType = table.getAuditType();
		boolean captureFullStateOnInsert = tableAuditType != null && tableAuditType.isCaptureFullStateOnInsert();
		boolean captureFullStateOnDelete = tableAuditType != null && tableAuditType.isCaptureFullStateOnDelete();

		List<SystemColumnStandard> systemColumnList = getSystemColumnService().getSystemColumnStandardListByTable(table.getId());
		for (SystemColumnStandard column : systemColumnList) {
			SystemAuditType auditType = column.getAuditType();
			boolean columnDef = true;
			if (auditType == null) {
				auditType = tableAuditType;
				columnDef = false;
			}
			if (auditType != null && !BeanUtils.isSystemManagedField(column.getBeanPropertyName()) && (auditType.isAuditInsert() || auditType.isAuditUpdate() || auditType.isAuditDelete())) {
				// make sure that auditing is enabled and that it's not a system managed field
				AuditFieldMetaData field = new AuditFieldMetaData();
				field.setColumnId(column.getId());
				field.setMaxFieldSize(auditType.getMaxFieldSize());
				field.setPropertyName(column.getBeanPropertyName());
				if (StringUtils.isEqual(SystemDataType.DECIMAL, column.getDataType().getName())) {
					field.setDecimalPrecision(daoConfiguration.getColumnByName(column.getName()).getDataType().getPrecision());
				}
				boolean idField = "id".equals(field.getPropertyName());
				if (auditType.isAuditInsert() && (columnDef || captureFullStateOnInsert || idField)) {
					insertList.add(field);
				}
				if (auditType.isAuditUpdate()) {
					field.setAuditUpdateType(auditType.getAuditUpdateType());
					updateList.add(field);
				}
				if (auditType.isAuditDelete() && (columnDef || captureFullStateOnDelete || idField)) {
					deleteList.add(field);
				}
			}
		}
		// what if empty?
		getCacheHandler().put(CACHE_NAME_INSERT, tableName, insertList);
		getCacheHandler().put(CACHE_NAME_UPDATE, tableName, updateList);
		getCacheHandler().put(CACHE_NAME_DELETE, tableName, deleteList);
		LogUtils.info(getClass(), "Cached audit meta data for table: " + tableName);
	}

	////////////////////////////////////////////////////////////////////////////
	///////                     CACHE CLEARING                        //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			// clear everything for now: doesn't happen often
			getCacheHandler().clear(CACHE_NAME_INSERT);
			getCacheHandler().clear(CACHE_NAME_UPDATE);
			getCacheHandler().clear(CACHE_NAME_DELETE);
			LogUtils.info(getClass(), "Cleared audit field cache meta data.");
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// do nothing
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public CacheHandler<String, List<AuditFieldMetaData>> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, List<AuditFieldMetaData>> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
