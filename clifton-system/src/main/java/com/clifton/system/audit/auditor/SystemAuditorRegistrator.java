package com.clifton.system.audit.auditor;


import com.clifton.system.audit.SystemAuditType;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemAuditorRegistrator</code> interface defines methods that help register/unregister auditors
 * as observers to corresponding DAO's based on schema meta data.
 *
 * @author vgomelsky
 */
public interface SystemAuditorRegistrator {

	/**
	 * Registers auditors enabled by auditType for the specified table's DAO.
	 *
	 * @param tableName
	 * @param auditType
	 * @param unregisterIfFalse specifies whether the method should also unregister auditors when corresponding flag is false
	 */
	public void registerAuditors(String tableName, SystemAuditType auditType, boolean unregisterIfFalse);


	/**
	 * Returns a populated {@link SystemAuditType} object that represents merged audit properties of the specified
	 * table (also looks at table's columns).
	 *
	 * @param table
	 */
	public SystemAuditType getSystemAuditTypeMerged(SystemTable table);
}
