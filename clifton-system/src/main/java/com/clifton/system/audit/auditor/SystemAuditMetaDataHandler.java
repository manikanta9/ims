package com.clifton.system.audit.auditor;


import java.util.List;


/**
 * The <code>SystemAuditMetaDataHandler</code> interface defines methods for getting field-level audit meta data.
 *
 * @author vgomelsky
 */
public interface SystemAuditMetaDataHandler {

	/**
	 * Returns a List of {@link AuditFieldMetaData} objects that represent each field
	 * that should have INSERT audited for the specified table.
	 */
	public List<AuditFieldMetaData> getInsertFieldInfo(String tableName);


	/**
	 * Returns a List of {@link AuditFieldMetaData} objects that represent each field
	 * that should have UPDATE audited for the specified table.
	 */
	public List<AuditFieldMetaData> getUpdateFieldInfo(String tableName);


	/**
	 * Returns a List of {@link AuditFieldMetaData} objects that represent each field
	 * that should have DELETE audited for the specified table.
	 */
	public List<AuditFieldMetaData> getDeleteFieldInfo(String tableName);
}
