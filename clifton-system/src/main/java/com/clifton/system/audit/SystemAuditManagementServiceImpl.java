package com.clifton.system.audit;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.audit.auditor.SystemAuditorRegistrator;
import com.clifton.system.audit.auditor.observer.SystemDaoAuditDeleteObserver;
import com.clifton.system.audit.auditor.observer.SystemDaoAuditInsertObserver;
import com.clifton.system.audit.auditor.observer.SystemDaoAuditUpdateObserver;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.search.SystemTableSearchForm;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class SystemAuditManagementServiceImpl<T extends IdentityObject> implements SystemAuditManagementService {

	private DaoLocator daoLocator;

	private SystemAuditorRegistrator systemAuditorRegistrator;

	private SystemDaoAuditInsertObserver<T> systemDaoAuditInsertObserver;
	private SystemDaoAuditUpdateObserver<T> systemDaoAuditUpdateObserver;
	private SystemDaoAuditDeleteObserver<T> systemDaoAuditDeleteObserver;

	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public List<SystemAuditTableConfig> getSystemAuditTableConfigList(boolean invalidOnly) {
		SystemTableSearchForm searchForm = new SystemTableSearchForm();
		searchForm.setDefaultDataSource(true);

		List<SystemAuditTableConfig> auditTableConfigList = new ArrayList<>();
		List<SystemTable> tableList = getSystemSchemaService().getSystemTableList(searchForm);
		for (SystemTable systemTable : tableList) {
			SystemAuditTableConfig tableConfig = SystemAuditTableConfig.forTableAndAuditType(systemTable, getSystemAuditorRegistrator().getSystemAuditTypeMerged(systemTable));
			ObserverableDAO<T> dao = (ObserverableDAO<T>) getDaoLocator().locate(systemTable.getName());
			tableConfig.setInsertObserverRegistered(dao.isInsertObserverRegisteredForObserver(getSystemDaoAuditInsertObserver()));
			tableConfig.setUpdateObserverRegistered(dao.isUpdateObserverRegisteredForObserver(getSystemDaoAuditUpdateObserver()));
			tableConfig.setDeleteObserverRegistered(dao.isDeleteObserverRegisteredForObserver(getSystemDaoAuditDeleteObserver()));
			auditTableConfigList.add(tableConfig);
		}

		if (invalidOnly) {
			return BeanUtils.filter(auditTableConfigList, SystemAuditTableConfig::isInvalid);
		}
		return auditTableConfigList;
	}


	@Override
	public void registerSystemAuditors() {
		SystemTableSearchForm searchForm = new SystemTableSearchForm();
		searchForm.setDefaultDataSource(true);

		List<SystemTable> tableList = getSystemSchemaService().getSystemTableList(searchForm);
		for (SystemTable systemTable : tableList) {
			getSystemAuditorRegistrator().registerAuditors(systemTable.getName(), getSystemAuditorRegistrator().getSystemAuditTypeMerged(systemTable), true);
		}
	}


	@Override
	public void registerSystemAuditorsForTableName(String tableName) {
		SystemTable systemTable = getSystemSchemaService().getSystemTableByName(tableName);
		ValidationUtils.assertNotNull(systemTable, "Cannot find system table with name " + tableName + " for default data source.");
		getSystemAuditorRegistrator().registerAuditors(tableName, getSystemAuditorRegistrator().getSystemAuditTypeMerged(systemTable), true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemAuditorRegistrator getSystemAuditorRegistrator() {
		return this.systemAuditorRegistrator;
	}


	public void setSystemAuditorRegistrator(SystemAuditorRegistrator systemAuditorRegistrator) {
		this.systemAuditorRegistrator = systemAuditorRegistrator;
	}


	public SystemDaoAuditInsertObserver<T> getSystemDaoAuditInsertObserver() {
		return this.systemDaoAuditInsertObserver;
	}


	public void setSystemDaoAuditInsertObserver(SystemDaoAuditInsertObserver<T> systemDaoAuditInsertObserver) {
		this.systemDaoAuditInsertObserver = systemDaoAuditInsertObserver;
	}


	public SystemDaoAuditUpdateObserver<T> getSystemDaoAuditUpdateObserver() {
		return this.systemDaoAuditUpdateObserver;
	}


	public void setSystemDaoAuditUpdateObserver(SystemDaoAuditUpdateObserver<T> systemDaoAuditUpdateObserver) {
		this.systemDaoAuditUpdateObserver = systemDaoAuditUpdateObserver;
	}


	public SystemDaoAuditDeleteObserver<T> getSystemDaoAuditDeleteObserver() {
		return this.systemDaoAuditDeleteObserver;
	}


	public void setSystemDaoAuditDeleteObserver(SystemDaoAuditDeleteObserver<T> systemDaoAuditDeleteObserver) {
		this.systemDaoAuditDeleteObserver = systemDaoAuditDeleteObserver;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
