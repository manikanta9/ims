package com.clifton.system.audit;


import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.restrictions.ArrayRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.DateRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.ForeignKeyRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.IntegerRangeRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.IntegerRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.LongRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.ShortRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.StringRestrictionDefinition;
import com.clifton.core.dataaccess.search.sql.SqlSearchFormConfigurer;
import com.clifton.core.dataaccess.search.sql.SqlUnionSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>SystemAuditServiceImpl</code> class provides default implementation of {@linkplain SystemAuditService}.
 *
 * @author vgomelsky
 */
@Service
public class SystemAuditServiceImpl implements SystemAuditService {

	private UpdatableDAO<SystemAuditEvent> systemAuditEventDAO;
	private UpdatableDAO<SystemAuditType> systemAuditTypeDAO;

	private DataTableRetrievalHandler dataTableRetrievalHandler;

	private DaoNamedEntityCache<SystemAuditType> systemAuditTypeCache;

	////////////////////////////////////////////////////////////////////////////
	////////          SystemAuditType Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemAuditType getSystemAuditType(short id) {
		return getSystemAuditTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public SystemAuditType getSystemAuditTypeByName(String name) {
		return getSystemAuditTypeCache().getBeanForKeyValueStrict(getSystemAuditTypeDAO(), name);
	}


	@Override
	public List<SystemAuditType> getSystemAuditTypeList() {
		return getSystemAuditTypeDAO().findAll();
	}


	@Override
	public SystemAuditType saveSystemAuditType(SystemAuditType bean) {
		return getSystemAuditTypeDAO().save(bean);
	}


	@Override
	public void deleteSystemAuditType(short id) {
		getSystemAuditTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////          SystemAuditEvent Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemAuditEvent saveSystemAuditEvent(SystemAuditEvent bean) {
		return getSystemAuditEventDAO().save(bean);
	}


	@Override
	public DataTable getSystemAuditEventList(SystemAuditEventSearchForm searchForm) {
		searchForm.addSearchRestriction("tableName", ComparisonConditions.EQUALS, searchForm.getTableName());
		searchForm.addSearchRestriction("childTables", ComparisonConditions.EQUALS, searchForm.getChildTables());
		searchForm.addSearchRestriction("auditTypeId", ComparisonConditions.EQUALS, searchForm.getAuditTypeId());
		searchForm.addSearchRestriction("entityId", ComparisonConditions.EQUALS, searchForm.getEntityId());
		searchForm.addSearchRestriction("entityIds", ComparisonConditions.IN, searchForm.getEntityIds());
		searchForm.addSearchRestriction("parentEntityId", ComparisonConditions.EQUALS, searchForm.getParentEntityId());
		searchForm.addSearchRestriction("systemColumnId", ComparisonConditions.EQUALS, searchForm.getSystemColumnId());

		SqlSearchFormConfigurer configurer = getSystemAuditEventSearchConfigurer(searchForm);
		SearchConfigurer<SqlSelectCommand> actualConfigurer = configurer;

		if (searchForm.containsSearchRestriction("childTables")) {
			// need to UNION ALL: actual entities + child entities
			configurer.addSearchFieldDefinition(new StringRestrictionDefinition("tableName", "t.TableName"));
			configurer.addSearchFieldDefinition((searchForm.getAuditTypeId() != null) ? new IntegerRestrictionDefinition("auditTypeId", "e.AuditTypeID") : new ArrayRestrictionDefinition("auditTypeId", "e.AuditTypeID"));
			configurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<Long>(new LongRestrictionDefinition("entityId", "e.MainPKFieldID")));
			configurer.addSearchFieldDefinition(new ArrayRestrictionDefinition("entityIds", "e.MainPKFieldID"));

			String[] childTables = searchForm.removeSearchRestriction("childTables").getValue().toString().split(",");
			SqlSearchFormConfigurer[] unionConfigurers = new SqlSearchFormConfigurer[1 + childTables.length];
			unionConfigurers[0] = configurer;

			for (int i = 0; i < childTables.length; i++) {
				// clone the search form and replace tableName restriction with child table value
				BaseEntitySearchForm newSearchForm = searchForm.newInstance();
				newSearchForm.removeSearchRestriction("tableName");
				newSearchForm.addSearchRestriction(new SearchRestriction("tableName", ComparisonConditions.EQUALS, childTables[i]));

				SqlSearchFormConfigurer parentConfigurer = getSystemAuditEventSearchConfigurer(newSearchForm);
				parentConfigurer.addSearchFieldDefinition(new StringRestrictionDefinition("tableName", "t.TableName"));
				parentConfigurer.addSearchFieldDefinition((searchForm.getAuditTypeId() != null) ? new IntegerRestrictionDefinition("auditTypeId", "e.AuditTypeID") : new ArrayRestrictionDefinition("auditTypeId", "e.AuditTypeID"));
				parentConfigurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<Long>(new LongRestrictionDefinition("entityId", "e.ParentPKFieldID")));
				parentConfigurer.addSearchFieldDefinition(new ArrayRestrictionDefinition("entityIds", "e.MainPKFieldID"));
				unionConfigurers[i + 1] = parentConfigurer;
			}

			//TODO - Allow for search form to define a custom function for additional unions as needed?

			actualConfigurer = new SqlUnionSearchFormConfigurer(unionConfigurers, true);
		}
		else {
			// single select
			configurer.addSearchFieldDefinition(new StringRestrictionDefinition("tableName", "t.TableName"));
			configurer.addSearchFieldDefinition(((searchForm.getAuditTypeId() != null) ? new IntegerRestrictionDefinition("auditTypeId", "e.AuditTypeID") : new ArrayRestrictionDefinition("auditTypeId", "e.AuditTypeID")));
			configurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<Long>(new LongRestrictionDefinition("entityId", "e.MainPKFieldID")));
			configurer.addSearchFieldDefinition(new ArrayRestrictionDefinition("entityIds", "e.MainPKFieldID"));
			configurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<Integer>(new IntegerRestrictionDefinition("parentEntityId", "e.ParentPKFieldID")));
			configurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<Integer>(new IntegerRestrictionDefinition("systemColumnId", "e.SystemColumnID")));
		}

		return getDataTableRetrievalHandler().findDataTable(actualConfigurer);
	}


	private SqlSearchFormConfigurer getSystemAuditEventSearchConfigurer(BaseEntitySearchForm searchForm) {
		SqlSearchFormConfigurer configurer = new SqlSearchFormConfigurer(searchForm);
		// NOTE: do not change the order or names of these columns: multiple places in UI and code rely on it
		configurer.setSelectClause("e.SystemColumnID \"ColumnID\", " + //
				"e.MainPKFieldID \"MainPKFieldID\", " + //
				"e.ParentPKFieldID \"ParentPKFieldID\", " + //
				"e.AuditDate \"Date\", " + //
				"u.SecurityUserName \"User\", " + //
				"CASE e.AuditTypeID WHEN 1 THEN 'insert' WHEN 2 THEN 'update' WHEN 3 THEN 'delete' ELSE 'UNKNOWN' END \"Action\", " + //
				"t.TableLabel \"Table\", " + //
				"c.ColumnLabel \"Field\", " + //
				"COALESCE(l.SystemLabel, 'ID = ' + CAST(e.MainPKFieldID AS NVARCHAR)) \"Label\", " + //
				"e.OldValue \"Old Value\", " + //
				"e.NewValue \"New Value\"");
		configurer.setFromClause("SystemAuditEvent e " + //
				"INNER JOIN SystemColumn c ON e.SystemColumnID = c.SystemColumnID " + //
				"INNER JOIN SystemTable t ON c.SystemTableID = t.SystemTableID " + //
				"INNER JOIN SecurityUser u ON e.SecurityUserID = u.SecurityUserID " + //
				"LEFT JOIN SystemLabel l ON l.SystemTableID = c.SystemTableID AND l.PKFieldID = e.MainPKFieldID");
		configurer.setOrderByClause("4 DESC, 8");

		configurer.addSearchFieldDefinition(new DateRestrictionDefinition("auditDate", "e.AuditDate"));
		configurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<Integer>(new IntegerRestrictionDefinition("securityUserId", "e.SecurityUserID", "u.SecurityUserName")));
		configurer.addSearchFieldDefinition(new IntegerRangeRestrictionDefinition("auditTypeId", "e.AuditTypeID", "6", new int[]{1, 2, 3}));
		configurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<Short>(new ShortRestrictionDefinition("systemTableId", "c.SystemTableID", "t.TableLabel")));
		configurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<Integer>(new IntegerRestrictionDefinition("systemColumnId", "e.SystemColumnID", "c.ColumnLabel")));
		configurer.addSearchFieldDefinition(new StringRestrictionDefinition("columnName", "c.ColumnLabel"));
		configurer.addSearchFieldDefinition(new StringRestrictionDefinition("label", "l.SystemLabel"));
		return configurer;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public UpdatableDAO<SystemAuditType> getSystemAuditTypeDAO() {
		return this.systemAuditTypeDAO;
	}


	public void setSystemAuditTypeDAO(UpdatableDAO<SystemAuditType> systemAuditTypeDAO) {
		this.systemAuditTypeDAO = systemAuditTypeDAO;
	}


	public UpdatableDAO<SystemAuditEvent> getSystemAuditEventDAO() {
		return this.systemAuditEventDAO;
	}


	public void setSystemAuditEventDAO(UpdatableDAO<SystemAuditEvent> systemAuditEventDAO) {
		this.systemAuditEventDAO = systemAuditEventDAO;
	}


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}


	public DaoNamedEntityCache<SystemAuditType> getSystemAuditTypeCache() {
		return this.systemAuditTypeCache;
	}


	public void setSystemAuditTypeCache(DaoNamedEntityCache<SystemAuditType> systemAuditTypeCache) {
		this.systemAuditTypeCache = systemAuditTypeCache;
	}
}
