package com.clifton.system.audit.auditor;


import com.clifton.system.audit.AuditUpdateTypes;

import java.io.Serializable;


/**
 * The AuditFieldMetaData class represents the data that describes a single column/property that needs to be
 * audited along with the details about how to audit. This data is usually cached for improved performance.
 *
 * @author vgomelsky
 */
public class AuditFieldMetaData implements Serializable {

	//Defines the maximum characters to be stored in the nvarchar(max) field
	private int maxFieldSize;

	private int columnId;
	private String propertyName;

	// Used to validate changes to decimal values only up to the specified precision of the field
	private Integer decimalPrecision;

	/**
	 * Option used to skip auditing updates to a column if set from previously NULL/Zero value
	 * This is often used for columns that aren't set on the insert of the bean, but when set
	 * it shouldn't change, so we do not need to audit the initial setting of the value only when changed
	 * from a previously populated value.
	 */

	private AuditUpdateTypes auditUpdateType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getMaxFieldSize() {
		return this.maxFieldSize;
	}


	public void setMaxFieldSize(int maxFieldSize) {
		this.maxFieldSize = maxFieldSize;
	}


	public int getColumnId() {
		return this.columnId;
	}


	public void setColumnId(int columnId) {
		this.columnId = columnId;
	}


	public String getPropertyName() {
		return this.propertyName;
	}


	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}


	public Integer getDecimalPrecision() {
		return this.decimalPrecision;
	}


	public void setDecimalPrecision(Integer decimalPrecision) {
		this.decimalPrecision = decimalPrecision;
	}


	public AuditUpdateTypes getAuditUpdateType() {
		return this.auditUpdateType;
	}


	public void setAuditUpdateType(AuditUpdateTypes auditUpdateType) {
		this.auditUpdateType = auditUpdateType;
	}
}
