package com.clifton.system.audit;


import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;


/**
 * @author AbhinayaM
 * The <code>DoNotAuditUpdate</code> defines enum  used to skip auditing updates to a column if set from previously NULL/ZERO  value
 * This is often used for columns that aren't set on the insert of the bean, but when set
 * it shouldn't change, so we do not need to audit the initial setting of the value only when changed
 * from a previously populated value.
 */
public enum AuditUpdateTypes {

	DO_NOT_AUDIT_UPDATE_FROM_NULL(true, false),
	DO_NOT_AUDIT_UPDATE_FROM_ZERO(false, true);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private boolean skipNullValue;

	private boolean skipZero;


	AuditUpdateTypes(boolean skipNullValue, boolean skipZero) {
		this.skipNullValue = skipNullValue;
		this.skipZero = skipZero;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSkipAudit(Object originalValue) {
		if (originalValue == null && isSkipNullValue()) {
			return true;
		}
		if (isSkipZero() && originalValue instanceof Number && MathUtils.isEqual(0, (Number) originalValue)) {
			return true;
		}
		if (isSkipZero() && originalValue instanceof String && StringUtils.isEqual("0", (String) originalValue)) {
			return true;
		}

		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSkipNullValue() {
		return this.skipNullValue;
	}


	public boolean isSkipZero() {
		return this.skipZero;
	}
}
