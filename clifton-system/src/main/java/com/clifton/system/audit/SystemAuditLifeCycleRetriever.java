package com.clifton.system.audit;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.UpdatableEntity;
import com.clifton.core.beans.UpdatableOnlyEntity;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.lifecycle.SystemLifeCycleEvent;
import com.clifton.system.lifecycle.SystemLifeCycleEventContext;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleEventRetriever;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.column.SystemColumnService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Component
public class SystemAuditLifeCycleRetriever implements SystemLifeCycleEventRetriever {

	private SecurityUserService securityUserService;

	private SystemAuditService systemAuditService;
	private SystemColumnService systemColumnService;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity != null;
	}


	@Override
	public List<SystemLifeCycleEvent> getSystemLifeCycleEventListForEntity(SystemLifeCycleEventContext lifeCycleEventContext) {
		List<SystemLifeCycleEvent> lifeCycleEventList = new ArrayList<>();

		// Add the original insert and last updated
		appendCreateAndLastUpdatedEvents(lifeCycleEventContext, lifeCycleEventList);

		SystemAuditEventSearchForm searchForm = new SystemAuditEventSearchForm();
		searchForm.setTableName(lifeCycleEventContext.getCurrentProcessingEntityTableName());
		searchForm.setEntityId(BeanUtils.getIdentityAsLong(lifeCycleEventContext.getCurrentProcessingEntity()));
		DataTable auditHistory = getSystemAuditService().getSystemAuditEventList(searchForm);
		addAuditEvents(lifeCycleEventList, lifeCycleEventContext, auditHistory);

		return lifeCycleEventList;
	}


	protected void addAuditEvents(List<SystemLifeCycleEvent> lifeCycleEventList, SystemLifeCycleEventContext lifeCycleEventContext, DataTable auditHistory) {
		if (auditHistory != null && auditHistory.getTotalRowCount() > 0) {
			for (DataRow dataRow : auditHistory.getRowList()) {
				lifeCycleEventList.add(convertToSystemLifeCycleEvent(dataRow, lifeCycleEventContext));
			}
		}
	}


	private void appendCreateAndLastUpdatedEvents(SystemLifeCycleEventContext lifeCycleEventContext, List<SystemLifeCycleEvent> lifeCycleEventList) {
		IdentityObject entity = lifeCycleEventContext.getCurrentProcessingEntity();
		if (entity instanceof UpdatableEntity) {
			UpdatableEntity updatableEntity = (UpdatableEntity) entity;
			SystemLifeCycleEvent createEvent = newCreateOrUpdateAction(entity, updatableEntity.getCreateDate(), updatableEntity.getCreateUserId(), "Insert", lifeCycleEventContext);
			lifeCycleEventList.add(createEvent);

			if (!DateUtils.isEqual(updatableEntity.getCreateDate(), updatableEntity.getUpdateDate())) {
				SystemLifeCycleEvent lastUpdateEvent = newCreateOrUpdateAction(entity, updatableEntity.getUpdateDate(), updatableEntity.getUpdateUserId(), "Last Update", lifeCycleEventContext);
				lifeCycleEventList.add(lastUpdateEvent);
			}
		}
		else if (entity instanceof UpdatableOnlyEntity) {
			UpdatableOnlyEntity updatableOnlyEntity = (UpdatableOnlyEntity) entity;
			SystemLifeCycleEvent lastUpdateEvent = newCreateOrUpdateAction(entity, updatableOnlyEntity.getUpdateDate(), updatableOnlyEntity.getUpdateUserId(), "Last Update", lifeCycleEventContext);
			lifeCycleEventList.add(lastUpdateEvent);
		}
	}


	private SystemLifeCycleEvent newCreateOrUpdateAction(IdentityObject entity, Date eventDate, short userId, String actionName, SystemLifeCycleEventContext lifeCycleEventContext) {
		SystemLifeCycleEvent event = new SystemLifeCycleEvent();
		event.setLinkedSystemTable(lifeCycleEventContext.getSystemTableByName(getSystemSchemaService(), lifeCycleEventContext.getCurrentProcessingEntityTableName()));
		event.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(entity));
		event.setSourceSystemTable(event.getLinkedSystemTable());
		event.setSourceFkFieldId(event.getLinkedFkFieldId());
		event.setEventDate(eventDate);
		event.setEventUser(lifeCycleEventContext.getSecurityUserById(getSecurityUserService(), userId));
		event.setEventSource("Entity " + actionName);
		event.setEventAction(actionName);
		event.setEventDetails(BeanUtils.getLabelAdvanced(entity, false));
		return event;
	}


	private SystemLifeCycleEvent convertToSystemLifeCycleEvent(DataRow auditRow, SystemLifeCycleEventContext lifeCycleEventContext) {
		SystemLifeCycleEvent event = new SystemLifeCycleEvent();
		event.setLinkedSystemTable(getSystemColumnService().getSystemColumn((Integer) auditRow.getValue("ColumnID")).getTable());
		event.setLinkedFkFieldId(MathUtils.getNumberAsLong((Number) auditRow.getValue("MainPKFieldID")));
		// Data table doesn't have the Audit ID?
		event.setSourceSystemTable(lifeCycleEventContext.getSystemTableByName(getSystemSchemaService(), "SystemAuditEvent"));

		event.setEventDate((Date) auditRow.getValue("Date"));
		event.setEventSource("Audit Trail");
		event.setEventAction(getActionName((String) auditRow.getValue("Action")));
		String fieldName = (String) auditRow.getValue("Field");
		String oldValue = (String) auditRow.getValue("Old Value");
		String newValue = (String) auditRow.getValue("New Value");
		if (StringUtils.isEmpty(oldValue)) {
			if ("Insert".equals(event.getEventAction())) {
				if ("DOUBLE CLICK FOR MORE INFO".equals(newValue)) {
					event.setEventDetails("inserted [" + auditRow.getValue("Label") + "]");
				}
				else {
					event.setEventDetails("[" + fieldName + "] inserted value [" + newValue + "]");
				}
			}
			else {
				event.setEventDetails("[" + fieldName + "] updated value to [" + newValue + "]");
			}
		}
		else if (StringUtils.isEmpty(newValue)) {
			event.setEventDetails("[" + fieldName + "] deleted value [" + oldValue + "]");
		}
		else {
			event.setEventDetails("[" + fieldName + "] updated from [" + oldValue + "] to [" + newValue + "]");
		}
		event.setEventUser(lifeCycleEventContext.getSecurityUserByName(getSecurityUserService(), (String) auditRow.getValue("User")));
		return event;
	}


	protected String getActionName(String auditAction) {
		return StringUtils.capitalize(auditAction);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemAuditService getSystemAuditService() {
		return this.systemAuditService;
	}


	public void setSystemAuditService(SystemAuditService systemAuditService) {
		this.systemAuditService = systemAuditService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
