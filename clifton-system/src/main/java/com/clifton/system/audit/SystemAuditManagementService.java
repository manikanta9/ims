package com.clifton.system.audit;

import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>SystemAuditManagementService</code> is used to both validate that the auditing types on tables matches the dao auditors registered as well management of re-registering audit observers across all or for a specific table.
 *
 * @author manderson
 */
public interface SystemAuditManagementService {

	/**
	 * Returns the list of tables and their auditing types and if observers are properly registered.  Invalid only means there is a mismatch
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public List<SystemAuditTableConfig> getSystemAuditTableConfigList(boolean invalidOnly);


	/**
	 * Re-register auditors across all tables
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	@RequestMapping("systemAuditorsRegister")
	public void registerSystemAuditors();


	/**
	 * Re-register auditors for a single table
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	@RequestMapping("systemAuditorsForTableNameRegister")
	public void registerSystemAuditorsForTableName(String tableName);
}
