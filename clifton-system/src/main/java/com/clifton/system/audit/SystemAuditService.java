package com.clifton.system.audit;


import com.clifton.core.dataaccess.datatable.DataTable;

import java.util.List;


/**
 * The <code>SystemAuditService</code> interface defines methods for managing field level auditing.
 *
 * @author vgomelsky
 */
public interface SystemAuditService {

	////////////////////////////////////////////////////////////////////////////
	////////          SystemAuditType Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemAuditType getSystemAuditType(short id);


	public SystemAuditType getSystemAuditTypeByName(String name);


	public List<SystemAuditType> getSystemAuditTypeList();


	public SystemAuditType saveSystemAuditType(SystemAuditType bean);


	public void deleteSystemAuditType(short id);


	////////////////////////////////////////////////////////////////////////////
	////////          SystemAuditEvent Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	public DataTable getSystemAuditEventList(SystemAuditEventSearchForm searchForm);


	public SystemAuditEvent saveSystemAuditEvent(SystemAuditEvent bean);
}
