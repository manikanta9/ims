package com.clifton.system.audit.auditor.observer;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.audit.SystemAuditEvent;
import com.clifton.system.audit.auditor.AuditFieldMetaData;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemDaoAuditInsertObserver</code> class is a DAO observer that should be registered for DAO's
 * that weed to provide field level auditing for INSERT operations.
 *
 * @param <T>
 * @author vgomelsky
 */

@Component
public class SystemDaoAuditInsertObserver<T extends IdentityObject> extends SystemDaoAuditDeleteObserver<T> {

	public SystemDaoAuditInsertObserver() {
		super();
		setOrder(-1000); // make sure that audit observer is always the first one to run
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// don't audit if there was an exception thrown (nothing was saved)
		if (e == null) {
			AssertUtils.assertTrue(DaoEventTypes.INSERT == event, "Unexpected event type. Expected INSERT but was: ", event);

			boolean customData = (bean instanceof SystemColumnValue);

			Date auditDate = new Date();
			short securityUserId = (Short) ((IdentityObject) getContextHandler().getBean(Context.USER_BEAN_NAME)).getIdentity();
			Map<String, Object> propertyValueMap = BeanUtils.describe(bean);
			Long parentPKFieldId = getParentPKFieldId(propertyValueMap, dao);
			List<AuditFieldMetaData> fieldList = filterAuditFieldList(getSystemAuditMetaDataHandler().getInsertFieldInfo(dao.getConfiguration().getTableName()));
			for (AuditFieldMetaData field : CollectionUtils.getIterable(fieldList)) {
				Object value = propertyValueMap.get(field.getPropertyName());

				if (bean instanceof SystemColumnCustomJsonAware && value instanceof CustomJsonString) {
					generateAuditEventsForCustomJsonValue(field.getColumnId(), (SystemColumnCustomJsonAware) bean, null, (CustomJsonString) value, null, auditDate, securityUserId, parentPKFieldId);
				}
				else {
					SystemAuditEvent auditEvent = new SystemAuditEvent();
					auditEvent.setSystemColumnId((customData ? ((SystemColumnValue) bean).getColumn().getId() : field.getColumnId()));
					auditEvent.setMainPKFieldId((customData ? ((SystemColumnValue) bean).getFkFieldId() : ((Number) bean.getIdentity())).longValue());
					auditEvent.setAuditDate(auditDate);
					auditEvent.setSecurityUserId(securityUserId);
					auditEvent.setAuditTypeId(SystemAuditEvent.AUDIT_INSERT);
					auditEvent.setParentPKFieldId(parentPKFieldId);
					auditEvent.setOldValue(null);
					auditEvent.setNewValue(truncateValue(value, field.getMaxFieldSize()));
					if (auditEvent.getNewValue() != null) {
						// do not audit null inserts
						getSystemAuditService().saveSystemAuditEvent(auditEvent);
					}
				}
			}
		}
	}
}
