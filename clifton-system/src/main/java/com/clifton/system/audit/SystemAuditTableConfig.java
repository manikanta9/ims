package com.clifton.system.audit;

import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemTableAuditConfig</code> object is a simple object used to populate and validate audit types on the table/columns match the registered audit observers.
 *
 * @author manderson
 */
public class SystemAuditTableConfig {

	private String tableName;

	/**
	 * This is the merged audit type for the table that includes checking all columns
	 * i.e. if the table is no audit, but a column is audit updates, then this will show audit updates
	 */
	private SystemAuditType auditType;

	private boolean insertObserverRegistered;

	private boolean updateObserverRegistered;

	private boolean deleteObserverRegistered;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemAuditTableConfig forTableAndAuditType(SystemTable table, SystemAuditType auditType) {
		SystemAuditTableConfig tableConfig = new SystemAuditTableConfig();
		tableConfig.setTableName(table.getName());
		tableConfig.setAuditType(auditType);
		return tableConfig;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isInvalid() {
		if (getAuditType() != null) {
			if (getAuditType().isAuditInsert() != isInsertObserverRegistered()) {
				return true;
			}
			if (getAuditType().isAuditUpdate() != isUpdateObserverRegistered()) {
				return true;
			}
			if (getAuditType().isAuditDelete() != isDeleteObserverRegistered()) {
				return true;
			}
			return false;
		}
		return isInsertObserverRegistered() || isUpdateObserverRegistered() || isDeleteObserverRegistered();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public SystemAuditType getAuditType() {
		return this.auditType;
	}


	public void setAuditType(SystemAuditType auditType) {
		this.auditType = auditType;
	}


	public boolean isInsertObserverRegistered() {
		return this.insertObserverRegistered;
	}


	public void setInsertObserverRegistered(boolean insertObserverRegistered) {
		this.insertObserverRegistered = insertObserverRegistered;
	}


	public boolean isUpdateObserverRegistered() {
		return this.updateObserverRegistered;
	}


	public void setUpdateObserverRegistered(boolean updateObserverRegistered) {
		this.updateObserverRegistered = updateObserverRegistered;
	}


	public boolean isDeleteObserverRegistered() {
		return this.deleteObserverRegistered;
	}


	public void setDeleteObserverRegistered(boolean deleteObserverRegistered) {
		this.deleteObserverRegistered = deleteObserverRegistered;
	}
}
