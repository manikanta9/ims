package com.clifton.system.lock;

/**
 * SystemLockWithNoteAware interface should be implemented by any class that wants to utilize a "locking" feature but also require a note when locking the entity.
 * The "lock" note is then cleared when the entity is unlocked.
 * <p>
 * Example: BillingInvoices can be locked and prevents other users from re-processing or approving the invoice until the person that locked the invoice unlocks it.
 * The lock note helps others to know why someone locked it
 * <p/>
 * When this interface class is extended, DAO Observers are automatically registered that prevents anyone else from making any updates.
 *
 * @author manderson
 */
public interface SystemLockWithNoteAware extends SystemLockAware {


	public String getLockNote();


	public void setLockNote(String lockNote);
}
