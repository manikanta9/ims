package com.clifton.system.lock;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemLockAwareObserver</code> validates:
 * 1. Updates cannot be made by anyone else if the entity is currently locked.
 * 2. Deletes cannot be performed if the entity is currently locked
 * 3. Updates - Unlocking can be done only by the person locked it (or an admin)
 * 4. Updates - Locking can only be done if the entity isn't currently locked by someone else
 * <p>
 *
 * @author manderson
 */
@Component
public class SystemLockAwareObserver<T extends SystemLockAware> extends BaseDaoEventObserver<T> {


	private SecurityAuthorizationService securityAuthorizationService;
	private SecurityUserService securityUserService;


	public SystemLockAwareObserver() {
		super();
		// Setting order to below default so this Observer runs before those at default.
		setOrder(-100);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		String msg = " [" + BeanUtils.getLabel(bean) + "] because: ";
		if (event.isDelete()) {
			if (bean.getLockedByUser() != null) {
				if (!isCurrentUserLockedByUser(bean, false)) {
					throw new ValidationException("Cannot delete" + msg + "it is currently locked by " + bean.getLockedByUser().getDisplayName() + ".");
				}
			}
		}
		if (event.isUpdate()) {
			T originalBean = getOriginalBean(dao, bean);
			if (originalBean != null && originalBean.getLockedByUser() != null) {
				msg += "it is currently locked by " + originalBean.getLockedByUser().getDisplayName() + ".";
				if (bean.getLockedByUser() == null) {
					if (!isCurrentUserLockedByUser(originalBean, true)) {
						throw new ValidationException("Cannot unlock" + msg);
					}
				}
				else if (!bean.getLockedByUser().equals(originalBean.getLockedByUser())) {
					throw new ValidationException("Cannot lock" + msg);
				}
				else if (!isCurrentUserLockedByUser(bean, false)) {
					throw new ValidationException("Cannot update" + msg);
				}
			}
		}
	}


	private boolean isCurrentUserLockedByUser(T bean, boolean orAdmin) {
		if (orAdmin && getSecurityAuthorizationService().isSecurityUserAdmin()) {
			return true;
		}
		else if (bean.getLockedByUser() != null && bean.getLockedByUser().equals(getSecurityUserService().getSecurityUserCurrent())) {
			return true;
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
