package com.clifton.system.lock;

import com.clifton.core.security.authorization.SecureMethod;


/**
 * SystemLockService defines methods available to lock or unlock an entity
 *
 * @author manderson
 */
public interface SystemLockService {


	/**
	 * Lock the specified entity
	 * Note: lockNote is only required / used if the entity implements {@link SystemLockWithNoteAware}
	 * This same method is used if the current user is the one that has the entity locked and wants to update the lock note.
	 */
	@SecureMethod(dynamicTableNameUrlParameter = "tableName")
	public void lockSystemLockAwareEntity(String tableName, Integer fkFieldId, String lockNote);


	/**
	 * Unlock the specified entity
	 * Note: If the entity implements {@link SystemLockWithNoteAware} then the lock note is cleared
	 * Only the user that locked the entity (or an administrator) can unlock an entity.
	 */
	@SecureMethod(dynamicTableNameUrlParameter = "tableName")
	public void unlockSystemLockAwareEntity(String tableName, Integer fkFieldId);
}
