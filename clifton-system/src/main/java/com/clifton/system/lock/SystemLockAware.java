package com.clifton.system.lock;

import com.clifton.core.beans.IdentityObject;
import com.clifton.security.user.SecurityUser;


/**
 * SystemLockAware interface should be implemented by any class that wants to utilize a "locking" feature.
 * Example: BillingInvoices can be locked and prevents other users from re-processing or approving the invoice until the person that locked the invoice unlocks it.
 * <p/>
 * When this interface class is extended, DAO Observers are automatically registered that prevents anyone else from making any updates.
 *
 * @author manderson
 */
public interface SystemLockAware extends IdentityObject {


	public SecurityUser getLockedByUser();


	public void setLockedByUser(SecurityUser lockedByUser);
}
