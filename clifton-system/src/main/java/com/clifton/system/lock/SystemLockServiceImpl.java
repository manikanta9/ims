package com.clifton.system.lock;

import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Service;


/**
 * @author manderson
 */
@Service
public class SystemLockServiceImpl implements SystemLockService {

	private DaoLocator daoLocator;

	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Note: All validation to verify current user can lock the bean is handled by {@link SystemLockAwareObserver}
	 */
	@Override
	public void lockSystemLockAwareEntity(String tableName, Integer fkFieldId, String lockNote) {
		saveSystemLockAwareEntityImpl(tableName, fkFieldId, true, lockNote);
	}


	/**
	 * Note: All validation to verify current user can unlock the bean is handled by {@link SystemLockAwareObserver}
	 */
	@Override
	public void unlockSystemLockAwareEntity(String tableName, Integer fkFieldId) {
		saveSystemLockAwareEntityImpl(tableName, fkFieldId, false, null);
	}


	/**
	 * Handles locking/unlocking in one method
	 */
	private void saveSystemLockAwareEntityImpl(String tableName, Integer id, boolean lock, String lockNote) {
		UpdatableDAO<SystemLockAware> dao = (UpdatableDAO<SystemLockAware>) getDaoLocator().<SystemLockAware>locate(tableName);
		ValidationUtils.assertNotNull(dao, "Invalid table name: " + tableName, "tableName");
		SystemLockAware bean = dao.findByPrimaryKey(id);
		ValidationUtils.assertNotNull(bean, "Cannot find bean with id = " + id + " for table: " + tableName, "id");

		if (lock) {
			if (bean instanceof SystemLockWithNoteAware) {
				ValidationUtils.assertNotEmpty(lockNote, "A lock note is required when locking this " + tableName);
				((SystemLockWithNoteAware) bean).setLockNote(lockNote);
			}
			bean.setLockedByUser(getSecurityUserService().getSecurityUserCurrent());
		}
		else {
			bean.setLockedByUser(null);
			if (bean instanceof SystemLockWithNoteAware) {
				((SystemLockWithNoteAware) bean).setLockNote(null);
			}
		}
		dao.save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
