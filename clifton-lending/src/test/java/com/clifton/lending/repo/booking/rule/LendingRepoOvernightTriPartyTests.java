package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCloseAtCostBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveJournalDetailsBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveZeroValueRecordBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRuleListBookingRule;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoTestObjectFactory;
import com.clifton.lending.repo.builder.LendingRepoBuilder;
import com.clifton.lending.repo.builder.LendingRepoBuilder.SecurityCategories;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class LendingRepoOvernightTriPartyTests {

	@Test
	@Disabled // we currently do not support non-reverse tri-party repo's, so this test is disabled until support is added
	public void testRepoTriPartyOpening() {
		testRepoOvernightTriParty(true, false,
				"-11	11696	123456	123456	Position	912810FH6	103	-500,000	-716,392.49	04/20/2012	1.3225	-947,429.07	-716,392.49	04/20/2012	04/25/2012	Tri-Party: Position close from REPO opening for 912810FH6",
				"-12	null	123456	789456	Position	912810FH6	103	500,000	716,392.49	04/20/2012	1.3225	947,429.07	716,392.49	04/20/2012	04/25/2012	Tri-Party: Position opening from REPO opening for 912810FH6",
				"-13	-12	123456	789456	Position	912810FH6	103	-500,000	-716,392.49	04/20/2012	1.3225	-947,429.07	-716,392.49	04/20/2012	04/25/2012	Position opening from REPO opening for 912810FH6",
				"-14	null	123456	654321	Position	912810FH6	103	500,000	716,392.49	04/20/2012	1.3225	947,429.07	716,392.49	04/20/2012	04/25/2012	Position opening from REPO opening for 912810FH6",
				"-15	-11	123456	123456	Cash	USD			-680,572.8655	04/20/2012	1	-680,572.87	0	04/20/2012	04/25/2012	Cash expense from REPO opening for 912810FH6",
				"-16	-12	123456	789456	Cash	USD			680,572.8655	04/20/2012	1	680,572.87	0	04/20/2012	04/25/2012	Cash proceeds from REPO opening for 912810FH6"
		);
	}


	@Test
	public void testRepoTriPartyOpeningReverse() {
		testRepoOvernightTriParty(true, true,
				"-10	null	123456	789456	Cash	USD			680,572.8655	04/20/2012	1	680,572.87	0	04/20/2012	04/25/2012	Cash transfer to fund reverse REPO opening for 912810FH6",
				"-11	null	123456	123456	Cash	USD			-680,572.8655	04/20/2012	1	-680,572.87	0	04/20/2012	04/25/2012	Cash transfer to fund reverse REPO opening for 912810FH6",
				"-12	null	123456	654321	Position	912810FH6	103	-500,000	-708,815.1	04/20/2012	1	-708,815.1	-708,815.1	04/25/2012	04/25/2012	Position close from reverse REPO opening for 912810FH6",
				"-13	null	123456	789456	Position	912810FH6	103	500,000	708,815.1	04/20/2012	1	708,815.1	708,815.1	04/25/2012	04/25/2012	Position opening from reverse REPO opening for 912810FH6",
				"-14	-12	123456	654321	Cash	USD			680,572.8655	04/20/2012	1	680,572.87	0	04/20/2012	04/25/2012	Cash proceeds from reverse REPO opening for 912810FH6",
				"-15	-13	123456	789456	Cash	USD			-680,572.8655	04/20/2012	1	-680,572.87	0	04/20/2012	04/25/2012	Cash expense from reverse REPO opening for 912810FH6"
		);
	}


	@Test
	@Disabled // we currently do not support non-reverse tri-party repo's, so this test is disabled until support is added
	public void testRepoTriPartyMaturity() {
		testRepoOvernightTriParty(false, false,
				"-10	11694	123456	789456	Position	912810FH6	103	-500,000	-708,815.1	05/01/2012	1	-708,815.1	-708,815.1	04/25/2012	05/04/2012	Position close from REPO maturity for 912810FH6",
				"-11	null	123456	123456	Position	912810FH6	103	500,000	708,815.1	05/01/2012	1	708,815.1	708,815.1	04/25/2012	05/04/2012	Position opening from REPO maturity for 912810FH6",
				"-12	-10	123456	654321	Cash	USD			680,572.8655	05/01/2012			0	05/01/2012	05/04/2012	Cash proceeds from REPO maturity for 912810FH6",
				"-13	-11	123456	789456	Cash	USD			-680,572.8655	05/01/2012			0	05/01/2012	05/04/2012	Cash expense from REPO maturity for 912810FH6",
				"-14	-11	123456	789456	Cash	USD			-37.81	05/01/2012			0	05/01/2012	05/04/2012	REPO Interest Expense from REPO maturity for 912810FH6",
				"-15	-11	123456	789456	REPO Interest Expense	912810FH6			37.81	05/01/2012			0	05/01/2012	05/04/2012	REPO Interest Expense from REPO maturity for 912810FH6.",
				"-16	-11	123456	789456	Position	912810FH6	103	-500,000	-708,815.1	05/01/2012	1	-708,815.1	-708,815.1	04/25/2012	05/04/2012	Tri-Party: Position opening from REPO maturity for 912810FH6",
				"-17	null	123456	123456	Position	912810FH6	103	500,000	708,815.1	05/01/2012	1	708,815.1	708,815.1	04/25/2012	05/04/2012	Tri-Party: Position opening from REPO maturity for 912810FH6"
		);
	}


	@Test
	public void testRepoTriPartyMaturityReverse() {
		testRepoOvernightTriParty(false, true,
				"-10	11690	123456	789456	Position	912810FH6	103	-500,000	-716,392.49	05/01/2012	1.3225	-947,429.07	-716,392.49	04/20/2012	05/04/2012	Position close from reverse REPO maturity for 912810FH6",
				"-11	11691	123456	654321	Position	912810FH6	103	500,000	716,392.49	05/01/2012	1.3225	947,429.07	716,392.49	04/20/2012	05/04/2012	Position close from reverse REPO maturity for 912810FH6",
				"-12	-10	123456	789456	Cash	USD			680,572.8655	05/01/2012	1	680,572.87	0	05/01/2012	05/04/2012	Cash proceeds from reverse REPO maturity for 912810FH6",
				"-13	-10	123456	789456	Cash	USD			37.81	05/01/2012	1	37.81	0	05/01/2012	05/04/2012	REPO Interest Income from reverse REPO maturity for 912810FH6",
				"-14	-10	123456	789456	REPO Interest Income	912810FH6			-37.81	05/01/2012	1	-37.81	0	05/01/2012	05/04/2012	REPO Interest Income from reverse REPO maturity for 912810FH6.",
				"-15	-11	123456	654321	Cash	USD			-680,572.8655	05/01/2012	1	-680,572.87	0	05/01/2012	05/04/2012	Cash expense from reverse REPO maturity for 912810FH6",
				"-16	-10	123456	789456	Cash	USD			-37.81	05/01/2012	1	-37.81	0	05/01/2012	05/04/2012	REPO Interest Income from reverse REPO maturity for 912810FH6",
				"-17	-10	123456	789456	REPO Interest Income	912810FH6			37.81	05/01/2012	1	37.81	0	05/01/2012	05/04/2012	REPO Interest Income from reverse REPO maturity for 912810FH6.",
				"-18	-10	123456	123456	Cash	USD			37.81	05/01/2012	1	37.81	0	05/01/2012	05/04/2012	REPO Interest Income from reverse REPO maturity for 912810FH6",
				"-19	-10	123456	123456	REPO Interest Income	912810FH6			-37.81	05/01/2012	1	-37.81	0	05/01/2012	05/04/2012	REPO Interest Income from reverse REPO maturity for 912810FH6.",
				"-20	null	123456	123456	Cash	USD			680,572.8655	05/01/2012	1	680,572.87	0	05/01/2012	05/04/2012	Cash transfer from reverse REPO maturity for 912810FH6",
				"-21	null	123456	789456	Cash	USD			-680,572.8655	05/01/2012	1	-680,572.87	0	05/01/2012	05/04/2012	Cash transfer from reverse REPO maturity for 912810FH6"
		);
	}


	private void testRepoOvernightTriParty(boolean open, boolean reverse, String... expectedFormattedDetails) {
		LendingRepo repo = LendingRepoBuilder.createRepo()
				.setReverse(reverse)
				.ofType("Term Tri-Party")
				.addInterestRate(new BigDecimal("1.5"))
				.addTerm(11)
				.forSecurity("912810FH6", "912810FH6", 9165, SecurityCategories.Bonds, new BigDecimal("0.01"), LendingRepoBuilder.getUSD(), new BigDecimal("10.32423"))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.forTriPartyAccount("GBR Tri-Party Repo", 620, "789456", LendingRepoBuilder.getUSD())
				.addRepoData(new BigDecimal("1.37634"), new BigDecimal("500000"), new BigDecimal("103"), new BigDecimal("5.0"), new BigDecimal("716392.49"), new BigDecimal("37.81"))
				.addExchangeRateToBase(BigDecimal.ONE)
				.toRepo();

		repo.setTradeDate(DateUtils.toDate("04/20/2012"));
		repo.setSettlementDate(DateUtils.toDate("04/25/2012"));
		repo.setMaturityDate(DateUtils.toDate("05/01/2012"));
		repo.setMaturitySettlementDate(DateUtils.toDate("05/04/2012"));

		AccountingBookingRuleTestExecutor.newTestForEntity(repo)
				.forBookingRules(getRuleList(repo, open, reverse))
				.withExpectedResults(expectedFormattedDetails)
				.execute();
	}


	private List<AccountingBookingRule<LendingRepo>> getRuleList(LendingRepo repo, boolean open, boolean reverse) {
		List<AccountingBookingRule<LendingRepo>> result = new ArrayList<>();
		if (!reverse) {
			if (open) {
				result.addAll(getRuleListTriPartyPosition(repo, open));
				AccountingRuleListBookingRule<LendingRepo> rule = new AccountingRuleListBookingRule<>();
				rule.setBookingRulesList(getBaseRuleList(repo, open, reverse));
				result.add(rule);
			}
			else {
				result.addAll(getBaseRuleList(repo, open, reverse));
				result.addAll(getRuleListTriPartyPosition(repo, open));
			}
		}
		else {
			result = getBaseRuleList(repo, open, reverse);
		}
		return result;
	}


	private List<AccountingBookingRule<LendingRepo>> getRuleListTriPartyPosition(LendingRepo repo, boolean open) {
		List<AccountingBookingRule<LendingRepo>> result = new ArrayList<>();

		LendingRepoTriPartyPositionCloseBookingRule lendingRepoPositionCloseMaturityBookingRule = LendingRepoTestObjectFactory.newLendingRepoTriPartyPositionCloseBookingRule(open);
		AccountingPositionSplitterBookingRule<LendingRepo> splitterRuleLIFO = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule();
		splitterRuleLIFO.setClosingOrder(AccountingPositionOrders.LIFO);
		configureAccountingPositionService(repo, open, repo.getRepoDefinition().isReverse(), true, splitterRuleLIFO.getAccountingPositionService(), splitterRuleLIFO.getAccountingTransactionService(), lendingRepoPositionCloseMaturityBookingRule.getAccountingJournalService());
		lendingRepoPositionCloseMaturityBookingRule.setAccountingTransactionService(splitterRuleLIFO.getAccountingTransactionService());

		AccountingRealizedGainLossBookingRule<LendingRepo> gainLossRule = AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule();

		AccountingRemoveJournalDetailsBookingRule<LendingRepo> removeGainLossRule = AccountingTestObjectFactory.newRemoveJournalDetailsBookingRule(AccountingAccount.REVENUE_REALIZED,
				AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS);

		AccountingCloseAtCostBookingRule<LendingRepo> closeAtCostBookingRule = AccountingTestObjectFactory.newCloseAtCostBookingRule();

		AccountingRemoveZeroValueRecordBookingRule<LendingRepo> accountingRemoveZeroValueRecordBookingRule = AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule();

		LendingRepoTriPartyPositionOpenBookingRule lendingRepoPositionOpenMaturityBookingRule = LendingRepoTestObjectFactory.newLendingRepoTriPartyPositionOpenBookingRule(open);
		lendingRepoPositionOpenMaturityBookingRule.setAccountingJournalService(lendingRepoPositionCloseMaturityBookingRule.getAccountingJournalService());
		lendingRepoPositionOpenMaturityBookingRule.setAccountingTransactionService(lendingRepoPositionCloseMaturityBookingRule.getAccountingTransactionService());

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//<ref bean="lendingRepoPositionCloseMaturityBookingRule" />
		result.add(lendingRepoPositionCloseMaturityBookingRule);

		//<ref bean="accountingPositionSplitterBookingRuleLIFO" />
		result.add(splitterRuleLIFO);

		//<ref bean="accountingRealizedGainLossBookingRule"/>
		result.add(gainLossRule);

		//<ref bean="accountingRemoveRealizedGainLossBookingRule"/>
		result.add(removeGainLossRule);

		//<ref bean="accountingCloseAtCostBookingRule" />
		result.add(closeAtCostBookingRule);

		//<ref bean="lendingRepoPositionOpenMaturityBookingRule" />
		result.add(lendingRepoPositionOpenMaturityBookingRule);

		//<ref bean="accountingPositionSplitterBookingRuleLIFO" />
		result.add(splitterRuleLIFO);

		if (!open) {
			//<ref bean="accountingRealizedGainLossBookingRule" />
			result.add(gainLossRule);

			//<ref bean="accountingRemoveZeroValueRecordBookingRule" />
			result.add(accountingRemoveZeroValueRecordBookingRule);
		}

		return result;
	}


	private List<AccountingBookingRule<LendingRepo>> getBaseRuleList(LendingRepo repo, boolean open, boolean reverse) {
		List<AccountingBookingRule<LendingRepo>> result = new ArrayList<>();

		LendingRepoPositionCloseBookingRule positionCloseBookingRule = LendingRepoTestObjectFactory.newLendingRepoPositionCloseBookingRule(open);
		AccountingPositionSplitterBookingRule<LendingRepo> splitterRuleLIFO = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule();
		splitterRuleLIFO.setClosingOrder(AccountingPositionOrders.LIFO);
		configureAccountingPositionService(repo, open, reverse, true, splitterRuleLIFO.getAccountingPositionService(), splitterRuleLIFO.getAccountingTransactionService(), positionCloseBookingRule.getAccountingJournalService());
		positionCloseBookingRule.setAccountingTransactionService(splitterRuleLIFO.getAccountingTransactionService());

		AccountingRealizedGainLossBookingRule<LendingRepo> gainLossRule = AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule();

		AccountingRemoveJournalDetailsBookingRule<LendingRepo> removeGainLossRule = AccountingTestObjectFactory.newRemoveJournalDetailsBookingRule(AccountingAccount.REVENUE_REALIZED,
				AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS);

		AccountingCloseAtCostBookingRule<LendingRepo> closeAtCostBookingRule = AccountingTestObjectFactory.newCloseAtCostBookingRule();

		AccountingRemoveZeroValueRecordBookingRule<LendingRepo> accountingRemoveZeroValueRecordBookingRule = AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule();

		LendingRepoPositionOpenBookingRule lendingRepoPositionOpenMaturityBookingRule = LendingRepoTestObjectFactory.newLendingRepoPositionOpenBookingRule(open);
		AccountingPositionSplitterBookingRule<LendingRepo> openingSplitterRuleLIFO;
		if (reverse && !open) {
			openingSplitterRuleLIFO = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule();
			openingSplitterRuleLIFO.setClosingOrder(AccountingPositionOrders.LIFO);
			configureAccountingPositionService(repo, open, reverse, false, openingSplitterRuleLIFO.getAccountingPositionService(), openingSplitterRuleLIFO.getAccountingTransactionService(), lendingRepoPositionOpenMaturityBookingRule.getAccountingJournalService());
			lendingRepoPositionOpenMaturityBookingRule.setAccountingTransactionService(openingSplitterRuleLIFO.getAccountingTransactionService());
		}
		else {
			openingSplitterRuleLIFO = splitterRuleLIFO;
			lendingRepoPositionOpenMaturityBookingRule.setAccountingJournalService(positionCloseBookingRule.getAccountingJournalService());
			lendingRepoPositionOpenMaturityBookingRule.setAccountingTransactionService(positionCloseBookingRule.getAccountingTransactionService());
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if (reverse && open) {
			result.add(LendingRepoTestObjectFactory.newLendingRepoTriPartyReverseCashBookingRule(open));
		}

		//<ref bean="lendingRepoPositionCloseMaturityBookingRule" />
		result.add(positionCloseBookingRule);

		//<ref bean="accountingPositionSplitterBookingRuleLIFO" />
		result.add(splitterRuleLIFO);

		//<ref bean="accountingRealizedGainLossBookingRule"/>
		result.add(gainLossRule);

		//<ref bean="accountingRemoveRealizedGainLossBookingRule"/>
		result.add(removeGainLossRule);

		//<ref bean="accountingCloseAtCostBookingRule" />
		result.add(closeAtCostBookingRule);

		//<ref bean="lendingRepoPositionOpenMaturityBookingRule" />
		result.add(lendingRepoPositionOpenMaturityBookingRule);

		//<ref bean="accountingPositionSplitterBookingRuleLIFO" />
		result.add(openingSplitterRuleLIFO);

		if (!open) {
			//<ref bean="accountingRealizedGainLossBookingRule" />
			result.add(gainLossRule);

			//<ref bean="accountingRemoveZeroValueRecordBookingRule" />
			result.add(accountingRemoveZeroValueRecordBookingRule);
		}

		//<ref bean="lendingRepoCashOrCurrencyMaturityBookingRule" />
		result.add(LendingRepoTestObjectFactory.newLendingRepoCashOrCurrencyBookingRule(open));

		if (reverse && !open) {
			result.add(LendingRepoTestObjectFactory.newLendingRepoTriPartyReverseCashBookingRule(open));
		}

		return result;
	}


	private void configureAccountingPositionService(LendingRepo repo, boolean open, boolean reverse, boolean closingRule, AccountingPositionService positionService, AccountingTransactionService transactionService, AccountingJournalService journalService) {
		List<AccountingPosition> accountPositions = new ArrayList<>();
		List<AccountingTransaction> accountTransactions = new ArrayList<>();


		if (reverse && !open && closingRule) {
			AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11690L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(500000).price("103").costBasis("716392.49").exchangeRateToBase("1.3225").on("4/20/2012").build();
			tran.setClientInvestmentAccount(repo.getClientInvestmentAccount());
			tran.setHoldingInvestmentAccount(repo.getRepoDefinition().getTriPartyInvestmentAccount());
			accountTransactions.add(tran);

			tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11691L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(-500000).price("103").costBasis("-716392.49").exchangeRateToBase("1.3225").on("4/20/2012").build();
			tran.setClientInvestmentAccount(repo.getClientInvestmentAccount());
			tran.setHoldingInvestmentAccount(repo.getRepoDefinition().getRepoInvestmentAccount());
			accountTransactions.add(tran);
		}
		else {
			AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(open ? 11696L : 11694L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(500000).price("103").costBasis("716392.49").exchangeRateToBase("1.3225").on("4/20/2012").build();
			tran.setClientInvestmentAccount(repo.getClientInvestmentAccount());
			tran.setHoldingInvestmentAccount(open ? repo.getRepoDefinition().getHoldingInvestmentAccount() : repo.getRepoDefinition().getTriPartyInvestmentAccount());
		}

		accountTransactions.forEach(transaction -> {
			accountPositions.add(AccountingTestObjectFactory.newAccountingPosition(transaction));
			Mockito.when(transactionService.getAccountingTransaction(ArgumentMatchers.eq(transaction.getId()))).thenReturn(transaction);
		});

		Mockito.when(positionService.getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
				new AccountingPositionCommand().forClientAccount(repo.getRepoDefinition().getClientInvestmentAccount().getId())
						.forInvestmentSecurity(repo.getRepoDefinition().getInvestmentSecurity().getId()))))
				.then(invocationOnMock -> {
					Integer holdingInvestmentAccountId = ((AccountingPositionCommand) invocationOnMock.getArgument(0)).getHoldingInvestmentAccountId();
					return CollectionUtils.getFiltered(accountPositions, position -> position.getHoldingInvestmentAccount().getId().equals(holdingInvestmentAccountId));
				});

		Mockito.when(transactionService.getAccountingTransactionList(ArgumentMatchers.any())).then(invocationOnMock -> {
			Integer holdingInvestmentAccountId = ((AccountingTransactionSearchForm) invocationOnMock.getArgument(0)).getHoldingInvestmentAccountId();
			return CollectionUtils.getFiltered(accountTransactions, transaction -> transaction.getHoldingInvestmentAccount().getId().equals(holdingInvestmentAccountId));
		});

		AccountingJournal journal = new AccountingJournal();
		journal.setId(1948785L);
		Mockito.when(journalService.getAccountingJournalBySourceAndSequence(ArgumentMatchers.eq("LendingRepo"), ArgumentMatchers.eq(repo.getId()), ArgumentMatchers.eq(1))).thenReturn(journal);
	}
}
