package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCloseAtCostBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveJournalDetailsBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveZeroValueRecordBookingRule;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoTestObjectFactory;
import com.clifton.lending.repo.builder.LendingRepoBuilder;
import com.clifton.lending.repo.builder.LendingRepoBuilder.SecurityCategories;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class LendingRepoMaturityBookingRuleTests {

	@Test
	public void testShortPositionCloseOnMaturity() {
		LendingRepo repo = LendingRepoBuilder.createRepo()
				.notReverse()
				.ofType("Term")
				.addInterestRate(new BigDecimal("1.5"))
				.addTerm(11)
				.forSecurity("FR0010235176", "FR0010235176", 1, SecurityCategories.Bonds, new BigDecimal("0.01"), LendingRepoBuilder.getEUR(), new BigDecimal("103.4697"))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoData(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("0.2"), new BigDecimal("1591562.51"), new BigDecimal("75.79"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();

		repo.setTradeDate(DateUtils.toDate("04/20/2012"));
		repo.setSettlementDate(DateUtils.toDate("04/25/2012"));
		repo.setMaturityDate(DateUtils.toDate("05/01/2012"));
		repo.setMaturitySettlementDate(DateUtils.toDate("05/04/2012"));


		AccountingBookingRuleTestExecutor.newTestForEntity(repo)
				.forBookingRules(getRuleList(repo, false, false))
				.withExpectedResults(
						"-10	11691	123456	654321	Position	FR0010235176	105.18	-364,000	-424,372.02	05/01/2012	1.3339	-566,069.84	-424,372.02	04/04/2012	05/04/2012	Position close from REPO maturity for FR0010235176",
						"-11	11692	123456	654321	Position	FR0010235176	105.03	-122,000	-142,171.4	05/01/2012	1.31745	-187,303.71	-142,171.4	04/13/2012	05/04/2012	Position close from REPO maturity for FR0010235176",
						"-12	11693	123456	654321	Position	FR0010235176	105.03	-889,000	-1,035,986.69	05/01/2012	1.31745	-1,364,860.66	-1,035,986.69	04/13/2012	05/04/2012	Position close from REPO maturity for FR0010235176",
						"-16	11694	123456	123456	Position	FR0010235176	105.18	364,000	422,796.89	05/01/2012	1.3225	559,148.89	422,796.89	05/02/2012	05/04/2012	Partial position close  from REPO maturity for FR0010235176",
						"-17	11694	123456	123456	Position	FR0010235176	105.03	122,000	141,706.65	05/01/2012	1.3225	187,407.05	141,706.65	05/02/2012	05/04/2012	Partial position close  from REPO maturity for FR0010235176",
						"-18	11694	123456	123456	Position	FR0010235176	105.03	889,000	1,032,600.11	05/01/2012	1.3225	1,365,613.64	1,032,600.11	05/02/2012	05/04/2012	Full position close  from REPO maturity for FR0010235176",
						"-22	-17	123456	123456	Realized Gain / Loss	FR0010235176	105.03	122,000	464.75	05/01/2012	1.31745	612.28	0	05/02/2012	05/04/2012	Realized Gain / Loss loss for FR0010235176",
						"-23	-17	123456	123456	Currency Translation Gain / Loss	FR0010235176			0	05/01/2012	1.31745	-715.62	0	05/02/2012	05/04/2012	Currency Translation Gain / Loss for FR0010235176",
						"-24	-16	123456	123456	Realized Gain / Loss	FR0010235176	105.18	364,000	1,575.13	05/01/2012	1.3339	2,101.07	0	05/02/2012	05/04/2012	Realized Gain / Loss loss for FR0010235176",
						"-25	-16	123456	123456	Currency Translation Gain / Loss	FR0010235176			0	05/01/2012	1.3339	4,819.88	0	05/02/2012	05/04/2012	Currency Translation Gain / Loss for FR0010235176",
						"-26	-18	123456	123456	Realized Gain / Loss	FR0010235176	105.03	889,000	3,386.58	05/01/2012	1.31745	4,461.65	0	05/02/2012	05/04/2012	Realized Gain / Loss loss for FR0010235176",
						"-27	-18	123456	123456	Currency Translation Gain / Loss	FR0010235176			0	05/01/2012	1.31745	-5,214.63	0	05/02/2012	05/04/2012	Currency Translation Gain / Loss for FR0010235176",
						"-28	-10	123456	654321	Currency	EUR			1,588,379.38498	05/01/2012	1	1,588,379.38	0	05/01/2012	05/04/2012	Currency proceeds from REPO maturity for FR0010235176",
						"-29	-16	123456	123456	Currency	EUR			-1,588,379.38498	05/01/2012	1	-1,588,379.38	0	05/01/2012	05/04/2012	Currency expense from REPO maturity for FR0010235176",
						"-30	-16	123456	123456	Currency	EUR			-75.79	05/01/2012	1	-75.79	0	05/01/2012	05/04/2012	REPO Interest Expense from REPO maturity for FR0010235176",
						"-31	-16	123456	123456	REPO Interest Expense	FR0010235176			75.79	05/01/2012	1	75.79	0	05/01/2012	05/04/2012	REPO Interest Expense from REPO maturity for FR0010235176."
				)
				.execute();
	}


	@Test
	public void testReverseRepoMaturity() {
		LendingRepo repo = LendingRepoBuilder.createRepo()
				.reverse()
				.ofType("Term")
				.addInterestRate(new BigDecimal("1.5"))
				.addTerm(11)
				.forSecurity("FR0010235176", "FR0010235176", 1, SecurityCategories.Bonds, new BigDecimal("0.01"), LendingRepoBuilder.getUSD(), new BigDecimal("103.4697"))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoData(new BigDecimal("1.11062"), new BigDecimal("500000.00"), new BigDecimal("103.4697"), new BigDecimal("0.2"), new BigDecimal("716392.49"), new BigDecimal("75.79"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();

		repo.setTradeDate(DateUtils.toDate("04/20/2012"));
		repo.setSettlementDate(DateUtils.toDate("04/25/2012"));
		repo.setMaturityDate(DateUtils.toDate("05/01/2012"));
		repo.setMaturitySettlementDate(DateUtils.toDate("05/04/2012"));


		AccountingBookingRuleTestExecutor.newTestForEntity(repo)
				.forBookingRules(getRuleList(repo, false, true))
				.withExpectedResults(
						"-10	11694	123456	123456	Position	FR0010235176	103.4697	-500,000	-716,392.49	05/01/2012	1.3225	-947,429.07	-716,392.49	04/20/2012	05/04/2012	Position close from reverse REPO maturity for FR0010235176",
						"-11	11695	123456	654321	Position	FR0010235176	103.4697	500,000	716,392.49	05/01/2012	1.3225	947,429.07	716,392.49	04/20/2012	05/04/2012	Position close from reverse REPO maturity for FR0010235176",
						"-12	-10	123456	123456	Cash	USD			714,959.70502	05/01/2012	1	714,959.71	0	05/01/2012	05/04/2012	Cash proceeds from reverse REPO maturity for FR0010235176",
						"-13	-10	123456	123456	Cash	USD			75.79	05/01/2012	1	75.79	0	05/01/2012	05/04/2012	REPO Interest Income from reverse REPO maturity for FR0010235176",
						"-14	-10	123456	123456	REPO Interest Income	FR0010235176			-75.79	05/01/2012	1	-75.79	0	05/01/2012	05/04/2012	REPO Interest Income from reverse REPO maturity for FR0010235176.",
						"-15	-11	123456	654321	Cash	USD			-714,959.70502	05/01/2012	1	-714,959.71	0	05/01/2012	05/04/2012	Cash expense from reverse REPO maturity for FR0010235176"
				).execute();
	}


	private List<AccountingBookingRule<LendingRepo>> getRuleList(LendingRepo repo, boolean open, boolean reverseRepo) {
		List<AccountingBookingRule<LendingRepo>> result = new ArrayList<>();

		//<ref bean="lendingRepoPositionCloseMaturityBookingRule" />
		LendingRepoPositionCloseBookingRule closeBookingRule = LendingRepoTestObjectFactory.newLendingRepoPositionCloseBookingRule(open);
		result.add(closeBookingRule);

		//<ref bean="accountingPositionSplitterBookingRuleLIFO" />
		AccountingPositionSplitterBookingRule<LendingRepo> splitterRuleLIFO = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule();
		splitterRuleLIFO.setClosingOrder(AccountingPositionOrders.LIFO);
		if (reverseRepo) {
			configureReverseAccountingPositionService(repo, splitterRuleLIFO.getAccountingPositionService(), splitterRuleLIFO.getAccountingTransactionService(), closeBookingRule.getAccountingJournalService());
		}
		else {
			configureAccountingPositionService(repo, splitterRuleLIFO.getAccountingPositionService(), splitterRuleLIFO.getAccountingTransactionService(), closeBookingRule.getAccountingJournalService());
		}
		closeBookingRule.setAccountingTransactionService(splitterRuleLIFO.getAccountingTransactionService());
		if (open) {
			result.add(splitterRuleLIFO);
		}

		//<ref bean="accountingRealizedGainLossBookingRule"/>
		AccountingRealizedGainLossBookingRule<LendingRepo> gainLossRule = AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule();
		result.add(gainLossRule);

		//<ref bean="accountingRemoveRealizedGainLossBookingRule"/>
		AccountingRemoveJournalDetailsBookingRule<LendingRepo> removeGainLossRule = AccountingTestObjectFactory.newRemoveJournalDetailsBookingRule(AccountingAccount.REVENUE_REALIZED,
				AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS);
		result.add(removeGainLossRule);

		//<ref bean="accountingCloseAtCostBookingRule" />
		AccountingCloseAtCostBookingRule<LendingRepo> closeAtCostBookingRule = AccountingTestObjectFactory.newCloseAtCostBookingRule();
		result.add(closeAtCostBookingRule);

		//<ref bean="lendingRepoPositionOpenMaturityBookingRule" />
		LendingRepoPositionOpenBookingRule lendingRepoPositionOpenMaturityBookingRule = LendingRepoTestObjectFactory.newLendingRepoPositionOpenBookingRule(open);
		result.add(lendingRepoPositionOpenMaturityBookingRule);
		//<ref bean="accountingPositionSplitterBookingRuleLIFO" />
		result.add(splitterRuleLIFO);
		lendingRepoPositionOpenMaturityBookingRule.setAccountingTransactionService(splitterRuleLIFO.getAccountingTransactionService());
		if (reverseRepo) {
			lendingRepoPositionOpenMaturityBookingRule.setAccountingTransactionService(splitterRuleLIFO.getAccountingTransactionService());
			lendingRepoPositionOpenMaturityBookingRule.setAccountingJournalService(closeBookingRule.getAccountingJournalService());
		}

		//<ref bean="accountingRealizedGainLossBookingRule" />
		result.add(gainLossRule);

		//<ref bean="accountingRemoveZeroValueRecordBookingRule" />
		AccountingRemoveZeroValueRecordBookingRule<LendingRepo> accountingRemoveZeroValueRecordBookingRule = AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule();
		result.add(accountingRemoveZeroValueRecordBookingRule);

		//<ref bean="lendingRepoCashOrCurrencyMaturityBookingRule" />
		result.add(LendingRepoTestObjectFactory.newLendingRepoCashOrCurrencyBookingRule(open));

		return result;
	}


	private void configureReverseAccountingPositionService(LendingRepo repo, AccountingPositionService positionService, AccountingTransactionService transactionService, AccountingJournalService journalService) {
		List<AccountingPosition> repoAccountPositions = new ArrayList<>();
		List<AccountingTransaction> repoAccountTransactions = new ArrayList<>();
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11694L, repo.getRepoDefinition().getInvestmentSecurity())
				.qty(500000).price("103.4697").costBasis("716392.49").exchangeRateToBase("1.3225").on("4/20/2012").build();
		tran.setClientInvestmentAccount(repo.getClientInvestmentAccount());
		tran.setHoldingInvestmentAccount(repo.getHoldingInvestmentAccount());
		repoAccountTransactions.add(tran);
		repoAccountPositions.add(AccountingTestObjectFactory.newAccountingPosition(tran));

		List<AccountingPosition> holdingAccountPositions = new ArrayList<>();
		tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11695L, repo.getRepoDefinition().getInvestmentSecurity())
				.qty(-500000).price("103.4697").costBasis("-716392.49").exchangeRateToBase("1.3225").on("4/20/2012").build();
		tran.setClientInvestmentAccount(repo.getClientInvestmentAccount());
		tran.setHoldingInvestmentAccount(repo.getRepoDefinition().getRepoInvestmentAccount());
		repoAccountTransactions.add(tran);
		holdingAccountPositions.add(AccountingTestObjectFactory.newAccountingPosition(tran));


		Mockito.when(positionService.getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
				new AccountingPositionCommand().forClientAccount(repo.getRepoDefinition().getClientInvestmentAccount().getId())
						.forHoldingAccount(repo.getRepoDefinition().getRepoInvestmentAccount().getId())
						.forInvestmentSecurity(repo.getRepoDefinition().getInvestmentSecurity().getId())
		))).thenReturn(repoAccountPositions);

		Mockito.when(positionService.getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
				new AccountingPositionCommand().forClientAccount(repo.getRepoDefinition().getClientInvestmentAccount().getId())
						.forHoldingAccount(repo.getRepoDefinition().getHoldingInvestmentAccount().getId())
						.forInvestmentSecurity(repo.getRepoDefinition().getInvestmentSecurity().getId())
		))).thenReturn(holdingAccountPositions);

		repoAccountTransactions.forEach(transaction -> Mockito.when(transactionService.getAccountingTransaction(ArgumentMatchers.eq(transaction.getId()))).thenReturn(transaction));

		Mockito.when(transactionService.getAccountingTransactionList(ArgumentMatchers.any())).then(invocationOnMock -> {
			int holdingInvestmentAccountId = ((AccountingTransactionSearchForm) invocationOnMock.getArgument(0)).getHoldingInvestmentAccountId();
			return CollectionUtils.getFiltered(repoAccountTransactions, transaction -> transaction.getHoldingInvestmentAccount().getId().equals(holdingInvestmentAccountId));
		});

		AccountingJournal journal = new AccountingJournal();
		journal.setId(1948785L);
		Mockito.when(journalService.getAccountingJournalBySourceAndSequence(ArgumentMatchers.eq("LendingRepo"), ArgumentMatchers.eq(repo.getId()), ArgumentMatchers.eq(1))).thenReturn(journal);
	}


	private void configureAccountingPositionService(LendingRepo repo, AccountingPositionService positionService, AccountingTransactionService transactionService, AccountingJournalService journalService) {
		List<AccountingPosition> repoAccountPositions = new ArrayList<>();
		List<AccountingTransaction> repoAccountTransactions = new ArrayList<>();
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11691L, repo.getRepoDefinition().getInvestmentSecurity())
				.qty(364000).price("105.18").costBasis("424372.02").exchangeRateToBase("1.3339").on("04/04/2012").build();
		tran.setClientInvestmentAccount(repo.getClientInvestmentAccount());
		tran.setHoldingInvestmentAccount(repo.getRepoDefinition().getRepoInvestmentAccount());
		repoAccountTransactions.add(tran);
		repoAccountPositions.add(AccountingTestObjectFactory.newAccountingPosition(tran));
		Mockito.when(transactionService.getAccountingTransaction(ArgumentMatchers.eq(tran.getId()))).thenReturn(tran);

		tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11692L, repo.getRepoDefinition().getInvestmentSecurity())
				.qty(122000).price("105.03").costBasis("142171.40").exchangeRateToBase("1.31745").on("04/13/2012").build();
		tran.setClientInvestmentAccount(repo.getClientInvestmentAccount());
		tran.setHoldingInvestmentAccount(repo.getRepoDefinition().getRepoInvestmentAccount());
		repoAccountTransactions.add(tran);
		repoAccountPositions.add(AccountingTestObjectFactory.newAccountingPosition(tran));
		Mockito.when(transactionService.getAccountingTransaction(ArgumentMatchers.eq(tran.getId()))).thenReturn(tran);

		tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11693L, repo.getRepoDefinition().getInvestmentSecurity())
				.qty(889000).price("105.03").costBasis("1035986.69").exchangeRateToBase("1.31745").on("04/13/2012").build();
		tran.setClientInvestmentAccount(repo.getClientInvestmentAccount());
		tran.setHoldingInvestmentAccount(repo.getRepoDefinition().getRepoInvestmentAccount());
		repoAccountTransactions.add(tran);
		repoAccountPositions.add(AccountingTestObjectFactory.newAccountingPosition(tran));
		Mockito.when(transactionService.getAccountingTransaction(ArgumentMatchers.eq(tran.getId()))).thenReturn(tran);

		Mockito.when(positionService.getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
				new AccountingPositionCommand().forClientAccount(repo.getRepoDefinition().getClientInvestmentAccount().getId())
						.forHoldingAccount(repo.getRepoDefinition().getRepoInvestmentAccount().getId())
						.forInvestmentSecurity(repo.getRepoDefinition().getInvestmentSecurity().getId())
		))).thenReturn(repoAccountPositions);

		AccountingJournal journal = new AccountingJournal();
		journal.setId(1948785L);
		Mockito.when(journalService.getAccountingJournalBySourceAndSequence(ArgumentMatchers.eq("LendingRepo"), ArgumentMatchers.eq(repo.getId()), ArgumentMatchers.eq(1))).thenReturn(journal);

		Mockito.when(transactionService.getAccountingTransactionList(ArgumentMatchers.any())).thenReturn(repoAccountTransactions);


		List<AccountingPosition> holdingAccountPositions = new ArrayList<>();

		tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11694L, repo.getRepoDefinition().getInvestmentSecurity())
				.qty(-1375000).price("104.41").costBasis("-1597103.65").exchangeRateToBase("1.3225").on("05/02/2012").build();
		holdingAccountPositions.add(AccountingTestObjectFactory.newAccountingPosition(tran));
		Mockito.when(transactionService.getAccountingTransaction(ArgumentMatchers.eq(tran.getId()))).thenReturn(tran);

		Mockito.when(positionService.getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
				new AccountingPositionCommand().forClientAccount(repo.getRepoDefinition().getClientInvestmentAccount().getId())
						.forHoldingAccount(repo.getRepoDefinition().getHoldingInvestmentAccount().getId())
						.forInvestmentSecurity(repo.getRepoDefinition().getInvestmentSecurity().getId())
		))).thenReturn(holdingAccountPositions);
	}
}
