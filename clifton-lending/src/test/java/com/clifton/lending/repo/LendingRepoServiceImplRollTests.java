package com.clifton.lending.repo;


import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.lending.repo.builder.LendingRepoBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class LendingRepoServiceImplRollTests {

	@Resource
	private LendingRepoService lendingRepoService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRollLendingRepo_Term() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.forDates("10/01/2012", "10/02/2012", "10/09/2012", "10/10/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo.setConfirmedDate(new Date());
		repo.setConfirmationNote("Test");
		repo = this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(8, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/10/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("464.75"), repo.getInterestAmount());
		Assertions.assertNotNull(repo.getConfirmedDate());
		Assertions.assertNotNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		LendingRepo rolledRepo = rollLendingRepo(repo, "1.5", "103.4697", null); // Infer maturity date using term from original repo
		Assertions.assertEquals(8, rolledRepo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), rolledRepo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/10/2012"), rolledRepo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/17/2012"), rolledRepo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/18/2012"), rolledRepo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), rolledRepo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), rolledRepo.getNetCash());
		Assertions.assertEquals(new BigDecimal("464.75"), rolledRepo.getInterestAmount());
		Assertions.assertNull(rolledRepo.getConfirmedDate());
		Assertions.assertNull(rolledRepo.getConfirmationNote());
		Assertions.assertEquals(repo.getId(), rolledRepo.getParentIdentifier());
	}


	@Test
	public void testRollLendingRepo_TermWeekend() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.forDates("10/01/2012", "10/02/2012", "10/04/2012", "10/05/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo.setConfirmedDate(new Date());
		repo.setConfirmationNote("Test");
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(3, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/04/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("174.28"), repo.getInterestAmount());
		Assertions.assertNotNull(repo.getConfirmedDate());
		Assertions.assertNotNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		LendingRepo rolledRepo = rollLendingRepo(repo, "1.5", "103.4697", null); // Infer maturity date using term from original repo
		Assertions.assertEquals(4, rolledRepo.getTerm()); // Term adjusts to 4 due to non-business days
		Assertions.assertEquals(DateUtils.toDate("10/04/2012"), rolledRepo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), rolledRepo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), rolledRepo.getMaturityDate()); // Holiday weekend: 10/6, 10/7, and 10/8
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), rolledRepo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), rolledRepo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), rolledRepo.getNetCash());
		Assertions.assertEquals(new BigDecimal("232.38"), rolledRepo.getInterestAmount());
		Assertions.assertNull(rolledRepo.getConfirmedDate());
		Assertions.assertNull(rolledRepo.getConfirmationNote());
		Assertions.assertEquals(repo.getId(), rolledRepo.getParentIdentifier());
	}


	@Test
	public void testRollLendingRepo_Term_AlteredLength() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.forDates("10/01/2012", "10/01/2012", "10/09/2012", "10/09/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("SWIB", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("SWIB REPO", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo.setConfirmedDate(new Date());
		repo.setConfirmationNote("Test");
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(8, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("464.75"), repo.getInterestAmount());
		Assertions.assertNotNull(repo.getConfirmedDate());
		Assertions.assertNotNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		LendingRepo rolledRepo = rollLendingRepo(repo, "5", "123.4567", "11/08/2012");
		Assertions.assertEquals(30, rolledRepo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), rolledRepo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), rolledRepo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("11/08/2012"), rolledRepo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("11/08/2012"), rolledRepo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1697529.63"), rolledRepo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1663579.04"), rolledRepo.getNetCash());
		Assertions.assertEquals(new BigDecimal("6931.58"), rolledRepo.getInterestAmount());
		Assertions.assertNull(rolledRepo.getConfirmedDate());
		Assertions.assertNull(rolledRepo.getConfirmationNote());
		Assertions.assertEquals(repo.getId(), rolledRepo.getParentIdentifier());
	}


	@Test
	public void testRollLendingRepo_Overnight() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Overnight")
				.forDates("10/01/2012", "10/01/2012", "10/02/2012", "10/02/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo.setConfirmedDate(new Date());
		repo.setConfirmationNote("Test");
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(1, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNotNull(repo.getConfirmedDate());
		Assertions.assertNotNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		LendingRepo rolledRepo = rollLendingRepo(repo, "1.5", "103.4697", null);
		Assertions.assertEquals(1, rolledRepo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), rolledRepo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), rolledRepo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/03/2012"), rolledRepo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/03/2012"), rolledRepo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), rolledRepo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), rolledRepo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), rolledRepo.getInterestAmount());
		Assertions.assertNull(rolledRepo.getConfirmedDate());
		Assertions.assertNull(rolledRepo.getConfirmationNote());
		Assertions.assertEquals(repo.getId(), rolledRepo.getParentIdentifier());
	}


	@Test
	public void testRollLendingRepo_Overnight_Weekend() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Overnight")
				.forDates("10/04/2012", "10/04/2012", "10/05/2012", "10/05/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("SWIB", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("SWIB REPO", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("123.4567"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo.setConfirmedDate(new Date());
		repo.setConfirmationNote("Test1");
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(1, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/04/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/04/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1697529.63"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1663579.04"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("69.32"), repo.getInterestAmount());
		Assertions.assertNotNull(repo.getConfirmedDate());
		Assertions.assertNotNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		LendingRepo rolledRepo = rollLendingRepo(repo, "1.5", "234.5678", null);
		Assertions.assertEquals(4, rolledRepo.getTerm()); // 3-day weekend -> 4-day term
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), rolledRepo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), rolledRepo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), rolledRepo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), rolledRepo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("3225307.25"), rolledRepo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("3160801.11"), rolledRepo.getNetCash());
		Assertions.assertEquals(new BigDecimal("526.80"), rolledRepo.getInterestAmount());
		Assertions.assertNull(rolledRepo.getConfirmedDate());
		Assertions.assertNull(rolledRepo.getConfirmationNote());
		Assertions.assertEquals(repo.getId(), rolledRepo.getParentIdentifier());
	}


	@Test
	public void testRollLendingRepo_OpenWithMaturityDate() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Open")
				.forDates("10/01/2012", "10/01/2012", "10/02/2012", "10/02/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo.setConfirmedDate(new Date());
		repo.setConfirmationNote("Test");
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(1, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNotNull(repo.getConfirmedDate());
		Assertions.assertNotNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		LendingRepo rolledRepo = rollLendingRepo(repo, "1.5", "103.4697", null);
		Assertions.assertEquals(1, rolledRepo.getTerm()); // Open repo term is always one
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), rolledRepo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), rolledRepo.getSettlementDate());
		Assertions.assertNull(rolledRepo.getMaturityDate());
		Assertions.assertNull(rolledRepo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), rolledRepo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), rolledRepo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), rolledRepo.getInterestAmount());
	}


	@Test
	public void testRollLendingRepo_OpenWithNoMaturityDate() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Open")
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo.setConfirmedDate(new Date());
		repo.setConfirmationNote("Test1");
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(1, repo.getTerm()); // Open repo term is always one
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertNull(repo.getMaturityDate());
		Assertions.assertNull(repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNotNull(repo.getConfirmedDate());
		Assertions.assertNotNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		TestUtils.expectException(FieldValidationException.class, () -> rollLendingRepo(repo, "1.5", "103.4697", null), "A maturity date is required to roll a repo.");
	}


	@Test
	public void testRollLendingRepo_OpenWithNoMaturitySettlementDate() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Open")
				.forDates("10/01/2012", "10/01/2012", "10/02/2012", null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo.setConfirmedDate(new Date());
		repo.setConfirmationNote("Test1");
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(1, repo.getTerm()); // Open repo term is always one
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturityDate());
		Assertions.assertNull(repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNotNull(repo.getConfirmedDate());
		Assertions.assertNotNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		TestUtils.expectException(FieldValidationException.class, () -> rollLendingRepo(repo, "1.5", "103.4697", null), "A maturity settlement date is required to roll a repo.");
	}


	@Test
	public void testRollLendingRepo_OpenWithTermAndNoMaturityDate() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Open")
				.addTerm(1)
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo.setConfirmedDate(new Date());
		repo.setConfirmationNote("Test1");
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(1, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertNull(repo.getMaturityDate()); // Maturity date is not inferred
		Assertions.assertNull(repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNotNull(repo.getConfirmedDate());
		Assertions.assertNotNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		TestUtils.expectException(FieldValidationException.class, () -> rollLendingRepo(repo, "1.5", "103.4697", null), "A maturity date is required to roll a repo.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private LendingRepo rollLendingRepo(LendingRepo repo, String newInterestRate, String newPrice, String newMaturityDate) {
		LendingRepo rolledRepo = this.lendingRepoService.rollLendingRepo(repo.getId());
		rolledRepo.setInterestRate(new BigDecimal(newInterestRate));
		rolledRepo.setPrice(new BigDecimal(newPrice));
		if (newMaturityDate != null) {
			rolledRepo.setMaturityDate(null);
			rolledRepo.setMaturitySettlementDate(DateUtils.toDate(newMaturityDate));
		}
		return this.lendingRepoService.saveLendingRepo(rolledRepo);
	}
}
