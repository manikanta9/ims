package com.clifton.lending.repo.instruction;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.date.DateUtils;
import com.clifton.lending.repo.LendingRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@ContextConfiguration("classpath:com/clifton/lending/repo/instruction/LendingRepoCounterpartyInstructionSelectorTest-context.xml")
@Transactional
public class LendingRepoCounterpartyInstructionSelectorTest extends BaseInMemoryDatabaseTests {

	@Resource
	private LendingRepoCounterpartyInstructionSelector lendingRepoCounterpartyInstructionSelector;


	@Test
	public void getEntityListTest() {
		Assertions.assertNotNull(this.lendingRepoCounterpartyInstructionSelector);

		List<LendingRepo> lendingRepoList = this.lendingRepoCounterpartyInstructionSelector.getEntityList(null, null, DateUtils.toDate("2/11/2016", DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertNotNull(lendingRepoList);
		Assertions.assertEquals(24, lendingRepoList.size());
	}
}
