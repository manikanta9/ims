package com.clifton.lending.repo;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.lending.repo.interest.LendingRepoInterestRate;
import com.clifton.lending.repo.interest.LendingRepoInterestRateCounterParty;
import com.clifton.lending.repo.interest.LendingRepoInterestRateDate;
import com.clifton.lending.repo.interest.LendingRepoInterestRateService;
import com.clifton.lending.repo.interest.search.LendingRepoInterestRateSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author stevenf
 */
@ContextConfiguration("classpath:com/clifton/lending/LendingInMemoryDatabaseTests-context.xml")
@Transactional
public class LendingRepoInterestRateDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private LendingRepoInterestRateService lendingRepoInterestRateService;


	@Test
	public void testLendingRepoInterestRates() {
		Date rateDate = DateUtils.toDate("08/17/2017");

		List<LendingRepoInterestRate> interestRateList = getLendingRepoInterestRateList(rateDate);
		validateInterestRateList(interestRateList, true);

		//Ensure all were deleted
		interestRateList = getLendingRepoInterestRateList(rateDate);
		Assertions.assertEquals(CollectionUtils.getSize(interestRateList), 0, "Failed to find expected results: ");

		//Now rebuild
		LendingRepoInterestRateDate interestRateDate = getLendingRepoInterestRateService().getLendingRepoInterestRateDateForDate(rateDate);
		Assertions.assertEquals(CollectionUtils.getSize(interestRateDate.getCounterPartyRateList()), 2, "Failed to find expected results: ");
		for (LendingRepoInterestRateCounterParty interestRateCounterParty : CollectionUtils.getIterable(interestRateDate.getCounterPartyRateList())) {
			if (interestRateCounterParty.getCounterparty().getLabel().equals("Bank of New York Mellon") && interestRateCounterParty.getCancellationNoticeTerm() != null && interestRateCounterParty.getCancellationNoticeTerm() == 7) {
				interestRateCounterParty.setInterestRate(new BigDecimal("1.26"));
			}
			else if (interestRateCounterParty.getCounterparty().getLabel().equals("Bank of New York Mellon")) {
				interestRateCounterParty.setInterestRate(new BigDecimal("1.19"));
			}
			else {
				interestRateCounterParty.setInterestRate(new BigDecimal("1.2"));
			}
		}
		getLendingRepoInterestRateService().buildLendingRepoInterestRate(interestRateDate);

		//Requery, and validate again.
		interestRateList = getLendingRepoInterestRateList(rateDate);
		validateInterestRateList(interestRateList, false);
	}


	private List<LendingRepoInterestRate> getLendingRepoInterestRateList(Date rateDate) {
		LendingRepoInterestRateSearchForm searchForm = new LendingRepoInterestRateSearchForm();
		searchForm.setInterestRateDate(rateDate);
		searchForm.setMultipleInterestRatesPerPeriod(true);
		searchForm.setOrderBy("interestAmount:ASC");
		searchForm.setExcludeWorkflowStateName("Cancelled");
		return getLendingRepoInterestRateService().getLendingRepoInterestRateList(searchForm);
	}


	private void validateInterestRateList(List<LendingRepoInterestRate> interestRateList, boolean delete) {
		Assertions.assertEquals(94, CollectionUtils.getSize(interestRateList), "Failed to find expected results: ");
		Assertions.assertEquals(2, interestRateList.stream().map(interestRate -> interestRate.getRepo().getType()).distinct().collect(Collectors.toList()).size(), "Failed to find expected results: ");
		Assertions.assertEquals(1, interestRateList.stream().map(interestRate -> interestRate.getRepo().getRepoDefinition().getCounterparty()).distinct().collect(Collectors.toList()).size(), "Failed to find expected results: ");
		Assertions.assertEquals(2, interestRateList.stream().map(interestRate -> interestRate.getRepo().getCancellationNoticeTerm()).distinct().collect(Collectors.toList()).size(), "Failed to find expected results: ");
		for (int i = 0; i < CollectionUtils.getSize(interestRateList); i++) {
			LendingRepoInterestRate interestRate = interestRateList.get(i);
			Assertions.assertEquals(toStringFormatted(interestRate), EXPECTED_LENDING_REPORT_INTEREST_RATES_08_17_17[i]);
			if (delete) {
				//Delete the rate after we validate - we will end up rebuilding and validating again
				getLendingRepoInterestRateService().deleteLendingRepoInterestRate(interestRate.getId());
			}
		}
	}


	private String toStringFormatted(LendingRepoInterestRate interestRate) {
		StringBuilder builder = new StringBuilder();
		builder.append(interestRate.getRepo().getType().getName()).append("\t")
				.append(interestRate.getRepo().getClientInvestmentAccount().getLabel()).append("\t")
				.append(interestRate.getRepo().getRepoDefinition().getCounterparty().getLabel()).append("\t")
				.append(interestRate.getRepo().getTerm()).append("\t")
				.append(interestRate.getRepo().getCancellationNoticeTerm() != null ? interestRate.getRepo().getCancellationNoticeTerm() : "").append("\t")
				.append(interestRate.getRepo().getInvestmentSecurity().getSymbol()).append("\t")
				.append(CoreMathUtils.formatNumberMoney(interestRate.getRepo().getOriginalFace())).append("\t")
				.append(CoreMathUtils.formatNumberMoney(interestRate.getRepo().getNetCash())).append("\t")
				.append(DateUtils.fromDate(interestRate.getInterestRateDate(), DateUtils.DATE_FORMAT_INPUT)).append("\t")
				.append(CoreMathUtils.formatNumberMoney(interestRate.getInterestRate())).append("\t")
				.append(CoreMathUtils.formatNumberMoney(interestRate.getInterestAmount()));
		return builder.toString();
	}


	private static final String[] EXPECTED_LENDING_REPORT_INTEREST_RATES_08_17_17 = new String[]{
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RL4\t5,000,000.00\t4,723,618.94\t08/17/2017\t1.19\t156.14",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810FQ6\t2,760,000.00\t5,316,727.84\t08/17/2017\t1.19\t175.75",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828N71\t5,340,000.00\t5,428,514.17\t08/17/2017\t1.19\t179.44",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828QV5\t5,020,000.00\t5,494,973.32\t08/17/2017\t1.19\t181.64",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RF7\t5,000,000.00\t5,544,860.40\t08/17/2017\t1.19\t183.29",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828SA9\t5,280,000.00\t5,587,617.29\t08/17/2017\t1.19\t184.70",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828H45\t5,640,000.00\t5,600,511.88\t08/17/2017\t1.19\t185.13",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828WU0\t6,020,000.00\t5,965,213.99\t08/17/2017\t1.19\t197.18",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810QF8\t4,140,000.00\t5,999,206.29\t08/17/2017\t1.19\t198.31",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828WU0\t5,676,000.00\t5,678,199.70\t08/17/2017\t1.26\t198.74",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828XL9\t6,020,000.00\t6,036,847.05\t08/17/2017\t1.19\t199.55",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828B25\t5,810,000.00\t6,061,397.25\t08/17/2017\t1.19\t200.36",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828UH1\t5,940,000.00\t6,109,356.02\t08/17/2017\t1.19\t201.95",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828SA9\t5,455,000.00\t5,802,948.31\t08/17/2017\t1.26\t203.10",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828TE0\t5,900,000.00\t6,151,387.42\t08/17/2017\t1.19\t203.34",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828VM9\t5,990,000.00\t6,212,139.04\t08/17/2017\t1.19\t205.35",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828B25\t5,606,000.00\t5,891,578.87\t08/17/2017\t1.26\t206.21",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828UH1\t5,747,000.00\t5,961,359.84\t08/17/2017\t1.26\t208.65",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828VM9\t5,728,000.00\t5,987,849.84\t08/17/2017\t1.26\t209.57",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828TE0\t5,721,000.00\t6,003,654.86\t08/17/2017\t1.26\t210.13",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828Q60\t6,178,000.00\t6,271,729.81\t08/17/2017\t1.26\t219.51",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912810FS2\t4,709,000.00\t6,423,717.36\t08/17/2017\t1.26\t224.83",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912810FH6\t3,243,000.00\t6,522,117.39\t08/17/2017\t1.26\t228.27",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828C99\t6,890,000.00\t7,093,783.50\t08/17/2017\t1.19\t234.49",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828K33\t7,150,000.00\t7,362,444.70\t08/17/2017\t1.19\t243.37",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828C99\t7,064,000.00\t7,247,461.82\t08/17/2017\t1.26\t253.66",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828K33\t7,051,000.00\t7,255,915.08\t08/17/2017\t1.26\t253.96",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RA8\t7,980,000.00\t8,137,053.00\t08/17/2017\t1.19\t268.97",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810QP6\t6,230,000.00\t8,890,126.80\t08/17/2017\t1.19\t293.87",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828NM8\t7,313,000.00\t8,397,775.13\t08/17/2017\t1.26\t293.92",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828K33\t8,710,000.00\t8,949,933.22\t08/17/2017\t1.19\t295.85",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912810FR4\t6,194,000.00\t9,043,660.25\t08/17/2017\t1.26\t316.53",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828JX9\t8,240,000.00\t9,683,065.17\t08/17/2017\t1.19\t320.08",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810FQ6\t5,410,000.00\t10,690,808.40\t08/17/2017\t1.19\t353.39",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828PP9\t9,225,000.00\t10,535,983.75\t08/17/2017\t1.26\t368.76",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810PV4\t8,580,000.00\t11,329,776.91\t08/17/2017\t1.19\t374.51",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810PZ5\t8,220,000.00\t11,433,920.66\t08/17/2017\t1.19\t377.95",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810QV3\t10,960,000.00\t11,484,946.48\t08/17/2017\t1.19\t379.64",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810PS1\t8,270,000.00\t11,845,603.83\t08/17/2017\t1.19\t391.56",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828LA6\t10,240,000.00\t12,209,466.44\t08/17/2017\t1.19\t403.59",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810FD5\t6,160,000.00\t12,275,471.95\t08/17/2017\t1.19\t405.77",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828MF4\t11,020,000.00\t12,814,646.81\t08/17/2017\t1.19\t423.60",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810FS2\t9,420,000.00\t13,169,016.40\t08/17/2017\t1.19\t435.31",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810QF8\t9,400,000.00\t13,779,827.16\t08/17/2017\t1.19\t455.50",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RL4\t13,970,000.00\t14,335,283.31\t08/17/2017\t1.19\t473.86",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RF7\t12,380,000.00\t14,498,966.85\t08/17/2017\t1.19\t479.27",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RR1\t14,360,000.00\t15,964,808.27\t08/17/2017\t1.19\t527.73",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RA8\t15,820,000.00\t16,435,952.98\t08/17/2017\t1.19\t543.30",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810FH6\t8,080,000.00\t16,634,366.02\t08/17/2017\t1.19\t549.86",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810QP6\t11,620,000.00\t16,976,720.55\t08/17/2017\t1.19\t561.17",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RR1\t16,380,000.00\t17,499,131.62\t08/17/2017\t1.19\t578.44",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828JX9\t14,980,000.00\t17,608,067.12\t08/17/2017\t1.19\t582.04",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828S50\t19,100,000.00\t19,043,401.92\t08/17/2017\t1.19\t629.49",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810FR4\t13,260,000.00\t19,772,450.08\t08/17/2017\t1.19\t653.59",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828NM8\t17,950,000.00\t20,823,612.76\t08/17/2017\t1.19\t688.34",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810PV4\t16,290,000.00\t21,582,138.91\t08/17/2017\t1.19\t713.41",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828V49\t23,000,000.00\t22,522,076.57\t08/17/2017\t1.19\t744.48",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828PP9\t19,690,000.00\t22,662,933.19\t08/17/2017\t1.19\t749.14",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810QV3\t21,180,000.00\t23,014,735.91\t08/17/2017\t1.19\t760.76",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810PS1\t16,080,000.00\t23,200,519.09\t08/17/2017\t1.19\t766.91",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828N71\t22,760,000.00\t23,588,855.13\t08/17/2017\t1.19\t779.74",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810PZ5\t16,830,000.00\t23,629,705.33\t08/17/2017\t1.19\t781.09",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828S50\t24,020,000.00\t23,706,311.06\t08/17/2017\t1.19\t783.63",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828S50\t24,590,000.00\t23,747,292.45\t08/17/2017\t1.19\t784.98",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828WU0\t24,140,000.00\t24,320,862.60\t08/17/2017\t1.19\t803.94",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828V49\t25,150,000.00\t24,603,393.67\t08/17/2017\t1.19\t813.28",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828XL9\t24,110,000.00\t24,764,143.66\t08/17/2017\t1.19\t818.59",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828MF4\t21,420,000.00\t24,884,681.72\t08/17/2017\t1.19\t822.58",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828QV5\t22,700,000.00\t24,948,968.26\t08/17/2017\t1.19\t824.70",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828H45\t24,740,000.00\t25,015,942.14\t08/17/2017\t1.19\t826.92",
			"Evergreen (Open)\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t7\t912828V49\t24,401,000.00\t23,973,168.77\t08/17/2017\t1.26\t839.06",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828TE0\t24,190,000.00\t25,420,333.82\t08/17/2017\t1.19\t840.28",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828C99\t25,730,000.00\t26,354,266.51\t08/17/2017\t1.19\t871.15",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828SA9\t24,770,000.00\t26,405,967.25\t08/17/2017\t1.19\t872.86",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828B25\t25,380,000.00\t26,830,133.67\t08/17/2017\t1.19\t886.88",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828Q60\t26,560,000.00\t27,005,122.39\t08/17/2017\t1.19\t892.67",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828Q60\t27,150,000.00\t27,623,437.00\t08/17/2017\t1.19\t913.11",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810FS2\t19,960,000.00\t28,016,565.41\t08/17/2017\t1.19\t926.10",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RL4\t26,960,000.00\t28,140,631.75\t08/17/2017\t1.19\t930.20",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828UH1\t28,360,000.00\t29,431,195.38\t08/17/2017\t1.19\t972.86",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912810RF7\t24,400,000.00\t29,611,901.67\t08/17/2017\t1.19\t978.84",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828VM9\t28,320,000.00\t29,811,259.49\t08/17/2017\t1.19\t985.43",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828X39\t32,000,000.00\t31,472,568.86\t08/17/2017\t1.19\t1,040.34",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828K33\t30,900,000.00\t31,718,702.35\t08/17/2017\t1.19\t1,048.48",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828Q60\t31,410,000.00\t31,813,098.97\t08/17/2017\t1.19\t1,051.60",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828C99\t31,120,000.00\t31,884,291.71\t08/17/2017\t1.19\t1,053.95",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828XL9\t32,190,000.00\t33,156,702.56\t08/17/2017\t1.19\t1,096.01",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828NM8\t32,690,000.00\t37,888,450.69\t08/17/2017\t1.19\t1,252.42",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828PP9\t37,510,000.00\t43,161,439.25\t08/17/2017\t1.19\t1,426.73",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828N71\t43,450,000.00\t45,533,776.74\t08/17/2017\t1.19\t1,505.14",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828WU0\t49,180,000.00\t49,673,888.28\t08/17/2017\t1.19\t1,642.00",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828H45\t48,940,000.00\t49,765,511.29\t08/17/2017\t1.19\t1,645.03",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828TE0\t48,550,000.00\t51,063,177.24\t08/17/2017\t1.19\t1,687.92",
			"Open\t578509: SWIB - TIPS\tBank of New York Mellon\t1\t\t912828B25\t48,120,000.00\t51,202,599.48\t08/17/2017\t1.19\t1,692.53"
	};

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public LendingRepoInterestRateService getLendingRepoInterestRateService() {
		return this.lendingRepoInterestRateService;
	}


	public void setLendingRepoInterestRateService(LendingRepoInterestRateService lendingRepoInterestRateService) {
		this.lendingRepoInterestRateService = lendingRepoInterestRateService;
	}
}
