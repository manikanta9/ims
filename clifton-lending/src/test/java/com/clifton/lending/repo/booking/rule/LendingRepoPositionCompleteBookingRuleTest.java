package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoTestObjectFactory;
import com.clifton.lending.repo.builder.LendingRepoBuilder;
import com.clifton.lending.repo.builder.LendingRepoBuilder.SecurityCategories;
import com.clifton.marketdata.MarketDataTestObjectFactory;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class LendingRepoPositionCompleteBookingRuleTest {

	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoOpening() {
		testRepoDetailPositionCompleteBookingRuleRepo(true, false,
				"-11	11696	123456	123456	Position	912810FH6	156.06	-500,000	-1,074,660.37	02/01/2018	1	-1,074,660.37	-1,074,660.37	12/30/2011	02/01/2018	Position close from REPO opening for 912810FH6",
				"-12	null	123456	654321	Position	912810FH6	156.06	500,000	1,074,660.37	02/01/2018	1	1,074,660.37	1,074,660.37	12/30/2011	02/01/2018	Position opening from REPO opening for 912810FH6",
				"-13	-11	123456	123456	Cash	USD			680,572.8655	02/01/2018	1	680,572.87	0	02/01/2018	02/01/2018	Cash proceeds from REPO opening for 912810FH6",
				"-14	-12	123456	654321	Cash	USD			-680,572.8655	02/01/2018	1	-680,572.87	0	02/01/2018	02/01/2018	Cash expense from REPO opening for 912810FH6"
		);
	}


	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoMaturity() {
		testRepoDetailPositionCompleteBookingRuleRepo(false, false,
				"-10	11696	123456	654321	Position	912810FH6	156.06	-500,000	-1,074,660.37	02/05/2018	1	-1,074,660.37	-1,074,660.37	12/30/2011	02/06/2018	Position close from REPO maturity for 912810FH6",
				"-11	null	123456	123456	Position	912810FH6	156.06	500,000	1,074,660.37	02/05/2018	1	1,074,660.37	1,074,660.37	12/30/2011	02/06/2018	Position opening from REPO maturity for 912810FH6",
				"-12	-10	123456	654321	Cash	USD			680,572.8655	02/05/2018	1	680,572.87	0	02/05/2018	02/06/2018	Cash proceeds from REPO maturity for 912810FH6",
				"-13	-11	123456	123456	Cash	USD			-680,572.8655	02/05/2018	1	-680,572.87	0	02/05/2018	02/06/2018	Cash expense from REPO maturity for 912810FH6",
				"-14	-11	123456	123456	Cash	USD			-37.81	02/05/2018	1	-37.81	0	02/05/2018	02/06/2018	REPO Interest Expense from REPO maturity for 912810FH6",
				"-15	-11	123456	123456	REPO Interest Expense	912810FH6			37.81	02/05/2018	1	37.81	0	02/05/2018	02/06/2018	REPO Interest Expense from REPO maturity for 912810FH6."
		);
	}


	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoOpeningMultipleLots() {
		testRepoDetailPositionCompleteBookingRuleRepo(true, true,
				"-11	11697	123456	123456	Position	912810FH6	156.06	-250,000	-2,157,918.03	02/01/2018	1	-2,157,918.03	-2,157,918.03	12/30/2011	02/01/2018	Position close from REPO opening for 912810FH6",
				"-12	11696	123456	123456	Position	912810FH6	156.06	-250,000	-2,157,918.03	02/01/2018	1	-2,157,918.03	-2,157,918.03	12/30/2011	02/01/2018	Position close from REPO opening for 912810FH6",
				"-13	null	123456	654321	Position	912810FH6	156.06	250,000	2,157,918.03	02/01/2018	1	2,157,918.03	2,157,918.03	12/30/2011	02/01/2018	Position opening from REPO opening for 912810FH6",
				"-14	null	123456	654321	Position	912810FH6	156.06	250,000	2,157,918.03	02/01/2018	1	2,157,918.03	2,157,918.03	12/30/2011	02/01/2018	Position opening from REPO opening for 912810FH6",
				"-15	-11	123456	123456	Cash	USD			680,572.8655	02/01/2018	1	680,572.87	0	02/01/2018	02/01/2018	Cash proceeds from REPO opening for 912810FH6",
				"-16	-13	123456	654321	Cash	USD			-680,572.8655	02/01/2018	1	-680,572.87	0	02/01/2018	02/01/2018	Cash expense from REPO opening for 912810FH6"
		);
	}


	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoMaturityMultipleLots() {
		testRepoDetailPositionCompleteBookingRuleRepo(false, true,
				"-10	11696	123456	654321	Position	912810FH6	156.06	-250,000	-2,157,918.03	02/05/2018	1	-2,157,918.03	-2,157,918.03	12/30/2011	02/06/2018	Position close from REPO maturity for 912810FH6",
				"-11	11697	123456	654321	Position	912810FH6	156.06	-250,000	-2,157,918.03	02/05/2018	1	-2,157,918.03	-2,157,918.03	12/30/2011	02/06/2018	Position close from REPO maturity for 912810FH6",
				"-12	null	123456	123456	Position	912810FH6	156.06	250,000	2,157,918.03	02/05/2018	1	2,157,918.03	2,157,918.03	12/30/2011	02/06/2018	Position opening from REPO maturity for 912810FH6",
				"-13	null	123456	123456	Position	912810FH6	156.06	250,000	2,157,918.03	02/05/2018	1	2,157,918.03	2,157,918.03	12/30/2011	02/06/2018	Position opening from REPO maturity for 912810FH6",
				"-14	-10	123456	654321	Cash	USD			680,572.8655	02/05/2018	1	680,572.87	0	02/05/2018	02/06/2018	Cash proceeds from REPO maturity for 912810FH6",
				"-15	-12	123456	123456	Cash	USD			-680,572.8655	02/05/2018	1	-680,572.87	0	02/05/2018	02/06/2018	Cash expense from REPO maturity for 912810FH6",
				"-16	-12	123456	123456	Cash	USD			-37.81	02/05/2018	1	-37.81	0	02/05/2018	02/06/2018	REPO Interest Expense from REPO maturity for 912810FH6",
				"-17	-12	123456	123456	REPO Interest Expense	912810FH6			37.81	02/05/2018	1	37.81	0	02/05/2018	02/06/2018	REPO Interest Expense from REPO maturity for 912810FH6.");
	}


	private void testRepoDetailPositionCompleteBookingRuleRepo(boolean open, boolean multipleLots, String... expectedFormattedDetails) {
		LendingRepo repo = LendingRepoBuilder.createRepo()
				.notReverse()
				.ofType("Term")
				.addInterestRate(new BigDecimal("1.5"))
				.addTerm(1)
				.forSecurity("912810FH6", "912810FH6", 1, SecurityCategories.Bonds, new BigDecimal("0.01"), LendingRepoBuilder.getUSD(), new BigDecimal("10.32423"))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoData(new BigDecimal("1.37634"), new BigDecimal(500000), new BigDecimal(103), new BigDecimal("5.0"), new BigDecimal("716392.49"), new BigDecimal("37.81"))
				.addExchangeRateToBase(BigDecimal.ONE)
				.toRepo();

		repo.setTradeDate(DateUtils.toDate("02/01/2018"));
		repo.setSettlementDate(DateUtils.toDate("02/01/2018"));
		if (!open) {
			repo.setMaturityDate(DateUtils.toDate("02/05/2018"));
			repo.setMaturitySettlementDate(DateUtils.toDate("02/06/2018"));
		}

		AccountingBookingRuleTestExecutor.newTestForEntity(repo)
				.forBookingRules(getRuleList(repo, open, multipleLots))
				.withExpectedResults(expectedFormattedDetails)
				.execute();
	}


	private List<AccountingBookingRule<LendingRepo>> getRuleList(LendingRepo repo, boolean open, boolean multipleLots) {
		List<AccountingTransaction> existingTransactions = new ArrayList<>();
		if (multipleLots) {
			existingTransactions.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(250000).price("156.06").costBasis(MathUtils.round(new BigDecimal("2157918.03"), 2)).on("12/30/2011").build());
			existingTransactions.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11697L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(250000).price("156.06").costBasis(MathUtils.round(new BigDecimal("2157918.03"), 2)).on("12/30/2011").build());
		}
		else {
			if (open) {
				existingTransactions.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, repo.getRepoDefinition().getInvestmentSecurity())
						.qty(1004000).price("156.06").costBasis(MathUtils.round(new BigDecimal("2157918.03"), 2)).on("12/30/2011").build());
			}
			else {
				existingTransactions.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, repo.getRepoDefinition().getInvestmentSecurity())
						.qty(500000).price("156.06").costBasis(MathUtils.round(new BigDecimal("1074660.37"), 2)).on("12/30/2011").build());
			}
		}

		return getRuleListForTransactions(repo, open, existingTransactions);
	}


	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoOpeningForeign() {
		testRepoDetailPositionCompleteBookingRuleRepoForeign(true,
				"-11	11696	123456	123456	Position	912810FH6	156.06	-500,000	-1,074,660.37	02/01/2018	1	-1,074,660.37	-1,074,660.37	12/30/2011	02/01/2018	Position close from REPO opening for 912810FH6",
				"-12	null	123456	654321	Position	912810FH6	156.06	500,000	1,074,660.37	02/01/2018	1	1,074,660.37	1,074,660.37	12/30/2011	02/01/2018	Position opening from REPO opening for 912810FH6",
				"-13	-11	123456	123456	Currency	USD			680,572.8655	02/01/2018	1.5	1,020,859.3	0	02/01/2018	02/01/2018	Currency proceeds from REPO opening for 912810FH6",
				"-14	-12	123456	654321	Currency	USD			-680,572.8655	02/01/2018	1.5	-1,020,859.3	0	02/01/2018	02/01/2018	Currency expense from REPO opening for 912810FH6"
		);
	}


	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoClosingForeign() {
		testRepoDetailPositionCompleteBookingRuleRepoForeign(false,
				"-10	11696	123456	654321	Position	912810FH6	156.06	-500,000	-1,074,660.37	02/05/2018	1	-1,074,660.37	-1,074,660.37	12/30/2011	02/06/2018	Position close from REPO maturity for 912810FH6",
				"-11	null	123456	123456	Position	912810FH6	156.06	500,000	1,074,660.37	02/05/2018	1	1,074,660.37	1,074,660.37	12/30/2011	02/06/2018	Position opening from REPO maturity for 912810FH6",
				"-12	-10	123456	654321	Currency	USD			680,572.8655	02/05/2018	1.5	1,020,859.3	0	02/05/2018	02/06/2018	Currency proceeds from REPO maturity for 912810FH6",
				"-13	-11	123456	123456	Currency	USD			-680,572.8655	02/05/2018	1.5	-1,020,859.3	0	02/05/2018	02/06/2018	Currency expense from REPO maturity for 912810FH6",
				"-14	-11	123456	123456	Currency	USD			-37.81	02/05/2018	1.5	-56.72	0	02/05/2018	02/06/2018	REPO Interest Expense from REPO maturity for 912810FH6",
				"-15	-11	123456	123456	REPO Interest Expense	912810FH6			37.81	02/05/2018	1.5	56.72	0	02/05/2018	02/06/2018	REPO Interest Expense from REPO maturity for 912810FH6."
		);
	}


	private void testRepoDetailPositionCompleteBookingRuleRepoForeign(boolean open, String... expectedFormattedDetails) {
		LendingRepo repo = LendingRepoBuilder.createRepo()
				.notReverse()
				.ofType("Term")
				.addInterestRate(new BigDecimal("1.5"))
				.addTerm(1)
				.forSecurity("912810FH6", "912810FH6", 1, SecurityCategories.Bonds, new BigDecimal("0.01"), LendingRepoBuilder.getUSD(), new BigDecimal("10.32423"))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getYEN())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoData(new BigDecimal("1.37634"), new BigDecimal(500000), new BigDecimal(103), new BigDecimal("5.0"), new BigDecimal("716392.49"), new BigDecimal("37.81"))
				.addExchangeRateToBase(new BigDecimal("1.5"))
				.toRepo();

		repo.setTradeDate(DateUtils.toDate("02/01/2018"));
		repo.setSettlementDate(DateUtils.toDate("02/01/2018"));
		if (!open) {
			repo.setMaturityDate(DateUtils.toDate("02/05/2018"));
			repo.setMaturitySettlementDate(DateUtils.toDate("02/06/2018"));
		}

		AccountingBookingRuleTestExecutor.newTestForEntity(repo)
				.forBookingRules(getRuleListForeign(repo, open))
				.withExpectedResults(expectedFormattedDetails)
				.execute();
	}


	private List<AccountingBookingRule<LendingRepo>> getRuleListForeign(LendingRepo repo, boolean open) {
		AccountingTransaction tran;
		if (open) {
			tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(1004000).price("156.06").costBasis("2157918.03").on("12/30/2011").build();
		}
		else {
			tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(500000).price("156.06").costBasis("1074660.37").on("12/30/2011").build();
		}
		return getRuleListForTransactions(repo, open, CollectionUtils.createList(tran));
	}


	private List<AccountingBookingRule<LendingRepo>> getRuleListForTransactions(LendingRepo repo, boolean open, List<AccountingTransaction> existingTransactions) {
		AccountingPositionSplitterBookingRule<LendingRepo> splitterRuleLIFO = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule();
		splitterRuleLIFO.setClosingOrder(AccountingPositionOrders.LIFO);
		LendingRepoPositionCloseBookingRule repoCloseBookingRule = LendingRepoTestObjectFactory.newLendingRepoPositionCloseBookingRule(repo, open, splitterRuleLIFO, existingTransactions);

		List<AccountingBookingRule<LendingRepo>> ruleList = new ArrayList<>();
		ruleList.add(repoCloseBookingRule);
		ruleList.add(splitterRuleLIFO);
		ruleList.add(AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule());
		ruleList.add(AccountingTestObjectFactory.newRemoveJournalDetailsBookingRule(AccountingAccount.REVENUE_REALIZED, AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS));
		ruleList.add(AccountingTestObjectFactory.newCloseAtCostBookingRule());

		ruleList.add(LendingRepoTestObjectFactory.newLendingRepoPositionOpenBookingRule(open));
		ruleList.add(splitterRuleLIFO);

		if (!open) {
			ruleList.add(AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule());
			ruleList.add(AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule());
		}

		LendingRepoCashOrCurrencyBookingRule repoCashOrCurrencyBookingRule = LendingRepoTestObjectFactory.newLendingRepoCashOrCurrencyBookingRule(open);
		repoCashOrCurrencyBookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService(LendingRepoBuilder.getUSD(), LendingRepoBuilder.getYEN(), new BigDecimal("1.5")));

		ruleList.add(repoCashOrCurrencyBookingRule);
		return ruleList;
	}
}
