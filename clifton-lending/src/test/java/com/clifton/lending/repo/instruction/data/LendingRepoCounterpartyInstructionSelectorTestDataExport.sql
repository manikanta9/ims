SELECT
	'LendingRepo' AS entityTableName,
	LendingRepoID AS entityId
FROM LendingRepo
WHERE LendingRepoID >= 1350 AND LendingRepoID <= 1390
UNION
SELECT
	'LendingRepoPairOff' AS entityTableName,
	LendingRepoPairOffID AS entityId
FROM LendingRepoPairOff WHERE PairOffDate=convert(datetime, '2016-02-11');
