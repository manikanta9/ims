package com.clifton.lending.repo;


import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.calculator.DayCountConventions;
import com.clifton.lending.repo.builder.LendingRepoBuilder;
import com.clifton.lending.repo.interest.LendingRepoInterestRate;
import com.clifton.lending.repo.interest.LendingRepoInterestRateService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class LendingRepoServiceImplSaveTests {

	@Resource
	private LendingRepoService lendingRepoService;

	@Resource
	private LendingRepoInterestRateService lendingRepoInterestRateService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveLendingRepo_OvernightWrongMaturityDate() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Overnight")
				.addTerm(2)
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		TestUtils.expectException(FieldValidationException.class, () -> this.lendingRepoService.saveLendingRepo(repo), "The maturity date for repos of type [Overnight] must be the business day on or immediately following the required term length of [1] days ([10/02/2012]).");
	}


	@Test
	public void testSaveLendingRepo_OpenWithTerm() {
		// Repo types which normally do not have a predetermined term may still have a term defined regardless
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Open")
				.addTerm(2)
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		repo = this.lendingRepoService.getLendingRepo(repo.getId());
		Assertions.assertEquals(1, repo.getTerm()); // Open repo term is always one
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertNull(repo.getMaturityDate());
		Assertions.assertNull(repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());
	}


	@Test
	public void testSaveLendingRepo_OpenNoMaturitySettlementDate() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		TestUtils.expectException(FieldValidationException.class, () -> this.lendingRepoService.saveLendingRepo(repo), "Maturity date is required for REPO's of type [Term].");
	}


	@Test
	public void testSaveLendingRepo_ExistingRepoNewDefinition() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Open")
				.addTerm(1)
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		repo.getRepoDefinition().setId(null);
		TestUtils.expectException(FieldValidationException.class, () -> this.lendingRepoService.saveLendingRepo(repo), "Modifications to the given fields are not allowed: [repoDefinition]");
	}


	@Test
	public void testSaveLendingRepo() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.forDates("10/01/2012", "10/01/2012", null, "10/02/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(1, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());
	}


	@Test
	public void testSaveLendingRepoSaveDefinition() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.forDates("10/01/2012", "10/01/2012", null, "10/02/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(1, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		// Validate repo definition re-saved with new properties (non-roll repo)
		String originalDayCountConvention = repo.getRepoDefinition().getDayCountConvention();
		repo.getRepoDefinition().setDayCountConvention(DayCountConventions.ACTUAL_ACTUAL.getAccrualString());
		this.lendingRepoService.saveLendingRepo(repo);
		LendingRepo newRepo = this.lendingRepoService.getLendingRepo(repo.getId());
		Assertions.assertNotEquals(originalDayCountConvention, newRepo.getRepoDefinition().getDayCountConvention());
		Assertions.assertEquals(DayCountConventions.ACTUAL_ACTUAL.getAccrualString(), newRepo.getRepoDefinition().getDayCountConvention());
		Assertions.assertEquals(1, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("57.14"), repo.getInterestAmount()); // Interest amount is calculated with ACTUAL_ACTUAL, or 1/366 (leap year)
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());
	}


	@Test
	public void testSaveLendingRepoSaveRoll() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.forDates("10/01/2012", "10/01/2012", null, "10/02/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		Assertions.assertEquals(1, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		LendingRepo repo2 = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.forDates("10/01/2012", "10/01/2012", null, "10/02/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		repo2.setRepoDefinition(repo.getRepoDefinition());
		repo2.setParentIdentifier(repo.getId());
		this.lendingRepoService.saveLendingRepo(repo2);
		Assertions.assertEquals(1, repo2.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo2.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo2.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo2.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), repo2.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo2.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo2.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo2.getInterestAmount());
		Assertions.assertNull(repo2.getConfirmedDate());
		Assertions.assertNull(repo2.getConfirmationNote());
		Assertions.assertEquals(repo.getId(), repo2.getParentIdentifier());

		// Retain original definition values
		repo2.getRepoDefinition().setDayCountConvention(DayCountConventions.ACTUAL_ACTUAL.getAccrualString());
		this.lendingRepoService.saveLendingRepo(repo2);
		LendingRepo newRepo = this.lendingRepoService.getLendingRepo(repo2.getId());
		Assertions.assertNotEquals(DayCountConventions.ACTUAL_ACTUAL.getAccrualString(), newRepo.getRepoDefinition().getDayCountConvention());
		Assertions.assertEquals(DayCountConventions.ACTUAL_THREESIXTY.getAccrualString(), newRepo.getRepoDefinition().getDayCountConvention());
		Assertions.assertEquals(1, newRepo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), newRepo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), newRepo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), newRepo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/02/2012"), newRepo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), newRepo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), newRepo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), newRepo.getInterestAmount());
		Assertions.assertNull(newRepo.getConfirmedDate());
		Assertions.assertNull(newRepo.getConfirmationNote());
		Assertions.assertEquals(repo.getId(), newRepo.getParentIdentifier());
	}


	@Test
	public void testSaveLendingRepoOpen() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Open")
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		repo = this.lendingRepoService.getLendingRepo(repo.getId());
		Assertions.assertEquals(1, repo.getTerm()); // Open repo term is always one
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertNull(repo.getMaturityDate());
		Assertions.assertNull(repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		List<LendingRepoInterestRate> rates = this.lendingRepoInterestRateService.getLendingRepoInterestRateByRepo(repo.getId());
		Assertions.assertEquals(1, rates.size());
		LendingRepoInterestRate rate = CollectionUtils.getOnlyElementStrict(rates);
		Assertions.assertEquals(new BigDecimal("58.09"), rate.getInterestAmount()); // Single-day interest amount for repos with no term
		Assertions.assertEquals(new BigDecimal("1.5"), rate.getInterestRate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), rate.getInterestRateDate());
		Assertions.assertEquals(repo, rate.getRepo());
	}


	@Test
	public void testSaveLendingRepoOpenWithTerm() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Open")
				.addTerm(3)
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		repo = this.lendingRepoService.getLendingRepo(repo.getId());
		Assertions.assertEquals(1, repo.getTerm()); // Open repo term is always one
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertNull(repo.getMaturityDate());
		Assertions.assertNull(repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("58.09"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		List<LendingRepoInterestRate> rates = this.lendingRepoInterestRateService.getLendingRepoInterestRateByRepo(repo.getId());
		Assertions.assertEquals(1, rates.size());
		LendingRepoInterestRate rate = CollectionUtils.getOnlyElementStrict(rates);
		Assertions.assertEquals(new BigDecimal("58.09"), rate.getInterestAmount());
		Assertions.assertEquals(new BigDecimal("1.5"), rate.getInterestRate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), rate.getInterestRateDate());
		Assertions.assertEquals(repo, rate.getRepo());
	}


	@Test
	public void testSaveLendingRepoTerm() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.addTerm(4)
				.forDates("10/01/2012", "10/01/2012", "10/05/2012", "10/05/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		repo = this.lendingRepoService.getLendingRepo(repo.getId());
		Assertions.assertEquals(4, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("232.38"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		List<LendingRepoInterestRate> rates = this.lendingRepoInterestRateService.getLendingRepoInterestRateByRepo(repo.getId());
		Assertions.assertEquals(0, CollectionUtils.getSize(rates));
		Assertions.assertEquals(new BigDecimal("232.38"), repo.getInterestAmount());
	}


	@Test
	public void testSaveLendingRepoInferredTerm() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.addTerm(4)
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("98"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		repo = this.lendingRepoService.getLendingRepo(repo.getId());
		Assertions.assertEquals(4, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/05/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394254.21"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("232.38"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		List<LendingRepoInterestRate> rates = this.lendingRepoInterestRateService.getLendingRepoInterestRateByRepo(repo.getId());
		Assertions.assertEquals(0, CollectionUtils.getSize(rates));
	}


	@Test
	public void testSaveLendingRepoInferredTermWeekend() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.addTerm(5)
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("102"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		repo = this.lendingRepoService.getLendingRepo(repo.getId());
		Assertions.assertEquals(8, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394812.14"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("464.94"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		List<LendingRepoInterestRate> rates = this.lendingRepoInterestRateService.getLendingRepoInterestRateByRepo(repo.getId());
		Assertions.assertEquals(0, CollectionUtils.getSize(rates));
	}


	@Test
	public void testSaveLendingRepoInferredTermWeekendPlusHoliday() {
		LendingRepo repo = LendingRepoBuilder.createNewRepo()
				.ofType("Term")
				.addTerm(6)
				.forDates("10/01/2012", "10/01/2012", null, null)
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity(this.investmentInstrumentService.getInvestmentSecurity(1))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoDataForSave(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("102"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();
		this.lendingRepoService.saveLendingRepo(repo);
		repo = this.lendingRepoService.getLendingRepo(repo.getId());
		Assertions.assertEquals(8, repo.getTerm());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getTradeDate());
		Assertions.assertEquals(DateUtils.toDate("10/01/2012"), repo.getSettlementDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), repo.getMaturityDate());
		Assertions.assertEquals(DateUtils.toDate("10/09/2012"), repo.getMaturitySettlementDate());
		Assertions.assertEquals(new BigDecimal("1422708.38"), repo.getMarketValue());
		Assertions.assertEquals(new BigDecimal("1394812.14"), repo.getNetCash());
		Assertions.assertEquals(new BigDecimal("464.94"), repo.getInterestAmount());
		Assertions.assertNull(repo.getConfirmedDate());
		Assertions.assertNull(repo.getConfirmationNote());
		Assertions.assertNull(repo.getParentIdentifier());

		List<LendingRepoInterestRate> rates = this.lendingRepoInterestRateService.getLendingRepoInterestRateByRepo(repo.getId());
		Assertions.assertEquals(0, CollectionUtils.getSize(rates));
	}
}
