SELECT 'LendingRepoInterestRate' AS entityTableName, LendingRepoInterestRateID AS entityId FROM LendingRepoInterestRate
	WHERE LendingRepoInterestRateID IN (
		SELECT LendingRepoInterestRateID FROM LendingRepoInterestRate
			WHERE LendingRepoID IN ( SELECT LendingRepoID From LendingRepo WHERE LendingRepoDefinitionID IN (SELECT LendingRepoDefinitionID FROM LendingRepoDefinition WHERE RepoInvestmentAccountID IN (SELECT HoldingInvestmentAccountID From CollateralBalance WHERE CollateralBalanceID = 1014654)))
  				AND RepoInterestRateDate = '08/17/2017')
UNION
SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId FROM CalendarDay WHERE CalendarYearID = (Select CalendarYearID = 2017)
UNION
SELECT 'CalendarHolidayDay' AS entityTableName, CalendarHolidayDayID AS entityId FROM CalendarHolidayDay WHERE CalendarDayID IN (SELECT CalendarDayID FROM CalendarDay WHERE CalendarYearID = (Select CalendarYearID = 2017));
