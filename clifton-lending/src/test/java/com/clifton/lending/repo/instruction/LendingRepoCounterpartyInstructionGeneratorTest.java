package com.clifton.lending.repo.instruction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.beans.RepoRateTypes;
import com.clifton.instruction.messages.trade.DeliverAgainstPayment;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.LendingRepoType;
import com.clifton.lending.repo.LendingRepoTypes;
import com.clifton.lending.repo.builder.LendingRepoBuilder;
import com.clifton.lending.repo.pairoff.LendingRepoPairOff;
import com.clifton.lending.repo.pairoff.LendingRepoPairOffService;
import com.clifton.system.schema.SystemTableBuilder;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;


/**
 * <code>LendingRepoCounterpartyInstructionGeneratorTest</code>
 */
public class LendingRepoCounterpartyInstructionGeneratorTest<I extends Instruction, E extends IdentityObject> {

	private static final BusinessCompany PPA_MPLS;


	static {
		PPA_MPLS = new BusinessCompany();
		PPA_MPLS.setId(1);
		PPA_MPLS.setName(BusinessCompanyUtilHandler.PPA_MINNEAPOLIS_COMPANY_NAME);
		PPA_MPLS.setBusinessIdentifierCode("PPSCUS66");
	}


	@Mock
	public InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;
	@Mock
	private ReadOnlyDAO<LendingRepo> dao;

	@Mock
	private DaoLocator daoLocator;

	@Mock
	private InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler;

	@Mock
	private BusinessCompanyUtilHandler businessCompanyUtilHandler;

	@Mock
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	@Mock
	private LendingRepoPairOffService lendingRepoPairOffService;

	@Mock
	private LendingRepoService lendingRepoService;

	@Mock
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	@Mock
	private BusinessCompanyService businessCompanyService;


	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);

		Mockito.when(this.daoLocator.<LendingRepo>locate("LendingRepo")).thenReturn(this.dao);
		Mockito.when(this.businessCompanyUtilHandler.getParametricBusinessIdentifierCode()).thenReturn(PPA_MPLS.getBusinessIdentifierCode());
		LendingRepoType lendingRepoType = new LendingRepoType();
		lendingRepoType.setId(8);
		lendingRepoType.setName(LendingRepoTypes.OPEN.getTypeName());
		Mockito.when(this.lendingRepoService.getLendingRepoTypeByName(LendingRepoTypes.OPEN.getTypeName())).thenReturn(lendingRepoType);
		LendingRepoPairOff lendingRepoPairOff = new LendingRepoPairOff();
		lendingRepoPairOff.setId(1);
		Mockito.when(this.lendingRepoPairOffService.getLendingRepoPairOffList(ArgumentMatchers.any())).thenReturn(Collections.singletonList(lendingRepoPairOff));
		Mockito.when(this.businessCompanyService.getBusinessCompanyByTypeAndName("Custodian", "Bank of New York Mellon"))
				.thenReturn(BusinessCompanyBuilder.createBankOfNewYorkMellon().toBusinessCompany());
	}


	@Test
	public void testDeliverAgainstPaymentFuture() {
		LendingRepo lendingRepo = LendingRepoBuilder.createRepo(42)
				.ofType("Term")
				.forDates("10/01/2012", "10/02/2012", "10/04/2012", "10/05/2012")
				.notReverse()
				.addInterestRate(new BigDecimal("1.5"))
				.forSecurity("FR0010235176", "FR0010235176", 1, LendingRepoBuilder.SecurityCategories.Bonds, new BigDecimal("0.01"), LendingRepoBuilder.getEUR(), new BigDecimal("103.4697"))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoData(new BigDecimal("1.11062"), new BigDecimal("1375000.00"), new BigDecimal("103.4697"), new BigDecimal("0.2"), new BigDecimal("1591562.51"), new BigDecimal("75.79"))
				.addExchangeRateToBase(new BigDecimal("1.3143"))
				.toRepo();

		lendingRepo.setOriginalFace(new BigDecimal("33333.33"));
		BusinessCompany issuingCompany = new BusinessCompany();
		issuingCompany.setBusinessIdentifierCode("Place of Settlement");
		issuingCompany.setDtcNumber("DTC Number");
		lendingRepo.getInvestmentSecurity().setIsin("ISIN Number");

		BusinessCompany counterparty = new BusinessCompany();
		counterparty.setBusinessIdentifierCode("COUNTER");
		counterparty.setDtcNumber("COUNTER");
		lendingRepo.getRepoDefinition().setCounterparty(counterparty);
		lendingRepo.getRepoDefinition().setReverse(true);

		BusinessCompany deliveryCompany = new BusinessCompany();
		deliveryCompany.setBusinessIdentifierCode("12345");
		InvestmentAccount repoAccount = new InvestmentAccount();
		repoAccount.setIssuingCompany(deliveryCompany);
		lendingRepo.getRepoDefinition().setRepoInvestmentAccount(repoAccount);

		InvestmentSecurity investmentSecurity = lendingRepo.getInvestmentSecurity();
		investmentSecurity.setEndDate(DateUtils.toDate("10/10/2020"));
		investmentSecurity.setStartDate(DateUtils.toDate("1/10/2010"));
		lendingRepo.getRepoDefinition().setInvestmentSecurity(investmentSecurity);

		InvestmentAccount investmentAccount = InvestmentAccountBuilder.createTestAccount1().toInvestmentAccount();
		investmentAccount.getIssuingCompany().setBusinessIdentifierCode("Business Identifier Code");
		Mockito.when(this.dao.findByPrimaryKey(42)).thenReturn(lendingRepo);
		Mockito.when(this.investmentAccountRelationshipService.
				getInvestmentAccountRelatedForPurposeStrict(
						510,
						510,
						1,
						InvestmentAccountRelationshipPurpose.CUSTODIAN_ACCOUNT_PURPOSE_NAME,
						DateUtils.asUtilDate(LocalDate.of(2012, 10, 1)))
		).thenReturn(investmentAccount);

		InvestmentInstructionDeliveryType investmentInstructionDeliveryType = new InvestmentInstructionDeliveryType();
		InvestmentInstructionDefinition investmentInstructionDefinition = new InvestmentInstructionDefinition();
		investmentInstructionDefinition.setDeliveryType(investmentInstructionDeliveryType);
		InvestmentInstructionCategory investmentInstructionCategory = new InvestmentInstructionCategory();
		investmentInstructionCategory.setTable(SystemTableBuilder.createLendingRepo().toSystemTable());
		investmentInstructionDefinition.setCategory(investmentInstructionCategory);
		InvestmentInstruction investmentInstruction = new InvestmentInstruction();
		investmentInstruction.setInstructionDate(new Date());
		investmentInstruction.setDefinition(investmentInstructionDefinition);
		InvestmentInstructionItem investmentInstructionItem = new InvestmentInstructionItem();
		investmentInstructionItem.setId(111);
		investmentInstructionItem.setFkFieldId(lendingRepo.getId());
		investmentInstructionItem.setInstruction(investmentInstruction);

		InvestmentInstructionDelivery investmentInstructionDelivery = new InvestmentInstructionDelivery();
		investmentInstructionDelivery.setDeliveryAccountNumber("Clearing Account Number");
		investmentInstructionDelivery.setDeliveryABA("Delivery ABA");
		investmentInstructionDelivery.setDeliveryForFurtherCredit("Delivery for Further Credit");
		Mockito.when(this.investmentInstructionDeliveryUtilHandler.getInvestmentInstructionDelivery(
				ArgumentMatchers.same(investmentInstructionItem),
				ArgumentMatchers.same(deliveryCompany),
				ArgumentMatchers.same(lendingRepo.getSettlementCurrency())
		)).thenReturn(investmentInstructionDelivery);
		Mockito.when(this.instructionUtilHandler.getTransactionReferenceNumber(ArgumentMatchers.any())).thenReturn("IMS" + investmentInstructionItem.getId() + "II;" + "IMS" + lendingRepo.getId() + "LR");
		Mockito.when(this.investmentSecurityUtilHandler.getInterestRate(ArgumentMatchers.any(InvestmentSecurity.class))).thenReturn(new BigDecimal("2.22"));
		Mockito.when(this.investmentSecurityUtilHandler.getCouponType(ArgumentMatchers.any(InvestmentSecurity.class))).thenReturn("Fixed");
		Mockito.when(this.investmentSecurityUtilHandler.getISITCCodeStrict(ArgumentMatchers.any(InvestmentInstrument.class))).thenReturn(ISITCCodes.TIPS);

		LendingRepoCounterpartyInstructionGenerator<I> converter = new LendingRepoCounterpartyInstructionGenerator<>();
		converter.setDaoLocator(this.daoLocator);
		converter.setInstructionUtilHandler(this.instructionUtilHandler);
		converter.setBusinessCompanyUtilHandler(this.businessCompanyUtilHandler);
		converter.setInvestmentAccountRelationshipService(this.investmentAccountRelationshipService);
		converter.setLendingRepoPairOffService(this.lendingRepoPairOffService);
		converter.setInvestmentSecurityUtilHandler(this.investmentSecurityUtilHandler);
		converter.setInvestmentInstructionDeliveryUtilHandler(this.investmentInstructionDeliveryUtilHandler);
		converter.setBusinessCompanyService(this.businessCompanyService);

		InstructionMessage message = converter.generateInstructionMessageList(investmentInstructionItem).get(0);

		Assertions.assertTrue(message instanceof DeliverAgainstPayment);
		DeliverAgainstPayment deliverAgainstPayment = (DeliverAgainstPayment) message;

		Assertions.assertEquals(lendingRepo.isBuy(), deliverAgainstPayment.isBuy());
		Assertions.assertTrue(deliverAgainstPayment.isRepurchaseAgreement());
		Assertions.assertEquals("IMS" + 111 + "II;" + "IMS" + lendingRepo.getId() + "LR", deliverAgainstPayment.getTransactionReferenceNumber());

		Assertions.assertEquals("Clearing Account Number", deliverAgainstPayment.getClearingSafekeepingAccount());
		Assertions.assertEquals(new PartyIdentifier("Delivery ABA", MessagePartyIdentifierTypes.USFW), deliverAgainstPayment.getClearingBrokerIdentifier());
		Assertions.assertEquals("Delivery for Further Credit", deliverAgainstPayment.getSafekeepingAccount());

		Assertions.assertEquals("PPSCUS66", deliverAgainstPayment.getSenderBIC());
		Assertions.assertEquals(MessageFunctions.NEW_MESSAGE, deliverAgainstPayment.getMessageFunctions());
		Assertions.assertEquals(DateUtils.fromDate(lendingRepo.getMaturityDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(deliverAgainstPayment.getTradeDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(DateUtils.fromDate(lendingRepo.getMaturitySettlementDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(deliverAgainstPayment.getSettlementDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(0, lendingRepo.getOriginalFace().compareTo(deliverAgainstPayment.getOriginalFaceAmount()));
		Assertions.assertEquals(DateUtils.fromDate(lendingRepo.getMaturityDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(deliverAgainstPayment.getRepoClosingDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(0, lendingRepo.getNetCash().add(lendingRepo.getInterestAmount()).compareTo(deliverAgainstPayment.getRepoNetCashAmount()));
		Assertions.assertEquals(0, lendingRepo.getInterestAmount().compareTo(deliverAgainstPayment.getRepoAccruedInterestAmount()));
		Assertions.assertEquals(LendingRepoBuilder.getEUR().getSymbol(), deliverAgainstPayment.getSecurityCurrency());
		Assertions.assertEquals("FRNYUS33", deliverAgainstPayment.getPlaceOfSettlement());
		Assertions.assertEquals(0, lendingRepo.getNetCash().compareTo(deliverAgainstPayment.getNetSettlementAmount()));

		Assertions.assertEquals("Business Identifier Code", deliverAgainstPayment.getReceiverBIC());
		Assertions.assertEquals("Business Identifier Code", deliverAgainstPayment.getCustodyBIC());
		Assertions.assertEquals("88-12354", deliverAgainstPayment.getCustodyAccountNumber());

		Assertions.assertEquals(ISITCCodes.TIPS, deliverAgainstPayment.getIsitcCode());
		Assertions.assertEquals("ISIN Number", deliverAgainstPayment.getIsin());

		Assertions.assertEquals("COUNTER", deliverAgainstPayment.getExecutingBrokerIdentifier().getIdentifier());

		Assertions.assertEquals(0, deliverAgainstPayment.getRepoInterestRate().compareTo(new BigDecimal("1.5")));
		Assertions.assertEquals(0, deliverAgainstPayment.getPrice().compareTo(new BigDecimal("103.4697")));
		Assertions.assertEquals(RepoRateTypes.FIXED, deliverAgainstPayment.getRepoRateType());
		Assertions.assertEquals(TradeMessagePriceTypes.PERCENTAGE, deliverAgainstPayment.getPriceType());
		Assertions.assertEquals(0, deliverAgainstPayment.getDealAmount().compareTo(new BigDecimal("1591562.51").multiply(BigDecimal.ONE.subtract(MathUtils.divide(new BigDecimal("0.2"), new BigDecimal(100))))));
		MatcherAssert.assertThat(deliverAgainstPayment.getMaturityDate(), IsEqual.equalTo(DateUtils.toDate("10/10/2020")));
		MatcherAssert.assertThat(deliverAgainstPayment.getIssueDate(), IsEqual.equalTo(DateUtils.toDate("1/10/2010")));
		Assertions.assertEquals(0, deliverAgainstPayment.getInterestRate().compareTo(new BigDecimal("2.22")));
	}
}
