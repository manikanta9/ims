package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCloseAtCostBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveJournalDetailsBookingRule;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoTestObjectFactory;
import com.clifton.lending.repo.builder.LendingRepoBuilder;
import com.clifton.lending.repo.builder.LendingRepoBuilder.SecurityCategories;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;


public class LendingRepoPositionOpenBookingRuleTest {

	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoOpening() {
		testRepoDetailPositionOpenBookingRuleRepo(true,
				"-11	11696	123456	123456	Position	912810FH6	156.06	-500,000	-1,074,660.37	02/05/2018	1	-1,074,660.37	-1,074,660.37	12/30/2011	02/05/2018	Position close from REPO opening for 912810FH6",
				"-12	null	123456	654321	Position	912810FH6	156.06	500,000	1,074,660.37	02/05/2018	1	1,074,660.37	1,074,660.37	12/30/2011	02/05/2018	Position opening from REPO opening for 912810FH6"
		);
	}


	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoClosing() {
		testRepoDetailPositionOpenBookingRuleRepo(false,
				"-10	11697	123456	654321	Position	912810FH6	156.06	-500,000	-1,074,660.37	02/08/2018	1	-1,074,660.37	-1,074,660.37	12/30/2011	02/08/2018	Position close from REPO maturity for 912810FH6",
				"-11	null	123456	123456	Position	912810FH6	156.06	500,000	1,074,660.37	02/08/2018	1	1,074,660.37	1,074,660.37	12/30/2011	02/08/2018	Position opening from REPO maturity for 912810FH6");
	}


	private void testRepoDetailPositionOpenBookingRuleRepo(boolean open, String... expectedFormattedDetails) {
		LendingRepo repo = LendingRepoBuilder.createRepo()
				.notReverse()
				.ofType("Term")
				.addInterestRate(new BigDecimal("1.5"))
				.addTerm(1)
				.forSecurity("912810FH6", "912810FH6", 1, SecurityCategories.Bonds, new BigDecimal("0.01"), LendingRepoBuilder.getUSD(), new BigDecimal("10.32423"))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoData(new BigDecimal("1.37634"), new BigDecimal("500000"), new BigDecimal("103"), new BigDecimal("5.0"), new BigDecimal("716392.49"), new BigDecimal("37.81"))
				.addExchangeRateToBase(BigDecimal.ONE)
				.toRepo();

		repo.setTradeDate(DateUtils.toDate("02/05/2018"));
		repo.setSettlementDate(DateUtils.toDate("02/05/2018"));
		if (!open) {
			repo.setMaturityDate(DateUtils.toDate("02/08/2018"));
			repo.setMaturitySettlementDate(DateUtils.toDate("02/08/2018"));
		}

		AccountingTransaction tran;
		if (open) {
			tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(1004000).price("156.06").costBasis("2157918.03").on("12/30/2011").build();
		}
		else {
			tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11697L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(500000).price("156.06").costBasis("1074660.37").on("12/30/2011").build();
		}

		AccountingBookingRuleTestExecutor.newTestForEntity(repo)
				.forBookingRules(getRuleList(repo, open, CollectionUtils.createList(tran), null))
				.withExpectedResults(expectedFormattedDetails)
				.execute();
	}


	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoOpeningReverse() {
		testRepoDetailPositionOpenBookingRuleRepoReverse(true,
				"-11	11696	123456	654321	Position	912810FH6	156.06	-500,000	-1,074,660.37	02/05/2018	1	-1,074,660.37	-1,074,660.37	12/30/2011	02/05/2018	Position close from reverse REPO opening for 912810FH6",
				"-12	null	123456	123456	Position	912810FH6	156.06	500,000	1,074,660.37	02/05/2018	1	1,074,660.37	1,074,660.37	12/30/2011	02/05/2018	Position opening from reverse REPO opening for 912810FH6");
	}


	@Test
	public void testRepoDetailPositionOpenBookingRuleRepoClosingReverse() {
		testRepoDetailPositionOpenBookingRuleRepoReverse(false,
				"-10	11697	123456	123456	Position	912810FH6	156.06	-500,000	-1,074,660.37	02/08/2018	1	-1,074,660.37	-1,074,660.37	12/30/2011	02/08/2018	Position close from reverse REPO maturity for 912810FH6",
				"-11	11698	123456	654321	Position	912810FH6	156.06	500,000	1,074,660.37	02/08/2018	1	1,074,660.37	1,074,660.37	12/30/2011	02/08/2018	Position close from reverse REPO maturity for 912810FH6");
	}


	private void testRepoDetailPositionOpenBookingRuleRepoReverse(boolean open, String... expectedFormattedDetails) {
		LendingRepo repo = LendingRepoBuilder.createRepo()
				.reverse()
				.ofType("Term")
				.addInterestRate(new BigDecimal("1.5"))
				.addTerm(1)
				.forSecurity("912810FH6", "912810FH6", 1, SecurityCategories.Bonds, new BigDecimal("0.01"), LendingRepoBuilder.getUSD(), new BigDecimal("10.32423"))
				.forClient("GBR", 510, "123456", LendingRepoBuilder.getUSD())
				.forRepoAccount("GBR Repo", 520, "654321", LendingRepoBuilder.getUSD())
				.addRepoData(new BigDecimal("1.37634"), new BigDecimal("500000"), new BigDecimal("103"), new BigDecimal("5.0"), new BigDecimal("716392.49"), new BigDecimal("37.81"))
				.addExchangeRateToBase(BigDecimal.ONE)
				.toRepo();

		repo.setTradeDate(DateUtils.toDate("02/05/2018"));
		repo.setSettlementDate(DateUtils.toDate("02/05/2018"));
		if (!open) {
			repo.setMaturityDate(DateUtils.toDate("02/08/2018"));
			repo.setMaturitySettlementDate(DateUtils.toDate("02/08/2018"));
		}

		AccountingBookingRuleTestExecutor.newTestForEntity(repo)
				.forBookingRules(getRuleList(repo, open, getReverseClosingTransactionList(repo, open), getRevereOpenTransactionsList(repo, open)))
				.withExpectedResults(expectedFormattedDetails)
				.execute();
	}


	private List<AccountingTransaction> getReverseClosingTransactionList(LendingRepo repo, boolean open) {
		if (!open) {
			return CollectionUtils.createList(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11697L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(500000).price("156.06").costBasis("1074660.37").on("12/30/2011").build());
		}
		return CollectionUtils.createList(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, repo.getRepoDefinition().getInvestmentSecurity())
				.qty(1004000).price("156.06").costBasis("2157918.03").on("12/30/2011").build());
	}


	private List<AccountingTransaction> getRevereOpenTransactionsList(LendingRepo repo, boolean open) {
		if (!open) {
			return CollectionUtils.createList(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11698L, repo.getRepoDefinition().getInvestmentSecurity())
					.qty(-500000).price("156.06").costBasis("-1074660.37").on("12/30/2011").build());
		}
		return null;
	}


	private List<AccountingBookingRule<LendingRepo>> getRuleList(LendingRepo repo, boolean open, List<AccountingTransaction> transactionList, List<AccountingTransaction> openingRuleTransactions) {
		AccountingPositionSplitterBookingRule<LendingRepo> splitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule();
		splitterRule.setClosingOrder(AccountingPositionOrders.LIFO);

		LendingRepoPositionCloseBookingRule repoCloseBookingRule = LendingRepoTestObjectFactory.newLendingRepoPositionCloseBookingRule(repo, open, splitterRule, transactionList);

		AccountingRealizedGainLossBookingRule<LendingRepo> gainLossRule = AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule();
		AccountingRemoveJournalDetailsBookingRule<LendingRepo> removeGainLossRule = AccountingTestObjectFactory.newRemoveJournalDetailsBookingRule(AccountingAccount.REVENUE_REALIZED, AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS);
		AccountingCloseAtCostBookingRule<LendingRepo> closeAtCostBookingRule = AccountingTestObjectFactory.newCloseAtCostBookingRule();
		LendingRepoPositionOpenBookingRule repoOpenBookingRule;
		if (CollectionUtils.isEmpty(openingRuleTransactions)) {
			repoOpenBookingRule = LendingRepoTestObjectFactory.newLendingRepoPositionOpenBookingRule(open);
		}
		else {
			repoOpenBookingRule = LendingRepoTestObjectFactory.newLendingRepoPositionOpenBookingRule(repo, open, splitterRule, openingRuleTransactions);
		}

		return CollectionUtils.createList(repoCloseBookingRule, splitterRule, gainLossRule, removeGainLossRule, closeAtCostBookingRule, repoOpenBookingRule, splitterRule);
	}
}
