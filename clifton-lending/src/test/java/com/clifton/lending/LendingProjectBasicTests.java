package com.clifton.lending;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class LendingProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "lending";
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		HashSet<String> skipMethods = new HashSet<>();
		skipMethods.add("getLendingRepoExtendedList");
		skipMethods.add("saveLendingRepoInterestRate");
		return skipMethods.contains(method.getName());
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.transaction.support.TransactionSynchronizationManager");
	}
}
