package com.clifton.lending.api.repo;


import com.clifton.business.shared.Company;
import com.clifton.investment.shared.ClientAccount;
import com.clifton.investment.shared.HoldingAccount;
import com.clifton.investment.shared.Security;
import com.clifton.security.shared.user.User;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Represents a Repurchase Agreement or REPO.
 */
public class Repo implements Serializable {

	private Integer id;

	private Integer parentId;

	private Security security;
	private ClientAccount clientAccount;
	private HoldingAccount holdingAccount;
	private HoldingAccount repoAccount;

	private Company counterparty;

	private User traderUser;

	private String repoType;
	private String workflowState;
	private String workflowStatus;


	/**
	 * Number of days the repo is for. For repos with multiple interest rates, such as open and evergreen repos, this is the number of days for which each iterative interest rate
	 * applies, which is typically one.
	 */
	private Integer term;

	/**
	 * Determines if the REPO is a reverse REPO.
	 */
	private boolean reverse;


	/**
	 * The number of days notice that either party has to give to cancel the REPO.  Currently used for Evergreen REPO's.
	 */
	private Short cancellationNoticeTerm;

	/**
	 * The interest rate of the REPO.
	 */
	private BigDecimal interestRate;
	private String description;

	private BigDecimal price;
	private BigDecimal haircutPercent;

	/**
	 * The total value of the bond borrowed or lent.
	 */
	private BigDecimal marketValue;
	/**
	 * Total cash borrowed or lent in the REPO.  marketValue*(1 - repoDefinitionDetail.hairCutPercent)
	 */
	private BigDecimal netCash;
	/**
	 * How much the loan will cost based on the term and interest rate.
	 */
	private BigDecimal interestAmount;
	private BigDecimal exchangeRateToBase;
	/**
	 * Some securities may adjust accounting notional by this multiplier that is tied to something.
	 * TIPS use it to adjust for changes in inflation: Index Ratio.
	 */
	private BigDecimal indexRatio;

	private BigDecimal quantityIntended;
	/**
	 * The original face of the bond being either received or lent out.
	 */
	private BigDecimal originalFace;

	private Date tradeDate;
	private Date settlementDate;
	/**
	 * Maturity Date is also known as "Action Date".
	 * It's used to tie REPO security trade to corresponding REPO maturity: same transaction and settlement dates.
	 */
	private Date maturityDate;
	/**
	 * The date at which an action needs to be taken, i.e. initiating a roll of the REPO
	 * NOTE: REPO Collateral depends on the "Maturity Date" which for users on screen is actually this field
	 * If fields are ever to be moved around, need to ensure REPO Collateral is using the correct "maturity Date" field for comparison
	 */
	private Date maturitySettlementDate;

	/**
	 * Date and time when accounting confirmed the opening REPO trade.
	 * REPO's must be confirmed before maturity.
	 */
	private Date confirmedDate;
	private String confirmationNote;

	private Date bookingDate;
	private Date maturityBookingDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isConfirmed() {
		return getConfirmedDate() != null;
	}


	public boolean isRoll() {
		return getParentId() != null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Security getSecurity() {
		return this.security;
	}


	public void setSecurity(Security security) {
		this.security = security;
	}


	public ClientAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(ClientAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public HoldingAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(HoldingAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public HoldingAccount getRepoAccount() {
		return this.repoAccount;
	}


	public void setRepoAccount(HoldingAccount repoAccount) {
		this.repoAccount = repoAccount;
	}


	public Company getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(Company counterparty) {
		this.counterparty = counterparty;
	}


	public User getTraderUser() {
		return this.traderUser;
	}


	public void setTraderUser(User traderUser) {
		this.traderUser = traderUser;
	}


	public String getRepoType() {
		return this.repoType;
	}


	public void setRepoType(String repoType) {
		this.repoType = repoType;
	}


	public String getWorkflowState() {
		return this.workflowState;
	}


	public void setWorkflowState(String workflowState) {
		this.workflowState = workflowState;
	}


	public String getWorkflowStatus() {
		return this.workflowStatus;
	}


	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}


	public Integer getTerm() {
		return this.term;
	}


	public void setTerm(Integer term) {
		this.term = term;
	}


	public boolean isReverse() {
		return this.reverse;
	}


	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}


	public Short getCancellationNoticeTerm() {
		return this.cancellationNoticeTerm;
	}


	public void setCancellationNoticeTerm(Short cancellationNoticeTerm) {
		this.cancellationNoticeTerm = cancellationNoticeTerm;
	}


	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getHaircutPercent() {
		return this.haircutPercent;
	}


	public void setHaircutPercent(BigDecimal haircutPercent) {
		this.haircutPercent = haircutPercent;
	}


	public BigDecimal getMarketValue() {
		return this.marketValue;
	}


	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}


	public BigDecimal getNetCash() {
		return this.netCash;
	}


	public void setNetCash(BigDecimal netCash) {
		this.netCash = netCash;
	}


	public BigDecimal getInterestAmount() {
		return this.interestAmount;
	}


	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}


	public BigDecimal getQuantityIntended() {
		return this.quantityIntended;
	}


	public void setQuantityIntended(BigDecimal quantityIntended) {
		this.quantityIntended = quantityIntended;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public Date getMaturitySettlementDate() {
		return this.maturitySettlementDate;
	}


	public void setMaturitySettlementDate(Date maturitySettlementDate) {
		this.maturitySettlementDate = maturitySettlementDate;
	}


	public Date getConfirmedDate() {
		return this.confirmedDate;
	}


	public void setConfirmedDate(Date confirmedDate) {
		this.confirmedDate = confirmedDate;
	}


	public String getConfirmationNote() {
		return this.confirmationNote;
	}


	public void setConfirmationNote(String confirmationNote) {
		this.confirmationNote = confirmationNote;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Date getMaturityBookingDate() {
		return this.maturityBookingDate;
	}


	public void setMaturityBookingDate(Date maturityBookingDate) {
		this.maturityBookingDate = maturityBookingDate;
	}
}
