package com.clifton.lending.api.repo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author vgomelsky
 */
public interface LendingRepoApiService {


	public Repo getRepo(int id);


	public List<Repo> getRepoList(RepoSearchCommand searchCommand);


	public List<Repo> getRepoListForIds(Integer[] ids);


	////////////////////////////////////////////////////////////////////////////
	//////////           Accrued Interest Calculators                 //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Properly calculates the accrued interest for a repo from the settlement date to the given date
	 * If Open/Multiple Rates uses the LendingRepoInterestTable else (Term) takes the proportion of the interest on the
	 * repo for the number of days since settlement
	 */
	public BigDecimal getRepoAccruedInterest(int repoId, Date date);


	public BigDecimal getRepoAccruedInterestForCommand(RepoSearchCommand searchCommand, Date measureDate);
}
