package com.clifton.lending.api.repo;


import com.clifton.workflow.shared.WorkflowSearchCommand;

import java.util.Date;


/**
 * @author vgomelsky
 */
public class RepoSearchCommand extends WorkflowSearchCommand {

	private Integer typeId;

	private String typeName;


	private Integer clientInvestmentAccountId;

	private String clientInvestmentAccountNumber;

	private Integer holdingInvestmentAccountId;

	private String holdingInvestmentAccountNumber;

	private Integer repoInvestmentAccountId;


	private Date beforeSettlementDate;

	private Date tradeDate;

	private Date tradeDateBefore;

	private Date settlementDate;

	private Date maturitySettlementDate;

	private Date maturitySettlementDateAfterOrNull;

	private Date bookingDate;

	private Date maturityDate;

	private Date openingUnbookedOrMaturityUnbookedOnDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public String getClientInvestmentAccountNumber() {
		return this.clientInvestmentAccountNumber;
	}


	public void setClientInvestmentAccountNumber(String clientInvestmentAccountNumber) {
		this.clientInvestmentAccountNumber = clientInvestmentAccountNumber;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public String getHoldingInvestmentAccountNumber() {
		return this.holdingInvestmentAccountNumber;
	}


	public void setHoldingInvestmentAccountNumber(String holdingInvestmentAccountNumber) {
		this.holdingInvestmentAccountNumber = holdingInvestmentAccountNumber;
	}


	public Integer getRepoInvestmentAccountId() {
		return this.repoInvestmentAccountId;
	}


	public void setRepoInvestmentAccountId(Integer repoInvestmentAccountId) {
		this.repoInvestmentAccountId = repoInvestmentAccountId;
	}


	public Date getBeforeSettlementDate() {
		return this.beforeSettlementDate;
	}


	public void setBeforeSettlementDate(Date beforeSettlementDate) {
		this.beforeSettlementDate = beforeSettlementDate;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getTradeDateBefore() {
		return this.tradeDateBefore;
	}


	public void setTradeDateBefore(Date tradeDateBefore) {
		this.tradeDateBefore = tradeDateBefore;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getMaturitySettlementDate() {
		return this.maturitySettlementDate;
	}


	public void setMaturitySettlementDate(Date maturitySettlementDate) {
		this.maturitySettlementDate = maturitySettlementDate;
	}


	public Date getMaturitySettlementDateAfterOrNull() {
		return this.maturitySettlementDateAfterOrNull;
	}


	public void setMaturitySettlementDateAfterOrNull(Date maturitySettlementDateAfterOrNull) {
		this.maturitySettlementDateAfterOrNull = maturitySettlementDateAfterOrNull;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public Date getOpeningUnbookedOrMaturityUnbookedOnDate() {
		return this.openingUnbookedOrMaturityUnbookedOnDate;
	}


	public void setOpeningUnbookedOrMaturityUnbookedOnDate(Date openingUnbookedOrMaturityUnbookedOnDate) {
		this.openingUnbookedOrMaturityUnbookedOnDate = openingUnbookedOrMaturityUnbookedOnDate;
	}
}
