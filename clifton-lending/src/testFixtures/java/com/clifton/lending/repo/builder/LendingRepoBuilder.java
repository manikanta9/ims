package com.clifton.lending.repo.builder;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.DayCountConventions;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoDefinition;
import com.clifton.lending.repo.LendingRepoType;
import com.clifton.security.user.SecurityUser;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


public class LendingRepoBuilder {

	private static final InvestmentSecurity USD = InvestmentSecurityBuilder.newUSD();
	private static final InvestmentSecurity YEN = InvestmentSecurityBuilder.newJPY();
	private static final InvestmentSecurity EUR = InvestmentSecurityBuilder.newEUR();
	private static final BusinessCompany GOLDMAN;
	private final LendingRepo repo;


	private LendingRepoBuilder(LendingRepo lendingRepo) {
		this.repo = lendingRepo;
	}


	/**
	 * Creates an "existing" trade with id = 42.
	 */
	public static LendingRepoBuilder createRepo() {
		return createRepo(42);
	}


	/**
	 * Creates a new Trade: trade.isNewBean() == true
	 */
	public static LendingRepoBuilder createNewRepo() {
		return createRepo(null);
	}


	public static LendingRepoBuilder createRepo(Integer id) {
		LendingRepo innerRepo = new LendingRepo();
		if (id != null) {
			innerRepo.setId(id);
		}
		innerRepo.setRepoDefinition(new LendingRepoDefinition());
		innerRepo.setSettlementDate(new Date());
		innerRepo.setTradeDate(new Date());
		innerRepo.getRepoDefinition().setCounterparty(GOLDMAN);
		innerRepo.getRepoDefinition().setDayCountConvention(DayCountConventions.ACTUAL_THREESIXTY.getAccrualString());
		return new LendingRepoBuilder(innerRepo);
	}


	public static InvestmentSecurity getUSD() {
		return USD;
	}


	public static InvestmentSecurity getYEN() {
		return YEN;
	}


	public static InvestmentSecurity getEUR() {
		return EUR;
	}


	/**
	 * Must be called after or instead of set term.
	 */
	public LendingRepoBuilder forDates(String tradeDate, String settlementDate, String maturityDate, String maturitySettlementDate) {
		getRepo().setTradeDate(!StringUtils.isEmpty(tradeDate) ? DateUtils.toDate(tradeDate) : null);
		getRepo().setSettlementDate(!StringUtils.isEmpty(settlementDate) ? DateUtils.toDate(settlementDate) : null);
		getRepo().setMaturityDate(!StringUtils.isEmpty(maturityDate) ? DateUtils.toDate(maturityDate) : null);
		getRepo().setMaturitySettlementDate(!StringUtils.isEmpty(maturitySettlementDate) ? DateUtils.toDate(maturitySettlementDate) : null);

		if ((getRepo().getSettlementDate() != null) && (getRepo().getMaturitySettlementDate() != null)) {
			getRepo().setTerm(DateUtils.getDaysDifference(getRepo().getMaturitySettlementDate(), getRepo().getSettlementDate()));
		}
		return this;
	}


	public LendingRepoBuilder forSecurity(InvestmentSecurity security) {
		getRepo().getRepoDefinition().setInvestmentSecurity(security);
		getRepo().getRepoDefinition().setInstrumentHierarchy(security.getInstrument().getHierarchy());
		return this;
	}


	public LendingRepoBuilder forSecurity(String securityName, String securitySymbol, Integer securityID, com.clifton.lending.repo.builder.LendingRepoBuilder.SecurityCategories bonds,
	                                      BigDecimal multiplier, InvestmentSecurity currency, BigDecimal unitPrice) {
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(multiplier);
		instrument.setTradingCurrency(currency);
		instrument.setId(1);
		instrument.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_HALF_UP);

		InvestmentType type = new InvestmentType();
		type.setName(bonds.name());

		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		hierarchy.setId(MathUtils.SHORT_ONE);
		instrument.setHierarchy(hierarchy);

		InvestmentSecurity security = new InvestmentSecurity();
		security.setName(securityName);
		security.setSymbol(securitySymbol);
		security.setCusip(securitySymbol);
		security.setInstrument(instrument);
		security.setId(securityID);

		getRepo().getRepoDefinition().setInvestmentSecurity(security);
		getRepo().getRepoDefinition().setInstrumentHierarchy(hierarchy);
		getRepo().setPrice(unitPrice);
		return this;
	}


	public LendingRepoBuilder ofType(String name) {
		LendingRepoType type = new LendingRepoType();
		type.setName(name);
		if ("Open".equals(name)) {
			type.setMultipleInterestRatesPerPeriod(true);
			type.setMaturityDateRequiredForOpen(false);
			type.setId(1);
		}
		else if ("Term".equals(name)) {
			type.setMultipleInterestRatesPerPeriod(false);
			type.setMaturityDateRequiredForOpen(true);
			type.setId(2);
		}
		else if ("Overnight".equals(name)) {
			type.setMultipleInterestRatesPerPeriod(false);
			type.setMaturityDateRequiredForOpen(true);
			type.setRequiredTerm(1);
			type.setId(3);
		}
		else if ("Open Tri-Party".equals(name)) {
			type.setMultipleInterestRatesPerPeriod(true);
			type.setMaturityDateRequiredForOpen(false);
			type.setTriPartyRepo(true);
			type.setId(4);
		}
		else if ("Term Tri-Party".equals(name)) {
			type.setMultipleInterestRatesPerPeriod(false);
			type.setMaturityDateRequiredForOpen(true);
			type.setTriPartyRepo(true);
			type.setId(5);
		}
		else if ("Overnight Tri-Party".equals(name)) {
			type.setMultipleInterestRatesPerPeriod(false);
			type.setMaturityDateRequiredForOpen(true);
			type.setRequiredTerm(1);
			type.setTriPartyRepo(true);
			type.setId(6);
		}
		getRepo().setType(type);
		return this;
	}


	public LendingRepoBuilder forRepoAccount(String accountName, Integer repoAccountID, InvestmentSecurity baseCurrency) {
		return forRepoAccount(accountName, repoAccountID, null, baseCurrency);
	}


	public LendingRepoBuilder forRepoAccount(String accountName, Integer repoAccountID, String accountNumber, InvestmentSecurity baseCurrency) {
		InvestmentAccount investAcc = new InvestmentAccount();
		investAcc.setName(accountName);
		investAcc.setBaseCurrency(baseCurrency);
		investAcc.setId(repoAccountID);
		investAcc.setNumber(accountNumber);
		investAcc.setType(new InvestmentAccountType());
		investAcc.getType().setName("REPO");
		investAcc.setIssuingCompany(new BusinessCompany());
		investAcc.getIssuingCompany().setName("REPO Issuing Company");
		getRepo().getRepoDefinition().setRepoInvestmentAccount(investAcc);
		return this;
	}


	public LendingRepoBuilder forTriPartyAccount(String accountName, Integer repoAccountID, String accountNumber, InvestmentSecurity baseCurrency) {
		InvestmentAccount investAcc = new InvestmentAccount();
		investAcc.setName(accountName);
		investAcc.setBaseCurrency(baseCurrency);
		investAcc.setId(repoAccountID);
		investAcc.setNumber(accountNumber);
		investAcc.setType(new InvestmentAccountType());
		investAcc.getType().setName("TRI-PARTY");
		getRepo().getRepoDefinition().setTriPartyInvestmentAccount(investAcc);
		return this;
	}


	public LendingRepoBuilder forClient(String clientName, Integer clientID, String accountNumber, InvestmentSecurity baseCurrency) {
		InvestmentAccount clientAccount = new InvestmentAccount();
		clientAccount.setName(clientName);
		clientAccount.setBaseCurrency(baseCurrency);
		clientAccount.setId(clientID);
		clientAccount.setNumber(accountNumber);
		clientAccount.setType(new InvestmentAccountType());
		clientAccount.getType().setOurAccount(true);
		getRepo().getRepoDefinition().setClientInvestmentAccount(clientAccount);

		InvestmentAccount holdingAccount = new InvestmentAccount();
		holdingAccount.setName(clientName);
		holdingAccount.setBaseCurrency(baseCurrency);
		holdingAccount.setId(clientID);
		holdingAccount.setNumber(accountNumber);
		holdingAccount.setType(new InvestmentAccountType());
		holdingAccount.getType().setName("Custodian");
		getRepo().getRepoDefinition().setHoldingInvestmentAccount(holdingAccount);
		return this;
	}


	public LendingRepoBuilder addRepoDataForSave(BigDecimal indexRatio, BigDecimal originalFace, BigDecimal price, BigDecimal haircutPercent) {
		// Set repo data which is not automatically inferred by the system on save
		this.getRepo().setIndexRatio(indexRatio);
		this.getRepo().setOriginalFace(originalFace);
		this.getRepo().setPrice(price);
		this.getRepo().setHaircutPercent(haircutPercent);
		return this;
	}


	public LendingRepoBuilder addRepoData(BigDecimal indexRatio, BigDecimal originalFace, BigDecimal price, BigDecimal haircutPercent, BigDecimal marketValue, BigDecimal interestAmount) {
		this.getRepo().setIndexRatio(indexRatio);
		this.getRepo().setOriginalFace(originalFace);
		this.getRepo().setPrice(price);
		this.getRepo().setHaircutPercent(haircutPercent);
		this.getRepo().setMarketValue(marketValue);
		this.getRepo().setInterestAmount(interestAmount);
		this.getRepo().setNetCash(marketValue.multiply(BigDecimal.ONE.subtract(MathUtils.divide(haircutPercent, new BigDecimal(100)))));
		return this;
	}


	public LendingRepoBuilder addExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.getRepo().setExchangeRateToBase(exchangeRateToBase);
		return this;
	}


	public LendingRepo toRepo() {
		return this.repo;
	}


	public LendingRepoBuilder addTerm(int term) {
		getRepo().setTerm(term);
		getRepo().setMaturityDate(DateUtils.addDays(this.repo.getTradeDate(), term - 1));
		getRepo().setMaturitySettlementDate(DateUtils.addDays(this.repo.getTradeDate(), term));
		return this;
	}


	public LendingRepoBuilder addInterestRate(BigDecimal rate) {
		getRepo().setInterestRate(rate);
		return this;
	}


	public LendingRepoBuilder setReverse(boolean reverse) {
		getRepo().getRepoDefinition().setReverse(reverse);
		return this;
	}


	public LendingRepoBuilder reverse() {
		getRepo().getRepoDefinition().setReverse(true);
		return this;
	}


	public LendingRepoBuilder notReverse() {
		getRepo().getRepoDefinition().setReverse(false);
		return this;
	}


	public LendingRepoBuilder withTradeId(int id) {
		getRepo().setId(id);
		return this;
	}


	private LendingRepo getRepo() {
		return this.repo;
	}


	public LendingRepoBuilder forTrader(String traderName) {
		SecurityUser securityUser = new SecurityUser();
		securityUser.setUserName(traderName);
		this.repo.setTraderUser(securityUser);
		return this;
	}


	public enum SecurityCategories {
		Bonds
	}

	static {
		GOLDMAN = new BusinessCompany();
		GOLDMAN.setName("Goldman Sachs");
	}
}
