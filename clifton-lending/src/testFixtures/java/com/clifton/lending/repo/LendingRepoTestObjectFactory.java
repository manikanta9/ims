package com.clifton.lending.repo;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarBusinessDayTypes;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.lending.repo.booking.rule.BaseLendingRepoPositionBookingRule;
import com.clifton.lending.repo.booking.rule.LendingRepoCashOrCurrencyBookingRule;
import com.clifton.lending.repo.booking.rule.LendingRepoPositionCloseBookingRule;
import com.clifton.lending.repo.booking.rule.LendingRepoPositionOpenBookingRule;
import com.clifton.lending.repo.booking.rule.LendingRepoTriPartyPositionCloseBookingRule;
import com.clifton.lending.repo.booking.rule.LendingRepoTriPartyPositionOpenBookingRule;
import com.clifton.lending.repo.booking.rule.LendingRepoTriPartyReverseCashBookingRule;
import com.clifton.marketdata.MarketDataTestObjectFactory;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.core.util.MathUtils;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class LendingRepoTestObjectFactory {

	public static LendingRepoTriPartyReverseCashBookingRule newLendingRepoTriPartyReverseCashBookingRule(boolean open) {
		LendingRepoTriPartyReverseCashBookingRule bookingRule = new LendingRepoTriPartyReverseCashBookingRule();
		bookingRule.setSystemSchemaService(newSystemSchemaService());
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService());
		bookingRule.setRepoOpening(open);
		return bookingRule;
	}


	public static LendingRepoPositionCloseBookingRule newLendingRepoPositionCloseBookingRule(boolean open) {
		LendingRepoPositionCloseBookingRule bookingRule = new LendingRepoPositionCloseBookingRule();
		bookingRule.setSystemSchemaService(newSystemSchemaService());
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setRepoOpening(open);

		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setAccountingJournalService(Mockito.mock(AccountingJournalService.class));
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		bookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService());
		return bookingRule;
	}


	public static LendingRepoPositionCloseBookingRule newLendingRepoPositionCloseBookingRule(LendingRepo repo, boolean open, AccountingPositionSplitterBookingRule<LendingRepo> splitterRule, List<AccountingTransaction> transactionList) {
		LendingRepoPositionCloseBookingRule result = newLendingRepoPositionCloseBookingRule(open);
		return (LendingRepoPositionCloseBookingRule) newBaseLendingRepoPositionBookingRule(result, repo, open, true, splitterRule, transactionList);
	}


	private static BaseLendingRepoPositionBookingRule newBaseLendingRepoPositionBookingRule(BaseLendingRepoPositionBookingRule rule, LendingRepo repo, boolean open, boolean closingRule, AccountingPositionSplitterBookingRule<LendingRepo> splitterRule, List<AccountingTransaction> transactionList) {
		if (splitterRule != null) {
			rule.setAccountingTransactionService(splitterRule.getAccountingTransactionService());
		}

		List<AccountingPosition> existingPositions = new ArrayList<>();
		InvestmentAccount holdingAccount = getHoldingAccount(repo, closingRule, open);
		transactionList.forEach(transaction -> {
			transaction.setClientInvestmentAccount(repo.getClientInvestmentAccount());
			transaction.setHoldingInvestmentAccount(holdingAccount);
			existingPositions.add(AccountingPosition.forOpeningTransaction(transaction));
			Mockito.when(rule.getAccountingTransactionService().getAccountingTransaction(ArgumentMatchers.eq(transaction.getId()))).thenReturn(transaction);
		});


		if (splitterRule != null) {
			Mockito.when(splitterRule.getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
					new AccountingPositionCommand().forClientAccount(repo.getRepoDefinition().getClientInvestmentAccount().getId())
							.forHoldingAccount(holdingAccount.getId())
							.forInvestmentSecurity(repo.getRepoDefinition().getInvestmentSecurity().getId())
			))).thenReturn(existingPositions);
		}

		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setHoldingInvestmentAccountId(holdingAccount.getId());
		Mockito.when(rule.getAccountingTransactionService().getAccountingTransactionList(AccountingTestObjectFactory.newAccountingTransactionSearchFormMatcher(searchForm))).then(invocationOnMock -> {
			Integer holdingInvestmentAccountId = ((AccountingTransactionSearchForm) invocationOnMock.getArgument(0)).getHoldingInvestmentAccountId();
			if (holdingInvestmentAccountId != null) {
				return CollectionUtils.getFiltered(transactionList, transaction -> transaction.getHoldingInvestmentAccount().getId().equals(holdingInvestmentAccountId));
			}
			return transactionList;
		});

		AccountingJournal journal = new AccountingJournal();
		journal.setId(1948785L);
		Mockito.when(rule.getAccountingJournalService().getAccountingJournalBySourceAndSequence(ArgumentMatchers.eq("LendingRepo"), ArgumentMatchers.eq(repo.getId()), ArgumentMatchers.eq(1))).thenReturn(journal);

		return rule;
	}


	protected static InvestmentAccount getHoldingAccount(LendingRepo repo, boolean closingRule, boolean open) {
		InvestmentAccount holdingInvestmentAccount;
		if ((closingRule && open) || (!closingRule && !open)) {
			holdingInvestmentAccount = repo.getRepoDefinition().isReverse() ? repo.getRepoDefinition().getRepoInvestmentAccount() : repo.getHoldingInvestmentAccount();
		}
		else {
			holdingInvestmentAccount = !repo.getRepoDefinition().isReverse() ? repo.getRepoDefinition().getRepoInvestmentAccount() : repo.getHoldingInvestmentAccount();
		}
		return holdingInvestmentAccount;
	}


	public static LendingRepoTriPartyPositionCloseBookingRule newLendingRepoTriPartyPositionCloseBookingRule(boolean open) {
		LendingRepoTriPartyPositionCloseBookingRule bookingRule = new LendingRepoTriPartyPositionCloseBookingRule();
		bookingRule.setSystemSchemaService(newSystemSchemaService());
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setRepoOpening(open);

		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setAccountingJournalService(Mockito.mock(AccountingJournalService.class));
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		return bookingRule;
	}


	public static LendingRepoPositionOpenBookingRule newLendingRepoPositionOpenBookingRule(boolean open) {
		LendingRepoPositionOpenBookingRule bookingRule = new LendingRepoPositionOpenBookingRule();
		bookingRule.setSystemSchemaService(newSystemSchemaService());
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setRepoOpening(open);

		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setAccountingJournalService(Mockito.mock(AccountingJournalService.class));
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		return bookingRule;
	}


	public static LendingRepoPositionOpenBookingRule newLendingRepoPositionOpenBookingRule(LendingRepo repo, boolean open, AccountingPositionSplitterBookingRule<LendingRepo> splitterRule, List<AccountingTransaction> transactionList) {
		LendingRepoPositionOpenBookingRule result = newLendingRepoPositionOpenBookingRule(open);
		return (LendingRepoPositionOpenBookingRule) newBaseLendingRepoPositionBookingRule(result, repo, open, false, splitterRule, transactionList);
	}


	public static LendingRepoTriPartyPositionOpenBookingRule newLendingRepoTriPartyPositionOpenBookingRule(boolean open) {
		LendingRepoTriPartyPositionOpenBookingRule bookingRule = new LendingRepoTriPartyPositionOpenBookingRule();
		bookingRule.setSystemSchemaService(newSystemSchemaService());
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setRepoOpening(open);

		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setAccountingJournalService(Mockito.mock(AccountingJournalService.class));
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		return bookingRule;
	}


	public static LendingRepoCashOrCurrencyBookingRule newLendingRepoCashOrCurrencyBookingRule(boolean open) {
		LendingRepoCashOrCurrencyBookingRule bookingRule = new LendingRepoCashOrCurrencyBookingRule();
		bookingRule.setSystemSchemaService(newSystemSchemaService());
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService());
		bookingRule.setRepoOpening(open);
		return bookingRule;
	}


	private static SystemSchemaService newSystemSchemaService() {
		SystemSchemaService result = Mockito.mock(SystemSchemaService.class);
		SystemTable table = new SystemTable();
		table.setName("LendingRepo");
		table.setId(new Short("450"));
		Mockito.when(result.getSystemTableByName("LendingRepo")).thenReturn(table);
		return result;
	}


	public static CalendarSetupService newCalendarSetupService() {
		CalendarSetupService result = Mockito.mock(CalendarSetupService.class);
		Calendar c = new Calendar();
		c.setId(MathUtils.SHORT_ONE);

		Mockito.when(result.getCalendarByName(ArgumentMatchers.any(String.class))).thenReturn(c);
		return result;
	}


	public static CalendarBusinessDayService newCalendarBusinessDayService() {
		CalendarBusinessDayService result = Mockito.mock(CalendarBusinessDayService.class);

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.getDaysDifference((Date) args[1], (Date) args[0]);
		}).when(result).getBusinessDaysBetween(ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.nullable(CalendarBusinessDayTypes.class), ArgumentMatchers.anyShort());

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.addDays(((CalendarBusinessDayCommand) args[0]).getDate(), (Integer) args[2]);
		}).when(result).getBusinessDayFrom(ArgumentMatchers.any(CalendarBusinessDayCommand.class), ArgumentMatchers.anyInt());

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.addDays(((CalendarBusinessDayCommand) args[0]).getDate(), -1);
		}).when(result).getPreviousBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class));

		Mockito.when(result.isBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class))).thenReturn(true);
		return result;
	}
}
