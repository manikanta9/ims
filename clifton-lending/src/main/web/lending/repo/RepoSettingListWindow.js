Clifton.lending.repo.RepoSettingListWindow = Ext.extend(TCG.app.Window, {
	id: 'lendingRepoSettingListWindow',
	title: 'REPO Settings',
	iconCls: 'repo',
	width: 1000,
	height: 400,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,

		items: [
			{
				title: 'REPO Types',
				items: [{
					name: 'lendingRepoTypeListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					wikiPage: 'IT/REPO+Requirements',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 25, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 75, dataIndex: 'description', hidden: true},

						{header: 'REPO Purpose', width: 25, dataIndex: 'repoInvestmentAccountRelationshipPurpose.name', filter: {searchFieldName: 'repoInvestmentAccountRelationshipPurposeName'}},
						{header: 'Tri-Party Purpose', width: 25, dataIndex: 'triPartyInvestmentAccountRelationshipPurpose.name', filter: {searchFieldName: 'triPartyInvestmentAccountRelationshipPurposeName'}},

						{header: 'Required Term', width: 15, dataIndex: 'requiredTerm', type: 'int', useNull: true},

						{header: 'Multiple Rates', width: 15, dataIndex: 'multipleInterestRatesPerPeriod', type: 'boolean', qtip: 'The REPO type uses multiple interest rates per period.  Typically one per day.'},
						{header: 'Maturity Date Required for Open', width: 20, dataIndex: 'maturityDateRequiredForOpen', type: 'boolean', qtip: 'The maturity date is required on one opening.  This is always true unless the REPO is an Open type when the close date of the REPO is not predetermined.'},
						{header: 'Tri-Party', width: 15, dataIndex: 'triPartyRepo', type: 'boolean'},
						{header: 'Auto Pairoff', width: 15, dataIndex: 'autoPairoff', type: 'boolean'}

					],
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'REPO Settings',
				items: [{
					name: 'lendingRepoSettingListFind',
					xtype: 'gridpanel',
					instructions: 'Defines default REPO settings for securities in the specified Investment Hierarchy.',
					wikiPage: 'IT/REPO+Requirements',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Hierarchy', width: 150, dataIndex: 'instrumentHierarchy.labelExpanded', defaultSortColumn: true, filter: {searchFieldName: 'hierarchyName'}},
						{header: 'Days To Settle', width: 30, dataIndex: 'daysToSettle', type: 'int'},
						{header: 'Day Count', width: 30, dataIndex: 'dayCountConvention'}
					],
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoSettingWindow'
					}
				}]
			}
		]
	}]
});
