Clifton.lending.repo.RepoTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'repoTypeWindow',
	titlePrefix: 'REPO Type',
	iconCls: 'repo',

	items: [{
		xtype: 'formpanel',
		url: 'lendingRepoType.json',

		items: [
			{fieldLabel: 'Type Name', name: 'name', xtype: 'textfield', disabled: true},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', disabled: true},

			{fieldLabel: 'REPO Purpose', name: 'repoInvestmentAccountRelationshipPurpose.name', hiddenName: 'repoInvestmentAccountRelationshipPurpose.id', xtype: 'displayfield'},
			{fieldLabel: 'Tri-Party Purpose', name: 'triPartyInvestmentAccountRelationshipPurpose.name', hiddenName: 'triPartyInvestmentAccountRelationshipPurpose.id', xtype: 'displayfield'},

			{fieldLabel: 'Required Term', name: 'requiredTerm', xtype: 'textfield', disabled: true},
			{fieldLabel: '', boxLabel: 'Multiple interest rates per period', name: 'multipleInterestRatesPerPeriod', xtype: 'checkbox', disabled: true},
			{fieldLabel: '', boxLabel: 'Maturity date required for open', name: 'maturityDateRequiredForOpen', xtype: 'checkbox', disabled: true},
			{fieldLabel: '', boxLabel: 'Tri-Party', name: 'triPartyRepo', xtype: 'checkbox', disabled: true},
			{fieldLabel: '',boxLabel: 'Auto Pairoff',name:'autoPairoff', xtype: 'checkbox', disabled: true}
		]
	}]
});
