Clifton.lending.repo.RepoBondTradeExistingPositionsFormGrid = {
	xtype: 'formgrid',
	storeRoot: 'positionList',
	detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
	title: 'No existing positions found for selected bond',
	bindToFormLoad: false,
	readOnly: true,
	collapsible: true,
	collapsed: true,
	anchor: '-20',
	columnsConfig: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Collateral', width: 60, dataIndex: 'transaction.accountingAccount.collateral', type: 'boolean'},
		{header: 'Client Account', width: 260, dataIndex: 'transaction.clientInvestmentAccount.label', defaultSortColumn: true},
		{header: 'Holding Account', width: 260, dataIndex: 'transaction.holdingInvestmentAccount.label'},
		{header: 'Open Date', width: 80, dataIndex: 'transaction.originalTransactionDate'},
		{header: 'Open Price', width: 90, dataIndex: 'transaction.price', type: 'float', negativeInRed: true},
		{header: 'FX Rate', width: 90, dataIndex: 'transaction.exchangeRateToBase', type: 'currency', numberFormat: '0,000.0000000000', negativeInRed: true, hidden: true},
		{header: 'Current Face', width: 100, dataIndex: 'remainingQuantity', type: 'float', negativeInRed: true, summaryType: 'sum'}
	]
};

Clifton.lending.repo.BondRepoForm = Ext.extend(TCG.form.FormPanel, {
	xtype: 'formpanel',
	loadDefaultDataAfterRender: true,
	labelWidth: 100,
	defaults: {anchor: '0'},
	calendarName: 'US Banks Calendar',
	getDefaultData: function(win) {
		if (win.defaultDataIsReal) {
			return win.defaultData;
		}
		this.repoType = TCG.data.getData('lendingRepoTypeByName.json?name=Overnight', this, 'lending.repo.type.Overnight');
		const now = new Date().format('Y-m-d 00:00:00');
		return {
			tradeDate: now,
			settlementDate: now,
			traderUser: TCG.getCurrentUser(),
			current: true,
			repoDefinition: {
				type: this.repoType,
				dayCountConvention: 'Actual/360',
				active: true
			},
			...win.defaultData
		};
	},
	getBusinessDayFromDate: function(date, days, forceSkipCache) {
		const calendar = Clifton.calendar.getCalendarByName(this.calendarName);
		return Clifton.calendar.getBusinessDayFromDateForCalendar(date, days, calendar.id);
	},
	getBusinessDaysBetween: function(startDate, endDate) {
		const calendar = Clifton.calendar.getCalendarByName(this.calendarName);
		return Clifton.calendar.getBusinessDaysBetween(startDate, endDate, calendar.id);
	},
	isBusinessDay: function(date) {
		const calendar = Clifton.calendar.getCalendarByName(this.calendarName);
		return Clifton.calendar.isBusinessDayForCalendar(date, calendar.id);
	},
	updateRepoSetting: function(instrumentHierarchyId) {
		const fp = this;
		const f = this.getForm();

		const repoSetting = this.repoSetting = TCG.data.getData('lendingRepoSettingByInstrumentHierarchy.json?instrumentHierarchyId=' + instrumentHierarchyId, this, 'lending.repo.setting-' + instrumentHierarchyId);
		this.repoSetting = repoSetting;
		if (repoSetting) {
			if (repoSetting.daysToSettle) {
				const tradeDate = f.findField('tradeDate').getValue();
				const settlementDate = fp.getBusinessDayFromDate(tradeDate, repoSetting.daysToSettle);
				f.findField('settlementDate').setValue(settlementDate);
				fp.updateDateValues();
			}
			if (repoSetting.dayCountConvention) {
				const dayCount = {text: repoSetting.dayCountConvention, value: repoSetting.dayCountConvention, tooltip: ''};
				f.findField('repoDefinition.dayCountConvention').setValue(dayCount);
			}
		}
	},
	updateDateValues: function(config = {}) {
		const fp = this;
		const form = fp.getForm();
		const settlementDateField = form.findField('settlementDate');
		const tradeDate = config.tradeDate || form.findField('tradeDate').getValue();
		let settlementDate = config.settlementDate || settlementDateField.getValue();
		let daysToSettle;
		if (config.tradeDate) {
			if (this.repoSetting && this.repoSetting.daysToSettle) {
				settlementDate = fp.getBusinessDayFromDate(config.tradeDate, this.repoSetting.daysToSettle);
				daysToSettle = this.repoSetting.daysToSettle;
			}
			else {
				settlementDate = config.tradeDate;
				daysToSettle = 0;
			}
			settlementDateField.setValue(settlementDate);
		}

		// Update maturity dates based on terms only; ignore for open repos, whose terms do not provide maturity info
		const termField = form.findField('term');
		const maturityDateField = form.findField('maturityDate');
		const maturitySettlementDateField = form.findField('maturitySettlementDate');
		const term = parseInt(config.term || termField.getValue());
		const repoType = this.getRepoType();
		const multipleInterestRateRepo = repoType && repoType.multipleInterestRatesPerPeriod;
		if (TCG.isNotBlank(term) && settlementDate && !multipleInterestRateRepo) {
			let maturitySettlementDate = new Date(settlementDate);
			maturitySettlementDate.setDate(maturitySettlementDate.getDate() + term);
			if (fp.isBusinessDay(maturitySettlementDate) !== true) {
				maturitySettlementDate = fp.getBusinessDayFromDate(maturitySettlementDate, 1);
			}
			maturitySettlementDateField.setValue(maturitySettlementDate);

			const newTerm = TCG.dayDifference(maturitySettlementDate, settlementDateField.getValue());
			termField.setValue(newTerm);
		}

		// Infer maturity date from maturity settlement date
		daysToSettle = daysToSettle || fp.getBusinessDaysBetween(tradeDate, settlementDate);
		if (maturitySettlementDateField.getValue()) {
			const maturitySettlementDate = maturitySettlementDateField.getValue();
			const maturityDate = !daysToSettle ? maturitySettlementDate : fp.getBusinessDayFromDate(maturitySettlementDate, -1 * daysToSettle);
			maturityDateField.setValue(maturityDate);
		}

		fp.updateIndexRatio();
	},
	activateBondType: function(hierarchy) {
		const f = this.getForm();
		this.activateField(f, 'indexRatio', !!hierarchy.indexRatioAdjusted);
		try {
			this.doLayout();
		}
		catch (e) {
			// strange error due to removal of elements with allowBlank = false
		}
	},
	activateField: function(form, field, show) {
		const f = form.findField(field);
		f.setVisible(show);
		if (f.readOnly === false) {
			f.allowBlank = !show;
		}
		if (!show) {
			f.setValue('');
			f.originalValue = f.getValue();
		}
	},
	updateIndexRatio: function(force) {
		const f = this.getForm();
		const securityId = f.findField('repoDefinition.investmentSecurity.label').getValue();
		let indexRatio = 1;
		if (securityId && (force || f.findField('indexRatio').isVisible())) {
			const url = 'marketDataIndexRatioForSecurity.json?securityId=' + securityId + '&date=' + f.findField('settlementDate').getValue().format('m/d/Y');
			indexRatio = TCG.getResponseText(url, this);
		}
		this.setFormValue('indexRatio', indexRatio, true);
	},
	parseValue: function(value) {
		value = parseFloat(this.removeFormattingChars(String(value).replace(this.decimalSeparator, '.')));
		return isNaN(value) ? '' : value;
	},
	removeFormattingChars: function(value) {
		return value.replace(/,/g, '');
	},
	updateMarketValue: function(config = {}) {
		const fp = this;
		const f = fp.getForm();

		const maturityDateField = f.findField('maturityDate');
		const securityId = f.findField('repoDefinition.investmentSecurity.id').getValue();
		const settlementDate = f.findField('settlementDate').getValue();
		const term = f.findField('term').getNumericValue();
		const rate = f.findField('interestRate').getNumericValue();
		const dayCountConvention = f.findField('repoDefinition.dayCountConvention').getValue();

		if (!config.originalFace) {
			config.originalFace = f.findField('originalFace').getNumericValue();
		}
		if (!config.price) {
			config.price = f.findField('price').getNumericValue();
		}
		if (!config.haircutPercent) {
			config.haircutPercent = f.findField('haircutPercent').getNumericValue();
		}
		if (!config.indexRatio) {
			const ir = f.findField('indexRatio').getNumericValue();
			config.indexRatio = ir ? ir : 1;
		}

		let marketValue = 0;
		const security = securityId
			? TCG.data.getData(`investmentSecurity.json?id=${securityId}`, fp)
			: null;
		if (!security) {
			f.findField('marketValue').setValue(null);
		}
		else if (config.originalFace && config.price) {
			// support inflation adjusted bond whose price is adjusted for the index ratio
			const purchaseFace = config.originalFace * (security.instrument.hierarchy.notionalMultiplierForPriceNotUsed !== true ? config.indexRatio : 1);
			const notional = parseFloat(fp.parseValue(TCG.getResponseText('investmentSecurityNotional.json?securityId=' + securityId + '&price=' + config.price + '&quantity=' + config.originalFace + '&notionalMultiplier=' + config.indexRatio, this)));
			let url = 'investmentSecurityAccruedInterest.json?securityId=' + securityId + '&notional=' + purchaseFace + '&date=' + settlementDate.format('m/d/Y');
			if (security.instrument.hierarchy.notionalMultiplierForPriceNotUsed === true) {
				url += '&notionalMultiplier=' + config.indexRatio;
			}
			const ai = parseFloat(fp.parseValue(TCG.getResponseText(url, this)));
			marketValue = notional + (TCG.isNotBlank(ai) ? ai : 0);

			const marketValueField = f.findField('marketValue');
			marketValueField.setValue(marketValue);
		}
		if (config.haircutPercent && rate && dayCountConvention && settlementDate) {
			const netCashField = f.findField('netCash');
			const netCash = config.haircutPercent > 100 ? marketValue / (config.haircutPercent / 100) : marketValue * (config.haircutPercent / 100);
			netCashField.setValue(netCash);

			// Estimate interest for a single day if a term is not provided
			let effectiveTerm = term || 1;
			if (maturityDateField.allowBlank) {
				const endDate = fp.getBusinessDayFromDate(settlementDate, effectiveTerm);
				effectiveTerm = TCG.dayDifference(endDate, settlementDate);
			}
			const interest = parseFloat(fp.parseValue(TCG.getResponseText(`investmentInterestForDays.json?&notional=${netCash}&rate=${rate}&startDate=${settlementDate.format('m/d/Y')}&days=${effectiveTerm}&dayCountConventionKey=${dayCountConvention}`, this)));
			const interestField = f.findField('interestAmount');
			interestField.setValue(interest);

			const totalRepoAmountField = f.findField('totalRepoAmount');
			totalRepoAmountField.setValue(interest + netCash);
			totalRepoAmountField.originalValue = totalRepoAmountField.getValue(); // Prevent dirty-checking
		}
	},

	updateAvailablePosition: function() {
		const fp = this;
		const f = fp.getForm();

		const securityId = f.findField('repoDefinition.investmentSecurity.label').getValue();
		const clientInvestmentAccountId = f.findField('repoDefinition.clientInvestmentAccount.label').getValue();
		const holdingInvestmentAccountId = f.findField('repoDefinition.holdingInvestmentAccount.label').getValue();
		const settlementDate = f.findField('settlementDate').getValue();
		const availablePositionField = f.findField('availablePosition');

		if (securityId && clientInvestmentAccountId && holdingInvestmentAccountId && settlementDate) {
			// load the available position
			const loader = new TCG.data.JsonLoader({
				params: {
					investmentSecurityId: securityId,
					clientInvestmentAccountId: clientInvestmentAccountId,
					holdingInvestmentAccountId: holdingInvestmentAccountId,
					transactionDate: settlementDate.format('m/d/Y')
				},
				onLoad: function(record, conf) {
					let remainingQuantity = 0;
					if (record) {
						for (let i = 0; i < record.length; i++) {
							remainingQuantity += record[i].remainingQuantity;
						}
					}
					availablePositionField.setValue(remainingQuantity);
				}
			});
			loader.load('accountingPositionList.json', fp);
		}
	},

	getRepoType: function() {
		// Guard-clause: No repo is requested or repo type is already set
		const repoTypeId = this.getFormValue('type.id', true);
		if (!repoTypeId || (this.repoType && this.repoType.id === repoTypeId)) {
			return this.repoType;
		}

		// Load repo type
		this.repoType = TCG.data.getData(`lendingRepoType.json?id=${repoTypeId}`, this, `lending.repo.type.${repoTypeId}`);
		return this.repoType;
	},

	commonEntryItems: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .35,
					layout: 'form',
					labelWidth: 100,
					defaults: {xtype: 'textfield', width: 175},
					items: [
						{
							fieldLabel: 'Type', name: 'type.name', hiddenName: 'type.id',
							xtype: 'combo', url: 'lendingRepoTypeListFind.json',
							detailPageClass: 'Clifton.lending.repo.RepoTypeWindow',
							limitRequestedProperties: false, allowBlank: false, loadAll: true,
							listeners: {
								select: function(combo, record, index) {
									const fp = combo.getParentForm();
									fp.repoType = record.json;
									fp.configureMaturityDateField(true);
									fp.configureTriPartyField();
									fp.configureTermField();
									fp.configureCancellationNoticeTerm();
									fp.updateDateValues();
								}
							}
						}, {
							fieldLabel: 'Bond', name: 'repoDefinition.investmentSecurity.label', hiddenName: 'repoDefinition.investmentSecurity.id', displayField: 'label',
							xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=false&investmentType=Bonds', queryParam: 'searchPatternUsingBeginsWith',
							detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							limitRequestedProperties: false, allowBlank: false,
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									const hierarchyId = f.findField('repoDefinition.instrumentHierarchy.name').getValue();
									combo.store.baseParams = hierarchyId ? {hierarchyId: hierarchyId, activeOnDate: f.findField('tradeDate').value} : {activeOnDate: f.findField('tradeDate').value};
								},
								select: function(combo, record, index) {
									const fp = combo.getParentForm();
									const f = fp.getForm();
									if (!f.formValues.repoDefinition) {
										f.formValues.repoDefinition = {};
									}
									f.formValues.repoDefinition.instrumentHierarchy = record.json.instrument.hierarchy; // makes bond type selection stay

									const type = f.findField('repoDefinition.instrumentHierarchy.name');
									if (!type.getValue()) {
										const h = record.json.instrument.hierarchy;
										type.setValue({text: h.name, value: h.id});

										fp.activateBondType(h);
									}

									fp.updateRepoSetting(record.json.instrument.hierarchy.id);
									fp.updateIndexRatio();
									fp.updateMarketValue();
									fp.updateAvailablePosition();
								}
							}
						},
						{
							fieldLabel: 'Bond Type', name: 'repoDefinition.instrumentHierarchy.name', hiddenName: 'repoDefinition.instrumentHierarchy.id',
							xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json?investmentTypeName=Bonds', queryParam: 'labelExpanded',
							requestedProps: 'indexRatioAdjusted', // Include index ratio adjusted flag for processing
							detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
							allowBlank: false, listWidth: 600,
							listeners: {
								beforeselect: function(combo, record) {
									const fp = combo.getParentForm();
									const f = fp.getForm();
									const s = f.findField('repoDefinition.investmentSecurity.label');
									s.store.removeAll();
									s.lastQuery = null;
									s.store.baseParams = {hierarchyId: record.id};
									// default hierarchy for add new
									s.getDefaultData = () => ({instrument: {hierarchy: record.json}});
									s.clearValue();
								},
								select: function(combo, record) {
									const fp = combo.getParentForm();
									fp.activateBondType(record.json);
									fp.updateRepoSetting(record.id);
								}
							}
						},
						{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', detailPageClass: 'Clifton.security.user.UserWindow', allowBlank: false},
						{fieldLabel: 'Reverse REPO', name: 'repoDefinition.reverse', xtype: 'checkbox'}
					]
				},

				{
					columnWidth: .34,
					layout: 'form',
					labelWidth: 110,
					defaults: {xtype: 'textfield', width: 170},
					items: [
						{
							fieldLabel: 'Term (days)', name: 'term', xtype: 'integerfield', allowDecimals: false, maxValue: 1000000,
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = field.getParentFormPanel();
									fp.updateDateValues({term: newValue});
									fp.updateMarketValue();
								}
							}
						},
						{fieldLabel: 'Cancellation Term', name: 'cancellationNoticeTerm', xtype: 'integerfield', allowDecimals: false, maxValue: 1000000, enabled: false, allowBlank: true, hidden: true, qtip: 'Number of days notice required to cancel the REPO.'},
						{
							fieldLabel: 'Interest Rate (%)', name: 'interestRate', xtype: 'floatfield',
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = field.getParentFormPanel();
									fp.updateMarketValue();
								}
							}
						},
						{
							fieldLabel: 'Day Count', name: 'repoDefinition.dayCountConvention', loadAll: true, mode: 'local',
							allowBlank: false, xtype: 'combo',
							valueField: 'value',
							displayField: 'text',
							tooltipField: 'tooltip',
							url: 'systemListItemListFind.json?listName=Bond Interest Calculation Types'
						},
						{fieldLabel: '', boxLabel: 'REPO definition is active', name: 'repoDefinition.active', xtype: 'checkbox', readOnly: true, disabled: true, alwaysReadOnly: true},
						{fieldLabel: '', boxLabel: 'REPO is a roll', name: 'roll', xtype: 'checkbox', readOnly: true, disabled: true, alwaysReadOnly: true}

					]
				},

				{
					columnWidth: .31,
					layout: 'form',
					labelWidth: 150,
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{
							fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = field.getParentFormPanel();
									fp.updateDateValues({tradeDate: newValue});
									fp.updateMarketValue();
								}
							}
						},
						{
							fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false,
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = field.getParentFormPanel();
									fp.updateDateValues({settlementDate: newValue});
									fp.updateMarketValue();
									fp.updateAvailablePosition();
								}
							}
						},
						{xtype: 'accounting-bookingdate', name: 'bookingDate', sourceTable: 'LendingRepo'},
						{fieldLabel: 'Maturity Date', name: 'maturityDate', xtype: 'datefield', allowBlank: false},
						{
							fieldLabel: 'Maturity Settlement Date', name: 'maturitySettlementDate', xtype: 'datefield', allowBlank: false,
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = field.getParentFormPanel();
									const form = fp.getForm();
									const settlementDate = form.findField('settlementDate').getValue();
									const newTerm = settlementDate && newValue
										? TCG.dayDifference(newValue, settlementDate)
										: null;
									fp.updateDateValues({term: newTerm});
									fp.updateMarketValue();
								}
							}
						},
						{
							xtype: 'accounting-bookingdate',
							fieldLabel: 'Maturity Booking Date',
							name: 'maturityBookingDate',
							sourceTable: 'LendingRepo',
							journalSequence: 2
						}
					]
				}]
		},

		{xtype: 'label', html: '<hr/>'},
		{
			fieldLabel: 'Client Account', name: 'repoDefinition.clientInvestmentAccount.label', hiddenName: 'repoDefinition.clientInvestmentAccount.id', displayField: 'label',
			xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active&relatedPurpose=REPO',
			detailPageClass: 'Clifton.investment.account.AccountWindow',
			limitRequestedProperties: false, allowBlank: false, anchor: '-20',
			listeners: {
				beforeselect: function(combo, record) {
					const fp = combo.getParentForm();
					const f = fp.getForm();
					const ha = f.findField('repoDefinition.holdingInvestmentAccount.label');
					const relatedPurpose = fp.repoType.triPartyRepo ? fp.repoType.triPartyInvestmentAccountRelationshipPurpose.name : fp.repoType.repoInvestmentAccountRelationshipPurpose.name;

					ha.store.removeAll();
					ha.lastQuery = null;
					ha.store.baseParams = {
						mainAccountId: record.id,
						ourAccount: false,
						workflowStatusNameEquals: 'Active',
						relatedPurpose: relatedPurpose,
						relatedPurposeActiveOnDate: f.findField('tradeDate').value,
						excludeAccountType: 'Tri-Party REPO'
					};
					ha.store.on('load', function(store, records) {
						if (records.length === 1) {
							const r = records[0];
							ha.setValue({value: r.get('id'), text: r.get('label')});
							ha.updateAccounts(ha, r);
							fp.updateAvailablePosition();
						}
					});
					ha.doQuery('');

					const cp = f.findField('repoDefinition.counterparty.label');
					cp.store.removeAll();
					cp.lastQuery = null;
					cp.setValue('');
					cp.store.baseParams = {
						contractInvestmentAccountId: record.id
					};
					cp.store.on('load', function(store, records) {
						if (records.length === 1) {
							const r = records[0];
							cp.setValue({value: r.get('id'), text: r.get('label')});
							cp.updateRepoAccount(cp, r);
						}
					});
					cp.doQuery('');
				},
				select: function(combo, record) {
					const fp = combo.getParentForm();

					fp.updateAvailablePosition();
				}
			}
		},
		{
			fieldLabel: 'Holding Account', name: 'repoDefinition.holdingInvestmentAccount.label', hiddenName: 'repoDefinition.holdingInvestmentAccount.id', displayField: 'label',
			xtype: 'combo', url: 'investmentAccountListFind.json',
			detailPageClass: 'Clifton.investment.account.AccountWindow',
			allowBlank: false, anchor: '-20', requiredFields: ['repoDefinition.clientInvestmentAccount.label'],
			qtip: 'Limits holding accounts to related accounts for selected client account and those that have an active "REPO Account" relationship.',
			selectHandler: function(combo, record, index) {
				const fp = combo.getParentForm();

				fp.updateAvailablePosition();
			},
			updateAccounts: function(combo, record) {
				const fp = combo.getParentForm();
				const f = fp.getForm();
				const tradeDate = f.findField('tradeDate').value;
				let ra, cp = undefined;
				if (fp.repoType.triPartyRepo) {
					ra = f.findField('repoDefinition.triPartyInvestmentAccount.label');
				}
				else {
					ra = f.findField('repoDefinition.repoInvestmentAccount.label');
					cp = f.findField('repoDefinition.counterparty.label');
				}

				const relatedPurpose = fp.repoType.triPartyRepo ? fp.repoType.repoInvestmentAccountRelationshipPurpose.name : undefined;
				const mainPurpose = fp.repoType.triPartyRepo ? fp.repoType.triPartyInvestmentAccountRelationshipPurpose.name : fp.repoType.repoInvestmentAccountRelationshipPurpose.name;

				ra.store.removeAll();
				ra.lastQuery = null;
				ra.store.baseParams = {
					mainAccountId: record.id,
					ourAccount: false,
					workflowStatusNameEquals: 'Active',
					relatedPurpose: relatedPurpose,
					relatedPurposeActiveOnDate: TCG.isNotNull(relatedPurpose) ? tradeDate : undefined,

					mainPurpose: mainPurpose,
					mainPurposeActiveOnDate: TCG.isNotNull(mainPurpose) ? tradeDate : undefined,

					issuingCompanyId: cp ? cp.getValue() : undefined
				};
				ra.store.on('load', function(store, records) {
					if (records.length === 1) {
						const r = records[0];
						ra.setValue({value: r.get('id'), text: r.get('label')});
						if (ra.updateRepoAccount) {
							ra.updateRepoAccount(ra, r);
						}
						else {
							ra.selectHandler(ra, r);
						}
					}
				});
				ra.doQuery('');
			},
			listeners: {
				beforeselect: function(combo, record) {
					combo.updateAccounts(combo, record);
				}
			}
		},
		{
			fieldLabel: 'Tri-Party Account', name: 'repoDefinition.triPartyInvestmentAccount.label', hiddenName: 'repoDefinition.triPartyInvestmentAccount.id', displayField: 'label',
			xtype: 'combo', url: 'investmentAccountListFind.json',
			detailPageClass: 'Clifton.investment.account.AccountWindow',
			allowBlank: false, anchor: '-20', requiredFields: ['repoDefinition.clientInvestmentAccount.label'],
			updateRepoAccount: function(combo, record) {
				const fp = combo.getParentForm();
				const f = fp.getForm();
				const ra = f.findField('repoDefinition.repoInvestmentAccount.label');

				ra.store.removeAll();
				ra.lastQuery = null;
				ra.store.baseParams = {
					mainAccountId: record.id,
					ourAccount: false,
					workflowStatusNameEquals: 'Active',
					mainPurpose: fp.repoType.repoInvestmentAccountRelationshipPurpose.name,
					mainPurposeActiveOnDate: f.findField('tradeDate').value
				};
				ra.store.on('load', function(store, records) {
					if (records.length === 1) {
						const r = records[0];
						ra.setValue({value: r.get('id'), text: r.get('label')});
						ra.selectHandler(ra, r);
					}
				});
				ra.doQuery('');
			},
			listeners: {
				beforeselect: function(combo, record) {
					combo.updateRepoAccount(combo, record);
				}
			}
		},
		{
			fieldLabel: 'REPO Account', name: 'repoDefinition.repoInvestmentAccount.label', hiddenName: 'repoDefinition.repoInvestmentAccount.id', displayField: 'label',
			xtype: 'combo', url: 'investmentAccountListFind.json',
			detailPageClass: 'Clifton.investment.account.AccountWindow',
			allowBlank: false, anchor: '-20', requiredFields: ['repoDefinition.clientInvestmentAccount.label'],
			selectHandler: function(combo, record, index) {
				const fp = combo.getParentForm();
				const f = fp.getForm();
				const cp = f.findField('repoDefinition.counterparty.label');

				cp.setValue({value: record.json.issuingCompany.id, text: record.json.issuingCompany.label});
			}
		},
		{
			fieldLabel: 'Counterparty', name: 'repoDefinition.counterparty.label', hiddenName: 'repoDefinition.counterparty.id', displayField: 'label',
			xtype: 'combo', url: 'businessCompanyListFind.json?contractTypeName=Mast Repurchase Agreement&contractPartyRoleName=Counterparty&issuer=true',
			detailPageClass: 'Clifton.business.company.CompanyWindow',
			allowBlank: false, anchor: '-20'
		},

		{xtype: 'label', html: '<hr/>'},
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .35,
					layout: 'form',
					labelWidth: 100,
					defaults: {xtype: 'textfield', width: 175},
					items: [
						{fieldLabel: 'Available Position', name: 'availablePosition', xtype: 'currencyfield', readOnly: true, submitValue: false},
						{
							fieldLabel: 'Original Face', name: 'originalFace', xtype: 'currencyfield',
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							onChangeHandler: function(field, newValue, oldValue) {
								const fp = field.getParentFormPanel();
								const form = fp.getForm();

								const quantityIntendedField = form.findField('quantityIntended');
								quantityIntendedField.setValue(newValue.replace(/,/g, ''));

								fp.updateMarketValue({originalFace: newValue.replace(/,/g, '')});
							}
						},
						{fieldLabel: 'Quantity Intended', name: 'quantityIntended', xtype: 'currencyfield', hidden: true},
						{
							fieldLabel: 'Price', name: 'price', xtype: 'floatfield',
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							onChangeHandler: function(field, newValue, oldValue) {
								const fp = field.getParentFormPanel();
								fp.updateMarketValue({price: newValue.replace(/,/g, '')});
							}
						},
						{
							fieldLabel: 'Index Ratio', name: 'indexRatio', xtype: 'floatfield', allowBlank: false,
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							onChangeHandler: function(field, newValue, oldValue) {
								const fp = field.getParentFormPanel();
								fp.updateMarketValue({indexRatio: newValue.replace(/,/g, '')});
							}
						}
					]
				},

				{
					columnWidth: .34,
					layout: 'form',
					labelWidth: 110,
					defaults: {xtype: 'textfield', width: 170},
					items: [
						{fieldLabel: 'Market Value', name: 'marketValue', xtype: 'currencyfield', readOnly: true},
						{
							fieldLabel: 'Haircut %', name: 'haircutPercent', xtype: 'floatfield', qtip: 'Net Cash = (Haircut Percent > 100) ? (100 * Market Value / Haircut Percent) : (Market Value * Haircut Percent / 100)',
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							onChangeHandler: function(field, newValue, oldValue) {
								const fp = field.getParentFormPanel();
								fp.updateMarketValue({haircutPercent: newValue.replace(/,/g, '')});
							}
						}
					]
				},

				{
					columnWidth: .31,
					layout: 'form',
					labelWidth: 130,
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Net Cash', name: 'netCash', xtype: 'currencyfield', readOnly: true, submitValue: false},
						{fieldLabel: 'Interest Amount', name: 'interestAmount', xtype: 'floatfield', readOnly: true, submitValue: false},
						{fieldLabel: 'Total', name: 'totalRepoAmount', xtype: 'currencyfield', readOnly: true, submitValue: false}
					]
				}
			]
		},

		{xtype: 'label', html: '<hr/>'},
		{fieldLabel: 'REPO Note', name: 'description', xtype: 'textarea', height: 50, anchor: '-20'}
	]
});
Ext.reg('repo-bond-form', Clifton.lending.repo.BondRepoForm);

Clifton.lending.repo.BondRepoWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'REPO',
	iconCls: 'shopping-cart',
	width: 1000,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,
		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.lending.repo.DefaultRepoWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.lending.repo.DefaultRepoWindowAdditionalTabs[i]);
				}
			}
		},
		items: [
			{
				title: 'REPO Current Term',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'LendingRepo',
					finalState: 'Canceled',
					additionalSubmitParameters: {
						requestedMaxDepth: 5,
						requestedPropertiesRoot: 'data',
						requestedPropertiesToExclude: 'interestRateList'
					}
				},
				items: [{
					xtype: 'repo-bond-form',
					url: 'lendingRepo.json?requestedMaxDepth=5&requestedPropertiesToExclude=interestRateList',
					getSaveURL: function() {
						return 'lendingRepoSave.json?requestedMaxDepth=5&requestedPropertiesToExclude=interestRateList';
					},

					labelFieldName: 'id',

					listeners: {
						beforerender: function() {
							const status = TCG.getValue('workflowStatus.name', this.getWindow().defaultData);
							this.readOnlyMode = (status && status !== 'Draft');
							if (this.readOnlyMode) {
								this.readOnlyMode = (this.getLoadURL() !== false);
							}
							if (this.readOnlyMode) {
								this.add(this.readOnlyItems);
							}
							else {
								this.add(this.commonEntryItems);
								this.add(this.additionalEntryItems);
							}

							const state = TCG.getValue('workflowState.name', this.getWindow().defaultData);
							this.configureMaturityDateField(false, state);
							this.configureTriPartyField();
							this.configureTermField();
						},
						afterload: function() {
							const ro = ('Draft' !== this.getFormValue('workflowStatus.name', true));
							this.setReadOnlyMode(ro);

							// Update total repo amount
							const form = this.getForm();
							const interestAmount = form.findField('interestAmount').getNumericValue() || 0;
							const netCashAmount = form.findField('netCash').getNumericValue() || 0;
							const totalRepoAmountField = form.findField('totalRepoAmount');
							totalRepoAmountField.setValue(interestAmount + netCashAmount);
							totalRepoAmountField.originalValue = totalRepoAmountField.getValue(); // Prevent dirty-checking

							const state = this.getFormValue('workflowState.name', true);
							this.setFormValue('bookingDate', this.getForm().formValues.bookingDate, true);
							this.setFormValue('maturityBookingDate', this.getForm().formValues.maturityBookingDate, true);
							this.configureMaturityDateField(false, state);
							this.configureTriPartyField();
							this.configureTermField();
							this.configureCancellationNoticeTerm();
						}
					},
					configureTriPartyField: function() {
						const fp = this;
						const repoType = fp.getRepoType();
						const triPartyField = fp.getForm().findField('repoDefinition.triPartyInvestmentAccount.label');

						if (repoType && TCG.isFalse(repoType.triPartyRepo)) {
							triPartyField.hide();
							if (triPartyField.isXType('combo')) {
								triPartyField.clearAndReset();
							}
							triPartyField.allowBlank = true;
						}
						else {
							triPartyField.show();
							triPartyField.allowBlank = false;
						}
						// Check and update validity state
						fp.getForm().isValid();
					},
					configureTermField: function() {
						const fp = this;
						const repoType = fp.getRepoType();
						const termField = fp.getForm().findField('term');
						if (repoType && repoType.requiredTerm) {
							termField.setValue(repoType.requiredTerm);
							termField.setDisabled(true);
						}
						else if (repoType && repoType.multipleInterestRatesPerPeriod) {
							termField.setValue(1); // Term is statically set to 1 for multi-interest rate repo types
							termField.setDisabled(true);
						}
						else {
							termField.setDisabled(false);
						}
						termField.allowBlank = !(repoType && repoType.maturityDateRequiredForOpen);
						// Check and update validity state
						fp.getForm().isValid();
					},
					configureCancellationNoticeTerm: function() {
						const fp = this;
						const type = fp.getRepoType();
						const cancellationNoticeTermField = fp.getForm().findField('cancellationNoticeTerm');

						if (type && !TCG.isTrue(type.cancellationNoticeTermRequired)) {
							cancellationNoticeTermField.hide();
							cancellationNoticeTermField.setValue(null);
							cancellationNoticeTermField.allowBlank = true;
						}
						else {
							cancellationNoticeTermField.show();
							cancellationNoticeTermField.allowBlank = false;
						}
						// Check and update validity state
						fp.getForm().isValid();
					},
					configureMaturityDateField: function(clearDate, state) {
						const fp = this;
						const repoType = fp.getRepoType();
						const maturityDateField = fp.getForm().findField('maturityDate');
						const maturitySettlementDateField = fp.getForm().findField('maturitySettlementDate');
						const interestRateField = fp.getForm().findField('interestRate');

						if (repoType && TCG.isFalse(repoType.maturityDateRequiredForOpen)) {
							maturityDateField.allowBlank = true;
							maturitySettlementDateField.allowBlank = true;
							interestRateField.allowBlank = true;
							if (clearDate) {
								maturityDateField.setValue('');
								maturitySettlementDateField.setValue('');
							}

							if (state && state === 'Active Repo') {
								maturityDateField.setReadOnly(false);
								maturitySettlementDateField.setReadOnly(false);
							}
						}
						else {
							maturityDateField.allowBlank = false;
							maturitySettlementDateField.allowBlank = false;
							interestRateField.allowBlank = false;
						}
						// Check and update validity state
						fp.getForm().isValid();
						try {
							fp.doLayout();
						}
						catch (e) {
							// strange error due to removal of elements with allowBlank = false
						}
					},
					getWarningMessage: function(form) {
						let msg = undefined;
						if (form.formValues) {
							if (form.formValues.workflowState.name === 'Invalid' || form.formValues.workflowState.name === 'Executed Invalid') {
								msg = 'This trade failed compliance check(s). See <b>Compliance</b> tab for more information.';
							}
						}
						return msg;
					},

					readOnlyMode: true,
					setReadOnlyMode: function(ro) {
						if (this.readOnlyMode === ro) {
							return;
						}
						this.readOnlyMode = ro;
						this.removeAll(true);
						if (this.readOnlyMode) {
							this.add(this.readOnlyItems);
						}
						else {
							this.add(this.commonEntryItems);
							this.add(this.additionalEntryItems);
						}
						try {
							this.doLayout();
						}
						catch (e) {
							// strange error due to removal of elements with allowBlank = false
						}
						const f = this.getForm();
						f.setValues(f.formValues);
						this.loadValidationMetaData(true);
						f.isValid();
					},

					additionalEntryItems: [],
					readOnlyItems: [
						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .35,
									layout: 'form',
									labelWidth: 100,
									items: [
										{fieldLabel: 'REPO Type', name: 'type.name', xtype: 'linkfield', detailPageClass: 'Clifton.lending.repo.RepoTypeWindow', detailIdField: 'type.id', submitValue: false},
										{fieldLabel: 'Bond', name: 'repoDefinition.investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'repoDefinition.investmentSecurity.id', submitValue: false},
										{fieldLabel: 'Bond Type', name: 'repoDefinition.instrumentHierarchy.label', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'repoDefinition.instrumentHierarchy.id', xtype: 'linkfield', submitValue: false},
										{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow', submitValue: false},
										{fieldLabel: 'Reverse REPO', name: 'repoDefinition.reverse', xtype: 'checkbox', readOnly: true, disabled: true, submitValue: false}
									]
								},

								{
									columnWidth: .34,
									layout: 'form',
									labelWidth: 110,
									defaults: {xtype: 'textfield', width: 170},
									items: [
										{fieldLabel: 'Term (days)', name: 'term', xtype: 'integerfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Cancellation Term', name: 'cancellationNoticeTerm', xtype: 'integerfield', readOnly: true, submitValue: false, hidden: true, qtip: 'Number of days notice required to cancel the REPO.'},
										{fieldLabel: 'Interest Rate (%)', name: 'interestRate', xtype: 'floatfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Day Count', name: 'repoDefinition.dayCountConvention', readOnly: true, submitValue: false},
										{fieldLabel: '', boxLabel: 'REPO definition is active', name: 'repoDefinition.active', xtype: 'checkbox', readOnly: true, disabled: true, alwaysReadOnly: true, submitValue: false},
										{fieldLabel: '', boxLabel: 'REPO is a roll', name: 'roll', xtype: 'checkbox', readOnly: true, disabled: true, alwaysReadOnly: true, submitValue: false}

									]
								},

								{
									columnWidth: .31,
									layout: 'form',
									labelWidth: 150,
									defaults: {xtype: 'textfield', anchor: '-20'},
									items: [
										{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true, submitValue: false},
										{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true, submitValue: false},
										{xtype: 'accounting-bookingdate', name: 'bookingDate', sourceTable: 'LendingRepo'},
										{fieldLabel: 'Action Date', name: 'maturityDate', xtype: 'datefield', readOnly: true},
										{fieldLabel: 'Maturity Date', name: 'maturitySettlementDate', xtype: 'datefield', readOnly: true},
										{
											xtype: 'accounting-bookingdate',
											fieldLabel: 'Maturity Booking Date',
											name: 'maturityBookingDate',
											sourceTable: 'LendingRepo',
											journalSequence: 2
										}
									]
								}
							]
						},

						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Client Account', name: 'repoDefinition.clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'repoDefinition.clientInvestmentAccount.id', submitValue: false},
						{fieldLabel: 'Holding Account', name: 'repoDefinition.holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'repoDefinition.holdingInvestmentAccount.id', submitValue: false},
						{fieldLabel: 'Tri-Part Account', name: 'repoDefinition.triPartyInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'repoDefinition.triPartyInvestmentAccount.id', submitValue: false},
						{fieldLabel: 'REPO Account', name: 'repoDefinition.repoInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'repoDefinition.repoInvestmentAccount.id', submitValue: false},
						{fieldLabel: 'Counterparty', name: 'repoDefinition.counterparty.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'repoDefinition.counterparty.id', submitValue: false},

						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .35,
									layout: 'form',
									labelWidth: 100,
									defaults: {xtype: 'textfield', width: 175},
									items: [
										{fieldLabel: 'Original Face', name: 'originalFace', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Price', name: 'price', xtype: 'floatfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Index Ratio', name: 'indexRatio', xtype: 'floatfield', readOnly: true, submitValue: false}
									]
								},

								{
									columnWidth: .34,
									layout: 'form',
									labelWidth: 110,
									defaults: {xtype: 'textfield', width: 170},
									items: [
										{fieldLabel: 'Market Value', name: 'marketValue', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Haircut %', name: 'haircutPercent', xtype: 'floatfield', readOnly: true, submitValue: false, qtip: 'Net Cash = (Haircut Percent > 100) ? (100 * Market Value / Haircut Percent) : (Market Value * Haircut Percent / 100)'}
									]
								},

								{
									columnWidth: .31,
									layout: 'form',
									labelWidth: 130,
									defaults: {xtype: 'textfield', anchor: '-20'},
									items: [
										{fieldLabel: 'Net Cash', name: 'netCash', xtype: 'currencyfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Interest Amount', name: 'interestAmount', xtype: 'floatfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Total', name: 'totalRepoAmount', xtype: 'currencyfield', readOnly: true, submitValue: false}
									]
								}]
						},

						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'REPO Note', name: 'description', xtype: 'textarea', height: 50, anchor: '-20', readOnly: true, submitValue: false}
					]
				}]
			},


			{
				title: 'Previous REPO Terms',
				items: [{
					xtype: 'lending-repoGrid',
					columnOverrides: [
						{dataIndex: 'id', hidden: false},
						{dataIndex: 'repoDefinition.investmentSecurity.symbol', hidden: true},
						{dataIndex: 'repoDefinition.clientInvestmentAccount.label', hidden: true},
						{dataIndex: 'repoDefinition.counterparty.label', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						const settlementDate = TCG.parseDate(this.getWindow().getMainForm().formValues.settlementDate).format('m/d/Y');
						return {
							repoDefinitionId: this.getWindow().getMainForm().formValues.repoDefinition.id,
							excludeRepoDetailId: this.getWindow().getMainForm().formValues.id,
							beforeSettlementDate: settlementDate
						};
					},
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow'
					}
				}]
			},


			{
				title: 'Compliance',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'LendingRepo'
				}]
			},


			{
				title: 'Interest Rates',
				items: [{
					xtype: 'lending-repoInterestRateGrid',
					submitRateDate: true,
					getTopToolbarFilters: function(toolbar) {
						// do nothing so default filters don't show
					},
					isPagingEnabled: function() {
						return false;
					},
					getLoadParams: function(firstLoad) {
						return {
							repoId: this.getWindow().getMainFormId()
						};
					},
					getNewRowDefaults: function() {
						return {repo: this.getWindow().getMainForm().formValues};
					},
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow',
						addEnabled: true,
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add({
								text: 'Add',
								tooltip: 'Add a new item',
								iconCls: 'add',
								scope: gridPanel,
								handler: function() {
									const store = this.grid.getStore();
									const rowClass = store.recordType;
									this.grid.stopEditing();

									const defaults = this.getNewRowDefaults();
									const row = new rowClass();
									for (let i = 0; i < this.columns.length; i++) {
										const column = this.columns[i];
										row.set(column.dataIndex, TCG.getValue(column.dataIndex, defaults));
									}
									row.set('repo.id', TCG.getValue('repo.id', defaults));

									store.insert(0, row);
									this.grid.startEditing(0, 0);
								}
							});
							toolBar.add('-');
						},
						addEditButtons: function(t, gridPanel) {
							t.add({
								text: 'Save',
								tooltip: '',
								iconCls: 'disk',
								handler: function() {
									gridPanel.saveInterestRates();
								}
							});
							t.add('-');

							TCG.grid.GridEditor.prototype.addEditButtons.call(this, t);
						},
						getDetailPageId: (gridPanel, row) => row.data['repo.id']
					},
					plugins: {ptype: 'gridsummary'}
				}]
			},


			{
				title: 'REPO Instructions',
				items: [{
					xtype: 'investment-instruction-item-grid',
					tableName: 'LendingRepo',
					defaultCategoryName: 'REPO Counterparty',
					getEntityId: function() {
						return TCG.getValue('id', this.getWindow().getMainForm().formValues);
					},
					getInstructionDate: function() {
						return TCG.getValue('tradeDate', this.getWindow().getMainForm().formValues);
					}
				}]
			}
		]
	}]
});
