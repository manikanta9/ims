Clifton.lending.repo.RepoConfirmationWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Confirm Repo',
	iconCls: 'repo',
	height: 250,
	modal: true,
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelWidth: 100,
		instructions: 'Enter a comment explaining how you confirmed the REPO.',
		items: [
			{name: 'ids', xtype: 'hidden'},
			{fieldLabel: 'Note', name: 'confirmationNote', xtype: 'textarea', allowBlank: true}
		],
		getSaveURL: function() {
			return 'lendingRepoConfirm.json?requestedMaxDepth=5';
		}
	}]
});
