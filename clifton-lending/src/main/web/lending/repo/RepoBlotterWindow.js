Clifton.lending.repo.RepoBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'repoBlotterWindow',
	title: 'REPO Blotter',
	iconCls: 'repo',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'All REPOs',
				items: [{
					xtype: 'lending-repoGrid',
					instructions: 'Lists REPO details for all REPOs. Defaults REPOs to today\'s and future REPOs for all users.',
					columnOverrides: [
						{dataIndex: 'maturityDate', hidden: false}
					],
					configureToolsMenu: function(menu) {
						const gridPanel = this;
						menu.add('-');
						menu.add({
							text: 'Preview Instruction Message',
							tooltip: 'Preview Instruction SWIFT Message',
							iconCls: 'swift',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								const repos = sm.getSelections();
								if (sm.getCount() !== 1) {
									TCG.showError('Please select a single REPO to preview.', 'Incorrect Selection');
								}
								else {
									const id = repos[0].id;
									Clifton.instruction.openInstructionPreview(id, 'LendingRepo', gridPanel);
								}
							}
						});
					}
				}]
			},


			{
				title: 'Draft REPOs',
				items: [{
					workflowStatusName: 'Draft',
					xtype: 'lending-repoGrid',
					instructions: 'Draft REPOs include REPOs that have not been submitted for approval as well as REPOs that failed validation.',
					rowSelectionModel: 'checkbox',
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow',
						drillDownOnly: true,
						requestedMaxDepth: 5,
						ptype: 'workflowAwareEntity-grideditor',
						tableName: 'LendingRepo',
						transitionWorkflowStateList: [
							{stateName: 'Validated', iconCls: 'run', buttonText: 'Validate Selected', buttonTooltip: 'Validate Selected REPOs'}
						],
						sortList: function(rows, stateName, gridEditor, gridPanel) {
							const repos = [];
							let j = 0;
							let addedRepos = '';
							for (let i = 0; i < rows.length; i++) {
								const repo = rows[i].data;
								if (addedRepos.indexOf(repo.id) < 0) {
									addedRepos += repo.id;
									repos[j] = rows[i];
									j++;
								}
							}
							return repos;
						}
					}
				}]
			},


			{
				title: 'Pending REPOs',
				items: [{
					workflowStatusName: 'Pending',
					xtype: 'lending-repoGrid',
					instructions: 'Pending REPOs are REPOs that are waiting to be approved.', // Add back later -- Select the REPOs that you want to approve and click the approve button.',
					rowSelectionModel: 'checkbox',
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow',
						drillDownOnly: true,
						requestedMaxDepth: 5,
						ptype: 'workflowAwareEntity-grideditor',
						tableName: 'LendingRepo',
						transitionWorkflowStateList: [
							{stateName: 'Approved', iconCls: 'run', buttonText: 'Approve Selected', buttonTooltip: 'Approved Selected REPOs'}
						],
						sortList: function(rows, stateName, gridEditor, gridPanel) {
							const repos = [];
							let j = 0;
							let addedRepos = '';
							for (let i = 0; i < rows.length; i++) {
								const repo = rows[i].data;
								if (addedRepos.indexOf(repo.id) < 0) {
									addedRepos += repo.id;
									repos[j] = rows[i];
									j++;
								}
							}
							return repos;
						}
					}
				}]
			},


			{
				title: 'Approved REPOs',
				items: [{
					workflowStatusName: 'Approved',
					xtype: 'lending-repoGrid',
					rowSelectionModel: 'checkbox',
					instructions: 'Approved REPOs have been validated, approved and are waiting to be booked.',
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Active REPOs',
				items: [{
					workflowStateName: 'Active Repo',
					workflowStatusName: 'Closed',
					xtype: 'lending-repoGrid',
					instructions: 'Active REPOs that have been opened and not matured or closed.',
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Open REPO Roll',
				items: [{
					xtype: 'lending-repoInterestRateGrid'
				}]
			},


			{
				title: 'Closed REPOs',
				items: [{
					workflowStateName: 'Closed',
					workflowStatusName: 'Closed',
					xtype: 'lending-repoGrid',
					instructions: 'REPOs that have matured and posted to the general ledger.',
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Cancelled REPOs',
				items: [{
					xtype: 'lending-repoGrid',
					instructions: 'Cancelled REPOs were cancelled prior to being booked.',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
						}
						return {
							active: this.showOnlyActiveRepos ? this.showOnlyActiveRepos : null,
							workflowStateName: 'Cancelled',
							requestedMaxDepth: 6
						};
					},
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
