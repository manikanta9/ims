Clifton.lending.repo.RepoRollUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'lendingRepoRollUploadWindow',
	title: 'Lending Repo Rolls - Upload Window',
	iconCls: 'import',
	height: 500,
	width: 700,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: () => TCG.openFile('lending/repo/upload/LendingRepoRollUploadFileSample.xls')
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Lending Repo Rolls Upload allows uploading a list of repos to automatically roll with new prices and interest rates.',
		getSaveURL: () => 'lendingRepoRollUploadFileUpload.json',
		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{xtype: 'sectionheaderfield', header: 'If invalid data is encountered'},
			{
				xtype: 'radiogroup', columns: 1,
				items: [
					{boxLabel: 'Fail this upload and don\'t load any data.', name: 'partialUploadAllowed', inputValue: 'false', checked: true},
					{boxLabel: 'Skip rows with invalid data but upload valid rows.', name: 'partialUploadAllowed', inputValue: 'true'}
				]
			},
			{boxLabel: 'Run Position Retrieval with Option Recompile (<b>Note:</b> Only use this option after getting timeouts from initial attempt)', name: 'positionRetrievalOptionRecompile', xtype: 'checkbox'},
			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false, height: 250}
		]
	}]
});
