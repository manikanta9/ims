Clifton.lending.repo.RepoSettingWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'repoSettingWindow',
	title: 'Repo Settings',
	iconCls: 'repo',

	items: [{
		xtype: 'formpanel',
		url: 'lendingRepoSetting.json',
		instructions: 'Defines default REPO settings for securities in the specified Investment Hierarchy.',
		labelWidth: 140,

		items: [
			{fieldLabel: 'Investment Hierarchy', name: 'instrumentHierarchy.labelExpanded', hiddenName: 'instrumentHierarchy.id', displayField: 'labelExpanded', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json', queryParam: 'labelExpanded', listWidth: 600, detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow'},
			{fieldLabel: 'Days To Settle', name: 'daysToSettle', xtype: 'integerfield'},
			{
				fieldLabel: 'Day Count', name: 'dayCountConvention', loadAll: true, mode: 'local',
				allowBlank: false, xtype: 'combo',
				valueField: 'value',
				displayField: 'text',
				tooltipField: 'tooltip',
				url: 'systemListItemListFind.json?listName=Bond Interest Calculation Types'
			}
		]
	}]
});
