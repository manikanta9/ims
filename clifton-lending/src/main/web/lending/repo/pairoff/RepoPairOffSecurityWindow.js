Clifton.lending.repo.pairoff.RepoPairOffSecurityWindow = Ext.extend(TCG.app.Window, {
	id: 'repoPairOffSecurityWindow',
	titlePrefix: 'REPO Security Pair Off',
	iconCls: 'repo',
	width: 1000,
	height: 550,
	render: function() {
		TCG.Window.superclass.render.apply(this, arguments);

		const win = this;
		win.on('beforeclose', function() {
			if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
				win.openerCt.reload(win);
			}
		});

		if (win.defaultData && win.defaultData.label) {
			TCG.Window.superclass.setTitle.call(this, this.titlePrefix + ' - ' + win.defaultData.label, this.iconCls);
		}
	},
	items: [{
		name: 'lendingRepoPairOffSecurityListFind',
		isPagingEnabled: function() {
			return false;
		},
		xtype: 'gridpanel',
		instructions: 'Security level summary on selected day for REPO maturities and openings for selected account and counter party.  Only REPOs that are booked and confirmed will appear in the summary.',
		additionalPropertiesToRequest: 'id|label|clientInvestmentAccount.id|holdingInvestmentAccount.id|tradeDate|investmentSecurity.id',
		getLoadParams: function() {
			// default to last 30 days of trades
			const data = this.getWindow().defaultData;
			if (data) {
				return {
					pairOffDate: data.pairOffDate,
					counterpartyId: data.counterpartyId,
					clientInvestmentAccountId: data.clientInvestmentAccountId,
					holdingInvestmentAccountId: data.holdingInvestmentAccountId,
					excludeWorkflowStateName: 'Cancelled',
					confirmed: true,
					requestedMaxDepth: 6
				};
			}
			return false;
		},
		columns: [
			{header: 'Pair Off Date', width: 13, dataIndex: 'pairOffDate', filter: {searchFieldName: 'pairOffDate'}, hidden: true},
			{header: 'Client Account', width: 50, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
			{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
			{header: 'REPO Account', width: 40, dataIndex: 'repoInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'repoInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false&accountType=REPO'}, hidden: true},
			{header: 'Counterparty', width: 35, dataIndex: 'counterparty.label', filter: {searchFieldName: 'counterpartyName'}},
			{header: 'Security', width: 18, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, defaultSortColumn: true},
			{header: 'Opening Count', width: 20, dataIndex: 'openingRepoCount', type: 'int'},
			{header: 'Maturing Count', width: 20, dataIndex: 'maturingRepoCount', type: 'int'},
			{header: 'Completed', width: 15, dataIndex: 'completed', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
		],
		editor: {
			detailPageClass: 'Clifton.lending.repo.pairoff.RepoPairOffWindow',
			drillDownOnly: true,
			getDefaultData: function(gridPanel, row) {
				const win = gridPanel.getWindow();
				// use selected row parameters for drill down filtering
				return {
					label: row.json.label,
					pairOffDate: win.defaultData.pairOffDate,
					holdingInvestmentAccountId: win.defaultData.holdingInvestmentAccountId,
					counterpartyId: win.defaultData.counterpartyId,
					securityId: row.json.investmentSecurity.id
				};
			}
		}
	}]
});
