Clifton.lending.repo.pairoff.RepoPairOffWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Repo Pair Off',
	iconCls: 'repo',
	width: 1000,
	currentPairOffList: undefined,
	reload: function() {
		this.reloadGrids();
	},
	reloadGrids: function() {
		const win = this;
		const maturingGrid = TCG.getChildByName(win, 'lendingRepoPairOffMaturing');
		const openingGrid = TCG.getChildByName(win, 'lendingRepoPairOffOpening');
		const maturingModel = maturingGrid.grid.getSelectionModel();
		const openingModel = openingGrid.grid.getSelectionModel();

		this.currentPairOffList = undefined;

		maturingModel.clearSelections(true);
		openingModel.clearSelections(true);

		maturingGrid.rolled = undefined;
		openingGrid.rolled = undefined;

		maturingGrid.reload();
		openingGrid.reload();
	},
	deselectGridItems: function(record, maturingGrid, openingGrid) {
		const maturingModel = maturingGrid.grid.getSelectionModel();
		const openingModel = openingGrid.grid.getSelectionModel();

		if (this.currentPairOffList) {
			maturingModel.suspendEvents(false);
			openingModel.suspendEvents(false);

			for (let i = 0; i < maturingGrid.grid.store.data.items.length; i++) {
				maturingModel.deselectRow(i);
			}
			for (let i = 0; i < openingGrid.grid.store.data.items.length; i++) {
				openingModel.deselectRow(i);
			}

			maturingModel.resumeEvents(false);
			openingModel.resumeEvents(false);
		}
		else if ((maturingModel.getCount() < 1) && (openingModel.getCount() < 1)) {

			maturingModel.clearSelections(true);
			openingModel.clearSelections(true);

			maturingGrid.rolled = undefined;
			openingGrid.rolled = undefined;

			maturingGrid.reload();
			openingGrid.reload();
		}

		this.currentPairOffList = undefined;
	},
	loadPairOffs: function(record, maturingGrid, openingGrid, paired) {
		if (paired) {
			const win = this;
			const loader = new TCG.data.JsonLoader({
				params: {
					repoIdForGroup: record.id,
					pairOffDate: win.defaultData.pairOffDate,
					requestedMaxDepth: 6
				},
				onLoad: function(data, conf) {
					win.selectGridItems(data, maturingGrid, openingGrid, paired);
				}
			});
			loader.load('lendingRepoPairOffListFind.json', win);
		}
		else {
			this.selectGridItems(undefined, maturingGrid, openingGrid, paired);
		}
	},
	selectGridItems: function(data, maturingGrid, openingGrid, paired) {
		if (!paired && (maturingGrid.rolled || openingGrid.rolled)) {
			this.currentPairOffList = undefined;

			maturingGrid.rolled = undefined;
			openingGrid.rolled = undefined;

			maturingGrid.reload();
			openingGrid.reload();
		}
		else if (paired) {
			if (!data) {
				data = this.currentPairOffList;
			}
			const maturingModel = maturingGrid.grid.getSelectionModel();
			const openingModel = openingGrid.grid.getSelectionModel();

			maturingModel.clearSelections(true);
			openingModel.clearSelections(true);

			maturingModel.suspendEvents(false);
			openingModel.suspendEvents(false);

			for (let i = 0; i < maturingGrid.grid.store.data.items.length; i++) {
				const row = maturingGrid.grid.store.data.items[i];
				let found = false;
				for (let j = 0; j < data.length; j++) {
					const record = data[j];
					if (record.maturingRepo && record.maturingRepo.id === row.id) {
						maturingModel.selectRow(i, true);
						found = true;
						break;
					}
				}
				if (!found) {
					maturingModel.deselectRow(i);
				}
			}
			for (let i = 0; i < openingGrid.grid.store.data.items.length; i++) {
				const row = openingGrid.grid.store.data.items[i];
				let found = false;
				for (let j = 0; j < data.length; j++) {
					const record = data[j];
					if (record.openingRepo && record.openingRepo.id === row.id) {
						openingModel.selectRow(i, true);
						found = true;
						break;
					}
				}
				if (!found) {
					openingModel.deselectRow(i);
				}
			}

			this.currentPairOffList = data;

			maturingModel.resumeEvents(false);
			openingModel.resumeEvents(false);


			if (maturingGrid.rolled === false) {
				maturingGrid.reconciled = undefined;
				maturingGrid.reload();
			}
			if (openingGrid.rolled === false) {
				openingGrid.reconciled = undefined;
				openingGrid.reload();
			}
		}
	},
	setPairOffDate: function() {
		const win = this;
		if (this.currentPairOffList) {
			this.doSetPairOffDate(win.currentPairOffList[0]);
		}
		else {
			const selectedPairOffGroupList = this.getSelectedRepoList();
			if (selectedPairOffGroupList.length !== 1) {
				TCG.showError('Please select only one REPO pair to change the date.', 'Invalid Selection');
				return;
			}
			const loader = new TCG.data.JsonLoader({
				params: {
					repoIdForGroup: selectedPairOffGroupList[0].id,
					pairOffDate: win.defaultData.pairOffDate,
					requestedMaxDepth: 6
				},
				onLoad: function(data, conf) {
					if (data.length > 0) {
						win.doSetPairOffDate(data[0]);
					}
				}
			});
			loader.load('lendingRepoPairOffListFind.json', win);
		}
	},
	doSetPairOffDate: function(currentPairOff) {
		const win = this;
		if (currentPairOff) {
			TCG.createComponent('Clifton.lending.repo.pairoff.RepoPairOffDateWindow', {
				defaultData: {
					pairOffGroupId: currentPairOff.pairOffGroup,
					pairOffDate: currentPairOff.pairOffDate,
					note: currentPairOff.pairOffDateChangeNote
				},
				openerCt: win
			});
		}
	},
	splitSelectedRepos: function() {
		const win = this;
		const maturingGrid = TCG.getChildByName(win, 'lendingRepoPairOffMaturing');
		const openingGrid = TCG.getChildByName(win, 'lendingRepoPairOffOpening');
		const maturingModel = maturingGrid.grid.getSelectionModel();
		const openingModel = openingGrid.grid.getSelectionModel();

		for (let i = 0; i < maturingModel.getSelections().length; i++) {
			if (maturingModel.getSelections()[i].json.maturingCompleted === true) {
				TCG.showError('The selected REPO\'s are already paired.  You must mark as uncompleted before pairing.', 'Invalid Pair');
				return;
			}
		}
		for (let i = 0; i < openingModel.getSelections().length; i++) {
			if (openingModel.getSelections()[i].json.openingCompleted === true) {
				TCG.showError('The selected REPO\'s are already paired.  You must mark as uncompleted before pairing.', 'Invalid Pair');
				return;
			}
		}
		if (win.currentPairOffList) {
			const loader = new TCG.data.JsonLoader({
				params: {
					pairOffGroupId: win.currentPairOffList[0].pairOffGroup,
					requestedMaxDepth: 6
				},
				onLoad: function(data, conf) {
					win.reloadGrids();
				}
			});
			loader.load('lendingRepoPairOffSplit.json', win);
		}
	},
	pairSelectedRepos: function() {
		const win = this;
		const maturingGrid = TCG.getChildByName(win, 'lendingRepoPairOffMaturing');
		const openingGrid = TCG.getChildByName(win, 'lendingRepoPairOffOpening');
		const maturingModel = maturingGrid.grid.getSelectionModel();
		const openingModel = openingGrid.grid.getSelectionModel();

		const params = {};
		for (let i = 0; i < maturingModel.getSelections().length; i++) {
			if (maturingModel.getSelections()[i].json.maturingCompleted === true) {
				TCG.showError('The selected REPO\'s are already paired.  You must mark as uncompleted before pairing.', 'Invalid Pair');
				return;
			}
			params['maturingRepoList[' + i + '].id'] = maturingModel.getSelections()[i].id;
			// add the definition id to the view doesn't break trying to get the buy property
			params['maturingRepoList[' + i + '].repoDefinition.id'] = maturingModel.getSelections()[i].json.repoDefinition.id;
			params['maturingRepoList[' + i + '].type.id'] = maturingModel.getSelections()[i].json.type.id;
		}
		for (let i = 0; i < openingModel.getSelections().length; i++) {
			if (openingModel.getSelections()[i].json.openingCompleted === true) {
				TCG.showError('The selected REPO\'s are already paired.  You must mark as uncompleted before pairing.', 'Invalid Pair');
				return;
			}
			params['openingRepoList[' + i + '].id'] = openingModel.getSelections()[i].id;
			//add the definition id to the view doesn't break trying to get the buy property
			params['openingRepoList[' + i + '].repoDefinition.id'] = openingModel.getSelections()[i].json.repoDefinition.id;
			params['openingRepoList[' + i + '].type.id'] = openingModel.getSelections()[i].json.type.id;
		}
		params.pairOffDate = this.defaultData.pairOffDate;
		params.overrideSinglePairs = true;
		params.requestedMaxDepth = 6;

		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Matching Trades',
			waitTarget: win,
			params: params,
			timeout: 240000,
			onLoad: function(data, conf) {
				win.reloadGrids();
			}
		});
		loader.load('lendingRepoPairOffPair.json');
	},
	getSelectedRepoList: function() {
		const win = this;
		const maturingGrid = TCG.getChildByName(win, 'lendingRepoPairOffMaturing');
		const openingGrid = TCG.getChildByName(win, 'lendingRepoPairOffOpening');
		const maturingModel = maturingGrid.grid.getSelectionModel();
		const openingModel = openingGrid.grid.getSelectionModel();

		const repoList = [];
		for (let i = 0; i < maturingModel.getSelections().length; i++) {
			repoList.push(maturingModel.getSelections()[i].json);
		}
		for (let i = 0; i < openingModel.getSelections().length; i++) {
			repoList.push(openingModel.getSelections()[i].json);
		}
		return repoList;
	},
	markCompleted: function(completed) {
		const win = this;
		const maturingGrid = TCG.getChildByName(win, 'lendingRepoPairOffMaturing');
		const openingGrid = TCG.getChildByName(win, 'lendingRepoPairOffOpening');
		const maturingModel = maturingGrid.grid.getSelectionModel();
		const openingModel = openingGrid.grid.getSelectionModel();
		completed = true;

		maturingModel.selectAll();
		openingModel.selectAll();

		const pairOffGroupList = [];
		for (let i = 0; i < maturingModel.getSelections().length; i++) {
			const selection = maturingModel.getSelections()[i].json;
			if (selection.maturingCompleted === true) {
				completed = false;
			}
			const indexOf = pairOffGroupList.indexOf(selection.maturingPairOffGroup);
			if (indexOf === -1) {
				pairOffGroupList.push(selection.maturingPairOffGroup);
			}
		}
		for (let i = 0; i < openingModel.getSelections().length; i++) {
			const selection = openingModel.getSelections()[i].json;
			if (selection.openingCompleted === true) {
				completed = false;
			}
			const indexOf = pairOffGroupList.indexOf(selection.openingPairOffGroup);
			if (indexOf === -1) {
				pairOffGroupList.push(selection.openingPairOffGroup);
			}
		}
		this.saveCompleted(pairOffGroupList, 0, completed);
	},
	saveCompleted: function(pairOffGroupList, count, completed) {
		if (pairOffGroupList.length !== 0) {
			const win = this;
			const params = {
				pairOffGroupId: pairOffGroupList[count],
				completed: completed
			};
			const loader = new TCG.data.JsonLoader({
				waitTarget: win,
				waitMsg: 'Marking Completed...',
				params: params,
				onLoad: function(record, conf) {
					count++;
					if (count === pairOffGroupList.length) { // refresh after all repos were transitioned
						win.reloadGrids();
					}
					else {
						win.saveCompleted(pairOffGroupList, count, completed);
					}
				}
			});
			loader.load('lendingRepoPairOffGroupCompletedSave.json');
		}
	},
	render: function() {
		TCG.Window.superclass.render.apply(this, arguments);

		const win = this;
		win.on('beforeclose', function() {
			if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
				win.openerCt.reload(win);
			}
		});


		if (win.defaultData && win.defaultData.label) {
			TCG.Window.superclass.setTitle.call(this, this.titlePrefix + ' - ' + win.defaultData.label, this.iconCls);
		}


		const t = this.getTopToolbar();
		t.add({
			text: 'Reload',
			tooltip: 'Reload latest grid data',
			iconCls: 'table-refresh',
			handler: function() {
				win.reloadGrids();
			}
		});
		t.add('-');
		t.add({
			text: 'Pair Selected',
			iconCls: 'link',
			handler: function() {
				win.pairSelectedRepos();
			}
		}, '-');
		t.add({
			text: 'Split Selected',
			iconCls: 'cancel',
			handler: function() {
				win.splitSelectedRepos();
			}
		}, '-');
		t.add({
			text: 'Mark All Completed/Uncompleted',
			iconCls: 'row_reconciled',
			handler: function() {
				win.markCompleted();
			}
		}, '-');
		t.add({
			text: 'Set Pairoff Date',
			iconCls: 'repo',
			handler: function() {
				win.setPairOffDate();
			}
		}, '-');
	},
	layout: {
		type: 'border',
		align: 'stretch'
	},
	frame: true,
	tbar: [],
	items: [{
		title: 'Maturing REPO\'s',
		name: 'lendingRepoPairOffMaturing',
		loadURL: 'lendingRepoExtendedListFind.json',
		additionalPropertiesToRequest: 'repoDefinition.id|openingPairOffGroup|maturingPairOffGroup|type.id',
		xtype: 'gridpanel',
		instructions: 'TBD',
		region: 'west',
		split: true,
		layout: 'fit',
		flex: 1,
		width: 490,
		hideStandardButtons: true,
		rowSelectionModel: 'checkbox',
		//groupField: 'trade.id',
		getRowSelectionModel: function() {
			const win = this.getWindow();
			if (typeof this.rowSelectionModel != 'object') {
				this.rowSelectionModel = new Ext.grid.CheckboxSelectionModel({
					singleSelect: false,
					listeners: {
						'rowselect': function(model, rowIndex, record) {
							const maturingGrid = TCG.getChildByName(win, 'lendingRepoPairOffMaturing');
							const openingGrid = TCG.getChildByName(win, 'lendingRepoPairOffOpening');
							win.loadPairOffs(record, maturingGrid, openingGrid, record.json.maturingRolled);
						},
						'rowdeselect': function(model, rowIndex, record) {
							const maturingGrid = TCG.getChildByName(win, 'lendingRepoPairOffMaturing');
							const openingGrid = TCG.getChildByName(win, 'lendingRepoPairOffOpening');
							win.deselectGridItems(record, maturingGrid, openingGrid);
						}
					}
				});
			}
			return this.rowSelectionModel;
		},
		plugins: {ptype: 'gridsummary'},
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Security', width: 18, dataIndex: 'repoDefinition.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
			{header: 'CCY', width: 10, dataIndex: 'repoDefinition.investmentSecurity.instrument.tradingCurrency.symbol', hidden: true, filter: {type: 'combo', searchFieldName: 'ccySecurityId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
			{header: 'Face', width: 13, dataIndex: 'originalFace', type: 'float', summaryType: 'sum'},
			{header: 'Index Ratio', width: 12, dataIndex: 'indexRatio', type: 'float', hidden: true},
			{header: 'Price', width: 12, dataIndex: 'price', type: 'float', hidden: true},
			{header: 'Interest', width: 12, dataIndex: 'interestAmount', type: 'currency', hidden: true},
			{header: 'Net Cash', width: 12, dataIndex: 'netCash', type: 'currency', hidden: true},
			{header: 'Maturity Date', width: 13, dataIndex: 'maturityDate', type: 'date'},
			{header: 'Settlement Date', width: 13, dataIndex: 'maturitySettlementDate', type: 'date', qtip: 'Maturity Settlement Date'},
			{
				header: 'Paired', width: 10, dataIndex: 'maturingRolled', type: 'boolean', renderer: function(v) {
					return TCG.renderCheck(v, 'link.png');
				}, align: 'center'
			},
			{header: 'Completed', width: 10, dataIndex: 'maturingCompleted', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
		],
		getLoadParams: function() {
			const data = this.getWindow().defaultData;
			if (data) {
				return {
					rolled: this.rolled,
					maturingPairOffDate: data.pairOffDate,
					holdingInvestmentAccountId: data.holdingInvestmentAccountId,
					clientInvestmentAccountId: data.clientInvestmentAccountId,
					counterpartyId: data.counterpartyId,
					securityId: data.securityId,
					excludeWorkflowStateName: 'Cancelled',
					confirmed: true,
					requestedMaxDepth: 6
				};
			}
			return false;
		},
		editor: {
			detailPageClass: 'Clifton.lending.repo.BondRepoWindow',
			drillDownOnly: true
		}
	},


		{
			title: 'New REPO\'s',
			name: 'lendingRepoPairOffOpening',
			loadURL: 'lendingRepoExtendedListFind.json',
			additionalPropertiesToRequest: 'repoDefinition.id|openingPairOffGroup|type.id',
			xtype: 'gridpanel',
			instructions: 'TBD',
			layout: 'fit',
			flex: 1,
			region: 'center',
			hideStandardButtons: true,
			rowSelectionModel: 'checkbox',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Security', width: 18, dataIndex: 'repoDefinition.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
				{header: 'CCY', width: 10, dataIndex: 'repoDefinition.investmentSecurity.instrument.tradingCurrency.symbol', hidden: true, filter: {type: 'combo', searchFieldName: 'ccySecurityId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
				{header: 'Index Ratio', width: 12, dataIndex: 'indexRatio', type: 'float', hidden: true},
				{header: 'Price', width: 12, dataIndex: 'price', type: 'float', hidden: true},
				{header: 'Interest', width: 12, dataIndex: 'interestAmount', type: 'currency', hidden: true},
				{header: 'Net Cash', width: 12, dataIndex: 'netCash', type: 'currency', hidden: true},
				{header: 'Face', width: 12, dataIndex: 'originalFace', type: 'float', summaryType: 'sum'},
				{header: 'Trade Date', width: 13, dataIndex: 'tradeDate', type: 'date'},
				{header: 'Settlement Date', width: 13, dataIndex: 'settlementDate', type: 'date'},
				{
					header: 'Paired', width: 10, dataIndex: 'openingRolled', type: 'boolean', renderer: function(v) {
						return TCG.renderCheck(v, 'link.png');
					}, align: 'center'
				},
				{header: 'Completed', width: 10, dataIndex: 'openingCompleted', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
			],
			plugins: {ptype: 'gridsummary'},
			getRowSelectionModel: function() {
				const win = this.getWindow();
				if (typeof this.rowSelectionModel != 'object') {
					this.rowSelectionModel = new Ext.grid.CheckboxSelectionModel({
						singleSelect: false,
						listeners: {
							'rowselect': function(model, rowIndex, record) {
								const maturingGrid = TCG.getChildByName(win, 'lendingRepoPairOffMaturing');
								const openingGrid = TCG.getChildByName(win, 'lendingRepoPairOffOpening');
								win.loadPairOffs(record, maturingGrid, openingGrid, record.json.openingRolled);
							},
							'rowdeselect': function(model, rowIndex, record) {
								const maturingGrid = TCG.getChildByName(win, 'lendingRepoPairOffMaturing');
								const openingGrid = TCG.getChildByName(win, 'lendingRepoPairOffOpening');
								win.deselectGridItems(record, maturingGrid, openingGrid);
							}
						}
					});
				}
				return this.rowSelectionModel;
			},
			getLoadParams: function() {
				const data = this.getWindow().defaultData;
				if (data) {
					return {
						rolled: this.rolled,
						openingPairOffDate: data.pairOffDate,
						holdingInvestmentAccountId: data.holdingInvestmentAccountId,
						clientInvestmentAccountId: data.clientInvestmentAccountId,
						counterpartyId: data.counterpartyId,
						securityId: data.securityId,
						excludeWorkflowStateName: 'Cancelled',
						confirmed: true,
						requestedMaxDepth: 6
					};
				}
				return false;
			},
			editor: {
				detailPageClass: 'Clifton.lending.repo.BondRepoWindow',
				drillDownOnly: true
			}
		}]
});


Clifton.lending.repo.pairoff.RepoPairOffDateWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'REPO Pairoff: Set Pair Off Date',
	iconCls: 'repo',
	height: 300,
	width: 600,
	modal: true,
	allowOpenFromModal: true, // contact drill down

	items: [{ // TRADE: investmentSecurity.id, holdingAccount.issuingCompany.id, executingBroker.id
		xtype: 'formpanel',
		instructions: 'Change REPO pair off date:',
		items: [
			{xtype: 'datefield', name: 'pairOffDate'},
			{boxLabel: 'Note', xtype: 'textarea', name: 'note', height: 150}
		],
		getSubmitParams: function() {
			return {pairOffGroupId: this.getDefaultData(this.getWindow()).pairOffGroupId};
		},
		getSaveURL: function() {
			return 'lendingRepoPairOffGroupDateSave.json';
		}
	}]
});

