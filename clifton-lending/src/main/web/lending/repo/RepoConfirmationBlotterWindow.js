Clifton.lending.repo.RepoConfirmationBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'repoConfirmationBlotterWindow',
	title: 'REPO Confirmation Blotter',
	iconCls: 'repo',
	width: 1200,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		listeners: {
			// keep selected client account
			beforetabchange: function(tabPanel, newTab, currentTab) {
				if (currentTab) {
					const o = TCG.getChildByName(currentTab.getTopToolbar() ? currentTab.getTopToolbar() : currentTab.items.get(0).getTopToolbar(), 'tradeDate');
					if (o && o.getValue()) {
						tabPanel.savedData = {
							value: o.getValue()
						};
					}
				}
			},
			tabchange: function(tabPanel, tab) {
				const s = tabPanel.savedData;
				if (s) {
					const o = TCG.getChildByName(tab.getTopToolbar() ? tab.getTopToolbar() : tab.items.get(0).getTopToolbar(), 'tradeDate');
					if (o && (o.getValue() !== s.value)) {
						o.setValue(s.value);
						o.fireEvent('select', o, s);
					}
				}
			}
		},
		reloadOnChange: true,
		items: [{
			title: 'REPO Confirmation\'s',
			items: [{
				xtype: 'lending-repoGrid',
				rowSelectionModel: 'multiple',
				columnOverrides: [
					{dataIndex: 'confirmed', hidden: false},
					{dataIndex: 'repoDefinition.indexRatio', hidden: true},
					{dataIndex: 'workflowState.name', hidden: true},
					{dataIndex: 'workflowStatus.name', hidden: true}
				],
				instructions: 'All REPO openings and rolls for the selected date.',
				getLoadParams: function(firstLoad) {
					if (firstLoad) {
						this.setFilterValue('confirmed', false);
					}

					return {
						workflowStatusNameEquals: 'Closed',
						excludeWorkflowStateName: 'Cancelled',
						requestedMaxDepth: 6
					};
				},
				setRepoConfirmed: function(id, confirmed) {
					const gridPanel = this;
					const rows = gridPanel.getRowSelectionModel().getSelections();
					if (!rows) {
						TCG.showError('No REPO was selected to be confirmed.', 'Confirm REPO');
						return;
					}
					const ids = [];
					for (const row of rows) {
						ids.push(row.data.id);
					}
					const loader = new TCG.data.JsonLoader({
						waitMsg: 'Confirming...',
						waitTarget: gridPanel,

						params: {ids: ids},
						onLoad: function(record, conf) {
							gridPanel.reload();
						},
						onFailure: function() {
							gridPanel.reload();
						}
					});
					loader.load('lendingRepoConfirm.json');
				},
				editor: {
					detailPageClass: 'Clifton.lending.repo.RepoWindow',
					drillDownOnly: true,
					addEditButtons: function(t, gridPanel) {
						t.add({
							text: 'Confirm Repo',
							iconCls: 'run',
							handler: function() {
								const rows = gridPanel.getRowSelectionModel().getSelections();
								if (!rows) {
									TCG.showError('Please select one or more rows to be confirmed.', 'Confirm REPO');
									return;
								}
								const ids = [];
								for (const row of rows) {
									if (row.data.confirmed) {
										TCG.showError('One or more of the  selected REPO\'s is already confirmed.', 'Confirm REPO');
									}
									ids.push(row.data.id);
								}
								TCG.createComponent('Clifton.lending.repo.RepoConfirmationWindow', {
									defaultData: {
										ids: ids
									},
									openerCt: gridPanel
								});
							}
						}, '-');
					}
				}
			}]
		},


			{
				title: 'REPO Activity',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				tbar: ['->',
					{xtype: 'tbtext', text: '&nbsp;Trade Date:&nbsp;'},
					{
						xtype: 'datefield', name: 'tradeDate', width: 90,
						listeners: {
							select: function(field) {
								const panel = TCG.getParentByClass(this, TCG.TabPanel);

								TCG.getChildByName(panel, 'currentOpeningRepoList').reload();
								TCG.getChildByName(panel, 'currentMaturingRepoList').reload();
							},
							specialkey: function(field, e) {
								if (e.getKey() === e.ENTER) {
									const panel = TCG.getParentByClass(this, TCG.TabPanel);

									TCG.getChildByName(panel, 'currentOpeningRepoList').reload();
									TCG.getChildByName(panel, 'currentMaturingRepoList').reload();
								}
							}
						}
					}
				],
				getLoadParams: function() {
					const bd = TCG.getChildByName(this.getTopToolbar(), 'tradeDate');
					let dateValue;
					if (bd.getValue() !== '') {
						dateValue = (bd.getValue()).format('m/d/Y');
					}
					else {
						dateValue = new Date().format('m/d/Y');
						bd.setValue(dateValue);
					}

					return {
						dateValue: dateValue,
						params: {
							workflowStatusNameEquals: 'Closed',
							excludeWorkflowStateName: 'Cancelled',
							requestedMaxDepth: 6
						}
					};
				},
				items: [{
					title: 'Opening REPO\'s',
					name: 'currentOpeningRepoList',
					loadURL: 'lendingRepoListFind.json',
					workflowStatusName: 'Closed',
					xtype: 'lending-repoGrid',
					border: 1,
					flex: 1,
					columnOverrides: [
						{dataIndex: 'confirmed', config: {hidden: false}},
						{dataIndex: 'repoDefinition.indexRatio', config: {hidden: true}},
						{dataIndex: 'workflowState.name', config: {hidden: true}},
						{dataIndex: 'workflowStatus.name', config: {hidden: true}}
					],
					getLoadParams: function() {
						const data = TCG.getParentByClass(this, Ext.Panel).getLoadParams();
						data.params.tradeDate = data.dateValue;
						return data.params;
					},
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow',
						drillDownOnly: true
					}
				}, {
					title: 'Maturing REPO\'s',
					name: 'currentMaturingRepoList',
					loadURL: 'lendingRepoListFind.json',
					workflowStatusName: 'Closed',
					xtype: 'lending-repoGrid',
					border: 1,
					flex: 1,
					columnOverrides: [
						{dataIndex: 'confirmed', config: {hidden: false}},
						{dataIndex: 'repoDefinition.indexRatio', config: {hidden: true}},
						{dataIndex: 'workflowState.name', config: {hidden: true}},
						{dataIndex: 'workflowStatus.name', config: {hidden: true}}
					],
					getLoadParams: function() {
						const data = TCG.getParentByClass(this, Ext.Panel).getLoadParams();
						data.params.maturityDate = data.dateValue;
						return data.params;
					},
					editor: {
						detailPageClass: 'Clifton.lending.repo.RepoWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'REPO Pair Off',
				items: [{
					name: 'lendingRepoPairOffAccountListFind',
					isPagingEnabled: function() {
						return false;
					},
					xtype: 'gridpanel',
					instructions: 'Pair Offs are used to link maturing REPO positions to corresponding opening REPO positions for the same security. Pair offs are used during REPO confirmation process to minimize transfers of positions on REPO (only transfer the difference).  Only REPOs that are booked and confirmed will appear in the summary.',
					additionalPropertiesToRequest: 'id|label|clientInvestmentAccount.id|holdingInvestmentAccount.id|tradeDate|counterparty.id',
					groupField: 'pairOffDate',
					remoteSort: true,
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('pairOffDate', {'after': new Date().add(Date.DAY, -5)});
						}
						return {
							excludeWorkflowStateName: 'Cancelled',
							confirmed: true,
							requestedMaxDepth: 6
						};
					},
					columns: [
						{header: 'Pair Off Date', width: 20, dataIndex: 'pairOffDate', filter: {searchFieldName: 'pairOffDate'}, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Client Account', width: 50, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', width: 43, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'REPO Account', width: 35, dataIndex: 'repoInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'repoInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false&accountType=REPO'}, hidden: true},
						{header: 'Counterparty', width: 32, dataIndex: 'counterparty.label', filter: {searchFieldName: 'counterpartyName'}},
						{header: 'Opening Count', width: 20, dataIndex: 'openingRepoCount', type: 'int'},
						{header: 'Maturing Count', width: 20, dataIndex: 'maturingRepoCount', type: 'int'},
						{header: 'Completed', width: 14, dataIndex: 'completed', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
					],
					editor: {
						detailPageClass: 'Clifton.lending.repo.pairoff.RepoPairOffSecurityWindow',
						drillDownOnly: true,
						getDefaultData: function(gridPanel, row) {
							// use selected row parameters for drill down filtering
							return {
								label: row.json.label,
								pairOffDate: TCG.parseDate(row.json.pairOffDate).format('m/d/Y'),
								counterpartyId: row.json.counterparty.id,
								clientInvestmentAccountId: row.json.clientInvestmentAccount.id,
								holdingInvestmentAccountId: row.json.holdingInvestmentAccount.id
							};
						}
					}
				}]
			}]
	}]
});
