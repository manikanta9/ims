TCG.use('Clifton.lending.repo.RepoWindow');

Clifton.lending.repo.RepoDetailWindow = Ext.extend(Clifton.lending.repo.RepoWindow, {
	url: 'lendingRepo.json?requestedMaxDepth=5&requestedPropertiesToExclude=data.parent|data.interestRateList',

	prepareEntity: function(entity) {
		return entity ? entity.repo : entity;
	}
});
