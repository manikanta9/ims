Clifton.lending.repo.RepoWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'lendingRepo.json?requestedMaxDepth=5&requestedPropertiesToExclude=parent|interestRateList',

	getClassName: function(config, entity) {
		let investmentType = this.investmentType;
		if (entity && TCG.isNotBlank(entity.repoDefinition.instrumentHierarchy) && TCG.isNotBlank(entity.repoDefinition.instrumentHierarchy.investmentType)) {
			investmentType = entity.repoDefinition.instrumentHierarchy.investmentType.name;
		}
		if (investmentType === 'Bonds') {
			return 'Clifton.lending.repo.BondRepoWindow';
		}
		return null;
	}
});
