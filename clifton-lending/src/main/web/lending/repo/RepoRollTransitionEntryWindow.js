Clifton.lending.repo.RepoRollTransitionEntryWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Roll Repo',
	iconCls: 'repo',
	height: 250,
	modal: true,
	init: function() {
		// change defaultData to a method so can convert to each forms default data from the transition
		this.transitionParams = this.defaultData.transitionParams;

		const dd = this.defaultData;
		this.defaultData = this.getDefaultData(dd);

		Clifton.lending.repo.RepoRollTransitionEntryWindow.superclass.init.apply(this, arguments);

		this.win.saveForm = this.win.saveForm.createInterceptor(this.executeTransition);
	},

	getDefaultData: function(dd) {
		const win = this;
		const fp = win.openerCt.getFormPanel();
		return {
			investmentSecurity: fp.getFormValue('repoDefinition.investmentSecurity')
		};
	},

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Enter the new rate and price for the REPO.',
		items: [
			{fieldLabel: 'Bond', name: 'investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'investmentSecurity.id', submitValue: false},
			{fieldLabel: 'Interest Rate (%)', name: 'interestRate', xtype: 'floatfield', allowBlank: false},
			{fieldLabel: 'Price', name: 'price', xtype: 'floatfield', allowBlank: false}
		]
	}],

	executeTransition: function() {
		const win = this;
		const fp = this.getMainFormPanel();

		const transitionParams = {
			...this.transitionParams,
			interestRate: fp.getForm().findField('interestRate').value,
			price: fp.getForm().findField('price').value,
			repoId: this.transitionParams.id
		};

		const url = 'lendingRepoRollExecute.json';
		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Executing Transition',
			waitTarget: fp,
			params: transitionParams,
			onLoad: function(record, conf) {
				win.openNewLendingRepo(record);
				win.closeWindow();
				win.openerCt.getFormPanel().getWindow().closeWindow();

			}
		});
		loader.load(url);
		return false;
	},

	openNewLendingRepo: function(repo) {
		const win = this;
		const defaultData = undefined;
		const className = 'Clifton.lending.repo.RepoWindow';

		Promise.resolve(defaultData).then(function(defaultData) {
			const params = {'id': repo.id};
			const cmpId = TCG.getComponentId(className, id);
			TCG.createComponent(className, {
				id: cmpId,
				defaultData: defaultData,
				defaultDataIsReal: true,
				params: params,
				openerCt: win.openerCt,
				defaultIconCls: win.iconCls
			});
		});
	},

	setTitle: function(title, iconCls) {
		TCG.Window.superclass.setTitle.call(this, 'Workflow Transition Entry', iconCls);
	}
});
