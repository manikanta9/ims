Clifton.lending.repo.RepoInterestRateBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'repoInterestRateBlotterWindow',
	title: 'REPO Interest Rate Blotter',
	iconCls: 'repo',
	width: 1150,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [{
			title: 'REPO\'s',
			items: [{
				xtype: 'lending-repoInterestRateGrid'
			}]
		}]
	}]
});
