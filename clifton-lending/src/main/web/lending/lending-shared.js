Ext.ns('Clifton.lending', 'Clifton.lending.repo', 'Clifton.lending.repo.pairoff');

Clifton.lending.repo.DefaultRepoWindowAdditionalTabs = [];

Clifton.lending.repo.DefaultRepoWindowAdditionalTabs[Clifton.lending.repo.DefaultRepoWindowAdditionalTabs.length] = {
	title: 'Confirmation/Notes',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},

	items: [{
		title: 'Reconciliation Notes and Supporting Documents',
		xtype: 'document-system-note-grid',
		tableName: 'LendingRepo',
		showAttachmentInfo: true,
		showInternalInfo: false,
		showPrivateInfo: false,
		border: 1,
		flex: 1
	}]
};

Clifton.lending.repo.DefaultRepoWindowAdditionalTabs[Clifton.lending.repo.DefaultRepoWindowAdditionalTabs.length] = {
	title: 'SWIFT Messages',
	items: [{
		xtype: 'swift-message-grid-panel',
		instructions: 'SWIFT messages created for Repo Instructions.',
		tableName: 'LendingRepo'
	}]
};

Clifton.lending.repo.RepoInterestRateWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Build REPO Interest Rates',
	iconCls: 'reconcile',
	height: 250,
	modal: true,
	defaultDataIsReal: true,
	items: [{
		xtype: 'formpanel',
		url: 'lendingRepoInterestRateDateForDate.json',
		loadValidation: false,
		labelWidth: 120,
		loadDefaultDataAfterRender: true,
		getDefaultData: function(win) {
			const dd = win.defaultData || {};
			return TCG.data.getData('lendingRepoInterestRateDateForDate.json?rateDate=' + dd.interestRateDate.format('m/d/Y'), this);
		},
		items: [
			{fieldLabel: 'Interest Rate Date', name: 'interestRateDate', xtype: 'datefield', allowBlank: false},
			{
				xtype: 'formgrid',
				title: 'Rates',
				storeRoot: 'counterPartyRateList',
				dtoClass: 'com.clifton.lending.repo.interest.LendingRepoInterestRateCounterParty',
				readOnly: true,
				addToolbarButtons: function(toolBar) {
					//
				},
				columnsConfig: [
					{header: 'Counterparty', dataIndex: 'counterparty.name', idDataIndex: 'counterparty.id', width: 200},
					{header: 'REPO Type', dataIndex: 'repoType.name', idDataIndex: 'repoType.id', allowBlank: false},
					{header: 'Cancellation Term', dataIndex: 'cancellationNoticeTerm', allowBlank: false, qtip: 'The number of days needed to give notice of a cancellation.  This is currently used for Evergreen REPO\'s.'},
					{header: 'Rate', dataIndex: 'interestRate', width: 70, type: 'float', editor: {xtype: 'floatfield', allowBlank: false}, useNull: true}
				]
			}
		],
		getSaveURL: function() {
			return 'lendingRepoInterestRateBuild.json';
		}
	}]
});

Clifton.lending.repo.RepoInterestRateGrid = Ext.extend(TCG.grid.EditorGridPanel, {
	name: 'lendingRepoInterestRateListFind',
	instructions: 'Open REPOs can have interest rate charged for borrowing change daily. This section allows entering daily Rate and will automatically calculated accrued Interest for that date and will also update REPO Interest Amount field with the sum for of all dates.',
	excludeCancelled: true,
	additionalPropertiesToRequest: 'repo.id',
	rowSelectionModel: 'checkbox',
	calendarName: 'US Banks Calendar',

	calculateInterest: function(field, rate) {
		const grid = field.gridEditor ? field.gridEditor.containerGrid.grid : this.grid;
		const row = field.gridEditor.row;
		const rowData = grid.getStore().getAt(row).data;

		// Estimate interest for a single day if a term is not provided
		const term = rowData['repo.term'] || 1;
		const netCash = rowData['repo.netCash'];
		const interestRateDate = rowData['interestRateDate'];
		const dayCountConvention = rowData['repo.repoDefinition.dayCountConvention'];
		const calendar = Clifton.calendar.getCalendarByName(this.calendarName);
		const endDate = Clifton.calendar.getBusinessDayFromDateForCalendar(interestRateDate, term, calendar.id);
		const newTerm = TCG.dayDifference(endDate, interestRateDate);

		const interest = TCG.getResponseText('investmentInterestForDays.json?&notional=' + netCash
			+ '&rate=' + rate
			+ '&startDate=' + interestRateDate.format('m/d/Y')
			+ '&days=' + newTerm
			+ '&dayCountConventionKey=' + dayCountConvention, this);

		const interestAmountIndex = grid.getColumnModel().findColumnIndex('interestAmount');
		grid.startEditing(row, interestAmountIndex);
		const interestAmountField = grid.getColumnModel().getCellEditor(interestAmountIndex, row).field;
		interestAmountField.setValue(interest);
	},
	updateRecord: function(store, record, index) {
		//
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Interest Date', xtype: 'toolbar-datefield', name: 'interestRateDate'},
			{
				text: 'Build Rates',
				tooltip: 'Create the interest rate entries for the selected date.',
				iconCls: 'run',
				//scope: gridPanel,
				handler: function(field) {
					const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
					gridPanel.buildInterestRates();
				}
			}
		];
	},
	buildInterestRates: function() {
		const gridPanel = this;
		const t = gridPanel.getTopToolbar();
		const bd = TCG.getChildByName(t, 'interestRateDate');
		if (bd.getValue() !== '') {
			const interestRateDate = bd.getValue();
			TCG.createComponent('Clifton.lending.repo.RepoInterestRateWindow', {
				defaultData: {interestRateDate: interestRateDate},
				openerCt: gridPanel
			});

		}
	},
	saveInterestRates: function() {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const repoList = [];
		let row = -1;

		// TODO: only save rows that were edited
		grid.getStore().each(function(record) {
			row = row + 1;
			if (record.modified) {
				const rowData = grid.getStore().getAt(row).data;
				const repoRate = {
					id: rowData['id'],
					interestRate: rowData['interestRate'],
					interestAmount: rowData['interestAmount'],
					'repo.id': rowData['repo.id']
				};
				if (gridPanel.submitRateDate) {
					repoRate.interestRateDate = TCG.parseDate(rowData['interestRateDate']).format('m/d/Y');
				}
				repoList.push(repoRate);
			}
		});
		this.doSaveInterestRates(repoList, 0, gridPanel);
	},
	doSaveInterestRates: function(repoList, count, gridPanel) {
		if (repoList.length > 0) {
			const repo = repoList[count];
			const loader = new TCG.data.JsonLoader({
				waitMsg: 'Saving...',
				waitTarget: this,
				params: repo,
				timeout: 120000,
				onLoad: function(record, conf) {
					count++;
					if (count === repoList.length) { // refresh after all trades were transitioned
						gridPanel.reload();
						gridPanel.grid.getSelectionModel().clearSelections(true);
					}
					else {
						gridPanel.doSaveInterestRates(repoList, count, gridPanel);
					}
				},
				onFailure: function() {
					// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
					gridPanel.reload();
				}
			});
			loader.load('lendingRepoInterestRateSave.json');
		}
	},
	columns: [
		{header: 'ID', width: 11, dataIndex: 'id', hidden: true},
		{header: 'RepoID', width: 11, dataIndex: 'repo.id', hidden: true},
		{header: 'Type', width: 11, dataIndex: 'repo.type.name', filter: {type: 'combo', searchFieldName: 'typeId', loadAll: true, displayField: 'label', url: 'lendingRepoTypeListFind.json'}, hidden: true},
		{header: 'Workflow Status', width: 17, dataIndex: 'repo.workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
		{header: 'Workflow State', width: 18, dataIndex: 'repo.workflowState.name', filter: {searchFieldName: 'workflowStateName'}, hidden: true},
		{header: 'Client Account', width: 35, dataIndex: 'repo.repoDefinition.clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 35, dataIndex: 'repo.repoDefinition.holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
		{header: 'REPO Account', width: 35, dataIndex: 'repo.repoDefinition.repoInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'repoInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false&accountType=REPO'}, hidden: true},
		{header: 'Counterparty', width: 30, dataIndex: 'repo.repoDefinition.counterparty.label', filter: {searchFieldName: 'counterpartyName'}},
		{header: 'Description', width: 50, dataIndex: 'repo.description', hidden: true},
		{header: 'Term', tooltip: 'Number of days the REPO is for', width: 10, dataIndex: 'repo.term', useNull: true, type: 'int'},
		{header: 'Cancellation Term', tooltip: 'The number of days needed to give notice of a cancellation.  This is currently used for Evergreen REPO\'s.', width: 10, dataIndex: 'repo.cancellationNoticeTerm', useNull: true, type: 'int', hidden: true},
		{header: 'Reverse', width: 7, dataIndex: 'repo.repoDefinition.reverse', type: 'boolean', hidden: true},

		{header: 'Security', width: 18, dataIndex: 'repo.repoDefinition.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'CCY', width: 18, dataIndex: 'repo.repoDefinition.instrument.tradingCurrency.name', hidden: true, filter: {type: 'combo', searchFieldName: 'ccySecurityId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
		//{header: 'Index Ratio', width: 12, dataIndex: 'indexRatio', type: 'float', hidden: true},
		{header: 'Quantity', width: 12, dataIndex: 'repo.quantityIntended', type: 'float', hidden: true},
		{header: 'Face', width: 15, dataIndex: 'repo.originalFace', type: 'float'},
		{header: 'Price', width: 10, dataIndex: 'repo.price', type: 'float', hidden: true},
		{header: 'Market Value', width: 20, dataIndex: 'repo.marketValue', type: 'currency', hidden: true},
		{header: 'Haircut %', width: 10, dataIndex: 'repo.haircutPercent', type: 'percent', hidden: true, tooltip: 'Net Cash = (Haircut Percent > 100) ? (100 * Market Value / Haircut Percent) : (Market Value * Haircut Percent / 100)'},
		{header: 'Net Cash', width: 17, dataIndex: 'repo.netCash', type: 'currency'},
		{header: 'Rate Date', width: 14, dataIndex: 'interestRateDate', defaultSortColumn: true, defaultSortDirection: 'DESC', filter: {searchFieldName: 'interestRateDate'}, editor: {xtype: 'datefield'}},
		{
			header: 'Rate', width: 12, dataIndex: 'interestRate', type: 'float', useNull: true, editor: {
				xtype: 'floatfield',
				onChangeHandler: function(field, newValue, oldValue) {
					const grid = field.gridEditor.containerGrid;
					grid.calculateInterest(field, newValue.replace(/,/g, ''));
				}
			}
		},
		{header: 'Interest', width: 12, dataIndex: 'interestAmount', summaryType: 'sum', type: 'currency', useNull: true, editor: {xtype: 'floatfield', readOnly: true}},

		{header: 'Day Count', width: 20, dataIndex: 'repo.repoDefinition.dayCountConvention', hidden: true},
		{header: 'Roll', width: 7, dataIndex: 'repo.roll', type: 'boolean', filter: {searchFieldName: 'roll'}, hidden: true},
		{header: 'Trader', width: 13, dataIndex: 'repo.traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}, hidden: true},
		{header: 'Confirmed', width: 7, dataIndex: 'repo.confirmed', type: 'boolean', renderer: TCG.renderCheck, align: 'center', filter: {searchFieldName: 'confirmed'}, hidden: true},
		{header: 'Confirmation Note', width: 50, dataIndex: 'repo.confirmationNote', searchFieldName: 'confirmationNote', hidden: true},
		{header: 'Confirmed On', width: 20, dataIndex: 'repo.confirmedDate', searchFieldName: 'confirmedDate', hidden: true},
		{header: 'Traded On', width: 13, dataIndex: 'repo.tradeDate', searchFieldName: 'tradeDate', hidden: true},
		{header: 'Settled On', width: 13, dataIndex: 'repo.settlementDate', searchFieldName: 'settlementDate', hidden: true},
		{header: 'Action Date', width: 13, dataIndex: 'repo.maturityDate', searchFieldName: 'maturityDate', hidden: true},
		{
			header: 'Matures On', width: 13, dataIndex: 'repo.maturitySettlementDate', hidden: true,
			filter: {
				searchFieldName: 'maturitySettlementDate',
				orNull: true
			}
		}
	],

	getLoadParams: function(firstLoad) {
		const t = this.getTopToolbar();
		let dateValue;
		const bd = TCG.getChildByName(t, 'interestRateDate');
		if (bd.getValue() !== '') {
			dateValue = (bd.getValue()).format('m/d/Y');
		}
		else {
			dateValue = new Date().format('m/d/Y');
			bd.setValue(dateValue);
		}

		const result = {
			interestRateDate: dateValue,
			multipleInterestRatesPerPeriod: true,
			requestedMaxDepth: 6
		};
		if (this.excludeCancelled) {
			result.excludeWorkflowStateName = 'Cancelled';
		}
		if (this.workflowStatusName) {
			result.workflowStatusName = this.workflowStatusName;
		}
		if (this.workflowStateName) {
			result.workflowStateName = this.workflowStateName;
		}
		return result;
	},
	removeRepoRates: function(repoRates, count, gridPanel) {
		const grid = gridPanel.grid;
		const repoRate = repoRates[count];
		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Deleting...',
			params: {id: repoRate.id},
			conf: {rowId: repoRate.id},
			onLoad: function(record, conf) {
				count++;
				if (count === repoRates.length) { // refresh after all repos were transitioned
					gridPanel.reload();
				}
				else {
					gridPanel.removeRepoRates(repoRates, count, gridPanel);
				}
			}
		});
		loader.load(gridPanel.editor.getDeleteURL());
	},

	editor: {
		detailPageClass: 'Clifton.lending.repo.RepoWindow',
		addEnabled: false,

		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Save',
				tooltip: '',
				iconCls: 'disk',
				handler: function() {
					gridPanel.saveInterestRates();
				}
			});
			t.add('-');

			TCG.grid.GridEditor.prototype.addEditButtons.call(this, t);
		},
		getDetailPageId: function(gridPanel, row) {
			return row.json.repo.id;
		},
		addToolbarDeleteButton: function(toolBar) {
			toolBar.add({
				text: 'Remove',
				tooltip: 'Remove selected item(s)',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					const gridPanel = this.getGridPanel();
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
					}
					else {
						Ext.Msg.confirm('Delete Selected Rows', 'Would you like to delete selected rows?', function(a) {
							if (a === 'yes') {
								const ut = sm.getSelections();
								// sort repos by tradeDate and then averageUnitPrice: proper booking order
								const repoRates = [];
								let addedRepoRates = '';
								for (let i = 0; i < ut.length; i++) {
									const repoRate = ut[i].json;
									if (addedRepoRates.indexOf(repoRate.id)) {
										addedRepoRates += repoRate.id;
										repoRates.push(repoRate);
									}
								}
								const count = 0;
								gridPanel.removeRepoRates(repoRates, count, gridPanel);
							}
						});
					}
				}
			});
			toolBar.add('-');
		}
	}
});
Ext.reg('lending-repoInterestRateGrid', Clifton.lending.repo.RepoInterestRateGrid);

Clifton.lending.repo.RepoGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'lendingRepoListFind',
	xtype: 'gridpanel',
	instructions: 'Repos (a.k.a. Repurchase Agreements) are typically used for short-term borrowing. Repos are exchanges of collateral (usually government securities) for cash in which the dealer agrees to repurchase the collateral for an agreed-upon price at some date in the future.',
	wikiPage: 'IT/REPO+Requirements',
	excludeCancelled: true,
	importTableName: [
		{table: 'LendingRepo', label: 'Lending Repo Rolls', importComponentName: 'Clifton.lending.repo.RepoRollUploadWindow'}
	],
	getColumnModel: function() {
		let columnOverrides = this.columnOverrides;
		if (this.workflowStatusName) { // hide status if always the same
			if (!columnOverrides) {
				columnOverrides = [];
			}
			columnOverrides.push({dataIndex: 'workflowStatus.name', hidden: true});
		}
		return Clifton.lending.repo.RepoGrid.superclass.getColumnModel.apply(this, arguments);
	},
	columns: [
		{header: 'ID', width: 11, dataIndex: 'id', hidden: true},
		{header: 'REPO Type', width: 13, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', loadAll: true, displayField: 'label', url: 'lendingRepoTypeListFind.json'}},
		{header: 'Workflow Status', width: 17, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}},
		{header: 'Workflow State', width: 18, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, defaultSortColumn: true},
		{header: 'Client Account', width: 35, dataIndex: 'repoDefinition.clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 35, dataIndex: 'repoDefinition.holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
		{header: 'REPO Account', width: 35, dataIndex: 'repoDefinition.repoInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'repoInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false&accountType=REPO'}, hidden: true},
		{header: 'Counterparty', width: 30, dataIndex: 'repoDefinition.counterparty.label', filter: {searchFieldName: 'counterpartyName'}},
		{header: 'Description', width: 50, dataIndex: 'description', hidden: true},
		{header: 'Term', tooltip: 'Number of days the REPO is for', width: 10, dataIndex: 'term', useNull: true, type: 'int'},
		{header: 'Cancellation Term', tooltip: 'The number of days needed to give notice of a cancellation.  This is currently used for Evergreen REPO\'s.', width: 10, dataIndex: 'cancellationNoticeTerm', useNull: true, type: 'int', hidden: true},
		{header: 'Rate', width: 9, dataIndex: 'interestRate', type: 'float', useNull: true},
		{header: 'Reverse', width: 7, dataIndex: 'repoDefinition.reverse', type: 'boolean', hidden: true},

		{header: 'Security', width: 18, dataIndex: 'repoDefinition.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'CCY', width: 10, dataIndex: 'repoDefinition.investmentSecurity.instrument.tradingCurrency.symbol', hidden: true, filter: {type: 'combo', searchFieldName: 'ccySecurityId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
		{header: 'Index Ratio', width: 12, dataIndex: 'indexRatio', type: 'float', hidden: true},
		{header: 'Quantity', width: 12, dataIndex: 'quantityIntended', type: 'float', hidden: true},
		{header: 'Face', width: 15, dataIndex: 'originalFace', type: 'float'},
		{header: 'Price', width: 10, dataIndex: 'price', type: 'float', hidden: true},
		{header: 'Market Value', width: 20, dataIndex: 'marketValue', type: 'currency', hidden: true},
		{header: 'Haircut %', width: 10, dataIndex: 'haircutPercent', type: 'percent', hidden: true, tooltip: 'Net Cash = (Haircut Percent > 100) ? (100 * Market Value / Haircut Percent) : (Market Value * Haircut Percent / 100)'},
		{header: 'Net Cash', width: 17, dataIndex: 'netCash', type: 'currency'},
		{header: 'Interest', width: 12, dataIndex: 'interestAmount', type: 'currency'},

		{header: 'Roll', width: 7, dataIndex: 'roll', type: 'boolean', filter: {searchFieldName: 'roll'}},

		{header: 'Trader', width: 13, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}, defaultSortColumn: true},
		{header: 'Confirmed', width: 13, dataIndex: 'confirmed', type: 'boolean', hidden: true, renderer: TCG.renderCheck, align: 'center', filter: {searchFieldName: 'confirmed'}},
		{header: 'Confirmation Note', width: 50, dataIndex: 'confirmationNote', hidden: true},
		{header: 'Confirmed On', width: 20, dataIndex: 'confirmedDate', hidden: true},
		{header: 'Traded On', width: 13, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
		{header: 'Settled On', width: 13, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true},
		{header: 'Action Date', width: 13, dataIndex: 'maturityDate', searchFieldName: 'maturityDate', hidden: true},
		{
			header: 'Matures On', width: 13, dataIndex: 'maturitySettlementDate',
			filter: {
				searchFieldName: 'maturitySettlementDate',
				orNull: true
			}
		}
	],
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('maturitySettlementDate', {'after': new Date().add(Date.DAY, -1)});
		}
		const result = {
			requestedMaxDepth: 6
		};
		if (this.excludeCancelled) {
			result.excludeWorkflowStateName = 'Cancelled';
		}
		if (this.workflowStatusName) {
			result.workflowStatusName = this.workflowStatusName;
		}
		if (this.workflowStateName) {
			result.workflowStateName = this.workflowStateName;
		}
		return result;
	},
	editor: {
		detailPageClass: 'Clifton.lending.repo.RepoWindow',
		copyURL: 'lendingRepoCopy.json',
		deleteURL: 'lendingRepoDelete.json',

		getDeleteParams: function(selectionModel) {
			const row = selectionModel.getSelected();
			return {id: row.id};
		},
		getDetailPageId: function(gridPanel, row) {
			return row.id;
		},
		addToolbarAddButton: function(t) {
			const grid = this.getGridPanel();
			t.add({
				text: 'New Repo',
				iconCls: 'add',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'Bond Repo',
						iconCls: 'bonds',
						handler: function() {
							TCG.createComponent('Clifton.lending.repo.RepoWindow', {investmentType: 'Bonds', openerCt: grid});
						}
					}]
				})
			}, '-');
		},
		copyHandler: function(gridPanel) {
			const sm = gridPanel.grid.getSelectionModel();
			if (sm.getCount() === 0) {
				TCG.showError('Please select a row to be copied.', 'No Row(s) Selected');
			}
			else if (sm.getCount() !== 1) {
				TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
			}
			else {
				Ext.Msg.confirm('Copy REPO', 'You are about to copy this REPO.<br/>Would you like to continue?', function(a) {
					if (a === 'yes') {
						const params = {};
						params[this.copyIdParameter] = sm.getSelected().id;
						params['copyDefinition'] = true;
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							waitMsg: 'Copying...',
							params: params,
							conf: params,
							onLoad: function(record, conf) {
								gridPanel.reload();
								}
							});
							loader.load(this.copyURL);
						}
					}, this
				);
			}
		}
	}
});
Ext.reg('lending-repoGrid', Clifton.lending.repo.RepoGrid);

Clifton.lending.repo.RepoAccruedInterestGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'lendingRepoAccruedInterestListFind',
	xtype: 'gridpanel',
	instructions: 'Displays for the selected REPOs and date the accrued interest to date, and accrued interest on the selected date.  Note that for calculating accrued interest to a given date, the interest from the following non-business days are also included.  So, for example, Friday Accrued Interest amounts also include Saturday and Sunday.  If a date was changed for interest calculation, interest values are displayed in blue with lookup date on hover.',
	excludeCancelled: true,
	columns: [
		{header: 'ID', width: 11, dataIndex: 'repo.id', hidden: true},
		{header: 'Type', width: 11, dataIndex: 'repo.type.name', filter: {type: 'combo', searchFieldName: 'typeId', loadAll: true, displayField: 'label', url: 'lendingRepoTypeListFind.json'}},
		{header: 'Workflow Status', width: 17, dataIndex: 'repo.workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}},
		{header: 'Workflow State', width: 18, dataIndex: 'repo.workflowState.name', filter: {searchFieldName: 'workflowStateName'}, defaultSortColumn: true},
		{header: 'Client Account', width: 35, dataIndex: 'repo.repoDefinition.clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 35, dataIndex: 'repo.repoDefinition.holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
		{header: 'REPO Account', width: 35, dataIndex: 'repo.repoDefinition.repoInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'repoInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false&accountType=REPO'}, hidden: true},
		{header: 'Counterparty', width: 30, dataIndex: 'repo.repoDefinition.counterparty.label', filter: {searchFieldName: 'counterpartyName'}},
		{header: 'Description', width: 50, dataIndex: 'repo.description', hidden: true, filter: {searchFieldName: 'description'}},
		{header: 'Term', tooltip: 'Number of days the REPO is for', width: 10, dataIndex: 'repo.term', useNull: true, type: 'int', filter: {searchFieldName: 'term'}},
		{header: 'Cancellation Term', tooltip: 'The number of days needed to give notice of a cancellation.  This is currently used for Evergreen REPO\'s.', width: 10, dataIndex: 'cancellationNoticeTerm', useNull: true, type: 'int', hidden: true},
		{header: 'Rate', width: 9, dataIndex: 'repo.interestRate', type: 'float', useNull: true, filter: {searchFieldName: 'interestRate'}},
		{header: 'Reverse', width: 7, dataIndex: 'repo.repoDefinition.reverse', type: 'boolean', hidden: true},

		{header: 'Security', width: 18, dataIndex: 'repo.repoDefinition.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'CCY', width: 10, dataIndex: 'repo.repoDefinition.investmentSecurity.instrument.tradingCurrency.symbol', hidden: true, filter: {type: 'combo', searchFieldName: 'ccySecurityId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
		{header: 'Index Ratio', width: 12, dataIndex: 'repo.indexRatio', type: 'float', hidden: true, filter: {searchFieldName: 'indexRatio'}},
		{header: 'Quantity', width: 12, dataIndex: 'repo.quantityIntended', type: 'float', hidden: true, filter: {searchFieldName: 'quantityIntended'}},
		{header: 'Face', width: 15, dataIndex: 'repo.originalFace', type: 'float', filter: {searchFieldName: 'originalFace'}},
		{header: 'Price', width: 10, dataIndex: 'repo.price', type: 'float', hidden: true, filter: {searchFieldName: 'price'}},
		{header: 'Market Value', width: 20, dataIndex: 'repo.marketValue', type: 'currency', hidden: true, filter: {searchFieldName: 'marketValue'}},
		{header: 'Haircut %', width: 10, dataIndex: 'repo.haircutPercent', type: 'percent', hidden: true, filter: {searchFieldName: 'haircutPercent'}, tooltip: 'Net Cash = (Haircut Percent > 100) ? (100 * Market Value / Haircut Percent) : (Market Value * Haircut Percent / 100)'},
		{header: 'Net Cash', width: 17, dataIndex: 'repo.netCash', type: 'currency', filter: {searchFieldName: 'netCash'}},
		{header: 'Interest', width: 12, dataIndex: 'repo.interestAmount', type: 'currency', filter: {searchFieldName: 'interestAmount'}},

		{header: 'Roll', width: 7, dataIndex: 'repo.roll', type: 'boolean', filter: {searchFieldName: 'roll'}},

		{header: 'Trader', width: 13, dataIndex: 'repo.traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}, defaultSortColumn: true},
		{header: 'Confirmed', width: 13, dataIndex: 'repo.confirmed', type: 'boolean', hidden: true, renderer: TCG.renderCheck, align: 'center', filter: {searchFieldName: 'confirmed'}},
		{header: 'Confirmation Note', width: 50, dataIndex: 'repo.confirmationNote', hidden: true},
		{header: 'Confirmed On', width: 20, dataIndex: 'repo.confirmedDate', hidden: true, filter: {searchFieldName: 'confirmedDate'}},
		{header: 'Traded On', width: 13, dataIndex: 'repo.tradeDate', filter: {searchFieldName: 'tradeDate'}},
		{header: 'Settled On', width: 13, dataIndex: 'repo.settlementDate', searchFieldName: 'settlementDate', hidden: true},
		{header: 'Action Date', width: 13, dataIndex: 'repo.maturityDate', searchFieldName: 'maturityDate', hidden: true},
		{header: 'Matures On', width: 13, dataIndex: 'repo.maturitySettlementDate', filter: false},

		{
			header: 'Accrued Interest Measure Date', width: 16, dataIndex: 'accruedInterestMeasureDate', type: 'date', filter: false,
			qtip: 'Populated when Measure Date for accrued interest falls before a non-business day.  Accrued interest measure date is then moved to the next business day - 1 day.'
		},
		{
			header: 'Total Accrued Interest', width: 16, dataIndex: 'accruedInterestToDate', type: 'currency', filter: false, negativeInRed: true, positiveInGreen: true, summaryType: 'sum',
			renderer: function(v, metaData, r) {
				if (r.data.accruedInterestMeasureDate !== '') {
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'Accrued Interest Included to ' + TCG.renderDate(r.data.accruedInterestMeasureDate) + '\'';
				}
				return TCG.renderAmount(v, true, '0,000.00');
			}
		},
		{
			header: 'Date Accrued Interest', width: 16, dataIndex: 'measureDateAccruedInterest', type: 'currency', filter: false, summaryType: 'sum', negativeInRed: true, positiveInGreen: true,
			renderer: function(v, metaData, r) {
				if (r.data.accruedInterestMeasureDate !== '') {
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'Accrued Interest Included to ' + TCG.renderDate(r.data.accruedInterestMeasureDate) + '\'';
				}
				return TCG.renderAmount(v, true, '0,000.00');
			}
		}
	],
	plugins: {ptype: 'gridsummary'},
	getTopToolbarFilters: function(toolbar) {
		return [{fieldLabel: 'Measure Date', xtype: 'toolbar-datefield', name: 'measureDate'}];
	},
	getLoadParams: function(firstLoad) {
		const t = this.getTopToolbar();
		let dateValue;
		const bd = TCG.getChildByName(t, 'measureDate');
		if (bd.getValue() !== '') {
			dateValue = (bd.getValue()).format('m/d/Y');
		}
		else {
			dateValue = new Date().format('m/d/Y');
			bd.setValue(dateValue);
		}

		const result = {
			measureDate: dateValue,
			requestedMaxDepth: 6
		};
		if (this.excludeCancelled) {
			result.excludeWorkflowStateName = 'Cancelled';
		}
		if (this.workflowStatusName) {
			result.workflowStatusName = this.workflowStatusName;
		}
		if (this.workflowStateName) {
			result.workflowStateName = this.workflowStateName;
		}
		return result;
	},
	editor: {
		detailPageClass: 'Clifton.lending.repo.RepoWindow',
		drillDownOnly: true,

		getDetailPageId: function(gridPanel, row) {
			return row.json.repo.id;
		}
	}
});
Ext.reg('lending-repoAccruedInterestGrid', Clifton.lending.repo.RepoAccruedInterestGrid);

Clifton.lending.repo.BookingDateField = Ext.extend(TCG.form.LinkField, {
	fieldLabel: 'Booking Date',
	name: 'createDate', // need real field that will be set on load so that we can override with booking date
	dateFieldName: 'bookingDate',
	detailIdField: 'id',
	detailPageClass: 'Clifton.accounting.journal.JournalWindow',
	submitDetailField: false,
	type: 'date',
	journalSequence: 1,

	// drill into first booked fill journal
	getDetailIdFieldValue: function(formPanel) {
		const sourceId = formPanel.getForm().findField(this.detailIdField, true).getValue();
		const journal = TCG.data.getData('accountingJournalBySourceAndSequence.json?requestedMaxDepth=2&sourceTable=LendingRepo&sourceId=' + sourceId + '&journalSequence=' + this.journalSequence, this);
		return journal ? journal.id : null;
	}
});
Ext.reg('repo-bookingdate', Clifton.lending.repo.BookingDateField);
