package com.clifton.lending.api.repo;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoAccruedInterest;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.search.LendingRepoSearchForm;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author vgomelsky
 */
@Component
public class LendingRepoApiServiceImpl implements LendingRepoApiService {

	private LendingRepoService lendingRepoService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Repo getRepo(int id) {
		LendingRepo repo = getLendingRepoService().getLendingRepo(id);
		return Optional.ofNullable(repo).map(LendingRepo::toRepo).orElse(null);
	}


	@Override
	public List<Repo> getRepoList(RepoSearchCommand searchCommand) {
		LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
		BeanUtils.copyProperties(searchCommand, searchForm);
		List<LendingRepo> results = getLendingRepoService().getLendingRepoList(searchForm);
		return CollectionUtils.getStream(results).map(LendingRepo::toRepo).collect(Collectors.toList());
	}


	@Override
	public List<Repo> getRepoListForIds(Integer[] ids) {
		List<LendingRepo> results = getLendingRepoService().getLendingRepoListForIds(ids, false);
		return CollectionUtils.getStream(results).map(LendingRepo::toRepo).collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	//////////           Accrued Interest Calculators                 //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getRepoAccruedInterest(int repoId, Date date) {
		return getLendingRepoService().getLendingRepoAccruedInterest(repoId, date);
	}


	@Override
	public BigDecimal getRepoAccruedInterestForCommand(RepoSearchCommand searchCommand, Date measureDate) {
		LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
		BeanUtils.copyProperties(searchCommand, searchForm);
		List<LendingRepoAccruedInterest> accruedInterestList = getLendingRepoService().getLendingRepoAccruedInterestList(measureDate, searchForm);
		return CoreMathUtils.sumProperty(accruedInterestList, LendingRepoAccruedInterest::getAccruedInterestToDate);
	}

	////////////////////////////////////////////////////////////////////////////
	//////////             Getters and Setters                        //////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}
}
