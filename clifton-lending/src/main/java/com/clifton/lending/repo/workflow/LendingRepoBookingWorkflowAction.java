package com.clifton.lending.repo.workflow;


import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.interest.LendingRepoInterestRate;
import com.clifton.lending.repo.interest.LendingRepoInterestRateService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.Date;
import java.util.List;


/**
 * The <code>LendingRepoBookingWorkflowAction</code> ...
 *
 * @author mwacker
 */
public class LendingRepoBookingWorkflowAction implements WorkflowTransitionActionHandler<LendingRepo> {

	private boolean doNotPostWhenBooking = false;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private AccountingBookingService<LendingRepo> accountingBookingService;
	private LendingRepoInterestRateService lendingRepoInterestRateService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepo processAction(LendingRepo repo, WorkflowTransition transition) {
		if (repo.getBookingDate() != null) {
			ValidationUtils.assertNotNull(repo.getMaturityDate(), "Cannot mature a repo with maturity date [" + DateUtils.fromDate(repo.getMaturityDate()) + "].");
		}
		if (!repo.getType().isMaturityDateRequiredForOpen() && (repo.getBookingDate() == null) && !LendingRepoService.REPO_UNBOOKED_WORKFLOW_STATE_NAME.equals(repo.getWorkflowState().getName())) {
			ValidationUtils.assertNull(repo.getMaturityDate(), "Cannot book opening of type [" + repo.getType().getName() + " with maturity date [" + DateUtils.fromDate(repo.getMaturityDate()) + "].");
		}
		if (!repo.getType().isMaturityDateRequiredForOpen() && (repo.getBookingDate() != null) && (repo.getMaturityBookingDate() == null)) {
			validateRepoInterestRates(repo);
		}

		// book the repo
		if ((repo.getBookingDate() == null) || (repo.getMaturityBookingDate() == null)) {
			getAccountingBookingService().bookAccountingJournal(AccountingJournalType.REPO_JOURNAL, repo, !isDoNotPostWhenBooking());
		}
		return repo;
	}


	////////////////////////////////////////////////////////////////////////////
	////////                     Helper Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////
	private void validateRepoInterestRates(LendingRepo repo) {
		Calendar calendar = getCalendarSetupService().getCalendarByName(LendingRepoService.LENDING_REPO_CALENDAR_NAME);
		Date lastRateDate = repo.getMaturitySettlementDate() == null ? null : getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(repo.getMaturitySettlementDate(), calendar.getId()));

		List<LendingRepoInterestRate> rateList = getLendingRepoInterestRateService().getLendingRepoInterestRateByRepo(repo.getId());

		// check for extra events
		int extraDays = 0;
		StringBuilder extraDaysError = new StringBuilder();
		for (LendingRepoInterestRate rate : CollectionUtils.getIterable(rateList)) {
			if ((DateUtils.compare(rate.getInterestRateDate(), repo.getSettlementDate(), false) < 0) || (DateUtils.compare(rate.getInterestRateDate(), lastRateDate, false) > 0)
					|| !getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(rate.getInterestRateDate(), calendar.getId()))) {
				extraDaysError.append("Extra interest of [").append(CoreMathUtils.formatNumberDecimal(rate.getInterestAmount())).append("] exists on [").append(DateUtils.fromDate(rate.getInterestRateDate(), DateUtils.DATE_FORMAT_INPUT)).append("].<br/>");
				extraDays++;
			}
		}

		// check for missing events
		int missingDays = 0;
		StringBuilder missingDaysError = new StringBuilder();
		Date date = repo.getSettlementDate();
		// safety count
		int count = 0;
		while (DateUtils.compare(lastRateDate, date, false) >= 0) {
			if (getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendar.getId()))) {
				LendingRepoInterestRate repoRate = getLendingRepoInterestRateService().getLendingRepoInterestRateByRepoAndDate(repo.getId(), date);
				if (repoRate == null) {
					missingDaysError.append("No interest rate exists for [").append(DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT)).append("].<br/>");
					missingDays++;
				}
			}
			date = DateUtils.addDays(date, 1);

			// break if we've been through this too many times and might be in an infinite loop
			count++;
			if (count > 2000) {
				break;
			}
		}

		if (extraDays > 0 || missingDays > 0) {
			StringBuilder error = new StringBuilder();
			error.append("<br/>");
			if (extraDays > 0) {
				error.append("<br/>[").append(extraDays).append("] extra day(s) of interest rate(s).<br/>");
				error.append(extraDaysError.toString()).append("<br/>");
			}
			if (missingDays > 0) {
				error.append("<br/>[").append(missingDays).append("] missing day(s) of interest rate(s).<br/>");
				error.append(missingDaysError.toString());
			}
			throw new RuntimeException(error.toString() + "<br/>");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBookingService<LendingRepo> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<LendingRepo> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public boolean isDoNotPostWhenBooking() {
		return this.doNotPostWhenBooking;
	}


	public void setDoNotPostWhenBooking(boolean doNotPostWhenBooking) {
		this.doNotPostWhenBooking = doNotPostWhenBooking;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public LendingRepoInterestRateService getLendingRepoInterestRateService() {
		return this.lendingRepoInterestRateService;
	}


	public void setLendingRepoInterestRateService(LendingRepoInterestRateService lendingRepoInterestRateService) {
		this.lendingRepoInterestRateService = lendingRepoInterestRateService;
	}
}
