package com.clifton.lending.repo.pairoff.search;


import com.clifton.core.dataaccess.search.SearchField;


public class LendingRepoPairOffSecuritySearchForm extends LendingRepoPairOffAccountSearchForm {

	@SearchField(searchField = "investmentSecurity.id")
	private Integer securityId;

	@SearchField(searchField = "symbol", searchFieldPath = "investmentSecurity")
	private String securitySymbol;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}
}
