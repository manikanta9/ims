package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>LendingRepoTriPartyReverseCashBookingRule</code> transfers cash from the holding to the tri-party account for the
 * opening of a reverse REPO, and from the tri-party to holding account the maturity.
 *
 * @author mwacker
 */
public class LendingRepoTriPartyReverseCashBookingRule extends LendingRepoCashOrCurrencyBookingRule {

	@Override
	public void applyRule(BookingSession<LendingRepo> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		LendingRepo repo = bookingSession.getBookableEntity();

		// this rule only applies to reverse tri-party REPO's
		if (!repo.getRepoDefinition().isReverse() || !repo.getType().isTriPartyRepo()) {
			return;
		}

		if (!isRepoOpening()) {
			List<? extends AccountingJournalDetailDefinition> details = CoreCollectionUtils.clone(journal.getJournalDetailList());

			for (AccountingJournalDetailDefinition entry : CollectionUtils.getIterable(details)) {
				if (AccountingAccount.REPO_REVENUE_INTEREST_INCOME.equals(entry.getAccountingAccount().getName())) {

					AccountingJournalDetail cashExpenseEntry = createTriPartyCashRecord(repo, true, true, getInterestAmount(repo), (AccountingJournalDetail) entry.getParentDefinition());
					journal.addJournalDetail(cashExpenseEntry);
					journal.addJournalDetail(createInterestExpenseRecord(repo, cashExpenseEntry, (AccountingJournalDetail) entry.getParentDefinition(), getInterestAmount(repo),
							cashExpenseEntry.getHoldingInvestmentAccount()));

					AccountingJournalDetail cashIncomeEntry = createTriPartyCashRecord(repo, false, true, getInterestAmount(repo), (AccountingJournalDetail) entry.getParentDefinition());
					journal.addJournalDetail(cashIncomeEntry);
					journal.addJournalDetail(createInterestExpenseRecord(repo, cashIncomeEntry, (AccountingJournalDetail) entry.getParentDefinition(), getInterestAmount(repo).negate(),
							cashIncomeEntry.getHoldingInvestmentAccount()));

					break;
				}
			}
		}

		// add the cash opening
		journal.addJournalDetail(createTriPartyCashRecord(repo, false, false, repo.getNetCash(), null));
		// add the cash closing
		journal.addJournalDetail(createTriPartyCashRecord(repo, true, false, repo.getNetCash(), null));
	}


	private AccountingJournalDetail createTriPartyCashRecord(LendingRepo repo, boolean cashClosing, boolean interest, BigDecimal amount, AccountingJournalDetail positionEntry) {
		InvestmentSecurity tradingCurrency = repo.getRepoDefinition().getInvestmentSecurity().getInstrument().getTradingCurrency();
		InvestmentSecurity clientBaseCurrency = InvestmentUtils.getClientAccountBaseCurrency(repo.getRepoDefinition());

		// get the main holding account, used to determine where the cash records are put
		InvestmentAccount holdingInvestmentAccount;
		if (isRepoOpening()) {
			holdingInvestmentAccount = cashClosing ? repo.getRepoDefinition().getHoldingInvestmentAccount() : repo.getRepoDefinition().getTriPartyInvestmentAccount();
		}
		else {
			holdingInvestmentAccount = !cashClosing ? repo.getRepoDefinition().getHoldingInvestmentAccount() : repo.getRepoDefinition().getTriPartyInvestmentAccount();
		}

		AccountingJournalDetail cashCurrencyEntry = new AccountingJournalDetail();

		cashCurrencyEntry.setHoldingInvestmentAccount(holdingInvestmentAccount);
		cashCurrencyEntry.setClientInvestmentAccount(repo.getRepoDefinition().getClientInvestmentAccount());

		if (isRepoOpening()) {
			cashCurrencyEntry.setTransactionDate(repo.getTradeDate());
			cashCurrencyEntry.setSettlementDate(repo.getSettlementDate());
			cashCurrencyEntry.setOriginalTransactionDate(repo.getTradeDate());
		}
		else {
			cashCurrencyEntry.setTransactionDate(repo.getMaturityDate());
			cashCurrencyEntry.setSettlementDate(repo.getMaturitySettlementDate());
			cashCurrencyEntry.setOriginalTransactionDate(repo.getMaturityDate());
		}

		cashCurrencyEntry.setParentTransaction(null);
		cashCurrencyEntry.setParentDefinition(positionEntry);

		cashCurrencyEntry.setQuantity(null);
		cashCurrencyEntry.setPrice(null);

		if (isRepoOpening()) {
			// when opening use the FX rate from the REPO which is set as of the trade date.
			cashCurrencyEntry.setExchangeRateToBase(repo.getExchangeRateToBase());
		}
		else {
			// when maturing use the FX rate on the REPO maturity date
			BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(repo.fxSourceCompany(), !repo.fxSourceCompanyOverridden(),
					repo.getRepoDefinition().getInvestmentSecurity().getInstrument().getTradingCurrency().getSymbol(),
					repo.getRepoDefinition().getClientInvestmentAccount().getBaseCurrency().getSymbol(), repo.getMaturityDate()).flexibleLookup());
			cashCurrencyEntry.setExchangeRateToBase(fxRate);
		}

		// go the opposite direction of the position
		cashCurrencyEntry.setLocalDebitCredit(!cashClosing ? amount : amount.negate());
		cashCurrencyEntry.setBaseDebitCredit(MathUtils.multiply(cashCurrencyEntry.getLocalDebitCredit(), cashCurrencyEntry.getExchangeRateToBase(), 2));
		cashCurrencyEntry.setPositionCommission(BigDecimal.ZERO);

		String interestAccountName = repo.getRepoDefinition().isReverse() ? AccountingAccount.REPO_REVENUE_INTEREST_INCOME : AccountingAccount.REPO_EXPENSE_INTEREST_EXPENSE;
		if (clientBaseCurrency.equals(tradingCurrency)) {
			cashCurrencyEntry.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CASH));
			cashCurrencyEntry.setInvestmentSecurity(clientBaseCurrency);
			cashCurrencyEntry.setPositionCostBasis(BigDecimal.ZERO);

			String description;
			if (interest) {
				description = interestAccountName + " from " + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO " + (isRepoOpening() ? "opening" : "maturity") + " for "
						+ repo.getInvestmentSecurity().getSymbol();
			}
			else {
				description = "Cash " + (isRepoOpening() ? "transfer to fund " : "transfer from ") + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO "
						+ (isRepoOpening() ? "opening" : "maturity") + " for " + repo.getInvestmentSecurity().getSymbol();
			}
			cashCurrencyEntry.setDescription(description);
		}
		else {
			cashCurrencyEntry.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
			cashCurrencyEntry.setInvestmentSecurity(tradingCurrency);
			cashCurrencyEntry.setPositionCostBasis(cashCurrencyEntry.getLocalDebitCredit());
			String description;
			if (interest) {
				description = interestAccountName + " from " + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO " + (isRepoOpening() ? "opening" : "maturity") + " for "
						+ repo.getInvestmentSecurity().getSymbol();
			}
			else {
				description = "Currency " + (isRepoOpening() ? "transfer to fund " : "transfer from ") + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO "
						+ (isRepoOpening() ? "opening" : "maturity") + " for " + repo.getInvestmentSecurity().getSymbol();
			}
			cashCurrencyEntry.setDescription(description);
		}

		cashCurrencyEntry.setOpening(true);
		return cashCurrencyEntry;
	}
}
