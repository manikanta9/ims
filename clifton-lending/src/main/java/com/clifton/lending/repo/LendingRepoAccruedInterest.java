package com.clifton.lending.repo;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.date.DateUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>LendingRepoAccruedInterest</code> can be used to display a LendingRepo
 * and the calculated AccruedInterest to a date, as well as the daily accrued interest value
 *
 * @author manderson
 */
@NonPersistentObject
public class LendingRepoAccruedInterest {

	private LendingRepo repo;

	private Date measureDate;

	/**
	 * Because LendingRepo Accrued interest for a measure date also includes
	 * the following non business days - this date property can be set so users know which date
	 * is actually being included (only populated when different than measureDate)
	 */
	private Date accruedInterestMeasureDate;

	/**
	 * Accrued Interest value since previous business day
	 */
	private BigDecimal measureDateAccruedInterest;

	/**
	 * Accrued interest for the repo from start until measure date
	 */
	private BigDecimal accruedInterestToDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoAccruedInterest(LendingRepo repo, Date measureDate, Date accruedInterestMeasureDate, BigDecimal accruedInterestToDate, BigDecimal measureDateAccruedInterest) {
		this.repo = repo;
		this.measureDate = measureDate;
		this.accruedInterestToDate = accruedInterestToDate;
		this.measureDateAccruedInterest = measureDateAccruedInterest;
		if (accruedInterestMeasureDate != null && DateUtils.compare(measureDate, accruedInterestMeasureDate, false) != 0) {
			this.accruedInterestMeasureDate = accruedInterestMeasureDate;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepo getRepo() {
		return this.repo;
	}


	public void setRepo(LendingRepo repo) {
		this.repo = repo;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public BigDecimal getMeasureDateAccruedInterest() {
		return this.measureDateAccruedInterest;
	}


	public void setMeasureDateAccruedInterest(BigDecimal measureDateAccruedInterest) {
		this.measureDateAccruedInterest = measureDateAccruedInterest;
	}


	public BigDecimal getAccruedInterestToDate() {
		return this.accruedInterestToDate;
	}


	public void setAccruedInterestToDate(BigDecimal accruedInterestToDate) {
		this.accruedInterestToDate = accruedInterestToDate;
	}


	public Date getAccruedInterestMeasureDate() {
		return this.accruedInterestMeasureDate;
	}


	public void setAccruedInterestMeasureDate(Date accruedInterestMeasureDate) {
		this.accruedInterestMeasureDate = accruedInterestMeasureDate;
	}
}
