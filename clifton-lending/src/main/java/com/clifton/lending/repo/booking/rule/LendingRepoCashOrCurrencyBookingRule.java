package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.interest.LendingRepoInterestRate;
import com.clifton.lending.repo.interest.LendingRepoInterestRateService;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>LendingRepoPositionOpenBookingRule</code> creates the opening transactions for the assets that are being lent or re-purchased in the REPO  at the same cost as the close.
 *
 * @author mwacker
 */
public class LendingRepoCashOrCurrencyBookingRule extends BaseLendingRepoPositionBookingRule {

	private LendingRepoInterestRateService lendingRepoInterestRateService;


	@Override
	public void applyRule(BookingSession<LendingRepo> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		LendingRepo repo = bookingSession.getBookableEntity();

		AccountingAccount positionAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION);

		List<? extends AccountingJournalDetailDefinition> details = CoreCollectionUtils.clone(journal.getJournalDetailList());

		// TODO: Clean this up to make it easier to follow.

		// get the main holding account, used to determine where the cash records are put
		InvestmentAccount holdingInvestmentAccount;
		if (!repo.getRepoDefinition().isReverse()) {
			// if it's not a reverse REPO then the logic below will put the transactions in the correct accounts so always send true for closingRule
			holdingInvestmentAccount = getHoldingAccount(repo, true);
		}
		else {
			// for a reverse REPO the holding account needs to flipped for opening and closings to make the logic below get the transactions in the correct accounts.
			holdingInvestmentAccount = getHoldingAccount(repo, isRepoOpening());
		}

		BigDecimal interestAmount = getInterestAmount(repo);

		boolean holdingAccountIsClosing = !(repo.getRepoDefinition().isReverse() && !isRepoOpening());
		boolean closingCashAdded = false;
		boolean openingCashAdded = false;
		for (AccountingJournalDetailDefinition positionEntry : CollectionUtils.getIterable(details)) {
			if (positionEntry.getAccountingAccount().equals(positionAccount) && positionEntry.getHoldingInvestmentAccount().equals(holdingInvestmentAccount) && !closingCashAdded) {
				// add the cash for the closing account
				journal.addJournalDetail(createCashRecord(repo, (AccountingJournalDetail) positionEntry, holdingAccountIsClosing, false, repo.getNetCash()));
				closingCashAdded = true;
			}
			else if (!openingCashAdded && positionEntry.getAccountingAccount().equals(positionAccount) && !positionEntry.getHoldingInvestmentAccount().equals(holdingInvestmentAccount)) {
				// add the cash for the opening account
				AccountingJournalDetail cashEntry = createCashRecord(repo, (AccountingJournalDetail) positionEntry, !holdingAccountIsClosing, false, repo.getNetCash());
				journal.addJournalDetail(cashEntry);
				if (!isRepoOpening()) {
					journal.addJournalDetail(createCashRecord(repo, (AccountingJournalDetail) positionEntry, !holdingAccountIsClosing, true, interestAmount));
					journal.addJournalDetail(createInterestExpenseRecord(repo, cashEntry, (AccountingJournalDetail) positionEntry, repo.getRepoDefinition().isReverse() ? interestAmount.negate()
							: interestAmount, repo.getHoldingInvestmentAccount()));
				}
				openingCashAdded = true;
			}
		}
	}


	protected BigDecimal getInterestAmount(LendingRepo repo) {
		if (repo.getType().isMultipleInterestRatesPerPeriod()) {
			List<LendingRepoInterestRate> rateList = getLendingRepoInterestRateService().getLendingRepoInterestRateByRepo(repo.getId());
			BigDecimal interestAmount = BigDecimal.ZERO;
			for (LendingRepoInterestRate rate : CollectionUtils.getIterable(rateList)) {
				ValidationUtils.assertBefore(rate.getInterestRateDate(), repo.getMaturityDate(), "interestRateDate");
				ValidationUtils.assertTrue(rate.getInterestRateDate() != null && (DateUtils.compare(rate.getInterestRateDate(), repo.getSettlementDate(), false) >= 0),
						"Interest rate date must be after or equal to settlement date.", "interestRateDate");
				interestAmount = MathUtils.add(interestAmount, rate.getInterestAmount());
			}
			return interestAmount;
		}
		return repo.getInterestAmount();
	}


	protected AccountingJournalDetail createInterestExpenseRecord(LendingRepo repo, AccountingJournalDetail cashEntry, AccountingJournalDetail positionEntry, BigDecimal interestAmount,
	                                                              InvestmentAccount holdingAccount) {
		//InvestmentSecurity tradingCurrency = positionEntry.getInvestmentSecurity().getInstrument().getTradingCurrency();

		AccountingJournalDetail interestExpenseEntry = new AccountingJournalDetail();
		//BeanUtils.copyProperties(cashEntry, interestExpenseEntry);

		interestExpenseEntry.setFkFieldId(repo.getId());
		interestExpenseEntry.setSystemTable(getLendingRepoTable());

		interestExpenseEntry.setParentDefinition(positionEntry);
		interestExpenseEntry.setQuantity(null);
		interestExpenseEntry.setPrice(null);

		interestExpenseEntry.setTransactionDate(cashEntry.getTransactionDate());
		interestExpenseEntry.setOriginalTransactionDate(cashEntry.getOriginalTransactionDate());
		interestExpenseEntry.setSettlementDate(cashEntry.getSettlementDate());

		if (repo.getRepoDefinition().isReverse()) {
			interestExpenseEntry.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.REPO_REVENUE_INTEREST_INCOME));
		}
		else {
			interestExpenseEntry.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.REPO_EXPENSE_INTEREST_EXPENSE));
		}

		interestExpenseEntry.setInvestmentSecurity(positionEntry.getInvestmentSecurity());

		interestExpenseEntry.setExchangeRateToBase(cashEntry.getExchangeRateToBase());
		interestExpenseEntry.setLocalDebitCredit(interestAmount);
		interestExpenseEntry.setBaseDebitCredit(MathUtils.multiply(interestExpenseEntry.getLocalDebitCredit(), interestExpenseEntry.getExchangeRateToBase(), 2));
		interestExpenseEntry.setPositionCostBasis(BigDecimal.ZERO);
		interestExpenseEntry.setPositionCommission(BigDecimal.ZERO);

		interestExpenseEntry.setOpening(true);
		interestExpenseEntry.setClientInvestmentAccount(repo.getRepoDefinition().getClientInvestmentAccount());
		// NOTE: Must call repo.getHoldingInvestmentAccount() because that will return the correct account for Tri-Party REPO's
		interestExpenseEntry.setHoldingInvestmentAccount(holdingAccount);
		interestExpenseEntry.setDescription(interestExpenseEntry.getAccountingAccount().getName() + " from " + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO "
				+ (isRepoOpening() ? "opening" : "maturity") + " for " + repo.getInvestmentSecurity().getSymbol() + ".");

		return interestExpenseEntry;
	}


	public LendingRepoInterestRateService getLendingRepoInterestRateService() {
		return this.lendingRepoInterestRateService;
	}


	public void setLendingRepoInterestRateService(LendingRepoInterestRateService lendingRepoInterestRateService) {
		this.lendingRepoInterestRateService = lendingRepoInterestRateService;
	}
}
