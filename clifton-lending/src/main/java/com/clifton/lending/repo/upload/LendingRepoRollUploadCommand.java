package com.clifton.lending.repo.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadCommand;
import com.clifton.lending.repo.LendingRepo;


/**
 * The {@link LendingRepoRollUploadCommand} is a {@link FileUploadCommand} for uploading {@link LendingRepo} rolls. This command is used to perform several repo rolls
 * simultaneously.
 *
 * @author mikeh
 * @see LendingRepoRollUploadTemplate
 * @see LendingRepoUploadService
 */
@NonPersistentObject
public class LendingRepoRollUploadCommand extends FileUploadCommand<LendingRepoRollUploadTemplate> {

	/**
	 * If <code>true</code>, partial uploads will be allowed, meaning that individual failures will not roll-back the entire operation. Otherwise, any failures will cause the
	 * entire action to be aborted, resulting in no changes.
	 */
	private boolean partialUploadAllowed;

	/**
	 * RARE and should ONLY be used if getting timeouts running the REPO upload
	 * Will set the position retrieval select command to use OPTION (RECOMPILE) and avoid bad index usage
	 */
	private boolean positionRetrievalOptionRecompile;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<LendingRepoRollUploadTemplate> getUploadBeanClass() {
		return LendingRepoRollUploadTemplate.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Clears the current results for this object. This may be necessary, for example, when re-uploading using an existing form, since that form may have been populated with
	 * results from a previous upload, in which case the client would send a non-empty results string.
	 */
	public void clearResults() {
		setUploadResultsString(null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isPartialUploadAllowed() {
		return this.partialUploadAllowed;
	}


	public void setPartialUploadAllowed(boolean partialUploadAllowed) {
		this.partialUploadAllowed = partialUploadAllowed;
	}


	public boolean isPositionRetrievalOptionRecompile() {
		return this.positionRetrievalOptionRecompile;
	}


	public void setPositionRetrievalOptionRecompile(boolean positionRetrievalOptionRecompile) {
		this.positionRetrievalOptionRecompile = positionRetrievalOptionRecompile;
	}
}
