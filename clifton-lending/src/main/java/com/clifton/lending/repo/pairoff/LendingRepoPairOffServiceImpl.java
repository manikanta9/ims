package com.clifton.lending.repo.pairoff;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.SubselectParameterExpression;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.pairoff.search.LendingRepoExtendedSearchForm;
import com.clifton.lending.repo.pairoff.search.LendingRepoPairOffAccountSearchForm;
import com.clifton.lending.repo.pairoff.search.LendingRepoPairOffSearchForm;
import com.clifton.lending.repo.pairoff.search.LendingRepoPairOffSecuritySearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class LendingRepoPairOffServiceImpl implements LendingRepoPairOffService {

	private AdvancedUpdatableDAO<LendingRepoPairOff, Criteria> lendingRepoPairOffDAO;
	private AdvancedReadOnlyDAO<LendingRepoPairOffAccount, Criteria> lendingRepoPairOffAccountDAO;
	private AdvancedReadOnlyDAO<LendingRepoPairOffSecurity, Criteria> lendingRepoPairOffSecurityDAO;
	private AdvancedReadOnlyDAO<LendingRepoExtended, Criteria> lendingRepoExtendedDAO;

	private DaoSingleKeyListCache<LendingRepoPairOff, Integer> lendingRepoPairOffListByOpeningRepoCache;
	private DaoSingleKeyListCache<LendingRepoPairOff, Integer> lendingRepoPairOffListByMaturingRepoCache;

	private LendingRepoService lendingRepoService;

	////////////////////////////////////////////////////////////////////////////
	////////      LendingRepoPairOffAccount Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<LendingRepoPairOffAccount> getLendingRepoPairOffAccountList(final LendingRepoPairOffAccountSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				// need to add 3 times because the parameter is used in 3 places
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				// need to add 2 times because the parameter is used in 2 places
				criteria.add(new SubselectParameterExpression(new StringType(), searchForm.getExcludeWorkflowStateName()));
				criteria.add(new SubselectParameterExpression(new StringType(), searchForm.getExcludeWorkflowStateName()));

				// need to add 3 times because the parameter is used in 3 places
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				// need to add 2 times because the parameter is used in 2 places
				criteria.add(new SubselectParameterExpression(new StringType(), searchForm.getExcludeWorkflowStateName()));
				criteria.add(new SubselectParameterExpression(new StringType(), searchForm.getExcludeWorkflowStateName()));

				super.configureCriteria(criteria);
			}
		};
		return getLendingRepoPairOffAccountDAO().findBySearchCriteria(config);
	}

	////////////////////////////////////////////////////////////////////////////
	////////      LendingRepoPairOffSecurity Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<LendingRepoPairOffSecurity> getLendingRepoPairOffSecurityList(final LendingRepoPairOffSecuritySearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				// need to add 3 times because the parameter is used in 3 places
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				// need to add 2 times because the parameter is used in 2 places
				criteria.add(new SubselectParameterExpression(new StringType(), searchForm.getExcludeWorkflowStateName()));
				criteria.add(new SubselectParameterExpression(new StringType(), searchForm.getExcludeWorkflowStateName()));

				// need to add 3 times because the parameter is used in 3 places
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				criteria.add(new SubselectParameterExpression(new BooleanType(), searchForm.getConfirmed()));
				// need to add 2 times because the parameter is used in 2 places
				criteria.add(new SubselectParameterExpression(new StringType(), searchForm.getExcludeWorkflowStateName()));
				criteria.add(new SubselectParameterExpression(new StringType(), searchForm.getExcludeWorkflowStateName()));

				super.configureCriteria(criteria);
			}
		};
		return getLendingRepoPairOffSecurityDAO().findBySearchCriteria(config);
	}

	////////////////////////////////////////////////////////////////////////////
	////////         LendingRepoExtended Business Methods          /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<LendingRepoExtended> getLendingRepoExtendedList(final LendingRepoExtendedSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getRolled() != null) {
					criteria.add(Restrictions.ne("openingRolled", true)).add(Restrictions.ne("maturingRolled", true));
				}
			}
		};
		return getLendingRepoExtendedDAO().findBySearchCriteria(config);
	}

	////////////////////////////////////////////////////////////////////////////
	////////          LendingRepoPairOff Business Methods          /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepoPairOff getLendingRepoPairOff(int id) {
		return getLendingRepoPairOffDAO().findByPrimaryKey(id);
	}


	@Override
	public List<LendingRepoPairOff> getLendingRepoPairOffList(final LendingRepoPairOffSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// add search parameters that are dependant on the pair off date
				if (searchForm.getPairOffDate() != null) {
					boolean pairOffDateSet = false;
					// set the restrictions for checking if pair off exist
					if ((searchForm.getCheckOpeningRepoId() != null) || (searchForm.getCheckMaturingRepoId() != null)) {
						ValidationUtils.assertFalse((searchForm.getCheckOpeningRepoId() != null) && (searchForm.getCheckMaturingRepoId() != null),
								"Cannot check both opening and maturing repo's at the same time.");

						String sql = "";
						List<Object> repoSqlParams = new ArrayList<>();
						List<Type> repoSqlParamTypes = new ArrayList<>();

						if (searchForm.getCheckOpeningRepoId() != null) {
							sql = "({alias}.OpeningRepoID = ? OR ({alias}.MaturingRepoID = ? AND {alias}.PairOffDate = ?))";
							repoSqlParams.add(searchForm.getCheckOpeningRepoId());
							repoSqlParamTypes.add(IntegerType.INSTANCE);
							repoSqlParams.add(searchForm.getCheckOpeningRepoId());
							repoSqlParamTypes.add(IntegerType.INSTANCE);
							repoSqlParams.add(DateUtils.fromDate(searchForm.getPairOffDate(), DateUtils.DATE_FORMAT_INPUT));
							repoSqlParamTypes.add(StringType.INSTANCE);
						}
						else if (searchForm.getCheckMaturingRepoId() != null) {
							sql = "({alias}.MaturingRepoID = ? OR ({alias}.OpeningRepoID = ? AND {alias}.PairOffDate = ?))";
							repoSqlParams.add(searchForm.getCheckMaturingRepoId());
							repoSqlParamTypes.add(IntegerType.INSTANCE);
							repoSqlParams.add(searchForm.getCheckMaturingRepoId());
							repoSqlParamTypes.add(IntegerType.INSTANCE);
							repoSqlParams.add(DateUtils.fromDate(searchForm.getPairOffDate(), DateUtils.DATE_FORMAT_INPUT));
							repoSqlParamTypes.add(StringType.INSTANCE);
						}
						pairOffDateSet = true;
						criteria.add(Restrictions.sqlRestriction(sql, repoSqlParams.toArray(), repoSqlParamTypes.toArray(new Type[0])));
					}

					// set the restriction for repo group using a repo id
					if (searchForm.getRepoIdForGroup() != null) {
						criteria.add(Restrictions.sqlRestriction("{alias}.PairOffGroup = (SELECT PairOffGroup FROM LendingRepoPairOff WHERE PairOffDate = {alias}.PairOffDate AND (MaturingRepoID = ? OR OpeningRepoID = ?))",
								new Object[]{searchForm.getRepoIdForGroup(), searchForm.getRepoIdForGroup()},
								new Type[]{IntegerType.INSTANCE, IntegerType.INSTANCE}));
					}

					if (!pairOffDateSet) {
						criteria.add(Restrictions.eq("pairOffDate", searchForm.getPairOffDate()));
					}
				}
				if (searchForm.getOpeningOrMaturingRepoId() != null) {
					criteria.add(Restrictions.sqlRestriction("(MaturingRepoID = ? OR OpeningRepoID = ?)",
							new Object[]{searchForm.getOpeningOrMaturingRepoId(), searchForm.getOpeningOrMaturingRepoId()},
							new Type[]{IntegerType.INSTANCE, IntegerType.INSTANCE}));
				}
			}
		};
		return getLendingRepoPairOffDAO().findBySearchCriteria(config);
	}


	protected List<LendingRepoPairOff> getLendingRepoPairOffListByOpeningRepo(int openingRepoId) {
		return getLendingRepoPairOffListByOpeningRepoCache().getBeanListForKeyValue(getLendingRepoPairOffDAO(), openingRepoId);
	}


	protected List<LendingRepoPairOff> getLendingRepoPairOffListByMaturingRepo(int maturingRepoId) {
		return getLendingRepoPairOffListByMaturingRepoCache().getBeanListForKeyValue(getLendingRepoPairOffDAO(), maturingRepoId);
	}


	protected List<LendingRepoPairOff> getLendingRepoPairOffListByRepoId(Integer openingRepoId, Integer maturingRepoId) {
		List<LendingRepoPairOff> pairOffList;
		if (openingRepoId != null) {
			pairOffList = getLendingRepoPairOffListByOpeningRepo(openingRepoId);
			if (maturingRepoId != null) {
				pairOffList = BeanUtils.filter(pairOffList, pairOff -> BeanUtils.getBeanIdentity(pairOff.getMaturingRepo()), maturingRepoId);
			}
		}
		else {
			pairOffList = getLendingRepoPairOffListByMaturingRepo(maturingRepoId);
		}
		return pairOffList;
	}


	@Override
	public LendingRepoPairOff saveLendingRepoPairOff(LendingRepoPairOff bean) {
		return getLendingRepoPairOffDAO().save(bean);
	}


	@Override
	public void deleteLendingRepoPairOff(int id) {
		getLendingRepoPairOffDAO().delete(id);
	}


	@Override
	@Transactional
	public void deleteLendingRepoPairOffByRepoId(Integer openingRepoId, Integer maturingRepoId) {
		List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffListByRepoId(openingRepoId, maturingRepoId);
		LendingRepoPairOff pairOff = CollectionUtils.getFirstElement(pairOffList);
		if (pairOff != null) {
			deleteLendingRepoPairOffByGroup(pairOff.getPairOffGroup());
		}
	}


	@Override
	@Transactional
	public void splitLendingRepoPairOff(String pairOffGroupId) {
		// delete the existing pair offs
		List<LendingRepoPairOff> pairOffList = DaoUtils.executeWithPostUpdateFlushEnabled(() -> deleteLendingRepoPairOffByGroup(pairOffGroupId));

		// create the new pair offs
		List<Integer> addedOpeningRepoList = new ArrayList<>();
		List<Integer> addedMaturityRepoList = new ArrayList<>();
		List<LendingRepoPairOff> newPairOffList = new ArrayList<>();
		for (LendingRepoPairOff oldPairOff : CollectionUtils.getIterable(pairOffList)) {
			if ((oldPairOff.getOpeningRepo() != null) && !addedOpeningRepoList.contains(oldPairOff.getOpeningRepo().getId())) {
				LendingRepoPairOff pairOff = createOpeningPairoff(oldPairOff.getOpeningRepo(), oldPairOff.getPairOffDate());
				newPairOffList.add(pairOff);
				addedOpeningRepoList.add(oldPairOff.getOpeningRepo().getId());
			}
			if ((oldPairOff.getMaturingRepo() != null) && !addedMaturityRepoList.contains(oldPairOff.getMaturingRepo().getId())) {
				LendingRepoPairOff pairOff = createMaturingPairoff(oldPairOff.getMaturingRepo(), oldPairOff.getPairOffDate());
				newPairOffList.add(pairOff);
				addedMaturityRepoList.add(oldPairOff.getMaturingRepo().getId());
			}
		}
		getLendingRepoPairOffDAO().saveList(newPairOffList);
	}


	@Override
	@Transactional
	public void pairLendingRepoPairOff(LendingRepoPairOffGroup pairOffGroupConfig) {
		removeExistingPairOff(pairOffGroupConfig);
		populateLendingRepoPairOffGroupConfig(pairOffGroupConfig);
		validateLendingRepoPairOffGroupConfig(pairOffGroupConfig);

		String groupId = generatePairOffGroupId(pairOffGroupConfig);
		List<LendingRepoPairOff> pairOffList = new ArrayList<>();

		if (!CollectionUtils.isEmpty(pairOffGroupConfig.getOpeningRepoList()) || !CollectionUtils.isEmpty(pairOffGroupConfig.getMaturingRepoList())) {
			// if there a no opening repo's add a null
			if (CollectionUtils.isEmpty(pairOffGroupConfig.getOpeningRepoList())) {
				for (LendingRepo maturingRepo : CollectionUtils.getIterable(pairOffGroupConfig.getMaturingRepoList())) {
					LendingRepoPairOff pairOff = new LendingRepoPairOff();
					pairOff.setMaturingRepo(maturingRepo);
					pairOff.setPairOffDate(pairOffGroupConfig.getPairOffDate());
					pairOff.setPairOffGroup(groupId);
					pairOffList.add(pairOff);
				}
			}
			// if there a no maturing repo's add a null
			else if (CollectionUtils.isEmpty(pairOffGroupConfig.getMaturingRepoList())) {
				for (LendingRepo newRepo : CollectionUtils.getIterable(pairOffGroupConfig.getOpeningRepoList())) {
					LendingRepoPairOff pairOff = new LendingRepoPairOff();
					pairOff.setOpeningRepo(newRepo);
					pairOff.setPairOffDate(pairOffGroupConfig.getPairOffDate());
					pairOff.setPairOffGroup(groupId);
					pairOffList.add(pairOff);
				}
			}
			else {
				for (LendingRepo newRepo : CollectionUtils.getIterable(pairOffGroupConfig.getOpeningRepoList())) {
					for (LendingRepo maturingRepo : CollectionUtils.getIterable(pairOffGroupConfig.getMaturingRepoList())) {
						LendingRepoPairOff pairOff = new LendingRepoPairOff();
						pairOff.setOpeningRepo(newRepo);
						pairOff.setMaturingRepo(maturingRepo);
						pairOff.setPairOffDate(pairOffGroupConfig.getPairOffDate());
						pairOff.setPairOffGroup(groupId);
						pairOffList.add(pairOff);
					}
				}
			}
		}
		getLendingRepoPairOffDAO().saveList(pairOffList);
	}


	@Override
	@Transactional
	public void setLendingRepoPairOffGroupDate(String pairOffGroupId, Date pairOffDate, String note) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(pairOffGroupId), "Cannot set pair off date for pair off group [" + pairOffGroupId + "].");

		LendingRepoPairOffSearchForm searchForm = new LendingRepoPairOffSearchForm();
		searchForm.setPairOffGroup(pairOffGroupId);
		List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffList(searchForm);
		for (LendingRepoPairOff pairOff : CollectionUtils.getIterable(pairOffList)) {
			ValidationUtils.assertFalse(pairOff.isCompleted(), "Cannot set pair off date on a completed REPO.");

			StringBuilder sb = new StringBuilder();
			//sb.append(pairOff.getPairOffDateChangeNote());
			//sb.append(StringUtils.isEmpty(pairOff.getPairOffDateChangeNote()) ? "" : "\n\r\n\r");
			sb.append(note);
			sb.append(StringUtils.isEmpty(note) ? "" : "\n\r");
			sb.append("Pairoff date changed from [").append(DateUtils.fromDate(pairOff.getPairOffDate(), DateUtils.DATE_FORMAT_INPUT)).append("] to [").append(DateUtils.fromDate(pairOffDate, DateUtils.DATE_FORMAT_INPUT)).append("].");

			pairOff.setPairOffDate(pairOffDate);
			pairOff.setPairOffDateChangeNote(sb.toString());
		}
		getLendingRepoPairOffDAO().saveList(pairOffList);
	}


	@Override
	@Transactional
	public void setLendingRepoPairOffGroupCompleted(String pairOffGroupId, boolean completed) {
		LendingRepoPairOffSearchForm searchForm = new LendingRepoPairOffSearchForm();
		searchForm.setPairOffGroup(pairOffGroupId);
		List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffList(searchForm);
		for (LendingRepoPairOff pairOff : CollectionUtils.getIterable(pairOffList)) {
			pairOff.setCompleted(completed);
		}
		getLendingRepoPairOffDAO().saveList(pairOffList);
	}


	@Override
	public boolean isPairOffPresent(Integer openingRepoId, Integer maturingRepoId) {
		List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffListByRepoId(openingRepoId, maturingRepoId);
		return pairOffList != null && !pairOffList.isEmpty();
	}

	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	private String generatePairOffGroupId(LendingRepoPairOffGroup pairOffGroupConfig) {
		List<String> idList = new ArrayList<>();
		for (LendingRepo repo : CollectionUtils.getIterable(pairOffGroupConfig.getOpeningRepoList())) {
			idList.add(repo.getId().toString());
		}
		for (LendingRepo repo : CollectionUtils.getIterable(pairOffGroupConfig.getMaturingRepoList())) {
			idList.add(repo.getId().toString());
		}
		return StringUtils.join(idList.toArray(new String[idList.size()]), ":");
	}


	private List<LendingRepoPairOff> deleteLendingRepoPairOffByGroup(String pairOffGroupId) {
		LendingRepoPairOffSearchForm searchForm = new LendingRepoPairOffSearchForm();
		searchForm.setPairOffGroup(pairOffGroupId);
		List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffList(searchForm);
		getLendingRepoPairOffDAO().deleteList(pairOffList);
		return pairOffList;
	}


	private LendingRepoPairOff createOpeningPairoff(LendingRepo repo, Date pairOffDate) {
		LendingRepoPairOff pairOff = new LendingRepoPairOff();
		pairOff.setOpeningRepo(repo);
		pairOff.setPairOffDate(pairOffDate);
		pairOff.setPairOffGroup(repo.getId().toString());
		return pairOff;
	}


	private LendingRepoPairOff createMaturingPairoff(LendingRepo repo, Date pairOffDate) {
		LendingRepoPairOff pairOff = new LendingRepoPairOff();
		pairOff.setMaturingRepo(repo);
		pairOff.setPairOffDate(pairOffDate);
		pairOff.setPairOffGroup(repo.getId().toString());
		return pairOff;
	}


	private void removeExistingPairOff(LendingRepoPairOffGroup pairOffGroupConfig) {
		for (String pairOffGroup : CollectionUtils.getIterable(pairOffGroupConfig.getRemoveGroupList())) {
			deleteLendingRepoPairOffByGroup(pairOffGroup);
		}
		// delete any single pairs
		if (pairOffGroupConfig.isOverrideSinglePairs()) {
			for (LendingRepo repo : CollectionUtils.getIterable(pairOffGroupConfig.getOpeningRepoList())) {
				List<LendingRepoPairOff> pairOffList = BeanUtils.filter(getLendingRepoPairOffListByOpeningRepo(repo.getId()), pairOff -> pairOff.getMaturingRepo() == null);
				getLendingRepoPairOffDAO().deleteList(pairOffList);
			}
			for (LendingRepo repo : CollectionUtils.getIterable(pairOffGroupConfig.getMaturingRepoList())) {
				List<LendingRepoPairOff> pairOffList = BeanUtils.filter(getLendingRepoPairOffListByMaturingRepo(repo.getId()), pairOff -> pairOff.getOpeningRepo() == null);
				getLendingRepoPairOffDAO().deleteList(pairOffList);
			}
		}
	}


	private void populateLendingRepoPairOffGroupConfig(LendingRepoPairOffGroup pairOffGroupConfig) {
		List<LendingRepo> openingRepoList = new ArrayList<>();
		pairOffGroupConfig.getOpeningRepoList().stream().forEach(repo -> {
			LendingRepo foundRepo = getLendingRepoService().getLendingRepo(repo.getId());
			if (foundRepo != null) {
				openingRepoList.add(foundRepo);
			}
		});
		pairOffGroupConfig.setOpeningRepoList(openingRepoList);

		List<LendingRepo> maturingRepoList = new ArrayList<>();
		pairOffGroupConfig.getMaturingRepoList().stream().forEach(repo -> {
			LendingRepo foundRepo = getLendingRepoService().getLendingRepo(repo.getId());
			if (foundRepo != null) {
				maturingRepoList.add(foundRepo);
			}
		});
		pairOffGroupConfig.setMaturingRepoList(maturingRepoList);
	}


	private void validateLendingRepoPairOffGroupConfig(LendingRepoPairOffGroup pairOffGroupConfig) {
		// validate the list of new repo's
		for (LendingRepo repo : CollectionUtils.getIterable(pairOffGroupConfig.getOpeningRepoList())) {
			LendingRepoPairOffSearchForm searchForm = new LendingRepoPairOffSearchForm();
			searchForm.setCheckOpeningRepoId(repo.getId());
			searchForm.setPairOffDate(pairOffGroupConfig.getPairOffDate());
			List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffList(searchForm);
			ValidationUtils.assertTrue((pairOffList == null) || (pairOffList.isEmpty()), "Cannot generate pair because new repo [" + repo.getId() + "] is already paired.");
		}
		// validate the list of maturing repo's
		for (LendingRepo repo : CollectionUtils.getIterable(pairOffGroupConfig.getMaturingRepoList())) {
			LendingRepoPairOffSearchForm searchForm = new LendingRepoPairOffSearchForm();
			searchForm.setCheckMaturingRepoId(repo.getId());
			searchForm.setPairOffDate(pairOffGroupConfig.getPairOffDate());
			List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffList(searchForm);
			ValidationUtils.assertTrue((pairOffList == null) || (pairOffList.isEmpty()), "Cannot generate pair because maturing repo [" + repo.getId() + "] is already paired.");
		}

		for (LendingRepo openingRepo : CollectionUtils.getIterable(pairOffGroupConfig.getOpeningRepoList())) {
			for (LendingRepo maturingRepo : CollectionUtils.getIterable(pairOffGroupConfig.getMaturingRepoList())) {
				List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffListByRepoId(maturingRepo.getId(), openingRepo.getId());
				ValidationUtils.assertTrue((pairOffList == null) || (pairOffList.isEmpty()), "Cannot generate pair of maturing repo [" + maturingRepo + "] to opening repo [" + openingRepo
						+ "] because they are already paired in the opposite direction.");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<LendingRepoPairOff, Criteria> getLendingRepoPairOffDAO() {
		return this.lendingRepoPairOffDAO;
	}


	public void setLendingRepoPairOffDAO(AdvancedUpdatableDAO<LendingRepoPairOff, Criteria> lendingRepoPairOffDAO) {
		this.lendingRepoPairOffDAO = lendingRepoPairOffDAO;
	}


	public AdvancedReadOnlyDAO<LendingRepoPairOffAccount, Criteria> getLendingRepoPairOffAccountDAO() {
		return this.lendingRepoPairOffAccountDAO;
	}


	public void setLendingRepoPairOffAccountDAO(AdvancedReadOnlyDAO<LendingRepoPairOffAccount, Criteria> lendingRepoPairOffAccountDAO) {
		this.lendingRepoPairOffAccountDAO = lendingRepoPairOffAccountDAO;
	}


	public AdvancedReadOnlyDAO<LendingRepoPairOffSecurity, Criteria> getLendingRepoPairOffSecurityDAO() {
		return this.lendingRepoPairOffSecurityDAO;
	}


	public void setLendingRepoPairOffSecurityDAO(AdvancedReadOnlyDAO<LendingRepoPairOffSecurity, Criteria> lendingRepoPairOffSecurityDAO) {
		this.lendingRepoPairOffSecurityDAO = lendingRepoPairOffSecurityDAO;
	}


	public AdvancedReadOnlyDAO<LendingRepoExtended, Criteria> getLendingRepoExtendedDAO() {
		return this.lendingRepoExtendedDAO;
	}


	public void setLendingRepoExtendedDAO(AdvancedReadOnlyDAO<LendingRepoExtended, Criteria> lendingRepoExtendedDAO) {
		this.lendingRepoExtendedDAO = lendingRepoExtendedDAO;
	}


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}


	public DaoSingleKeyListCache<LendingRepoPairOff, Integer> getLendingRepoPairOffListByOpeningRepoCache() {
		return this.lendingRepoPairOffListByOpeningRepoCache;
	}


	public void setLendingRepoPairOffListByOpeningRepoCache(DaoSingleKeyListCache<LendingRepoPairOff, Integer> lendingRepoPairOffListByOpeningRepoCache) {
		this.lendingRepoPairOffListByOpeningRepoCache = lendingRepoPairOffListByOpeningRepoCache;
	}


	public DaoSingleKeyListCache<LendingRepoPairOff, Integer> getLendingRepoPairOffListByMaturingRepoCache() {
		return this.lendingRepoPairOffListByMaturingRepoCache;
	}


	public void setLendingRepoPairOffListByMaturingRepoCache(DaoSingleKeyListCache<LendingRepoPairOff, Integer> lendingRepoPairOffListByMaturingRepoCache) {
		this.lendingRepoPairOffListByMaturingRepoCache = lendingRepoPairOffListByMaturingRepoCache;
	}
}
