package com.clifton.lending.repo.interest.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class LendingRepoInterestRateSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "repo.id")
	private Integer repoId;

	@SearchField(searchField = "repoDefinition.id", searchFieldPath = "repo", sortField = "tradeDate")
	private Integer repoDefinitionId;

	@SearchField(searchField = "type.id", searchFieldPath = "repo")
	private Integer typeId;

	@SearchField(searchField = "name", searchFieldPath = "repo.type")
	private String typeName;

	@SearchField(searchField = "multipleInterestRatesPerPeriod", searchFieldPath = "repo.type")
	private Boolean multipleInterestRatesPerPeriod;

	@SearchField(searchField = "interestRateDate", comparisonConditions = ComparisonConditions.LESS_THAN)
	private Date beforeRateDate;

	@SearchField(searchFieldPath = "repo", searchField = "settlementDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date beforeSettlementDate;

	@SearchField(searchFieldPath = "repo")
	private Date tradeDate;

	@SearchField(searchFieldPath = "repo")
	private Date settlementDate;

	@SearchField(searchFieldPath = "repo")
	private Date maturitySettlementDate;

	@SearchField(searchFieldPath = "repo")
	private Date maturityDate;

	@SearchField
	private Date interestRateDate;

	@SearchField(searchField = "parentIdentifier", searchFieldPath = "repo", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean roll;

	@SearchField(searchField = "maturityBookingDate", searchFieldPath = "repo", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean maturityBooked;

	@SearchField(searchField = "bookingDate", searchFieldPath = "repo", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean openingBooked;

	@SearchField(searchFieldPath = "repo")
	private Integer term;

	@SearchField(searchFieldPath = "repo")
	private Short cancellationNoticeTerm;

	@SearchField(searchField = "traderUser.id", searchFieldPath = "repo")
	private Short traderId;

	@SearchField(searchField = "counterparty.id", searchFieldPath = "repo.repoDefinition", sortField = "counterparty.name")
	private Integer counterpartyId;

	@SearchField(searchField = "name", searchFieldPath = "repo.repoDefinition.counterparty", sortField = "name")
	private String counterpartyName;

	@SearchField(searchField = "reverse", searchFieldPath = "repo.repoDefinition")
	private Boolean reverse;

	@SearchField(searchFieldPath = "repo.workflowState", searchField = "name")
	private String workflowStateName;

	@SearchField(searchFieldPath = "repo.workflowStatus", searchField = "name")
	private String workflowStatusName;

	@SearchField(searchFieldPath = "repo.workflowState", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeWorkflowStateName;

	@SearchField(searchFieldPath = "repo.workflowStatus", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeWorkflowStatusName;

	@SearchField(searchField = "investmentSecurity.id", searchFieldPath = "repo.repoDefinition", sortField = "investmentSecurity.symbol")
	private Integer securityId;

	@SearchField(searchField = "clientInvestmentAccount.id", searchFieldPath = "repo.repoDefinition")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.id", searchFieldPath = "repo.repoDefinition")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "repoInvestmentAccount.id", searchFieldPath = "repo.repoDefinition")
	private Integer repoInvestmentAccountId;


	public Integer getRepoDefinitionId() {
		return this.repoDefinitionId;
	}


	public void setRepoDefinitionId(Integer repoDefinitionId) {
		this.repoDefinitionId = repoDefinitionId;
	}


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Date getBeforeSettlementDate() {
		return this.beforeSettlementDate;
	}


	public void setBeforeSettlementDate(Date beforeSettlementDate) {
		this.beforeSettlementDate = beforeSettlementDate;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getMaturitySettlementDate() {
		return this.maturitySettlementDate;
	}


	public void setMaturitySettlementDate(Date maturitySettlementDate) {
		this.maturitySettlementDate = maturitySettlementDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public Integer getTerm() {
		return this.term;
	}


	public void setTerm(Integer term) {
		this.term = term;
	}


	public Short getTraderId() {
		return this.traderId;
	}


	public void setTraderId(Short traderId) {
		this.traderId = traderId;
	}


	public Integer getCounterpartyId() {
		return this.counterpartyId;
	}


	public void setCounterpartyId(Integer counterpartyId) {
		this.counterpartyId = counterpartyId;
	}


	public String getCounterpartyName() {
		return this.counterpartyName;
	}


	public void setCounterpartyName(String counterpartyName) {
		this.counterpartyName = counterpartyName;
	}


	public Boolean getReverse() {
		return this.reverse;
	}


	public void setReverse(Boolean reverse) {
		this.reverse = reverse;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	public String getWorkflowStatusName() {
		return this.workflowStatusName;
	}


	public void setWorkflowStatusName(String workflowStatusName) {
		this.workflowStatusName = workflowStatusName;
	}


	public String getExcludeWorkflowStateName() {
		return this.excludeWorkflowStateName;
	}


	public void setExcludeWorkflowStateName(String excludeWorkflowStateName) {
		this.excludeWorkflowStateName = excludeWorkflowStateName;
	}


	public String getExcludeWorkflowStatusName() {
		return this.excludeWorkflowStatusName;
	}


	public void setExcludeWorkflowStatusName(String excludeWorkflowStatusName) {
		this.excludeWorkflowStatusName = excludeWorkflowStatusName;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer getRepoInvestmentAccountId() {
		return this.repoInvestmentAccountId;
	}


	public void setRepoInvestmentAccountId(Integer repoInvestmentAccountId) {
		this.repoInvestmentAccountId = repoInvestmentAccountId;
	}


	public Date getInterestRateDate() {
		return this.interestRateDate;
	}


	public void setInterestRateDate(Date interestRateDate) {
		this.interestRateDate = interestRateDate;
	}


	public Integer getRepoId() {
		return this.repoId;
	}


	public void setRepoId(Integer repoId) {
		this.repoId = repoId;
	}


	public Boolean getMultipleInterestRatesPerPeriod() {
		return this.multipleInterestRatesPerPeriod;
	}


	public void setMultipleInterestRatesPerPeriod(Boolean multipleInterestRatesPerPeriod) {
		this.multipleInterestRatesPerPeriod = multipleInterestRatesPerPeriod;
	}


	public Date getBeforeRateDate() {
		return this.beforeRateDate;
	}


	public void setBeforeRateDate(Date beforeRateDate) {
		this.beforeRateDate = beforeRateDate;
	}


	public Boolean getRoll() {
		return this.roll;
	}


	public void setRoll(Boolean roll) {
		this.roll = roll;
	}


	public Boolean getMaturityBooked() {
		return this.maturityBooked;
	}


	public void setMaturityBooked(Boolean maturityBooked) {
		this.maturityBooked = maturityBooked;
	}


	public Boolean getOpeningBooked() {
		return this.openingBooked;
	}


	public void setOpeningBooked(Boolean openingBooked) {
		this.openingBooked = openingBooked;
	}


	public Short getCancellationNoticeTerm() {
		return this.cancellationNoticeTerm;
	}


	public void setCancellationNoticeTerm(Short cancellationNoticeTerm) {
		this.cancellationNoticeTerm = cancellationNoticeTerm;
	}
}
