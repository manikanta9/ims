package com.clifton.lending.repo;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;


@CacheByName
public class LendingRepoType extends NamedEntity<Integer> {

	/**
	 * The REPO uses multiple interest rates for the given period.
	 */
	private boolean multipleInterestRatesPerPeriod;
	/**
	 * Indicates if a maturity date is required on the opening of a REPO.
	 */
	private boolean maturityDateRequiredForOpen;
	/**
	 * Indicates that a REPO is Tri-Party.
	 */
	private boolean triPartyRepo;
	/**
	 * A cancellation term is required on the REPO.  Currently, used for Evergreen REPO's.
	 */
	private boolean cancellationNoticeTermRequired;
	/**
	 * A flag used for setting of the <code>LendingRepoPairOff#pairOffGroup</code> on certain Repo types.
	 * Currently, defaulted to true for all types except for 'Overnight' and 'Term'
	 */
	private boolean autoPairoff;
	/**
	 * Used to set the required term for a REPO type.
	 * <p/>
	 * For example, Overnight REPO have a required term of 1 (NOTE: The term can be greater than the required term,
	 * when the next day is a holiday or weekend.  An Overnight REPO starting on Friday would have a 3 day term.)
	 */
	private Integer requiredTerm;
	/**
	 * The investment account relationship purpose used to look up REPO accounts.
	 */
	private InvestmentAccountRelationshipPurpose repoInvestmentAccountRelationshipPurpose;
	/**
	 * The investment account relationship purpose used to look up Tri-Party accounts.
	 */
	private InvestmentAccountRelationshipPurpose triPartyInvestmentAccountRelationshipPurpose;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isMultipleInterestRatesPerPeriod() {
		return this.multipleInterestRatesPerPeriod;
	}


	public void setMultipleInterestRatesPerPeriod(boolean multipleInterestRatesPerPeriod) {
		this.multipleInterestRatesPerPeriod = multipleInterestRatesPerPeriod;
	}


	public boolean isMaturityDateRequiredForOpen() {
		return this.maturityDateRequiredForOpen;
	}


	public void setMaturityDateRequiredForOpen(boolean maturityDateRequiredForOpen) {
		this.maturityDateRequiredForOpen = maturityDateRequiredForOpen;
	}


	public boolean isTriPartyRepo() {
		return this.triPartyRepo;
	}


	public void setTriPartyRepo(boolean triPartyRepo) {
		this.triPartyRepo = triPartyRepo;
	}


	public boolean isAutoPairoff() {
		return this.autoPairoff;
	}


	public void setAutoPairoff(boolean autoPairoff) {
		this.autoPairoff = autoPairoff;
	}


	public InvestmentAccountRelationshipPurpose getRepoInvestmentAccountRelationshipPurpose() {
		return this.repoInvestmentAccountRelationshipPurpose;
	}


	public void setRepoInvestmentAccountRelationshipPurpose(InvestmentAccountRelationshipPurpose repoInvestmentAccountRelationshipPurpose) {
		this.repoInvestmentAccountRelationshipPurpose = repoInvestmentAccountRelationshipPurpose;
	}


	public InvestmentAccountRelationshipPurpose getTriPartyInvestmentAccountRelationshipPurpose() {
		return this.triPartyInvestmentAccountRelationshipPurpose;
	}


	public void setTriPartyInvestmentAccountRelationshipPurpose(InvestmentAccountRelationshipPurpose triPartyInvestmentAccountRelationshipPurpose) {
		this.triPartyInvestmentAccountRelationshipPurpose = triPartyInvestmentAccountRelationshipPurpose;
	}


	public Integer getRequiredTerm() {
		return this.requiredTerm;
	}


	public void setRequiredTerm(Integer requiredTerm) {
		this.requiredTerm = requiredTerm;
	}


	public boolean isCancellationNoticeTermRequired() {
		return this.cancellationNoticeTermRequired;
	}


	public void setCancellationNoticeTermRequired(boolean cancellationNoticeTermRequired) {
		this.cancellationNoticeTermRequired = cancellationNoticeTermRequired;
	}
}
