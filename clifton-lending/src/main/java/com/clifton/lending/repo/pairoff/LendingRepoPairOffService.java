package com.clifton.lending.repo.pairoff;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.lending.repo.pairoff.search.LendingRepoExtendedSearchForm;
import com.clifton.lending.repo.pairoff.search.LendingRepoPairOffAccountSearchForm;
import com.clifton.lending.repo.pairoff.search.LendingRepoPairOffSearchForm;
import com.clifton.lending.repo.pairoff.search.LendingRepoPairOffSecuritySearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


public interface LendingRepoPairOffService {

	////////////////////////////////////////////////////////////////////////////
	////////      LendingRepoPairOffAccount Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = LendingRepoPairOff.class)
	public List<LendingRepoPairOffAccount> getLendingRepoPairOffAccountList(LendingRepoPairOffAccountSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////      LendingRepoPairOffSecurity Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = LendingRepoPairOff.class)
	public List<LendingRepoPairOffSecurity> getLendingRepoPairOffSecurityList(LendingRepoPairOffSecuritySearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////      LendingRepoExtended Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = LendingRepoPairOff.class)
	public List<LendingRepoExtended> getLendingRepoExtendedList(LendingRepoExtendedSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////          LendingRepoPairOff Business Methods          /////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoPairOff getLendingRepoPairOff(int id);


	public List<LendingRepoPairOff> getLendingRepoPairOffList(LendingRepoPairOffSearchForm searchForm);


	public LendingRepoPairOff saveLendingRepoPairOff(LendingRepoPairOff bean);


	public void deleteLendingRepoPairOff(int id);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_WRITE)
	public void deleteLendingRepoPairOffByRepoId(Integer openingRepoId, Integer maturingRepoId);


	@RequestMapping("lendingRepoPairOffSplit")
	public void splitLendingRepoPairOff(String pairOffGroupId);


	@RequestMapping("lendingRepoPairOffPair")
	public void pairLendingRepoPairOff(LendingRepoPairOffGroup pairOffGroupConfig);


	@RequestMapping("lendingRepoPairOffGroupDateSave")
	@SecureMethod(dtoClass = LendingRepoPairOff.class)
	public void setLendingRepoPairOffGroupDate(String pairOffGroupId, Date pairOffDate, String note);


	@RequestMapping("lendingRepoPairOffGroupCompletedSave")
	@SecureMethod(dtoClass = LendingRepoPairOff.class)
	public void setLendingRepoPairOffGroupCompleted(String pairOffGroupId, boolean completed);


	@DoNotAddRequestMapping
	public boolean isPairOffPresent(Integer openingRepoId, Integer maturingRepoId);
}
