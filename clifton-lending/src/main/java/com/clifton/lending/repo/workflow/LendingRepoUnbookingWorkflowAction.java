package com.clifton.lending.repo.workflow;


import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.ArrayList;
import java.util.List;


public class LendingRepoUnbookingWorkflowAction implements WorkflowTransitionActionHandler<LendingRepo> {

	private AccountingJournalService accountingJournalService;
	private AccountingPostingService accountingPostingService;
	private LendingRepoService lendingRepoService;

	private boolean unbookMaturityOnly;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepo processAction(LendingRepo repo, WorkflowTransition transition) {
		boolean updateRepo = false;
		List<Long> journalList = new ArrayList<>();

		if (repo.getMaturityBookingDate() != null) {
			AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence("LendingRepo", repo.getId(), 2);
			ValidationUtils.assertNotNull(journal, "Cannot find accounting journal for trade fill with id = " + repo.getId());
			journalList.add(journal.getId());
		}
		if (repo.getBookingDate() != null && !isUnbookMaturityOnly()) {
			if (repo.getConfirmedDate() != null) {
				updateRepo = true;
				repo.setConfirmedDate(null);
			}

			AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence("LendingRepo", repo.getId(), 1);
			ValidationUtils.assertNotNull(journal, "Cannot find accounting journal for trade fill with id = " + repo.getId());
			journalList.add(journal.getId());
		}

		// unbook each booked fill separately
		for (Long journalId : journalList) {
			getAccountingPostingService().unpostAccountingJournal(journalId);
		}

		if (updateRepo) {
			return getLendingRepoService().saveLendingRepo(repo);
		}
		return repo;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}


	public boolean isUnbookMaturityOnly() {
		return this.unbookMaturityOnly;
	}


	public void setUnbookMaturityOnly(boolean unbookOnlyMaturity) {
		this.unbookMaturityOnly = unbookOnlyMaturity;
	}
}
