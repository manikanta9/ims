package com.clifton.lending.repo.instruction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instruction.selector.BaseInvestmentInstructionSelector;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.search.LendingRepoSearchForm;

import java.util.Date;
import java.util.List;


/**
 * <code>LendingRepoCounterpartyInstructionSelector</code>
 */
public class LendingRepoCounterpartyInstructionSelector extends BaseInvestmentInstructionSelector<LendingRepo> {

	private LendingRepoService lendingRepoService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<LendingRepo> getEntityList(InvestmentInstructionCategory category, InvestmentInstructionDefinition definition, Date date) {
		LendingRepoSearchForm lendingRepoSearchForm = new LendingRepoSearchForm();
		lendingRepoSearchForm.setConfirmed(true);
		lendingRepoSearchForm.setPairOffDate(date);
		lendingRepoSearchForm.setTypeTriPartyRepo(false);
		return getLendingRepoService().getLendingRepoList(lendingRepoSearchForm);
	}


	@Override
	public BusinessCompany getRecipient(LendingRepo entity) {
		return entity.getHoldingInvestmentAccount().getIssuingCompany();
	}


	@Override
	public String getInstructionItemLabel(LendingRepo entity) {
		return entity.getLabel();
	}


	@Override
	public boolean isInstructionItemBooked(InvestmentInstructionCategory category, LendingRepo entity) {
		return entity.getBookingDate() != null;
	}


	@Override
	public void populateInstructionSelectionDetails(InvestmentInstructionSelection selection, LendingRepo entity) {
		selection.setClientAccount(entity.getClientInvestmentAccount());
		selection.setHoldingAccount(entity.getHoldingInvestmentAccount());
		selection.setSecurity(entity.getInvestmentSecurity());
		selection.setBooked(isInstructionItemBooked(selection.getCategory(), entity));

		StringBuilder sb = new StringBuilder(64);
		if (entity.isBuy()) {
			sb.append("BUY ");
		}
		else {
			sb.append("SELL ");
		}
		sb.append(CoreMathUtils.formatNumberDecimal(entity.getQuantityIntended()));
		selection.setDescription(sb.toString());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}
}
