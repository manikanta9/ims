package com.clifton.lending.repo.interest;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.lending.repo.interest.search.LendingRepoInterestRateSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


public interface LendingRepoInterestRateService {

	////////////////////////////////////////////////////////////////////////////
	////////       LendingRepoInterestRate Business Methods        /////////////
	////////////////////////////////////////////////////////////////////////////
	public LendingRepoInterestRate getLendingRepoInterestRate(int id);


	public LendingRepoInterestRate getLendingRepoInterestRateByRepoAndDate(int repoId, Date interestRateDate);


	public List<LendingRepoInterestRate> getLendingRepoInterestRateByRepo(int repoId);


	public List<LendingRepoInterestRate> getLendingRepoInterestRateList(LendingRepoInterestRateSearchForm searchForm);


	public void deleteLendingRepoInterestRate(int id);


	public void deleteLendingRepoInterestRateList(List<LendingRepoInterestRate> rateList);


	public LendingRepoInterestRate saveLendingRepoInterestRate(LendingRepoInterestRate bean);


	@RequestMapping("lendingRepoInterestRateBuild")
	public void buildLendingRepoInterestRate(LendingRepoInterestRateDate rateCounterPartyList);


	@SecureMethod(dtoClass = LendingRepoInterestRate.class)
	public LendingRepoInterestRateDate getLendingRepoInterestRateDateForDate(Date rateDate);
}
