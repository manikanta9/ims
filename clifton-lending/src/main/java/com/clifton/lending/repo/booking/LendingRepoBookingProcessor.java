package com.clifton.lending.repo.booking;


import com.clifton.accounting.gl.booking.processor.TypeBasedRulesBookingProcessor;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.LendingRepoServiceImpl;
import com.clifton.lending.repo.search.LendingRepoSearchForm;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The <code>RepoDetailBookingProcessor</code> class is AccountingBookingProcessor for LendingRepo objects.
 * <p/>
 * Booking of repo must be done at Detail level in order to support LIFO transfers of positions to the REPO accounts.
 *
 * @author mwacker
 */
public class LendingRepoBookingProcessor extends TypeBasedRulesBookingProcessor<LendingRepo> {

	private LendingRepoService lendingRepoService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJournalTypeName() {
		return AccountingJournalType.REPO_JOURNAL;
	}


	@Override
	public LendingRepo getBookableEntity(Serializable id) {
		return getLendingRepoService().getLendingRepo((Integer) id);
	}


	@Override
	@ValueIgnoringGetter
	public String getTypeProperty() {
		return "repo.type.name";
	}


	@Override
	public List<LendingRepo> getUnbookedEntityList() {
		LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
		searchForm.setExcludeWorkflowStatusName(LendingRepoService.REPO_CLOSED_WORKFLOW_STATUS_NAME);
		return getLendingRepoService().getLendingRepoList(searchForm);
	}


	@Override
	protected String getBookingEntityType(LendingRepo repo) {
		ValidationUtils.assertFalse(repo.getType().isTriPartyRepo() && !repo.getRepoDefinition().isReverse(), "Non-Reverse Tri-Party REPO's are not supported at this time.");

		String result = repo.getType().getName() + (repo.getBookingDate() != null ? "-Maturity" : "-Opening");
		return result + (repo.getRepoDefinition().isReverse() ? "-Reverse" : "");
	}


	@Override
	public BookingSession<LendingRepo> newBookingSession(AccountingJournal journal, LendingRepo repo) {
		if (repo == null) {
			repo = getBookableEntity(journal.getFkFieldId());
		}

		if (StringUtils.isEmpty(repo.getDescription())) {
			StringBuilder description = new StringBuilder();
			description.append(repo.getRepoDefinition().isReverse() ? "Borrow " : "Lend ");
			description.append(repo.getNetCash());
			description.append(" for ");
			description.append(repo.getRepoDefinition().getInvestmentSecurity().getSymbol());
			description.append(" valued at ");
			description.append(repo.getMarketValue());

			journal.setDescription(description.toString());
		}
		return new SimpleBookingSession<>(journal, repo);
	}


	@Override
	public LendingRepo markBooked(LendingRepo repoDetail) {
		if (repoDetail.getBookingDate() == null) {
			repoDetail.setBookingDate(new Date());
		}
		else {
			ValidationUtils.assertNull(repoDetail.getMaturityBookingDate(), "Cannot book REPO maturity with id = " + repoDetail.getId() + " because it already has maturityBookingDate set to "
					+ repoDetail.getMaturityBookingDate());
			repoDetail.setMaturityBookingDate(new Date());
		}
		if (TransactionSynchronizationManager.isActualTransactionActive()) {
			return repoDetail;
		}
		return ((LendingRepoServiceImpl) getLendingRepoService()).getLendingRepoDAO().save(repoDetail);
	}


	@Override
	public LendingRepo markUnbooked(int sourceEntityId) {
		LendingRepo repo = getBookableEntity(sourceEntityId);

		ValidationUtils.assertNotNull(repo, "Cannot find repo with id = " + sourceEntityId);
		ValidationUtils.assertNotNull(repo.getBookingDate(), "Cannot unbook a repo that isn't currently booked.");

		// clear booking date bypassing fill save validation
		if (repo.getMaturityBookingDate() != null) {
			repo.setMaturityBookingDate(null);
		}
		else {
			repo.setBookingDate(null);
		}
		return ((LendingRepoServiceImpl) getLendingRepoService()).getLendingRepoDAO().save(repo);
	}


	@Override
	public void deleteSourceEntity(int sourceEntityId) {
		throw new ValidationException("Cannot delete an existing REPO: " + sourceEntityId);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}
}
