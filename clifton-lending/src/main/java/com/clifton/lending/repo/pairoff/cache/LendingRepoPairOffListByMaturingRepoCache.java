package com.clifton.lending.repo.pairoff.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.lending.repo.pairoff.LendingRepoPairOff;
import org.springframework.stereotype.Component;


@Component
public class LendingRepoPairOffListByMaturingRepoCache extends SelfRegisteringSingleKeyDaoListCache<LendingRepoPairOff, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "maturingRepo.id";
	}


	@Override
	protected Integer getBeanKeyValue(LendingRepoPairOff bean) {
		return BeanUtils.getBeanIdentity(bean.getMaturingRepo());
	}
}
