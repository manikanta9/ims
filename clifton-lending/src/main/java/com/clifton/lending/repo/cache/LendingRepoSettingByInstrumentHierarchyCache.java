package com.clifton.lending.repo.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.lending.repo.LendingRepoSetting;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class LendingRepoSettingByInstrumentHierarchyCache extends SelfRegisteringSingleKeyDaoCache<LendingRepoSetting, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "instrumentHierarchy.id";
	}


	@Override
	protected Short getBeanKeyValue(LendingRepoSetting bean) {
		if (bean.getInstrumentHierarchy() != null) {
			return bean.getInstrumentHierarchy().getId();
		}
		return null;
	}
}
