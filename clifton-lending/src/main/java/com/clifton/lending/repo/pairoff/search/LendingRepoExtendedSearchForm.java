package com.clifton.lending.repo.pairoff.search;


import com.clifton.lending.repo.search.LendingRepoSearchForm;


public class LendingRepoExtendedSearchForm extends LendingRepoSearchForm {

	// Custom search field
	private Boolean rolled;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getRolled() {
		return this.rolled;
	}


	public void setRolled(Boolean rolled) {
		this.rolled = rolled;
	}
}
