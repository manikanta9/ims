package com.clifton.lending.repo.pairoff.workflow;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.pairoff.LendingRepoPairOffGroup;
import com.clifton.lending.repo.pairoff.LendingRepoPairOffService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.Date;


/**
 * The <code>LendingRepoBookingWorkflowAction</code> ...
 *
 * @author mwacker
 */
public class LendingRepoPairOffWorkflowAction implements WorkflowTransitionActionHandler<LendingRepo> {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private LendingRepoService lendingRepoService;
	private LendingRepoPairOffService lendingRepoPairOffService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepo processAction(LendingRepo repo, WorkflowTransition transition) {
		// book the repo
		if ((repo.getBookingDate() == null) || (repo.getMaturityBookingDate() == null)) {
			// create the pairoff's if they don't exist
			createPairOff(repo);
		}
		return repo;
	}


	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////
	private void createPairOff(LendingRepo repo) {
		LendingRepoPairOffGroup pairoffConfig = new LendingRepoPairOffGroup();
		if (repo.isRoll() && (repo.getBookingDate() == null)) {
			LendingRepo parentRepo = getLendingRepoService().getLendingRepo(repo.getParentIdentifier());
			ValidationUtils.assertNotNull(parentRepo, "Cannot find a parent LendingRepo with ID: " + repo.getParentIdentifier());

			// only create is the roll pair off if it does not exist, the pairoff is today or in the future or if no pair exists for either side
			if (!getLendingRepoPairOffService().isPairOffPresent(repo.getId(), parentRepo.getId())
					&& ((DateUtils.compare(new Date(), repo.getTradeDate(), false) <= 0) || (!getLendingRepoPairOffService().isPairOffPresent(repo.getId(), null)
					&& !getLendingRepoPairOffService().isPairOffPresent(null, parentRepo.getId()) && (DateUtils.compare(new Date(), repo.getTradeDate(), false) > 0)))) {

				pairoffConfig.getMaturingRepoList().add(parentRepo);
				pairoffConfig.getOpeningRepoList().add(repo);
				pairoffConfig.setPairOffDate(repo.getTradeDate());
			}
			else {
				// add the single pairoff's
				if (!getLendingRepoPairOffService().isPairOffPresent(repo.getId(), null)) {
					pairoffConfig.getOpeningRepoList().add(repo);
					pairoffConfig.setPairOffDate(repo.getTradeDate());
				}
				if (!getLendingRepoPairOffService().isPairOffPresent(null, parentRepo.getId())) {
					pairoffConfig.getMaturingRepoList().add(parentRepo);
					pairoffConfig.setPairOffDate(repo.getTradeDate());
				}
			}
		}
		else {
			if ((repo.getBookingDate() == null) && !getLendingRepoPairOffService().isPairOffPresent(repo.getId(), null)) {
				pairoffConfig.getOpeningRepoList().add(repo);
				pairoffConfig.setPairOffDate(repo.getTradeDate());
			}
			else if ((repo.getBookingDate() != null) && !getLendingRepoPairOffService().isPairOffPresent(null, repo.getId())) {
				pairoffConfig.getMaturingRepoList().add(repo);
				pairoffConfig.setPairOffDate(repo.getMaturityDate());
			}
		}
		if ((pairoffConfig.getPairOffDate() != null) && ((!pairoffConfig.getMaturingRepoList().isEmpty()) || (!pairoffConfig.getOpeningRepoList().isEmpty()))) {
			getLendingRepoPairOffService().pairLendingRepoPairOff(pairoffConfig);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}


	public LendingRepoPairOffService getLendingRepoPairOffService() {
		return this.lendingRepoPairOffService;
	}


	public void setLendingRepoPairOffService(LendingRepoPairOffService lendingRepoPairOffService) {
		this.lendingRepoPairOffService = lendingRepoPairOffService;
	}
}
