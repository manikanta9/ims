package com.clifton.lending.repo.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class LendingRepoTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private Boolean multipleInterestRatesPerPeriod;

	@SearchField
	private Boolean maturityDateRequiredForOpen;

	@SearchField
	private Boolean triPartyRepo;

	@SearchField
	private Boolean cancellationNoticeTermRequired;

	@SearchField
	private Integer requiredTerm;

	@SearchField(searchField = "name", searchFieldPath = "repoInvestmentAccountRelationshipPurpose")
	private String repoInvestmentAccountRelationshipPurposeName;

	@SearchField(searchField = "name", searchFieldPath = "triPartyInvestmentAccountRelationshipPurpose")
	private String triPartyInvestmentAccountRelationshipPurposeName;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Boolean getMultipleInterestRatesPerPeriod() {
		return this.multipleInterestRatesPerPeriod;
	}


	public void setMultipleInterestRatesPerPeriod(Boolean multipleInterestRatesPerPeriod) {
		this.multipleInterestRatesPerPeriod = multipleInterestRatesPerPeriod;
	}


	public Boolean getMaturityDateRequiredForOpen() {
		return this.maturityDateRequiredForOpen;
	}


	public void setMaturityDateRequiredForOpen(Boolean maturityDateRequiredForOpen) {
		this.maturityDateRequiredForOpen = maturityDateRequiredForOpen;
	}


	public Boolean getTriPartyRepo() {
		return this.triPartyRepo;
	}


	public void setTriPartyRepo(Boolean triPartyRepo) {
		this.triPartyRepo = triPartyRepo;
	}


	public Boolean getCancellationNoticeTermRequired() {
		return this.cancellationNoticeTermRequired;
	}


	public void setCancellationNoticeTermRequired(Boolean cancellationNoticeTermRequired) {
		this.cancellationNoticeTermRequired = cancellationNoticeTermRequired;
	}


	public Integer getRequiredTerm() {
		return this.requiredTerm;
	}


	public void setRequiredTerm(Integer requiredTerm) {
		this.requiredTerm = requiredTerm;
	}


	public String getRepoInvestmentAccountRelationshipPurposeName() {
		return this.repoInvestmentAccountRelationshipPurposeName;
	}


	public void setRepoInvestmentAccountRelationshipPurposeName(String repoInvestmentAccountRelationshipPurposeName) {
		this.repoInvestmentAccountRelationshipPurposeName = repoInvestmentAccountRelationshipPurposeName;
	}


	public String getTriPartyInvestmentAccountRelationshipPurposeName() {
		return this.triPartyInvestmentAccountRelationshipPurposeName;
	}


	public void setTriPartyInvestmentAccountRelationshipPurposeName(String triPartyInvestmentAccountRelationshipPurposeName) {
		this.triPartyInvestmentAccountRelationshipPurposeName = triPartyInvestmentAccountRelationshipPurposeName;
	}
}
