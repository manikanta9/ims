package com.clifton.lending.repo.pairoff.workflow;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.pairoff.LendingRepoPairOff;
import com.clifton.lending.repo.pairoff.LendingRepoPairOffService;
import com.clifton.lending.repo.pairoff.search.LendingRepoPairOffSearchForm;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.Date;
import java.util.List;


public class LendingRepoRemovePairOffWorkflowAction implements WorkflowTransitionActionHandler<LendingRepo> {

	private boolean maturityOnly;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private LendingRepoPairOffService lendingRepoPairOffService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepo processAction(LendingRepo repo, WorkflowTransition transition) {
		if (repo.getMaturityBookingDate() != null) {
			LendingRepoPairOffSearchForm searchForm = new LendingRepoPairOffSearchForm();
			searchForm.setMaturingRepoId(repo.getId());
			searchForm.setCompleted(true);
			List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffService().getLendingRepoPairOffList(searchForm);

			// if the maturity date is on or after the current date or no completed pair offs exists, then remove the pairoff's
			if ((DateUtils.compare(new Date(), repo.getMaturityDate(), false) <= 0) || CollectionUtils.isEmpty(pairOffList)) {
				getLendingRepoPairOffService().deleteLendingRepoPairOffByRepoId(null, repo.getId());
			}
		}
		if (repo.getBookingDate() != null && !isMaturityOnly()) {
			LendingRepoPairOffSearchForm searchForm = new LendingRepoPairOffSearchForm();
			searchForm.setOpeningRepoId(repo.getId());
			searchForm.setCompleted(true);
			List<LendingRepoPairOff> pairOffList = getLendingRepoPairOffService().getLendingRepoPairOffList(searchForm);

			// if the trade date is on or after the current date or no completed pair offs exists, then remove the pairoff's
			if ((DateUtils.compare(new Date(), repo.getTradeDate(), false) <= 0) || CollectionUtils.isEmpty(pairOffList)) {
				getLendingRepoPairOffService().deleteLendingRepoPairOffByRepoId(repo.getId(), null);
			}
		}
		return repo;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isMaturityOnly() {
		return this.maturityOnly;
	}


	public void setMaturityOnly(boolean maturityOnly) {
		this.maturityOnly = maturityOnly;
	}


	public LendingRepoPairOffService getLendingRepoPairOffService() {
		return this.lendingRepoPairOffService;
	}


	public void setLendingRepoPairOffService(LendingRepoPairOffService lendingRepoPairOffService) {
		this.lendingRepoPairOffService = lendingRepoPairOffService;
	}
}
