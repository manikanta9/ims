package com.clifton.lending.repo;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.CoreValidationUtils;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The {@link LendingRepoValidator} performs {@link LendingRepo} entity validation.
 *
 * @author MikeH
 */
@Component
public class LendingRepoValidator extends SelfRegisteringDaoValidator<LendingRepo> {

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(LendingRepo repo, DaoEventTypes config) throws ValidationException {
		if (repo.getType().getRequiredTerm() != null) {
			Calendar calendar = getCalendarSetupService().getCalendarByName(LendingRepoService.LENDING_REPO_CALENDAR_NAME);
			Date expectedMaturitySettlementDate = getCalendarBusinessDayService().getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(repo.getSettlementDate(), calendar.getId()), repo.getType().getRequiredTerm());
			ValidationUtils.assertEquals(expectedMaturitySettlementDate, repo.getMaturitySettlementDate(), String.format("The maturity date for repos of type [%s] must be the business day on or immediately following the required term length of [%d] days ([%s]).", repo.getType().getName(), repo.getType().getRequiredTerm(), DateUtils.fromDateShort(expectedMaturitySettlementDate)), "maturitySettlementDate");
		}
		if (repo.getType().isCancellationNoticeTermRequired()) {
			ValidationUtils.assertNotNull(repo.getCancellationNoticeTerm(), "Cancellation Notice Term is required for REPO's of type [" + repo.getType().getName() + "].", "cancellationNoticeTerm");
		}
		if (repo.getType().isMaturityDateRequiredForOpen()) {
			ValidationUtils.assertNotNull(repo.getMaturitySettlementDate(), "Maturity date is required for REPO's of type [" + repo.getType().getName() + "].", "maturityDate");
		}

		// Validate trade, settlement, maturity, and maturity settlement date order
		ValidationUtils.assertTrue(repo.getTradeDate() == null || repo.getSettlementDate() == null
				|| DateUtils.isDateBeforeOrEqual(repo.getTradeDate(), repo.getSettlementDate(), false), "The settlement date must be after or equal to the trade date.", "settlementDate");
		ValidationUtils.assertTrue(repo.getTradeDate() == null || repo.getMaturityDate() == null
				|| DateUtils.isDateBeforeOrEqual(repo.getTradeDate(), repo.getMaturityDate(), false), "The maturity date must be after or equal to the trade date.", "maturityDate");
		ValidationUtils.assertTrue(repo.getMaturityDate() == null || repo.getMaturitySettlementDate() == null
				|| DateUtils.isDateBeforeOrEqual(repo.getMaturityDate(), repo.getMaturitySettlementDate(), false), "The maturity settlement date must be after or equal to the maturity date.", "maturitySettlementDate");

		// Validate term and settlement durations
		ValidationUtils.assertNotNull(repo.getTerm(), "A term must be provided.");
		if (repo.getType().isMultipleInterestRatesPerPeriod()) {
			ValidationUtils.assertEquals(1, repo.getTerm(), "The term for repos with iteratively-updated interest rates must be 1.");
		}
		else if (repo.getMaturitySettlementDate() != null) {
			int calculatedTerm = DateUtils.getDaysDifference(repo.getMaturitySettlementDate(), repo.getSettlementDate());
			ValidationUtils.assertEquals(repo.getTerm(), calculatedTerm, String.format("The provided repo term length ([%d] days) must be equal to the number of days between the initial settlement and the maturity settlement ([%d] days).", repo.getTerm(), calculatedTerm));
		}
		ValidationUtils.assertTrue(repo.getTerm() > 0, "The term must be greater than zero."); // Most generic validation message last

		if (repo.getType().isTriPartyRepo()) {
			ValidationUtils.assertNotNull(repo.getRepoDefinition().getTriPartyInvestmentAccount(), "Tri-Party account is required for REPO type [" + repo.getType().getName() + "].", "triPartyInvestmentAccount");
			ValidationUtils.assertTrue(repo.getRepoDefinition().isReverse(), "Non-Reverse Tri-Party REPO's are not supported at this time.");
		}
		else {
			ValidationUtils.assertNull(repo.getRepoDefinition().getTriPartyInvestmentAccount(), "Tri-Party account CANNOT be set for REPO type [" + repo.getType().getName() + "].", "triPartyInvestmentAccount");
		}

		if (!repo.isNewBean()) {
			LendingRepo existingRepo = getOriginalBean(repo);
			CoreValidationUtils.assertDisallowedModifiedFields(repo, existingRepo, "repoDefinition");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
