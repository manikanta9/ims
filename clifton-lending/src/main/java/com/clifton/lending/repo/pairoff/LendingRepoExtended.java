package com.clifton.lending.repo.pairoff;


import com.clifton.lending.repo.LendingRepo;


public class LendingRepoExtended extends LendingRepo {

	private boolean openingPaired;
	private boolean maturingPaired;
	private boolean openingRolled;
	private boolean maturingRolled;

	private String openingPairOffGroup;
	private String maturingPairOffGroup;

	private boolean openingCompleted;
	private boolean maturingCompleted;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isOpeningPaired() {
		return this.openingPaired;
	}


	public void setOpeningPaired(boolean openingPaired) {
		this.openingPaired = openingPaired;
	}


	public boolean isMaturingPaired() {
		return this.maturingPaired;
	}


	public void setMaturingPaired(boolean maturingPaired) {
		this.maturingPaired = maturingPaired;
	}


	public boolean isOpeningRolled() {
		return this.openingRolled;
	}


	public void setOpeningRolled(boolean openingRolled) {
		this.openingRolled = openingRolled;
	}


	public boolean isMaturingRolled() {
		return this.maturingRolled;
	}


	public void setMaturingRolled(boolean maturingRolled) {
		this.maturingRolled = maturingRolled;
	}


	public String getOpeningPairOffGroup() {
		return this.openingPairOffGroup;
	}


	public void setOpeningPairOffGroup(String openingPairOffGroup) {
		this.openingPairOffGroup = openingPairOffGroup;
	}


	public String getMaturingPairOffGroup() {
		return this.maturingPairOffGroup;
	}


	public void setMaturingPairOffGroup(String maturingPairOffGroup) {
		this.maturingPairOffGroup = maturingPairOffGroup;
	}


	public boolean isOpeningCompleted() {
		return this.openingCompleted;
	}


	public void setOpeningCompleted(boolean openingCompleted) {
		this.openingCompleted = openingCompleted;
	}


	public boolean isMaturingCompleted() {
		return this.maturingCompleted;
	}


	public void setMaturingCompleted(boolean maturingCompleted) {
		this.maturingCompleted = maturingCompleted;
	}
}
