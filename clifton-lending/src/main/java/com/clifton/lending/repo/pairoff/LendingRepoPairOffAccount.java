package com.clifton.lending.repo.pairoff;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;


public class LendingRepoPairOffAccount extends BaseSimpleEntity<Integer> implements LabeledObject {

	private String uuid;
	private Date pairOffDate;
	private BusinessCompany counterparty;
	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	private InvestmentAccount repoInvestmentAccount;

	private int maturingRepoCount;
	private int openingRepoCount;

	private boolean completed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getClientInvestmentAccount().getLabel() + " for " + DateUtils.fromDate(getPairOffDate(), DateUtils.DATE_FORMAT_INPUT);
	}


	public BusinessCompany getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(BusinessCompany counterparty) {
		this.counterparty = counterparty;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public int getMaturingRepoCount() {
		return this.maturingRepoCount;
	}


	public void setMaturingRepoCount(int maturingRepoCount) {
		this.maturingRepoCount = maturingRepoCount;
	}


	public int getOpeningRepoCount() {
		return this.openingRepoCount;
	}


	public void setOpeningRepoCount(int openingRepoCount) {
		this.openingRepoCount = openingRepoCount;
	}


	public Date getPairOffDate() {
		return this.pairOffDate;
	}


	public void setPairOffDate(Date pairOffDate) {
		this.pairOffDate = pairOffDate;
	}


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public InvestmentAccount getRepoInvestmentAccount() {
		return this.repoInvestmentAccount;
	}


	public void setRepoInvestmentAccount(InvestmentAccount repoInvestmentAccount) {
		this.repoInvestmentAccount = repoInvestmentAccount;
	}


	public boolean isCompleted() {
		return this.completed;
	}


	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
}
