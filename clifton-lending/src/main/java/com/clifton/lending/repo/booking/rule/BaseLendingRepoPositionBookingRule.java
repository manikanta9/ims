package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCommonBookingRule;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseLendingRepoPositionBookingRule</code> defines a base booking rule for REPO's.
 * <p/>
 * Lending REPO rules that implement this class can be made reversible for the Opening and Maturity bookings
 * by using the repoOpening property to determine if it's the opening or maturity instance of the run
 * and getHoldingAccount method to get the correct holding account for the transactions.
 *
 * @author mwacker
 */
public abstract class BaseLendingRepoPositionBookingRule extends AccountingCommonBookingRule<LendingRepo> implements AccountingBookingRule<LendingRepo> {

	private static final String LENDING_REPO_TABLE_NAME = "LendingRepo";

	private AccountingJournalService accountingJournalService;
	private SystemSchemaService systemSchemaService;

	private SystemTable lendingRepoTable;

	/**
	 * True when the rule is for the initial booking of the REPO and false when the rule is being run for Maturity.
	 */
	private boolean repoOpening;


	protected SystemTable getLendingRepoTable() {
		if (this.lendingRepoTable == null) {
			this.lendingRepoTable = getSystemSchemaService().getSystemTableByName(LENDING_REPO_TABLE_NAME);
			AssertUtils.assertNotNull(this.lendingRepoTable, "Cannot find system table with name: LendingRepo");
		}
		return this.lendingRepoTable;
	}


	/**
	 * This method will return the correct holding account based on the if the rule is for REPO opening or maturity, the rule is building the closing transaction set
	 * and if the REPO is a reverse REPO.
	 * <p/>
	 * For example, when running the Position Close rule for an opening a non reverse repo, the position is being close in out of the holding account so the
	 * result is repo.getHoldingInvestmentAccount().  When the Position Close rule is run for a maturity, the position is being close out of repo account so the
	 * result is repo.getRepoDefinition().getRepoInvestmentAccount().
	 * <p/>
	 * NOTE: Must call repo.getHoldingInvestmentAccount() because that will return the correct account for Tri-Party REPO's.
	 */
	protected InvestmentAccount getHoldingAccount(LendingRepo repo, boolean closingRule) {
		InvestmentAccount holdingInvestmentAccount;
		if ((closingRule && isRepoOpening()) || (!closingRule && !isRepoOpening())) {
			holdingInvestmentAccount = repo.getRepoDefinition().isReverse() ? repo.getRepoDefinition().getRepoInvestmentAccount() : repo.getHoldingInvestmentAccount();
		}
		else {
			holdingInvestmentAccount = !repo.getRepoDefinition().isReverse() ? repo.getRepoDefinition().getRepoInvestmentAccount() : repo.getHoldingInvestmentAccount();
		}
		return holdingInvestmentAccount;
	}


	/**
	 * This method does the same thing as getHoldingAccount but replaces the RepoInvestmentAccount with TriPartyInvestmentAccount.
	 */
	protected InvestmentAccount getHoldingAccountForTriParty(LendingRepo repo, boolean closingRule) {
		InvestmentAccount holdingInvestmentAccount;
		if ((closingRule && isRepoOpening()) || (!closingRule && !isRepoOpening())) {
			holdingInvestmentAccount = repo.getRepoDefinition().isReverse() ? repo.getRepoDefinition().getTriPartyInvestmentAccount() : repo.getRepoDefinition().getHoldingInvestmentAccount();
		}
		else {
			holdingInvestmentAccount = !repo.getRepoDefinition().isReverse() ? repo.getRepoDefinition().getTriPartyInvestmentAccount() : repo.getRepoDefinition().getHoldingInvestmentAccount();
		}
		return holdingInvestmentAccount;
	}


	protected List<AccountingJournalDetail> createBondCloseRecordList(BookingSession<LendingRepo> bookingSession, LendingRepo repo, InvestmentAccount holdingInvestmentAccount, String descriptionPrefix) {
		List<AccountingTransaction> transactionList = getOpeningTransactionsToBeClosedForRepoMaturity(repo, holdingInvestmentAccount);
		List<AccountingJournalDetail> result = new ArrayList<>();
		for (AccountingTransaction positionBeingClosed : CollectionUtils.getIterable(transactionList)) {
			AccountingJournalDetail positionEntry = new AccountingJournalDetail();
			BeanUtils.copyProperties(positionBeingClosed, positionEntry, new String[]{"parentTransaction", "parentDefinition"});

			positionEntry.setParentTransaction(positionBeingClosed);
			positionEntry.setPrice(positionEntry.getParentTransaction().getPrice());
			positionEntry.setExchangeRateToBase(positionEntry.getParentTransaction().getExchangeRateToBase());
			positionEntry.setOriginalTransactionDate(positionEntry.getParentTransaction().getOriginalTransactionDate());

			positionEntry.setQuantity(positionBeingClosed.getQuantity().negate());
			positionEntry.setTransactionDate(repo.getMaturityDate());
			positionEntry.setSettlementDate(repo.getMaturitySettlementDate());

			positionEntry.setLocalDebitCredit(positionBeingClosed.getLocalDebitCredit().negate());
			positionEntry.setBaseDebitCredit(positionBeingClosed.getBaseDebitCredit().negate());
			positionEntry.setPositionCostBasis(positionBeingClosed.getPositionCostBasis().negate());

			positionEntry.setOpening(false);
			positionEntry.setHoldingInvestmentAccount(holdingInvestmentAccount);

			positionEntry.setDescription(descriptionPrefix + "Position close from " + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO " + (isRepoOpening() ? "opening" : "maturity")
					+ " for " + repo.getInvestmentSecurity().getSymbol());

			populateSessionWithClosingPosition(bookingSession, positionEntry, AccountingPosition.forOpeningTransaction(positionBeingClosed));
			result.add(positionEntry);
		}
		return result;
	}


	protected List<AccountingTransaction> getOpeningTransactionsToBeClosedForRepoMaturity(LendingRepo repo, InvestmentAccount holdingInvestmentAccount) {
		// get the journal for the REPO opening
		AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence(getLendingRepoTable().getName(), repo.getId(), 1);
		ValidationUtils.assertNotNull(journal, "The REPO opening must be booked before the maturity.");
		// get the opening transactions
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setJournalId(journal.getId());
		searchForm.setHoldingInvestmentAccountId(holdingInvestmentAccount.getId());
		searchForm.setOpening(true);
		searchForm.setPositionAccountingAccount(true);
		return getAccountingTransactionService().getAccountingTransactionList(searchForm);
	}


	protected void populateSessionWithClosingPosition(BookingSession<LendingRepo> bookingSession, AccountingJournalDetail closingDetail, AccountingPosition positionBeingClosed) {
		Map<String, List<BookingPosition>> positionsByClient = bookingSession.getCurrentPositions();
		String key = bookingSession.getPositionKey(closingDetail);
		List<BookingPosition> currentOpenPositions = positionsByClient.get(key);
		if (currentOpenPositions == null) {
			currentOpenPositions = new ArrayList<>();
		}
		BookingPosition bookingPosition = new BookingPosition(positionBeingClosed);
		currentOpenPositions.add(bookingPosition);
		bookingSession.getCurrentPositions().put(key, currentOpenPositions);

		// update remaining quantity to close to avoid double closing
		bookingPosition.setRemainingQuantity(bookingPosition.getRemainingQuantity().add(closingDetail.getQuantity()));
	}


	protected AccountingJournalDetail createCashRecord(LendingRepo repo, AccountingJournalDetail positionEntry, boolean positionClosing, boolean interest, BigDecimal amount) {
		InvestmentSecurity tradingCurrency = positionEntry.getInvestmentSecurity().getInstrument().getTradingCurrency();
		InvestmentSecurity clientBaseCurrency = InvestmentUtils.getClientAccountBaseCurrency(positionEntry);

		AccountingJournalDetail cashCurrencyEntry = new AccountingJournalDetail();
		BeanUtils.copyProperties(positionEntry, cashCurrencyEntry, new String[]{"parentTransaction", "parentDefinition"});

		cashCurrencyEntry.setParentTransaction(null);
		cashCurrencyEntry.setParentDefinition(positionEntry);

		cashCurrencyEntry.setQuantity(null);
		cashCurrencyEntry.setPrice(null);
		cashCurrencyEntry.setOriginalTransactionDate(positionEntry.getTransactionDate());

		if (isRepoOpening()) {
			// when opening use the FX rate from the REPO which is set as of the trade date.
			cashCurrencyEntry.setExchangeRateToBase(repo.getExchangeRateToBase());
		}
		else {
			// when maturing use the FX rate on the REPO maturity date
			BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(repo.fxSourceCompany(), !repo.fxSourceCompanyOverridden(),
					repo.getRepoDefinition().getInvestmentSecurity().getInstrument().getTradingCurrency().getSymbol(),
					InvestmentUtils.getClientAccountBaseCurrencySymbol(repo.getRepoDefinition()), repo.getMaturityDate()).flexibleLookup());
			cashCurrencyEntry.setExchangeRateToBase(fxRate);
		}

		// go the opposite direction of the position
		cashCurrencyEntry.setLocalDebitCredit(positionClosing ? amount : amount.negate());
		cashCurrencyEntry.setBaseDebitCredit(MathUtils.multiply(cashCurrencyEntry.getLocalDebitCredit(), cashCurrencyEntry.getExchangeRateToBase(), 2));
		cashCurrencyEntry.setPositionCommission(BigDecimal.ZERO);

		String interestAccountName = repo.getRepoDefinition().isReverse() ? AccountingAccount.REPO_REVENUE_INTEREST_INCOME : AccountingAccount.REPO_EXPENSE_INTEREST_EXPENSE;
		if (clientBaseCurrency.equals(tradingCurrency)) {
			cashCurrencyEntry.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CASH));
			cashCurrencyEntry.setInvestmentSecurity(clientBaseCurrency);
			cashCurrencyEntry.setPositionCostBasis(BigDecimal.ZERO);
			String description;
			if (interest) {
				description = interestAccountName + " from " + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO " + (isRepoOpening() ? "opening" : "maturity") + " for "
						+ repo.getInvestmentSecurity().getSymbol();
			}
			else {
				description = AccountingAccount.ASSET_CASH + (positionClosing ? PROCEEDS_DESCRIPTION_SEGMENT : EXPENSE_DESCRIPTION_SEGMENT) + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO "
						+ (isRepoOpening() ? "opening" : "maturity") + " for " + repo.getInvestmentSecurity().getSymbol();
			}
			cashCurrencyEntry.setDescription(description);
		}
		else {
			cashCurrencyEntry.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
			cashCurrencyEntry.setInvestmentSecurity(tradingCurrency);
			cashCurrencyEntry.setPositionCostBasis(BigDecimal.ZERO);
			String description;
			if (interest) {
				description = interestAccountName + " from " + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO " + (isRepoOpening() ? "opening" : "maturity") + " for "
						+ repo.getInvestmentSecurity().getSymbol();
			}
			else {
				description = AccountingAccount.ASSET_CURRENCY + (positionClosing ? PROCEEDS_DESCRIPTION_SEGMENT : EXPENSE_DESCRIPTION_SEGMENT) + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO "
						+ (isRepoOpening() ? "opening" : "maturity") + " for " + repo.getInvestmentSecurity().getSymbol();
			}
			cashCurrencyEntry.setDescription(description);
		}

		cashCurrencyEntry.setOpening(isRepoOpening());
		return cashCurrencyEntry;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isRepoOpening() {
		return this.repoOpening;
	}


	public void setRepoOpening(boolean repoOpening) {
		this.repoOpening = repoOpening;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}
}
