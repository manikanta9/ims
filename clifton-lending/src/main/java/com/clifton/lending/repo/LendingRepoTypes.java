package com.clifton.lending.repo;

public enum LendingRepoTypes {

	OVERNIGHT("Overnight"),
	TERM("Term"),
	OPEN("Open"),
	OPEN_TRI_PARTY("Open Tri-Party"),
	TERM_TRI_PARTY("Term Tri-Party"),
	OVERNIGHT_TRI_PARTY("Overnight Tri-Party"),
	EVERGREEN_OPEN("Evergreen (Open)");

	private final String typeName;


	LendingRepoTypes(String typeName) {
		this.typeName = typeName;
	}


	public String getTypeName() {
		return this.typeName;
	}
}
