package com.clifton.lending.repo.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class LendingRepoSettingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "instrumentHierarchy.id")
	private Short instrumentHierarchyId;

	@SearchField(searchField = "name", searchFieldPath = "instrumentHierarchy")
	private String hierarchyName;

	@SearchField
	private Integer daysToSettle;

	@SearchField
	private String dayCountConvention;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	public String getDayCountConvention() {
		return this.dayCountConvention;
	}


	public void setDayCountConvention(String dayCountConvention) {
		this.dayCountConvention = dayCountConvention;
	}


	public String getHierarchyName() {
		return this.hierarchyName;
	}


	public void setHierarchyName(String hierarchyName) {
		this.hierarchyName = hierarchyName;
	}
}
