package com.clifton.lending.repo.interest;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.lending.repo.LendingRepo;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>LendingRepoInterestRate</code> defines an interest rate for a day for an open REPO.
 *
 * @author mwacker
 */
public class LendingRepoInterestRate extends BaseEntity<Integer> {

	private LendingRepo repo;

	private Date interestRateDate;
	private BigDecimal interestRate;
	private BigDecimal interestAmount;

	/**
	 * Date and time when accounting confirmed the opening REPO trade.
	 */
	@NonPersistentField
	private Date confirmedDate;
	@NonPersistentField
	private String confirmationNote;


	public LendingRepo getRepo() {
		return this.repo;
	}


	public void setRepo(LendingRepo repo) {
		this.repo = repo;
	}


	public Date getInterestRateDate() {
		return this.interestRateDate;
	}


	public void setInterestRateDate(Date interestRateDate) {
		this.interestRateDate = interestRateDate;
	}


	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}


	public BigDecimal getInterestAmount() {
		return this.interestAmount;
	}


	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}


	public Date getConfirmedDate() {
		return this.confirmedDate;
	}


	public void setConfirmedDate(Date confirmedDate) {
		this.confirmedDate = confirmedDate;
	}


	public String getConfirmationNote() {
		return this.confirmationNote;
	}


	public void setConfirmationNote(String confirmationNote) {
		this.confirmationNote = confirmationNote;
	}
}
