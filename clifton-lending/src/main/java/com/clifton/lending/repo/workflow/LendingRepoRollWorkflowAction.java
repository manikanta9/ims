package com.clifton.lending.repo.workflow;


import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>LendingRepoBookingWorkflowAction</code> ...
 *
 * @author mwacker
 */
public class LendingRepoRollWorkflowAction implements WorkflowTransitionActionHandler<LendingRepo> {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private LendingRepoService lendingRepoService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepo processAction(LendingRepo repo, WorkflowTransition transition) {
		return getLendingRepoService().rollLendingRepo(repo.getId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}
}
