package com.clifton.lending.repo;

import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.math.BigDecimal;
import java.util.Date;


@NonPersistentObject
public class LendingRepoRollTransitionCommand {

	private Integer repoId;
	private BigDecimal interestRate;
	private BigDecimal price;
	/**
	 * If present, this overrides the default maturity settlement date, which is typically inferred based on the term and maturity settlement date of the original repo.
	 */
	private Date maturitySettlementDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRepoId() {
		return this.repoId;
	}


	public void setRepoId(Integer repoId) {
		this.repoId = repoId;
	}


	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public Date getMaturitySettlementDate() {
		return this.maturitySettlementDate;
	}


	public void setMaturitySettlementDate(Date maturitySettlementDate) {
		this.maturitySettlementDate = maturitySettlementDate;
	}
}
