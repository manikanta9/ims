package com.clifton.lending.repo;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.lending.api.repo.Repo;
import com.clifton.lending.repo.interest.LendingRepoInterestRate;
import com.clifton.security.user.SecurityUser;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The <code>LendingRepo</code> defines the terms of a REPO or reverse REPO agreement.
 * <p/>
 * "parent" field is used during REPO rolls to link new REPO to the old one.
 *
 * @author mwacker
 */
public class LendingRepo extends BaseEntity<Integer> implements WorkflowAware, BookableEntity, AccountingBean {

	private LendingRepoType type;
	private LendingRepoDefinition repoDefinition;
	private SecurityUser traderUser;

	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;

	/**
	 * Number of days the repo is for. For repos with multiple interest rates, such as open and evergreen repos, this is the number of days for which each iterative interest rate
	 * applies, which is typically one.
	 */
	private Integer term;

	/**
	 * The number of days notice that either party has to give to cancel the REPO.  Currently used for Evergreen REPO's.
	 */
	private Short cancellationNoticeTerm;

	/**
	 * The interest rate of the REPO.
	 */
	private BigDecimal interestRate;
	private String description;

	private BigDecimal price;

	/**
	 * Specifies the Haircut to be applied to the Market Value of the security used for REPO.  Net Cash can never be greater than Market Value.
	 * Used to calculate Net Cash = (Haircut Percent > 100) ? (100 * Market Value / Haircut Percent) : (Market Value * Haircut Percent / 100)
	 */
	private BigDecimal haircutPercent;

	/**
	 * The total value of the bond borrowed or lent.
	 */
	private BigDecimal marketValue;
	/**
	 * Total cash borrowed or lent in the REPO.  marketValue*(1 - repoDefinitionDetail.hairCutPercent)
	 */
	private BigDecimal netCash;
	/**
	 * How much the loan will cost based on the term and interest rate.
	 */
	private BigDecimal interestAmount;
	private BigDecimal exchangeRateToBase;
	/**
	 * Some securities may adjust accounting notional by this multiplier that is tied to something.
	 * TIPS use it to adjust for changes in inflation: Index Ratio.
	 */
	private BigDecimal indexRatio;

	private BigDecimal quantityIntended;
	/**
	 * The original face of the bond being either received or lent out.
	 */
	private BigDecimal originalFace;

	private Date tradeDate;
	private Date settlementDate;
	/**
	 * Maturity Date is also known as "Action Date".
	 * It's used to tie REPO security trade to corresponding REPO maturity: same transaction and settlement dates.
	 */
	private Date maturityDate;
	/**
	 * The date at which an action needs to be taken, i.e. initiating a roll of the REPO
	 * NOTE: REPO Collateral depends on the "Maturity Date" which for users on screen is actually this field
	 * If fields are ever to be moved around, need to ensure REPO Collateral is using the correct "maturity Date" field for comparison
	 */
	private Date maturitySettlementDate;

	/**
	 * Date and time when accounting confirmed the opening REPO trade.
	 * REPO's must be confirmed before maturity.
	 */
	private Date confirmedDate;
	private String confirmationNote;

	private Date bookingDate;
	private Date maturityBookingDate;

	private List<LendingRepoInterestRate> interestRateList;

	/**
	 * Returns the primary key ID this object's parent object.
	 * Root nodes will return null.
	 */
	private Integer parentIdentifier;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Repo toRepo() {
		Repo result = new Repo();
		result.setId(getId());
		result.setParentId(getParentIdentifier());
		if (getInvestmentSecurity() != null) {
			result.setSecurity(getInvestmentSecurity().toSecurity());
		}
		if (getClientInvestmentAccount() != null) {
			result.setClientAccount(getClientInvestmentAccount().toClientAccount());
		}
		if (getHoldingInvestmentAccount() != null) {
			result.setHoldingAccount(getHoldingInvestmentAccount().toHoldingAccount());
		}
		if (getType() != null) {
			result.setRepoType(getType().getName());
		}
		if (getWorkflowState() != null) {
			result.setWorkflowState(getWorkflowState().getName());
		}
		if (getWorkflowStatus() != null) {
			result.setWorkflowStatus(getWorkflowStatus().getName());
		}
		if (getTraderUser() != null) {
			result.setTraderUser(getTraderUser().toUser());
		}

		LendingRepoDefinition repoDef = getRepoDefinition();
		if (repoDef != null) {
			if (repoDef.getRepoInvestmentAccount() != null) {
				result.setRepoAccount(repoDef.getRepoInvestmentAccount().toHoldingAccount());
			}
			if (repoDef.getCounterparty() != null) {
				result.setCounterparty(repoDef.getCounterparty().toCompany());
			}
			result.setReverse(repoDef.isReverse());
		}

		result.setTerm(getTerm());
		result.setCancellationNoticeTerm(getCancellationNoticeTerm());
		result.setInterestRate(getInterestRate());
		result.setDescription(getDescription());

		result.setPrice(getPrice());
		result.setHaircutPercent(getHaircutPercent());
		result.setMarketValue(getMarketValue());
		result.setNetCash(getNetCash());
		result.setInterestAmount(getInterestAmount());
		result.setExchangeRateToBase(getExchangeRateToBase());
		result.setIndexRatio(getIndexRatio());
		result.setQuantityIntended(getQuantityIntended());
		result.setOriginalFace(getOriginalFace());

		result.setTradeDate(getTradeDate());
		result.setSettlementDate(getSettlementDate());
		result.setMaturityDate(getMaturityDate());
		result.setMaturitySettlementDate(getMaturitySettlementDate());
		result.setConfirmedDate(getConfirmedDate());
		result.setConfirmationNote(getConfirmationNote());
		result.setBookingDate(getBookingDate());
		result.setMaturityBookingDate(getMaturityBookingDate());

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getUniqueLabel();
	}


	/**
	 * Returns the natural key fields as a label for this Repo
	 */
	public String getUniqueLabel() {
		StringBuilder sb = new StringBuilder(64);
		if (getClientInvestmentAccount() != null) {
			sb.append(getClientInvestmentAccount().getNumber());
			sb.append("-");
		}
		if (getHoldingInvestmentAccount() != null) {
			sb.append(getHoldingInvestmentAccount().getNumber());
			sb.append(": ");
		}
		sb.append(getLabelShort());
		return sb.toString();
	}


	public String getLabelShort() {
		StringBuilder sb = new StringBuilder(64);
		if (isBuy()) {
			sb.append("BUY ");
		}
		else {
			sb.append("SELL ");
		}
		sb.append(CoreMathUtils.formatNumberDecimal(getQuantityIntended()));
		if (getInvestmentSecurity() != null) {
			sb.append(" of ");
			sb.append(getInvestmentSecurity().getSymbol());
		}
		if (getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getTradeDate()));
		}
		return sb.toString();
	}


	@Override
	public Set<IdentityObject> getEntityLockSet() {
		return CollectionUtils.createHashSet(getClientInvestmentAccount());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getSourceEntityId() {
		return getId();
	}


	@Override
	public boolean isCollateral() {
		return false;
	}


	public boolean isConfirmed() {
		return getConfirmedDate() != null;
	}


	@Override
	public BigDecimal getQuantity() {
		return getQuantityIntended();
	}


	@Override
	public BigDecimal getQuantityNormalized() {
		return getQuantity();
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return getRepoDefinition().getInvestmentSecurity();
	}


	@Override
	public InvestmentSecurity getSettlementCurrency() {
		InvestmentSecurity result = getInvestmentSecurity();
		if (result != null) {
			return result.getInstrument().getTradingCurrency();
		}
		return null;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return getRepoDefinition().getClientInvestmentAccount();
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return getType() != null && getType().isTriPartyRepo() ? getRepoDefinition().getTriPartyInvestmentAccount() : getRepoDefinition().getHoldingInvestmentAccount();
	}


	@Override
	public BigDecimal getAccountingNotional() {
		return getMarketValue();
	}


	@Override
	public boolean isBuy() {
		return getRepoDefinition().isReverse();
	}


	@Override
	public Date getTransactionDate() {
		return getTradeDate();
	}


	public boolean isRoll() {
		return getParentIdentifier() != null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoDefinition getRepoDefinition() {
		return this.repoDefinition;
	}


	public void setRepoDefinition(LendingRepoDefinition repoDefinition) {
		this.repoDefinition = repoDefinition;
	}


	public SecurityUser getTraderUser() {
		return this.traderUser;
	}


	public void setTraderUser(SecurityUser traderUser) {
		this.traderUser = traderUser;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	@Override
	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public Integer getTerm() {
		return this.term;
	}


	public void setTerm(Integer term) {
		this.term = term;
	}


	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}


	@Override
	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	@Override
	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	@Override
	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	@Override
	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public List<LendingRepoInterestRate> getInterestRateList() {
		return this.interestRateList;
	}


	public void setInterestRateList(List<LendingRepoInterestRate> interestRateList) {
		this.interestRateList = interestRateList;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getMarketValue() {
		return this.marketValue;
	}


	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}


	public BigDecimal getNetCash() {
		return this.netCash;
	}


	public void setNetCash(BigDecimal netCash) {
		this.netCash = netCash;
	}


	public BigDecimal getInterestAmount() {
		return this.interestAmount;
	}


	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}


	@Override
	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}


	public Date getConfirmedDate() {
		return this.confirmedDate;
	}


	public void setConfirmedDate(Date confirmedDate) {
		this.confirmedDate = confirmedDate;
	}


	public String getConfirmationNote() {
		return this.confirmationNote;
	}


	public void setConfirmationNote(String confirmationNote) {
		this.confirmationNote = confirmationNote;
	}


	@Override
	public Date getBookingDate() {
		return this.bookingDate;
	}


	@Override
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Date getMaturityBookingDate() {
		return this.maturityBookingDate;
	}


	public void setMaturityBookingDate(Date maturityBookingDate) {
		this.maturityBookingDate = maturityBookingDate;
	}


	public BigDecimal getHaircutPercent() {
		return this.haircutPercent;
	}


	public void setHaircutPercent(BigDecimal haircutPercent) {
		this.haircutPercent = haircutPercent;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public BigDecimal getQuantityIntended() {
		return this.quantityIntended;
	}


	public void setQuantityIntended(BigDecimal quantityIntended) {
		this.quantityIntended = quantityIntended;
	}


	public LendingRepoType getType() {
		return this.type;
	}


	public void setType(LendingRepoType type) {
		this.type = type;
	}


	public Date getMaturitySettlementDate() {
		return this.maturitySettlementDate;
	}


	public void setMaturitySettlementDate(Date maturitySettlementDate) {
		this.maturitySettlementDate = maturitySettlementDate;
	}


	public Integer getParentIdentifier() {
		return this.parentIdentifier;
	}


	public void setParentIdentifier(Integer parentIdentifier) {
		this.parentIdentifier = parentIdentifier;
	}


	public Short getCancellationNoticeTerm() {
		return this.cancellationNoticeTerm;
	}


	public void setCancellationNoticeTerm(Short cancellationNoticeTerm) {
		this.cancellationNoticeTerm = cancellationNoticeTerm;
	}
}
