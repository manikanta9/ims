package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.lending.repo.LendingRepo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * The <code>LendingRepoPositionCloseBookingRule</code> creates the closing transactions for the assets that are being lent or re-purchased in the REPO.
 *
 * @author mwacker
 */
public class LendingRepoPositionCloseBookingRule extends BaseLendingRepoPositionBookingRule {

	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<LendingRepo> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		LendingRepo repo = bookingSession.getBookableEntity();

		journal.setDescription(repo.getDescription());

		// Handle non-reverse Tri-Party opening
		if (isRepoOpening() && repo.getType().isTriPartyRepo() && !repo.getRepoDefinition().isReverse()) {
			AccountingAccount positionAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION);

			List<? extends AccountingJournalDetailDefinition> details = CoreCollectionUtils.clone(journal.getJournalDetailList());
			for (AccountingJournalDetailDefinition positionEntry : CollectionUtils.getIterable(details)) {
				if (positionEntry.getAccountingAccount().equals(positionAccount) && positionEntry.getDescription().startsWith("Tri-Party:")
						&& positionEntry.getHoldingInvestmentAccount().equals(repo.getRepoDefinition().getTriPartyInvestmentAccount())) {
					AccountingJournalDetail openingPositionEntry = createBondCloseRecordTriParty((AccountingJournalDetail) positionEntry, getHoldingAccount(repo, true), "");
					journal.addJournalDetail(openingPositionEntry);
				}
			}
		}
		else {
			// position and distribution
			List<AccountingJournalDetail> positionEntry = new ArrayList<>();
			if (isRepoOpening()) {
				positionEntry.add(createBondCloseRecordForOpening(repo, getHoldingAccount(repo, true), ""));
			}
			else {
				positionEntry.addAll(createBondCloseRecordList(bookingSession, repo, getHoldingAccount(repo, true), ""));
				// for reverse REPO's create the opposing entry
				if (repo.getRepoDefinition().isReverse()) {
					positionEntry.addAll(createBondCloseRecordList(bookingSession, repo, getHoldingAccount(repo, false), ""));
				}
			}
			positionEntry.forEach(journal::addJournalDetail);
		}
	}


	protected AccountingJournalDetail createBondCloseRecordTriParty(AccountingJournalDetail positionBeingClosed, InvestmentAccount holdingInvestmentAccount, String descriptionPrefix) {
		AccountingJournalDetail positionEntry = new AccountingJournalDetail();
		BeanUtils.copyProperties(positionBeingClosed, positionEntry, new String[]{"parentTransaction", "parentDefinition"});

		positionEntry.setParentDefinition(positionBeingClosed);
		positionEntry.setQuantity(positionBeingClosed.getQuantity().negate());

		positionEntry.setOriginalTransactionDate(positionBeingClosed.getOriginalTransactionDate());
		positionEntry.setExchangeRateToBase(positionBeingClosed.getExchangeRateToBase());

		positionEntry.setLocalDebitCredit(positionBeingClosed.getLocalDebitCredit().negate());
		positionEntry.setBaseDebitCredit(positionBeingClosed.getBaseDebitCredit().negate());
		positionEntry.setPositionCostBasis(positionBeingClosed.getPositionCostBasis().negate());

		positionEntry.setOpening(true);
		positionEntry.setHoldingInvestmentAccount(holdingInvestmentAccount);
		positionEntry.setDescription("Position opening from transfer to " + holdingInvestmentAccount.getLabel());

		positionEntry.setDescription(descriptionPrefix + "Position opening from REPO " + (isRepoOpening() ? "opening" : "maturity") + " for " + positionEntry.getInvestmentSecurity().getSymbol());

		return positionEntry;
	}


	protected AccountingJournalDetail createBondCloseRecordForOpening(LendingRepo repo, InvestmentAccount holdingInvestmentAccount, String descriptionPrefix) {
		BigDecimal multiplier = repo.getRepoDefinition().getInvestmentSecurity().getPriceMultiplier();

		AccountingAccount positionAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION);

		AccountingJournalDetail positionEntry = new AccountingJournalDetail();
		positionEntry.setAccountingAccount(positionAccount);
		positionEntry.setFkFieldId(repo.getId());
		positionEntry.setSystemTable(getLendingRepoTable());
		positionEntry.setInvestmentSecurity(repo.getRepoDefinition().getInvestmentSecurity());
		positionEntry.setHoldingInvestmentAccount(holdingInvestmentAccount);
		positionEntry.setClientInvestmentAccount(repo.getRepoDefinition().getClientInvestmentAccount());
		positionEntry.setPrice(repo.getPrice());
		positionEntry.setQuantity(repo.getOriginalFace().negate());
		positionEntry.setExchangeRateToBase(repo.getExchangeRateToBase());
		positionEntry.setLocalDebitCredit(MathUtils.multiply(MathUtils.multiply(MathUtils.multiply(positionEntry.getPrice(), positionEntry.getQuantity()), multiplier), repo.getIndexRatio(), 2));
		positionEntry.setBaseDebitCredit(MathUtils.multiply(positionEntry.getLocalDebitCredit(),
				(positionEntry.getExchangeRateToBase() == null ? BigDecimal.ONE : positionEntry.getExchangeRateToBase()), 2));
		positionEntry.setPositionCostBasis(positionEntry.getLocalDebitCredit());
		positionEntry.setTransactionDate(repo.getTradeDate());
		positionEntry.setSettlementDate(repo.getSettlementDate());

		positionEntry.setOriginalTransactionDate(repo.getSettlementDate());
		positionEntry.setPositionCommission(BigDecimal.ZERO);
		positionEntry.setOpening(false);

		positionEntry.setDescription(descriptionPrefix + "Position close from " + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO " + (isRepoOpening() ? "opening" : "maturity")
				+ " for " + repo.getInvestmentSecurity().getSymbol());

		return positionEntry;
	}
}
