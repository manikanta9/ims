package com.clifton.lending.repo;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.lending.repo.interest.LendingRepoInterestRate;
import com.clifton.lending.repo.interest.LendingRepoInterestRateService;
import com.clifton.lending.repo.interest.search.LendingRepoInterestRateSearchForm;
import com.clifton.lending.repo.pairoff.LendingRepoPairOff;
import com.clifton.lending.repo.pairoff.LendingRepoPairOffService;
import com.clifton.lending.repo.pairoff.search.LendingRepoPairOffSearchForm;
import com.clifton.lending.repo.search.LendingRepoDefinitionSearchForm;
import com.clifton.lending.repo.search.LendingRepoSearchForm;
import com.clifton.lending.repo.search.LendingRepoSearchFormConfigurer;
import com.clifton.lending.repo.search.LendingRepoSettingSearchForm;
import com.clifton.lending.repo.search.LendingRepoTypeSearchForm;
import com.clifton.marketdata.MarketDataService;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.security.user.SecurityUserService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class LendingRepoServiceImpl implements LendingRepoService {

	private AdvancedUpdatableDAO<LendingRepoType, Criteria> lendingRepoTypeDAO;
	private AdvancedUpdatableDAO<LendingRepoDefinition, Criteria> lendingRepoDefinitionDAO;
	private AdvancedUpdatableDAO<LendingRepo, Criteria> lendingRepoDAO;
	private AdvancedUpdatableDAO<LendingRepoSetting, Criteria> lendingRepoSettingDAO;

	private DaoNamedEntityCache<LendingRepoType> lendingRepoTypeCache;
	private DaoSingleKeyCache<LendingRepoSetting, Short> lendingRepoSettingByInstrumentHierarchyCache;

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;
	private LendingRepoInterestRateService lendingRepoInterestRateService;
	private LendingRepoPairOffService lendingRepoPairOffService;
	private MarketDataService marketDataService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private SecurityUserService securityUserService;
	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////        LendingRepoDefinition Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepoDefinition getLendingRepoDefinition(int id) {
		return getLendingRepoDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<LendingRepoDefinition> getLendingRepoDefinitionList(LendingRepoDefinitionSearchForm searchForm) {
		return getLendingRepoDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private LendingRepoDefinition saveLendingRepoDefinition(LendingRepoDefinition bean) {
		return getLendingRepoDefinitionDAO().save(bean);
	}


	@Transactional
	protected void deleteLendingRepoDefinition(int id) {
		LendingRepoDefinition definition = getLendingRepoDefinition(id);
		LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
		searchForm.setRepoDefinitionId(definition.getId());
		List<LendingRepo> repoList = getLendingRepoList(searchForm);
		for (LendingRepo repo : CollectionUtils.getIterable(repoList)) {
			doDeleteLendingRepo(repo);
		}
		doDeleteLendingRepoDefinition(definition);
	}


	private void doDeleteLendingRepoDefinition(LendingRepoDefinition definition) {
		getLendingRepoDefinitionDAO().delete(definition);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            LendingRepoType Business Methods   	       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepoType getLendingRepoType(int id) {
		return getLendingRepoTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public LendingRepoType getLendingRepoTypeByName(String name) {
		return getLendingRepoTypeCache().getBeanForKeyValue(getLendingRepoTypeDAO(), name);
	}


	@Override
	public List<LendingRepoType> getLendingRepoTypeList(LendingRepoTypeSearchForm searchForm) {
		return getLendingRepoTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////             LendingRepo Business Methods   	       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepo getLendingRepo(int id) {
		LendingRepo result = getLendingRepoDAO().findByPrimaryKey(id);
		if (result != null) {
			result.setInterestRateList(getLendingRepoInterestRateService().getLendingRepoInterestRateByRepo(result.getId()));
		}
		return result;
	}


	@Override
	public List<LendingRepo> getLendingRepoListForIds(Integer[] repoIds, boolean populateInterestRateList) {
		List<LendingRepo> result = getLendingRepoDAO().findByPrimaryKeys(repoIds);
		if (populateInterestRateList && !CollectionUtils.isEmpty(result)) {
			CollectionUtils.getStream(result).forEach(lendingRepo -> lendingRepo.setInterestRateList(getLendingRepoInterestRateService().getLendingRepoInterestRateByRepo(lendingRepo.getId())));
		}
		return result;
	}


	@Override
	public List<LendingRepo> getLendingRepoListByParent(int parentRepoId) {
		return getLendingRepoDAO().findByField("parentIdentifier", parentRepoId);
	}


	@Override
	public List<LendingRepo> getLendingRepoList(final LendingRepoSearchForm searchForm) {
		return getLendingRepoDAO().findBySearchCriteria(new LendingRepoSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	@Override
	@Transactional
	public LendingRepo saveLendingRepo(LendingRepo bean) {
		if ((bean.isNewBean() && bean.getRepoDefinition().isNewBean()) || !bean.isRoll()) {
			// save the definition
			LendingRepoDefinition savedLendingRepoDefinition = saveLendingRepoDefinition(bean.getRepoDefinition());
			bean.setRepoDefinition(savedLendingRepoDefinition);
		}
		else {
			// Ensure that the correct repo definition is used for internal calculations
			bean.setRepoDefinition(getLendingRepoDefinition(bean.getRepoDefinition().getId()));
		}

		if (bean.getExchangeRateToBase() == null) {
			InvestmentAccount clientAccount = bean.getRepoDefinition().getClientInvestmentAccount();
			InvestmentSecurity security = bean.getRepoDefinition().getInvestmentSecurity();
			if (security.getInstrument().getTradingCurrency().equals(clientAccount.getBaseCurrency())) {
				bean.setExchangeRateToBase(BigDecimal.ONE);
			}
			else {
				BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(
						FxRateLookupCommand.forFxSource(bean.fxSourceCompany(), !bean.fxSourceCompanyOverridden(), security.getInstrument().getTradingCurrency().getSymbol(), clientAccount.getBaseCurrency().getSymbol(), bean.getTradeDate())
								.flexibleLookup()
				);
				bean.setExchangeRateToBase(fxRate);
			}
		}

		return getLendingRepoDAO().save(bean);
	}


	@Override
	public LendingRepo saveLendingRepoInternal(LendingRepo bean) {
		return getLendingRepoDAO().save(bean);
	}


	@Override
	@Transactional
	public void deleteLendingRepo(int id) {
		// TODO: add workflow and validate that the repo is in the correct state
		LendingRepo repo = getLendingRepo(id);
		LendingRepoDefinition repoDefinition = repo.getRepoDefinition();
		getLendingRepoDAO().delete(id);

		// get the list of REPO definitions
		LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
		searchForm.setRepoDefinitionId(repoDefinition.getId());
		List<LendingRepo> repoList = getLendingRepoList(searchForm);
		// if this is only REPO attached to the definition delete the definition
		if (CollectionUtils.isEmpty(repoList)) {
			doDeleteLendingRepoDefinition(repoDefinition);
		}
	}


	private void doDeleteLendingRepo(LendingRepo repo) {
		getLendingRepoDAO().delete(repo);
	}


	@Override
	@Transactional
	public LendingRepo rollLendingRepo(int id) {
		// Get and validate repo to roll
		LendingRepo repo = getLendingRepo(id);
		ValidationUtils.assertNotNull(repo.getMaturityDate(), "A maturity date is required to roll a repo.", "maturityDate");
		ValidationUtils.assertNotNull(repo.getMaturitySettlementDate(), "A maturity settlement date is required to roll a repo.", "maturitySettlementDate");

		// Clone original repo, setting standard field values
		LendingRepo newRepo = doCopyLendingRepo(repo, false);
		newRepo.setParentIdentifier(repo.getId());
		newRepo.setTraderUser(getSecurityUserService().getSecurityUserCurrent());
		newRepo.setTradeDate(repo.getMaturityDate());
		newRepo.setSettlementDate(repo.getMaturitySettlementDate());
		newRepo.setMaturityDate(null);
		newRepo.setMaturitySettlementDate(null);
		newRepo.setTerm(null);
		newRepo.setInterestRate(null);
		newRepo.setInterestAmount(null);
		newRepo.setConfirmationNote(null);
		newRepo.setConfirmedDate(null);

		// Update additional fields
		Date settlementDate = repo.getMaturitySettlementDate();
		Calendar calendar = getCalendarSetupService().getCalendarByName(LendingRepoService.LENDING_REPO_CALENDAR_NAME);
		if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(settlementDate, calendar.getId()))) {
			settlementDate = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(settlementDate, calendar.getId()));
		}

		// Set the term to maturity for repo types which require a term length
		if (newRepo.getType().isMaturityDateRequiredForOpen()) {
			// Determine term, maturity, and maturity settlement
			Integer term = ObjectUtils.coalesce(newRepo.getType().getRequiredTerm(), repo.getTerm());
			ValidationUtils.assertNotNull(term, String.format("Unable to infer a term the given repo of type [%s]. The repo type requires a term, but no explicit term length is defined on the repo type and the repo being rolled does not have a term defined.", newRepo.getType().getName()));
			Date maturitySettlementDate = getCalendarBusinessDayService().getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(settlementDate, calendar.getId()), term);
			LendingRepoSetting repoSetting = getLendingRepoSettingByInstrumentHierarchy(repo.getRepoDefinition().getInstrumentHierarchy().getId());
			int daysToSettle = repoSetting != null
					? repoSetting.getDaysToSettle()
					: getCalendarBusinessDayService().getBusinessDaysBetween(repo.getTradeDate(), repo.getSettlementDate(), null, calendar.getId());
			Date maturityDate = getCalendarBusinessDayService().getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(maturitySettlementDate, calendar.getId()), -1 * daysToSettle);

			newRepo.setMaturityDate(maturityDate);
			newRepo.setMaturitySettlementDate(maturitySettlementDate);
			newRepo.setTerm(DateUtils.getDaysDifference(maturitySettlementDate, settlementDate));
		}

		// update the index ratio
		if (newRepo.getRepoDefinition().getInvestmentSecurity().getInstrument().getHierarchy().isIndexRatioAdjusted()) {
			BigDecimal indexRatio = getMarketDataService().getMarketDataIndexRatioForSecurity(newRepo.getRepoDefinition().getInvestmentSecurity().getId(), settlementDate, false);
			newRepo.setIndexRatio(indexRatio);
		}
		return saveLendingRepo(newRepo);
	}


	@Override
	@Transactional
	public LendingRepo executeLendingRepoRoll(LendingRepoRollTransitionCommand rollCommand) {
		ValidationUtils.assertNotNull(rollCommand.getRepoId(), "A REPO id is required.", "repoId");
		ValidationUtils.assertNotNull(rollCommand.getPrice(), "A price for the new REPO is required.", "price");
		ValidationUtils.assertNotNull(rollCommand.getInterestRate(), "An interest rate for the new REPO is required.", "interestRate");

		LendingRepo existingRepo = getLendingRepo(rollCommand.getRepoId());
		WorkflowState rollState = getWorkflowDefinitionService().getWorkflowStateByName(existingRepo.getWorkflowState().getWorkflow().getId(), REPO_ROLL_WORKFLOW_STATE_NAME);
		WorkflowTransition rollTransition = getWorkflowTransitionService().getWorkflowTransitionForEntity(existingRepo, existingRepo.getWorkflowState(), rollState);
		getWorkflowTransitionService().executeWorkflowTransitionByTransition(LendingRepo.class.getSimpleName(), rollCommand.getRepoId(), rollTransition.getId());

		List<LendingRepo> repoListForParent = getLendingRepoListByParent(rollCommand.getRepoId());
		List<LendingRepo> nonCancelledRepoList = CollectionUtils.getFiltered(repoListForParent, repo -> !LendingRepoService.REPO_CANCELLED_WORKFLOW_STATE_NAME.equals(repo.getWorkflowState().getName()));
		LendingRepo newRepo = CollectionUtils.getOnlyElementStrict(nonCancelledRepoList);
		newRepo.setInterestRate(rollCommand.getInterestRate());
		newRepo.setPrice(rollCommand.getPrice());
		if (rollCommand.getMaturitySettlementDate() != null) {
			// Override the default inferred maturity and maturity settlement dates
			newRepo.setMaturityDate(null); // Re-infer on save
			newRepo.setMaturitySettlementDate(rollCommand.getMaturitySettlementDate());
		}
		return saveLendingRepo(newRepo);
	}


	@Override
	@Transactional
	public LendingRepo copyLendingRepo(int id, boolean copyDefinition) {
		LendingRepo repo = getLendingRepo(id);
		LendingRepo newRepo = doCopyLendingRepo(repo, copyDefinition);
		return saveLendingRepo(newRepo);
	}


	@Transactional
	protected LendingRepo doCopyLendingRepo(LendingRepo repo, boolean copyDefinition) {
		LendingRepo newRepo = BeanUtils.cloneBean(repo, false, false);
		newRepo.setId(null);
		newRepo.setWorkflowState(null);
		newRepo.setWorkflowStatus(null);

		if (copyDefinition) {
			// copy the definition and the repo
			LendingRepoDefinition newRepoDefinition = BeanUtils.cloneBean(repo.getRepoDefinition(), false, false);
			newRepoDefinition.setId(null);
			newRepo.setRepoDefinition(newRepoDefinition);
		}

		newRepo.setBookingDate(null);
		newRepo.setMaturityBookingDate(null);
		newRepo.setConfirmedDate(null);

		return newRepo;
	}


	@Override
	@Transactional
	public void confirmLendingRepo(Integer[] ids, String confirmationNote) {
		ValidationUtils.assertNotEmpty(confirmationNote, "REPO confirmation note is required.", "confirmationNote");
		List<LendingRepo> lendingRepoList = this.getLendingRepoListForIds(ids, false);
		Set<String> lendingRepoPairOffGroupIdList = new HashSet<>();
		for (LendingRepo lendingRepo : CollectionUtils.getIterable(lendingRepoList)) {
			if(!lendingRepo.getType().isAutoPairoff()){
				LendingRepoPairOffSearchForm searchForm  = new LendingRepoPairOffSearchForm();
				searchForm.setOpeningOrMaturingRepoId(lendingRepo.getId());
				searchForm.setCompleted(Boolean.FALSE);
				lendingRepoPairOffGroupIdList.addAll(CollectionUtils.getStream(getLendingRepoPairOffService().getLendingRepoPairOffList(searchForm)).map(LendingRepoPairOff::getPairOffGroup).collect(Collectors.toList()));
			}
			lendingRepo.setConfirmationNote(confirmationNote);
			lendingRepo.setConfirmedDate(new Date());
			saveLendingRepo(lendingRepo);
		}
		// Handle collected pairOffGroupId's
		Set<String> newLendingRepoPairOffIdList = new HashSet<>();
		for(String pairOffGroupId: CollectionUtils.getIterable(lendingRepoPairOffGroupIdList)){
			//Unpair lendingRepoPairOff's
			if (pairOffGroupId.contains(":")) {
				getLendingRepoPairOffService().splitLendingRepoPairOff(pairOffGroupId);
				newLendingRepoPairOffIdList.addAll(CollectionUtils.createList(StringUtils.split(pairOffGroupId, ":")));
			}
			else {
				newLendingRepoPairOffIdList.add(pairOffGroupId);
			}
		}
		for(String pairOffGroupId: CollectionUtils.getIterable(newLendingRepoPairOffIdList)){
			//Complete pairoff
			getLendingRepoPairOffService().setLendingRepoPairOffGroupCompleted(pairOffGroupId,true);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	///////        LendingRepo Accrued Interest Business Methods   	  //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Moves date passed to next business day less one day and calculates the repo for that date
	 * This supports always including following non-business day accrued interest on the previous business day
	 * to match what open repos currently do (convention to input manually on business days only)
	 */
	@Override
	public BigDecimal getLendingRepoAccruedInterest(int repoId, Date date) {
		LendingRepo repo = getLendingRepo(repoId);
		if (repo == null) {
			return null;
		}
		return getLendingRepoAccruedInterestImpl(repo, getLendingRepoAccruedInterestLookupDateForDate(date));
	}


	/**
	 * Returns next business day less one day of date passed.  Logic used for accrued interest for repos
	 */
	private Date getLendingRepoAccruedInterestLookupDateForDate(Date date) {
		return DateUtils.addDays(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(date), 1), -1);
	}


	/**
	 * Assumes Date passed in uses correct logic for moving to next business day - 1 day so that Friday includes Saturday & Sunday
	 */
	private BigDecimal getLendingRepoAccruedInterestImpl(LendingRepo repo, Date date) {
		if (repo != null) {
			// Pull from Interest Rate Table
			if (repo.getType().isMultipleInterestRatesPerPeriod()) {
				LendingRepoInterestRateSearchForm searchForm = new LendingRepoInterestRateSearchForm();
				searchForm.setRepoId(repo.getId());
				searchForm.setBeforeRateDate(DateUtils.addDays(date, 1));
				List<LendingRepoInterestRate> rateList = getLendingRepoInterestRateService().getLendingRepoInterestRateList(searchForm);
				return CoreMathUtils.sumProperty(rateList, LendingRepoInterestRate::getInterestAmount);
			}
			// Pull in Current Portion of the Interest
			BigDecimal interest = repo.getInterestAmount();
			int daysSinceSettle = DateUtils.getDaysDifferenceInclusive(date, repo.getSettlementDate());
			return CoreMathUtils.getValueProrated(BigDecimal.valueOf(daysSinceSettle), BigDecimal.valueOf(repo.getTerm()), interest);
		}
		return BigDecimal.ZERO;
	}


	/**
	 * NOTE: REPO Accrued Interest is calculated so that holidays and weekend interest is applied to the previous business day
	 * i.e. Friday accrued interest includes Saturday and Sunday.
	 */
	@Override
	public List<LendingRepoAccruedInterest> getLendingRepoAccruedInterestList(Date measureDate, LendingRepoSearchForm searchForm) {
		ValidationUtils.assertNotNull(measureDate, "Measure Date is required to lookup Lending Repos with Accrued Interest.");
		Date nextBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(measureDate), 1);

		searchForm.setExcludeWorkflowStateName("Cancelled");
		searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN, nextBusinessDay));
		searchForm.addSearchRestriction(new SearchRestriction("maturitySettlementDate", ComparisonConditions.GREATER_THAN_OR_IS_NULL, nextBusinessDay));

		List<LendingRepo> repoList = getLendingRepoList(searchForm);
		List<LendingRepoAccruedInterest> repoInterestList = null;
		int totalCount;
		if (!CollectionUtils.isEmpty(repoList)) {
			if (repoList instanceof PagingArrayList) {
				totalCount = ((PagingArrayList<LendingRepo>) repoList).getTotalElementCount();
				repoInterestList = new PagingArrayList<>(((PagingArrayList<LendingRepo>) repoList).getFirstElementIndex(), ((PagingArrayList<LendingRepo>) repoList).getPageSize());
			}
			else {
				totalCount = repoList.size();
				repoInterestList = new ArrayList<>();
			}
			// Compare to Previous Day, Not Previous Business Day since Interest Is applied to previous business days for non-business days
			Date previousDay = DateUtils.addDays(measureDate, -1);
			// Move date to the next business day less one day, i.e. Friday date would be moved to Monday - 1 day = Sunday to include non-business days following the date
			// Call it only once and use the impl method for accrued interest so no extra look ups since it's all calculated for the same date
			Date date = getLendingRepoAccruedInterestLookupDateForDate(measureDate);

			for (LendingRepo repo : CollectionUtils.getIterable(repoList)) {
				// Previous Interest Amount
				BigDecimal lastInterestAmount = getLendingRepoAccruedInterestImpl(repo, previousDay);
				BigDecimal currentInterest = getLendingRepoAccruedInterestImpl(repo, date);
				BigDecimal dailyInterest = MathUtils.subtract(currentInterest, lastInterestAmount);
				repoInterestList.add(new LendingRepoAccruedInterest(repo, measureDate, date, currentInterest, dailyInterest));
			}

			if (repoInterestList instanceof PagingArrayList) {
				((PagingArrayList<LendingRepoAccruedInterest>) repoInterestList).setTotalElementCount(totalCount);
			}
		}
		return repoInterestList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////         LendingRepoSetting Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepoSetting getLendingRepoSetting(int id) {
		return getLendingRepoSettingDAO().findByPrimaryKey(id);
	}


	@Override
	public LendingRepoSetting getLendingRepoSettingByInstrumentHierarchy(short instrumentHierarchyId) {
		return getLendingRepoSettingByInstrumentHierarchyCache().getBeanForKeyValue(getLendingRepoSettingDAO(), instrumentHierarchyId);
	}


	@Override
	public List<LendingRepoSetting> getLendingRepoSettingList(LendingRepoSettingSearchForm searchForm) {
		return getLendingRepoSettingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public LendingRepoSetting saveLendingRepoSetting(LendingRepoSetting bean) {
		return getLendingRepoSettingDAO().save(bean);
	}


	@Override
	public void deleteLendingRepoSetting(int id) {
		getLendingRepoSettingDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<LendingRepoType, Criteria> getLendingRepoTypeDAO() {
		return this.lendingRepoTypeDAO;
	}


	public void setLendingRepoTypeDAO(AdvancedUpdatableDAO<LendingRepoType, Criteria> lendingRepoTypeDAO) {
		this.lendingRepoTypeDAO = lendingRepoTypeDAO;
	}


	public AdvancedUpdatableDAO<LendingRepoDefinition, Criteria> getLendingRepoDefinitionDAO() {
		return this.lendingRepoDefinitionDAO;
	}


	public void setLendingRepoDefinitionDAO(AdvancedUpdatableDAO<LendingRepoDefinition, Criteria> lendingRepoDefinitionDAO) {
		this.lendingRepoDefinitionDAO = lendingRepoDefinitionDAO;
	}


	public AdvancedUpdatableDAO<LendingRepo, Criteria> getLendingRepoDAO() {
		return this.lendingRepoDAO;
	}


	public void setLendingRepoDAO(AdvancedUpdatableDAO<LendingRepo, Criteria> lendingRepoDAO) {
		this.lendingRepoDAO = lendingRepoDAO;
	}


	public AdvancedUpdatableDAO<LendingRepoSetting, Criteria> getLendingRepoSettingDAO() {
		return this.lendingRepoSettingDAO;
	}


	public void setLendingRepoSettingDAO(AdvancedUpdatableDAO<LendingRepoSetting, Criteria> lendingRepoSettingDAO) {
		this.lendingRepoSettingDAO = lendingRepoSettingDAO;
	}


	public DaoNamedEntityCache<LendingRepoType> getLendingRepoTypeCache() {
		return this.lendingRepoTypeCache;
	}


	public void setLendingRepoTypeCache(DaoNamedEntityCache<LendingRepoType> lendingRepoTypeCache) {
		this.lendingRepoTypeCache = lendingRepoTypeCache;
	}


	public DaoSingleKeyCache<LendingRepoSetting, Short> getLendingRepoSettingByInstrumentHierarchyCache() {
		return this.lendingRepoSettingByInstrumentHierarchyCache;
	}


	public void setLendingRepoSettingByInstrumentHierarchyCache(DaoSingleKeyCache<LendingRepoSetting, Short> lendingRepoSettingByInstrumentHierarchyCache) {
		this.lendingRepoSettingByInstrumentHierarchyCache = lendingRepoSettingByInstrumentHierarchyCache;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public LendingRepoInterestRateService getLendingRepoInterestRateService() {
		return this.lendingRepoInterestRateService;
	}


	public void setLendingRepoInterestRateService(LendingRepoInterestRateService lendingRepoInterestRateService) {
		this.lendingRepoInterestRateService = lendingRepoInterestRateService;
	}


	public LendingRepoPairOffService getLendingRepoPairOffService() {
		return this.lendingRepoPairOffService;
	}


	public void setLendingRepoPairOffService(LendingRepoPairOffService lendingRepoPairOffService) {
		this.lendingRepoPairOffService = lendingRepoPairOffService;
	}


	public MarketDataService getMarketDataService() {
		return this.marketDataService;
	}


	public void setMarketDataService(MarketDataService marketDataService) {
		this.marketDataService = marketDataService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
