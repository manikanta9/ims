package com.clifton.lending.repo;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;


public class LendingRepoSetting extends BaseEntity<Integer> {

	private InvestmentInstrumentHierarchy instrumentHierarchy;

	private int daysToSettle = 0;

	// TODO: use the actual day count class, submitting the enum is not working correctly
	private String dayCountConvention;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentHierarchy getInstrumentHierarchy() {
		return this.instrumentHierarchy;
	}


	public void setInstrumentHierarchy(InvestmentInstrumentHierarchy instrumentHierarchy) {
		this.instrumentHierarchy = instrumentHierarchy;
	}


	public int getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(int daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	public String getDayCountConvention() {
		return this.dayCountConvention;
	}


	public void setDayCountConvention(String dayCountConvention) {
		this.dayCountConvention = dayCountConvention;
	}
}
