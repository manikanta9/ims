package com.clifton.lending.repo;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.lending.repo.search.LendingRepoDefinitionSearchForm;
import com.clifton.lending.repo.search.LendingRepoSearchForm;
import com.clifton.lending.repo.search.LendingRepoSettingSearchForm;
import com.clifton.lending.repo.search.LendingRepoTypeSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public interface LendingRepoService {

	public static final String REPO_CLOSED_WORKFLOW_STATUS_NAME = "Closed";
	public static final String REPO_CANCELLED_WORKFLOW_STATE_NAME = "Cancelled";
	public static final String REPO_ACTIVE_WORKFLOW_STATE_NAME = "Active Repo";
	public static final String REPO_CLOSED_WORKFLOW_STATE_NAME = "Closed";
	public static final String REPO_UNBOOKED_WORKFLOW_STATE_NAME = "Unbooked";
	public static final String REPO_ROLL_WORKFLOW_STATE_NAME = "Roll";
	public static final String REPO_VALIDATED_WORKFLOW_STATE_NAME = "Validated";

	/**
	 * REPO usually follows bank holidays (custodians).
	 */
	public static final String LENDING_REPO_CALENDAR_NAME = "US Banks Calendar";

	////////////////////////////////////////////////////////////////////////////
	////////        LendingRepoDefinition Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoDefinition getLendingRepoDefinition(int id);


	public List<LendingRepoDefinition> getLendingRepoDefinitionList(LendingRepoDefinitionSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////            LendingRepoType Business Methods   	       /////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoType getLendingRepoType(int id);


	public LendingRepoType getLendingRepoTypeByName(String name);


	public List<LendingRepoType> getLendingRepoTypeList(LendingRepoTypeSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////             LendingRepo Business Methods   	       /////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepo getLendingRepo(int id);


	@DoNotAddRequestMapping
	public List<LendingRepo> getLendingRepoListForIds(Integer[] repoIds, boolean populateInterestRateList);


	public List<LendingRepo> getLendingRepoListByParent(int parentRepoId);


	public List<LendingRepo> getLendingRepoList(LendingRepoSearchForm searchForm);


	/**
	 * Saves the LendingRepo when it's an existing LendingRepo, and
	 * saves the LendingRepo and LendingRepoDefinition when it's not an existing object.
	 */
	public LendingRepo saveLendingRepo(LendingRepo bean);


	/**
	 * Saves the LendingRepo with out any validation or calculations.
	 */
	@DoNotAddRequestMapping
	public LendingRepo saveLendingRepoInternal(LendingRepo bean);


	/**
	 * Deletes the LendingRepo and related LendingRepoDefinition.
	 */
	public void deleteLendingRepo(int id);


	/**
	 * Copies the entire LendingRepo object including the LendingRepoDefinition and returns it to the caller.
	 */
	public LendingRepo copyLendingRepo(int id, boolean copyDefinition);


	/**
	 * Creates a new LendingRepo that reference the same LendingRepoDefinition,
	 * and sets the original LendingRepo as the parent of the new LendingRepo object.
	 */
	@DoNotAddRequestMapping
	public LendingRepo rollLendingRepo(int id);


	/**
	 * Will call the workflow transition service to roll the existing REPO.  Then it will find
	 * the newly create REPO, update the price and rate and return the new REPO.
	 */
	@RequestMapping("lendingRepoRollExecute")
	public LendingRepo executeLendingRepoRoll(LendingRepoRollTransitionCommand rollCommand);


	@RequestMapping("lendingRepoConfirm")
	public void confirmLendingRepo(Integer[] ids, String confirmationNote);

	////////////////////////////////////////////////////////////////////////////
	///////        LendingRepo Accrued Interest Business Methods   	  //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Properly calculates the accrued interest for a repo from the settlement date to the given date
	 * If Open/Multiple Rates uses the LendingRepoInterestTable else (Term) takes the proportion of the interest on the
	 * repo for the number of days since settlement
	 */
	@SecureMethod(dtoClass = LendingRepo.class)
	public BigDecimal getLendingRepoAccruedInterest(int repoId, Date date);


	@SecureMethod(dtoClass = LendingRepo.class)
	public List<LendingRepoAccruedInterest> getLendingRepoAccruedInterestList(Date measureDate, LendingRepoSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////         LendingRepoSetting Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoSetting getLendingRepoSetting(int id);


	public LendingRepoSetting getLendingRepoSettingByInstrumentHierarchy(short instrumentHierarchyId);


	public List<LendingRepoSetting> getLendingRepoSettingList(LendingRepoSettingSearchForm searchForm);


	public LendingRepoSetting saveLendingRepoSetting(LendingRepoSetting bean);


	public void deleteLendingRepoSetting(int id);
}
