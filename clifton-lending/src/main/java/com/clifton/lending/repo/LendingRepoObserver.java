package com.clifton.lending.repo;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.lending.repo.interest.LendingRepoInterestRate;
import com.clifton.lending.repo.interest.LendingRepoInterestRateService;
import com.clifton.lending.repo.interest.search.LendingRepoInterestRateSearchForm;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.transition.WorkflowTransitionUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.Function;


/**
 * The {@link LendingRepoObserver} is the observer for {@link LendingRepo} DAO actions.
 * <p>
 * This observer handles property normalization, especially with regards to calculated values such as {@link LendingRepo#marketValue market value}, {@link
 * LendingRepo#interestAmount interest amount}, and {@link LendingRepo#netCash net cash}.
 *
 * @author MikeH
 */
@Component
public class LendingRepoObserver extends SelfRegisteringDaoObserver<LendingRepo> {

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;
	private InvestmentCalculatorService investmentCalculatorService;
	private LendingRepoInterestRateService lendingRepoInterestRateService;
	private LendingRepoService lendingRepoService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public LendingRepoObserver() {
		// Run before validator
		setOrder(-100);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeTransactionMethodCallImpl(ReadOnlyDAO<LendingRepo> dao, DaoEventTypes event, LendingRepo repo) {
		applyMaturityDates(repo);
		applyNormalizedTerm(repo);
		applyUpdatedRepoAmounts(repo);
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<LendingRepo> dao, DaoEventTypes event, LendingRepo repo, Throwable e) {
		// Guard-clause: Ignore when exceptions are thrown
		if (e != null) {
			return;
		}
		applyInterestRateValues(repo, event.isInsert());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Automatically infers and applies the maturity date and maturity settlement date for the given repo.
	 */
	private void applyMaturityDates(LendingRepo repo) {
		if (!repo.getType().isMultipleInterestRatesPerPeriod() && repo.getMaturitySettlementDate() == null && repo.getTerm() != null) {
			// Infer maturity settlement date from term unless the term is a stand-in for iterative interest rate duration
			Calendar calendar = getCalendarSetupService().getCalendarByName(LendingRepoService.LENDING_REPO_CALENDAR_NAME);
			repo.setMaturitySettlementDate(getCalendarBusinessDayService().getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(repo.getSettlementDate(), calendar.getId()), repo.getTerm()));
		}

		// Infer maturity date from maturity settlement date
		if (repo.getMaturityDate() == null && repo.getMaturitySettlementDate() != null) {
			Calendar calendar = getCalendarSetupService().getCalendarByName(LendingRepoService.LENDING_REPO_CALENDAR_NAME);
			LendingRepoSetting repoSetting = getLendingRepoService().getLendingRepoSettingByInstrumentHierarchy(repo.getRepoDefinition().getInstrumentHierarchy().getId());
			int daysToSettle = repoSetting != null
					// Use days-to-settle from setting
					? repoSetting.getDaysToSettle()
					// Infer days-to-settle from the settlement of the open transaction
					: getCalendarBusinessDayService().getBusinessDaysBetween(repo.getTradeDate(), repo.getSettlementDate(), null, calendar.getId());
			Date maturityDate = getCalendarBusinessDayService().getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(repo.getMaturitySettlementDate(), calendar.getId()), -1 * daysToSettle);
			repo.setMaturityDate(maturityDate);
		}
	}


	/**
	 * Updates the repo term to match the actual open and maturity settlement date difference. This may be necessary when the original term would have ended on a non-business day,
	 * in which case the maturity settlement will be extended to match the following business day.
	 */
	private void applyNormalizedTerm(LendingRepo repo) {
		if (repo.getType().isMultipleInterestRatesPerPeriod()) {
			// Open/evergreen repos use a static one-day term for the duration of interest rates only
			repo.setTerm(1);
		}
		else if (repo.getSettlementDate() != null && repo.getMaturitySettlementDate() != null) {
			repo.setTerm(DateUtils.getDaysDifference(repo.getMaturitySettlementDate(), repo.getSettlementDate()));
		}
	}


	private void applyUpdatedRepoAmounts(LendingRepo repo) {
		InvestmentNotionalCalculatorTypes calculator = InvestmentCalculatorUtils.getNotionalCalculator(repo.getInvestmentSecurity());
		repo.setMarketValue(calculateRepoMarketValue(repo));
		repo.setNetCash(calculateRepoNetCash(repo, calculator));
		/*
		 * Only set interest amounts for single-rate repos. Single-rate repos can have their interest amounts inferred based on interest rate and term, but multi-rate repos need
		 * interest amounts aggregated for each individual interest rate. These aggregations are performed on interest rate save.
		 */
		if (!repo.getType().isMultipleInterestRatesPerPeriod()) {
			repo.setInterestAmount(calculateRepoInterestAmount(repo));
		}
	}


	private BigDecimal calculateRepoMarketValue(LendingRepo repo) {
		BigDecimal indexRatio = repo.getInvestmentSecurity().getInstrument().getHierarchy().isIndexRatioAdjusted() ? repo.getIndexRatio() : BigDecimal.ONE;
		// both methods handle the logic to skip Index Ratio adjustment based on hierarchy configuration
		BigDecimal notional = getInvestmentCalculatorService().getInvestmentSecurityNotional(repo.getInvestmentSecurity().getId(), repo.getPrice(), repo.getOriginalFace(), indexRatio);
		BigDecimal accruedInterest = getInvestmentCalculatorService().getInvestmentSecurityAccruedInterest(repo.getInvestmentSecurity().getId(), repo.getOriginalFace(), indexRatio, repo.getSettlementDate(), null);
		return MathUtils.add(notional, accruedInterest);
	}


	private BigDecimal calculateRepoNetCash(LendingRepo repo, InvestmentNotionalCalculatorTypes roundingCalculator) {
		// Assumes that the market value has already been calculated
		BigDecimal haircutPercent = MathUtils.divide(repo.getHaircutPercent(), new BigDecimal("100"));
		BigDecimal cashNetOfHaircut = MathUtils.isGreaterThan(haircutPercent, BigDecimal.ONE)
				? MathUtils.divide(repo.getMarketValue(), haircutPercent)
				: MathUtils.multiply(repo.getMarketValue(), haircutPercent);
		return roundingCalculator.round(cashNetOfHaircut);
	}


	/**
	 * Updates interest amounts, but only if a single interest amount exists to avoid re-evaluating all historic interest rates.
	 */
	private BigDecimal calculateRepoInterestAmount(LendingRepo repo) {
		// Guard-clause: An interest rate has not yet been set
		if (repo.getInterestRate() == null) {
			return null;
		}
		// Assumes that the net cash has already been calculated
		int term = ObjectUtils.coalesce(repo.getTerm(), 1); // If no term is defined, temporarily display and store the interest for a single day
		return getInvestmentCalculatorService().getInvestmentInterestForDays(repo.getNetCash(), repo.getInterestRate(), repo.getSettlementDate(), term,
				repo.getRepoDefinition().getDayCountConvention());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void applyInterestRateValues(LendingRepo repo, boolean isFirstSave) {
		// Guard-clause: If no interest rate is assigned then we do not have sufficient information to calculate values
		if (repo.getInterestRate() == null) {
			return;
		}

		if (repo.getType().isMultipleInterestRatesPerPeriod()) {
			// Remove any old interest rates
			if (!isFirstSave) {
				cleanupRepoInterestRates(repo);
			}

			// Set up the first interest rate entry
			LendingRepoInterestRate firstInterestRate = getRepoFirstAndOnlyInterestRate(repo, isFirstSave);
			if (firstInterestRate != null) {
				firstInterestRate.setRepo(repo);
				firstInterestRate.setInterestRate(repo.getInterestRate());
				firstInterestRate.setInterestRateDate(repo.getSettlementDate());
				firstInterestRate.setInterestAmount(calculateRepoInterestAmount(repo));
				if (isFirstSave) {
					// this is an inner save on an insert and we want the workflow manager observer for the initial save to handle the transitions
					WorkflowTransitionUtils.executeWithAutomaticWorkflowTransitionsDisabledAndReturn(() -> getLendingRepoInterestRateService().saveLendingRepoInterestRate(firstInterestRate));
				}
				else {
					getLendingRepoInterestRateService().saveLendingRepoInterestRate(firstInterestRate);
				}
			}
		}
	}


	private LendingRepoInterestRate getRepoFirstAndOnlyInterestRate(LendingRepo repo, boolean isFirstSave) {
		// Guard-clause: Trivial case; first save, so no prior rates may exist
		if (isFirstSave) {
			return new LendingRepoInterestRate();
		}
		LendingRepoInterestRate firstInterestRate = null;
		List<LendingRepoInterestRate> rateList = getLendingRepoInterestRateService().getLendingRepoInterestRateByRepo(repo.getId());
		if (CollectionUtils.isEmpty(rateList)) {
			firstInterestRate = new LendingRepoInterestRate();
		}
		else if (CollectionUtils.getSize(rateList) == 1) {
			firstInterestRate = CollectionUtils.getOnlyElementStrict(rateList);
		}
		else {
			// Multiple interest rates exist; assume that the interest rate entries are steady and do not retrieve a rate for mutation
		}
		return firstInterestRate;
	}


	/**
	 * Removes all attached interest rates which took place before the current repo settlement date.
	 */
	private void cleanupRepoInterestRates(LendingRepo repo) {
		LendingRepoInterestRateSearchForm searchForm = new LendingRepoInterestRateSearchForm();
		searchForm.setRepoId(repo.getId());
		searchForm.setBeforeRateDate(repo.getSettlementDate());
		List<LendingRepoInterestRate> rateList = getLendingRepoInterestRateService().getLendingRepoInterestRateList(searchForm);
		if (!CollectionUtils.isEmpty(rateList)) {
			Function<LendingRepoInterestRate, String> rateToStringFunction = rate -> String.format("(ID: [%d]) [%s] on [%s]", rate.getId(), rate.getInterestRate(), DateUtils.fromDateShort(rate.getInterestRateDate()));
			String rateListString = CollectionUtils.toString(CollectionUtils.getConverted(rateList, rateToStringFunction), 10);
			LogUtils.warn(LendingRepoObserver.class, String.format("Repo interest rates were unexpectedly found for repo [%d] which do not apply after the repo settlement date of [%s]. These interest rates will be deleted. Invalid rate list: %s", repo.getId(), DateUtils.fromDateShort(repo.getSettlementDate()), rateListString));
			getLendingRepoInterestRateService().deleteLendingRepoInterestRateList(rateList);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public InvestmentCalculatorService getInvestmentCalculatorService() {
		return this.investmentCalculatorService;
	}


	public void setInvestmentCalculatorService(InvestmentCalculatorService investmentCalculatorService) {
		this.investmentCalculatorService = investmentCalculatorService;
	}


	public LendingRepoInterestRateService getLendingRepoInterestRateService() {
		return this.lendingRepoInterestRateService;
	}


	public void setLendingRepoInterestRateService(LendingRepoInterestRateService lendingRepoInterestRateService) {
		this.lendingRepoInterestRateService = lendingRepoInterestRateService;
	}


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}
}
