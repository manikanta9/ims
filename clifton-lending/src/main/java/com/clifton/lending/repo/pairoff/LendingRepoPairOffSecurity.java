package com.clifton.lending.repo.pairoff;


import com.clifton.investment.instrument.InvestmentSecurity;


public class LendingRepoPairOffSecurity extends LendingRepoPairOffAccount {

	private InvestmentSecurity investmentSecurity;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return super.getLabel() + (getInvestmentSecurity() != null ? " [" + getInvestmentSecurity().getLabel() + "]" : "");
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}
}
