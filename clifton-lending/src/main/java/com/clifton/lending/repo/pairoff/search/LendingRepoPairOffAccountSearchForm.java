package com.clifton.lending.repo.pairoff.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


public class LendingRepoPairOffAccountSearchForm extends BaseEntitySearchForm {

	@Override
	public boolean isFilterRequired() {
		return true;
	}


	@SearchField
	private Date pairOffDate;

	@SearchField(searchField = "counterparty.id")
	private Integer counterpartyId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.id")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "name", searchFieldPath = "counterparty")
	private String counterpartyName;

	@SearchField(searchField = "name", searchFieldPath = "clientInvestmentAccount")
	private String clientInvestmentAccountName;

	@SearchField(searchField = "name", searchFieldPath = "holdingInvestmentAccount")
	private String holdingInvestmentAccountName;

	@SearchField
	private Integer maturingRepoCount;

	@SearchField
	private Integer openingRepoCount;

	// Custom search field
	private String excludeWorkflowStateName;

	// Custom search field
	private Boolean confirmed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getPairOffDate() {
		return this.pairOffDate;
	}


	public void setPairOffDate(Date pairOffDate) {
		this.pairOffDate = pairOffDate;
	}


	public Integer getCounterpartyId() {
		return this.counterpartyId;
	}


	public void setCounterpartyId(Integer counterpartyId) {
		this.counterpartyId = counterpartyId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public String getCounterpartyName() {
		return this.counterpartyName;
	}


	public void setCounterpartyName(String counterpartyName) {
		this.counterpartyName = counterpartyName;
	}


	public String getClientInvestmentAccountName() {
		return this.clientInvestmentAccountName;
	}


	public void setClientInvestmentAccountName(String clientInvestmentAccountName) {
		this.clientInvestmentAccountName = clientInvestmentAccountName;
	}


	public String getHoldingInvestmentAccountName() {
		return this.holdingInvestmentAccountName;
	}


	public void setHoldingInvestmentAccountName(String holdingInvestmentAccountName) {
		this.holdingInvestmentAccountName = holdingInvestmentAccountName;
	}


	public Integer getMaturingRepoCount() {
		return this.maturingRepoCount;
	}


	public void setMaturingRepoCount(Integer maturingRepoCount) {
		this.maturingRepoCount = maturingRepoCount;
	}


	public Integer getOpeningRepoCount() {
		return this.openingRepoCount;
	}


	public void setOpeningRepoCount(Integer openingRepoCount) {
		this.openingRepoCount = openingRepoCount;
	}


	public String getExcludeWorkflowStateName() {
		return this.excludeWorkflowStateName;
	}


	public void setExcludeWorkflowStateName(String excludeWorkflowStateName) {
		this.excludeWorkflowStateName = excludeWorkflowStateName;
	}


	public Boolean getConfirmed() {
		return this.confirmed;
	}


	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}
}
