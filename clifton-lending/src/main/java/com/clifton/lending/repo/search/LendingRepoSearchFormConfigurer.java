package com.clifton.lending.repo.search;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.pairoff.LendingRepoPairOff;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;


public class LendingRepoSearchFormConfigurer extends WorkflowAwareSearchFormConfigurer {

	public LendingRepoSearchFormConfigurer(BaseEntitySearchForm searchForm, WorkflowDefinitionService workflowDefinitionService) {
		super(searchForm, workflowDefinitionService);
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		LendingRepoSearchForm searchForm = (LendingRepoSearchForm) getSortableSearchForm();

		if (searchForm.getActivityDate() != null) {
			criteria.add(Restrictions.or(Restrictions.eq("tradeDate", searchForm.getActivityDate()), Restrictions.eq("maturitySettlementDate", searchForm.getActivityDate())));
		}
		if (searchForm.getActiveOnDate() != null) {
			criteria.add(Restrictions.le("settlementDate", searchForm.getActiveOnDate()));
			criteria.add(Restrictions.or(Restrictions.gt("maturitySettlementDate", searchForm.getActiveOnDate()), Restrictions.isNull("maturitySettlementDate")));
		}

		// add filters for the confirmation screen
		if (searchForm.getConfirmationDate() != null) {
			// create the trade date criteria
			Criterion tradeDateCriteria = Restrictions.eq("tradeDate", searchForm.getConfirmationDate());

			// create the criteria to include unconfirmed REPO's from other dates
			Criterion previousRepoCriteria = Restrictions.isNull("confirmedDate");

			// add the or clause to get either by trade date or by unconfirmed
			criteria.add(Restrictions.or(previousRepoCriteria, tradeDateCriteria));

			// include only closed REPO's if not already included
			if (!LendingRepoService.REPO_CLOSED_WORKFLOW_STATUS_NAME.equals(searchForm.getWorkflowStatusName())) {
				String repoWorkflowStatusAlias = getPathAlias("workflowStatus", criteria);
				criteria.add(Restrictions.ne(repoWorkflowStatusAlias + ".name", LendingRepoService.REPO_CLOSED_WORKFLOW_STATUS_NAME));
			}
			// exclude cancelled if not already excluded
			if (!LendingRepoService.REPO_CANCELLED_WORKFLOW_STATE_NAME.equals(searchForm.getExcludeWorkflowStateName())) {
				String repoWorkflowStateAlias = getPathAlias("workflowState", criteria);
				criteria.add(Restrictions.ne(repoWorkflowStateAlias + ".name", LendingRepoService.REPO_CANCELLED_WORKFLOW_STATE_NAME));
			}
		}

		if (searchForm.getOpeningUnbookedOrMaturityUnbookedOnDate() != null) {
			Criterion tradeDateCriteria = Restrictions.le("tradeDate", searchForm.getOpeningUnbookedOrMaturityUnbookedOnDate());
			Criterion bookingDateCriteria = Restrictions.isNull("bookingDate");
			Criterion maturityDateCriteria = Restrictions.le("maturityDate", searchForm.getOpeningUnbookedOrMaturityUnbookedOnDate());
			Criterion maturityBookingDateCriteria = Restrictions.isNull("maturityBookingDate");
			criteria.add(Restrictions.or(Restrictions.and(tradeDateCriteria, bookingDateCriteria), Restrictions.and(maturityDateCriteria, maturityBookingDateCriteria)));
		}

		// get REPOs for opening and maturing LendingRepoPairOff with the pair off date.
		if (searchForm.getPairOffDate() != null) {
			// opening repo subquery
			DetachedCriteria openingRepo = DetachedCriteria.forClass(LendingRepoPairOff.class, "op");
			openingRepo.setProjection(Projections.property("openingRepo"));
			openingRepo.add(Property.forName("pairOffDate").eq(searchForm.getPairOffDate()));

			// maturity repo subquery
			DetachedCriteria maturingRepo = DetachedCriteria.forClass(LendingRepoPairOff.class, "mr");
			maturingRepo.setProjection(Projections.property("maturingRepo"));
			maturingRepo.add(Property.forName("pairOffDate").eq(searchForm.getPairOffDate()));

			// repo in opening or maturing
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Property.forName("id").in(openingRepo));
			disjunction.add(Property.forName("id").in(maturingRepo));
			criteria.add(disjunction);
		}
	}
}
