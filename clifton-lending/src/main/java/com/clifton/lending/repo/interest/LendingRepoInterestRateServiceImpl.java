package com.clifton.lending.repo.interest;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.interest.search.LendingRepoInterestRateSearchForm;
import com.clifton.lending.repo.search.LendingRepoSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class LendingRepoInterestRateServiceImpl implements LendingRepoInterestRateService {

	private AdvancedUpdatableDAO<LendingRepoInterestRate, Criteria> lendingRepoInterestRateDAO;

	private DaoSingleKeyListCache<LendingRepoInterestRate, Integer> lendingRepoInterestRateListByRepoCache;

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;
	private InvestmentCalculatorService investmentCalculatorService;

	// NOTE: Circular dependency - need to look up REPO's to build interest rates
	private LendingRepoService lendingRepoService;


	////////////////////////////////////////////////////////////////////////////
	////////       LendingRepoInterestRate Business Methods        /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LendingRepoInterestRate getLendingRepoInterestRate(int id) {
		return getLendingRepoInterestRateDAO().findByPrimaryKey(id);
	}


	@Override
	public LendingRepoInterestRate getLendingRepoInterestRateByRepoAndDate(int repoId, Date interestRateDate) {
		return CollectionUtils.getOnlyElement(BeanUtils.filter(getLendingRepoInterestRateByRepo(repoId), lendingRepoInterestRate -> DateUtils.isEqualWithoutTime(interestRateDate, lendingRepoInterestRate.getInterestRateDate())));
	}


	@Override
	public List<LendingRepoInterestRate> getLendingRepoInterestRateByRepo(int repoId) {
		return getLendingRepoInterestRateListByRepoCache().getBeanListForKeyValue(getLendingRepoInterestRateDAO(), repoId);
	}


	@Override
	public List<LendingRepoInterestRate> getLendingRepoInterestRateList(LendingRepoInterestRateSearchForm searchForm) {
		return getLendingRepoInterestRateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public void deleteLendingRepoInterestRateList(List<LendingRepoInterestRate> rateList) {
		getLendingRepoInterestRateDAO().deleteList(rateList);
	}


	@Override
	@Transactional
	public void deleteLendingRepoInterestRate(int id) {
		LendingRepoInterestRate rate = getLendingRepoInterestRate(id);

		getLendingRepoInterestRateDAO().delete(id);

		// remove the interest from the REPO field, using lookup of interest rate list in case the original value in repo.interestAmount is incorrect.
		List<LendingRepoInterestRate> rateList = getLendingRepoInterestRateByRepo(rate.getRepo().getId());
		BigDecimal interestTotal =  CoreMathUtils.sumProperty(rateList, LendingRepoInterestRate::getInterestAmount);
		saveLendingRepoWithUpdatedInterestAmount(rate.getRepo(), interestTotal);
	}


	@Override
	@Transactional
	public LendingRepoInterestRate saveLendingRepoInterestRate(LendingRepoInterestRate bean) {
		ValidationUtils.assertNull(bean.getRepo().getMaturityBookingDate(), "Cannot modify interest on a repo that has been fully booked.");
		ValidationUtils.assertTrue(bean.getInterestRate() == null || (bean.getInterestRate() != null && bean.getInterestAmount() != null), "Interest amount is required when an interest rate exists.");

		Calendar calendar = getCalendarSetupService().getCalendarByName(LendingRepoService.LENDING_REPO_CALENDAR_NAME);
		Date lastRateDate = bean.getRepo().getMaturitySettlementDate() == null ? null : getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(bean.getRepo().getMaturitySettlementDate(), calendar.getId()));
		// validate that the rate date is a business day
		ValidationUtils.assertTrue(getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(bean.getInterestRateDate(), calendar.getId())),
				"Cannot enter an interest rate on [" + DateUtils.fromDate(bean.getInterestRateDate()) + "] because it is not a business day for REPO calendar ["
						+ LendingRepoService.LENDING_REPO_CALENDAR_NAME + "].");
		// validate that the rate date is with in the REPO's settlement and maturity settlement dates
		ValidationUtils.assertTrue((DateUtils.compare(bean.getInterestRateDate(), bean.getRepo().getSettlementDate(), false) >= 0)
						&& ((bean.getRepo().getMaturitySettlementDate() == null) || DateUtils.compare(bean.getInterestRateDate(), lastRateDate, false) <= 0),
				"Cannot save interest rate on [" + DateUtils.fromDate(bean.getInterestRateDate(), DateUtils.DATE_FORMAT_INPUT) + "] because the REPO is not active on that date.");

		bean = getLendingRepoInterestRateDAO().save(bean);

		List<LendingRepoInterestRate> rateList = getLendingRepoInterestRateByRepo(bean.getRepo().getId());
		BigDecimal interestTotal =  CoreMathUtils.sumProperty(rateList, LendingRepoInterestRate::getInterestAmount);
		LendingRepo savedLendingRepo = saveLendingRepoWithUpdatedInterestAmount(bean.getRepo(), interestTotal);
		bean.setRepo(savedLendingRepo);
		return bean;
	}


	@Override
	@Transactional
	public void buildLendingRepoInterestRate(LendingRepoInterestRateDate rateCounterPartyList) {
		for (LendingRepoInterestRateCounterParty cp : CollectionUtils.getIterable(rateCounterPartyList.getCounterPartyRateList())) {
			if (cp.getInterestRate() != null) {
				doBuildLendingRepoInterestRateForDate(rateCounterPartyList.getInterestRateDate(), cp);
			}
		}
	}


	@Override
	public LendingRepoInterestRateDate getLendingRepoInterestRateDateForDate(Date rateDate) {
		LendingRepoInterestRateDate result = new LendingRepoInterestRateDate();
		result.setInterestRateDate(rateDate);

		LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
		searchForm.setMultipleInterestRatesPerPeriod(true);
		searchForm.setActiveOnDate(rateDate);
		searchForm.setOpeningBooked(true);
		searchForm.setMaturityBooked(false);

		Set<String> addedCompaniesAndType = new HashSet<>();
		List<LendingRepo> repoList = getLendingRepoService().getLendingRepoList(searchForm);
		for (LendingRepo repo : CollectionUtils.getIterable(repoList)) {
			String key = repo.getRepoDefinition().getCounterparty().getId() + "_" + repo.getType().getId() + "_" + repo.getCancellationNoticeTerm();
			if (!addedCompaniesAndType.contains(key)) {
				LendingRepoInterestRateCounterParty cp = new LendingRepoInterestRateCounterParty();
				cp.setCounterparty(repo.getRepoDefinition().getCounterparty());
				cp.setRepoType(repo.getType());
				cp.setCancellationNoticeTerm(repo.getCancellationNoticeTerm());
				result.getCounterPartyRateList().add(cp);
				addedCompaniesAndType.add(key);
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private LendingRepo saveLendingRepoWithUpdatedInterestAmount(LendingRepo lendingRepo, BigDecimal interestAmount) {
		if (MathUtils.isNotEqual(lendingRepo.getInterestAmount(), interestAmount)) {
			// Only save the Lending Repo if the Interest Amount has changed.
			lendingRepo.setInterestAmount(interestAmount);
			return getLendingRepoService().saveLendingRepoInternal(lendingRepo);
		}
		return lendingRepo;
	}


	private void doBuildLendingRepoInterestRateForDate(Date interestRateDate, LendingRepoInterestRateCounterParty cp) {
		Calendar calendar = getCalendarSetupService().getCalendarByName(LendingRepoService.LENDING_REPO_CALENDAR_NAME);
		ValidationUtils.assertTrue(getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(interestRateDate, calendar.getId())), "Cannot enter interest rates on [" + DateUtils.fromDate(interestRateDate)
				+ "] because it is not a business day for REPO calendar [" + LendingRepoService.LENDING_REPO_CALENDAR_NAME + "].");

		LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
		searchForm.setMultipleInterestRatesPerPeriod(true);
		searchForm.setActiveOnDate(interestRateDate);
		searchForm.setOpeningBooked(true);
		searchForm.setMaturityBooked(false);
		searchForm.setCounterpartyId(cp.getCounterparty().getId());
		searchForm.setTypeId(cp.getRepoType().getId());
		if (MathUtils.isNullOrZero(cp.getCancellationNoticeTerm())) {
			searchForm.setCancellationNoticeTermIsNull(true);
		}
		else {
			searchForm.setCancellationNoticeTerm(cp.getCancellationNoticeTerm());
		}

		StringBuilder errors = null;
		Throwable firstCause = null;

		List<LendingRepo> repoList = getLendingRepoService().getLendingRepoList(searchForm);
		for (LendingRepo repo : CollectionUtils.getIterable(repoList)) {
			try {
				LendingRepoInterestRateSearchForm rateSearchForm = new LendingRepoInterestRateSearchForm();
				rateSearchForm.setRepoId(repo.getId());
				rateSearchForm.setInterestRateDate(interestRateDate);

				LendingRepoInterestRate rate = CollectionUtils.getOnlyElement(getLendingRepoInterestRateList(rateSearchForm));
				if (rate == null) {
					rate = new LendingRepoInterestRate();
					rate.setInterestRateDate(interestRateDate);
					rate.setRepo(repo);
					rate.setInterestRate(cp.getInterestRate());
					rate.setInterestAmount(calculateInterest(rate, calendar));
					saveLendingRepoInterestRate(rate);
				}
			}
			catch (Throwable e) {
				// full log for first error and only causes for consecutive errors
				boolean firstError = false;
				if (errors == null) {
					firstCause = e;
					firstError = true;
					errors = new StringBuilder(1024);
				}
				else {
					errors.append("\n\n");
				}
				errors.append("Failed to create interest rate for REPO [").append(repo.getId()).append("]");
				if (!firstError) {
					errors.append("\nCAUSED BY ");
					errors.append(ExceptionUtils.getDetailedMessage(e));
				}
			}
		}

		if (errors != null) {
			throw new RuntimeException(errors.toString(), firstCause);
		}
	}


	private BigDecimal calculateInterest(LendingRepoInterestRate rate, Calendar calendar) {
		Date endDate = getCalendarBusinessDayService().getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(rate.getInterestRateDate(), calendar.getId()), rate.getRepo().getTerm());
		int daysOfInterest = DateUtils.getDaysDifference(endDate, rate.getInterestRateDate());

		return getInvestmentCalculatorService().getInvestmentInterestForDays(rate.getRepo().getNetCash(), rate.getInterestRate(), rate.getInterestRateDate(), daysOfInterest,
				rate.getRepo().getRepoDefinition().getDayCountConvention());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<LendingRepoInterestRate, Criteria> getLendingRepoInterestRateDAO() {
		return this.lendingRepoInterestRateDAO;
	}


	public void setLendingRepoInterestRateDAO(AdvancedUpdatableDAO<LendingRepoInterestRate, Criteria> lendingRepoInterestRateDAO) {
		this.lendingRepoInterestRateDAO = lendingRepoInterestRateDAO;
	}


	public DaoSingleKeyListCache<LendingRepoInterestRate, Integer> getLendingRepoInterestRateListByRepoCache() {
		return this.lendingRepoInterestRateListByRepoCache;
	}


	public void setLendingRepoInterestRateListByRepoCache(DaoSingleKeyListCache<LendingRepoInterestRate, Integer> lendingRepoInterestRateListByRepoCache) {
		this.lendingRepoInterestRateListByRepoCache = lendingRepoInterestRateListByRepoCache;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}


	public InvestmentCalculatorService getInvestmentCalculatorService() {
		return this.investmentCalculatorService;
	}


	public void setInvestmentCalculatorService(InvestmentCalculatorService investmentCalculatorService) {
		this.investmentCalculatorService = investmentCalculatorService;
	}
}
