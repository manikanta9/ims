package com.clifton.lending.repo.upload;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.lending.repo.LendingRepo;


/**
 * The {@link LendingRepoUploadService} type declares methods for uploading {@link LendingRepo} entities.
 *
 * @author mikeh
 */
public interface LendingRepoUploadService {

	@SecureMethod(dtoClass = LendingRepo.class)
	public void uploadLendingRepoRollUploadFile(LendingRepoRollUploadCommand command);
}
