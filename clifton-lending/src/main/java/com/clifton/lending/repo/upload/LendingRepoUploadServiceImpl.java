package com.clifton.lending.repo.upload;

import com.clifton.accounting.gl.position.AccountingPositionServiceUtils;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.file.upload.FileUploadCommand;
import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoRollTransitionCommand;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.LendingRepoType;
import com.clifton.lending.repo.search.LendingRepoSearchForm;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;


/**
 * The {@link LendingRepoUploadServiceImpl} implements {@link LendingRepo} upload methods.
 *
 * @author mikeh
 */
@Service
public class LendingRepoUploadServiceImpl implements LendingRepoUploadService {

	private static final String STATUS_DISCOVERED_ROW = "Repo Roll Discovered Row";
	private static final String STATUS_UPLOAD_SUCCESS = "Repo Roll Upload Success";
	private static final String STATUS_RESULT_MESSAGE = "Repo Roll Result Message";

	private FileUploadHandler fileUploadHandler;
	private LendingRepoService lendingRepoService;
	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadLendingRepoRollUploadFile(LendingRepoRollUploadCommand command) {
		command.clearResults();
		if (command.isPositionRetrievalOptionRecompile()) {
			AccountingPositionServiceUtils.executeWithOptionRecompile(() -> doUploadLendingRepoRollUploadFile(command));
		}
		else {
			doUploadLendingRepoRollUploadFile(command);
		}
	}


	private void doUploadLendingRepoRollUploadFile(LendingRepoRollUploadCommand command) {
		Status resultStatus = processLendingRepoRollUpload(command);
		processRepoRollResultStatus(command, command.isPartialUploadAllowed(), resultStatus);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Processes the given <code>command</code> for {@link LendingRepo} roll uploads and returns the result of the upload in the given {@link Status} object.
	 */
	private Status processLendingRepoRollUpload(LendingRepoRollUploadCommand command) {
		Status status = Status.ofEmptyMessage();
		Map<LendingRepo, LendingRepoRollUploadTemplate> repoToTemplateMap = getLendingRepoRollUploadRepoToTemplateMap(command, status);

		// Early exit: Handle errors
		if (repoToTemplateMap.isEmpty() || (status.getErrorCount() > 0 && !command.isPartialUploadAllowed())) {
			return status;
		}

		try {
			// Execute rolls
			executeLendingRepoRollUploadRepoToTemplateMap(repoToTemplateMap, command.isPartialUploadAllowed(), status);
		}
		catch (Exception e) {
			// Handle rollback
			status.addDetail(STATUS_RESULT_MESSAGE, e.getMessage());
			status.clearDetailListForCategory(STATUS_UPLOAD_SUCCESS);
		}
		return status;
	}


	/**
	 * Discovers all {@link LendingRepo} entities matching the provided <code>command</code> and returns the resulting map of {@link LendingRepo} entities to {@link
	 * LendingRepoRollUploadTemplate} objects.
	 * <p>
	 * Invalid elements in the <code>command</code> will be tracked in the given <code>status</code> object and excluded from the result.
	 */
	private Map<LendingRepo, LendingRepoRollUploadTemplate> getLendingRepoRollUploadRepoToTemplateMap(LendingRepoRollUploadCommand command, Status status) {
		Map<LendingRepo, LendingRepoRollUploadTemplate> actualRepoToTemplateMap = new HashMap<>();
		Map<LendingRepo, LendingRepoRollUploadTemplate> discoveredRepoToTemplateMap = new HashMap<>(); // Map used for tracking discovered values for validation
		Map<String, List<LendingRepo>> clientHoldingAccountActiveRepoListMap = new HashMap<>(); // Map used to look up and store active repos for an account and then use that to filter for each repo in the file

		List<LendingRepoRollUploadTemplate> rollUploadTemplateList = getFileUploadHandler().convertFileUploadFileToBeanList(command);
		for (LendingRepoRollUploadTemplate rollUploadTemplate : rollUploadTemplateList) {
			// Track discovered row
			status.addDetail(STATUS_DISCOVERED_ROW, "Found row: " + rollUploadTemplate.getSourceDescription() + ", " + rollUploadTemplate.getTargetDescription());
			// Validate template
			if (!isLendingRepoRollUploadTemplateValid(rollUploadTemplate, status)) {
				continue;
			}
			// Find matching repo
			LendingRepo originalRepo = getExistingLendingRepoFromRollTemplate(clientHoldingAccountActiveRepoListMap, rollUploadTemplate, status);
			if (originalRepo == null) {
				continue;
			}
			// Validate that the repo has not already been matched
			if (discoveredRepoToTemplateMap.containsKey(originalRepo)) {
				actualRepoToTemplateMap.remove(originalRepo);
				LendingRepoRollUploadTemplate existingUploadTemplate = discoveredRepoToTemplateMap.get(originalRepo);
				status.addError(String.format("Unable to process lines [%s] and [%s]. These lines matched the same active repo (ID: [%d]).", existingUploadTemplate.getSourceDescription(), rollUploadTemplate.getSourceDescription(), originalRepo.getId()));
				continue;
			}
			discoveredRepoToTemplateMap.put(originalRepo, rollUploadTemplate);
			// Add mapping entry for repo to template
			actualRepoToTemplateMap.put(originalRepo, rollUploadTemplate);
		}
		return actualRepoToTemplateMap;
	}


	/**
	 * Executes rolls using the given one-to-one map. If any failures occur and <code>partialUploadAllowed</code> is <code>false</code> then the entire result will be rolled back.
	 * <p>
	 * Failed rolls will be tracked in the given <code>status</code> object.
	 */
	@Transactional
	protected void executeLendingRepoRollUploadRepoToTemplateMap(Map<LendingRepo, LendingRepoRollUploadTemplate> repoToTemplateMap, boolean partialUploadAllowed, Status status) {
		// Guard-clause: Empty source
		if (repoToTemplateMap.isEmpty()) {
			return;
		}
		AssertUtils.assertTrue(MapUtils.isOneToOne(repoToTemplateMap), "Illegal state: The repo-to-template map is not one-to-one. Map: [%s]", repoToTemplateMap);

		// Get relevant workflow states for transitions
		LendingRepo firstRepo = CollectionUtils.getFirstElementStrict(repoToTemplateMap.keySet());
		WorkflowState repoWorkflow = firstRepo.getWorkflowState();
		WorkflowState repoRollWorkflowState = getWorkflowDefinitionService().getWorkflowStateByName(repoWorkflow.getWorkflow().getId(), LendingRepoService.REPO_ROLL_WORKFLOW_STATE_NAME);

		// Attempt to roll all repos; record all failures
		for (Map.Entry<LendingRepo, LendingRepoRollUploadTemplate> repoToTemplateEntry : repoToTemplateMap.entrySet()) {
			LendingRepo repo = repoToTemplateEntry.getKey();
			LendingRepoRollUploadTemplate rollUploadTemplate = repoToTemplateEntry.getValue();
			try {
				// Execute roll, update, and validate
				LendingRepo newRepo = executeLendingRepoRoll(repo, rollUploadTemplate, repoRollWorkflowState);

				// Track successful row
				status.addDetail(STATUS_UPLOAD_SUCCESS, String.format("Repo for account [%s] and security [%s] (ID: [%d]) rolled to new price [%s], rate [%s], and maturity [%s] (ID: [%d]).", repo.getClientInvestmentAccount().getNumber(), repo.getInvestmentSecurity().getSymbol(), repo.getId(), CoreMathUtils.formatNumberMoney(newRepo.getPrice()), CoreMathUtils.formatNumberDecimal(newRepo.getInterestRate()), DateUtils.fromDateShort(newRepo.getMaturitySettlementDate()), newRepo.getId()));
			}
			catch (Exception e) {
				String errorMessage = String.format("Unable to process repo ID [%d] and line [%s]: %s", repo.getId(), rollUploadTemplate.getSourceDescription(), ExceptionUtils.getOriginalMessage(e));
				LogUtils.errorOrInfo(LendingRepoUploadServiceImpl.class, errorMessage, e);
				status.addError(errorMessage);
			}
		}

		// Rollback all results if any operations failed
		if (status.getErrorCount() > 0 && !partialUploadAllowed) {
			throw new RuntimeException("Errors occurred while processing repos and partial uploads are not allowed. Operation cancelled.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private LendingRepo executeLendingRepoRoll(LendingRepo repo, LendingRepoRollUploadTemplate rollUploadTemplate, WorkflowState repoRollWorkflowState) {
		WorkflowTransition rollTransition = getWorkflowTransitionService().getWorkflowTransitionForEntity(repo, repo.getWorkflowState(), repoRollWorkflowState);
		AssertUtils.assertNotNull(rollTransition, "No transition was found from repo state [%s] to repo state [%s].", repo.getWorkflowState() != null ? repo.getWorkflowState().getName() : null, repoRollWorkflowState != null ? repoRollWorkflowState.getName() : null);
		LendingRepoRollTransitionCommand rollCommand = new LendingRepoRollTransitionCommand();
		rollCommand.setRepoId(repo.getId());
		rollCommand.setPrice(rollUploadTemplate.getNewCleanPrice());
		rollCommand.setInterestRate(rollUploadTemplate.getNewInterestRate());
		rollCommand.setMaturitySettlementDate(getNewRepoMaturitySettlementDate(repo, rollUploadTemplate.getTerm(), rollUploadTemplate.getMaturityDate()));
		return getLendingRepoService().executeLendingRepoRoll(rollCommand);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns <code>true</code> if the given {@link LendingRepoRollUploadTemplate} is valid, or <code>false</code> otherwise. If the <code>rollUploadTemplate</code> is invalid
	 * then the reason will be tracked in the given <code>status</code> object.
	 */
	private boolean isLendingRepoRollUploadTemplateValid(LendingRepoRollUploadTemplate rollUploadTemplate, Status status) {
		// Compile validation errors
		List<String> errorList = new ArrayList<>();
		List<String> missingFieldList = new ArrayList<>();
		if (StringUtils.isEmpty(rollUploadTemplate.getRepoTypeName())) {
			missingFieldList.add("Repo Type Name");
		}
		if (StringUtils.isEmpty(rollUploadTemplate.getClientAccountNumber())) {
			missingFieldList.add("Client Account Number");
		}
		if (StringUtils.isEmpty(rollUploadTemplate.getClientAccountNumber())) {
			missingFieldList.add("Counterparty Name");
		}
		if (StringUtils.isEmpty(rollUploadTemplate.getCusip())) {
			missingFieldList.add("CUSIP");
		}
		if (rollUploadTemplate.getOriginalFace() == null) {
			missingFieldList.add("Original Face");
		}
		if (rollUploadTemplate.getOldRepoTradeDate() == null) {
			missingFieldList.add("Old Repo Trade Date");
		}
		if (rollUploadTemplate.getNewCleanPrice() == null) {
			missingFieldList.add("New Clean Price");
		}
		if (rollUploadTemplate.getNewInterestRate() == null) {
			missingFieldList.add("New Interest Rate");
		}

		if (!StringUtils.isEmpty(rollUploadTemplate.getRepoTypeName())) {
			LendingRepoType repoType = getLendingRepoService().getLendingRepoTypeByName(rollUploadTemplate.getRepoTypeName());
			// Conditionally required term/maturity date values
			if (repoType != null && repoType.isMaturityDateRequiredForOpen() && repoType.getRequiredTerm() == null
					&& (rollUploadTemplate.getTerm() == null && rollUploadTemplate.getMaturityDate() == null)) {
				missingFieldList.add(String.format("Term or Maturity Date (required for [%s] repos)", repoType.getName()));
			}
		}

		// Process validation status
		if (!missingFieldList.isEmpty()) {
			errorList.add(String.format("The following required fields were missing: %s.", CollectionUtils.toString(missingFieldList, 5)));
		}
		final boolean valid = errorList.isEmpty();
		if (!valid) {
			status.addError(String.format("Unable to process line [%s]. %s", rollUploadTemplate.getSourceDescription(), StringUtils.join(errorList, " ")));
		}
		return valid;
	}


	/**
	 * Gets the singular {@link LendingRepo} for the given {@link LendingRepoRollUploadTemplate}. If a single repo cannot be found, then the reason will be tracked in the given
	 * <code>status</code> object.
	 */
	private LendingRepo getExistingLendingRepoFromRollTemplate(Map<String, List<LendingRepo>> clientHoldingAccountActiveRepoListMap, LendingRepoRollUploadTemplate rollUploadTemplate, Status status) {
		// Query matching repos
		String key = StringUtils.generateKey(rollUploadTemplate.getClientAccountNumber(), rollUploadTemplate.getHoldingAccountNumber());
		clientHoldingAccountActiveRepoListMap.computeIfAbsent(key, k -> {
			LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
			searchForm.setWorkflowStateName(LendingRepoService.REPO_ACTIVE_WORKFLOW_STATE_NAME);
			searchForm.setClientInvestmentAccountNumber(rollUploadTemplate.getClientAccountNumber());
			searchForm.setHoldingInvestmentAccountNumber(rollUploadTemplate.getHoldingAccountNumber());
			return getLendingRepoService().getLendingRepoList(searchForm);
		});

		List<LendingRepo> lendingRepoList = clientHoldingAccountActiveRepoListMap.get(key).stream().filter(repo -> {
			if (!StringUtils.isEqualIgnoreCase(rollUploadTemplate.getRepoTypeName(), repo.getType().getName())) {
				return false;
			}
			if (!StringUtils.isEqualIgnoreCase(rollUploadTemplate.getCounterpartyName(), repo.getRepoDefinition().getCounterparty().getName())) {
				return false;
			}
			if (repo.getRepoDefinition().isReverse() && !BooleanUtils.isTrue(rollUploadTemplate.getReverseRepo())) {
				return false;
			}
			if (!StringUtils.isEqualIgnoreCase(rollUploadTemplate.getCusip(), repo.getInvestmentSecurity().getCusip())) {
				return false;
			}
			if (!MathUtils.isEqual(rollUploadTemplate.getOriginalFace(), repo.getOriginalFace())) {
				return false;
			}
			if (!DateUtils.isEqualWithoutTime(rollUploadTemplate.getOldRepoTradeDate(), repo.getTradeDate())) {
				return false;
			}
			return true;
		}).collect(Collectors.toList());

		// Process results to single repo
		final LendingRepo repo;
		if (CollectionUtils.isEmpty(lendingRepoList)) {
			status.addError(String.format("Unable to process line [%s]. No active repos were found that match this line. Please check the values for this line to ensure that they match an existing active repo.", rollUploadTemplate.getSourceDescription()));
			repo = null;
		}
		else if (lendingRepoList.size() > 1) {
			List<Integer> matchedRepoIdList = CollectionUtils.getConverted(lendingRepoList, BaseSimpleEntity::getId);
			status.addError(String.format("Unable to process line [%s]. More than one matching active repo was found. Please make sure that the values for this line uniquely match a single existing active repo. Matched repo IDs: %s", rollUploadTemplate.getSourceDescription(), CollectionUtils.toString(matchedRepoIdList, 5)));
			repo = null;
		}
		else {
			repo = CollectionUtils.getOnlyElementStrict(lendingRepoList);
		}
		return repo;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Date getNewRepoMaturitySettlementDate(LendingRepo originalRepo, Integer term, Date maturityDate) {
		Date settlementDate = originalRepo.getMaturitySettlementDate();
		Date termDate = term != null ? DateUtils.addDays(settlementDate, term) : null;
		ValidationUtils.assertTrue(termDate == null || maturityDate == null || DateUtils.isEqual(termDate, maturityDate), () -> String.format("The repo term length does not produce a date equal to the given maturity date. Term: [%d]. Resulting term date: [%s]. Maturity date: [%s].", term, DateUtils.fromDateShort(termDate), DateUtils.fromDateShort(maturityDate)));
		return ObjectUtils.coalesce(termDate, maturityDate);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Processes the result of the repo upload, attaching the results to the command object.
	 */
	private void processRepoRollResultStatus(FileUploadCommand<?> command, boolean partialUploadAllowed, Status resultStatus) {
		// Get counts
		int totalCount = resultStatus.getDetailListForCategory(STATUS_DISCOVERED_ROW).size();
		int successCount = resultStatus.getDetailListForCategory(STATUS_UPLOAD_SUCCESS).size();
		int resultMessageCount = resultStatus.getDetailListForCategory(STATUS_RESULT_MESSAGE).size();
		int errorCount = resultStatus.getErrorCount();

		// Generate summary message
		StringJoiner messageSj = new StringJoiner(" ");
		if (errorCount == 0) {
			messageSj.add("Success.");
		}
		else {
			messageSj.add("Errors occurred while processing.");
			messageSj.add(partialUploadAllowed ? "Failed repo actions have been ignored." : "No repo actions have been executed.");
		}
		messageSj.add(String.format("[%d] of [%d] repo actions completed.", successCount, totalCount));
		if (errorCount > 0) {
			messageSj.add(String.format("[%d] errors occurred.", errorCount));
		}
		command.appendResult(messageSj.toString());

		// Append high-level result messages
		if (resultMessageCount > 0) {
			command.appendResult(StringUtils.EMPTY_STRING); // Blank line
			resultStatus.getDetailListForCategory(STATUS_RESULT_MESSAGE).stream()
					.map(StatusDetail::getNote)
					.forEach(command::appendResult);
		}
		// Append error details
		if (errorCount > 0) {
			command.appendResult(StringUtils.NEW_LINE + "Errors:");
			// Add extra newline between details for readability since error messages may be long
			String aggregateErrorMessage = resultStatus.getErrorList().stream()
					.map(StatusDetail::getNote)
					.collect(Collectors.joining(StringUtils.NEW_LINE + StringUtils.NEW_LINE + "- ", "- ", StringUtils.EMPTY_STRING));
			command.appendResult(aggregateErrorMessage);
		}
		// Append success details
		if (successCount > 0) {
			command.appendResult(StringUtils.NEW_LINE + "Actions:");
			resultStatus.getDetailListForCategory(STATUS_UPLOAD_SUCCESS).stream()
					.map(StatusDetail::getNote)
					.forEach(successNote -> command.appendResult("- " + successNote));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FileUploadHandler getFileUploadHandler() {
		return this.fileUploadHandler;
	}


	public void setFileUploadHandler(FileUploadHandler fileUploadHandler) {
		this.fileUploadHandler = fileUploadHandler;
	}


	public LendingRepoService getLendingRepoService() {
		return this.lendingRepoService;
	}


	public void setLendingRepoService(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
