package com.clifton.lending.repo.upload;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.lending.repo.LendingRepo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.StringJoiner;


/**
 * The {@link LendingRepoRollUploadTemplate} represents a single row for a {@link LendingRepo} roll upload action. This object encapsulates the information necessary to find an
 * existing rollable-{@link LendingRepo} and to roll it to a new repo using new values.
 *
 * @author mikeh
 * @see LendingRepoRollUploadCommand
 * @see LendingRepoUploadService
 */
public class LendingRepoRollUploadTemplate {

	// Fields to identify existing repo
	private String repoTypeName;
	private String clientAccountNumber;
	private String holdingAccountNumber;
	private String counterpartyName;
	private Boolean reverseRepo;
	private String cusip;
	private BigDecimal originalFace;
	private Date oldRepoTradeDate;

	// Fields for rolled repo
	private BigDecimal newCleanPrice;
	private BigDecimal newInterestRate;
	private Integer term;
	private Date maturityDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSourceDescription() {
		StringJoiner sj = new StringJoiner(", ");
		sj.add("Repo Type Name='" + getRepoTypeName() + '\'');
		sj.add("Client Account Number='" + getClientAccountNumber() + '\'');
		if (getHoldingAccountNumber() != null) {
			sj.add("Holding Account Number='" + getHoldingAccountNumber() + '\'');
		}
		sj.add("Counterparty Name='" + getCounterpartyName() + '\'');
		if (getReverseRepo() != null) {
			sj.add("Reverse Repo=" + getReverseRepo());
		}
		sj.add("CUSIP='" + getCusip() + '\'');
		sj.add("Original Face=" + CoreMathUtils.formatNumberMoney(getOriginalFace()));
		sj.add("Old Repo Trade Date=" + DateUtils.fromDateShort(getOldRepoTradeDate()));
		return sj.toString();
	}


	public String getTargetDescription() {
		StringJoiner sj = new StringJoiner(", ");
		sj.add("New Clean Price=" + CoreMathUtils.formatNumberMoney(getNewCleanPrice()));
		sj.add("New Interest Rate=" + CoreMathUtils.formatNumberDecimal(getNewInterestRate()));
		if (getTerm() != null) {
			sj.add("Term=" + getTerm());
		}
		if (getMaturityDate() != null) {
			sj.add("Maturity Date=" + DateUtils.fromDateShort(getMaturityDate()));
		}
		return sj.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRepoTypeName() {
		return this.repoTypeName;
	}


	public void setRepoTypeName(String repoTypeName) {
		this.repoTypeName = repoTypeName;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public String getHoldingAccountNumber() {
		return this.holdingAccountNumber;
	}


	public void setHoldingAccountNumber(String holdingAccountNumber) {
		this.holdingAccountNumber = holdingAccountNumber;
	}


	public String getCounterpartyName() {
		return this.counterpartyName;
	}


	public void setCounterpartyName(String counterpartyName) {
		this.counterpartyName = counterpartyName;
	}


	public Boolean getReverseRepo() {
		return this.reverseRepo;
	}


	public void setReverseRepo(Boolean reverseRepo) {
		this.reverseRepo = reverseRepo;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public Date getOldRepoTradeDate() {
		return this.oldRepoTradeDate;
	}


	public void setOldRepoTradeDate(Date oldRepoTradeDate) {
		this.oldRepoTradeDate = oldRepoTradeDate;
	}


	public BigDecimal getNewCleanPrice() {
		return this.newCleanPrice;
	}


	public void setNewCleanPrice(BigDecimal newCleanPrice) {
		this.newCleanPrice = newCleanPrice;
	}


	public BigDecimal getNewInterestRate() {
		return this.newInterestRate;
	}


	public void setNewInterestRate(BigDecimal newInterestRate) {
		this.newInterestRate = newInterestRate;
	}


	public Integer getTerm() {
		return this.term;
	}


	public void setTerm(Integer term) {
		this.term = term;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}
}
