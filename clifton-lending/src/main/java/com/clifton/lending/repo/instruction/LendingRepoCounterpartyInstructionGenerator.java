package com.clifton.lending.repo.instruction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionCancellationHandler;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.beans.RepoRateTypes;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.DeliverAgainstPayment;
import com.clifton.instruction.messages.trade.ReceiveAgainstPayment;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.instruction.messages.transfers.DeliverFreeAgainstPayment;
import com.clifton.instruction.messages.transfers.ReceiveFreeAgainstPayment;
import com.clifton.instruction.messages.transfers.SettlementTransactionTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoDefinition;
import com.clifton.lending.repo.LendingRepoType;
import com.clifton.lending.repo.pairoff.LendingRepoPairOff;
import com.clifton.lending.repo.pairoff.LendingRepoPairOffService;
import com.clifton.lending.repo.pairoff.search.LendingRepoPairOffSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


/**
 * <code>LendingRepoCounterpartyInstructionGenerator</code>
 */
@Component
public class LendingRepoCounterpartyInstructionGenerator<I extends Instruction> implements InstructionGenerator<InstructionMessage, LendingRepo, InstructionDeliveryFieldCommand> {

	public static final String FEDERAL_RESERVE_BANK = "FRNYUS33";
	public static final String BANK_OF_NEW_YORK_MELLON = "Bank of New York Mellon";
	private static final String COMPANY_TYPE = "Custodian";

	private DaoLocator daoLocator;
	private InstructionCancellationHandler instructionCancellationHandler;
	private InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler;
	private BusinessCompanyUtilHandler businessCompanyUtilHandler;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private LendingRepoPairOffService lendingRepoPairOffService;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;
	private BusinessCompanyService businessCompanyService;
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public Class<LendingRepo> getSupportedMessageClass() {
		return LendingRepo.class;
	}


	@Override
	public boolean isCancellationSupported() {
		return true;
	}


	@Override
	public List<InstructionMessage> generateInstructionCancelMessageList(Instruction instruction) {
		return getInstructionCancellationHandler().generateCancellationMessageList(instruction);
	}


	@Override
	public List<InstructionMessage> generateInstructionMessageList(Instruction instruction) {
		LendingRepo lendingRepo = getIdentityObject(instruction);
		AssertUtils.assertNotNull(lendingRepo, "Lending Repo could not be found for instruction [%s]", instruction.getIdentity());

		boolean isMaturity = isMaturity(lendingRepo, getInstructionDate(instruction));
		AbstractTradeMessage result = getInstructionMessage(lendingRepo, isMaturity);
		List<InstructionMessage> resultList = new ArrayList<>();
		resultList.add(result);

		result.setRepurchaseAgreement(true);
		result.setBuy(lendingRepo.isBuy());
		result.setSettlementTransactionType(lendingRepo.getRepoDefinition().isReverse() ? SettlementTransactionTypes.REVERSE_REPURCHASE_AGREEMENT : SettlementTransactionTypes.REPURCHASE_AGREEMENT);
		result.setTransactionReferenceNumber(getInstructionUtilHandler().getTransactionReferenceNumber(Arrays.asList(instruction, lendingRepo)));

		populateClearingBroker(instruction, result, lendingRepo);
		if (isMaturity) {
			result.setTradeDate(lendingRepo.getMaturityDate());
			result.setSettlementDate(lendingRepo.getMaturitySettlementDate());
		}
		else {
			result.setTradeDate(lendingRepo.getTradeDate());
			result.setSettlementDate(lendingRepo.getSettlementDate());
		}
		populateRepoMessage(instruction, result, lendingRepo);

		return resultList;
	}


	@Override
	public InstructionDeliveryFieldCommand getDeliveryInstructionProvider(Instruction instruction, LendingRepo lendingRepo) {
		return new InstructionDeliveryFieldCommand() {
			@Override
			public Instruction getInstruction() {
				return instruction;
			}


			@Override
			public BusinessCompany getDeliveryCompany() {
				return lendingRepo.getRepoDefinition().getRepoInvestmentAccount().getIssuingCompany();
			}


			@Override
			public InvestmentAccount getDeliveryAccount() {
				return lendingRepo.getHoldingInvestmentAccount();
			}


			@Override
			public InvestmentSecurity getDeliveryCurrency() {
				return lendingRepo.getSettlementCurrency();
			}


			@Override
			public InvestmentAccount getDeliveryClientAccount() {
				return lendingRepo.getClientInvestmentAccount();
			}
		};
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private void populateRepoMessage(Instruction instruction, AbstractTradeMessage tradeMessage, LendingRepo lendingRepo) {
		tradeMessage.setSenderBIC(trimToNull(getBusinessCompanyUtilHandler().getParametricBusinessIdentifierCode()));
		tradeMessage.setMessageFunctions(MessageFunctions.NEW_MESSAGE);
		tradeMessage.setPrice(lendingRepo.getPrice());
		tradeMessage.setPriceType(TradeMessagePriceTypes.PERCENTAGE);
		tradeMessage.setOriginalFaceAmount(lendingRepo.getOriginalFace());
		tradeMessage.setRepoClosingDate(lendingRepo.getMaturityDate());
		tradeMessage.setRepoNetCashAmount(MathUtils.add(lendingRepo.getNetCash(), lendingRepo.getInterestAmount()));
		tradeMessage.setRepoAccruedInterestAmount(lendingRepo.getInterestAmount());
		tradeMessage.setRepoInterestRate(lendingRepo.getInterestRate());
		tradeMessage.setPlaceOfSettlement(FEDERAL_RESERVE_BANK);
		tradeMessage.setDealAmount(lendingRepo.getNetCash());
		tradeMessage.setNetSettlementAmount(lendingRepo.getNetCash());
		tradeMessage.setRepoRateType(getRepoRateType(lendingRepo.getType()));

		populateCustodian(tradeMessage, lendingRepo);
		populateSecurity(tradeMessage, lendingRepo);
		populateExecutingBroker(instruction, tradeMessage, lendingRepo);
	}


	/**
	 * receive = 543 (initial)
	 * deliver = 541 (maturity)
	 * if counterparty if Bank of New York Mellon:
	 * ReceiveFreeAgainstPayment 542 (initial)
	 * DeliverFreeAgainstPayment 540 (maturity)
	 */
	private AbstractTradeMessage getInstructionMessage(LendingRepo lendingRepo, boolean isMaturity) {
		AbstractTradeMessage result;

		BusinessCompany counterParty = lendingRepo.getRepoDefinition().getCounterparty();
		BusinessCompany bankOfNewYorkMellon = getBusinessCompanyService().getBusinessCompanyByTypeAndName(COMPANY_TYPE, BANK_OF_NEW_YORK_MELLON);
		AssertUtils.assertNotNull(bankOfNewYorkMellon, () -> String.format("The [%s] company [%s] could not be found.", COMPANY_TYPE, BANK_OF_NEW_YORK_MELLON));
		if (Objects.equals(counterParty.getId(), bankOfNewYorkMellon.getId())) {
			if (isMaturity) {
				result = new DeliverFreeAgainstPayment();
			}
			else {
				result = new ReceiveFreeAgainstPayment();
			}
		}
		else if (isMaturity) {
			result = new DeliverAgainstPayment();
		}
		else {
			result = new ReceiveAgainstPayment();
		}
		return result;
	}


	private void populateCustodian(AbstractTradeMessage tradeMessage, LendingRepo lendingRepo) {
		InvestmentAccount custodianAccount = getCustodianAccount(lendingRepo);
		ValidationUtils.assertNotNull(custodianAccount, "Custodian Account cannot be null.");
		ValidationUtils.assertNotNull(custodianAccount.getIssuingCompany(), String.format("Custodian Account [%s] Issuing Company cannot be null", custodianAccount.getLabel()));
		String custodianBIC = trimToNull(custodianAccount.getIssuingCompany().getBusinessIdentifierCode());
		ValidationUtils.assertNotEmpty(custodianBIC, () -> String.format("The account [%s] with issuing company [%s] does not have a Business Identifier Code (BIC)",
				custodianAccount.getLabel(), custodianAccount.getIssuingCompany().getNameWithType()));

		tradeMessage.setReceiverBIC(custodianBIC);
		tradeMessage.setCustodyBIC(custodianBIC);
		tradeMessage.setCustodyAccountNumber(custodianAccount.getNumber());
	}


	private void populateSecurity(AbstractTradeMessage tradeMessage, LendingRepo lendingRepo) {
		InvestmentSecurity investmentSecurity = lendingRepo.getInvestmentSecurity();

		InvestmentInstrument investmentInstrument = investmentSecurity.getInstrument();
		ISITCCodes isitcCode = getInvestmentSecurityUtilHandler().getISITCCodeStrict(investmentInstrument);
		tradeMessage.setIsitcCode(isitcCode == ISITCCodes.NONE ? null : isitcCode);

		Optional.of(investmentSecurity).map(InvestmentSecurity::getIsin).ifPresent(tradeMessage::setIsin);
		tradeMessage.setMaturityDate(investmentSecurity.getEndDate());
		tradeMessage.setIssueDate(investmentSecurity.getStartDate());
		tradeMessage.setInterestRate(getInvestmentSecurityUtilHandler().getInterestRate(investmentSecurity));
		tradeMessage.setSecurityCurrency(trimToNull(investmentSecurity.getInstrument().getTradingCurrency().getSymbol()));
	}


	private RepoRateTypes getRepoRateType(LendingRepoType lendingRepoType) {
		return lendingRepoType.isMultipleInterestRatesPerPeriod() ? RepoRateTypes.VARIABLE : RepoRateTypes.FIXED;
	}


	private void populateExecutingBroker(Instruction instruction, AbstractTradeMessage tradeMessage, LendingRepo lendingRepo) {
		tradeMessage.setExecutingBrokerIdentifier(
				getPartyIdentifier(Optional.of(lendingRepo)
								.map(LendingRepo::getRepoDefinition)
								.map(LendingRepoDefinition::getCounterparty)
								.map(BusinessCompany::getDtcNumber)
								.map(String::trim)
								.filter(s -> !s.isEmpty())
								.orElse(null)
						, MessagePartyIdentifierTypes.DTC
				)
		);
		InstructionDeliveryFieldCommand deliveryFieldProvider = getDeliveryInstructionProvider(instruction, lendingRepo);
		Optional<InvestmentInstructionDelivery> deliveryInstruction = Optional.ofNullable(deliveryFieldProvider.getDeliveryCompany())
				.map(c -> getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(instruction, c, deliveryFieldProvider.getDeliveryCurrency()));
		deliveryInstruction
				.map(InvestmentInstructionDelivery::getDeliveryForFurtherCredit)
				.ifPresent(tradeMessage::setSafekeepingAccount);
	}


	private void populateClearingBroker(Instruction instruction, AbstractTradeMessage tradeMessage, LendingRepo lendingRepo) {
		InstructionDeliveryFieldCommand deliveryFieldProvider = getDeliveryInstructionProvider(instruction, lendingRepo);
		Optional<InvestmentInstructionDelivery> deliveryInstruction = Optional.ofNullable(deliveryFieldProvider.getDeliveryCompany())
				.map(c -> getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(instruction, c, deliveryFieldProvider.getDeliveryCurrency()));

		deliveryInstruction
				.map(InvestmentInstructionDelivery::getDeliveryAccountNumber)
				.ifPresent(tradeMessage::setClearingSafekeepingAccount);
		if (StringUtils.isEmpty(tradeMessage.getClearingSafekeepingAccount())) {
			tradeMessage.setClearingSafekeepingAccount(lendingRepo.getRepoDefinition().getRepoInvestmentAccount().getNumber());
		}

		deliveryInstruction
				.map(InvestmentInstructionDelivery::getDeliveryABA)
				.map(String::trim)
				.filter(s -> !s.isEmpty())
				.ifPresent(v -> tradeMessage.setClearingBrokerIdentifier(getPartyIdentifier(v, MessagePartyIdentifierTypes.USFW)));
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private LendingRepo getIdentityObject(Instruction instruction) {
		ReadOnlyDAO<LendingRepo> dao = getDaoLocator().locate(instruction.getSystemTable().getName());
		return dao.findByPrimaryKey(instruction.getFkFieldId());
	}


	private Date getInstructionDate(Instruction instruction) {
		ValidationUtils.assertTrue(instruction instanceof InvestmentInstructionItem, "Instruction expected to be an instance of InvestmentInstructionItem.");
		return Optional.of(instruction)
				.filter(InvestmentInstructionItem.class::isInstance)
				.map(InvestmentInstructionItem.class::cast)
				.map(InvestmentInstructionItem::getInstruction)
				.map(InvestmentInstruction::getInstructionDate)
				.orElseThrow(() -> new RuntimeException("Expected an instruction date."));
	}


	private String trimToNull(String value) {
		String trimmed = StringUtils.trim(value);
		return StringUtils.isEmpty(trimmed) ? null : trimmed;
	}


	private InvestmentAccount getCustodianAccount(LendingRepo lendingRepo) {
		if (lendingRepo != null) {
			ValidationUtils.assertNotNull(lendingRepo.getHoldingInvestmentAccount(), "Repo #: " + lendingRepo.getId() + " is missing a holding account selection. Unable to determine instruction recipient.");
			return getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurposeStrict(lendingRepo.getClientInvestmentAccount().getId(),
					lendingRepo.getHoldingInvestmentAccount().getId(), lendingRepo.getInvestmentSecurity().getId(), InvestmentAccountRelationshipPurpose.CUSTODIAN_ACCOUNT_PURPOSE_NAME, lendingRepo.getTradeDate());
		}
		return null;
	}


	private PartyIdentifier getPartyIdentifier(String identifier, MessagePartyIdentifierTypes type) {
		String trimmed = trimToNull(identifier);
		return Objects.isNull(trimmed) ? null : new PartyIdentifier(trimmed, type);
	}


	private boolean isMaturity(LendingRepo lendingRepo, Date instructionDate) {
		LendingRepoPairOffSearchForm searchForm = new LendingRepoPairOffSearchForm();
		searchForm.setMaturingRepoId(lendingRepo.getId());
		searchForm.setPairOffDate(instructionDate);
		List<LendingRepoPairOff> lendingRepoPairOffList = getLendingRepoPairOffService().getLendingRepoPairOffList(searchForm);
		return !CollectionUtils.isEmpty(lendingRepoPairOffList);
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public InstructionCancellationHandler getInstructionCancellationHandler() {
		return this.instructionCancellationHandler;
	}


	public void setInstructionCancellationHandler(InstructionCancellationHandler instructionCancellationHandler) {
		this.instructionCancellationHandler = instructionCancellationHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public InstructionUtilHandler<I, InstructionDeliveryFieldCommand> getInstructionUtilHandler() {
		return this.instructionUtilHandler;
	}


	public void setInstructionUtilHandler(InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler) {
		this.instructionUtilHandler = instructionUtilHandler;
	}


	public BusinessCompanyUtilHandler getBusinessCompanyUtilHandler() {
		return this.businessCompanyUtilHandler;
	}


	public void setBusinessCompanyUtilHandler(BusinessCompanyUtilHandler businessCompanyUtilHandler) {
		this.businessCompanyUtilHandler = businessCompanyUtilHandler;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public LendingRepoPairOffService getLendingRepoPairOffService() {
		return this.lendingRepoPairOffService;
	}


	public void setLendingRepoPairOffService(LendingRepoPairOffService lendingRepoPairOffService) {
		this.lendingRepoPairOffService = lendingRepoPairOffService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public InvestmentInstructionDeliveryUtilHandler getInvestmentInstructionDeliveryUtilHandler() {
		return this.investmentInstructionDeliveryUtilHandler;
	}


	public void setInvestmentInstructionDeliveryUtilHandler(InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler) {
		this.investmentInstructionDeliveryUtilHandler = investmentInstructionDeliveryUtilHandler;
	}
}
