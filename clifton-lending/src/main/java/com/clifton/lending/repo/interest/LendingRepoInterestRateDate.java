package com.clifton.lending.repo.interest;


import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>LendingRepoInterestRateDate</code> defines an interest rates for a date by counter party.q
 *
 * @author mwacker
 */
@NonPersistentObject
public class LendingRepoInterestRateDate {

	private Date interestRateDate;
	private List<LendingRepoInterestRateCounterParty> counterPartyRateList = new ArrayList<>();


	public Date getInterestRateDate() {
		return this.interestRateDate;
	}


	public void setInterestRateDate(Date interestRateDate) {
		this.interestRateDate = interestRateDate;
	}


	public List<LendingRepoInterestRateCounterParty> getCounterPartyRateList() {
		return this.counterPartyRateList;
	}


	public void setCounterPartyRateList(List<LendingRepoInterestRateCounterParty> counterPartyRateList) {
		this.counterPartyRateList = counterPartyRateList;
	}
}
