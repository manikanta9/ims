package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.lending.repo.LendingRepo;

import java.util.List;


/**
 * The <code>LendingRepoTriPartyPositionOpenBookingRule</code> creates the opening position in either the holding account or tri-party account.
 *
 * @author mwacker
 */
public class LendingRepoTriPartyPositionOpenBookingRule extends LendingRepoPositionOpenBookingRule {

	@Override
	public void applyRule(BookingSession<LendingRepo> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		LendingRepo repo = bookingSession.getBookableEntity();

		AccountingAccount positionAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION);

		List<? extends AccountingJournalDetailDefinition> details = CoreCollectionUtils.clone(journal.getJournalDetailList());
		for (AccountingJournalDetailDefinition positionEntry : CollectionUtils.getIterable(details)) {
			if (positionEntry.getAccountingAccount().equals(positionAccount) && positionEntry.getDescription().startsWith("Tri-Party:")) {
				AccountingJournalDetail openingPositionEntry = createOpeningPosition(repo, (AccountingJournalDetail) positionEntry, getHoldingAccountForTriParty(repo, false), "Tri-Party: ");
				journal.addJournalDetail(openingPositionEntry);
			}
		}
	}
}
