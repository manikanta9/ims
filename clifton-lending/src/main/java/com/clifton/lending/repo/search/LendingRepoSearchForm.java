package com.clifton.lending.repo.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>LendingRepo</code> defines the terms of a REPO or reverse REPO agreement.  The REPO can be a grouping of multiple
 * clients and securities but all of the same REPO type with the same counter party, term and rate.  The total of the detail entries
 * must match the REPO amount.
 *
 * @author mwacker
 */
public class LendingRepoSearchForm extends BaseWorkflowAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "repoDefinition.id", sortField = "tradeDate")
	private Integer repoDefinitionId;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Integer excludeRepoDetailId;

	@SearchField(searchField = "type.id")
	private Integer typeId;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String typeName;

	@SearchField(searchField = "triPartyRepo", searchFieldPath = "type")
	private Boolean typeTriPartyRepo;

	@SearchField(searchField = "multipleInterestRatesPerPeriod", searchFieldPath = "type")
	private Boolean multipleInterestRatesPerPeriod;

	@SearchField(searchField = "settlementDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date beforeSettlementDate;

	@SearchField
	private Date tradeDate;

	@SearchField(searchField = "tradeDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date tradeDateBefore;

	@SearchField
	private Date settlementDate;

	@SearchField
	private Date maturitySettlementDate;

	@SearchField(searchField = "maturitySettlementDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_IS_NULL)
	private Date maturitySettlementDateAfterOrNull;

	@SearchField
	private Date bookingDate;

	@SearchField
	private Date maturityDate;

	// Custom search field
	/**
	 * Filter REPO's are either traded or maturing on the given date.
	 */
	private Date activityDate;

	// Custom search field
	/**
	 * Filter REPO's that have a trade date less than the given date when the booking date is null
	 * OR
	 * REPO's that have a maturity date less than the given date when the maturity booking date is null.
	 */
	private Date openingUnbookedOrMaturityUnbookedOnDate;

	// Custom search field
	/**
	 * Indicates that we are filtering all REPO's that need to be confirmed.
	 * This will include all REPO's that have a tradeDate equal to the confirmationDate, and all REPO's
	 * from any other date that are not confirmed and have a workflow status of Closed but are not in the Cancelled state.
	 */
	private Date confirmationDate;

	// Custom search field
	/**
	 * Filter REPOs based on the linked maturing or opening REPO's pair-off dates.
	 */
	private Date pairOffDate;

	// Custom search field
	/**
	 * Filter REPO's that are active on the given date.  The given date is greater than or equal to settlementDate and less than maturity date or maturity date
	 * is null.
	 */
	private Date activeOnDate;

	@SearchField(searchField = "confirmedDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean confirmed;

	@SearchField(searchField = "parentIdentifier", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean roll;

	@SearchField(searchField = "maturityBookingDate", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean maturityBooked;

	@SearchField(searchField = "bookingDate", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean openingBooked;

	@SearchField(searchField = "repoInterestRateList.interestRateDate", comparisonConditions = ComparisonConditions.EXISTS)
	private Date interestRateDate;

	@SearchField
	private Integer term;

	@SearchField
	private Short cancellationNoticeTerm;

	@SearchField(searchField = "cancellationNoticeTerm", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean cancellationNoticeTermIsNull;

	@SearchField
	private BigDecimal interestRate;

	@SearchField
	private String description;

	@SearchField
	private BigDecimal price;

	@SearchField
	private BigDecimal haircutPercent;

	@SearchField
	private BigDecimal marketValue;

	@SearchField
	private BigDecimal netCash;

	@SearchField
	private BigDecimal interestAmount;

	@SearchField
	private BigDecimal exchangeRateToBase;

	@SearchField(searchField = "originalFace")
	private BigDecimal originalFace;

	@SearchField(searchField = "quantityIntended")
	private BigDecimal quantityIntended;

	@SearchField(searchField = "traderUser.id")
	private Short traderId;

	@SearchField(searchField = "counterparty.id", searchFieldPath = "repoDefinition", sortField = "counterparty.name")
	private Integer counterpartyId;

	@SearchField(searchField = "name", searchFieldPath = "repoDefinition.counterparty", sortField = "name")
	private String counterpartyName;

	@SearchField(searchField = "reverse", searchFieldPath = "repoDefinition")
	private Boolean reverse;

	@SearchField(searchField = "investmentSecurity.id", searchFieldPath = "repoDefinition", sortField = "investmentSecurity.symbol")
	private Integer securityId;

	@SearchField(searchField = "investmentSecurity.cusip", searchFieldPath = "repoDefinition")
	private String securityCusip;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "repoDefinition.investmentSecurity.instrument", sortField = "tradingCurrency.symbol")
	private Integer ccySecurityId;

	@SearchField(searchField = "clientInvestmentAccount.id", searchFieldPath = "repoDefinition")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.number", searchFieldPath = "repoDefinition")
	private String clientInvestmentAccountNumber;

	@SearchField(searchField = "holdingInvestmentAccount.id", searchFieldPath = "repoDefinition")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.number", searchFieldPath = "repoDefinition")
	private String holdingInvestmentAccountNumber;

	@SearchField(searchField = "repoInvestmentAccount.id", searchFieldPath = "repoDefinition")
	private Integer repoInvestmentAccountId;

	@SearchField(searchField = "repoOpeningPairList.pairOffDate", comparisonConditions = ComparisonConditions.EXISTS)
	private Date openingPairOffDate;

	@SearchField(searchField = "repoMaturingPairList.pairOffDate", comparisonConditions = ComparisonConditions.EXISTS)
	private Date maturingPairOffDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getRepoDefinitionId() {
		return this.repoDefinitionId;
	}


	public void setRepoDefinitionId(Integer repoDefinitionId) {
		this.repoDefinitionId = repoDefinitionId;
	}


	public Integer getExcludeRepoDetailId() {
		return this.excludeRepoDetailId;
	}


	public void setExcludeRepoDetailId(Integer excludeRepoDetailId) {
		this.excludeRepoDetailId = excludeRepoDetailId;
	}


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Boolean getTypeTriPartyRepo() {
		return this.typeTriPartyRepo;
	}


	public void setTypeTriPartyRepo(Boolean typeTriPartyRepo) {
		this.typeTriPartyRepo = typeTriPartyRepo;
	}


	public Boolean getMultipleInterestRatesPerPeriod() {
		return this.multipleInterestRatesPerPeriod;
	}


	public void setMultipleInterestRatesPerPeriod(Boolean multipleInterestRatesPerPeriod) {
		this.multipleInterestRatesPerPeriod = multipleInterestRatesPerPeriod;
	}


	public Date getBeforeSettlementDate() {
		return this.beforeSettlementDate;
	}


	public void setBeforeSettlementDate(Date beforeSettlementDate) {
		this.beforeSettlementDate = beforeSettlementDate;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getTradeDateBefore() {
		return this.tradeDateBefore;
	}


	public void setTradeDateBefore(Date tradeDateBefore) {
		this.tradeDateBefore = tradeDateBefore;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getMaturitySettlementDate() {
		return this.maturitySettlementDate;
	}


	public void setMaturitySettlementDate(Date maturitySettlementDate) {
		this.maturitySettlementDate = maturitySettlementDate;
	}


	public Date getMaturitySettlementDateAfterOrNull() {
		return this.maturitySettlementDateAfterOrNull;
	}


	public void setMaturitySettlementDateAfterOrNull(Date maturitySettlementDateAfterOrNull) {
		this.maturitySettlementDateAfterOrNull = maturitySettlementDateAfterOrNull;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public Date getActivityDate() {
		return this.activityDate;
	}


	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}


	public Date getOpeningUnbookedOrMaturityUnbookedOnDate() {
		return this.openingUnbookedOrMaturityUnbookedOnDate;
	}


	public void setOpeningUnbookedOrMaturityUnbookedOnDate(Date openingUnbookedOrMaturityUnbookedOnDate) {
		this.openingUnbookedOrMaturityUnbookedOnDate = openingUnbookedOrMaturityUnbookedOnDate;
	}


	public Date getConfirmationDate() {
		return this.confirmationDate;
	}


	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}


	public Date getPairOffDate() {
		return this.pairOffDate;
	}


	public void setPairOffDate(Date pairOffDate) {
		this.pairOffDate = pairOffDate;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public Boolean getConfirmed() {
		return this.confirmed;
	}


	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}


	public Boolean getRoll() {
		return this.roll;
	}


	public void setRoll(Boolean roll) {
		this.roll = roll;
	}


	public Boolean getMaturityBooked() {
		return this.maturityBooked;
	}


	public void setMaturityBooked(Boolean maturityBooked) {
		this.maturityBooked = maturityBooked;
	}


	public Boolean getOpeningBooked() {
		return this.openingBooked;
	}


	public void setOpeningBooked(Boolean openingBooked) {
		this.openingBooked = openingBooked;
	}


	public Date getInterestRateDate() {
		return this.interestRateDate;
	}


	public void setInterestRateDate(Date interestRateDate) {
		this.interestRateDate = interestRateDate;
	}


	public Integer getTerm() {
		return this.term;
	}


	public void setTerm(Integer term) {
		this.term = term;
	}


	public Short getCancellationNoticeTerm() {
		return this.cancellationNoticeTerm;
	}


	public void setCancellationNoticeTerm(Short cancellationNoticeTerm) {
		this.cancellationNoticeTerm = cancellationNoticeTerm;
	}


	public Boolean getCancellationNoticeTermIsNull() {
		return this.cancellationNoticeTermIsNull;
	}


	public void setCancellationNoticeTermIsNull(Boolean cancellationNoticeTermIsNull) {
		this.cancellationNoticeTermIsNull = cancellationNoticeTermIsNull;
	}


	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getHaircutPercent() {
		return this.haircutPercent;
	}


	public void setHaircutPercent(BigDecimal haircutPercent) {
		this.haircutPercent = haircutPercent;
	}


	public BigDecimal getMarketValue() {
		return this.marketValue;
	}


	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}


	public BigDecimal getNetCash() {
		return this.netCash;
	}


	public void setNetCash(BigDecimal netCash) {
		this.netCash = netCash;
	}


	public BigDecimal getInterestAmount() {
		return this.interestAmount;
	}


	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public BigDecimal getQuantityIntended() {
		return this.quantityIntended;
	}


	public void setQuantityIntended(BigDecimal quantityIntended) {
		this.quantityIntended = quantityIntended;
	}


	public Short getTraderId() {
		return this.traderId;
	}


	public void setTraderId(Short traderId) {
		this.traderId = traderId;
	}


	public Integer getCounterpartyId() {
		return this.counterpartyId;
	}


	public void setCounterpartyId(Integer counterpartyId) {
		this.counterpartyId = counterpartyId;
	}


	public String getCounterpartyName() {
		return this.counterpartyName;
	}


	public void setCounterpartyName(String counterpartyName) {
		this.counterpartyName = counterpartyName;
	}


	public Boolean getReverse() {
		return this.reverse;
	}


	public void setReverse(Boolean reverse) {
		this.reverse = reverse;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public String getSecurityCusip() {
		return this.securityCusip;
	}


	public void setSecurityCusip(String securityCusip) {
		this.securityCusip = securityCusip;
	}


	public Integer getCcySecurityId() {
		return this.ccySecurityId;
	}


	public void setCcySecurityId(Integer ccySecurityId) {
		this.ccySecurityId = ccySecurityId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public String getClientInvestmentAccountNumber() {
		return this.clientInvestmentAccountNumber;
	}


	public void setClientInvestmentAccountNumber(String clientInvestmentAccountNumber) {
		this.clientInvestmentAccountNumber = clientInvestmentAccountNumber;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public String getHoldingInvestmentAccountNumber() {
		return this.holdingInvestmentAccountNumber;
	}


	public void setHoldingInvestmentAccountNumber(String holdingInvestmentAccountNumber) {
		this.holdingInvestmentAccountNumber = holdingInvestmentAccountNumber;
	}


	public Integer getRepoInvestmentAccountId() {
		return this.repoInvestmentAccountId;
	}


	public void setRepoInvestmentAccountId(Integer repoInvestmentAccountId) {
		this.repoInvestmentAccountId = repoInvestmentAccountId;
	}


	public Date getOpeningPairOffDate() {
		return this.openingPairOffDate;
	}


	public void setOpeningPairOffDate(Date openingPairOffDate) {
		this.openingPairOffDate = openingPairOffDate;
	}


	public Date getMaturingPairOffDate() {
		return this.maturingPairOffDate;
	}


	public void setMaturingPairOffDate(Date maturingPairOffDate) {
		this.maturingPairOffDate = maturingPairOffDate;
	}
}
