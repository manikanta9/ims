package com.clifton.lending.repo.interest;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.lending.repo.LendingRepoType;

import java.math.BigDecimal;


/**
 * The <code>LendingRepoInterestRateCounterParty</code> defines an interest rate for a given counter party.
 *
 * @author mwacker
 */
@NonPersistentObject
public class LendingRepoInterestRateCounterParty extends BaseEntity<Integer> {

	private BusinessCompany counterparty;
	private LendingRepoType repoType;
	private Short cancellationNoticeTerm;
	private BigDecimal interestRate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(BusinessCompany company) {
		this.counterparty = company;
	}


	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	public void setInterestRate(BigDecimal rateDate) {
		this.interestRate = rateDate;
	}


	public LendingRepoType getRepoType() {
		return this.repoType;
	}


	public void setRepoType(LendingRepoType repoType) {
		this.repoType = repoType;
	}


	public Short getCancellationNoticeTerm() {
		return this.cancellationNoticeTerm;
	}


	public void setCancellationNoticeTerm(Short cancellationNoticeTerm) {
		this.cancellationNoticeTerm = cancellationNoticeTerm;
	}
}
