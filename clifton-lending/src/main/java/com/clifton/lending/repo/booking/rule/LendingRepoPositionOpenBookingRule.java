package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.lending.repo.LendingRepo;

import java.util.List;
import java.util.Set;


/**
 * The <code>LendingRepoPositionOpenBookingRule</code> creates the opening transactions for the assets that are being lent or re-purchased in the REPO  at the same cost as the close.
 *
 * @author mwacker
 */
public class LendingRepoPositionOpenBookingRule extends BaseLendingRepoPositionBookingRule {

	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<LendingRepo> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		LendingRepo repo = bookingSession.getBookableEntity();

		AccountingAccount positionAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION);

		if (isRepoOpening() || !repo.getRepoDefinition().isReverse()) {
			List<? extends AccountingJournalDetailDefinition> details = CoreCollectionUtils.clone(journal.getJournalDetailList());
			for (AccountingJournalDetailDefinition positionEntry : CollectionUtils.getIterable(details)) {
				if (positionEntry.getAccountingAccount().equals(positionAccount) && !positionEntry.getDescription().startsWith("Tri-Party:")) {
					AccountingJournalDetail openingPositionEntry = createOpeningPosition(repo, (AccountingJournalDetail) positionEntry, getHoldingAccount(repo, false), "");
					journal.addJournalDetail(openingPositionEntry);
				}
			}
		}
	}


	protected AccountingJournalDetail createOpeningPosition(LendingRepo repo, AccountingJournalDetail closingPositionEntry, InvestmentAccount holdingInvestmentAccount, String descriptionPrefix) {
		AccountingJournalDetail positionEntry = new AccountingJournalDetail();
		BeanUtils.copyProperties(closingPositionEntry, positionEntry, new String[]{"parentTransaction", "parentDefinition"});

		positionEntry.setQuantity(closingPositionEntry.getQuantity().negate());

		positionEntry.setOriginalTransactionDate(closingPositionEntry.getOriginalTransactionDate());
		positionEntry.setExchangeRateToBase(closingPositionEntry.getExchangeRateToBase());

		positionEntry.setLocalDebitCredit(closingPositionEntry.getLocalDebitCredit().negate());
		positionEntry.setBaseDebitCredit(closingPositionEntry.getBaseDebitCredit().negate());
		positionEntry.setPositionCostBasis(closingPositionEntry.getPositionCostBasis().negate());

		positionEntry.setOpening(true);
		positionEntry.setHoldingInvestmentAccount(holdingInvestmentAccount);
		positionEntry.setDescription("Position opening from transfer to " + holdingInvestmentAccount.getLabel());

		positionEntry.setDescription(descriptionPrefix + "Position opening from " + (repo.getRepoDefinition().isReverse() ? "reverse " : "") + "REPO " + (isRepoOpening() ? "opening" : "maturity")
				+ " for " + positionEntry.getInvestmentSecurity().getSymbol());

		return positionEntry;
	}
}
