package com.clifton.lending.repo.interest.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.lending.repo.interest.LendingRepoInterestRate;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class LendingRepoInterestRateListByRepoCache extends SelfRegisteringSingleKeyDaoListCache<LendingRepoInterestRate, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "repo.id";
	}


	@Override
	protected Integer getBeanKeyValue(LendingRepoInterestRate bean) {
		if (bean.getRepo() != null) {
			return bean.getRepo().getId();
		}
		return null;
	}
}
