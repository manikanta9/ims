package com.clifton.lending.repo.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.lending.repo.LendingRepo;

import java.util.List;


/**
 * The <code>LendingRepoTriPartyPositionCloseBookingRule</code> creates the closing position in either the holding account or tri-party account.
 *
 * @author mwacker
 */
public class LendingRepoTriPartyPositionCloseBookingRule extends LendingRepoPositionCloseBookingRule {

	@Override
	public void applyRule(BookingSession<LendingRepo> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		LendingRepo repo = bookingSession.getBookableEntity();

		journal.setDescription(repo.getDescription());
		// Handle non-reverse Tri-Party maturity
		if (!isRepoOpening() && repo.getType().isTriPartyRepo() && !repo.getRepoDefinition().isReverse()) {
			AccountingAccount positionAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION);

			List<? extends AccountingJournalDetailDefinition> details = journal.getJournalDetailList();
			if (details != null && !details.isEmpty()) {
				details = CoreCollectionUtils.clone(details);
			}
			for (AccountingJournalDetailDefinition positionEntry : CollectionUtils.getIterable(details)) {
				if (positionEntry.getAccountingAccount().equals(positionAccount) && !positionEntry.getDescription().startsWith("Tri-Party:")
						&& positionEntry.getHoldingInvestmentAccount().equals(repo.getRepoDefinition().getTriPartyInvestmentAccount())) {
					AccountingJournalDetail openingPositionEntry = createBondCloseRecordTriParty((AccountingJournalDetail) positionEntry, positionEntry.getHoldingInvestmentAccount(), "Tri-Party: ");
					journal.addJournalDetail(openingPositionEntry);
				}
			}
		}
		else {
			// position and distribution
			AccountingJournalDetail positionEntry = createBondCloseRecordForOpening(repo, getHoldingAccountForTriParty(repo, true), "Tri-Party: ");
			journal.addJournalDetail(positionEntry);
		}
	}
}
