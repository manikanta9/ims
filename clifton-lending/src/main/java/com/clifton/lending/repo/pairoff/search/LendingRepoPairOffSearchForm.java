package com.clifton.lending.repo.pairoff.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class LendingRepoPairOffSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "openingRepo.id")
	private Integer openingRepoId;

	@SearchField(searchField = "openingRepo.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean openingRepoIsNull;

	@SearchField(searchField = "maturingRepo.id")
	private Integer maturingRepoId;

	@SearchField(searchField = "maturingRepo.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean maturingRepoIsNull;

	@SearchField
	private String pairOffGroup;

	// custom search field
	private Date pairOffDate;

	// custom search field
	/**
	 * REPO id used to lookup group id and filter for pairoff's using pairOffGroup's with the retrieved group id.
	 * <p/>
	 * pairOffDate is required, if it's not populated the filter will not be applied.
	 */
	private Integer repoIdForGroup;

	// custom search field
	private Integer openingOrMaturingRepoId;

	// custom search field
	/**
	 * Checks if a REPO is paired as an opening or a maturity on the pair off date.
	 * <p/>
	 * pairOffDate is required, if it's not populated the filter will not be applied.
	 */
	private Integer checkOpeningRepoId;

	/**
	 * Checks if a REPO is paired as a maturity or an opening on the pair off date.
	 * <p/>
	 * pairOffDate is required, if it's not populated the filter will not be applied.
	 */
	private Integer checkMaturingRepoId;

	@SearchField
	private Boolean completed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getOpeningRepoId() {
		return this.openingRepoId;
	}


	public void setOpeningRepoId(Integer openingRepoId) {
		this.openingRepoId = openingRepoId;
	}


	public Integer getMaturingRepoId() {
		return this.maturingRepoId;
	}


	public void setMaturingRepoId(Integer maturingRepoId) {
		this.maturingRepoId = maturingRepoId;
	}


	public String getPairOffGroup() {
		return this.pairOffGroup;
	}


	public void setPairOffGroup(String pairOffGroup) {
		this.pairOffGroup = pairOffGroup;
	}


	public Date getPairOffDate() {
		return this.pairOffDate;
	}


	public void setPairOffDate(Date pairOffDate) {
		this.pairOffDate = pairOffDate;
	}


	public Boolean getOpeningRepoIsNull() {
		return this.openingRepoIsNull;
	}


	public void setOpeningRepoIsNull(Boolean openingRepoIsNull) {
		this.openingRepoIsNull = openingRepoIsNull;
	}


	public Boolean getMaturingRepoIsNull() {
		return this.maturingRepoIsNull;
	}


	public void setMaturingRepoIsNull(Boolean maturingRepoIsNull) {
		this.maturingRepoIsNull = maturingRepoIsNull;
	}


	public Integer getCheckOpeningRepoId() {
		return this.checkOpeningRepoId;
	}


	public void setCheckOpeningRepoId(Integer checkOpeningRepoId) {
		this.checkOpeningRepoId = checkOpeningRepoId;
	}


	public Integer getCheckMaturingRepoId() {
		return this.checkMaturingRepoId;
	}


	public void setCheckMaturingRepoId(Integer checkMaturingRepoId) {
		this.checkMaturingRepoId = checkMaturingRepoId;
	}


	public Integer getOpeningOrMaturingRepoId() {
		return this.openingOrMaturingRepoId;
	}


	public void setOpeningOrMaturingRepoId(Integer openingOrMaturingRepoId) {
		this.openingOrMaturingRepoId = openingOrMaturingRepoId;
	}


	public Integer getRepoIdForGroup() {
		return this.repoIdForGroup;
	}


	public void setRepoIdForGroup(Integer repoIdForGroup) {
		this.repoIdForGroup = repoIdForGroup;
	}


	public Boolean getCompleted() {
		return this.completed;
	}


	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}
}
