package com.clifton.lending.repo;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentObjectInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;


/**
 * The <code>LendingRepo</code> defines items of REPO transaction that cannot change when the REPO is rolled.
 * When a REPO is rolled, new REPO will have the same definition.
 *
 * @author mwacker
 */
public class LendingRepoDefinition extends BaseEntity<Integer> implements InvestmentObjectInfo {

	private BusinessCompany counterparty;
	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	private InvestmentAccount repoInvestmentAccount;
	private InvestmentAccount triPartyInvestmentAccount;
	private InvestmentSecurity investmentSecurity;
	private InvestmentInstrumentHierarchy instrumentHierarchy; // NOTE: do we need this field or is it redundant to investmentSecurity?

	/**
	 * Determines if the REPO is a reverse REPO.
	 */
	private boolean reverse;

	/**
	 * Determines if the REPO definition is active.
	 */
	private boolean active;

	// TODO: use the actual day count class, submitting the enum is not working correctly
	private String dayCountConvention;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(BusinessCompany counterparty) {
		this.counterparty = counterparty;
	}


	public boolean isReverse() {
		return this.reverse;
	}


	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}


	public InvestmentInstrumentHierarchy getInstrumentHierarchy() {
		return this.instrumentHierarchy;
	}


	public void setInstrumentHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		this.instrumentHierarchy = hierarchy;
	}


	public String getDayCountConvention() {
		return this.dayCountConvention;
	}


	public void setDayCountConvention(String dayCountConvention) {
		this.dayCountConvention = dayCountConvention;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public InvestmentAccount getRepoInvestmentAccount() {
		return this.repoInvestmentAccount;
	}


	public void setRepoInvestmentAccount(InvestmentAccount repoInvestmentAccount) {
		this.repoInvestmentAccount = repoInvestmentAccount;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public InvestmentAccount getTriPartyInvestmentAccount() {
		return this.triPartyInvestmentAccount;
	}


	public void setTriPartyInvestmentAccount(InvestmentAccount triPartyInvestmentAccount) {
		this.triPartyInvestmentAccount = triPartyInvestmentAccount;
	}
}
