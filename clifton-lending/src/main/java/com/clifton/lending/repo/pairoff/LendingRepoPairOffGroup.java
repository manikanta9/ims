package com.clifton.lending.repo.pairoff;


import com.clifton.lending.repo.LendingRepo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>LendingRepoPairOffSaveEntity</code> is used to submit a new pair off group to the service.
 *
 * @author mwacker
 */
//@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class LendingRepoPairOffGroup {

	/**
	 * The list of opening repo's.
	 */
	private List<LendingRepo> openingRepoList = new ArrayList<>();

	/**
	 * The list of maturing repo's.
	 */
	private List<LendingRepo> maturingRepoList = new ArrayList<>();

	/**
	 * The list of pair off groups being replaced.
	 */
	private List<String> removeGroupList;

	/**
	 * When saving, delete any existing pair where the opposite side is NULL.
	 */
	private boolean overrideSinglePairs = true;

	/**
	 * The date of the pair off, this will be the date that the confirm prints.
	 */
	private Date pairOffDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<LendingRepo> getOpeningRepoList() {
		return this.openingRepoList;
	}


	public void setOpeningRepoList(List<LendingRepo> openingRepoList) {
		this.openingRepoList = openingRepoList;
	}


	public List<LendingRepo> getMaturingRepoList() {
		return this.maturingRepoList;
	}


	public void setMaturingRepoList(List<LendingRepo> maturingRepoList) {
		this.maturingRepoList = maturingRepoList;
	}


	public List<String> getRemoveGroupList() {
		return this.removeGroupList;
	}


	public void setRemoveGroupList(List<String> removeGroupList) {
		this.removeGroupList = removeGroupList;
	}


	public boolean isOverrideSinglePairs() {
		return this.overrideSinglePairs;
	}


	public void setOverrideSinglePairs(boolean overrideSinglePairs) {
		this.overrideSinglePairs = overrideSinglePairs;
	}


	public Date getPairOffDate() {
		return this.pairOffDate;
	}


	public void setPairOffDate(Date pairOffDate) {
		this.pairOffDate = pairOffDate;
	}
}
