package com.clifton.dw.rebuild;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlStoredProcedureCommand;
import com.clifton.core.dataaccess.sql.SqlUpdateCommand;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.concurrent.LockBusyException;
import com.clifton.core.util.concurrent.synchronize.SynchronizableBuilder;
import com.clifton.core.util.concurrent.synchronize.handler.SynchronizationHandler;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.calculator.accrual.AccrualDatesCommand;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.MarketDataService;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.clifton.core.util.date.DateUtils.fromDate;
import static com.clifton.core.util.status.Status.ofMessage;


/**
 * The <code>DwRebuildServiceImpl</code> class provides basic implementation of DwRebuildService interface.
 * Uses our services to calculate snapshot measures and SQL based updates (no Hibernate).
 *
 * @author vgomelsky
 */
@Service
public class DwRebuildServiceImpl implements DwRebuildService {

	public static final String DW_DATA_SOURCE = "dwDataSource";

	private AccountingPositionService accountingPositionService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataService marketDataService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private SecurityAuthorizationService securityAuthorizationService;

	private SqlHandler dwSqlHandler;
	private SynchronizationHandler synchronizationHandler;
	private RunnerHandler runnerHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void rebuildDimensions() {
		getDwSqlHandler().executeStoredProcedure(SqlStoredProcedureCommand.forProcedure("etl.DimensionTables_Rebuild"));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void rebuildAccountingSnapshotsForClientAccount(int clientAccountId, Date snapshotDate) {
		InvestmentAccount clientAccount = getInvestmentAccountService().getInvestmentAccount(clientAccountId);
		doRebuildAccountingSnapshotsForClientAccountWithLock(clientAccount, snapshotDate, new DwRebuildContext(getMarketDataExchangeRatesApiService(), DwRebuildCommand.UpdateModes.DELETE_INSERT));
	}


	@Override
	public Status rebuildAccountingSnapshots(final DwRebuildCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Synchronously running for " + command);
			doRebuildAccountingSnapshots(command, status);
			return status;
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("DW-SNAPSHOTS", runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					doRebuildAccountingSnapshots(command, statusHolder.getStatus());
				}
				catch (Throwable e) {
					getStatus().setMessage("Error rebuilding snapshots for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding snapshots for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);

		return Status.ofMessage("Started snapshots rebuild: " + runId + ". Processing will be completed shortly.");
	}


	////////////////////////////////////////////////////////////////////////////
	/////////                       Helper Methods                    //////////
	////////////////////////////////////////////////////////////////////////////


	private void doRebuildAccountingSnapshotsForClientAccountWithLock(InvestmentAccount clientAccount, Date snapshotDate, DwRebuildContext rebuildContext) {
		String lockMessage = DateUtils.fromDateShort(snapshotDate) + " rebuild for " + clientAccount.getLabel();
		String lockKey = clientAccount.getId() + '-' + DateUtils.fromDateShort(snapshotDate);

		getSynchronizationHandler().execute(SynchronizableBuilder.forLocking("DW-REBUILD", lockMessage, lockKey)
				.withMillisecondsToWaitIfBusy(2000)
				.buildWithExecutableAction(() -> doRebuildAccountingSnapshotsForClientAccount(clientAccount, snapshotDate, rebuildContext)));
	}


	private void doRebuildAccountingSnapshotsForClientAccount(InvestmentAccount clientAccount, Date snapshotDate, DwRebuildContext rebuildContext) {
		try {
			// 1. rebuild lot level cash and security positions
			Set<InvestmentSecurity> snapshotSecurities = rebuildAccountingPositionSnapshotForClientAccount(clientAccount, snapshotDate, rebuildContext);

			// if security was rebuilt for a different client, don't rebuild it again
			Set<InvestmentSecurity> newSecuritiesToRebuild = new HashSet<>();
			for (InvestmentSecurity security : snapshotSecurities) {
				if (rebuildContext.getMarketDataSecurities().add(security)) {
					newSecuritiesToRebuild.add(security);
				}
			}

			// 2. rebuild market data values for securities that have positions
			rebuildMarketDataValueSnapshot(snapshotDate, newSecuritiesToRebuild, rebuildContext);

			// 3. use stored procedure for AccountingBalanceSnapshot rebuild: fairly simple process that crunches a lot of data and needs to be fast
			doRebuildAccountingBalanceSnapshotForClientOnDate(clientAccount.getId(), snapshotDate, rebuildContext.getBalanceSnapshotUpdateMode());
		}
		catch (Throwable e) {
			if (CoreExceptionUtils.isValidationException(e)) {
				throw new ValidationException("Validation Error in doRebuildAccountingSnapshotsForClientAccount for " + clientAccount + " on " + DateUtils.fromDateShort(snapshotDate), e);
			}
			throw new RuntimeException("Error in doRebuildAccountingSnapshotsForClientAccount for " + clientAccount + " on " + DateUtils.fromDateShort(snapshotDate), e);
		}
	}


	@Transactional(timeout = 120)
	protected void doRebuildAccountingBalanceSnapshotForClientOnDate(int clientAccountId, Date snapshotDate, DwRebuildCommand.UpdateModes updateMode) {
		// using parameters can significantly improve performance (cached execution plan)
		LinkedHashMap<String, Object> params = new LinkedHashMap<>();
		params.put("ClientInvestmentAccountID", clientAccountId);
		params.put("SnapshotDate", snapshotDate);
		if (updateMode != null) {
			params.put("UpdateMode", updateMode.getUpdateMode());
		}

		getDwSqlHandler().executeStoredProcedure(SqlStoredProcedureCommand.forProcedure("etl.AccountingBalanceSnapshot_RebuildForClientOnDate").withParameters(params));
	}


	private void doRebuildAccountingSnapshots(DwRebuildCommand command, Status status) {
		// Validates Start/End Snapshot Dates and If Rebuilding Today or in the Future and global rebuild limites
		command.validate(getCalendarBusinessDayService(), getSecurityAuthorizationService());


		// get client accounts being rebuilt
		List<InvestmentAccount> accountList;
		if (command.getClientAccountId() == null) { // all accounts or group
			InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
			if (command.getInvestmentAccountGroupId() != null) {
				searchForm.setInvestmentAccountGroupId(command.getInvestmentAccountGroupId());
			}
			searchForm.setOurAccount(true);
			searchForm.setOrderBy("number"); // so that it's easy to see what was processed so far
			accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		}
		else {
			accountList = new ArrayList<>();
			accountList.add(getInvestmentAccountService().getInvestmentAccount(command.getClientAccountId()));
		}
		int totalAccounts = CollectionUtils.getSize(accountList);
		int processedSnapshots = 0;

		Date startDate = command.getStartSnapshotDate();
		Date endDate = command.getEndSnapshotDate();
		Date firstAvailableDate = DateUtils.toDate("12/31/2006"); // no transaction level data exists prior to 2007
		if (startDate.before(firstAvailableDate)) {
			startDate = firstAvailableDate;
		}
		// rebuild one snapshot at a time for one or all client accounts (optimizes price look ups)
		DwRebuildContext rebuildContext = new DwRebuildContext(getMarketDataExchangeRatesApiService(), command.getBalanceSnapshotUpdateMode());

		boolean dimensionsRebuilt = false;

		// start with monthly snapshots, and start with first available date so we get from inception
		Date snapshotDate = firstAvailableDate;
		if (command.isBuildEachMonthEnd()) {
			while (DateUtils.compare(snapshotDate, endDate, false) <= 0) {
				for (InvestmentAccount clientAccount : accountList) {
					Date inceptionDate = getClientAccountInceptionDate(clientAccount, snapshotDate);
					if (inceptionDate == null) {
						status.addSkipped("Skipped rebuild on " + DateUtils.fromDateShort(snapshotDate) + " because Inception Date (Positions On Date) is not set for " + clientAccount.getLabel());
					}
					else if (DateUtils.compare(inceptionDate, snapshotDate, false) > 0) {
						status.addSkipped("Skipped rebuild on " + DateUtils.fromDateShort(snapshotDate) + " because Inception Date (Positions On Date) is in the future for " + clientAccount.getLabel());
					}
					else {
						// continue processing if there's an error during a single account rebuild
						try {
							doRebuildAccountingSnapshotsForClientAccountWithLock(clientAccount, snapshotDate, rebuildContext);
						}
						catch (Throwable e) {
							dimensionsRebuilt = handleRebuildException(e, status, dimensionsRebuilt, clientAccount, snapshotDate, rebuildContext);
						}
						processedSnapshots++;
						status.setMessage("Accounts: " + totalAccounts + "; Processed: " + processedSnapshots + " Snapshots; Errors: " + status.getErrorCount() + "; Warnings: " + status.getWarningCount() + "; Skipped: " + status.getSkippedCount() + "; Last: " + clientAccount.getNumber() + " on " + DateUtils.fromDateShort(snapshotDate));
					}
				}
				rebuildContext.clear(false);
				snapshotDate = DateUtils.getLastDayOfMonth(DateUtils.addMonths(snapshotDate, 1));
			}
		}

		// For daily rebuilds, only rebuild within date range
		// Prevent starting on a weekend that isn't month end by moving date back one day and getting next snapshot date
		snapshotDate = getNextSnapshotDate(DateUtils.addDays(startDate, -1));
		while (DateUtils.compare(snapshotDate, endDate, false) <= 0) {
			// don't rebuild the same snapshot twice
			if (!(command.isBuildEachMonthEnd() && DateUtils.isLastDayOfMonth(snapshotDate))) {
				rebuildContext.clear(false);
				for (InvestmentAccount clientAccount : accountList) {
					Date inceptionDate = getClientAccountInceptionDate(clientAccount, snapshotDate);
					if (inceptionDate == null) {
						status.addSkipped("Skipped rebuild on " + DateUtils.fromDateShort(snapshotDate) + " because Inception Date (Positions On Date) is not set for " + clientAccount.getLabel());
					}
					else if (DateUtils.compare(inceptionDate, snapshotDate, false) > 0) {
						status.addSkipped("Skipped rebuild on " + DateUtils.fromDateShort(snapshotDate) + " because Inception Date (Positions On Date) is in the future for " + clientAccount.getLabel());
					}
					else {
						// continue processing if there's an error during a single account rebuild
						try {
							doRebuildAccountingSnapshotsForClientAccountWithLock(clientAccount, snapshotDate, rebuildContext);
						}
						catch (Throwable e) {
							dimensionsRebuilt = handleRebuildException(e, status, dimensionsRebuilt, clientAccount, snapshotDate, rebuildContext);
						}
						processedSnapshots++;
						status.setMessage("Accounts: " + totalAccounts + "; Processed: " + processedSnapshots + " Snapshots; Errors: " + status.getErrorCount() + "; Warnings: " + status.getWarningCount() + "; Skipped: " + status.getSkippedCount() + "; Last: " + clientAccount.getNumber() + " on " + DateUtils.fromDateShort(snapshotDate));
					}
				}
			}
			snapshotDate = getNextSnapshotDate(snapshotDate);
		}

		// Used for historical rebuild job so we can track how many snapshots were actually rebuilt
		command.setResultProcessedSnapshots(command.getResultProcessedSnapshots() + processedSnapshots);
		int errorCount = status.getErrorCount();
		int warningCount = status.getWarningCount();
		int skippedCount = status.getSkippedCount();
		if (errorCount > 0) {
			status.setMessage("Completed with Errors. Accounts: " + totalAccounts + "; Snapshots: " + processedSnapshots + "; Errors: " + errorCount + "; Warnings: " + warningCount + "; Skipped: " + skippedCount);
		}
		else if (warningCount > 0) {
			status.setMessage("Completed with Warnings. Accounts: " + totalAccounts + "; Snapshots: " + processedSnapshots + "; Warnings: " + warningCount + "; Skipped: " + skippedCount);
		}
		else if (skippedCount > 0) {
			status.setMessage("Completed with Skipped Accounts. Accounts: " + totalAccounts + "; Snapshots: " + processedSnapshots + "; Skipped: " + skippedCount);
		}
		else {
			status.setMessage("Completed Successfully. Accounts: " + totalAccounts + "; Snapshots: " + processedSnapshots);
		}
	}


	/**
	 * Returns the next snapshot date for a given date.
	 * Includes all Weekdays, and Weekend Date ONLY if it's Month End
	 */
	private Date getNextSnapshotDate(Date snapshotDate) {
		// Move to Next Week Day: NEVER SKIP THE LAST DAY OF THE MONTH (used for YTD, QTD, MTD reporting)
		Date nextDate = DateUtils.getNextWeekday(snapshotDate);
		if (DateUtils.getMonthOfYear(snapshotDate) != DateUtils.getMonthOfYear(nextDate)) {
			// moved to new month: if not from last date, then move to last date
			if (!DateUtils.isLastDayOfMonth(snapshotDate)) {
				nextDate = DateUtils.getLastDayOfMonth(snapshotDate);
			}
		}
		return nextDate;
	}


	private boolean handleRebuildException(Throwable e, Status status, boolean dimensionsRebuilt, InvestmentAccount clientAccount, Date snapshotDate, DwRebuildContext rebuildContext) {
		// if the error is due to out-dated dimensions, then try rebuilding dimensions first and then rerun last step
		String originalMessage = ExceptionUtils.getOriginalMessage(e);
		if (!dimensionsRebuilt && originalMessage != null && originalMessage.contains("statement conflicted with the FOREIGN KEY constraint")) {
			// sample error message: The INSERT statement conflicted with the FOREIGN KEY constraint "FK_AccountingPositionSnapshot_InvestmentSecurityDimension_InvestmentSecurityID".
			dimensionsRebuilt = true;
			try {
				rebuildDimensions();
				doRebuildAccountingSnapshotsForClientAccountWithLock(clientAccount, snapshotDate, rebuildContext);
			}
			catch (LockBusyException lockBusyException) {
				LogUtils.warn(getClass(), lockBusyException.getMessage(), lockBusyException);
				status.addWarning(lockBusyException.getMessage());
			}
			catch (Throwable ex) {
				LogUtils.errorOrInfo(LogCommand.ofThrowableAndMessage(getClass(), ex, "Error in doRebuildAccountingSnapshots after dimensions were rebuilt for " + clientAccount + " on " + DateUtils.fromDateShort(snapshotDate)));
				status.addError("Client Account " + clientAccount.getNumber() + ": " + ExceptionUtils.getDetailedMessage(ex));
			}
		}
		else if (e instanceof LockBusyException) {
			LogUtils.warn(getClass(), e.getMessage(), e);
			status.addWarning(e.getMessage());
		}
		else {
			LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Error in doRebuildAccountingSnapshots for " + clientAccount + " on " + DateUtils.fromDateShort(snapshotDate)));
			status.addError("Client Account " + clientAccount.getNumber() + ": " + ExceptionUtils.getDetailedMessage(e));
			// NOTE: should we fail if there are too many errors? 20+?
		}
		return dimensionsRebuilt;
	}


	/**
	 * Returns "Inception Date" of the specified investment account. If it's not defined but the account
	 * has positions on Snapshot Date, then returns Snapshot Date.  Otherwise returns null.
	 */
	private Date getClientAccountInceptionDate(InvestmentAccount clientAccount, Date snapshotDate) {
		Date inceptionDate = clientAccount.getInceptionDate();
		if (inceptionDate == null) {
			// this should be very infrequent
			List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForClientAccount(clientAccount.getId(), snapshotDate);
			if (!CollectionUtils.isEmpty(positionList)) {
				// still rebuild but also log a warning
				LogUtils.warn(getClass(), "Inception Date is not defined for Client Account with positions: " + clientAccount.getLabel());
				inceptionDate = snapshotDate;
			}
		}
		return inceptionDate;
	}


	/**
	 * Uses Maps to cash looked up date for improved performance.  Returns a Set of securities used for the snapshot.
	 */
	private Set<InvestmentSecurity> rebuildAccountingPositionSnapshotForClientAccount(InvestmentAccount clientAccount, Date snapshotDate, DwRebuildContext rebuildContext) {
		Set<InvestmentSecurity> result = new HashSet<>();
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForClientAccount(clientAccount.getId(), snapshotDate);
		int positionLotCount = CollectionUtils.getSize(positionList);
		final List<RawAccountingPositionSnapshot> snapshotList = new ArrayList<>(positionLotCount);

		for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
			RawAccountingPositionSnapshot snapshot = new RawAccountingPositionSnapshot();
			snapshot.setSnapshotDate(snapshotDate);
			snapshot.setAccountingTransactionId(position.getId());
			snapshot.setClientInvestmentAccountId(position.getClientInvestmentAccount().getId());
			snapshot.setHoldingInvestmentAccountId(position.getHoldingInvestmentAccount().getId());
			snapshot.setInvestmentSecurityId(position.getInvestmentSecurity().getId());
			snapshot.setAccountingAccountId(position.getAccountingAccount().getId());

			InvestmentSecurity security = getInvestmentSecurity(snapshot.getInvestmentSecurityId(), rebuildContext.getSecurityMap());
			result.add(security);

			snapshot.setQuantity(position.getRemainingQuantity());
			snapshot.setSnapshotPrice(getPrice(security, snapshotDate, position.getPrice(), rebuildContext));
			snapshot.setSnapshotExchangeRateToBase(rebuildContext.getFxRateLookupCache().getExchangeRate(position.fxSourceCompany(), position.fxSourceCompanyOverridden(), security.getInstrument().getTradingCurrency().getSymbol(), clientAccount.getBaseCurrency().getSymbol(), snapshotDate, true));
			snapshot.setLocalCostBasis(position.getRemainingCostBasis());
			if (InvestmentUtils.isNoPaymentOnOpen(security)) {
				// Debit/Credit = 0 and Cost Basis is only stored in Local currency: use opening FX to calculate base Cost Basis
				snapshot.setLocalCost(BigDecimal.ZERO);
				snapshot.setBaseCost(BigDecimal.ZERO);
				// Unrealized = Base Notional - Base Cost Basis
				// Need to use Snapshot FX for base cost basis calculation in order to get accurate gain/loss = Snapshot FX * (Local Notional - Local Cost Basis)
				snapshot.setBaseCostBasis(MathUtils.multiply(position.getRemainingCostBasis(), snapshot.getSnapshotExchangeRateToBase(), 2));
			}
			else {
				snapshot.setLocalCost(position.getRemainingCostBasis());
				snapshot.setBaseCost(position.getRemainingBaseDebitCredit());
				snapshot.setBaseCostBasis(position.getRemainingBaseDebitCredit());
			}
			// TIPS use quantity (face) adjusted by index ratio; default to snapshot date (convention used by most banks)
			BigDecimal notionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(security, snapshotDate, position.getSettlementDate(), false);
			snapshot.setLocalNotional(getInvestmentCalculator().calculateNotional(security, snapshot.getSnapshotPrice(), snapshot.getQuantity(), notionalMultiplier));

			AccrualBasis accrualBasis = AccrualBasis.forSecurity(security, snapshot.getLocalCostBasis(), snapshot.getQuantity());
			AccrualDates accrualDates = getInvestmentCalculator().calculateAccrualDates(AccrualDatesCommand.of(position, snapshotDate));
			snapshot.setLocalAccrual1(getInvestmentCalculator().calculateAccruedInterestForEvent1(security, accrualBasis.getAccrualBasis1(), notionalMultiplier, accrualDates.getAccrueUpToDate1(), accrualDates.getAccrueFromDate1()));
			snapshot.setLocalAccrual2(getInvestmentCalculator().calculateAccruedInterestForEvent2(security, accrualBasis.getAccrualBasis2(), notionalMultiplier, accrualDates.getAccrueUpToDate2(), accrualDates.getAccrueFromDate2()));
			snapshotList.add(snapshot);
		}

		// delete and then insert the list if not empty
		// NOTE: do not use prepared statement with parameters for delete because it can be much slower sometimes times out
		getDwSqlHandler().executeUpdate("DELETE FROM AccountingPositionSnapshot WHERE ClientInvestmentAccountId = " + clientAccount.getId() + " AND SnapshotDate = '" + fromDate(snapshotDate, DateUtils.DATE_FORMAT_SQL) + "'");

		if (positionLotCount > 0) {
			String sql = "INSERT INTO AccountingPositionSnapshot(AccountingTransactionID, ClientInvestmentAccountID, HoldingInvestmentAccountID, InvestmentSecurityID, AccountingAccountID, " //
					+ "Quantity, SnapshotPrice, SnapshotExchangeRateToBase, LocalCost, BaseCost, LocalCostBasis, BaseCostBasis, LocalNotional, LocalAccrual1, LocalAccrual2, SnapshotDate) " //
					+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			final java.sql.Date sqlSnapshotDate = new java.sql.Date(snapshotDate.getTime());
			final int batchCount = positionLotCount;
			getDwSqlHandler().executeBatchUpdate(sql, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					RawAccountingPositionSnapshot snapshot = snapshotList.get(i);
					ps.setLong(1, snapshot.getAccountingTransactionId());
					ps.setInt(2, snapshot.getClientInvestmentAccountId());
					ps.setInt(3, snapshot.getHoldingInvestmentAccountId());
					ps.setInt(4, snapshot.getInvestmentSecurityId());
					ps.setInt(5, snapshot.getAccountingAccountId());

					ps.setBigDecimal(6, snapshot.getQuantity());
					ps.setBigDecimal(7, snapshot.getSnapshotPrice());
					ps.setBigDecimal(8, snapshot.getSnapshotExchangeRateToBase());

					ps.setBigDecimal(9, snapshot.getLocalCost());
					ps.setBigDecimal(10, snapshot.getBaseCost());
					ps.setBigDecimal(11, snapshot.getLocalCostBasis());
					ps.setBigDecimal(12, snapshot.getBaseCostBasis());
					ps.setBigDecimal(13, snapshot.getLocalNotional());

					ps.setBigDecimal(14, snapshot.getLocalAccrual1());
					ps.setBigDecimal(15, snapshot.getLocalAccrual2());

					ps.setDate(16, sqlSnapshotDate);
				}


				@Override
				public int getBatchSize() {
					return batchCount;
				}
			});
		}
		return result;
	}


	private void rebuildMarketDataValueSnapshot(Date snapshotDate, Set<InvestmentSecurity> marketDataSecurities, DwRebuildContext rebuildContext) {
		final List<MarketDataValueSnapshot> snapshotList = new ArrayList<>(marketDataSecurities.size());
		for (InvestmentSecurity security : marketDataSecurities) {
			if (DateUtils.isDateBetween(snapshotDate, security.getStartDate(), security.getLastPositionDate(), true)) {
				MarketDataValueSnapshot snapshot = new MarketDataValueSnapshot();
				snapshot.setInvestmentSecurityId(security.getId());
				snapshot.setSnapshotDate(snapshotDate);
				snapshot.setDv01(BigDecimal.ZERO);
				snapshot.setDv01Spot(BigDecimal.ZERO);
				snapshot.setCouponRate(BigDecimal.ZERO);
				snapshot.setCurrentFactor(BigDecimal.ONE);
				snapshot.setIndexRatio(BigDecimal.ONE);

				snapshot.setPrice(getPrice(security, snapshotDate, null, rebuildContext));
				snapshot.setDirtyPrice(snapshot.getPrice());

				// fixed income security fields
				InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
				if (hierarchy.getNameExpanded().startsWith("Fixed Income")) {
					snapshot.setDuration(getMarketDataRetriever().getDurationFlexible(security, snapshotDate, false));
					snapshot.setDv01(getMarketDataRetriever().getDV01Flexible(security, snapshotDate, false));
					snapshot.setDv01Spot(getMarketDataRetriever().getDV01SpotFlexible(security, snapshotDate, false));
				}

				// TIPS specific fields
				if (hierarchy.isIndexRatioAdjusted()) {
					Date settlementDate = snapshotDate; // default to snapshot date (convention used by most banks)
					if (DateUtils.compare(security.getStartDate(), settlementDate, false) == 0) {
						// cannot be before issue settlement date
						settlementDate = getInvestmentCalculator().calculateSettlementDate(security, null, snapshotDate);
					}
					snapshot.setIndexRatio(getMarketDataService().getMarketDataIndexRatioForSecurity(security.getId(), settlementDate, true));
				}

				InvestmentSecurityEventType factorEventType = hierarchy.getFactorChangeEventType();
				if (factorEventType != null) {
					InvestmentSecurityEvent factorEvent = getInvestmentSecurityEventService().getInvestmentSecurityEventWithLatestExDate(security.getId(), factorEventType.getName(), snapshotDate);
					if (factorEvent != null) {
						snapshot.setCurrentFactor(factorEvent.getAfterEventValue());
					}
				}

				// Cash Coupon Payments for bonds, Interest Leg payments for total return swaps
				String paymentEventType = InvestmentUtils.getAccrualEventTypeName(security);
				if (paymentEventType != null) {
					InvestmentSecurityEvent paymentEvent = getInvestmentSecurityEventService().getInvestmentSecurityEventForAccrualEndDate(security.getId(), paymentEventType, snapshotDate);
					if (paymentEvent != null) {
						snapshot.setCouponRate(paymentEvent.getAfterEventValue());
						snapshot.setPreviousAccrualEndDate(paymentEvent.getAccrualStartDate());
						snapshot.setCurrentAccrualEndDate(paymentEvent.getAccrualEndDate());

						// Dirty Price calculation is dependent for some cases (IRS/CDS because they use discount convention for the notional calculator) on the position held (long/short)
						// Currently, for the Snapshot, we will default to the Long Value only and pass in null for the quantity to use default long quantity of 1 Billion (sizable face to calculate accrued interest against)
						snapshot.setDirtyPrice(getInvestmentCalculator().calculateDirtyPrice(security, null, snapshot.getPrice(), snapshotDate));
					}
				}

				snapshotList.add(snapshot);
			}
		}

		// delete and then insert the list if not empty
		if (!snapshotList.isEmpty()) {
			deleteThenInsertMarketDataValueSnapshots(snapshotList, snapshotDate);
		}
	}


	@Transactional
	protected void deleteThenInsertMarketDataValueSnapshots(List<MarketDataValueSnapshot> snapshotList, Date snapshotDate) {
		String securityIds = BeanUtils.getPropertyValues(snapshotList, "investmentSecurityId", ",");
		getDwSqlHandler().executeUpdate(new SqlUpdateCommand("DELETE FROM MarketDataValueSnapshot WHERE SnapshotDate = ? AND InvestmentSecurityID IN (" + securityIds + ")")
				.addDateParameterValue(snapshotDate)
		);

		String sql = "INSERT INTO MarketDataValueSnapshot(InvestmentSecurityID, SnapshotDate, Price, DirtyPrice, " //
				+ "Duration, DV01, DV01Spot, IndexRatio, CouponRate, CurrentFactor, PreviousAccrualEndDate, CurrentAccrualEndDate) " //
				+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		final java.sql.Date sqlSnapshotDate = new java.sql.Date(snapshotDate.getTime());
		getDwSqlHandler().executeBatchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				MarketDataValueSnapshot snapshot = snapshotList.get(i);
				ps.setInt(1, snapshot.getInvestmentSecurityId());
				ps.setDate(2, sqlSnapshotDate);

				ps.setBigDecimal(3, snapshot.getPrice());
				ps.setBigDecimal(4, snapshot.getDirtyPrice());
				ps.setBigDecimal(5, snapshot.getDuration());
				ps.setBigDecimal(6, snapshot.getDv01());
				ps.setBigDecimal(7, snapshot.getDv01Spot());
				ps.setBigDecimal(8, snapshot.getIndexRatio());
				ps.setBigDecimal(9, snapshot.getCouponRate());
				ps.setBigDecimal(10, snapshot.getCurrentFactor());
				ps.setDate(11, snapshot.getPreviousAccrualEndDate() == null ? null : new java.sql.Date(snapshot.getPreviousAccrualEndDate().getTime()));
				ps.setDate(12, snapshot.getCurrentAccrualEndDate() == null ? null : new java.sql.Date(snapshot.getCurrentAccrualEndDate().getTime()));
			}


			@Override
			public int getBatchSize() {
				return snapshotList.size();
			}
		});
	}


	private InvestmentSecurity getInvestmentSecurity(int investmentSecurityId, Map<Integer, InvestmentSecurity> securityMap) {
		InvestmentSecurity result = securityMap.get(investmentSecurityId);
		if (result == null) {
			result = getInvestmentInstrumentService().getInvestmentSecurity(investmentSecurityId);
			ValidationUtils.assertNotNull(result, "Cannot find investment security for id = " + investmentSecurityId);
			securityMap.put(investmentSecurityId, result);
		}
		return result;
	}


	/**
	 * Returns the price for the specified security on the specified date. Caches the price for faster future performance.
	 * Optional lot openingPrice will be used if not null and market data price is not available.
	 */
	private BigDecimal getPrice(InvestmentSecurity security, Date snapshotDate, BigDecimal openingPrice, DwRebuildContext rebuildContext) {
		BigDecimal result = rebuildContext.getSecurityPrice(security.getId());
		if (result == null) {
			result = getMarketDataRetriever().getPriceFlexible(security, snapshotDate, false);
			if (result == null) {
				result = openingPrice;
				if (result == null) {
					result = rebuildContext.getSecurityTradePrice(security.getId());
				}
				else {
					rebuildContext.cacheSecurityTradePrice(security.getId(), result);
				}
				ValidationUtils.assertNotNull(result, "Cannot find price for investment security " + security + " on " + snapshotDate);
			}
			else {
				rebuildContext.cacheSecurityPrice(security.getId(), result);
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataService getMarketDataService() {
		return this.marketDataService;
	}


	public void setMarketDataService(MarketDataService marketDataService) {
		this.marketDataService = marketDataService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public SqlHandler getDwSqlHandler() {
		return this.dwSqlHandler;
	}


	public void setDwSqlHandler(SqlHandler dwSqlHandler) {
		this.dwSqlHandler = dwSqlHandler;
	}


	public SynchronizationHandler getSynchronizationHandler() {
		return this.synchronizationHandler;
	}


	public void setSynchronizationHandler(SynchronizationHandler synchronizationHandler) {
		this.synchronizationHandler = synchronizationHandler;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
