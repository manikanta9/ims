package com.clifton.dw.rebuild;


import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>RawAccountingBalanceSnapshot</code> class represents Data Warehouse Fact table entity that
 * keeps local and base GL balances on snapshot date. This snapshot is used to significantly simplify
 * reporting for various income statement and balance sheet requests.
 * <p/>
 * Realized portion of this snapshot is built from the general ledger (AccountingTransaction) by aggregating
 * Debit and Credit balances for the specified dimensions.
 * <p/>
 * Unrealized portion of the snapshot (Unrealized Gain/Loss, Accruals, Currency Translation Gain/Loss.) is added from existing positions on the
 * snapshot date from AccountingPositionSnapshot table.
 * <p/>
 * In order to improve performance, this table can be rebuilt from the 2 tables above in a stored procedure.
 *
 * @author vgomelsky
 */
public class RawAccountingBalanceSnapshot {

	private int clientInvestmentAccountId;
	private int holdingInvestmentAccountId;
	private int investmentSecurityId;
	private short accountingAccountId;

	private BigDecimal localDebitCreditBalance;
	private BigDecimal baseDebitCreditBalance;
	private Date snapshotDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(int clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public int getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(int holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public int getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(int investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public BigDecimal getLocalDebitCreditBalance() {
		return this.localDebitCreditBalance;
	}


	public void setLocalDebitCreditBalance(BigDecimal localDebitCreditBalance) {
		this.localDebitCreditBalance = localDebitCreditBalance;
	}


	public BigDecimal getBaseDebitCreditBalance() {
		return this.baseDebitCreditBalance;
	}


	public void setBaseDebitCreditBalance(BigDecimal baseDebitCreditBalance) {
		this.baseDebitCreditBalance = baseDebitCreditBalance;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}
}
