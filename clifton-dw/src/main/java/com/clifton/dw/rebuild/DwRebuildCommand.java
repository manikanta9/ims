package com.clifton.dw.rebuild;


import com.clifton.accounting.gl.AccountingSnapshotRebuildCommand;
import com.clifton.core.util.date.DateUtils;


public class DwRebuildCommand extends AccountingSnapshotRebuildCommand {


	// Note: Client Account and Account Group Fields are standard on the preceding command object

	private boolean buildEachMonthEnd;
	private UpdateModes balanceSnapshotUpdateMode;


	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////

	/**
	 * Defines different options for how a rebuild should be performed.
	 * Different choices may effect performance and concurrency.
	 */
	public enum UpdateModes {
		/**
		 * delete all old data and then insert new data (usually fastest but may result in unnecessary index rebuilds)
		 */
		DELETE_INSERT(1),
		/**
		 * inset/update/delete only when necessary (uses CURSOR based implementation: usually faster)
		 */
		SMART_UPDATE_WITH_CURSOR(2),
		/**
		 * inset/update/delete only when necessary (uses 3 separate queries: usually slower than CURSOR based implementation)
		 */
		SMART_UPDATE(3);


		UpdateModes(int updateMode) {
			this.updateMode = updateMode;
		}


		private int updateMode;


		public int getUpdateMode() {
			return this.updateMode;
		}
	}


	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	public String getRunId() {
		StringBuilder runId = new StringBuilder("");
		if (getClientAccountId() != null) {
			runId.append("Acct_").append(getClientAccountId());
		}
		else if (getInvestmentAccountGroupId() != null) {
			runId.append("AcctGroup_").append(getInvestmentAccountGroupId());
		}
		else {
			runId.append("ALL");
		}
		runId.append("-").append(DateUtils.fromDateShort(getStartSnapshotDate()));
		runId.append("-").append(DateUtils.fromDateShort(getEndSnapshotDate()));
		if (isBuildEachMonthEnd()) {
			runId.append("-ME");
		}
		return runId.toString();
	}


	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	public boolean isBuildEachMonthEnd() {
		return this.buildEachMonthEnd;
	}


	public void setBuildEachMonthEnd(boolean buildEachMonthEnd) {
		this.buildEachMonthEnd = buildEachMonthEnd;
	}


	public UpdateModes getBalanceSnapshotUpdateMode() {
		return this.balanceSnapshotUpdateMode;
	}


	public void setBalanceSnapshotUpdateMode(UpdateModes balanceSnapshotUpdateMode) {
		this.balanceSnapshotUpdateMode = balanceSnapshotUpdateMode;
	}
}
