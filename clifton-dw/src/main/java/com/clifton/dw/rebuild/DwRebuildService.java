package com.clifton.dw.rebuild;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;


/**
 * The <code>DwRebuildService</code> interface defines methods that rebuild DW tables.
 * Because DW rebuild process is relatively slow (needs to process a lot of data), various
 * method signatures are available to rebuilt portions of the data.
 *
 * @author vgomelsky
 */
public interface DwRebuildService {

	/**
	 * Rebuilds all "Dimension" tables in the Data Warehouse
	 */
	@RequestMapping("dwDimensionsRebuild")
	@SecureMethod(dtoClass = AccountingPositionDaily.class)
	public void rebuildDimensions();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Rebuilds (delete/insert) all snapshots (AccountingPositionSnapshot, AccountingBalanceSnapshot, etc.)
	 * for the specified client investment account on the specified date.
	 */
	@RequestMapping("dwAccountingSnapshotsForClientAccountRebuild")
	@SecureMethod(dtoClass = AccountingPositionDaily.class)
	public void rebuildAccountingSnapshotsForClientAccount(int clientAccountId, Date snapshotDate);


	/**
	 * Rebuilds (delete/insert) all snapshots (AccountingPositionSnapshot, AccountingBalanceSnapshot, etc.)
	 * for the specified arguments.
	 * <p/>
	 * NOTE: asynchronous execution because this may take a while
	 * <p/>
	 * The following config parameters are available:
	 * clientAccountId - optional: set to null to rebuild all client accounts
	 * investmentAccountGroupId - optional: set to null to rebuild all client accounts, or will rebuild for accounts in the specified group (ignored if clientAccountId is set)
	 * dailySnapshotCount - optionally build the following number of daily snapshots up to startDate
	 * startDate - start date to build daily snapshots for (1 for up to yesterday)
	 * buildEachMonthEnd - create a snapshot for the last day of each month starting from account's inception
	 * synchronous - specifies whether this execution should be performed asynchronously
	 *
	 * @return message with execution status (number of accounts, snapshots, etc.)
	 */
	@ModelAttribute("result")
	@RequestMapping("dwAccountingSnapshotsRebuild")
	@SecureMethod(dtoClass = AccountingPositionDaily.class)
	public Status rebuildAccountingSnapshots(DwRebuildCommand command);
}
