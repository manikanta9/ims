package com.clifton.dw.rebuild;

import com.clifton.accounting.gl.position.daily.SnapshotRebuildContext;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;


/**
 * @author vgomelsky
 */
public class DwRebuildContext extends SnapshotRebuildContext {

	private DwRebuildCommand.UpdateModes balanceSnapshotUpdateMode;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DwRebuildContext(MarketDataExchangeRatesApiService apiService, DwRebuildCommand.UpdateModes balanceSnapshotUpdateMode) {
		super(apiService);
		this.balanceSnapshotUpdateMode = balanceSnapshotUpdateMode;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DwRebuildCommand.UpdateModes getBalanceSnapshotUpdateMode() {
		return this.balanceSnapshotUpdateMode;
	}


	public void setBalanceSnapshotUpdateMode(DwRebuildCommand.UpdateModes balanceSnapshotUpdateMode) {
		this.balanceSnapshotUpdateMode = balanceSnapshotUpdateMode;
	}
}
