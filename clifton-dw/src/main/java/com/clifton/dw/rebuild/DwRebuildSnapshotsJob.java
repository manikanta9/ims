package com.clifton.dw.rebuild;


import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;

import java.util.Date;
import java.util.Map;


/**
 * The <code>DwRebuildSnapshotsJob</code> class is a batch job that rebuilds Data Warehouse snapshots
 * using the specified parameters.
 *
 * @author Mary Anderson
 */
public class DwRebuildSnapshotsJob implements Task, StatusHolderObjectAware<Status>, ValidationAware {

	private AccountingTransactionService accountingTransactionService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private DwRebuildService dwRebuildService;

	private String startDateGenerationOption;

	private String endDateGenerationOption;

	private StatusHolderObject<Status> statusHolderObject;

	/////////////////////////////////////////////////////////////////////////

	// optional: set to null to rebuild all client accounts, or will rebuild for accounts in the specified group
	private Integer investmentAccountGroupId;

	// dailySnapshotCount - optionally build the following number of daily snapshots up to dailyToDaysFromToday
	private Integer dailySnapshotCount;

	// number of business days from today (in past) to build daily snapshots for (1 for up to yesterday)
	private Integer dailyToDaysFromToday;

	// create a snapshot for the last day of each month starting from account's inception
	private boolean buildEachMonthEnd;

	// Checks Client Accounts that have historical postings done in the past two days for earlier transactions and rebuilds dw for them
	private boolean historicalRebuild;
	// Optionally specifies the maximum number of days from today to start historical rebuilds from
	private Integer historicalRebuildMaxDaysBack;

	private DwRebuildCommand.UpdateModes balanceSnapshotUpdateMode;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolderObject.getStatus();

		// Common properties
		DwRebuildCommand command = new DwRebuildCommand();
		command.setBuildEachMonthEnd(isBuildEachMonthEnd());
		command.setSynchronous(true);
		command.setStatus(status);
		command.setBalanceSnapshotUpdateMode(getBalanceSnapshotUpdateMode());

		if (isHistoricalRebuild()) {
			Map<Integer, Date> clientDateMap = getAccountingTransactionService().getAccountingTransactionHistoricalChangeMap(getDailyToDaysFromToday());
			if (clientDateMap == null || clientDateMap.isEmpty()) {
				status.setMessage("No historical changes found to rebuild Accounting Snapshots for.");
			}
			else {
				int totalAccounts = 0, errorCount = 0;
				Date startDate = null;
				Date minStartDate = null;
				if (getHistoricalRebuildMaxDaysBack() != null) {
					minStartDate = DateUtils.addDays(DateUtils.clearTime(new Date()), -getHistoricalRebuildMaxDaysBack());
				}
				Date endDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -getDailyToDaysFromToday());
				command.setEndSnapshotDate(endDate);
				command.setAllowTodaySnapshots(getDailyToDaysFromToday() == 0); // If rebuilding to today need to explicitly allow this on the rebuild command
				for (Map.Entry<Integer, Date> integerDateEntry : clientDateMap.entrySet()) {
					try {
						command.setClientAccountId(integerDateEntry.getKey());
						startDate = integerDateEntry.getValue();
						if (minStartDate != null && minStartDate.after(startDate)) {
							startDate = minStartDate;
						}
						command.setStartSnapshotDate(startDate);
						getDwRebuildService().rebuildAccountingSnapshots(command);
						totalAccounts++;
						status.setMessage("Total Accounts: " + clientDateMap.size() + ", Processed: " + totalAccounts + ", Failed: " + errorCount + ", Total snapshots processed: " + command.getResultProcessedSnapshots());
					}
					catch (Exception e) {
						errorCount++;
						status.addError("Failed Client Account ID " + integerDateEntry.getKey() + " from " + DateUtils.fromDateShort(startDate) + ": " + ExceptionUtils.getDetailedMessage(e));
						LogUtils.errorOrInfo(getClass(), "Error rebuilding Accounting Snapshots for " + integerDateEntry.getKey() + " from " + DateUtils.fromDateShort(startDate), e);
					}
				}
				status.setMessage("Total Accounts Processed: " + totalAccounts + ", Failed: " + errorCount + ", Total snapshots processed: " + command.getResultProcessedSnapshots());
			}
		}
		else {
			try {
				Date endDate;
				Date startDate;
				if (getStartDateGenerationOption() != null) {
					startDate = getCalendarDateGenerationHandler().generateDate(getStartDateGenerationOption());
					endDate = getCalendarDateGenerationHandler().generateDate(getEndDateGenerationOption());
				}
				else {
					endDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -getDailyToDaysFromToday());
					startDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(endDate), 1 - getDailySnapshotCount());
				}

				command.setStartSnapshotDate(startDate);
				command.setEndSnapshotDate(endDate);
				command.setAllowTodaySnapshots(getDailyToDaysFromToday() == 0); // If rebuilding to today need to explicitly allow this on the rebuild command
				command.setInvestmentAccountGroupId(getInvestmentAccountGroupId());
				command.setStatus(this.statusHolderObject.getStatus());
				getDwRebuildService().rebuildAccountingSnapshots(command);
			}
			catch (Exception e) {
				status.addError(ExceptionUtils.getDetailedMessage(e));
				LogUtils.error(getClass(), "Error rebuilding Accounting Snapshots: " + e.getMessage(), e);
			}
		}

		status.setActionPerformed(command.getResultProcessedSnapshots() != 0);
		return status;
	}


	@Override
	public void validate() throws ValidationException {
		if ((getDailyToDaysFromToday() == null || getDailySnapshotCount() == null) && (getStartDateGenerationOption() == null || getEndDateGenerationOption() == null)) {
			throw new ValidationException("One pair of snapshot window options must be defined: Daily To Days From Today & Daily Snapshot Count, or Start Date Generation Option & End Date Generation Option.");
		}
		else if ((getDailyToDaysFromToday() != null && getDailySnapshotCount() == null) && (getStartDateGenerationOption() != null || getEndDateGenerationOption() != null)) {
			throw new ValidationException("Exactly one pair of snapshot window options must be defined: Daily To Days From Today & Daily Snapshot Count, or Start Date Generation Option & End Date Generation Option.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Integer getDailySnapshotCount() {
		return this.dailySnapshotCount;
	}


	public void setDailySnapshotCount(Integer dailySnapshotCount) {
		this.dailySnapshotCount = dailySnapshotCount;
	}


	public Integer getDailyToDaysFromToday() {
		return this.dailyToDaysFromToday;
	}


	public void setDailyToDaysFromToday(Integer dailyToDaysFromToday) {
		this.dailyToDaysFromToday = dailyToDaysFromToday;
	}


	public boolean isBuildEachMonthEnd() {
		return this.buildEachMonthEnd;
	}


	public void setBuildEachMonthEnd(boolean buildEachMonthEnd) {
		this.buildEachMonthEnd = buildEachMonthEnd;
	}


	public DwRebuildService getDwRebuildService() {
		return this.dwRebuildService;
	}


	public void setDwRebuildService(DwRebuildService dwRebuildService) {
		this.dwRebuildService = dwRebuildService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public boolean isHistoricalRebuild() {
		return this.historicalRebuild;
	}


	public void setHistoricalRebuild(boolean historicalRebuild) {
		this.historicalRebuild = historicalRebuild;
	}


	public Integer getHistoricalRebuildMaxDaysBack() {
		return this.historicalRebuildMaxDaysBack;
	}


	public void setHistoricalRebuildMaxDaysBack(Integer historicalRebuildMaxDaysBack) {
		this.historicalRebuildMaxDaysBack = historicalRebuildMaxDaysBack;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public DwRebuildCommand.UpdateModes getBalanceSnapshotUpdateMode() {
		return this.balanceSnapshotUpdateMode;
	}


	public void setBalanceSnapshotUpdateMode(DwRebuildCommand.UpdateModes balanceSnapshotUpdateMode) {
		this.balanceSnapshotUpdateMode = balanceSnapshotUpdateMode;
	}


	public String getStartDateGenerationOption() {
		return this.startDateGenerationOption;
	}


	public void setStartDateGenerationOption(String startDateGenerationOption) {
		this.startDateGenerationOption = startDateGenerationOption;
	}


	public String getEndDateGenerationOption() {
		return this.endDateGenerationOption;
	}


	public void setEndDateGenerationOption(String endDateGenerationOption) {
		this.endDateGenerationOption = endDateGenerationOption;
	}
}
