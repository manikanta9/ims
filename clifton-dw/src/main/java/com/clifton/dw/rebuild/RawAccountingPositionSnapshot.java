package com.clifton.dw.rebuild;


import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>RawAccountingPositionSnapshot</code> class represents Data Warehouse Fact table entity that
 * stores all positions at lot level on the snapshot date. This snapshot is used to significantly simplify
 * reporting for various position related requests.
 * <p>
 * This snapshot is built from the General Ledger by getting open security lots on the snapshot date.
 * Then various calculations and look ups are performed: get prices and exchange rates on snapshot date,
 * perform accrued interest calculations, etc.
 *
 * @author vgomelsky
 */
public class RawAccountingPositionSnapshot {

	// PK = accountingTransactionId + snapshotDate
	private long accountingTransactionId; // points to the opening lot
	private Date snapshotDate;

	// snapshot dimensions:
	private int clientInvestmentAccountId;
	private int holdingInvestmentAccountId;
	private int investmentSecurityId;
	private short accountingAccountId; // can be used to identify Collateral positions

	// snapshot's calculated/looked-up measures (fields represent remaining as opposed to original values):
	private BigDecimal quantity;
	private BigDecimal snapshotPrice; // latest known price if snapshotDate price is not available; corresponding Big security price if security has a corresponding Big security
	private BigDecimal snapshotExchangeRateToBase;

	private BigDecimal localCost; // cost of futures is 0 and for stocks it's the price paid, etc.
	private BigDecimal baseCost;
	private BigDecimal localCostBasis; // Market Value = Notional - Cost Basis + Cost + Accrual
	private BigDecimal baseCostBasis;
	private BigDecimal localNotional; // Unrealized Gain / Loss = Notional - Cost Basis

	// for securities that support it: Accrued Interest for bonds, set to Zero otherwise
	private BigDecimal localAccrual1;
	private BigDecimal localAccrual2;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(int clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public int getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(int holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public int getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(int investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public long getAccountingTransactionId() {
		return this.accountingTransactionId;
	}


	public void setAccountingTransactionId(long accountingTransactionId) {
		this.accountingTransactionId = accountingTransactionId;
	}


	public BigDecimal getSnapshotPrice() {
		return this.snapshotPrice;
	}


	public void setSnapshotPrice(BigDecimal snapshotPrice) {
		this.snapshotPrice = snapshotPrice;
	}


	public BigDecimal getSnapshotExchangeRateToBase() {
		return this.snapshotExchangeRateToBase;
	}


	public void setSnapshotExchangeRateToBase(BigDecimal snapshotExchangeRateToBase) {
		this.snapshotExchangeRateToBase = snapshotExchangeRateToBase;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getLocalCost() {
		return this.localCost;
	}


	public void setLocalCost(BigDecimal localCost) {
		this.localCost = localCost;
	}


	public BigDecimal getBaseCost() {
		return this.baseCost;
	}


	public void setBaseCost(BigDecimal baseCost) {
		this.baseCost = baseCost;
	}


	public BigDecimal getLocalCostBasis() {
		return this.localCostBasis;
	}


	public void setLocalCostBasis(BigDecimal localCostBasis) {
		this.localCostBasis = localCostBasis;
	}


	public BigDecimal getBaseCostBasis() {
		return this.baseCostBasis;
	}


	public void setBaseCostBasis(BigDecimal baseCostBasis) {
		this.baseCostBasis = baseCostBasis;
	}


	public BigDecimal getLocalNotional() {
		return this.localNotional;
	}


	public void setLocalNotional(BigDecimal localNotional) {
		this.localNotional = localNotional;
	}


	public BigDecimal getLocalAccrual1() {
		return this.localAccrual1;
	}


	public void setLocalAccrual1(BigDecimal localAccrual1) {
		this.localAccrual1 = localAccrual1;
	}


	public BigDecimal getLocalAccrual2() {
		return this.localAccrual2;
	}


	public void setLocalAccrual2(BigDecimal localAccrual2) {
		this.localAccrual2 = localAccrual2;
	}
}
