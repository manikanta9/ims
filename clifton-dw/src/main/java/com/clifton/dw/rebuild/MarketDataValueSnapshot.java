package com.clifton.dw.rebuild;


import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataValueSnapshot</code> class represents Data Warehouse Fact table entity that
 * stores daily market data (prices, etc.) information about securities that we have positions for.
 *
 * @author vgomelsky
 */
public class MarketDataValueSnapshot {

	// PK = investmentSecurityId + snapshotDate
	private int investmentSecurityId;
	private Date snapshotDate;

	// snapshot's calculated/looked-up measures:
	private BigDecimal price; // latest known price if snapshotDate price is not available; corresponding Big security price if security has a corresponding Big security
	/**
	 * includes accrued interest for bonds/swaps: Calculation is dependent on the notional calculator see
	 * {@link com.clifton.investment.instrument.calculator.InvestmentNotionalCalculatorTypes}
	 * For some swaps, the accrued interest is can be positive or negative based on the direction of the position held, which causes a change in dirty price
	 * Currently, for the Snapshot, we will default to the Long Value only and pass in null for the quantity to use default long quantity of 1 Billion (sizable face to calculate accrued interest against)
	 */
	private BigDecimal dirtyPrice;

	private BigDecimal indexRatio; // used by TIPS and is based on CPI changes
	private BigDecimal duration;
	private BigDecimal dv01;
	private BigDecimal dv01Spot;
	private BigDecimal couponRate;
	private BigDecimal currentFactor;

	private Date previousAccrualEndDate; // coupon/dividend/etc. accrual period dates
	private Date currentAccrualEndDate;


	public int getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(int investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getDirtyPrice() {
		return this.dirtyPrice;
	}


	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}


	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}


	public BigDecimal getDuration() {
		return this.duration;
	}


	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}


	public BigDecimal getCouponRate() {
		return this.couponRate;
	}


	public void setCouponRate(BigDecimal couponRate) {
		this.couponRate = couponRate;
	}


	public Date getPreviousAccrualEndDate() {
		return this.previousAccrualEndDate;
	}


	public void setPreviousAccrualEndDate(Date previousAccrualEndDate) {
		this.previousAccrualEndDate = previousAccrualEndDate;
	}


	public Date getCurrentAccrualEndDate() {
		return this.currentAccrualEndDate;
	}


	public void setCurrentAccrualEndDate(Date currentAccrualEndDate) {
		this.currentAccrualEndDate = currentAccrualEndDate;
	}


	public BigDecimal getCurrentFactor() {
		return this.currentFactor;
	}


	public void setCurrentFactor(BigDecimal currentFactor) {
		this.currentFactor = currentFactor;
	}


	public BigDecimal getDv01() {
		return this.dv01;
	}


	public void setDv01(BigDecimal dv01) {
		this.dv01 = dv01;
	}


	public BigDecimal getDv01Spot() {
		return this.dv01Spot;
	}


	public void setDv01Spot(BigDecimal dv01Spot) {
		this.dv01Spot = dv01Spot;
	}
}
