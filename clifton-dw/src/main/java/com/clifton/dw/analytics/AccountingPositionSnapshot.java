package com.clifton.dw.analytics;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.dw.rebuild.RawAccountingPositionSnapshot;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The <code>AccountingPositionSnapshot</code> class represents fully populated version of AccountingPositionSnapshot.
 * It has manually hydrated fully populated foreign key objects as well as database-calculated columns.
 *
 * @author vgomelsky
 */
public class AccountingPositionSnapshot extends RawAccountingPositionSnapshot implements IdentityObject {

	@NonPersistentField
	private InvestmentAccount clientInvestmentAccount;
	@NonPersistentField
	private InvestmentAccount holdingInvestmentAccount;
	@NonPersistentField
	private InvestmentSecurity investmentSecurity;
	@NonPersistentField
	private AccountingAccount accountingAccount;

	private BigDecimal baseNotional;

	private BigDecimal baseAccrual1;
	private BigDecimal baseAccrual2;
	private BigDecimal baseAccrual;
	private BigDecimal localAccrual;

	private BigDecimal localMarketValue;
	private BigDecimal baseMarketValue;


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || !o.getClass().equals(getClass())) {
			return false;
		}
		AccountingPositionSnapshot entity = (AccountingPositionSnapshot) o;
		if (entity.getAccountingTransactionId() == getAccountingTransactionId()) {
			if (getSnapshotDate() != null) {
				return getSnapshotDate().equals(entity.getSnapshotDate());
			}
		}
		return false;
	}


	@Override
	public int hashCode() {
		return new Long(getAccountingTransactionId()).hashCode() + (getSnapshotDate() == null ? 0 : getSnapshotDate().hashCode());
	}


	@Override
	public Serializable getIdentity() {
		return null;
	}


	@Override
	public boolean isNewBean() {
		return false;
	}


	public BigDecimal getAbsBaseNotional() {
		return MathUtils.abs(getBaseNotional());
	}


	public BigDecimal getBaseNotional() {
		return this.baseNotional;
	}


	public void setBaseNotional(BigDecimal baseNotional) {
		this.baseNotional = baseNotional;
	}


	public BigDecimal getBaseAccrual1() {
		return this.baseAccrual1;
	}


	public void setBaseAccrual1(BigDecimal baseAccrual1) {
		this.baseAccrual1 = baseAccrual1;
	}


	public BigDecimal getBaseAccrual2() {
		return this.baseAccrual2;
	}


	public void setBaseAccrual2(BigDecimal baseAccrual2) {
		this.baseAccrual2 = baseAccrual2;
	}


	public BigDecimal getBaseAccrual() {
		return this.baseAccrual;
	}


	public void setBaseAccrual(BigDecimal baseAccrual) {
		this.baseAccrual = baseAccrual;
	}


	public BigDecimal getLocalAccrual() {
		return this.localAccrual;
	}


	public void setLocalAccrual(BigDecimal localAccrual) {
		this.localAccrual = localAccrual;
	}


	public BigDecimal getLocalMarketValue() {
		return this.localMarketValue;
	}


	public void setLocalMarketValue(BigDecimal localMarketValue) {
		this.localMarketValue = localMarketValue;
	}


	public BigDecimal getBaseMarketValue() {
		return this.baseMarketValue;
	}


	public void setBaseMarketValue(BigDecimal baseMarketValue) {
		this.baseMarketValue = baseMarketValue;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}
}
