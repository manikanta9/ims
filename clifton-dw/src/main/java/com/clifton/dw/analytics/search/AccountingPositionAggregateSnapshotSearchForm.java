package com.clifton.dw.analytics.search;

import com.clifton.core.dataaccess.search.SearchField;

import java.math.BigDecimal;
import java.util.Date;


public class AccountingPositionAggregateSnapshotSearchForm extends BaseAccountingPositionSnapshotSearchForm {

	@SearchField
	private BigDecimal openingQuantity;

	@SearchField(searchField = "quantity")
	private BigDecimal unadjustedQuantity;

	@SearchField
	private BigDecimal currentFactor;
	@SearchField
	private BigDecimal indexRatio;
	@SearchField
	private BigDecimal currentCoupon;

	@SearchField
	private Date previousAccrualEndDate;
	@SearchField
	private Date currentAccrualEndDate;

	@SearchField
	private BigDecimal duration;
	@SearchField
	private BigDecimal dv01;
	@SearchField
	private BigDecimal dv01Spot;

	// Optional command to retrieve advanced fields (slower retrieval)
	private String dataSourceNameForAdvancedFields;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDataSourceNameForAdvancedFields() {
		return this.dataSourceNameForAdvancedFields;
	}


	public void setDataSourceNameForAdvancedFields(String dataSourceNameForAdvancedFields) {
		this.dataSourceNameForAdvancedFields = dataSourceNameForAdvancedFields;
	}


	public BigDecimal getOpeningQuantity() {
		return this.openingQuantity;
	}


	public void setOpeningQuantity(BigDecimal openingQuantity) {
		this.openingQuantity = openingQuantity;
	}


	public BigDecimal getUnadjustedQuantity() {
		return this.unadjustedQuantity;
	}


	public void setUnadjustedQuantity(BigDecimal unadjustedQuantity) {
		this.unadjustedQuantity = unadjustedQuantity;
	}


	public BigDecimal getCurrentFactor() {
		return this.currentFactor;
	}


	public void setCurrentFactor(BigDecimal currentFactor) {
		this.currentFactor = currentFactor;
	}


	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}


	public BigDecimal getCurrentCoupon() {
		return this.currentCoupon;
	}


	public void setCurrentCoupon(BigDecimal currentCoupon) {
		this.currentCoupon = currentCoupon;
	}


	public Date getPreviousAccrualEndDate() {
		return this.previousAccrualEndDate;
	}


	public void setPreviousAccrualEndDate(Date previousAccrualEndDate) {
		this.previousAccrualEndDate = previousAccrualEndDate;
	}


	public Date getCurrentAccrualEndDate() {
		return this.currentAccrualEndDate;
	}


	public void setCurrentAccrualEndDate(Date currentAccrualEndDate) {
		this.currentAccrualEndDate = currentAccrualEndDate;
	}


	public BigDecimal getDuration() {
		return this.duration;
	}


	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}


	public BigDecimal getDv01() {
		return this.dv01;
	}


	public void setDv01(BigDecimal dv01) {
		this.dv01 = dv01;
	}


	public BigDecimal getDv01Spot() {
		return this.dv01Spot;
	}


	public void setDv01Spot(BigDecimal dv01Spot) {
		this.dv01Spot = dv01Spot;
	}
}
