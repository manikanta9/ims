package com.clifton.dw.analytics;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.dw.analytics.search.AccountingBalanceSnapshotSearchForm;
import com.clifton.dw.analytics.search.AccountingPositionAggregateSnapshotSearchForm;
import com.clifton.dw.analytics.search.AccountingPositionSnapshotSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>DwAnalyticsService</code> interface defines methods that return various
 * analytics from DW tables.
 *
 * @author vgomelsky
 */
public interface DwAnalyticsService {


	@RequestMapping("dwAccountingBalanceSnapshotListFind")
	public List<AccountingBalanceSnapshot> getAccountingBalanceSnapshotList(AccountingBalanceSnapshotSearchForm searchForm);


	@RequestMapping("dwAccountingPositionSnapshotListFind")
	public List<AccountingPositionSnapshot> getAccountingPositionSnapshotList(AccountingPositionSnapshotSearchForm searchForm);


	@RequestMapping("dwAccountingPositionAggregateSnapshotListFind")
	@SecureMethod(dtoClass = AccountingPositionSnapshot.class)
	public List<AccountingPositionAggregateSnapshot> getAccountingPositionAggregateSnapshotList(final AccountingPositionAggregateSnapshotSearchForm searchForm);
}
