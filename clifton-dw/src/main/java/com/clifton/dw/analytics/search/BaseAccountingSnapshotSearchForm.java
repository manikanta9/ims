package com.clifton.dw.analytics.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * @author vgomelsky
 */
public abstract class BaseAccountingSnapshotSearchForm extends BaseEntitySearchForm {

	@SearchField
	private Date snapshotDate;

	@SearchField
	private Integer clientInvestmentAccountId;
	@SearchField
	private Integer holdingInvestmentAccountId;
	@SearchField
	private Integer investmentSecurityId;
	@SearchField
	private Short accountingAccountId;

	// Custom Search Fields
	private Integer holdingCompanyId;
	private Short investmentGroupId;
	private Short securityGroupId;
	private Short clientInvestmentAccountGroupId;
	private Short holdingInvestmentAccountGroupId;
	private Short accountingAccountGroupId;
	private Short accountingAccountTypeId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public void setSecurityGroupId(Short securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public Short getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Short clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Short getHoldingInvestmentAccountGroupId() {
		return this.holdingInvestmentAccountGroupId;
	}


	public void setHoldingInvestmentAccountGroupId(Short holdingInvestmentAccountGroupId) {
		this.holdingInvestmentAccountGroupId = holdingInvestmentAccountGroupId;
	}


	public Short getAccountingAccountGroupId() {
		return this.accountingAccountGroupId;
	}


	public void setAccountingAccountGroupId(Short accountingAccountGroupId) {
		this.accountingAccountGroupId = accountingAccountGroupId;
	}


	public Short getAccountingAccountTypeId() {
		return this.accountingAccountTypeId;
	}


	public void setAccountingAccountTypeId(Short accountingAccountTypeId) {
		this.accountingAccountTypeId = accountingAccountTypeId;
	}
}

