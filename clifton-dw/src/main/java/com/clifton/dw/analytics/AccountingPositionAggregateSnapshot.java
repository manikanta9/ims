package com.clifton.dw.analytics;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * AccountingPositionAggregateSnapshot class represents a position balance in the Data Warehouse for the specified Snapshot Date.
 * It groups lot-level AccountingPositionSnapshot records by client account, holding account, investment security, accounting account, snapshot price and exchange rate to base.
 * <p>
 * This snapshot also adds many other measures that can be useful for various analytics.
 *
 * @author vgomelsky
 */
public class AccountingPositionAggregateSnapshot extends BaseSimpleEntity<String> implements AccountingObjectInfo {

	private String uuid;

	private Date snapshotDate;

	// snapshot dimensions:
	private int clientInvestmentAccountId;
	private int holdingInvestmentAccountId;
	private int investmentSecurityId;
	private short accountingAccountId; // can be used to identify Collateral positions


	// snapshot's calculated/looked-up measures (fields represent remaining as opposed to original values):
	private BigDecimal snapshotPrice; // latest known price if snapshotDate price is not available; corresponding Big security price if security has a corresponding Big security
	private BigDecimal snapshotExchangeRateToBase;

	private BigDecimal openingQuantity; // opening quantity (Original Face/Notional for ABS/CDS)
	private BigDecimal quantity; // remaining quantity

	private BigDecimal localCost; // cost of futures is 0 and for stocks it's the price paid, etc.
	private BigDecimal baseCost;

	private BigDecimal localCostBasis; // Market Value = Notional - Cost Basis + Cost + Accrual
	private BigDecimal baseCostBasis;

	// for securities that support it: Accrued Interest for bonds, set to Zero otherwise
	private BigDecimal localAccrual1;
	private BigDecimal baseAccrual1;
	private BigDecimal localAccrual2;
	private BigDecimal baseAccrual2;
	private BigDecimal localAccrual;
	private BigDecimal baseAccrual;

	private BigDecimal localNotional; // Unrealized Gain / Loss = Notional - Cost Basis
	private BigDecimal baseNotional;

	private BigDecimal localMarketValue;
	private BigDecimal baseMarketValue;

	// fields from MarketDataValueSnapshot
	private BigDecimal currentFactor;
	private BigDecimal indexRatio;
	private BigDecimal currentCoupon;

	private Date previousAccrualEndDate;
	private Date currentAccrualEndDate;

	private BigDecimal duration;
	private BigDecimal dv01;
	private BigDecimal dv01Spot;


	// manually hydrated fields
	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	private InvestmentSecurity investmentSecurity;
	private AccountingAccount accountingAccount;

	// fields that require additional look ups and are populated only when requested "dataSourceNameForAdvancedFields"
	private String securityMarketSector;
	private String underlyingMarketSector;
	private String securitySymbolOverride;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns "Remaining Quantity" for all securities that do not have Factor Change Event.
	 * For securities with Factor Change Event (CDS and ABS), returns opening quantity (Original Notional or Face)
	 */
	public BigDecimal getUnadjustedQuantity() {
		// if security was not set, return null because we cannot determine what quantity field to use
		if (getInvestmentSecurity() == null) {
			return null;
		}

		BigDecimal result = getQuantity();
		if (getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() != null) {
			if (!MathUtils.isNullOrZero(result)) {
				result = getOpeningQuantity();
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public int getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(int clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public int getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(int holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public int getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(int investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public BigDecimal getSnapshotPrice() {
		return this.snapshotPrice;
	}


	public void setSnapshotPrice(BigDecimal snapshotPrice) {
		this.snapshotPrice = snapshotPrice;
	}


	public BigDecimal getSnapshotExchangeRateToBase() {
		return this.snapshotExchangeRateToBase;
	}


	public void setSnapshotExchangeRateToBase(BigDecimal snapshotExchangeRateToBase) {
		this.snapshotExchangeRateToBase = snapshotExchangeRateToBase;
	}


	public BigDecimal getOpeningQuantity() {
		return this.openingQuantity;
	}


	public void setOpeningQuantity(BigDecimal openingQuantity) {
		this.openingQuantity = openingQuantity;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getLocalCost() {
		return this.localCost;
	}


	public void setLocalCost(BigDecimal localCost) {
		this.localCost = localCost;
	}


	public BigDecimal getBaseCost() {
		return this.baseCost;
	}


	public void setBaseCost(BigDecimal baseCost) {
		this.baseCost = baseCost;
	}


	public BigDecimal getLocalCostBasis() {
		return this.localCostBasis;
	}


	public void setLocalCostBasis(BigDecimal localCostBasis) {
		this.localCostBasis = localCostBasis;
	}


	public BigDecimal getBaseCostBasis() {
		return this.baseCostBasis;
	}


	public void setBaseCostBasis(BigDecimal baseCostBasis) {
		this.baseCostBasis = baseCostBasis;
	}


	public BigDecimal getLocalAccrual1() {
		return this.localAccrual1;
	}


	public void setLocalAccrual1(BigDecimal localAccrual1) {
		this.localAccrual1 = localAccrual1;
	}


	public BigDecimal getBaseAccrual1() {
		return this.baseAccrual1;
	}


	public void setBaseAccrual1(BigDecimal baseAccrual1) {
		this.baseAccrual1 = baseAccrual1;
	}


	public BigDecimal getBaseAccrual2() {
		return this.baseAccrual2;
	}


	public void setBaseAccrual2(BigDecimal baseAccrual2) {
		this.baseAccrual2 = baseAccrual2;
	}


	public BigDecimal getLocalAccrual2() {
		return this.localAccrual2;
	}


	public void setLocalAccrual2(BigDecimal localAccrual2) {
		this.localAccrual2 = localAccrual2;
	}


	public BigDecimal getBaseAccrual() {
		return this.baseAccrual;
	}


	public void setBaseAccrual(BigDecimal baseAccrual) {
		this.baseAccrual = baseAccrual;
	}


	public BigDecimal getLocalAccrual() {
		return this.localAccrual;
	}


	public void setLocalAccrual(BigDecimal localAccrual) {
		this.localAccrual = localAccrual;
	}


	public BigDecimal getLocalNotional() {
		return this.localNotional;
	}


	public void setLocalNotional(BigDecimal localNotional) {
		this.localNotional = localNotional;
	}


	public BigDecimal getBaseNotional() {
		return this.baseNotional;
	}


	public void setBaseNotional(BigDecimal baseNotional) {
		this.baseNotional = baseNotional;
	}


	public BigDecimal getLocalMarketValue() {
		return this.localMarketValue;
	}


	public void setLocalMarketValue(BigDecimal localMarketValue) {
		this.localMarketValue = localMarketValue;
	}


	public BigDecimal getBaseMarketValue() {
		return this.baseMarketValue;
	}


	public void setBaseMarketValue(BigDecimal baseMarketValue) {
		this.baseMarketValue = baseMarketValue;
	}


	public BigDecimal getCurrentFactor() {
		return this.currentFactor;
	}


	public void setCurrentFactor(BigDecimal currentFactor) {
		this.currentFactor = currentFactor;
	}


	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}


	public BigDecimal getCurrentCoupon() {
		return this.currentCoupon;
	}


	public void setCurrentCoupon(BigDecimal currentCoupon) {
		this.currentCoupon = currentCoupon;
	}


	public Date getPreviousAccrualEndDate() {
		return this.previousAccrualEndDate;
	}


	public void setPreviousAccrualEndDate(Date previousAccrualEndDate) {
		this.previousAccrualEndDate = previousAccrualEndDate;
	}


	public Date getCurrentAccrualEndDate() {
		return this.currentAccrualEndDate;
	}


	public void setCurrentAccrualEndDate(Date currentAccrualEndDate) {
		this.currentAccrualEndDate = currentAccrualEndDate;
	}


	public BigDecimal getDuration() {
		return this.duration;
	}


	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}


	public BigDecimal getDv01() {
		return this.dv01;
	}


	public void setDv01(BigDecimal dv01) {
		this.dv01 = dv01;
	}


	public BigDecimal getDv01Spot() {
		return this.dv01Spot;
	}


	public void setDv01Spot(BigDecimal dv01Spot) {
		this.dv01Spot = dv01Spot;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	@Override
	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}


	public String getSecurityMarketSector() {
		return this.securityMarketSector;
	}


	public void setSecurityMarketSector(String securityMarketSector) {
		this.securityMarketSector = securityMarketSector;
	}


	public String getUnderlyingMarketSector() {
		return this.underlyingMarketSector;
	}


	public void setUnderlyingMarketSector(String underlyingMarketSector) {
		this.underlyingMarketSector = underlyingMarketSector;
	}


	public String getSecuritySymbolOverride() {
		return this.securitySymbolOverride;
	}


	public void setSecuritySymbolOverride(String securitySymbolOverride) {
		this.securitySymbolOverride = securitySymbolOverride;
	}
}
