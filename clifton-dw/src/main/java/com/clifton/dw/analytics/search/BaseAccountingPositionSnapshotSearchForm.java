package com.clifton.dw.analytics.search;

import com.clifton.core.dataaccess.search.SearchField;

import java.math.BigDecimal;


public abstract class BaseAccountingPositionSnapshotSearchForm extends BaseAccountingSnapshotSearchForm {

	// snapshot's calculated/looked-up measures (fields represent remaining as opposed to original values):
	@SearchField
	private BigDecimal quantity;
	@SearchField
	private BigDecimal snapshotPrice; // latest known price if snapshotDate price is not available; corresponding Big security price if security has a corresponding Big security
	@SearchField
	private BigDecimal snapshotExchangeRateToBase;

	@SearchField
	private BigDecimal localCost; // cost of futures is 0 and for stocks it's the price paid, etc.
	@SearchField
	private BigDecimal baseCost;
	@SearchField
	private BigDecimal localCostBasis; // Market Value = Notional - Cost Basis + Cost + Accrual
	@SearchField
	private BigDecimal baseCostBasis;
	@SearchField
	private BigDecimal localNotional; // Unrealized Gain / Loss = Notional - Cost Basis
	@SearchField
	private BigDecimal baseNotional;
	@SearchField
	private BigDecimal localMarketValue;
	@SearchField
	private BigDecimal baseMarketValue;

	@SearchField
	private BigDecimal localAccrual; // for securities that support it: Accrued Interest for bonds
	@SearchField
	private BigDecimal baseAccrual; // set to Zero otherwise
	@SearchField
	private BigDecimal localAccrual1;
	@SearchField
	private BigDecimal baseAccrual1;
	@SearchField
	private BigDecimal localAccrual2;
	@SearchField
	private BigDecimal baseAccrual2;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getSnapshotPrice() {
		return this.snapshotPrice;
	}


	public void setSnapshotPrice(BigDecimal snapshotPrice) {
		this.snapshotPrice = snapshotPrice;
	}


	public BigDecimal getSnapshotExchangeRateToBase() {
		return this.snapshotExchangeRateToBase;
	}


	public void setSnapshotExchangeRateToBase(BigDecimal snapshotExchangeRateToBase) {
		this.snapshotExchangeRateToBase = snapshotExchangeRateToBase;
	}


	public BigDecimal getLocalCost() {
		return this.localCost;
	}


	public void setLocalCost(BigDecimal localCost) {
		this.localCost = localCost;
	}


	public BigDecimal getBaseCost() {
		return this.baseCost;
	}


	public void setBaseCost(BigDecimal baseCost) {
		this.baseCost = baseCost;
	}


	public BigDecimal getLocalCostBasis() {
		return this.localCostBasis;
	}


	public void setLocalCostBasis(BigDecimal localCostBasis) {
		this.localCostBasis = localCostBasis;
	}


	public BigDecimal getBaseCostBasis() {
		return this.baseCostBasis;
	}


	public void setBaseCostBasis(BigDecimal baseCostBasis) {
		this.baseCostBasis = baseCostBasis;
	}


	public BigDecimal getLocalNotional() {
		return this.localNotional;
	}


	public void setLocalNotional(BigDecimal localNotional) {
		this.localNotional = localNotional;
	}


	public BigDecimal getBaseNotional() {
		return this.baseNotional;
	}


	public void setBaseNotional(BigDecimal baseNotional) {
		this.baseNotional = baseNotional;
	}


	public BigDecimal getLocalMarketValue() {
		return this.localMarketValue;
	}


	public void setLocalMarketValue(BigDecimal localMarketValue) {
		this.localMarketValue = localMarketValue;
	}


	public BigDecimal getBaseMarketValue() {
		return this.baseMarketValue;
	}


	public void setBaseMarketValue(BigDecimal baseMarketValue) {
		this.baseMarketValue = baseMarketValue;
	}


	public BigDecimal getLocalAccrual() {
		return this.localAccrual;
	}


	public void setLocalAccrual(BigDecimal localAccrual) {
		this.localAccrual = localAccrual;
	}


	public BigDecimal getBaseAccrual() {
		return this.baseAccrual;
	}


	public void setBaseAccrual(BigDecimal baseAccrual) {
		this.baseAccrual = baseAccrual;
	}


	public BigDecimal getLocalAccrual1() {
		return this.localAccrual1;
	}


	public void setLocalAccrual1(BigDecimal localAccrual1) {
		this.localAccrual1 = localAccrual1;
	}


	public BigDecimal getBaseAccrual1() {
		return this.baseAccrual1;
	}


	public void setBaseAccrual1(BigDecimal baseAccrual1) {
		this.baseAccrual1 = baseAccrual1;
	}


	public BigDecimal getLocalAccrual2() {
		return this.localAccrual2;
	}


	public void setLocalAccrual2(BigDecimal localAccrual2) {
		this.localAccrual2 = localAccrual2;
	}


	public BigDecimal getBaseAccrual2() {
		return this.baseAccrual2;
	}


	public void setBaseAccrual2(BigDecimal baseAccrual2) {
		this.baseAccrual2 = baseAccrual2;
	}
}
