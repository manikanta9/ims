package com.clifton.dw.analytics;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.dw.analytics.search.AccountingBalanceSnapshotSearchForm;
import com.clifton.dw.analytics.search.AccountingPositionAggregateSnapshotSearchForm;
import com.clifton.dw.analytics.search.AccountingPositionSnapshotSearchForm;
import com.clifton.dw.analytics.search.BaseAccountingSnapshotSearchForm;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceSecurityHolder;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class DwAnalyticsServiceImpl implements DwAnalyticsService {

	private AdvancedReadOnlyDAO<AccountingBalanceSnapshot, Criteria> accountingBalanceSnapshotDAO;
	private AdvancedReadOnlyDAO<AccountingPositionSnapshot, Criteria> accountingPositionSnapshotDAO;
	private AdvancedReadOnlyDAO<AccountingPositionAggregateSnapshot, Criteria> accountingPositionAggregateSnapshotDAO;

	private AccountingAccountService accountingAccountService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true) // faster hydration when there's only one transaction
	public List<AccountingBalanceSnapshot> getAccountingBalanceSnapshotList(AccountingBalanceSnapshotSearchForm searchForm) {
		List<AccountingBalanceSnapshot> result = getAccountingBalanceSnapshotDAO().findBySearchCriteria(createSearchConfigurer(searchForm));

		// hydrate snapshot objects
		Map<Short, AccountingAccount> accountingAccountMap = new HashMap<>();
		Map<Integer, InvestmentAccount> investmentAccountMap = new HashMap<>();
		for (AccountingBalanceSnapshot snapshot : CollectionUtils.getIterable(result)) {
			snapshot.setClientInvestmentAccount(getInvestmentAccount(snapshot.getClientInvestmentAccountId(), investmentAccountMap));
			snapshot.setHoldingInvestmentAccount(getInvestmentAccount(snapshot.getHoldingInvestmentAccountId(), investmentAccountMap));
			snapshot.setInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurity(snapshot.getInvestmentSecurityId()));
			snapshot.setAccountingAccount(getAccountingAccount(snapshot.getAccountingAccountId(), accountingAccountMap));
		}

		return result;
	}


	@Override
	@Transactional(readOnly = true) // faster hydration when there's only one transaction
	public List<AccountingPositionSnapshot> getAccountingPositionSnapshotList(final AccountingPositionSnapshotSearchForm searchForm) {
		List<AccountingPositionSnapshot> result = getAccountingPositionSnapshotDAO().findBySearchCriteria(createSearchConfigurer(searchForm));

		// hydrate snapshot objects
		Map<Short, AccountingAccount> accountingAccountMap = new HashMap<>();
		Map<Integer, InvestmentAccount> investmentAccountMap = new HashMap<>();
		for (AccountingPositionSnapshot snapshot : CollectionUtils.getIterable(result)) {
			snapshot.setClientInvestmentAccount(getInvestmentAccount(snapshot.getClientInvestmentAccountId(), investmentAccountMap));
			snapshot.setHoldingInvestmentAccount(getInvestmentAccount(snapshot.getHoldingInvestmentAccountId(), investmentAccountMap));
			snapshot.setInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurity(snapshot.getInvestmentSecurityId()));
			snapshot.setAccountingAccount(getAccountingAccount(snapshot.getAccountingAccountId(), accountingAccountMap));
		}

		return result;
	}


	@Override
	@Transactional(readOnly = true) // faster hydration when there's only one transaction
	public List<AccountingPositionAggregateSnapshot> getAccountingPositionAggregateSnapshotList(final AccountingPositionAggregateSnapshotSearchForm searchForm) {
		ValidationUtils.assertNotNull(searchForm.getSnapshotDate(), "Snapshot Date filter is required.", "snapshotDate");
		List<AccountingPositionAggregateSnapshot> result = getAccountingPositionAggregateSnapshotDAO().findBySearchCriteria(createSearchConfigurer(searchForm));

		// hydrate snapshot objects
		MarketDataSource dataSource = null;
		if (!StringUtils.isEmpty(searchForm.getDataSourceNameForAdvancedFields())) {
			dataSource = getMarketDataSourceService().getMarketDataSourceByName(searchForm.getDataSourceNameForAdvancedFields());
		}

		Map<Short, AccountingAccount> accountingAccountMap = new HashMap<>();
		Map<Integer, InvestmentAccount> investmentAccountMap = new HashMap<>();
		for (AccountingPositionAggregateSnapshot snapshot : CollectionUtils.getIterable(result)) {
			snapshot.setClientInvestmentAccount(getInvestmentAccount(snapshot.getClientInvestmentAccountId(), investmentAccountMap));
			snapshot.setHoldingInvestmentAccount(getInvestmentAccount(snapshot.getHoldingInvestmentAccountId(), investmentAccountMap));
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(snapshot.getInvestmentSecurityId());
			snapshot.setInvestmentSecurity(security);
			snapshot.setAccountingAccount(getAccountingAccount(snapshot.getAccountingAccountId(), accountingAccountMap));

			if (dataSource != null) {
				InvestmentInstrument instrument = security.getInstrument();
				MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(instrument, dataSource);
				if (mapping != null) {
					String marketSector = mapping.getMarketSector().getName();
					snapshot.setSecurityMarketSector(marketSector);
					MarketDataSourceSecurityHolder securityHolder = getMarketDataSourceService().getMarketDataSourceSecuritySymbol(security.getSymbol(), dataSource.getName(), marketSector);
					if (securityHolder != null) {
						snapshot.setSecuritySymbolOverride(securityHolder.getDataSourceSymbol());
					}
				}
				instrument = instrument.getUnderlyingInstrument();
				if (instrument != null) {
					mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(instrument, dataSource);
					if (mapping != null) {
						String marketSector = mapping.getMarketSector().getName();
						snapshot.setUnderlyingMarketSector(marketSector);
					}
				}
			}
		}

		return result;
	}


	private SearchConfigurer<Criteria> createSearchConfigurer(final BaseAccountingSnapshotSearchForm searchForm) {
		return new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getHoldingCompanyId() != null) {
					criteria.add(Restrictions.sqlRestriction("{alias}.HoldingInvestmentAccountID IN (SELECT InvestmentAccountID FROM InvestmentAccountDimension WHERE IssuingCompanyID = " + searchForm.getHoldingCompanyId() + ")"));
				}
				if (searchForm.getInvestmentGroupId() != null) {
					criteria.add(Restrictions.sqlRestriction(" EXISTS ("
							+ "SELECT isd.InvestmentInstrumentID FROM InvestmentSecurityDimension isd INNER JOIN InvestmentGroupInstrumentDimension gid ON isd.InvestmentInstrumentID = gid.InvestmentInstrumentID "
							+ " WHERE isd.InvestmentSecurityID = {alias}.InvestmentSecurityID " + "AND gid.InvestmentGroupID = " + searchForm.getInvestmentGroupId() + ")"));
				}
				if (searchForm.getSecurityGroupId() != null) {
					criteria.add(Restrictions.sqlRestriction(" EXISTS ("
							+ "SELECT * FROM InvestmentSecurityGroupSecurityDimension sg WHERE sg.InvestmentSecurityID = {alias}.InvestmentSecurityID " + "AND sg.InvestmentSecurityGroupID = " + searchForm.getSecurityGroupId() + ")"));
				}
				if (searchForm.getClientInvestmentAccountGroupId() != null) {
					criteria.add(Restrictions.sqlRestriction(" EXISTS (" + "SELECT * FROM InvestmentAccountGroupDimension ag WHERE ag.InvestmentAccountID = {alias}.ClientInvestmentAccountID "
							+ "AND ag.InvestmentAccountGroupID = " + searchForm.getClientInvestmentAccountGroupId() + ")"));
				}
				if (searchForm.getHoldingInvestmentAccountGroupId() != null) {
					criteria.add(Restrictions.sqlRestriction(" EXISTS (" + "SELECT * FROM InvestmentAccountGroupDimension ag WHERE ag.InvestmentAccountID = {alias}.HoldingInvestmentAccountID "
							+ "AND ag.InvestmentAccountGroupID = " + searchForm.getHoldingInvestmentAccountGroupId() + ")"));
				}
				if (searchForm.getAccountingAccountGroupId() != null) {
					criteria.add(Restrictions.sqlRestriction(" EXISTS (" + "SELECT * FROM AccountingAccountGroupAccountDimension ag WHERE ag.AccountingAccountID = {alias}.AccountingAccountID "
							+ "AND ag.AccountingAccountGroupID = " + searchForm.getAccountingAccountGroupId() + ")"));
				}
				if (searchForm.getAccountingAccountTypeId() != null) {
					criteria.add(Restrictions.sqlRestriction("{alias}.AccountingAccountID IN (SELECT AccountingAccountID FROM AccountingAccountDimension WHERE AccountingAccountTypeID = " + searchForm.getAccountingAccountTypeId() + ")"));
				}
			}


			@Override
			protected String getOrderByFieldName(OrderByField field, Criteria criteria) {
				if ("holdingCompanyId".equals(field.getName())) {
					// NOTE: would need to join to InvestmentAccountDimension table in order to do proper sorting: good enough for now
					return "holdingInvestmentAccountId";
				}
				if ("accountingAccountTypeId".equals(field.getName())) {
					// NOTE: would need to join to AccountingAccountDimension table in order to do proper sorting: good enough for now
					return "accountingAccountId";
				}
				return super.getOrderByFieldName(field, criteria);
			}
		};
	}


	private AccountingAccount getAccountingAccount(short accountingAccountId, Map<Short, AccountingAccount> sessionCache) {
		return sessionCache.computeIfAbsent(accountingAccountId, i -> getAccountingAccountService().getAccountingAccount(i));
	}


	private InvestmentAccount getInvestmentAccount(int investmentAccountId, Map<Integer, InvestmentAccount> sessionCache) {
		return sessionCache.computeIfAbsent(investmentAccountId, i -> getInvestmentAccountService().getInvestmentAccount(i));
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<AccountingBalanceSnapshot, Criteria> getAccountingBalanceSnapshotDAO() {
		return this.accountingBalanceSnapshotDAO;
	}


	public void setAccountingBalanceSnapshotDAO(AdvancedReadOnlyDAO<AccountingBalanceSnapshot, Criteria> accountingBalanceSnapshotDAO) {
		this.accountingBalanceSnapshotDAO = accountingBalanceSnapshotDAO;
	}


	public AdvancedReadOnlyDAO<AccountingPositionSnapshot, Criteria> getAccountingPositionSnapshotDAO() {
		return this.accountingPositionSnapshotDAO;
	}


	public void setAccountingPositionSnapshotDAO(AdvancedReadOnlyDAO<AccountingPositionSnapshot, Criteria> accountingPositionSnapshotDAO) {
		this.accountingPositionSnapshotDAO = accountingPositionSnapshotDAO;
	}


	public AdvancedReadOnlyDAO<AccountingPositionAggregateSnapshot, Criteria> getAccountingPositionAggregateSnapshotDAO() {
		return this.accountingPositionAggregateSnapshotDAO;
	}


	public void setAccountingPositionAggregateSnapshotDAO(AdvancedReadOnlyDAO<AccountingPositionAggregateSnapshot, Criteria> accountingPositionAggregateSnapshotDAO) {
		this.accountingPositionAggregateSnapshotDAO = accountingPositionAggregateSnapshotDAO;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
