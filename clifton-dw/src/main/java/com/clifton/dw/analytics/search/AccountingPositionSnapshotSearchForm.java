package com.clifton.dw.analytics.search;


import com.clifton.core.dataaccess.search.SearchField;


public class AccountingPositionSnapshotSearchForm extends BaseAccountingPositionSnapshotSearchForm {

	@SearchField
	private Long accountingTransactionId; // points to the opening lot


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Long getAccountingTransactionId() {
		return this.accountingTransactionId;
	}


	public void setAccountingTransactionId(Long accountingTransactionId) {
		this.accountingTransactionId = accountingTransactionId;
	}
}
