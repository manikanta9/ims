package com.clifton.dw.analytics.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchUtils;

import java.math.BigDecimal;


/**
 * @author vgomelsky
 */
public class AccountingBalanceSnapshotSearchForm extends BaseAccountingSnapshotSearchForm {

	@SearchField
	private BigDecimal localDebitCreditBalance;

	@SearchField
	private BigDecimal baseDebitCreditBalance;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		SearchUtils.validateRequiredFilter(this, "localDebitCreditBalance", "baseDebitCreditBalance");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getLocalDebitCreditBalance() {
		return this.localDebitCreditBalance;
	}


	public void setLocalDebitCreditBalance(BigDecimal localDebitCreditBalance) {
		this.localDebitCreditBalance = localDebitCreditBalance;
	}


	public BigDecimal getBaseDebitCreditBalance() {
		return this.baseDebitCreditBalance;
	}


	public void setBaseDebitCreditBalance(BigDecimal baseDebitCreditBalance) {
		this.baseDebitCreditBalance = baseDebitCreditBalance;
	}
}
