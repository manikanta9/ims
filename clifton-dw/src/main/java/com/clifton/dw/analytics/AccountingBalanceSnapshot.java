package com.clifton.dw.analytics;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.dw.rebuild.RawAccountingBalanceSnapshot;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.io.Serializable;


/**
 * The <code>AccountingBalanceSnapshot</code> class represents fully populated version of AccountingBalanceSnapshot.
 * It has manually hydrated fully populated foreign key objects.
 *
 * @author vgomelsky
 */
public class AccountingBalanceSnapshot extends RawAccountingBalanceSnapshot implements IdentityObject {

	@NonPersistentField
	private InvestmentAccount clientInvestmentAccount;
	@NonPersistentField
	private InvestmentAccount holdingInvestmentAccount;
	@NonPersistentField
	private InvestmentSecurity investmentSecurity;
	@NonPersistentField
	private AccountingAccount accountingAccount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Serializable getIdentity() {
		return null;
	}


	@Override
	public boolean isNewBean() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}
}
