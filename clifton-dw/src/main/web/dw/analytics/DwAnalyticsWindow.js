Clifton.dw.analytics.DwAnalyticsWindow = Ext.extend(TCG.app.Window, {
	id: 'dwAnalyticsWindow',
	title: 'Data Warehouse',
	iconCls: 'database',
	width: 1600,
	height: 700,

	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Positions',
				items: [{
					xtype: 'gridpanel',
					name: 'dwAccountingPositionAggregateSnapshotListFind',
					instructions: 'Aggregates positions from the Data Warehouse by Client Account, Holding Account, Security, GL Account, Price and FX Rate. Accounting positions snapshot is built from the General Ledger and market data. Positions may get stale and may need to be rebuilt in order to see accurate data when GL or market data changes.',
					pageSize: 200,
					reloadOnRender: false,
					appendStandardColumns: false,
					additionalPropertiesToRequest: 'clientInvestmentAccount.id|holdingInvestmentAccount.id|accountingAccount.id|investmentSecurity.id',

					viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral'],
					configureViews: function(menu, gridPanel) {
						menu.add('-');
						menu.add({
							text: 'Retrieve Bloomberg Fields',
							xtype: 'menucheckitem',
							handler: function(item, event) {
								gridPanel.retrieveBloombergFields = !item.checked;
								gridPanel.reload();
							}
						});
					},

					columns: [
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 140, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral']},
						{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', width: 100, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral']},
						{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.name', width: 120, hidden: true, filter: {type: 'combo', searchFieldName: 'holdingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, viewNames: ['Collateral']},
						{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 90, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}, hidden: true},

						{header: 'Hierarchy', dataIndex: 'investmentSecurity.instrument.hierarchy.nameExpanded', width: 150, filter: false, hidden: true},
						{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 60, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral']},
						{header: 'Security Name', dataIndex: 'investmentSecurity.name', width: 70, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral']},
						{header: 'Security Market Sector', dataIndex: 'securityMarketSector', width: 60, filter: false, hidden: true},
						{header: 'Security Symbol Override', dataIndex: 'securitySymbolOverride', width: 60, filter: false, hidden: true},
						{header: 'CCY Denom', dataIndex: 'investmentSecurity.instrument.tradingCurrency.symbol', width: 50, filter: false, sortable: false, hidden: true},
						{header: 'Underlying Symbol', dataIndex: 'investmentSecurity.underlyingSecurity.symbol', width: 60, filter: false, hidden: true},
						{header: 'Underlying Market Sector', dataIndex: 'underlyingMarketSector', width: 60, filter: false, hidden: true},
						{header: 'Primary Exchange', dataIndex: 'investmentSecurity.instrument.exchange.exchangeCode', width: 60, filter: false, hidden: true},
						{header: 'Composite Exchange', dataIndex: 'investmentSecurity.instrument.compositeExchange.exchangeCode', width: 60, filter: false, hidden: true},
						{header: 'Price Multiplier', dataIndex: 'investmentSecurity.priceMultiplier', type: 'float', useNull: true, width: 50, hidden: true},

						{header: 'Unadjusted Qty', dataIndex: 'unadjustedQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 60, tooltip: 'Opening Quantity (Original Notional or Face) for securities with Factor Changes and Remaining Quantity for those without.', viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral']},
						{header: 'Qty', dataIndex: 'quantity', type: 'float', negativeInRed: true, positiveInGreen: true, width: 50, hidden: true},

						{header: 'Index Ratio', dataIndex: 'indexRatio', type: 'float', useNull: true, width: 50, hidden: true, viewNames: ['Inflation Linked']},
						{header: 'Current Factor', dataIndex: 'currentFactor', type: 'float', useNull: true, width: 50, hidden: true, viewNames: ['With Factor Changes']},
						{header: 'Current Coupon', dataIndex: 'currentCoupon', type: 'float', useNull: true, width: 50, hidden: true, viewNames: ['With Factor Changes', 'Inflation Linked']},
						{header: 'Previous Accrual End Date', dataIndex: 'previousAccrualEndDate', width: 70, hidden: true},
						{header: 'Current Accrual End Date', dataIndex: 'currentAccrualEndDate', width: 70, hidden: true},

						{header: 'Price', dataIndex: 'snapshotPrice', type: 'float', width: 50, viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral']},
						{header: 'FX Rate', dataIndex: 'snapshotExchangeRateToBase', type: 'float', width: 50, viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral']},

						{header: 'Duration', dataIndex: 'duration', type: 'float', useNull: true, width: 50, hidden: true},
						{header: 'DV01', dataIndex: 'dv01', type: 'float', useNull: true, width: 50, hidden: true},
						{header: 'DV01 Spot', dataIndex: 'dv01Spot', type: 'float', useNull: true, width: 50, hidden: true},

						{header: 'Local Cost', dataIndex: 'localCost', type: 'currency', negativeInRed: true, width: 80, hidden: true},
						{header: 'Base Cost', dataIndex: 'baseCost', type: 'currency', negativeInRed: true, width: 80, summaryType: 'sum', hidden: true},
						{header: 'Local Cost Basis', dataIndex: 'localCostBasis', type: 'currency', negativeInRed: true, width: 80, hidden: true},
						{header: 'Base Cost Basis', dataIndex: 'baseCostBasis', type: 'currency', negativeInRed: true, width: 70, summaryType: 'sum', hidden: true},
						{header: 'Local Accrual 1', dataIndex: 'localAccrual1', type: 'currency', negativeInRed: true, width: 70, hidden: true},
						{header: 'Local Accrual 2', dataIndex: 'localAccrual2', type: 'currency', negativeInRed: true, width: 70, hidden: true},
						{header: 'Local Accrual', dataIndex: 'localAccrual', type: 'currency', negativeInRed: true, width: 70, hidden: true},
						{header: 'Base Accrual 1', dataIndex: 'baseAccrual1', type: 'currency', negativeInRed: true, width: 70, hidden: true, summaryType: 'sum'},
						{header: 'Base Accrual 2', dataIndex: 'baseAccrual2', type: 'currency', negativeInRed: true, width: 70, hidden: true, summaryType: 'sum'},
						{header: 'Base Accrual', dataIndex: 'baseAccrual', type: 'currency', negativeInRed: true, width: 70, summaryType: 'sum', hidden: true, viewNames: ['Collateral']},
						{header: 'Local Notional', dataIndex: 'localNotional', type: 'currency', negativeInRed: true, width: 80, hidden: true},
						{header: 'Base Notional', dataIndex: 'baseNotional', type: 'currency', negativeInRed: true, width: 80, summaryType: 'sum', viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral']},
						{header: 'Local Market Value', dataIndex: 'localMarketValue', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 85, hidden: true},
						{header: 'Base Market Value', dataIndex: 'baseMarketValue', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 85, summaryType: 'sum', viewNames: ['Default View', 'With Factor Changes', 'Inflation Linked', 'Collateral']}
					],
					plugins: {ptype: 'gridsummary'},
					switchToViewBeforeReload: function(viewName) {
						// Default Instrument Group Filter to CCY Forwards for Collateral View
						if (viewName === 'Collateral') {
							const igField = TCG.getChildByName(this.getTopToolbar(), 'investmentGroupId');
							TCG.data.getDataPromiseUsingCaching('investmentGroupByName.json?requestedPropertiesRoot=data&requestedProperties=id|name&name=Currency Forwards', this, 'investment.group.Currency Forwards')
								.then(function(data) {
									const record = {text: data.name, value: data.id};
									igField.setValue(record);
									igField.fireEvent('select', igField, record);
								});
							return true; // cancel reload: select event will reload
						}
					},
					getLoadParams: function(first) {
						const tb = this.getTopToolbar();
						const lp = {readUncommittedRequested: true};
						lp.snapshotDate = TCG.getChildByName(tb, 'snapshotDate').getValue().format('m/d/Y');
						let v = TCG.getChildByName(tb, 'investmentGroupId').getValue();
						if (v) {
							lp.investmentGroupId = v;
						}
						v = TCG.getChildByName(tb, 'securityGroupId').getValue();
						if (v) {
							lp.securityGroupId = v;
						}
						v = TCG.getChildByName(tb, 'clientInvestmentAccountGroupId').getValue();
						if (v) {
							lp.clientInvestmentAccountGroupId = v;
						}
						v = TCG.getChildByName(tb, 'holdingInvestmentAccountGroupId').getValue();
						if (v) {
							lp.holdingInvestmentAccountGroupId = v;
						}

						if (this.retrieveBloombergFields) {
							lp.dataSourceNameForAdvancedFields = 'Bloomberg';
						}

						return lp;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Security Group', xtype: 'toolbar-combo', name: 'securityGroupId', width: 150, url: 'investmentSecurityGroupListFind.json'},
							{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'},
							{fieldLabel: 'Holding Acct Group', xtype: 'toolbar-combo', name: 'holdingInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'},
							{fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'snapshotDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')}
						];
					},
					editor: {
						detailPageClass: 'Clifton.accounting.gl.TransactionListWindow',
						drillDownOnly: true,
						getDefaultData: function(gridPanel, row) {
							const data = row.json;
							return [
								{field: 'clientInvestmentAccountId', comparison: 'EQUALS', value: data.clientInvestmentAccount.id},
								{field: 'holdingInvestmentAccountId', comparison: 'EQUALS', value: data.holdingInvestmentAccount.id},
								{field: 'accountingAccountId', comparison: 'EQUALS', value: data.accountingAccount.id},
								{field: 'investmentSecurityId', comparison: 'EQUALS', value: data.investmentSecurity.id},
								{field: 'transactionDate', comparison: 'LESS_THAN', value: new Date(TCG.getChildByName(gridPanel.getTopToolbar(), 'snapshotDate').value).add(Date.DAY, 1).format('m/d/Y')}
							];
						}
					}
				}]
			},


			{
				title: 'Lot-Level Positions',
				items: [{
					xtype: 'gridpanel',
					name: 'dwAccountingPositionSnapshotListFind',
					instructions: 'Lot-Level positions from the Data Warehouse. Accounting positions snapshot is built from the General Ledger and market data. Positions may get stale and may need to be rebuilt in order to see accurate data when GL or market data changes.',
					pageSize: 200,
					reloadOnRender: false,
					appendStandardColumns: false,
					columns: [
						{header: 'Transaction ID', dataIndex: 'accountingTransactionId', width: 50, type: 'int', hidden: true},
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 140, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', width: 140, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.name', width: 140, hidden: true, filter: {type: 'combo', searchFieldName: 'holdingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 90, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}, hidden: true},
						{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 50, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', dataIndex: 'investmentSecurity.name', width: 70, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'CCY Denom', dataIndex: 'investmentSecurity.instrument.tradingCurrency.symbol', width: 50, filter: false, sortable: false, hidden: true},
						{header: 'Qty', dataIndex: 'quantity', type: 'float', negativeInRed: true, positiveInGreen: true, width: 50},
						{header: 'Price', dataIndex: 'snapshotPrice', type: 'float', width: 50},
						{header: 'FX Rate', dataIndex: 'snapshotExchangeRateToBase', type: 'float', width: 50},
						{header: 'Local Cost', dataIndex: 'localCost', type: 'currency', negativeInRed: true, width: 80, hidden: true},
						{header: 'Base Cost', dataIndex: 'baseCost', type: 'currency', negativeInRed: true, width: 80, summaryType: 'sum', hidden: true},
						{header: 'Local Cost Basis', dataIndex: 'localCostBasis', type: 'currency', negativeInRed: true, width: 80, hidden: true},
						{header: 'Base Cost Basis', dataIndex: 'baseCostBasis', type: 'currency', negativeInRed: true, width: 80, summaryType: 'sum', hidden: true},
						{header: 'Local Accrual 1', dataIndex: 'localAccrual1', type: 'currency', negativeInRed: true, width: 80, hidden: true},
						{header: 'Local Accrual 2', dataIndex: 'localAccrual2', type: 'currency', negativeInRed: true, width: 80, hidden: true},
						{header: 'Local Accrual', dataIndex: 'localAccrual', type: 'currency', negativeInRed: true, width: 80, hidden: true},
						{header: 'Base Accrual 1', dataIndex: 'baseAccrual1', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
						{header: 'Base Accrual 2', dataIndex: 'baseAccrual2', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
						{header: 'Base Accrual', dataIndex: 'baseAccrual', type: 'currency', negativeInRed: true, width: 80, summaryType: 'sum'},
						{header: 'Local Notional', dataIndex: 'localNotional', type: 'currency', negativeInRed: true, width: 80, hidden: true},
						{header: 'Base Notional', dataIndex: 'baseNotional', type: 'currency', negativeInRed: true, width: 80, summaryType: 'sum'},
						{header: 'Local Market Value', dataIndex: 'localMarketValue', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 85, hidden: true},
						{header: 'Base Market Value', dataIndex: 'baseMarketValue', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 85, summaryType: 'sum'}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function(first) {
						const tb = this.getTopToolbar();
						const lp = {readUncommittedRequested: true};
						lp.snapshotDate = TCG.getChildByName(tb, 'snapshotDate').getValue().format('m/d/Y');
						let v = TCG.getChildByName(tb, 'investmentGroupId').getValue();
						if (v) {
							lp.investmentGroupId = v;
						}
						v = TCG.getChildByName(tb, 'securityGroupId').getValue();
						if (v) {
							lp.securityGroupId = v;
						}
						v = TCG.getChildByName(tb, 'clientInvestmentAccountGroupId').getValue();
						if (v) {
							lp.clientInvestmentAccountGroupId = v;
						}
						v = TCG.getChildByName(tb, 'holdingInvestmentAccountGroupId').getValue();
						if (v) {
							lp.holdingInvestmentAccountGroupId = v;
						}
						return lp;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Security Group', xtype: 'toolbar-combo', name: 'securityGroupId', width: 150, url: 'investmentSecurityGroupListFind.json'},
							{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'},
							{fieldLabel: 'Holding Acct Group', xtype: 'toolbar-combo', name: 'holdingInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'},
							{fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'snapshotDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')}
						];
					},
					editor: {
						detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.accountingTransactionId;
						}
					}
				}]
			},


			{
				title: 'Balance Snapshots',
				items: [{
					xtype: 'gridpanel',
					name: 'dwAccountingBalanceSnapshotListFind',
					instructions: 'Aggregation of GL Balances from the beginning of time to the Snapshot Date grouped by Client Account, Holding Account, Investment Security and GL Account. Also includes calculated balances for Unrealized Gain/Loss, Accruals, Currency Translation Gain/Loss, and Market Value Adjustment.',
					pageSize: 200,
					reloadOnRender: false,
					appendStandardColumns: false,
					columns: [
						{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 140, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', width: 140, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.name', width: 140, hidden: true, filter: {type: 'combo', searchFieldName: 'holdingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 60, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', dataIndex: 'investmentSecurity.name', width: 80, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'GL Account Type', dataIndex: 'accountingAccount.accountType.name', width: 60, filter: {type: 'combo', searchFieldName: 'accountingAccountTypeId', url: 'accountingAccountTypeList.json', showNotEquals: true, loadAll: true}, hidden: true},
						{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 100, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'name', url: 'accountingAccountListFind.json', showNotEquals: true}},
						{header: 'Local Balance', dataIndex: 'localDebitCreditBalance', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 50},
						{header: 'Base Balance', dataIndex: 'baseDebitCreditBalance', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 50, summaryType: 'sum'},
						{header: 'Snapshot Date', dataIndex: 'snapshotDate', width: 50, hidden: true}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function(first) {
						const tb = this.getTopToolbar();
						const lp = {readUncommittedRequested: true};
						let v = TCG.getChildByName(tb, 'snapshotDate').getValue();
						if (v) {
							lp.snapshotDate = v.format('m/d/Y');
						}
						v = TCG.getChildByName(tb, 'investmentGroupId').getValue();
						if (v) {
							lp.investmentGroupId = v;
						}
						v = TCG.getChildByName(tb, 'securityGroupId').getValue();
						if (v) {
							lp.securityGroupId = v;
						}
						v = TCG.getChildByName(tb, 'clientInvestmentAccountGroupId').getValue();
						if (v) {
							lp.clientInvestmentAccountGroupId = v;
						}
						v = TCG.getChildByName(tb, 'holdingInvestmentAccountGroupId').getValue();
						if (v) {
							lp.holdingInvestmentAccountGroupId = v;
						}
						v = TCG.getChildByName(tb, 'accountingAccountGroupId').getValue();
						if (v) {
							lp.accountingAccountGroupId = v;
						}
						return lp;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 145, url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Security Group', xtype: 'toolbar-combo', name: 'securityGroupId', width: 150, url: 'investmentSecurityGroupListFind.json'},
							{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientInvestmentAccountGroupId', width: 145, url: 'investmentAccountGroupListFind.json'},
							{fieldLabel: 'Holding Acct Group', xtype: 'toolbar-combo', name: 'holdingInvestmentAccountGroupId', width: 145, url: 'investmentAccountGroupListFind.json'},
							{fieldLabel: 'GL Acct Group', xtype: 'toolbar-combo', name: 'accountingAccountGroupId', width: 145, url: 'accountingAccountGroupListFind.json'},
							{fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'snapshotDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')}
						];
					}
				}]
			},


			{
				title: 'Rebuild',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						loadValidation: false, // using the form only to get background color/padding
						height: 200,
						labelWidth: 180,
						instructions: 'Many reports use precalculated data from the Data Warehouse which can be updated using this section. Rebuild for a specific date or for multiple dates based on filters below. Dimensions (investment securities, investment accounts, accounting accounts, etc.) change infrequently and are rebuilt automatically every night.',
						listeners: {
							afterrender: function() {
								this.setFormValue('startSnapshotDate', Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), true);
								this.setFormValue('endSnapshotDate', Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), true);
							}
						},
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .60,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'clientAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', mutuallyExclusiveFields: ['groupName']},
											{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['accountLabel'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
											{boxLabel: 'Rebuild each month end from client\'s inception date', name: 'buildEachMonthEnd', xtype: 'checkbox'}
										]
									}]
								},
								{columnWidth: .15, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .25,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 100,
										items: [
											{fieldLabel: 'Start Date', name: 'startSnapshotDate', xtype: 'datefield', allowBlank: false},
											{fieldLabel: 'End Date', name: 'endSnapshotDate', xtype: 'datefield', allowBlank: false},
											{fieldLabel: '', boxLabel: 'Allow Today Snapshots', name: 'allowTodaySnapshots', xtype: 'checkbox', qtip: 'By default the last snapshot allowed is for the previous day.  Checking this box will allow you to rebuild snapshots for today.'},
											{fieldLabel: '', boxLabel: 'Allow Future Snapshots', name: 'allowFutureSnapshots', xtype: 'checkbox', qtip: 'By default the last snapshot allowed is for the previous day.  Checking this box will allow you to rebuild snapshots for the future (limit of no more than 60 days).'}
										]
									}]
								}
							]
						}],
						buttons: [
							{
								text: 'Rebuild Dimensions',
								tooltip: 'Update investment security, investment account and accounting account information.',
								iconCls: 'run',
								width: 170,
								handler: function() {
									const loader = new TCG.data.JsonLoader({
										onLoad: function(record, conf) {
											TCG.showInfo('Data Warehouse Dimension tables (updates to investment securities, investment accounts, accounting accounts, etc.) have been rebuilt.', 'Data Warehouse');
										}
									});
									loader.load('dwDimensionsRebuild.json');
								}
							},
							{
								text: 'Rebuild Snapshots',
								tooltip: 'Update position and balance snapshot information',
								iconCls: 'run',
								width: 170,
								handler: function() {
									const owner = this.findParentByType('formpanel');
									const form = owner.getForm();
									const url = 'dwAccountingSnapshotsRebuild.json?UI_SOURCE=DW_Analytics';
									form.submit(Ext.applyIf({
										url: encodeURI(url),
										waitMsg: 'Rebuilding...',
										success: function(form, action) {
											Ext.Msg.alert('Processing Started', action.result.result.message, function() {
												const grid = owner.ownerCt.items.get(1);
												grid.reload.defer(300, grid);
											});
										}
									}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
								}
							}
						]
					},

					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'DW-SNAPSHOTS',
						instantRunner: true,
						title: 'Current Processing',
						flex: 1
					}
				]
			}
		]
	}]
});
