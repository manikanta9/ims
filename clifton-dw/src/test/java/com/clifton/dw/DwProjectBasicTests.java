package com.clifton.dw;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class DwProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "dw";
	}


	@Override
	public void configureApprovedClassImports(List<String> imports) {
		imports.add("java.sql.");
		imports.add("org.springframework.dao.");
		imports.add("org.springframework.jdbc.");
	}


	@Override
	public boolean isMethodSkipped(Method method) {
		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("getAccountingBalanceSnapshotList");
		return skipMethods.contains((method.getName()));
	}
}
