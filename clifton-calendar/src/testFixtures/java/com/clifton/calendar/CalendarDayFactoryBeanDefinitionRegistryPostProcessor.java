package com.clifton.calendar;

import com.clifton.calendar.holiday.CalendarHoliday;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.setup.CalendarDay;
import com.clifton.calendar.setup.CalendarMonth;
import com.clifton.calendar.setup.CalendarWeekday;
import com.clifton.calendar.setup.CalendarYear;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.config.ListFactoryBean;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.ManagedList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;


/**
 * The {@link CalendarDayFactoryBeanDefinitionRegistryPostProcessor} is a {@link BeanDefinitionRegistryPostProcessor} for automatically generating {@link CalendarDay} beans and
 * all required associated entities, such as {@link CalendarYear} and {@link CalendarMonth} entities. This is used to automatically generate dates for testing purposes without
 * having to manually enter each individual bean definition.
 * <p>
 * Example usage:
 * <pre><code>
 * &lt;-- Create days between 12/28/2008 and 01/07/2009, including New Year's Day as a holiday --&gt;
 * &lt;bean class="com.clifton.calendar.CalendarDayFactoryBeanDefinitionRegistryPostProcessor" p:startDate="12/28/2008" p:endDate="01/07/2009" p:holidayCalendarRef="calendar_us"&gt;
 *     &lt;property name="holidayDateList"&gt;
 *         &lt;util:list&gt;
 *             &lt;value&gt;01/01/2009&lt;/value&gt;
 *         &lt;/util:list&gt;
 *     &lt;/property&gt;
 * &lt;/bean&gt;
 *
 * &lt;-- Create days between 10/01/2012 and 11/30/2012, including a few separate holidays --&gt;
 * &lt;bean class="com.clifton.calendar.CalendarDayFactoryBeanDefinitionRegistryPostProcessor" p:startDate="10/01/2012" p:endDate="11/30/2012" p:holidayCalendarRef="calendar_us"&gt;
 *     &lt;property name="holidayDateList"&gt;
 *         &lt;util:list&gt;
 *             &lt;value&gt;10/06/2012&lt;/value&gt;
 *             &lt;value&gt;10/07/2012&lt;/value&gt;
 *             &lt;value&gt;10/08/2012&lt;/value&gt;
 *         &lt;/util:list&gt;
 *     &lt;/property&gt;
 * &lt;/bean&gt;
 * </code></pre>
 *
 * @author MikeH
 */
@SuppressWarnings("MagicConstant")
public class CalendarDayFactoryBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

	private static final String YEAR_BEAN_NAME_PREFIX = "calendar_year_";
	private static final String MONTH_BEAN_NAME_PREFIX = "calendar_month_";
	private static final String WEEKDAY_BEAN_NAME_PREFIX = "calendar_weekday_";
	private static final String DAY_BEAN_NAME_PREFIX = "calendar_day_";
	private static final String HOLIDAY_BEAN_NAME_PREFIX = "calendar_holiday_";
	private static final String HOLIDAY_DAY_BEAN_NAME_PREFIX = "calendar_holiday_day_";

	private String startDate;
	private String endDate;
	private List<String> holidayDateList = new ArrayList<>();
	private Map<String, String> holidayByNameMap = new HashMap<>();
	private String holidayCalendarRef;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		Date startDate = DateUtils.toDate(getStartDate());
		Date endDate = DateUtils.toDate(getEndDate());
		AssertUtils.assertTrue(DateUtils.isDateBefore(startDate, endDate, false), "The start date must be before the end date when generating calendar days.");
		List<Date> dateList = DateUtils.getDateListForRange(startDate, endDate);
		for (Date date : dateList) {
			getCalendarDayBeanDefinitionName(date, registry);
		}
		for (Date holidayDate : CollectionUtils.getConverted(getHolidayDateList(), DateUtils::toDate)) {
			getCalendarHolidayBeanDefinitionName(null, holidayDate, registry);
		}
		for (Map.Entry<String, String> holidayByName : getHolidayByNameMap().entrySet()) {
			getCalendarHolidayBeanDefinitionName(holidayByName.getKey(), DateUtils.toDate(holidayByName.getValue()), registry);
		}
	}


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		// Do nothing
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getCalendarDayBeanDefinitionName(Date date, BeanDefinitionRegistry registry) {
		int daysSinceNaturalStart = DateUtils.getDaysSinceNaturalStart(date);
		String dayBeanDefinitionName = DAY_BEAN_NAME_PREFIX + daysSinceNaturalStart;
		if (!registry.containsBeanDefinition(dayBeanDefinitionName)) {
			// Prepare calendar object
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);

			// Prepare calendar day
			registry.registerBeanDefinition(dayBeanDefinitionName, BeanDefinitionBuilder.genericBeanDefinition(CalendarDay.class)
					.addPropertyValue("id", daysSinceNaturalStart)
					.addPropertyValue("dayNumberInMonth", calendar.get(Calendar.DAY_OF_MONTH))
					.addPropertyValue("dayNumberInYear", calendar.get(Calendar.DAY_OF_YEAR))
					.addPropertyValue("startDate", calendar.getTime())
					.addPropertyValue("endDate", DateUtils.addDays(calendar.getTime(), 1))
					.addPropertyReference("year", getCalendarYearBeanDefinitionName(calendar, registry))
					.addPropertyReference("month", getCalendarMonthBeanDefinitionName(calendar, registry))
					.addPropertyReference("weekday", getCalendarWeekdayBeanDefinitionName(calendar, registry))
					.getBeanDefinition());
			addBeanReferenceToDao(dayBeanDefinitionName, CalendarDay.class, registry);
		}
		return dayBeanDefinitionName;
	}


	private String getCalendarHolidayBeanDefinitionName(String holidayName, Date date, BeanDefinitionRegistry registry) {
		Short holidayId = TestUtils.generateId(Short.class);
		String holidayBeanDefinitionName = HOLIDAY_BEAN_NAME_PREFIX + holidayId;
		if (!registry.containsBeanDefinition(holidayBeanDefinitionName)) {
			registry.registerBeanDefinition(holidayBeanDefinitionName, BeanDefinitionBuilder.genericBeanDefinition(CalendarHoliday.class)
					.addPropertyValue("id", holidayId)
					.addPropertyValue("name", ObjectUtils.coalesce(holidayName, "Test Holiday " + holidayId))
					.getBeanDefinition());
			addBeanReferenceToDao(holidayBeanDefinitionName, CalendarHoliday.class, registry);

			Short holidayDayId = TestUtils.generateId(Short.class);
			String holidayDayBeanDefinitionName = HOLIDAY_DAY_BEAN_NAME_PREFIX + holidayDayId;
			registry.registerBeanDefinition(holidayDayBeanDefinitionName, BeanDefinitionBuilder.genericBeanDefinition(CalendarHolidayDay.class)
					.addPropertyValue("id", holidayDayId)
					.addPropertyReference("calendar", getHolidayCalendarRef())
					.addPropertyReference("holiday", holidayBeanDefinitionName)
					.addPropertyReference("day", getCalendarDayBeanDefinitionName(date, registry))
					.addPropertyValue("tradeHoliday", true)
					.addPropertyValue("settlementHoliday", true)
					.addPropertyValue("fullDayHoliday", true)
					.getBeanDefinition());
			addBeanReferenceToDao(holidayDayBeanDefinitionName, CalendarHolidayDay.class, registry);
		}
		return holidayBeanDefinitionName;
	}


	private String getCalendarWeekdayBeanDefinitionName(Calendar calendar, BeanDefinitionRegistry registry) {
		String weekdayBeanDefinitionName = WEEKDAY_BEAN_NAME_PREFIX + calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG_STANDALONE, Locale.getDefault()).toLowerCase();
		if (!registry.containsBeanDefinition(weekdayBeanDefinitionName)) {
			int weekdayNumber = calendar.get(Calendar.DAY_OF_WEEK);
			registry.registerBeanDefinition(weekdayBeanDefinitionName, BeanDefinitionBuilder.genericBeanDefinition(CalendarWeekday.class)
					.addPropertyValue("id", weekdayNumber)
					.addPropertyValue("name", calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG_STANDALONE, Locale.getDefault()))
					.addPropertyValue("shortName", calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT_STANDALONE, Locale.getDefault()))
					.getBeanDefinition());
			addBeanReferenceToDao(weekdayBeanDefinitionName, CalendarWeekday.class, registry);
		}
		return weekdayBeanDefinitionName;
	}


	private String getCalendarMonthBeanDefinitionName(Calendar calendar, BeanDefinitionRegistry registry) {
		String monthBeanDefinitionName = MONTH_BEAN_NAME_PREFIX + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG_STANDALONE, Locale.getDefault()).toLowerCase();
		if (!registry.containsBeanDefinition(monthBeanDefinitionName)) {
			int monthNumber = calendar.get(Calendar.MONTH);
			final int daysInMonth;
			final int leapYearDaysInMonth;
			if (monthNumber == 1) {
				// February
				daysInMonth = 28;
				leapYearDaysInMonth = 29;
			}
			else if ((monthNumber < 7 && monthNumber % 2 == 0) // Every other month up to July has 31 days
					|| (monthNumber >= 7 && monthNumber % 2 == 1)) { // Every other month from August onward has 31 days
				daysInMonth = 31;
				leapYearDaysInMonth = 31;
			}
			else {
				daysInMonth = 30;
				leapYearDaysInMonth = 30;
			}
			registry.registerBeanDefinition(monthBeanDefinitionName, BeanDefinitionBuilder.genericBeanDefinition(CalendarMonth.class)
					.addPropertyValue("id", monthNumber)
					.addPropertyValue("name", calendar.getDisplayName(Calendar.MONTH, Calendar.LONG_STANDALONE, Locale.getDefault()))
					.addPropertyValue("shortName", calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT_STANDALONE, Locale.getDefault()))
					.addPropertyValue("daysInMonth", daysInMonth)
					.addPropertyValue("leapYearDaysInMonth", leapYearDaysInMonth)
					.getBeanDefinition());
			addBeanReferenceToDao(monthBeanDefinitionName, CalendarMonth.class, registry);
		}
		return monthBeanDefinitionName;
	}


	private String getCalendarYearBeanDefinitionName(Calendar calendar, BeanDefinitionRegistry registry) {
		String yearBeanName = YEAR_BEAN_NAME_PREFIX + calendar.get(Calendar.YEAR);
		if (!registry.containsBeanDefinition(yearBeanName)) {
			int yearNumber = calendar.get(Calendar.YEAR);
			boolean leapYear = yearNumber % 4 == 0; // Rough estimate, but correct for the foreseeable future
			registry.registerBeanDefinition(yearBeanName, BeanDefinitionBuilder.genericBeanDefinition(CalendarYear.class)
					.addPropertyValue("id", yearNumber)
					.addPropertyValue("leapYear", leapYear)
					.addPropertyValue("daysInYear", leapYear ? 366 : 365)
					.getBeanDefinition());
			addBeanReferenceToDao(yearBeanName, CalendarYear.class, registry);
		}
		return yearBeanName;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void addBeanReferenceToDao(String beanName, Class<?> beanClass, BeanDefinitionRegistry beanFactory) {
		String daoDataListBeanName = StringUtils.deCapitalize(beanClass.getSimpleName()) + "DAO_data";
		final List<RuntimeBeanReference> daoReferenceList;
		if (beanFactory.containsBeanDefinition(daoDataListBeanName)) {
			// Extract source list from bean definition
			BeanDefinition daoDataListBeanDefinition = beanFactory.getBeanDefinition(daoDataListBeanName);
			if (ListFactoryBean.class.getName().equals(daoDataListBeanDefinition.getBeanClassName())) {
				// <util:list> definitions
				@SuppressWarnings("unchecked")
				List<RuntimeBeanReference> sourceList = (List<RuntimeBeanReference>) Objects.requireNonNull(daoDataListBeanDefinition.getPropertyValues().getPropertyValue("sourceList")).getValue();
				daoReferenceList = Objects.requireNonNull(sourceList);
			}
			else if (List.class.isAssignableFrom(CoreClassUtils.getClass(daoDataListBeanDefinition.getBeanClassName()))) {
				// Code-generated definitions
				ConstructorArgumentValues.ValueHolder sourceListConstructorArgument = Objects.requireNonNull(daoDataListBeanDefinition.getConstructorArgumentValues().getIndexedArgumentValue(0, List.class));
				@SuppressWarnings("unchecked")
				List<RuntimeBeanReference> sourceList = (List<RuntimeBeanReference>) sourceListConstructorArgument.getValue();
				daoReferenceList = Objects.requireNonNull(sourceList);
			}
			else {
				throw new RuntimeException(String.format("The DAO data list with name [%s] and type [%s] does not match any of the expected types: %s.",
						daoDataListBeanName,
						daoDataListBeanDefinition.getBeanClassName(),
						Arrays.asList(ListFactoryBean.class.getName(), List.class.getName())));
			}
		}
		else {
			// Create new source list
			daoReferenceList = new ManagedList<>();
			AbstractBeanDefinition daoDataListBeanDefinition = BeanDefinitionBuilder.genericBeanDefinition(ArrayList.class)
					.addConstructorArgValue(daoReferenceList)
					.getBeanDefinition();
			beanFactory.registerBeanDefinition(daoDataListBeanName, daoDataListBeanDefinition);
		}
		Objects.requireNonNull(daoReferenceList);
		daoReferenceList.add(new RuntimeBeanReference(beanName));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getStartDate() {
		return this.startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public String getEndDate() {
		return this.endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public List<String> getHolidayDateList() {
		return this.holidayDateList;
	}


	public void setHolidayDateList(List<String> holidayDateList) {
		this.holidayDateList = holidayDateList;
	}


	public Map<String, String> getHolidayByNameMap() {
		return this.holidayByNameMap;
	}


	public void setHolidayByNameMap(Map<String, String> holidayByNameMap) {
		this.holidayByNameMap = holidayByNameMap;
	}


	public String getHolidayCalendarRef() {
		return this.holidayCalendarRef;
	}


	public void setHolidayCalendarRef(String holidayCalendarRef) {
		this.holidayCalendarRef = holidayCalendarRef;
	}
}
