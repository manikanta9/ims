package com.clifton.calendar.setup;

public class CalendarBuilder {

	private Calendar calendar;


	private CalendarBuilder(Calendar calendar) {
		this.calendar = calendar;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static CalendarBuilder createNoHoliday() {
		Calendar calendar = new Calendar();

		calendar.setId((short) 1);
		calendar.setName("No Holiday Calendar");
		calendar.setDescription("A calendar with no holidays.");
		calendar.setSystemDefined(true);

		return new CalendarBuilder(calendar);
	}


	public static CalendarBuilder createUSBanks() {
		Calendar calendar = new Calendar();

		calendar.setId((short) 2);
		calendar.setName("US Banks Calendar");
		calendar.setDescription("United States bank holidays. Holidays need to be assigned for each year as they become known. Also known as New York.");
		calendar.setSystemDefined(true);

		return new CalendarBuilder(calendar);
	}


	public static CalendarBuilder createSystem() {
		Calendar calendar = new Calendar();

		calendar.setId((short) 4);
		calendar.setName("System Calendar");
		calendar.setDescription("The default calendar for the system. Also used for all US Exchanges for CDR code EX");
		calendar.setCode("EX");
		calendar.setSystemDefined(true);

		return new CalendarBuilder(calendar);
	}


	public static CalendarBuilder createUKGreatBritain() {
		Calendar calendar = new Calendar();

		calendar.setId((short) 12);
		calendar.setName("UK - Great Britain");
		calendar.setDescription("United Kingdom National Holidays.  Also known as London.");
		calendar.setCode("GB");

		return new CalendarBuilder(calendar);
	}


	public static CalendarBuilder createNYSE() {
		Calendar calendar = new Calendar();

		calendar.setId((short) 44);
		calendar.setName("New York Stock Exchange");
		calendar.setDescription("New York Stock Exchange (NY)");
		calendar.setCode("NY");

		return new CalendarBuilder(calendar);
	}


	public static CalendarBuilder createUSTrading() {
		Calendar calendar = new Calendar();

		calendar.setId((short) 100);
		calendar.setName("US Trading Calendar");
		calendar.setDescription("Trading Calendar for US.");
		calendar.setCode("#A");

		return new CalendarBuilder(calendar);
	}


	public static CalendarBuilder createChicagoMercantile() {
		Calendar calendar = new Calendar();

		calendar.setId((short) 103);
		calendar.setName("CME - Chicago Mercantile");
		calendar.setDescription("CME - Chicago Mercantile (CE)");
		calendar.setCode("CE");

		return new CalendarBuilder(calendar);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBuilder withId(short id) {
		getCalendar().setId(id);
		return this;
	}


	public CalendarBuilder withName(String name) {
		getCalendar().setName(name);
		return this;
	}


	public CalendarBuilder withDescription(String description) {
		getCalendar().setDescription(description);
		return this;
	}


	public CalendarBuilder withSystemDefined(boolean systemDefined) {
		getCalendar().setSystemDefined(systemDefined);
		return this;
	}


	public CalendarBuilder withCode(String code) {
		getCalendar().setCode(code);
		return this;
	}


	public Calendar toCalendar() {
		return this.calendar;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Calendar getCalendar() {
		return this.calendar;
	}
}
