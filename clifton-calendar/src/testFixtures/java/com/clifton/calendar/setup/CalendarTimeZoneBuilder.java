package com.clifton.calendar.setup;

public class CalendarTimeZoneBuilder {

	private CalendarTimeZone calendarTimeZone;


	private CalendarTimeZoneBuilder(CalendarTimeZone calendarTimeZone) {
		this.calendarTimeZone = calendarTimeZone;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static CalendarTimeZoneBuilder createEastern() {
		CalendarTimeZone calendarTimeZone = new CalendarTimeZone();

		calendarTimeZone.setId(35);
		calendarTimeZone.setName("America/New_York");
		calendarTimeZone.setDescription("Eastern Time (US and Canada)");
		calendarTimeZone.setGmtOffsetMinutes(-300);
		calendarTimeZone.setDstOn(true);
		calendarTimeZone.setActive(true);

		return new CalendarTimeZoneBuilder(calendarTimeZone);
	}


	public static CalendarTimeZoneBuilder createCentral() {
		CalendarTimeZone calendarTimeZone = new CalendarTimeZone();

		calendarTimeZone.setId(23);
		calendarTimeZone.setName("America/Chicago");
		calendarTimeZone.setDescription("Central Time (US and Canada)");
		calendarTimeZone.setGmtOffsetMinutes(-360);
		calendarTimeZone.setDstOn(true);
		calendarTimeZone.setActive(true);

		return new CalendarTimeZoneBuilder(calendarTimeZone);
	}


	public static CalendarTimeZoneBuilder createCentralTime() {
		CalendarTimeZone calendarTimeZone = new CalendarTimeZone();

		calendarTimeZone.setId(91);
		calendarTimeZone.setName("Etc/GMT+6");
		calendarTimeZone.setDescription("Central Time (US and Canada)");
		calendarTimeZone.setGmtOffsetMinutes(-360);
		calendarTimeZone.setActive(true);

		return new CalendarTimeZoneBuilder(calendarTimeZone);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarTimeZoneBuilder withId(int id) {
		getCalendarTimeZone().setId(id);
		return this;
	}


	public CalendarTimeZoneBuilder withName(String name) {
		getCalendarTimeZone().setName(name);
		return this;
	}


	public CalendarTimeZoneBuilder withDescription(String description) {
		getCalendarTimeZone().setDescription(description);
		return this;
	}


	public CalendarTimeZoneBuilder withDescription(int gmtOffsetMinutes) {
		getCalendarTimeZone().setGmtOffsetMinutes(gmtOffsetMinutes);
		return this;
	}


	public CalendarTimeZoneBuilder withDstOn(boolean dstOn) {
		getCalendarTimeZone().setDstOn(dstOn);
		return this;
	}


	public CalendarTimeZoneBuilder withActive(boolean active) {
		getCalendarTimeZone().setActive(active);
		return this;
	}


	public CalendarTimeZone toCalendarTimeZone() {
		return this.calendarTimeZone;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarTimeZone getCalendarTimeZone() {
		return this.calendarTimeZone;
	}
}
