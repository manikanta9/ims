package com.clifton.calendar;


import com.clifton.calendar.holiday.CalendarHoliday;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;

import java.util.List;


/**
 * Convenience methods for {@link CalendarHoliday}s.
 *
 * @author michaelm
 */
public class CalendarHolidayUtils {

	public static CalendarHoliday createCalendarHolidayWithDayForSchedule(CalendarHolidayService calendarHolidayService, Calendar calendar, String holidayName, String holidayDate) {
		CalendarHoliday calendarHoliday = new CalendarHoliday();
		try {
			calendarHoliday.setName(holidayName);
			calendarHoliday = calendarHolidayService.saveCalendarHoliday(calendarHoliday);
		}
		catch (Exception e) {
			calendarHoliday = calendarHolidayService.getCalendarHolidayByName(holidayName);
		}
		try {
			calendarHolidayService.linkCalendarHolidayToDay(calendar.getId(), calendarHoliday.getId(), DateUtils.toDate(holidayDate));
		}
		catch (Exception e) {
			// holiday already exists for the calendar associated with the given schedule
		}
		return calendarHoliday;
	}


	public static void removeCalendarHolidayAndDays(CalendarHolidayService calendarHolidayService, short holidayId) {
		List<CalendarHolidayDay> holidayDayList = calendarHolidayService.getCalendarHolidayDayListByHoliday(holidayId);
		for (CalendarHolidayDay holidayDay : CollectionUtils.getIterable(holidayDayList)) {
			/**
			 * this should use {@link CalendarHolidayService#deleteCalendarHolidayDayList(List)} but it is generating an error during integration tests
			 */
			calendarHolidayService.deleteCalendarHolidayDay(holidayDay.getId());
		}
		calendarHolidayService.deleteCalendarHoliday(holidayId);
	}
}
