package com.clifton.calendar.api;

import java.io.Serializable;


/**
 * The <code>Day</code> class defines a specific date
 * <p/>
 * The ID value is equal to the day number where January 1, 1900 would be
 * the first day and have an ID of 1.
 *
 * @author mitchellf
 */
public class Day implements Serializable {

	private Integer id;

	/**
	 * Represents the year, for example 2020, 1995, 2021
	 */
	private int year;

	/**
	 * Represents the month number.  Allowed values are 1-12, with 1 = January, 2 = February, etc.
	 */
	private int month;

	/**
	 * Represents the number of the day of the week. Allowed values are 1-7, with 1 = Sunday, 2 = Monday, etc.
	 */
	private int weekday;

	/**
	 * The number of the day of the month.  Allowed values vary depending on the month, can be 1-30, 1-31, 1-28 (for February), or 1-29 (for leap year)
	 */
	private int dayNumberInMonth;

	/**
	 * The number of the day in the year. Can be 1-365 (366 for leap year)
	 */
	private int dayNumberInYear;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public int getYear() {
		return this.year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public int getMonth() {
		return this.month;
	}


	public void setMonth(int month) {
		this.month = month;
	}


	public int getWeekday() {
		return this.weekday;
	}


	public void setWeekday(int weekday) {
		this.weekday = weekday;
	}


	public int getDayNumberInMonth() {
		return this.dayNumberInMonth;
	}


	public void setDayNumberInMonth(int dayNumberInMonth) {
		this.dayNumberInMonth = dayNumberInMonth;
	}


	public int getDayNumberInYear() {
		return this.dayNumberInYear;
	}


	public void setDayNumberInYear(int dayNumberInYear) {
		this.dayNumberInYear = dayNumberInYear;
	}
}
