package com.clifton.calendar.api.holiday;

import java.io.Serializable;


/**
 * The <code>Holiday</code> class defines a holiday
 * i.e. Labor Day, Christmas.
 *
 * @author mitchellf
 */
public class Holiday implements Serializable {

	private Short id;

	private String name;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
