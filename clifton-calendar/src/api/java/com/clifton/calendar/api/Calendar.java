package com.clifton.calendar.api;

import java.io.Serializable;
import java.util.List;


/**
 * Calendars define whether a given day is a holiday or a business day.
 * Each country usually has a unique calendar. Moreover, businesses (Stock Exchanges, Banks, etc.)
 * in the same country can use different calendars.
 * <p/>
 * For example, the US Bank Holidays would be one calendar.
 *
 * @author mitchellf
 */
public class Calendar implements Serializable {

	private Short id;

	private String name;

	/**
	 * Each calendar may have a short code (usually 2 letters) that uniquely identifies it and
	 * can be used to reference this calendar with integrating with other systems.
	 */
	private String code;

	/**
	 * Is the default calendar for the system
	 */
	private boolean defaultSystemCalendar;

	/**
	 * Indicates whether this calendar is a rollup of other calendars
	 */
	private boolean rollupCalendar;

	/**
	 * List of child calendars that are rolled up into this calendar, if this calendar is a rollup. Populated synthetically in getter.
	 */
	private List<Calendar> childCalendars;

	/**
	 * Indicates whether this calendar is system defined
	 */
	private boolean systemDefined;

	////////////////////////////////////////////////////////////////////////////
	////////                    Entity Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		if (isDefaultSystemCalendar()) {
			return getName() + " (DEFAULT)";
		}
		return getName();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCode() {
		return this.code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public boolean isDefaultSystemCalendar() {
		return this.defaultSystemCalendar;
	}


	public void setDefaultSystemCalendar(boolean defaultSystemCalendar) {
		this.defaultSystemCalendar = defaultSystemCalendar;
	}


	public boolean isRollupCalendar() {
		return this.rollupCalendar;
	}


	public void setRollupCalendar(boolean rollupCalendar) {
		this.rollupCalendar = rollupCalendar;
	}


	public List<Calendar> getChildCalendars() {
		return this.childCalendars;
	}


	public void setChildCalendars(List<Calendar> childCalendars) {
		this.childCalendars = childCalendars;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
}
