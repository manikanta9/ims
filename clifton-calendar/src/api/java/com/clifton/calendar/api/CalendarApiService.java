package com.clifton.calendar.api;

/**
 * Service to allow access to the Calendar API objects.  Implementation can vary - for example, it may use our hibernate access DAOs to retrieve objects from the database
 *
 * @author mitchellf
 */
public interface CalendarApiService {

	public Calendar getCalendar(short id);


	public Day getDay(int id);
}
