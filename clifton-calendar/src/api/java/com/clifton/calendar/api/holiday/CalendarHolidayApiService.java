package com.clifton.calendar.api.holiday;

/**
 * API to access Calendar Holiday-related objects. Implementation Could use hibernate DAO service to retrieve objects, but doesn't have to.
 *
 * @author mitchellf
 */
public interface CalendarHolidayApiService {

	public Holiday getHoliday(short id);


	public HolidayDay getHolidayDay(int id);


	public BusinessDay getBusinessDay(int id);
}
