package com.clifton.calendar.api.holiday;

import com.clifton.calendar.api.Calendar;
import com.clifton.calendar.api.Day;

import java.io.Serializable;


/**
 * The <code>HolidayDay</code> ties a given calendar to a holiday for a specified date.
 * <p>
 * Each holiday must be either a Trading and / or Settlement Holiday.  A holiday is only considered as a business day if the full day holiday flag = false.
 * Logic:
 * Business Day - not a weekend and not a full day holiday (trading or settlement)
 * Trading Business Day - not a weekend and not a full day holiday (trading)
 * Settlement Business Day - not a weekend and not a full day holiday (settlement)
 *
 * @author mitchellf
 */
public class HolidayDay implements Serializable {

	private Integer id;

	private Calendar calendar;
	private Holiday holiday;
	private Day day;

	/**
	 * Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays.
	 * Each holiday recorded must be either a Trading Holiday and/or a Settlement Holiday.
	 * <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic
	 * trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li>
	 * <li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed,
	 * clearing agency is closed, specified market is close, or some combination of the above.</li>
	 * <p>
	 * When applicable, use this field to determine if it's a trading day holiday.
	 */
	private boolean tradeHoliday;
	/**
	 * Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays.
	 * Each holiday recorded must be either a Trading Holiday and/or a Settlement Holiday.
	 * <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic
	 * trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li>
	 * <li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed,
	 * clearing agency is closed, specified market is close, or some combination of the above.</li>
	 * <p>
	 * When applicable, use this field to determine if it's a settlement day holiday.
	 */
	private boolean settlementHoliday;
	/**
	 * Most holidays are full day holidays.  However, some investment exchanges may have partial day holidays.  Partial Day holidays are still considered to be business days.
	 * In case of a partial day holiday, partial day start/end times should be used in order to determine if the exchange is open.
	 */
	private boolean fullDayHoliday;

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Calendar getCalendar() {
		return this.calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public Holiday getHoliday() {
		return this.holiday;
	}


	public void setHoliday(Holiday holiday) {
		this.holiday = holiday;
	}


	public Day getDay() {
		return this.day;
	}


	public void setDay(Day day) {
		this.day = day;
	}


	public boolean isTradeHoliday() {
		return this.tradeHoliday;
	}


	public void setTradeHoliday(boolean tradeHoliday) {
		this.tradeHoliday = tradeHoliday;
	}


	public boolean isSettlementHoliday() {
		return this.settlementHoliday;
	}


	public void setSettlementHoliday(boolean settlementHoliday) {
		this.settlementHoliday = settlementHoliday;
	}


	public boolean isFullDayHoliday() {
		return this.fullDayHoliday;
	}


	public void setFullDayHoliday(boolean fullDayHoliday) {
		this.fullDayHoliday = fullDayHoliday;
	}
}
