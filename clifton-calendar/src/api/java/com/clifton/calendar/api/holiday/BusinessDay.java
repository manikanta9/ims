package com.clifton.calendar.api.holiday;

import com.clifton.calendar.api.Calendar;
import com.clifton.calendar.api.Day;


/**
 * The <code>BusinessDay</code> class is a representation of a {@link com.clifton.calendar.api.Day} for a specific {@link com.clifton.calendar.api.Calendar}
 * and represents if it's a holiday or business day
 *
 * @author mitchellf
 */
public class BusinessDay extends Day {

	private Calendar calendar;

	private HolidayDay holidayDay;

	/**
	 * Not a Weekend and Not a Full Day Holiday (Trading or Settlement)
	 */
	private boolean businessDay;

	/**
	 * Not a Weekend and Not a Full Day Trading Holiday
	 */
	private boolean tradingBusinessDay;

	/**
	 * Not a Weekend and Not a Full Day Settlement Holiday
	 */
	private boolean settlementBusinessDay;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Calendar getCalendar() {
		return this.calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public HolidayDay getHolidayDay() {
		return this.holidayDay;
	}


	public void setHolidayDay(HolidayDay holidayDay) {
		this.holidayDay = holidayDay;
	}


	public boolean isBusinessDay() {
		return this.businessDay;
	}


	public void setBusinessDay(boolean businessDay) {
		this.businessDay = businessDay;
	}


	public boolean isTradingBusinessDay() {
		return this.tradingBusinessDay;
	}


	public void setTradingBusinessDay(boolean tradingBusinessDay) {
		this.tradingBusinessDay = tradingBusinessDay;
	}


	public boolean isSettlementBusinessDay() {
		return this.settlementBusinessDay;
	}


	public void setSettlementBusinessDay(boolean settlementBusinessDay) {
		this.settlementBusinessDay = settlementBusinessDay;
	}
}
