Ext.ns('Clifton.calendar', 'Clifton.calendar.holiday', 'Clifton.calendar.setup', 'Clifton.calendar.date');


Clifton.calendar.BUSINESS_DAY_CONVENTIONS = [
	['Next', 'NEXT', 'The first business day after the given holiday.'],
	['Next Modified', 'NEXT_MODIFIED', 'The first business day after the given holiday unless it is a different month.  Then use previous day before the given holiday.'],
	['Previous', 'PREVIOUS', 'The first business day before the given holiday.'],
	['Previous Modified', 'PREVIOUS_MODIFIED', 'The first business day before the given holiday unless it is a different month.  Then use next day after the given holiday.'],
	['Unadjusted', 'UNADJUSTED', 'Ignore business days.'],
	['Skip', 'SKIP', 'If the day is not a business day, then no result is returned. For MONTHLY and YEARLY schedules this means the occurrence for that month or that year is skipped.']
];


Clifton.calendar.date.DATE_GENERATION_OPTIONS = [
	['Business Days Back', 'BUSINESS_DAYS_BACK', 'Generates the date a given number of business days back, requires a day count.'],
	['Business Days Forward', 'BUSINESS_DAYS_FORWARD', 'Generates the date a given number of business days back, requires a day count.'],
	['Days Back', 'DAYS_BACK', 'Generates a date a given number of days back.'],
	['Days Forward', 'DAYS_FORWARD', 'Generates a date a given number of days forward.'],
	['First Day of the Previous Month', 'FIRST_DAY_OF_PREVIOUS_MONTH', 'Generates the first day of the previous month.'],
	['Last Day of the Previous Month', 'LAST_DAY_OF_PREVIOUS_MONTH', 'Generates the last day of the previous month.'],
	['Last Day of the Previous Quarter', 'LAST_DAY_OF_PREVIOUS_QUARTER', 'Generates the last day of the previous quarter.'],
	['Last Business Day of the Previous Month', 'LAST_BUSINESS_DAY_OF_PREVIOUS_MONTH', 'Generates the last business day of the previous month'],
	['Last Business Day of the Current Month', 'LAST_BUSINESS_DAY_OF_CURRENT_MONTH', 'Generates the last business day of the current month'],
	['Last Business Day of the Previous Quarter', 'LAST_BUSINESS_DAY_OF_PREVIOUS_QUARTER', 'Generates the last business day of the previous quarter'],
	['Previous Business Day', 'PREVIOUS_BUSINESS_DAY', 'Generates the previous business day.'],
	['Previous Week Day', 'PREVIOUS_WEEK_DAY', 'Generates the previous week day.'],
	['Today', 'TODAY', 'Generates the current date.'],
	['Weekdays Back', 'WEEKDAYS_BACK', 'Generates a date a given number of weekdays days back.  NOTE: 5 weekdays.'],
	['Weekdays Forward', 'WEEKDAYS_FORWARD', 'Generates a date a given number of weekdays days forward, if that date falls on the weekend, it will use the previous friday.']
];

Clifton.calendar.date.COMMON_DATE_GENERATION_OPTIONS = [
	['Previous Business Day', 'PREVIOUS_BUSINESS_DAY', 'Generates the previous business day.'],
	['2 Business Days Back', 'BUSINESS_DAYS_BACK:2', 'Generates a date two business days before the current date.'],
	['3 Business Days Back', 'BUSINESS_DAYS_BACK:3', 'Generates a date three business days before the current date.'],
	['1 Business Days Forward', 'BUSINESS_DAYS_FORWARD:1', 'Generates a date one business day after the current date.'],
	['2 Business Days Forward', 'BUSINESS_DAYS_FORWARD:2', 'Generates a date two business days after the current date.'],
	['One Day Back', 'DAYS_BACK:1', 'Generates a date one day before the current date.'],
	['First Day of Previous Month', 'FIRST_DAY_OF_PREVIOUS_MONTH', 'Generates the first day of the previous month.'],
	['Last Day of Previous Month', 'LAST_DAY_OF_PREVIOUS_MONTH', 'Generates the last day of the previous month.'],
	['Last Day of the Previous Quarter', 'LAST_DAY_OF_PREVIOUS_QUARTER', 'Generates the last day of the previous quarter.'],
	['Last Business Day of Previous Month', 'LAST_BUSINESS_DAY_OF_PREVIOUS_MONTH', 'Generates the last business day of the previous month'],
	['Last Business Day of the Previous Quarter', 'LAST_BUSINESS_DAY_OF_PREVIOUS_QUARTER', 'Generates the last business day of the previous quarter'],
	['Previous Week Day', 'PREVIOUS_WEEK_DAY', 'Generates the previous week day.'],
	['2 Weekdays Back', 'WEEKDAYS_BACK:2', 'Generates a date two weekdays before the current date.'],
	['3 Weekdays Days', 'WEEKDAYS_BACK:3', 'Generates a date three weekdays before the current date.'],
	['Today', 'TODAY', 'Generates the current date.'],

	['Weeks Back: 1', 'DAYS_BACK:7', 'Generates a date 7 days before the current date.'],
	['Weeks Back: 2', 'DAYS_BACK:14', 'Generates a date 14 days before the current date.'],
	['Weeks Back: 3', 'DAYS_BACK:21', 'Generates a date 21 days before the current date.'],
	['Weeks Back: 4', 'DAYS_BACK:28', 'Generates a date 28 days before the current date.'],

	['Weeks Forward: 1', 'DAYS_FORWARD:7', 'Generates a date 7 days after the current date.'],
	['Weeks Forward: 2', 'DAYS_FORWARD:14', 'Generates a date 14 days after the current date.'],

	['First Day of Current Year', 'FIRST_DAY_OF_YEAR', 'Generates a date that is the first day of the current year']
];

// returns Date object which is number of specified business days from today
Clifton.calendar.getBusinessDayFrom = function(days, forceSkipCache) {
	let result = TCG.CacheUtils.get('CALENDAR_BUSINESSDAY_CACHE', days);
	if (forceSkipCache || !result) {
		result = TCG.parseDate(TCG.getResponseText('calendarBusinessDayFrom.json?days=' + days));
		TCG.CacheUtils.put('CALENDAR_BUSINESSDAY_CACHE', result, days);
	}
	return result;
};

//returns Date object which is number of specified business days from the specified date
Clifton.calendar.getBusinessDayFromDate = function(date, days, forceSkipCache) {
	const dateStr = date.format('m/d/Y');
	const key = dateStr + ' ' + days;
	let result = TCG.CacheUtils.get('CALENDAR_BUSINESSDAY_CACHE', key);
	if (forceSkipCache || !result) {
		result = TCG.parseDate(TCG.getResponseText('calendarBusinessDayFrom.json?days=' + days + '&date=' + dateStr));
		TCG.CacheUtils.put('CALENDAR_BUSINESSDAY_CACHE', result, key);
	}
	return result;
};

//returns Date object which is number of specified business days from the specified date
Clifton.calendar.getBusinessDayFromDatePromise = function(date, days, forceSkipCache) {
	const dateStr = date.format('m/d/Y');
	const key = dateStr + ' ' + days;
	let result = TCG.CacheUtils.get('CALENDAR_BUSINESSDAY_CACHE', key);
	if (forceSkipCache || !result) {
		return TCG.data.getDataValuePromise('calendarBusinessDayFrom.json?days=' + days + '&date=' + dateStr)
			.then(function(d) {
				result = TCG.parseDate(d);
				TCG.CacheUtils.put('CALENDAR_BUSINESSDAY_CACHE', result, key);
				return result;
			});
	}
	return Promise.resolve(result);
};

Clifton.calendar.getBusinessDayFromDateForCalendar = function(date, days, calendarId, forceSkipCache) {
	const dateStr = date.format('m/d/Y');
	const key = dateStr + ' ' + days + ' ' + calendarId;
	let result = TCG.CacheUtils.get('CALENDAR_BUSINESSDAY_CACHE', key);
	if (forceSkipCache || !result) {
		result = TCG.parseDate(TCG.getResponseText('calendarBusinessDayFrom.json?days=' + days + '&date=' + dateStr + '&calendarIds=' + calendarId));
		TCG.CacheUtils.put('CALENDAR_BUSINESSDAY_CACHE', result, key);
	}
	return result;
};

Clifton.calendar.getCalendarByName = function(name, forceSkipCache) {
	let result = TCG.CacheUtils.get('CALENDAR_CACHE', name);
	if (forceSkipCache || !result) {
		result = TCG.data.getData('calendarByName.json?name=' + name);
		if (TCG.isNotNull(result)) {
			TCG.CacheUtils.put('CALENDAR_CACHE', result, name);
		}
	}
	return result;
};

Clifton.calendar.isBusinessDay = function(date, forceSkipCache) {
	const dateStr = date.format('m/d/Y');
	const key = dateStr;
	let result = TCG.CacheUtils.get('CALENDAR_BUSINESSDAY_CACHE', key);
	if (forceSkipCache || !result) {
		result = TCG.getResponseText('calendarBusinessDay.json?date=' + dateStr) === 'true';
		TCG.CacheUtils.put('CALENDAR_BUSINESSDAY_CACHE', result, key);
	}
	return result;
};

Clifton.calendar.isBusinessDayForCalendar = function(date, calendarId, forceSkipCache) {
	const dateStr = date.format('m/d/Y');
	const key = dateStr + ' ' + calendarId;
	let result = TCG.CacheUtils.get('CALENDAR_BUSINESSDAY_CACHE', key);
	if (forceSkipCache || !result) {
		result = TCG.getResponseText('calendarBusinessDay.json?date=' + dateStr + '&calendarIds=' + calendarId) === 'true';
		TCG.CacheUtils.put('CALENDAR_BUSINESSDAY_CACHE', result, key);
	}
	return result;
};

Clifton.calendar.getBusinessDaysBetween = function(startDate, endDate, calendarId) {
	const startDateStr = startDate.format('m/d/Y');
	const endDateStr = endDate.format('m/d/Y');
	return TCG.getResponseText('calendarBusinessDaysBetween.json?startDate=' + startDateStr + '&endDate=' + endDateStr + '&calendarIds=' + (calendarId ? calendarId : ''));
};

Clifton.calendar.CalendarHolidayDaysGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'calendarHolidayDayListFind',
	instructions: 'The following holidays are defined for this Calendar.',
	importTableName: 'CalendarHolidayDay',
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('day.startDate', {'after': new Date().add(Date.DAY, -90)});
		}
	},
	editor: {
		detailPageClass: 'Clifton.calendar.holiday.HolidayDayWindow',
		drillDownOnly: true
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Calendar', width: 150, dataIndex: 'calendar.name', idDataIndex:'calendar.id', filter: {searchFieldName: 'calendarId', type: 'combo', url: 'calendarListFind.json'}},
		{header: 'Holiday', width: 150, dataIndex: 'holiday.name', idDataIndex:'holiday.id', filter: {searchFieldName: 'holidayId', type: 'combo', url: 'calendarHolidayListFind.json'}},
		{header: 'Day', width: 100, dataIndex: 'day.startDate', filter: {searchFieldName: 'startDate'}, defaultSortColumn: true},
		{header: 'Weekday', width: 100, dataIndex: 'day.weekday.name', filter: {searchFieldName: 'weekdayName'}},
		{header: 'Year', width: 70, dataIndex: 'day.year.year', idDataIndex:'year.id', type: 'int', doNotFormat: true, filter: {searchFieldName: 'yearId'}},
		{header: 'Trade', width: 70, dataIndex: 'tradeHoliday', type: 'boolean', filter: {searchFieldName: 'tradeHoliday'}, tooltip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a trading day holiday.'},
		{header: 'Settle', width: 70, dataIndex: 'settlementHoliday', type: 'boolean', filter: {searchFieldName: 'settlementHoliday'}, tooltip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a settlement day holiday.'},
		{header: 'Full Day', width: 70, dataIndex: 'fullDayHoliday', type: 'boolean', filter: {searchFieldName: 'fullDayHoliday'}, tooltip: 'Most holidays are full day holidays.  However, some investment exchanges may have partial day holidays. In case of a partial day holiday, partial day start/end times should be used in order to determine if the exchange is open.'}
	]
});
Ext.reg('calendar-calendarHolidayDaysGrid', Clifton.calendar.CalendarHolidayDaysGrid);
