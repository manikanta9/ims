Clifton.calendar.holiday.HolidayWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Holiday',
	iconCls: 'calendar',
	width: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Enter holiday details below.  To view specific Calendar Days tied to this holiday, click on the Holiday Days tab.',
					url: 'calendarHoliday.json',
					items: [
						{fieldLabel: 'Holiday Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
					]
				}]
			},


			{
				title: 'Holiday Days',
				items: [{
					name: 'calendarHolidayDayListFind',
					xtype: 'gridpanel',
					instructions: 'The following days are tied to this holiday.  To add another date, select a Calendar, enter a date, and click Add.',
					importTableName: 'CalendarHolidayDay',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('day.startDate', {'after': new Date().add(Date.DAY, -90)});
						}
						return {'holidayId': this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Calendar', width: 200, dataIndex: 'calendar.name'},
						{header: 'Day', width: 100, dataIndex: 'day.startDate', filter: {searchFieldName: 'startDate'}, defaultSortColumn: true},
						{header: 'Weekday', width: 100, dataIndex: 'day.weekday.name'},
						{header: 'Year', width: 70, dataIndex: 'day.year.year', type: 'int', doNotFormat: true},
						{header: 'Trade', width: 70, dataIndex: 'tradeHoliday', type: 'boolean', tooltip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a trading day holiday.'},
						{header: 'Settle', width: 70, dataIndex: 'settlementHoliday', type: 'boolean', tooltip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a settlement day holiday.'},
						{header: 'Full Day', width: 70, dataIndex: 'fullDayHoliday', type: 'boolean', tooltip: 'Most holidays are full day holidays.  However, some investment exchanges may have partial day holidays. In case of a partial day holiday, partial day start/end times should be used in order to determine if the exchange is open.'}
					],
					editor: {
						detailPageClass: 'Clifton.calendar.holiday.HolidayDayWindow',
						deleteURL: 'calendarHolidayDayDelete.json',
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'calendar', url: 'calendarListFind.json?rollupCalendar=false', displayField: 'label', width: 120, listWidth: 230}));
							toolBar.add(new Ext.form.DateField({name: 'day', width: 85}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add Holiday To Calendar',
								iconCls: 'add',
								handler: function() {
									const holidayId = gridPanel.getWindow().getMainFormId();
									const calendarId = TCG.getChildByName(toolBar, 'calendar').getValue();
									let dateValue = '';
									if (TCG.isNotBlank(TCG.getChildByName(toolBar, 'day').getValue())) {
										dateValue = (TCG.getChildByName(toolBar, 'day').getValue()).format('m/d/Y');
									}

									if (TCG.isBlank(calendarId)) {
										TCG.showError('You must select a calendar.');
									}
									else if (TCG.isBlank(dateValue)) {
										TCG.showError('You must select a date.');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Adding...',
											params: {calendarId: calendarId, holidayId: holidayId, date: dateValue},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'day').reset();
											}
										});
										loader.load('calendarHolidayToDayLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			}
		]
	}]
});
