Clifton.calendar.holiday.HolidayDayWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Holiday Day',
	iconCls: 'calendar',
	width: 750,

	items: [{
		xtype: 'formpanel',
		instructions: 'Defines a Holiday along with its attributes for the specified Calendar on the specified Date.  Every holiday must be at least a Trading or Settlement holiday.  Business Days are deemed any weekday that is not a full day holiday.',
		url: 'calendarHolidayDay.json',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Calendar', name: 'calendar.name', hiddenName: 'calendar.id', xtype: 'combo', url: 'calendarListFind.json?rollupCalendar=false', detailPageClass: 'Clifton.calendar.setup.StandardCalendarWindow'},
			{fieldLabel: 'Holiday', name: 'holiday.name', hiddenName: 'holiday.id', xtype: 'combo', url: 'calendarHolidayListFind.json', detailPageClass: 'Clifton.calendar.holiday.HolidayWindow'},
			{name: 'day.id', xtype: 'hidden'},
			{fieldLabel: 'Date', name: 'day.startDate', hiddenName: 'day.id', xtype: 'datefield', readOnly: true, submitValue: false},

			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Trading Holiday', name: 'tradeHoliday', xtype: 'checkbox', qtip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a trading day holiday.'},
			{fieldLabel: 'Settlement Holiday', name: 'settlementHoliday', xtype: 'checkbox', qtip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a settlement day holiday.'},
			{fieldLabel: 'Full Day Holiday', name: 'fullDayHoliday', xtype: 'checkbox', qtip: 'Most holidays are full day holidays.  However, some investment exchanges may have partial day holidays. Partial Day holidays are still considered to be business days. In case of a partial day holiday, partial day start/end times should be used in order to determine if the exchange is open.'}
		]
	}]
});
