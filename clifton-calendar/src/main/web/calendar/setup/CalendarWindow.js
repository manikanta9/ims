Clifton.calendar.setup.CalendarWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'calendar.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.calendar.setup.StandardCalendarWindow';
		if (entity && entity.rollupCalendar) {
			className = 'Clifton.calendar.setup.RollupCalendarWindow';
		}
		return className;
	}
});
