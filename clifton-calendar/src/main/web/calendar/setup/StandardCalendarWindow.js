Clifton.calendar.setup.StandardCalendarWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Calendar (Standard)',
	iconCls: 'calendar',
	width: 850,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Calendar',
				items: [{
					xtype: 'formpanel',
					instructions: 'A calendar is used to classify dates.  To assign holidays to this calendar, click on the Holidays tab.',
					url: 'calendar.json',
					items: [
						{fieldLabel: 'Calendar Name', name: 'name'},
						{fieldLabel: 'Calendar Code', name: 'code', qtip: 'An internal code used to identify this calendar for integration with other systems.'},
						{fieldLabel: 'MIC', name: 'marketIdentifierCode', qtip: 'A Market Identifier Code (MIC) used to link this calendar to an investment exchange. These codes are used with standard calendars, and are not intended for use with rollup calendars.'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{xtype: 'system-entity-modify-condition-combo'},
						{fieldLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true}
					],
					listeners: {
						afterload: function(form) {
							if (form.getFormValue('systemDefined')) {
								TCG.getChildByName(form, 'name').setDisabled(true);
							}
						}
					}
				}]
			},


			{
				title: 'Holidays',
				items: [{
					xtype: 'calendar-calendarHolidayDaysGrid',
					instructions: 'The following holidays are defined for this Calendar.  To add another date, select a Holiday, enter a date, and click Add.',
					columnOverrides: [{dataIndex: 'calendar.name', hidden: 'true'}],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('day.startDate', {'after': new Date().add(Date.DAY, -90)});
						}
						return {'calendarId': this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.calendar.holiday.HolidayDayWindow',
						deleteURL: 'calendarHolidayDayDelete.json',
						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add(new TCG.form.ComboBox({name: 'holiday', url: 'calendarHolidayListFind.json?rollupCalendar=false', width: 180, listWidth: 230}));
							toolBar.add(new Ext.form.DateField({name: 'day', width: 85}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add Holiday To Calendar',
								iconCls: 'add',
								handler: function() {
									const calendarId = gridPanel.getWindow().getMainFormId();
									const holidayId = TCG.getChildByName(toolBar, 'holiday').getValue();
									let dateValue = '';
									if (TCG.isNotBlank(TCG.getChildByName(toolBar, 'day').getValue())) {
										dateValue = (TCG.getChildByName(toolBar, 'day').getValue()).format('m/d/Y');
									}
									if (TCG.isBlank(holidayId)) {
										TCG.showError('You must select a holiday.');
									}
									else if (TCG.isBlank(dateValue)) {
										TCG.showError('You must select a date.');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Adding...',
											params: {calendarId: calendarId, holidayId: holidayId, date: dateValue},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'day').reset();
											}
										});
										loader.load('calendarHolidayToDayLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Days',
				items: [{
					xtype: 'gridpanel',
					name: 'calendarBusinessDayListFind',
					instructions: 'This section contains information about each day for this calendar and any holidays tied to specific dates as well as if the day is a business day or not.',
					getTopToolbarFilters: function() {
						return [
							{fieldLabel: 'Start Date', xtype: 'toolbar-datefield', name: 'startDate'},
							{fieldLabel: 'End Date', xtype: 'toolbar-datefield', name: 'endDate'}
						];
					},
					getLoadParams: function(firstLoad) {
						const calendarId = this.getWindow().getMainFormId();
						//If parent form doesn't have an id; don't load the grid
						if (TCG.isBlank(calendarId)) {
							return false;
						}

						const loadParams = {
							calendarId: calendarId
						};
						const toolbar = this.getTopToolbar();
						const startDate = TCG.getChildByName(toolbar, 'startDate');
						const endDate = TCG.getChildByName(toolbar, 'endDate');
						if (firstLoad) {
							// default start to -30 days and end to + 30 days
							if (TCG.isBlank(startDate.getValue())) {
								startDate.setValue((new Date().add(Date.DAY, -30)).format('m/d/Y'));
								startDate.originalValue = startDate.getValue(); // prevent on change event
							}
							if (TCG.isBlank(endDate.getValue())) {
								endDate.setValue((new Date().add(Date.DAY, 30)).format('m/d/Y'));
								endDate.originalValue = endDate.getValue(); // prevent on change event
							}
						}
						if (TCG.isNotBlank(startDate.getValue())) {
							loadParams.activeOnDateRangeStartDate = (startDate.getValue()).format('m/d/Y');
						}
						if (TCG.isNotBlank(endDate.getValue)) {
							loadParams.activeOnDateRangeEndDate = (endDate.getValue()).format('m/d/Y');
						}
						return loadParams;
					},
					columns: [
						{header: 'Date', dataIndex: 'startDate', width: 50},
						{header: 'Business Day', dataIndex: 'businessDay', type: 'boolean', width: 60, tooltip: 'Business Day for this calendar:  Not a weekend and not a full day holiday.'},
						{header: 'Trading Business Day', dataIndex: 'tradingBusinessDay', type: 'boolean', width: 70, tooltip: 'Trading Business Day for this calendar:  Not a weekend and not a full day trading holiday.'},
						{header: 'Settlement Business Day', dataIndex: 'settlementBusinessDay', type: 'boolean', width: 70, tooltip: 'Settlement Business Day for this calendar:  Not a weekend and not a full day settlement holiday.'},
						{header: 'Holiday', dataIndex: 'holidayDay.holiday.name', width: 100, filter: {type: 'combo', searchFieldName: 'holidayId', url: 'calendarHolidayListFind.json'}},
						{header: 'Full Day Holiday', dataIndex: 'holidayDay.fullDayHoliday', type: 'boolean', width: 60, filter: {searchFieldName: 'fullDayHoliday'}, hidden: true},
						{header: 'Weekend', dataIndex: 'weekday.weekend', type: 'boolean', width: 60, filter: {searchFieldName: 'weekend'}, hidden: true}
					]
				}]
			}
		]
	}]
});
