Clifton.calendar.setup.RollupCalendarWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Calendar (Rollup)',
	iconCls: 'calendar',
	width: 700,
	height: 520,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Calendar',
				items: [{
					xtype: 'formpanel',
					instructions: 'Rollup Calendars take their holidays from underlying child calendars.  Holidays cannot be applied directly to rollup calendars.  If there are multiple holidays from child calendars on the same day then the holiday attributes will be merged (i.e. if it is trading holiday on on child calendar but not the other, the rollup calendar will be marked as a trading holiday).  The actual holiday name will use the name from the calendar with the smallest ID value (as the label of the holiday is not as important as the attributes).',
					url: 'calendar.json',
					items: [
						{fieldLabel: 'Calendar Name', name: 'name'},
						{fieldLabel: 'Calendar Code', name: 'code'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{xtype: 'system-entity-modify-condition-combo'},
						{fieldLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true},
						{name: 'rollupCalendar', xtype: 'hidden', value: 'true'},
						{
							xtype: 'formgrid-scroll',
							title: 'Child Calendars',
							storeRoot: 'childCalendars',
							dtoClass: 'com.clifton.calendar.setup.Calendar',
							collapsible: false,
							height: 180,
							heightResized: true,
							viewConfig: {forceFit: true, emptyText: 'This Rollup Calendar currently has no child calendars', deferEmptyText: false},
							columnsConfig: [
								{header: 'Calendar', dataIndex: 'label', idDataIndex: 'id', editor: {xtype: 'combo', url: 'calendarListFind.json?rollupCalendar=false', displayField: 'label', detailPageClass: 'Clifton.calendar.setup.CalendarWindow'}}
							]
						}
					],
					listeners: {
						afterload: function(form) {
							if (form.getFormValue('systemDefined')) {
								TCG.getChildByName(form, 'name').setDisabled(true);
							}
						}
					}
				}]
			},


			{
				title: 'Holidays',
				items: [{
					xtype: 'calendar-calendarHolidayDaysGrid',
					instructions: 'The following holidays are defined this Rollup Calendar.  Rollup calendar holidays are system generated based on its child calendar holidays, with attributed merged.',
					columnOverrides: [{dataIndex: 'calendar.name', hidden: 'true'}],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('day.startDate', {'after': new Date().add(Date.DAY, -90)});
						}
						return {'calendarId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Days',
				items: [{
					xtype: 'gridpanel',
					name: 'calendarBusinessDayListFind',
					instructions: 'This section contains information about each day for this calendar and any holidays tied to specific dates as well as if the day is a business day or not.',
					getTopToolbarFilters: function() {
						return [
							{fieldLabel: 'Start Date', xtype: 'toolbar-datefield', name: 'startDate'},
							{fieldLabel: 'End Date', xtype: 'toolbar-datefield', name: 'endDate'}
						];
					},
					getLoadParams: function(firstLoad) {
						const calendarId = this.getWindow().getMainFormId();
						//If parent form doesn't have an id; don't load the grid
						if (TCG.isBlank(calendarId)) {
							return false;
						}

						const loadParams = {
							calendarId: calendarId
						};
						const toolbar = this.getTopToolbar();
						const startDate = TCG.getChildByName(toolbar, 'startDate');
						const endDate = TCG.getChildByName(toolbar, 'endDate');
						if (firstLoad) {
							// default start to -30 days and end to + 30 days
							if (TCG.isBlank(startDate.getValue())) {
								startDate.setValue((new Date().add(Date.DAY, -30)).format('m/d/Y'));
								startDate.originalValue = startDate.getValue(); // prevent on change event
							}
							if (TCG.isBlank(endDate.getValue())) {
								endDate.setValue((new Date().add(Date.DAY, 30)).format('m/d/Y'));
								endDate.originalValue = endDate.getValue(); // prevent on change event
							}
						}
						if (TCG.isNotBlank(startDate.getValue())) {
							loadParams.activeOnDateRangeStartDate = (startDate.getValue()).format('m/d/Y');
						}
						if (TCG.isNotBlank(endDate.getValue)) {
							loadParams.activeOnDateRangeEndDate = (endDate.getValue()).format('m/d/Y');
						}
						return loadParams;
					},
					columns: [
						{header: 'Date', dataIndex: 'startDate', width: 50},
						{header: 'Business Day', dataIndex: 'businessDay', type: 'boolean', width: 60, tooltip: 'Business Day for this calendar:  Not a weekend and not a full day holiday.'},
						{header: 'Trading Business Day', dataIndex: 'tradingBusinessDay', type: 'boolean', width: 70, tooltip: 'Trading Business Day for this calendar:  Not a weekend and not a full day trading holiday.'},
						{header: 'Settlement Business Day', dataIndex: 'settlementBusinessDay', type: 'boolean', width: 70, tooltip: 'Settlement Business Day for this calendar:  Not a weekend and not a full day settlement holiday.'},
						{header: 'Holiday', dataIndex: 'holidayDay.holiday.name', width: 100, filter: {type: 'combo', searchFieldName: 'holidayId', url: 'calendarHolidayListFind.json'}},
						{header: 'Full Day Holiday', dataIndex: 'holidayDay.fullDayHoliday', type: 'boolean', width: 60, filter: {searchFieldName: 'fullDayHoliday'}, hidden: true},
						{header: 'Weekend', dataIndex: 'weekday.weekend', type: 'boolean', width: 60, filter: {searchFieldName: 'weekend'}, hidden: true}
					]
				}]
			}
		]
	}]
});
