Clifton.calendar.CalendarSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'calendarSetupWindow',
	iconCls: 'calendar',
	title: 'Calendar Setup',
	width: 1200,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Calendars',
				items: [{
					name: 'calendarListFind',
					xtype: 'gridpanel',
					instructions: 'Calendars define whether a given day is a holiday or a business day. Each country usually has a unique calendar. Moreover, businesses (Stock Exchanges, Banks, etc.) in the same country can use different calendars. For example, the US Bank Holidays would be one calendar.',
					wikiPage: 'IT/Calendar+and+Scheduling',
					importTableName: 'Calendar',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Calendar Name', width: 110, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Code', width: 40, dataIndex: 'code', tooltip: 'An internal code used to identify this calendar for integration with other systems.'},
						{header: 'MIC', width: 20, dataIndex: 'marketIdentifierCode', tooltip: 'A Market Identifier Code (MIC) used to link this calendar to an investment exchange. These codes are used with standard calendars, and are not intended for use with rollup calendars.'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Modify Condition', width: 150, dataIndex: 'entityModifyCondition.name', filter: false, hidden: true},
						{header: 'Default', width: 30, dataIndex: 'defaultSystemCalendar', type: 'boolean'},
						{header: 'Rollup Calendar', width: 40, dataIndex: 'rollupCalendar', type: 'boolean', tooltip: 'A grouping of multiple standard calendars'},
						{header: 'System Defined', width: 40, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								const t = rowItem.get('rollupCalendar');
								return (t) ? 'Rollup Calendar' : 'Standard Calendar';
							},
							items: [
								{text: 'Standard Calendar', iconCls: 'calendar', className: 'Clifton.calendar.setup.StandardCalendarWindow'},
								{text: 'Rollup Calendar', iconCls: 'calendar', className: 'Clifton.calendar.setup.RollupCalendarWindow'}
							]
						}
					}
				}]
			},


			{
				title: 'Calendar Holidays',
				items: [{
					xtype: 'calendar-calendarHolidayDaysGrid',
					instructions: 'The following holidays are defined across the listed Calendars.'
				}]
			},


			{
				title: 'Holidays',
				items: [{
					name: 'calendarHolidayList',
					xtype: 'gridpanel',
					instructions: 'Holidays defined here can be used globally across calendars. Holidays defined for a calendar are those that are considered to be non business days.',
					importTableName: 'CalendarHoliday',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Holiday Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 400, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.calendar.holiday.HolidayWindow'
					}
				}]
			},


			{
				title: 'Years',
				items: [{
					name: 'calendarYearList',
					xtype: 'gridpanel',
					instructions: 'The following years are defined in the system.  To add another year, enter the year number and click Add.  All the days of the year will be generated automatically for you.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Year', width: 100, dataIndex: 'year', type: 'int', doNotFormat: true},
						{header: 'Days In Year', width: 150, dataIndex: 'daysInYear', type: 'int'},
						{header: 'Leap Year', width: 50, dataIndex: 'leapYear', type: 'boolean'}
					],
					editor: {
						deleteEnabled: false,
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new Ext.form.NumberField({
								name: 'yearNumber',
								fieldLabel: 'Year',
								width: 50,
								maxLength: 4, // for validation
								allowDecimals: false,
								minValue: 1900,
								maxValue: 2999
							}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add Calendar Year',
								iconCls: 'add',
								handler: function() {
									const yearNum = TCG.getChildByName(toolBar, 'yearNumber').getValue();
									if (TCG.isBlank(yearNum)) {
										TCG.showError('You must first enter year number to add.');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Adding...',
											params: {year: yearNum},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'yearNumber').reset();
											}
										});
										loader.load('calendarYearForYearSave.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Months',
				items: [{
					name: 'calendarMonthList',
					xtype: 'gridpanel',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Month Number', width: 30, dataIndex: 'number', type: 'int'},
						{header: 'Abbreviation', width: 40, dataIndex: 'shortName'},
						{header: 'Month Name', width: 100, dataIndex: 'name'},
						{header: 'Total Days', width: 40, dataIndex: 'daysInMonth', type: 'int'},
						{header: 'Leap Year Total Days', width: 40, dataIndex: 'leapYearDaysInMonth', type: 'int'}
					]
				}]
			},


			{
				title: 'Weekdays',
				items: [{
					name: 'calendarWeekdayList',
					xtype: 'gridpanel',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Abbreviation', width: 50, dataIndex: 'shortName'},
						{header: 'Weekday Name', width: 100, dataIndex: 'name'}
					]
				}]
			},


			{
				title: 'Time Zones',
				items: [{
					name: 'calendarTimeZoneListFind',
					xtype: 'gridpanel',
					instructions: 'Time zones represents regions of Earth that have different time zones.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Time Zone Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'GMT Offset', width: 50, dataIndex: 'gmtOffsetMinutes', type: 'int'},
						{header: 'DST', width: 50, dataIndex: 'dstOn', type: 'boolean'},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'}
					]
				}]
			}
		]
	}]
});

