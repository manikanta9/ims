package com.clifton.calendar.date;


import com.clifton.calendar.setup.Calendar;

import java.util.Date;


/**
 * The <code>CalendarDateGenerationService</code> a set of functions used to generate a date.  Mostly used
 * for exports and report generation where we need to get a specific date to generate the content for.
 *
 * @author mwacker
 */
public interface CalendarDateGenerationHandler {

	/**
	 * Will convert a short hand date string to a date.  The string must
	 * be in format ${dateGenerationOption}, ${dateGenerationOption}:${dayCount}, ${dateGenerationOption}:${startDate}, ${dateGenerationOption}:${startDate}:${dayCount}
	 * <p/>
	 * For example:
	 * PREVIOUS_BUSINESS_DAY			// previous business day
	 * BUSINESS_DAYS_BACK:2 			// 2 business days back
	 * PREVIOUS_BUSINESS_DAY:11/1/2014	// previous business day from 11/1/14
	 * BUSINESS_DAYS_BACK:11/1/2014:3	// 2 business days back from 11/1/14
	 *
	 * @param dateGenerationString
	 */
	public Date generateDate(String dateGenerationString);


	public Date generateDate(DateGenerationOptions dateGenerationOption);


	public Date generateDate(DateGenerationOptions dateGenerationOption, Date startDate, Calendar calendar);


	public Date generateDate(DateGenerationOptions dateGenerationOption, Integer dayCount, Calendar calendar);


	public Date generateDate(DateGenerationOptions dateGenerationOption, Date startDate);


	public Date generateDate(DateGenerationOptions dateGenerationOption, Integer dayCount);


	public Date generateDate(DateGenerationOptions dateGenerationOption, Date startDate, Integer dayCount);
}
