package com.clifton.calendar.date;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class CalendarDateGenerationHandlerImpl implements CalendarDateGenerationHandler {

	private CalendarBusinessDayService calendarBusinessDayService;


	@Override
	public Date generateDate(String dateGenerationString) {
		if (StringUtils.isEmpty(dateGenerationString)) {
			return null;
		}
		String[] dateGenerationStringItems = dateGenerationString.split(":");
		//if the string passed in is already a valid date, then just return that
		if (DateUtils.isValidDate(dateGenerationStringItems[0])) {
			return DateUtils.toDate(dateGenerationStringItems[0]);
		}
		else {
			DateGenerationOptions dateGenerationOption = DateGenerationOptions.valueOf(dateGenerationStringItems[0]);
			String startDateString = null;
			String dayCountString = null;
			if (dateGenerationStringItems.length > 1) {
				if (DateUtils.isValidDate(dateGenerationStringItems[1])) {
					startDateString = dateGenerationStringItems[1];
				}
				else {
					dayCountString = dateGenerationStringItems[1];
				}
			}
			if (dateGenerationStringItems.length > 2) {
				if (DateUtils.isValidDate(dateGenerationStringItems[2])) {
					startDateString = dateGenerationStringItems[2];
				}
				else {
					dayCountString = dateGenerationStringItems[2];
				}
			}

			Date startDate = !StringUtils.isEmpty(startDateString) ? DateUtils.toDate(startDateString) : DateUtils.clearTime(new Date());
			Integer dayCount = dayCountString != null ? new Integer(dayCountString) : null;
			return generateDate(dateGenerationOption, startDate, dayCount, null);
		}
	}


	@Override
	public Date generateDate(DateGenerationOptions dateGenerationOption) {
		return generateDate(dateGenerationOption, DateUtils.clearTime(new Date()), null, null);
	}


	@Override
	public Date generateDate(DateGenerationOptions dateGenerationOption, Date startDate) {
		return generateDate(dateGenerationOption, startDate, null, null);
	}


	@Override
	public Date generateDate(DateGenerationOptions dateGenerationOption, Integer dayCount) {
		return generateDate(dateGenerationOption, DateUtils.clearTime(new Date()), dayCount, null);
	}


	@Override
	public Date generateDate(DateGenerationOptions dateGenerationOption, Integer dayCount, Calendar calendar) {
		return generateDate(dateGenerationOption, DateUtils.clearTime(new Date()), dayCount, calendar);
	}


	@Override
	public Date generateDate(DateGenerationOptions dateGenerationOption, Date startDate, Calendar calendar) {
		return generateDate(dateGenerationOption, startDate, null, calendar);
	}


	@Override
	public Date generateDate(DateGenerationOptions dateGenerationOption, Date startDate, Integer dayCount) {
		return generateDate(dateGenerationOption, startDate, dayCount, null);
	}


	public Date generateDate(DateGenerationOptions dateGenerationOption, Date startDate, Integer dayCount, Calendar calendar) {
		if (dateGenerationOption.isDayCountRequired()) {
			ValidationUtils.assertNotNull(dayCount, "Date generation for option [" + dateGenerationOption + "] requires a number of days.");
			// get the absolute value of the day count because the generation option will determine the direction.
			dayCount = Math.abs(dayCount);
		}

		Date result = startDate == null ? DateUtils.clearTime(new Date()) : startDate;
		switch (dateGenerationOption) {
			case TODAY:
				result = DateUtils.clearTime(new Date());
				break;
			case PREVIOUS_WEEK_DAY:
				result = DateUtils.getPreviousWeekday(result);
				break;
			case FIRST_DAY_OF_PREVIOUS_MONTH:
				result = DateUtils.getFirstDayOfMonth(DateUtils.addMonths(result, -1));
				break;
			case LAST_DAY_OF_PREVIOUS_MONTH:
				result = DateUtils.getLastDayOfMonth(DateUtils.addMonths(result, -1));
				break;
			case LAST_DAY_OF_PREVIOUS_QUARTER:
				result = DateUtils.addDays(DateUtils.getFirstDayOfQuarter(result), -1);
				break;
			case DAYS_BACK:
				result = DateUtils.addDays(result, -dayCount);
				break;
			case DAYS_FORWARD:
				result = DateUtils.addDays(result, dayCount);
				break;
			case WEEKDAYS_BACK:
				result = DateUtils.addWeekDays(result, -dayCount);
				break;
			case WEEKDAYS_FORWARD:
				result = DateUtils.addWeekDays(result, dayCount);
				break;
			case BUSINESS_DAYS_BACK:
				result = getBusinessDate(result, -dayCount, calendar);
				break;
			case BUSINESS_DAYS_FORWARD:
				result = getBusinessDate(result, dayCount, calendar);
				break;
			case LAST_BUSINESS_DAY_OF_PREVIOUS_MONTH:
				result = getBusinessDate(DateUtils.getFirstDayOfMonth(result), -1, calendar);
				break;
			case LAST_BUSINESS_DAY_OF_CURRENT_MONTH:
				result = getBusinessDate(DateUtils.getFirstDayOfMonth(DateUtils.addMonths(result, 1)), -1, calendar);
				break;
			case LAST_BUSINESS_DAY_OF_PREVIOUS_QUARTER:
				result = getBusinessDate(DateUtils.getFirstDayOfQuarter(result), -1, calendar);
				break;
			case PREVIOUS_BUSINESS_DAY:
				result = getBusinessDate(result, -1, calendar);
				break;
			case FIRST_DAY_OF_YEAR:
				result = DateUtils.getFirstDayOfYear(new Date());
				break;
		}

		return result;
	}


	private Date getBusinessDate(Date date, int days, Calendar calendar) {
		return calendar == null ? getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(date), days) : getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, calendar.getId()), days);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
