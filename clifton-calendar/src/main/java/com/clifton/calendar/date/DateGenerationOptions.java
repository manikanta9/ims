package com.clifton.calendar.date;


/**
 * The <code>DateGenerationOptions</code> defines a list of options that define how to look up
 * a relative date.
 *
 * @author mwacker
 */
public enum DateGenerationOptions {
	/**
	 * Business Days Back
	 */
	BUSINESS_DAYS_BACK("Business Days Back", true, true),
	/**
	 * Business Days Forward.  Requires a number of days to calculate.
	 */
	BUSINESS_DAYS_FORWARD("Business Days Forward", true, true),
	/**
	 * Days Back.  Requires a number of days to calculate.
	 */
	DAYS_BACK("Days Back", false, true),
	DAYS_FORWARD("Days Forward", false, true),

	FIRST_DAY_OF_PREVIOUS_MONTH("First Day of Previous Month", false, false),
	LAST_DAY_OF_PREVIOUS_MONTH("Last Day of Previous Month", false, false),
	LAST_DAY_OF_PREVIOUS_QUARTER("Last Business Day of the Previous Quarter", false, false),
	LAST_BUSINESS_DAY_OF_PREVIOUS_MONTH("Last Business Day of the Previous Month", true, false),
	LAST_BUSINESS_DAY_OF_CURRENT_MONTH("Last Business Day of the Current Month", true, false),
	LAST_BUSINESS_DAY_OF_PREVIOUS_QUARTER("Last Business Day of the Previous Quarter", true, false),
	PREVIOUS_BUSINESS_DAY("Previous Business Day", true, false),
	PREVIOUS_WEEK_DAY("Previous Week Day", false, false),
	TODAY("Today", false, false),
	/**
	 * Weekdays Back.  Goes back x number of days and if that day is on the weekend then it goes to the previous friday.
	 */
	WEEKDAYS_BACK("Weekdays Back", false, true),
	WEEKDAYS_FORWARD("Weekdays Forward", false, true),

	/**
	 * For getting the first day of this year
	 */
	FIRST_DAY_OF_YEAR("First Day of Current Year", false, false);

	/**
	 * Indicates that this is for business days only.
	 */
	private final boolean businessDaysOnly;
	/**
	 * This option requires a number of days to calculate the date.
	 */
	private final boolean dayCountRequired;


	DateGenerationOptions(String label, boolean businessDaysOnly, boolean dayCountRequired) {
		this.label = label;
		this.businessDaysOnly = businessDaysOnly;
		this.dayCountRequired = dayCountRequired;
	}


	private final String label;


	public String getLabel() {
		return this.label;
	}


	public boolean isBusinessDaysOnly() {
		return this.businessDaysOnly;
	}


	public boolean isDayCountRequired() {
		return this.dayCountRequired;
	}
}
