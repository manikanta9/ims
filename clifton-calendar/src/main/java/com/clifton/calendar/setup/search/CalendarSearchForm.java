package com.clifton.calendar.setup.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class CalendarSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String code;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String marketIdentifierCode;

	@SearchField(searchField="marketIdentifierCode", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean marketIdentifierCodeNotNull;

	@SearchField
	private Boolean defaultSystemCalendar;

	@SearchField
	private Boolean rollupCalendar;

	@SearchField(searchField = "parentCalendarList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short parentCalendarId;

	@SearchField(searchField = "childCalendarList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short childCalendarId;

	@SearchField
	private Boolean systemDefined;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getDefaultSystemCalendar() {
		return this.defaultSystemCalendar;
	}


	public void setDefaultSystemCalendar(Boolean defaultSystemCalendar) {
		this.defaultSystemCalendar = defaultSystemCalendar;
	}


	public Boolean getRollupCalendar() {
		return this.rollupCalendar;
	}


	public void setRollupCalendar(Boolean rollupCalendar) {
		this.rollupCalendar = rollupCalendar;
	}


	public Short getParentCalendarId() {
		return this.parentCalendarId;
	}


	public void setParentCalendarId(Short parentCalendarId) {
		this.parentCalendarId = parentCalendarId;
	}


	public Short getChildCalendarId() {
		return this.childCalendarId;
	}


	public void setChildCalendarId(Short childCalendarId) {
		this.childCalendarId = childCalendarId;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public String getCode() {
		return this.code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMarketIdentifierCode() {
		return this.marketIdentifierCode;
	}


	public void setMarketIdentifierCode(String marketIdentifierCode) {
		this.marketIdentifierCode = marketIdentifierCode;
	}


	public Boolean getMarketIdentifierCodeNotNull() {
		return this.marketIdentifierCodeNotNull;
	}


	public void setMarketIdentifierCodeNotNull(Boolean marketIdentifierCodeNotNull) {
		this.marketIdentifierCodeNotNull = marketIdentifierCodeNotNull;
	}
}
