package com.clifton.calendar.setup;


import com.clifton.calendar.api.Day;
import com.clifton.core.beans.BaseEntityWithNaturalKey;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>CalendarDay</code> class defines a specific date
 * <p/>
 * The ID value is equal to the day number where January 1, 1900 would be
 * the first day and have an ID of 1.
 *
 * @author manderson
 */
public class CalendarDay extends BaseEntityWithNaturalKey<Integer> implements LabeledObject {

	private CalendarYear year;
	private CalendarMonth month;
	private CalendarWeekday weekday;
	private int dayNumberInMonth;
	private int dayNumberInYear;
	private Date startDate;
	private Date endDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Day toDay() {
		Day day = new Day();
		day.setId(getId());
		day.setYear(getYear().getYear());
		day.setMonth(getMonth().getNumber());
		// Have to do some processing to get the weekday number - first convert to localDateTime using ID
		Date date = DateUtils.addDays(DateUtils.toDate("01/01/1900"), getId());
		DateUtils.asLocalDateTime(date);
		int dayOfWeek = DateUtils.getDayOfWeek(date);
		day.setWeekday(dayOfWeek);
		day.setDayNumberInMonth(getDayNumberInMonth());
		day.setDayNumberInYear(getDayNumberInYear());

		return day;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getWeekday().getShortName() + " " + getMonth().getShortName() + " " + getDayNumberInMonth() + ", " + getYear().getYear();
	}


	public CalendarYear getYear() {
		return this.year;
	}


	public void setYear(CalendarYear year) {
		this.year = year;
	}


	public CalendarMonth getMonth() {
		return this.month;
	}


	public void setMonth(CalendarMonth month) {
		this.month = month;
	}


	public CalendarWeekday getWeekday() {
		return this.weekday;
	}


	public void setWeekday(CalendarWeekday weekday) {
		this.weekday = weekday;
	}


	public int getDayNumberInMonth() {
		return this.dayNumberInMonth;
	}


	public void setDayNumberInMonth(int dayNumberInMonth) {
		this.dayNumberInMonth = dayNumberInMonth;
	}


	public int getDayNumberInYear() {
		return this.dayNumberInYear;
	}


	public void setDayNumberInYear(int dayNumberInYear) {
		this.dayNumberInYear = dayNumberInYear;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
