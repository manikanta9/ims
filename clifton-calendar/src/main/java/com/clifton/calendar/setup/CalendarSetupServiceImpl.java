package com.clifton.calendar.setup;


import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.holiday.search.CalendarHolidayDaySearchForm;
import com.clifton.calendar.setup.search.CalendarRollupSearchForm;
import com.clifton.calendar.setup.search.CalendarSearchForm;
import com.clifton.calendar.setup.search.CalendarTimeZoneSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>CalendarSetupServiceImpl</code> provides implementation for the {@link CalendarSetupService} interface
 *
 * @author manderson
 */
@Service
public class CalendarSetupServiceImpl implements CalendarSetupService {

	public static final String CACHE_DEFAULT_CALENDAR = "CALENDAR_DEFAULT";

	private AdvancedUpdatableDAO<Calendar, Criteria> calendarDAO;
	private UpdatableDAO<CalendarYear> calendarYearDAO;
	private ReadOnlyDAO<CalendarMonth> calendarMonthDAO;
	private ReadOnlyDAO<CalendarWeekday> calendarWeekdayDAO;
	private AdvancedUpdatableDAO<CalendarDay, Criteria> calendarDayDAO;
	private AdvancedReadOnlyDAO<CalendarTimeZone, Criteria> calendarTimeZoneDAO;
	private AdvancedUpdatableDAO<CalendarRollup, Criteria> calendarRollupDAO;

	private DaoNamedEntityCache<Calendar> calendarCache;

	private CalendarHolidayService calendarHolidayService;
	private CacheHandler<Object, Calendar> cacheHandler;


	////////////////////////////////////////////////////////////////////////////
	////////              Calendar Business Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Calendar getCalendar(short id) {
		Calendar calendar = getCalendarDAO().findByPrimaryKey(id);
		populateChildCalendars(calendar);
		return calendar;
	}


	@Override
	public Calendar getCalendarByName(String name) {
		return getCalendarCache().getBeanForKeyValue(getCalendarDAO(), name);
	}


	@Override
	public Calendar getDefaultCalendar() {
		Calendar result = getCalendarDefaultFromCache();
		if (result == null) {
			result = getCalendarDAO().findOneByField("defaultSystemCalendar", true);
			ValidationUtils.assertNotNull(result, "Cannot find default calendar. One calendar must be marked as default.");
			setCalendarDefaultInCache(result);
		}
		return result;
	}


	@Override
	public List<Calendar> getCalendarList(CalendarSearchForm searchForm) {
		return getCalendarDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public Calendar saveCalendar(Calendar bean) {
		// validate uniqueness of calendar code
		if (!StringUtils.isEmpty(bean.getCode())) {
			CalendarSearchForm searchForm = new CalendarSearchForm();
			searchForm.setCode(bean.getCode());
			List<Calendar> dupeList = getCalendarList(searchForm);
			Calendar dupe = CollectionUtils.getFirstElement(dupeList);
			if (dupe != null) {
				if (!dupe.getId().equals(bean.getId())) {
					throw new FieldValidationException("This calendar code is already used by: " + dupe.getLabel(), "code");
				}
			}
		}

		Calendar result = getCalendarDAO().save(bean);

		if (bean.isRollupCalendar()) {
			boolean updateHolidays = false;
			// new rollup calendar
			if (bean.isNewBean()) {
				List<CalendarRollup> newRollupList = populateRollupList(bean, bean.getChildCalendars(), true);
				saveCalendarRollupList(newRollupList);
				updateHolidays = true;
			}
			// updated rollup calendar
			else {
				CalendarSearchForm searchForm = new CalendarSearchForm();
				searchForm.setParentCalendarId(bean.getId());
				List<Calendar> originalChildCalendars = getCalendarList(searchForm);

				// a list of child calendars on the bean, that are not already on the parent
				List<Calendar> newChildCalendars = CoreCollectionUtils.removeAll(bean.getChildCalendars(), originalChildCalendars);

				if (!CollectionUtils.isEmpty(newChildCalendars)) {
					// save new rollups, process new holidays
					List<CalendarRollup> newRollupList = populateRollupList(bean, newChildCalendars, true);
					saveCalendarRollupList(newRollupList);
					updateHolidays = true;
				}

				// a list of child calendars on the parent, that are no longer on the bean
				List<Calendar> deletedChildCalendars = CoreCollectionUtils.removeAll(originalChildCalendars, bean.getChildCalendars());

				if (!CollectionUtils.isEmpty(deletedChildCalendars)) {
					// delete deleted rollups, process deleted holidays
					List<CalendarRollup> deletedRollupList = populateRollupList(bean, deletedChildCalendars, false);
					deleteCalendarRollupList(deletedRollupList);
					updateHolidays = true;
				}
			}
			if (updateHolidays) {
				processHolidaysForChildCalendars(result, bean.getChildCalendars());
			}
		}
		return result;
	}


	@Override
	@Transactional
	public void deleteCalendar(short id) {
		Calendar calendar = getCalendar(id);
		if (calendar.isRollupCalendar()) {
			// delete many to many relationships to child
			CalendarRollupSearchForm searchForm = new CalendarRollupSearchForm();
			searchForm.setParentCalendarId(id);
			List<CalendarRollup> childRollups = getCalendarRollupList(searchForm);

			if (!CollectionUtils.isEmpty(childRollups)) {
				deleteCalendarRollupList(childRollups);
			}
			getCalendarHolidayService().deleteCalendarHolidayDayList(getCalendarHolidayService().getCalendarHolidayDayListByCalendar(id));
		}
		getCalendarDAO().delete(id);
	}


	private void populateChildCalendars(Calendar parentCalendar) {
		if (parentCalendar != null && parentCalendar.isRollupCalendar()) {

			CalendarSearchForm searchForm = new CalendarSearchForm();
			searchForm.setParentCalendarId(parentCalendar.getId());

			List<Calendar> childCalendars = getCalendarList(searchForm);
			parentCalendar.setChildCalendars(childCalendars);
		}
	}


	/**
	 * Helper method that will populate a list of CalendarRollups for a given parent and
	 * list of child calendars.  If createNewRollups is true, new rollup beans will be created,
	 * else existing rollups will be retrieved from the DB.
	 *
	 * @param parent
	 * @param childCalendars
	 * @param createNewRollups
	 */
	private List<CalendarRollup> populateRollupList(Calendar parent, List<Calendar> childCalendars, boolean createNewRollups) {
		List<CalendarRollup> result = null;

		if (parent != null && !CollectionUtils.isEmpty(childCalendars)) {
			result = new ArrayList<>();

			if (createNewRollups) {
				for (Calendar child : CollectionUtils.getIterable(childCalendars)) {
					CalendarRollup rollup = new CalendarRollup();
					rollup.setReferenceOne(parent);
					rollup.setReferenceTwo(child);

					result.add(rollup);
				}
			}
			else {
				// get existing rollups
				for (Calendar child : CollectionUtils.getIterable(childCalendars)) {
					CalendarRollup rollup = getCalendarRollup(parent.getId(), child.getId());

					result.add(rollup);
				}
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Calendar Year Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarYear getCalendarYear(short id) {
		return getCalendarYearDAO().findByPrimaryKey(id);
	}


	@Override
	public List<CalendarYear> getCalendarYearList() {
		return getCalendarYearDAO().findAll();
	}


	@Override
	@Transactional
	public CalendarYear saveCalendarYearForYear(short year) {
		if (getCalendarYear(year) != null) {
			throw new ValidationException("Calendars have already been setup for year [" + year + "]");
		}
		if (year < DateUtils.DATE_NATURAL_START_YEAR || year > DateUtils.DATE_NATURAL_MAX_YEAR) {
			throw new ValidationException("Invalid Year [" + year + "].  Valid Year range is between " + DateUtils.DATE_NATURAL_START_YEAR + " and " + DateUtils.DATE_NATURAL_MAX_YEAR + ".");
		}
		CalendarYear calendarYear = new CalendarYear();
		calendarYear.setId(year);
		calendarYear = getCalendarYearDAO().save(calendarYear);

		java.util.Calendar javaCalendar = java.util.Calendar.getInstance();
		javaCalendar.set(year, java.util.Calendar.JANUARY, 1); // Set Java Calendar to first day of Year (Month of 0 is January)

		// Retrieve the id of the last day in the prior year so that determining ids
		// of each day of the current year can be done by adding the day number in year to that id.
		int idOfFirstDayInYear = DateUtils.getDaysSinceNaturalStart(javaCalendar.getTime());

		List<CalendarDay> dayList = new ArrayList<>();
		while (javaCalendar.get(java.util.Calendar.YEAR) == year) {
			dayList.add(generateCalendarDayForDate(javaCalendar, calendarYear, idOfFirstDayInYear));
			javaCalendar.add(java.util.Calendar.DAY_OF_YEAR, 1);
		}
		calendarYear.setDaysInYear(CollectionUtils.getSize(dayList));
		calendarYear.setLeapYear(calendarYear.getDaysInYear() == 366);
		calendarYear = getCalendarYearDAO().save(calendarYear);
		getCalendarDayDAO().saveList(dayList);
		return calendarYear;
	}


	////////////////////////////////////////////////////////////////////////////
	////////           Calendar Month Business Methods              ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarMonth getCalendarMonth(short id) {
		return getCalendarMonthDAO().findByPrimaryKey(id);
	}


	@Override
	public List<CalendarMonth> getCalendarMonthList() {
		return getCalendarMonthDAO().findAll();
	}


	////////////////////////////////////////////////////////////////////////////
	////////          Calendar Weekday Business Methods             ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarWeekday getCalendarWeekday(short id) {
		return getCalendarWeekdayDAO().findByPrimaryKey(id);
	}


	@Override
	public List<CalendarWeekday> getCalendarWeekdayList() {
		return getCalendarWeekdayDAO().findAll();
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Calendar Day Business Methods               ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarDay getCalendarDay(int id) {
		return getCalendarDayDAO().findByPrimaryKey(id);
	}


	@Override
	public CalendarDay getCalendarDayByDate(Date date) {
		return getCalendarDayDAO().findByPrimaryKey(DateUtils.getDaysSinceNaturalStart(date));
	}


	@Override
	public List<CalendarDay> getCalendarDayListByYear(short year) {
		return getCalendarDayDAO().findByField("year.id", year);
	}


	/**
	 * Returns a {@link CalendarDay} object representing the specified date and calendar.
	 *
	 * @param dateInstance
	 * @param year
	 * @param idOfFirstDayInYear
	 */
	private CalendarDay generateCalendarDayForDate(java.util.Calendar dateInstance, CalendarYear year, int idOfFirstDayInYear) {
		DateUtils.clearTime(dateInstance);
		CalendarDay day = new CalendarDay();
		day.setYear(year);
		day.setDayNumberInMonth(dateInstance.get(java.util.Calendar.DAY_OF_MONTH));
		day.setDayNumberInYear(dateInstance.get(java.util.Calendar.DAY_OF_YEAR));
		day.setStartDate(dateInstance.getTime());
		day.setEndDate(DateUtils.getEndOfDay(day.getStartDate()));

		// Set the ID to the day number where ID of 1 would be 01/01/1900
		// To make the method much more efficient the calling class should
		// determine the id of the first day in the year and then just add
		// the day number - 1 to that id value to determine this day's id value.
		day.setId(idOfFirstDayInYear + (day.getDayNumberInYear() - 1));

		// Java Calendar has first month as number 0
		day.setMonth(getCalendarMonth((short) (dateInstance.get(java.util.Calendar.MONTH) + 1)));
		day.setWeekday(getCalendarWeekday((short) dateInstance.get(java.util.Calendar.DAY_OF_WEEK)));
		return day;
	}


	////////////////////////////////////////////////////////////////////////////
	////////         Calendar Time Zones Business Methods           ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarTimeZone getCalendarTimeZone(int id) {
		return getCalendarTimeZoneDAO().findByPrimaryKey(id);
	}


	@Override
	public List<CalendarTimeZone> getCalendarTimeZoneList(CalendarTimeZoneSearchForm searchForm) {
		return getCalendarTimeZoneDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////          Calendar Rollup Business Methods           ///////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public List<CalendarRollup> getCalendarRollupList(CalendarRollupSearchForm searchForm) {
		return getCalendarRollupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public CalendarRollup getCalendarRollup(short parentId, short childId) {
		return getCalendarRollupDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{parentId, childId});
	}


	@Override
	public void saveCalendarRollupList(List<CalendarRollup> beanList) {
		getCalendarRollupDAO().saveList(beanList);
	}


	@Override
	public void deleteCalendarRollupList(List<CalendarRollup> beanList) {
		getCalendarRollupDAO().deleteList(beanList);
	}


	/**
	 * Re-creates the parent calendar holidays based on the list of child calendars.  Called when rollups are updated (and only if there is a change in the calendar selections)
	 */
	private void processHolidaysForChildCalendars(Calendar parentCalendar, List<Calendar> childCalendars) {
		List<CalendarHolidayDay> existingParentHolidays = getCalendarHolidayService().getCalendarHolidayDayListByCalendar(parentCalendar.getId());
		List<CalendarHolidayDay> newParentHolidays = new ArrayList<>();
		Map<CalendarDay, CalendarHolidayDay> parentHolidays = BeanUtils.getBeanMap(existingParentHolidays, CalendarHolidayDay::getDay);


		CalendarHolidayDaySearchForm searchForm = new CalendarHolidayDaySearchForm();
		searchForm.setCalendarIds(BeanUtils.getBeanIdentityArray(childCalendars, Short.class));
		Map<CalendarDay, List<CalendarHolidayDay>> childHolidayMap = BeanUtils.getBeansMap(getCalendarHolidayService().getCalendarHolidayDayList(searchForm), CalendarHolidayDay::getDay);

		for (Map.Entry<CalendarDay, List<CalendarHolidayDay>> childMapEntry : childHolidayMap.entrySet()) {
			CalendarHolidayDay newParentHoliday = populateParentCalendarHolidayDayForChildren(parentHolidays.get(childMapEntry.getKey()), childMapEntry.getValue(), null);
			if (newParentHoliday != null) {
				newParentHoliday.setCalendar(parentCalendar);
				newParentHolidays.add(newParentHoliday);
			}
		}
		getCalendarHolidayService().saveCalendarHolidayDayListForCalendar(parentCalendar, newParentHolidays);
	}


	@Override
	public void processChildHolidayDeletion(Calendar parent, CalendarHolidayDay childHoliday) {
		// get corresponding parent holiday
		CalendarHolidayDay parentHoliday = getCalendarHolidayService().getCalendarHolidayDayByCalendarAndDay(parent.getId(), childHoliday.getDay().getId());

		// get list of child calendar holidays for the day and remove the one we are deleting from the list
		List<CalendarHolidayDay> childHolidayDayList = getChildCalendarHolidayDaysForParentCalendarAndDay(parent.getId(), childHoliday.getDay().getId());
		// Remove the holiday we are deleting from the list
		childHolidayDayList = BeanUtils.filter(childHolidayDayList, calendarHolidayDay -> !calendarHolidayDay.equals(childHoliday));


		CalendarHolidayDay newParentHoliday = populateParentCalendarHolidayDayForChildren(parentHoliday, childHolidayDayList, null);
		saveUpdatedParentHolidayDay(parent, parentHoliday, newParentHoliday);
	}


	@Override
	public void processNewChildHoliday(Calendar parent, CalendarHolidayDay newChildHoliday) {
		// get the parent holiday - if exists
		CalendarHolidayDay parentHoliday = getCalendarHolidayService().getCalendarHolidayDayByCalendarAndDay(parent.getId(), newChildHoliday.getDay().getId());
		CalendarHolidayDay newParentHoliday = populateParentCalendarHolidayDayForChildren(parentHoliday, getChildCalendarHolidayDaysForParentCalendarAndDay(parent.getId(), newChildHoliday.getDay().getId()), newChildHoliday);
		saveUpdatedParentHolidayDay(parent, parentHoliday, newParentHoliday);
	}


	private CalendarHolidayDay saveUpdatedParentHolidayDay(Calendar parentCalendar, CalendarHolidayDay existingParentHoliday, CalendarHolidayDay newParentHoliday) {
		if (newParentHoliday == null) {
			if (existingParentHoliday != null) {
				getCalendarHolidayService().deleteCalendarHolidayDay(existingParentHoliday.getId());
			}
		}
		else {
			newParentHoliday.setCalendar(parentCalendar);
			return getCalendarHolidayService().saveCalendarHolidayDay(newParentHoliday);
		}
		return newParentHoliday;
	}


	private List<CalendarHolidayDay> getChildCalendarHolidayDaysForParentCalendarAndDay(short parentCalendarId, int calendarDayId) {
		// get list of child calendars
		CalendarSearchForm childCalendarSearchForm = new CalendarSearchForm();
		childCalendarSearchForm.setParentCalendarId(parentCalendarId);
		List<Calendar> childCalendars = getCalendarList(childCalendarSearchForm);

		if (CollectionUtils.isEmpty(childCalendars)) {
			return null;
		}

		// Get list of holidays for the children calendars
		CalendarHolidayDaySearchForm holidayDaySearchForm = new CalendarHolidayDaySearchForm();
		if (childCalendars.size() == 1) {
			holidayDaySearchForm.setCalendarId(childCalendars.get(0).getId());
		}
		else {
			holidayDaySearchForm.setCalendarIds(BeanUtils.getBeanIdentityArray(childCalendars, Short.class));
		}
		holidayDaySearchForm.setDayId(calendarDayId);
		return getCalendarHolidayService().getCalendarHolidayDayList(holidayDaySearchForm);
	}


	private CalendarHolidayDay populateParentCalendarHolidayDayForChildren(CalendarHolidayDay parentHoliday, List<CalendarHolidayDay> childCalendarHolidayList, CalendarHolidayDay updatedChildHoliday) {
		if (CollectionUtils.isEmpty(childCalendarHolidayList)) {
			// No child holidays - return null so parent holiday is deleted.
			if (updatedChildHoliday == null) {
				return null;
			}
			// No existing child holidays - use the updated bean only (put into the child list
			else {
				childCalendarHolidayList = CollectionUtils.createList(updatedChildHoliday);
			}
		}

		// If parent holiday is not null - reset all the flags so we rebuild from scratch
		if (parentHoliday != null) {
			parentHoliday = BeanUtils.cloneBean(parentHoliday, false, true);
			parentHoliday.setTradeHoliday(false);
			parentHoliday.setSettlementHoliday(false);
			parentHoliday.setFullDayHoliday(false);
			parentHoliday.setHoliday(null);
		}
		// If more than one child, order by calendar id - we use the holiday name for the first one
		// This would likely be rare and which holiday name is used doesn't affect the business day logic so for simplicity we just use the first one
		if (childCalendarHolidayList.size() > 1) {
			childCalendarHolidayList = BeanUtils.sortWithFunction(childCalendarHolidayList, calendarHolidayDay -> calendarHolidayDay.getCalendar().getId(), true);
		}

		// Otherwise build the parent fields based on the children
		for (CalendarHolidayDay calendarHolidayDay : childCalendarHolidayList) {
			CalendarHolidayDay child = calendarHolidayDay;
			if (calendarHolidayDay.equals(updatedChildHoliday)) {
				child = updatedChildHoliday;
			}

			if (parentHoliday == null) {
				parentHoliday = BeanUtils.cloneBean(child, false, false);
			}
			else {
				if (parentHoliday.getHoliday() == null) {
					parentHoliday.setHoliday(child.getHoliday());
				}
				// Make sure we use the "max" value
				if (child.isTradeHoliday()) {
					parentHoliday.setTradeHoliday(true);
				}
				if (child.isSettlementHoliday()) {
					parentHoliday.setSettlementHoliday(true);
				}
				if (child.isFullDayHoliday()) {
					parentHoliday.setFullDayHoliday(true);
				}
			}
		}
		return parentHoliday;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////                  Cache Helper Methods                   //////////
	////////////////////////////////////////////////////////////////////////////


	private Calendar getCalendarDefaultFromCache() {
		return getCacheHandler().get(CACHE_DEFAULT_CALENDAR, true);
	}


	private void setCalendarDefaultInCache(Calendar calendar) {
		getCacheHandler().put(CACHE_DEFAULT_CALENDAR, true, calendar);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public UpdatableDAO<CalendarYear> getCalendarYearDAO() {
		return this.calendarYearDAO;
	}


	public void setCalendarYearDAO(UpdatableDAO<CalendarYear> calendarYearDAO) {
		this.calendarYearDAO = calendarYearDAO;
	}


	public ReadOnlyDAO<CalendarMonth> getCalendarMonthDAO() {
		return this.calendarMonthDAO;
	}


	public void setCalendarMonthDAO(ReadOnlyDAO<CalendarMonth> calendarMonthDAO) {
		this.calendarMonthDAO = calendarMonthDAO;
	}


	public ReadOnlyDAO<CalendarWeekday> getCalendarWeekdayDAO() {
		return this.calendarWeekdayDAO;
	}


	public void setCalendarWeekdayDAO(ReadOnlyDAO<CalendarWeekday> calendarWeekdayDAO) {
		this.calendarWeekdayDAO = calendarWeekdayDAO;
	}


	public AdvancedUpdatableDAO<CalendarDay, Criteria> getCalendarDayDAO() {
		return this.calendarDayDAO;
	}


	public void setCalendarDayDAO(AdvancedUpdatableDAO<CalendarDay, Criteria> calendarDayDAO) {
		this.calendarDayDAO = calendarDayDAO;
	}


	public AdvancedUpdatableDAO<Calendar, Criteria> getCalendarDAO() {
		return this.calendarDAO;
	}


	public void setCalendarDAO(AdvancedUpdatableDAO<Calendar, Criteria> calendarDAO) {
		this.calendarDAO = calendarDAO;
	}


	public AdvancedReadOnlyDAO<CalendarTimeZone, Criteria> getCalendarTimeZoneDAO() {
		return this.calendarTimeZoneDAO;
	}


	public void setCalendarTimeZoneDAO(AdvancedReadOnlyDAO<CalendarTimeZone, Criteria> calendarTimeZoneDAO) {
		this.calendarTimeZoneDAO = calendarTimeZoneDAO;
	}


	public AdvancedUpdatableDAO<CalendarRollup, Criteria> getCalendarRollupDAO() {
		return this.calendarRollupDAO;
	}


	public void setCalendarRollupDAO(AdvancedUpdatableDAO<CalendarRollup, Criteria> calendarRollupDAO) {
		this.calendarRollupDAO = calendarRollupDAO;
	}


	public DaoNamedEntityCache<Calendar> getCalendarCache() {
		return this.calendarCache;
	}


	public void setCalendarCache(DaoNamedEntityCache<Calendar> calendarCache) {
		this.calendarCache = calendarCache;
	}


	public CalendarHolidayService getCalendarHolidayService() {
		return this.calendarHolidayService;
	}


	public void setCalendarHolidayService(CalendarHolidayService calendarHolidayService) {
		this.calendarHolidayService = calendarHolidayService;
	}


	public CacheHandler<Object, Calendar> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Object, Calendar> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
