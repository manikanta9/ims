package com.clifton.calendar.setup;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>CalendarTimeZone</code> class represents regions of Earth that have different time zones.
 *
 * @author vgomelsky
 */
public class CalendarTimeZone extends NamedEntity<Integer> {

	/**
	 * This time zone offset in minutes from GMT (Greenwich Mean Time).
	 */
	private int gmtOffsetMinutes;
	/**
	 * Specifies whether DST (Daylight Saving Time) is used in that time zone.
	 */
	private boolean dstOn;
	private boolean active;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(64);
		result.append(getName());
		result.append(" (");
		result.append(getDescription());
		result.append(") GMT");
		result.append((getGmtOffsetMinutes() < 0) ? " - " : " + ");
		if (getGmtOffsetMinutes() % 60 == 0) {
			result.append(Math.abs(getGmtOffsetMinutes() / 60));
		}
		else {
			result.append(Math.abs((double) getGmtOffsetMinutes() / 60));
		}
		result.append(" hours");
		if (isDstOn()) {
			result.append(" DST");
		}
		return result.toString();
	}


	public int getGmtOffsetMinutes() {
		return this.gmtOffsetMinutes;
	}


	public void setGmtOffsetMinutes(int gmtOffsetMinutes) {
		this.gmtOffsetMinutes = gmtOffsetMinutes;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public boolean isDstOn() {
		return this.dstOn;
	}


	public void setDstOn(boolean dstOn) {
		this.dstOn = dstOn;
	}
}
