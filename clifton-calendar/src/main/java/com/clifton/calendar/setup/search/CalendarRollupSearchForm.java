package com.clifton.calendar.setup.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class CalendarRollupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Short parentCalendarId;

	@SearchField(searchField = "referenceTwo.id")
	private Short childCalendarId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getParentCalendarId() {
		return this.parentCalendarId;
	}


	public void setParentCalendarId(Short parentCalendarId) {
		this.parentCalendarId = parentCalendarId;
	}


	public Short getChildCalendarId() {
		return this.childCalendarId;
	}


	public void setChildCalendarId(Short childCalendarId) {
		this.childCalendarId = childCalendarId;
	}
}
