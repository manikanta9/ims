package com.clifton.calendar.setup;


import com.clifton.core.beans.NamedEntityWithNaturalKey;


/**
 * The <code>CalendarWeekday</code> class represents one of the 7 days of the week, i.e. Sun, Mon, Tue, etc.
 * <p/>
 * Where the ID value represents the {@link java.util.Calendar} numerical representation of that weekday.
 * Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6, Saturday = 7
 *
 * @author manderson
 */
public class CalendarWeekday extends NamedEntityWithNaturalKey<Short> {

	private String shortName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getShortName() {
		return this.shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
}
