package com.clifton.calendar.setup;


import com.clifton.core.beans.NamedEntityWithNaturalKey;


/**
 * The <code>CalendarMonth</code> defines the months in the Calendar.
 * <p/>
 * The id field is the numerical representation of the the month
 * i.e. January = 1, May = 5.
 * <p/>
 * This ID value is equal to the {@link java.util.Calendar} int
 * representation of the month + 1. Reason behind this is that Java
 * Calendar starts months at 0 and we start at 1.
 *
 * @author manderson
 */
public class CalendarMonth extends NamedEntityWithNaturalKey<Short> {

	private String shortName;
	private int daysInMonth;
	private int leapYearDaysInMonth;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @return the numerical representation of the month
	 * which is the same as the ID value.
	 */
	public short getNumber() {
		return getId();
	}


	public String getShortName() {
		return this.shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public int getDaysInMonth() {
		return this.daysInMonth;
	}


	public void setDaysInMonth(int daysInMonth) {
		this.daysInMonth = daysInMonth;
	}


	public int getLeapYearDaysInMonth() {
		return this.leapYearDaysInMonth;
	}


	public void setLeapYearDaysInMonth(int leapYearDaysInMonth) {
		this.leapYearDaysInMonth = leapYearDaysInMonth;
	}
}
