package com.clifton.calendar.setup;


import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.setup.search.CalendarRollupSearchForm;
import com.clifton.calendar.setup.search.CalendarSearchForm;
import com.clifton.calendar.setup.search.CalendarTimeZoneSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>CalendarSetupService</code> interface defines methods for working with Calendars
 *
 * @author manderson
 */
public interface CalendarSetupService {

	//////////////////////////////////////////////////////////////////////////// 
	////////              Calendar Business Methods                ///////////// 
	//////////////////////////////////////////////////////////////////////////// 


	public Calendar getCalendar(short id);


	public Calendar getCalendarByName(String name);


	@RequestMapping("calendarDefault")
	public Calendar getDefaultCalendar();


	public List<Calendar> getCalendarList(CalendarSearchForm searchForm);


	/**
	 * If the calendar is a rollup calendar, this method will process new/deleted child calendars and
	 * child holidays
	 */
	public Calendar saveCalendar(Calendar bean);


	/**
	 * If the calendar is a rollup calendar, all child calendars will be updated.  If not,
	 * all parent calendars will be updated
	 */
	public void deleteCalendar(short id);


	//////////////////////////////////////////////////////////////////////////// 
	////////            Calendar Year Business Methods             ///////////// 
	//////////////////////////////////////////////////////////////////////////// 


	public CalendarYear getCalendarYear(short id);


	public List<CalendarYear> getCalendarYearList();


	/**
	 * Creates calendar year for given year and also generates all Calendar Days for
	 * that given year.
	 */
	public CalendarYear saveCalendarYearForYear(short year);


	//////////////////////////////////////////////////////////////////////////// 
	////////           Calendar Month Business Methods              //////////// 
	//////////////////////////////////////////////////////////////////////////// 


	public CalendarMonth getCalendarMonth(short id);


	public List<CalendarMonth> getCalendarMonthList();


	//////////////////////////////////////////////////////////////////////////// 
	////////          Calendar Weekday Business Methods             //////////// 
	//////////////////////////////////////////////////////////////////////////// 


	public CalendarWeekday getCalendarWeekday(short id);


	public List<CalendarWeekday> getCalendarWeekdayList();


	//////////////////////////////////////////////////////////////////////////// 
	////////            Calendar Day Business Methods               //////////// 
	//////////////////////////////////////////////////////////////////////////// 


	public CalendarDay getCalendarDay(int id);


	public CalendarDay getCalendarDayByDate(Date date);


	public List<CalendarDay> getCalendarDayListByYear(final short year);


	////////////////////////////////////////////////////////////////////////////
	////////         Calendar Time Zones Business Methods           //////////// 
	//////////////////////////////////////////////////////////////////////////// 


	public CalendarTimeZone getCalendarTimeZone(int id);


	public List<CalendarTimeZone> getCalendarTimeZoneList(CalendarTimeZoneSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////           Calendar Rollup Business Methods             ////////////
	////////////////////////////////////////////////////////////////////////////
	public CalendarRollup getCalendarRollup(short parentId, short childId);


	public List<CalendarRollup> getCalendarRollupList(CalendarRollupSearchForm searchForm);


	public void saveCalendarRollupList(List<CalendarRollup> beanList);


	public void deleteCalendarRollupList(List<CalendarRollup> beanList);


	/**
	 * If this holiday does not already exist on the parent calendar, it will be added
	 * to the parent calendar.  If it does exist, will update the flags to match the maximum value of each option
	 * i.e. Trading Holiday on one child Calendar and Settlement Holiday on another child calendar, then the parent is a holiday on both
	 */
	@DoNotAddRequestMapping
	public void processNewChildHoliday(Calendar parent, CalendarHolidayDay newChildHoliday);


	/**
	 * If no other child calendars have a holiday on the same day, the corresponding parent
	 * holiday will be deleted
	 */
	@DoNotAddRequestMapping
	public void processChildHolidayDeletion(Calendar parent, CalendarHolidayDay childHoliday);
}
