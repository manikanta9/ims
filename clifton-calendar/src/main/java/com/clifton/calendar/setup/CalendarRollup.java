package com.clifton.calendar.setup;


import com.clifton.core.beans.ManyToManyEntity;


/**
 * The <code>CalendarRollup</code> class associates two Calendars with one another
 * as parent and child calendars.  Parent calendars are considered "rollups" of child calendars.
 * Child calendars can be rolled up into different parent calendars.
 * <p/>
 * Rollup calendars cannot have holidays added to them directly.  They can only inherit the holidays
 * from the calendars that are rolled into them.  Also, a rollup calendar cannot be rolled up into other
 * rollup calendars.
 *
 * @author rbrooks
 */
public class CalendarRollup extends ManyToManyEntity<Calendar, Calendar, Integer> {
	// empty
}
