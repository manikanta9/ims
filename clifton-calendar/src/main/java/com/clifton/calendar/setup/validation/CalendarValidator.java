package com.clifton.calendar.setup.validation;


import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class CalendarValidator extends SelfRegisteringDaoValidator<Calendar> {

	private CalendarSetupService calendarSetupService;


	@Override
	public void validate(Calendar bean, DaoEventTypes config) throws ValidationException {

		if (config.isInsert()) {
			if (bean.isSystemDefined()) {
				throw new ValidationException("System Defined Calendars may not be added.");
			}
			validateSave(bean);
		}
		else {
			Calendar originalBean = getOriginalBean(bean);
			if (originalBean != null) {
				if (config.isUpdate()) {
					ValidationUtils.assertEquals(bean.isSystemDefined(), originalBean.isSystemDefined(), "\"System Defined\" field may not be modified.");
					ValidationUtils.assertEquals(bean.isRollupCalendar(), originalBean.isRollupCalendar(), "Rollup Calendar status may not be modified.");

					if (originalBean.isSystemDefined()) {
						ValidationUtils.assertEquals(bean.getName(), originalBean.getName(), "\"Name\" field may not be modified on System Defined Calendars.");
					}

					validateSave(bean);
				}
				if (config.isDelete()) {
					if (originalBean.isSystemDefined()) {
						throw new ValidationException("System Defined Calendars may not be deleted.");
					}
				}
			}
		}
	}


	private void validateSave(Calendar bean) {
		if (bean.isRollupCalendar() && !CollectionUtils.isEmpty(bean.getChildCalendars())) {
			// remove empty calendar entries (id == null)
			List<Calendar> childCalendarList = BeanUtils.filterNotNull(bean.getChildCalendars(), Calendar::getId);

			//remove duplicate calendars
			childCalendarList = CollectionUtils.removeDuplicates(childCalendarList);
			bean.setChildCalendars(childCalendarList);

			// ensure no child calendars are rollup calendars
			List<Calendar> rollupList = BeanUtils.filter(bean.getChildCalendars(), Calendar::isRollupCalendar, true);
			ValidationUtils.assertEmpty(rollupList, "One or more child calendars are Rollup Calendars.  Rollup Calendars may not be rolled up into other calendars.");
		}

		// ensure there is only one default calendar
		if (bean.isDefaultSystemCalendar()) {
			Calendar defaultCalendar = getCalendarSetupService().getDefaultCalendar();
			if ((defaultCalendar != null) && !bean.equals(defaultCalendar)) {
				throw new ValidationException("There is already a default calendar defined in the system.");
			}
		}
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
