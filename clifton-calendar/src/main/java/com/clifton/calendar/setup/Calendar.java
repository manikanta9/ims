package com.clifton.calendar.setup;


import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Calendars define whether a given day is a holiday or a business day.
 * Each country usually has a unique calendar. Moreover, businesses (Stock Exchanges, Banks, etc.)
 * in the same country can use different calendars.
 * <p/>
 * For example, the US Bank Holidays would be one calendar.
 *
 * @author manderson
 */
@CacheByName
public class Calendar extends CalendarNamedEntityModifyConditionAware<Short> {

	/**
	 * Each calendar may have a short code (usually 2 letters) that uniquely identifies it and
	 * can be used to reference this calendar with integrating with other systems.
	 */
	private String code;

	/**
	 * A calendar may optionally have a Market Identifier Code (MIC) to associate the calendar with an exchange.
	 */
	private String marketIdentifierCode;

	/**
	 * Is the default calendar for the system
	 */
	private boolean defaultSystemCalendar;

	/**
	 * Indicates whether this calendar is a rollup of other calendars
	 */
	private boolean rollupCalendar;

	/**
	 * List of child calendars that are rolled up into this calendar, if this calendar is a rollup. Populated synthetically in getter.
	 */
	private List<Calendar> childCalendars;

	/**
	 * Indicates whether this calendar is system defined
	 */
	private boolean systemDefined;

	////////////////////////////////////////////////////////////////////////////
	////////                    Entity Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (isDefaultSystemCalendar()) {
			return getName() + " (DEFAULT)";
		}
		return getName();
	}


	public com.clifton.calendar.api.Calendar toCalendar() {
		com.clifton.calendar.api.Calendar calendar = new com.clifton.calendar.api.Calendar();
		calendar.setId(getId());
		calendar.setName(getName());
		calendar.setCode(getCode());
		calendar.setDefaultSystemCalendar(isDefaultSystemCalendar());
		calendar.setRollupCalendar(isRollupCalendar());
		calendar.setSystemDefined(isSystemDefined());

		if (!CollectionUtils.isEmpty(getChildCalendars())) {
			calendar.setChildCalendars(
					getChildCalendars()
							.stream()
							.map(Calendar::toCalendar)
							.collect(Collectors.toList()));
		}
		return calendar;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isDefaultSystemCalendar() {
		return this.defaultSystemCalendar;
	}


	public void setDefaultSystemCalendar(boolean defaultSystemCalendar) {
		this.defaultSystemCalendar = defaultSystemCalendar;
	}


	public boolean isRollupCalendar() {
		return this.rollupCalendar;
	}


	public void setRollupCalendar(boolean rollupCalendar) {
		this.rollupCalendar = rollupCalendar;
	}


	public List<Calendar> getChildCalendars() {
		return this.childCalendars;
	}


	public void setChildCalendars(List<Calendar> childCalendars) {
		this.childCalendars = childCalendars;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public String getCode() {
		return this.code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMarketIdentifierCode() {
		return this.marketIdentifierCode;
	}


	public void setMarketIdentifierCode(String marketIdentifierCode) {
		this.marketIdentifierCode = marketIdentifierCode;
	}
}
