package com.clifton.calendar.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;

import java.io.Serializable;


/**
 * The <code>CalendarNamedEntityModifyConditionAware</code> abstract class adds {@link SystemEntityModifyConditionAware} functionality
 * to {@link NamedEntity} for calendar related entities.
 *
 * @author NickK
 */
public abstract class CalendarNamedEntityModifyConditionAware<T extends Serializable> extends NamedEntity<T> implements SystemEntityModifyConditionAware {

	/**
	 * For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit this entity
	 */
	private SystemCondition entityModifyCondition;

	////////////////////////////////////////////////////////////////////////////
	////////                    Entity Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
