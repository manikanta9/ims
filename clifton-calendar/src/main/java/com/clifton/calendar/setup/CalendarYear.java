package com.clifton.calendar.setup;


import com.clifton.core.beans.BaseEntityWithNaturalKey;


/**
 * The <code>CalendarYear</code> class represents a Calendar Year, i.e. 2009, 2010, etc.
 * The calendar year number is used as the Calendar ID.
 *
 * @author manderson
 */
public class CalendarYear extends BaseEntityWithNaturalKey<Short> {

	private int daysInYear;
	private boolean leapYear;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Return The Calendar Year # for this entity.
	 * The Calendar Year is the same as the ID field.
	 *
	 * @return the year
	 */
	public int getYear() {
		return getId();
	}


	public int getDaysInYear() {
		return this.daysInYear;
	}


	public void setDaysInYear(int daysInYear) {
		this.daysInYear = daysInYear;
	}


	public boolean isLeapYear() {
		return this.leapYear;
	}


	public void setLeapYear(boolean leapYear) {
		this.leapYear = leapYear;
	}
}
