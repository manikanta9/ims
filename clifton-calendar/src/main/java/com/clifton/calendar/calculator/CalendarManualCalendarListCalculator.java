package com.clifton.calendar.calculator;

import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.calendar.setup.search.CalendarSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * * Base implementation of {@link CalendarListCalculator} that produces a list of calendars based on specific calendars to include and/or exclude.
 * <p>
 * Logic is applied by first getting the list of calendars to include, if defined.  If it is not defined, the list will include all calendars in the system.
 * Any calendars to exclude will then be removed from that list.
 *
 * @author mitchellf
 */
public class CalendarManualCalendarListCalculator implements CalendarListCalculator, ValidationAware {

	/**
	 * This field defines a list of calendars to always include in the result
	 */
	private List<Short> calendarIdsToInclude;

	/**
	 * This field defines a list of calendars to always exclude from the result
	 */
	private List<Short> calendarIdsToExclude;

	private CalendarSetupService calendarSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarCalculationResult calculate(Date balanceDate) {

		Status status = new Status();

		Set<Calendar> calendars = new HashSet<>();

		boolean modified = addCalendarsToInclude(calendars);

		if (!modified) {
			CalendarSearchForm sf = new CalendarSearchForm();
			sf.setRollupCalendar(false);
			calendars = new HashSet<>(getCalendarSetupService().getCalendarList(sf));
		}

		removedCalendarsToExclude(calendars);

		status.setMessage(calendars.isEmpty() ? "Did not find any calendars for the provided criteria." : "Successfully found " + calendars.size() + " calendars.");
		CalendarCalculationResult result = new CalendarCalculationResult();
		result.setCalendarList(CollectionUtils.toArrayList(calendars));
		result.setStatus(status);

		return result;
	}


	@Override
	public void validate() throws ValidationException {
		if (CollectionUtils.isEmpty(getCalendarIdsToExclude()) && CollectionUtils.isEmpty(getCalendarIdsToInclude())) {
			throw new ValidationException("You must provide at least one of [Calendar List to Include, Calendar List to Exclude]");
		}
	}


	protected boolean addCalendarsToInclude(Set<Calendar> calendars) {
		boolean modified = false;
		if (!CollectionUtils.isEmpty(getCalendarIdsToInclude())) {
			for (Short id : getCalendarIdsToInclude()) {
				Calendar calendarToAdd = getCalendarSetupService().getCalendar(id);
				if (calendarToAdd.isRollupCalendar()) {
					modified = calendars.addAll(calendarToAdd.getChildCalendars());
				}
				else {
					modified = calendars.add(calendarToAdd);
				}
			}
		}

		return modified;
	}


	protected boolean removedCalendarsToExclude(Collection<Calendar> calendars) {
		boolean modified = false;
		if (!CollectionUtils.isEmpty(getCalendarIdsToExclude())) {
			for (Short id : getCalendarIdsToExclude()) {
				Calendar calendarToRemove = getCalendarSetupService().getCalendar(id);
				if (calendarToRemove.isRollupCalendar()) {
					modified = calendars.removeAll(calendarToRemove.getChildCalendars());
				}
				else {
					modified = calendars.remove(calendarToRemove);
				}
			}
		}

		return modified;
	}
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<Short> getCalendarIdsToInclude() {
		return this.calendarIdsToInclude;
	}


	public void setCalendarIdsToInclude(List<Short> calendarIdsToInclude) {
		this.calendarIdsToInclude = calendarIdsToInclude;
	}


	public List<Short> getCalendarIdsToExclude() {
		return this.calendarIdsToExclude;
	}


	public void setCalendarIdsToExclude(List<Short> calendarIdsToExclude) {
		this.calendarIdsToExclude = calendarIdsToExclude;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
