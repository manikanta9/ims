package com.clifton.calendar.calculator;

import java.util.Date;


/**
 * A calculator that can be used to generate a set of calendars based the parameters defined in the command.  How calendars are found can vary based on implementation,
 * calculate method requires at minimum a balance date.
 *
 * @author mitchellf
 */
public interface CalendarListCalculator {

	/**
	 * Method to process a balance date and return a result containing any calendars that meet the criteria defined in the implementation.
	 */
	public CalendarCalculationResult calculate(Date balanceDate);
}
