package com.clifton.calendar.calculator;

import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.status.Status;

import java.util.List;


/**
 * The result of a calculation to find a set of calendars.  Contains the calendars that are found, and a status containing any relevant details about the calculator processing.
 *
 * @author mitchellf
 */
public class CalendarCalculationResult {

	List<Calendar> calendarList;
	Status status;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<Calendar> getCalendarList() {
		return this.calendarList;
	}


	public void setCalendarList(List<Calendar> calendarList) {
		this.calendarList = calendarList;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
