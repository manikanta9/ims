package com.clifton.calendar.comparison.field;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.comparison.field.BaseBeanFieldValueComparison;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.date.DateUtilFunctions;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>BeanFieldDateValueComparison</code> is an abstract class
 * used for comparing date bean fields to x days from today (Optionally supports business days), or static date value, or sql based value
 * NOTE: NULL date bean field values will always return false - if needed can use comparison OR with value is NULL or this comparison
 * <p/>
 *
 * @author manderson
 */
public class BeanFieldDateValueComparison<T extends IdentityObject> extends BaseBeanFieldValueComparison<T> {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private CalendarBusinessDayService calendarBusinessDayService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If populated uses date specific logic (use 0 for today)
	 * If null, uses date function option, else super value/valueIsSql logic to get actual value
	 */
	private Short daysFromToday;

	/**
	 * Used to compare bean date value to a function of the date - i.e. last day of month, last date of quarter
	 * Options are defined by system defined list Date Util Functions
	 */
	private DateUtilFunctions dateUtilFunction;

	/**
	 * If true, uses the bean value for the date util function
	 * i.e. if true, is Date = Last Day of Quarter for the Date Value
	 * if false, is Date = Last Day of Today's Quarter
	 */
	private boolean useBeanValueForDateUtilFunction;


	/**
	 * If true, uses our business days (default calendar) for days from today
	 * Otherwise just uses DateUtils.addDays
	 */
	private boolean useBusinessDays;


	/**
	 * Uses system defined SystemList (Date Comparison Options) to determine pre-defined options from the full set of selections
	 */
	private ComparisonConditions comparisonCondition;


	@Override
	public boolean evaluate(T bean, ComparisonContext context) {
		Object beanValue = BeanUtils.getPropertyValue(bean, getBeanFieldName());
		boolean result = false;
		if (beanValue != null) {
			Date dateBeanValue;
			Date actualValue;

			if (beanValue instanceof Date) {
				dateBeanValue = (Date) beanValue;
			}
			else {
				throw new IllegalStateException("Bean Field [" + getBeanFieldName() + "] selected must be a date to perform date comparisons.");
			}
			actualValue = getActualValueAsDate(bean, dateBeanValue);
			result = evaluateCondition(dateBeanValue, actualValue);
			if (context != null) {
				if (result) {
					context.recordTrueMessage("(" + getBeanFieldName() + ": " + DateUtils.fromDateShort(dateBeanValue) + " " + getComparisonCondition().getComparisonExpression() + " " + DateUtils.fromDateShort(actualValue) + ")");
				}
				else {
					context.recordFalseMessage("(" + getBeanFieldName() + ": " + DateUtils.fromDateShort(dateBeanValue) + " is not " + getComparisonCondition().getComparisonExpression() + " " + (actualValue == null ? "NULL" : DateUtils.fromDateShort(actualValue)) + ")");
				}
			}
		}
		else if (context != null) {
			context.recordFalseMessage("(" + getBeanFieldName() + " IS NULL)");
		}
		return result;
	}


	public boolean evaluateCondition(Date d1, Date d2) {
		AssertUtils.assertNotNull(getComparisonCondition(), "Comparison Condition is required to compare dates");

		int result = DateUtils.compare(d1, d2, false);
		// If Before - OK If Less Than, Less Than or Equal, or Not Equals
		if (result < 0 && (ComparisonConditions.LESS_THAN == getComparisonCondition() || ComparisonConditions.LESS_THAN_OR_EQUALS == getComparisonCondition() || ComparisonConditions.NOT_EQUALS == getComparisonCondition())) {
			return true;
		}
		// If Equal - OK If Less Than or Equal, Greater Than or Equal or Equal
		if (result == 0 && (ComparisonConditions.LESS_THAN_OR_EQUALS == getComparisonCondition() || ComparisonConditions.GREATER_THAN_OR_EQUALS == getComparisonCondition() || ComparisonConditions.EQUALS == getComparisonCondition())) {
			return true;
		}
		// Otherwise If After - OK If Greater Than, Greater Than or Equal, or Not Equals
		return (result > 0 && (ComparisonConditions.GREATER_THAN == getComparisonCondition() || ComparisonConditions.GREATER_THAN_OR_EQUALS == getComparisonCondition() || ComparisonConditions.NOT_EQUALS == getComparisonCondition()));
	}


	private Date getActualValueAsDate(T bean, Date beanDateValue) {
		// Users can either enter days from today or use static or sql value look up
		Date actualValue;
		if (isUseBeanValueForDateUtilFunction()) {
			actualValue = beanDateValue;
		}
		else {
			if (getDaysFromToday() == null) {
				String value = super.getActualValue(bean);
				actualValue = DateUtils.toDate(value);
			}
			else {
				if (getDaysFromToday() == 0) {
					actualValue = new Date();
				}
				else if (isUseBusinessDays()) {
					actualValue = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), getDaysFromToday());
				}
				else {
					actualValue = DateUtils.addDays(new Date(), getDaysFromToday());
				}
			}
		}
		if (getDateUtilFunction() != null) {
			actualValue = getDateUtilFunction().evaluateDate(actualValue);
		}
		return actualValue;
	}


	///////////////////////////////////////////////////////////
	//////////      Getter & Setter Methods          //////////
	///////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public boolean isUseBusinessDays() {
		return this.useBusinessDays;
	}


	public void setUseBusinessDays(boolean useBusinessDays) {
		this.useBusinessDays = useBusinessDays;
	}


	public Short getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Short daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public ComparisonConditions getComparisonCondition() {
		return this.comparisonCondition;
	}


	public void setComparisonCondition(ComparisonConditions comparisonCondition) {
		this.comparisonCondition = comparisonCondition;
	}


	public DateUtilFunctions getDateUtilFunction() {
		return this.dateUtilFunction;
	}


	public void setDateUtilFunction(DateUtilFunctions dateUtilFunction) {
		this.dateUtilFunction = dateUtilFunction;
	}


	public boolean isUseBeanValueForDateUtilFunction() {
		return this.useBeanValueForDateUtilFunction;
	}


	public void setUseBeanValueForDateUtilFunction(boolean useBeanValueForDateUtilFunction) {
		this.useBeanValueForDateUtilFunction = useBeanValueForDateUtilFunction;
	}
}
