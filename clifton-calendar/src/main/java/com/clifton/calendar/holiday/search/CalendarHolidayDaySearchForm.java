package com.clifton.calendar.holiday.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class CalendarHolidayDaySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "calendar.id")
	private Short calendarId;

	@SearchField(searchField = "calendar.id")
	private Short[] calendarIds;

	@SearchField(searchFieldPath = "calendar", searchField = "rollupCalendar")
	private Boolean rollupCalendar;

	@SearchField(searchField = "holiday.id")
	private Short holidayId;

	@SearchField(searchField = "day.id")
	private Integer dayId;

	@SearchField(searchField = "name", searchFieldPath = "day.weekday")
	private String weekdayName;

	@SearchField(dateFieldIncludesTime = true, searchField = "startDate", searchFieldPath = "day")
	private Date startDate;

	@SearchField(dateFieldIncludesTime = true, searchField = "endDate", searchFieldPath = "day")
	private Date endDate;

	@SearchField(searchField = "day.weekday.id")
	private Short weekdayId;


	@SearchField(searchField = "day.year.id")
	private Short yearId;

	@SearchField
	private Boolean tradeHoliday;

	@SearchField
	private Boolean settlementHoliday;

	@SearchField
	private Boolean fullDayHoliday;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getCalendarId() {
		return this.calendarId;
	}


	public void setCalendarId(Short calendarId) {
		this.calendarId = calendarId;
	}


	public Short[] getCalendarIds() {
		return this.calendarIds;
	}


	public void setCalendarIds(Short[] calendarIds) {
		this.calendarIds = calendarIds;
	}


	public Short getHolidayId() {
		return this.holidayId;
	}


	public void setHolidayId(Short holidayId) {
		this.holidayId = holidayId;
	}


	public Integer getDayId() {
		return this.dayId;
	}


	public void setDayId(Integer dayId) {
		this.dayId = dayId;
	}


	public String getWeekdayName() {
		return this.weekdayName;
	}


	public void setWeekdayName(String weekdayName) {
		this.weekdayName = weekdayName;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Boolean getRollupCalendar() {
		return this.rollupCalendar;
	}


	public void setRollupCalendar(Boolean rollupCalendar) {
		this.rollupCalendar = rollupCalendar;
	}


	public Short getWeekdayId() {
		return this.weekdayId;
	}


	public void setWeekdayId(Short weekdayId) {
		this.weekdayId = weekdayId;
	}


	public Short getYearId() {
		return this.yearId;
	}


	public void setYearId(Short yearId) {
		this.yearId = yearId;
	}


	public Boolean getTradeHoliday() {
		return this.tradeHoliday;
	}


	public void setTradeHoliday(Boolean tradeHoliday) {
		this.tradeHoliday = tradeHoliday;
	}


	public Boolean getSettlementHoliday() {
		return this.settlementHoliday;
	}


	public void setSettlementHoliday(Boolean settlementHoliday) {
		this.settlementHoliday = settlementHoliday;
	}


	public Boolean getFullDayHoliday() {
		return this.fullDayHoliday;
	}


	public void setFullDayHoliday(Boolean fullDayHoliday) {
		this.fullDayHoliday = fullDayHoliday;
	}
}
