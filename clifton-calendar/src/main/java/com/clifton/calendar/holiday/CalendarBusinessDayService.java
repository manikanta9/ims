package com.clifton.calendar.holiday;


import com.clifton.calendar.holiday.search.CalendarBusinessDaySearchForm;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;


/**
 * The <code>CalendarBusinessDayService</code> defines methods that can be used for determining business days
 * from/between dates for a given calendar.
 * <p>
 * A CalendarBusinessDay is considered to be a {@link java.util.Date} that is
 * not a weekend or a holiday for that calendar.
 * <p>
 * When not supplied all methods use the generic BUSINESS_DAY logic, otherwise can check for Trading or Settlement Business Days specifically
 *
 * @author manderson
 */
public interface CalendarBusinessDayService {

	////////////////////////////////////////////////////////////////////////////
	////////            Calendar Business Day Methods                ///////////
	////////////////////////////////////////////////////////////////////////////


	@RequestMapping("calendarBusinessDay")
	@ResponseBody
	@SecureMethod(dtoClass = CalendarHoliday.class)
	public boolean isBusinessDay(CalendarBusinessDayCommand command);


	@RequestMapping("calendarBusinessDayPartial")
	@ResponseBody
	@SecureMethod(dtoClass = CalendarHoliday.class)
	public boolean isBusinessDayPartial(CalendarBusinessDayCommand command);


	@RequestMapping("calendarBusinessNext")
	@ResponseBody
	@SecureMethod(dtoClass = CalendarHoliday.class)
	public Date getNextBusinessDay(CalendarBusinessDayCommand command);


	@RequestMapping("calendarBusinessDayPrevious")
	@ResponseBody
	@SecureMethod(dtoClass = CalendarHoliday.class)
	public Date getPreviousBusinessDay(CalendarBusinessDayCommand command);


	/**
	 * Returns the count of business days between the given dates
	 * for the given calendar.
	 * <p>
	 * If startDate is before endDate will return a positive number, else
	 * will return a negative number.
	 * <p>
	 * i.e. # of Business Days between 10/27/2009 & 10/28/2009 would be 1,
	 * but the # of Business Days between 10/28/2009 & 10/27/2009 would be -1
	 */
	@RequestMapping("calendarBusinessDaysBetween")
	@ResponseBody
	@SecureMethod(dtoClass = CalendarHoliday.class)
	public Integer getBusinessDaysBetween(Date startDate, Date endDate, CalendarBusinessDayTypes businessDayType, Short... calendarIds);


	/**
	 * Returns the business day number of the month the given date is
	 * for the given calendar.  If date passed is not a business day - get's # for the previous business day from the date.
	 * <p>
	 * example: February 1, 2013 is the 1st business day of the month, February 2, 2013 is a Saturday, so it's still the 1st business day of the month
	 * February 4th is the 2nd Business day of the month
	 */
	@RequestMapping("calendarBusinessDayOfMonth")
	@ResponseBody
	@SecureMethod(dtoClass = CalendarHoliday.class)
	public Integer getBusinessDayOfMonth(CalendarBusinessDayCommand command);


	/**
	 * Returns the business day on or in nearest proximity to the target calendar date. The target calendar date is given by adding <code>days</code> days to the given
	 * <code>date</code>. This method is useful for finding the nearest business day on-or-after a given date.
	 * <p>
	 * If the target date is not a business day and <code>days</code> is positive, then the <i>next</i> business day will be returned. If the target date is not a business day and
	 * <code>days</code> is negative, then the <i>previous</i> business day will be returned.
	 */
	@RequestMapping("calendarNearestBusinessDayFromDateForCalendar")
	@ResponseBody
	@SecureMethod(dtoClass = CalendarHoliday.class)
	public Date getNearestBusinessDayFrom(CalendarBusinessDayCommand command, int days);


	/**
	 * Returns the first business day that is <code>days</code> <b>business days</b> before or after the given date for the given calendars.
	 * <p>
	 * For business days before the given date, use a negative number for <code>days</code> For business days after the given date, use a positive number.
	 * <p>
	 * If days = 0 returns the provided date.
	 * <p>
	 * If no calendars are provided, this method will function as {@link #getBusinessDayFrom(CalendarBusinessDayCommand, int)}
	 */
	@DoNotAddRequestMapping
	public Date getBusinessDayFrom(Date date, int days, CalendarBusinessDayTypes businessDayType, Calendar... calendars);


	/**
	 * Returns the first business day that is <code>days</code> <b>business days</b> before or after today for the default calendar.
	 * <p>
	 * For business days before today, use a negative number for <code>days</code> For business days after today, use a positive number.
	 * <p>
	 * If days = 0 the current day is returned.
	 */
	@RequestMapping("calendarBusinessDayFrom")
	@ResponseBody
	@SecureMethod(dtoClass = CalendarHoliday.class)
	public Date getBusinessDayFrom(CalendarBusinessDayCommand command, int days);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Requires calendarId property
	 * For the specified calendar returns the list of CalendarDays and their holiday / business day information
	 */
	@SecureMethod(dtoClass = CalendarHoliday.class)
	public List<CalendarBusinessDay> getCalendarBusinessDayList(CalendarBusinessDaySearchForm searchForm);
}
