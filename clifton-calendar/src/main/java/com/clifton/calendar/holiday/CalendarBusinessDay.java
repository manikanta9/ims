package com.clifton.calendar.holiday;

import com.clifton.calendar.api.holiday.BusinessDay;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarDay;


/**
 * The <code>CalendarBusinessDay</code> class is a representation of a {@link com.clifton.calendar.setup.CalendarDay} for a specific {@link com.clifton.calendar.setup.Calendar}
 * and represents if it's a holiday or business day
 *
 * @author manderson
 */
public class CalendarBusinessDay extends CalendarDay {

	private Calendar calendar;

	private CalendarHolidayDay holidayDay;

	/**
	 * Not a Weekend and Not a Full Day Holiday (Trading or Settlement)
	 */
	private boolean businessDay;

	/**
	 * Not a Weekend and Not a Full Day Trading Holiday
	 */
	private boolean tradingBusinessDay;

	/**
	 * Not a Weekend and Not a Full Day Settlement Holiday
	 */
	private boolean settlementBusinessDay;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessDay toBusinessDay() {
		BusinessDay dayToReturn = (BusinessDay) toDay();

		dayToReturn.setCalendar(getCalendar().toCalendar());
		dayToReturn.setHolidayDay(getHolidayDay().toHolidayDay());
		dayToReturn.setBusinessDay(isBusinessDay());
		dayToReturn.setTradingBusinessDay(isTradingBusinessDay());
		dayToReturn.setSettlementBusinessDay(isSettlementBusinessDay());

		return dayToReturn;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Calendar getCalendar() {
		return this.calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public CalendarHolidayDay getHolidayDay() {
		return this.holidayDay;
	}


	public void setHolidayDay(CalendarHolidayDay holidayDay) {
		this.holidayDay = holidayDay;
	}


	public boolean isBusinessDay() {
		return this.businessDay;
	}


	public void setBusinessDay(boolean businessDay) {
		this.businessDay = businessDay;
	}


	public boolean isTradingBusinessDay() {
		return this.tradingBusinessDay;
	}


	public void setTradingBusinessDay(boolean tradingBusinessDay) {
		this.tradingBusinessDay = tradingBusinessDay;
	}


	public boolean isSettlementBusinessDay() {
		return this.settlementBusinessDay;
	}


	public void setSettlementBusinessDay(boolean settlementBusinessDay) {
		this.settlementBusinessDay = settlementBusinessDay;
	}
}
