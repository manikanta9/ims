package com.clifton.calendar.holiday.cache;


import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.logging.LogUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;


/**
 * The <code>CalendarHolidayCacheImpl</code> class provides caching and retrieval from cache of int day ids that
 * are holidays for a given calendar.
 * <p>
 * The class also implements {@link DaoEventObserver} and clears the cache whenever update methods to related DTO's happen.
 *
 * @author manderson
 */
@Component
public class CalendarHolidayCacheImpl extends SelfRegisteringSimpleDaoCache<CalendarHolidayDay, Short, Object> implements CalendarHolidayCache {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public Map<Date, CalendarHolidayDayHolder> getCalendarHolidayMap(short calendarId) {
		return (Map<Date, CalendarHolidayDayHolder>) getCacheHandler().get(getCacheName(), calendarId);
	}


	@Override
	public void setCalendarHolidayMap(short calendarId, Map<Date, CalendarHolidayDayHolder> days) {
		getCacheHandler().put(getCacheName(), calendarId, days);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<CalendarHolidayDay> dao, @SuppressWarnings("unused") DaoEventTypes event, CalendarHolidayDay bean,
	                                @SuppressWarnings("unused") Throwable e) {
		getCacheHandler().remove(getCacheName(), bean.getCalendar().getId());
		LogUtils.info(getClass(), "Cleared Calendar Holiday cache for calendar " + bean.getCalendar());
	}
}
