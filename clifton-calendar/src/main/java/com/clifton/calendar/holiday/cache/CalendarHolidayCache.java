package com.clifton.calendar.holiday.cache;

import java.util.Date;
import java.util.Map;


/**
 * * The <code>CalendarHolidayCache</code> class provides caching and retrieval from cache of int day ids that
 * are holidays for a given calendar.
 *
 * @author manderson
 */
public interface CalendarHolidayCache {


	public Map<Date, CalendarHolidayDayHolder> getCalendarHolidayMap(short calendarId);


	public void setCalendarHolidayMap(short calendarId, Map<Date, CalendarHolidayDayHolder> days);
}
