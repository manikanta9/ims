package com.clifton.calendar.holiday;


import com.clifton.calendar.holiday.search.CalendarHolidayDaySearchForm;
import com.clifton.calendar.holiday.search.CalendarHolidaySearchForm;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.Date;
import java.util.List;


/**
 * The <code>CalendarHolidayService</code> interface defines methods for working with Calendars and Holidays.
 *
 * @author manderson
 */
public interface CalendarHolidayService {

	//////////////////////////////////////////////////////////////////////////// 
	////////           Calendar Holiday Business Methods             /////////// 
	//////////////////////////////////////////////////////////////////////////// 


	public CalendarHoliday getCalendarHoliday(short id);


	public CalendarHoliday getCalendarHolidayByName(String name);


	public List<CalendarHoliday> getCalendarHolidayList();


	public List<CalendarHoliday> getCalendarHolidayList(CalendarHolidaySearchForm searchForm);


	public CalendarHoliday saveCalendarHoliday(CalendarHoliday bean);


	public void deleteCalendarHoliday(short id);


	//////////////////////////////////////////////////////////////////////////// 
	////////          Calendar Holiday Day Business Methods            ///////// 
	//////////////////////////////////////////////////////////////////////////// 


	public CalendarHolidayDay getCalendarHolidayDay(int id);


	public CalendarHolidayDay getCalendarHolidayDayByCalendarAndDay(short calendarId, int calendarDayId);


	public List<CalendarHolidayDay> getCalendarHolidayDayListByCalendar(short calendarId);


	public List<CalendarHolidayDay> getCalendarHolidayDayListByHoliday(short holidayId);


	public List<CalendarHolidayDay> getCalendarHolidayDayList(CalendarHolidayDaySearchForm searchForm);


	/**
	 * Creates a new CalendarHolidayDay object by linking a calendar, holiday & day together
	 * The Day is retrieved from the system by looking up by start date.
	 * Sets all holiday flags as true by default (trading, settlement, and full day)
	 */
	@SecureMethod(dtoClass = CalendarHolidayDay.class)
	public void linkCalendarHolidayToDay(short calendarId, short holidayId, Date date);


	@DoNotAddRequestMapping
	public void saveCalendarHolidayDayListForCalendar(Calendar calendar, List<CalendarHolidayDay> holidayDayList);


	public CalendarHolidayDay saveCalendarHolidayDay(CalendarHolidayDay bean);


	public void deleteCalendarHolidayDay(int id);


	public void deleteCalendarHolidayDayList(List<CalendarHolidayDay> beanList);
}
