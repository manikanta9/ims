package com.clifton.calendar.holiday;


import com.clifton.calendar.holiday.search.CalendarHolidayDaySearchForm;
import com.clifton.calendar.holiday.search.CalendarHolidaySearchForm;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarDay;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.date.DateUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>CalendarHolidayServiceImpl</code> provide implementation for the {@link CalendarHolidayService} interface
 *
 * @author manderson
 */
@Service
public class CalendarHolidayServiceImpl implements CalendarHolidayService {

	private AdvancedUpdatableDAO<CalendarHoliday, Criteria> calendarHolidayDAO;
	private AdvancedUpdatableDAO<CalendarHolidayDay, Criteria> calendarHolidayDayDAO;

	private CalendarSetupService calendarSetupService;


	//////////////////////////////////////////////////////////////////////////// 
	////////            Calendar Holiday Business Methods              ///////// 
	//////////////////////////////////////////////////////////////////////////// 


	@Override
	public CalendarHoliday getCalendarHoliday(short id) {
		return getCalendarHolidayDAO().findByPrimaryKey(id);
	}


	@Override
	public CalendarHoliday getCalendarHolidayByName(String name) {
		return getCalendarHolidayDAO().findOneByField("name", name);
	}


	@Override
	public List<CalendarHoliday> getCalendarHolidayList() {
		return getCalendarHolidayDAO().findAll();
	}


	@Override
	public List<CalendarHoliday> getCalendarHolidayList(CalendarHolidaySearchForm searchForm) {
		return getCalendarHolidayDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public CalendarHoliday saveCalendarHoliday(CalendarHoliday bean) {
		return getCalendarHolidayDAO().save(bean);
	}


	@Override
	public void deleteCalendarHoliday(short id) {
		getCalendarHolidayDAO().delete(id);
	}


	//////////////////////////////////////////////////////////////////////////// 
	////////          Calendar Holiday Day Business Methods            ///////// 
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarHolidayDay getCalendarHolidayDay(int id) {
		return getCalendarHolidayDayDAO().findByPrimaryKey(id);
	}


	@Override
	public CalendarHolidayDay getCalendarHolidayDayByCalendarAndDay(short calendarId, int calendarDayId) {
		return getCalendarHolidayDayDAO().findOneByFields(new String[]{"calendar.id", "day.id"}, new Object[]{calendarId, calendarDayId});
	}


	@Override
	public List<CalendarHolidayDay> getCalendarHolidayDayListByCalendar(short calendarId) {
		return getCalendarHolidayDayDAO().findByField("calendar.id", calendarId);
	}


	@Override
	public List<CalendarHolidayDay> getCalendarHolidayDayListByHoliday(short holidayId) {
		return getCalendarHolidayDayDAO().findByField("holiday.id", holidayId);
	}


	@Override
	public List<CalendarHolidayDay> getCalendarHolidayDayList(CalendarHolidayDaySearchForm searchForm) {
		return getCalendarHolidayDayDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public void linkCalendarHolidayToDay(short calendarId, short holidayId, Date date) {
		CalendarHolidayDay holidayDay = new CalendarHolidayDay();
		holidayDay.setCalendar(getCalendarSetupService().getCalendar(calendarId));
		holidayDay.setHoliday(getCalendarHoliday(holidayId));
		CalendarDay day = getCalendarSetupService().getCalendarDayByDate(date);
		if (day == null) {
			// Add dates into the system.
			getCalendarSetupService().saveCalendarYearForYear(new Integer(DateUtils.fromDate(date, "yyyy")).shortValue());
			// Day should now exist.
			day = getCalendarSetupService().getCalendarDayByDate(date);
		}
		holidayDay.setDay(day);

		holidayDay.setTradeHoliday(true);
		holidayDay.setSettlementHoliday(true);
		holidayDay.setFullDayHoliday(true);
		saveCalendarHolidayDay(holidayDay);
	}


	@Override
	public void saveCalendarHolidayDayListForCalendar(Calendar calendar, List<CalendarHolidayDay> holidayDayList) {
		getCalendarHolidayDayDAO().saveList(holidayDayList, getCalendarHolidayDayListByCalendar(calendar.getId()));
	}


	/**
	 * Saves a CalendarHolidayDay.  NOTE:  CalendarHolidayDayObserver updates parent calendars
	 * when child calendar holidays  are inserted or deleted
	 */
	@Override
	public CalendarHolidayDay saveCalendarHolidayDay(CalendarHolidayDay bean) {
		return getCalendarHolidayDayDAO().save(bean);
	}


	/**
	 * Deletes a CalendarHolidayDay.  NOTE:  CalendarHolidayDayObserver updates parent calendars
	 * when child calendar holidays  are inserted or deleted
	 */
	@Override
	public void deleteCalendarHolidayDay(int id) {
		getCalendarHolidayDayDAO().delete(id);
	}


	/**
	 * Deletes a list of CalendarHolidayDays.  NOTE:  CalendarHolidayDayObserver updates parent calendars
	 * when child calendar holidays  are inserted or deleted
	 */
	@Override
	public void deleteCalendarHolidayDayList(List<CalendarHolidayDay> beanList) {
		getCalendarHolidayDayDAO().deleteList(beanList);
	}


	//////////////////////////////////////////////////////////////////////////// 
	////////                 Getter & Setter Methods                   ///////// 
	//////////////////////////////////////////////////////////////////////////// 


	public AdvancedUpdatableDAO<CalendarHoliday, Criteria> getCalendarHolidayDAO() {
		return this.calendarHolidayDAO;
	}


	public void setCalendarHolidayDAO(AdvancedUpdatableDAO<CalendarHoliday, Criteria> calendarHolidayDAO) {
		this.calendarHolidayDAO = calendarHolidayDAO;
	}


	public AdvancedUpdatableDAO<CalendarHolidayDay, Criteria> getCalendarHolidayDayDAO() {
		return this.calendarHolidayDayDAO;
	}


	public void setCalendarHolidayDayDAO(AdvancedUpdatableDAO<CalendarHolidayDay, Criteria> calendarHolidayDayDAO) {
		this.calendarHolidayDayDAO = calendarHolidayDayDAO;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
