package com.clifton.calendar.holiday;


import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.calendar.setup.search.CalendarSearchForm;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class CalendarHolidayDayObserver extends SelfRegisteringDaoObserver<CalendarHolidayDay> {

	CalendarSetupService calendarSetupService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<CalendarHolidayDay> dao, DaoEventTypes event, CalendarHolidayDay bean) {
		if (event.isInsert() || event.isUpdate()) {
			ValidationUtils.assertTrue(bean.isTradeHoliday() || bean.isSettlementHoliday(), "A holiday must be at least a trading or settlement holiday (or both), but cannot be neither.  Please check at least one of the Trade or Settlement holiday options.");
		}
	}


	@Override
	protected void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<CalendarHolidayDay> dao, DaoEventTypes event, CalendarHolidayDay bean, Throwable e) {
		if (e == null) {
			if (!bean.getCalendar().isRollupCalendar()) {

				CalendarSearchForm searchForm = new CalendarSearchForm();
				searchForm.setChildCalendarId(bean.getCalendar().getId());

				List<Calendar> parentCalendars = getCalendarSetupService().getCalendarList(searchForm);

				if (event.isDelete()) {
					for (Calendar calendar : CollectionUtils.getIterable(parentCalendars)) {
						getCalendarSetupService().processChildHolidayDeletion(calendar, bean);
					}
				}

				if (event.isInsert()) {
					for (Calendar calendar : CollectionUtils.getIterable(parentCalendars)) {
						getCalendarSetupService().processNewChildHoliday(calendar, bean);
					}
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
