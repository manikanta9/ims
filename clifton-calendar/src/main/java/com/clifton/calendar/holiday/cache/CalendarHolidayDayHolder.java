package com.clifton.calendar.holiday.cache;

import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>CalendarHolidayDayHolder</code> objects holds key fields of the CalendarHolidayDay object.
 * It is a light weight, immutable version of CalendarHolidayDay object used to minimize memory footprint
 * when caching calendar holidays.
 *
 * @author manderson
 */
public class CalendarHolidayDayHolder extends BaseSimpleEntity<Integer> implements Serializable {

	/**
	 * The calendar the holiday is associated with
	 */
	private final short calendarId;

	/**
	 * The day for the holiday
	 */
	private final int calendarDayId;

	/**
	 * The date - makes it easier to find without having to look up the day by date.
	 * The date will be date only with time cleared.
	 */
	private final Date date;

	private final boolean tradeHoliday;

	private final boolean settlementHoliday;

	private final boolean fullDayHoliday;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarHolidayDayHolder(CalendarHolidayDay calendarHolidayDay) {
		this.setId(calendarHolidayDay.getId());
		this.calendarId = calendarHolidayDay.getCalendar().getId();
		this.calendarDayId = calendarHolidayDay.getDay().getId();
		this.date = DateUtils.clearTime(calendarHolidayDay.getDay().getStartDate());
		this.tradeHoliday = calendarHolidayDay.isTradeHoliday();
		this.settlementHoliday = calendarHolidayDay.isSettlementHoliday();
		this.fullDayHoliday = calendarHolidayDay.isFullDayHoliday();
	}


	public static CalendarHolidayDayHolder of(CalendarHolidayDay calendarHolidayDay) {
		ValidationUtils.assertNotNull(calendarHolidayDay, "Unable to create A Calendar Holiday Day Holder without aa Calendar Holiday Day");
		return new CalendarHolidayDayHolder(calendarHolidayDay);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public short getCalendarId() {
		return this.calendarId;
	}


	public int getCalendarDayId() {
		return this.calendarDayId;
	}


	public Date getDate() {
		return this.date;
	}


	public boolean isTradeHoliday() {
		return this.tradeHoliday;
	}


	public boolean isSettlementHoliday() {
		return this.settlementHoliday;
	}


	public boolean isFullDayHoliday() {
		return this.fullDayHoliday;
	}
}
