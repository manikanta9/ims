package com.clifton.calendar.holiday.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithTimeSearchForm;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * @author manderson
 */
public class CalendarBusinessDaySearchForm extends BaseAuditableEntityDateRangeWithTimeSearchForm {

	// Required Search Field Value - Directly inserted into the SQL (part of the FROM) because of the Joins are built
	private Short calendarId;

	@SearchField(searchFieldPath = "holidayDay", searchField = "holiday.id")
	private Short holidayId;

	@SearchField(searchFieldPath = "holidayDay", searchField = "fullDayHoliday")
	private Boolean fullDayHoliday;


	@SearchField
	private Boolean businessDay;

	@SearchField
	private Boolean tradingBusinessDay;

	@SearchField
	private Boolean settlementBusinessDay;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getCalendarId(), "Calendar ID is required");
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Short getCalendarId() {
		return this.calendarId;
	}


	public void setCalendarId(Short calendarId) {
		this.calendarId = calendarId;
	}


	public Short getHolidayId() {
		return this.holidayId;
	}


	public void setHolidayId(Short holidayId) {
		this.holidayId = holidayId;
	}


	public Boolean getFullDayHoliday() {
		return this.fullDayHoliday;
	}


	public void setFullDayHoliday(Boolean fullDayHoliday) {
		this.fullDayHoliday = fullDayHoliday;
	}


	public Boolean getBusinessDay() {
		return this.businessDay;
	}


	public void setBusinessDay(Boolean businessDay) {
		this.businessDay = businessDay;
	}


	public Boolean getTradingBusinessDay() {
		return this.tradingBusinessDay;
	}


	public void setTradingBusinessDay(Boolean tradingBusinessDay) {
		this.tradingBusinessDay = tradingBusinessDay;
	}


	public Boolean getSettlementBusinessDay() {
		return this.settlementBusinessDay;
	}


	public void setSettlementBusinessDay(Boolean settlementBusinessDay) {
		this.settlementBusinessDay = settlementBusinessDay;
	}
}
