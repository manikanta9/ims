package com.clifton.calendar.holiday;


import com.clifton.calendar.api.holiday.Holiday;
import com.clifton.core.beans.NamedEntity;


/**
 * The <code>CalendarHoliday</code> class defines a holiday
 * i.e. Labor Day, Christmas.
 *
 * @author manderson
 */
public class CalendarHoliday extends NamedEntity<Short> {

	// NOTHING HERE FOR NOW


	public Holiday toHoliday() {
		Holiday holiday = new Holiday();
		holiday.setId(getId());
		holiday.setName(getName());

		return holiday;
	}
}
