package com.clifton.calendar.holiday;

import com.clifton.calendar.holiday.cache.CalendarHolidayDayHolder;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * @author manderson
 */
public enum CalendarBusinessDayTypes {

	BUSINESS_DAY,
	TRADE_BUSINESS_DAY,
	SETTLEMENT_BUSINESS_DAY;


	/**
	 * Returns true for the given date if it's a business day for the type
	 */
	public boolean isBusinessDay(Date date, CalendarHolidayDayHolder holidayDayHolder) {
		return !isNotBusinessDay(date, holidayDayHolder);
	}


	/**
	 * Returns true for the given date if it's a PARTIAL  business day for the type
	 * Which means it's a business day AND a Partial Day Holiday for the selected type
	 */
	public boolean isPartialBusinessDay(Date date, CalendarHolidayDayHolder holidayDayHolder) {
		if (isBusinessDay(date, holidayDayHolder)) {
			if (holidayDayHolder != null && !holidayDayHolder.isFullDayHoliday()) {
				if (this == TRADE_BUSINESS_DAY) {
					return holidayDayHolder.isTradeHoliday();
				}
				if (this == SETTLEMENT_BUSINESS_DAY) {
					return holidayDayHolder.isSettlementHoliday();
				}
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns true for the given date if it's NOT a business day for the type
	 */
	public boolean isNotBusinessDay(Date date, CalendarHolidayDayHolder holidayDayHolder) {
		if (DateUtils.isWeekend(date)) {
			return true;
		}
		if (holidayDayHolder != null && holidayDayHolder.isFullDayHoliday()) {
			if (this == TRADE_BUSINESS_DAY) {
				return holidayDayHolder.isTradeHoliday();
			}
			if (this == SETTLEMENT_BUSINESS_DAY) {
				return holidayDayHolder.isSettlementHoliday();
			}
			return true;
		}
		return false;
	}
}
