package com.clifton.calendar.holiday;


import com.clifton.calendar.holiday.cache.CalendarHolidayCache;
import com.clifton.calendar.holiday.cache.CalendarHolidayDayHolder;
import com.clifton.calendar.holiday.search.CalendarBusinessDaySearchForm;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.SubselectParameterExpression;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.type.ShortType;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>CalendarBusinessDayServiceImpl</code> class implements CalendarBusinessDayService interface based
 * on our definition of business days.
 *
 * @author manderson
 */
@Service
public class CalendarBusinessDayServiceImpl implements CalendarBusinessDayService {

	private CalendarHolidayService calendarHolidayService;
	private CalendarSetupService calendarSetupService;

	private CalendarHolidayCache calendarHolidayCache;


	private AdvancedReadOnlyDAO<CalendarBusinessDay, Criteria> calendarBusinessDayDAO;


	////////////////////////////////////////////////////////////////////////////
	////////            Calendar Business Day Methods                ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isBusinessDay(CalendarBusinessDayCommand command) {
		return isBusinessDayImpl(command.getDate(), command.getBusinessDayType(), command.getCalendarIds());
	}


	@Override
	public boolean isBusinessDayPartial(CalendarBusinessDayCommand command) {
		return isBusinessDayImpl(command.getDate(), command.getBusinessDayType(), true, command.getCalendarIds());
	}


	@Override
	public Date getNextBusinessDay(CalendarBusinessDayCommand command) {
		return getBusinessDayFrom(command, 1);
	}


	@Override
	public Date getPreviousBusinessDay(CalendarBusinessDayCommand command) {
		return getBusinessDayFrom(command, -1);
	}


	@Override
	public Integer getBusinessDaysBetween(Date startDate, Date endDate, CalendarBusinessDayTypes businessDayType, Short... calendarIds) {
		if (startDate == null || endDate == null) {
			throw new IllegalArgumentException("Both dates are required in order to determine the days difference");
		}

		int difference = 0;

		// Same Day - Return 0 difference
		if (DateUtils.isEqualWithoutTime(startDate, endDate)) {
			return 0;
		}
		int increment = (endDate.before(startDate) ? -1 : 1);
		Date currentDate = startDate;
		while (!DateUtils.isEqualWithoutTime(currentDate, endDate)) {
			currentDate = DateUtils.addDays(currentDate, increment);
			if (isBusinessDayImpl(currentDate, businessDayType, calendarIds)) {
				difference++;
			}
		}

		if (endDate.before(startDate)) {
			return difference * -1;
		}

		return difference;
	}


	@Override
	public Integer getBusinessDayOfMonth(CalendarBusinessDayCommand command) {
		Date date = command.getDate();

		// Use get Business Day's Between using last business day of previous month
		command.setDate(DateUtils.getFirstDayOfMonth(date));
		Date startDate = getPreviousBusinessDay(command);
		// What's the last business day if not today?
		command.setDate(DateUtils.addDays(date, 1));
		Date endDate = getPreviousBusinessDay(command);
		return getBusinessDaysBetween(startDate, endDate, command.getBusinessDayType(), command.getCalendarIds());
	}


	@Override
	public Date getNearestBusinessDayFrom(CalendarBusinessDayCommand command, int days) {
		Date targetDate = DateUtils.addDays(command.getDate(), days);

		return isBusinessDayImpl(targetDate, command.getBusinessDayType(), command.getCalendarIds())
				? targetDate
				: getBusinessDayFromImpl(targetDate, command.getBusinessDayType(), days < 0 ? -1 : 1, command.getCalendarIds());
	}


	@Override
	public Date getBusinessDayFrom(CalendarBusinessDayCommand command, int days) {
		return getBusinessDayFromImpl(command.getDate(), command.getBusinessDayType(), days, command.getCalendarIds());
	}


	@Override
	public Date getBusinessDayFrom(Date date, int days, CalendarBusinessDayTypes businessDayType, Calendar... calendars) {
		if (days == 0) {
			return date;
		}
		ValidationUtils.assertNotNull(date, "Required argument 'date' cannot be null.");


		// get distinct business day(s) for the provided calendars
		Comparator<Date> dateComparator = (days < 0) ? Comparator.reverseOrder() : Comparator.naturalOrder();
		List<Date> sortedDistinctDateList = ArrayUtils.getStream(calendars)
				.filter(Objects::nonNull)
				.map(calendar -> getBusinessDayFromImpl(date, businessDayType, days, calendar.getId()))
				.sorted(dateComparator)
				.distinct()
				.collect(Collectors.toList());

		int size = CollectionUtils.getSize(sortedDistinctDateList);
		if (size == 0) {
			return getBusinessDayFromImpl(date, businessDayType, days, null);
		}
		else if (size == 1) {
			return sortedDistinctDateList.get(0);
		}

		// get closest business day from the start date and find first matching business day for all calendars from it
		Date closestDate = sortedDistinctDateList.get(0);
		int increment = (days > 0 ? 1 : -1);
		LocalTime localTime = DateUtils.asLocalTime(closestDate);
		LocalDate localDate = DateUtils.asLocalDate(closestDate);
		int maxIterations = (days < 365) ? 500 : days;
		int iterationCount = 0;
		boolean businessDayForAll;
		boolean maxIterationsReached;
		do {
			localDate = localDate.plusDays(increment);
			businessDayForAll = true;
			for (Calendar calendar : calendars) {
				if (!isBusinessDayImpl(DateUtils.asUtilDate(localDate), businessDayType, calendar.getId())) {
					businessDayForAll = false;
					break;
				}
			}
			maxIterationsReached = (iterationCount++ > maxIterations);
		}
		while (!businessDayForAll && !maxIterationsReached);
		if (maxIterationsReached) {
			throw new RuntimeException("Failed to find a matching Business Day for the provided Calendars within " + maxIterations + " days after the days from conversion.");
		}
		return DateUtils.asUtilDate(localDate.atTime(localTime));
	}

	////////////////////////////////////////////////////////////////////////////
	////////                     Helper Methods                      ///////////
	////////////////////////////////////////////////////////////////////////////


	private boolean isBusinessDayImpl(Date date, CalendarBusinessDayTypes businessDayType, Short... calendarIds) {
		return isBusinessDayImpl(date, businessDayType, false, calendarIds);
	}


	private boolean isBusinessDayImpl(Date date, CalendarBusinessDayTypes businessDayType, boolean partialBusinessDay, Short... calendarIds) {
		//  If the day is a weekend, then it's not a business day - this is checked in the CalendarBusinessDayTypes logic as well, but added here to prevent additional unnecessary lookup
		if (DateUtils.isWeekend(date)) {
			return false;
		}
		Set<Short> idSet = ArrayUtils.getStream(calendarIds).collect(Collectors.toSet());
		if (idSet.size() < 2) {
			return isBusinessDayImpl(date, CollectionUtils.getFirstElement(idSet), businessDayType, partialBusinessDay);
		}

		boolean returnVal = false;
		for (Short calendarId : idSet) {
			// if we find a full day holiday, we can return false regardless of "Partial"
			if (!isBusinessDayImpl(date, calendarId, businessDayType, false)) {
				return false;
			}
			// if we make it to this line, we know it's not a full day holiday, it's either a full business day or a partial business day
			if (!partialBusinessDay || isBusinessDayImpl(date, calendarId, businessDayType, partialBusinessDay)) {
				returnVal = true;
			}
		}
		return returnVal;
	}


	private boolean isBusinessDayImpl(Date date, Short calendarId, CalendarBusinessDayTypes businessDayType, boolean partialBusinessDay) {
		// Looks like in some cases calendar id can be passed as a -1 or 0 - not sure where that comes from (UI?) so added check here to be sure all cases are caught
		calendarId = (calendarId == null || calendarId <= 0 ? getCalendarSetupService().getDefaultCalendar().getId() : calendarId);
		businessDayType = ObjectUtils.coalesce(businessDayType, CalendarBusinessDayTypes.BUSINESS_DAY);

		Map<Date, CalendarHolidayDayHolder> days = getCalendarHolidayCache().getCalendarHolidayMap(calendarId);
		if (days == null) {
			days = new HashMap<>();
			List<CalendarHolidayDay> list = getCalendarHolidayService().getCalendarHolidayDayListByCalendar(calendarId);
			for (CalendarHolidayDay holidayDay : CollectionUtils.getIterable(list)) {
				if (holidayDay != null) {
					CalendarHolidayDayHolder holder = CalendarHolidayDayHolder.of(holidayDay);
					days.put(holder.getDate(), holder);
				}
			}
			getCalendarHolidayCache().setCalendarHolidayMap(calendarId, days);
		}
		CalendarHolidayDayHolder calendarHolidayDayHolder = days.get(DateUtils.clearTime(date));
		return (partialBusinessDay) ? businessDayType.isPartialBusinessDay(date, calendarHolidayDayHolder) : businessDayType.isBusinessDay(date, calendarHolidayDayHolder);
	}


	private Date getBusinessDayFromImpl(Date date, CalendarBusinessDayTypes businessDayType, int days, Short... calendarIds) {
		if (days == 0) {
			return date;
		}
		int increment = (days > 0 ? 1 : -1);
		int currentDays = 0;
		LocalTime localTime = DateUtils.asLocalTime(date);
		LocalDate localDate = DateUtils.asLocalDate(date);
		while (days != currentDays) {
			localDate = localDate.plusDays(increment);
			if (isBusinessDayImpl(DateUtils.asUtilDate(localDate), businessDayType, calendarIds)) {
				currentDays += increment;
			}
		}
		return DateUtils.asUtilDate(localDate.atTime(localTime));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CalendarBusinessDay> getCalendarBusinessDayList(CalendarBusinessDaySearchForm searchForm) {
		ValidationUtils.assertNotNull(searchForm.getCalendarId(), "A specific calendar is required in order to retrieve business days");
		return getCalendarBusinessDayDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(new SubselectParameterExpression(new ShortType(), searchForm.getCalendarId()));
				super.configureCriteria(criteria);
			}
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public CalendarHolidayService getCalendarHolidayService() {
		return this.calendarHolidayService;
	}


	public void setCalendarHolidayService(CalendarHolidayService calendarHolidayService) {
		this.calendarHolidayService = calendarHolidayService;
	}


	public CalendarHolidayCache getCalendarHolidayCache() {
		return this.calendarHolidayCache;
	}


	public void setCalendarHolidayCache(CalendarHolidayCache calendarHolidayCache) {
		this.calendarHolidayCache = calendarHolidayCache;
	}


	public AdvancedReadOnlyDAO<CalendarBusinessDay, Criteria> getCalendarBusinessDayDAO() {
		return this.calendarBusinessDayDAO;
	}


	public void setCalendarBusinessDayDAO(AdvancedReadOnlyDAO<CalendarBusinessDay, Criteria> calendarBusinessDayDAO) {
		this.calendarBusinessDayDAO = calendarBusinessDayDAO;
	}
}
