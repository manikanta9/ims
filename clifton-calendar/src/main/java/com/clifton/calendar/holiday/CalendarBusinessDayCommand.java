package com.clifton.calendar.holiday;

import com.clifton.core.util.date.DateUtils;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>CalendarBusinessDayCommand</code> is used by various methods in the CalendarBusinessDayService to find business day information for a date and optional calendar and the type of business day you are looking for.
 * <p>
 * The command object is used to consolidate the methods needed for the various ways in which business day retrieval is needed, i.e. Full Business Day vs. Partial Business Day, Business Day vs. Trading Business Day, etc.
 *
 * @author manderson
 */
public class CalendarBusinessDayCommand implements Serializable {


	/**
	 * Defaults to today
	 */
	private Date date = DateUtils.clearTime(new Date());

	/**
	 * If Null will use the System Default Calendar
	 */
	private Short[] calendarIds;


	private CalendarBusinessDayTypes businessDayType;

	private boolean partialBusinessDay = false;


	////////////////////////////////////////////////////////////////////////////
	////////                  Business Days                             ////////
	////////////////////////////////////////////////////////////////////////////


	public static CalendarBusinessDayCommand forDate(Date date, Short... calendarIds) {
		return forDateAndCalendarAndBusinessDayType(date, CalendarBusinessDayTypes.BUSINESS_DAY, calendarIds);
	}


	public static CalendarBusinessDayCommand forToday(Short... calendarIds) {
		return forDateAndCalendarAndBusinessDayType(null, CalendarBusinessDayTypes.BUSINESS_DAY, calendarIds);
	}


	public static CalendarBusinessDayCommand forDefaultCalendar(Date date) {
		return forDateAndCalendarAndBusinessDayType(date, CalendarBusinessDayTypes.BUSINESS_DAY);
	}


	public static CalendarBusinessDayCommand forTodayAndDefaultCalendar() {
		return forDateAndCalendarAndBusinessDayType(null, CalendarBusinessDayTypes.BUSINESS_DAY);
	}


	////////////////////////////////////////////////////////////////////////////
	////////               Trade Business Days                          ////////
	////////////////////////////////////////////////////////////////////////////


	public static CalendarBusinessDayCommand forTrade(Date date, Short... calendarIds) {
		return forDateAndCalendarAndBusinessDayType(date, CalendarBusinessDayTypes.TRADE_BUSINESS_DAY, calendarIds);
	}


	public static CalendarBusinessDayCommand forTradeToday(Short... calendarIds) {
		return forDateAndCalendarAndBusinessDayType(null, CalendarBusinessDayTypes.TRADE_BUSINESS_DAY, calendarIds);
	}


	public static CalendarBusinessDayCommand forTradeDefaultCalendar(Date date) {
		return forDateAndCalendarAndBusinessDayType(date, CalendarBusinessDayTypes.TRADE_BUSINESS_DAY);
	}


	public static CalendarBusinessDayCommand forTradeTodayAndDefaultCalendar() {
		return forDateAndCalendarAndBusinessDayType(null, CalendarBusinessDayTypes.TRADE_BUSINESS_DAY);
	}

	////////////////////////////////////////////////////////////////////////////
	////////             Settlement Business Days                       ////////
	////////////////////////////////////////////////////////////////////////////


	public static CalendarBusinessDayCommand forSettlement(Date date, Short... calendarIds) {
		return forDateAndCalendarAndBusinessDayType(date, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY, calendarIds);
	}


	public static CalendarBusinessDayCommand forSettlementToday(Short... calendarIds) {
		return forDateAndCalendarAndBusinessDayType(null, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY, calendarIds);
	}


	public static CalendarBusinessDayCommand forSettlementAndDefaultCalendar(Date date) {
		return forDateAndCalendarAndBusinessDayType(date, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY);
	}


	public static CalendarBusinessDayCommand forSettlementTodayAndDefaultCalendar() {
		return forDateAndCalendarAndBusinessDayType(null, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayCommand withPartialBusinessDay(boolean partialBusinessDay) {
		setPartialBusinessDay(partialBusinessDay);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static CalendarBusinessDayCommand forDateAndCalendarAndBusinessDayType(Date date, CalendarBusinessDayTypes businessDayType, Short... calendarIds) {
		CalendarBusinessDayCommand command = new CalendarBusinessDayCommand();
		if (date != null) {
			command.setDate(date);
		}
		command.setCalendarIds(calendarIds);
		command.setBusinessDayType(businessDayType);
		return command;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public Short[] getCalendarIds() {
		return this.calendarIds;
	}


	public void setCalendarIds(Short[] calendarIds) {
		this.calendarIds = calendarIds;
	}


	public CalendarBusinessDayTypes getBusinessDayType() {
		return this.businessDayType;
	}


	public void setBusinessDayType(CalendarBusinessDayTypes businessDayType) {
		this.businessDayType = businessDayType;
	}


	public boolean isPartialBusinessDay() {
		return this.partialBusinessDay;
	}


	public void setPartialBusinessDay(boolean partialBusinessDay) {
		this.partialBusinessDay = partialBusinessDay;
	}
}
