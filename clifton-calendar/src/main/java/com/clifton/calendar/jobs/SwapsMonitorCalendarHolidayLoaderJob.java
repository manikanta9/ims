package com.clifton.calendar.jobs;

import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.holiday.search.CalendarHolidayDaySearchForm;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarDay;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.calendar.setup.search.CalendarSearchForm;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.logging.LogLevels;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;


/**
 * A batch job to update the IMS Calendar, by syncing IMS Calendar holiday dat data with corresponding data from the SWAPS Monitor holiday data.
 * This job may remove IMS Calendar Holidays over a defined day interval for holidays that do not match the incoming reference data.  The job also
 * adds missing IMS Calendar holiday days over a specified days interval.
 *
 * @author davidi
 */
public class SwapsMonitorCalendarHolidayLoaderJob implements Task, StatusHolderObjectAware<Status>, ValidationAware {

	private enum CalendarTypes {
		TRADING,
		SETTLEMENT,
		BANKING,
		OTHER
	}

	/**
	 * File directory and file name for SWAPS Monitor calendar file.
	 */
	private String calendarSourceFileDirectoryPath;
	private String calendarSourceFileName;

	/**
	 * File directory and file name for mapping proprietary calendar codes to Market Identifier Codes (MIC)
	 */
	private String micCodeMappingFileDirectoryPath;
	private String micCodeMappingFileName;

	/**
	 * File directory and file name for the holiday.des file.  This is used to help determine the SWAPS Monitor calendar type based on the proprietary code and the
	 * description. Types will be mapped to the CalendarTypes enum to identify the calendar types as a Trading, Settlement, Banking, or Other (unknown) holiday calendar.
	 */
	private String calendarDescriptionMappingFileDirectoryPath;
	private String calendarDescriptionMappingFileName;

	/**
	 * File directory to archive processed files. If blank, no archiving will be done.
	 */
	private String archiveFilePath;

	/**
	 * Flag to delete processed file after copied and processed. This is only applicable if the archiveFilePath is specified.
	 */
	private boolean deleteFileAfterProcessed;

	/**
	 * If a file directory contains many dated files matching the file name, these properties assist in ordering the files in order to process the latest one - (e.g. file-prefix_202102111500.csv could use file-prefix_(\d{12}).csv).
	 */
	private String calendarSourceFileNameDatePattern;

	/**
	 * Used to define the date format to use for the File Name Date Extraction Regex in order to convert the extracted segment to a date for sorting (e.g. file-prefix_202102111500.csv would use yyyyMMddHHmm).
	 */
	private String calendarSourceFileNameDateFormat;

	/**
	 * Applicable for CSV file only. Defines the delimiter for the CSV column values. If blank, the whitespace character is used.
	 */
	private String csvCalendarSourceFileDelimiterCharacter = " ";

	/**
	 * Applicable for CSV file only. Defines an optional secondary delimiter for string values resulting from the previous row decomposition. Optional field.
	 */
	private String csvCalendarSourceFileSecondaryDelimiterCharacter = ",";

	/**
	 * Applicable to the MIC Mapping file only, this can be used to define the value delimiter to use for this file. Default is comma.
	 */
	private String csvMicFileDelimiterCharacter = ",";

	/**
	 * Applicable to the MIC Mapping file only, this can be used to define the value delimiter to use for this file. Default is comma.
	 */
	private String csvCalendarDescriptionFileDelimiterCharacter = " ";

	/**
	 * Flag to define whether the processed file has a header row (first row). If true, processes from row 0, otherwise row 1.
	 */
	private boolean noCalendarSourceFileHeaderRow;

	/**
	 * Flag to define whether the processed file has a header row (first row). If true, processes from row 0, otherwise row 1.
	 */
	private boolean noMicMappingFileHeaderRow;

	/**
	 * Flag to define whether the processed file has a header row (first row). If true, processes from row 0, otherwise row 1.
	 */
	private boolean noCalendarDescriptionFileHeaderRow;

	/**
	 * Property indicating a key substrings found in the holiday.des file records that would indicate the calendar type: TRADING
	 */
	private String[] tradingCalendarTypeKeys;

	/**
	 * Property indicating a key substrings found in the holiday.des file records that would indicate the calendar type: SETTLEMENT
	 */
	private String[] settlementCalendarTypeKeys;

	/**
	 * Property indicating a key substrings found in the holiday.des file records that would indicate the calendar type: BANKING
	 */
	private String[] bankingCalendarTypeKeys;

	/**
	 * An optional date from which processing should be performed. If blank, processing will be performed as of the current date.
	 */
	private Date processingDate;

	/**
	 * Changes the maximum number of status messages allowed while processing a file.
	 */
	private Integer maxStatusMessageCount;

	/**
	 * Specifies the maximum number of days to go back (from today) for performing updates of holiday data (additions, updates). (Optional field, if not specified, traverses to the oldest holiday date in the calendar holiday file).
	 */
	private Integer maxDaysBackForUpdates;

	/**
	 * Specifies the maximum number of days to go back (from today) for performing deletions of holiday entries not found in the holiday data. (Optional field, if not specified, traverses to the oldest holiday date in the calendar data file).
	 */
	private Integer maxDaysBackForDeletions;

	/**
	 * Specifies the maximum number of days to go back (from today) for performing updates of holiday data (additions, updates). (Optional field, if not specified, traverses to the highest holiday date in the calendar holiday file).
	 */
	private Integer maxDaysForwardForUpdates;

	/**
	 * Specifies the maximum number of days to go forward (from today) for performing deletions of holiday entries not found in the holiday data. (Optional field, if not specified, traverses to the highest holiday date in the calendar data file).
	 */
	private Integer maxDaysForwardForDeletions;

	private StatusHolderObject<Status> statusHolderObject;

	private CalendarSetupService calendarSetupService;
	private CalendarHolidayService calendarHolidayService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getCalendarSourceFileDirectoryPath(), "The calendar holiday file path where the calendar data file can be found is required");
		ValidationUtils.assertTrue(FileContainerFactory.getFileContainer(FilePath.forPath(getCalendarSourceFileDirectoryPath())).isDirectory(), "The calendar file path where the calendar data file can be found must be a directory");

		ValidationUtils.assertNotEmpty(getCalendarSourceFileName(), "The calendar holiday file name for the calendar data file is required");
		String fileExtension = FileUtils.getFileExtension(getCalendarSourceFileName());
		ValidationUtils.assertNotEmpty(fileExtension, "The calendar file name for the is missing a file extension");

		try {
			Pattern.compile(getCalendarSourceFileName());
		}
		catch (PatternSyntaxException e) {
			throw new ValidationException("The calendar file name cannot be parsed.", e);
		}

		if (!StringUtils.isEmpty(getCalendarSourceFileNameDatePattern())) {
			try {
				Pattern.compile(getCalendarSourceFileNameDatePattern());
			}
			catch (PatternSyntaxException e) {
				throw new ValidationException("The file name date pattern cannot be parsed.", e);
			}
			ValidationUtils.assertNotEmpty(getCalendarSourceFileNameDateFormat(), "File name date format is required when file name date pattern is defined");
		}

		if (!StringUtils.isEmpty(getArchiveFilePath())) {
			FileContainer archiveDirectory = FileContainerFactory.getFileContainer(getArchiveFilePath());
			if (archiveDirectory.exists()) {
				ValidationUtils.assertTrue(archiveDirectory.isDirectory(), "The archive directory path does not resolve to a directory");
			}
		}

		ValidationUtils.assertTrue(getCsvCalendarSourceFileDelimiterCharacter().length() == 1, "The calendar file CSV delimiter can only be one character");
		ValidationUtils.assertTrue(getCsvCalendarSourceFileSecondaryDelimiterCharacter().length() == 1, "The calendar file secondary CSV delimiter can only be one character");
		ValidationUtils.assertTrue(getCsvMicFileDelimiterCharacter().length() == 1, "The MIC mapping file CSV delimiter can only be one character");
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = getStatusHolderObject().getStatus();
		ProcessingStatistics processingStatistics = new ProcessingStatistics();

		if (getMaxStatusMessageCount() != null) {
			status.setMaxDetailCount(getMaxStatusMessageCount());
		}

		CalendarHolidayFile calendarHolidayFile = getCalendarHolidayFileToProcess(status);
		if (calendarHolidayFile.exists()) {
			processCalendarHolidayFile(calendarHolidayFile, status, processingStatistics);
			transferStatsToStatus(processingStatistics, status);
			archiveCalendarSourceFile(calendarHolidayFile);
			if (isDeleteFileAfterProcessed()) {
				deleteSecurityListSourceFile(calendarHolidayFile);
			}
		}
		else {
			status.addError(String.format("Processing of calendar holiday file failed because file did not exist (path [%s}, fileName/regex [%s])", getCalendarSourceFileName(), getCalendarSourceFileName()));
		}
		return status;
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	// Transfers message and details from temporary ProcessingStatistics instance to the Status instance contained in the StatusHolder.
	private void transferStatsToStatus(ProcessingStatistics processingStatistics, Status status) {
		String statusMessage = "Holiday updates across " + processingStatistics.getTotalCalendarCount() + " calendars: Trading[" + processingStatistics.getTotalDeleteCount(true) + " deleted; " +
				processingStatistics.getTotalInsertCount(true) + " inserted; " + processingStatistics.getTotalUpdateCount(true) + " updated] " +
				"Settlement[" + processingStatistics.getTotalDeleteCount(false) + " deleted; " + processingStatistics.getTotalInsertCount(false) + " inserted; " + processingStatistics.getTotalUpdateCount(false) + " updated]";
		StringBuilder combinedDetailMessageBuilder = new StringBuilder();

		for (Calendar calendar : CollectionUtils.getIterable(processingStatistics.getCalendarSet())) {
			StringBuilder detailMessageStringBuilder = new StringBuilder();
			detailMessageStringBuilder.append("Calendar \"");
			detailMessageStringBuilder.append(calendar.getName());
			detailMessageStringBuilder.append("\" ");
			if (!(processingStatistics.hasTradeHolidayUpdates(calendar) || processingStatistics.hasSettlementHolidayUpdates(calendar))) {
				detailMessageStringBuilder.append("processed with no updates.");
			}
			else {
				detailMessageStringBuilder.append("processed with updates for ");
				if (processingStatistics.hasTradeHolidayUpdates(calendar)) {
					detailMessageStringBuilder.append("Trading[")
					.append(processingStatistics.getDeleteCount(calendar, true))
					.append(" deleted; ")
					.append(processingStatistics.getInsertCount(calendar, true))
					.append(" inserted; ")
					.append(processingStatistics.getUpdateCount(calendar, true))
					.append(" updated] ");
				}
				if (processingStatistics.hasSettlementHolidayUpdates(calendar)) {
					detailMessageStringBuilder.append("Settlement[")
					.append(processingStatistics.getDeleteCount(calendar, false))
					.append(" deleted; ")
					.append(processingStatistics.getInsertCount(calendar, false))
					.append(" inserted; ")
					.append(processingStatistics.getUpdateCount(calendar, false))
					.append(" updated]");
				}
			}
			String currentLine = detailMessageStringBuilder.toString();
			status.addDetail(StatusDetail.CATEGORY_MESSAGE, currentLine);

			combinedDetailMessageBuilder.append(currentLine)
				.append(System.lineSeparator());
		}

		if (combinedDetailMessageBuilder.length() > 0) {
			statusMessage += System.lineSeparator() + System.lineSeparator() + "Details: " + System.lineSeparator() + System.lineSeparator() + combinedDetailMessageBuilder.toString();
		}

		status.setMessage(statusMessage);
	}


	/**
	 * Traverses the SwapMonitor's holidays.dat file, one line at a time, and submits non-blank lines to the processCalendarFileEntry
	 * method for further processing and IMS CalendarHolidayDay table updates.
	 */
	private void processCalendarHolidayFile(CalendarHolidayFile calendarHolidayFile, Status status, ProcessingStatistics processingStatistics) {
		final Map<String, String> proprietaryCodeToMicMapping = loadMicMappingData(status);
		final Map<String, Set<Calendar>> imsCalendarsByMic = getImsCalendarsByMic();
		final Map<String, CalendarTypes> calendarTypesMap = loadCalendarTypeMappingData(status);
		// This map tracks which calendar types (TRADING or SETTLEMENT) have been applied to a calendar. This enables us to perform date removal operations only once,
		// when adding entries from different Swap Monitor calendars with matching MIC codes.
		final Map<CalendarTypes, HashSet<Calendar>> previouslyProcessedCalendarMap = new EnumMap<>(CalendarTypes.class);

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(calendarHolidayFile.getFileToProcess().getInputStream()))) {
			Date processingDt = ObjectUtils.coalesce(getProcessingDate(), new Date());
			Date minimumUpdateDate = null;
			Date maximumUpdateDate = null;
			Date minimumDeleteDate = null;
			Date maximumDeleteDate = null;
			if (getMaxDaysBackForUpdates() != null) {
				minimumUpdateDate = DateUtils.addDays(processingDt, getMaxDaysBackForUpdates() < 0 ? getMaxDaysBackForUpdates() : -getMaxDaysBackForUpdates());
			}
			if (getMaxDaysForwardForUpdates() != null) {
				maximumUpdateDate = DateUtils.addDays(processingDt, getMaxDaysForwardForUpdates());
			}
			if (getMaxDaysBackForDeletions() != null) {
				minimumDeleteDate = DateUtils.addDays(processingDt, getMaxDaysBackForDeletions() < 0 ? getMaxDaysBackForDeletions() : -getMaxDaysBackForDeletions());
			}
			if (getMaxDaysForwardForDeletions() != null) {
				maximumDeleteDate = DateUtils.addDays(processingDt, getMaxDaysForwardForDeletions());
			}

			final Date absoluteMinimumDate = DateUtils.compare(minimumDeleteDate, minimumUpdateDate, false) == -1 ? minimumDeleteDate : minimumUpdateDate;
			final Date absoluteMaximumDate = DateUtils.compare(maximumDeleteDate, maximumUpdateDate, false) == 1 ? maximumDeleteDate : maximumUpdateDate;

			boolean firstLine = true;
			String line;
			while ((line = reader.readLine()) != null) {
				if (firstLine) {
					firstLine = false;
					// if first line is a header row, skip it
					if (!isNoCalendarSourceFileHeaderRow()) {
						continue;
					}
				}
				if (!StringUtils.isEmpty(line)) {
					processCalendarFileEntry(line, imsCalendarsByMic, proprietaryCodeToMicMapping, previouslyProcessedCalendarMap, calendarTypesMap, minimumUpdateDate, maximumUpdateDate, minimumDeleteDate, maximumDeleteDate, absoluteMinimumDate, absoluteMaximumDate, status, processingStatistics);
				}
			}
		}
		catch (IOException exc) {
			String filename = calendarHolidayFile.getFileToProcess().getName() != null ? calendarHolidayFile.getFileToProcess().getName() : "unknown";
			status.addError("Error opening calendar holiday file \"" + filename + "\"  for reading.");
		}
	}


	/**
	 * Processes a single line of the Swaps Monitor holidays.dat file.  This function will process entries that successfully map to a MIC Code.  This MIC code
	 * is used to lookup corresponding calendars in the IMS Calendar table.  When calendars are found, the IMS CalendarHolidayDay table is updated to match
	 * the calendar holiday dates in the holidays.dat file. Calendar updates may be constrained to a date range established by the . Calendar entries in CalendarHolidayDay which are not in the holidays.dat file will be deleted,
	 * unless they fall outside of the date range established by the maxDaysBackForDeletions and maxDaysForwardForDeletions properties.
	 */
	@Transactional
	protected void processCalendarFileEntry(String fileRecord, Map<String, Set<Calendar>> imsCalendarsByMic, Map<String, String> proprietaryCodeToMicMapping, Map<CalendarTypes, HashSet<Calendar>> previouslyProcessedCalendarMap, Map<String, CalendarTypes> calendarTypesMap, Date minimumUpdateDate, Date maximumUpdateDate, Date minimumDeleteDate, Date maximumDeleteDate,
	                                        Date absoluteMinimumDate, Date absoluteMaximumDate, Status status, ProcessingStatistics processingStatistics) {
		String[] components = fileRecord.split(getCsvCalendarSourceFileDelimiterCharacter(), 2);
		String proprietaryCode = components.length > 0 ? components[0] : null;
		String micCode = proprietaryCode != null ? proprietaryCodeToMicMapping.get(proprietaryCode) : null;

		if (!StringUtils.isEmpty(micCode) && imsCalendarsByMic.containsKey(micCode)) {
			Set<Date> calendarDateSet = getDateSetFromHolidayCalendarEntry(fileRecord, absoluteMinimumDate, absoluteMaximumDate, status);
			CalendarTypes calendarType = calendarTypesMap.get(proprietaryCode);

			// currently we support only TRADING and SETTLEMENT Calendars
			if (calendarType == CalendarTypes.TRADING || calendarType == CalendarTypes.SETTLEMENT) {
				LogUtils.log(this.getClass(), LogLevels.DEBUG, "Processing Swaps Monitor calendar with proprietary code: \"" + proprietaryCode + "\" with MIC: \"" + micCode + "\".");
				for (Calendar imsCalendar : CollectionUtils.getIterable(imsCalendarsByMic.get(micCode))) {
					ImsCalendarHolidayDayDateMapHolder imsCalendarHolidayDayDateMapHolder = getImsCalendarHolidayDayDateMapHolder(imsCalendar, absoluteMinimumDate, absoluteMaximumDate);
					insertMissingHolidays(imsCalendar, imsCalendarHolidayDayDateMapHolder, calendarType, calendarDateSet, minimumUpdateDate, maximumUpdateDate, status, processingStatistics);

					// remove non-matching holidays only once, to prevent removal of newly-added days in cases where multiple calendars are merged into a single IMS calendar.
					Set<Calendar> previouslyProcessedCalendarSet = previouslyProcessedCalendarMap.computeIfAbsent(calendarType, k -> new HashSet<>());
					if (!previouslyProcessedCalendarSet.contains(imsCalendar)) {
						status.addMessage("Processing calendar: " + imsCalendar.getName() + " MIC: " + micCode);
						removeNonMatchingHolidays(imsCalendar, imsCalendarHolidayDayDateMapHolder, calendarType, calendarDateSet, minimumDeleteDate, maximumDeleteDate, status, processingStatistics);
						previouslyProcessedCalendarSet.add(imsCalendar);
					}
				}
			}
			else {
				status.addWarning("Calendar type: " + calendarType + " for proprietary code " + proprietaryCode + " and MIC code: " + micCode + " is not a calendar of trading or settlement holidays. This calendar will not be processed.");
			}
		}
	}


	protected void insertMissingHolidays(Calendar calendar, ImsCalendarHolidayDayDateMapHolder imsCalendarHolidayDayDateMapHolder, CalendarTypes calendarType, Set<Date> calendarDateSet, Date minimumUpdateDate, Date maximumUpdateDate, Status status, ProcessingStatistics processingStatistics) {
		Set<Date> calendarDateSetCopy = new HashSet<>(calendarDateSet);

		// removing the CalendarHolidayDay dates from the SWAPS Monitor calendar holiday date set, will yield a difference, which are the days that need to be added.
		calendarDateSetCopy.removeAll(imsCalendarHolidayDayDateMapHolder.getDateSetByCalendarType(calendarType));

		try {
			int savedCount = 0;
			for (Date date : CollectionUtils.getIterable(calendarDateSetCopy)) {
				if ((minimumUpdateDate == null || DateUtils.isDateAfterOrEqual(date, minimumUpdateDate)) && (maximumUpdateDate == null || DateUtils.isDateBeforeOrEqual(date, maximumUpdateDate, false))) {
					// if the calendarHolidayDay already exists, update it.
					CalendarHolidayDay calendarHolidayDay = imsCalendarHolidayDayDateMapHolder.getCalendarHolidayDayForDate(date);
					if (calendarHolidayDay != null) {
						if (calendarType == CalendarTypes.TRADING) {
							calendarHolidayDay.setTradeHoliday(true);
							processingStatistics.incrementUpdates(calendar, true);
						}
						else {
							calendarHolidayDay.setSettlementHoliday(true);
							processingStatistics.incrementUpdates(calendar, false);
						}
					}
					else {
						CalendarDay calendarDay = getCalendarSetupService().getCalendarDayByDate(date);
						if (calendarDay == null) {
							status.addError("Cannot insert holiday for date: " + date + ". There is no CalendarDay entry in IMS for this date.");
							continue;
						}
						calendarHolidayDay = new CalendarHolidayDay();
						calendarHolidayDay.setCalendar(calendar);
						calendarHolidayDay.setDay(calendarDay);

						//There is no reliable data to derive the and match the detailed name of the holiday, so we will use "Market Holiday" and "Non-Settlement Day"
						calendarHolidayDay.setHoliday(getCalendarHolidayService().getCalendarHolidayByName(calendarType == CalendarTypes.TRADING ? "Market Holiday" : "Non-Settlement Day"));
						if (calendarType == CalendarTypes.TRADING) {
							calendarHolidayDay.setTradeHoliday(true);
							processingStatistics.incrementInserts(calendar, true);
						}
						else {
							calendarHolidayDay.setSettlementHoliday(true);
							processingStatistics.incrementInserts(calendar, false);
						}
						// SWAPS data does not currently have indicators as to whether holiday date is full or partial holiday.  Use full day for now.
						calendarHolidayDay.setFullDayHoliday(true);
					}

					getCalendarHolidayService().saveCalendarHolidayDay(calendarHolidayDay);
					++savedCount;
				}
			}
			LogUtils.log(this.getClass(), LogLevels.DEBUG, "Added and saved: " + savedCount + " " + calendarType + " entries into calendar: \"" + calendar.getName() + "\".");
		}
		catch (Exception exc) {
			status.addError("Cannot add CalendarHolidayDay entries to calendar \"" + calendar.getName() + "\": " + exc.getMessage());
		}
	}


	protected void removeNonMatchingHolidays(Calendar calendar, ImsCalendarHolidayDayDateMapHolder imsCalendarHolidayDayDateMapHolder, CalendarTypes calendarType, Set<Date> calendarDateSet, Date minimumDeleteDate, Date maximumDeleteDate, Status status, ProcessingStatistics processingStatistics) {
		Set<Date> calendarHolidayDayDateSetCopy = new HashSet<>(imsCalendarHolidayDayDateMapHolder.getDateSetByCalendarType(calendarType));

		// removing all SWAPS Monitor File calendar dates from our calendarHolidayDayDateSet will yield a set of days that are in our IMS Calendar, but not in the SWAPS Monitor calendar.  These will be removed.
		calendarHolidayDayDateSetCopy.removeAll(calendarDateSet);
		int removeCount = 0;

		for (Date date : CollectionUtils.getIterable(calendarHolidayDayDateSetCopy)) {
			CalendarHolidayDay calendarHolidayDay = imsCalendarHolidayDayDateMapHolder.getCalendarHolidayDayForDate(date);
			if (calendarHolidayDay != null && (minimumDeleteDate == null || DateUtils.isDateAfterOrEqual(date, minimumDeleteDate)) && (maximumDeleteDate == null || DateUtils.isDateBeforeOrEqual(date, maximumDeleteDate, false))) {
				try {
					// Update flags on existing entry if it is both a trading and settlement holiday.
					if (calendarType == CalendarTypes.TRADING && calendarHolidayDay.isSettlementHoliday()) {
						calendarHolidayDay.setTradeHoliday(false);
						getCalendarHolidayService().saveCalendarHolidayDay(calendarHolidayDay);
						processingStatistics.incrementUpdates(calendar, true);
					}
					else if (calendarType == CalendarTypes.SETTLEMENT && calendarHolidayDay.isTradeHoliday()) {
						calendarHolidayDay.setSettlementHoliday(false);
						getCalendarHolidayService().saveCalendarHolidayDay(calendarHolidayDay);
						processingStatistics.incrementUpdates(calendar, false);
					}
					else {
						getCalendarHolidayService().deleteCalendarHolidayDay(calendarHolidayDay.getId());
						processingStatistics.incrementDeletes(calendar, calendarType == CalendarTypes.TRADING);
					}
					++removeCount;
				}
				catch (Exception exc) {
					status.addError("Cannot remove CalendarHolidayDay entry from calendar \"" + calendar.getName() + "\": " + exc.getMessage());
				}
			}
		}
		LogUtils.log(this.getClass(), LogLevels.DEBUG, "Removed and saved: " + removeCount + " " + calendarType + " entries from calendar: \"" + calendar.getName() + "\".");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Loads the Mic Mapping csv file into a Map of custom identifiers in the calendar file to MIC codes specified in the IMS Calendars.
	 * Blank lines and CSV rows without proprietary code or MIC code are skipped.
	 */
	private Map<String, String> loadMicMappingData(Status status) {
		Map<String, String> micCodeMapping = new HashMap<>();
		final String csvDelimiter = getCsvMicFileDelimiterCharacter() != null ? getCsvMicFileDelimiterCharacter() : ",";
		final String directory = getMicCodeMappingFileDirectoryPath() != null ? getMicCodeMappingFileDirectoryPath() : getCalendarSourceFileDirectoryPath();
		final FilePath filePath = FilePath.forPath(FileUtils.combinePath(directory, getMicCodeMappingFileName()));
		final FileContainer micMappingFile = FileContainerFactory.getFileContainer(filePath);
		final String noMicCodEntry = "----";

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(micMappingFile.getInputStream()))) {
			boolean firstLine = true;
			String line;
			while ((line = reader.readLine()) != null) {
				if (firstLine) {
					firstLine = false;
					// if first line is a header row, skip it
					if (!isNoMicMappingFileHeaderRow()) {
						continue;
					}
				}

				line = line.trim();
				if (!StringUtils.isEmpty(line)) {
					String[] components = line.split(csvDelimiter, 2);
					if (components.length == 2) {
						String proprietaryCode = components[0].trim().toUpperCase();
						String micCode = components[1].trim().toUpperCase();
						if (!StringUtils.isEmpty(proprietaryCode) && !StringUtils.isEmpty(micCode) && !noMicCodEntry.equals(micCode)) {
							micCodeMapping.put(proprietaryCode, micCode);
						}
					}
				}
			}
		}
		catch (IOException exc) {
			String filename = micMappingFile.getName();
			status.addError("Error opening MIC Mapping file \"" + filename + "\"  for reading: " + exc.getMessage());
		}
		return micCodeMapping;
	}


	/**
	 * Loads the holiday.des file and parses the lines to create a map of proprietary calendar codes to the CalendarType enumeration values.
	 * Returns a Map that maps a calendar proprietary code to a CalendarType enumeration value.
	 */
	private Map<String, CalendarTypes> loadCalendarTypeMappingData(Status status) {
		Map<String, CalendarTypes> calendarTypesMapping = new HashMap<>();

		final String directory = getCalendarDescriptionMappingFileDirectoryPath() != null ? getCalendarDescriptionMappingFileDirectoryPath() : getCalendarSourceFileDirectoryPath();
		final FilePath filePath = FilePath.forPath(FileUtils.combinePath(directory, getCalendarDescriptionMappingFileName()));
		final FileContainer calendarTypeMappingFile = FileContainerFactory.getFileContainer(filePath);

		// Keys (substrings) used to identify calendar types
		final Set<String> tradeCalendarKeys = CollectionUtils.createHashSet(!ArrayUtils.isEmpty(getTradingCalendarTypeKeys()) ? getTradingCalendarTypeKeys() : ArrayUtils.createArray("trading holidays", "trade date holidays"));
		final Set<String> settlementCalendarKeys = CollectionUtils.createHashSet(!ArrayUtils.isEmpty(getSettlementCalendarTypeKeys()) ? getSettlementCalendarTypeKeys() : ArrayUtils.createArray("settlement holidays"));
		final Set<String> bankingCalendarKeys = CollectionUtils.createHashSet(!ArrayUtils.isEmpty(getBankingCalendarTypeKeys()) ? getBankingCalendarTypeKeys() : ArrayUtils.createArray("bank holidays", "banking holidays"));

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(calendarTypeMappingFile.getInputStream()))) {
			boolean firstLine = true;
			String line;
			while ((line = reader.readLine()) != null) {
				if (firstLine) {
					firstLine = false;
					// if first line is a header row, skip it
					if (!isNoCalendarDescriptionFileHeaderRow()) {
						continue;
					}
				}
				line = line.trim();
				if (!StringUtils.isEmpty(line)) {
					String[] components = line.split(getCsvCalendarDescriptionFileDelimiterCharacter(), 2);
					if (components.length == 2) {
						String proprietaryCode = components[0].trim().toUpperCase();
						String description = components[1].trim();
						if (!StringUtils.isEmpty(proprietaryCode) && !StringUtils.isEmpty(description)) {
							if (stringContainsAny(description, tradeCalendarKeys)) {
								calendarTypesMapping.put(proprietaryCode, CalendarTypes.TRADING);
							}
							else if (stringContainsAny(description, settlementCalendarKeys)) {
								calendarTypesMapping.put(proprietaryCode, CalendarTypes.SETTLEMENT);
							}
							else if (stringContainsAny(description, bankingCalendarKeys)) {
								calendarTypesMapping.put(proprietaryCode, CalendarTypes.BANKING);
							}
							else {
								calendarTypesMapping.put(proprietaryCode, CalendarTypes.OTHER);
							}
						}
					}
				}
			}
		}
		catch (IOException exc) {
			String filename = calendarTypeMappingFile.getName();
			status.addError("Error opening Calendar Description Mapping file \"" + filename + "\"  for reading.");
		}
		return calendarTypesMapping;
	}


	/**
	 * Checks the string to determine if it contains any string values from the subStrings parameter.
	 * Returns true if the string contains any of the substrings in the subStrings parameter.
	 * Returns false if no substrings are contained within the string.
	 */
	private boolean stringContainsAny(String string, Set<String> subStrings) {
		if (StringUtils.isEmpty(string) || CollectionUtils.isEmpty(subStrings)) {
			return false;
		}
		for (String subString : subStrings) {
			if (string.contains(subString)) {
				return true;
			}
		}

		return false;
	}


	/**
	 * Returns a map of MIC codes mapped to one or more matching IMS Calendar entries assigned to the MIC code.
	 */
	private Map<String, Set<Calendar>> getImsCalendarsByMic() {
		Map<String, Set<Calendar>> micCalendarMap = new HashMap<>();
		CalendarSearchForm calendarSearchForm = new CalendarSearchForm();
		calendarSearchForm.setRollupCalendar(false);
		calendarSearchForm.setMarketIdentifierCodeNotNull(true);
		List<Calendar> calendarList = getCalendarSetupService().getCalendarList(calendarSearchForm);
		for (Calendar calendar : CollectionUtils.getIterable(calendarList)) {
			Set<Calendar> calendarsForMicSet = micCalendarMap.computeIfAbsent(calendar.getMarketIdentifierCode().toUpperCase(), key -> new HashSet<>());
			calendarsForMicSet.add(calendar);
		}

		return micCalendarMap;
	}


	/**
	 * Returns an {@link ImsCalendarHolidayDayDateMapHolder} class instance containing calendar holiday data for the specified Calendar.
	 * The date range for the data is constrained by an absolute range defined by the absoluteMinimum and absoluteMaximum date parameters.
	 */
	private ImsCalendarHolidayDayDateMapHolder getImsCalendarHolidayDayDateMapHolder(Calendar calendar, Date absoluteMinimumDate, Date absoluteMaximumDate) {
		Map<Date, CalendarHolidayDay> calendarHolidayDayDateMap = new HashMap<>();

		CalendarHolidayDaySearchForm calendarHolidayDaySearchForm = new CalendarHolidayDaySearchForm();
		calendarHolidayDaySearchForm.setCalendarId(calendar.getId());
		calendarHolidayDaySearchForm.addSearchRestriction("startDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, absoluteMinimumDate);
		calendarHolidayDaySearchForm.addSearchRestriction("startDate", ComparisonConditions.LESS_THAN_OR_EQUALS, absoluteMaximumDate);

		List<CalendarHolidayDay> calendarHolidayDayList = getCalendarHolidayService().getCalendarHolidayDayList(calendarHolidayDaySearchForm);

		for (CalendarHolidayDay calendarHolidayDay : CollectionUtils.getIterable(calendarHolidayDayList)) {
			if (calendarHolidayDay != null && calendarHolidayDay.getDay() != null && calendarHolidayDay.getDay().getStartDate() != null) {
				calendarHolidayDayDateMap.put(DateUtils.clearTime(calendarHolidayDay.getDay().getStartDate()), calendarHolidayDay);
			}
		}
		return new ImsCalendarHolidayDayDateMapHolder(calendarHolidayDayDateMap);
	}


	/**
	 * Parses a single line of data from the SWAPS monitor file, to extract the dates and return a set of holiday dates.
	 */
	private Set<Date> getDateSetFromHolidayCalendarEntry(String entry, Date minimumDate, Date maximumDate, Status status) {
		Set<Date> holidayDateSet = new HashSet<>();
		if (!StringUtils.isEmpty(entry)) {
			String[] components = entry.split(getCsvCalendarSourceFileDelimiterCharacter(), 2);
			if (ArrayUtils.getLength(components) == 2) {
				String calendarData = components[1];
				// remove quoted substrings
				calendarData = calendarData.replaceAll("\".*?\"", "").trim();
				// remove trailing secondary delimiter
				if (calendarData.endsWith(getCsvCalendarSourceFileSecondaryDelimiterCharacter())) {
					calendarData = calendarData.substring(0, calendarData.length() - 2);
				}
				String[] holidayDateArray = calendarData.split(getCsvCalendarSourceFileSecondaryDelimiterCharacter());
				for (String holidayDateString : holidayDateArray) {
					try {
						Date holidayDate = DateUtils.toDate(holidayDateString.trim(), "yyyyMMdd");
						if (holidayDate != null && DateUtils.isDateBetween(holidayDate, minimumDate, maximumDate, false)) {
							holidayDateSet.add(holidayDate);
						}
					}
					catch (Exception exc) {
						status.addError("Cannot parse holiday date: \"" + holidayDateString + "\" in calendar with proprietary code " + components[0] + ": " + exc.getMessage());
					}
				}
			}
		}

		return holidayDateSet;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A private class to facilitate the calendar holiday classifications (trading vs settlement) given a
	 * Map<Date, CalendarHolidayDay>.  This helps determine which dates correspond to trading holidays vs. settlement holidays.
	 * In some cases a CalendarHolidayDay may be both.  Using this class simplifies calendar date comparisons with external
	 * calendar data, and set operations to determine in what manner to add or delete an entry (e.g. via an update to existing entry,
	 * or
	 */
	private static class ImsCalendarHolidayDayDateMapHolder {

		private final Set<Date> tradingHolidayDateSet = new HashSet<>();
		private final Set<Date> settlementHolidayDateSet = new HashSet<>();
		private Map<Date, CalendarHolidayDay> calendarHolidayDaysByDateMap = null;


		public ImsCalendarHolidayDayDateMapHolder(Map<Date, CalendarHolidayDay> holidayDaysByDateMap) {
			if (holidayDaysByDateMap != null) {
				this.calendarHolidayDaysByDateMap = holidayDaysByDateMap;
				for (Map.Entry<Date, CalendarHolidayDay> mapEntry : CollectionUtils.getIterable(holidayDaysByDateMap.entrySet())) {
					if (mapEntry.getValue() != null && mapEntry.getValue().isTradeHoliday()) {
						this.tradingHolidayDateSet.add(mapEntry.getKey());
					}
					if (mapEntry.getValue() != null && mapEntry.getValue().isSettlementHoliday()) {
						this.settlementHolidayDateSet.add(mapEntry.getKey());
					}
				}
			}
		}


		public CalendarHolidayDay getCalendarHolidayDayForDate(Date date) {
			return this.calendarHolidayDaysByDateMap.get(date);
		}


		public Set<Date> getDateSetByCalendarType(CalendarTypes calendarType) {
			if (calendarType == CalendarTypes.TRADING) {
				return getTradingHolidayDateSet();
			}
			else if (calendarType == CalendarTypes.SETTLEMENT) {
				return getSettlementHolidayDateSet();
			}
			else {
				// other calendar types currently not supported.
				return new HashSet<>();
			}
		}


		public Set<Date> getTradingHolidayDateSet() {
			return this.tradingHolidayDateSet;
		}


		public Set<Date> getSettlementHolidayDateSet() {
			return this.settlementHolidayDateSet;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarHolidayFile getCalendarHolidayFileToProcess(Status status) {
		Pattern fileNamePattern = Pattern.compile(getCalendarSourceFileName());
		FileContainer calendarDataDirectory = FileContainerFactory.getFileContainer(getCalendarSourceFileDirectoryPath());
		try {
			List<FileContainer> matchingSecurityListFileList = calendarDataDirectory.list("*", fileName -> fileNamePattern.matcher(fileName).matches())
					.map(fileName -> FileContainerFactory.getFileContainer(FileUtils.combinePath(getCalendarSourceFileDirectoryPath(), fileName)))
					.collect(Collectors.toList());
			return findCalendarHolidayFileToProcess(matchingSecurityListFileList, status);
		}
		catch (IOException io) {
			String message = String.format("Failed to find applicable calendar holiday file using path [%s] and name [%s]", getCalendarSourceFileDirectoryPath(), getCalendarSourceFileName());
			status.addError(message);
			throw new RuntimeException(message, io);
		}
	}


	private CalendarHolidayFile findCalendarHolidayFileToProcess(List<FileContainer> possibleCalendarFileList, Status status) {
		int size = possibleCalendarFileList.size();
		if (size == 0) {
			status.addMessage(String.format("File not found (path [%s}, fileName/regex [%s])", getCalendarSourceFileDirectoryPath(), getCalendarSourceFileName()));
			return CalendarHolidayFile.newEmptyMatch();
		}
		else if (size == 1) {
			status.addMessage(String.format("Found single file match (path [%s}, fileName/regex [%s])", getCalendarSourceFileDirectoryPath(), getCalendarSourceFileName()));
			return CalendarHolidayFile.newSingleMatch(CollectionUtils.getFirstElement(possibleCalendarFileList));
		}

		if (!StringUtils.isEmpty(getCalendarSourceFileNameDatePattern())) {
			status.addMessage(String.format("Found multiple file matches applying date filter sorting (path [%s}, fileName/regex [%s], datePatter [%s], dateFormat [%s])", getCalendarSourceFileDirectoryPath(), getCalendarSourceFileName(), getCalendarSourceFileNameDatePattern(), getCalendarSourceFileNameDateFormat()));
			Pattern dataPattern = Pattern.compile(getCalendarSourceFileNameDatePattern());
			Map<Date, FileContainer> dateToFileNameMap = new TreeMap<>(Collections.reverseOrder());
			for (FileContainer entry : CollectionUtils.getIterable(possibleCalendarFileList)) {
				Matcher matcher = dataPattern.matcher(entry.getName());
				if (matcher.matches()) {
					Date date = DateUtils.toDate(matcher.group(1), getCalendarSourceFileNameDateFormat());
					dateToFileNameMap.put(date, entry);
				}
			}

			List<FileContainer> otherMatchingFiles = new ArrayList<>();
			Iterator<Map.Entry<Date, FileContainer>> sortedFileMatchIterator = dateToFileNameMap.entrySet().iterator();
			FileContainer toProcess = sortedFileMatchIterator.next().getValue();
			while (sortedFileMatchIterator.hasNext()) {
				otherMatchingFiles.add(sortedFileMatchIterator.next().getValue());
			}
			return CalendarHolidayFile.newWithMultipleMatches(toProcess, otherMatchingFiles);
		}

		String message = String.format("Found multiple calendar holiday files using path [%s] and name [%s]. If applicable use the file name date pattern and format fields.", getCalendarSourceFileDirectoryPath(), getCalendarSourceFileName());
		status.addError(message);
		throw new RuntimeException(message);
	}


	private void archiveCalendarSourceFile(CalendarHolidayFile calendarFile) {
		if (calendarFile.exists() && !StringUtils.isEmpty(getArchiveFilePath())) {
			if (createArchiveDirectory()) {
				calendarFile.performAction(this::moveFileOverwrite);
			}
		}
	}


	private void moveFileOverwrite(FileContainer fileToMove) {
		FileContainer archiveFile = FileContainerFactory.getFileContainer(FileUtils.combinePath(getArchiveFilePath(), fileToMove.getName()));
		try {
			FileUtils.moveFileOverwrite(fileToMove, archiveFile);
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to archive security list file: " + archiveFile.getPath(), e);
		}
	}


	private boolean createArchiveDirectory() {
		if (!StringUtils.isEmpty(getArchiveFilePath())) {
			try {
				FileContainer archiveDirectory = FileContainerFactory.getFileContainer(getArchiveFilePath());
				if (archiveDirectory.exists()) {
					return true;
				}
				return FileUtils.createDirectory(getArchiveFilePath());
			}
			catch (Exception e) {
				throw new RuntimeException("Failed to create calendar holiday file archive directory: " + getArchiveFilePath(), e);
			}
		}
		return false;
	}


	private void deleteSecurityListSourceFile(CalendarHolidayFile securityListFile) {
		if (securityListFile.exists() && isDeleteFileAfterProcessed()) {
			securityListFile.performAction(this::deleteFile);
		}
	}


	private void deleteFile(FileContainer fileToDelete) {
		try {
			fileToDelete.deleteFile();
		}
		catch (IOException io) {
			throw new RuntimeException("Failed to delete calendar holiday file: " + fileToDelete.getPath(), io);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private static final class CalendarHolidayFile {

		private final FileContainer fileToProcess;
		private final List<FileContainer> matchingFileList;


		private CalendarHolidayFile(FileContainer fileToProcess, List<FileContainer> matchingFileList) {
			this.fileToProcess = fileToProcess;
			this.matchingFileList = matchingFileList;
		}


		static CalendarHolidayFile newEmptyMatch() {
			return new CalendarHolidayFile(null, Collections.emptyList());
		}


		static CalendarHolidayFile newSingleMatch(FileContainer fileToProcess) {
			return new CalendarHolidayFile(fileToProcess, Collections.emptyList());
		}


		static CalendarHolidayFile newWithMultipleMatches(FileContainer fileToProcess, List<FileContainer> otherMatchingFiles) {
			return new CalendarHolidayFile(fileToProcess, otherMatchingFiles);
		}


		public void performAction(Consumer<FileContainer> fileContainerAction) {
			if (fileContainerAction != null && exists()) {
				fileContainerAction.accept(getFileToProcess());
				// Filter matching for non-null and those not matching processed file name to avoid redundant action
				CollectionUtils.getStream(getMatchingFileList())
						.filter(Objects::nonNull)
						.filter(file -> !file.getName().equals(getFileToProcess().getName()))
						.forEach(fileContainerAction);
			}
		}


		public boolean exists() {
			return getFileToProcess() != null && getFileToProcess().exists();
		}


		public FileContainer getFileToProcess() {
			return this.fileToProcess;
		}


		public List<FileContainer> getMatchingFileList() {
			return this.matchingFileList;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * A private class used to facilitate the gathering of statistics by calendar and calendar type.
	 */
	private static final class ProcessingStatistics {

		public enum ModifyModes {
			INSERT_TRADING_CAL,
			INSERT_SETTLEMENT_CAL,
			UPDATE_TRADING_CAL,
			UPDATE_SETTLEMENT_CAL,
			DELETE_TRADING_CAL,
			DELETE_SETTLEMENT_CAL
		}

		private final Map<Calendar, Map<ModifyModes, Integer>> statsByCalendarMap = new HashMap<>();

		public boolean hasTradeHolidayUpdates(Calendar calendar) {
			Set<ModifyModes> keySet = getStatsByCalendarMap().get(calendar).keySet();
			return keySet.contains(ModifyModes.INSERT_TRADING_CAL) || keySet.contains(ModifyModes.UPDATE_TRADING_CAL) || keySet.contains(ModifyModes.DELETE_TRADING_CAL);
		}

		public boolean hasSettlementHolidayUpdates(Calendar calendar) {
			Set<ModifyModes> keySet = getStatsByCalendarMap().get(calendar).keySet();
			return keySet.contains(ModifyModes.INSERT_SETTLEMENT_CAL) || keySet.contains(ModifyModes.UPDATE_SETTLEMENT_CAL) || keySet.contains(ModifyModes.DELETE_SETTLEMENT_CAL);
		}


		public void incrementInserts(Calendar calendar, boolean isTradingEntry) {
			incrementCounter(calendar, isTradingEntry ? ModifyModes.INSERT_TRADING_CAL : ModifyModes.INSERT_SETTLEMENT_CAL);
		}


		public void incrementUpdates(Calendar calendar, boolean isTradingEntry) {
			incrementCounter(calendar, isTradingEntry ? ModifyModes.UPDATE_TRADING_CAL : ModifyModes.UPDATE_SETTLEMENT_CAL);
		}


		public void incrementDeletes(Calendar calendar, boolean isTradingEntry) {
			incrementCounter(calendar, isTradingEntry ? ModifyModes.DELETE_TRADING_CAL : ModifyModes.DELETE_SETTLEMENT_CAL);
		}


		public int getInsertCount(Calendar calendar, boolean isTrading) {
			return getCounterByCalendarAndModifyMode(calendar, isTrading ? ModifyModes.INSERT_TRADING_CAL : ModifyModes.INSERT_SETTLEMENT_CAL);
		}


		public int getUpdateCount(Calendar calendar, boolean isTrading) {
			return getCounterByCalendarAndModifyMode(calendar, isTrading ? ModifyModes.UPDATE_TRADING_CAL : ModifyModes.UPDATE_SETTLEMENT_CAL);
		}


		public int getDeleteCount(Calendar calendar, boolean isTrading) {
			return getCounterByCalendarAndModifyMode(calendar, isTrading ? ModifyModes.DELETE_TRADING_CAL : ModifyModes.DELETE_SETTLEMENT_CAL);
		}


		public int getTotalInsertCount(boolean isTrading) {
			return getSum(isTrading ? ModifyModes.INSERT_TRADING_CAL : ModifyModes.INSERT_SETTLEMENT_CAL);
		}


		public int getTotalUpdateCount(boolean isTrading) {
			return getSum(isTrading ? ModifyModes.UPDATE_TRADING_CAL : ModifyModes.UPDATE_SETTLEMENT_CAL);
		}


		public int getTotalDeleteCount(boolean isTrading) {
			return getSum(isTrading ? ModifyModes.DELETE_TRADING_CAL : ModifyModes.DELETE_SETTLEMENT_CAL);
		}


		public int getTotalCalendarCount() {
			return CollectionUtils.getSize(getStatsByCalendarMap().keySet());
		}


		public Set<Calendar> getCalendarSet() {
			return getStatsByCalendarMap().keySet();
		}

		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////


		private void incrementCounter(Calendar calendar, ModifyModes modifyMode) {
			Map<ModifyModes, Integer> statisticsByModifyMode = getStatsByCalendarMap().computeIfAbsent(calendar, k -> new EnumMap<>(ModifyModes.class));
			statisticsByModifyMode.compute(modifyMode, (key, current) -> current == null ? 1 : current + 1);
		}


		private int getCounterByCalendarAndModifyMode(Calendar calendar, ModifyModes modifyMode) {
			Map<ModifyModes, Integer> statisticsByModifyMode = getStatsByCalendarMap().get(calendar);
			if (statisticsByModifyMode == null) {
				return 0;
			}
			Integer counter = statisticsByModifyMode.get(modifyMode);
			return counter == null ? 0 : counter;
		}


		private int getSum(ModifyModes modifyMode) {
			int sum = 0;
			for (Map<ModifyModes, Integer> statsMap : CollectionUtils.getIterable(getStatsByCalendarMap().values())) {
				Integer stat = statsMap.get(modifyMode);
				sum += (stat != null ? stat : 0);
			}
			return sum;
		}

		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////


		private Map<Calendar, Map<ModifyModes, Integer>> getStatsByCalendarMap() {
			return this.statsByCalendarMap;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getCalendarSourceFileDirectoryPath() {
		return this.calendarSourceFileDirectoryPath;
	}


	public void setCalendarSourceFileDirectoryPath(String calendarSourceFileDirectoryPath) {
		this.calendarSourceFileDirectoryPath = calendarSourceFileDirectoryPath;
	}


	public String getCalendarSourceFileName() {
		return this.calendarSourceFileName;
	}


	public void setCalendarSourceFileName(String calendarSourceFileName) {
		this.calendarSourceFileName = calendarSourceFileName;
	}


	public String getMicCodeMappingFileDirectoryPath() {
		return this.micCodeMappingFileDirectoryPath;
	}


	public void setMicCodeMappingFileDirectoryPath(String micCodeMappingFileDirectoryPath) {
		this.micCodeMappingFileDirectoryPath = micCodeMappingFileDirectoryPath;
	}


	public String getMicCodeMappingFileName() {
		return this.micCodeMappingFileName;
	}


	public void setMicCodeMappingFileName(String micCodeMappingFileName) {
		this.micCodeMappingFileName = micCodeMappingFileName;
	}


	public String getCalendarDescriptionMappingFileDirectoryPath() {
		return this.calendarDescriptionMappingFileDirectoryPath;
	}


	public void setCalendarDescriptionMappingFileDirectoryPath(String calendarDescriptionMappingFileDirectoryPath) {
		this.calendarDescriptionMappingFileDirectoryPath = calendarDescriptionMappingFileDirectoryPath;
	}


	public String getCalendarDescriptionMappingFileName() {
		return this.calendarDescriptionMappingFileName;
	}


	public void setCalendarDescriptionMappingFileName(String calendarDescriptionMappingFileName) {
		this.calendarDescriptionMappingFileName = calendarDescriptionMappingFileName;
	}


	public String getArchiveFilePath() {
		return this.archiveFilePath;
	}


	public void setArchiveFilePath(String archiveFilePath) {
		this.archiveFilePath = archiveFilePath;
	}


	public boolean isDeleteFileAfterProcessed() {
		return this.deleteFileAfterProcessed;
	}


	public void setDeleteFileAfterProcessed(boolean deleteFileAfterProcessed) {
		this.deleteFileAfterProcessed = deleteFileAfterProcessed;
	}


	public String getCalendarSourceFileNameDatePattern() {
		return this.calendarSourceFileNameDatePattern;
	}


	public void setCalendarSourceFileNameDatePattern(String calendarSourceFileNameDatePattern) {
		this.calendarSourceFileNameDatePattern = calendarSourceFileNameDatePattern;
	}


	public String getCalendarSourceFileNameDateFormat() {
		return this.calendarSourceFileNameDateFormat;
	}


	public void setCalendarSourceFileNameDateFormat(String calendarSourceFileNameDateFormat) {
		this.calendarSourceFileNameDateFormat = calendarSourceFileNameDateFormat;
	}


	public String getCsvCalendarSourceFileDelimiterCharacter() {
		return this.csvCalendarSourceFileDelimiterCharacter;
	}


	public void setCsvCalendarSourceFileDelimiterCharacter(String csvCalendarSourceFileDelimiterCharacter) {
		this.csvCalendarSourceFileDelimiterCharacter = csvCalendarSourceFileDelimiterCharacter;
	}


	public String getCsvCalendarSourceFileSecondaryDelimiterCharacter() {
		return this.csvCalendarSourceFileSecondaryDelimiterCharacter;
	}


	public void setCsvCalendarSourceFileSecondaryDelimiterCharacter(String csvCalendarSourceFileSecondaryDelimiterCharacter) {
		this.csvCalendarSourceFileSecondaryDelimiterCharacter = csvCalendarSourceFileSecondaryDelimiterCharacter;
	}


	public String getCsvMicFileDelimiterCharacter() {
		return this.csvMicFileDelimiterCharacter;
	}


	public void setCsvMicFileDelimiterCharacter(String csvMicFileDelimiterCharacter) {
		this.csvMicFileDelimiterCharacter = csvMicFileDelimiterCharacter;
	}


	public String getCsvCalendarDescriptionFileDelimiterCharacter() {
		return this.csvCalendarDescriptionFileDelimiterCharacter;
	}


	public void setCsvCalendarDescriptionFileDelimiterCharacter(String csvCalendarDescriptionFileDelimiterCharacter) {
		this.csvCalendarDescriptionFileDelimiterCharacter = csvCalendarDescriptionFileDelimiterCharacter;
	}


	public boolean isNoCalendarSourceFileHeaderRow() {
		return this.noCalendarSourceFileHeaderRow;
	}


	public void setNoCalendarSourceFileHeaderRow(boolean noCalendarSourceFileHeaderRow) {
		this.noCalendarSourceFileHeaderRow = noCalendarSourceFileHeaderRow;
	}


	public boolean isNoMicMappingFileHeaderRow() {
		return this.noMicMappingFileHeaderRow;
	}


	public void setNoMicMappingFileHeaderRow(boolean noMicMappingFileHeaderRow) {
		this.noMicMappingFileHeaderRow = noMicMappingFileHeaderRow;
	}


	public boolean isNoCalendarDescriptionFileHeaderRow() {
		return this.noCalendarDescriptionFileHeaderRow;
	}


	public void setNoCalendarDescriptionFileHeaderRow(boolean noCalendarDescriptionFileHeaderRow) {
		this.noCalendarDescriptionFileHeaderRow = noCalendarDescriptionFileHeaderRow;
	}


	public String[] getTradingCalendarTypeKeys() {
		return this.tradingCalendarTypeKeys;
	}


	public void setTradingCalendarTypeKeys(String[] tradingCalendarTypeKeys) {
		this.tradingCalendarTypeKeys = tradingCalendarTypeKeys;
	}


	public String[] getSettlementCalendarTypeKeys() {
		return this.settlementCalendarTypeKeys;
	}


	public void setSettlementCalendarTypeKeys(String[] settlementCalendarTypeKeys) {
		this.settlementCalendarTypeKeys = settlementCalendarTypeKeys;
	}


	public String[] getBankingCalendarTypeKeys() {
		return this.bankingCalendarTypeKeys;
	}


	public void setBankingCalendarTypeKeys(String[] bankingCalendarTypeKeys) {
		this.bankingCalendarTypeKeys = bankingCalendarTypeKeys;
	}


	public Date getProcessingDate() {
		return this.processingDate;
	}


	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}


	public Integer getMaxStatusMessageCount() {
		return this.maxStatusMessageCount;
	}


	public void setMaxStatusMessageCount(Integer maxStatusMessageCount) {
		this.maxStatusMessageCount = maxStatusMessageCount;
	}


	public Integer getMaxDaysBackForUpdates() {
		return this.maxDaysBackForUpdates;
	}


	public void setMaxDaysBackForUpdates(Integer maxDaysBackForUpdates) {
		this.maxDaysBackForUpdates = maxDaysBackForUpdates;
	}


	public Integer getMaxDaysBackForDeletions() {
		return this.maxDaysBackForDeletions;
	}


	public void setMaxDaysBackForDeletions(Integer maxDaysBackForDeletions) {
		this.maxDaysBackForDeletions = maxDaysBackForDeletions;
	}


	public Integer getMaxDaysForwardForUpdates() {
		return this.maxDaysForwardForUpdates;
	}


	public void setMaxDaysForwardForUpdates(Integer maxDaysForwardForUpdates) {
		this.maxDaysForwardForUpdates = maxDaysForwardForUpdates;
	}


	public Integer getMaxDaysForwardForDeletions() {
		return this.maxDaysForwardForDeletions;
	}


	public void setMaxDaysForwardForDeletions(Integer maxDaysForwardForDeletions) {
		this.maxDaysForwardForDeletions = maxDaysForwardForDeletions;
	}


	public StatusHolderObject<Status> getStatusHolderObject() {
		return this.statusHolderObject;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public CalendarHolidayService getCalendarHolidayService() {
		return this.calendarHolidayService;
	}


	public void setCalendarHolidayService(CalendarHolidayService calendarHolidayService) {
		this.calendarHolidayService = calendarHolidayService;
	}
}
