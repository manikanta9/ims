package com.clifton.calendar;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CalendarProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "calendar";
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		HashSet<String> skipMethods = new HashSet<>();
		skipMethods.add("getCalendarBusinessDayList");
		return skipMethods.contains(method.getName());
	}


	@Override
	protected void configureApprovedTestClassImports(@SuppressWarnings("unused") List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("java.beans.Introspector");
		imports.add("org.springframework.beans.");
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
