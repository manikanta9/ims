package com.clifton.calendar.setup;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>CalendarTimeZoneTests</code> class tests CalendarTimeZone methods.
 *
 * @author vgomelsky
 */
public class CalendarTimeZoneTests {

	@Test
	public void testGetLabel() {
		CalendarTimeZone timeZone = new CalendarTimeZone();
		timeZone.setName("123");
		timeZone.setDescription("456");

		Assertions.assertEquals("123 (456) GMT + 0 hours", timeZone.getLabel());

		timeZone.setGmtOffsetMinutes(60);
		Assertions.assertEquals("123 (456) GMT + 1 hours", timeZone.getLabel());

		timeZone.setGmtOffsetMinutes(-120);
		Assertions.assertEquals("123 (456) GMT - 2 hours", timeZone.getLabel());

		timeZone.setGmtOffsetMinutes(150);
		Assertions.assertEquals("123 (456) GMT + 2.5 hours", timeZone.getLabel());

		timeZone.setDstOn(true);
		Assertions.assertEquals("123 (456) GMT + 2.5 hours DST", timeZone.getLabel());
	}
}
