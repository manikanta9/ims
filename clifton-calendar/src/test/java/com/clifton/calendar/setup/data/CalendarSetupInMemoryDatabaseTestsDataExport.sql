SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId
FROM CalendarDay
WHERE CalendarYearID IN (2019, 2020);
