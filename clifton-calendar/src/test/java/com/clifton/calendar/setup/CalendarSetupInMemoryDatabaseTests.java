package com.clifton.calendar.setup;

import com.clifton.calendar.holiday.CalendarHoliday;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.setup.search.CalendarSearchForm;
import com.clifton.core.dataaccess.dao.DaoException;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * These tests are setup as in-memory database tests because we need to use the exists functionality in hibernate in order to support the rollup logic when populating holidays
 * <p>
 * Tests both the {@link CalendarSetupService} and {@link CalendarHolidayService} which appear to have some circular dependencies
 *
 * @author manderson
 */
@ContextConfiguration
@Transactional
public class CalendarSetupInMemoryDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	CalendarHolidayService calendarHolidayService;

	@Resource
	CalendarSetupService calendarSetupService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalendarHolidayDaySave_MissingHolidayFlags() {
		Calendar calendar = new Calendar();
		calendar.setName("Test Calendar");
		calendar.setDescription("Test Calendar");
		calendar = this.calendarSetupService.saveCalendar(calendar);

		CalendarHoliday testHoliday1 = new CalendarHoliday();
		testHoliday1.setName("Test Holiday");
		this.calendarHolidayService.saveCalendarHoliday(testHoliday1);


		CalendarHolidayDay holidayDay = new CalendarHolidayDay();
		holidayDay.setCalendar(calendar);
		holidayDay.setDay(this.calendarSetupService.getCalendarDayByDate(DateUtils.toDate("01/01/2019")));
		holidayDay.setHoliday(testHoliday1);
		TestUtils.expectException(DaoException.class, () -> this.calendarHolidayService.saveCalendarHolidayDay(holidayDay), "A holiday must be at least a trading or settlement holiday (or both), but cannot be neither.  Please check at least one of the Trade or Settlement holiday options.");
	}


	@Test
	public void testRollupCalendarHolidayUpdates() {
		CalendarHoliday testHoliday1 = new CalendarHoliday();
		testHoliday1.setName("Test Holiday 1");
		this.calendarHolidayService.saveCalendarHoliday(testHoliday1);

		CalendarHoliday testHoliday2 = new CalendarHoliday();
		testHoliday2.setName("Test Holiday 2");
		this.calendarHolidayService.saveCalendarHoliday(testHoliday2);


		Calendar childCalendar1 = new Calendar();
		childCalendar1.setName("Child Calendar 1");
		childCalendar1.setDescription(childCalendar1.getName());
		childCalendar1 = this.calendarSetupService.saveCalendar(childCalendar1);
		// Add a Trading Holiday to this calendar for Jan 1, 2020
		populateCalendarHolidayDay(childCalendar1, testHoliday1, DateUtils.toDate("01/01/2020"), true, false, true, true);
		// Add a Trading Partial Day Holiday to this calendar for Dec, 31 2019
		populateCalendarHolidayDay(childCalendar1, testHoliday2, DateUtils.toDate("12/31/2019"), true, false, false, true);

		Calendar childCalendar2 = new Calendar();
		childCalendar2.setName("Child Calendar 2");
		childCalendar2.setDescription(childCalendar2.getName());
		childCalendar2 = this.calendarSetupService.saveCalendar(childCalendar2);
		// Add a Trading and Settlement Holiday to this calendar for Jan 1, 2020
		populateCalendarHolidayDay(childCalendar2, testHoliday2, DateUtils.toDate("01/01/2020"), true, true, true, true);

		Calendar rollupCalendar = new Calendar();
		rollupCalendar.setName("Rollup Calendar");
		rollupCalendar.setDescription("Rollup Calendar");
		rollupCalendar.setRollupCalendar(true);
		rollupCalendar = this.calendarSetupService.saveCalendar(rollupCalendar);

		// No children - validate no holidays
		List<CalendarHolidayDay> parentHolidays = this.calendarHolidayService.getCalendarHolidayDayListByCalendar(rollupCalendar.getId());
		Assertions.assertEquals(0, CollectionUtils.getSize(parentHolidays));

		// Add first calendar as child
		rollupCalendar.setChildCalendars(CollectionUtils.createList(childCalendar1));
		rollupCalendar = this.calendarSetupService.saveCalendar(rollupCalendar);

		// Should have 2 records - same as child calendar 1
		parentHolidays = this.calendarHolidayService.getCalendarHolidayDayListByCalendar(rollupCalendar.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(parentHolidays));
		validateCalendarHolidayDayExists(parentHolidays, testHoliday1, DateUtils.toDate("01/01/2020"), true, false, true);
		validateCalendarHolidayDayExists(parentHolidays, testHoliday2, DateUtils.toDate("12/31/2019"), true, false, false);

		// Add second calendar
		rollupCalendar.setChildCalendars(CollectionUtils.createList(childCalendar1, childCalendar2));
		rollupCalendar = this.calendarSetupService.saveCalendar(rollupCalendar);

		// Should still only have 2 records - same as child calendar 1 + 2 (but both use overlapping date)
		parentHolidays = this.calendarHolidayService.getCalendarHolidayDayListByCalendar(rollupCalendar.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(parentHolidays));
		// Note: Should still be Test Holiday 1 because should use the holiday from the first calendar (ordered by calendar id)
		validateCalendarHolidayDayExists(parentHolidays, testHoliday1, DateUtils.toDate("01/01/2020"), true, true, true);
		validateCalendarHolidayDayExists(parentHolidays, testHoliday2, DateUtils.toDate("12/31/2019"), true, false, false);


		// Remove calendar 1
		rollupCalendar.setChildCalendars(CollectionUtils.createList(childCalendar2));
		rollupCalendar = this.calendarSetupService.saveCalendar(rollupCalendar);

		// Should only have 1 record - same as child calendar 2
		parentHolidays = this.calendarHolidayService.getCalendarHolidayDayListByCalendar(rollupCalendar.getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(parentHolidays));
		// Now should be test holiday 2 because the first calendar was removed as a chile
		validateCalendarHolidayDayExists(parentHolidays, testHoliday2, DateUtils.toDate("01/01/2020"), true, true, true);

		// Delete the rollup calendar
		this.calendarSetupService.deleteCalendar(rollupCalendar.getId());
	}


	@Test
	public void testCalendar_save_and_search_by_mic() {
		Calendar calendar3 = new Calendar();
		calendar3.setName("Test Calendar3");
		calendar3.setDescription("Test Calendar 3");
		calendar3.setMarketIdentifierCode("XCBT");
		calendar3 = this.calendarSetupService.saveCalendar(calendar3);

		Calendar calendar4 = new Calendar();
		calendar4.setName("Test Calendar4");
		calendar4.setDescription("Test Calendar 4");
		calendar4.setMarketIdentifierCode("BATO");
		calendar4 = this.calendarSetupService.saveCalendar(calendar4);

		CalendarSearchForm calendarSearchForm3 = new CalendarSearchForm();
		calendarSearchForm3.setMarketIdentifierCode("XCBT");
		List<Calendar> calendarList3 = this.calendarSetupService.getCalendarList(calendarSearchForm3);
		Assertions.assertEquals(1, calendarList3.size());
		Assertions.assertEquals(calendar3.getId(), calendarList3.get(0).getId());

		CalendarSearchForm calendarSearchForm4 = new CalendarSearchForm();
		calendarSearchForm4.setMarketIdentifierCode("BATO");
		List<Calendar> calendarList4 = this.calendarSetupService.getCalendarList(calendarSearchForm4);
		Assertions.assertEquals(1, calendarList4.size());
		Assertions.assertEquals(calendar4.getId(), calendarList4.get(0).getId());

		// non existent entry
		CalendarSearchForm calendarSearchForm5 = new CalendarSearchForm();
		calendarSearchForm5.setMarketIdentifierCode("XBDA");
		List<Calendar> calendarList5 = this.calendarSetupService.getCalendarList(calendarSearchForm5);
		Assertions.assertEquals(0, calendarList5.size());


		// Lookup by defined code
		CalendarSearchForm calendarSearchForm6 = new CalendarSearchForm();
		calendarSearchForm4.setMarketIdentifierCodeNotNull(true);
		Set<Short> expectedIds = new HashSet<>();
		expectedIds.add(calendar3.getId());
		expectedIds.add(calendar4.getId());

		List<Calendar> calendarList6 = this.calendarSetupService.getCalendarList(calendarSearchForm6);
		Assertions.assertEquals(2, calendarList6.size());
		Assertions.assertTrue(expectedIds.contains(calendarList6.get(0).getId()));
		Assertions.assertTrue(expectedIds.contains(calendarList6.get(1).getId()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarHolidayDay populateCalendarHolidayDay(Calendar calendar, CalendarHoliday holiday, Date date, boolean trading, boolean settlement, boolean fullDay, boolean save) {
		CalendarHolidayDay holidayDay = new CalendarHolidayDay();
		holidayDay.setCalendar(calendar);
		holidayDay.setDay(this.calendarSetupService.getCalendarDayByDate(date));
		holidayDay.setHoliday(holiday);
		holidayDay.setFullDayHoliday(fullDay);
		holidayDay.setTradeHoliday(trading);
		holidayDay.setSettlementHoliday(settlement);
		if (save) {
			return this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		}
		return holidayDay;
	}


	private void validateCalendarHolidayDayExists(List<CalendarHolidayDay> list, CalendarHoliday holiday, Date date, boolean trading, boolean settlement, boolean fullDay) {
		boolean found = false;
		for (CalendarHolidayDay holidayDay : list) {
			if (DateUtils.isEqualWithoutTime(date, holidayDay.getDay().getStartDate())) {
				found = true;
				Assertions.assertEquals(holiday, holidayDay.getHoliday());
				Assertions.assertEquals(trading, holidayDay.isTradeHoliday());
				Assertions.assertEquals(settlement, holidayDay.isSettlementHoliday());
				Assertions.assertEquals(fullDay, holidayDay.isFullDayHoliday());
				break;
			}
		}

		Assertions.assertTrue(found, "Did not find parent holiday day for date " + DateUtils.fromDateShort(date));
	}
}
