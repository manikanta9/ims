package com.clifton.calendar.setup;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class CalendarSetupServiceImplTests {

	@Resource
	private CalendarSetupService calendarSetupService;
	@Resource
	private SecurityAuthorizationService securityAuthorizationService;


	@Test
	public void testSaveCalendarForInvalidYearBefore() {
		Assertions.assertThrows(ValidationException.class, () -> {
			short yr = 1899;
			this.calendarSetupService.saveCalendarYearForYear(yr);
		});
	}


	@Test
	public void testSaveCalendarForInvalidYearAfter() {
		Assertions.assertThrows(ValidationException.class, () -> {
			short yr = 3000;
			this.calendarSetupService.saveCalendarYearForYear(yr);
		});
	}


	@Test
	public void testSaveCalendarForYear() {
		// Generate Calendar entries for year 2012
		short yr = 2012;
		this.calendarSetupService.saveCalendarYearForYear(yr);

		// Validate Year properties
		CalendarYear year = this.calendarSetupService.getCalendarYear(yr);
		Assertions.assertTrue(year.isLeapYear());
		Assertions.assertEquals(366, year.getDaysInYear());

		List<CalendarDay> days = this.calendarSetupService.getCalendarDayListByYear(yr);
		Assertions.assertEquals(366, days.size());

		// Spot checking Days
		// Check 01/01/2012, 04/14/2012, 02/29/2012, 12/31/2012
		int foundCount = 0;
		String foundString = "";
		int currentId = 0;
		for (CalendarDay day : days) {
			if ("Sun Jan 1, 2012".equals(day.getLabel())) {
				foundCount++;
				foundString += day.getLabel() + "; ";
				currentId = day.getId();
			}
			else {
				currentId++;
			}

			// Ensure all IDs are being properly set.
			Assertions.assertEquals((Integer) currentId, day.getId());
			Assertions.assertEquals(DateUtils.getDaysSinceNaturalStart(day.getStartDate()), currentId);
			if ("Sat Apr 14, 2012".equals(day.getLabel())) {
				foundCount++;
				foundString += day.getLabel() + "; ";
			}
			else if ("Wed Feb 29, 2012".equals(day.getLabel())) {
				foundCount++;
				foundString += day.getLabel() + "; ";
			}
			else if ("Mon Dec 31, 2012".equals(day.getLabel())) {
				foundCount++;
				foundString += day.getLabel() + "; ";
			}
		}
		Assertions.assertEquals(4, foundCount, "Did not find all 4 days in calendar.  Found[" + foundString + "]");
	}


	@Test
	public void testSaveCalendarForYearExists() {
		Assertions.assertThrows(Exception.class, () -> {
			// Year 2009 already has dates set up - should throw an exception
			short yr = 2009;
			this.calendarSetupService.saveCalendarYearForYear(yr);
		});
	}


	@Test
	public void testGetCalendarDayByDate() {
		Date date = DateUtils.toDate("12/28/2008");
		CalendarDay day = this.calendarSetupService.getCalendarDayByDate(date);

		Assertions.assertNotNull(day);
		Assertions.assertEquals("Sun Dec 28, 2008", day.getLabel());
	}


	@Test
	public void testSaveCalendar() {
		Calendar validCalendar = new Calendar();
		validCalendar.setName("validCalendar");
		validCalendar.setDescription("description");
		validCalendar.setSystemDefined(false);

		this.calendarSetupService.saveCalendar(validCalendar);
	}


	@Test
	public void testInsertSystemDefinedCalendar() {
		Assertions.assertThrows(ValidationException.class, () -> {
			Calendar validCalendar = new Calendar();
			validCalendar.setName("validCalendar");
			validCalendar.setDescription("description");
			validCalendar.setSystemDefined(true);

			this.calendarSetupService.saveCalendar(validCalendar);
		});
	}


	@Test
	public void testUpdateSystemDefinedCalendar() {
		testUpdateSystemDefinedCalendarWithName("System Defined Calendar");
	}


	private void testUpdateSystemDefinedCalendarWithName(String name) {
		Calendar existingCalendar = this.calendarSetupService.getCalendarByName("System Defined Calendar");
		existingCalendar.setName(name);
		existingCalendar.setDescription("description");
		existingCalendar.setSystemDefined(true);

		this.calendarSetupService.saveCalendar(existingCalendar);
	}


	@Test
	public void testUpdateSystemDefinedCalendarNameChange() {
		Assertions.assertThrows(ValidationException.class, () -> testUpdateSystemDefinedCalendarWithName("Different Name"));
	}


	@Test
	public void testSetSystemDefinedAttribute() {
		Assertions.assertThrows(ValidationException.class, () -> {
			Calendar existingCalendar = this.calendarSetupService.getCalendarByName("System Defined Calendar");
			existingCalendar.setSystemDefined(false);

			this.calendarSetupService.saveCalendar(existingCalendar);
		});
	}


	@Test
	public void testDeleteSystemDefinedCalendar() {
		Assertions.assertThrows(ValidationException.class, () -> {
			Calendar existingCalendar = this.calendarSetupService.getCalendarByName("System Defined Calendar");
			short id = existingCalendar.getId();
			this.calendarSetupService.deleteCalendar(id);
		});
	}


	@Test
	public void testEntityModifyConditionCalendar() {
		TestUtils.expectException(ValidationException.class, () -> {
			setupTestAsAdmin(false);
			Calendar calendar = this.calendarSetupService.getCalendarByName("Entity Modify Condition Calendar");
			calendar.setDescription("This should not save");
			this.calendarSetupService.saveCalendar(calendar);
		}, "You do not have permission to UPDATE this Calendar because: FALSE");
	}


	private void setupTestAsAdmin(boolean isAdmin) {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(isAdmin);
	}
}
