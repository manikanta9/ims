SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId FROM CalendarDay WHERE CalendarYearID BETWEEN 2019 AND 2023
UNION
SELECT 'CalendarHolidayDay' AS entityTableName, CalendarHolidayDayID as entityId FROM CalendarHolidayDay WHERE CalendarID = (SELECT CalendarID FROM Calendar WHERE CalendarName = 'Athens Stock Exchange')
UNION
SELECT 'CalendarHoliday' AS entityTableName, CalendarHolidayID as entityID FROM CalendarHoliday;
