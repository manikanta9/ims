package com.clifton.calendar.holiday;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.CalendarTestServiceHolder;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 *
 */
public class CalendarBusinessDayTests {

	private CalendarTestServiceHolder scheduleServiceHolder;


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		if (this.scheduleServiceHolder != null) {
			return this.scheduleServiceHolder.getCalendarBusinessDayService();
		}
		return null;
	}


	@BeforeEach
	public void setup() {
		if (this.scheduleServiceHolder == null) {
			this.scheduleServiceHolder = CalendarTestObjectFactory.createCalendarsAndServices(DateUtils.toDate("01/01/2010"), DateUtils.toDate("12/31/2015"));
		}
	}


	@Test
	public void getBusinessDayFromTest() {
		Date date = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("2015-03-01 02:00:00", DateUtils.DATE_FORMAT_FULL), CalendarTestObjectFactory.CALENDAR_NO_HOLIDAY), 10);
		Assertions.assertEquals("03/13/2015 2:00 AM", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SHORT));


		date = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("2015-03-01 02:00:00", DateUtils.DATE_FORMAT_FULL), CalendarTestObjectFactory.CALENDAR_NO_HOLIDAY), 200);
		Assertions.assertEquals("12/04/2015 2:00 AM", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SHORT));
	}
}
