package com.clifton.calendar.holiday;

import com.clifton.calendar.jobs.SwapsMonitorCalendarHolidayLoaderJob;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarDay;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.status.StatusHolderObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.HashMap;


/**
 * The CalendarHolidayLoaderJobTests test the basic calendar update logic for the CalendarHolidayLoaderJob.
 *
 * @author davidi
 */

@ContextConfiguration
public class SwapsMonitorCalendarHolidayLoaderJobTests extends BaseInMemoryDatabaseTests {

	@Resource
	private CalendarHolidayService calendarHolidayService;

	@Resource
	private CalendarSetupService calendarSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Removes trading and settlement holidays and adds invalid holidays prior to executing the batch job code, which should correct these changes.
	 */
	private void reconfigureHolidayDayCalendar() {
		Calendar calendar = this.calendarSetupService.getCalendarByName("Athens Stock Exchange");
		calendar.setMarketIdentifierCode("XATH");
		this.calendarSetupService.saveCalendar(calendar);

		// Remove 12/26/2022
		CalendarHolidayDay holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(calendar.getId(), DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("12/26/2022")));
		this.calendarHolidayService.deleteCalendarHolidayDay(holidayDay.getId());
		checkForHoliday("12/26/2022", false, false, false);

		// Remove 03/25/2021, settlement holiday only
		holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(calendar.getId(), DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("03/25/2021")));
		holidayDay.setTradeHoliday(false);
		this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		checkForHoliday("03/25/2021", false, true, true);

		// Remove 04/02/2021, trading holiday only
		holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(calendar.getId(), DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("04/02/2021")));
		holidayDay.setSettlementHoliday(false);
		this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		checkForHoliday("04/02/2021", true, false, true);

		// Add holidays that should not exist (in processing range)
		holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(calendar.getId(), DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("01/01/2018")));
		CalendarDay newDay = this.calendarSetupService.getCalendarDayByDate(DateUtils.toDate("05/10/2021"));
		holidayDay.setDay(newDay);
		this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		checkForHoliday("05/10/2021", true, true, true);

		// Add holidays that should not exist (out of processing range)
		holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(calendar.getId(), DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("01/01/2019")));
		newDay = this.calendarSetupService.getCalendarDayByDate(DateUtils.toDate("05/10/2020"));
		holidayDay.setDay(newDay);
		this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		checkForHoliday("05/10/2020", true, true, true);

		// Add holidays that should not exist (out of processing range)
		holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(calendar.getId(), DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("01/01/2024")));
		newDay = this.calendarSetupService.getCalendarDayByDate(DateUtils.toDate("05/10/2023"));
		holidayDay.setDay(newDay);
		this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		checkForHoliday("05/10/2023", true, true, true);

		SwapsMonitorCalendarHolidayLoaderJob calendarHolidayLoaderJob = createBatchJob();
		calendarHolidayLoaderJob.setMaxDaysBackForDeletions(365);
		calendarHolidayLoaderJob.setMaxDaysBackForUpdates(365);
		calendarHolidayLoaderJob.setMaxDaysForwardForDeletions(365);
		calendarHolidayLoaderJob.setMaxDaysForwardForUpdates(365);
		calendarHolidayLoaderJob.setProcessingDate(DateUtils.toDate("01/02/2022", "MM/dd/yyyy"));


		try {
			Status status = calendarHolidayLoaderJob.run(new HashMap<>());
			if (!CollectionUtils.isEmpty(status.getErrorList())) {
				StringBuilder errorStringBuilder = new StringBuilder();
				for (StatusDetail statusDetail : CollectionUtils.getIterable(status.getErrorList())) {
					errorStringBuilder.append(statusDetail.toString());
					errorStringBuilder.append("; ");
				}
				Assertions.fail(errorStringBuilder.toString());
			}
		}
		catch (Exception exc) {
			Assertions.fail(exc.getMessage());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void calendarHolidayLoaderJobTests() {
		reconfigureHolidayDayCalendar();

		testBatchJobMissingHolidayAdded_12_26_2022();
		testBatchJobMissingTradingHolidayAdded_03_25_2021();
		testBatchJobMissingSettlementHolidayAdded_04_02_2021();
		testBatchJobInvalidHolidayRemoved_05_10_2021_in_Range();
		testBatchJobInvalidHolidayNotRemoved_05_10_2023_out_of_range();
		testBatchJobInvalidHolidayNotRemoved_05_10_2020_out_of_range();
	}


	private void testBatchJobMissingHolidayAdded_12_26_2022() {
		checkForHoliday("12/26/2022", true, true, true);
	}


	private void testBatchJobMissingTradingHolidayAdded_03_25_2021() {
		checkForHoliday("03/25/2021", true, true, true);
	}


	private void testBatchJobMissingSettlementHolidayAdded_04_02_2021() {
		checkForHoliday("04/02/2021", true, true, true);
	}


	private void testBatchJobInvalidHolidayRemoved_05_10_2021_in_Range() {
		checkForHoliday("05/10/2021", true, true, false);
	}


	private void testBatchJobInvalidHolidayNotRemoved_05_10_2023_out_of_range() {
		checkForHoliday("05/10/2023", true, true, true);
	}


	private void testBatchJobInvalidHolidayNotRemoved_05_10_2020_out_of_range() {
		checkForHoliday("05/10/2020", true, true, true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SwapsMonitorCalendarHolidayLoaderJob createBatchJob() {
		SwapsMonitorCalendarHolidayLoaderJob calendarHolidayLoaderJob = BeanUtils.instantiateClass(SwapsMonitorCalendarHolidayLoaderJob.class);

		calendarHolidayLoaderJob.setCalendarHolidayService(this.calendarHolidayService);
		calendarHolidayLoaderJob.setCalendarSetupService(this.calendarSetupService);

		try {
			String calendarHolidaySourceDirectory = new ClassPathResource("com/clifton/calendar/holiday/data/files").getFile().getAbsolutePath();
			Assertions.assertFalse(StringUtils.isEmpty(calendarHolidaySourceDirectory));
			calendarHolidayLoaderJob.setCalendarSourceFileDirectoryPath(calendarHolidaySourceDirectory);
			calendarHolidayLoaderJob.setCalendarSourceFileName("holidays.dat");
			calendarHolidayLoaderJob.setMicCodeMappingFileName("MIC_Mapping.csv");
			calendarHolidayLoaderJob.setCalendarDescriptionMappingFileName("holidays.des");
			calendarHolidayLoaderJob.setNoCalendarSourceFileHeaderRow(true);

			calendarHolidayLoaderJob.setProcessingDate(DateUtils.toDate("01/02/2022", "MM/dd/yyyy"));
			calendarHolidayLoaderJob.setStatusHolderObject(new StatusHolder());
			return calendarHolidayLoaderJob;
		}
		catch (Exception exc) {
			throw new RuntimeException(exc);
		}
	}


	void checkForHoliday(String dateStr, boolean isTrading, boolean isSettlement, boolean exists) {
		Calendar calendar = this.calendarSetupService.getCalendarByName("Athens Stock Exchange");
		CalendarHolidayDay holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(calendar.getId(), DateUtils.getDaysSinceNaturalStart(DateUtils.toDate(dateStr)));

		if (exists) {
			Assertions.assertNotNull(holidayDay, "Cannot find holiday day for date: " + dateStr);
			Assertions.assertTrue(holidayDay.isTradeHoliday() == isTrading && holidayDay.isSettlementHoliday() == isSettlement, "Missing trading / settlement holiday day for date " + dateStr);
		}
		else {
			Assertions.assertNull(holidayDay, "Calendar holiday entry did not get deleted for date: " + dateStr);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static class StatusHolder implements StatusHolderObject<Status> {

		private final Status status = new Status();


		@Override
		public Status getStatus() {
			return this.status;
		}
	}
}
