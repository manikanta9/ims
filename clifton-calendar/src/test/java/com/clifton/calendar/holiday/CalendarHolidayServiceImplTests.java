package com.clifton.calendar.holiday;

import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>CalendarHolidayServiceImplTests</code> ...
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class CalendarHolidayServiceImplTests {

	private static final String US_CALENDAR_NAME = "US Bank Holidays";
	private static final String LONDON_CALENDAR_NAME = "London Holidays";
	private static final short ENTITY_MODIFY_CONDITION_CALENDAR = 3;

	@Resource
	private CalendarSetupService calendarSetupService;
	@Resource
	private CalendarHolidayService calendarHolidayService;
	@Resource
	private SecurityAuthorizationService securityAuthorizationService;


	@Test
	public void testLinkCalendarHoliday() {
		// Test Linking Holiday to Date that Exists in the System
		Calendar londonCalendar = this.calendarSetupService.getCalendarByName(LONDON_CALENDAR_NAME);
		CalendarHoliday newYearsDayHoliday = this.calendarHolidayService.getCalendarHolidayByName("New Year's Day");
		this.calendarHolidayService.linkCalendarHolidayToDay(londonCalendar.getId(), newYearsDayHoliday.getId(), DateUtils.toDate("01/01/2009"));

		// Test Linking Holiday to Date that does NOT exist in the System (Needs to add all the dates of year 2010)
		Calendar usCalendar = this.calendarSetupService.getCalendarByName(US_CALENDAR_NAME);
		CalendarHoliday laborDayHoliday = this.calendarHolidayService.getCalendarHolidayByName("Labor Day");
		this.calendarHolidayService.linkCalendarHolidayToDay(usCalendar.getId(), laborDayHoliday.getId(), DateUtils.toDate("09/06/2010"));

		List<CalendarHolidayDay> holidayDayList = this.calendarHolidayService.getCalendarHolidayDayListByCalendar(usCalendar.getId());
		Assertions.assertEquals(6, CollectionUtils.getSize(holidayDayList));

		List<CalendarHolidayDay> namedHolidayDayList = CollectionUtils.getFiltered(holidayDayList, holidayDay -> !holidayDay.getHoliday().getName().startsWith("Test Holiday "));
		Assertions.assertEquals(3, CollectionUtils.getSize(namedHolidayDayList));
		Assertions.assertEquals(1, CollectionUtils.getFiltered(namedHolidayDayList, holidayDay -> "New Year's Day".equals(holidayDay.getHoliday().getName())).size());
		Assertions.assertEquals(2, CollectionUtils.getFiltered(namedHolidayDayList, holidayDay -> "Labor Day".equals(holidayDay.getHoliday().getName())).size());

		List<CalendarHolidayDay> newYearsDayHolidayDayList = this.calendarHolidayService.getCalendarHolidayDayListByHoliday(newYearsDayHoliday.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(newYearsDayHolidayDayList));
		Assertions.assertTrue(CollectionUtils.anyMatch(newYearsDayHolidayDayList, holidayDay -> usCalendar.equals(holidayDay.getCalendar())));
		Assertions.assertTrue(CollectionUtils.anyMatch(newYearsDayHolidayDayList, holidayDay -> londonCalendar.equals(holidayDay.getCalendar())));
		Assertions.assertTrue(CollectionUtils.allMatch(newYearsDayHolidayDayList, holidayDay -> "Thu Jan 1, 2009".equals(holidayDay.getDay().getLabel())));

		// Remove the generated London New Year's holiday so that it won't affect other tests
		List<CalendarHolidayDay> londonNewYearsHolidayDayList = CollectionUtils.getFiltered(newYearsDayHolidayDayList, holidayDay -> londonCalendar.equals(holidayDay.getCalendar()));
		Assertions.assertEquals(1, londonNewYearsHolidayDayList.size());
		CalendarHolidayDay londonNewYearsHolidayDay = CollectionUtils.getOnlyElementStrict(londonNewYearsHolidayDayList);
		this.calendarHolidayService.deleteCalendarHolidayDay(londonNewYearsHolidayDay.getId());
	}


	@Test
	public void testLinkCalendarHolidayEntityModifyCondition() {
		setupTestAsAdmin(false);
		// Calendar is set up with entityModifyCondition, which CalendarHolidayDay relies on
		Calendar usCalendar = this.calendarSetupService.getCalendarByName(US_CALENDAR_NAME);
		TestUtils.expectException(ValidationException.class, () -> this.calendarHolidayService.linkCalendarHolidayToDay(ENTITY_MODIFY_CONDITION_CALENDAR, usCalendar.getId(), DateUtils.toDate("01/01/2009")), "You do not have permission to INSERT this CalendarHolidayDay because: FALSE");
	}


	private void setupTestAsAdmin(boolean isAdmin) {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(isAdmin);
	}
}
