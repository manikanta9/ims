package com.clifton.calendar.holiday;

import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarDay;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author NickK
 */
@ContextConfiguration
@Transactional
public class CalendarBusinessDayServiceInMemoryDatabaseTests extends BaseInMemoryDatabaseTests {

	private static final short HONG_KONG_CALENDAR_ID = 88;
	private static final short US_BANKS_CALENDAR_ID = 2;

	private static final short MARKET_HOLIDAY_ID = 12;

	@Resource
	CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	CalendarHolidayService calendarHolidayService;

	@Resource
	CalendarSetupService calendarSetupService;


	@Test
	public void testGetBusinessDayFrom_MultipleCalendars() {
		Calendar usBanksCalendar = this.calendarSetupService.getCalendar(US_BANKS_CALENDAR_ID);
		Calendar hongKongCalendar = this.calendarSetupService.getCalendar(HONG_KONG_CALENDAR_ID);
		Calendar defaultCalendar = this.calendarSetupService.getDefaultCalendar(); // should be CalendarID 4 "System Calendar"

		// Similar test for settlement date calculation for trading currency HKD with USD base currency on Aug. 31, 2017 and Dec. 21, 2017 respectively
		validateBusinessDateFrom("09/05/2017", "08/31/2017", 2, usBanksCalendar, hongKongCalendar);
		validateBusinessDateFrom("12/27/2017", "12/21/2017", 2, usBanksCalendar, hongKongCalendar);
		// Similar test for settlement date calculation for trading currency EUR with USD base currency on Aug. 31, 2017 and Dec. 21, 2017 respectively
		validateBusinessDateFrom("09/05/2017", "08/31/2017", 2, usBanksCalendar, defaultCalendar);
		validateBusinessDateFrom("12/26/2017", "12/21/2017", 2, usBanksCalendar, defaultCalendar);
		// Test the reverse of each of the above
		validateBusinessDateFrom("08/31/2017", "09/05/2017", -2, usBanksCalendar, hongKongCalendar);
		validateBusinessDateFrom("12/21/2017", "12/27/2017", -2, usBanksCalendar, hongKongCalendar);
		validateBusinessDateFrom("08/31/2017", "09/05/2017", -2, usBanksCalendar, defaultCalendar);
		validateBusinessDateFrom("12/21/2017", "12/26/2017", -2, usBanksCalendar, defaultCalendar);
	}


	@Test
	public void testGetBusinessDayFrom_MultipleCalendarsNullChecking() {
		validateBusinessDateFrom("09/05/2017", "08/31/2017", 2);
		validateBusinessDateFrom("12/26/2017", "12/21/2017", 2, (Calendar[]) null);
		validateBusinessDateFrom("09/05/2017", "08/31/2017", 2, new Calendar[]{null});
		validateBusinessDateFrom("09/05/2017", "08/31/2017", 2, new Calendar());
		Calendar calendar = new Calendar();
		calendar.setId(US_BANKS_CALENDAR_ID);
		validateBusinessDateFrom("09/05/2017", "08/31/2017", 2, calendar);
	}


	@Test
	public void testGetBusinessDayFrom_MultipleCalendarsNoOverlappingBusinessDays() {
		Calendar calendar1 = createCalendarWithSingleBusinessDay("Calendar with January 3 Business Day", 3);
		Calendar calendar2 = createCalendarWithSingleBusinessDay("Calendar with January 4 Business Day", 4);

		TestUtils.expectException(RuntimeException.class,
				() -> validateBusinessDateFrom("01/01/2017", "01/01/2017", 1, calendar1, calendar2),
				"Failed to find a matching Business Day for the provided Calendars within 500 days after the days from conversion.");
	}


	private void validateBusinessDateFrom(String expectedDate, String startDate, int days, Calendar... calendars) {
		Assertions.assertEquals(DateUtils.toDate(expectedDate), this.calendarBusinessDayService.getBusinessDayFrom(DateUtils.toDate(startDate), days, null, calendars));
	}


	private Calendar createCalendarWithSingleBusinessDay(String calendarName, int businessDay) {
		Calendar calendar = new Calendar();
		calendar.setName(calendarName);
		calendar.setDescription("I'm one of the best test calendars around!");
		this.calendarSetupService.saveCalendar(calendar);

		createHolidayDaysForCalendarForYear(calendar, (short) 2017, businessDay);
		createHolidayDaysForCalendarForYear(calendar, (short) 2018, businessDay);
		return calendar;
	}


	private void createHolidayDaysForCalendarForYear(Calendar calendar, short year, int businessDay) {
		CalendarHoliday holiday = this.calendarHolidayService.getCalendarHoliday(MARKET_HOLIDAY_ID);
		List<CalendarDay> yearDayList = this.calendarSetupService.getCalendarDayListByYear(year);
		yearDayList.forEach(day -> {
			if (day.getDayNumberInYear() != businessDay) {
				CalendarHolidayDay holidayDay = new CalendarHolidayDay();
				holidayDay.setCalendar(calendar);
				holidayDay.setDay(day);
				holidayDay.setHoliday(holiday);
				holidayDay.setTradeHoliday(true);
				holidayDay.setSettlementHoliday(true);
				holidayDay.setFullDayHoliday(true);
				this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
			}
		});
	}
}
