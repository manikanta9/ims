package com.clifton.calendar.holiday;

import com.clifton.calendar.holiday.search.CalendarBusinessDaySearchForm;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


/**
 * The <code>CalendarBusinessDayServiceImplTests</code> ...
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = "CalendarHolidayServiceImplTests-context.xml")
public class CalendarBusinessDayServiceImplTests {

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private CalendarHolidayService calendarHolidayService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static final short US_CALENDAR_ID = 1;
	private static final short LONDON_CALENDAR_ID = 2;
	private static final short BUSINESS_DAY_CALENDAR_ID = 4;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Default logic sets all holidays as full holidays - so in order to test trade or settlement or partial day holidays we need to override some of the attributes
	 * called explicitly before any test that would actually need this calendar
	 */
	protected void reconfigureBusinessDayCalendar() {
		// Change 11/29/2019 to be a partial day holiday
		CalendarHolidayDay holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(BUSINESS_DAY_CALENDAR_ID, DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("11/29/2019")));
		if (holidayDay.isFullDayHoliday()) {
			holidayDay.setFullDayHoliday(false);
			this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		}

		// Change 12/24/2019 to be a Settlement Holiday Only
		holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(BUSINESS_DAY_CALENDAR_ID, DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("12/24/2019")));
		if (holidayDay.isTradeHoliday()) {
			holidayDay.setTradeHoliday(false);
			this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		}

		// Change 12/26/2019 to be a PARTIAL Trade Holiday Only
		holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(BUSINESS_DAY_CALENDAR_ID, DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("12/26/2019")));
		if (holidayDay.isSettlementHoliday()) {
			holidayDay.setSettlementHoliday(false);
			holidayDay.setFullDayHoliday(false);
			this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		}

		// Change 12/31/2019 to be a Trade Holiday Only
		holidayDay = this.calendarHolidayService.getCalendarHolidayDayByCalendarAndDay(BUSINESS_DAY_CALENDAR_ID, DateUtils.getDaysSinceNaturalStart(DateUtils.toDate("12/31/2019")));
		if (holidayDay.isSettlementHoliday()) {
			holidayDay.setSettlementHoliday(false);
			this.calendarHolidayService.saveCalendarHolidayDay(holidayDay);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIsBusinessDayForMissingDate() {
		// No longer an error is date is missing - but would just validate weekday / weekend
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(DateUtils.toDate("12/31/2013"), US_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(DateUtils.toDate("12/29/2013"), US_CALENDAR_ID))); // weekend
	}


	@Test
	public void testIsBusinessDay() {
		reconfigureBusinessDayCalendar();

		// Weekend is NOT a business day
		Date date = DateUtils.toDate("12/28/2008"); // Sunday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, LONDON_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, LONDON_CALENDAR_ID)));

		// Weekday with no holidays is a business day
		date = DateUtils.toDate("12/29/2008"); // Monday
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, LONDON_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, LONDON_CALENDAR_ID)));

		// Weekday is flagged as a holiday in US calendar, but not in london
		date = DateUtils.toDate("01/01/2009"); // Thursday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, US_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, US_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));

		// Not a Full Day Holiday - Yes Business Day
		date = DateUtils.toDate("11/29/2019");
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));

		// Settlement Holiday Only - Not a Business Day, Not a Settlement Business Day, Yes a Trade Business Day
		date = DateUtils.toDate("12/24/2019");
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));


		// Trade Holiday Only - Not a Business Day, Yes a Settlement Business Day, Not a Trade Business Day
		date = DateUtils.toDate("12/31/2019");
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));
	}


	@Test
	public void testIsBusinessDay_multipleCalendars() {
		reconfigureBusinessDayCalendar();

		Short[] calendarIds = new Short[]{US_CALENDAR_ID, LONDON_CALENDAR_ID};

		// Weekend is NOT a business day
		Date date = DateUtils.toDate("12/28/2008"); // Sunday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		// Weekday with no holidays is a business day
		date = DateUtils.toDate("12/29/2008"); // Monday
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		// Weekday is flagged as a holiday in US calendar, but not in london
		date = DateUtils.toDate("01/01/2009"); // Thursday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		calendarIds = new Short[]{BUSINESS_DAY_CALENDAR_ID, LONDON_CALENDAR_ID};

		// Not a Full Day Holiday - Yes Business Day
		date = DateUtils.toDate("11/29/2019");
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		// Settlement Holiday Only - Not a Business Day, Not a Settlement Business Day, Yes a Trade Business Day
		date = DateUtils.toDate("12/24/2019");
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));


		// Trade Holiday Only - Not a Business Day, Yes a Settlement Business Day, Not a Trade Business Day
		date = DateUtils.toDate("12/31/2019");
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));
	}


	@Test
	public void testIsBusinessDayPartial() {
		reconfigureBusinessDayCalendar();

		// Weekend is NOT a business day
		Date date = DateUtils.toDate("12/28/2008"); // Sunday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, LONDON_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, LONDON_CALENDAR_ID)));

		// Weekday with no holidays is a business day BUT NOT a PARTIAL BUSINESS DAY
		date = DateUtils.toDate("12/29/2008"); // Monday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, LONDON_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, LONDON_CALENDAR_ID)));

		// Weekday is flagged as a holiday in US calendar, but not in london
		date = DateUtils.toDate("01/01/2009"); // Thursday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, US_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, US_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));

		// Not a Full Day Holiday - Trading AND Settlement
		date = DateUtils.toDate("11/29/2019");
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));

		// Settlement Holiday Only - Not a Business Day, Not a Settlement Business Day, Yes a Trade Business Day, BUT NOT A PARTIAL
		date = DateUtils.toDate("12/24/2019");
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));

		// Trade Holiday Only - PARTIAL ONLY
		date = DateUtils.toDate("12/26/2019");
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));
	}


	@Test
	public void testIsBusinessDayPartial_multipleCalendars() {
		reconfigureBusinessDayCalendar();

		Short[] calendarIds = new Short[]{US_CALENDAR_ID, LONDON_CALENDAR_ID};

		// Weekend is NOT a business day
		Date date = DateUtils.toDate("12/28/2008"); // Sunday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		// Weekday with no holidays is a business day BUT NOT a PARTIAL BUSINESS DAY
		date = DateUtils.toDate("12/29/2008"); // Monday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		// Weekday is flagged as a holiday in US calendar, but not in london
		date = DateUtils.toDate("01/01/2009"); // Thursday
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		calendarIds = new Short[]{BUSINESS_DAY_CALENDAR_ID, LONDON_CALENDAR_ID};

		// Not a Full Day Holiday - Trading AND Settlement
		date = DateUtils.toDate("11/29/2019");
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		// Settlement Holiday Only - Not a Business Day, Not a Settlement Business Day, Yes a Trade Business Day, BUT NOT A PARTIAL
		date = DateUtils.toDate("12/24/2019");
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		// Trade Holiday Only - PARTIAL ONLY
		date = DateUtils.toDate("12/26/2019");
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		Assertions.assertTrue(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		Assertions.assertFalse(this.calendarBusinessDayService.isBusinessDayPartial(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));
	}


	private void validateDateEquals(String expected, Date date) {
		Assertions.assertEquals(expected, DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));
	}


	@Test
	public void testGetPreviousBusinessDay() {
		reconfigureBusinessDayCalendar();

		// Previous Business day of a Monday would be Friday
		Date date = DateUtils.toDate("01/05/2009"); // Monday
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forTrade(date, US_CALENDAR_ID)));
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forSettlement(date, US_CALENDAR_ID)));

		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));

		// Previous Business Day from Jan 2 would be Dec 31 in US Bank Holidays
		date = DateUtils.toDate("01/02/2009");
		validateDateEquals("12/31/2008", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		validateDateEquals("12/31/2008", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forTrade(date, US_CALENDAR_ID)));
		validateDateEquals("12/31/2008", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forSettlement(date, US_CALENDAR_ID)));

		// Previous Business Day from Jan 2 would be Jan 1 in London Holidays
		validateDateEquals("01/01/2009", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));

		// Previous Business Day from Dec 2 (Monday) would be Nov 29 for all (because NOT a full day holiday)
		date = DateUtils.toDate("12/2/2019");
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));

		// Previous Business Day from Dec 25 would be Dec 24 for Trade only, otherwise 12/23
		date = DateUtils.toDate("12/25/2019");
		validateDateEquals("12/23/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("12/24/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("12/23/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));


		// Previous Business Day from Jan 2 would be Dec 31 for Settlement only, otherwise 12/30
		date = DateUtils.toDate("01/02/2020");
		validateDateEquals("12/30/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("12/30/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("12/31/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));
	}


	@Test
	public void testGetPreviousBusinessDay_multipleCalendars() {
		reconfigureBusinessDayCalendar();

		Short[] calendarIds = new Short[]{US_CALENDAR_ID, LONDON_CALENDAR_ID};

		// Previous Business day of a Monday would be Friday
		Date date = DateUtils.toDate("01/05/2009"); // Monday
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));


		// Previous Business Day from Jan 2 would be Dec 31 in US Bank Holidays, Jan 1 in London Holidays
		// Should return Dex 31, because that is first business day that exists in BOTH calendars
		date = DateUtils.toDate("01/02/2009");
		validateDateEquals("12/31/2008", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));

		// Previous Business Day from Dec 2 (Monday) would be Nov 29 for all (because NOT a full day holiday)
		date = DateUtils.toDate("12/2/2019");
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));

		calendarIds = new Short[]{BUSINESS_DAY_CALENDAR_ID, US_CALENDAR_ID};

		// Previous Business Day from Dec 25 would be Dec 24 for Trade only, otherwise 12/23
		date = DateUtils.toDate("12/25/2019");
		validateDateEquals("12/23/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		validateDateEquals("12/24/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		validateDateEquals("12/23/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));


		// Previous Business Day from Jan 2 would be Dec 31 for Settlement only, otherwise 12/30
		date = DateUtils.toDate("01/02/2020");
		validateDateEquals("12/30/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		validateDateEquals("12/30/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		validateDateEquals("12/31/2019", this.calendarBusinessDayService.getPreviousBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));
	}


	@Test
	public void testGetNextBusinessDay() {
		reconfigureBusinessDayCalendar();

		// Next Business day of a Friday would be Monday
		Date date = DateUtils.toDate("01/02/2009"); // Friday
		validateDateEquals("01/05/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		validateDateEquals("01/05/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, US_CALENDAR_ID)));
		validateDateEquals("01/05/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, US_CALENDAR_ID)));

		validateDateEquals("01/05/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));

		// Next Business Day from Dec 31 would be Jan 2 in US Bank Holidays
		date = DateUtils.toDate("12/31/2008");
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID)));
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, US_CALENDAR_ID)));
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, US_CALENDAR_ID)));

		// Next Business Day from Dec 31 would be Jan 1 in London Holidays
		validateDateEquals("01/01/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, LONDON_CALENDAR_ID)));

		// Next Business Day from Nov 27 would be Nov 29 for all (because NOT a full day holiday) (Nov 28 is a full day holiday)
		date = DateUtils.toDate("11/27/2019");
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));

		// Next Business Day from Dec 23 would be Dec 24 for Trade only, otherwise 12/26 (12/25 is full day holiday)
		date = DateUtils.toDate("12/23/2019");
		validateDateEquals("12/26/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("12/24/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("12/26/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));


		// Next Business Day from Dec 30 would be Dec 31 for Settlement only, otherwise 1/2
		date = DateUtils.toDate("12/30/2019");
		validateDateEquals("01/02/2020", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("01/02/2020", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, BUSINESS_DAY_CALENDAR_ID)));
		validateDateEquals("12/31/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, BUSINESS_DAY_CALENDAR_ID)));
	}


	@Test
	public void testGetNextBusinessDay_multipleCalendars() {
		reconfigureBusinessDayCalendar();

		Short[] calendarIds = new Short[]{US_CALENDAR_ID, LONDON_CALENDAR_ID};

		// Next Business day of a Friday would be Monday
		Date date = DateUtils.toDate("01/02/2009"); // Friday
		validateDateEquals("01/05/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		validateDateEquals("01/05/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		validateDateEquals("01/05/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		// Next Business Day from Dec 31 would be Jan 1 in London Holidays, Jan 2 in US Bank Holidays
		date = DateUtils.toDate("12/31/2008");
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		validateDateEquals("01/02/2009", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		calendarIds = new Short[]{BUSINESS_DAY_CALENDAR_ID, US_CALENDAR_ID};

		// Next Business Day from Nov 27 would be Nov 29 for all (because NOT a full day holiday) (Nov 28 is a full day holiday)
		date = DateUtils.toDate("11/27/2019");
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		validateDateEquals("11/29/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));

		// Next Business Day from Dec 23 would be Dec 24 for Trade only, otherwise 12/26 (12/25 is full day holiday)
		date = DateUtils.toDate("12/23/2019");
		validateDateEquals("12/26/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		validateDateEquals("12/24/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		validateDateEquals("12/26/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));


		// Next Business Day from Dec 30 would be Dec 31 for Settlement only, otherwise 1/2
		date = DateUtils.toDate("12/30/2019");
		validateDateEquals("01/02/2020", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarIds)));
		validateDateEquals("01/02/2020", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forTrade(date, calendarIds)));
		validateDateEquals("12/31/2019", this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forSettlement(date, calendarIds)));
	}


	@Test
	public void testGetNearestBusinessDayFrom() {
		// Business day forward
		validateDateEquals("10/02/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 1));
		validateDateEquals("10/03/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 2));
		validateDateEquals("10/04/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 3));
		validateDateEquals("10/05/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 4));
		validateDateEquals("10/09/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 5));
		validateDateEquals("10/09/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 6));
		validateDateEquals("10/09/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 7));
		validateDateEquals("10/09/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 8));
		validateDateEquals("10/10/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 9));
		validateDateEquals("10/22/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 19));
		validateDateEquals("10/25/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 24));
		validateDateEquals("10/26/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 25));
		validateDateEquals("10/29/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), US_CALENDAR_ID), 26));

		// Business day backward
		validateDateEquals("10/30/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -1));
		validateDateEquals("10/29/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -2));
		validateDateEquals("10/26/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -3));
		validateDateEquals("10/26/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -4));
		validateDateEquals("10/26/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -5));
		validateDateEquals("10/25/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -6));
		validateDateEquals("10/24/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -7));
		validateDateEquals("10/23/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -8));
		validateDateEquals("10/22/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -9));
		validateDateEquals("10/19/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -10));
		validateDateEquals("10/19/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -11));
		validateDateEquals("10/12/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -19));
		validateDateEquals("10/05/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -24));
		validateDateEquals("10/05/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -25));
		validateDateEquals("10/05/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -26));
		validateDateEquals("10/04/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), US_CALENDAR_ID), -27));
	}


	@Test
	public void testGetNearestBusinessDayFrom_multipleCalendars() {

		Short[] calendarIds = new Short[]{US_CALENDAR_ID, LONDON_CALENDAR_ID};

		// Business day forward
		validateDateEquals("10/02/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 1));
		validateDateEquals("10/03/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 2));
		validateDateEquals("10/04/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 3));
		validateDateEquals("10/05/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 4));
		validateDateEquals("10/09/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 5));
		validateDateEquals("10/09/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 6));
		validateDateEquals("10/09/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 7));
		validateDateEquals("10/09/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 8));
		validateDateEquals("10/10/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 9));
		validateDateEquals("10/22/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 19));
		validateDateEquals("10/25/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 24));
		validateDateEquals("10/26/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 25));
		validateDateEquals("10/29/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/01/2012"), calendarIds), 26));

		// Business day backward
		validateDateEquals("10/30/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -1));
		validateDateEquals("10/29/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -2));
		validateDateEquals("10/26/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -3));
		validateDateEquals("10/26/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -4));
		validateDateEquals("10/26/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -5));
		validateDateEquals("10/25/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -6));
		validateDateEquals("10/24/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -7));
		validateDateEquals("10/23/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -8));
		validateDateEquals("10/22/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -9));
		validateDateEquals("10/19/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -10));
		validateDateEquals("10/19/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -11));
		validateDateEquals("10/12/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -19));
		validateDateEquals("10/05/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -24));
		validateDateEquals("10/05/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -25));
		validateDateEquals("10/05/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -26));
		validateDateEquals("10/04/2012", this.calendarBusinessDayService.getNearestBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.toDate("10/31/2012"), calendarIds), -27));
	}


	@Test
	public void testGetBusinessDayFrom() {
		Date date = DateUtils.toDate("12/29/2008");
		Date newDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID), 5);
		validateDateEquals("01/06/2009", newDate);

		date = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(newDate, US_CALENDAR_ID), -5);
		validateDateEquals("12/29/2008", date);

		// Get the business day 0 days away should ALWAYS return the given date regardless if
		// the date is a business day or not.
		date = DateUtils.toDate("12/28/2008");
		newDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID), 0);
		validateDateEquals("12/28/2008", newDate);

		date = DateUtils.toDate("12/29/2008");
		newDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, US_CALENDAR_ID), 0);
		validateDateEquals("12/29/2008", newDate);
	}


	@Test
	public void testGetBusinessDayFrom_multipleCalendars() {

		Short[] calendarIds = new Short[]{US_CALENDAR_ID, LONDON_CALENDAR_ID};

		Date date = DateUtils.toDate("12/29/2008");
		Date newDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, calendarIds), 5);
		validateDateEquals("01/06/2009", newDate);

		date = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(newDate, calendarIds), -5);
		validateDateEquals("12/29/2008", date);

		// Get the business day 0 days away should ALWAYS return the given date regardless if
		// the date is a business day or not.
		date = DateUtils.toDate("12/28/2008");
		newDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, calendarIds), 0);
		validateDateEquals("12/28/2008", newDate);

		date = DateUtils.toDate("12/29/2008");
		newDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, calendarIds), 0);
		validateDateEquals("12/29/2008", newDate);
	}


	@Test
	public void testGetBusinessDaysBetweenMissingStartDate() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			this.calendarBusinessDayService.getBusinessDaysBetween(null, new Date(), null, US_CALENDAR_ID);
		});
	}


	@Test
	public void testGetBusinessDaysBetweenMissingEndDate() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			this.calendarBusinessDayService.getBusinessDaysBetween(new Date(), null, null, US_CALENDAR_ID);
		});
	}


	@Test
	public void testGetBusinessDaysBetween() {
		reconfigureBusinessDayCalendar();

		Date startDate = DateUtils.toDate("12/29/2008");
		Date endDate = DateUtils.toDate("01/06/2009");
		Assertions.assertEquals(5, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, null, US_CALENDAR_ID));
		Assertions.assertEquals(5, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY, US_CALENDAR_ID));
		Assertions.assertEquals(5, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.BUSINESS_DAY, US_CALENDAR_ID));

		Assertions.assertEquals(-5, this.calendarBusinessDayService.getBusinessDaysBetween(endDate, startDate, null, US_CALENDAR_ID));
		Assertions.assertEquals(-5, this.calendarBusinessDayService.getBusinessDaysBetween(endDate, startDate, CalendarBusinessDayTypes.TRADE_BUSINESS_DAY, US_CALENDAR_ID));

		// Same start & end date should return a difference of 0
		Assertions.assertEquals(0, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, startDate, null, US_CALENDAR_ID));

		// 11/27/19 - 12/2/2019 - 28 full day holiday, 29 partial day holiday (i.e. is a business day)
		startDate = DateUtils.toDate("11/27/2019");
		endDate = DateUtils.toDate("12/02/2019");
		Assertions.assertEquals(2, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.BUSINESS_DAY, BUSINESS_DAY_CALENDAR_ID));
		Assertions.assertEquals(2, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.TRADE_BUSINESS_DAY, BUSINESS_DAY_CALENDAR_ID));
		Assertions.assertEquals(2, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY, BUSINESS_DAY_CALENDAR_ID));

		// 12/23/19 - 12/26/2019 - 12/24 (settlement  holiday only), 25 holiday, 12/31 trade holiday only 1/1 holiday
		startDate = DateUtils.toDate("12/23/2019");
		endDate = DateUtils.toDate("12/26/2019");
		Assertions.assertEquals(1, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.BUSINESS_DAY, BUSINESS_DAY_CALENDAR_ID));
		Assertions.assertEquals(2, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.TRADE_BUSINESS_DAY, BUSINESS_DAY_CALENDAR_ID));
		Assertions.assertEquals(1, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY, BUSINESS_DAY_CALENDAR_ID));


		// 12/23/19 - 01/02/2020 - 12/24 (settlement  holiday only), 25 holiday, 12/31 trade holiday only 1/1 holiday
		startDate = DateUtils.toDate("12/23/2019");
		endDate = DateUtils.toDate("01/02/2020");
		Assertions.assertEquals(4, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.BUSINESS_DAY, BUSINESS_DAY_CALENDAR_ID));
		Assertions.assertEquals(5, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.TRADE_BUSINESS_DAY, BUSINESS_DAY_CALENDAR_ID));
		Assertions.assertEquals(5, this.calendarBusinessDayService.getBusinessDaysBetween(startDate, endDate, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY, BUSINESS_DAY_CALENDAR_ID));
	}


	@Test
	public void testGetBusinessDayOfMonth() {
		// Setup 2013 Calendar Days
		short yr = 2013;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);
		}
		Integer expected = 1;
		Assertions.assertEquals(expected, this.calendarBusinessDayService.getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(DateUtils.toDate("02/01/2013"), US_CALENDAR_ID)));
		Assertions.assertEquals(expected, this.calendarBusinessDayService.getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(DateUtils.toDate("02/02/2013"), US_CALENDAR_ID)));
		Assertions.assertEquals(expected, this.calendarBusinessDayService.getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(DateUtils.toDate("02/03/2013"), US_CALENDAR_ID)));
		expected++; // 2
		Assertions.assertEquals(expected, this.calendarBusinessDayService.getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(DateUtils.toDate("02/04/2013"), US_CALENDAR_ID)));
		expected++; // 3
		Assertions.assertEquals(expected, this.calendarBusinessDayService.getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(DateUtils.toDate("02/05/2013"), US_CALENDAR_ID)));
		expected++; // 4
		Assertions.assertEquals(expected, this.calendarBusinessDayService.getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(DateUtils.toDate("02/06/2013"), US_CALENDAR_ID)));
		expected++; // 5
		Assertions.assertEquals(expected, this.calendarBusinessDayService.getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(DateUtils.toDate("02/07/2013"), US_CALENDAR_ID)));
		expected++; // 6
		Assertions.assertEquals(expected, this.calendarBusinessDayService.getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(DateUtils.toDate("02/08/2013"), US_CALENDAR_ID)));
		Assertions.assertEquals(expected, this.calendarBusinessDayService.getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(DateUtils.toDate("02/09/2013"), US_CALENDAR_ID)));
	}


	@Test
	public void testGetCalendarBusinessDayListMissingCalendar() {
		Assertions.assertThrows(ValidationException.class, () -> {
			CalendarBusinessDaySearchForm searchForm = new CalendarBusinessDaySearchForm();
			// Calendar Id is a required parameter
			this.calendarBusinessDayService.getCalendarBusinessDayList(searchForm);
		});
	}
}
