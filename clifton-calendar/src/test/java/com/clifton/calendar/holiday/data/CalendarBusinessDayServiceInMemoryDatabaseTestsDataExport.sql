SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId FROM CalendarDay WHERE CalendarYearID IN (2017, 2018)
UNION
SELECT 'CalendarHolidayDay' AS entityTableName, CalendarHolidayDayID AS entityId FROM CalendarHolidayDay  hd INNER JOIN CalendarDay cd ON hd.CalendarDayID = cd.CalendarDayID WHERE cd.CalendarYearID IN (2017, 2018)
UNION
SELECT 'Calendar' AS entityTableName, CalendarID AS entityId FROM Calendar WHERE CalendarID IN (88, 2);
