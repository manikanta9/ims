package com.clifton.calendar.comparison.field;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.CalendarDay;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.date.DateUtilFunctions;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


/**
 * @author manderson
 */

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class BeanFieldDateValueComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private CalendarSetupService calendarSetupService;


	@BeforeEach
	public void resetTests() {
		// Set Up Calendar Days for Previous, This, and Next Year
		short year = new Integer(DateUtils.getYear(new Date())).shortValue();
		if (this.calendarSetupService.getCalendarYear(year) == null) {
			this.calendarSetupService.saveCalendarYearForYear(year);

			// Previous Year and Next Year
			this.calendarSetupService.saveCalendarYearForYear(((Integer) (year - 1)).shortValue());
			this.calendarSetupService.saveCalendarYearForYear(((Integer) (year + 1)).shortValue());
		}
	}


	@Test
	public void testDateComparisons() {
		CalendarDay nullDate = new CalendarDay();
		CalendarDay tenDaysAgo = this.calendarSetupService.getCalendarDayByDate(DateUtils.addDays(new Date(), -10));
		CalendarDay yesterday = this.calendarSetupService.getCalendarDayByDate(DateUtils.addDays(new Date(), -1));
		CalendarDay today = this.calendarSetupService.getCalendarDayByDate(new Date());
		CalendarDay tomorrow = this.calendarSetupService.getCalendarDayByDate(DateUtils.addDays(new Date(), 1));
		CalendarDay tenDaysAhead = this.calendarSetupService.getCalendarDayByDate(DateUtils.addDays(new Date(), 10));

		// Before Today
		BeanFieldDateValueComparison<CalendarDay> comparison = setupBeanFieldDateValueComparison(ComparisonConditions.LESS_THAN, 0, false, null, null, false);
		Date compareToDate = new Date();

		validateComparison(comparison, nullDate, compareToDate, false);
		validateComparison(comparison, tenDaysAgo, compareToDate, true);
		validateComparison(comparison, yesterday, compareToDate, true);
		validateComparison(comparison, today, compareToDate, false);
		validateComparison(comparison, tomorrow, compareToDate, false);
		validateComparison(comparison, tenDaysAhead, compareToDate, false);

		// On Or Before Yesterday
		comparison = setupBeanFieldDateValueComparison(ComparisonConditions.LESS_THAN_OR_EQUALS, -1, false, null, null, false);
		compareToDate = DateUtils.addDays(new Date(), -1);

		validateComparison(comparison, nullDate, compareToDate, false);
		validateComparison(comparison, tenDaysAgo, compareToDate, true);
		validateComparison(comparison, yesterday, compareToDate, true);
		validateComparison(comparison, today, compareToDate, false);
		validateComparison(comparison, tomorrow, compareToDate, false);
		validateComparison(comparison, tenDaysAhead, compareToDate, false);

		// Equal To Tomorrow
		compareToDate = DateUtils.addDays(new Date(), 1);
		comparison = setupBeanFieldDateValueComparison(ComparisonConditions.EQUALS, 1, false, null, null, false);

		validateComparison(comparison, nullDate, compareToDate, false);
		validateComparison(comparison, tenDaysAgo, compareToDate, false);
		validateComparison(comparison, yesterday, compareToDate, false);
		validateComparison(comparison, today, compareToDate, false);
		validateComparison(comparison, tomorrow, compareToDate, true);
		validateComparison(comparison, tenDaysAhead, compareToDate, false);

		// On Or After Today
		comparison = setupBeanFieldDateValueComparison(ComparisonConditions.GREATER_THAN_OR_EQUALS, 0, false, null, null, false);
		compareToDate = new Date();

		validateComparison(comparison, nullDate, compareToDate, false);
		validateComparison(comparison, tenDaysAgo, compareToDate, false);
		validateComparison(comparison, yesterday, compareToDate, false);
		validateComparison(comparison, today, compareToDate, true);
		validateComparison(comparison, tomorrow, compareToDate, true);
		validateComparison(comparison, tenDaysAhead, compareToDate, true);


		// After Previous Business Day
		comparison = setupBeanFieldDateValueComparison(ComparisonConditions.GREATER_THAN, -1, true, null, null, false);
		compareToDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);

		validateComparison(comparison, nullDate, compareToDate, false);
		validateComparison(comparison, tenDaysAgo, compareToDate, false);
		validateComparison(comparison, today, compareToDate, true);
		validateComparison(comparison, tomorrow, compareToDate, true);
		validateComparison(comparison, tenDaysAhead, compareToDate, true);

		// Equals Quarter End - comparing to bean date value
		comparison = setupBeanFieldDateValueComparison(ComparisonConditions.EQUALS, null, false, null, DateUtilFunctions.QUARTER_END, true);

		Date previousQuarterEndDate = DateUtils.getLastDayOfPreviousQuarter(new Date());
		Date quarterEndDate = DateUtils.getLastDayOfQuarter(new Date());
		CalendarDay previousQuarterEnd = this.calendarSetupService.getCalendarDayByDate(previousQuarterEndDate);
		CalendarDay quarterEnd = this.calendarSetupService.getCalendarDayByDate(quarterEndDate);
		CalendarDay quarterStart = this.calendarSetupService.getCalendarDayByDate(DateUtils.getFirstDayOfQuarter(quarterEndDate));
		CalendarDay monthEnd = this.calendarSetupService.getCalendarDayByDate(DateUtils.getLastDayOfMonth(quarterStart.getEndDate()));

		validateComparison(comparison, nullDate, quarterEndDate, false);
		validateComparison(comparison, previousQuarterEnd, previousQuarterEndDate, true);
		validateComparison(comparison, quarterEnd, quarterEndDate, true);
		validateComparison(comparison, quarterStart, quarterEndDate, false);
		validateComparison(comparison, monthEnd, quarterEndDate, false);

		// Equals Quarter End - comparing to today
		comparison = setupBeanFieldDateValueComparison(ComparisonConditions.EQUALS, 0, false, null, DateUtilFunctions.QUARTER_END, false);

		validateComparison(comparison, nullDate, quarterEndDate, false);
		validateComparison(comparison, previousQuarterEnd, quarterEndDate, false);
		validateComparison(comparison, quarterEnd, quarterEndDate, true);
		validateComparison(comparison, quarterStart, quarterEndDate, false);
		validateComparison(comparison, monthEnd, quarterEndDate, false);
	}

	/////////////////////////////////////////////////


	private BeanFieldDateValueComparison<CalendarDay> setupBeanFieldDateValueComparison(ComparisonConditions condition, Integer daysFromToday, boolean useBusinessDays, String value, DateUtilFunctions dateUtilFunction, boolean useBeanValueForDateUtilFunction) {
		BeanFieldDateValueComparison<CalendarDay> comparison = new BeanFieldDateValueComparison<>();
		comparison.setBeanFieldName("startDate");
		comparison.setComparisonCondition(condition);
		comparison.setDaysFromToday(daysFromToday == null ? null : daysFromToday.shortValue());
		comparison.setUseBusinessDays(useBusinessDays);
		comparison.setValue(value);
		comparison.setDateUtilFunction(dateUtilFunction);
		comparison.setUseBeanValueForDateUtilFunction(useBeanValueForDateUtilFunction);
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	private void validateComparison(BeanFieldDateValueComparison<CalendarDay> comparison, CalendarDay bean, Date actualValue, boolean expectTrue) {
		SimpleComparisonContext context = new SimpleComparisonContext();
		if (expectTrue) {
			Assertions.assertTrue(comparison.evaluate(bean, context));
			Assertions.assertEquals(getExpectedMessage(comparison, bean, actualValue, true), context.getTrueMessage());
		}
		else {
			Assertions.assertFalse(comparison.evaluate(bean, context));
			Assertions.assertEquals(getExpectedMessage(comparison, bean, actualValue, false), context.getFalseMessage());
		}
	}


	private String getExpectedMessage(BeanFieldDateValueComparison<CalendarDay> comparison, CalendarDay bean, Date actualValue, boolean expectTrue) {
		StringBuilder sb = new StringBuilder("(startDate");
		if (bean.getStartDate() == null) {
			sb.append(" IS NULL)");
		}
		else {
			sb.append(": ").append(DateUtils.fromDateShort(bean.getStartDate()));
			if (!expectTrue) {
				sb.append(" is not");
			}
			sb.append(" ");
			sb.append(comparison.getComparisonCondition().getComparisonExpression());
			sb.append(" ");
			sb.append(DateUtils.fromDateShort(actualValue));
			sb.append(")");
		}
		return sb.toString();
	}

	/////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
