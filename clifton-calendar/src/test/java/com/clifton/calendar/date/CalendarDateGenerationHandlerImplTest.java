package com.clifton.calendar.date;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.holiday.CalendarBusinessDayServiceImpl;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class CalendarDateGenerationHandlerImplTest {

	private static final short CALENDAR = 1;

	private static List<Date> US_HOLIDAYS;

	private CalendarDateGenerationHandler calendarDateGenerationHandler;


	private Date getScheduleStartDate() {
		return DateUtils.toDate("01/01/2014");
	}


	private Date getScheduleEndDate() {
		return DateUtils.toDate("03/15/2015");
	}


	@BeforeEach
	public void setup() {
		Calendar calendar = new Calendar();
		calendar.setId(CALENDAR);
		calendar.setName("Default");

		CalendarBusinessDayServiceImpl calendarBusinessDayServiceImpl = (CalendarBusinessDayServiceImpl) CalendarTestObjectFactory.newCalendarBusinessDayService(getScheduleStartDate(), getScheduleEndDate(),
				US_HOLIDAYS, calendar);

		this.calendarDateGenerationHandler = new CalendarDateGenerationHandlerImpl();
		((CalendarDateGenerationHandlerImpl) this.calendarDateGenerationHandler).setCalendarBusinessDayService(calendarBusinessDayServiceImpl);
	}


	@Test
	public void testGenerateDateFromString() {
		// These tests are disabled because the require setting up todays date in the calendar
		//String previousBusinessDay = formatDate(this.calendarBusinessDayService.getBusinessDayFrom(-1));
		//Assertions.assertEquals(previousBusinessDay, formatDate(this.calendarDateGenerationHandler.generateDate("PREVIOUS_BUSINESS_DAY")));
		//previousBusinessDay = formatDate(this.calendarBusinessDayService.getBusinessDayFrom(-2));
		//Assertions.assertEquals(previousBusinessDay, formatDate(this.calendarDateGenerationHandler.generateDate("BUSINESS_DAYS_BACK:2")));
		//Assert.assertEquals(previousBusinessDay, formatDate(this.calendarDateGenerationHandler.generateDate("BUSINESS_DAYS_BACK:2")));
		Assertions.assertEquals("11/01/2014", formatDate(this.calendarDateGenerationHandler.generateDate("11/1/2014")));

		Assertions.assertEquals("10/31/2014", formatDate(this.calendarDateGenerationHandler.generateDate("PREVIOUS_BUSINESS_DAY:11/1/2014")));
		Assertions.assertEquals("10/29/2014", formatDate(this.calendarDateGenerationHandler.generateDate("BUSINESS_DAYS_BACK:11/1/2014:3")));
		Assertions.assertEquals("10/31/2014", formatDate(this.calendarDateGenerationHandler.generateDate("LAST_DAY_OF_PREVIOUS_MONTH:11/1/2014")));
		Assertions.assertEquals("10/31/2014", formatDate(this.calendarDateGenerationHandler.generateDate("LAST_DAY_OF_PREVIOUS_MONTH:11/1/2014:3")));
		Assertions.assertEquals("11/04/2014", formatDate(this.calendarDateGenerationHandler.generateDate("BUSINESS_DAYS_FORWARD:11/1/2014:2")));
	}


	@Test
	public void testGenerateDateLastBusinessDayOfCurrentMonth() {
		// These tests are disabled because the require setting up todays date in the calendar
		Date date = DateUtils.toDate("02/03/2015");
		Assertions.assertEquals("02/27/2015", formatDate(this.calendarDateGenerationHandler.generateDate(DateGenerationOptions.LAST_BUSINESS_DAY_OF_CURRENT_MONTH, date)));
	}


	@Test
	public void testGenerateDateFirstDayOfPreviousMonth() {
		Date date = DateUtils.toDate("02/03/2015");
		Assertions.assertEquals("01/01/2015", formatDate(this.calendarDateGenerationHandler.generateDate(DateGenerationOptions.FIRST_DAY_OF_PREVIOUS_MONTH, date)));
	}


	@Test
	public void testGenerateDateLastDayOfPreviousQuarter() {
		Date date = DateUtils.toDate("02/03/2015");
		Assertions.assertEquals("12/31/2014", formatDate(this.calendarDateGenerationHandler.generateDate(DateGenerationOptions.LAST_BUSINESS_DAY_OF_PREVIOUS_QUARTER, date)));
	}


	@Test
	public void testWeekDay() {
		Assertions.assertEquals("10/30/2014", formatDate(this.calendarDateGenerationHandler.generateDate("WEEKDAYS_BACK:11/3/2014:2")));
		Assertions.assertEquals("10/27/2014", formatDate(this.calendarDateGenerationHandler.generateDate("WEEKDAYS_BACK:11/3/2014:5")));

		Assertions.assertEquals("11/05/2014", formatDate(this.calendarDateGenerationHandler.generateDate("WEEKDAYS_FORWARD:11/3/2014:2")));
		Assertions.assertEquals("11/10/2014", formatDate(this.calendarDateGenerationHandler.generateDate("WEEKDAYS_FORWARD:11/3/2014:5")));
	}


	@Test
	public void testFirstDayOfYear() {
		Date firstOfThisYear = DateUtils.getFirstDayOfYear(new Date());
		Assertions.assertEquals(formatDate(firstOfThisYear), formatDate(this.calendarDateGenerationHandler.generateDate(DateGenerationOptions.FIRST_DAY_OF_YEAR, new Date())));
	}


	private String formatDate(Date date) {
		return DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT);
	}


	static {
		US_HOLIDAYS = new ArrayList<>();

		US_HOLIDAYS.add(DateUtils.toDate("01/01/2014"));
		US_HOLIDAYS.add(DateUtils.toDate("01/20/2014"));
		US_HOLIDAYS.add(DateUtils.toDate("02/17/2014"));
		US_HOLIDAYS.add(DateUtils.toDate("04/18/2014"));
		US_HOLIDAYS.add(DateUtils.toDate("05/26/2014"));
		US_HOLIDAYS.add(DateUtils.toDate("07/04/2014"));
		US_HOLIDAYS.add(DateUtils.toDate("09/01/2014"));
		US_HOLIDAYS.add(DateUtils.toDate("11/27/2014"));
		US_HOLIDAYS.add(DateUtils.toDate("12/25/2014"));
	}
}
