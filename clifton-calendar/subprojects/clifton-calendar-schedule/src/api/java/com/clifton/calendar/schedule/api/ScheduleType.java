package com.clifton.calendar.schedule.api;

import java.io.Serializable;


/**
 * Defines a specific type of schedule. Primarily used to designate whether a schedule support time, and to reference a schedule to a specific {@link com.clifton.system.schema.SystemTable}.
 *
 * @author mitchellf
 */
public class ScheduleType implements Serializable {

	private short id;

	private String name;

	private boolean timeSupported;

	private boolean nameSystemDefined;

	private String systemTableName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public short getId() {
		return this.id;
	}


	public void setId(short id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public boolean isTimeSupported() {
		return this.timeSupported;
	}


	public void setTimeSupported(boolean timeSupported) {
		this.timeSupported = timeSupported;
	}


	public boolean isNameSystemDefined() {
		return this.nameSystemDefined;
	}


	public void setNameSystemDefined(boolean nameSystemDefined) {
		this.nameSystemDefined = nameSystemDefined;
	}


	public String getSystemTableName() {
		return this.systemTableName;
	}


	public void setSystemTableName(String systemTableName) {
		this.systemTableName = systemTableName;
	}
}
