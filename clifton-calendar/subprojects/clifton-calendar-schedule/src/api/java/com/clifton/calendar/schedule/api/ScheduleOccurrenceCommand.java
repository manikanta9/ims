package com.clifton.calendar.schedule.api;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.date.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Command processed to find a set of occurrences for a given schedule. By design, it should be used to find occurrences in one of the following ways
 * - A set number of occurrences after a given date
 * - A set number of occurrences before a given date
 * - All occurrences between two dates
 *
 * @author mitchellf
 */
public class ScheduleOccurrenceCommand implements Serializable {

	/**
	 * ID of the schedule to get occurrences from
	 */
	private final Integer scheduleId;

	/**
	 * As an alternative to the ScheduleID field, a schedule object can be provided.  Schedule ID should be used if possible, but this field is for cases where the schedule may not be persisted to the database, and as such will not have an ID
	 */
	private final Schedule schedule;

	/**
	 * Indicates whether the command should find past occurrences.  If not specified in the constructor, it will default to false (indicating the user wants future occurrences)
	 */
	private boolean pastOccurrences;

	/**
	 * The number of occurrences we want to get
	 */
	private int occurrences;

	/**
	 * Start date to find occurrences.  If we are getting future occurrences, we will get occurrences after this date.  If we are getting past occurrences, we will get occurrences before this date.
	 */
	private Date startDate;

	/**
	 * End date if we are finding a range of occurrences.  If this is specified, there is no need to set the pastOccurrences field.  While the use may set the occurrences field to specify a number,
	 * that is not recommended, it is better to either get the number of occurrences or all occurrences withing a date range.
	 */
	private Date endDate;

	/**
	 * There is also the option to override the calendar that is used for the calculation of business days/holidays.  A list of calendars can be specified here, which will override the calendar that the provided schedule references.
	 */
	private List<Short> calendarIdList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private ScheduleOccurrenceCommand(int scheduleId, Date startDate) {
		this.scheduleId = scheduleId;
		this.schedule = null;
		this.startDate = startDate;
	}


	private ScheduleOccurrenceCommand(Schedule schedule, Date startDate) {
		this.scheduleId = null;
		this.schedule = schedule;
		this.startDate = startDate;
	}


	public static ScheduleOccurrenceCommand forOccurrences(int scheduleId, int occurrences, Date startDate) {
		ScheduleOccurrenceCommand command = new ScheduleOccurrenceCommand(scheduleId, startDate)
				.withPastOccurrences(occurrences < 0);
		command.setOccurrences(Math.abs(occurrences));
		return command;
	}


	public static ScheduleOccurrenceCommand forOccurrencesBetween(int scheduleId, Date startDate, Date endDate) {
		ScheduleOccurrenceCommand command = new ScheduleOccurrenceCommand(scheduleId, startDate)
				.withPastOccurrences(DateUtils.isDateBefore(endDate, startDate, true));
		command.setEndDate(endDate);
		return command;
	}


	public static ScheduleOccurrenceCommand forOccurrences(Schedule schedule, int occurrences, Date startDate) {
		ScheduleOccurrenceCommand command = new ScheduleOccurrenceCommand(schedule, startDate)
				.withPastOccurrences(occurrences < 0);
		command.setOccurrences(occurrences);
		return command;
	}


	public static ScheduleOccurrenceCommand forOccurrencesBetween(Schedule schedule, Date startDate, Date endDate) {
		ScheduleOccurrenceCommand command = new ScheduleOccurrenceCommand(schedule, startDate)
				.withPastOccurrences(DateUtils.isDateBefore(endDate, startDate, true));
		command.setEndDate(endDate);
		return command;
	}


	public ScheduleOccurrenceCommand withPastOccurrences(boolean pastOccurrences) {
		this.pastOccurrences = pastOccurrences;
		return this;
	}


	public ScheduleOccurrenceCommand withOverrideCalendarIdList(List<Short> calendarIdList) {
		this.calendarIdList = calendarIdList;
		return this;
	}


	public ScheduleOccurrenceCommand withOverrideCalendarIds(Short... calendarIds) {
		this.calendarIdList = ArrayUtils.getStream(calendarIds).collect(Collectors.toList());
		return this;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getScheduleId() {
		return this.scheduleId;
	}


	public Schedule getSchedule() {
		return this.schedule;
	}


	public boolean isPastOccurrences() {
		return this.pastOccurrences;
	}


	public void setPastOccurrences(boolean pastOccurrences) {
		this.pastOccurrences = pastOccurrences;
	}


	public int getOccurrences() {
		return this.occurrences;
	}


	public void setOccurrences(int occurrences) {
		this.occurrences = occurrences;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public List<Short> getCalendarIdList() {
		return this.calendarIdList;
	}


	public void setCalendarIdList(List<Short> calendarIdList) {
		this.calendarIdList = calendarIdList;
	}
}
