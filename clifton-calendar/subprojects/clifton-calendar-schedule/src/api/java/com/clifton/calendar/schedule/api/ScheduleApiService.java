package com.clifton.calendar.schedule.api;


import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * API to access Calendar-Schedule related objects, and to get a set of occurrences for a given calendar/schedule
 *
 * @author mitchellf
 */
public interface ScheduleApiService extends Serializable {

	public Schedule getSchedule(int id);


	public Schedule getScheduleByName(String name);


	public Schedule saveSchedule(Schedule bean);


	public void deleteSchedule(int id);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ScheduleType getScheduleType(short id);


	public ScheduleType getScheduleTypeByName(String name);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Get a single occurrence of a Calendar Schedule, most likely the occurrence immediately before or after a given date
	 */
	public Date getScheduleOccurrence(ScheduleOccurrenceCommand command);


	/**
	 * Get a set of occurrences.  Exact details of how a set of occurrences is obtained and/or filtered will be up to the implementation, but the {@link ScheduleOccurrenceCommand}
	 * parameter is designed to get either a set number of occurrences before/after a specific date, or all occurrences between two dates.
	 */
	public List<Date> getScheduleOccurrences(ScheduleOccurrenceCommand command);
}

