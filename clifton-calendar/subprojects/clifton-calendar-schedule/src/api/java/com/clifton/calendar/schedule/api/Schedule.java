package com.clifton.calendar.schedule.api;

import com.clifton.calendar.api.Calendar;
import com.clifton.core.util.date.Time;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * Defines a specific schedule, which can be used to calculate occurrences based on the frequency parameters and the business days defined by the {@link Calendar} parameter.
 *
 * @author mitchellf
 */
public class Schedule implements Serializable {

	private Integer id;
	private String name;
	private String description;

	private Calendar calendar;

	private ScheduleType scheduleType;

	private String businessDayConvention;

	/**
	 * Indicates whether this schedule is a rollup of other schedules
	 */
	private boolean rollupCalendarSchedule;

	/**
	 * List of child calendar schedules that are rolled up into this schedule
	 */
	private List<Schedule> childScheduleIdList;

	/**
	 * Specifies whether this schedule is system defined (created and managed by other system entities: InvestmentCalendar, etc.)
	 */
	public boolean systemDefined;

	/**
	 * startDate and endDate are stored without time components which are cleared by the set property methods.
	 */
	private Date startDate;
	private Date endDate;
	/**
	 * startTime and endTime are saved as integers see {@link Time}.
	 */
	private Time startTime;
	private Time endTime;

	/**
	 * Recurrence is the number of minutes, days, weeks, months or years to skip.
	 * <br/>
	 * Hourly recurrence is in minutes. Occurs every x minutes each hour.<br/>
	 * Daily recurrence is in days.<br/>
	 * Weekly recurrence is in weeks.<br/>
	 * Monthly recurrence is in months.<br/>
	 * Yearly recurrence is in years.<br/>
	 */
	private int recurrence;

	private String frequency;

	private Integer dayOfMonth;

	/**
	 * The number of days to adjust the final output of the schedule.
	 */
	private Integer additionalBusinessDays;

	// minutes properties

	// daily properties

	// weekly properties
	private boolean sunday;
	private boolean monday;
	private boolean tuesday;
	private boolean wednesday;
	private boolean thursday;
	private boolean friday;
	private boolean saturday;

	// monthly properties
	private boolean countFromLastDayOfMonth;

	// yearly properties
	private boolean january;
	private boolean february;
	private boolean march;
	private boolean april;
	private boolean may;
	private boolean june;
	private boolean july;
	private boolean august;
	private boolean september;
	private boolean october;
	private boolean november;
	private boolean december;

	/**
	 * Local property used to store the first occurrence of the schedule.
	 */
	private Date firstOccurrence;

	/**
	 * A comma delimited list of occurrences for a monthly weekday schedule.  For example if the schedule was the
	 * 1st and 3rd Thursday of the month the string would be "1,3".
	 */
	private String weekdayOccurrenceListString;
	private List<Integer> weekdayOccurrenceList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Calendar getCalendar() {
		return this.calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public ScheduleType getScheduleType() {
		return this.scheduleType;
	}


	public void setScheduleType(ScheduleType scheduleType) {
		this.scheduleType = scheduleType;
	}


	public String getBusinessDayConvention() {
		return this.businessDayConvention;
	}


	public void setBusinessDayConvention(String businessDayConvention) {
		this.businessDayConvention = businessDayConvention;
	}


	public boolean isRollupCalendarSchedule() {
		return this.rollupCalendarSchedule;
	}


	public void setRollupCalendarSchedule(boolean rollupCalendarSchedule) {
		this.rollupCalendarSchedule = rollupCalendarSchedule;
	}


	public List<Schedule> getChildScheduleIdList() {
		return this.childScheduleIdList;
	}


	public void setChildScheduleIdList(List<Schedule> childScheduleIdList) {
		this.childScheduleIdList = childScheduleIdList;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Time getStartTime() {
		return this.startTime;
	}


	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}


	public Time getEndTime() {
		return this.endTime;
	}


	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}


	public int getRecurrence() {
		return this.recurrence;
	}


	public void setRecurrence(int recurrence) {
		this.recurrence = recurrence;
	}


	public String getFrequency() {
		return this.frequency;
	}


	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}


	public Integer getDayOfMonth() {
		return this.dayOfMonth;
	}


	public void setDayOfMonth(Integer dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}


	public Integer getAdditionalBusinessDays() {
		return this.additionalBusinessDays;
	}


	public void setAdditionalBusinessDays(Integer additionalBusinessDays) {
		this.additionalBusinessDays = additionalBusinessDays;
	}


	public boolean isSunday() {
		return this.sunday;
	}


	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}


	public boolean isMonday() {
		return this.monday;
	}


	public void setMonday(boolean monday) {
		this.monday = monday;
	}


	public boolean isTuesday() {
		return this.tuesday;
	}


	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}


	public boolean isWednesday() {
		return this.wednesday;
	}


	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}


	public boolean isThursday() {
		return this.thursday;
	}


	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}


	public boolean isFriday() {
		return this.friday;
	}


	public void setFriday(boolean friday) {
		this.friday = friday;
	}


	public boolean isSaturday() {
		return this.saturday;
	}


	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}


	public boolean isCountFromLastDayOfMonth() {
		return this.countFromLastDayOfMonth;
	}


	public void setCountFromLastDayOfMonth(boolean countFromLastDayOfMonth) {
		this.countFromLastDayOfMonth = countFromLastDayOfMonth;
	}


	public boolean isJanuary() {
		return this.january;
	}


	public void setJanuary(boolean january) {
		this.january = january;
	}


	public boolean isFebruary() {
		return this.february;
	}


	public void setFebruary(boolean february) {
		this.february = february;
	}


	public boolean isMarch() {
		return this.march;
	}


	public void setMarch(boolean march) {
		this.march = march;
	}


	public boolean isApril() {
		return this.april;
	}


	public void setApril(boolean april) {
		this.april = april;
	}


	public boolean isMay() {
		return this.may;
	}


	public void setMay(boolean may) {
		this.may = may;
	}


	public boolean isJune() {
		return this.june;
	}


	public void setJune(boolean june) {
		this.june = june;
	}


	public boolean isJuly() {
		return this.july;
	}


	public void setJuly(boolean july) {
		this.july = july;
	}


	public boolean isAugust() {
		return this.august;
	}


	public void setAugust(boolean august) {
		this.august = august;
	}


	public boolean isSeptember() {
		return this.september;
	}


	public void setSeptember(boolean september) {
		this.september = september;
	}


	public boolean isOctober() {
		return this.october;
	}


	public void setOctober(boolean october) {
		this.october = october;
	}


	public boolean isNovember() {
		return this.november;
	}


	public void setNovember(boolean november) {
		this.november = november;
	}


	public boolean isDecember() {
		return this.december;
	}


	public void setDecember(boolean december) {
		this.december = december;
	}


	public Date getFirstOccurrence() {
		return this.firstOccurrence;
	}


	public void setFirstOccurrence(Date firstOccurrence) {
		this.firstOccurrence = firstOccurrence;
	}


	public String getWeekdayOccurrenceListString() {
		return this.weekdayOccurrenceListString;
	}


	public void setWeekdayOccurrenceListString(String weekdayOccurrenceListString) {
		this.weekdayOccurrenceListString = weekdayOccurrenceListString;
	}


	public List<Integer> getWeekdayOccurrenceList() {
		return this.weekdayOccurrenceList;
	}


	public void setWeekdayOccurrenceList(List<Integer> weekdayOccurrenceList) {
		this.weekdayOccurrenceList = weekdayOccurrenceList;
	}
}
