package com.clifton.calendar.schedule;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CalendarScheduleProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "calendar-schedule";
	}


	@Override
	protected void configureApprovedTestClassImports(@SuppressWarnings("unused") List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("java.beans.Introspector");
		imports.add("org.springframework.beans.");
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
