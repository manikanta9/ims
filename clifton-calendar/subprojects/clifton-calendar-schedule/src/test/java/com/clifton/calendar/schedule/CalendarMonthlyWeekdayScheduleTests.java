package com.clifton.calendar.schedule;

import com.clifton.calendar.schedule.executor.CalendarScheduleTestExecutor;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
@ContextConfiguration("CalendarScheduleTests-context.xml")
public class CalendarMonthlyWeekdayScheduleTests extends BaseCalendarScheduleTests {

	@Test
	public void testMonthlyWeekdayPreviousSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_PREVIOUS_SCHEDULE)
				.withOccurrencesBetween("12/01/2012 12:00 AM", "04/30/2013 12:00 AM"))
				.withExpectedResults(
						"12/07/2012 12:00 AM",
						"12/21/2012 12:00 AM",
						"01/04/2013 12:00 AM",
						"01/18/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"03/01/2013 12:00 AM",
						"03/15/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"04/18/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayPreviousSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_PREVIOUS_SCHEDULE)
				.withOccurrencesFromStartDate("04/30/2013 12:00 AM", -10))
				.withExpectedResults(
						"04/18/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"03/15/2013 12:00 AM",
						"03/01/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"01/18/2013 12:00 AM",
						"01/04/2013 12:00 AM",
						"12/21/2012 12:00 AM",
						"12/07/2012 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayPreviousModifiedSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_PREVIOUS_MODIFIED_SCHEDULE)
				.withOccurrencesBetween("12/01/2012 12:00 AM", "04/30/2013 12:00 AM"))
				.withExpectedResults(
						"12/03/2012 12:00 AM",
						"12/14/2012 12:00 AM",
						"01/04/2013 12:00 AM",
						"01/18/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayPreviousModifiedSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_PREVIOUS_MODIFIED_SCHEDULE)
				.withOccurrencesFromStartDate("04/30/2013 12:00 AM", -4))
				.withExpectedResults(
						"01/18/2013 12:00 AM",
						"01/04/2013 12:00 AM",
						"12/14/2012 12:00 AM",
						"12/03/2012 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdaySkipSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_SKIP_SCHEDULE)
				.withOccurrencesBetween("12/01/2012 12:00 AM", "04/30/2013 12:00 AM"))
				.withExpectedResults(
						"12/07/2012 12:00 AM",
						"12/21/2012 12:00 AM",
						"01/04/2013 12:00 AM",
						"01/18/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"03/01/2013 12:00 AM",
						"03/15/2013 12:00 AM",
						"04/05/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdaySkipSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_SKIP_SCHEDULE)
				.withOccurrencesFromStartDate("04/30/2013 12:00 AM", -9))
				.withExpectedResults(
						"04/05/2013 12:00 AM",
						"03/15/2013 12:00 AM",
						"03/01/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"01/18/2013 12:00 AM",
						"01/04/2013 12:00 AM",
						"12/21/2012 12:00 AM",
						"12/07/2012 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayNextSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_NEXT_SCHEDULE)
				.withOccurrencesBetween("12/01/2012 12:00 AM", "04/30/2013 12:00 AM"))
				.withExpectedResults(
						"12/07/2012 12:00 AM",
						"12/21/2012 12:00 AM",
						"01/04/2013 12:00 AM",
						"01/18/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"03/01/2013 12:00 AM",
						"03/15/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"04/22/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayNextSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_NEXT_SCHEDULE)
				.withOccurrencesFromStartDate("04/30/2013 12:00 AM", -10))
				.withExpectedResults(
						"04/22/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"03/15/2013 12:00 AM",
						"03/01/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"01/18/2013 12:00 AM",
						"01/04/2013 12:00 AM",
						"12/21/2012 12:00 AM",
						"12/07/2012 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayNextModifiedSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_NEXT_MODIFIED_SCHEDULE)
				.withOccurrencesBetween("12/01/2012 12:00 AM", "04/30/2013 12:00 AM"))
				.withExpectedResults(
						"03/11/2013 12:00 AM",
						"03/28/2013 12:00 AM",
						"04/15/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayNextModifiedSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_NEXT_MODIFIED_SCHEDULE)
				.withOccurrencesFromStartDate("04/30/2013 12:00 AM", -3))
				.withExpectedResults(
						"04/15/2013 12:00 AM",
						"03/28/2013 12:00 AM",
						"03/11/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayRecurringSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_RECURRING_SCHEDULE)
				.withOccurrencesBetween("12/02/2012 12:00 AM", "04/30/2013 12:00 AM"))
				.withExpectedResults(
						"12/07/2012 12:00 AM",
						"12/21/2012 12:00 AM",
						"02/01/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"04/18/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayRecurringSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_RECURRING_SCHEDULE)
				.withOccurrencesFromStartDate("04/30/2013 12:00 AM", -6))
				.withExpectedResults(
						"04/18/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"12/21/2012 12:00 AM",
						"12/07/2012 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayMultipleDaysRecurringSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_MULTIPLE_DAYS_RECURRING_SCHEDULE)
				.withOccurrencesBetween("12/01/2012 12:00 AM", "04/30/2013 12:00 AM"))
				.withExpectedResults(
						"12/06/2012 12:00 AM",
						"12/07/2012 12:00 AM",
						"12/20/2012 12:00 AM",
						"12/21/2012 12:00 AM",
						"01/03/2013 12:00 AM",
						"01/04/2013 12:00 AM",
						"01/17/2013 12:00 AM",
						"01/18/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"02/07/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"02/21/2013 12:00 AM",
						"03/01/2013 12:00 AM",
						"03/07/2013 12:00 AM",
						"03/15/2013 12:00 AM",
						"03/21/2013 12:00 AM",
						"04/04/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"04/18/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayMultipleDaysRecurringSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_MULTIPLE_DAYS_RECURRING_SCHEDULE)
				.withOccurrencesFromStartDate("04/30/2013 12:00 AM", -19))
				.withExpectedResults(
						"04/18/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"04/04/2013 12:00 AM",
						"03/21/2013 12:00 AM",
						"03/15/2013 12:00 AM",
						"03/07/2013 12:00 AM",
						"03/01/2013 12:00 AM",
						"02/21/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"02/07/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"01/18/2013 12:00 AM",
						"01/17/2013 12:00 AM",
						"01/04/2013 12:00 AM",
						"01/03/2013 12:00 AM",
						"12/21/2012 12:00 AM",
						"12/20/2012 12:00 AM",
						"12/07/2012 12:00 AM",
						"12/06/2012 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayThreeMonthRecurringSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_THREE_MONTH_RECURRING_SCHEDULE_ID)
				.withOccurrencesBetween("08/13/2012 12:00 AM", "07/11/2013 12:00 AM"))
				.withExpectedResults(
						"08/13/2012 12:00 AM",
						"11/09/2012 12:00 AM",
						"02/11/2013 12:00 AM",
						"05/13/2013 12:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_WEEKDAY_THREE_MONTH_RECURRING_SCHEDULE_ID)
				.withOccurrencesBetween("08/13/2012 12:00 AM", "09/11/2013 12:00 AM"))
				.withExpectedResults(
						"08/13/2012 12:00 AM",
						"11/09/2012 12:00 AM",
						"02/11/2013 12:00 AM",
						"05/13/2013 12:00 AM",
						"08/12/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayThreeMonthRecurringSchedule_PreviousOccurrences() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(MONTHLY_WEEKDAY_THREE_MONTH_RECURRING_SCHEDULE_ID);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate("07/11/2013 12:00 AM", -4))
				.withExpectedResults(
						"05/13/2013 12:00 AM",
						"02/11/2013 12:00 AM",
						"11/09/2012 12:00 AM",
						"08/13/2012 12:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate("09/11/2013 12:00 AM", -5))
				.withExpectedResults(
						"08/12/2013 12:00 AM",
						"05/13/2013 12:00 AM",
						"02/11/2013 12:00 AM",
						"11/09/2012 12:00 AM",
						"08/13/2012 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayBusinessDayAdjustmentEdgeCase() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(MONTHLY_WEEKDAY_THIRD_DAY_RECURRING_SCHEDULE_ID);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate("04/14/2022 12:01 AM", 1))
				.withExpectedResults(
						"05/20/2022 12:00 AM"
				).execute();
	}
}
