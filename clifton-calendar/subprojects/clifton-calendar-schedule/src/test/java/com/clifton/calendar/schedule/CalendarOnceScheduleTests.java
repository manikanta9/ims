package com.clifton.calendar.schedule;


import com.clifton.calendar.schedule.executor.CalendarScheduleTestExecutor;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import java.util.Date;


@ContextConfiguration("CalendarScheduleTests-context.xml")
public class CalendarOnceScheduleTests extends BaseCalendarScheduleTests {

	@Test
	public void testOnceDayLightSavings() {
		Date startDate = DateUtils.toDate("2011-03-13 3:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		startDate = DateUtils.toDate("2011-11-06 3:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("11/06/2011 4:15 AM").execute();
	}


	@Test
	public void testOnceDayLightSavings_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID)
				.withPreviousOccurrenceFromStartDate(DateUtils.toDate("2011-03-13 4:23:00", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID)
				.withPreviousOccurrenceFromStartDate(DateUtils.toDate("2011-11-06 4:23:00", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults("11/06/2011 4:15 AM").execute();
	}


	@Test
	public void testOnce_NextOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("09/30/2009 8:00 PM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("10/01/2009 4:00 PM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("10/01/2009 5:00 PM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("10/01/2009 5:01 PM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("10/02/2009 8:00 PM")).execute();
	}


	@Test
	public void testOnce_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("09/30/2009 8:00 PM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("10/01/2009 4:00 PM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("10/01/2009 5:00 PM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("10/01/2009 5:01 PM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(ONCE_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("10/02/2009 8:00 PM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();
	}
}
