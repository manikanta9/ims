package com.clifton.calendar.schedule;

import com.clifton.calendar.schedule.executor.CalendarScheduleTestExecutor;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;


@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
@ContextConfiguration("CalendarScheduleTests-context.xml")
public class CalendarYearlyScheduleTests extends BaseCalendarScheduleTests {

	@Test
	public void testNext6MonthRecurrenceSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_JAN_JUL_WITH_NEXT_BUSINESS_DAY_CONVENTION_SCHEDULE_ID)
				.withOccurrencesBetween("12/15/2013 12:00 AM", "01/15/2015 12:00 AM"))
				.withExpectedResults(
						"01/02/2014 8:00 AM",
						"07/01/2014 8:00 AM",
						"01/02/2015 8:00 AM"
				).execute();
	}


	@Test
	public void testNext6MonthRecurrenceSchedule_withoutTime() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_JAN_JUL_WITH_NEXT_BUSINESS_DAY_CONVENTION_SCHEDULE_WITHOUT_TIME_ID)
				.withOccurrencesBetween("12/15/2013 12:00 AM", "01/15/2015 12:00 AM"))
				.withExpectedResults(
						"01/02/2014 12:00 AM",
						"07/01/2014 12:00 AM",
						"01/02/2015 12:00 AM"
				).execute();
	}


	@Test
	public void testNext6MonthRecurrenceSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_JAN_JUL_WITH_NEXT_BUSINESS_DAY_CONVENTION_SCHEDULE_ID)
				.withOccurrencesFromStartDate("01/15/2015 12:00 AM", -3))
				.withExpectedResults(
						"01/02/2015 8:00 AM",
						"07/01/2014 8:00 AM",
						"01/02/2014 8:00 AM"
				).execute();
	}


	@Test
	public void testJulyFirstRecurrenceSchedule() {
		Date startDate = DateUtils.toDate("2014-05-07 15:50:07", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2014-05-07 16:50:07", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_JULY_SCHEDULE_ID)
				.withOccurrencesBetween(startDate, endDate)).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_JULY_SCHEDULE_ID)
				.withOccurrencesBetween("07/01/2014 6:30 AM", "07/01/2014 7:30 AM"))
				.withExpectedResults("07/01/2014 7:00 AM").execute();
	}


	@Test
	public void testJulyFirstRecurrenceSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_JULY_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate(DateUtils.toDate("2014-07-01 7:30:00", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults("07/01/2014 7:00 AM").execute();
	}


	@Test
	public void testYearlyDayLightSavings() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID)
				.withOccurrencesBetween("03/13/2011 3:23 AM", "03/13/2011 4:23 AM"))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID)
				.withOccurrencesBetween("03/01/2011 12:00 AM", "03/31/2011 12:00 AM"))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID)
				.withOccurrencesBetween("11/06/2011 3:23 AM", "11/06/2011 4:23 AM"))
				.withExpectedResults("11/06/2011 4:15 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID)
				.withOccurrencesBetween("11/01/2011 12:00 AM", "11/30/2011 12:00 AM"))
				.withExpectedResults("11/06/2011 4:15 AM").execute();
	}


	@Test
	public void testYearlyDayLightSavings_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID)
				.withPreviousOccurrenceFromStartDate("03/13/2011 4:23 AM"))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID)
				.withPreviousOccurrenceFromStartDate("03/31/2011 12:00 AM"))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID)
				.withPreviousOccurrenceFromStartDate("11/06/2011 4:23 AM"))
				.withExpectedResults("11/06/2011 4:15 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID)
				.withPreviousOccurrenceFromStartDate("11/30/2011 12:00 AM"))
				.withExpectedResults("11/06/2011 4:15 AM").execute();
	}


	@Test
	public void testYearlyDayOfMonth15() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withNextOccurrenceFromStartDate("11/15/2007 8:00 AM"))
				.withExpectedResults("01/15/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withNextOccurrenceFromStartDate("01/15/2008 8:00 AM"))
				.withExpectedResults("01/15/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withNextOccurrenceFromStartDate("01/15/2008 8:01 AM"))
				.withExpectedResults("04/15/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withNextOccurrenceFromStartDate("03/15/2009 8:01 AM"))
				.withExpectedResults("04/15/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesBetween("11/15/2007 12:00 AM", "12/31/2009 12:00 AM"))
				.withExpectedResults(
						"01/15/2008 8:00 AM",
						"04/15/2008 8:00 AM",
						"07/15/2008 8:00 AM",
						"10/15/2008 8:00 AM",
						"01/15/2009 8:00 AM",
						"04/15/2009 8:00 AM",
						"07/15/2009 8:00 AM",
						"10/15/2009 8:00 AM"
				).execute();
	}


	@Test
	public void testYearlyDayOfMonth15_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withPreviousOccurrenceFromStartDate("03/15/2008 8:00 AM"))
				.withExpectedResults("01/15/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withPreviousOccurrenceFromStartDate("01/15/2008 8:00 AM"))
				.withExpectedResults("01/15/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withPreviousOccurrenceFromStartDate("01/15/2008 7:59 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withPreviousOccurrenceFromStartDate("01/15/2009 7:59 AM"))
				.withExpectedResults("10/15/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withPreviousOccurrenceFromStartDate("05/15/2009 8:01 AM"))
				.withExpectedResults("04/15/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesFromStartDate("12/31/2009 12:00 AM", -8))
				.withExpectedResults(
						"10/15/2009 8:00 AM",
						"07/15/2009 8:00 AM",
						"04/15/2009 8:00 AM",
						"01/15/2009 8:00 AM",
						"10/15/2008 8:00 AM",
						"07/15/2008 8:00 AM",
						"04/15/2008 8:00 AM",
						"01/15/2008 8:00 AM"
				).execute();
	}


	@Test
	public void testYearlyDayOfMonth15_VariousBusinessDayConventions() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID);
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);
		schedule.setDayOfMonth(7);
		schedule.setMay(true);
		schedule.setApril(false);
		getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesBetween("01/01/2009 12:00 AM", "12/31/2009 12:00 AM"))
				.withExpectedResults(
						"05/07/2009 8:00 AM",
						"07/07/2009 8:00 AM",
						"10/07/2009 8:00 AM"
				).execute();

		schedule = setScheduleBusinessDayConvention(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID, BusinessDayConventions.NEXT);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesBetween("01/01/2009 12:00 AM", "12/31/2009 12:00 AM"))
				.withExpectedResults(
						"01/08/2009 8:00 AM",
						"05/07/2009 8:00 AM",
						"07/07/2009 8:00 AM",
						"10/07/2009 8:00 AM"
				).execute();

		schedule = setScheduleBusinessDayConvention(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID, BusinessDayConventions.PREVIOUS_MODIFIED);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withNextOccurrenceFromStartDate("01/06/2009 9:01 AM"))
				.withExpectedResults("05/07/2009 8:00 AM").execute();

		schedule = setScheduleBusinessDayConvention(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID, BusinessDayConventions.PREVIOUS);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesBetween("01/01/2009 12:00 AM", "12/31/2009 12:00 AM"))
				.withExpectedResults(
						"01/06/2009 8:00 AM",
						"05/07/2009 8:00 AM",
						"07/07/2009 8:00 AM",
						"10/07/2009 8:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesFromStartDate("01/06/2009 8:01 AM", 5))
				.withExpectedResults(
						"05/07/2009 8:00 AM",
						"07/07/2009 8:00 AM",
						"10/07/2009 8:00 AM"
				).execute();

		schedule.setRecurrence(2);
		getCalendarScheduleService().saveCalendarSchedule(schedule);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesBetween(DateUtils.toDate("01/01/2008"), DateUtils.toDate("12/31/2009")))
				.withExpectedResults(
						"01/07/2008 8:00 AM",
						"05/07/2008 8:00 AM",
						"07/07/2008 8:00 AM",
						"10/07/2008 8:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withNextOccurrenceFromStartDate("01/06/2009 9:01 AM")).execute();
	}


	@Test
	public void testYearlyDayOfMonth15_VariousBusinessDayConventions_PreviousOccurrences() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID);
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);
		schedule.setDayOfMonth(7);
		schedule.setMay(true);
		schedule.setApril(false);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesFromStartDate("12/31/2009 12:00 AM", -3))
				.withExpectedResults(
						"10/07/2009 8:00 AM",
						"07/07/2009 8:00 AM",
						"05/07/2009 8:00 AM"
				).execute();

		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesFromStartDate("12/31/2009 12:00 AM", -4))
				.withExpectedResults(
						"10/07/2009 8:00 AM",
						"07/07/2009 8:00 AM",
						"05/07/2009 8:00 AM",
						"01/08/2009 8:00 AM"
				).execute();

		schedule.setBusinessDayConvention(BusinessDayConventions.PREVIOUS_MODIFIED);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withPreviousOccurrenceFromStartDate("01/06/2009 9:01 AM"))
				.withExpectedResults("10/07/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withPreviousOccurrenceFromStartDate("01/07/2009 9:01 AM"))
				.withExpectedResults("01/06/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withPreviousOccurrenceFromStartDate("01/07/2009 7:59 AM"))
				.withExpectedResults("10/07/2008 8:00 AM").execute();

		schedule.setBusinessDayConvention(BusinessDayConventions.PREVIOUS);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesFromStartDate("12/31/2009 12:00 AM", -4))
				.withExpectedResults(
						"10/07/2009 8:00 AM",
						"07/07/2009 8:00 AM",
						"05/07/2009 8:00 AM",
						"01/06/2009 8:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesFromStartDate(DateUtils.toDate("12/06/2008 8:01 AM"), -5))
				.withExpectedResults(
						"10/07/2008 8:00 AM",
						"07/07/2008 8:00 AM",
						"05/07/2008 8:00 AM",
						"01/07/2008 8:00 AM"
				).execute();

		schedule.setRecurrence(2);
		getCalendarScheduleService().saveCalendarSchedule(schedule);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withOccurrencesFromStartDate("12/31/2009 12:00 AM", -4))
				.withExpectedResults(
						"10/07/2008 8:00 AM",
						"07/07/2008 8:00 AM",
						"05/07/2008 8:00 AM",
						"01/07/2008 8:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID)
				.withPreviousOccurrenceFromStartDate("01/06/2008 9:01 AM")).execute();
	}


	@Test
	public void testYearlyDecemberSchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_DECEMBER_SCHEDULE_ID)
				.withOccurrencesBetween("06/06/2008 12:00 AM", "06/06/2010 12:00 AM"))
				.withExpectedResults(
						"12/31/2008 12:00 AM",
						"12/31/2009 12:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_DECEMBER_SCHEDULE_2012_ID)
				.withOccurrencesBetween("12/31/2012 12:00 AM", "06/12/2013 12:00 AM"))
				.withExpectedResults("12/31/2012 12:00 AM").execute();
	}


	@Test
	public void testYearlyDecemberSchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_DECEMBER_SCHEDULE_ID)
				.withOccurrencesFromStartDate("06/06/2010 12:00 AM", -2))
				.withExpectedResults(
						"12/31/2009 12:00 AM",
						"12/31/2008 12:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(YEARLY_DECEMBER_SCHEDULE_2012_ID)
				.withPreviousOccurrenceFromStartDate("06/12/2013 12:00 AM"))
				.withExpectedResults("12/31/2012 12:00 AM").execute();
	}
}
