package com.clifton.calendar.schedule;

import com.clifton.calendar.CalendarHolidayUtils;
import com.clifton.calendar.schedule.executor.CalendarScheduleTestExecutor;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;


@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
@ContextConfiguration("CalendarScheduleTests-context.xml")
public class CalendarMonthlyScheduleTests extends BaseCalendarScheduleTests {

	@Test
	public void testMonthlySchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesBetween("01/01/2013 12:00 AM", "12/31/2013 12:00 AM"))
				.withExpectedResults(
						"01/31/2013 12:00 AM",
						"02/28/2013 12:00 AM",
						"03/28/2013 12:00 AM",
						"04/30/2013 12:00 AM",
						"05/31/2013 12:00 AM",
						"06/28/2013 12:00 AM",
						"07/31/2013 12:00 AM",
						"08/30/2013 12:00 AM",
						"09/30/2013 12:00 AM",
						"10/31/2013 12:00 AM",
						"11/29/2013 12:00 AM",
						"12/31/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlySchedule_withoutTime() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withOccurrencesBetween("01/01/2013 12:00 AM", "12/31/2013 12:00 AM"))
				.withExpectedResults(
						"01/31/2013 12:00 AM",
						"02/28/2013 12:00 AM",
						"03/28/2013 12:00 AM",
						"04/30/2013 12:00 AM",
						"05/31/2013 12:00 AM",
						"06/28/2013 12:00 AM",
						"07/31/2013 12:00 AM",
						"08/30/2013 12:00 AM",
						"09/30/2013 12:00 AM",
						"10/31/2013 12:00 AM",
						"11/29/2013 12:00 AM",
						"12/31/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlySchedule_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("12/31/2013 12:00 AM", -12))
				.withExpectedResults(
						"12/31/2013 12:00 AM",
						"11/29/2013 12:00 AM",
						"10/31/2013 12:00 AM",
						"09/30/2013 12:00 AM",
						"08/30/2013 12:00 AM",
						"07/31/2013 12:00 AM",
						"06/28/2013 12:00 AM",
						"05/31/2013 12:00 AM",
						"04/30/2013 12:00 AM",
						"03/28/2013 12:00 AM",
						"02/28/2013 12:00 AM",
						"01/31/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlySchedule_Recurrence6() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_6_WITH_NEXT_MODIFIED_BUSINESS_DAY_CONVENTION)
				.withOccurrencesBetween("08/01/2012 12:00 AM", "04/01/2013 12:00 AM"))
				.withExpectedResults("01/31/2013 12:00 AM").execute();
	}


	@Test
	public void testMonthlySchedule_Recurrence6_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_6_WITH_NEXT_MODIFIED_BUSINESS_DAY_CONVENTION)
				.withPreviousOccurrenceFromStartDate("04/01/2013 12:00 AM"))
				.withExpectedResults("01/31/2013 12:00 AM").execute();
	}


	@Test
	public void testMonthly_NextBusinessDayConvention() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("11/15/2008 8:00 AM"))
				.withExpectedResults("12/08/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/15/2008 8:00 AM"))
				.withExpectedResults("01/08/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/08/2008 8:01 AM"))
				.withExpectedResults("01/08/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/08/2009 8:00 AM"))
				.withExpectedResults("01/08/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("02/07/2009 8:01 AM"))
				.withExpectedResults("02/09/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("02/09/2009 8:01 AM"))
				.withExpectedResults("03/09/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("02/09/2009 8:00 AM"))
				.withExpectedResults("02/09/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("04/07/2009 8:01 AM"))
				.withExpectedResults("05/07/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("05/07/2009 8:01 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("06/02/2009 8:01 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2009 8:00 AM"))
				.withExpectedResults(
						"12/08/2008 8:00 AM",
						"01/08/2009 8:00 AM",
						"02/09/2009 8:00 AM",
						"03/09/2009 8:00 AM",
						"04/07/2009 8:00 AM",
						"05/07/2009 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthly_NextBusinessDayConvention_withoutTime() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("11/15/2008 8:00 AM"))
				.withExpectedResults("12/08/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/15/2008 8:00 AM"))
				.withExpectedResults("01/08/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/08/2008 12:01 AM"))
				.withExpectedResults("01/08/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/08/2009 12:00 AM"))
				.withExpectedResults("01/08/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("02/07/2009 12:01 AM"))
				.withExpectedResults("02/09/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("02/09/2009 12:01 AM"))
				.withExpectedResults("03/09/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("02/09/2009 12:00 AM"))
				.withExpectedResults("02/09/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("04/07/2009 12:01 AM"))
				.withExpectedResults("05/07/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("05/07/2009 12:01 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("06/02/2009 12:01 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2009 8:00 AM"))
				.withExpectedResults(
						"12/08/2008 12:00 AM",
						"01/08/2009 12:00 AM",
						"02/09/2009 12:00 AM",
						"03/09/2009 12:00 AM",
						"04/07/2009 12:00 AM",
						"05/07/2009 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthly_NextBusinessDayConvention_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/08/2008 7:59 AM")).execute(); // goes back to before the schedule start date

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/08/2008 8:00 AM"))
				.withExpectedResults("12/08/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/07/2009 7:59 AM"))
				.withExpectedResults("12/08/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/08/2009 8:00 AM"))
				.withExpectedResults("01/08/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("02/09/2009 7:59 AM"))
				.withExpectedResults("01/08/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("02/09/2009 8:00 AM"))
				.withExpectedResults("02/09/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("02/09/2009 8:01 AM"))
				.withExpectedResults("02/09/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("06/07/2009 7:59 AM"))
				.withExpectedResults("05/07/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("05/31/2009 8:00 AM", -6))
				.withExpectedResults(
						"05/07/2009 8:00 AM",
						"04/07/2009 8:00 AM",
						"03/09/2009 8:00 AM",
						"02/09/2009 8:00 AM",
						"01/08/2009 8:00 AM",
						"12/08/2008 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthly_PreviousBusinessDayConvention() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID, BusinessDayConventions.PREVIOUS);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/07/2009 7:00 AM"))
				.withExpectedResults("02/06/2009 8:00 AM").execute();
	}


	@Test
	public void testMonthly_PreviousBusinessDayConvention_PreviousOccurrences() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID, BusinessDayConventions.PREVIOUS);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("02/07/2009 8:01 AM"))
				.withExpectedResults("02/06/2009 8:00 AM").execute();
	}


	@Test
	public void testMonthly_SkipBusinessDayConvention() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID, BusinessDayConventions.SKIP);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/01/2009 8:00 AM"))
				.withExpectedResults("04/07/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2009 8:00 AM"))
				.withExpectedResults(
						"04/07/2009 8:00 AM",
						"05/07/2009 8:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesBetween("01/15/2008 8:00 AM", "01/28/2009 8:00 AM")).execute();
	}


	@Test
	public void testMonthly_SkipBusinessDayConvention_PreviousOccurrences() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID, BusinessDayConventions.SKIP);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("04/07/2009 7:59 AM")).execute(); // skips all weekends and Jan 7th is not a business day

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("05/31/2009 8:00 AM", -3)) // only two occurrences due to SKIP convention
				.withExpectedResults(
						"05/07/2009 8:00 AM",
						"04/07/2009 8:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/31/2008 7:59 AM")).execute();
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("11/15/2008 8:00 AM"))
				.withExpectedResults("12/31/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 8:01 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/31/2008 8:01 AM"))
				.withExpectedResults("12/31/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("02/28/2009 8:15 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("03/31/2009 7:59 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months_NextBusinessDayConvention() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, BusinessDayConventions.NEXT);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 8:01 AM"))
				.withExpectedResults("03/02/2009 8:00 AM").execute();
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months_NextBusinessDayConvention_PreviousOccurrences() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, BusinessDayConventions.NEXT);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("04/02/2009 8:00 AM"))
				.withExpectedResults("03/02/2009 8:00 AM").execute();
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months_NextModifiedBusinessDayConvention() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, BusinessDayConventions.NEXT_MODIFIED);
		short holidayId = addCalendarHoliday(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, "Test Holiday", "12/31/2008");

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/30/2008 8:01 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/30/2008 8:00 AM"))
				.withExpectedResults("12/30/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/30/2008 7:01 AM"))
				.withExpectedResults("12/30/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/30/2008 8:01 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("02/27/2009 8:00 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();

		CalendarHolidayUtils.removeCalendarHolidayAndDays(this.getCalendarHolidayService(), holidayId);
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months_NextModifiedBusinessDayConvention_PreviousOccurrences() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, BusinessDayConventions.NEXT_MODIFIED);
		short holidayId = addCalendarHoliday(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, "Test Holiday", "12/31/2008");

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("03/30/2009 8:00 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/30/2008 8:00 AM"))
				.withExpectedResults("12/30/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/30/2008 7:01 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("02/27/2009 8:00 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();

		CalendarHolidayUtils.removeCalendarHolidayAndDays(this.getCalendarHolidayService(), holidayId);
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months_SkipBusinessDayConvention() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, BusinessDayConventions.SKIP);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 7:01 AM"))
				.withExpectedResults("12/31/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 8:01 AM"))
				.withExpectedResults("04/30/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("02/01/2009 12:00 AM"))
				.withExpectedResults("04/30/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("03/31/2009 12:00 AM"))
				.withExpectedResults("04/30/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 8:01 AM"))
				.withExpectedResults("04/30/2009 8:00 AM").execute();
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months_SkipBusinessDayConvention_PreviousOccurrences() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, BusinessDayConventions.SKIP);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/31/2008 8:01 AM"))
				.withExpectedResults("12/31/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("04/30/2009 7:59 AM"))
				.withExpectedResults("12/31/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("03/31/2009 12:00 AM"))
				.withExpectedResults("12/31/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("04/30/2009 7:59 AM"))
				.withExpectedResults("12/31/2008 8:00 AM").execute();
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months_PreviousBusinessDayConvention() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, BusinessDayConventions.PREVIOUS);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2009 8:00 AM"))
				.withExpectedResults(
						"12/31/2008 8:00 AM",
						"02/27/2009 8:00 AM",
						"04/30/2009 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthEndScheduleRecurringEvery2Months_PreviousBusinessDayConvention_PreviousOccurrences() {
		setScheduleBusinessDayConvention(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID, BusinessDayConventions.PREVIOUS);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("05/31/2009 8:00 AM", -3))
				.withExpectedResults(
						"04/30/2009 8:00 AM",
						"02/27/2009 8:00 AM",
						"12/31/2008 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyDayLightSavings_Spring() {
		Date startDate = DateUtils.toDate("2011-03-13 3:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		startDate = DateUtils.toDate("2011-03-01 0:00:00", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2011-03-31 0:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("03/13/2011 4:15 AM").execute();
	}


	@Test
	public void testMonthlyDayLightSavings_Fall() {
		Date startDate = DateUtils.toDate("2011-11-06 3:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("11/06/2011 4:15 AM").execute();

		startDate = DateUtils.toDate("2011-11-01 0:00:00", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2011-11-30 0:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("11/06/2011 4:15 AM").execute();
	}


	@Test
	public void testMonthlyDayLightSavings_Spring_withoutTime() {
		Date startDate = DateUtils.toDate("2011-03-12 23:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_WITHOUT_TIME_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("03/13/2011 12:00 AM").execute();

		startDate = DateUtils.toDate("2011-03-01 0:00:00", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2011-03-31 0:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_WITHOUT_TIME_ID)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("03/13/2011 12:00 AM").execute();
	}


	@Test
	public void testMonthlyDayLightSavings_Fall_withoutTime() {
		Date startDate = DateUtils.toDate("2011-11-05 23:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_WITHOUT_TIME_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("11/06/2011 12:00 AM").execute();

		startDate = DateUtils.toDate("2011-11-01 0:00:00", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2011-11-30 0:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_WITHOUT_TIME_ID)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("11/06/2011 12:00 AM").execute();
	}


	@Test
	public void testBusinessDayScheduleStartOfMonth_NextOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withNextOccurrenceFromStartDate("12/01/2008 8:00 AM"))
				.withExpectedResults("12/03/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withNextOccurrenceFromStartDate("01/01/2009 8:00 AM"))
				.withExpectedResults("01/06/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withNextOccurrenceFromStartDate("01/06/2009 8:01 AM"))
				.withExpectedResults("02/04/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withNextOccurrenceFromStartDate("02/04/2009 8:01 AM"))
				.withExpectedResults("03/04/2009 8:00 AM").execute();

		CalendarSchedule calendarSchedule = this.getCalendarScheduleService().getCalendarSchedule(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID);
		calendarSchedule.setAdditionalBusinessDays(1);
		calendarSchedule.setEndDate(DateUtils.toDate("07/15/2009"));
		this.getCalendarScheduleService().saveCalendarSchedule(calendarSchedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withNextOccurrenceFromStartDate("06/02/2009 8:00 AM"))
				.withExpectedResults("06/02/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withNextOccurrenceFromStartDate("06/02/2009 7:59 AM"))
				.withExpectedResults("06/02/2009 8:00 AM").execute();
	}


	@Test
	public void testBusinessDayScheduleStartOfMonth_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("12/03/2008 8:01 AM"))
				.withExpectedResults("12/03/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("02/01/2009 7:59 AM"))
				.withExpectedResults("01/06/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("03/04/2009 7:59 AM"))
				.withExpectedResults("02/04/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("03/04/2009 8:00 AM"))
				.withExpectedResults("03/04/2009 8:00 AM").execute();

		CalendarSchedule calendarSchedule = this.getCalendarScheduleService().getCalendarSchedule(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID);
		calendarSchedule.setAdditionalBusinessDays(1);
		calendarSchedule.setEndDate(DateUtils.toDate("07/15/2009"));
		this.getCalendarScheduleService().saveCalendarSchedule(calendarSchedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("06/02/2009 8:01 AM"))
				.withExpectedResults("06/02/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("06/02/2009 8:00 AM"))
				.withExpectedResults("06/02/2009 8:00 AM").execute();
	}


	@Test
	public void testAdditionalBusinessDaysMonthly_NextOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_PREVIOUS_BUSINESS_DAYS_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("04/01/2009 12:01 AM"))
				.withExpectedResults("04/30/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_PREVIOUS_BUSINESS_DAYS_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("04/30/2009 12:00 AM"))
				.withExpectedResults("04/30/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_PREVIOUS_BUSINESS_DAYS_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("04/30/2009 12:01 AM"))
				.withExpectedResults("05/29/2009 12:00 AM").execute();
	}


	@Test
	public void testAdditionalBusinessDaysMonthly_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_PREVIOUS_BUSINESS_DAYS_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("05/01/2009 12:00 AM"))
				.withExpectedResults("04/30/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_PREVIOUS_BUSINESS_DAYS_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("04/30/2009 12:00 AM"))
				.withExpectedResults("04/30/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_PREVIOUS_BUSINESS_DAYS_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("06/29/2009 11:59 AM"))
				.withExpectedResults("05/29/2009 12:00 AM").execute();

		setScheduleAdditionalBusinessDays(MONTHLY_PREVIOUS_BUSINESS_DAYS_SCHEDULE_ID, -2);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_PREVIOUS_BUSINESS_DAYS_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("03/30/2009 12:30 AM"))
				.withExpectedResults("03/30/2009 12:00 AM").execute();
	}


	@Test
	public void testBusinessDayScheduleEndOfMonth_NextOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_END_OF_MONTH_ID)
				.withNextOccurrenceFromStartDate("12/01/2008 8:00 AM"))
				.withExpectedResults("12/29/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_END_OF_MONTH_ID)
				.withNextOccurrenceFromStartDate("01/01/2009 8:00 AM"))
				.withExpectedResults("03/27/2009 8:00 AM").execute();
	}


	@Test
	public void testBusinessDayScheduleEndOfMonth_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_END_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("01/01/2009 8:00 AM"))
				.withExpectedResults("12/29/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_END_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("12/29/2008 8:00 AM"))
				.withExpectedResults("12/29/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_END_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("12/29/2008 7:59 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_BUSINESS_DAY_SCHEDULE_END_OF_MONTH_ID)
				.withPreviousOccurrenceFromStartDate("03/27/2009 7:59 AM"))
				.withExpectedResults("12/29/2008 8:00 AM").execute();
	}


	@Test
	public void testMonthlyScheduleWithCountBackFromLastDayOfMonth() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_COUNT_FROM_LAST_DAY_PREVIOUS_BUSINESS_DAY_ID)
				.withOccurrencesBetween(DateUtils.toDate("12/01/2008 12:00 AM"), DateUtils.toDate("12/01/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT)))
				.withExpectedResults(
						"12/02/2008 12:00 AM",
						"01/02/2009 12:00 AM",
						"01/30/2009 12:00 AM",
						"03/02/2009 12:00 AM",
						"04/01/2009 12:00 AM",
						"05/01/2009 12:00 AM",
						"06/01/2009 12:00 AM",
						"07/02/2009 12:00 AM",
						"07/31/2009 12:00 AM",
						"09/01/2009 12:00 AM",
						"10/02/2009 12:00 AM",
						"10/30/2009 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleWithCountBackFromLastDayOfMonth_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_COUNT_FROM_LAST_DAY_PREVIOUS_BUSINESS_DAY_ID)
				.withOccurrencesFromStartDate("12/01/2009 12:00 AM", -12))
				.withExpectedResults(
						"10/30/2009 12:00 AM",
						"10/02/2009 12:00 AM",
						"09/01/2009 12:00 AM",
						"07/31/2009 12:00 AM",
						"07/02/2009 12:00 AM",
						"06/01/2009 12:00 AM",
						"05/01/2009 12:00 AM",
						"04/01/2009 12:00 AM",
						"03/02/2009 12:00 AM",
						"01/30/2009 12:00 AM",
						"01/02/2009 12:00 AM",
						"12/02/2008 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyOccurrence_NextOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_RECURRENCE_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("12/01/2008 8:00 AM"))
				.withExpectedResults("12/31/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_RECURRENCE_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("01/30/2009 8:00 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_RECURRENCE_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("02/27/2009 8:01 AM"))
				.withExpectedResults("04/30/2009 8:00 AM").execute();
	}


	@Test
	public void testMonthlyOccurrence_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_RECURRENCE_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("01/31/2009 8:00 AM"))
				.withExpectedResults("12/31/2008 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_RECURRENCE_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("03/30/2009 8:00 AM"))
				.withExpectedResults("02/27/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_RECURRENCE_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("06/01/2009 8:01 AM"))
				.withExpectedResults("04/30/2009 8:00 AM").execute();
	}


	@Test
	public void testMonthlyScheduleNoCalendarWithAdditionalBusinessDays() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_NO_CALENDAR_WITH_ADDITIONAL_BUSINESS_DAYS_ID)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2009 8:00 AM"))
				.withExpectedResults(
						"12/07/2008 8:00 AM",
						"01/07/2009 8:00 AM",
						"02/07/2009 8:00 AM",
						"03/07/2009 8:00 AM",
						"04/07/2009 8:00 AM",
						"05/07/2009 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleNoCalendarWithAdditionalBusinessDays_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_NO_CALENDAR_WITH_ADDITIONAL_BUSINESS_DAYS_ID)
				.withOccurrencesFromStartDate("05/31/2009 8:00 AM", -6))
				.withExpectedResults(
						"05/07/2009 8:00 AM",
						"04/07/2009 8:00 AM",
						"03/07/2009 8:00 AM",
						"02/07/2009 8:00 AM",
						"01/07/2009 8:00 AM",
						"12/07/2008 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleWithCalendarNoAdditionalBusinessDays() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_WITH_CALENDAR_NO_ADDITIONAL_BUSINESS_DAYS_ID)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2009 8:00 AM"))
				.withExpectedResults(
						"12/08/2008 8:00 AM",
						"01/08/2009 8:00 AM",
						"02/09/2009 8:00 AM",
						"03/09/2009 8:00 AM",
						"04/07/2009 8:00 AM",
						"05/07/2009 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleWithCalendarNoAdditionalBusinessDays_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_WITH_CALENDAR_NO_ADDITIONAL_BUSINESS_DAYS_ID)
				.withOccurrencesFromStartDate("05/31/2009 8:00 AM", -6))
				.withExpectedResults(
						"05/07/2009 8:00 AM",
						"04/07/2009 8:00 AM",
						"03/09/2009 8:00 AM",
						"02/09/2009 8:00 AM",
						"01/08/2009 8:00 AM",
						"12/08/2008 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleNoCalendarNoAdditionalBusinessDays() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_NO_CALENDAR_NO_ADDITIONAL_BUSINESS_DAYS_ID)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2009 8:00 AM"))
				.withExpectedResults(
						"12/07/2008 8:00 AM",
						"01/07/2009 8:00 AM",
						"02/07/2009 8:00 AM",
						"03/07/2009 8:00 AM",
						"04/07/2009 8:00 AM",
						"05/07/2009 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleNoCalendarNoAdditionalBusinessDays_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_NO_CALENDAR_NO_ADDITIONAL_BUSINESS_DAYS_ID)
				.withOccurrencesFromStartDate("05/31/2009 8:00 AM", -6))
				.withExpectedResults(
						"05/07/2009 8:00 AM",
						"04/07/2009 8:00 AM",
						"03/07/2009 8:00 AM",
						"02/07/2009 8:00 AM",
						"01/07/2009 8:00 AM",
						"12/07/2008 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleWithCalendarWithAdditionalBusinessDays() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_WITH_CALENDAR_WITH_ADDITIONAL_BUSINESS_DAYS_ID)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2009 8:00 AM"))
				.withExpectedResults(
						"12/10/2008 8:00 AM",
						"01/12/2009 8:00 AM",
						"02/11/2009 8:00 AM",
						"03/11/2009 8:00 AM",
						"04/09/2009 8:00 AM",
						"05/11/2009 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleWithCalendarWithAdditionalBusinessDays_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_WITH_CALENDAR_WITH_ADDITIONAL_BUSINESS_DAYS_ID)
				.withOccurrencesFromStartDate("05/31/2009 8:00 AM", -6))
				.withExpectedResults(
						"05/11/2009 8:00 AM",
						"04/09/2009 8:00 AM",
						"03/11/2009 8:00 AM",
						"02/11/2009 8:00 AM",
						"01/12/2009 8:00 AM",
						"12/10/2008 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleWithNextOccurrenceEarlierDayOfMonthThanPreviousOccurrence() {
		// Test counting from last business day.
		setScheduleAdditionalBusinessDays(MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_ID, 4);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("06/01/2012 8:00 AM", 10))
				.withExpectedResults(
						"06/06/2012 12:00 AM",
						"07/06/2012 12:00 AM",
						"08/06/2012 12:00 AM",
						"09/07/2012 12:00 AM",
						"10/04/2012 12:00 AM",
						"11/06/2012 12:00 AM",
						"12/06/2012 12:00 AM",
						"01/07/2013 12:00 AM",
						"02/06/2013 12:00 AM",
						"03/06/2013 12:00 AM"
				).execute();

		// Test without counting from last day of month. Result has 6 occurrences instead of 7 because there are only 6 occurrences between 12/1/2008 and 6/1/2009.
		setScheduleAdditionalBusinessDays(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID, -6);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("11/01/2008 8:00 AM", 7))
				.withExpectedResults(
						"12/29/2008 8:00 AM",
						"01/30/2009 8:00 AM",
						"02/27/2009 8:00 AM",
						"03/30/2009 8:00 AM",
						"04/29/2009 8:00 AM",
						"05/29/2009 8:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleWithNextOccurrenceEarlierDayOfMonthThanPreviousOccurrence_PreviousOccurrences() {
		// Test counting from last business day.
		setScheduleAdditionalBusinessDays(MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_ID, 4);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("05/06/2014 8:00 AM", -10))
				.withExpectedResults(
						"05/06/2014 12:00 AM",
						"04/04/2014 12:00 AM",
						"03/06/2014 12:00 AM",
						"02/06/2014 12:00 AM",
						"01/07/2014 12:00 AM",
						"12/05/2013 12:00 AM",
						"11/06/2013 12:00 AM",
						"10/04/2013 12:00 AM",
						"09/06/2013 12:00 AM",
						"08/06/2013 12:00 AM"
				).execute();

		// Test without counting from last day of month. Result has 6 occurrences instead of 7 because there are only 6 occurrences between 12/1/2008 and 6/1/2009.
		setScheduleAdditionalBusinessDays(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID, -6);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("07/01/2009 8:00 AM", -7))
				.withExpectedResults(
						"05/29/2009 8:00 AM",
						"04/29/2009 8:00 AM",
						"03/30/2009 8:00 AM",
						"02/27/2009 8:00 AM",
						"01/30/2009 8:00 AM",
						"12/29/2008 8:00 AM"
				).execute();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarSchedule setScheduleAdditionalBusinessDays(int id, int additionalBusinessDays) {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(id);
		schedule.setAdditionalBusinessDays(additionalBusinessDays);
		return getCalendarScheduleService().saveCalendarSchedule(schedule);
	}
}
