package com.clifton.calendar.schedule;

import com.clifton.calendar.CalendarHolidayUtils;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.hibernate.Criteria;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ExtendWith(SpringExtension.class)
public abstract class BaseCalendarScheduleTests {

	protected static final int ONCE_SCHEDULE_ID = 100;
	protected static final int ONCE_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID = 101;
	protected static final int ONCE_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID = 102;

	protected static final int HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID = 111;
	protected static final int HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID = 112;
	protected static final int HOURLY_SCHEDULE_AT_0_MINUTES_ID = 113;
	protected static final int HOURLY_SCHEDULE_DAYLIGHT_SAVINGS_ID = 114;

	protected static final int DAILY_SCHEDULE_ID = 121;
	protected static final int DAILY_SCHEDULE_WITHOUT_TIME_ID = 128;
	protected static final int DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID = 122;
	protected static final int DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID = 129;
	protected static final int DAILY_MARKET_DATA_SCHEDULE_ID = 123;
	protected static final int DAILY_SCHEDULE_DAYLIGHT_SAVINGS_ID = 124;
	protected static final int DAILY_SCHEDULE_WITH_ENTITY_MODIFY_CONDITION_ID = 125;
	protected static final int DAILY_SCHEDULE_WITHOUT_HOLIDAYS_ID = 126;
	protected static final int DAILY_SCHEDULE_WITHOUT_HOLIDAYS_TIME_NOT_SUPPORTED_ID = 127;

	protected static final int WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID = 131;
	protected static final int WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID = 138;
	protected static final int WEEKLY_SCHEDULE_2_NEXT_DAY_CONVENTION_MWF_ID = 132;
	protected static final int WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID = 133;
	protected static final int WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID = 134;
	protected static final int WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_ID = 135;
	protected static final int WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_WITHOUT_TIME_ID = 137;
	protected static final int WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_LATEST_ID = 136;

	protected static final int MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_ID = 140;
	protected static final int MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID = 155;
	protected static final int MONTHLY_SCHEDULE_RECURRENCE_6_WITH_NEXT_MODIFIED_BUSINESS_DAY_CONVENTION = 141;
	protected static final int MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_ID = 142;
	protected static final int MONTHLY_SCHEDULE_DAY_OF_MONTH_7_WITH_NEXT_BUSINESS_DAY_CONVENTION_WITHOUT_TIME_ID = 156;
	protected static final int MONTHLY_SCHEDULE_RECURRENCE_2_WITH_PREVIOUS_DAY_CONVENTION_ID = 143;
	protected static final int MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID = 144;
	protected static final int MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID = 145;
	protected static final int MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_WITHOUT_TIME_ID = 157;
	protected static final int MONTHLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_WITHOUT_TIME_ID = 158;
	protected static final int MONTHLY_BUSINESS_DAY_SCHEDULE_START_OF_MONTH_ID = 146;
	protected static final int MONTHLY_BUSINESS_DAY_SCHEDULE_END_OF_MONTH_ID = 147;
	protected static final int MONTHLY_COUNT_FROM_LAST_DAY_PREVIOUS_BUSINESS_DAY_ID = 148;
	protected static final int MONTHLY_RECURRENCE_SCHEDULE_ID = 149;
	protected static final int MONTHLY_PREVIOUS_BUSINESS_DAYS_SCHEDULE_ID = 150;
	protected static final int MONTHLY_SCHEDULE_NO_CALENDAR_WITH_ADDITIONAL_BUSINESS_DAYS_ID = 151;
	protected static final int MONTHLY_SCHEDULE_WITH_CALENDAR_NO_ADDITIONAL_BUSINESS_DAYS_ID = 152;
	protected static final int MONTHLY_SCHEDULE_NO_CALENDAR_NO_ADDITIONAL_BUSINESS_DAYS_ID = 153;
	protected static final int MONTHLY_SCHEDULE_WITH_CALENDAR_WITH_ADDITIONAL_BUSINESS_DAYS_ID = 154;

	protected static final int MONTHLY_WEEKDAY_PREVIOUS_SCHEDULE = 160;
	protected static final int MONTHLY_WEEKDAY_PREVIOUS_MODIFIED_SCHEDULE = 161;
	protected static final int MONTHLY_WEEKDAY_SKIP_SCHEDULE = 162;
	protected static final int MONTHLY_WEEKDAY_NEXT_SCHEDULE = 163;
	protected static final int MONTHLY_WEEKDAY_NEXT_MODIFIED_SCHEDULE = 164;
	protected static final int MONTHLY_WEEKDAY_RECURRING_SCHEDULE = 165;
	protected static final int MONTHLY_WEEKDAY_MULTIPLE_DAYS_RECURRING_SCHEDULE = 166;
	protected static final int MONTHLY_WEEKDAY_THREE_MONTH_RECURRING_SCHEDULE_ID = 167;
	protected static final int MONTHLY_WEEKDAY_THIRD_DAY_RECURRING_SCHEDULE_ID = 168;

	protected static final int YEARLY_JAN_JUL_WITH_NEXT_BUSINESS_DAY_CONVENTION_SCHEDULE_ID = 170;
	protected static final int YEARLY_JAN_JUL_WITH_NEXT_BUSINESS_DAY_CONVENTION_SCHEDULE_WITHOUT_TIME_ID = 177;
	protected static final int YEARLY_JULY_SCHEDULE_ID = 171;
	protected static final int YEARLY_SCHEDULE_DAY_OF_MONTH_15_ID = 172;
	protected static final int YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_ID = 173;
	protected static final int YEARLY_SCHEDULE_DAYLIGHT_SAVINGS_FALL_ID = 174;
	protected static final int YEARLY_DECEMBER_SCHEDULE_ID = 175;
	protected static final int YEARLY_DECEMBER_SCHEDULE_2012_ID = 176;

	protected static final int SCHEDULE_ROLLUP_ID = 181;
	protected static final int SCHEDULE_ROLLUP_OF_ROLLUP_ID = 182;

	protected static final short STANDARD_CALENDAR_SCHEDULE_TYPE_ID = 1;
	protected static final short STANDARD_CALENDAR_SCHEDULE_TYPE_WITHOUT_TIME_ID = 2;


	@Resource
	private CalendarScheduleService calendarScheduleService;
	@Resource
	private CalendarHolidayService calendarHolidayService;
	@Resource
	private AdvancedUpdatableDAO<CalendarHolidayDay, Criteria> calendarHolidayDayDAO;
	@Resource
	private SecurityAuthorizationService securityAuthorizationService;
	@Resource
	private CalendarSetupService calendarSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected CalendarSchedule setScheduleBusinessDayConvention(int id, BusinessDayConventions action) {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(id);
		schedule.setBusinessDayConvention(action);
		return getCalendarScheduleService().saveCalendarSchedule(schedule);
	}


	protected short addCalendarHoliday(int scheduleId, String holidayName, String holidayDate) {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(scheduleId);
		return CalendarHolidayUtils.createCalendarHolidayWithDayForSchedule(getCalendarHolidayService(), schedule.getCalendar(), holidayName, holidayDate).getId();
	}


	protected void setupTestAsAdmin(boolean isAdmin) {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(isAdmin);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarScheduleService getCalendarScheduleService() {
		return this.calendarScheduleService;
	}


	public void setCalendarScheduleService(CalendarScheduleService calendarScheduleService) {
		this.calendarScheduleService = calendarScheduleService;
	}


	public CalendarHolidayService getCalendarHolidayService() {
		return this.calendarHolidayService;
	}


	public void setCalendarHolidayService(CalendarHolidayService calendarHolidayService) {
		this.calendarHolidayService = calendarHolidayService;
	}


	public AdvancedUpdatableDAO<CalendarHolidayDay, Criteria> getCalendarHolidayDayDAO() {
		return this.calendarHolidayDayDAO;
	}


	public void setCalendarHolidayDayDAO(AdvancedUpdatableDAO<CalendarHolidayDay, Criteria> calendarHolidayDayDAO) {
		this.calendarHolidayDayDAO = calendarHolidayDayDAO;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
