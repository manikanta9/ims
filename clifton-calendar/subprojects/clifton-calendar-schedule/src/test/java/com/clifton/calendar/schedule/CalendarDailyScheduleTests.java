package com.clifton.calendar.schedule;


import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.schedule.executor.CalendarScheduleTestExecutor;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import java.util.Date;


@ContextConfiguration("CalendarScheduleTests-context.xml")
public class CalendarDailyScheduleTests extends BaseCalendarScheduleTests {

	@Test
	public void testScheduleStartOnDayLightSavings() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(DAILY_SCHEDULE_WITHOUT_HOLIDAYS_ID);
		schedule.setStartDate(DateUtils.toDate("03/08/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-06-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/19/2015 12:59 AM", "08/19/2015 1:59 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/19/2015 1:59 AM", "08/19/2015 2:59 AM"))
				.withExpectedResults("08/19/2015 2:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/20/2015 12:59 AM", "08/20/2015 1:59 AM"))
				.withExpectedResults().execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/20/2015 1:59 AM", "08/20/2015 2:59 AM"))
				.withExpectedResults("08/20/2015 2:00 AM").execute();
	}


	@Test
	public void testDailyDayLightSavings() {
		Date startDate = DateUtils.toDate("2011-03-13 3:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		startDate = DateUtils.toDate("2011-11-06 3:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("11/06/2011 4:15 AM").execute();
	}


	@Test
	public void testEveryWeekDayAt2AM() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(DAILY_SCHEDULE_WITHOUT_HOLIDAYS_ID);
		schedule.setStartDate(DateUtils.toDate("03/08/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-06-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/19/2015 12:59 AM", "08/19/2015 1:59 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/19/2015 1:59 AM", "08/19/2015 2:59 AM"))
				.withExpectedResults("08/19/2015 2:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/20/2015 12:59 AM", "08/20/2015 1:59 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/20/2015 1:59 AM", "08/20/2015 2:59 AM"))
				.withExpectedResults("08/20/2015 2:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("12/17/2015 1:59 AM", "12/17/2015 2:59 AM"))
				.withExpectedResults("12/17/2015 2:00 AM").execute();
	}


	@Test
	public void testEveryWeekDay_withoutTime() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(DAILY_SCHEDULE_WITHOUT_HOLIDAYS_TIME_NOT_SUPPORTED_ID);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/19/2015 12:00 AM", "08/19/2015 11:59 PM"))
				.withExpectedResults("08/19/2015 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/20/2015 12:00 AM", "08/20/2015  11:59 PM"))
				.withExpectedResults("08/20/2015 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("12/17/2015 12:00 AM", "12/17/2015  11:59 PM"))
				.withExpectedResults("12/17/2015 12:00 AM").execute();
	}


	@Test
	public void testEveryBusinessDayAt1230pm_UNADJUSTED() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setBusinessDayConvention(BusinessDayConventions.UNADJUSTED);
		schedule.setName("Every Business Day at 12:30 pm");
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(11);
		schedule.setStartDate(DateUtils.toDate("03/09/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-03-09 12:30:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		Date startDate = DateUtils.toDate("2015-03-13 4:14:26", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2015-03-13 5:14:26", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("03/09/2015 12:00 AM", "03/22/2015 12:00 AM"))
				.withExpectedResults(
						"03/09/2015 12:30 PM",
						"03/20/2015 12:30 PM"
				).execute();

		startDate = DateUtils.toDate("2015-03-19 12:14:26", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2015-03-19 1:14:26", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();


		startDate = DateUtils.toDate("2015-03-20 12:14:26", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2015-03-20 13:14:26", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("03/20/2015 12:30 PM").execute();
	}


	@Test
	public void testEveryBusinessDayWithoutTime_UNADJUSTED() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setBusinessDayConvention(BusinessDayConventions.UNADJUSTED);
		schedule.setName("Every Business Day");
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(11);
		schedule.setStartDate(DateUtils.toDate("03/09/2015"));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_WITHOUT_TIME_ID));

		Date startDate = DateUtils.toDate("2015-03-13 4:14:26", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2015-03-13 5:14:26", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("03/09/2015 12:00 AM", "03/22/2015 12:00 AM"))
				.withExpectedResults(
						"03/09/2015 12:00 AM",
						"03/20/2015 12:00 AM"
				).execute();

		startDate = DateUtils.toDate("2015-03-20 0:00:00", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2015-03-20 23:59:59", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("03/20/2015 12:00 AM").execute();
	}


	@Test
	public void testEveryBusinessDayAt1230pm() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);
		schedule.setName("Every Business Day at 12:30 pm");
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(11);
		schedule.setStartDate(DateUtils.toDate("03/09/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-03-09 12:30:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		Date startDate = DateUtils.toDate("2015-03-13 4:14:26", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2015-03-13 5:14:26", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("03/09/2015 12:00 AM", "03/25/2015 12:00 AM"))
				.withExpectedResults(
						"03/09/2015 12:30 PM",
						"03/24/2015 12:30 PM"
				).execute();


		startDate = DateUtils.toDate("2015-03-19 12:14:26", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2015-03-19 1:14:26", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		startDate = DateUtils.toDate("2015-03-24 12:14:26", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2015-03-24 13:14:26", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("03/24/2015 12:30 PM").execute();
	}


	@Test
	public void testBusinessDayWeekendDaily() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule.setName("Every Business Day at 9AM");
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(1);
		schedule.setStartDate(DateUtils.toDate("11/01/2014"));
		schedule.setStartTime(new Time(DateUtils.toDate("2014-01-11 9:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		// check that no instances return when calling between 12am-1am on 12/06 (this was returning 12/05/14 9AM)
		Date startDate = DateUtils.toDate("2014-12-06 0:19:30", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2014-12-06 1:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		// check that no instances return when calling between 12am-1am on 12/06 (this was returning 12/05/14 9AM)
		startDate = DateUtils.toDate("2014-12-06 8:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-12-06 9:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		// check that no instances return when calling between 12am-1am on 12/07 (this was returning 12/05/14 9AM)
		startDate = DateUtils.toDate("2014-12-07 0:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-12-07 1:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		// check that no instances return when calling between 12am-1am on 12/07 (this was returning 12/05/14 9AM)
		startDate = DateUtils.toDate("2014-12-07 8:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-12-07 9:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		// check that no instances return when calling for a whole weekend 12/06-12/07 (this was returning 12/05/14 9AM)
		startDate = DateUtils.toDate("2014-12-06 0:00:00", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-12-07 11:59:59", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults().execute();

		// check that no instances return when calling between 12am-1am on 12/08
		startDate = DateUtils.toDate("2014-12-08 0:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-12-08 1:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults().execute();

		// check that 12/08/14 9am returns when calling between 8am-9am on 12/08
		startDate = DateUtils.toDate("2014-12-08 8:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-12-08 9:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("12/08/2014 9:00 AM").execute();
	}


	@Test
	public void testThanksGivingDaily() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule.setName("Every Business Day at 9AM");
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(1);
		schedule.setStartDate(DateUtils.toDate("11/01/2014"));
		schedule.setStartTime(new Time(DateUtils.toDate("2014-01-11 9:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		// check that no instances return when calling between 12am-1am on 11/27 (this was returning 11/26/14 9AM)
		Date startDate = DateUtils.toDate("2014-11-27 0:19:30", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2014-11-27 1:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		// check that no instances return when calling between 2am-3am on 11/27 (this was returning 11/26/14 9AM)
		startDate = DateUtils.toDate("2014-11-27 2:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-11-27 3:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		// check that no instances return when calling between 8am-9am on 11/27 (this was returning 11/26/14 9AM)
		startDate = DateUtils.toDate("2014-11-27 8:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-11-27 9:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		// check that no instances return when calling between 7am-8am on 11/28
		startDate = DateUtils.toDate("2014-11-28 7:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-11-28 8:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		// check that 11/28/14 9am returns when calling between 8am-9am on 11/28
		startDate = DateUtils.toDate("2014-11-28 8:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-11-28 9:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("11/28/2014 9:00 AM").execute();

		// check that no instances return when calling between 9am-10am on 11/28
		startDate = DateUtils.toDate("2014-11-28 9:19:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-11-28 10:19:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate)).execute();

		// check that the whole week can be scheduled correctly
		startDate = DateUtils.toDate("2014-11-21 9:00:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-11-29 9:00:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults(
						"11/24/2014 9:00 AM",
						"11/25/2014 9:00 AM",
						"11/26/2014 9:00 AM",
						"11/28/2014 9:00 AM"
				).execute();
	}


	@Test
	public void testThanksGivingDaily_withoutTime() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule.setName("Every Business Day");
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(1);
		schedule.setStartDate(DateUtils.toDate("11/01/2014"));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_WITHOUT_TIME_ID));

		// check that 11/28/14 12am returns when calling between 12am-11:59pm on 11/28
		Date startDate = DateUtils.toDate("2014-11-28 0:00:00", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2014-11-28 23:59:59", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("11/28/2014 12:00 AM").execute();


		// check that the whole week can be scheduled correctly
		startDate = DateUtils.toDate("2014-11-21 0:00:30", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2014-11-29 0:00:30", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults(
						"11/24/2014 12:00 AM",
						"11/25/2014 12:00 AM",
						"11/26/2014 12:00 AM",
						"11/28/2014 12:00 AM"
				).execute();
	}


	@Test
	public void testDaily_Recurrence2() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("01/01/2008 8:00 PM"))
				.withExpectedResults("12/28/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("12/28/2008 12:01 PM"))
				.withExpectedResults("12/30/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_ID)
				.withNextOccurrenceFromStartDate("12/28/2008 12:00 PM"))
				.withExpectedResults("12/28/2008 12:00 PM").execute();
	}


	@Test
	public void testDaily_Recurrence2_withoutTime() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/01/2008 8:00 PM"))
				.withExpectedResults("12/28/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/28/2008 12:01 AM"))
				.withExpectedResults("12/30/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/28/2008 12:00 AM"))
				.withExpectedResults("12/28/2008 12:00 AM").execute();
	}


	@Test
	public void testDaily_Recurrence2_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("01/01/2008 8:00 PM")).execute(); // shouldn't get any previous results before schedule start

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_ID)
				.withPreviousOccurrenceFromStartDate("12/30/2008 11:59 AM"))
				.withExpectedResults("12/28/2008 12:00 PM").execute(); // 28th is a Sunday which is okay because this schedule doesn't use business days
	}


	@Test
	public void testDaily_Recurrence2_PreviousOccurrences_withoutTime() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITHOUT_TIME_ID)
				.withPreviousOccurrenceFromStartDate("01/01/2008 8:00 PM")).execute(); // shouldn't get any previous results before schedule start

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITHOUT_TIME_ID)
				.withPreviousOccurrenceFromStartDate("12/30/2008 11:59 AM"))
				.withExpectedResults("12/28/2008 12:00 AM").execute(); // 28th is a Sunday which is okay because this schedule doesn't use business days
	}


	@Test
	public void testDaily_NextBusinessDayConvention() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/27/2008 12:01 PM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/28/2008 12:01 PM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/28/2008 12:00 PM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/29/2008 12:00 PM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/29/2008 11:00 AM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 12:00 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/29/2008 12:01 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 12:01 PM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/02/2009 12:01 PM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:01 PM"))
				.withExpectedResults("01/08/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/08/2009 12:00 AM"))
				.withExpectedResults("01/08/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/08/2009 12:01 AM"))
				.withExpectedResults("01/08/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/08/2009 12:01 PM")).execute();

		// test list
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("12/28/2008 8:00 AM", 4))
				.withExpectedResults(
						"12/29/2008 12:00 PM",
						"12/31/2008 12:00 PM",
						"01/05/2009 12:00 PM",
						"01/08/2009 12:00 PM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withOccurrencesBetween("12/29/2008 11:00 AM", "12/29/2008 1:00 PM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withOccurrencesBetween("12/29/2008 11:35 AM", "12/29/2008 12:35 PM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();
	}


	@Test
	public void testDaily_NextBusinessDayConvention_withoutTime() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/27/2008 12:01 PM"))
				.withExpectedResults("12/29/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/28/2008 12:01 PM"))
				.withExpectedResults("12/29/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/28/2008 12:00 PM"))
				.withExpectedResults("12/29/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/29/2008 12:00 AM"))
				.withExpectedResults("12/29/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/29/2008 11:00 AM"))
				.withExpectedResults("12/31/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/30/2008 12:00 PM"))
				.withExpectedResults("12/31/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/29/2008 12:01 AM"))
				.withExpectedResults("12/31/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 12:01 PM"))
				.withExpectedResults("01/05/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/02/2009 12:01 PM"))
				.withExpectedResults("01/05/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:01 PM"))
				.withExpectedResults("01/08/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/08/2009 12:00 AM"))
				.withExpectedResults("01/08/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/08/2009 12:01 AM")).execute();

		// test list
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withOccurrencesFromStartDate("12/28/2008 8:00 AM", 4))
				.withExpectedResults(
						"12/29/2008 12:00 AM",
						"12/31/2008 12:00 AM",
						"01/05/2009 12:00 AM",
						"01/08/2009 12:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withOccurrencesBetween("12/28/2008 11:00 PM", "12/29/2008 1:00 AM"))
				.withExpectedResults("12/29/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withOccurrencesBetween("12/28/2008 11:35 PM", "12/29/2008 12:35 AM"))
				.withExpectedResults("12/29/2008 12:00 AM").execute();
	}


	@Test
	public void testDaily_NextBusinessDayConvention_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/30/2008 12:00 PM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/28/2008 12:01 PM"))
				.withExpectedResults().execute(); // no result because 28th is the schedule start date and Sunday which is not a business day

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/29/2008 1:00 PM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/31/2008 12:00 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/02/2009 11:59 AM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/04/2009 11:59 AM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/02/2009 12:01 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/05/2009 12:01 PM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/08/2009 12:01 PM"))
				.withExpectedResults("01/08/2009 12:00 PM")
				.execute();

		// test multiple occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("01/10/2009 12:00 PM", -4))
				.withExpectedResults(
						"01/08/2009 12:00 PM",
						"01/05/2009 12:00 PM",
						"12/31/2008 12:00 PM",
						"12/29/2008 12:00 PM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("01/08/2009 11:59 AM", -4)) // request 4 occurrences but only get 3 due to schedule start date of 12/28/2008
				.withExpectedResults(
						"01/05/2009 12:00 PM",
						"12/31/2008 12:00 PM",
						"12/29/2008 12:00 PM"
				).execute();
	}


	@Test
	public void getDailyOccurrenceWithNoHolidaySchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITHOUT_HOLIDAYS_ID)
				.withOccurrencesBetween("11/09/2009 11:16 AM", "11/09/2009 12:16 PM"))
				.withExpectedResults("11/09/2009 11:30 AM");

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITHOUT_HOLIDAYS_ID)
				.withOccurrencesBetween("11/13/2009 12:16 PM", "11/13/2009 1:16 PM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITHOUT_HOLIDAYS_ID)
				.withOccurrencesBetween("11/14/2009 12:16 PM", "11/14/2009 1:16 PM")).execute();
	}


	@Test
	public void getDailyOccurrenceWithInHour() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2010"));
		schedule.setStartTime(new Time(41400000));
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(1);
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("11/09/2010 11:16 AM", "11/09/2010 12:16 PM"))
				.withExpectedResults("11/09/2010 11:30 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("11/09/2010 12:16 PM", "11/09/2010 1:16 PM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("11/10/2010 11:16 AM", "11/10/2010 12:16 PM"))
				.withExpectedResults("11/10/2010 11:30 AM").execute();
	}


	@Test
	public void testMarketDataSchedule() {
		Date startDate = DateUtils.toDate("06/25/2011 4:00 PM", DateUtils.DATE_FORMAT_SHORT);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_MARKET_DATA_SCHEDULE_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60))).execute();

		startDate = DateUtils.toDate("06/26/2011 4:00 PM", DateUtils.DATE_FORMAT_SHORT);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_MARKET_DATA_SCHEDULE_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60))).execute();

		startDate = DateUtils.toDate("06/27/2011 4:00 PM", DateUtils.DATE_FORMAT_SHORT);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(DAILY_MARKET_DATA_SCHEDULE_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("06/27/2011 4:15 PM").execute();
	}
}
