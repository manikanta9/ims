package com.clifton.calendar.schedule;


import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.schedule.executor.CalendarScheduleTestExecutor;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;


@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
@ContextConfiguration("CalendarScheduleTests-context.xml")
public class CalendarWeeklyScheduleTests extends BaseCalendarScheduleTests {

	@Test
	public void testWeeklyDayLightSavings2014() {
		Date startDate = DateUtils.toDate("2014-03-08 2:00:00", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.addMinutes(startDate, 60);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_LATEST_ID)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("03/08/2014 3:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_SPRING_LATEST_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(endDate, 60)))
				.withExpectedResults("03/08/2014 3:00 AM").execute();
	}


	@Test
	public void testWeeklyDayLightSavings() {
		Date startDate = DateUtils.toDate("2011-03-20 3:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("03/20/2011 4:15 AM").execute();

		startDate = DateUtils.toDate("2011-03-13 3:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		startDate = DateUtils.toDate("2011-03-11 0:00:00", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2011-03-17 0:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("03/13/2011 4:15 AM").execute();

		startDate = DateUtils.toDate("2011-11-06 3:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("11/06/2011 4:15 AM").execute();

		startDate = DateUtils.toDate("2011-11-03 0:00:00", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2011-11-09 0:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("11/06/2011 4:15 AM").execute();
	}


	@Test
	public void testWeeklyDayLightSavings_withoutTime() {
		Date startDate = DateUtils.toDate("2011-03-19 23:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_WITHOUT_TIME_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("03/20/2011 12:00 AM").execute();

		startDate = DateUtils.toDate("2011-03-12 23:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_WITHOUT_TIME_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("03/13/2011 12:00 AM").execute();

		startDate = DateUtils.toDate("2011-03-11 0:00:00", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2011-03-17 0:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_WITHOUT_TIME_ID)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("03/13/2011 12:00 AM").execute();

		startDate = DateUtils.toDate("2011-11-05 23:23:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_WITHOUT_TIME_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults("11/06/2011 12:00 AM").execute();

		startDate = DateUtils.toDate("2011-11-03 0:00:00", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2011-11-09 0:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_WITHOUT_TIME_ID)
				.withOccurrencesBetween(startDate, endDate))
				.withExpectedResults("11/06/2011 12:00 AM").execute();
	}


	@Test
	public void testWeekly_NextFiveDates_PastEndDate() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesFromStartDate("11/03/2011 12:00 AM", 5))
				.withExpectedResults("11/06/2011 4:15 AM").execute();
	}


	@Test
	public void testWeekly_1WeekLimitedToBusinessDaySchedule() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/15/2008 8:00 AM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withOccurrencesBetween("12/28/2008 8:00 AM", "01/17/2009 12:00 AM"))
				.withExpectedResults(
						"12/29/2008 12:00 PM",
						"12/31/2008 12:00 PM",
						"01/02/2009 12:00 PM",
						"01/05/2009 12:00 PM",
						"01/09/2009 12:00 PM",
						"01/12/2009 12:00 PM",
						"01/14/2009 12:00 PM",
						"01/16/2009 12:00 PM"
				).execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID, BusinessDayConventions.NEXT);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:01 PM"))
				.withExpectedResults("01/08/2009 12:00 PM").execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID, BusinessDayConventions.NEXT_MODIFIED);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:00 PM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:01 PM"))
				.withExpectedResults("01/08/2009 12:00 PM").execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID, BusinessDayConventions.PREVIOUS);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:01 PM"))
				.withExpectedResults("01/06/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/06/2009 12:01 PM"))
				.withExpectedResults("01/09/2009 12:00 PM").execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID, BusinessDayConventions.PREVIOUS_MODIFIED);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/06/2009 12:01 PM"))
				.withExpectedResults("01/09/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withOccurrencesBetween("12/28/2008 8:00 AM", "01/17/2009 12:00 AM"))
				.withExpectedResults(
						"12/29/2008 12:00 PM",
						"12/31/2008 12:00 PM",
						"01/02/2009 12:00 PM",
						"01/05/2009 12:00 PM",
						"01/06/2009 12:00 PM",
						"01/09/2009 12:00 PM",
						"01/12/2009 12:00 PM",
						"01/14/2009 12:00 PM",
						"01/16/2009 12:00 PM"
				).execute();

		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID);
		schedule.setThursday(true);
		schedule.setFriday(false);
		schedule.setBusinessDayConvention(BusinessDayConventions.PREVIOUS);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/30/2008 8:00 AM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 12:00 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 12:01 PM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		schedule.setWednesday(false);
		schedule.setBusinessDayConvention(BusinessDayConventions.PREVIOUS_MODIFIED);
		getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/29/2008 12:01 PM"))
				.withExpectedResults("01/02/2009 12:00 PM").execute();
	}


	@Test
	public void testWeekly_1WeekLimitedToBusinessDaySchedule_withoutTime() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/15/2008 8:00 AM"))
				.withExpectedResults("12/29/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withOccurrencesBetween("12/28/2008 8:00 AM", "01/16/2009 12:00 AM"))
				.withExpectedResults(
						"12/29/2008 12:00 AM",
						"12/31/2008 12:00 AM",
						"01/02/2009 12:00 AM",
						"01/05/2009 12:00 AM",
						"01/09/2009 12:00 AM",
						"01/12/2009 12:00 AM",
						"01/14/2009 12:00 AM",
						"01/16/2009 12:00 AM"
				).execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID, BusinessDayConventions.NEXT);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:01 AM"))
				.withExpectedResults("01/08/2009 12:00 AM").execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID, BusinessDayConventions.NEXT_MODIFIED);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:00 AM"))
				.withExpectedResults("01/05/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:01 PM"))
				.withExpectedResults("01/08/2009 12:00 AM").execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID, BusinessDayConventions.PREVIOUS);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/05/2009 12:01 AM"))
				.withExpectedResults("01/06/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/06/2009 12:01 AM"))
				.withExpectedResults("01/09/2009 12:00 AM").execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID, BusinessDayConventions.PREVIOUS_MODIFIED);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("01/06/2009 12:01 AM"))
				.withExpectedResults("01/09/2009 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withOccurrencesBetween("12/28/2008 8:00 AM", "01/16/2009 12:00 AM"))
				.withExpectedResults(
						"12/29/2008 12:00 AM",
						"12/31/2008 12:00 AM",
						"01/02/2009 12:00 AM",
						"01/05/2009 12:00 AM",
						"01/06/2009 12:00 AM",
						"01/09/2009 12:00 AM",
						"01/12/2009 12:00 AM",
						"01/14/2009 12:00 AM",
						"01/16/2009 12:00 AM"
				).execute();

		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID);
		schedule.setThursday(true);
		schedule.setFriday(false);
		schedule.setBusinessDayConvention(BusinessDayConventions.PREVIOUS);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/30/2008 8:00 AM"))
				.withExpectedResults("12/31/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 12:00 AM"))
				.withExpectedResults("12/31/2008 12:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 12:01 AM"))
				.withExpectedResults("01/05/2009 12:00 AM").execute();

		schedule.setWednesday(false);
		schedule.setBusinessDayConvention(BusinessDayConventions.PREVIOUS_MODIFIED);
		getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_WITHOUT_TIME_ID)
				.withNextOccurrenceFromStartDate("12/29/2008 12:01 AM"))
				.withExpectedResults("01/02/2009 12:00 AM").execute();
	}


	@Test
	public void testWeekly_1WeekLimitedToBusinessDaySchedule_PreviousOccurrences() {
		// test 1 week limited to business day schedule
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/15/2008 8:00 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/31/2008 11:59 AM"))
				.withExpectedResults("12/29/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/31/2008 12:00 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/31/2008 12:01 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("01/17/2009 12:00 AM", -8))
				.withExpectedResults(
						"01/16/2009 12:00 PM",
						"01/14/2009 12:00 PM",
						"01/12/2009 12:00 PM",
						"01/09/2009 12:00 PM",
						"01/05/2009 12:00 PM",
						"01/02/2009 12:00 PM",
						"12/31/2008 12:00 PM",
						"12/29/2008 12:00 PM"
				).execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID, BusinessDayConventions.NEXT);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/08/2009 12:01 PM"))
				.withExpectedResults("01/08/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/08/2009 12:00 PM"))
				.withExpectedResults("01/08/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/08/2009 11:59 AM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID, BusinessDayConventions.NEXT_MODIFIED);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/05/2009 12:00 PM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/05/2009 12:01 PM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID, BusinessDayConventions.PREVIOUS);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/07/2009 12:01 PM"))
				.withExpectedResults("01/06/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/07/2009 12:00 PM"))
				.withExpectedResults("01/06/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/07/2009 11:59 AM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		setScheduleBusinessDayConvention(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID, BusinessDayConventions.PREVIOUS_MODIFIED);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/06/2009 12:01 PM"))
				.withExpectedResults("01/05/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withOccurrencesFromStartDate("01/17/2009 12:01 PM", -9))
				.withExpectedResults(
						"01/16/2009 12:00 PM",
						"01/14/2009 12:00 PM",
						"01/12/2009 12:00 PM",
						"01/09/2009 12:00 PM",
						"01/06/2009 12:00 PM",
						"01/05/2009 12:00 PM",
						"01/02/2009 12:00 PM",
						"12/31/2008 12:00 PM",
						"12/29/2008 12:00 PM"
				).execute();

		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID);
		schedule.setThursday(true);
		schedule.setFriday(false);
		schedule.setBusinessDayConvention(BusinessDayConventions.PREVIOUS);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/31/2008 12:01 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("12/31/2008 12:00 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/02/2009 12:01 PM"))
				.withExpectedResults("12/31/2008 12:00 PM").execute();

		schedule.setWednesday(false);
		schedule.setBusinessDayConvention(BusinessDayConventions.PREVIOUS_MODIFIED);
		getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/02/2009 12:01 PM"))
				.withExpectedResults("01/02/2009 12:00 PM").execute();
	}


	@Test
	public void testWeekly_2WeekScheduleWithNextDayConvention() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_2_NEXT_DAY_CONVENTION_MWF_ID)
				.withNextOccurrenceFromStartDate("01/19/2009 8:00 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_2_NEXT_DAY_CONVENTION_MWF_ID)
				.withNextOccurrenceFromStartDate("01/09/2009 8:00 AM"))
				.withExpectedResults("01/12/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_2_NEXT_DAY_CONVENTION_MWF_ID)
				.withOccurrencesBetween("12/28/2008 8:00 AM", "01/17/2009 12:00 AM"))
				.withExpectedResults(
						"12/29/2008 12:00 PM",
						"12/31/2008 12:00 PM",
						"01/02/2009 12:00 PM",
						"01/12/2009 12:00 PM",
						"01/14/2009 12:00 PM",
						"01/16/2009 12:00 PM"
				).execute();
	}


	@Test
	public void testWeekly_2WeekScheduleWithNextDayConvention_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_2_NEXT_DAY_CONVENTION_MWF_ID)
				.withPreviousOccurrenceFromStartDate("12/28/2008 11:59 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_2_NEXT_DAY_CONVENTION_MWF_ID)
				.withPreviousOccurrenceFromStartDate("01/12/2009 11:59 AM"))
				.withExpectedResults("01/02/2009 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_2_NEXT_DAY_CONVENTION_MWF_ID)
				.withOccurrencesFromStartDate("01/17/2009 12:00 AM", -7))
				.withExpectedResults(
						"01/16/2009 12:00 PM",
						"01/14/2009 12:00 PM",
						"01/12/2009 12:00 PM",
						"01/02/2009 12:00 PM",
						"12/31/2008 12:00 PM",
						"12/29/2008 12:00 PM"
				).execute();
	}


	@Test
	public void testWeekly_3WeekScheduleWithNextDayConvention() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withNextOccurrenceFromStartDate("01/09/2009 7:00 AM"))
				.withExpectedResults("01/19/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withNextOccurrenceFromStartDate("01/14/2009 7:00 AM"))
				.withExpectedResults("01/19/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withNextOccurrenceFromStartDate("01/20/2009 7:00 AM"))
				.withExpectedResults("01/23/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withOccurrencesBetween("12/28/2008 8:00 AM", "01/24/2009 8:00 AM"))
				.withExpectedResults(
						"12/29/2008 8:00 AM",
						"01/02/2009 8:00 AM",
						"01/19/2009 8:00 AM",
						"01/23/2009 8:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withOccurrencesBetween("01/19/2009 7:00 AM", "01/19/2009 8:00 AM"))
				.withExpectedResults("01/19/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withOccurrencesBetween("01/19/2009 10:00 AM", "01/19/2009 11:00 AM")).execute();
	}


	@Test
	public void testWeekly_3WeekScheduleWithNextDayConvention_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withPreviousOccurrenceFromStartDate("01/19/2009 8:00 AM"))
				.withExpectedResults("01/19/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withPreviousOccurrenceFromStartDate("01/19/2009 7:00 AM"))
				.withExpectedResults("01/02/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withPreviousOccurrenceFromStartDate("01/19/2009 8:01 AM"))
				.withExpectedResults("01/19/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withPreviousOccurrenceFromStartDate("01/26/2009 7:00 AM"))
				.withExpectedResults("01/23/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_3_NEXT_DAY_CONVENTION_MF_ID)
				.withOccurrencesFromStartDate("01/25/2009 12:00 AM", -4))
				.withExpectedResults(
						"01/23/2009 8:00 AM",
						"01/19/2009 8:00 AM",
						"01/02/2009 8:00 AM",
						"12/29/2008 8:00 AM"
				).execute();
	}


	@Test
	public void testWeekly_SaturdayAt4am() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID)
				.withNextOccurrenceFromStartDate(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults("05/29/2010 4:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID)
				.withOccurrencesBetween(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL),
						DateUtils.toDate("2010-06-10 12:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults(
						"05/29/2010 4:00 AM",
						"06/05/2010 4:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID)
				.withOccurrencesBetween(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL),
						DateUtils.toDate("2010-05-27 12:34:09", DateUtils.DATE_FORMAT_FULL))).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID)
				.withOccurrencesFromStartDate(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL), 4))
				.withExpectedResults(
						"05/29/2010 4:00 AM",
						"06/05/2010 4:00 AM",
						"06/12/2010 4:00 AM",
						"06/19/2010 4:00 AM"
				).execute();
	}


	@Test
	public void testWeekly_SundayAt4am() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setStartDate(DateUtils.toDate("02/28/2021"));
		schedule.setRecurrence(1);
		schedule.setStartTime(new Time(DateUtils.toDate("2021-02-28 4:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setSunday(true);
		schedule.setFrequency(ScheduleFrequencies.WEEKLY);
		// A schedule may include a weekend and weekday. The business day convention should only apply to the weekday but not the weekend day.
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withNextOccurrenceFromStartDate(DateUtils.toDate("2021-02-28 12:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults(
						"03/07/2021 4:00 AM"
				)
				.execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(DateUtils.toDate("2021-03-01 12:34:09", DateUtils.DATE_FORMAT_FULL),
						DateUtils.toDate("2021-03-30 12:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults(
						"03/07/2021 4:00 AM",
						"03/14/2021 4:00 AM",
						"03/21/2021 4:00 AM",
						"03/28/2021 4:00 AM"
				).execute();


		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("2021-03-01 12:34:09", DateUtils.DATE_FORMAT_FULL), 4))
				.withExpectedResults(
						"03/07/2021 4:00 AM",
						"03/14/2021 4:00 AM",
						"03/21/2021 4:00 AM",
						"03/28/2021 4:00 AM"
				).execute();
	}


	@Test
	public void testWeekly_SaturdayAt4am_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID)
				.withPreviousOccurrenceFromStartDate(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults("05/22/2010 4:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID)
				.withOccurrencesFromStartDate(DateUtils.toDate("2010-06-10 12:34:09", DateUtils.DATE_FORMAT_FULL), -2))
				.withExpectedResults(
						"06/05/2010 4:00 AM",
						"05/29/2010 4:00 AM"
				).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID)
				.withOccurrencesFromStartDate(DateUtils.toDate("2010-01-22 0:34:09", DateUtils.DATE_FORMAT_FULL), -4))
				.withExpectedResults( // only get 3 occurrences since schedule starts on 1/1/2010
						"01/16/2010 4:00 AM",
						"01/09/2010 4:00 AM",
						"01/02/2010 4:00 AM"
				).execute();
	}
}
