package com.clifton.calendar.schedule;


import com.clifton.calendar.schedule.executor.CalendarScheduleTestExecutor;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;


@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
@ContextConfiguration("CalendarScheduleTests-context.xml")
public class CalendarHourlyScheduleTests extends BaseCalendarScheduleTests {

	@Test
	public void testEvery5MinutesBetween6am_and_10pm() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName("Every 5 Minutes from 6am to 10pm");
		schedule.setFrequency(ScheduleFrequencies.HOURLY);
		schedule.setRecurrence(5);
		schedule.setStartDate(DateUtils.toDate("01/01/2014"));
		schedule.setStartTime(new Time(21600000));
		schedule.setEndTime(new Time(79200000));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween(DateUtils.toDate("2015-04-01 10:50:41", DateUtils.DATE_FORMAT_FULL), DateUtils.toDate("2015-04-01 11:50:41", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults(
						"04/01/2015 10:55 AM",
						"04/01/2015 11:00 AM",
						"04/01/2015 11:05 AM",
						"04/01/2015 11:10 AM",
						"04/01/2015 11:15 AM",
						"04/01/2015 11:20 AM",
						"04/01/2015 11:25 AM",
						"04/01/2015 11:30 AM",
						"04/01/2015 11:35 AM",
						"04/01/2015 11:40 AM",
						"04/01/2015 11:45 AM",
						"04/01/2015 11:50 AM"
				).execute();
	}


	@Test
	public void testEvery5MinutesBetween6am_and_10pm_PreviousOccurrences() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName("Every 5 Minutes from 6am to 10pm");
		schedule.setFrequency(ScheduleFrequencies.HOURLY);
		schedule.setRecurrence(5);
		schedule.setStartDate(DateUtils.toDate("01/01/2014"));
		schedule.setStartTime(new Time(21600000));
		schedule.setEndTime(new Time(79200000));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("2015-04-01 11:50:41", DateUtils.DATE_FORMAT_FULL), -12))
				.withExpectedResults(
						"04/01/2015 11:50 AM",
						"04/01/2015 11:45 AM",
						"04/01/2015 11:40 AM",
						"04/01/2015 11:35 AM",
						"04/01/2015 11:30 AM",
						"04/01/2015 11:25 AM",
						"04/01/2015 11:20 AM",
						"04/01/2015 11:15 AM",
						"04/01/2015 11:10 AM",
						"04/01/2015 11:05 AM",
						"04/01/2015 11:00 AM",
						"04/01/2015 10:55 AM"
				).execute();
	}


	@Test
	public void testHourlyDayLightSavings() {
		Date startDate = DateUtils.toDate("2011-03-11 2:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults(
						"03/11/2011 2:00 AM",
						"03/11/2011 2:30 AM",
						"03/11/2011 3:00 AM"
				).execute();

		startDate = DateUtils.toDate("2011-03-13 1:45:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults(
						"03/13/2011 3:00 AM",
						"03/13/2011 3:30 AM"
				).execute();

		startDate = DateUtils.toDate("2011-03-13 2:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults(
						"03/13/2011 3:00 AM",
						"03/13/2011 3:30 AM",
						"03/13/2011 4:00 AM"
				).execute();

		startDate = DateUtils.toDate("2011-03-13 3:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults(
						"03/13/2011 3:00 AM",
						"03/13/2011 3:30 AM",
						"03/13/2011 4:00 AM"
				).execute();

		startDate = DateUtils.toDate("2011-03-13 4:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults(
						"03/13/2011 4:00 AM",
						"03/13/2011 4:30 AM",
						"03/13/2011 5:00 AM"
				).execute();

		startDate = DateUtils.toDate("2011-11-06 1:45:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults(
						"11/06/2011 2:00 AM",
						"11/06/2011 2:30 AM"
				).execute();

		startDate = DateUtils.toDate("2011-11-05 2:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults(
						"11/05/2011 2:00 AM",
						"11/05/2011 2:30 AM",
						"11/05/2011 3:00 AM"
				).execute();

		startDate = DateUtils.toDate("2011-11-06 2:00:00", DateUtils.DATE_FORMAT_FULL);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_DAYLIGHT_SAVINGS_ID)
				.withOccurrencesBetween(startDate, DateUtils.addMinutes(startDate, 60)))
				.withExpectedResults(
						"11/06/2011 2:00 AM",
						"11/06/2011 2:30 AM",
						"11/06/2011 3:00 AM"
				)
				.execute();
	}


	@Test
	public void testHourly_Recurrence30() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("09/30/2009 8:00 PM"))
				.withExpectedResults("10/01/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/06/2009 8:00 PM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/01/2009 8:00 AM"))
				.withExpectedResults("10/01/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/01/2009 8:15 AM"))
				.withExpectedResults("10/01/2009 8:30 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/01/2009 12:45 PM"))
				.withExpectedResults("10/01/2009 1:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/01/2009 4:53 PM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/01/2009 5:00 PM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/01/2009 5:01 PM"))
				.withExpectedResults("10/02/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/02/2009 6:00 AM"))
				.withExpectedResults("10/02/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/02/2009 12:45 PM"))
				.withExpectedResults("10/02/2009 1:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/02/2009 5:01 PM"))
				.withExpectedResults("10/03/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/05/2009 5:01 PM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/05/2009 5:00 PM"))
				.withExpectedResults("10/05/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withNextOccurrenceFromStartDate("10/05/2009 4:39 PM"))
				.withExpectedResults("10/05/2009 5:00 PM").execute();
	}


	@Test
	public void testHourly_Recurrence30_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("09/30/2009 8:00 PM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/06/2009 8:00 PM"))
				.withExpectedResults("10/05/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/05/2009 5:01 PM"))
				.withExpectedResults("10/05/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/05/2009 5:00 PM"))
				.withExpectedResults("10/05/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/01/2009 8:00 AM"))
				.withExpectedResults("10/01/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/01/2009 8:31 AM"))
				.withExpectedResults("10/01/2009 8:30 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/01/2009 12:59 PM"))
				.withExpectedResults("10/01/2009 12:30 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/01/2009 4:53 PM"))
				.withExpectedResults("10/01/2009 4:30 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/01/2009 5:00 PM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/02/2009 7:59 AM"))
				.withExpectedResults("10/01/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/02/2009 6:00 PM"))
				.withExpectedResults("10/02/2009 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/02/2009 12:45 PM"))
				.withExpectedResults("10/02/2009 12:30 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/01/2009 7:59 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withPreviousOccurrenceFromStartDate("10/05/2009 4:39 PM"))
				.withExpectedResults("10/05/2009 4:30 PM").execute();
	}


	@Test
	public void testHourly_NextBusinessDayConvention() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/01/2009 12:45 PM"))
				.withExpectedResults("01/02/2009 8:00 AM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("12/31/2008 5:01 PM"))
				.withExpectedResults("01/02/2009 8:00 AM").execute();

		// skip the weekend
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/02/2009 5:01 PM"))
				.withExpectedResults("01/05/2009 8:00 AM").execute();

		// check when endDate is a holiday
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/06/2009 5:01 PM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withNextOccurrenceFromStartDate("01/07/2009 12:45 PM")).execute();
	}


	@Test
	public void testHourly_NextBusinessDayConvention_PreviousOccurrences() {
		// test business days
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/01/2009 12:45 PM"))
				.withExpectedResults("12/31/2008 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/02/2009 7:59 AM"))
				.withExpectedResults("12/31/2008 5:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/02/2009 8:00 AM"))
				.withExpectedResults("01/02/2009 8:00 AM").execute();

		// skip the weekend
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/05/2009 7:59 AM"))
				.withExpectedResults("01/02/2009 5:00 PM").execute();

		// check when endDate is a holiday
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID);
		schedule.setStartDate(DateUtils.toDate("01/07/2009"));
		schedule.setEndDate(DateUtils.toDate("01/09/2009"));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/08/2009 7:59 AM")).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_NEXT_DAY_CONVENTION_ID)
				.withPreviousOccurrenceFromStartDate("01/07/2009 12:45 PM")).execute();
	}


	@Test
	public void testHourly_Recurrence90() {
		// test 90 minute recurrence
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID);
		schedule.setRecurrence(90);
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withOccurrencesBetween("10/01/2009 8:00 AM", "10/01/2009 12:00 PM"))
				.withExpectedResults(
						"10/01/2009 8:00 AM",
						"10/01/2009 9:30 AM",
						"10/01/2009 11:00 AM"
				).execute();

		schedule.setStartTime(null);
		schedule.setEndTime(null);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withOccurrencesBetween("10/01/2009 12:00 AM", "10/01/2009 8:00 AM"))
				.withExpectedResults(
						"10/01/2009 12:00 AM",
						"10/01/2009 1:30 AM",
						"10/01/2009 3:00 AM",
						"10/01/2009 4:30 AM",
						"10/01/2009 6:00 AM",
						"10/01/2009 7:30 AM"
				).execute();
	}


	@Test
	public void testHourly_Recurrence90_PreviousOccurrences() {
		// test 90 minute recurrence
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID);
		schedule.setRecurrence(90);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withOccurrencesFromStartDate(DateUtils.toDate("10/01/2009 12:00 PM", DateUtils.DATE_FORMAT_SHORT), -3))
				.withExpectedResults(
						"10/01/2009 11:00 AM",
						"10/01/2009 9:30 AM",
						"10/01/2009 8:00 AM"
				).execute();

		schedule.setStartTime(null);
		schedule.setEndTime(null);
		getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withOccurrencesFromStartDate("10/01/2009 8:00 AM", -6))
				.withExpectedResults(
						"10/01/2009 7:30 AM",
						"10/01/2009 6:00 AM",
						"10/01/2009 4:30 AM",
						"10/01/2009 3:00 AM",
						"10/01/2009 1:30 AM",
						"10/01/2009 12:00 AM"

				).execute();
	}


	@Test
	public void testHourly_VariousRecurrences() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID);
		schedule.setStartTime(null);
		schedule.setEndTime(null);
		schedule.setRecurrence(1);
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withOccurrencesBetween("10/01/2009 12:00 AM", "10/01/2009 12:30 AM"))
				.withExpectedResults(
						"10/01/2009 12:00 AM",
						"10/01/2009 12:01 AM",
						"10/01/2009 12:02 AM",
						"10/01/2009 12:03 AM",
						"10/01/2009 12:04 AM",
						"10/01/2009 12:05 AM",
						"10/01/2009 12:06 AM",
						"10/01/2009 12:07 AM",
						"10/01/2009 12:08 AM",
						"10/01/2009 12:09 AM",
						"10/01/2009 12:10 AM",
						"10/01/2009 12:11 AM",
						"10/01/2009 12:12 AM",
						"10/01/2009 12:13 AM",
						"10/01/2009 12:14 AM",
						"10/01/2009 12:15 AM",
						"10/01/2009 12:16 AM",
						"10/01/2009 12:17 AM",
						"10/01/2009 12:18 AM",
						"10/01/2009 12:19 AM",
						"10/01/2009 12:20 AM",
						"10/01/2009 12:21 AM",
						"10/01/2009 12:22 AM",
						"10/01/2009 12:23 AM",
						"10/01/2009 12:24 AM",
						"10/01/2009 12:25 AM",
						"10/01/2009 12:26 AM",
						"10/01/2009 12:27 AM",
						"10/01/2009 12:28 AM",
						"10/01/2009 12:29 AM",
						"10/01/2009 12:30 AM"
				).execute();

		schedule.setRecurrence(2);
		getCalendarScheduleService().saveCalendarSchedule(schedule);
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withOccurrencesBetween("10/01/2009 12:00 AM", "10/01/2009 12:30 AM"))
				.withExpectedResults(
						"10/01/2009 12:00 AM",
						"10/01/2009 12:02 AM",
						"10/01/2009 12:04 AM",
						"10/01/2009 12:06 AM",
						"10/01/2009 12:08 AM",
						"10/01/2009 12:10 AM",
						"10/01/2009 12:12 AM",
						"10/01/2009 12:14 AM",
						"10/01/2009 12:16 AM",
						"10/01/2009 12:18 AM",
						"10/01/2009 12:20 AM",
						"10/01/2009 12:22 AM",
						"10/01/2009 12:24 AM",
						"10/01/2009 12:26 AM",
						"10/01/2009 12:28 AM",
						"10/01/2009 12:30 AM"
				).execute();
	}


	@Test
	public void testHourly_VariousRecurrences_PreviousOccurrences() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID);
		schedule.setStartTime(null);
		schedule.setEndTime(null);
		schedule.setRecurrence(2);
		getCalendarScheduleService().saveCalendarSchedule(schedule);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID)
				.withOccurrencesFromStartDate("10/01/2009 12:30 AM", -16))
				.withExpectedResults(
						"10/01/2009 12:30 AM",
						"10/01/2009 12:28 AM",
						"10/01/2009 12:26 AM",
						"10/01/2009 12:24 AM",
						"10/01/2009 12:22 AM",
						"10/01/2009 12:20 AM",
						"10/01/2009 12:18 AM",
						"10/01/2009 12:16 AM",
						"10/01/2009 12:14 AM",
						"10/01/2009 12:12 AM",
						"10/01/2009 12:10 AM",
						"10/01/2009 12:08 AM",
						"10/01/2009 12:06 AM",
						"10/01/2009 12:04 AM",
						"10/01/2009 12:02 AM",
						"10/01/2009 12:00 AM"
				).execute();
	}


	@Test
	public void testHourly_EveryHourOnTheHour() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_AT_0_MINUTES_ID)
				.withNextOccurrenceFromStartDate(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults("05/24/2010 1:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_AT_0_MINUTES_ID)
				.withOccurrencesBetween(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL), DateUtils.toDate("2010-05-24 22:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults(
						"05/24/2010 1:00 PM",
						"05/24/2010 2:00 PM",
						"05/24/2010 3:00 PM",
						"05/24/2010 4:00 PM",
						"05/24/2010 5:00 PM",
						"05/24/2010 6:00 PM",
						"05/24/2010 7:00 PM",
						"05/24/2010 8:00 PM",
						"05/24/2010 9:00 PM",
						"05/24/2010 10:00 PM"
				);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_AT_0_MINUTES_ID)
				.withOccurrencesBetween(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL),
						DateUtils.toDate("2010-05-24 12:57:09", DateUtils.DATE_FORMAT_FULL))).execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_AT_0_MINUTES_ID)
				.withOccurrencesFromStartDate(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL), 4))
				.withExpectedResults(
						"05/24/2010 1:00 PM",
						"05/24/2010 2:00 PM",
						"05/24/2010 3:00 PM",
						"05/24/2010 4:00 PM"
				).execute();
	}


	@Test
	public void testHourly_EveryHourOnTheHour_PreviousOccurrences() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_AT_0_MINUTES_ID)
				.withPreviousOccurrenceFromStartDate(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults("05/24/2010 12:00 PM").execute();

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_AT_0_MINUTES_ID)
				.withOccurrencesFromStartDate(DateUtils.toDate("2010-05-24 2:34:09", DateUtils.DATE_FORMAT_FULL), -10))
				.withExpectedResults(
						"05/24/2010 2:00 AM",
						"05/24/2010 1:00 AM",
						"05/24/2010 12:00 AM",
						"05/23/2010 11:00 PM",
						"05/23/2010 10:00 PM",
						"05/23/2010 9:00 PM",
						"05/23/2010 8:00 PM",
						"05/23/2010 7:00 PM",
						"05/23/2010 6:00 PM",
						"05/23/2010 5:00 PM"
				).execute();
	}
}
