package com.clifton.calendar.schedule;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.schedule.executor.CalendarScheduleTestExecutor;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;


/**
 * @author mitchellf
 */
@ContextConfiguration("CalendarScheduleTests-context.xml")
public class CalendarScheduleOccurrenceCalendarOverridesTests extends BaseCalendarScheduleTests {

	// So we need to test each possible frequency, and verify that the business day adjustment is correctly applied when accounting for both calendars


	@Test
	public void testHourlyScheduleSkipBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("11/01/2015"));
		schedule.setEndDate(DateUtils.toDate("11/30/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-11-01 00:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		// every 3 hours
		schedule.setRecurrence(180);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.HOURLY);
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);


		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("11/09/2015 12:00 PM", "11/12/2015 12:00 PM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"11/09/2015 12:00 PM",
						"11/09/2015 3:00 PM",
						"11/09/2015 6:00 PM",
						"11/09/2015 9:00 PM",
						"11/10/2015 12:00 AM",
						"11/12/2015 12:00 AM",
						"11/12/2015 3:00 AM",
						"11/12/2015 6:00 AM",
						"11/12/2015 9:00 AM",
						"11/12/2015 12:00 PM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("2015-11-12 12:00:00"), -10)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"11/12/2015 12:00 PM",
						"11/12/2015 9:00 AM",
						"11/12/2015 6:00 AM",
						"11/12/2015 3:00 AM",
						"11/12/2015 12:00 AM",
						"11/10/2015 12:00 AM",
						"11/09/2015 9:00 PM",
						"11/09/2015 6:00 PM",
						"11/09/2015 3:00 PM",
						"11/09/2015 12:00 PM"
				)
				.execute();
	}


	@Test
	public void testHourlyScheduleNextBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("11/01/2015"));
		schedule.setEndDate(DateUtils.toDate("11/30/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-11-01 00:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		// every 3 hours
		schedule.setRecurrence(180);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.HOURLY);
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);


		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("11/09/2015 12:00 PM", "11/12/2015 12:00 PM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"11/09/2015 12:00 PM",
						"11/09/2015 3:00 PM",
						"11/09/2015 6:00 PM",
						"11/09/2015 9:00 PM",
						"11/10/2015 12:00 AM",
						"11/12/2015 12:00 AM",
						"11/12/2015 3:00 AM",
						"11/12/2015 6:00 AM",
						"11/12/2015 9:00 AM",
						"11/12/2015 12:00 PM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("2015-11-12 12:00:00"), -10)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"11/12/2015 12:00 PM",
						"11/12/2015 9:00 AM",
						"11/12/2015 6:00 AM",
						"11/12/2015 3:00 AM",
						"11/12/2015 12:00 AM",
						"11/10/2015 12:00 AM",
						"11/09/2015 9:00 PM",
						"11/09/2015 6:00 PM",
						"11/09/2015 3:00 PM",
						"11/09/2015 12:00 PM"
				)
				.execute();
	}


	@Test
	public void testDailyScheduleWithNextBusinessDay() {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(DAILY_SCHEDULE_NEXT_DAY_CONVENTION_ID);
		schedule.setStartDate(DateUtils.toDate("01/01/2015"));
		schedule.setEndDate(DateUtils.toDate("12/31/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-01-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setRecurrence(1);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));

		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("02/01/2015 12:00 AM", "02/28/2015 12:00 AM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"02/02/2015 2:00 AM",
						"02/03/2015 2:00 AM",
						"02/04/2015 2:00 AM",
						"02/05/2015 2:00 AM",
						"02/06/2015 2:00 AM",
						"02/09/2015 2:00 AM",
						"02/11/2015 2:00 AM",
						"02/12/2015 2:00 AM",
						"02/13/2015 2:00 AM",
						"02/17/2015 2:00 AM",
						"02/18/2015 2:00 AM",
						"02/19/2015 2:00 AM",
						"02/23/2015 2:00 AM",
						"02/24/2015 2:00 AM",
						"02/25/2015 2:00 AM",
						"02/26/2015 2:00 AM",
						"02/27/2015 2:00 AM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate("02/28/2015 12:00 AM", -17)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"02/27/2015 2:00 AM",
						"02/26/2015 2:00 AM",
						"02/25/2015 2:00 AM",
						"02/24/2015 2:00 AM",
						"02/23/2015 2:00 AM",
						"02/19/2015 2:00 AM",
						"02/18/2015 2:00 AM",
						"02/17/2015 2:00 AM",
						"02/13/2015 2:00 AM",
						"02/12/2015 2:00 AM",
						"02/11/2015 2:00 AM",
						"02/09/2015 2:00 AM",
						"02/06/2015 2:00 AM",
						"02/05/2015 2:00 AM",
						"02/04/2015 2:00 AM",
						"02/03/2015 2:00 AM",
						"02/02/2015 2:00 AM"
				)
				.execute();
	}


	@Test
	public void testWeeklyScheduleSkipBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2015"));
		schedule.setEndDate(DateUtils.toDate("12/31/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-01-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setRecurrence(1);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.WEEKLY);
		schedule.setFriday(true);
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);

		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("04/01/2015 12:00 AM", "04/30/2015 12:00 AM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"04/17/2015 2:00 AM",
						"04/24/2015 2:00 AM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("04/30/2015 12:00 AM"), -3)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"04/24/2015 2:00 AM",
						"04/17/2015 2:00 AM",
						"03/27/2015 2:00 AM"
				)
				.execute();
	}


	@Test
	public void testWeeklyScheduleNextBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2015"));
		schedule.setEndDate(DateUtils.toDate("12/31/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-01-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setRecurrence(1);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.WEEKLY);
		schedule.setFriday(true);
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);

		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("04/01/2015 12:00 AM", "04/30/2015 12:00 AM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"04/06/2015 2:00 AM",
						"04/13/2015 2:00 AM",
						"04/17/2015 2:00 AM",
						"04/24/2015 2:00 AM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("04/30/2015 12:00 AM"), -4)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"04/24/2015 2:00 AM",
						"04/17/2015 2:00 AM",
						"04/13/2015 2:00 AM",
						"04/06/2015 2:00 AM"
				)
				.execute();
	}


	@Test
	public void testMonthlyScheduleSkipBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2015"));
		schedule.setEndDate(DateUtils.toDate("12/31/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-01-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setRecurrence(1);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.MONTHLY);
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);
		schedule.setDayOfMonth(3);
		// saving schedule this time to make sure functionality still works when schedule is passed by ID
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);

		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule.getId())
				.withOccurrencesBetween("01/01/2015 12:00 AM", "12/31/2015 12:00 AM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"02/03/2015 2:00 AM",
						"03/03/2015 2:00 AM",
						"06/03/2015 2:00 AM",
						"08/03/2015 2:00 AM",
						"09/03/2015 2:00 AM",
						"12/03/2015 2:00 AM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("12/31/2015 12:00 AM"), -6)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"12/03/2015 2:00 AM",
						"09/03/2015 2:00 AM",
						"08/03/2015 2:00 AM",
						"06/03/2015 2:00 AM",
						"03/03/2015 2:00 AM",
						"02/03/2015 2:00 AM"
				)
				.execute();
	}


	@Test
	public void testMonthlyScheduleNextBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2015"));
		schedule.setEndDate(DateUtils.toDate("12/31/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-01-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setRecurrence(1);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.MONTHLY);
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule.setDayOfMonth(3);
		// saving schedule this time to make sure functionality still works when schedule is passed by ID
		schedule = getCalendarScheduleService().saveCalendarSchedule(schedule);

		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule.getId())
				.withOccurrencesBetween("01/01/2015 12:00 AM", "12/31/2015 12:00 AM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"01/05/2015 2:00 AM",
						"02/03/2015 2:00 AM",
						"03/03/2015 2:00 AM",
						"04/06/2015 2:00 AM",
						"05/04/2015 2:00 AM",
						"06/03/2015 2:00 AM",
						"07/06/2015 2:00 AM",
						"08/03/2015 2:00 AM",
						"09/03/2015 2:00 AM",
						"10/05/2015 2:00 AM",
						"11/04/2015 2:00 AM",
						"12/03/2015 2:00 AM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("12/31/2015 12:00 AM"), -12)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"12/03/2015 2:00 AM",
						"11/04/2015 2:00 AM",
						"10/05/2015 2:00 AM",
						"09/03/2015 2:00 AM",
						"08/03/2015 2:00 AM",
						"07/06/2015 2:00 AM",
						"06/03/2015 2:00 AM",
						"05/04/2015 2:00 AM",
						"04/06/2015 2:00 AM",
						"03/03/2015 2:00 AM",
						"02/03/2015 2:00 AM",
						"01/05/2015 2:00 AM"
				)
				.execute();
	}


	@Test
	public void testMonthlyWeekdayScheduleSkipBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2015"));
		schedule.setEndDate(DateUtils.toDate("12/31/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-01-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setRecurrence(1);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.MONTHLY_WEEKDAY);
		schedule.setMonday(true);
		schedule.setWeekdayOccurrenceListString("2,4");
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);

		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("05/01/2015 12:00 AM", "08/31/2015 12:00 AM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"05/11/2015 2:00 AM",
						"06/08/2015 2:00 AM",
						"06/22/2015 2:00 AM",
						"07/13/2015 2:00 AM",
						"07/27/2015 2:00 AM",
						"08/24/2015 2:00 AM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("08/31/2015 12:00 AM"), -6)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"08/24/2015 2:00 AM",
						"07/27/2015 2:00 AM",
						"07/13/2015 2:00 AM",
						"06/22/2015 2:00 AM",
						"06/08/2015 2:00 AM",
						"05/11/2015 2:00 AM"
				)
				.execute();
	}


	@Test
	public void testMonthlyWeekdayScheduleNextBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2015"));
		schedule.setEndDate(DateUtils.toDate("12/31/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-01-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setRecurrence(1);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.MONTHLY_WEEKDAY);
		schedule.setMonday(true);
		schedule.setWeekdayOccurrenceListString("2,4");
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);

		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("05/01/2015 12:00 AM", "08/31/2015 12:00 AM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"05/11/2015 2:00 AM",
						"05/26/2015 2:00 AM",
						"06/08/2015 2:00 AM",
						"06/22/2015 2:00 AM",
						"07/13/2015 2:00 AM",
						"07/27/2015 2:00 AM",
						"08/11/2015 2:00 AM",
						"08/24/2015 2:00 AM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("08/31/2015 12:00 AM"), -8)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"08/24/2015 2:00 AM",
						"08/11/2015 2:00 AM",
						"07/27/2015 2:00 AM",
						"07/13/2015 2:00 AM",
						"06/22/2015 2:00 AM",
						"06/08/2015 2:00 AM",
						"05/26/2015 2:00 AM",
						"05/11/2015 2:00 AM"
				)
				.execute();
	}


	@Test
	public void testYearlyScheduleSkipBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2012"));
		schedule.setEndDate(DateUtils.toDate("12/31/2016"));
		schedule.setStartTime(new Time(DateUtils.toDate("2012-01-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setRecurrence(1);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.YEARLY);
		schedule.setJanuary(true);
		schedule.setDayOfMonth(20);
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);

		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("01/01/2012 12:00 AM", "12/01/2016 12:00 AM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"01/20/2012 2:00 AM",
						"01/20/2016 2:00 AM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("12/01/2016 12:00 AM"), -2)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"01/20/2016 2:00 AM",
						"01/20/2012 2:00 AM"
				)
				.execute();
	}


	@Test
	public void testYearlyScheduleNextBusinessDay() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2012"));
		schedule.setEndDate(DateUtils.toDate("12/31/2016"));
		schedule.setStartTime(new Time(DateUtils.toDate("2012-01-01 2:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		schedule.setRecurrence(1);
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setFrequency(ScheduleFrequencies.YEARLY);
		schedule.setJanuary(true);
		schedule.setDayOfMonth(20);
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);

		// next occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("01/01/2012 12:00 AM", "12/01/2016 12:00 AM")
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"01/20/2012 2:00 AM",
						"01/22/2013 2:00 AM",
						"01/21/2014 2:00 AM",
						"01/21/2015 2:00 AM",
						"01/20/2016 2:00 AM"
				)
				.execute();

		// previous occurrences
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate(DateUtils.toDate("12/01/2016 12:00 AM"), -5)
				.withOverriddenCalendars(CalendarTestObjectFactory.CALENDAR_EXTRA_HOLIDAYS, CalendarTestObjectFactory.CALENDAR_US))
				.withExpectedResults(
						"01/20/2016 2:00 AM",
						"01/21/2015 2:00 AM",
						"01/21/2014 2:00 AM",
						"01/22/2013 2:00 AM",
						"01/20/2012 2:00 AM"
				)
				.execute();
	}
}
