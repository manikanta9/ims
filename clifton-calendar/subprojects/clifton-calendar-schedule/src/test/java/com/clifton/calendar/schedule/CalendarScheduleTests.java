package com.clifton.calendar.schedule;

import com.clifton.calendar.schedule.executor.CalendarScheduleTestExecutor;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;


@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
@ContextConfiguration
public class CalendarScheduleTests extends BaseCalendarScheduleTests {

	@Test
	public void testCycleAlgorithm() {
		CalendarSchedule o7 = new CalendarSchedule();
		o7.setId(7);

		CalendarSchedule o1 = new CalendarSchedule();
		o1.setId(1);

		CalendarSchedule o2 = new CalendarSchedule();
		o2.setId(2);

		List<CalendarSchedule> child1 = new ArrayList<>();
		child1.add(o1);
		child1.add(o2);

		CalendarSchedule o3 = new CalendarSchedule();
		o3.setId(3);

		o3.setChildCalendarScheduleList(child1);

		CalendarSchedule o4 = new CalendarSchedule();
		o4.setId(4);
		CalendarSchedule o5 = new CalendarSchedule();
		o5.setId(5);

		List<CalendarSchedule> child2 = new ArrayList<>();
		child2.add(o4);
		child2.add(o5);

		CalendarSchedule o6 = new CalendarSchedule();
		o6.setId(6);

		o6.setChildCalendarScheduleList(child2);

		List<CalendarSchedule> child3 = new ArrayList<>();
		child3.add(o6);
		child3.add(o3);

		o7.setChildCalendarScheduleList(child3);

		try {
			Method m = getCalendarScheduleService().getClass().getDeclaredMethod("containsRollupCycle", List.class, CalendarSchedule.class);
			m.setAccessible(true);

			Assertions.assertFalse((Boolean) m.invoke(getCalendarScheduleService(), o7.getChildCalendarScheduleList(), o7));

			child1.add(o7); //create cycle

			Assertions.assertTrue((Boolean) m.invoke(getCalendarScheduleService(), o7.getChildCalendarScheduleList(), o7));
		}
		catch (Exception e) {
			Assertions.fail(e.getLocalizedMessage());
		}
	}


	@Test
	public void testNextFiveDatesRollup() {
		CalendarSchedule parent = getCalendarScheduleService().getCalendarSchedule(SCHEDULE_ROLLUP_ID);

		CalendarSchedule once = getCalendarScheduleService().getCalendarSchedule(ONCE_SCHEDULE_ID);
		CalendarSchedule daily = getCalendarScheduleService().getCalendarSchedule(DAILY_SCHEDULE_ID);

		List<CalendarSchedule> children = new ArrayList<>();
		children.add(once);
		children.add(daily);
		parent.setChildCalendarScheduleList(children);

		// ONCE_SCHEDULE_ID and DAILY_SCHEDULE_ID... parent is SCHEDULE_ROLLUP_ID
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(parent)
				.withOccurrencesFromStartDate("12/01/2008 8:00 AM", 5))
				.withExpectedResults(
						"12/28/2008 12:00 PM",
						"12/30/2008 12:00 PM",
						"01/01/2009 12:00 PM",
						"01/03/2009 12:00 PM",
						"01/05/2009 12:00 PM"
				).execute();
	}


	@Test
	public void tesDatesRollupInBetween() {
		CalendarSchedule parent = getCalendarScheduleService().getCalendarSchedule(SCHEDULE_ROLLUP_ID);

		CalendarSchedule once = getCalendarScheduleService().getCalendarSchedule(ONCE_SCHEDULE_ID);
		CalendarSchedule daily = getCalendarScheduleService().getCalendarSchedule(DAILY_SCHEDULE_ID);

		List<CalendarSchedule> children = new ArrayList<>();
		children.add(once);
		children.add(daily);
		parent.setChildCalendarScheduleList(children);

		// ONCE_SCHEDULE_ID and DAILY_SCHEDULE_ID... parent is SCHEDULE_ROLLUP_ID
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(parent)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2010 8:00 AM"))
				.withExpectedResults(
						"12/28/2008 12:00 PM",
						"12/30/2008 12:00 PM",
						"01/01/2009 12:00 PM",
						"01/03/2009 12:00 PM",
						"01/05/2009 12:00 PM",
						"01/07/2009 12:00 PM",
						"10/01/2009 5:00 PM"
				).execute();
	}


	public void testDatesRollupOfRollupInBetween() {
		CalendarSchedule parent = getCalendarScheduleService().getCalendarSchedule(SCHEDULE_ROLLUP_OF_ROLLUP_ID);

		CalendarSchedule rollupChild = getCalendarScheduleService().getCalendarSchedule(SCHEDULE_ROLLUP_ID);
		CalendarSchedule normalChild = getCalendarScheduleService().getCalendarSchedule(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID);

		CalendarSchedule once = getCalendarScheduleService().getCalendarSchedule(ONCE_SCHEDULE_ID);
		CalendarSchedule daily = getCalendarScheduleService().getCalendarSchedule(DAILY_SCHEDULE_ID);

		List<CalendarSchedule> children = new ArrayList<>();
		children.add(once);
		children.add(daily);
		rollupChild.setChildCalendarScheduleList(children);

		List<CalendarSchedule> children2 = new ArrayList<>();
		children2.add(rollupChild);
		children2.add(normalChild);
		parent.setChildCalendarScheduleList(children2);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(parent)
				.withOccurrencesBetween("12/01/2008 8:00 AM", "05/31/2010 8:00 AM"))
				.withExpectedResults().execute();
	}


	@Test
	public void testNextFiveDatesRollupOfRollup() {
		CalendarSchedule parent = getCalendarScheduleService().getCalendarSchedule(SCHEDULE_ROLLUP_OF_ROLLUP_ID);

		CalendarSchedule rollupChild = getCalendarScheduleService().getCalendarSchedule(SCHEDULE_ROLLUP_ID);
		CalendarSchedule normalChild = getCalendarScheduleService().getCalendarSchedule(WEEKLY_SCHEDULE_SKIP_DAY_CONVENTION_ID);

		CalendarSchedule once = getCalendarScheduleService().getCalendarSchedule(ONCE_SCHEDULE_ID);
		CalendarSchedule daily = getCalendarScheduleService().getCalendarSchedule(DAILY_SCHEDULE_ID);

		List<CalendarSchedule> children = new ArrayList<>();
		children.add(once);
		children.add(daily);
		rollupChild.setChildCalendarScheduleList(children);

		List<CalendarSchedule> children2 = new ArrayList<>();
		children2.add(rollupChild);
		children2.add(normalChild);
		parent.setChildCalendarScheduleList(children2);

		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(parent)
				.withOccurrencesFromStartDate("12/01/2008 8:00 AM", 5))
				.withExpectedResults(
						"12/28/2008 12:00 PM",
						"12/29/2008 12:00 PM",
						"12/30/2008 12:00 PM",
						"12/31/2008 12:00 PM",
						"01/01/2009 12:00 PM"
				).execute();
	}


	@Test
	public void testSortedSet() {
		SortedSet<Date> dates = new TreeSet<>();

		//duplicates
		Date date1 = DateUtils.toDate("12/01/2008 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date date2 = DateUtils.toDate("12/01/2008 8:00 AM", DateUtils.DATE_FORMAT_SHORT);

		dates.add(date1);
		Assertions.assertEquals(1, dates.size());
		dates.add(date2);
		Assertions.assertEquals(1, dates.size());

		//ordering
		Date date3 = DateUtils.toDate("12/05/2008 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date date4 = DateUtils.toDate("12/01/2008 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date date5 = DateUtils.toDate("12/03/2008 8:00 AM", DateUtils.DATE_FORMAT_SHORT);

		dates.clear();
		dates.add(date3);
		dates.add(date4);
		dates.add(date5);

		Assertions.assertEquals(date4, dates.first());
		Assertions.assertEquals(date3, dates.last());
	}


	@Test
	public void testStartAndEndDateCalendarSchedule() {
		CalendarSchedule schedule = getCalendarScheduleService().copyCalendarSchedule(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID, "testHourlySchedule");
		schedule.setStartDate(DateUtils.toDate("01/26/2010 8:00 AM", DateUtils.DATE_FORMAT_SHORT));
		Assertions.assertEquals("01/26/2010 12:00 AM", DateUtils.fromDate(schedule.getStartDate(), DateUtils.DATE_FORMAT_SHORT));

		schedule.setEndDate(DateUtils.toDate("01/26/2010 8:00 AM", DateUtils.DATE_FORMAT_SHORT));
		Assertions.assertEquals("01/26/2010 12:00 AM", DateUtils.fromDate(schedule.getEndDate(), DateUtils.DATE_FORMAT_SHORT));
	}


	@Test
	public void testSaveCalendarSchedule() {
		CalendarSchedule schedule = getCalendarScheduleService().copyCalendarSchedule(HOURLY_SCHEDULE_30_MINUTE_RECURRENCE_ID, "textHourlySchedule");

		schedule.setCalendar(getCalendarSetupService().getCalendar(MathUtils.SHORT_ONE));
		schedule.setBusinessDayConvention(null);
		Exception ex = null;
		try {
			getCalendarScheduleService().saveCalendarSchedule(schedule);
		}
		catch (Exception e) {
			ex = e;
		}
		if (ex == null) {
			throw new IllegalStateException("Expecting an exception during CalendarSchedule save but none was thrown.");
		}
		Assertions.assertTrue(ex instanceof ValidationException);
		Assertions.assertEquals("Business Day Convention is required when a calendar is selected.", ex.getMessage());
	}


	@Test
	public void testScheduleOccurrencesBetweenDates() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_AT_0_MINUTES_ID)
				.withOccurrencesBetween(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL),
						DateUtils.toDate("2010-05-24 13:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults("05/24/2010 1:00 PM").execute();

		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2010"));
		schedule.setStartTime(new Time(4 * 60 * 60 * 1000));
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(1);
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("10/05/2010 4:00 AM", "10/05/2010 5:00 AM"))
				.withExpectedResults("10/05/2010 4:00 AM").execute();
	}


	@Test
	public void testScheduleOccurrencesBetweenDates_withoutTime() {
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(HOURLY_SCHEDULE_AT_0_MINUTES_ID)
				.withOccurrencesBetween(DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL),
						DateUtils.toDate("2010-05-24 13:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults("05/24/2010 1:00 PM").execute();

		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2010"));
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(1);
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_WITHOUT_TIME_ID));
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("10/05/2010 12:00 AM", "10/05/2010 12:00 AM"))
				.withExpectedResults("10/05/2010 12:00 AM").execute();
	}


	@Test
	public void testModificationOfScheduleWithEntityModificationCondition() {
		TestUtils.expectException(ValidationException.class, () -> {
			setupTestAsAdmin(false);
			CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(DAILY_SCHEDULE_WITH_ENTITY_MODIFY_CONDITION_ID);
			schedule.setDescription("This should not work.");
			getCalendarScheduleService().saveCalendarSchedule(schedule);
		}, "You do not have permission to UPDATE this CalendarSchedule because: FALSE");
	}


	@Test
	public void testGetCalendarScheduleOccurrences_CountExceedsMaximumAllowed() {
		CalendarScheduleOccurrenceCommand occurrenceCommandWithPositiveCount = CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITH_ENTITY_MODIFY_CONDITION_ID).withOccurrencesFromStartDate(new Date(), 51).build();
		TestUtils.expectException(ValidationException.class, () -> getCalendarScheduleService().getCalendarScheduleOccurrences(occurrenceCommandWithPositiveCount),
				String.format("[%d] occurrences were requested for Calendar Schedule [%s], which exceeds the maximum allowed [50].", occurrenceCommandWithPositiveCount.getCount(),
						ObjectUtils.coalesce(occurrenceCommandWithPositiveCount.getCalendarSchedule(), occurrenceCommandWithPositiveCount.getScheduleId())));

		CalendarScheduleOccurrenceCommand occurrenceCommandWithNegativeCount = CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITH_ENTITY_MODIFY_CONDITION_ID).withOccurrencesFromStartDate(new Date(), -51).build();
		TestUtils.expectException(ValidationException.class, () -> getCalendarScheduleService().getCalendarScheduleOccurrences(occurrenceCommandWithNegativeCount),
				String.format("[%d] occurrences were requested for Calendar Schedule [%s], which exceeds the maximum allowed [50].", occurrenceCommandWithNegativeCount.getCount(),
						ObjectUtils.coalesce(occurrenceCommandWithNegativeCount.getCalendarSchedule(), occurrenceCommandWithNegativeCount.getScheduleId())));
	}


	@Test
	public void testGetCalendarScheduleOccurrencesBetween_StartDateAfterEndDate() {
		CalendarScheduleOccurrenceCommand occurrenceCommand = CalendarScheduleOccurrenceCommand.of(DAILY_SCHEDULE_WITH_ENTITY_MODIFY_CONDITION_ID)
				.withOccurrencesBetween(new Date(), DateUtils.addDays(new Date(), -1)).build();
		TestUtils.expectException(ValidationException.class, () -> getCalendarScheduleService().getCalendarScheduleOccurrences(occurrenceCommand),
				String.format("Start Date for occurrences [%s] cannot be after End Date for occurrences [%s].", DateUtils.fromDate(occurrenceCommand.getStartDateTime()), DateUtils.fromDate(occurrenceCommand.getEndDateTime())));
	}


	@Test
	public void testGetCalendarScheduleOccurrences_CountAndEndDateProvided() {
		// if providing both count and end date, expect count to take precedence

		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(DateUtils.toDate("01/01/2010"));
		schedule.setStartTime(new Time(4 * 60 * 60 * 1000));
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(1);
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));
		CalendarScheduleTestExecutor.newExecutorForOccurrenceCommandBuilder(getCalendarScheduleService(), CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("10/05/2010 4:00 AM", "10/20/2010 5:00 AM")
				.withOccurrencesFromStartDate("10/05/2010 4:00 AM", 1))
				.withExpectedResults("10/05/2010 4:00 AM").execute();
	}
}
