package com.clifton.calendar.schedule.api;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.schedule.BaseCalendarScheduleTests;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.schedule.executor.ScheduleApiTestExecutor;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.Date;


@ContextConfiguration("ScheduleApiTests-context.xml")
public class ScheduleApiTests extends BaseCalendarScheduleTests {

	@Resource
	private ScheduleApiService scheduleApiService;


	@Test
	public void testDailyScheduleOccurrences() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setCalendar(getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US));
		schedule.setBusinessDayConvention(BusinessDayConventions.SKIP);
		schedule.setName("Every Business Day at 12:30 pm");
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setRecurrence(11);
		schedule.setStartDate(DateUtils.toDate("03/09/2015"));
		schedule.setStartTime(new Time(DateUtils.toDate("2015-03-09 12:30:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		Date startDate = DateUtils.toDate("2015-03-13 4:14:26", DateUtils.DATE_FORMAT_FULL);
		Date endDate = DateUtils.toDate("2015-03-13 5:14:26", DateUtils.DATE_FORMAT_FULL);
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrencesBetween(schedule.toSchedule(), startDate, endDate)).execute();

		startDate = DateUtils.toDate("2015-03-09 00:00:00", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2015-03-25 00:00:00", DateUtils.DATE_FORMAT_FULL);
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrencesBetween(schedule.toSchedule(), startDate, endDate))
				.withExpectedResults(
						"03/09/2015 12:30 PM",
						"03/24/2015 12:30 PM"
				).execute();


		startDate = DateUtils.toDate("2015-03-19 12:14:26", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2015-03-19 1:14:26", DateUtils.DATE_FORMAT_FULL);
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrencesBetween(schedule.toSchedule(), startDate, endDate)).execute();

		startDate = DateUtils.toDate("2015-03-24 12:14:26", DateUtils.DATE_FORMAT_FULL);
		endDate = DateUtils.toDate("2015-03-24 13:14:26", DateUtils.DATE_FORMAT_FULL);
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrencesBetween(schedule.toSchedule(), startDate, endDate))
				.withExpectedResults("03/24/2015 12:30 PM").execute();
	}


	@Test
	public void testHourlyScheduleOccurrences() {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName("Every 5 Minutes from 6am to 10pm");
		schedule.setFrequency(ScheduleFrequencies.HOURLY);
		schedule.setRecurrence(5);
		schedule.setStartDate(DateUtils.toDate("01/01/2014"));
		schedule.setStartTime(new Time(21600000));
		schedule.setEndTime(new Time(79200000));
		schedule.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleType(STANDARD_CALENDAR_SCHEDULE_TYPE_ID));

		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrencesBetween(schedule.toSchedule(), DateUtils.toDate("2015-04-01 10:50:41", DateUtils.DATE_FORMAT_FULL), DateUtils.toDate("2015-04-01 11:50:41", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults(
						"04/01/2015 10:55 AM",
						"04/01/2015 11:00 AM",
						"04/01/2015 11:05 AM",
						"04/01/2015 11:10 AM",
						"04/01/2015 11:15 AM",
						"04/01/2015 11:20 AM",
						"04/01/2015 11:25 AM",
						"04/01/2015 11:30 AM",
						"04/01/2015 11:35 AM",
						"04/01/2015 11:40 AM",
						"04/01/2015 11:45 AM",
						"04/01/2015 11:50 AM"
				).execute();
	}


	@Test
	public void testMonthlyScheduleOccurrences() {
		// future occurrences
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrencesBetween(MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_ID,
				DateUtils.toDate("2013-01-01 00:00:00"),
				DateUtils.toDate("2013-12-31 00:00:00")))
				.withExpectedResults(
						"01/31/2013 12:00 AM",
						"02/28/2013 12:00 AM",
						"03/28/2013 12:00 AM",
						"04/30/2013 12:00 AM",
						"05/31/2013 12:00 AM",
						"06/28/2013 12:00 AM",
						"07/31/2013 12:00 AM",
						"08/30/2013 12:00 AM",
						"09/30/2013 12:00 AM",
						"10/31/2013 12:00 AM",
						"11/29/2013 12:00 AM",
						"12/31/2013 12:00 AM"
				).execute();

		// previous occurrences
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrences(MONTHLY_SCHEDULE_COUNT_FROM_LAST_DAY_WITH_PREVIOUS_BUSINESS_DAY_CONVENTION_ID,
				-12,
				DateUtils.toDate("2013-12-31 00:00:00")))
				.withExpectedResults(
						"12/31/2013 12:00 AM",
						"11/29/2013 12:00 AM",
						"10/31/2013 12:00 AM",
						"09/30/2013 12:00 AM",
						"08/30/2013 12:00 AM",
						"07/31/2013 12:00 AM",
						"06/28/2013 12:00 AM",
						"05/31/2013 12:00 AM",
						"04/30/2013 12:00 AM",
						"03/28/2013 12:00 AM",
						"02/28/2013 12:00 AM",
						"01/31/2013 12:00 AM"
				).execute();
	}


	@Test
	public void testMonthlyWeekdayScheduleOccurrences() {
		// next occurrences
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrencesBetween(MONTHLY_WEEKDAY_RECURRING_SCHEDULE, DateUtils.toDate("12/02/2012 12:00 AM"), DateUtils.toDate("04/30/2013 12:00 AM")))
				.withExpectedResults(
						"12/07/2012 12:00 AM",
						"12/21/2012 12:00 AM",
						"02/01/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"04/18/2013 12:00 AM"
				).execute();

		// previous occurrences
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrences(MONTHLY_WEEKDAY_RECURRING_SCHEDULE, -6, DateUtils.toDate("04/30/2013 12:00 AM")))
				.withExpectedResults(
						"04/18/2013 12:00 AM",
						"04/05/2013 12:00 AM",
						"02/15/2013 12:00 AM",
						"02/01/2013 12:00 AM",
						"12/21/2012 12:00 AM",
						"12/07/2012 12:00 AM"
				).execute();
	}


	@Test
	public void testWeeklyScheduleOccurrences() {
		//next occurrences
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrences(WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID, 4, DateUtils.toDate("2010-05-24 12:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults(
						"05/29/2010 4:00 AM",
						"06/05/2010 4:00 AM",
						"06/12/2010 4:00 AM",
						"06/19/2010 4:00 AM"
				).execute();

		// previous occurrences
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrences(WEEKLY_SCHEDULE_SATURDAY_AT_4_AM_ID, -4, DateUtils.toDate("2010-01-22 0:34:09", DateUtils.DATE_FORMAT_FULL)))
				.withExpectedResults( // only get 3 occurrences since schedule starts on 1/1/2010
						"01/16/2010 4:00 AM",
						"01/09/2010 4:00 AM",
						"01/02/2010 4:00 AM"
				).execute();
	}


	@Test
	public void testYearlyScheduleOccurrences() {
		// next occurrences
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrencesBetween(YEARLY_JAN_JUL_WITH_NEXT_BUSINESS_DAY_CONVENTION_SCHEDULE_ID, DateUtils.toDate("12/15/2013 12:00 AM"), DateUtils.toDate("01/15/2015 12:00 AM")))
				.withExpectedResults(
						"01/02/2014 8:00 AM",
						"07/01/2014 8:00 AM",
						"01/02/2015 8:00 AM"
				).execute();

		//previous occurrences
		ScheduleApiTestExecutor.newExecutorForOccurrenceCommand(getScheduleApiService(), ScheduleOccurrenceCommand.forOccurrences(YEARLY_JAN_JUL_WITH_NEXT_BUSINESS_DAY_CONVENTION_SCHEDULE_ID, -3, DateUtils.toDate("01/15/2015 12:00 AM")))
				.withExpectedResults(
						"01/02/2015 8:00 AM",
						"07/01/2014 8:00 AM",
						"01/02/2014 8:00 AM"
				).execute();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}
}
