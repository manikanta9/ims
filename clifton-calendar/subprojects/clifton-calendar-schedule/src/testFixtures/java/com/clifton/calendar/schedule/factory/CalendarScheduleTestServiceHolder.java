package com.clifton.calendar.schedule.factory;


import com.clifton.calendar.CalendarTestServiceHolder;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.api.ScheduleApiService;


/**
 * The <code>CalendarScheduleTestServiceHolder</code> holds the service object created be the test factory.
 *
 * @author mwacker
 */
public class CalendarScheduleTestServiceHolder extends CalendarTestServiceHolder {


	private CalendarScheduleService calendarScheduleService;

	private ScheduleApiService scheduleApiService;


	public CalendarScheduleService getCalendarScheduleService() {
		return this.calendarScheduleService;
	}


	public void setCalendarScheduleService(CalendarScheduleService calendarScheduleService) {
		this.calendarScheduleService = calendarScheduleService;
	}


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}
}
