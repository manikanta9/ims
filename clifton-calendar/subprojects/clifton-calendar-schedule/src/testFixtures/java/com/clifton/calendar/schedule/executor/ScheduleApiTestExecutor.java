package com.clifton.calendar.schedule.executor;

import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.test.executor.BaseTestExecutor;
import com.clifton.core.util.date.DateUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * @author mitchellf
 */
public class ScheduleApiTestExecutor extends BaseTestExecutor<Date> {

	private ScheduleApiService scheduleApiService;

	ScheduleOccurrenceCommand occurrenceCommand;


	private ScheduleApiTestExecutor(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ScheduleApiTestExecutor newExecutorForOccurrenceCommand(ScheduleApiService scheduleApiService, ScheduleOccurrenceCommand scheduleOccurrenceCommand) {
		return newExecutor(scheduleApiService).withCommand(scheduleOccurrenceCommand);
	}


	public static ScheduleApiTestExecutor newExecutor(ScheduleApiService scheduleApiService) {
		return new ScheduleApiTestExecutor(scheduleApiService);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ScheduleApiTestExecutor withCommand(ScheduleOccurrenceCommand occurrenceCommand) {
		this.occurrenceCommand = occurrenceCommand;
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getResultEntityTypeName() {
		return "Schedule Occurrence";
	}


	@Override
	protected List<Date> executeTest() {
		return this.scheduleApiService.getScheduleOccurrences(this.occurrenceCommand);
	}


	@Override
	protected String[] getStringValuesForResultEntity(Date date) {
		return Arrays.asList(DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SHORT)).toArray(new String[0]);
	}
}
