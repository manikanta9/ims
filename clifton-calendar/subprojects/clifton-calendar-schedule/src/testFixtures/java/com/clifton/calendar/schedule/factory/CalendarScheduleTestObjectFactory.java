package com.clifton.calendar.schedule.factory;


import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.CalendarTestServiceHolder;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.BusinessDayConventionUtilHandler;
import com.clifton.calendar.schedule.BusinessDayConventionUtilHandlerImpl;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.CalendarScheduleServiceImpl;
import com.clifton.calendar.schedule.CalendarScheduleType;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleApiServiceImpl;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.cache.SimpleCacheHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.NamedEntityCache;
import com.clifton.core.dataaccess.dao.xml.XmlUpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import org.hibernate.Criteria;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.beans.Introspector;
import java.util.Date;
import java.util.List;


public class CalendarScheduleTestObjectFactory {


	public static CalendarScheduleTestServiceHolder newCalendarServices(Date startDate, Date endDate) {

		CalendarTestServiceHolder holder = CalendarTestObjectFactory.createCalendarsAndServices(startDate, endDate);

		CalendarScheduleTestServiceHolder result = new CalendarScheduleTestServiceHolder();
		result.setCalendarBusinessDayService(holder.getCalendarBusinessDayService());
		result.setCalendarSetupService(holder.getCalendarSetupService());

		CalendarScheduleServiceImpl calendarScheduleServiceImpl = (CalendarScheduleServiceImpl) newCalendarScheduleService(startDate, endDate, null, null, holder.getCalendarBusinessDayService());
		result.setCalendarScheduleService(calendarScheduleServiceImpl);

		ScheduleApiServiceImpl scheduleApiServiceImpl = (ScheduleApiServiceImpl) newScheduleApiService(calendarScheduleServiceImpl, holder.getCalendarSetupService());
		result.setScheduleApiService(scheduleApiServiceImpl);

		return result;
	}


	public static CalendarScheduleService newCalendarScheduleService(Date startDate, Date endDate, List<Date> holidays, Calendar calendar) {
		return newCalendarScheduleService(startDate, endDate, holidays, calendar, null);
	}


	public static CalendarScheduleService newCalendarScheduleService(Date startDate, Date endDate, CalendarBusinessDayService businessDayService) {
		return newCalendarScheduleService(startDate, endDate, null, null, businessDayService);
	}


	public static CalendarScheduleService newCalendarScheduleService(Date startDate, Date endDate, List<Date> holidays, Calendar calendar, CalendarBusinessDayService businessDayService) {
		CalendarScheduleServiceImpl result = new CalendarScheduleServiceImpl();

		result.setCalendarScheduleTypeDAO(newCalendarScheduleTypeDAO());
		result.setCalendarScheduleTypeCache(newCalendarScheduleTypeNameCache());

		if (businessDayService != null) {
			result.setCalendarBusinessDayService(businessDayService);
		}
		else {
			result.setCalendarBusinessDayService(CalendarTestObjectFactory.newCalendarBusinessDayService(startDate, endDate, holidays, calendar));
		}

		result.setBusinessDayConventionUtilHandler(newBusinessDayConventionUtilHandler(businessDayService));

		return result;
	}


	public static ScheduleApiService newScheduleApiService(CalendarScheduleService calendarScheduleService, CalendarSetupService calendarSetupService) {
		ScheduleApiServiceImpl result = new ScheduleApiServiceImpl();

		result.setCalendarScheduleService(calendarScheduleService);
		result.setCalendarSetupService(calendarSetupService);

		return result;
	}


	private static AdvancedUpdatableDAO<CalendarScheduleType, Criteria> newCalendarScheduleTypeDAO() {
		CalendarScheduleType type = new CalendarScheduleType();
		type.setName("Standard Schedules");
		type.setTimeSupported(true);
		type.setId((short) 1);

		CalendarScheduleType type_timeUnsupported = new CalendarScheduleType();
		type_timeUnsupported.setName("Standard Schedules Without Time");
		type_timeUnsupported.setTimeSupported(false);
		type_timeUnsupported.setId((short) 2);
		List<CalendarScheduleType> scheduleTypeList = CollectionUtils.createList(type, type_timeUnsupported);

		@SuppressWarnings("unchecked")
		XmlUpdatableDAO<CalendarScheduleType> calendarScheduleTypeDao = Mockito.mock(XmlUpdatableDAO.class);
		Mockito.when(calendarScheduleTypeDao.findAll()).thenReturn(scheduleTypeList);
		Mockito.when(calendarScheduleTypeDao.findOneByField(ArgumentMatchers.eq("name"), ArgumentMatchers.eq(type.getName()))).thenReturn(type);
		Mockito.when(calendarScheduleTypeDao.findOneByFields(ArgumentMatchers.eq(new String[]{"name"}), ArgumentMatchers.eq(new String[]{type.getName()}))).thenReturn(type);

		Mockito.when(calendarScheduleTypeDao.findOneByField(ArgumentMatchers.eq("name"), ArgumentMatchers.eq(type_timeUnsupported.getName()))).thenReturn(type_timeUnsupported);
		Mockito.when(calendarScheduleTypeDao.findOneByFields(ArgumentMatchers.eq(new String[]{"name"}), ArgumentMatchers.eq(new String[]{type_timeUnsupported.getName()}))).thenReturn(type_timeUnsupported);
		return calendarScheduleTypeDao;
	}


	private static NamedEntityCache<CalendarScheduleType> newCalendarScheduleTypeNameCache() {
		NamedEntityCache<CalendarScheduleType> calendarScheduleTypeCache = new NamedEntityCache<>();
		calendarScheduleTypeCache.setDtoClass(CalendarScheduleType.class);
		calendarScheduleTypeCache.setCacheName(Introspector.decapitalize(CalendarScheduleType.class.getSimpleName() + "Cache"));
		calendarScheduleTypeCache.setCacheHandler(new SimpleCacheHandler<>());
		return calendarScheduleTypeCache;
	}


	public static BusinessDayConventionUtilHandler newBusinessDayConventionUtilHandler(CalendarBusinessDayService calendarBusinessDayService) {
		BusinessDayConventionUtilHandlerImpl result = new BusinessDayConventionUtilHandlerImpl();
		result.setCalendarBusinessDayService(calendarBusinessDayService);
		return result;
	}
}
