package com.clifton.calendar.schedule.executor;

import com.clifton.calendar.schedule.CalendarScheduleOccurrenceCommand;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.core.test.executor.BaseTestExecutor;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;
import java.util.List;


/**
 * This executor is designed to make {@link com.clifton.calendar.schedule.CalendarSchedule} tests easier to read/write.
 * It makes use of the {@link CalendarScheduleOccurrenceCommand.Builder}.
 *
 * @author michaelm
 */
public class CalendarScheduleTestExecutor extends BaseTestExecutor<Date> {


	private final CalendarScheduleService calendarScheduleService;

	CalendarScheduleOccurrenceCommand.Builder occurrenceCommandBuilder;


	private CalendarScheduleTestExecutor(CalendarScheduleService calendarScheduleService) {
		this.calendarScheduleService = calendarScheduleService;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static CalendarScheduleTestExecutor newExecutorForOccurrenceCommandBuilder(CalendarScheduleService calendarScheduleService, CalendarScheduleOccurrenceCommand.Builder occurrenceCommandBuilder) {
		return newExecutor(calendarScheduleService).withBuilder(occurrenceCommandBuilder);
	}


	public static CalendarScheduleTestExecutor newExecutor(CalendarScheduleService calendarScheduleService) {
		return new CalendarScheduleTestExecutor(calendarScheduleService);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarScheduleTestExecutor withBuilder(CalendarScheduleOccurrenceCommand.Builder occurrenceCommandBuilder) {
		this.occurrenceCommandBuilder = occurrenceCommandBuilder;
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getResultEntityTypeName() {
		return "Schedule Occurrence";
	}


	@Override
	protected List<Date> executeTest() {
		CalendarScheduleOccurrenceCommand occurrenceCommand = this.occurrenceCommandBuilder.build();
		return this.calendarScheduleService.getCalendarScheduleOccurrences(occurrenceCommand);
	}


	@Override
	protected String[] getStringValuesForResultEntity(Date date) {
		return new String[]{
				DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SHORT)
		};
	}
}
