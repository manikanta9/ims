package com.clifton.calendar.schedule;


import com.clifton.calendar.schedule.search.CalendarScheduleSearchForm;
import com.clifton.calendar.schedule.search.CalendarScheduleTypeSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Date;
import java.util.List;


/**
 * The <code>CalendarScheduleService</code> interface defines methods for managing Schedules.
 *
 * @author mwacker
 */
public interface CalendarScheduleService {

	/////////////////////////////////////////////////////////////////////////////
	////////           Calendar Schedule Business Methods             ///////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarSchedule getCalendarSchedule(int id);


	public List<CalendarSchedule> getCalendarScheduleList(CalendarScheduleSearchForm searchForm);


	public CalendarSchedule copyCalendarSchedule(int id, String name);


	public CalendarSchedule saveCalendarSchedule(CalendarSchedule bean);


	public void deleteCalendarSchedule(int id);

	/////////////////////////////////////////////////////////////////////////////
	////////           Calendar Schedule Type Business Methods             ///////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarScheduleType getCalendarScheduleType(short id);


	public CalendarScheduleType getCalendarScheduleTypeByName(String name);


	public List<CalendarScheduleType> getCalendarScheduleTypeList(CalendarScheduleTypeSearchForm searchForm);


	public CalendarScheduleType saveCalendarScheduleType(CalendarScheduleType bean);


	public void deleteCalendarScheduleType(short id);


	////////////////////////////////////////////////////////////////////////////
	////////               Schedule Occurrences Methods                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Get the previous or next number of occurrences.
	 * <p>
	 * If endDate is not specified on the command, it is assumed that a number of occurrences are wanted
	 * If endDate is specified on the command, it is assumed that all occurrences between start and end date are wanted
	 * <p>
	 * If both endDate and count are specified on the command, whichever is more restrictive (results in fewer number of occurrences) will be used.
	 * For example, if there are 5 occurrences between startDate and endDate, and command.getCount() == 4, only the first 4 occurrences will be returned.
	 * Alternatively, if there 5 occurrences between startDate and endDate, but command.getCount() == 6, only the 5 occurrences between the two dates will be returned.
	 */
	@ModelAttribute("data")
	@SecureMethod(dtoClass = CalendarSchedule.class)
	public List<Date> getCalendarScheduleOccurrences(CalendarScheduleOccurrenceCommand occurrenceCommand);


	/**
	 * Returns a List of {@link Date} objects that represent the date and time of each occurrence between the start and end dates.
	 */
	@DoNotAddRequestMapping
	public Date getCalendarScheduleOccurrence(CalendarScheduleOccurrenceCommand occurrenceCommand);
}
