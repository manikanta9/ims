package com.clifton.calendar.schedule.calculator;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceResult;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;

import java.util.Date;


public class DailyCalendarScheduleOccurrenceCalculator extends AbstractCalendarScheduleOccurrenceCalculator {

	@Override
	public ScheduleFrequencies getFrequency() {
		return ScheduleFrequencies.DAILY;
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		// check schedule start and end dates
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_START) {
			return new CalendarScheduleOccurrenceResult(getCalendarScheduleStartDateTime(schedule));
		}
		else if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END) {
			return null;
		}

		if (getBusinessDayConvention(schedule).isLimitedToBusinessDays()) {
			CalendarScheduleOccurrenceResult result = calculateOccurrenceByBusinessDays(schedule, occurrenceFromDate, true, overrideCalendarIds);
			return DateUtils.isDateBefore(result.getDateOfOccurrence(), occurrenceFromDate, schedule.getCalendarScheduleType().isTimeSupported()) ? null : result;
		}
		return calculateOccurrence(schedule, occurrenceFromDate, true);
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetPreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		// check schedule start and end dates
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_END) {
			return calculateLastValidOccurrenceForSchedule(schedule);
		}
		else if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START) {
			return null;
		}

		if (getBusinessDayConvention(schedule).isLimitedToBusinessDays()) {
			CalendarScheduleOccurrenceResult result = calculateOccurrenceByBusinessDays(schedule, occurrenceFromDate, false, overrideCalendarIds);
			return DateUtils.isDateAfter(result.getDateOfOccurrence(), occurrenceFromDate) ? null : result;
		}
		return calculateOccurrence(schedule, occurrenceFromDate, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarScheduleOccurrenceResult calculateLastValidOccurrenceForSchedule(CalendarSchedule schedule) {
		if (schedule.getCalendarScheduleType().isTimeSupported() && schedule.getEndTime() == null || schedule.getStartTime().getMilliseconds() <= schedule.getEndTime().getMilliseconds()) {
			return new CalendarScheduleOccurrenceResult(DateUtils.setTime(schedule.getEndDate(), schedule.getStartTime()), DateUtils.addDays(schedule.getEndDate(), -1));
		}
		return doGetPreviousOccurrence(schedule, DateUtils.addDays(schedule.getEndDate(), -1));
	}


	private CalendarScheduleOccurrenceResult calculateOccurrenceByBusinessDays(CalendarSchedule schedule, Date date, boolean nextOccurrence, Short... overrideCalendarIds) {
		Date startDateTime = getCalendarScheduleStartDateTime(schedule);

		Date currentDailyActualOccurrence = getBusinessDayConventionUtilHandler().applyBusinessDayConvention(DateUtils.setTime(date, startDateTime), schedule.getBusinessDayConvention(), overrideCalendarIds);

		if (currentDailyActualOccurrence == null || (!nextOccurrence && currentDailyActualOccurrence.after(date))) {
			return nextOccurrence ? doGetNextOccurrence(schedule, DateUtils.addDays(DateUtils.clearTime(date), 1), overrideCalendarIds) : doGetPreviousOccurrence(schedule, DateUtils.addDays(DateUtils.setTime(date, startDateTime), -1), overrideCalendarIds);
		}

		int days = getCalendarBusinessDayService().getBusinessDaysBetween(startDateTime, currentDailyActualOccurrence, null, overrideCalendarIds);

		int actualOccurrences = days / schedule.getRecurrence();
		actualOccurrences = nextOccurrence ? adjustForOccurrenceOfToday(date, startDateTime, actualOccurrences) : actualOccurrences;

		Date occurrenceResultDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(startDateTime, overrideCalendarIds), actualOccurrences * schedule.getRecurrence());
		return new CalendarScheduleOccurrenceResult(getBusinessDayConventionUtilHandler().applyBusinessDayConvention(occurrenceResultDate, schedule.getBusinessDayConvention(), overrideCalendarIds));
	}


	private CalendarScheduleOccurrenceResult calculateOccurrence(CalendarSchedule schedule, Date date, boolean nextOccurrence) {
		Date startDateTime = getCalendarScheduleStartDateTime(schedule);

		int days = DateUtils.getDaysDifference(date, schedule.getStartDate());
		int actualOccurrences = days / schedule.getRecurrence();

		// if today's occurrence hasn't happened yet then remove 1 occurrence
		Date startTime = DateUtils.setTime((Date) date.clone(), startDateTime);
		actualOccurrences = adjustForOccurrenceOfToday(date, startTime, actualOccurrences);

		Date occurrenceResultDate = DateUtils.addDays(startDateTime, actualOccurrences * schedule.getRecurrence());
		if (nextOccurrence) {
			boolean dateBefore = schedule.getCalendarScheduleType().isTimeSupported() ?
					DateUtils.isDateBefore(DateUtils.setTime(occurrenceResultDate, schedule.getStartTime()), date, true)
					: DateUtils.isDateBefore(occurrenceResultDate, date, false);
			if (dateBefore) {
				return doGetNextOccurrence(schedule, DateUtils.addDays(DateUtils.clearTime(date), 1));
			}
		}
		else {
			if (DateUtils.isDateAfter(schedule.getCalendarScheduleType().isTimeSupported() ? DateUtils.setTime(occurrenceResultDate, schedule.getStartTime()) : occurrenceResultDate, date)) {
				return doGetPreviousOccurrence(schedule, DateUtils.addDays(DateUtils.clearTime(date), -1));
			}
		}
		return new CalendarScheduleOccurrenceResult(occurrenceResultDate);
	}


	/**
	 * If today's occurrence has happened then add 1 occurrence.
	 */
	private int adjustForOccurrenceOfToday(Date occurrenceFromDate, Date startDate, int actualOccurrences) {
		Time instanceTime = new Time(occurrenceFromDate);
		Time scheduleTime = new Time(startDate);
		if (instanceTime.getMilliseconds() > scheduleTime.getMilliseconds()) {
			return actualOccurrences + 1;
		}
		return actualOccurrences;
	}
}
