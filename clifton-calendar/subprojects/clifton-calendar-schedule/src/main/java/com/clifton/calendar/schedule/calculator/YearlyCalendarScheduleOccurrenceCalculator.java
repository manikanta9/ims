package com.clifton.calendar.schedule.calculator;


import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceResult;
import com.clifton.calendar.schedule.CalendarScheduleUtils;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.core.util.date.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class YearlyCalendarScheduleOccurrenceCalculator extends AbstractCalendarScheduleOccurrenceCalculator {

	@Override
	public ScheduleFrequencies getFrequency() {
		return ScheduleFrequencies.YEARLY;
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_START) {
			occurrenceFromDate = getCalendarScheduleStartDateTime(schedule, false);
		}
		else if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END) {
			return null;
		}

		if (DateUtils.getDayOfMonth(occurrenceFromDate) > getScheduleDayOfMonth(schedule) || !getMonthOccurrences(schedule).contains(DateUtils.getMonthOfYear(occurrenceFromDate))) {
			occurrenceFromDate = DateUtils.getFirstDayOfNextMonthWithoutTime(occurrenceFromDate);
		}

		return calculateNextOccurrence(schedule, occurrenceFromDate, overrideCalendarIds);
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetPreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_END) {
			occurrenceFromDate = getCalendarScheduleEndDateTime(schedule);
		}
		else if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START) {
			return null;
		}

		if (DateUtils.getDayOfMonth(occurrenceFromDate) < getScheduleDayOfMonth(schedule) || !getMonthOccurrences(schedule).contains(DateUtils.getMonthOfYear(occurrenceFromDate))) {
			return getPreviousOccurrence(schedule, DateUtils.setTime(DateUtils.getLastDayOfMonth(DateUtils.addMonths(occurrenceFromDate, -1)), getCalendarScheduleEndDateTimeOrStartDateTime(schedule)), overrideCalendarIds);
		}

		return calculatePreviousOccurrence(schedule, occurrenceFromDate, overrideCalendarIds);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarScheduleOccurrenceResult calculateNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {

		Date startDateTime = getCalendarScheduleStartDateTime(schedule);
		Date fromDateWithScheduleStartTime = DateUtils.setTime(occurrenceFromDate, startDateTime);
		if (DateUtils.getMinutesFromMidnight(occurrenceFromDate) > DateUtils.getMinutesFromMidnight(fromDateWithScheduleStartTime)) {
			return getNextOccurrence(schedule, DateUtils.addDays(occurrenceFromDate, 1), overrideCalendarIds);
		}

		Date firstDayOfStartYear = DateUtils.getFirstDayOfYear(startDateTime);
		int years = DateUtils.getYearsDifference(occurrenceFromDate, firstDayOfStartYear);
		int actualOccurrences = years / schedule.getRecurrence();
		if (years - actualOccurrences * schedule.getRecurrence() >= 1) {
			return getNextOccurrence(schedule, DateUtils.addYears(firstDayOfStartYear, schedule.getRecurrence()), overrideCalendarIds);
		}

		int monthOfYear = DateUtils.getMonthOfYear(occurrenceFromDate);
		int monthsToAdd = CalendarScheduleUtils.getCountToAdd(getMonthOccurrences(schedule), monthOfYear, 12);
		if (monthsToAdd < 0) {
			return getNextOccurrence(schedule, DateUtils.addYears(DateUtils.getFirstDayOfYear(occurrenceFromDate), schedule.getRecurrence()), overrideCalendarIds);
		}

		Date nextOccurrenceDate = DateUtils.addDays(DateUtils.addMonths(occurrenceFromDate, monthsToAdd), getDaysToAdd(schedule, occurrenceFromDate));

		// move or skip the date based on the business day action
		Date businessDayResult = applyBusinessDays(nextOccurrenceDate, schedule, overrideCalendarIds);

		if (businessDayResult == null && getBusinessDayConvention(schedule) == BusinessDayConventions.SKIP) {
			return getNextOccurrence(schedule, DateUtils.getFirstDayOfNextMonthWithoutTime(occurrenceFromDate), overrideCalendarIds);
		}
		else if (businessDayResult == null) {
			return getNextOccurrence(schedule, DateUtils.clearTime(DateUtils.addDays(DateUtils.getLastDayOfMonth(nextOccurrenceDate), 1)), overrideCalendarIds);
		}
		else {
			nextOccurrenceDate = businessDayResult;
		}

		return new CalendarScheduleOccurrenceResult(DateUtils.setTime(nextOccurrenceDate, getCalendarScheduleStartDateTime(schedule)));
	}


	private CalendarScheduleOccurrenceResult calculatePreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {

		Date fromDateWithScheduleEndOrStartTime = DateUtils.setTime(occurrenceFromDate, getCalendarScheduleEndDateTimeOrStartDateTime(schedule));
		if (DateUtils.getMinutesFromMidnight(occurrenceFromDate) < DateUtils.getMinutesFromMidnight(fromDateWithScheduleEndOrStartTime)) {
			return getPreviousOccurrence(schedule, DateUtils.addDays(fromDateWithScheduleEndOrStartTime, -1), overrideCalendarIds);
		}

		Date firstDayOfStartYear = DateUtils.getFirstDayOfYear(getCalendarScheduleStartDateTime(schedule));
		int years = DateUtils.getYearsDifference(occurrenceFromDate, firstDayOfStartYear);
		int actualOccurrences = years / schedule.getRecurrence();
		if (years - actualOccurrences * schedule.getRecurrence() >= 1) {
			return getPreviousOccurrence(schedule, DateUtils.addYears(DateUtils.getLastDayOfYear(occurrenceFromDate), -(schedule.getRecurrence() - 1)), overrideCalendarIds);
		}

		int monthOfYear = DateUtils.getMonthOfYear(occurrenceFromDate);
		int monthsToSubtract = CalendarScheduleUtils.getCountToSubtract(getMonthOccurrences(schedule), monthOfYear);
		if (monthsToSubtract < 0) {
			return getPreviousOccurrence(schedule, DateUtils.addYears(DateUtils.getLastDayOfYear(occurrenceFromDate), -schedule.getRecurrence()), overrideCalendarIds);
		}

		Date previousOccurrenceDate = DateUtils.addDays(DateUtils.addMonths(occurrenceFromDate, -monthsToSubtract), getDaysToAdd(schedule, occurrenceFromDate));

		// move or skip the date based on the business day action
		Date businessDayResult = applyBusinessDays(previousOccurrenceDate, schedule, overrideCalendarIds);

		if (businessDayResult == null || isDateResultWithScheduleEndTimeAfterRequestDate(schedule, businessDayResult, occurrenceFromDate)) {
			return getPreviousOccurrence(schedule, DateUtils.addDays(DateUtils.setTime(occurrenceFromDate, getCalendarScheduleEndDateTimeOrStartDateTime(schedule)), -1), overrideCalendarIds);
		}
		else {
			previousOccurrenceDate = businessDayResult;
		}

		return new CalendarScheduleOccurrenceResult(DateUtils.setTime(previousOccurrenceDate, getCalendarScheduleEndDateTimeOrStartDateTime(schedule)));
	}


	/**
	 * Returns the list of integers that represent the months when the schedule will have an occurrence.
	 *
	 * @param schedule
	 */
	private List<Integer> getMonthOccurrences(CalendarSchedule schedule) {
		List<Integer> result = new ArrayList<>();
		if (schedule.isJanuary()) {
			result.add(1);
		}
		if (schedule.isFebruary()) {
			result.add(2);
		}
		if (schedule.isMarch()) {
			result.add(3);
		}
		if (schedule.isApril()) {
			result.add(4);
		}
		if (schedule.isMay()) {
			result.add(5);
		}
		if (schedule.isJune()) {
			result.add(6);
		}
		if (schedule.isJuly()) {
			result.add(7);
		}
		if (schedule.isAugust()) {
			result.add(8);
		}
		if (schedule.isSeptember()) {
			result.add(9);
		}
		if (schedule.isOctober()) {
			result.add(10);
		}
		if (schedule.isNovember()) {
			result.add(11);
		}
		if (schedule.isDecember()) {
			result.add(12);
		}

		if (result.isEmpty()) {
			result.add(DateUtils.getMonthOfYear(schedule.getStartDate()));
		}
		return result;
	}


	private int getDaysToAdd(CalendarSchedule schedule, Date occurrenceFromDate) {
		return getScheduleDayOfMonth(schedule) - DateUtils.getDayOfMonth(occurrenceFromDate);
	}


	private int getScheduleDayOfMonth(CalendarSchedule schedule) {
		return schedule.getDayOfMonth() == 0 ? DateUtils.getDayOfMonth(schedule.getStartDate()) : schedule.getDayOfMonth();
	}
}
