package com.clifton.calendar.schedule;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.date.DateUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Objects;


@Component
public class BusinessDayConventionUtilHandlerImpl implements BusinessDayConventionUtilHandler {

	private CalendarBusinessDayService calendarBusinessDayService;

	/////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date applyBusinessDayConvention(CalendarSchedule schedule, Date date) {
		return applyBusinessDayConvention(date, getBusinessDayConvention(schedule), schedule.getCalendar());
	}


	public Date applyBusinessDayConvention(Date date, Calendar... calendars) {
		return applyBusinessDayConvention(date, BusinessDayConventions.UNADJUSTED, calendars);
	}


	@Override
	public Date applyBusinessDayConvention(Date date, BusinessDayConventions convention, Calendar... calendars) {

		if (convention == null) {
			convention = BusinessDayConventions.UNADJUSTED;
		}

		Short[] calendarIds = ArrayUtils.getStream(calendars)
				.filter(Objects::nonNull)
				.map(Calendar::getId)
				.distinct()
				.toArray(Short[]::new);

		return applyBusinessDayConvention(date, convention, calendarIds);
	}


	@Override
	public Date applyBusinessDayConvention(Date date, BusinessDayConventions convention, Short... calendarIds) {
		if (date == null) {
			return null;
		}
		Date result = date;

		if (convention == null || convention == BusinessDayConventions.UNADJUSTED || ArrayUtils.isEmpty(calendarIds)) {
			return result;
		}

		if (convention.isLimitedToBusinessDays() && !getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(result, calendarIds))) {
			switch (convention) {
				case NEXT:
					result = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(result, calendarIds));
					break;
				case NEXT_MODIFIED: {
					Date businessDayResult = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(result, calendarIds));
					if (DateUtils.getMonthOfYear(businessDayResult) != DateUtils.getMonthOfYear(result)) {
						result = getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(result, calendarIds));
					}
					else {
						result = businessDayResult;
					}
					break;
				}
				case SKIP:
					result = null;
					break;
				case PREVIOUS: {
					result = getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(result, calendarIds));
					break;
				}
				case PREVIOUS_MODIFIED: {
					Date previousBusinessDay = getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(result, calendarIds));
					if (DateUtils.getMonthOfYear(previousBusinessDay) != DateUtils.getMonthOfYear(result)) {
						result = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(result, calendarIds));
					}
					else {
						result = previousBusinessDay;
					}
					break;
				}
				default:
					break;
			}
		}
		return result;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessDayConventions getBusinessDayConvention(CalendarSchedule schedule) {
		return schedule.getBusinessDayConvention() == null ? BusinessDayConventions.UNADJUSTED : schedule.getBusinessDayConvention();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
