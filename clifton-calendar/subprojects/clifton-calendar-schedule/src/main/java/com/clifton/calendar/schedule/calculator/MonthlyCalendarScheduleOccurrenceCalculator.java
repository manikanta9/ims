package com.clifton.calendar.schedule.calculator;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceResult;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


public class MonthlyCalendarScheduleOccurrenceCalculator extends AbstractCalendarScheduleOccurrenceCalculator {

	@Override
	public ScheduleFrequencies getFrequency() {
		return ScheduleFrequencies.MONTHLY;
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		return getNextMonthlyOccurrence(schedule, occurrenceFromDate, occurrenceFromDate, overrideCalendarIds);
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetPreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		return getPreviousMonthlyOccurrence(schedule, occurrenceFromDate, occurrenceFromDate, overrideCalendarIds);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected Date getFirstDayOfNextMonthWithScheduleStartOrEndTime(Date date, CalendarSchedule schedule) {
		return DateUtils.setTime(DateUtils.addDays(DateUtils.getLastDayOfMonth(date), 1), getCalendarScheduleEndDateTimeOrStartDateTime(schedule));
	}


	protected Date getLastDayOfPreviousMonthWithScheduleEndOrStartTime(CalendarSchedule schedule, Date date) {
		return DateUtils.setTime(DateUtils.getLastDayOfPreviousMonth(date), getCalendarScheduleEndDateTimeOrStartDateTime(schedule));
	}


	protected Date getMonthlyVirtualStartDate(CalendarSchedule schedule) {
		return schedule.getFirstOccurrence() != null ? DateUtils.addMonths(schedule.getFirstOccurrence(), -1 * schedule.getRecurrence()) : null;
	}


	protected boolean isOccurrenceFromDateMonthViable(CalendarSchedule schedule, Date occurrenceFromDate) {
		if (getMonthlyVirtualStartDate(schedule) != null) {
			int months = DateUtils.getMonthsDifference(occurrenceFromDate, getMonthlyVirtualStartDate(schedule));
			int actualOccurrences = months / schedule.getRecurrence();
			if ((months - actualOccurrences * schedule.getRecurrence()) >= 1) {
				return false;
			}
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////               Next Occurrence Implementation               ////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarScheduleOccurrenceResult getNextMonthlyOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Date originalOccurrenceFromDate, Short... overrideCalendarIds) {
		// check schedule start and end dates
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_START) {
			occurrenceFromDate = getCalendarScheduleStartDateTime(schedule);
		}
		else if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END) {
			return null;
		}

		if (!isOccurrenceFromDateMonthViable(schedule, occurrenceFromDate)) {
			return getNextMonthlyOccurrence(schedule, getFirstDayOfNextMonthWithScheduleStartOrEndTime(occurrenceFromDate, schedule), originalOccurrenceFromDate, overrideCalendarIds);
		}

		// make sure we don't skip the first occurrence in the occurrence command (e.g. when additional business days moves occurrence to a different month)
		if (!MathUtils.isNullOrZero(schedule.getAdditionalBusinessDays()) && occurrenceFromDate == originalOccurrenceFromDate && !DateUtils.isEqual(occurrenceFromDate, getCalendarScheduleStartDateTime(schedule))) {
			CalendarScheduleOccurrenceResult nextOccurrenceFromPreviousMonth = getNextMonthlyOccurrence(schedule, DateUtils.addMonths(occurrenceFromDate, -1), originalOccurrenceFromDate, overrideCalendarIds);
			if (nextOccurrenceFromPreviousMonth != null && nextOccurrenceFromPreviousMonth.getDateOfOccurrence() != null) {
				return nextOccurrenceFromPreviousMonth;
			}
		}

		int scheduleDayOfMonth = getScheduleDayOfMonth(schedule, occurrenceFromDate);
		Date nextOccurrenceDate = DateUtils.addDays(DateUtils.getFirstDayOfMonth(occurrenceFromDate), scheduleDayOfMonth - 1);
		Date nextOccurrenceAdjustedForBusinessDays = getOccurrenceAdjustedForBusinessDays(schedule, nextOccurrenceDate, originalOccurrenceFromDate, true, overrideCalendarIds);
		Date adjustedNextOccurrence = nextOccurrenceAdjustedForBusinessDays != null ? nextOccurrenceAdjustedForBusinessDays : nextOccurrenceDate;

		if (isNextOccurrenceRequired(schedule, originalOccurrenceFromDate, adjustedNextOccurrence)) {
			return getNextMonthlyOccurrence(schedule, getFirstDayOfNextMonthWithScheduleStartOrEndTime(occurrenceFromDate, schedule), originalOccurrenceFromDate, overrideCalendarIds);
		}
		if (isDateResultWithScheduleEndTimeAfterRequestDate(schedule, adjustedNextOccurrence, getCalendarScheduleEndDateTime(schedule))) {
			return null;
		}

		// move or skip the date based on the business day action
		if (nextOccurrenceAdjustedForBusinessDays == null && getBusinessDayConvention(schedule) == BusinessDayConventions.SKIP) {
			return getNextMonthlyOccurrence(schedule, getFirstDayOfNextMonthWithScheduleStartOrEndTime(occurrenceFromDate, schedule), originalOccurrenceFromDate, overrideCalendarIds);
		}
		else if (nextOccurrenceAdjustedForBusinessDays == null || isDateBeforeAndNotEqualToRequestDate(DateUtils.setTime(nextOccurrenceAdjustedForBusinessDays, getCalendarScheduleStartDateTime(schedule)), originalOccurrenceFromDate, schedule.getCalendarScheduleType().isTimeSupported())) {
			if (DateUtils.clearTime(DateUtils.addDays(DateUtils.getLastDayOfMonth(nextOccurrenceDate), 1)).equals(occurrenceFromDate)) {
				return null;
			}
			return getNextMonthlyOccurrence(schedule, DateUtils.clearTime(DateUtils.addDays(DateUtils.getLastDayOfMonth(nextOccurrenceDate), 1)), originalOccurrenceFromDate, overrideCalendarIds);
		}
		// set the time of day for the result and return
		return new CalendarScheduleOccurrenceResult(DateUtils.setTime(nextOccurrenceAdjustedForBusinessDays, getCalendarScheduleStartDateTime(schedule)),
				getScheduleNextOccurrenceFromDate(schedule, occurrenceFromDate, nextOccurrenceAdjustedForBusinessDays, nextOccurrenceDate));
	}


	private boolean isNextOccurrenceRequired(CalendarSchedule schedule, Date originalOccurrenceFromDate, Date adjustedNextOccurrence) {
		return isOriginalOccurrenceFromDateAfterAdjustedNextOccurrence(schedule, originalOccurrenceFromDate, adjustedNextOccurrence) || isDateResultWithScheduleStartTimeBeforeRequestDate(schedule, adjustedNextOccurrence, getCalendarScheduleStartDateTime(schedule));
	}


	private boolean isOriginalOccurrenceFromDateAfterAdjustedNextOccurrence(CalendarSchedule schedule, Date originalOccurrenceFromDate, Date adjustedNextOccurrence) {
		Date adjustedNextOccurrenceWithScheduleStartTime = DateUtils.setTime(adjustedNextOccurrence, getCalendarScheduleStartDateTime(schedule));
		return DateUtils.isDateBefore(adjustedNextOccurrenceWithScheduleStartTime, originalOccurrenceFromDate, true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////             Previous Occurrence Implementation             ////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarScheduleOccurrenceResult getPreviousMonthlyOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Date originalOccurrenceFromDate, Short... overrideCalendarIds) {
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_END) {
			occurrenceFromDate = getCalendarScheduleEndDateTimeOrStartDateTime(schedule);
		}
		else if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START) {
			return null;
		}

		// make sure we don't skip the first occurrence in the occurrence command (e.g. when additional business days moves occurrence to a different month)
		if (!MathUtils.isNullOrZero(schedule.getAdditionalBusinessDays()) && occurrenceFromDate == originalOccurrenceFromDate && !DateUtils.isEqual(occurrenceFromDate, getCalendarScheduleEndDateTimeOrStartDateTime(schedule))) {
			CalendarScheduleOccurrenceResult previousOccurrenceFromNextMonth = getPreviousMonthlyOccurrence(schedule, DateUtils.addMonths(occurrenceFromDate, 1), originalOccurrenceFromDate, overrideCalendarIds);
			if (previousOccurrenceFromNextMonth != null && !isDateResultWithScheduleEndTimeAfterRequestDate(schedule, previousOccurrenceFromNextMonth.getDateOfOccurrence(), occurrenceFromDate)) {
				return previousOccurrenceFromNextMonth;
			}
		}

		if (!isOccurrenceFromDateMonthViable(schedule, occurrenceFromDate)) {
			return getPreviousMonthlyOccurrence(schedule, getLastDayOfPreviousMonthWithScheduleEndOrStartTime(schedule, occurrenceFromDate), originalOccurrenceFromDate, overrideCalendarIds);
		}

		int scheduleDayOfMonth = getScheduleDayOfMonth(schedule, occurrenceFromDate);
		Date previousOccurrenceDate = DateUtils.addDays(DateUtils.getFirstDayOfMonth(occurrenceFromDate), scheduleDayOfMonth - 1);
		Date previousOccurrenceAdjustedForBusinessDays = getOccurrenceAdjustedForBusinessDays(schedule, previousOccurrenceDate, originalOccurrenceFromDate, false, overrideCalendarIds);
		Date adjustedPreviousOccurrence = previousOccurrenceAdjustedForBusinessDays != null ? previousOccurrenceAdjustedForBusinessDays : previousOccurrenceDate;

		if (isPreviousOccurrenceRequired(schedule, originalOccurrenceFromDate, adjustedPreviousOccurrence)) {
			return getPreviousMonthlyOccurrence(schedule, getLastDayOfPreviousMonthWithScheduleEndOrStartTime(schedule, occurrenceFromDate), originalOccurrenceFromDate, overrideCalendarIds);
		}
		if (isDateResultWithScheduleStartTimeBeforeRequestDate(schedule, previousOccurrenceAdjustedForBusinessDays != null ? previousOccurrenceAdjustedForBusinessDays : previousOccurrenceDate, getCalendarScheduleStartDateTime(schedule))) {
			return null;
		}

		// move or skip the date based on the business day action
		if (previousOccurrenceAdjustedForBusinessDays == null && getBusinessDayConvention(schedule) == BusinessDayConventions.SKIP) {
			Date endOfPreviousMonth = getLastDayOfPreviousMonthWithScheduleEndOrStartTime(schedule, occurrenceFromDate);
			return getPreviousMonthlyOccurrence(schedule, endOfPreviousMonth, originalOccurrenceFromDate, overrideCalendarIds);
		}
		else if (previousOccurrenceAdjustedForBusinessDays == null || isDateAfterAndNotEqualToRequestDate(DateUtils.setTime(previousOccurrenceAdjustedForBusinessDays, getCalendarScheduleStartDateTime(schedule)), originalOccurrenceFromDate)) {
			Date endOfPreviousMonth = getLastDayOfPreviousMonthWithScheduleEndOrStartTime(schedule, previousOccurrenceDate);
			return getPreviousMonthlyOccurrence(schedule, endOfPreviousMonth, originalOccurrenceFromDate, overrideCalendarIds);
		}
		// set the time of day for the result and return
		return new CalendarScheduleOccurrenceResult(DateUtils.setTime(previousOccurrenceAdjustedForBusinessDays, getCalendarScheduleStartDateTime(schedule)),
				getSchedulePreviousOccurrenceFromDate(schedule, occurrenceFromDate, previousOccurrenceAdjustedForBusinessDays, previousOccurrenceDate));
	}


	private boolean isPreviousOccurrenceRequired(CalendarSchedule schedule, Date originalOccurrenceFromDate, Date adjustedPreviousOccurrence) {
		return isOriginalOccurrenceFromDateBeforeAdjustedPreviousOccurrence(schedule, originalOccurrenceFromDate, adjustedPreviousOccurrence) || isDateResultWithScheduleEndTimeAfterRequestDate(schedule, adjustedPreviousOccurrence, getCalendarScheduleEndDateTime(schedule));
	}


	private boolean isDateAfterAndNotEqualToRequestDate(Date date, Date requestDate) {
		return requestDate == null || (!date.before(requestDate) && DateUtils.compare(date, requestDate, true) != 0);
	}


	private boolean isOriginalOccurrenceFromDateBeforeAdjustedPreviousOccurrence(CalendarSchedule schedule, Date originalOccurrenceFromDate, Date adjustedPreviousOccurrence) {
		Date adjustedNextOccurrenceWithScheduleStartTime = DateUtils.setTime(adjustedPreviousOccurrence, getCalendarScheduleStartDateTime(schedule));
		return DateUtils.isDateBefore(originalOccurrenceFromDate, adjustedNextOccurrenceWithScheduleStartTime, true);
	}


	private Date getSchedulePreviousOccurrenceFromDate(CalendarSchedule schedule, Date occurrenceFromDate, Date previousOccurrenceAdjustedForBusinessDays, Date previousOccurrence) {
		if (isDateResultWithScheduleEndTimeAfterRequestDate(schedule, previousOccurrenceAdjustedForBusinessDays, occurrenceFromDate)) {
			return DateUtils.addMinutes(previousOccurrence, -1);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int getScheduleDayOfMonth(CalendarSchedule schedule, Date occurrenceFromDate) {
		int scheduleDayOfMonth = schedule.getDayOfMonth();
		if (schedule.isCountFromLastDayOfMonth()) {
			scheduleDayOfMonth = DateUtils.getDayOfMonth(DateUtils.getLastDayOfMonth(occurrenceFromDate)) - schedule.getDayOfMonth() + 1;
			if (scheduleDayOfMonth <= 0) {
				scheduleDayOfMonth = 1;
			}
		}
		return scheduleDayOfMonth;
	}


	private Date getOccurrenceAdjustedForBusinessDays(CalendarSchedule schedule, Date occurrenceDate, Date originalOccurrenceFromDate, boolean nextOccurrence, Short... overrideCalendarIds) {
		Date currentMonthActualOccurrence = getBusinessDayConventionUtilHandler().applyBusinessDayConvention(occurrenceDate, schedule.getBusinessDayConvention(), overrideCalendarIds);
		return applyAdditionalBusinessDays(schedule, currentMonthActualOccurrence, originalOccurrenceFromDate, nextOccurrence, overrideCalendarIds);
	}


	private Date applyAdditionalBusinessDays(CalendarSchedule schedule, Date date, Date requestDate, boolean nextOccurrence, Short... overrideCalendarIds) {

		// if there is no date or calendar, do nothing
		if (date != null && !ArrayUtils.isEmpty(overrideCalendarIds) && schedule.getFrequency().isAdditionalBusinessDaysSupported() && !MathUtils.isNullOrZero(schedule.getAdditionalBusinessDays())) {
			date = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, overrideCalendarIds), schedule.getAdditionalBusinessDays());
			if ((nextOccurrence && isDateResultWithScheduleStartTimeBeforeRequestDate(schedule, date, requestDate)) || (!nextOccurrence && isDateResultWithScheduleEndTimeAfterRequestDate(schedule, date, requestDate))) {
				date = null;
			}
		}
		return date;
	}


	private Date getScheduleNextOccurrenceFromDate(CalendarSchedule schedule, Date occurrenceFromDate, Date previousOccurrenceAdjustedForBusinessDays, Date nextOccurrence) {
		if (isDateResultWithScheduleStartTimeBeforeRequestDate(schedule, previousOccurrenceAdjustedForBusinessDays, occurrenceFromDate)) {
			return DateUtils.addMinutes(nextOccurrence, 1);
		}
		return null;
	}
}
