package com.clifton.calendar.schedule.calculator;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.BusinessDayConventionUtilHandler;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceResult;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;


public abstract class AbstractCalendarScheduleOccurrenceCalculator implements CalendarScheduleOccurrenceCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;
	private BusinessDayConventionUtilHandler businessDayConventionUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarScheduleOccurrenceResult getNextOrPreviousOccurrence(CalendarSchedule schedule, Date date, boolean nextOccurrence, Short... overrideCalendarIds) {
		if (schedule.getCalendar() != null && ArrayUtils.isEmpty(overrideCalendarIds)) {
			overrideCalendarIds = ArrayUtils.createArray(schedule.getCalendar().getId());
		}

		return nextOccurrence ? getNextOccurrence(schedule, date, overrideCalendarIds) : getPreviousOccurrence(schedule, date, overrideCalendarIds);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see CalendarScheduleOccurrenceCalculator#getNextOrPreviousOccurrence(CalendarSchedule, Date, boolean, Short...)
	 */
	public abstract CalendarScheduleOccurrenceResult doGetNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds);


	/**
	 * @see CalendarScheduleOccurrenceCalculator#getNextOrPreviousOccurrence(CalendarSchedule, Date, boolean, Short...)
	 */
	public abstract CalendarScheduleOccurrenceResult doGetPreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds);


	public abstract ScheduleFrequencies getFrequency();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected CalendarScheduleOccurrenceResult getNextOccurrence(CalendarSchedule schedule, Date date, Short... overrideCalendarIds) {
		validateRequiredInputs(schedule);
		CalendarScheduleOccurrenceResult nextOccurrence = doGetNextOccurrence(schedule, date, overrideCalendarIds);
		if (nextOccurrence != null && nextOccurrence.getDateOfOccurrence() != null) {
			ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(nextOccurrence.getDateOfOccurrence(), date), "Next occurrence [" + DateUtils.fromDate(nextOccurrence.getDateOfOccurrence(), DateUtils.DATE_FORMAT_SHORT)
					+ "] cannot be before [" + DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SHORT) + "]");
		}
		return nextOccurrence;
	}


	protected CalendarScheduleOccurrenceResult getPreviousOccurrence(CalendarSchedule schedule, Date date, Short... overrideCalendarIds) {
		validateRequiredInputs(schedule);
		CalendarScheduleOccurrenceResult previousOccurrence = doGetPreviousOccurrence(schedule, date, overrideCalendarIds);
		if (previousOccurrence != null && previousOccurrence.getDateOfOccurrence() != null) {
			ValidationUtils.assertTrue(DateUtils.isDateBeforeOrEqual(previousOccurrence.getDateOfOccurrence(), date, true), "Previous occurrence [" + DateUtils.fromDate(previousOccurrence.getDateOfOccurrence(), DateUtils.DATE_FORMAT_SHORT)
					+ "] cannot be after [" + DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SHORT) + "]");
		}
		return previousOccurrence;
	}


	/**
	 * Returns the {@link OccurrenceDateComparisons} indicating whether the given occurrenceFromDate is:
	 * <ul>
	 *     <li>before the schedule start date</li>
	 *     <li>equal to the schedule start date</li>
	 *     <li>between schedule start and end</li>
	 *     <li>equal to schedule end</li>
	 *     <li>after schedule end</li>
	 * </ul>
	 */
	protected OccurrenceDateComparisons compareOccurrenceFromDateToScheduleStartAndEnd(CalendarSchedule schedule, Date occurrenceFromDate) {
		Date startDate = getCalendarScheduleStartDateTime(schedule);
		Date endDate = getCalendarScheduleEndDateTime(schedule);

		if (occurrenceFromDate.before(startDate)) {
			return OccurrenceDateComparisons.BEFORE_SCHEDULE_START;
		}
		else if (DateUtils.compare(occurrenceFromDate, startDate, true) == 0) {
			return OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_START;
		}
		else if (endDate != null) {
			if (occurrenceFromDate.before(endDate)) {
				return OccurrenceDateComparisons.BETWEEN_SCHEDULE_START_AND_END;
			}
			else if (DateUtils.compare(occurrenceFromDate, endDate, true) == 0) {
				return OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_END;
			}
			else if (occurrenceFromDate.after(endDate)) {
				return OccurrenceDateComparisons.AFTER_SCHEDULE_END;
			}
		}

		return OccurrenceDateComparisons.BETWEEN_SCHEDULE_START_AND_END;
	}


	protected enum OccurrenceDateComparisons {
		BEFORE_SCHEDULE_START,
		EQUAL_TO_SCHEDULE_START,
		BETWEEN_SCHEDULE_START_AND_END,
		EQUAL_TO_SCHEDULE_END,
		AFTER_SCHEDULE_END
	}


	protected Date getYesterdayWithScheduleEndOrStartTime(CalendarSchedule schedule, Date date) {
		return DateUtils.setTime(DateUtils.addDays(date, -1), getCalendarScheduleEndDateTimeOrStartDateTime(schedule));
	}


	protected Date getCalendarScheduleEndDateTimeOrStartDateTime(CalendarSchedule schedule) {
		return ObjectUtils.coalesce(getCalendarScheduleEndDateTime(schedule), getCalendarScheduleStartDateTime(schedule));
	}


	protected boolean isDateResultWithScheduleStartTimeBeforeRequestDate(CalendarSchedule schedule, Date dateResult, Date requestDate) {
		return schedule.getStartTime() != null ? DateUtils.isDateBefore(DateUtils.setTime(dateResult, schedule.getStartTime()), requestDate, true)
				: DateUtils.isDateBefore(DateUtils.clearTime(dateResult), requestDate, true);
	}


	protected boolean isDateResultWithScheduleEndTimeAfterRequestDate(CalendarSchedule schedule, Date dateResult, Date requestDate) {
		Time scheduleEndTime = new Time(getCalendarScheduleEndDateTimeOrStartDateTime(schedule));
		return DateUtils.isDateAfter(DateUtils.setTime(dateResult, scheduleEndTime), requestDate);
	}


	/**
	 * Get the start date of the schedule.  If the start date is not a business day and the schedule is limited to business days,
	 * it will move the start date to the next business day.
	 */
	protected Date getCalendarScheduleStartDateTime(CalendarSchedule schedule) {
		return getCalendarScheduleStartDateTime(schedule, adjustForBusinessDays(schedule, schedule.getStartDate()));
	}


	/**
	 * Determine if fetching the next business day should occur for the provided schedule and start date.
	 */
	protected boolean adjustForBusinessDays(CalendarSchedule schedule, Date startDate) {
		if (schedule.getBusinessDayConvention() != null && schedule.getBusinessDayConvention().isLimitedToBusinessDays() && (schedule.isSaturday() || schedule.isSunday())) {
			// if the target day is a weekend day, ignore the next business day logic.
			return !DateUtils.isWeekend(schedule.getStartDate());
		}
		return true;
	}


	protected Date getCalendarScheduleStartDateTime(CalendarSchedule schedule, boolean adjustForBusinessDays) {
		Date result = schedule.getStartDate();
		if (adjustForBusinessDays && getBusinessDayConvention(schedule).isLimitedToBusinessDays() && !getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(result, schedule.getCalendar().getId()))) {
			result = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(result, schedule.getCalendar().getId()));
		}
		if (schedule.getStartTime() != null) {
			result = DateUtils.setTime(result, schedule.getStartTime());
		}
		return result;
	}


	protected Date getCalendarScheduleEndDateTime(CalendarSchedule schedule) {
		if (schedule.getEndDate() == null) {
			return null;
		}
		if (schedule.getEndTime() == null) {
			return schedule.getStartTime() != null ? DateUtils.setTime(schedule.getEndDate(), schedule.getStartTime()) : schedule.getEndDate();
		}
		return DateUtils.setTime(schedule.getEndDate(), schedule.getEndTime());
	}


	protected BusinessDayConventions getBusinessDayConvention(CalendarSchedule schedule) {
		return schedule.getBusinessDayConvention() == null ? BusinessDayConventions.UNADJUSTED : schedule.getBusinessDayConvention();
	}


	protected boolean isDateBeforeAndNotEqualToRequestDate(Date date, Date requestDate, boolean includeTime) {
		return requestDate == null || (!date.after(requestDate) && DateUtils.compare(date, requestDate, includeTime) != 0);
	}


	protected Date applyBusinessDays(Date date, CalendarSchedule schedule, Short... overrideCalendarIds) {
		return getBusinessDayConventionUtilHandler().applyBusinessDayConvention(date, schedule.getBusinessDayConvention(), overrideCalendarIds);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateRequiredInputs(CalendarSchedule schedule) {
		ValidationUtils.assertEquals(getFrequency(), schedule.getFrequency(), "[" + getClass() + "] does not support frequency [" + schedule.getFrequency() + "]");
		ValidationUtils.assertNotNull(getCalendarBusinessDayService(), "[CalendarBusinessDayService] service must be set before calling the calculator.");
		ValidationUtils.assertNotNull(getBusinessDayConventionUtilHandler(), "[BusinessDayConventionUtilHandler] service must be set before calling the calculator.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	@Override
	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public BusinessDayConventionUtilHandler getBusinessDayConventionUtilHandler() {
		return this.businessDayConventionUtilHandler;
	}


	@Override
	public void setBusinessDayConventionUtilHandler(BusinessDayConventionUtilHandler businessDayConventionUtilHandler) {
		this.businessDayConventionUtilHandler = businessDayConventionUtilHandler;
	}
}
