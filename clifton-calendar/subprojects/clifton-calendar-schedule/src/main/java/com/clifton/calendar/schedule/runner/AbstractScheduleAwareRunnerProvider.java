package com.clifton.calendar.schedule.runner;

import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerProvider;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * Provides a central place for logic pertaining to RunnerProviders that work with scheduled objects.
 *
 * @author theodorez
 */
public abstract class AbstractScheduleAwareRunnerProvider<T extends ScheduleAware<?>> implements RunnerProvider<T> {

	private ScheduleApiService scheduleApiService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Runner> getOccurrencesBetweenForEntity(T entity, Date startDateTime, Date endDateTime) {
		long startMillis = System.currentTimeMillis();
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Starting getOccurrencesBetweenForEntity at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE)));
		List<Runner> results = new ArrayList<>();
		if (entity.getSchedule() != null) {
			try {
				// Convert to CalendarSchedule for usage
				Schedule schedule = getScheduleApiService().getSchedule(entity.getSchedule().getId());
				List<Date> occurrences = getScheduleOccurrences(entity, schedule, startDateTime, endDateTime);
				LogUtils.trace(LogCommand.ofMessage(getClass(), "Date occurrence list compiled at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
				for (Date date : occurrences) {
					Runner runner = createRunnerForEntityAndDate(entity, date);
					results.add(runner);
				}
				LogUtils.trace(LogCommand.ofMessage(getClass(), "Finished creating runners for date occurrences at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
			}
			catch (Throwable e) {
				LogUtils.error(getClass(), "Failed to get schedule occurrences of " + entity.getClass() + " for [" + entity.getName() + "] between [" + DateUtils.fromDate(startDateTime) + "] and [" + DateUtils.fromDate(endDateTime) + "].", e);
			}
			finally {
				LogUtils.trace(LogCommand.ofMessage(getClass(), "Completed getOccurrencesBetweenForEntity at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
			}
		}
		return results;
	}


	// handle any exceptions without rolling back the inherited transaction
	@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
	protected List<Date> getScheduleOccurrences(T entity, Schedule schedule, Date startDateTime, Date endDateTime) {
		try {
			return getScheduleApiService().getScheduleOccurrences(ScheduleOccurrenceCommand.forOccurrencesBetween(schedule, startDateTime, endDateTime));
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Failed to get schedule occurrences of " + entity.getClass() + " for [" + entity.getName() + "] between [" + DateUtils.fromDate(startDateTime) + "] and [" + DateUtils.fromDate(endDateTime) + "].", e);
		}
		return Collections.emptyList();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}
}
