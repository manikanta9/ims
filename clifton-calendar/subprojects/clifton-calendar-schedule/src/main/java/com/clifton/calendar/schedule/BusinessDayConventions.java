package com.clifton.calendar.schedule;


public enum BusinessDayConventions {
	/**
	 * The first business day after the given holiday.
	 */
	NEXT(true),
	/**
	 * The first business day after the given holiday unless it's a different month.  Then use previous day before the given holiday.
	 */
	NEXT_MODIFIED(true),
	/**
	 * The first business day before the given holiday.
	 */
	PREVIOUS(true),
	/**
	 * The first business day before the given holiday unless it's a different month.  Then use next day after the given holiday.
	 */
	PREVIOUS_MODIFIED(true),
	/**
	 * Ignore business days.
	 */
	UNADJUSTED(false),
	/**
	 * If the day is not a business day, then no result is returned. For MONTHLY and YEARLY schedules this means the occurrence for that month or that year is skipped.
	 */
	SKIP(true);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private final boolean limitedToBusinessDays;


	BusinessDayConventions(boolean limitedToBusinessDays) {
		this.limitedToBusinessDays = limitedToBusinessDays;
	}


	/**
	 * Return true is the schedule is limited to business days (businessDayConvention != UNADJUSTED), and false if it is not limited to business days (businessDayConvention == UNADJUSTED).
	 */
	public boolean isLimitedToBusinessDays() {
		return this.limitedToBusinessDays;
	}
}
