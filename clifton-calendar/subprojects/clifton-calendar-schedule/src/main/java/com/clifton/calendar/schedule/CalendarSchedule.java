package com.clifton.calendar.schedule;


import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarNamedEntityModifyConditionAware;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>CalendarSchedule</code> class defines a specific schedule
 *
 * @author mwacker
 */
public class CalendarSchedule extends CalendarNamedEntityModifyConditionAware<Integer> {

	private Calendar calendar;

	private CalendarScheduleType calendarScheduleType;

	private BusinessDayConventions businessDayConvention;

	/**
	 * Indicates whether this schedule is a rollup of other schedules
	 */
	private boolean rollupCalendarSchedule;

	/**
	 * List of child calendar schedules that are rolled up into this schedule
	 */
	private List<CalendarSchedule> childCalendarScheduleList;

	/**
	 * Specifies whether this schedule is system defined (created and managed by other system entities: InvestmentCalendar, etc.)
	 */
	public boolean systemDefined;

	/**
	 * startDate and endDate are stored without time components which are cleared by the set property methods.
	 */
	private Date startDate;
	private Date endDate;
	/**
	 * startTime and endTime are saved as integers see {@link Time}.
	 */
	private Time startTime;
	private Time endTime;

	/**
	 * Recurrence is the number of minutes, days, weeks, months or years to skip.
	 * <br/>
	 * Hourly recurrence is in minutes. Occurs every x minutes each hour.<br/>
	 * Daily recurrence is in days.<br/>
	 * Weekly recurrence is in weeks.<br/>
	 * Monthly recurrence is in months.<br/>
	 * Yearly recurrence is in years.<br/>
	 */
	private int recurrence;

	private ScheduleFrequencies frequency;

	private Integer dayOfMonth;

	/**
	 * The number of days to adjust the final output of the schedule.
	 */
	private Integer additionalBusinessDays;

	// minutes properties

	// daily properties

	// weekly properties
	private boolean sunday;
	private boolean monday;
	private boolean tuesday;
	private boolean wednesday;
	private boolean thursday;
	private boolean friday;
	private boolean saturday;

	// monthly properties
	private boolean countFromLastDayOfMonth;

	// yearly properties
	private boolean january;
	private boolean february;
	private boolean march;
	private boolean april;
	private boolean may;
	private boolean june;
	private boolean july;
	private boolean august;
	private boolean september;
	private boolean october;
	private boolean november;
	private boolean december;

	/**
	 * Local property used to store the first occurrence of the schedule.
	 */
	@NonPersistentField
	private Date firstOccurrence;

	/**
	 * A comma delimited list of occurrences for a monthly weekday schedule.  For example if the schedule was the
	 * 1st and 3rd Thursday of the month the string would be "1,3".
	 */
	private String weekdayOccurrenceListString;
	private List<Integer> weekdayOccurrenceList;


	////////////////////////////////////////////////////////////////////////////
	////////                    Entity Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	public List<Integer> getWeekdayOccurrenceList() {
		// parse the list of occurrences
		if (!StringUtils.isEmpty(getWeekdayOccurrenceListString()) && (this.weekdayOccurrenceList == null)) {
			this.weekdayOccurrenceList = new ArrayList<>();
			List<String> occurrenceList = StringUtils.delimitedStringToList(getWeekdayOccurrenceListString(), ",");
			for (String occurrence : CollectionUtils.getIterable(occurrenceList)) {
				this.weekdayOccurrenceList.add(Integer.parseInt(occurrence));
			}
			Collections.sort(AssertUtils.assertNotNull(occurrenceList, "Weekday occurrence list was null."));
		}
		return this.weekdayOccurrenceList;
	}


	public Schedule toSchedule() {
		Schedule result = new Schedule();
		result.setId(getId());
		result.setName(getName());
		result.setDescription(getDescription());
		if (getCalendar() != null) {
			result.setCalendar(getCalendar().toCalendar());
		}
		if (getCalendarScheduleType() != null) {
			result.setScheduleType(getCalendarScheduleType().toScheduleType());
		}
		if (getBusinessDayConvention() != null) {
			result.setBusinessDayConvention(getBusinessDayConvention().name());
		}
		result.setRollupCalendarSchedule(isRollupCalendarSchedule());
		if (getChildCalendarScheduleList() != null) {
			result.setChildScheduleIdList(
					getChildCalendarScheduleList()
							.stream()
							.map(CalendarSchedule::toSchedule)
							.collect(Collectors.toList())
			);
		}
		result.setSystemDefined(isSystemDefined());
		result.setStartDate(getStartDate());
		result.setEndDate(getEndDate());
		result.setStartTime(getStartTime());
		result.setEndTime(getEndTime());
		result.setRecurrence(getRecurrence());
		result.setFrequency(getFrequency().name());
		result.setDayOfMonth(getDayOfMonth());
		result.setAdditionalBusinessDays(getAdditionalBusinessDays());

		// For the rest of the properties, only need to set those that are applicable to the given frequency
		if (getFrequency() == ScheduleFrequencies.WEEKLY || getFrequency() == ScheduleFrequencies.MONTHLY_WEEKDAY) {
			result.setSunday(isSunday());
			result.setMonday(isMonday());
			result.setTuesday(isTuesday());
			result.setWednesday(isWednesday());
			result.setThursday(isThursday());
			result.setFriday(isFriday());
			result.setSaturday(isSaturday());
		}
		else if (getFrequency() == ScheduleFrequencies.YEARLY) {
			result.setJanuary(isJanuary());
			result.setFebruary(isFebruary());
			result.setMarch(isMarch());
			result.setApril(isApril());
			result.setMay(isMay());
			result.setJune(isJune());
			result.setJuly(isJuly());
			result.setAugust(isAugust());
			result.setSeptember(isSeptember());
			result.setOctober(isOctober());
			result.setNovember(isNovember());
			result.setDecember(isDecember());
		}

		result.setCountFromLastDayOfMonth(isCountFromLastDayOfMonth());
		result.setFirstOccurrence(getFirstOccurrence());
		result.setWeekdayOccurrenceListString(getWeekdayOccurrenceListString());
		result.setWeekdayOccurrenceList(getWeekdayOccurrenceList());

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getStartDate() {
		return this.startDate;
	}


	@ValueChangingSetter
	public void setStartDate(Date startDate) {
		this.startDate = DateUtils.clearTime(startDate);
	}


	public Date getEndDate() {
		return this.endDate;
	}


	@ValueChangingSetter
	public void setEndDate(Date endDate) {
		this.endDate = DateUtils.clearTime(endDate);
	}


	public ScheduleFrequencies getFrequency() {
		return this.frequency;
	}


	public void setFrequency(ScheduleFrequencies frequency) {
		this.frequency = frequency;
	}


	public Time getStartTime() {
		return this.startTime;
	}


	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}


	public Time getEndTime() {
		return this.endTime;
	}


	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}


	public boolean isSunday() {
		return this.sunday;
	}


	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}


	public boolean isMonday() {
		return this.monday;
	}


	public void setMonday(boolean monday) {
		this.monday = monday;
	}


	public boolean isTuesday() {
		return this.tuesday;
	}


	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}


	public boolean isWednesday() {
		return this.wednesday;
	}


	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}


	public boolean isThursday() {
		return this.thursday;
	}


	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}


	public boolean isFriday() {
		return this.friday;
	}


	public void setFriday(boolean friday) {
		this.friday = friday;
	}


	public boolean isSaturday() {
		return this.saturday;
	}


	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}


	public boolean isJanuary() {
		return this.january;
	}


	public void setJanuary(boolean january) {
		this.january = january;
	}


	public boolean isFebruary() {
		return this.february;
	}


	public void setFebruary(boolean february) {
		this.february = february;
	}


	public boolean isMarch() {
		return this.march;
	}


	public void setMarch(boolean march) {
		this.march = march;
	}


	public boolean isApril() {
		return this.april;
	}


	public void setApril(boolean april) {
		this.april = april;
	}


	public boolean isMay() {
		return this.may;
	}


	public void setMay(boolean may) {
		this.may = may;
	}


	public boolean isJune() {
		return this.june;
	}


	public void setJune(boolean june) {
		this.june = june;
	}


	public boolean isJuly() {
		return this.july;
	}


	public void setJuly(boolean july) {
		this.july = july;
	}


	public boolean isAugust() {
		return this.august;
	}


	public void setAugust(boolean august) {
		this.august = august;
	}


	public boolean isSeptember() {
		return this.september;
	}


	public void setSeptember(boolean september) {
		this.september = september;
	}


	public boolean isOctober() {
		return this.october;
	}


	public void setOctober(boolean october) {
		this.october = october;
	}


	public boolean isNovember() {
		return this.november;
	}


	public void setNovember(boolean november) {
		this.november = november;
	}


	public boolean isDecember() {
		return this.december;
	}


	public void setDecember(boolean december) {
		this.december = december;
	}


	public Integer getDayOfMonth() {
		return this.dayOfMonth == null ? 0 : this.dayOfMonth;
	}


	public void setDayOfMonth(Integer dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}


	public Calendar getCalendar() {
		return this.calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public CalendarScheduleType getCalendarScheduleType() {
		return this.calendarScheduleType;
	}


	public void setCalendarScheduleType(CalendarScheduleType calendarScheduleType) {
		this.calendarScheduleType = calendarScheduleType;
	}


	public int getRecurrence() {
		return this.recurrence;
	}


	public void setRecurrence(int recurrence) {
		this.recurrence = recurrence;
	}


	public boolean isCountFromLastDayOfMonth() {
		return this.countFromLastDayOfMonth;
	}


	public void setCountFromLastDayOfMonth(boolean countFromLastDayOfMonth) {
		this.countFromLastDayOfMonth = countFromLastDayOfMonth;
	}


	public BusinessDayConventions getBusinessDayConvention() {
		return this.businessDayConvention;
	}


	public void setBusinessDayConvention(BusinessDayConventions businessDayConvention) {
		this.businessDayConvention = businessDayConvention;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Integer getAdditionalBusinessDays() {
		return this.additionalBusinessDays;
	}


	public void setAdditionalBusinessDays(Integer additionalBusinessDays) {
		this.additionalBusinessDays = additionalBusinessDays;
	}


	public String getWeekdayOccurrenceListString() {
		return this.weekdayOccurrenceListString;
	}


	public void setWeekdayOccurrenceListString(String weekdayOccurrenceListString) {
		this.weekdayOccurrenceListString = weekdayOccurrenceListString;
	}


	public Date getFirstOccurrence() {
		return this.firstOccurrence;
	}


	public void setFirstOccurrence(Date firstOccurrence) {
		this.firstOccurrence = firstOccurrence;
	}


	public boolean isRollupCalendarSchedule() {
		return this.rollupCalendarSchedule;
	}


	public void setRollupCalendarSchedule(boolean rollupCalendarSchedule) {
		this.rollupCalendarSchedule = rollupCalendarSchedule;
	}


	public List<CalendarSchedule> getChildCalendarScheduleList() {
		return this.childCalendarScheduleList;
	}


	public void setChildCalendarScheduleList(List<CalendarSchedule> childCalendarScheduleList) {
		this.childCalendarScheduleList = childCalendarScheduleList;
	}
}
