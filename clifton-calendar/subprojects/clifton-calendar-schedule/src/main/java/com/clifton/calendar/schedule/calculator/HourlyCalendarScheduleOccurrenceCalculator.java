package com.clifton.calendar.schedule.calculator;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceResult;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.date.TimeUtils;

import java.util.Date;


public class HourlyCalendarScheduleOccurrenceCalculator extends AbstractCalendarScheduleOccurrenceCalculator {

	@Override
	public ScheduleFrequencies getFrequency() {
		return ScheduleFrequencies.HOURLY;
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		occurrenceFromDate = adjustDateToNextOrPrevBusinessDay(schedule, occurrenceFromDate, true, overrideCalendarIds);

		// check schedule start and end dates
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_START) {
			return new CalendarScheduleOccurrenceResult(getCalendarScheduleStartDateTime(schedule));
		}
		else if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END) {
			return null;
		}
		return calculateOccurrence(schedule, occurrenceFromDate, true, overrideCalendarIds);
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetPreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		occurrenceFromDate = adjustDateToNextOrPrevBusinessDay(schedule, occurrenceFromDate, false, overrideCalendarIds);

		// check schedule start and end dates
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_END) {
			return new CalendarScheduleOccurrenceResult(getCalendarScheduleEndDateTime(schedule));
		}
		else if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START) {
			return null;
		}
		return calculateOccurrence(schedule, occurrenceFromDate, false, overrideCalendarIds);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarScheduleOccurrenceResult calculateOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, boolean nextOccurrence, Short... overrideCalendarIds) {
		Time startTime = schedule.getStartTime() == null ? new Time(0) : schedule.getStartTime();
		Time endTime = schedule.getEndTime() == null ? new Time((24 * 60 - 1) * 60 * 1000) : schedule.getEndTime();
		return nextOccurrence ? calculateNextOccurrence(schedule, occurrenceFromDate, startTime, endTime, overrideCalendarIds) :
				calculatePreviousOccurrence(schedule, occurrenceFromDate, startTime, endTime, overrideCalendarIds);
	}


	private CalendarScheduleOccurrenceResult calculateNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Time startTime, Time endTime, Short... overrideCalendarIds) {
		// date is a business day and is between start and end date
		int startMinute = startTime.getMinutes(occurrenceFromDate);
		int endMinute = endTime.getMinutes(occurrenceFromDate);
		int executionMinute = DateUtils.getMinutesFromMidnight(occurrenceFromDate);

		Date nextOccurrenceDate = null;
		if (executionMinute > startMinute && executionMinute < endMinute) {
			nextOccurrenceDate = adjustMinutesBasedOnScheduleRecurrence(schedule, occurrenceFromDate, startTime, true);
		}
		else if (executionMinute <= startMinute) {
			nextOccurrenceDate = DateUtils.setTime(occurrenceFromDate, startTime);
		}
		else if (executionMinute == endMinute) {
			nextOccurrenceDate = DateUtils.setTime(occurrenceFromDate, endTime);
		}
		else if (getBusinessDayConvention(schedule).isLimitedToBusinessDays()) {
			Date nextBusinessDay = DateUtils.clearTime(getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(occurrenceFromDate, overrideCalendarIds)));
			return getNextOccurrence(schedule, nextBusinessDay, overrideCalendarIds);
		}
		// If schedule is not limited to business days, calendar overrides don't matter, and they don't need to be passed to getNextOccurrence
		if (nextOccurrenceDate == null) {
			return new CalendarScheduleOccurrenceResult(DateUtils.setTime(DateUtils.addDays(occurrenceFromDate, 1), startTime));
		}
		if (DateUtils.isDateBefore(nextOccurrenceDate, occurrenceFromDate, true)) {
			return getNextOccurrence(schedule, DateUtils.addMinutes(nextOccurrenceDate, 1));
		}
		return new CalendarScheduleOccurrenceResult(nextOccurrenceDate);
	}


	private CalendarScheduleOccurrenceResult calculatePreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Time startTime, Time endTime, Short... overrideCalendarIds) {
		// date is a business day and is between start and end date
		int startMinute = startTime.getMinutes(occurrenceFromDate);
		int endMinute = endTime.getMinutes(occurrenceFromDate);
		int executionMinute = DateUtils.getMinutesFromMidnight(occurrenceFromDate);

		Date previousOccurrenceDate = null;
		if (executionMinute > endMinute) {
			previousOccurrenceDate = adjustMinutesBasedOnScheduleRecurrence(schedule, DateUtils.setTime(occurrenceFromDate, endTime), startTime, false);
		}
		else if (executionMinute > startMinute) {
			previousOccurrenceDate = adjustMinutesBasedOnScheduleRecurrence(schedule, occurrenceFromDate, startTime, false);
		}
		else if (executionMinute == startMinute) {
			previousOccurrenceDate = DateUtils.setTime(occurrenceFromDate, startTime);
		}
		else if (getBusinessDayConvention(schedule).isLimitedToBusinessDays()) {
			Date previousBusinessDay = DateUtils.setTime(getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(occurrenceFromDate, overrideCalendarIds)), endTime);
			return getPreviousOccurrence(schedule, previousBusinessDay, overrideCalendarIds);
		}
		// If schedule is not limited to business days, calendar overrides don't matter, and they don't need to be passed to getPreviousOccurrence
		if (previousOccurrenceDate == null) {
			return new CalendarScheduleOccurrenceResult(DateUtils.setTime(DateUtils.addDays(occurrenceFromDate, -1), endTime));
		}
		if (DateUtils.isDateAfter(previousOccurrenceDate, occurrenceFromDate)) {
			return getPreviousOccurrence(schedule, DateUtils.addMinutes(previousOccurrenceDate, -1));
		}
		return new CalendarScheduleOccurrenceResult(previousOccurrenceDate);
	}


	private Date adjustMinutesBasedOnScheduleRecurrence(CalendarSchedule schedule, Date occurrenceFromDate, Time startTime, boolean nextOccurrence) {
		int minutes = DateUtils.getMinutesFromMidnight(occurrenceFromDate) - startTime.getMinutes(occurrenceFromDate);
		int actualOccurrences = minutes / schedule.getRecurrence();

		Date result = DateUtils.setTime(occurrenceFromDate, startTime);
		if (schedule.getRecurrence() > 1 && (nextOccurrence && minutes != schedule.getRecurrence() * actualOccurrences)) {
			actualOccurrences += 1;
		}
		return DateUtils.addMinutes(result, actualOccurrences * schedule.getRecurrence());
	}


	private Date adjustDateToNextOrPrevBusinessDay(CalendarSchedule schedule, Date date, boolean nextOccurrence, Short... overrideCalendarIds) {

		if (getBusinessDayConvention(schedule).isLimitedToBusinessDays() && !getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(date, overrideCalendarIds))) {
			// set lookup time to beginning of next or end of previous business day
			date = nextOccurrence ? DateUtils.clearTime(getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, overrideCalendarIds))) :
					// End of previous business day is technically 12:00 am on the following day
					DateUtils.addMinutes(DateUtils.setTime(getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, overrideCalendarIds)), TimeUtils.toTime("23:59:00")), 1);
			return (date);
		}
		return date;
	}
}
