package com.clifton.calendar.schedule.calculator;


import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceResult;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


public class OnceCalendarScheduleOccurrenceCalculator extends AbstractCalendarScheduleOccurrenceCalculator {

	@Override
	public ScheduleFrequencies getFrequency() {
		return ScheduleFrequencies.ONCE;
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		Date startDateTime = getCalendarScheduleStartDateTime(schedule);
		if (DateUtils.isDateAfterOrEqual(startDateTime, occurrenceFromDate)) {
			return new CalendarScheduleOccurrenceResult(startDateTime);
		}
		return null;
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetPreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate,  Short... overrideCalendarIds) {
		Date startDateTime = getCalendarScheduleStartDateTime(schedule);
		if (DateUtils.isDateBeforeOrEqual(startDateTime, occurrenceFromDate, true)) {
			return new CalendarScheduleOccurrenceResult(startDateTime);
		}
		return null;
	}
}
