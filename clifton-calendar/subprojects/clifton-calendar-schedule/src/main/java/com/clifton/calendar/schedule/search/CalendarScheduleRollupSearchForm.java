package com.clifton.calendar.schedule.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class CalendarScheduleRollupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer parentCalendarScheduleId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer childCalendarScheduleId;


	public Integer getParentCalendarScheduleId() {
		return this.parentCalendarScheduleId;
	}


	public void setParentCalendarScheduleId(Integer parentCalendarScheduleId) {
		this.parentCalendarScheduleId = parentCalendarScheduleId;
	}


	public Integer getChildCalendarScheduleId() {
		return this.childCalendarScheduleId;
	}


	public void setChildCalendarScheduleId(Integer childCalendarScheduleId) {
		this.childCalendarScheduleId = childCalendarScheduleId;
	}
}
