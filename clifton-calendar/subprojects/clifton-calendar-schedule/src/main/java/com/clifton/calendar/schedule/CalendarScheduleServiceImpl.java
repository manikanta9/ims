package com.clifton.calendar.schedule;


import com.clifton.calendar.calculator.CalendarCalculationResult;
import com.clifton.calendar.calculator.CalendarListCalculator;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.search.CalendarScheduleRollupSearchForm;
import com.clifton.calendar.schedule.search.CalendarScheduleSearchForm;
import com.clifton.calendar.schedule.search.CalendarScheduleTypeSearchForm;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;


/**
 * The <code>CalendarScheduleServiceImpl</code> class provides basic implementation of {@link CalendarScheduleService} methods.
 *
 * @author mwacker
 */
@Service
public class CalendarScheduleServiceImpl implements CalendarScheduleService {

	private AdvancedUpdatableDAO<CalendarSchedule, Criteria> calendarScheduleDAO;
	private AdvancedUpdatableDAO<CalendarScheduleRollup, Criteria> calendarScheduleRollupDAO;
	private AdvancedUpdatableDAO<CalendarScheduleType, Criteria> calendarScheduleTypeDAO;

	private CalendarBusinessDayService calendarBusinessDayService;
	private BusinessDayConventionUtilHandler businessDayConventionUtilHandler;

	private DaoNamedEntityCache<CalendarScheduleType> calendarScheduleTypeCache;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////            Schedule Business Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarSchedule getCalendarSchedule(int id) {
		CalendarSchedule calendarSchedule = getCalendarScheduleDAO().findByPrimaryKey(id);
		populateCalendarScheduleFirstOccurrenceIfRequired(calendarSchedule);
		populateChildCalendarScheduleList(calendarSchedule);
		return calendarSchedule;
	}


	@Override
	public List<CalendarSchedule> getCalendarScheduleList(final CalendarScheduleSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getParentCalendarScheduleId() != null) {
					DetachedCriteria subSelect = DetachedCriteria.forClass(CalendarScheduleRollup.class, "r")
							.setProjection(Projections.id())
							.add(Restrictions.eq("referenceOne.id", searchForm.getParentCalendarScheduleId()))
							.add(Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".id"));

					criteria.add(Subqueries.exists(subSelect));
				}

				if (searchForm.getChildCalendarScheduleId() != null) {
					DetachedCriteria subSelect = DetachedCriteria.forClass(CalendarScheduleRollup.class, "r")
							.setProjection(Projections.id())
							.add(Restrictions.eqProperty("referenceOne.id", criteria.getAlias() + ".id"))
							.add(Restrictions.eq("referenceTwo.id", searchForm.getChildCalendarScheduleId()));

					criteria.add(Subqueries.exists(subSelect));
				}
			}
		};
		return getCalendarScheduleDAO().findBySearchCriteria(config);
	}


	@Override
	@Transactional
	public CalendarSchedule saveCalendarSchedule(CalendarSchedule bean) {
		if (!bean.isNewBean() && bean.getChildCalendarScheduleList() != null) {
			ValidationUtils.assertFalse(this.containsRollupCycle(bean.getChildCalendarScheduleList(), bean), "A rollup cannot directly or indirectly contain itself.");
		}

		ValidationUtils.assertNotNull(bean.getCalendarScheduleType(), "Calendar Schedule Type is required for a Calendar");
		if (bean.getDayOfMonth() == 0) {
			bean.setDayOfMonth(1);
		}
		if (!bean.getCalendarScheduleType().isTimeSupported()) {
			// clear time if not supported
			bean.setStartTime(null);
			bean.setEndTime(null);
		}

		List<CalendarSchedule> childCalendarScheduleList = bean.getChildCalendarScheduleList();
		bean = getCalendarScheduleDAO().save(bean);

		if (bean.isRollupCalendarSchedule()) {
			populateCalendarScheduleRollupList(bean, childCalendarScheduleList);
		}
		else {
			ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");
			ValidationUtils.assertBefore(bean.getStartTime(), bean.getEndTime(), "endTime");

			if ((bean.getCalendar() != null) && (bean.getBusinessDayConvention() == null)) {
				throw new FieldValidationException("Business Day Convention is required when a calendar is selected.", "businessDayConvention");
			}
			if (bean.getCalendar() == null && bean.getBusinessDayConvention() != null) {
				throw new FieldValidationException("Business Day Convention cannot be set unless a calendar is selected.", "businessDayConvention");
			}
			if (bean.getCalendar() == null && bean.getAdditionalBusinessDays() != null) {
				throw new FieldValidationException("Additional Business Days cannot be set unless a calendar is selected.", "additionalBusinessDays");
			}

			if (bean.getFrequency() == ScheduleFrequencies.MONTHLY_WEEKDAY) {
				ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getWeekdayOccurrenceListString()),
						"[weekdayOccurrenceListString] is required when using [" + bean.getFrequency() + "] frequency.", "weekdayOccurrenceListString");

				// call the property to verify that string can be parsed
				bean.getWeekdayOccurrenceList();

				List<Integer> weekDays = CalendarScheduleUtils.getWeekDayOccurrences(bean);
				ValidationUtils.assertTrue(weekDays.size() == 1, "Only one week day is supported for [" + bean.getFrequency() + "] frequency.");
			}
		}
		bean.setChildCalendarScheduleList(childCalendarScheduleList);
		return bean;
	}


	@Override
	@Transactional
	public CalendarSchedule copyCalendarSchedule(int id, String name) {
		CalendarSchedule original = getCalendarSchedule(id);
		CalendarSchedule newBean = BeanUtils.cloneBean(original, false, false);
		newBean.setName(name);
		return saveCalendarSchedule(newBean);
	}


	@Override
	@Transactional
	public void deleteCalendarSchedule(int id) {
		CalendarSchedule calendarSchedule = getCalendarSchedule(id);
		if (calendarSchedule.isRollupCalendarSchedule()) {
			// delete many to many relationships to child
			CalendarScheduleRollupSearchForm searchForm = new CalendarScheduleRollupSearchForm();
			searchForm.setParentCalendarScheduleId(id);
			List<CalendarScheduleRollup> childRollupList = getCalendarScheduleRollupList(searchForm);

			if (!CollectionUtils.isEmpty(childRollupList)) {
				deleteCalendarScheduleRollupList(childRollupList);
			}
		}
		getCalendarScheduleDAO().delete(id);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////           Calendar Schedule Type Business Methods             ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CalendarScheduleType getCalendarScheduleType(short id) {
		return getCalendarScheduleTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public CalendarScheduleType getCalendarScheduleTypeByName(String name) {
		return getCalendarScheduleTypeCache().getBeanForKeyValue(getCalendarScheduleTypeDAO(), name);
	}


	@Override
	public List<CalendarScheduleType> getCalendarScheduleTypeList(CalendarScheduleTypeSearchForm searchForm) {
		return getCalendarScheduleTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public CalendarScheduleType saveCalendarScheduleType(CalendarScheduleType bean) {
		if (!bean.isNewBean() && bean.isNameSystemDefined()) {
			String oldName = getCalendarScheduleType(bean.getId()).getName();
			ValidationUtils.assertEquals(oldName, bean.getName(), "Calendar Schedule Type [" + bean + "] has System Defined Name, so the name cannot be changed.");
		}

		return getCalendarScheduleTypeDAO().save(bean);
	}


	@Override
	public void deleteCalendarScheduleType(short id) {
		getCalendarScheduleTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Schedule Occurrences Methods                 ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Date> getCalendarScheduleOccurrences(CalendarScheduleOccurrenceCommand occurrenceCommand) {
		return doGetCalendarScheduleOccurrences(occurrenceCommand);
	}


	@Override
	public Date getCalendarScheduleOccurrence(CalendarScheduleOccurrenceCommand occurrenceCommand) {
		return CollectionUtils.getFirstElement(doGetCalendarScheduleOccurrences(occurrenceCommand));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void populateCalendarScheduleRollupList(CalendarSchedule bean, List<CalendarSchedule> childCalendarScheduleList) {
		// new rollup calendar schedule
		if (bean.isNewBean()) {
			List<CalendarScheduleRollup> newRollupList = populateRollupList(bean, childCalendarScheduleList, true);
			saveCalendarScheduleRollupList(newRollupList);
		}
		else {
			CalendarScheduleSearchForm searchForm = new CalendarScheduleSearchForm();
			searchForm.setParentCalendarScheduleId(bean.getId());
			List<CalendarSchedule> originalChildCalendarScheduleList = getCalendarScheduleList(searchForm);

			// a list of child schedules on the bean, that are not already on the parent
			List<CalendarSchedule> newChildCalendarScheduleList = CoreCollectionUtils.removeAll(childCalendarScheduleList, originalChildCalendarScheduleList);

			if (!CollectionUtils.isEmpty(newChildCalendarScheduleList)) {
				// save new rollups
				List<CalendarScheduleRollup> newRollupList = populateRollupList(bean, newChildCalendarScheduleList, true);
				saveCalendarScheduleRollupList(newRollupList);
			}

			// a list of child Schedule on the parent, that are no longer on the bean
			List<CalendarSchedule> deletedChildCalendarScheduleList = CoreCollectionUtils.removeAll(originalChildCalendarScheduleList, childCalendarScheduleList);

			if (!CollectionUtils.isEmpty(deletedChildCalendarScheduleList)) {
				// delete deleted rollups, process deleted holidays
				List<CalendarScheduleRollup> deletedRollupList = populateRollupList(bean, deletedChildCalendarScheduleList, false);
				deleteCalendarScheduleRollupList(deletedRollupList);
			}
		}
	}


	private CalendarSchedule populateCalendarScheduleFirstOccurrenceIfRequired(CalendarSchedule schedule) {
		if (schedule != null && schedule.getFrequency() != null && schedule.getFrequency().isFirstOccurrenceRequired()) {
			if (schedule.getFirstOccurrence() == null) {
				CalendarScheduleOccurrenceResult firstOccurrence = getNextOrPreviousOccurrence(schedule, schedule.getStartDate(), true);
				if (firstOccurrence != null) {
					schedule.setFirstOccurrence(firstOccurrence.getDateOfOccurrence());
				}
			}
		}
		return schedule;
	}


	/**
	 * Rollup Schedule cycle detection algorithm
	 * <p>
	 * Traverse the graph in a depth first search manner seeing if the parent has a route to itself anywhere.
	 *
	 * @return - cycle or not
	 */
	private boolean containsRollupCycle(List<CalendarSchedule> children, CalendarSchedule parent) {
		for (CalendarSchedule child : CollectionUtils.getIterable(children)) {
			if (parent.equals(child)) {
				//cycle detected
				return true;
			}

			populateChildCalendarScheduleList(child);
			if (child.getChildCalendarScheduleList() != null) {
				if (containsRollupCycle(child.getChildCalendarScheduleList(), parent)) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Helper method that will populate a list of CalendarScheduleRollups for a given parent and
	 * list of child schedules.  If createNewRollups is true, new rollup beans will be created,
	 * else existing rollups will be retrieved from the DB.
	 */
	private List<CalendarScheduleRollup> populateRollupList(CalendarSchedule parent, List<CalendarSchedule> childCalendarScheduleList, boolean createNewRollups) {
		List<CalendarScheduleRollup> result = null;
		if (parent != null && !CollectionUtils.isEmpty(childCalendarScheduleList)) {
			result = new ArrayList<>();

			if (createNewRollups) {
				for (CalendarSchedule child : CollectionUtils.getIterable(childCalendarScheduleList)) {
					CalendarScheduleRollup rollup = new CalendarScheduleRollup();
					rollup.setReferenceOne(parent);
					rollup.setReferenceTwo(child);
					result.add(rollup);
				}
			}
			else {
				// get existing rollups
				for (CalendarSchedule child : CollectionUtils.getIterable(childCalendarScheduleList)) {
					CalendarScheduleRollup rollup = getCalendarScheduleRollup(parent.getId(), child.getId());
					result.add(rollup);
				}
			}
		}
		return result;
	}


	private CalendarSchedule populateChildCalendarScheduleList(CalendarSchedule parentCalendarSchedule) {
		if (parentCalendarSchedule != null && parentCalendarSchedule.isRollupCalendarSchedule()
				&& CollectionUtils.isEmpty(parentCalendarSchedule.getChildCalendarScheduleList())) {

			CalendarScheduleSearchForm searchForm = new CalendarScheduleSearchForm();
			searchForm.setParentCalendarScheduleId(parentCalendarSchedule.getId());

			List<CalendarSchedule> childCalendarScheduleList = getCalendarScheduleList(searchForm);
			parentCalendarSchedule.setChildCalendarScheduleList(childCalendarScheduleList);
		}
		return parentCalendarSchedule;
	}


	private List<CalendarScheduleRollup> getCalendarScheduleRollupList(CalendarScheduleRollupSearchForm searchForm) {
		return getCalendarScheduleRollupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private CalendarScheduleRollup getCalendarScheduleRollup(int parentId, int childId) {
		return getCalendarScheduleRollupDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{parentId, childId});
	}


	private void saveCalendarScheduleRollupList(List<CalendarScheduleRollup> beanList) {

		// Validate correct types
		for (CalendarScheduleRollup rollup : CollectionUtils.getIterable(beanList)) {
			ValidationUtils.assertEquals(rollup.getReferenceOne().getCalendarScheduleType(), rollup.getReferenceTwo().getCalendarScheduleType(), "Parent and child schedule for rollup must of the same Schedule Type. "
					+ "Parent: [" + rollup.getReferenceOne() + "] of type [" + rollup.getReferenceOne().getCalendarScheduleType() + "], "
					+ "Child: [" + rollup.getReferenceTwo() + "] of type [" + rollup.getReferenceTwo().getCalendarScheduleType() + "].");
		}

		getCalendarScheduleRollupDAO().saveList(beanList);
	}


	private void deleteCalendarScheduleRollupList(List<CalendarScheduleRollup> beanList) {
		getCalendarScheduleRollupDAO().deleteList(beanList);
	}


	@SuppressWarnings("ImplicitSubclassInspection")
	@Transactional(readOnly = true)
	private List<Date> doGetCalendarScheduleOccurrences(CalendarScheduleOccurrenceCommand occurrenceCommand) {
		occurrenceCommand = populateCalendarScheduleOccurrenceCommand(occurrenceCommand);

		// If both start/end date are defined, assuming we want to get the occurrences between
		// doGetCalendarScheduleOccurrencesBetween will handle dealing with case where both endDate and count are provided
		if (occurrenceCommand.getEndDateTime() != null) {
			// Keeping this method to retain @Transactional functionality
			return doGetCalendarScheduleOccurrencesBetween(occurrenceCommand);
		}

		if (occurrenceCommand.getCalendarSchedule().isRollupCalendarSchedule()) {
			return getCalendarScheduleRollupOccurrences(occurrenceCommand);
		}

		AssertUtils.assertNotNull(occurrenceCommand.getStartDateTime(), "Start date is required in order to get occurrences.");
		List<Date> result = new ArrayList<>();

		Date scheduleDate = occurrenceCommand.getStartDateTime();

		Short[] overrideCalendarIds = getCalendarOverrideList(occurrenceCommand, scheduleDate);

		for (int i = 0; i < Math.abs(occurrenceCommand.getCount()); i++) {

			CalendarScheduleOccurrenceResult nextOrPreviousOccurrence = getNextOrPreviousOccurrence(occurrenceCommand.getCalendarSchedule(), scheduleDate, occurrenceCommand.isNextOccurrenceCommand(), overrideCalendarIds);

			if (nextOrPreviousOccurrence == null) {
				break;
			}

			// only add the date if it is within the valid range (depending on next or previous occurrence)
			if (isOccurrenceWithinRange(nextOrPreviousOccurrence, scheduleDate, occurrenceCommand.isNextOccurrenceCommand())) {
				result.add(nextOrPreviousOccurrence.getDateOfOccurrence());
			}

			int minutesToAdd = occurrenceCommand.isNextOccurrenceCommand() ? 1 : -1;
			scheduleDate = nextOrPreviousOccurrence.getScheduleOccurrenceFromDate() != null ? nextOrPreviousOccurrence.getScheduleOccurrenceFromDate()
					: DateUtils.addMinutes(nextOrPreviousOccurrence.getDateOfOccurrence(), minutesToAdd);
		}
		return result;
	}


	private Short[] getCalendarOverrideList(CalendarScheduleOccurrenceCommand occurrenceCommand, Date scheduleDate) {
		List<Short> combinedCalendarIds = new ArrayList<>();
		if (!MathUtils.isNullOrZero(occurrenceCommand.getOverrideCalendarListBeanId())) {
			List<Short> calendarIdsFromBean = new ArrayList<>(getCalendarOverrideListFromBeanId(occurrenceCommand.getOverrideCalendarListBeanId(), scheduleDate));
			AssertUtils.assertNotEmpty(calendarIdsFromBean, "No calendars are linked to the Override Calendar list with ID: " + occurrenceCommand.getOverrideCalendarListBeanId());

			combinedCalendarIds.addAll(calendarIdsFromBean);
		}
		if (!CollectionUtils.isEmpty(occurrenceCommand.getOverrideCalendarIds())) {
			combinedCalendarIds.addAll(occurrenceCommand.getOverrideCalendarIds());
		}

		return CollectionUtils.toArray(combinedCalendarIds, Short.class);
	}


	private Collection<Short> getCalendarOverrideListFromBeanId(Integer overrideCalendarListBeanId, Date balanceDate) {
		CalendarListCalculator calendarListCalculator = getCalendarListBean(overrideCalendarListBeanId);
		if (calendarListCalculator == null) {
			return null;
		}
		CalendarCalculationResult result = calendarListCalculator.calculate(balanceDate);
		return CollectionUtils.getConverted(result.getCalendarList(), BaseSimpleEntity::getId);
	}


	private CalendarListCalculator getCalendarListBean(Integer overrideCalendarListBeanId) {
		if (overrideCalendarListBeanId == null) {
			return null;
		}
		SystemBean systemBean = getSystemBeanService().getSystemBean(overrideCalendarListBeanId);
		ValidationUtils.assertNotNull(systemBean, "A valid Calender List Override ID is required. Attempted with ID: " + overrideCalendarListBeanId);
		return (CalendarListCalculator) getSystemBeanService().getBeanInstance(systemBean);
	}


	private boolean isOccurrenceWithinRange(CalendarScheduleOccurrenceResult occurrenceResult, Date scheduleDate, boolean nextOccurrence) {
		if (nextOccurrence) {
			return DateUtils.isDateAfterOrEqual(occurrenceResult.getDateOfOccurrence(), scheduleDate);
		}
		else {
			return DateUtils.isDateBeforeOrEqual(occurrenceResult.getDateOfOccurrence(), scheduleDate, true);
		}
	}


	private List<Date> getCalendarScheduleRollupOccurrences(CalendarScheduleOccurrenceCommand occurrenceCommand) {
		SortedSet<Date> dates = new TreeSet<>(); // unique dates only
		boolean nonRemaining = false;

		while (dates.size() < Math.abs(occurrenceCommand.getCount()) && !nonRemaining) {
			nonRemaining = true;
			Map<CalendarSchedule, Date> childrenLastDate = new HashMap<>();
			/*
			 * For each child schedule, within this rollup, grab out the next count amount of occurrences and add
			 * them to a single list. We need to keep doing this until we have the count we desire
			 *
			 * We cannot necessarily do it just once since there may be duplicates automatically removed via the
			 * tree set
			 */
			for (CalendarSchedule childSchedule : occurrenceCommand.getCalendarSchedule().getChildCalendarScheduleList()) {
				Date startDate = childrenLastDate.get(childSchedule) == null ? occurrenceCommand.getStartDateTime() : childrenLastDate.get(childSchedule);
				SortedSet<Date> addTo = new TreeSet<>(doGetCalendarScheduleOccurrences(CalendarScheduleOccurrenceCommand.of(childSchedule)
						.withOccurrencesFromStartDate(startDate, occurrenceCommand.getCount()).build()));
				dates.addAll(addTo);

				if (!addTo.isEmpty()) {
					nonRemaining = false;
					childrenLastDate.put(childSchedule, addTo.last());
				}
			}
		}

		List<Date> dateList = new ArrayList<>(dates);
		if (!dateList.isEmpty()) {
			dateList = dateList.subList(0, Math.min(dateList.size(), Math.abs(occurrenceCommand.getCount())));
		}
		return dateList;
	}


	@SuppressWarnings("ImplicitSubclassInspection")
	@Transactional(readOnly = true)
	private List<Date> doGetCalendarScheduleOccurrencesBetween(CalendarScheduleOccurrenceCommand occurrenceCommand) {
		return new ArrayList<>(computeCalendarScheduleOccurrencesBetween(occurrenceCommand));
	}


	private CalendarScheduleOccurrenceCommand populateCalendarScheduleOccurrenceCommand(CalendarScheduleOccurrenceCommand occurrenceCommand) {
		validateCalendarScheduleOccurrenceCommand(occurrenceCommand);
		if (occurrenceCommand.getCalendarSchedule() == null) {
			AssertUtils.assertNotNull(occurrenceCommand.getScheduleId(), "Schedule or Schedule ID is required in order to get occurrence(s).");
			CalendarSchedule schedule = getCalendarSchedule(occurrenceCommand.getScheduleId());
			AssertUtils.assertNotNull(schedule, "Unable to find Calendar Schedule with id [%d].", occurrenceCommand.getScheduleId());
			occurrenceCommand = CalendarScheduleOccurrenceCommand.forCommandWithOverriddenSchedule(occurrenceCommand, schedule);
		}
		// There are cases where an existing bean may not have these fields populated - we'll update this condition to populate if the fields aren't defined, regardless of new/existing bean
		if (CollectionUtils.isEmpty(occurrenceCommand.getCalendarSchedule().getChildCalendarScheduleList())) {
			occurrenceCommand = CalendarScheduleOccurrenceCommand.forCommandWithOverriddenSchedule(occurrenceCommand, populateChildCalendarScheduleList(occurrenceCommand.getCalendarSchedule()));
		}
		if (occurrenceCommand.getCalendarSchedule().getFirstOccurrence() == null) {
			occurrenceCommand = CalendarScheduleOccurrenceCommand.forCommandWithOverriddenSchedule(occurrenceCommand, populateCalendarScheduleFirstOccurrenceIfRequired(occurrenceCommand.getCalendarSchedule()));
		}

		return occurrenceCommand;
	}


	private void validateCalendarScheduleOccurrenceCommand(CalendarScheduleOccurrenceCommand occurrenceCommand) {
		if (occurrenceCommand.getCount() != null) {
			ValidationUtils.assertTrue(Math.abs(occurrenceCommand.getCount()) <= 50,
					String.format("[%d] occurrences were requested for Calendar Schedule [%s], which exceeds the maximum allowed [50].", occurrenceCommand.getCount(), ObjectUtils.coalesce(occurrenceCommand.getCalendarSchedule(), occurrenceCommand.getScheduleId())));
		}
		if (occurrenceCommand.getEndDateTime() != null) {
			ValidationUtils.assertTrue(DateUtils.isDateBeforeOrEqual(occurrenceCommand.getStartDateTime(), occurrenceCommand.getEndDateTime(), false),
					String.format("Start Date for occurrences [%s] cannot be after End Date for occurrences [%s].", DateUtils.fromDate(occurrenceCommand.getStartDateTime()), DateUtils.fromDate(occurrenceCommand.getEndDateTime())));
		}
	}


	private Set<Date> computeCalendarScheduleOccurrencesBetween(CalendarScheduleOccurrenceCommand command) {
		CalendarSchedule schedule = command.getCalendarSchedule();
		Date startDateTime = command.getStartDateTime();
		Date endDateTime = command.getEndDateTime();
		AssertUtils.assertNotNull(startDateTime, "Start date is required in order to get occurrences.");
		AssertUtils.assertNotNull(endDateTime, "End date is required in order to get occurrences.");
		Integer count = command.getCount();

		SortedSet<Date> dates = new TreeSet<>();
		if (schedule.isRollupCalendarSchedule()) {
			for (CalendarSchedule childSchedule : schedule.getChildCalendarScheduleList()) {
				CalendarScheduleOccurrenceCommand.Builder commandBuilder = CalendarScheduleOccurrenceCommand.of(childSchedule).withOccurrencesBetween(startDateTime, endDateTime);
				if (count != null) {
					commandBuilder.withOccurrencesFromStartDate(startDateTime, count);
				}
				dates.addAll(this.computeCalendarScheduleOccurrencesBetween(commandBuilder.build()));
			}
			if (count == null || dates.size() <= count) {
				return dates;
			}
			// At this point, we will have a list of all occurrences for all child schedules between dates.  Because a count is defined if we make it here, we will need to select the first n items where n = count
			return new TreeSet<>((new ArrayList<>(dates)).subList(0, count));
		}

		// Grab next occurrence until we've reached our desired end date
		List<Date> addTo;
		do {
			addTo = doGetCalendarScheduleOccurrences(CalendarScheduleOccurrenceCommand.of(schedule)
					.withNextOccurrenceFromStartDate(!dates.isEmpty() ? DateUtils.addMinutes(dates.last(), 1) : startDateTime)
					.withOverriddenCalendars(command.getOverrideCalendarIds())
					.build());

			if (addTo.isEmpty() || addTo.get(0).after(endDateTime)) {
				break;
			}

			dates.add(addTo.get(0));

			if (count != null && dates.size() == count) {
				break;
			}
		}
		while (!addTo.isEmpty());

		return dates;
	}


	private CalendarScheduleOccurrenceResult getNextOrPreviousOccurrence(CalendarSchedule schedule, Date date, boolean nextOccurrence, Short... overrideCalendarIds) {
		if (schedule.getFrequency().getOccurrenceCalculator() != null) {
			populateScheduleOccurrenceCalculator(schedule);
			return schedule.getFrequency().getOccurrenceCalculator().getNextOrPreviousOccurrence(schedule, date, nextOccurrence, overrideCalendarIds);
		}
		return null;
	}


	private void populateScheduleOccurrenceCalculator(CalendarSchedule schedule) {
		schedule.getFrequency().getOccurrenceCalculator().setCalendarBusinessDayService(getCalendarBusinessDayService());
		schedule.getFrequency().getOccurrenceCalculator().setBusinessDayConventionUtilHandler(getBusinessDayConventionUtilHandler());
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<CalendarSchedule, Criteria> getCalendarScheduleDAO() {
		return this.calendarScheduleDAO;
	}


	public void setCalendarScheduleDAO(AdvancedUpdatableDAO<CalendarSchedule, Criteria> scheduleDAO) {
		this.calendarScheduleDAO = scheduleDAO;
	}


	public AdvancedUpdatableDAO<CalendarScheduleType, Criteria> getCalendarScheduleTypeDAO() {
		return this.calendarScheduleTypeDAO;
	}


	public void setCalendarScheduleTypeDAO(AdvancedUpdatableDAO<CalendarScheduleType, Criteria> calendarScheduleTypeDAO) {
		this.calendarScheduleTypeDAO = calendarScheduleTypeDAO;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public BusinessDayConventionUtilHandler getBusinessDayConventionUtilHandler() {
		return this.businessDayConventionUtilHandler;
	}


	public void setBusinessDayConventionUtilHandler(BusinessDayConventionUtilHandler businessDayConventionUtilHandler) {
		this.businessDayConventionUtilHandler = businessDayConventionUtilHandler;
	}


	public AdvancedUpdatableDAO<CalendarScheduleRollup, Criteria> getCalendarScheduleRollupDAO() {
		return this.calendarScheduleRollupDAO;
	}


	public void setCalendarScheduleRollupDAO(AdvancedUpdatableDAO<CalendarScheduleRollup, Criteria> calendarScheduleRollupDAO) {
		this.calendarScheduleRollupDAO = calendarScheduleRollupDAO;
	}


	public DaoNamedEntityCache<CalendarScheduleType> getCalendarScheduleTypeCache() {
		return this.calendarScheduleTypeCache;
	}


	public void setCalendarScheduleTypeCache(DaoNamedEntityCache<CalendarScheduleType> calendarScheduleTypeCache) {
		this.calendarScheduleTypeCache = calendarScheduleTypeCache;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
