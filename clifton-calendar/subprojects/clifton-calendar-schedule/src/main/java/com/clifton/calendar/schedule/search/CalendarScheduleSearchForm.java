package com.clifton.calendar.schedule.search;


import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


public class CalendarScheduleSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "calendarScheduleType.id")
	private Short calendarScheduleTypeId;

	@SearchField(searchField = "calendarScheduleType.name")
	private String calendarScheduleTypeName;

	@SearchField
	private Boolean rollupCalendarSchedule;

	// custom restriction using exists clause
	private Integer parentCalendarScheduleId;

	// custom restriction using exists clause
	private Integer childCalendarScheduleId;

	@SearchField(searchField = "calendar.id")
	private Short calendarId;

	@SearchField(searchField = "name", searchFieldPath = "calendar")
	private String calendarName;

	@SearchField
	private BusinessDayConventions businessDayConvention;

	@SearchField
	private ScheduleFrequencies frequency;

	@SearchField(searchField = "name", searchFieldPath = "entityModifyCondition")
	private String entityModifyCondition;

	@SearchField
	private Boolean systemDefined;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Short getCalendarScheduleTypeId() {
		return this.calendarScheduleTypeId;
	}


	public void setCalendarScheduleTypeId(Short calendarScheduleTypeId) {
		this.calendarScheduleTypeId = calendarScheduleTypeId;
	}


	public String getCalendarScheduleTypeName() {
		return this.calendarScheduleTypeName;
	}


	public void setCalendarScheduleTypeName(String calendarScheduleTypeName) {
		this.calendarScheduleTypeName = calendarScheduleTypeName;
	}


	public Boolean getRollupCalendarSchedule() {
		return this.rollupCalendarSchedule;
	}


	public void setRollupCalendarSchedule(Boolean rollupCalendarSchedule) {
		this.rollupCalendarSchedule = rollupCalendarSchedule;
	}


	public Integer getParentCalendarScheduleId() {
		return this.parentCalendarScheduleId;
	}


	public void setParentCalendarScheduleId(Integer parentCalendarScheduleId) {
		this.parentCalendarScheduleId = parentCalendarScheduleId;
	}


	public Integer getChildCalendarScheduleId() {
		return this.childCalendarScheduleId;
	}


	public void setChildCalendarScheduleId(Integer childCalendarScheduleId) {
		this.childCalendarScheduleId = childCalendarScheduleId;
	}


	public Short getCalendarId() {
		return this.calendarId;
	}


	public void setCalendarId(Short calendarId) {
		this.calendarId = calendarId;
	}


	public String getCalendarName() {
		return this.calendarName;
	}


	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}


	public BusinessDayConventions getBusinessDayConvention() {
		return this.businessDayConvention;
	}


	public void setBusinessDayConvention(BusinessDayConventions businessDayConvention) {
		this.businessDayConvention = businessDayConvention;
	}


	public ScheduleFrequencies getFrequency() {
		return this.frequency;
	}


	public void setFrequency(ScheduleFrequencies frequency) {
		this.frequency = frequency;
	}


	public String getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(String entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
}
