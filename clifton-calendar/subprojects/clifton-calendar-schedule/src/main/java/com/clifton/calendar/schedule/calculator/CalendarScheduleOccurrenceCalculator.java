package com.clifton.calendar.schedule.calculator;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.BusinessDayConventionUtilHandler;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceResult;

import java.util.Date;


/**
 * The <code>CalendarScheduleOccurrenceCalculator</code> defines an occurrence calculator needed to get
 * the previous or next date for a schedule.
 *
 * @author mwacker
 */
public interface CalendarScheduleOccurrenceCalculator {

	/**
	 * When <code>nextOccurrence</code> is <code>true</code>, get the next scheduled date/time following the provided date for the provided {@link CalendarSchedule}. If the date
	 * is before the service start date, the first scheduled date for the service will be returned.  If the date is past the end time for the service, a null value will be returned.
	 * <p>
	 * When <code>nextOccurrence</code> is false, get the previous scheduled date/time preceding the provided date for the provided {@link CalendarSchedule}. If the date is after
	 * the service start date, the last scheduled date for the service will be returned.  If the date is before the start time for the service, a null value will be returned.
	 */
	public CalendarScheduleOccurrenceResult getNextOrPreviousOccurrence(CalendarSchedule schedule, Date date, boolean nextOccurrence, Short... overrideCalendarIds);


	/**
	 * Set the CalendarBusinessDayService needed by most calculators.
	 */
	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService);


	/**
	 * Set the CalendarBusinessDayService needed by most calculators.
	 */
	public void setBusinessDayConventionUtilHandler(BusinessDayConventionUtilHandler businessDayConventionUtilHandler);
}
