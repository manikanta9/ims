package com.clifton.calendar.schedule;

import com.clifton.calendar.schedule.api.ScheduleType;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.schema.SystemTable;


/**
 * @author MitchellF
 */
@CacheByName
public class CalendarScheduleType extends NamedEntity<Short> {

	private boolean timeSupported;

	private boolean nameSystemDefined;

	private SystemTable systemTable;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ScheduleType toScheduleType() {
		ScheduleType type = new ScheduleType();
		type.setId(getId());
		type.setName(getName());
		type.setTimeSupported(isTimeSupported());
		type.setNameSystemDefined(isNameSystemDefined());
		if (getSystemTable() != null) {
			type.setSystemTableName(getSystemTable().getName());
		}
		return type;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isTimeSupported() {
		return this.timeSupported;
	}


	public void setTimeSupported(boolean timeSupported) {
		this.timeSupported = timeSupported;
	}


	public boolean isNameSystemDefined() {
		return this.nameSystemDefined;
	}


	public void setNameSystemDefined(boolean nameSystemDefined) {
		this.nameSystemDefined = nameSystemDefined;
	}


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}
}
