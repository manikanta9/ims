package com.clifton.calendar.schedule;


import com.clifton.calendar.schedule.calculator.CalendarScheduleOccurrenceCalculator;
import com.clifton.calendar.schedule.calculator.DailyCalendarScheduleOccurrenceCalculator;
import com.clifton.calendar.schedule.calculator.HourlyCalendarScheduleOccurrenceCalculator;
import com.clifton.calendar.schedule.calculator.MonthlyCalendarScheduleOccurrenceCalculator;
import com.clifton.calendar.schedule.calculator.MonthlyWeekdayCalendarScheduleOccurrenceCalculator;
import com.clifton.calendar.schedule.calculator.OnceCalendarScheduleOccurrenceCalculator;
import com.clifton.calendar.schedule.calculator.WeeklyCalendarScheduleOccurrenceCalculator;
import com.clifton.calendar.schedule.calculator.YearlyCalendarScheduleOccurrenceCalculator;


public enum ScheduleFrequencies {
	ONCE(false, false, new OnceCalendarScheduleOccurrenceCalculator()),
	HOURLY(false, false, new HourlyCalendarScheduleOccurrenceCalculator()),
	DAILY(false, false, new DailyCalendarScheduleOccurrenceCalculator()),
	WEEKLY(false, false, new WeeklyCalendarScheduleOccurrenceCalculator()),
	MONTHLY(true, true, new MonthlyCalendarScheduleOccurrenceCalculator()),
	MONTHLY_WEEKDAY(false, true, new MonthlyWeekdayCalendarScheduleOccurrenceCalculator()),
	YEARLY(false, false, new YearlyCalendarScheduleOccurrenceCalculator());

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private final boolean additionalBusinessDaysSupported;
	private final boolean firstOccurrenceRequired;
	private final CalendarScheduleOccurrenceCalculator occurrenceCalculator;


	ScheduleFrequencies(boolean additionalBusinessDaysSupported, boolean firstOccurrenceRequired, CalendarScheduleOccurrenceCalculator occurrenceCalculator) {
		this.additionalBusinessDaysSupported = additionalBusinessDaysSupported;
		this.firstOccurrenceRequired = firstOccurrenceRequired;
		this.occurrenceCalculator = occurrenceCalculator;
	}


	/**
	 * Return true if the schedule is limited to business days (businessDayConvention != UNADJUSTED), and false if it is not limited to business days (businessDayConvention == UNADJUSTED).
	 */
	public boolean isAdditionalBusinessDaysSupported() {
		return this.additionalBusinessDaysSupported;
	}


	/**
	 * Return true if the CalendarSchedule object needs the first occurrence populated to schedule correctly.
	 */
	public boolean isFirstOccurrenceRequired() {
		return this.firstOccurrenceRequired;
	}


	/**
	 * Returns the next occurrence calculator.
	 */
	public CalendarScheduleOccurrenceCalculator getOccurrenceCalculator() {
		return this.occurrenceCalculator;
	}
}
