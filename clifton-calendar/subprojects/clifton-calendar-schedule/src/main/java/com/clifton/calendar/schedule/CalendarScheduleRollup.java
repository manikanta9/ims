package com.clifton.calendar.schedule;


import com.clifton.core.beans.ManyToManyEntity;


/**
 * The <code>CalendarScheduleRollup</code> class associates two Calendar Schedules with one another
 * as parent and child calendar schedules.  Parent calendar schedules are considered "rollups" of child calendar schedules.
 * Child calendar schedules can be rolled up into different parent calendar schedules.
 *
 * @author apopp
 */
public class CalendarScheduleRollup extends ManyToManyEntity<CalendarSchedule, CalendarSchedule, Integer> {
	// empty
}
