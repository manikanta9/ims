package com.clifton.calendar.schedule;

import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The <code>CalendarScheduleOccurrenceCommand</code> provides a convenient way to configure criteria for retrieving {@link CalendarSchedule} occurrences. It also
 * reduces the number of methods required in the {@link CalendarScheduleService}.
 *
 * @author michaelm
 */
public class CalendarScheduleOccurrenceCommand implements Serializable {

	private final Integer scheduleId;
	private final CalendarSchedule calendarSchedule;

	/**
	 * The number of occurrences to retrieve. Defaults to <code>1</code> when {@link #endDateTime} is not specified in the constructor.
	 */
	private final Integer count;

	/**
	 * Start date used to search for occurrences. Defaults to <code>now</code> when {@link #endDateTime} is not specified in the constructor.
	 */
	private final Date startDateTime;
	/**
	 * End date that should only be specified when searching for occurrences within a date range.
	 */
	private final Date endDateTime;

	private final List<Short> overrideCalendarIds;

	private final Integer overrideCalendarListBeanId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a <code>CalendarScheduleOccurrenceCommand.Builder</code> for the provided {@link #scheduleId}.
	 */
	public static CalendarScheduleOccurrenceCommand.Builder of(int scheduleId) {
		return new Builder(scheduleId);
	}


	/**
	 * Creates a <code>CalendarScheduleOccurrenceCommand.Builder</code> for the provided {@link #calendarSchedule}.
	 */
	public static CalendarScheduleOccurrenceCommand.Builder of(CalendarSchedule calendarSchedule) {
		return new Builder(calendarSchedule);
	}


	public static CalendarScheduleOccurrenceCommand forCommandWithOverriddenSchedule(CalendarScheduleOccurrenceCommand occurrenceCommand, CalendarSchedule schedule) {
		return new CalendarScheduleOccurrenceCommand(schedule.getId(), schedule, occurrenceCommand.getCount(), occurrenceCommand.getStartDateTime(), occurrenceCommand.getEndDateTime(), occurrenceCommand.overrideCalendarListBeanId, occurrenceCommand.getOverrideCalendarIds());
	}


	public CalendarScheduleOccurrenceCommand(Integer scheduleId, CalendarSchedule calendarSchedule, Integer count, Date startDateTime, Date endDateTime, Integer overrideCalendarListBeanId, List<Short> overrideCalendarIds) {
		this.scheduleId = scheduleId;
		this.calendarSchedule = calendarSchedule;
		this.count = count;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		this.overrideCalendarListBeanId = overrideCalendarListBeanId;
		this.overrideCalendarIds = overrideCalendarIds;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @return true if this command is configured to get future occurrences or occurrences between two dates, false if it is configured to get past occurrences
	 */
	public boolean isNextOccurrenceCommand() {
		return getCount() == null || getCount() >= 0;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getScheduleId() {
		return ObjectUtils.coalesce(this.scheduleId, getCalendarSchedule() != null ? getCalendarSchedule().getId() : null);
	}


	public Integer getCount() {
		return this.count;
	}


	public CalendarSchedule getCalendarSchedule() {
		return this.calendarSchedule;
	}


	public Date getStartDateTime() {
		return this.startDateTime;
	}


	public Date getEndDateTime() {
		return this.endDateTime;
	}


	public List<Short> getOverrideCalendarIds() {
		return this.overrideCalendarIds;
	}


	public Integer getOverrideCalendarListBeanId() {
		return this.overrideCalendarListBeanId;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static final class Builder {

		private Integer scheduleId;
		private CalendarSchedule calendarSchedule;

		/**
		 * The number of occurrences to retrieve. Defaults to <code>1</code> when {@link #endDateTime} is not specified in the constructor.
		 */
		private Integer count;

		/**
		 * Start date used to search for occurrences. Defaults to <code>now</code> when {@link #endDateTime} is not specified in the constructor.
		 */
		private Date startDateTime;

		/**
		 * End date that should only be specified when searching for occurrences within a date range.
		 */
		private Date endDateTime;


		private List<Short> overrideCalendarIds;


		private Integer overrideCalendarListBeanId;


		private Builder(int scheduleId) {
			this.scheduleId = scheduleId;
		}


		private Builder(CalendarSchedule calendarSchedule) {
			this.calendarSchedule = calendarSchedule;
		}


		public Builder withOccurrencesBetween(Date startDateTime, Date endDateTime) {
			this.startDateTime = startDateTime;
			this.endDateTime = endDateTime;
			return this;
		}


		public Builder withOccurrencesBetween(String startDateTimeString, String endDateTimeString) {
			this.startDateTime = DateUtils.toDate(startDateTimeString, DateUtils.DATE_FORMAT_SHORT);
			this.endDateTime = DateUtils.toDate(endDateTimeString, DateUtils.DATE_FORMAT_SHORT);
			return this;
		}


		public Builder withNextOccurrenceFromStartDate(String startDateTimeString) {
			return withNextOccurrenceFromStartDate(DateUtils.toDate(startDateTimeString, DateUtils.DATE_FORMAT_SHORT));
		}


		public Builder withPreviousOccurrenceFromStartDate(String startDateTimeString) {
			return withPreviousOccurrenceFromStartDate(DateUtils.toDate(startDateTimeString, DateUtils.DATE_FORMAT_SHORT));
		}


		public Builder withNextOccurrenceFromStartDate(Date startDateTime) {
			return withOccurrencesFromStartDate(startDateTime, 1);
		}


		public Builder withPreviousOccurrenceFromStartDate(Date startDateTime) {
			return withOccurrencesFromStartDate(startDateTime, -1);
		}


		public Builder withOverriddenCalendars(Short... calendars) {
			this.overrideCalendarIds = Arrays.asList(calendars);
			return this;
		}


		public Builder withOverriddenCalendars(List<Short> calendars) {
			this.overrideCalendarIds = calendars;
			return this;
		}


		public Builder withOverriddenCalendarListBeanId(Integer beanId) {
			this.overrideCalendarListBeanId = beanId;
			return this;
		}


		/**
		 * Configures the {@link #CalendarScheduleOccurrenceCommand} to retrieve future occurrences when <code>count</code> is positive and past occurrences when
		 * <code>count</code> is negative.
		 */
		public Builder withOccurrencesFromStartDate(Date startDateTime, int count) {
			this.startDateTime = startDateTime;
			this.count = count;
			return this;
		}


		/**
		 * Configures the {@link #CalendarScheduleOccurrenceCommand} to retrieve future occurrences when <code>count</code> is positive and past occurrences when
		 * <code>count</code> is negative.
		 */
		public Builder withOccurrencesFromStartDate(String startDateTimeString, int count) {
			this.startDateTime = DateUtils.toDate(startDateTimeString, DateUtils.DATE_FORMAT_SHORT);
			this.count = count;
			return this;
		}


		public CalendarScheduleOccurrenceCommand build() {
			return new CalendarScheduleOccurrenceCommand(this.scheduleId, this.calendarSchedule, this.count, this.startDateTime, this.endDateTime, this.overrideCalendarListBeanId, this.overrideCalendarIds);
		}
	}
}
