package com.clifton.calendar.schedule.calculator;


import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceResult;
import com.clifton.calendar.schedule.CalendarScheduleUtils;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


public class WeeklyCalendarScheduleOccurrenceCalculator extends AbstractCalendarScheduleOccurrenceCalculator {

	@Override
	public ScheduleFrequencies getFrequency() {
		return ScheduleFrequencies.WEEKLY;
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		// check schedule start and end dates
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_START) {
			occurrenceFromDate = getCalendarScheduleStartDateTime(schedule);
		}
		else if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END) {
			return null;
		}

		occurrenceFromDate = adjustOccurrenceFromDateBasedOnTimeOfDay(schedule, occurrenceFromDate, true);
		return calculateOccurrence(schedule, occurrenceFromDate, true, overrideCalendarIds);
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetPreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		// check schedule start and end dates
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_END) {
			occurrenceFromDate = getCalendarScheduleEndDateTime(schedule);
		}
		else if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START) {
			return null;
		}

		occurrenceFromDate = adjustOccurrenceFromDateBasedOnTimeOfDay(schedule, occurrenceFromDate, false);
		return calculateOccurrence(schedule, occurrenceFromDate, false, overrideCalendarIds);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarScheduleOccurrenceResult calculateOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, boolean nextOccurrence, Short... overrideCalendarIds) {
		// get the first day of the first week of the schedule start date
		Date firstDayOfFirstWeek = DateUtils.getFirstDayOfWeek(getCalendarScheduleStartDateTime(schedule));
		// determine the weeks difference between the given occurrenceFromDate and the first day of the first week of the schedule start date
		int weeks = DateUtils.getWeeksDifference(occurrenceFromDate, firstDayOfFirstWeek);
		return nextOccurrence ? calculateNextOccurrence(schedule, occurrenceFromDate, firstDayOfFirstWeek, weeks, overrideCalendarIds) :
				calculatePreviousOccurrence(schedule, occurrenceFromDate, firstDayOfFirstWeek, weeks, overrideCalendarIds);
	}


	private CalendarScheduleOccurrenceResult calculateNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Date firstDayOfFirstWeek, int weeks, Short... overrideCalendarIds) {

		int actualOccurrences = weeks / schedule.getRecurrence();
		// if this week is not viable, try next week
		if (weeks - (actualOccurrences * schedule.getRecurrence()) >= 1) {
			Date startOfNextWeek = DateUtils.getFirstDayOfWeek(DateUtils.addDays(firstDayOfFirstWeek, (actualOccurrences + 1) * schedule.getRecurrence() * 7));
			return getNextOccurrence(schedule, startOfNextWeek);
		}

		// determine how many days to add to get to given day of week
		int daysToAdd = CalendarScheduleUtils.getCountToAdd(CalendarScheduleUtils.getWeekDayOccurrences(schedule), DateUtils.getDayOfWeek(occurrenceFromDate), 7);
		if (daysToAdd < 0) {
			return getNextOccurrence(schedule, DateUtils.addDays(DateUtils.getFirstDayOfWeek(occurrenceFromDate), 7 * schedule.getRecurrence()), overrideCalendarIds);
		}
		Date nextOccurrenceDate = DateUtils.setTime(DateUtils.addDays(occurrenceFromDate, daysToAdd), getCalendarScheduleStartDateTime(schedule));

		// move or skip the occurrenceFromDate based on the business day action
		Date businessDayResult = adjustForBusinessDays(schedule, nextOccurrenceDate) ? applyBusinessDays(nextOccurrenceDate, schedule, overrideCalendarIds) : nextOccurrenceDate;
		if (businessDayResult == null || isDateResultWithScheduleStartTimeBeforeRequestDate(schedule, businessDayResult, occurrenceFromDate)) {
			return getNextOccurrence(schedule, DateUtils.addDays(nextOccurrenceDate, 1), overrideCalendarIds);
		}
		return new CalendarScheduleOccurrenceResult(businessDayResult);
	}


	private CalendarScheduleOccurrenceResult calculatePreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Date firstDayOfFirstWeek, int weeks, Short... overrideCalendarIds) {

		int actualOccurrences = weeks / schedule.getRecurrence();
		// if this week is not viable, try previous week
		if (weeks - (actualOccurrences * schedule.getRecurrence()) >= 1) {
			Date endOfPreviousWeek = DateUtils.getLastDayOfWeek(DateUtils.addDays(firstDayOfFirstWeek, actualOccurrences * schedule.getRecurrence() * -7));
			return getPreviousOccurrence(schedule, DateUtils.setTime(endOfPreviousWeek, getCalendarScheduleEndDateTimeOrStartDateTime(schedule)), overrideCalendarIds);
		}

		// determine how many days to add to get to given day of week
		int daysToSubtract = CalendarScheduleUtils.getCountToSubtract(CalendarScheduleUtils.getWeekDayOccurrences(schedule), DateUtils.getDayOfWeek(occurrenceFromDate));

		Date previousOccurrenceDate;

		// indicates that the occurrence for this week has already happened
		if (daysToSubtract < 0) {

			// We need to see if the business day adjustment will cause the occurrence before this one to rollover to the next week
			Date newEndDate = DateUtils.addDays(DateUtils.getLastDayOfWeek(occurrenceFromDate), -7 * schedule.getRecurrence());

			// Temporarily override business day convention to see what previous occurrence would be
			CalendarSchedule scheduleCopy = BeanUtils.cloneBean(schedule, false, false);
			scheduleCopy.setBusinessDayConvention(null);

			CalendarScheduleOccurrenceResult tentativeResult = getPreviousOccurrence(scheduleCopy, newEndDate);

			if (tentativeResult == null) {
				return null;
			}
			else {
				previousOccurrenceDate = tentativeResult.getDateOfOccurrence();
			}
		}
		else {
			previousOccurrenceDate = DateUtils.setTime(DateUtils.addDays(occurrenceFromDate, -daysToSubtract), getCalendarScheduleEndDateTimeOrStartDateTime(schedule));
		}

		if (previousOccurrenceDate == null) {
			return null;
		}

		// move or skip the occurrenceFromDate based on the business day action
		Date businessDayResult = applyBusinessDays(previousOccurrenceDate, schedule, overrideCalendarIds);

		if (businessDayResult == null || businessDayResult.after(occurrenceFromDate)) {
			return getPreviousOccurrence(schedule, DateUtils.addDays(occurrenceFromDate, -1), overrideCalendarIds);
		}
		return new CalendarScheduleOccurrenceResult(businessDayResult);
	}


	private Date adjustOccurrenceFromDateBasedOnTimeOfDay(CalendarSchedule schedule, Date occurrenceFromDate, boolean nextOccurrence) {
		// get the occurrence for the current data
		Date fromDateWithScheduleStartTime = DateUtils.setTime(occurrenceFromDate, getCalendarScheduleStartDateTime(schedule));
		if (nextOccurrence && DateUtils.getMinutesFromMidnight(occurrenceFromDate) > DateUtils.getMinutesFromMidnight(fromDateWithScheduleStartTime)) {
			// clear the time and add a day to the occurrenceFromDate if its time is after the schedule start time when searching for the next occurrence
			return DateUtils.clearTime(DateUtils.addDays(occurrenceFromDate, 1));
		}
		else if (!nextOccurrence && DateUtils.getMinutesFromMidnight(occurrenceFromDate) < DateUtils.getMinutesFromMidnight(fromDateWithScheduleStartTime)) {
			// set the time for the occurrenceFromDate to the schedule end time and subtract a day if it is before the schedule start time when searching for the previous occurrence
			return getYesterdayWithScheduleEndOrStartTime(schedule, occurrenceFromDate);
		}
		return occurrenceFromDate;
	}
}
