package com.clifton.calendar.schedule;


import com.clifton.calendar.schedule.api.Schedule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class CalendarScheduleUtils {

	/**
	 * Determine the number that needs to be added to value to get to the next item in the array.<br/>
	 * <br/>
	 * Examples:<br/>
	 * <br/>
	 * inputs:<br/>
	 * <b>values = [0,4,7]</b><br/>
	 * <b>value = 5</b><br/>
	 * <b>length = 7</b><br/>
	 * return: <b>2</b><br/>
	 * <br/>
	 * inputs:<br/>
	 * <b>values = [0,4,7]</b><br/>
	 * <b>value = 4</b><br/>
	 * <b>length = 7</b><br/>
	 * return: <b>0</b><br/>
	 * <br/>
	 * inputs:<br/>
	 * <b>values = [0,4,7]</b><br/>
	 * <b>value = 1</b><br/>
	 * <b>length = 7</b><br/>
	 * return: <b>3</b><br/>
	 *
	 * @param values
	 * @param value
	 * @param length
	 */
	public static int getCountToAdd(List<Integer> values, int value, int length) {
		if (values.contains(value)) {
			return 0;
		}
		if (value >= length) {
			return -1;
		}
		int result = -1;
		boolean found = false;
		for (int i = value; i <= length; i++) {
			result++;
			if (values.contains(i)) {
				found = true;
				break;
			}
		}
		return found ? result : -1;
	}


	/**
	 * Determine the number that needs to be subtracted from the value to get to the next item in the array.<br/>
	 * <br/>
	 * Examples:<br/>
	 * <br/>
	 * inputs:<br/>
	 * <b>values = [0,4,7]</b><br/>
	 * <b>value = 5</b><br/>
	 * return: <b>1</b><br/>
	 * <br/>
	 * inputs:<br/>
	 * <b>values = [0,4,7]</b><br/>
	 * <b>value = 4</b><br/>
	 * return: <b>0</b><br/>
	 * <br/>
	 * inputs:<br/>
	 * <b>values = [0,4,7]</b><br/>
	 * <b>value = 2</b><br/>
	 * return: <b>2</b><br/>
	 *
	 * @param values
	 * @param value
	 */
	public static int getCountToSubtract(List<Integer> values, int value) {
		if (values.contains(value)) {
			return 0;
		}
		if (value < 0) {
			return -1;
		}
		int result = -1;
		boolean found = false;
		for (int i = value; i >= 0; i--) {
			result++;
			if (values.contains(i)) {
				found = true;
				break;
			}
		}
		return found ? result : -1;
	}


	/**
	 * Returns the list of integers that represent the week days when schedule will have an occurrence.
	 *
	 * @param schedule
	 */
	public static List<Integer> getWeekDayOccurrences(CalendarSchedule schedule) {
		List<Integer> result = new ArrayList<>();
		if (schedule.isSunday()) {
			result.add(Calendar.SUNDAY);
		}
		if (schedule.isMonday()) {
			result.add(Calendar.MONDAY);
		}
		if (schedule.isTuesday()) {
			result.add(Calendar.TUESDAY);
		}
		if (schedule.isWednesday()) {
			result.add(Calendar.WEDNESDAY);
		}
		if (schedule.isThursday()) {
			result.add(Calendar.THURSDAY);
		}
		if (schedule.isFriday()) {
			result.add(Calendar.FRIDAY);
		}
		if (schedule.isSaturday()) {
			result.add(Calendar.SATURDAY);
		}
		return result;
	}


	public static void updateWeekDay(CalendarSchedule schedule, int dayOfWeek) {
		switch (dayOfWeek) {
			case 1:
				schedule.setSunday(true);
				break;
			case 2:
				schedule.setMonday(true);
				break;
			case 3:
				schedule.setTuesday(true);
				break;
			case 4:
				schedule.setWednesday(true);
				break;
			case 5:
				schedule.setThursday(true);
				break;
			case 6:
				schedule.setFriday(true);
				break;
			case 7:
				schedule.setSaturday(true);
				break;
		}
	}


	public static void updateWeekDay(Schedule schedule, int dayOfWeek) {
		switch (dayOfWeek) {
			case 1:
				schedule.setSunday(true);
				break;
			case 2:
				schedule.setMonday(true);
				break;
			case 3:
				schedule.setTuesday(true);
				break;
			case 4:
				schedule.setWednesday(true);
				break;
			case 5:
				schedule.setThursday(true);
				break;
			case 6:
				schedule.setFriday(true);
				break;
			case 7:
				schedule.setSaturday(true);
				break;
		}
	}


	public static void updateMonth(CalendarSchedule schedule, int monthOfYear) {
		switch (monthOfYear) {
			case 1:
				schedule.setJanuary(true);
				break;
			case 2:
				schedule.setFebruary(true);
				break;
			case 3:
				schedule.setMarch(true);
				break;
			case 4:
				schedule.setApril(true);
				break;
			case 5:
				schedule.setMay(true);
				break;
			case 6:
				schedule.setJune(true);
				break;
			case 7:
				schedule.setJuly(true);
				break;
			case 8:
				schedule.setAugust(true);
				break;
			case 9:
				schedule.setSeptember(true);
				break;
			case 10:
				schedule.setOctober(true);
				break;
			case 11:
				schedule.setNovember(true);
				break;
			case 12:
				schedule.setDecember(true);
				break;
		}
	}


	public static void updateMonth(Schedule schedule, int monthOfYear) {
		switch (monthOfYear) {
			case 1:
				schedule.setJanuary(true);
				break;
			case 2:
				schedule.setFebruary(true);
				break;
			case 3:
				schedule.setMarch(true);
				break;
			case 4:
				schedule.setApril(true);
				break;
			case 5:
				schedule.setMay(true);
				break;
			case 6:
				schedule.setJune(true);
				break;
			case 7:
				schedule.setJuly(true);
				break;
			case 8:
				schedule.setAugust(true);
				break;
			case 9:
				schedule.setSeptember(true);
				break;
			case 10:
				schedule.setOctober(true);
				break;
			case 11:
				schedule.setNovember(true);
				break;
			case 12:
				schedule.setDecember(true);
				break;
		}
	}
}
