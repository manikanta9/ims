package com.clifton.calendar.schedule.calculator;


import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceResult;
import com.clifton.calendar.schedule.CalendarScheduleUtils;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;


public class MonthlyWeekdayCalendarScheduleOccurrenceCalculator extends MonthlyCalendarScheduleOccurrenceCalculator {

	@Override
	public ScheduleFrequencies getFrequency() {
		return ScheduleFrequencies.MONTHLY_WEEKDAY;
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetNextOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		return getNextMonthlyWeekdayOccurrence(schedule, occurrenceFromDate, overrideCalendarIds);
	}


	@Override
	public CalendarScheduleOccurrenceResult doGetPreviousOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		return getPreviousMonthlyWeekdayOccurrence(schedule, occurrenceFromDate, overrideCalendarIds);
	}


	@Override
	protected boolean isOccurrenceFromDateMonthViable(CalendarSchedule schedule, Date occurrenceFromDate) {
		if (getMonthlyVirtualStartDate(schedule) != null) {
			int months = DateUtils.getMonthsDifference(DateUtils.getFirstDayOfMonth(occurrenceFromDate), DateUtils.getFirstDayOfMonth(getMonthlyVirtualStartDate(schedule)));
			int actualOccurrences = months / schedule.getRecurrence();
			if ((months - actualOccurrences * schedule.getRecurrence()) >= 1) {
				return false;
			}
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarScheduleOccurrenceResult getNextMonthlyWeekdayOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		// check schedule start and end dates
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_START) {
			occurrenceFromDate = getCalendarScheduleStartDateTime(schedule, false);
		}
		else if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END) {
			return null;
		}

		if (!isOccurrenceFromDateMonthViable(schedule, occurrenceFromDate)) {
			return getNextMonthlyWeekdayOccurrence(schedule, DateUtils.getFirstDayOfNextMonthWithoutTime(occurrenceFromDate), overrideCalendarIds);
		}

		Date nextOccurrenceDate = getNextMonthlyWeekDay(schedule, occurrenceFromDate);
		if (nextOccurrenceDate == null) {
			return getNextMonthlyWeekdayOccurrence(schedule, DateUtils.getFirstDayOfNextMonthWithoutTime(occurrenceFromDate), overrideCalendarIds);
		}

		nextOccurrenceDate = DateUtils.setTime(nextOccurrenceDate, getCalendarScheduleStartDateTime(schedule));
		if (isDateResultWithScheduleStartTimeBeforeOccurrenceFromDateTime(occurrenceFromDate, nextOccurrenceDate)) {
			return getNextMonthlyWeekdayOccurrence(schedule, DateUtils.clearTime(DateUtils.addDays(occurrenceFromDate, 1)), overrideCalendarIds);
		}

		// move or skip the date based on the business day action
		Date businessDayResult = applyBusinessDays(nextOccurrenceDate, schedule, overrideCalendarIds);
		if (businessDayResult == null || isDateResultWithScheduleStartTimeBeforeRequestDate(schedule, businessDayResult, occurrenceFromDate)) {
			return getNextMonthlyWeekdayOccurrence(schedule, DateUtils.addMinutes(nextOccurrenceDate, 1), overrideCalendarIds);
		}
		return new CalendarScheduleOccurrenceResult(businessDayResult);
	}


	private CalendarScheduleOccurrenceResult getPreviousMonthlyWeekdayOccurrence(CalendarSchedule schedule, Date occurrenceFromDate, Short... overrideCalendarIds) {
		// check schedule start and end dates
		OccurrenceDateComparisons compareStartEnd = compareOccurrenceFromDateToScheduleStartAndEnd(schedule, occurrenceFromDate);
		if (compareStartEnd == OccurrenceDateComparisons.AFTER_SCHEDULE_END || compareStartEnd == OccurrenceDateComparisons.EQUAL_TO_SCHEDULE_END) {
			occurrenceFromDate = getCalendarScheduleEndDateTimeOrStartDateTime(schedule);
		}
		else if (compareStartEnd == OccurrenceDateComparisons.BEFORE_SCHEDULE_START) {
			return null;
		}

		if (!isOccurrenceFromDateMonthViable(schedule, occurrenceFromDate)) {
			return getPreviousMonthlyWeekdayOccurrence(schedule, getLastDayOfPreviousMonthWithScheduleEndOrStartTime(schedule, occurrenceFromDate), overrideCalendarIds);
		}

		Date previousOccurrenceDate = getPreviousMonthlyWeekDay(schedule, occurrenceFromDate);
		if (previousOccurrenceDate == null) {
			return getPreviousMonthlyWeekdayOccurrence(schedule, getLastDayOfPreviousMonthWithScheduleEndOrStartTime(schedule, occurrenceFromDate), overrideCalendarIds);
		}

		previousOccurrenceDate = DateUtils.setTime(previousOccurrenceDate, getCalendarScheduleEndDateTimeOrStartDateTime(schedule));
		if (isDateResultWithScheduleEndOrStartTimeAfterOccurrenceFromDateTime(occurrenceFromDate, previousOccurrenceDate)) {
			return getPreviousMonthlyWeekdayOccurrence(schedule, getYesterdayWithScheduleEndOrStartTime(schedule, occurrenceFromDate), overrideCalendarIds);
		}

		// move or skip the date based on the business day action
		Date businessDayResult = applyBusinessDays(previousOccurrenceDate, schedule, overrideCalendarIds);
		if (businessDayResult == null || isDateResultWithScheduleEndTimeAfterRequestDate(schedule, businessDayResult, occurrenceFromDate)) {
			return getPreviousOccurrence(schedule, DateUtils.addMinutes(previousOccurrenceDate, -1), overrideCalendarIds);
		}
		return new CalendarScheduleOccurrenceResult(businessDayResult);
	}


	private boolean isDateResultWithScheduleStartTimeBeforeOccurrenceFromDateTime(Date occurrenceFromDate, Date nextOccurrenceDate) {
		int dayOfMonth = DateUtils.getDayOfMonth(occurrenceFromDate);
		return dayOfMonth == DateUtils.getDayOfMonth(nextOccurrenceDate) && DateUtils.getMinutesFromMidnight(occurrenceFromDate) > DateUtils.getMinutesFromMidnight(nextOccurrenceDate);
	}


	private boolean isDateResultWithScheduleEndOrStartTimeAfterOccurrenceFromDateTime(Date occurrenceFromDate, Date previousOccurrenceDate) {
		int dayOfMonth = DateUtils.getDayOfMonth(occurrenceFromDate);
		return dayOfMonth == DateUtils.getDayOfMonth(previousOccurrenceDate) && DateUtils.getMinutesFromMidnight(occurrenceFromDate) < DateUtils.getMinutesFromMidnight(previousOccurrenceDate);
	}


	private Date getNextMonthlyWeekDay(CalendarSchedule schedule, Date date) {
		List<Integer> occurrences = schedule.getWeekdayOccurrenceList();
		List<Integer> weekDays = CalendarScheduleUtils.getWeekDayOccurrences(schedule);
		Collections.sort(weekDays);
		Date nextMonthlyWeekday = null;
		for (Integer occurrence : CollectionUtils.getIterable(occurrences)) {
			for (Integer weekDay : CollectionUtils.getIterable(weekDays)) {
				Date newDate = DateUtils.getNthWeekdayOfMonth(date, occurrence, weekDay);
				if (DateUtils.isDateAfterOrEqual(newDate, date) && (nextMonthlyWeekday == null || DateUtils.isDateBefore(newDate, nextMonthlyWeekday, true))) {
					nextMonthlyWeekday = newDate;
				}
			}
		}
		return nextMonthlyWeekday;
	}


	private Date getPreviousMonthlyWeekDay(CalendarSchedule schedule, Date date) {
		List<Integer> occurrences = schedule.getWeekdayOccurrenceList();
		List<Integer> weekDays = CalendarScheduleUtils.getWeekDayOccurrences(schedule);
		Collections.sort(weekDays);
		Date previousMonthlyWeekday = null;
		for (Integer occurrence : CollectionUtils.getIterable(occurrences)) {
			for (Integer weekDay : CollectionUtils.getIterable(weekDays)) {
				Date newDate = DateUtils.getNthWeekdayOfMonth(date, occurrence, weekDay);
				if (DateUtils.isDateBeforeOrEqual(newDate, date, true) && (previousMonthlyWeekday == null || DateUtils.isDateAfter(newDate, previousMonthlyWeekday))) {
					previousMonthlyWeekday = newDate;
				}
			}
		}
		return previousMonthlyWeekday;
	}
}
