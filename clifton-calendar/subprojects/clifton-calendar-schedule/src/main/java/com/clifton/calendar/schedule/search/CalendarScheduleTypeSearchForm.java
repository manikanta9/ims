package com.clifton.calendar.schedule.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


/**
 * @author MitchellF
 */
public class CalendarScheduleTypeSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField
	private Boolean timeSupported;

	@SearchField
	private Boolean nameSystemDefined;

	@SearchField(searchField = "systemTable.id")
	private Short systemTableId;

	@SearchField(searchField = "systemTable.name")
	private String systemTableName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public Boolean getTimeSupported() {
		return this.timeSupported;
	}


	public void setTimeSupported(Boolean timeSupported) {
		this.timeSupported = timeSupported;
	}


	public Boolean getNameSystemDefined() {
		return this.nameSystemDefined;
	}


	public void setNameSystemDefined(Boolean nameSystemDefined) {
		this.nameSystemDefined = nameSystemDefined;
	}


	public Short getSystemTableId() {
		return this.systemTableId;
	}


	public void setSystemTableId(Short systemTableId) {
		this.systemTableId = systemTableId;
	}


	public String getSystemTableName() {
		return this.systemTableName;
	}


	public void setSystemTableName(String systemTableName) {
		this.systemTableName = systemTableName;
	}
}
