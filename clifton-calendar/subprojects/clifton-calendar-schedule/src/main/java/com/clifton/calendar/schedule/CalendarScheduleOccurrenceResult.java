package com.clifton.calendar.schedule;


import java.util.Date;


public class CalendarScheduleOccurrenceResult {

	private Date dateOfOccurrence;
	private Date scheduleOccurrenceFromDate;


	public CalendarScheduleOccurrenceResult(Date dateOfOccurrence) {
		this(dateOfOccurrence, null);
	}


	public CalendarScheduleOccurrenceResult(Date dateOfOccurrence, Date scheduleOccurrenceFromDate) {
		this.dateOfOccurrence = dateOfOccurrence;
		this.scheduleOccurrenceFromDate = scheduleOccurrenceFromDate;
	}


	public Date getDateOfOccurrence() {
		return this.dateOfOccurrence;
	}


	public void setDateOfOccurrence(Date dateOfOccurrence) {
		this.dateOfOccurrence = dateOfOccurrence;
	}


	public Date getScheduleOccurrenceFromDate() {
		return this.scheduleOccurrenceFromDate;
	}


	public void setScheduleOccurrenceFromDate(Date scheduleOccurrenceFromDate) {
		this.scheduleOccurrenceFromDate = scheduleOccurrenceFromDate;
	}
}
