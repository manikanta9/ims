package com.clifton.calendar.schedule.runner;

import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.core.beans.NamedObject;

import java.io.Serializable;


/**
 * The <code>ScheduleAware</code> interface marks objects that can be scheduled.
 *
 * @author theodorez
 */
public interface ScheduleAware<T extends Serializable> extends NamedObject {

	public boolean isEnabled();


	public T getId();


	public CalendarSchedule getSchedule();
}
