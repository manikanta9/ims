package com.clifton.calendar.schedule.api;

import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceCommand;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.schedule.search.CalendarScheduleSearchForm;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * Implementation of the API to get calendar schedules and schedule occurrences.  Uses {@link CalendarScheduleService} to retrieve schedules from the database,
 * or to calculate sets of occurrences for a given schedule/calendar
 *
 * @author mitchellf
 */
@Component
public class ScheduleApiServiceImpl implements ScheduleApiService {

	private CalendarScheduleService calendarScheduleService;
	private CalendarSetupService calendarSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Schedule getSchedule(int id) {
		CalendarSchedule schedule = getCalendarScheduleService().getCalendarSchedule(id);
		return Optional.ofNullable(schedule).map(CalendarSchedule::toSchedule).orElse(null);
	}


	@Override
	public Schedule getScheduleByName(String name) {
		CalendarScheduleSearchForm sf = new CalendarScheduleSearchForm();
		sf.setName(name);
		List<CalendarSchedule> schedules = getCalendarScheduleService().getCalendarScheduleList(sf);
		CalendarSchedule schedule = CollectionUtils.getOnlyElement(schedules);
		return Optional.ofNullable(schedule).map(CalendarSchedule::toSchedule).orElse(null);
	}


	@Override
	public Schedule saveSchedule(Schedule bean) {
		return getCalendarScheduleService().saveCalendarSchedule(toCalendarSchedule(bean)).toSchedule();
	}


	@Override
	public void deleteSchedule(int id) {
		getCalendarScheduleService().deleteCalendarSchedule(id);
	}


	@Override
	public ScheduleType getScheduleType(short id) {
		return getCalendarScheduleService().getCalendarScheduleType(id).toScheduleType();
	}


	@Override
	public ScheduleType getScheduleTypeByName(String name) {
		return getCalendarScheduleService().getCalendarScheduleTypeByName(name).toScheduleType();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getScheduleOccurrence(ScheduleOccurrenceCommand command) {

		CalendarScheduleOccurrenceCommand.Builder builder = getBuilder(command).withOccurrencesFromStartDate(command.getStartDate(), command.isPastOccurrences() ? -1 : 1);

		if (!CollectionUtils.isEmpty(command.getCalendarIdList())) {
			builder.withOverriddenCalendars(command.getCalendarIdList());
		}

		CalendarScheduleOccurrenceCommand newCommand = builder.build();

		return getCalendarScheduleService().getCalendarScheduleOccurrence(newCommand);
	}


	@Override
	public List<Date> getScheduleOccurrences(ScheduleOccurrenceCommand command) {

		CalendarScheduleOccurrenceCommand.Builder builder = getBuilder(command);

		if (command.getEndDate() != null) {
			builder.withOccurrencesBetween(command.getStartDate(), command.getEndDate());
		}

		if (!CollectionUtils.isEmpty(command.getCalendarIdList())) {
			builder.withOverriddenCalendars(command.getCalendarIdList());
		}

		CalendarScheduleOccurrenceCommand newCommand = builder.withOccurrencesFromStartDate(command.getStartDate(), (command.isPastOccurrences() ? -1 : 1) * command.getOccurrences()).build();

		return getCalendarScheduleService().getCalendarScheduleOccurrences(newCommand);
	}


	private CalendarScheduleOccurrenceCommand.Builder getBuilder(ScheduleOccurrenceCommand command) {

		if (command.getScheduleId() != null) {
			return CalendarScheduleOccurrenceCommand.of(command.getScheduleId());
		}
		else {
			ValidationUtils.assertNotNull(command.getSchedule(), "Either a Schedule ID or populated Schedule is required to calculate occurrences");
			if (command.getSchedule().getId() != null) {
				return CalendarScheduleOccurrenceCommand.of(command.getSchedule().getId());
			}
			else {
				return CalendarScheduleOccurrenceCommand.of(toCalendarSchedule(command.getSchedule()));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarSchedule toCalendarSchedule(Schedule schedule) {

		CalendarSchedule result = new CalendarSchedule();
		result.setId(schedule.getId());
		if (schedule.getCalendar() != null) {
			result.setCalendar(getCalendarSetupService().getCalendar(schedule.getCalendar().getId()));
		}
		result.setCalendarScheduleType(getCalendarScheduleService().getCalendarScheduleTypeByName(schedule.getScheduleType().getName()));
		if (schedule.getBusinessDayConvention() != null) {
			result.setBusinessDayConvention(BusinessDayConventions.valueOf(schedule.getBusinessDayConvention()));
		}
		result.setRollupCalendarSchedule(schedule.isRollupCalendarSchedule());

		if (schedule.isRollupCalendarSchedule()) {
			List<CalendarSchedule> childSchedules = CollectionUtils.getConverted(schedule.getChildScheduleIdList(), child -> {
				CalendarSchedule childCalendarSchedule = getCalendarScheduleService().getCalendarSchedule(child.getId());
				return childCalendarSchedule == null ? toCalendarSchedule(child) : childCalendarSchedule;
			});
			result.setChildCalendarScheduleList(childSchedules);
		}

		result.setSystemDefined(schedule.isSystemDefined());
		result.setStartDate(schedule.getStartDate());
		result.setEndDate(schedule.getEndDate());
		result.setStartTime(schedule.getStartTime());
		result.setEndTime(schedule.getEndTime());
		result.setRecurrence(schedule.getRecurrence());
		result.setFrequency(ScheduleFrequencies.valueOf(schedule.getFrequency()));
		result.setDayOfMonth(schedule.getDayOfMonth());
		result.setAdditionalBusinessDays(schedule.getAdditionalBusinessDays());

		// For the rest of the properties, only need to set those that are applicable to the given frequency
		switch (result.getFrequency()) {
			case WEEKLY:
			case MONTHLY_WEEKDAY:
				processWeeklyFrequencies(schedule, result);
				break;
			case YEARLY:
				processYearlyFrequencies(schedule, result);
				break;
			default:
				break;
		}

		result.setCountFromLastDayOfMonth(schedule.isCountFromLastDayOfMonth());
		result.setFirstOccurrence(schedule.getFirstOccurrence());
		result.setWeekdayOccurrenceListString(schedule.getWeekdayOccurrenceListString());

		return result;
	}


	private void processWeeklyFrequencies(Schedule schedule, CalendarSchedule result) {
		result.setSunday(schedule.isSunday());
		result.setMonday(schedule.isMonday());
		result.setTuesday(schedule.isTuesday());
		result.setWednesday(schedule.isWednesday());
		result.setThursday(schedule.isThursday());
		result.setFriday(schedule.isFriday());
		result.setSaturday(schedule.isSaturday());
	}


	private void processYearlyFrequencies(Schedule schedule, CalendarSchedule result) {
		result.setJanuary(schedule.isJanuary());
		result.setFebruary(schedule.isFebruary());
		result.setMarch(schedule.isMarch());
		result.setApril(schedule.isApril());
		result.setMay(schedule.isMay());
		result.setJune(schedule.isJune());
		result.setJuly(schedule.isJuly());
		result.setAugust(schedule.isAugust());
		result.setSeptember(schedule.isSeptember());
		result.setOctober(schedule.isOctober());
		result.setNovember(schedule.isNovember());
		result.setDecember(schedule.isDecember());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarScheduleService getCalendarScheduleService() {
		return this.calendarScheduleService;
	}


	public void setCalendarScheduleService(CalendarScheduleService calendarScheduleService) {
		this.calendarScheduleService = calendarScheduleService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
