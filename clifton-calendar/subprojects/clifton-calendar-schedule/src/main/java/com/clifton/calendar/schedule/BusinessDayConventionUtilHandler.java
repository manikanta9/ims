package com.clifton.calendar.schedule;

import com.clifton.calendar.setup.Calendar;

import java.util.Date;


public interface BusinessDayConventionUtilHandler {

	/**
	 * Returns a date with the supplied business day convention applied to it.
	 *
	 * @param schedule the schedule to use while applying the convention
	 * @param date     the date to apply the business day convention to
	 */
	public Date applyBusinessDayConvention(CalendarSchedule schedule, Date date);


	/**
	 * Returns a date with the supplied business day convention applied to it.
	 *
	 * @param calendars  the calendars that provides the business days
	 * @param date       the date to apply the business day convention to
	 * @param convention the business day convention
	 */
	public Date applyBusinessDayConvention(Date date, BusinessDayConventions convention, Calendar... calendars);


	/**
	 * Returns a date with the supplied business day convention applied to it.
	 *
	 * @param calendarIds IDs of the calendars that provides the business days
	 * @param date        the date to apply the business day convention to
	 * @param convention  the business day convention
	 */
	public Date applyBusinessDayConvention(Date date, BusinessDayConventions convention, Short... calendarIds);
}
