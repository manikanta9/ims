Clifton.calendar.schedule.ScheduleWithoutTimeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Schedule',
	iconCls: 'schedule',
	width: 735,
	height: 600,


	items: [{
		xtype: 'formpanel',
		loadDefaultDataAfterRender: true,
		validateDisabled: false,
		defaults: {},
		url: 'calendarSchedule.json',
		tbar: {xtype: 'schedule-preview-toolbar'},
		items: [
			{
				xtype: 'fieldset',
				title: 'Schedule',
				labelWidth: 150,
				items: [
					{name: 'systemDefined', xtype: 'hidden', value: 'false'},
					{fieldLabel: 'Schedule Name', name: 'name', xtype: 'textfield'},
					{fieldLabel: 'Schedule Type', name: 'calendarScheduleType.name', hiddenName: 'calendarScheduleType.id', xtype: 'combo', url: 'calendarScheduleTypeListFind.json', detailPageClass: 'Clifton.calendar.schedule.CalendarScheduleTypeWindow', detailIdField: 'calendarScheduleType.id'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
					{xtype: 'system-entity-modify-condition-combo'},
					{fieldLabel: 'Calendar', name: 'calendar.label', hiddenName: 'calendar.id', displayField: 'label', xtype: 'combo', url: 'calendarListFind.json', detailPageClass: 'Clifton.calendar.setup.CalendarWindow'},
					{
						fieldLabel: 'Business Day Convention', name: 'businessDayConvention', hiddenName: 'businessDayConvention', displayField: 'name', valueField: 'businessDayConvention', mode: 'local', xtype: 'combo',
						store: {
							xtype: 'arraystore',
							fields: ['name', 'businessDayConvention', 'description'],
							data: Clifton.calendar.BUSINESS_DAY_CONVENTIONS
						}, requiredFields: ['calendar.label']
					}
				]
			},

			{
				xtype: 'radioinputselect',
				selectName: 'frequency',
				selectTitle: 'Frequency',
				controlTitle: 'Recurrence',
				controlHeight: 200,
				labelWidth: 110,
				defaults: {width: 125},
				inputControls: [
					{
						selectControl: {
							boxLabel: 'Daily',
							inputValue: 'DAILY'
						},
						control: {
							items: [
								{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
								{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', tooltip: 'The scheduled end date.  Leave empty to continue indefinitely.'},
								{
									fieldLabel: 'Recur every', xtype: 'compositefield',
									items: [
										{name: 'recurrence', width: 60, xtype: 'spinnerfield', value: '1'},
										{value: 'day(s)', xtype: 'displayfield', width: 80}
									]
								}
							]
						}
					},
					{
						selectControl: {
							boxLabel: 'Weekly',
							inputValue: 'WEEKLY'
						},
						control: {
							items: [
								{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
								{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', tooltip: 'The scheduled end date.  Leave empty to continue indefinitely.'},
								{
									fieldLabel: 'Recur every', xtype: 'compositefield',
									items: [
										{name: 'recurrence', width: 60, xtype: 'spinnerfield', value: '1'},
										{value: 'week(s)', xtype: 'displayfield', width: 80}
									]
								},
								{
									xtype: 'checkboxgroup',
									fieldLabel: 'Weekdays',
									columns: 4,
									items: [
										{boxLabel: 'Sunday', name: 'sunday'},
										{boxLabel: 'Monday', name: 'monday'},
										{boxLabel: 'Tuesday', name: 'tuesday'},
										{boxLabel: 'Wednesday', name: 'wednesday'},
										{boxLabel: 'Thursday', name: 'thursday'},
										{boxLabel: 'Friday', name: 'friday'},
										{boxLabel: 'Saturday', name: 'saturday'}
									]
								}
							]
						}
					},
					{
						selectControl: {
							boxLabel: 'Monthly',
							inputValue: 'MONTHLY',
							checked: true
						},
						control: {
							items: [
								{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
								{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', tooltip: 'The scheduled end date.  Leave empty to continue indefinitely.'},
								{
									fieldLabel: 'Day', xtype: 'compositefield',
									items: [
										{name: 'dayOfMonth', width: 60, xtype: 'spinnerfield', value: '1', maxValue: 31},
										{value: '&nbsp;of every', xtype: 'displayfield', width: 60},
										{name: 'recurrence', width: 60, xtype: 'spinnerfield', value: '1', maxValue: 11},
										{value: 'month(s)', xtype: 'displayfield', width: 80}
									]
								},
								{boxLabel: '&nbsp;count days backwards from last day of month', name: 'countFromLastDayOfMonth', xtype: 'checkbox'},
								{
									fieldLabel: 'Also add', xtype: 'compositefield', qtip: 'Business days to apply AFTER performing Business Day Convention for day of month.',
									items: [
										{name: 'additionalBusinessDays', width: 60, xtype: 'spinnerfield', minValue: -30, maxValue: 30},
										{value: '&nbsp;business days (negative value to subtract)', xtype: 'displayfield', flex: 1}
									]
								}
							]
						}
					},
					{
						selectControl: {
							boxLabel: 'Monthly Weekday',
							inputValue: 'MONTHLY_WEEKDAY'
						},
						control: {
							items: [
								{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
								{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', tooltip: 'The scheduled end date.  Leave empty to continue indefinitely.'},
								{
									fieldLabel: 'Recur every', xtype: 'compositefield',
									items: [
										{name: 'recurrence', width: 60, xtype: 'spinnerfield', value: '1'},
										{value: '&nbsp;month(s)', xtype: 'displayfield', width: 80}
									]
								},
								{
									fieldLabel: 'Occurrence List', xtype: 'compositefield',
									items: [
										{name: 'weekdayOccurrenceListString', width: 60, xtype: 'textfield', allowBlank: false, tooltip: 'The list of instances.  For example, to schedule the 1st and 3rd Friday enter \'1,3\' here and select Friday in the weekdays.'},
										{value: '&nbsp;(i.e. for 1st and 3rd Friday, enter \'1,3\')', xtype: 'displayfield', flex: 1}
									]
								},
								{
									xtype: 'checkboxgroup',
									fieldLabel: 'Weekdays',
									columns: 4,
									items: [
										{boxLabel: 'Sunday', name: 'sunday'},
										{boxLabel: 'Monday', name: 'monday'},
										{boxLabel: 'Tuesday', name: 'tuesday'},
										{boxLabel: 'Wednesday', name: 'wednesday'},
										{boxLabel: 'Thursday', name: 'thursday'},
										{boxLabel: 'Friday', name: 'friday'},
										{boxLabel: 'Saturday', name: 'saturday'}
									]
								}
							]
						}
					},
					{
						selectControl: {
							boxLabel: 'Yearly',
							inputValue: 'YEARLY'
						},
						control: {
							items: [
								{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
								{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', tooltip: 'The scheduled end date.  Leave empty to continue indefinitely.'},
								{
									fieldLabel: 'Recur every', xtype: 'compositefield',
									items: [
										{name: 'recurrence', width: 60, xtype: 'spinnerfield', value: '1'},
										{value: 'year(s)', xtype: 'displayfield', width: 80}
									]
								},
								{
									xtype: 'checkboxgroup',
									fieldLabel: 'Months',
									columns: 4,
									items: [
										{boxLabel: 'January', name: 'january'},
										{boxLabel: 'February', name: 'february'},
										{boxLabel: 'March', name: 'march'},
										{boxLabel: 'April', name: 'april'},
										{boxLabel: 'May', name: 'may'},
										{boxLabel: 'June', name: 'june'},
										{boxLabel: 'July', name: 'july'},
										{boxLabel: 'August', name: 'august'},
										{boxLabel: 'September', name: 'september'},
										{boxLabel: 'October', name: 'october'},
										{boxLabel: 'November', name: 'november'},
										{boxLabel: 'December', name: 'december'}
									]
								}
							]
						}
					}
				]
			}
		]
	}]
});
