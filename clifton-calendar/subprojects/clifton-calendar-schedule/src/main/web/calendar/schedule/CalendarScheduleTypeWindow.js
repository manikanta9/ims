Clifton.calendar.schedule.CalendarScheduleTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Calendar Schedule Type',
	iconCls: 'schedule',
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		url: 'calendarScheduleType.json',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Schedule Type Name', name: 'name', xtype: 'textfield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'System Table', name: 'systemTable.name', hiddenName: 'systemTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow'},
			{fieldLabel: 'Time Supported', name: 'timeSupported', xtype: 'checkbox'}
		]
	}]
});
