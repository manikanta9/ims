Clifton.calendar.schedule.ScheduleWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'calendarSchedule.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.calendar.schedule.StandardScheduleWindow';
		if (entity && entity.rollupCalendarSchedule) {
			className = 'Clifton.calendar.schedule.RollupCalendarScheduleWindow';
		}
		else if ((entity && entity.calendarScheduleType && !entity.calendarScheduleType.timeSupported)
			|| (!entity && config && config.defaultData && config.defaultData.calendarScheduleType && !config.defaultData.calendarScheduleType.timeSupported)) {
			className = 'Clifton.calendar.schedule.ScheduleWithoutTimeWindow';
		}
		return className;
	}
});
