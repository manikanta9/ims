Clifton.calendar.schedule.RollupCalendarScheduleWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Calendar Schedule (Rollup)',
	iconCls: 'schedule',
	width: 745,
	height: 465,

	items: [{
		xtype: 'formpanel',
		validateDisabled: false,
		defaults: {},
		url: 'calendarSchedule.json',
		items: [{
			xtype: 'fieldset',
			title: 'Calendar Schedule',
			items: [
				{fieldLabel: 'Schedule Name', name: 'name', xtype: 'textfield'},
				{fieldLabel: 'Schedule Type', name: 'calendarScheduleType.name', xtype: 'linkfield', height: 23, detailPageClass: 'Clifton.calendar.schedule.CalendarScheduleTypeWindow', detailIdField: 'calendarScheduleType.id'},
				{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
				{fieldLabel: 'Date', name: 'startDate', hidden: true, xtype: 'datefield', value: new Date().format('m/d/Y')},
				{fieldLabel: 'Frequency', name: 'frequency', hidden: true, xtype: 'textfield', value: 'ONCE'},
				{name: 'rollupCalendarSchedule', xtype: 'hidden', value: 'true'},
				{xtype: 'system-entity-modify-condition-combo'},

				{
					xtype: 'formgrid-scroll',
					title: 'Child Calendar Schedules',
					storeRoot: 'childCalendarScheduleList',
					dtoClass: 'com.clifton.calendar.schedule.CalendarSchedule',
					height: 180,
					heightResized: true,
					collapsible: false,
					viewConfig: {forceFit: true, emptyText: 'This Rollup Schedules currently has no child schedules', deferEmptyText: false},
					columnsConfig: [
						{header: 'Schedule', dataIndex: 'label', idDataIndex: 'id', editor: {xtype: 'combo', url: 'calendarScheduleListFind.json', displayField: 'label', detailPageClass: 'Clifton.calendar.schedule.ScheduleWindow'}}
					]
				}
			]
		}]
	}]
});
