Clifton.calendar.schedule.PreviewRecurrencesWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Preview Event Recurrences',
	iconCls: 'schedule',
	width: 350,
	height: 525,

	items: [{
		xtype: 'gridpanel',
		instructions: 'The selected schedule will generate occurrences on the dates listed below.',
		readOnly: true,
		loadURL: 'calendarScheduleOccurrences.json',
		appendStandardColumns: false,
		hideToolsMenu: true,
		getLoadParams: function() {
			return this.ownerCt.params;
		},
		columns: [
			{
				header: 'Date', renderer: function(val, metaData, record) {
					return TCG.renderDateTime(record.json);
				}
			}
		]
	}],
	setTitle: function() {
		const title = this.titlePrefix + ' - ' + this.title;
		TCG.Window.superclass.setTitle.call(this, title, this.iconCls);
	}
});
