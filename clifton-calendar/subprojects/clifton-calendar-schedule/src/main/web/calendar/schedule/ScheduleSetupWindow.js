Clifton.calendar.schedule.ScheduleSetupWindow = Ext.extend(TCG.app.Window, {
	title: 'Schedule Setup',
	iconCls: 'schedule',
	id: 'calendarScheduleSetupWindow',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Schedules',
				items: [{
					name: 'calendarScheduleListFind',
					xtype: 'gridpanel',
					instructions: 'A schedule defines custom periodic occurrences and can be used in various parts of the system. Schedules can be configured to skip weekends and holidays.',
					wikiPage: 'IT/Calendar+and+Scheduling',
					importTableName: 'CalendarSchedule',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Schedule Name', width: 120, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Schedule Type', width: 50, dataIndex: 'calendarScheduleType.name', filter: {type: 'combo', searchFieldName: 'calendarScheduleTypeId', url: 'calendarScheduleTypeListFind.json'}},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Modify Condition', width: 100, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyCondition'}},
						{header: 'Calendar', width: 70, dataIndex: 'calendar.label', filter: {searchFieldName: 'calendarName'}},
						{header: 'Business Day Convention', width: 50, dataIndex: 'businessDayConvention', filter: {type: 'list', options: TCG.swapArrayOfArrayElements(Clifton.calendar.BUSINESS_DAY_CONVENTIONS, 0, 1)}},
						{header: 'Frequency', width: 40, dataIndex: 'frequency', filter: {type: 'list', options: Clifton.calendar.SCHEDULE_FREQUENCIES}},
						{header: 'Rollup Schedule', width: 35, dataIndex: 'rollupCalendarSchedule', type: 'boolean', tooltip: 'A grouping of multiple standard schedules'},
						{header: 'System', width: 25, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('systemDefined', false);
						}
						return {};
					},
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								const t = rowItem.get('rollupCalendarSchedule');
								return (t) ? 'Rollup Calendar Schedule' : 'Standard Calendar';
							},
							items: [
								{text: 'Standard Calendar', iconCls: 'calendar', className: 'Clifton.calendar.schedule.ScheduleWindow'},
								{text: 'Rollup Calendar Schedule', iconCls: 'calendar', className: 'Clifton.calendar.schedule.RollupCalendarScheduleWindow'}
							]
						},
						getDefaultData: function(gridPanel) {
							return this.savedDefaultData;
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'calendarScheduleType', url: 'calendarScheduleTypeListFind.json?', limitRequestedProperties: false, width: 150, listWidth: 200, emptyText: '< Select Schedule Type >'}));
							const detailPageItems = [];
							Ext.each(this.detailPageClass.items, function(item) {
								const className = item.className ? item.className : this.getDetailPageClassByText(item.text);
								detailPageItems.push(Ext.apply(item, {
									scope: this,
									handler: function() {
										let calendarScheduleType = TCG.getChildByName(toolBar, 'calendarScheduleType');
										const calendarScheduleTypeId = calendarScheduleType.getValue();
										if (TCG.isBlank(calendarScheduleTypeId)) {
											TCG.showError('You must first select desired schedule type from the list.');
										}
										else {
											calendarScheduleType = calendarScheduleType.store.getById(calendarScheduleTypeId);
											this.savedDefaultData = {
												calendarScheduleType: calendarScheduleType.json
											};
											this.openDetailPage(className, gridPanel, null, null, item.text);
										}
									}
								}));
							}, this);
							toolBar.add({
								text: 'Add',
								tooltip: 'Create a new Calendar Schedule of selected type',
								iconCls: 'add',
								scope: this,
								menu: new Ext.menu.Menu({
									items: detailPageItems
								})
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Schedule Types',
				items: [{
					name: 'calendarScheduleTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all calendar schedule types defined in the system.',
					wikiPage: 'IT/Calendar+and+Scheduling',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Schedule Type Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'System Table', width: 80, dataIndex: 'systemTable.name'},
						{header: 'Time Supported', width: 40, dataIndex: 'timeSupported', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.calendar.schedule.CalendarScheduleTypeWindow'
					}
				}]
			}
		]
	}]
});
