Ext.ns('Clifton.calendar.schedule');

Clifton.calendar.SCHEDULE_FREQUENCIES = [['ONCE', 'ONCE'], ['HOURLY', 'HOURLY'], ['DAILY', 'DAILY'], ['WEEKLY', 'WEEKLY'], ['MONTHLY', 'MONTHLY'], ['MONTHLY_WEEKDAY', 'MONTHLY_WEEKDAY'], ['YEARLY', 'YEARLY']];

Clifton.calendar.schedule.PreviewToolbar = Ext.extend(Ext.Panel, {
	tbar: {
		xtype: 'container',
		layout: {
			type: 'vbox',
			pack: 'start',
			align: 'stretch'
		},
		height: 27 * 2,
		defaults: {flex: 1},
		items: [
			{
				xtype: 'toolbar',
				style: 'border-style: none; border: 0;',
				name: 'previewToolbar1',
				items: [
					{
						text: 'Preview Schedule Occurrences',
						tooltip: '',
						iconCls: 'run',
						handler: function() {
							const w = this.getWindow();
							const previewToolbar = this.getParentPanel().getTopToolbar();
							const startDate = TCG.getChildByName(this.ownerCt, 'startDate');
							const startTime = TCG.getChildByName(this.ownerCt, 'startTime');
							let formattedStartDate = '';
							if (TCG.isNotBlank(startDate.getValue())) {
								const dateAndTime = new Date(startDate.getValue());
								if (TCG.isNotBlank(startTime.getValue())) {
									const timeValues = startTime.getValue().split(':');
									dateAndTime.setHours(...timeValues);
								}
								formattedStartDate = dateAndTime.format('Y-m-d H:i:s');
							}
							const numberOfOccurrences = TCG.getChildByName(this.ownerCt, 'numberOfOccurrences').getValue();

							if (TCG.isBlank(numberOfOccurrences) || numberOfOccurrences === 0) {
								TCG.showError('Please enter number of occurrences to preview', 'No Occurrence Count');
								return;
							}

							const scheduleId = w.getMainForm().formValues.id;

							const params = {
								scheduleId: scheduleId,
								startDateTime: formattedStartDate,
								count: numberOfOccurrences,
								overrideCalendarIds: JSON.stringify([TCG.getChildByName(previewToolbar, 'calendarOverride').getValue()]),
								overrideCalendarListBeanId: TCG.getChildByName(previewToolbar, 'calendarListBean').getValue()
							};

							const scheduleName = w.getMainForm().formValues.name;
							const windowId = TCG.getComponentId('Clifton.calendar.schedule.PreviewRecurrencesWindow', scheduleId);

							TCG.createComponent('Clifton.calendar.schedule.PreviewRecurrencesWindow', {
								id: windowId,
								openerCt: w,
								params: params,
								title: scheduleName
							});
						},
						getWindow: function() {
							let result = this.findParentByType(Ext.Window);
							if (TCG.isNull(result)) {
								result = this.findParentBy(function(o) {
									return o.baseCls === 'x-window';
								});
							}
							return result;
						},
						getParentPanel: function() {
							let result = this.findParentByType(Ext.Panel);
							if (TCG.isNull(result)) {
								result = this.findParentBy(function(o) {
									return o.baseCls === 'formpanel';
								});
							}
							return result;
						}
					},
					{text: ' starting on ', xtype: 'tbtext'},
					{xtype: 'datefield', name: 'startDate', value: new Date(), width: 80},
					{text: ' at ', xtype: 'tbtext'},
					{xtype: 'timefield', name: 'startTime', format: 'H:i:s', value: '00:00:00', width: 80},
					{text: ' with ', xtype: 'tbtext'},
					{xtype: 'integerfield', name: 'numberOfOccurrences', minValue: -50, maxValue: 50, width: 30, allowBlank: false},
					{text: ' occurrences', xtype: 'tbtext'}
				]
			},
			{
				xtype: 'toolbar',
				style: 'background: repeat-x bottom right;',
				name: 'previewToolbar2',
				items: [
					'->',
					{text: ' with Calendar Override ', xtype: 'tbtext'},
					{
						xtype: 'combo', name: 'calendarOverride', displayField: 'label', url: 'calendarListFind.json', allowBlank: true, tooltip: 'Choose either a Calendar Override or a Calendar List Override',
						listeners: {
							change: function(combo, newValue, oldValue) {
								const listBeanCombo = TCG.getChildByName(this.ownerCt, 'calendarListBean');
								listBeanCombo.setDisabled(!TCG.isBlank(newValue));
							}
						}
					},
					{text: ' with Calendar List Override ', xtype: 'tbtext'},
					{
						xtype: 'combo', name: 'calendarListBean', displayField: 'label', url: 'systemBeanListFind.json?groupName=Calendar List Calculator', allowBlank: true, tooltip: 'Choose either a Calendar List Override or a Calendar Override',
						listeners: {
							change: function(combo, newValue, oldValue) {
								const calendarCombo = TCG.getChildByName(this.ownerCt, 'calendarOverride');
								calendarCombo.setDisabled(!TCG.isBlank(newValue));
							}
						}
					}
				]
			}
		]
	}
});
Ext.reg('schedule-preview-toolbar', Clifton.calendar.schedule.PreviewToolbar);
