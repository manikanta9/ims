#!/bin/bash

###
# Runs the selected conversion scripts to convert from the source database to the target database.
#
# Author: Mike Hill <mhill@paraport.com>
# Last Modified: 2021-10-14
###

# Error-handling
set -e;
trap 'echo "Script terminated." >&2' INT TERM;

# Constants
declare -r TRUE="true";
declare -r FALSE="false";

declare -r SOURCE_DIR="$(dirname "$0")";
declare -r SCRIPTS_DIR="${SOURCE_DIR}/scripts";
declare -r OUTPUT_DIR="${SOURCE_DIR}/output";

declare -r DEFAULT_SOURCE_HOST='localhost';
declare -r DEFAULT_SOURCE_PORT='1433';
declare -r DEFAULT_SOURCE_USERNAME='cliftonUser';
declare -r DEFAULT_SOURCE_PASSWORD='password';
declare -r DEFAULT_SOURCE_DATABASE='';
#declare -r DEFAULT_SOURCE_DATABASE='ParametricOMS';

# Example hosts:
# - Aurora MySQL OMS shared: invsys-aurora-mysql-provisioned-test-instance-1.ciwsvzyfnamt.us-east-1.rds.amazonaws.com
# - Aurora MySQL OMS temporary QA: invsys-aurora-mysql-provisioned-mhill-gabi-dan-qa-cluster.ciwsvzyfnamt.us-east-1.rds.amazonaws.com
declare -r DEFAULT_TARGET_HOST='';
declare -r DEFAULT_TARGET_PORT='3306';
declare -r DEFAULT_TARGET_USERNAME='cliftonUser';
declare -r DEFAULT_TARGET_PASSWORD='password';
declare -r DEFAULT_TARGET_DATABASE='dbo';

# Declare variables
declare -a stepsToExecute=();
declare -a stepCommands=();
declare -a stepDescriptions=();
declare forceExecution="${FALSE}";
declare reuseScripts="${FALSE}";
declare sourceHost="${DEFAULT_SOURCE_HOST}";
declare sourcePort="${DEFAULT_SOURCE_PORT}";
declare sourceUsername="${DEFAULT_SOURCE_USERNAME}";
declare sourcePassword="${DEFAULT_SOURCE_PASSWORD}";
declare sourceDatabase="${DEFAULT_SOURCE_DATABASE}";
# shellcheck disable=SC2153
declare targetHost="${DEFAULT_TARGET_HOST}";
declare targetPort="${DEFAULT_TARGET_PORT}";
declare targetUsername="${DEFAULT_TARGET_USERNAME}";
declare targetPassword="${DEFAULT_TARGET_PASSWORD}";
declare targetDatabase="${DEFAULT_TARGET_DATABASE}";

# Prints the usage information for this utility.
usage() {
	cat <<-EOF
		Runs the appropriate conversion scripts to convert the source database to the
		target database, including both the schema and data. This includes modifying
		the schema and the data to match opinionated internal conventions.

		Individual components of this script can be executed by using the -s flag to
		explicitly select steps. Short descriptions of each step are detailed below.

		Usage: $(basename "$0") [-rf] [-s <steps to execute>]
		        [-i <source host>] [-l <source port>] [-u <source username>] [-p <source password>]
		        [-L <target port>] [-U <target username>] [-P <target password>] [-D <target database>]
		        -d <source database> -I <target host>

		    -r
		        Reuse scripts from previous runs, rather than only executing scripts
		        generated during this run. This allows re-executing all generated
		        scripts from the previous run, which can be used to recreate the target
		        database without having to re-export the source database.

		    -f
		        Forces execution of the generated SQL scripts without prompt.

		    -s <steps to execute>
		        The comma-delimited list of steps to execute. By default, all steps will
		        be executed.

		    -i <source host>
		        The source database host.

		    -l <source port>
		        The source database port.

		    -u <source username>
		        The source database username.

		    -p <source password>
		        The source database password.

		    -d <source database>
		        The source database name.

		    -I <target host>
		        The target database host.

		    -L <target port>
		        The target database port.

		    -U <target username>
		        The target database username.

		    -P <target password>
		        The target database password.

		    -D <target database>
		        The target database name.

		    -h
		        Shows this help message.
	EOF
	echo;
	printSteps;
}

registerStep() {
	local description="$1";
	local commandName="$2";
	stepDescriptions+=( "${description}" );
	stepCommands+=( "${commandName}" );
}

executeStep() {
	local step="$1";
	local stepDescription="${stepDescriptions[${step}]}";
	if [[ "${step}" -lt 0 || "${step}" -ge "${#stepCommands[@]}" ]]; then
		echo "Step ${step} could not be found. Number of registered steps: ${#stepCommands[@]}" >&2;
		exit 1;
	fi
	echo "--- Executing step ${step}: ${stepDescription} ---";
	"${stepCommands[${step}]}";
	echo;
}

printSteps() {
	echo "Step descriptions:";
	for step in "${!stepCommands[@]}"; do
		echo "    ${step}: ${stepDescriptions[${step}]}";
	done
}

# Register steps
registerStep 'Create config file' 'createConfig';
createConfig() {
	mkdir -p "$(dirname "${configFile}")";
	"${SCRIPTS_DIR}/00-create-config.sh" \
		"${sourceHost}" \
		"${sourcePort}" \
		"${sourceUsername}" \
		"${sourcePassword}" \
		"${sourceDatabase}" \
		"${targetHost}" \
		"${targetPort}" \
		"${targetUsername}" \
		"${targetPassword}" \
		"${targetDatabase}" > "${configFile}";
}

registerStep 'Export schema' 'exportSchema';
exportSchema() {
	mkdir -p "$(dirname "${sourceSchemaFile}")";
	"${SCRIPTS_DIR}/01-export-schema.sh" "${configFile}" > "${sourceSchemaFile}";
}

registerStep 'Process schema' 'processSchema';
processSchema() {
	mkdir -p "$(dirname "${processedSchemaSqlFile}")";
	"${SCRIPTS_DIR}/02-process-schema.sh" "${sourceSchemaFile}" > "${processedSchemaSqlFile}";
	if ! "${reuseScripts}"; then
		sqlScripts+=( "${processedSchemaSqlFile}" );
	fi
}

registerStep 'Export data' 'exportData';
exportData() {
	rm -rf "${dataExportDir}";
	mkdir -p "${dataExportDir}";
	"${SCRIPTS_DIR}/03-export-data.sh" "${configFile}" "${dataExportDir}";
}

registerStep 'Prepare data upload' 'prepareDataUpload';
prepareDataUpload() {
	mkdir -p "$(dirname "${dataExportSqlFile}")";
	"${SCRIPTS_DIR}/04-prepare-data-upload.sh" "${dataExportDir}" > "${dataExportSqlFile}";
	if ! "${reuseScripts}"; then
		sqlScripts+=( "${dataExportSqlFile}" );
	fi
}

registerStep 'Execute SQL scripts' 'executeSql';
executeSql() {
	if [[ "${#sqlScripts[@]}" == 0 ]] ; then
		echo 'No SQL scripts to execute. Exiting.';
		exit;
	fi
	echo 'The following SQL scripts will be executed:';
	for script in "${sqlScripts[@]}"; do
		echo "- ${script}";
	done
	echo >&2;
	if ! "${forceExecution}"; then
		POSIXLY_CORRECT=1 read -rp 'Do you wish to continue? [y/N]: ' answer;
		echo;
		if [[ "${answer,,}" != 'y' ]]; then
			exit 1;
		fi
	fi
	"${SCRIPTS_DIR}/05-run-sql.sh" "${configFile}" "${sqlScripts[@]}";
}

# Check parameters
while getopts ":rfs:i:l:u:p:d:I:L:U:P:D:h" opt; do
	case "${opt}" in
	r)
		reuseScripts="${TRUE}";
		;;
	f)
		forceExecution="${TRUE}";
		;;
	s)
		readarray -td',' stepsToExecute < <(printf '%s' "${OPTARG}");
		;;
	i)
		sourceHost="${OPTARG}";
		;;
	l)
		sourcePort="${OPTARG}";
		;;
	u)
		sourceUsername="${OPTARG}";
		;;
	p)
		sourcePassword="${OPTARG}";
		;;
	d)
		sourceDatabase="${OPTARG}";
		;;
	I)
		targetHost="${OPTARG}";
		;;
	L)
		targetPort="${OPTARG}";
		;;
	U)
		targetUsername="${OPTARG}";
		;;
	P)
		targetPassword="${OPTARG}";
		;;
	D)
		targetDatabase="${OPTARG}";
		;;
	h)
		usage >&2;
		exit 0;
		;;
	\?)
		echo "Invalid option: -${OPTARG}" >&2;
		exit 1;
		;;
	:)
		echo "Option -${OPTARG} requires an argument." >&2;
		exit 1;
		;;
	esac
done

# Shift out parameters
shift "$((OPTIND - 1))";

# Validate parameters
: "${sourceDatabase:?'The source database must be non-empty.'}";
: "${targetHost:?'The target host must be non-empty.'}";
for step in "${stepsToExecute[@]}"; do
	# Use POSIX conditions to force numeric comparison
	if [ "${step}" -ne "${step}" ]; then
		echo "${step} is not a valid number for use with steps." >&2;
		exit 1;
	fi
done

# Prepare runtime variables
declare configFile="${OUTPUT_DIR}/${sourceDatabase}/config.sh";
declare sourceSchemaFile="${OUTPUT_DIR}/${sourceDatabase}/schema-original.sql";
declare processedSchemaSqlFile="${OUTPUT_DIR}/${sourceDatabase}/schema.sql";
declare dataExportDir="${OUTPUT_DIR}/${sourceDatabase}/data";
declare dataExportSqlFile="${OUTPUT_DIR}/${sourceDatabase}/data.sql";
declare -a sqlScripts=();

# Reuse existing scripts
if "${reuseScripts}"; then
	sqlScripts+=( "${processedSchemaSqlFile}" );
	sqlScripts+=( "${dataExportSqlFile}" );
fi

# Prepare and sort steps to execute
if [[ "${#stepsToExecute[@]}" == 0 ]]; then
	stepsToExecute=( "${!stepCommands[@]}" );
fi
# Always execute configuration generation
stepsToExecute+=( '0' );
readarray -td '' stepsToExecute < <(printf '%s\0' "${stepsToExecute[@]}" | sort -nz | uniq -z);
for step in "${stepsToExecute[@]}"; do
	if [[ -z "${stepCommands[${step}]}" ]]; then
		echo "Invalid step number: ${step}. Number of registered steps: ${#stepCommands[@]}" >&2;
		exit 1;
	fi
done

# Execute steps
for step in "${stepsToExecute[@]}"; do
	executeStep "${step}";
done
