# MSSQL to MySQL Converter

This project provides a set of opinionated scripts for converting a database
from MSSQL to MySQL. This is intended to be used with database sources using our
existing conventions, and can be used to generate a fully-compatible MySQL
database which follows similar conventions.

### Prerequisites

- Install AWS SCT:
  https://docs.aws.amazon.com/SchemaConversionTool/latest/userguide/CHAP_Installing.html#CHAP_Installing.Procedure
- Install sqlcmd and bcp for data exports:
  https://docs.microsoft.com/en-us/sql/tools/bcp-utility
- Install the MySQL client for remote data uploads:
  https://dev.mysql.com/downloads/shell/
- Install Cygwin for execution (Git Bash will likely work, but is not tested.
  These scripts will work in Unix-based environments as well, but likely require
  some small adjustments, particularly for paths):
  https://cygwin.com/install.html

### Usage

Run `./run.sh -h` for supported syntax. Each sub-script in the "scripts"
directory also supports the `-h` flag and can be run independently if desired.

This set of scripts works by first generating a configuration file, which stores
source and target database connection information, and then using that
configuration file and any other shared properties to call each sub-script in
sequence. The sequence of scripts may be customized by selecting individual
scripts via the `-s <comma-delimited steps>` parameter, which is only supported
by the main `run.sh` script.

#### Example Usage

```shell
# Print help information
./run.sh -h;

# Run steps 0 (configuration generation) and 1 (DDL export via AWS SCT) for
# CliftonIMS.
# Note: Step 0 is always run, even if not explicitly included, since
# configuration parameters are required for all operations
./run.sh -s 0,1 -i my-source-database.domain.com -l 1433 -u cliftonUser -p password -d CliftonIMS \
		-I my-target-database.domain.com -L 3306 -U cliftonUser -P password -D dbo;

# Several parameters are defaulted to match our conventions for local
# development and can be excluded from most runs

# Run a full schema and data migration for the CliftonIMS database
./run.sh -d CliftonIMS -I my-target-database.domain.com;

# Clear and rebuild the target database using the previously-generated scripts
./run.sh -rfs 5 -d CliftonIMS -I my-target-database.domain.com;

# Re-export data and use it to rebuild the target database without regenerating
# the schema
./run.sh -fs 3,4,5 -d CliftonIMS -I my-target-database.domain.com;
```
