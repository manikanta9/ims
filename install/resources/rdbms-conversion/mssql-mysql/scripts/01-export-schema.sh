#!/bin/bash
# shellcheck disable=SC2155

###
# Runs AWS SCT and exports the schema from the configured source database for
# use in the configured target database.
#
# Author: Mike Hill <mhill@paraport.com>
# Last Modified: 2021-10-12
###

# Error-handling
set -e;
trap 'echo "Script terminated." >&2' INT TERM;

# Constants
declare -r AWS_SCT_PATH="$(cygpath 'C:/Program Files/AWS Schema Conversion Tool')";
declare -r AWS_SCT_JAVA_PATH="${AWS_SCT_PATH}/runtime/bin/java.exe";
declare -r AWS_SCT_JAR="$(cygpath -w "${AWS_SCT_PATH}/app/AWSSchemaConversionToolBatch.jar")";
declare -r AWS_SCT_DEBUG_PORT='5005';
declare -r TRUE="true";
declare -r FALSE="false";

# Prints the usage information for this utility.
usage() {
	cat >&2 <<-EOF
		Runs the AWS SCT tool to export appropriately-converted DDL for the source and target database types.

		Usage:
		    $(basename "$0") [-d] [-i] [-j <Java binary>] [-o <output file>] <config file>
		    $(basename "$0") -h

		    -d
		        Enables debugging on port ${AWS_SCT_DEBUG_PORT}.

		    -i
		        Enables interactive mode. This will disable several other features, including script processing.

		    -j <Java binary>
		        Selects a specific Java binary to use to run AWS SCT. This may be required when enabling debugging.

		    -o <output file>
		        Writes the generated SQL script to the specified output file. By default, it will write to stdout.

		    -h
		        Shows this help message.

		    <config file>
		        The configuration file including host and target database information.
	EOF
}

# Declare variables
declare javaPath="${AWS_SCT_JAVA_PATH}";
declare interactive="${FALSE}";
declare outputFile;
declare outputStdout="${TRUE}";
declare -a jvmArgs=();

# Check parameters
while getopts ":dij:o:h" opt; do
	case "${opt}" in
	d)
		jvmArgs+=( "-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=*:${AWS_SCT_DEBUG_PORT}" );
		echo "Enabled debugging on port ${AWS_SCT_DEBUG_PORT}." >&2;
		;;
	i)
		interactive="${TRUE}";
		echo 'Interactive mode enabled.' >&2;
		;;
	j)
		javaPath="${OPTARG}";
		;;
	o)
		outputFile="${OPTARG}";
		outputStdout="${FALSE}";
		;;
	h)
		usage;
		exit 0;
		;;
	\?)
		echo "Invalid option: -${OPTARG}" >&2;
		exit 1;
		;;
	:)
		echo "Option -${OPTARG} requires an argument." >&2;
		exit 1;
		;;
	esac
done

# Shift out parameters
shift "$((OPTIND - 1))";

# Process inputs
configFile="${1:?A configuration file must be provided.}";

# Verify Java version
version="$("${javaPath}" -version 2>&1 | awk -F '"' '/version/ {print $2}')";
if [[ "${version}" == '11' || "${version}" == '11.'* ]]; then
	echo "Detected supported Java version: ${version} (${javaPath})." >&2;
else
	echo "Detected unsupported Java version: ${version} (${javaPath}). Please use Java 11." >&2;
	exit 1;
fi

# Prepare files
sctScript="$(mktemp)";
trap 'rm -f "${sctScript}"' INT TERM EXIT;
if "${outputStdout}"; then
	outputFile="$(mktemp)";
	trap 'rm -f "${outputFile}"' INT TERM EXIT;
	echo 'SQL script will be written to stdout.' >&2;
else
	echo "SQL script will be written to file ${outputFile}." >&2;
fi

# Set environment variables
# shellcheck disable=SC1090
source "${configFile}";
# shellcheck disable=SC1003
export sctOutputFile="$(cygpath -w "${outputFile}" | tr '\\' '/')";
# shellcheck disable=SC1003
export sctProjectDir="$(cygpath -w "${TEMP}" | tr '\\' '/')";

# Store SCT script
cat <<-'EOF' > "${sctScript}"
	# Create transient project for conversion
	CreateProject
		-name: 'Conversion Project'
		-directory: '${sctProjectDir}'
		-source: 'MSSQL'
		-target: 'MYSQL'
	/

	# Define source database
	ConnectSource
		-host: '${sourceHost}'
		-port: '${sourcePort}'
		-database: '${sourceDatabase}'
		-user: '${sourceUsername}'
		-password: '${sourcePassword}'
		-processMode: 'EXTENDED'
	/

	# Define target database
	ConnectTarget
		-host: '${targetHost}'
		-port: '${targetPort}'
		-user: '${targetUsername}'
		-password: '${targetPassword}'
		-processMode: 'EXTENDED'
	/

	# Create conversion rules
	CreateRuleMap
		-rules: '[{
			    "name": "Remove DB Name",
			    "object": "database",
			    "action": "remove_database_name",
			    "state": "enabled",
			    "databaseName": "%"
			}, {
				// The MySQL converter creates databases in the form "<source database>_<source schema>". Convert this to "<source schema>".
			    "name": "Schema Names",
			    "object": "schema",
			    "action": "remove_prefix",
			    "state": "enabled",
			    "databaseName": "%",
			    "schemaName": "%",
			    "targetValue": "${sourceDatabase}_"
			}, {
			// Our instances of objects named "SQL" have been removed
			//     "name": "Column: Reserved Name \"SQL\"",
			//     "object": "column",
			//     "action": "rename",
			//     "state": "enabled",
			//     "databaseName": "%",
			//     "schemaName": "%",
			//     "tableName": "%",
			//     "columnName": "SQL",
			//     "targetValue": "SQLStatement"
			// }, {
			    "name": "Bit Column Types",
			    "object": "column",
			    "action": "change_data_type",
			    "state": "enabled",
			    "databaseName": "%",
			    "schemaName": "%",
			    "tableName": "%",
			    "columnName": "%",
			    "oldDataType": "BIT",
			    "newDataType": "BIT"
			}]'
	/

	# Create conversion filter
	CreateFilter
		-name: 'SchemaFilter'
		-origin: 'source'
		-objects: '[{
				"type": "include",
				"treePath": "Databases.${sourceDatabase}.Schemas.dbo.Tables"
			}, {
				"type": "include",
				"treePath": "Databases.${sourceDatabase}.Schemas.env.Tables"
			}]'
	/

	# Convert schema
	Convert
		-filter: 'SchemaFilter'
	/

	# Save target SQL
	SaveTargetSQL
		-filter: 'SchemaFilter'
		-file: '${sctOutputFile}'
		-useSourceSide: 'true'
	/
	EOF

# Run SCT
if "${interactive}"; then
	"${javaPath}" "${jvmArgs[@]}" -jar "${AWS_SCT_JAR}" -type interactive -highlight yes;
else
	"${javaPath}" "${jvmArgs[@]}" -jar "${AWS_SCT_JAR}" -type scts -script "$(cygpath -w "${sctScript}")" >&2;
	if "${outputStdout}"; then
		cat "${outputFile}";
	fi
fi
