#!/bin/bash

###
# Creates a single script to upload the selected data using INSERT statements.
#
# Author: Mike Hill <mhill@paraport.com>
# Last Modified: 2021-10-14
###

# Error-handling
set -e;
trap 'echo "Script terminated." >&2' INT TERM;

# Constants
declare -r INPUT_DIR='./data';
declare -r OUTPUT_FILE='./output/data.sql';

# Prints the usage information for this utility.
usage() {
    cat <<-EOF
		Creates a single script to upload the selected data using INSERT statements.

		Usage:
		    $(basename "$0")
		    $(basename "$0") -h

		    -h
		        Shows this help message.
	EOF
}

# Check parameters
while getopts ":h" opt; do
    case "${opt}" in
    h)
        usage >&2;
        exit 0;
        ;;
    \?)
        echo "Invalid option: -${OPTARG}" >&2;
        exit 1;
        ;;
    :)
        echo "Option -${OPTARG} requires an argument." >&2;
        exit 1;
        ;;
    esac
done

# Create output file
rm -f "${OUTPUT_FILE}";
echo "Writing contents to file: ${OUTPUT_FILE}..." >&2;
{
	printf '%s\n\n' 'SET FOREIGN_KEY_CHECKS=0;';
	ls -1 "${INPUT_DIR}"/*.sql \
		| grep -vFx "${OUTPUT_FILE}" \
		| tr '\n' '\0' \
		| xargs -0 cat \
		| sed -r 's/(INSERT INTO )[^.]+\./\1/g' \
		| sed -r 's/\\/\\\\/g';
	printf '\n\n%s\n' 'SET FOREIGN_KEY_CHECKS=1;';
} > ${OUTPUT_FILE};
echo "Contents written to file: ${OUTPUT_FILE}" >&2;
