#!/bin/bash

###
# Creates a configuration file containing database information for consumption
# by related scripts.
#
# Author: Mike Hill <mhill@paraport.com>
# Last Modified: 2021-10-12
###

# Error-handling
set -e;
trap 'echo "Script terminated." >&2' INT TERM;

# Prints the usage information for this utility.
usage() {
	cat <<-EOF
		Creates a configuration file containing database information for consumption
		by related scripts.

		Usage:
		    $(basename "$0")
		            <source host> <source port> <source username> <source password> <source database>
		            <target host> <target port> <target username> <target password> <target database>
		    $(basename "$0") -h

		    -h
		        Shows this help message.
	EOF
}

# Check parameters
while getopts ":h" opt; do
	case "${opt}" in
	h)
		usage >&2;
		exit 0;
		;;
	\?)
		echo "Invalid option: -${OPTARG}" >&2;
		exit 1;
		;;
	:)
		echo "Option -${OPTARG} requires an argument." >&2;
		exit 1;
		;;
	esac
done

# Shift out parameters
shift "$((OPTIND - 1))";

# Assign and validate arguments
# Source
declare sourceHost="${1:?'No source host was provided.'}"; shift;
declare sourcePort="${1:?'No source port was provided.'}"; shift;
declare sourceUsername="${1:?'No source username was provided.'}"; shift;
declare sourcePassword="${1:?'No source password was provided.'}"; shift;
declare sourceDatabase="${1:?'No source database was provided.'}"; shift;

# Target
declare targetHost="${1:?'No target host was provided.'}"; shift;
declare targetPort="${1:?'No target port was provided.'}"; shift;
declare targetUsername="${1:?'No target username was provided.'}"; shift;
declare targetPassword="${1:?'No target password was provided.'}"; shift;
declare targetDatabase="${1:?'No target database was provided.'}"; shift;

if [[ "$#" -gt 0 ]]; then
	echo "Excess arguments were provided. Unexpected arguments: $*" >&2;
	exit 1;
fi

# Write config file
echo "Writing configuration to stdout." >&2;
cat <<-EOF
	export sourceHost='${sourceHost}';
	export sourcePort='${sourcePort}';
	export sourceUsername='${sourceUsername}';
	export sourcePassword='${sourcePassword}';
	export sourceDatabase='${sourceDatabase}';

	export targetHost='${targetHost}';
	export targetPort='${targetPort}';
	export targetUsername='${targetUsername}';
	export targetPassword='${targetPassword}';
	export targetDatabase='${targetDatabase}';
EOF
