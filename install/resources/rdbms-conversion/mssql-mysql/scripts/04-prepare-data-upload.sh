#!/bin/bash

###
# Creates a single script to upload the selected data using LOAD DATA LOCAL
# INFILE statements. These are typically much more performant than INSERT
# statements for large amounts of data over the wire, but require the source
# data to be in table format (e.g., TSV or CSV).
#
# Author: Mike Hill <mhill@paraport.com>
# Last Modified: 2021-10-14
###

# Error-handling
set -e;
trap 'echo "Script terminated." >&2' INT TERM;

# Prints the usage information for this utility.
usage() {
	cat <<-EOF
	    Creates a single script to upload the selected data using LOAD DATA LOCAL
	    INFILE statements. These are typically much more performant than INSERT
	    statements for large amounts of data over the wire, but require the source
	    data to be in table format (e.g., TSV or CSV).

	    Usage:
	        $(basename "$0") <data directory>
	        $(basename "$0") -h

	        -h
	            Shows this help message.

	        <data directory>
	            The directory containing source data in TSV format.
	EOF
}

# Declare variables
declare dataDir;

# Check parameters
while getopts ":h" opt; do
	case "${opt}" in
	h)
	    usage >&2;
	    exit 0;
	    ;;
	\?)
	    echo "Invalid option: -${OPTARG}" >&2;
	    exit 1;
	    ;;
	:)
	    echo "Option -${OPTARG} requires an argument." >&2;
	    exit 1;
	    ;;
	esac
done

# Process inputs
dataDir="${1:?A data directory must be provided.}";
if [[ ! -d "${dataDir}" ]]; then
	echo "${dataDir} does not exist or is not a directory." >&2;
	exit 1;
fi

# Create output file
echo "Writing import statements from tables in ${dataDir}/..." >&2;
cat <<-'EOF'
	\W -- Enable warnings
	SET FOREIGN_KEY_CHECKS=0;

	-- Truncate all tables
	USE `dbo`;
	DELIMITER $$
	DROP PROCEDURE IF EXISTS TRUNCATE_ALL_TABLES$$
	CREATE PROCEDURE `TRUNCATE_ALL_TABLES`()
	    LANGUAGE SQL
	    NOT DETERMINISTIC
	    READS SQL DATA
	BEGIN
	    DECLARE TRUNCATE_TABLE_SCHEMA_VAR VARCHAR(256);
	    DECLARE TRUNCATE_TABLE_NAME_VAR VARCHAR(256);

	    DECLARE done INT DEFAULT 0;
	    DECLARE truncate_table_cursor CURSOR FOR
	        SELECT t.table_schema, t.table_name
	        FROM INFORMATION_SCHEMA.TABLES t
	        WHERE t.table_schema IN ('dbo', 'env')
	            AND t.table_type <> 'VIEW';
	    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	    OPEN truncate_table_cursor;
	    truncate_table_cursor_loop: LOOP
	        FETCH truncate_table_cursor INTO
	            TRUNCATE_TABLE_SCHEMA_VAR,
	            TRUNCATE_TABLE_NAME_VAR;
	        IF done THEN
	            LEAVE truncate_table_cursor_loop;
	        END IF;

	        SET @truncate_table_statement = CONCAT('TRUNCATE TABLE ', TRUNCATE_TABLE_SCHEMA_VAR, '.', TRUNCATE_TABLE_NAME_VAR);
	        PREPARE stmt FROM @truncate_table_statement;
	        EXECUTE stmt;
	        DEALLOCATE PREPARE stmt;
	    END LOOP truncate_table_cursor_loop;
	END$$
	DELIMITER ;

	CALL TRUNCATE_ALL_TABLES;
	DROP PROCEDURE IF EXISTS TRUNCATE_ALL_TABLES;

	BEGIN;
EOF
dataDirMySql="$(cygpath -aw "${dataDir}" | sed 's/\\/\//g')";
(cd "${dataDir}" ; find . -name '*.tsv' -print0) |
	while IFS= read -r -d '' path; do
	    pathWithoutFile="${path%/*}";
	    schemaName="${pathWithoutFile##*/}";
	    # schemaName="$(time echo "${path}" | rev | cut -d'/' -f2 | rev)"; # Slow
	    pathFile="${path##*/}";
	    tableName="${pathFile%.*}";
	    # tableName="$(time basename -s .tsv "${path}")"; # Slow
	    echo "\! echo '--- Importing table: ${tableName} ---'";
	    echo "LOAD DATA LOCAL INFILE '${dataDirMySql}/${path}' INTO TABLE ${schemaName}.${tableName};";
	done
cat <<-'EOF'
	COMMIT;

	SET FOREIGN_KEY_CHECKS=1;

	-- Run foreign-key checks (source: https://stackoverflow.com/a/5977191/1941654)
	USE `dbo`;
	DELIMITER $$
	DROP PROCEDURE IF EXISTS ANALYZE_INVALID_FOREIGN_KEYS$$
	CREATE PROCEDURE `ANALYZE_INVALID_FOREIGN_KEYS`(
	        checked_database_name VARCHAR(64),
	        checked_table_name VARCHAR(64),
	        temporary_result_table ENUM('Y', 'N'))
	    LANGUAGE SQL
	    NOT DETERMINISTIC
	    READS SQL DATA
	BEGIN
	    DECLARE TABLE_SCHEMA_VAR VARCHAR(64);
	    DECLARE TABLE_NAME_VAR VARCHAR(64);
	    DECLARE COLUMN_NAME_VAR VARCHAR(64);
	    DECLARE CONSTRAINT_NAME_VAR VARCHAR(64);
	    DECLARE REFERENCED_TABLE_SCHEMA_VAR VARCHAR(64);
	    DECLARE REFERENCED_TABLE_NAME_VAR VARCHAR(64);
	    DECLARE REFERENCED_COLUMN_NAME_VAR VARCHAR(64);
	    DECLARE KEYS_SQL_VAR VARCHAR(1024);

	    DECLARE done INT DEFAULT 0;

	    DECLARE foreign_key_cursor CURSOR FOR
	        SELECT
	            `TABLE_SCHEMA`,
	            `TABLE_NAME`,
	            `COLUMN_NAME`,
	            `CONSTRAINT_NAME`,
	            `REFERENCED_TABLE_SCHEMA`,
	            `REFERENCED_TABLE_NAME`,
	            `REFERENCED_COLUMN_NAME`
	        FROM
	            information_schema.KEY_COLUMN_USAGE
	        WHERE
	            `CONSTRAINT_SCHEMA` LIKE checked_database_name AND
	            `TABLE_NAME` LIKE checked_table_name AND
	            `REFERENCED_TABLE_SCHEMA` IS NOT NULL;

	    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	    IF temporary_result_table = 'N' THEN
	        DROP TEMPORARY TABLE IF EXISTS INVALID_FOREIGN_KEYS;
	        DROP TABLE IF EXISTS INVALID_FOREIGN_KEYS;

	        CREATE TABLE INVALID_FOREIGN_KEYS(
	            `TABLE_SCHEMA` VARCHAR(64),
	            `TABLE_NAME` VARCHAR(64),
	            `COLUMN_NAME` VARCHAR(64),
	            `CONSTRAINT_NAME` VARCHAR(64),
	            `REFERENCED_TABLE_SCHEMA` VARCHAR(64),
	            `REFERENCED_TABLE_NAME` VARCHAR(64),
	            `REFERENCED_COLUMN_NAME` VARCHAR(64),
	            `INVALID_KEY_COUNT` INT,
	            `INVALID_KEY_SQL` VARCHAR(1024)
	        );
	    ELSEIF temporary_result_table = 'Y' THEN
	        DROP TEMPORARY TABLE IF EXISTS INVALID_FOREIGN_KEYS;
	        DROP TABLE IF EXISTS INVALID_FOREIGN_KEYS;

	        CREATE TEMPORARY TABLE INVALID_FOREIGN_KEYS(
	            `TABLE_SCHEMA` VARCHAR(64),
	            `TABLE_NAME` VARCHAR(64),
	            `COLUMN_NAME` VARCHAR(64),
	            `CONSTRAINT_NAME` VARCHAR(64),
	            `REFERENCED_TABLE_SCHEMA` VARCHAR(64),
	            `REFERENCED_TABLE_NAME` VARCHAR(64),
	            `REFERENCED_COLUMN_NAME` VARCHAR(64),
	            `INVALID_KEY_COUNT` INT,
	            `INVALID_KEY_SQL` VARCHAR(1024)
	        );
	    END IF;

	    OPEN foreign_key_cursor;
	    foreign_key_cursor_loop: LOOP
	        FETCH foreign_key_cursor INTO
	            TABLE_SCHEMA_VAR,
	            TABLE_NAME_VAR,
	            COLUMN_NAME_VAR,
	            CONSTRAINT_NAME_VAR,
	            REFERENCED_TABLE_SCHEMA_VAR,
	            REFERENCED_TABLE_NAME_VAR,
	            REFERENCED_COLUMN_NAME_VAR;
	        IF done THEN
	            LEAVE foreign_key_cursor_loop;
	        END IF;

	        SET @from_part = CONCAT('FROM ', '`', TABLE_SCHEMA_VAR, '`.`', TABLE_NAME_VAR, '`', ' AS REFERRING ',
	                                'LEFT JOIN `', REFERENCED_TABLE_SCHEMA_VAR, '`.`', REFERENCED_TABLE_NAME_VAR, '`', ' AS REFERRED ',
	                                'ON (REFERRING', '.`', COLUMN_NAME_VAR, '`', ' = ', 'REFERRED', '.`', REFERENCED_COLUMN_NAME_VAR, '`', ') ',
	                                'WHERE REFERRING', '.`', COLUMN_NAME_VAR, '`', ' IS NOT NULL ',
	                                'AND REFERRED', '.`', REFERENCED_COLUMN_NAME_VAR, '`', ' IS NULL');
	        SET @full_query = CONCAT('SELECT COUNT(*) ', @from_part, ' INTO @invalid_key_count;');
	        PREPARE stmt FROM @full_query;
	        EXECUTE stmt;

	        IF @invalid_key_count > 0 THEN
	            INSERT INTO
	                INVALID_FOREIGN_KEYS
	            SET
	                `TABLE_SCHEMA` = TABLE_SCHEMA_VAR,
	                `TABLE_NAME` = TABLE_NAME_VAR,
	                `COLUMN_NAME` = COLUMN_NAME_VAR,
	                `CONSTRAINT_NAME` = CONSTRAINT_NAME_VAR,
	                `REFERENCED_TABLE_SCHEMA` = REFERENCED_TABLE_SCHEMA_VAR,
	                `REFERENCED_TABLE_NAME` = REFERENCED_TABLE_NAME_VAR,
	                `REFERENCED_COLUMN_NAME` = REFERENCED_COLUMN_NAME_VAR,
	                `INVALID_KEY_COUNT` = @invalid_key_count,
	                `INVALID_KEY_SQL` = CONCAT('SELECT ',
	                                           'REFERRING.', '`', COLUMN_NAME_VAR, '` ', 'AS "Invalid: ', COLUMN_NAME_VAR, '", ',
	                                           'REFERRING.* ',
	                                           @from_part, ';');
	        END IF;
	        DEALLOCATE PREPARE stmt;
	    END LOOP foreign_key_cursor_loop;
	END$$
	DELIMITER ;

	\! echo 'Analyzing invalid foreign keys...'
	CALL ANALYZE_INVALID_FOREIGN_KEYS('%', '%', 'Y');
	DROP PROCEDURE IF EXISTS ANALYZE_INVALID_FOREIGN_KEYS;
	-- Print out additional foreign key violation details
	-- SELECT * FROM INVALID_FOREIGN_KEYS;

	DELIMITER $$
	DROP PROCEDURE IF EXISTS PRINT_INVALID_FOREIGN_KEYS$$
	CREATE PROCEDURE `PRINT_INVALID_FOREIGN_KEYS`()
	BEGIN
		IF ((SELECT COUNT(*) FROM INVALID_FOREIGN_KEYS) > 0) THEN
			SELECT CONCAT('- ', TABLE_NAME, ': ', INVALID_KEY_COUNT) AS 'Invalid key counts:' FROM INVALID_FOREIGN_KEYS;
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid foreign keys found.';
		END IF;
	END$$
	DELIMITER ;
	CALL PRINT_INVALID_FOREIGN_KEYS();
	DROP PROCEDURE IF EXISTS PRINT_INVALID_FOREIGN_KEYS;
	\! echo 'No invalid foreign keys detected.'
EOF
