#!/bin/bash

###
# Runs the provided SQL scripts against the configured target database.
#
# Author: Mike Hill <mhill@paraport.com>
# Last Modified: 2021-10-14
###

# Error-handling
set -e;
trap 'echo "Script terminated." >&2' INT TERM;

# Prints the usage information for this utility.
usage() {
	cat <<-EOF
		Runs the provided SQL scripts against the configured target database.

		Usage:
		    $(basename "$0") <config file> <script file...>
		    $(basename "$0") -h

		    -h
		        Shows this help message.

		    <config file>
		        The configuration file including host and target database information.

		    <script file...>
		        The scripts to execute against the target base.
	EOF
}

# Declare variables
declare configFile;

# Check parameters
while getopts ":h" opt; do
    case "${opt}" in
    h)
        usage >&2;
        exit 0;
        ;;
    \?)
        echo "Invalid option: -${OPTARG}" >&2;
        exit 1;
        ;;
    :)
        echo "Option -${OPTARG} requires an argument." >&2;
        exit 1;
        ;;
    esac
done

# Process inputs
configFile="${1:?A configuration file must be provided.}";
shift;

# Run scripts
# shellcheck disable=SC1090
source "${configFile}";
for scriptFile in "$@"; do
	echo "Running script: ${scriptFile}..." >&2;
	# shellcheck disable=SC2154
	mysql -h "${targetHost}" -P "${targetPort}" -u "${targetUsername}" --password="${targetPassword}" -D "${targetDatabase}" < "${scriptFile}";
done
echo 'Scripts completed.' >&2;
