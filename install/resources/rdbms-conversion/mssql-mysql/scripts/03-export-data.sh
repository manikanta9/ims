#!/bin/bash
# shellcheck disable=SC2155

###
# Exports all selected data from the given source database into TSV files for
# import into the target database.
#
# Author: Mike Hill <mhill@paraport.com>
# Last Modified: 2021-10-14
###

# Error-handling
set -e;
trap 'echo "Script terminated." >&2' INT TERM;

# Prints the usage information for this utility.
usage() {
	cat <<-EOF
		Exports all selected data from the given source database into TSV files for
		import into the target database.

		Usage:
		    $(basename "$0") <config file> <data output directory>
		    $(basename "$0") -h

		    -h
		        Shows this help message.

		    <config file>
		        The configuration file including host and target database information.

		    <data output directory>
		        The output directory to which data files should be exported.
	EOF
}

# Declare variables
declare configFile;
declare outputDir;

# Check parameters
while getopts ":h" opt; do
	case "${opt}" in
	h)
		usage >&2;
		exit 0;
		;;
	\?)
		echo "Invalid option: -${OPTARG}" >&2;
		exit 1;
		;;
	:)
		echo "Option -${OPTARG} requires an argument." >&2;
		exit 1;
		;;
	esac
done

# Process inputs
configFile="${1:?A configuration file must be provided.}";
outputDir="${2:?A data output directory must be provided.}";

# Retrieve table names
# shellcheck disable=SC1090
source "${configFile}";
dataSelectionQuery="$(cat <<-'EOF'
	SET NOCOUNT ON;
	--	SELECT QUOTENAME(DB_NAME()) + '.' + QUOTENAME(c.TABLE_SCHEMA) + '.' + QUOTENAME(c.TABLE_NAME)
	--		 , STRING_AGG(CASE
	--						  WHEN c.COLUMN_NAME = 'rv' THEN '0'
	--						  ELSE 'ISNULL(CAST(' + c.COLUMN_NAME + ' AS VARCHAR(MAX)),''\N'')'
	--					  END, ',')
	--					  WITHIN GROUP (ORDER BY c.ORDINAL_POSITION)
	--	FROM INFORMATION_SCHEMA.COLUMNS c
	--	WHERE c.TABLE_SCHEMA IN ('dbo', 'env')
	--	  AND c.TABLE_NAME NOT IN ('sysdiagrams')
	--	  AND c.COLUMN_NAME NOT IN ('rv')
	--	GROUP BY c.TABLE_SCHEMA, c.TABLE_NAME
	WITH --
		 Columns AS (
			 SELECT QUOTENAME(DB_NAME()) + '.' + QUOTENAME(c.TABLE_SCHEMA) + '.' + QUOTENAME(c.TABLE_NAME) AS FqTableName
				  , QUOTENAME(c.COLUMN_NAME)                                                               AS QuotedColumnName
				  , c.*
			 FROM INFORMATION_SCHEMA.COLUMNS c
               LEFT JOIN INFORMATION_SCHEMA.TABLES t ON c.TABLE_NAME = t.TABLE_NAME
			 WHERE t.TABLE_SCHEMA IN ('dbo', 'env')
			   AND t.TABLE_NAME NOT IN ('sysdiagrams')
               AND t.TABLE_TYPE <> 'VIEW'
		 ),
		 ModifiedColumnValues AS (
			 SELECT c.*
				  , CASE
						WHEN c.COLUMN_NAME = 'rv' THEN '0'
						ELSE c.QuotedColumnName
					END AS NormalizedColumn1
			 FROM Columns c
		 ),
		 NormalizedColumnDataTypes AS (
			 SELECT c.*
				  , CASE
						WHEN c.DATA_TYPE = 'DATETIME' THEN 'CONVERT(VARCHAR(MAX),' + c.NormalizedColumn1 + ',121)'
						-- WHEN c.DATA_TYPE = 'BIT' THEN 'IIF(' + c.NormalizedColumn1 + '=1,''TRUE'',''FALSE'')'
						ELSE 'CONVERT(VARCHAR(MAX),' + c.NormalizedColumn1 + ')'
					END AS NormalizedColumn2
			 FROM ModifiedColumnValues c
		 ),
		 SpecialCharacterColumns AS (
			 SELECT c.*
				  , 'REPLACE(REPLACE(REPLACE(REPLACE(' + c.NormalizedColumn2 + ', ''\'', ''\\''), CHAR(9), ''\t''), CHAR(10), ''\n''), CHAR(13), ''\r'')' AS NormalizedColumn3
			 FROM NormalizedColumnDataTypes c
		 ),
		 NullSafeColumns AS (
			 SELECT c.*
				  , CASE
						WHEN c.IS_NULLABLE = 'YES' THEN 'ISNULL(' + c.NormalizedColumn3 + ',''\N'')'
						ELSE c.NormalizedColumn3
					END AS NormalizedColumn4
			 FROM SpecialCharacterColumns c
		 )
	SELECT c.FqTableName
		 , 'SELECT ' +
		   STRING_AGG(CAST(c.NormalizedColumn4 AS NVARCHAR(MAX)) + ' AS ' + QUOTENAME(c.COLUMN_NAME), ',') WITHIN GROUP (ORDER BY c.ORDINAL_POSITION) +
		   ' FROM ' +
		   c.FqTableName
	FROM NullSafeColumns c
	GROUP BY c.FqTableName
EOF
)";
# shellcheck disable=SC2154
readarray -t tableData < <(
	sqlcmd \
		-S "tcp:${sourceHost},${sourcePort}" -U "${sourceUsername}" -P "${sourcePassword}" -d "${sourceDatabase}" \
		-y0 -b -s $'\t' -Q "${dataSelectionQuery}" | dos2unix
	);
echo "Number of tables to export: ${#tableData[@]}" >&2;

# Place queries in temporary files to prevent xargs line-length limitations
tempDir="$(mktemp -d)";
declare -a tableFiles=();
trap 'rm -rf ${tempDir}' INT TERM EXIT;
for tableInfo in "${tableData[@]}"; do
	tableFile="${tempDir}/${#tableFiles[@]}";
	tableFiles+=( "${tableFile}" );
	echo "${tableInfo}" > "${tableFile}";
done

# Prepare output directory
rm -rf "${outputDir}";
mkdir -p "${outputDir}";

# Execute exports
exportData() {
	local tempOutFile="$(mktemp)";
	local -a tableInfo;
	readarray -d $'\t' -t tableInfo < "$1";
	local tablePath="${tableInfo[0]}";
	local tableQuery="${tableInfo[1]}";
	local tablePathNoBrackets="${tablePath//[\[\]]/}";
	local tablePathSlashes="${tablePathNoBrackets//./\/}";
	local tableDir="${tablePathSlashes%/*}";
	local tableName="${tablePathSlashes##*/}";
	mkdir -p "${outputDir}/${tableDir}";
	echo "Exporting: ${tablePath}" >&2;
	local outputFile="${outputDir}/${tableDir}/${tableName}.tsv";
	bcp "${tableQuery}" queryout "${outputFile}" -S "${sourceHost},${sourcePort}" -U "${sourceUsername}" -P "${sourcePassword}" -d "${sourceDatabase}" -k -T -c -C 65001 > "${tempOutFile}";
	dos2unix "${outputFile}" &> /dev/null;
	local errorCode="$?";
	if [[ "${errorCode}" -ne 0 ]]; then
		echo "Error: ${tablePath} (exit code: ${errorCode})" >&2;
		cat "${tempOutFile}" >&2;
	fi
	rm -f "${tempOutFile}";
}
export -f exportData;
export outputDir;
printf '%s\0' "${tableFiles[@]}" | xargs -n1 -0 -P8 bash -c 'exportData "$@"' _;
