import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant


val sharedVariant = registerVariant("shared", upstreamForMain = true)


dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-calendar"))
	api(project(":clifton-calendar:clifton-calendar-schedule")) { usingVariant("api") }
	api(project(":clifton-system:clifton-system-note"))
	api(project(":clifton-system:clifton-system-priority"))
	api(project(":clifton-websocket"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-calendar:clifton-calendar-schedule")))
	testFixturesApi(testFixtures(project(":clifton-system:clifton-system-note")))
	testFixturesApi(testFixtures(project(":clifton-websocket")))
}
