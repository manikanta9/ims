package com.clifton.workflow.definition;

public class WorkflowBuilder {

	private Workflow workflow;


	private WorkflowBuilder(Workflow workflow) {
		this.workflow = workflow;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static WorkflowBuilder createEmpty() {
		return new WorkflowBuilder(new Workflow());
	}


	public static WorkflowBuilder createTrade() {
		Workflow workflow = new Workflow();
		workflow.setId((short) 6);
		workflow.setName("Trade Workflow");
		workflow.setDescription("Most common trade workflow that supports full trade life cycle: compliance, approvals, FIX integration, allocations and booking.\n" +
				"\n" +
				"NOTE: Workflow State ORDER is important as the \"minimum\" trade workflow for trades associated with a group can rolled up to the group.");
		workflow.setActive(true);

		return new WorkflowBuilder(workflow);
	}


	public static WorkflowBuilder createRelationshipStatus() {
		Workflow workflow = new Workflow();
		workflow.setId((short) 1);
		workflow.setName("Client Relationship Status");
		workflow.setDescription("The workflow used to manage client relationship life cycles.");
		workflow.setActive(true);

		return new WorkflowBuilder(workflow);
	}


	public static WorkflowBuilder createInvestmentAccountStatus() {
		Workflow workflow = new Workflow();

		workflow.setId((short) 4);
		workflow.setName("Investment Account Status - Clifton Accounts");
		workflow.setDescription("The workflow used to manage investment accounts that are flagged as \"our\" accounts.\n" +
				"\n" +
				"NOTE: Workflow State ORDER is important as the \"minimum\" account workflow for accounts associated with a Client is rolled up as a field on the Client.  i.e. Active should always be 1, so that as long as a Client has at least one active account it shows as active.  Terminated would always be last so that in order for Terminated to rollup, ALL accounts would have to be in Terminated state.");
		workflow.setActive(true);

		return new WorkflowBuilder(workflow);
	}


	public static WorkflowBuilder createInvestmentAccountStatusNonCliftonAccounts() {
		Workflow workflow = new Workflow();

		workflow.setId((short) 5);
		workflow.setName("Investment Account Status - Non-Clifton Accounts");
		workflow.setDescription("The workflow used to manage investment accounts that are NOT flagged as \"our\" accounts.");
		workflow.setActive(true);

		return new WorkflowBuilder(workflow);
	}


	public static WorkflowBuilder createBusinessContractStatus() {
		Workflow workflow = new Workflow();

		workflow.setId((short) 3);
		workflow.setName("Business Contract Status");
		workflow.setDescription("The workflow used to manage client contract's life cycle. This workflow is used for contracts that need to go through approval processes and revisions, i.e. IMAs");
		workflow.setActive(true);

		return new WorkflowBuilder(workflow);
	}


	public static WorkflowBuilder createInvestmentInstructionDefinitionStatus() {
		Workflow workflow = new Workflow();

		workflow.setId((short) 23);
		workflow.setName("Investment Instruction Definition Status");
		workflow.setDescription("The workflow used to manage investment instruction definition life cycles.");
		workflow.setActive(true);

		return new WorkflowBuilder(workflow);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowBuilder withId(short id) {
		getWorkflow().setId(id);
		return this;
	}


	public WorkflowBuilder withName(String name) {
		getWorkflow().setName(name);
		return this;
	}


	public WorkflowBuilder withDescription(String description) {
		getWorkflow().setDescription(description);
		return this;
	}


	public WorkflowBuilder withActive(boolean active) {
		getWorkflow().setActive(active);
		return this;
	}


	public Workflow toWorkflow() {
		return this.workflow;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Workflow getWorkflow() {
		return this.workflow;
	}
}
