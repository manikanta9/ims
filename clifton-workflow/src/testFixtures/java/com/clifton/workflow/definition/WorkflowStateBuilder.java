package com.clifton.workflow.definition;


import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionBuilder;


public class WorkflowStateBuilder {

	private final WorkflowState workflowState;


	private WorkflowStateBuilder(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static WorkflowStateBuilder createEmpty() {
		return new WorkflowStateBuilder(new WorkflowState());
	}


	public static WorkflowStateBuilder createExecution() {
		WorkflowState workflowState = new WorkflowState();
		workflowState.setId((short) 31);
		workflowState.setWorkflow(WorkflowBuilder.createTrade().toWorkflow());
		workflowState.setStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		workflowState.setEntityUpdateCondition(SystemConditionBuilder.createActiveExecTradeModify().toSystemCondition());
		workflowState.setName("Execution");
		workflowState.setDescription("The trade was sent for execution using FIX and is currently in the process of being filled.");
		workflowState.setOrder(310);

		return new WorkflowStateBuilder(workflowState);
	}


	public static WorkflowStateBuilder createBooked() {
		WorkflowState workflowState = new WorkflowState();

		workflowState.setId((short) 34);
		workflowState.setWorkflow(WorkflowBuilder.createTrade().toWorkflow());
		workflowState.setStatus(WorkflowStatusBuilder.createClosed().toWorkflowStatus());
		workflowState.setEntityUpdateCondition(SystemConditionBuilder.createReadOnlyEntity().toSystemCondition());
		workflowState.setName("Booked");
		workflowState.setDescription("The trade was booked and posted to the General Ledger.");
		workflowState.setOrder(400);

		return new WorkflowStateBuilder(workflowState);
	}


	public static WorkflowStateBuilder createUnBooked() {
		WorkflowState workflowState = new WorkflowState();

		workflowState.setId((short) 36);
		workflowState.setWorkflow(WorkflowBuilder.createTrade().toWorkflow());
		workflowState.setStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		workflowState.setEntityUpdateCondition(SystemConditionBuilder.createReadOnlyEntity().toSystemCondition());
		workflowState.setName("Unbooked");
		workflowState.setDescription("The trade was unposted from the General Ledger and can be adjusted before reposting.");
		workflowState.setOrder(350);

		return new WorkflowStateBuilder(workflowState);
	}


	public static WorkflowStateBuilder createActive() {
		WorkflowState workflowState = new WorkflowState();

		workflowState.setId((short) 3);
		workflowState.setWorkflow(WorkflowBuilder.createRelationshipStatus().toWorkflow());
		workflowState.setStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		workflowState.setEntityUpdateCondition(SystemConditionBuilder.createTerminateDateNull().toSystemCondition());
		workflowState.setName("Active");
		workflowState.setDescription("An Active Client is one where we are currently earning revenue which will be billed at the end of the current quarter.");

		return new WorkflowStateBuilder(workflowState);
	}


	public static WorkflowStateBuilder createActiveClient() {
		WorkflowState workflowState = new WorkflowState();

		workflowState.setId((short) 19);
		workflowState.setWorkflow(WorkflowBuilder.createInvestmentAccountStatus().toWorkflow());
		workflowState.setStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		workflowState.setName("Active");
		workflowState.setDescription("An Active Client is one where we are currently earning revenue which will be billed at the end of the current quarter.");
		workflowState.setOrder(1);

		return new WorkflowStateBuilder(workflowState);
	}


	public static WorkflowStateBuilder createActiveAccount() {
		WorkflowState workflowState = new WorkflowState();

		workflowState.setId((short) 22);
		workflowState.setWorkflow(WorkflowBuilder.createInvestmentAccountStatusNonCliftonAccounts().toWorkflow());
		workflowState.setStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		workflowState.setName("Active");
		workflowState.setDescription("Account is active");

		return new WorkflowStateBuilder(workflowState);
	}


	public static WorkflowStateBuilder createPublished() {
		WorkflowState workflowState = new WorkflowState();

		workflowState.setId((short) 15);
		workflowState.setWorkflow(WorkflowBuilder.createBusinessContractStatus().toWorkflow());
		workflowState.setStatus(WorkflowStatusBuilder.createFinal().toWorkflowStatus());
		workflowState.setName("Published");
		workflowState.setDescription("The Contract has been approved by all parties and the final version has been published.");

		return new WorkflowStateBuilder(workflowState);
	}


	public static WorkflowStateBuilder createExecuted() {
		WorkflowState workflowState = new WorkflowState();

		workflowState.setId((short) 33);
		workflowState.setWorkflow(WorkflowBuilder.createTrade().toWorkflow());
		workflowState.setStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		workflowState.setEntityUpdateCondition(SystemConditionBuilder.createExecutedTradeModify().toSystemCondition());
		workflowState.setName("Executed");
		workflowState.setDescription("The trade was executed and allocated. It's waiting to be booked.");
		workflowState.setOrder(320);

		return new WorkflowStateBuilder(workflowState);
	}


	public static WorkflowStateBuilder createActiveInstruction() {
		WorkflowState workflowState = new WorkflowState();

		workflowState.setId((short) 186);
		workflowState.setWorkflow(WorkflowBuilder.createInvestmentInstructionDefinitionStatus().toWorkflow());
		workflowState.setStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		workflowState.setName("Active");
		workflowState.setDescription("Active instruction.");

		return new WorkflowStateBuilder(workflowState);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowStateBuilder withId(Short id) {
		getWorkflowState().setId(id);
		return this;
	}


	public WorkflowStateBuilder withName(String name) {
		getWorkflowState().setName(name);
		return this;
	}


	public WorkflowStateBuilder withDescription(String description) {
		getWorkflowState().setDescription(description);
		return this;
	}


	public WorkflowStateBuilder withWorkflow(Workflow workflow) {
		getWorkflowState().setWorkflow(workflow);
		return this;
	}


	public WorkflowStateBuilder withWorkflowStatus(WorkflowStatus workflowStatus) {
		getWorkflowState().setStatus(workflowStatus);
		return this;
	}


	public WorkflowStateBuilder withEntityModifyCondition(SystemCondition entityModifyCondition) {
		getWorkflowState().setEntityUpdateCondition(entityModifyCondition);
		return this;
	}


	public WorkflowState toWorkflowState() {
		return this.workflowState;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private WorkflowState getWorkflowState() {
		return this.workflowState;
	}
}
