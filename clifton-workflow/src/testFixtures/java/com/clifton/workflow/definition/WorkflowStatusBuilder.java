package com.clifton.workflow.definition;

public class WorkflowStatusBuilder {

	private WorkflowStatus workflowStatus;


	private WorkflowStatusBuilder(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static WorkflowStatusBuilder createEmpty() {
		return new WorkflowStatusBuilder(new WorkflowStatus());
	}


	public static WorkflowStatusBuilder createClosed() {
		WorkflowStatus workflowStatus = new WorkflowStatus();

		workflowStatus.setId((short) 2);
		workflowStatus.setName(WorkflowStatus.STATUS_CLOSED);
		workflowStatus.setDescription(WorkflowStatus.STATUS_CLOSED);

		return new WorkflowStatusBuilder(workflowStatus);
	}


	public static WorkflowStatusBuilder createActive() {
		WorkflowStatus workflowStatus = new WorkflowStatus();

		workflowStatus.setId((short) 3);
		workflowStatus.setName(WorkflowStatus.STATUS_ACTIVE);
		workflowStatus.setDescription(WorkflowStatus.STATUS_ACTIVE);

		return new WorkflowStatusBuilder(workflowStatus);
	}


	public static WorkflowStatusBuilder createPending() {
		WorkflowStatus workflowStatus = new WorkflowStatus();

		workflowStatus.setId((short) 5);
		workflowStatus.setName(WorkflowStatus.STATUS_PENDING);
		workflowStatus.setDescription(WorkflowStatus.STATUS_PENDING);

		return new WorkflowStatusBuilder(workflowStatus);
	}


	public static WorkflowStatusBuilder createFinal() {
		WorkflowStatus workflowStatus = new WorkflowStatus();

		workflowStatus.setId((short) 6);
		workflowStatus.setName("Final");
		workflowStatus.setDescription("Final");

		return new WorkflowStatusBuilder(workflowStatus);
	}


	public static WorkflowStatusBuilder createApproved() {
		WorkflowStatus workflowStatus = new WorkflowStatus();

		workflowStatus.setId((short) 8);
		workflowStatus.setName(WorkflowStatus.STATUS_APPROVED);
		workflowStatus.setDescription(WorkflowStatus.STATUS_APPROVED);

		return new WorkflowStatusBuilder(workflowStatus);
	}


	public static WorkflowStatusBuilder createDraft() {
		WorkflowStatus workflowStatus = new WorkflowStatus();

		workflowStatus.setId((short) 9);
		workflowStatus.setName(WorkflowStatus.STATUS_DRAFT);
		workflowStatus.setDescription(WorkflowStatus.STATUS_DRAFT);

		return new WorkflowStatusBuilder(workflowStatus);
	}


	public static WorkflowStatusBuilder createActiveAccount() {
		WorkflowStatus workflowStatus = new WorkflowStatus();

		workflowStatus.setId((short) 19);
		workflowStatus.setName("Active");
		workflowStatus.setDescription("Account is active\n" +
				"\n" +
				"Order should always be 1 for Active - used for Client \"Account Workflow State\" rollup (Client gets the min from the Accounts) - as long as 1 Active account, it's active");

		return new WorkflowStatusBuilder(workflowStatus);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowStatusBuilder withId(Short id) {
		getWorkflowStatus().setId(id);
		return this;
	}


	public WorkflowStatusBuilder withName(String name) {
		getWorkflowStatus().setName(name);
		return this;
	}


	public WorkflowStatusBuilder withDescription(String description) {
		getWorkflowStatus().setDescription(description);
		return this;
	}


	public WorkflowStatus toWorkflowStatus() {
		return this.workflowStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}
}
