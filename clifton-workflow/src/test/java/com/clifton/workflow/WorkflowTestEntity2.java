package com.clifton.workflow;


/**
 * The <code>WorkflowTestEntity2</code> class represents a sample DTO that supports workflows.
 *
 * @author vgomelsky
 */
public class WorkflowTestEntity2 extends BaseWorkflowAwareEntity<Integer> {

	// empty for now
}
