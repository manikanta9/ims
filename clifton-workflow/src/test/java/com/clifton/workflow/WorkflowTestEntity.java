package com.clifton.workflow;


/**
 * The <code>WorkflowTestEntity</code> class represents a sample DTO that supports workflows.
 *
 * @author vgomelsky
 */
public class WorkflowTestEntity extends BaseWorkflowAwareEntity<Integer> {

	private String comment;


	public String getComment() {
		return this.comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}
}
