package com.clifton.workflow.comparison;


import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.workflow.BaseWorkflowAwareEntity;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import com.clifton.workflow.history.WorkflowHistoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class WorkflowComparisonTests {

	WorkflowHistoryService workflowHistoryService;
	WorkflowAware wBean;


	@BeforeEach
	public void setup() {
		this.workflowHistoryService = new WorkflowHistoryServiceMock();
		this.wBean = new BaseWorkflowAwareEntity<Integer>();
	}


	@SuppressWarnings("rawtypes")
	static class WorkflowHistoryServiceMock extends WorkflowHistoryServiceImpl {

		@SuppressWarnings("unused")
		@Override
		public List<WorkflowHistory> getWorkflowHistoryListByWorkflowAwareEntity(WorkflowAware dtoInstance, boolean ascending) {
			List<WorkflowHistory> history = new ArrayList<>();

			WorkflowHistory wh1 = new WorkflowHistory();
			WorkflowHistory wh2 = new WorkflowHistory();
			WorkflowHistory wh3 = new WorkflowHistory();
			WorkflowHistory wh4 = new WorkflowHistory();
			WorkflowHistory wh5 = new WorkflowHistory();

			WorkflowStatus st1 = new WorkflowStatus();
			WorkflowStatus st2 = new WorkflowStatus();
			WorkflowStatus st3 = new WorkflowStatus();
			st1.setName("Open");
			st2.setName("Pending");
			st3.setName("Closed");

			WorkflowState ws1 = new WorkflowState();
			WorkflowState ws2 = new WorkflowState();
			WorkflowState ws3 = new WorkflowState();
			WorkflowState ws4 = new WorkflowState();

			ws1.setName("A");
			ws1.setStatus(st1);
			ws2.setName("B");
			ws2.setStatus(st1);
			ws3.setName("B");
			ws3.setStatus(st2);
			ws4.setName("C");
			ws4.setStatus(st3);

			wh1.setEndWorkflowState(ws1);
			wh2.setEndWorkflowState(ws2);
			wh3.setEndWorkflowState(ws3);
			wh4.setEndWorkflowState(ws4);
			wh5.setEndWorkflowState(ws3);

			// History is expected in descending order
			history.add(wh5);
			history.add(wh4);
			history.add(wh3);
			history.add(wh2);
			history.add(wh1);

			return history;
		}
	}


	@Test
	public void testMinStateVisited() {
		WorkflowStatePreviouslyVisitedComparison workflowStateComparison = new WorkflowStatePreviouslyVisitedComparison();
		workflowStateComparison.setWorkflowHistoryService(this.workflowHistoryService);
		workflowStateComparison.setMinVisitedCount(0);
		workflowStateComparison.setStateName("NO");

		validateComparisonResult(workflowStateComparison, true, null);

		workflowStateComparison.setMinVisitedCount(1);
		validateComparisonResult(workflowStateComparison, false, null);

		workflowStateComparison.setStateName("A");
		validateComparisonResult(workflowStateComparison, true, null);
	}


	@Test
	public void testRangeStateVisited() {
		WorkflowStatePreviouslyVisitedComparison workflowStateComparison = new WorkflowStatePreviouslyVisitedComparison();
		workflowStateComparison.setWorkflowHistoryService(this.workflowHistoryService);
		workflowStateComparison.setMinVisitedCount(0);
		workflowStateComparison.setMaxVisitedCount(2);
		workflowStateComparison.setStateName("A");

		validateComparisonResult(workflowStateComparison, true, null);

		workflowStateComparison.setStateName("B");
		validateComparisonResult(workflowStateComparison, false, "(The state [B] has been visited 3 times and the max visit count is 2)");

		workflowStateComparison.setMaxVisitedCount(3);
		validateComparisonResult(workflowStateComparison, true, "(The state [B] has been visited 3 times and the max visit count is 3)");

		workflowStateComparison.setStateName("NO");
		validateComparisonResult(workflowStateComparison, true, null);

		workflowStateComparison.setMinVisitedCount(1);
		validateComparisonResult(workflowStateComparison, false, null);
	}


	@Test
	public void testStateVisited_SincePreviousState() {
		// A Open -> B Open -> B Pending -> C Closed -> B Pending
		WorkflowStatePreviouslyVisitedComparison workflowStateComparison = new WorkflowStatePreviouslyVisitedComparison();
		workflowStateComparison.setWorkflowHistoryService(this.workflowHistoryService);
		workflowStateComparison.setMinVisitedCount(1);
		workflowStateComparison.setStateName("B");
		workflowStateComparison.setVisitedSinceStateName("C");

		validateComparisonResult(workflowStateComparison, true, "(The state [B] has been visited 1 times since State [C])");

		workflowStateComparison.setStateName("A");
		validateComparisonResult(workflowStateComparison, false, "(The state [A] has not been visited since State [C])");

		workflowStateComparison.setStateName("NO");
		validateComparisonResult(workflowStateComparison, false, "(The state [NO] has not been visited since State [C])");

		workflowStateComparison.setStateName("B");
		workflowStateComparison.setVisitedSinceStateName("A");

		validateComparisonResult(workflowStateComparison, true, "(The state [B] has been visited 3 times since State [A])");

		workflowStateComparison.setVisitedSinceStateName(null);
		workflowStateComparison.setVisitedSinceStatusName("Closed");
		workflowStateComparison.setStateName("B");
		validateComparisonResult(workflowStateComparison, true, "(The state [B] has been visited 1 times since Status [Closed])");

		workflowStateComparison.setStateName("A");
		validateComparisonResult(workflowStateComparison, false, "(The state [A] has not been visited since Status [Closed])");

		// Change so comparison returns true
		workflowStateComparison.setMaxVisitedCount(0);
		workflowStateComparison.setMinVisitedCount(0);
		validateComparisonResult(workflowStateComparison, true, "(The state [A] has not been visited since Status [Closed] and the max visit count is 0)");
	}


	/**
	 * @param expectedMessage - if blank then we don't validate the message, just the result
	 */
	private void validateComparisonResult(WorkflowStatePreviouslyVisitedComparison workflowStateComparison, boolean expectedResult, String expectedMessage) {
		// Run Validation
		workflowStateComparison.validate();
		SimpleComparisonContext comparisonContext = new SimpleComparisonContext();
		boolean result = workflowStateComparison.evaluate(this.wBean, comparisonContext);
		String message = (result ? comparisonContext.getTrueMessage() : comparisonContext.getFalseMessage());
		Assertions.assertEquals(expectedResult, result, "Expected " + expectedResult + (expectedMessage == null ? " " : " with a message of " + expectedMessage) + "but received a result of " + result + " with a message of " + message);
		if (expectedMessage != null) {
			Assertions.assertEquals(expectedMessage, message);
		}
	}
}
