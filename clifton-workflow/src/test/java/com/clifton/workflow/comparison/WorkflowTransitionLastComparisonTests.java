package com.clifton.workflow.comparison;


import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.workflow.WorkflowTestEntity;
import com.clifton.workflow.history.WorkflowHistoryService;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration(locations = "classpath:com/clifton/workflow/transition/WorkflowTransitionTests-context.xml")
@ExtendWith(SpringExtension.class)
public class WorkflowTransitionLastComparisonTests {

	@Resource
	WorkflowHistoryService workflowHistoryService;

	@Resource
	WorkflowTransitionService workflowTransitionService;

	@Resource
	private UpdatableDAO<WorkflowTestEntity> workflowTestEntityDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testWorkflowTransitionLastComparison() {

		WorkflowTransitionLastComparison equalComparison = setupComparison();
		equalComparison.setTransitionName("Validate");

		WorkflowTransitionLastComparison notEqualComparison = setupComparison();
		notEqualComparison.setTransitionName("Put On Hold");
		notEqualComparison.setTrueIfNotEquals(true);

		// Entity 1 - only had initial Create Issue transition
		WorkflowTestEntity testEntity = this.workflowTestEntityDAO.findByPrimaryKey(1);
		validateComparisonResult(testEntity, equalComparison, false, "Last Transition was not [Validate]");
		validateComparisonResult(testEntity, notEqualComparison, true, "Last Transition was not [Put On Hold]");

		// Entity 2 - Create Issue and then Start Progress
		equalComparison.setTransitionName("Start Progress");
		notEqualComparison.setTransitionName("Start Progress");
		WorkflowTestEntity testEntity2 = this.workflowTestEntityDAO.findByPrimaryKey(2);
		validateComparisonResult(testEntity2, equalComparison, true, "Last Transition was [Start Progress]");
		validateComparisonResult(testEntity2, notEqualComparison, false, "Last Transition was [Start Progress]");

		equalComparison.setTransitionName("Unavailable Transition");
		notEqualComparison.setTransitionName("Unavailable Transition");
		validateComparisonResult(testEntity2, equalComparison, false, "Last Transition was not [Unavailable Transition]");
		validateComparisonResult(testEntity2, notEqualComparison, true, "Last Transition was not [Unavailable Transition]");
	}


	/**
	 * @param expectedMessage - if blank then we don't validate the message, just the result
	 */
	private void validateComparisonResult(WorkflowTestEntity testEntity, WorkflowTransitionLastComparison transitionLastComparison, boolean expectedResult, String expectedMessage) {
		// Run Validation
		SimpleComparisonContext comparisonContext = new SimpleComparisonContext();
		boolean result = transitionLastComparison.evaluate(testEntity, comparisonContext);
		String message = (result ? comparisonContext.getTrueMessage() : comparisonContext.getFalseMessage());
		Assertions.assertEquals(expectedResult, result, "Expected " + expectedResult + (expectedMessage == null ? " " : " with a message of " + expectedMessage) + "but received a result of " + result + " with a message of " + message);
		if (expectedMessage != null) {
			Assertions.assertEquals(expectedMessage, message);
		}
	}


	private WorkflowTransitionLastComparison setupComparison() {
		WorkflowTransitionLastComparison comparison = new WorkflowTransitionLastComparison();
		comparison.setWorkflowHistoryService(this.workflowHistoryService);
		comparison.setWorkflowTransitionService(this.workflowTransitionService);
		return comparison;
	}
}
