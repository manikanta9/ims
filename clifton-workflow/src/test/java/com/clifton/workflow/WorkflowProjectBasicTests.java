package com.clifton.workflow;


import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class WorkflowProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "workflow";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.security.Principal");
		imports.add("java.sql.Types");
		imports.add("org.hibernate.Session");
		imports.add("org.hibernate.SessionFactory");
		imports.add("org.springframework.transaction.support.TransactionSynchronization");
		imports.add("org.springframework.transaction.support.TransactionSynchronizationAdapter");
		imports.add("org.springframework.transaction.support.TransactionSynchronizationManager");
		imports.add("org.springframework.ui.ModelMap");
		imports.add("org.springframework.web.context.request.RequestContextHolder");
		imports.add("freemarker.core.Environment");
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("approver");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		return CollectionUtils.createHashSet(
				"saveWorkflowTask",
				"saveWorkflowTaskDefinition",
				"saveWorkflowTaskStepDefinition",
				"deleteWorkflowState",
				"getWorkflowHistoryList"
		).contains(method.getName());
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveWorkflowHistoryEffectiveStartDate");
		return ignoredVoidSaveMethodSet;
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
