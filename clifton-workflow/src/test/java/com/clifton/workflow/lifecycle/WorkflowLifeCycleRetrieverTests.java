package com.clifton.workflow.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.lifecycle.retriever.BaseSystemLifeCycleRetrieverTests;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleEventRetriever;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleTestExecutor;
import com.clifton.workflow.WorkflowTestEntity;
import com.clifton.workflow.WorkflowTestEntity3;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class WorkflowLifeCycleRetrieverTests extends BaseSystemLifeCycleRetrieverTests {

	@Resource
	private WorkflowLifeCycleRetriever workflowLifeCycleRetriever;

	@Resource
	private WorkflowHistoryService workflowHistoryService;

	@Resource
	private ReadOnlyDAO<WorkflowTestEntity> workflowTestEntityDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected SystemLifeCycleEventRetriever getSystemLifeCycleRetriever() {
		return this.workflowLifeCycleRetriever;
	}


	@Override
	protected List<IdentityObject> getApplyToEntityFalseList() {
		return CollectionUtils.createList(new WorkflowState());
	}


	@Override
	protected List<IdentityObject> getApplyToEntityTrueList() {
		return CollectionUtils.createList(new WorkflowTestEntity(), new WorkflowTestEntity3(), this.workflowTestEntityDAO.findByPrimaryKey(1));
	}


	@Test
	@Override
	public void testGetSystemLifeCycleEventListForEntity_NoResults() {
		Mockito.when(this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(ArgumentMatchers.any(), ArgumentMatchers.anyBoolean())).thenReturn(null);
		SystemLifeCycleTestExecutor.forTableAndEntityAndRetriever("WorkflowTestEntity", this.workflowTestEntityDAO.findByPrimaryKey(1), getSystemLifeCycleRetriever())
				.execute();
	}


	@Test
	@Override
	public void testGetSystemLifeCycleEventListForEntity_Results() {
		Mockito.when(this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(ArgumentMatchers.any(), ArgumentMatchers.anyBoolean())).thenReturn(getTestWorkflowHistoryList());

		SystemLifeCycleTestExecutor.forTableAndEntityAndRetriever("WorkflowTestEntity", this.workflowTestEntityDAO.findByPrimaryKey(1), getSystemLifeCycleRetriever())
				.withExpectedResults("Source: WorkflowHistory [10]\tLink: N/A\tEvent Source: Workflow Transition\tAction: Create New Entity\tDate: 2021-06-07 18:29:55\tUser: Test User 1\tDetails: null",
						"Source: WorkflowHistory [20]\tLink: N/A\tEvent Source: Workflow Transition\tAction: Mark as Valid\tDate: 2021-06-07 18:29:56\tUser: System User\tDetails: because [(No Rule Violations Found)]")
				.execute();
	}


	private List<WorkflowHistory> getTestWorkflowHistoryList() {
		WorkflowHistory history1 = new WorkflowHistory();
		history1.setId(10);
		history1.setEndWorkflowState(WorkflowStateBuilder.createEmpty().withName("New").toWorkflowState());
		history1.setDescription("[Test User 1] did [Create New Entity] on [06/07/2021]");
		history1.setEndStateStartDate(DateUtils.toDate("2021-06-07 18:29:55", DateUtils.DATE_FORMAT_FULL));
		history1.setEffectiveEndStateStartDate(DateUtils.toDate("2021-06-07 18:29:55", DateUtils.DATE_FORMAT_FULL));
		history1.setEndStateEndDate(DateUtils.toDate("2021-06-07 18:29:56", DateUtils.DATE_FORMAT_FULL));
		history1.setCreateUserId((short) 2);

		WorkflowHistory history2 = new WorkflowHistory();
		history2.setId(20);
		history2.setStartWorkflowState(history1.getEndWorkflowState());
		history2.setEndStateStartDate(history1.getEndStateEndDate());
		history2.setEffectiveEndStateStartDate(history1.getEndStateEndDate());
		history2.setEndWorkflowState(WorkflowStateBuilder.createEmpty().withName("Valid").toWorkflowState());
		history2.setDescription("[SYSTEM] did [Mark as Valid] on [06/07/2021] because [(No Rule Violations Found)]");
		history2.setCreateUserId((short) 2);

		return CollectionUtils.createList(history1, history2);
	}
}
