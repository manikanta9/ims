package com.clifton.workflow.history;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 * The {@link WorkflowHistoryValidatorTests} is the test class for {@link WorkflowHistoryValidator}.
 *
 * @author mikeh
 */
public class WorkflowHistoryValidatorTests {

	private static final Date DATE_0 = DateUtils.toDate("2019-01-01 12:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
	private static final Date DATE_1 = DateUtils.toDate("2019-01-01 12:00:00.001", DateUtils.DATE_FORMAT_FULL_PRECISE);
	private static final Date DATE_2 = DateUtils.toDate("2019-01-01 12:00:00.002", DateUtils.DATE_FORMAT_FULL_PRECISE);
	private static final Date DATE_3 = DateUtils.toDate("2019-01-01 12:00:00.003", DateUtils.DATE_FORMAT_FULL_PRECISE);
	private static final Date DATE_4 = DateUtils.toDate("2019-01-01 12:00:00.004", DateUtils.DATE_FORMAT_FULL_PRECISE);
	private static final Date DATE_5 = DateUtils.toDate("2019-01-01 12:00:00.005", DateUtils.DATE_FORMAT_FULL_PRECISE);

	private WorkflowHistoryValidator validator;
	private WorkflowState workflowEndState;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		this.validator = new WorkflowHistoryValidator();
		this.workflowEndState = TestUtils.generateEntity(WorkflowState.class, Short.class);
		this.workflowEndState.setWorkflow(TestUtils.generateEntity(Workflow.class, Short.class));
		this.workflowEndState.getWorkflow().setName("Test Workflow");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidatePass_CorrectOrder() {
		// Assertions correct date validation
		validateWorkflowHistoryStartEndDates(DATE_0, DATE_0);
		validateWorkflowHistoryStartEndDates(DATE_0, DATE_1);
		validateWorkflowHistoryStartEndDates(DATE_0, DATE_2);
		validateWorkflowHistoryStartEndDates(DATE_0, DATE_3);
		validateWorkflowHistoryStartEndDates(DATE_0, DATE_4);
		validateWorkflowHistoryStartEndDates(DATE_0, DATE_5);
		validateWorkflowHistoryStartEndDates(DATE_1, DATE_1);
	}


	@Test
	public void testValidatePass_WithinThreshold() {
		validateWorkflowHistoryStartEndDates(DATE_1, DATE_0);
		validateWorkflowHistoryStartEndDates(DATE_2, DATE_0);
		validateWorkflowHistoryStartEndDates(DATE_3, DATE_0);
		validateWorkflowHistoryStartEndDates(DATE_4, DATE_0);
		validateWorkflowHistoryStartEndDates(DATE_2, DATE_1);
		validateWorkflowHistoryStartEndDates(DATE_3, DATE_1);
		validateWorkflowHistoryStartEndDates(DATE_4, DATE_1);
		validateWorkflowHistoryStartEndDates(DATE_5, DATE_1);
		validateWorkflowHistoryStartEndDates(DATE_3, DATE_2);
		validateWorkflowHistoryStartEndDates(DATE_4, DATE_2);
		validateWorkflowHistoryStartEndDates(DATE_5, DATE_2);
		validateWorkflowHistoryStartEndDates(DATE_4, DATE_3);
		validateWorkflowHistoryStartEndDates(DATE_5, DATE_3);
		validateWorkflowHistoryStartEndDates(DATE_5, DATE_4);
	}


	@Test
	public void testValidateFail() {
		final ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> validateWorkflowHistoryStartEndDates(DATE_5, DATE_0));
		Assertions.assertTrue(validationException.getMessage().endsWith("previous Workflow State(s) may need to be back dated first."));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateWorkflowHistoryStartEndDates(Date startDate, Date endDate) {
		WorkflowHistory wh = new WorkflowHistory();
		wh.setEndWorkflowState(this.workflowEndState);
		wh.setEffectiveEndStateStartDate(startDate);
		wh.setEffectiveEndStateEndDate(endDate);
		this.validator.validate(wh, DaoEventTypes.MODIFY);
	}
}
