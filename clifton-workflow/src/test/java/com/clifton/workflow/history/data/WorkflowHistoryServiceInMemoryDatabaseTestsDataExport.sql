USE PARAMETRICOMS
SELECT 'WorkflowHistory' AS EntityTableName, WorkflowHistoryID AS EntityID
FROM WorkflowHistory
WHERE SystemTableID = ((SELECT SystemTableID FROM SystemTable WHERE TableName = 'OrderAllocation'))
  AND FKFieldID IN (111, 112)
UNION
SELECT 'Workflow' AS EntityTableName, WorkflowID AS EntityID
FROM Workflow

