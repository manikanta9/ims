package com.clifton.workflow.history;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.workflow.jobs.WorkflowHistoryEndDateCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author AbhinayaM
 */
@ContextConfiguration
public class WorkflowHistoryServiceInMemoryDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private WorkflowHistoryService workflowHistoryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void populateWorkflowHistoryEndDateListForCommand_No_WeekDaysBack() {

		WorkflowHistoryEndDateCommand command = workflowHistoryEndDateCommand(null, (short) 4);
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.workflowHistoryService.populateWorkflowHistoryEndDateListForCommand(command));
		Assertions.assertEquals("Weekdays Back is required", exception.getMessage());
	}


	@Test
	public void populateWorkflowHistoryEndDateListFrCommand_with_Status() {
		WorkflowHistoryEndDateCommand command = workflowHistoryEndDateCommand(3, (short) 4);

		Status status = this.workflowHistoryService.populateWorkflowHistoryEndDateListForCommand(command);

		Assertions.assertEquals(0, status.getErrorCount());

		Map<Integer, String> expectedResultsMap = new HashMap<>();
		expectedResultsMap.put(993, "2021-06-18 12:24:49.000");
		expectedResultsMap.put(1000, "2021-06-18 12:24:49.000");
		expectedResultsMap.put(1001, "2021-06-18 13:13:33.000");
		expectedResultsMap.put(1015, "2021-06-18 13:42:39.000");
		expectedResultsMap.put(1052, "2021-06-18 13:46:18.000");
		expectedResultsMap.put(1067, "2021-06-18 13:48:20.000");
		expectedResultsMap.put(1076, "2021-06-18 13:48:24.000");
		expectedResultsMap.put(994, "2021-06-18 12:24:49.380");
		expectedResultsMap.put(1002, "2021-06-18 12:24:49.423");
		expectedResultsMap.put(1003, "2021-06-18 12:25:34.557");

		WorkflowHistorySearchForm searchForm = new WorkflowHistorySearchForm();
		searchForm.setTableName("OrderAllocation");
		searchForm.setEntityId((long) 111);
		List<WorkflowHistory> historyList = this.workflowHistoryService.getWorkflowHistoryList(searchForm);
		for (WorkflowHistory history : historyList) {
			String endDate = expectedResultsMap.get(history.getId());
			String historyEndDate = DateUtils.fromDate(history.getEndStateEndDate(), DateUtils.DATE_FORMAT_FULL_PRECISE);
			AssertUtils.assertTrue(StringUtils.isEqual(endDate, historyEndDate), "Expected same dates");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private WorkflowHistoryEndDateCommand workflowHistoryEndDateCommand(Integer weekdaysBack, Short workflowId) {
		WorkflowHistoryEndDateCommand command = new WorkflowHistoryEndDateCommand();
		command.setWorkflowId(workflowId);
		command.setWeekdaysBack(weekdaysBack);
		command.setSynchronous(true);
		return command;
	}
}
