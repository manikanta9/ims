package com.clifton.workflow.assignment;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.WorkflowTestEntity;
import com.clifton.workflow.WorkflowTestEntity2;
import com.clifton.workflow.WorkflowTestEntity4;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class WorkflowAssignmentTests {

	@Resource
	private SystemSchemaService systemSchemaService;
	@Resource
	private WorkflowAssignmentService workflowAssignmentService;
	@Resource
	private WorkflowDefinitionService workflowDefinitionService;
	@Resource
	private UpdatableDAO<WorkflowTestEntity> workflowTestEntityDAO;
	@Resource
	private UpdatableDAO<WorkflowTestEntity2> workflowTestEntity2DAO;
	@Resource
	private UpdatableDAO<WorkflowTestEntity4> workflowTestEntity4DAO;
	@Resource
	private ContextHandler contextHandler;


	@BeforeEach
	public void resetTests() {
		SecurityUser user = new SecurityUser();
		user.setId(new Integer(7).shortValue());

		user.setUserName("TestUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	@Test
	public void testSaveWorkflowAssignment() {
		// Verify Existing Beans do not have Workflow States set
		List<WorkflowTestEntity2> list = this.workflowTestEntity2DAO.findAll();
		Assertions.assertEquals(2, list.size());
		for (WorkflowTestEntity2 entity : list) {
			Assertions.assertNull(entity.getWorkflowState());
			Assertions.assertNull(entity.getWorkflowStatus());
		}

		// Assign System Table 2 to Workflow 1
		WorkflowAssignment assignment = new WorkflowAssignment();
		assignment.setWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		assignment.setTable(this.systemSchemaService.getSystemTable(MathUtils.SHORT_TWO));
		this.workflowAssignmentService.saveWorkflowAssignment(assignment);

		// Verify Existing Beans now DO have Workflow States set
		list = this.workflowTestEntity2DAO.findAll();
		Assertions.assertEquals(2, list.size());
		for (WorkflowTestEntity2 entity : list) {
			Assertions.assertNotNull(entity.getWorkflowState());
			Assertions.assertNotNull(entity.getWorkflowStatus());
		}

		//this.workflowTestEntity2DAO.deleteList(list);
		this.workflowAssignmentService.deleteWorkflowAssignment(assignment.getId());
	}


	@Test
	public void testSaveWorkflowAssignment_WorkflowNotRequired() {
		// Verify Existing Beans do not have Workflow States set
		List<WorkflowTestEntity4> list = this.workflowTestEntity4DAO.findAll();
		Assertions.assertEquals(2, list.size());
		for (WorkflowTestEntity4 entity : list) {
			Assertions.assertNull(entity.getWorkflowState());
			Assertions.assertNull(entity.getWorkflowStatus());
		}

		// Assign System Table 4 to Workflow 1
		WorkflowAssignment assignment = new WorkflowAssignment();
		assignment.setWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		assignment.setTable(this.systemSchemaService.getSystemTable(Short.parseShort("4")));
		this.workflowAssignmentService.saveWorkflowAssignment(assignment);

		// Verify Existing Beans still do not have workflow assigned.  Because the workflow is NOT required workflow is assigned ONLY on creation of new objects
		list = this.workflowTestEntity4DAO.findAll();
		Assertions.assertEquals(2, list.size());
		for (WorkflowTestEntity4 entity : list) {
			Assertions.assertNull(entity.getWorkflowState());
			Assertions.assertNull(entity.getWorkflowStatus());
		}
		this.workflowAssignmentService.deleteWorkflowAssignment(assignment.getId());
	}


	@Test
	public void testSaveWorkflowAssignmentFail() {
		// Should fail because the given table's DTO object does NOT implement WorkflowableAware
		WorkflowAssignment assignment = new WorkflowAssignment();
		assignment.setWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		assignment.setTable(this.systemSchemaService.getSystemTable(MathUtils.SHORT_THREE));

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowAssignmentService.saveWorkflowAssignment(assignment));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("The class associated with table [SystemTable] does not have support for workflows."));
	}


	@Test
	public void testInitialStateAndStatusAssignmentOnDtoCreate() {
		WorkflowTestEntity bean = new WorkflowTestEntity();
		this.workflowTestEntityDAO.save(bean);
		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state must have been set by observer");
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());
	}


	@Test
	public void testAssignmentOnDtoCreate_NoWorkflow() {
		WorkflowTestEntity4 bean = new WorkflowTestEntity4();
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertNull(bean.getWorkflowState(), "workflow state should not have been set by observer");
	}


	@Test
	public void testAssignmentOnDtoCreate_NoActiveWorkflowAssignedToTable() {
		// Remove the Assignment for the table
		short assignmentId = 400;
		this.workflowAssignmentService.deleteWorkflowAssignment(assignmentId);

		// Can still create the bean and save without a workflow because it is optional
		WorkflowTestEntity4 bean = new WorkflowTestEntity4();
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertNull(bean.getWorkflowState(), "workflow state should not have been set by observer");
	}


	@Test
	public void testAssignmentOnDtoCreate_WithSpecifiedWorkflow() {
		WorkflowTestEntity4 bean = new WorkflowTestEntity4();
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertNull(bean.getWorkflowState(), "workflow state should not have been set by observer because didn't fit condition");

		// Add assignment to Workflow 2 with no condition
		// AND ACTIVATE WORKFLOW 2
		Workflow workflow2 = this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_TWO);
		workflow2.setActive(true);
		this.workflowDefinitionService.saveWorkflow(workflow2);

		WorkflowAssignment assignment = new WorkflowAssignment();
		assignment.setWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_TWO));
		assignment.setTable(this.systemSchemaService.getSystemTable(Short.parseShort("4")));
		this.workflowAssignmentService.saveWorkflowAssignment(assignment);

		// Create new entity - explicit set to use Workflow 2
		// which assignment should make because no condition
		bean = new WorkflowTestEntity4();
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_TWO));
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state should have been set by observer because no condition");
		Assertions.assertEquals("Pending", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());

		// Create new entity - explicit set to use Workflow 1
		// which assignment should still skip because of condition
		bean = new WorkflowTestEntity4();
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertNull(bean.getWorkflowState(), "workflow state should not have been set by observer because didn't fit condition");

		// Create new entity - explicit set to use Workflow 1
		// which assignment should make because condition validates
		bean = new WorkflowTestEntity4();
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		bean.setCreateUserId(new Integer(555).shortValue());
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state should have been set by observer because did fit condition");
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());

		// Create new entity - explicit NOT to use a Workflow
		// which assignment should skip because workflow not set on bean
		bean = new WorkflowTestEntity4();
		bean.setMyWorkflow(null);
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertNull(bean.getWorkflowState(), "workflow state should not have been set by observer because workflow not selected on bean");
	}


	@Test
	public void testReassignment_WithSpecifiedWorkflow() {
		WorkflowTestEntity4 bean = new WorkflowTestEntity4();
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertNull(bean.getWorkflowState(), "workflow state should not have been set by observer because didn't fit condition");

		// Add assignment to Workflow 2 with no condition
		// AND ACTIVATE WORKFLOW 2
		Workflow workflow2 = this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_TWO);
		workflow2.setActive(true);
		this.workflowDefinitionService.saveWorkflow(workflow2);

		WorkflowAssignment assignment = new WorkflowAssignment();
		assignment.setWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_TWO));
		assignment.setTable(this.systemSchemaService.getSystemTable(Short.parseShort("4")));
		this.workflowAssignmentService.saveWorkflowAssignment(assignment);

		// Change workflow to #2 for the same bean and "reassign"
		// which assignment should make because no condition
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_TWO));
		this.workflowTestEntity4DAO.save(bean);

		this.workflowAssignmentService.reassignWorkflow("WorkflowTestEntity4", bean.getId());
		bean = this.workflowTestEntity4DAO.findByPrimaryKey(bean.getId());

		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state should have been set by reassignment because new workflow is valid and different");
		Assertions.assertEquals("Pending", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());

		// Switch workflow config back to Workflow 1 and try to reassign again - should skip it because doesn't fit condition for workflow 1
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		this.workflowTestEntity4DAO.save(bean);

		this.workflowAssignmentService.reassignWorkflow("WorkflowTestEntity4", bean.getId());
		bean = this.workflowTestEntity4DAO.findByPrimaryKey(bean.getId());

		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state should have been set by reassignment because new workflow is valid and different");
		Assertions.assertEquals("Pending", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());

		// Create new entity - explicit set to use Workflow 1
		// which assignment should make because condition validates
		bean = new WorkflowTestEntity4();
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		bean.setCreateUserId(new Integer(555).shortValue());
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state should have been set by observer because did fit condition");
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());

		// Clear workflow on the bean and reassign - should skip since now is null
		bean.setMyWorkflow(null);
		this.workflowTestEntity4DAO.save(bean);
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());

		// Switch workflow config to Workflow 2 and try to reassign again - should change it
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_TWO));
		this.workflowTestEntity4DAO.save(bean);

		this.workflowAssignmentService.reassignWorkflow("WorkflowTestEntity4", bean.getId());
		bean = this.workflowTestEntity4DAO.findByPrimaryKey(bean.getId());

		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state should have been set by reassignment because new workflow is valid and different");
		Assertions.assertEquals("Pending", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());

		// And then back to 1
		bean.setMyWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		bean.setCreateUserId(new Integer(555).shortValue());
		this.workflowTestEntity4DAO.save(bean);

		this.workflowAssignmentService.reassignWorkflow("WorkflowTestEntity4", bean.getId());
		bean = this.workflowTestEntity4DAO.findByPrimaryKey(bean.getId());

		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state should have been set by reassignment because new workflow is valid and different");
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());
	}


	@Test
	public void testNoWorkflowAssigned() {
		Workflow workflow = this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE);
		workflow.setActive(false);

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> this.workflowDefinitionService.saveWorkflow(workflow));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Cannot inactivate Workflow [Test Workflow 1] because it has already been used and has history records tied to it."));
	}


	@Test
	public void testConditionalWorkflowAssignmentInvalid() {
		Workflow workflow = this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE);
		workflow.setActive(true);
		this.workflowDefinitionService.saveWorkflow(workflow);

		WorkflowTestEntity2 bean = new WorkflowTestEntity2();
		bean.setCreateUserId(new Integer(444).shortValue()); // invalid condition id

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> this.workflowTestEntity2DAO.save(bean));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("There is no valid workflow assigned to 'WorkflowTestEntity2' table."));
	}


	@Test
	public void testConditionalWorkflowAssignment() {
		WorkflowTestEntity2 bean = new WorkflowTestEntity2();
		bean.setCreateUserId(new Integer(555).shortValue());
		this.workflowTestEntity2DAO.save(bean);
		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state must have been set by observer");
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());
	}


	@Test
	public void testManuallyChangeWorkflowReassignment_Fail() {
		WorkflowTestEntity2 bean = new WorkflowTestEntity2();
		bean.setCreateUserId(new Integer(555).shortValue());
		this.workflowTestEntity2DAO.save(bean);
		Assertions.assertNotNull(bean.getWorkflowState(), "workflow state must have been set by observer");
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open", bean.getWorkflowStatus().getName());

		// Manually change the workflow
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowStateByName(MathUtils.SHORT_TWO, "Pending"));
		bean.setWorkflowStatus(bean.getWorkflowState().getStatus());

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> this.workflowTestEntity2DAO.save(bean));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Cannot perform Workflow reassignment from [Test Workflow 1] to [Test Workflow 2] because reassignment execution mode is not enabled."));
	}
}
