package com.clifton.workflow.definition;

import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.search.WorkflowStateSearchForm;
import com.clifton.workflow.transition.WorkflowTransition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>WorkflowDefinitionServiceImplTests</code> class defines methods for testing the {@link WorkflowDefinitionServiceImpl}.
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class WorkflowDefinitionServiceImplTests {

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private DaoCompositeKeyCache<WorkflowState, Short, String> workflowStateCache;

	@Resource
	private XmlReadOnlyDAO<Workflow> workflowDAO;

	@Resource
	private XmlReadOnlyDAO<WorkflowState> workflowStateDAO;

	@Resource
	private XmlReadOnlyDAO<WorkflowTransition> workflowTransitionDAO;

	//////////////////////////////////////////////////////

	private static final short STATUS_OPEN = 100;
	private static final short STATUS_INACTIVE = 102;

	//////////////////////////////////////////////////////


	@BeforeEach
	public void resetTests() {
		this.workflowDAO.setExistsEvaluatesToTrue(true);
	}


	@Test
	public void testSaveInactiveWorkflow() {
		Workflow inactiveWorkflow = this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_TWO);
		Assertions.assertFalse(inactiveWorkflow.isActive());
		inactiveWorkflow.setName(inactiveWorkflow.getName() + "-Update");
		this.workflowDefinitionService.saveWorkflow(inactiveWorkflow);
	}


	@Test
	public void testSaveActiveWorkflow() {
		Workflow activeWorkflow = this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE);
		Assertions.assertTrue(activeWorkflow.isActive());
		activeWorkflow.setName(activeWorkflow.getName() + "-Update");
		this.workflowDefinitionService.saveWorkflow(activeWorkflow);
	}


	@Test
	public void testInactiveUnusedWorkflow() {
		Workflow activeWorkflow = this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_THREE);
		Assertions.assertTrue(activeWorkflow.isActive());
		activeWorkflow.setActive(false);
		this.workflowDAO.setExistsEvaluatesToTrue(false); // no history should be returned for this workflow
		this.workflowDefinitionService.saveWorkflow(activeWorkflow);
	}


	@Test
	public void testInactiveUsedWorkflow() {
		Workflow activeWorkflow = this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE);
		Assertions.assertTrue(activeWorkflow.isActive());
		activeWorkflow.setActive(false);

		Assertions.assertThrows(Exception.class, () -> this.workflowDefinitionService.saveWorkflow(activeWorkflow));
	}


	@Test
	public void testCreateWorkflow() {
		// New workflows must always be INACTIVE
		Workflow workflow = new Workflow();
		workflow.setName("Test New Workflow");
		workflow.setActive(true);

		this.workflowDefinitionService.saveWorkflow(workflow);
		Assertions.assertFalse(workflow.isActive());
	}


	@Test
	public void testDeleteInactiveWorkflow() {
		short workflowId = 4;
		Workflow workflow = this.workflowDefinitionService.getWorkflow(workflowId);
		List<WorkflowState> stateList = this.workflowDefinitionService.getWorkflowStateListByWorkflow(workflowId);
		Assertions.assertEquals(1, stateList.size());
		Assertions.assertFalse(workflow.isActive());
		this.workflowDefinitionService.deleteWorkflow(workflowId);

		workflow = this.workflowDefinitionService.getWorkflow(workflowId);
		stateList = this.workflowDefinitionService.getWorkflowStateListByWorkflow(workflowId);
		Assertions.assertNull(workflow);
		Assertions.assertTrue(CollectionUtils.isEmpty(stateList));
	}


	@Test
	public void testGetWorkflowStateList() {
		List<WorkflowState> stateList = this.workflowDefinitionService.getWorkflowStateListByWorkflow(MathUtils.SHORT_ONE);
		Assertions.assertEquals(3, stateList.size());

		WorkflowStateSearchForm searchForm = new WorkflowStateSearchForm();
		searchForm.setWorkflowId(MathUtils.SHORT_ONE);
		stateList = this.workflowDefinitionService.getWorkflowStateList(searchForm);
		Assertions.assertEquals(3, stateList.size());
	}


	@Test
	public void testSaveWorkflowStateActiveWorkflow() {
		// Try to update a workflow state for an active workflow and fail
		WorkflowState state = this.workflowDefinitionService.getWorkflowState(new Short("200"));
		state.setName(state.getName() + "-Updated");

		Assertions.assertThrows(Exception.class, () -> this.workflowDefinitionService.saveWorkflowState(state));
	}


	@Test
	public void testSaveWorkflowStateInactiveWorkflow() {
		// Try to update a workflow state for an inactive workflow
		WorkflowState state = this.workflowDefinitionService.getWorkflowState(new Short("203"));
		String stateName = state.getName() + "-Updated";
		state.setName(stateName);
		this.workflowDefinitionService.saveWorkflowState(state);
		Assertions.assertEquals(stateName, state.getName());
	}


	@Test
	public void testChangeWorkflowForWorkflowState() {
		// Try to change the workflow assigned to a state and fail
		WorkflowState state = this.workflowDefinitionService.getWorkflowState(new Short("203"));
		state.setWorkflow(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));

		Assertions.assertThrows(Exception.class, () -> this.workflowDefinitionService.saveWorkflowState(state));
	}


	@Test
	public void testDeleteWorkflowStateActiveWorkflow() {
		WorkflowState state = this.workflowDefinitionService.getWorkflowState(new Short("200"));
		Assertions.assertTrue(state.getWorkflow().isActive());

		Assertions.assertThrows(Exception.class, () -> this.workflowDefinitionService.deleteWorkflowState(state.getId()));
	}


	@Test
	public void testDeleteWorkflowStateInactiveWorkflow() {
		WorkflowState state = this.workflowDefinitionService.getWorkflowState(new Short("203"));
		Assertions.assertFalse(state.getWorkflow().isActive());
		this.workflowDefinitionService.deleteWorkflowState(state.getId());
	}


	@Test
	public void testSaveWorkflowStatusActiveWorkflow() {
		// Try to update a workflow status tied to an active workflow and fail
		WorkflowStatus status = this.workflowDefinitionService.getWorkflowStatus(STATUS_OPEN);
		status.setName(status.getName() + "-Updated");

		Assertions.assertDoesNotThrow(() -> this.workflowDefinitionService.saveWorkflowStatus(status));
	}


	@Test
	public void testSaveWorkflowStatusInactiveWorkflow() {
		// Try to update a workflow state for an inactive workflow
		WorkflowStatus status = this.workflowDefinitionService.getWorkflowStatus(STATUS_INACTIVE);
		String statusName = status.getName() + "-Updated";
		status.setName(statusName);
		this.workflowDefinitionService.saveWorkflowStatus(status);
		Assertions.assertEquals(statusName, status.getName());
	}


	@Test
	public void testDeleteWorkflowStatusInactiveWorkflow() {
		WorkflowStatus status = this.workflowDefinitionService.getWorkflowStatus(STATUS_INACTIVE);

		Assertions.assertDoesNotThrow(() -> this.workflowDefinitionService.deleteWorkflowStatus(status.getId()));
	}


	@Test
	public void testWorkflowStateCache_CallFromService() {
		short workflowId = 2;
		String stateName = "Entered";

		// Cache will now automatically lookup the missing bean - so should return correctly
		WorkflowState ws = this.workflowDefinitionService.getWorkflowStateByName(workflowId, stateName);
		AssertUtils.assertNotNull(ws, "Workflow State for Workflow ID 2 and stateName Entered should exist");
		Assertions.assertEquals(stateName, ws.getName());

		// Change Name - Pull again - cache should have been cleared and workflow state with OLD name should NOT exist
		ws.setName("Entered-Update");
		WorkflowState savedWorkflowState = this.workflowDefinitionService.saveWorkflowState(ws);

		ws = this.workflowDefinitionService.getWorkflowStateByName(workflowId, stateName);
		AssertUtils.assertNull(ws, "Workflow State for Workflow ID 2 and stateName Entered should NOT exist anymore");

		// Reset workflow state to original name
		savedWorkflowState.setName(stateName);
		this.workflowDefinitionService.saveWorkflowState(savedWorkflowState);
	}


	@Test
	public void testWorkflowStateCache_CallFromCache() {
		short workflowId = 2;
		String stateName = "Entered";

		// Cache will now automatically lookup the missing bean - so should return correctly
		WorkflowState ws = this.workflowStateCache.getBeanForKeyValues(this.workflowStateDAO, workflowId, stateName);
		AssertUtils.assertNotNull(ws, "Workflow State for Workflow ID 2 and stateName Entered should exist");
		Assertions.assertEquals(stateName, ws.getName());

		// Change Name - Pull again - cache should have been cleared and workflow state with OLD name should NOT exist
		ws.setName("Entered-Update");
		WorkflowState savedWorkflowState = this.workflowDefinitionService.saveWorkflowState(ws);

		ws = this.workflowStateCache.getBeanForKeyValues(this.workflowStateDAO, workflowId, stateName);
		AssertUtils.assertNull(ws, "Workflow State for Workflow ID 2 and stateName Entered should NOT exist anymore");

		// Reset workflow state to original name
		savedWorkflowState.setName(stateName);
		this.workflowDefinitionService.saveWorkflowState(savedWorkflowState);
	}


	@Test
	public void testWorkflowState_IsDisabled() {
		WorkflowState state = this.workflowDefinitionService.getWorkflowState(new Short("240"));
		state.setDisabled(true);
		this.workflowTransitionDAO.setLogicalEvaluatesToTrue(true);
		Exception exception = Assertions.assertThrows(Exception.class, () -> this.workflowDefinitionService.saveWorkflowState(state));
		Assertions.assertEquals("Workflow States can only be disabled if there are no transitions to or from that state.", exception.getMessage());
	}
}

