package com.clifton.workflow.transition;

import com.clifton.core.context.ContextHandler;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import javax.annotation.Resource;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * @author NickK
 */
public class WorkflowTransitionLockableTestAction implements WorkflowTransitionActionHandler<WorkflowAware> {

	private static final String TRANSITION_ID_BEAN_NAME = "transitionId";

	private final Lock lock = new ReentrantLock();
	// Lock used for sleeping, never allows acquiring a permit
	private final Semaphore sleepLock = new Semaphore(0);

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowAware processAction(WorkflowAware bean, WorkflowTransition transition) {
		// Obtains the lock and then sleeps for 4 seconds
		try {
			if (this.lock.tryLock(4, TimeUnit.SECONDS)) {
				try {
					if (this.contextHandler != null) {
						if (getTransitionStateId() != null) {
							WorkflowAware transitionedBean = this.workflowTransitionService.executeWorkflowTransition("WorkflowTestEntity", ((Number) bean.getIdentity()).intValue(), getTransitionStateId());
							if (!getTransitionStateId().equals(transitionedBean.getWorkflowState().getId())) {
								throw new IllegalStateException("Expected entity to be transitioned to state with ID: " + getTransitionStateId());
							}
						}
					}
					sleep(2, TimeUnit.SECONDS);
				}
				finally {
					this.lock.unlock();
				}
			}
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		return bean;
	}


	public void waitTillLocked() {
		while (!isLocked()) {
			sleep(500, TimeUnit.MILLISECONDS);
		}
	}


	private boolean isLocked() {
		if (this.lock.tryLock()) {
			this.lock.unlock();
			return false;
		}
		return true;
	}


	private void sleep(long duration, TimeUnit durationUnit) {
		try {
			this.sleepLock.tryAcquire(duration, durationUnit);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public void setTransitionStateId(Short transitionStateId) {
		if (transitionStateId != null) {
			this.contextHandler.setBean(TRANSITION_ID_BEAN_NAME, transitionStateId);
		}
	}


	public Short getTransitionStateId() {
		return (Short) this.contextHandler.getBean(TRANSITION_ID_BEAN_NAME);
	}
}
