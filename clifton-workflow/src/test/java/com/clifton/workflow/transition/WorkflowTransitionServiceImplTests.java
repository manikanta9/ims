package com.clifton.workflow.transition;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUser;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>WorkflowTransitionServiceImplTests</code> class defines methods for testing the {@link WorkflowTransitionServiceImpl}.
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class WorkflowTransitionServiceImplTests {

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	@Resource
	private WorkflowTransitionServiceImpl workflowTransitionService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private XmlReadOnlyDAO<Workflow> workflowDAO;

	@Resource
	private ContextHandler contextHandler;

	////////////////////////////////////////////

	private static final short STATE_200 = 200;
	private static final short STATE_202 = 202;
	private static final short STATE_203 = 203;
	private static final short STATE_250 = 250;

	////////////////////////////////////////////


	@BeforeEach
	public void resetTests() {
		this.workflowDAO.setExistsEvaluatesToTrue(true);

		SecurityUser user = new SecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUserName("TestUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);

		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);
	}


	@Test
	public void testSaveWorkflowTransitionInactiveWorkflow() {
		WorkflowTransition transition = this.workflowTransitionService.getWorkflowTransition(1003);
		String name = transition.getName() + "-Update";
		transition.setName(name);
		this.workflowTransitionService.saveWorkflowTransition(transition);
		Assertions.assertEquals(name, transition.getName());
	}


	@Test
	public void testSaveWorkflowTransitionActiveWorkflow() {
		WorkflowTransition transition = this.workflowTransitionService.getWorkflowTransition(1000);
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.workflowTransitionService.saveWorkflowTransition(transition));
		Assertions.assertEquals("Cannot edit Workflow Transition [Create Trade] unless your are an Administrator because it is tied to an active Workflow [Test Workflow 1].", exception.getMessage());
	}


	@Test
	public void testSaveWorkflowTransitionMissingEndState() {
		//  Missing Start & End States
		WorkflowTransition transition = new WorkflowTransition();
		transition.setStartWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_200));
		Exception exception = Assertions.assertThrows(Exception.class, () -> this.workflowTransitionService.saveWorkflowTransition(transition));
		Assertions.assertEquals("Workflow Transition End State is required", exception.getMessage());
	}


	@Test
	public void testSaveWorkflowTransitionSameStartAndEndStates_Automatic() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);

		WorkflowTransition transition = new WorkflowTransition();
		transition.setStartWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_200));
		transition.setEndWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_200));
		transition.setAutomatic(true);
		Exception exception = Assertions.assertThrows(Exception.class, () -> this.workflowTransitionService.saveWorkflowTransition(transition));
		Assertions.assertEquals("Workflow Transitions cannot be automatic if the Start and End States are the same.", exception.getMessage());
	}


	@Test
	public void testSaveWorkflowTransitionSameStartAndEndStates_NotAutomatic() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);

		WorkflowTransition transition = new WorkflowTransition();
		transition.setStartWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_200));
		transition.setEndWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_200));
		Assertions.assertDoesNotThrow(() -> this.workflowTransitionService.saveWorkflowTransition(transition));
	}


	@Test
	public void testSaveWorkflowTransitionDifferentWorkflows() {
		//  Start & End states belong to different workflows
		WorkflowTransition transition = new WorkflowTransition();
		transition.setStartWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_200));
		transition.setEndWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_203));
		Exception exception = Assertions.assertThrows(Exception.class, () -> this.workflowTransitionService.saveWorkflowTransition(transition));
		Assertions.assertEquals("Workflow Transition Start & End States must belong to the same Workflow", exception.getMessage());
	}


	@Test
	public void testSaveWorkflowTransitionSecondInitialState() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);

		// Add a new start transition for workflow where it already has one - fail
		WorkflowTransition transition = new WorkflowTransition();
		transition.setEndWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_202));
		Exception exception = Assertions.assertThrows(Exception.class, () -> this.workflowTransitionService.saveWorkflowTransition(transition));
		Assertions.assertEquals("Only one initial transition is allowed per workflow.  There already exists an initial transition [Create Trade] for Workflow [Test Workflow 1]", exception.getMessage());
	}


	@Test
	public void testSaveWorkflowTransitionFirstInitialState() {
		//  Add a new start transition for inactive workflow - successful
		WorkflowTransition transition = new WorkflowTransition();
		transition.setEndWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_203));

		this.workflowTransitionService.saveWorkflowTransition(transition);

		//  Newly saved transition
		Assertions.assertNotNull(this.workflowTransitionService.getWorkflowTransition(transition.getId()));

		//  Delete added transition so won't cause issues with later tests
		this.workflowTransitionService.deleteWorkflowTransition(transition.getId());
	}


	@Test
	public void testGetWorkflowTransitionFirst() {
		WorkflowTransition transition = this.workflowTransitionService.getWorkflowTransitionFirst(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_ONE));
		Assertions.assertNotNull(transition);

		transition = this.workflowTransitionService.getWorkflowTransitionFirst(this.workflowDefinitionService.getWorkflow(MathUtils.SHORT_TWO));
		Assertions.assertNull(transition);
	}


	@Test
	public void testDeleteWorkflowTransitionActiveWorkflow() {
		// Transitions that belong to active Workflows - should fail
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.workflowTransitionService.deleteWorkflowTransition(1000));
		Assertions.assertEquals("Cannot edit Workflow Transition [Create Trade] unless your are an Administrator because it is tied to an active Workflow [Test Workflow 1].", exception.getMessage());
	}


	@Test
	public void testDeleteWorkflowTransitionInactiveWorkflow() {
		// Transition belongs to inactive workflow - ok to edit
		Assertions.assertDoesNotThrow(() -> this.workflowTransitionService.deleteWorkflowTransition(1003));
	}


	@Test
	public void testSaveWorkflowTransitionDisabledWorkflow() {
		WorkflowTransition transition = new WorkflowTransition();

		transition.setName("Disabled transition");
		transition.setStartWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_250));
		transition.setEndWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_250));
		Exception exception = Assertions.assertThrows(Exception.class, () -> this.workflowTransitionService.saveWorkflowTransition(transition));
		Assertions.assertEquals("Cannot add a transition from state [TestDisabled], because it is disabled", exception.getMessage());

		//check for end state
		WorkflowTransition transition1 = new WorkflowTransition();
		transition1.setName("Disabled transition");
		transition1.setEndWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_250));
		Exception exception1 = Assertions.assertThrows(Exception.class, () -> this.workflowTransitionService.saveWorkflowTransition(transition1));
		Assertions.assertEquals("Cannot add a transition to state [TestDisabled], because it is disabled", exception1.getMessage());
	}


	@Test
	public void testCopyWorkflow() {
		Workflow newWorkflow = this.workflowDefinitionService.copyWorkflow(MathUtils.SHORT_ONE, "Copy of Workflow 1");

		// ID 3 should be Workflow we just created with copying
		newWorkflow = this.workflowDefinitionService.getWorkflow(newWorkflow.getId());
		Assertions.assertEquals("Copy of Workflow 1", newWorkflow.getName());

		List<WorkflowState> originalStateList = this.workflowDefinitionService.getWorkflowStateListByWorkflow(MathUtils.SHORT_ONE);
		List<WorkflowState> newStateList = this.workflowDefinitionService.getWorkflowStateListByWorkflow(newWorkflow.getId());
		Assertions.assertEquals(CollectionUtils.getSize(originalStateList), CollectionUtils.getSize(newStateList));
		for (WorkflowState originalState : CollectionUtils.getIterable(originalStateList)) {
			boolean found = false;
			for (WorkflowState newState : CollectionUtils.getIterable(newStateList)) {
				if ((originalState.getName()).equals((newState.getName()))) {
					found = true;
					break;
				}
			}
			Assertions.assertTrue(found, "Did not find state with name [" + originalState.getName() + "] in the copied list.");
		}

		List<WorkflowTransition> originalTransitionList = this.workflowTransitionService.getWorkflowTransitionListByWorkflow(MathUtils.SHORT_ONE);
		List<WorkflowTransition> newTransitionList = this.workflowTransitionService.getWorkflowTransitionListByWorkflow(newWorkflow.getId());
		Assertions.assertEquals(CollectionUtils.getSize(originalTransitionList), CollectionUtils.getSize(newTransitionList));
		for (WorkflowTransition originalTransition : CollectionUtils.getIterable(originalTransitionList)) {
			boolean found = false;
			for (WorkflowTransition newTransition : CollectionUtils.getIterable(newTransitionList)) {
				if ((originalTransition.getName()).equals((newTransition.getName()))) {
					found = true;
					break;
				}
			}
			Assertions.assertTrue(found, "Did not find transition with name [" + originalTransition.getName() + "] in the copied list.");
		}
	}
}
