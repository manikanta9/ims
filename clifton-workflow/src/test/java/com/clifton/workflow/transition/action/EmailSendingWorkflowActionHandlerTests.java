package com.clifton.workflow.transition.action;


import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.email.Email;
import com.clifton.core.util.email.EmailHandler;
import com.clifton.core.util.email.MimeContentTypes;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.workflow.WorkflowTestEntity;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatusBuilder;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class EmailSendingWorkflowActionHandlerTests {

	private static final String FROM_EMAIL = "A@paraport.com";
	private static final String SENDER_EMAIL = "B@paraport.com";
	private static final List<String> TO_EMAILS = Arrays.asList("C@paraport.com", "D@paraport.com");
	private static final List<String> CC_EMAILS = Arrays.asList("E@paraport.com", "F@paraport.com");
	private static final List<String> BCC_EMAILS = Arrays.asList("G@paraport.com", "H@paraport.com");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Mock
	private EmailHandler emailHandler;

	@Resource
	@InjectMocks
	private EmailSendingWorkflowActionHandler<WorkflowTask> emailSendingWorkflowActionHandlerWorkflowTask;

	@Resource
	@InjectMocks
	private EmailSendingWorkflowActionHandler<WorkflowTestEntity> emailSendingWorkflowActionHandlerWorkflowTestEntity;

	@Resource
	private WorkflowTaskService workflowTaskService;

	@Resource
	private SystemConditionService systemConditionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetStringFromTemplateWithWorkflowTask() {
		WorkflowTask workflowTask = new WorkflowTask();
		workflowTask.setLinkedEntityFkFieldId(1L);
		workflowTask.setDefinition(this.workflowTaskService.getWorkflowTaskDefinition((short) 1));
		workflowTask.setDescription("WorkflowTaskDescription");
		workflowTask.setName("WorkflowTaskName");

		this.emailSendingWorkflowActionHandlerWorkflowTask.setFromEmail(FROM_EMAIL);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSenderEmail(SENDER_EMAIL);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setToEmails(TO_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setCcEmails(CC_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setBccEmails(BCC_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSubject("${bean.name}");
		this.emailSendingWorkflowActionHandlerWorkflowTask.setText("${linkedBean.name}, ${linkedBean.definitionEntityModifyCondition.name}, ${bean.description}, ${bean.definition.linkedEntityTable.name}");

		ArgumentCaptor<Email> argument = ArgumentCaptor.forClass(Email.class);
		Assertions.assertEquals(workflowTask, this.emailSendingWorkflowActionHandlerWorkflowTask.processAction(workflowTask, null));
		Mockito.verify(this.emailHandler).send(argument.capture());
		Assertions.assertEquals(FROM_EMAIL, argument.getValue().getFromAddress());
		Assertions.assertEquals(SENDER_EMAIL, argument.getValue().getSenderAddress());
		Assertions.assertArrayEquals(CollectionUtils.toArray(TO_EMAILS, String.class), argument.getValue().getToAddresses());
		Assertions.assertArrayEquals(CollectionUtils.toArray(CC_EMAILS, String.class), argument.getValue().getCcAddresses());
		Assertions.assertArrayEquals(CollectionUtils.toArray(BCC_EMAILS, String.class), argument.getValue().getBccAddresses());
		Assertions.assertEquals("WorkflowTaskCategory1, SystemCondition1, WorkflowTaskDescription, WorkflowTaskCategory", argument.getValue().getContent());
		Assertions.assertEquals("WorkflowTaskName", argument.getValue().getSubject());
		Assertions.assertEquals(MimeContentTypes.TEXT_HTML, argument.getValue().getContentType());
	}


	@Test
	public void testGetStringFromTemplateWithNonWorkflowTaskObject() {
		WorkflowTestEntity workflowTestEntity = new WorkflowTestEntity();
		workflowTestEntity.setComment("WorkflowTestEntityComment");
		workflowTestEntity.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());

		WorkflowState workflowState = new WorkflowState();
		workflowState.setEntityUpdateCondition(this.systemConditionService.getSystemCondition(1));
		workflowTestEntity.setWorkflowState(workflowState);

		this.emailSendingWorkflowActionHandlerWorkflowTestEntity.setSubject("${bean.comment}");
		this.emailSendingWorkflowActionHandlerWorkflowTestEntity.setText("${bean.comment}, ${bean.workflowStatus.name}, ${bean.workflowState.entityUpdateCondition.name}");

		ArgumentCaptor<Email> argument = ArgumentCaptor.forClass(Email.class);
		Assertions.assertEquals(workflowTestEntity, this.emailSendingWorkflowActionHandlerWorkflowTestEntity.processAction(workflowTestEntity, null));
		Mockito.verify(this.emailHandler).send(argument.capture());
		Assertions.assertEquals("WorkflowTestEntityComment, Active, SystemCondition1", argument.getValue().getContent());
		Assertions.assertEquals("WorkflowTestEntityComment", argument.getValue().getSubject());
	}


	@Test
	public void testValidateMethodSuccess() {
		this.emailSendingWorkflowActionHandlerWorkflowTask.setFromEmail(FROM_EMAIL);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSenderEmail(null);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setToEmails(TO_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setCcEmails(CC_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setBccEmails(null);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSubject("Subject");
		this.emailSendingWorkflowActionHandlerWorkflowTask.setText("Text");
		this.emailSendingWorkflowActionHandlerWorkflowTask.validate();
	}


	@Test
	public void testValidateMethodNullFromEmail() {
		this.emailSendingWorkflowActionHandlerWorkflowTask.setFromEmail(null);
		Exception e = Assertions.assertThrows(ValidationException.class, () -> this.emailSendingWorkflowActionHandlerWorkflowTask.validate());
		Assertions.assertEquals("Email Address null is not valid.", e.getMessage());
	}


	@Test
	public void testValidateMethodInvalidSenderEmail() {
		this.emailSendingWorkflowActionHandlerWorkflowTask.setFromEmail(FROM_EMAIL);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSenderEmail("INVALID");
		Exception e = Assertions.assertThrows(ValidationException.class, () -> this.emailSendingWorkflowActionHandlerWorkflowTask.validate());
		Assertions.assertEquals("Email Address INVALID is not valid.", e.getMessage());
	}


	@Test
	public void testValidateMethodInvalidToEmail() {
		this.emailSendingWorkflowActionHandlerWorkflowTask.setFromEmail(FROM_EMAIL);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSenderEmail(null);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setToEmails(Arrays.asList("I@paraport.com", "INVALID"));
		Exception e = Assertions.assertThrows(ValidationException.class, () -> this.emailSendingWorkflowActionHandlerWorkflowTask.validate());
		Assertions.assertEquals("Email Address INVALID is not valid.", e.getMessage());
	}


	@Test
	public void testValidateMethodInvalidCcEmail() {
		this.emailSendingWorkflowActionHandlerWorkflowTask.setFromEmail(FROM_EMAIL);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSenderEmail(null);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setToEmails(TO_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setCcEmails(Arrays.asList("I@paraport.com", "INVALID"));
		Exception e = Assertions.assertThrows(ValidationException.class, () -> this.emailSendingWorkflowActionHandlerWorkflowTask.validate());
		Assertions.assertEquals("Email Address INVALID is not valid.", e.getMessage());
	}


	@Test
	public void testValidateMethodInvalidBccEmail() {
		this.emailSendingWorkflowActionHandlerWorkflowTask.setFromEmail(FROM_EMAIL);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSenderEmail(null);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setToEmails(TO_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setCcEmails(CC_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setBccEmails(Arrays.asList("I@paraport.com", "INVALID"));
		Exception e = Assertions.assertThrows(ValidationException.class, () -> this.emailSendingWorkflowActionHandlerWorkflowTask.validate());
		Assertions.assertEquals("Email Address INVALID is not valid.", e.getMessage());
	}


	@Test
	public void testValidateMethodEmptySubject() {
		this.emailSendingWorkflowActionHandlerWorkflowTask.setFromEmail(FROM_EMAIL);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSenderEmail(null);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setToEmails(TO_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setCcEmails(CC_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setBccEmails(BCC_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSubject("");
		Exception e = Assertions.assertThrows(ValidationException.class, () -> this.emailSendingWorkflowActionHandlerWorkflowTask.validate());
		Assertions.assertEquals("Subject cannot be empty", e.getMessage());
	}


	@Test
	public void testValidateMethodNullText() {
		this.emailSendingWorkflowActionHandlerWorkflowTask.setFromEmail(FROM_EMAIL);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSenderEmail(null);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setToEmails(TO_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setCcEmails(CC_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setBccEmails(BCC_EMAILS);
		this.emailSendingWorkflowActionHandlerWorkflowTask.setSubject("Subject");
		this.emailSendingWorkflowActionHandlerWorkflowTask.setText(null);
		Exception e = Assertions.assertThrows(ValidationException.class, () -> this.emailSendingWorkflowActionHandlerWorkflowTask.validate());
		Assertions.assertEquals("Message body cannot be empty", e.getMessage());
	}
}
