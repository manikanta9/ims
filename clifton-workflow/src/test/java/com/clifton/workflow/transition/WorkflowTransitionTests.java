package com.clifton.workflow.transition;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.concurrent.ConcurrentUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.concurrent.LockBusyException;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.workflow.WorkflowTestEntity;
import com.clifton.workflow.WorkflowTestEntity3;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class WorkflowTransitionTests {

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private WorkflowHistoryService workflowHistoryService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	@Resource
	private UpdatableDAO<WorkflowTestEntity> workflowTestEntityDAO;

	@Resource
	private UpdatableDAO<WorkflowTestEntity3> workflowTestEntity3DAO;

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////

	private static final String WORKFLOW_TEST_ENTITY_TABLE = "WorkflowTestEntity";

	private static final short WORKFLOW_ID = 1;

	private static final short STATE_300 = 300;
	private static final short STATE_301 = 301;
	private static final short STATE_302 = 302;
	private static final short STATE_303 = 303;

	private static final short STATUS_OPEN = 100;
	private static final short STATUS_ACTIVE = 102;

	private static final int VALIDATE_TRANSITION = 405;
	private static final int SAME_STATE_TRANSITION = 407; // CLOSED TO CLOSED (STATE 302)

	////////////////////////////////////////////


	@BeforeEach
	public void resetTests() {
		SecurityUser user = new SecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUserName("TestUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);

		// Most of the tests use this option, will just turn it off for the ones that want to test without it
		switchWorkflow_EndDateSavedOnHistory(WORKFLOW_ID, true);
	}


	@Test
	public void testInitialTransitionHistoryOnDtoCreate() {
		WorkflowTestEntity bean = new WorkflowTestEntity();
		this.workflowTestEntityDAO.save(bean);
		Assertions.assertFalse(bean.isNewBean());

		List<WorkflowHistory> historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		WorkflowHistory history = CollectionUtils.getOnlyElement(historyList);
		Assertions.assertNotNull(history);

		// Make sure the history record we just created is the last one
		Assertions.assertEquals(history, this.workflowHistoryService.getWorkflowHistoryLast(bean));

		Assertions.assertNull(history.getStartWorkflowState());
		Assertions.assertEquals("Open Issue", history.getEndWorkflowState().getName());
		Assertions.assertEquals("[TestUser] did [Create Issue] on [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "]", history.getDescription());
		Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
		Assertions.assertEquals(MathUtils.SHORT_ONE, history.getTable().getId());
		Assertions.assertNotNull(history.getTimeInEndStateFormatted());
	}


	@Test
	public void testValidTransitionOnDtoUpdate() {
		WorkflowTestEntity bean = this.workflowTestEntityDAO.findByPrimaryKey(1);
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_301));

		this.workflowTestEntityDAO.save(bean);

		// Make sure Status was updated properly
		Assertions.assertEquals(this.workflowDefinitionService.getWorkflowStatus(STATUS_ACTIVE), bean.getWorkflowStatus());

		List<WorkflowHistory> historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		Assertions.assertEquals(2, CollectionUtils.getSize(historyList));

		for (WorkflowHistory history : historyList) {
			if (history.getStartWorkflowState() == null) {
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals(MathUtils.SHORT_ONE, history.getTable().getId());
				continue;
			}
			else if ("Open Issue".equals(history.getStartWorkflowState().getName())) {
				Assertions.assertEquals("In Progress", history.getEndWorkflowState().getName());
				Assertions.assertEquals("[TestUser] did [Start Progress] on [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "]", history.getDescription());
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals(MathUtils.SHORT_ONE, history.getTable().getId());
				continue;
			}
			Assertions.fail("Invalid History found.");
		}
	}


	@Test
	public void testInitialTransitionHistoryOnDtoCreateWithEffectiveDate() {
		WorkflowTestEntity3 bean = new WorkflowTestEntity3();
		this.workflowTestEntity3DAO.save(bean);
		bean = this.workflowTestEntity3DAO.findByPrimaryKey(bean.getId());
		Assertions.assertFalse(bean.isNewBean());
		// Ensure Effective Start was Set
		Assertions.assertNotNull(bean.getWorkflowStateEffectiveStartDate());

		List<WorkflowHistory> historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		WorkflowHistory history = CollectionUtils.getOnlyElement(historyList);
		Assertions.assertNotNull(history);

		// Make sure the history record we just created is the last one
		Assertions.assertEquals(history, this.workflowHistoryService.getWorkflowHistoryLast(bean));

		Assertions.assertNull(history.getStartWorkflowState());
		Assertions.assertEquals("Open Issue", history.getEndWorkflowState().getName());
		Assertions.assertEquals("[TestUser] did [Create Issue] on [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "]", history.getDescription());
		Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
		Assertions.assertEquals(MathUtils.SHORT_TWO, history.getTable().getId());
		Assertions.assertEquals(bean.getWorkflowStateEffectiveStartDate(), history.getEffectiveEndStateStartDate());
		Assertions.assertNotNull(history.getTimeInEndStateFormatted());
	}


	@Test
	public void testTransitionHistoryOnDtoCreateAndUpdateWithoutEffectiveDate() {
		testTransitionHistoryOnDtoCreateAndUpdateWithoutEffectiveDateImpl(true);
	}


	@Test
	public void testTransitionHistoryOnDtoCreateAndUpdateWithoutEffectiveDate_DoNotSaveEndDateOnHistory() {
		testTransitionHistoryOnDtoCreateAndUpdateWithoutEffectiveDateImpl(false);
	}


	private void testTransitionHistoryOnDtoCreateAndUpdateWithoutEffectiveDateImpl(boolean endDateSavedOnHistory) {
		switchWorkflow_EndDateSavedOnHistory(WORKFLOW_ID, endDateSavedOnHistory);

		WorkflowTestEntity bean = new WorkflowTestEntity();
		this.workflowTestEntityDAO.save(bean);
		bean = this.workflowTestEntityDAO.findByPrimaryKey(bean.getId());
		Assertions.assertFalse(bean.isNewBean());

		List<WorkflowHistory> historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		WorkflowHistory initialHistory = CollectionUtils.getOnlyElement(historyList);
		Assertions.assertNotNull(initialHistory);

		// Make sure the history record we just created is the last one
		Assertions.assertEquals(initialHistory, this.workflowHistoryService.getWorkflowHistoryLast(bean));

		Assertions.assertNull(initialHistory.getStartWorkflowState());
		Assertions.assertEquals("Open Issue", initialHistory.getEndWorkflowState().getName());
		Assertions.assertEquals("[TestUser] did [Create Issue] on [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "]", initialHistory.getDescription());
		Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), initialHistory.getFkFieldId());
		Assertions.assertEquals(MathUtils.SHORT_ONE, initialHistory.getTable().getId());
		Assertions.assertEquals(0, DateUtils.compare(initialHistory.getEndStateStartDate(), initialHistory.getEffectiveEndStateStartDate(), true));
		Assertions.assertNull(initialHistory.getEndStateEndDate());

		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_301));
		bean = this.workflowTestEntityDAO.save(bean);

		// Make sure Status was updated properly
		Assertions.assertEquals(this.workflowDefinitionService.getWorkflowStatus(STATUS_ACTIVE), bean.getWorkflowStatus());

		historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		Assertions.assertEquals(2, CollectionUtils.getSize(historyList));

		for (WorkflowHistory history : historyList) {
			if (history.getStartWorkflowState() == null) {
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals(MathUtils.SHORT_ONE, history.getTable().getId());
				if (endDateSavedOnHistory) {
					Assertions.assertEquals(0, DateUtils.compare(history.getEndStateEndDate(), history.getEffectiveEndStateEndDate(), true));
				}
				else {
					//When endDateSavedOnHistory is not saved (false) EffectiveEndStateEndDate value is null
					Assertions.assertNull(history.getEffectiveEndStateEndDate());
				}

				Assertions.assertFalse(history.isEffectiveStartUsed());
				Assertions.assertFalse(history.isEffectiveEndUsed());
				// Even if end date isn't saved on history, when retrieving the list the code will populate it before returning
				Assertions.assertNotNull(history.getEndStateEndDate());
				continue;
			}
			else if ("Open Issue".equals(history.getStartWorkflowState().getName())) {
				Assertions.assertEquals("In Progress", history.getEndWorkflowState().getName());
				Assertions.assertEquals("[TestUser] did [Start Progress] on [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "]", history.getDescription());
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals(MathUtils.SHORT_ONE, history.getTable().getId());
				Assertions.assertEquals(0, DateUtils.compare(history.getEndStateStartDate(), history.getEffectiveEndStateStartDate(), true));
				Assertions.assertFalse(history.isEffectiveEndUsed());
				Assertions.assertFalse(history.isEffectiveStartUsed());
				continue;
			}
			Assertions.fail("Invalid History found.");
		}
	}


	@Test
	public void testValidTransitionOnDtoUpdateWithEffectiveDate() {
		WorkflowTestEntity3 bean = this.workflowTestEntity3DAO.findByPrimaryKey(1);
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_301));
		// Set Effective Start to 3 Days Ago
		Date effectiveStart = DateUtils.addDays(new Date(), -3);
		Date actualStart = null;
		bean.setWorkflowStateEffectiveStartDate(effectiveStart);

		this.workflowTestEntity3DAO.save(bean);
		bean = this.workflowTestEntity3DAO.findByPrimaryKey(1);
		Assertions.assertEquals(effectiveStart, bean.getWorkflowStateEffectiveStartDate());

		// Make sure Status was updated properly
		Assertions.assertEquals(this.workflowDefinitionService.getWorkflowStatus(STATUS_ACTIVE), bean.getWorkflowStatus());

		List<WorkflowHistory> historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		Assertions.assertEquals(2, CollectionUtils.getSize(historyList));

		for (WorkflowHistory history : historyList) {
			if (history.getStartWorkflowState() == null) {
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals(MathUtils.SHORT_TWO, history.getTable().getId());
				Assertions.assertFalse(history.isEffectiveStartUsed());
				Assertions.assertTrue(history.isEffectiveEndUsed());
				Assertions.assertNotNull(history.getEndStateEndDate());
				Assertions.assertEquals(0, DateUtils.compare(effectiveStart, history.getEffectiveEndStateEndDate(), true));
				continue;
			}
			else if ("Open Issue".equals(history.getStartWorkflowState().getName())) {
				Assertions.assertEquals("In Progress", history.getEndWorkflowState().getName());
				Assertions.assertEquals("[TestUser] did [Start Progress] on [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "]", history.getDescription());
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals(MathUtils.SHORT_TWO, history.getTable().getId());
				Assertions.assertEquals(0, DateUtils.compare(effectiveStart, history.getEffectiveEndStateStartDate(), true));
				Assertions.assertFalse(history.isEffectiveEndUsed());
				Assertions.assertTrue(history.isEffectiveStartUsed());
				actualStart = history.getEndStateStartDate();
				continue;
			}
			Assertions.fail("Invalid History found.");
		}

		// Clear the effective start
		bean.setWorkflowStateEffectiveStartDate(null);
		this.workflowTestEntity3DAO.save(bean);
		// Pull the bean again - effective start should now be actual start without time
		bean = this.workflowTestEntity3DAO.findByPrimaryKey(1);
		Assertions.assertEquals(DateUtils.clearTime(actualStart), bean.getWorkflowStateEffectiveStartDate());

		historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		Assertions.assertEquals(2, CollectionUtils.getSize(historyList));

		for (WorkflowHistory history : historyList) {
			if (history.getStartWorkflowState() == null) {
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals(MathUtils.SHORT_TWO, history.getTable().getId());
				// Should not match if including time, but should match if not including time
				Assertions.assertEquals(1, DateUtils.compare(actualStart, history.getEffectiveEndStateEndDate(), true));
				Assertions.assertEquals(0, DateUtils.compare(actualStart, history.getEffectiveEndStateEndDate(), false));
				Assertions.assertFalse(history.isEffectiveStartUsed());
				Assertions.assertFalse(history.isEffectiveEndUsed());
				Assertions.assertNotNull(history.getEndStateEndDate());
				continue;
			}
			else if ("Open Issue".equals(history.getStartWorkflowState().getName())) {
				Assertions.assertEquals("In Progress", history.getEndWorkflowState().getName());
				Assertions.assertEquals("[TestUser] did [Start Progress] on [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "]", history.getDescription());
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals(MathUtils.SHORT_TWO, history.getTable().getId());
				// Should not match if including time, but should match if not including time
				Assertions.assertEquals(1, DateUtils.compare(actualStart, history.getEffectiveEndStateStartDate(), true));
				Assertions.assertEquals(0, DateUtils.compare(actualStart, history.getEffectiveEndStateStartDate(), false));
				Assertions.assertFalse(history.isEffectiveEndUsed());
				Assertions.assertFalse(history.isEffectiveStartUsed());
				continue;
			}
			Assertions.fail("Invalid History found.");
		}
	}


	@Order(1)
	@Test
	public void testInvalidTransitionOnDtoUpdate() {
		WorkflowTestEntity bean = this.workflowTestEntityDAO.findByPrimaryKey(2);
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_300));

		Assertions.assertThrows(Exception.class, () -> this.workflowTestEntityDAO.save(bean));
	}


	@Order(2)
	@Test
	public void testTransitionHistoryDeleteOnDtoDelete() {
		WorkflowTestEntity bean = this.workflowTestEntityDAO.findByPrimaryKey(2);
		List<WorkflowHistory> historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		// Validate history exists for this bean
		Assertions.assertEquals(2, CollectionUtils.getSize(historyList));

		// Delete the bean
		this.workflowTestEntityDAO.delete(bean);

		// Make sure history records were deleted
		Assertions.assertNull(this.workflowHistoryService.getWorkflowHistory(501));
		Assertions.assertNull(this.workflowHistoryService.getWorkflowHistory(502));
	}


	@Test
	public void testAutomaticTransitionOnDtoUpdate() {
		WorkflowTestEntity bean = this.workflowTestEntityDAO.findByPrimaryKey(3);
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());

		List<WorkflowHistory> workflowHistoryList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		Assertions.assertEquals(1, CollectionUtils.getSize(workflowHistoryList));

		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_301));
		Assertions.assertEquals("In Progress", bean.getWorkflowState().getName());
		Assertions.assertEquals("Open Issue", this.workflowTestEntityDAO.findByPrimaryKey(3).getWorkflowState().getName());

		// in addition to history entry from "Open Issue" to "In Progress" state, also expecting automatic transition to "On Hold"
		this.workflowTestEntityDAO.save(bean);

		Assertions.assertEquals("On Hold", bean.getWorkflowState().getName());
		// Make sure Status was updated properly
		Assertions.assertEquals(this.workflowDefinitionService.getWorkflowStatus(STATUS_OPEN), bean.getWorkflowStatus());

		workflowHistoryList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		Assertions.assertEquals(3, CollectionUtils.getSize(workflowHistoryList));

		// Make sure the automatic transition to "On Hold" was used, not the manual one.
		Date stateChangeDate = null;
		for (WorkflowHistory history : workflowHistoryList) {

			Date endDate = ObjectUtils.coalesce(history.getEffectiveEndStateEndDate(), history.getEndStateEndDate());
			if (endDate == null) {
				Assertions.assertNotNull(history.getTimeInEndStateFormatted());
			}
			else {
				Assertions.assertEquals(DateUtils.getTimeDifferenceShort(endDate, history.getEffectiveEndStateStartDate()), history.getTimeInEndStateFormatted());
			}
			if (stateChangeDate != null) {
				Assertions.assertEquals(DateUtils.fromDate(stateChangeDate, DateUtils.DATE_FORMAT_SHORT), DateUtils.fromDate(history.getEndStateStartDate(), DateUtils.DATE_FORMAT_SHORT));
			}
			stateChangeDate = history.getEndStateEndDate();
			if (history.getEndWorkflowState().equals(bean.getWorkflowState())) {
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals("[SYSTEM] did [Put On Hold - Auto] on [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "] because [(id = 3)]", history.getDescription());
				break;
			}
		}
	}


	@Test
	public void testNotAutomaticTransitionOnDtoUpdate() {
		WorkflowTestEntity bean = this.workflowTestEntityDAO.findByPrimaryKey(4);
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_301));

		this.workflowTestEntityDAO.save(bean);

		Assertions.assertEquals("In Progress", bean.getWorkflowState().getName());
		// Make sure Status was updated properly
		Assertions.assertEquals(this.workflowDefinitionService.getWorkflowStatus(STATUS_ACTIVE), bean.getWorkflowStatus());

		List<WorkflowHistory> workflowHistoryList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		Assertions.assertEquals(2, CollectionUtils.getSize(workflowHistoryList));

		// Manually move the bean to On Hold & Validate the manual transition was used
		Assertions.assertEquals("In Progress", bean.getWorkflowState().getName());
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_303));

		this.workflowTestEntityDAO.save(bean);

		Assertions.assertEquals("On Hold", bean.getWorkflowState().getName());
		// Make sure Status was updated properly
		Assertions.assertEquals(this.workflowDefinitionService.getWorkflowStatus(STATUS_OPEN), bean.getWorkflowStatus());

		workflowHistoryList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, true);
		Assertions.assertEquals(3, CollectionUtils.getSize(workflowHistoryList));

		// Make sure the manual transition to "On Hold" was used, not the automatic one.
		for (WorkflowHistory history : workflowHistoryList) {
			if (history.getEndWorkflowState().equals(bean.getWorkflowState())) {
				Assertions.assertEquals(BeanUtils.getIdentityAsLong(bean), history.getFkFieldId());
				Assertions.assertEquals("[TestUser] did [Put On Hold] on [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "]", history.getDescription());
				break;
			}
		}
	}


	@Test
	public void testWorkflowTransitionsWithConditions() {
		testWorkflowTransitionsWithConditionsImpl(true);
	}


	@Test
	public void testWorkflowTransitionsWithConditions_DoNotSaveEndDateOnHistory() {
		testWorkflowTransitionsWithConditionsImpl(false);
	}


	private void testWorkflowTransitionsWithConditionsImpl(boolean endDateSavedOnHistory) {
		WorkflowTestEntity bean = new WorkflowTestEntity();
		this.workflowTestEntityDAO.save(bean);
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());

		// Move to In Progress
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_301));
		this.workflowTestEntityDAO.save(bean);
		Assertions.assertEquals("In Progress", bean.getWorkflowState().getName());

		// Try to move to Closed, but should fail because Comment is missing
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_302));

		try {
			this.workflowTestEntityDAO.save(bean);
		}
		catch (Exception e) {
			// Set the comment and try to re-save in state closed
			bean.setComment("TEST COMMENT");
			this.workflowTestEntityDAO.save(bean);
			Assertions.assertEquals("Closed", bean.getWorkflowState().getName());

			// Now try to edit the comment again - should fail b/c should be read only now
			try {
				bean.setComment("TEST COMMENT - UPDATED");
				this.workflowTestEntityDAO.save(bean);
			}
			catch (Exception e2) {
				// Exception expected.

				try {
					// Try to delete the entity - should also fail b/c the entity should be read only
					this.workflowTestEntityDAO.delete(bean);
				}
				catch (Exception e3) {
					// Exception expected
					return;
				}
				throw new RuntimeException("Expected exception to be thrown when trying to delete a read only entity");
			}
			throw new RuntimeException("Expected exception to be thrown when trying to edit a read only entity");
		}
		throw new RuntimeException("Expected exception to be thrown when trying to move entity to Closed state without a comment");
	}


	@Test
	public void testWorkflowTransitions_SameStateTransition() {
		testWorkflowTransitions_SameStateTransitionImpl(true);
	}


	@Test
	public void testWorkflowTransitions_SameStateTransition_DoNotSaveEndDateOnHistory() {
		testWorkflowTransitions_SameStateTransitionImpl(false);
	}


	private void testWorkflowTransitions_SameStateTransitionImpl(boolean endDateSavedOnHistory) {
		switchWorkflow_EndDateSavedOnHistory(WORKFLOW_ID, endDateSavedOnHistory);
		WorkflowTestEntity bean = new WorkflowTestEntity();
		this.workflowTestEntityDAO.save(bean);
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());

		// Move to In Progress
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_301));
		this.workflowTestEntityDAO.save(bean);
		Assertions.assertEquals("In Progress", bean.getWorkflowState().getName());

		// Add a comment and move to Closed
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_302));
		bean.setComment("Test Comment");
		bean = this.workflowTestEntityDAO.save(bean);
		Assertions.assertEquals("Closed", bean.getWorkflowState().getName());

		// Trigger specific transition that allows transitioning to itself
		this.workflowTransitionService.executeWorkflowTransitionByTransition(WORKFLOW_TEST_ENTITY_TABLE, bean.getId(), SAME_STATE_TRANSITION);

		// Check Workflow History
		WorkflowHistory history = this.workflowHistoryService.getWorkflowHistoryLast(bean);
		Assertions.assertEquals(STATE_302, history.getStartWorkflowState().getId());
		Assertions.assertEquals(STATE_302, history.getEndWorkflowState().getId());
		Assertions.assertTrue(history.getDescription().contains("[TestUser] did [Edit Closed] on ["));

		// Trigger specific transition that allows transitioning to itself AND Updates the comment
		bean.setComment("Test Comment - UPDATED");
		this.workflowTransitionService.executeWorkflowAwareSaveWithSameStateTransition(bean, SAME_STATE_TRANSITION);

		// Check Workflow History
		WorkflowHistory history2 = this.workflowHistoryService.getWorkflowHistoryLast(bean);
		Assertions.assertEquals(STATE_302, history2.getStartWorkflowState().getId());
		Assertions.assertEquals(STATE_302, history2.getEndWorkflowState().getId());
		Assertions.assertTrue(history2.getDescription().contains("[TestUser] did [Edit Closed] on ["));
		Assertions.assertFalse(CompareUtils.isEqual(history, history2), "Should have 2 distinct history records");

		// Confirm comment was updated
		bean = this.workflowTestEntityDAO.findByPrimaryKey(bean.getId());
		Assertions.assertEquals("Test Comment - UPDATED", bean.getComment());

		// Change the state and confirm exception
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_301));
		bean.setWorkflowStatus(bean.getWorkflowState().getStatus());

		final WorkflowTestEntity saveBean = bean;
		Exception exception = Assertions.assertThrows(ValidationException.class, () -> this.workflowTransitionService.executeWorkflowAwareSaveWithSameStateTransition(saveBean, SAME_STATE_TRANSITION));
		Assertions.assertEquals("Cannot execute same state transition [Edit Closed] because entity is currently in state [In Progress], and not in expected state [Closed]", exception.getMessage());

		// Put the state back and Try to perform a same state transition for a transition that is not a same state transition
		saveBean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_302));
		saveBean.setWorkflowStatus(bean.getWorkflowState().getStatus());
		exception = Assertions.assertThrows(ValidationException.class, () -> this.workflowTransitionService.executeWorkflowAwareSaveWithSameStateTransition(saveBean, VALIDATE_TRANSITION));
		Assertions.assertEquals("Selected transition [Validate] is not a same state transition.  Use the standard save or transition execution options to perform this transition and save.", exception.getMessage());

		List<WorkflowHistory> historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(bean, false);
		// All except the last (first in the list) history record should have an end date populated if opt to save end date, else none of them should
		Assertions.assertEquals(endDateSavedOnHistory ? historyList.size() - 1 : 4, CollectionUtils.getSize(BeanUtils.filterNotNull(historyList, WorkflowHistory::getEndStateEndDate)));
	}


	@Test
	public void testWorkflowTransitionAllowsOneConcurrentExecution() {
		testWorkflowTransitionSynchronization(false);
	}


	@Test
	public void testWorkflowTransitionAllowsOneConcurrentExecutionIsReentrant() {
		testWorkflowTransitionSynchronization(true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void switchWorkflow_EndDateSavedOnHistory(short workflowId, boolean endDateSavedOnHistory) {
		Workflow workflow = this.workflowDefinitionService.getWorkflow(workflowId);
		if (workflow.isEndDateSavedOnHistory() != endDateSavedOnHistory) {
			workflow.setEndDateSavedOnHistory(endDateSavedOnHistory);
			this.workflowDefinitionService.saveWorkflow(workflow);
		}
	}


	private void testWorkflowTransitionSynchronization(boolean nestedTransition) {
		WorkflowTestEntity bean = new WorkflowTestEntity();
		bean = this.workflowTestEntityDAO.save(bean);
		Assertions.assertEquals("Open Issue", bean.getWorkflowState().getName());

		// Move to In Progress
		bean.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_301));
		bean = this.workflowTestEntityDAO.save(bean);
		Assertions.assertEquals("In Progress", bean.getWorkflowState().getName());

		WorkflowTransitionAction lockableAction = CollectionUtils.getFirstElement(this.workflowTransitionService.getWorkflowTransitionActionListForTransitionId(VALIDATE_TRANSITION));
		Assertions.assertNotNull(lockableAction);
		WorkflowTransitionLockableTestAction lockableActionHandler = (WorkflowTransitionLockableTestAction) this.systemBeanService.getBeanInstance(lockableAction.getActionSystemBean());

		ExecutorService executorService = Executors.newFixedThreadPool(3);
		BlockingQueue<Object> processedEntity = new ArrayBlockingQueue<>(1);
		try {
			Integer beanId = bean.getId();
			executorService.submit(() -> {
				resetTests(); // set user
				if (nestedTransition) {
					// set back to In Progress
					lockableActionHandler.setTransitionStateId(STATE_301);
				}
				try {
					processedEntity.offer(this.workflowTransitionService.executeWorkflowTransitionByTransition(WORKFLOW_TEST_ENTITY_TABLE, beanId, VALIDATE_TRANSITION));
				}
				catch (Exception e) {
					processedEntity.offer(e);
				}
				finally {
					if (nestedTransition) {
						lockableActionHandler.setTransitionStateId(null);
					}
				}
			});

			lockableActionHandler.waitTillLocked();

			TestUtils.expectException(LockBusyException.class, () -> {
				this.workflowTransitionService.executeWorkflowTransitionByTransition(WORKFLOW_TEST_ENTITY_TABLE, beanId, VALIDATE_TRANSITION);
				Assertions.fail("Expected LockBusyException");
			}, "Failed to obtain lock with secure area [WORKFLOW-TRANSITION] and key [WorkflowTestEntity-" + beanId + "] because WORKFLOW-TRANSITION: TestUser locked [Transitioning WorkflowTestEntity with ID ");

			try {
				Object result = processedEntity.poll(10, TimeUnit.SECONDS);
				if (result instanceof Throwable) {
					Assertions.fail("Exception occurred during transition: " + ((Throwable) result).getMessage());
				}
				bean = (WorkflowTestEntity) result;
				Assertions.assertNotNull(bean);
				Assertions.assertEquals("Valid", bean.getWorkflowState().getName());
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
		finally {
			ConcurrentUtils.shutdownExecutorService(executorService);
		}
	}
}
