package com.clifton.workflow.task.lifecycle;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.system.audit.SystemAuditService;
import com.clifton.system.lifecycle.SystemLifeCycleEventService;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleTestExecutor;
import com.clifton.workflow.task.WorkflowTask;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Note: This test in addition to workflow history from real records, also tests the related entity lookup features (i.e. System Notes for the task)
 *
 * @author manderson
 */
@ContextConfiguration(locations = "classpath:com/clifton/workflow/WorkflowInMemoryDatabaseTests-context.xml")
@Transactional
public class WorkflowTaskLifeCycleRetrieverInMemoryDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private SystemLifeCycleEventService systemLifeCycleEventService;

	@Resource
	private SystemAuditService systemAuditService;


	/**
	 * Note: 2 tasks are pulled into the test data to confirm that the data retrieved only belongs to the selected task
	 */
	private static final int TEST_WORKFLOW_TASK_ID = 37476;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetSystemLifeCycleEventListForEntity() {
		// Ignore audit trail - doesn't work with in memory db
		Mockito.when(this.systemAuditService.getSystemAuditEventList(ArgumentMatchers.any())).thenReturn(null);

		SystemLifeCycleTestExecutor.forTableAndEntityId(WorkflowTask.TABLE_NAME, TEST_WORKFLOW_TASK_ID, this.systemLifeCycleEventService)
				.withExpectedResults("Source: WorkflowHistory [40061547]\tLink: WorkflowTask [37476]\tEvent Source: Workflow Transition\tAction: Verify Task\tDate: 2021-07-12 09:44:46\tUser: Vlad Gomelsky\tDetails: null",
						"Source: WorkflowTask [37476]\tLink: WorkflowTask [37476]\tEvent Source: Entity Last Update\tAction: Last Update\tDate: 2021-07-12 09:44:46\tUser: Vlad Gomelsky\tDetails: Seattle Traders",
						"Source: WorkflowHistory [40023754]\tLink: WorkflowTask [37476]\tEvent Source: Workflow Transition\tAction: Complete Task\tDate: 2021-07-08 14:39:37\tUser: Mary Anderson\tDetails: null",
						"Source: SystemNote [195331]\tLink: SystemNote [195331]\tEvent Source: Entity Last Update\tAction: Last Update\tDate: 2021-07-01 10:42:31\tUser: Michael McDermott\tDetails: Supporting Documentation: Zendesk ticket",
						"Source: SystemNote [195331]\tLink: SystemNote [195331]\tEvent Source: Entity Insert\tAction: Insert\tDate: 2021-07-01 10:42:30\tUser: Michael McDermott\tDetails: Supporting Documentation: Zendesk ticket",
						"Source: WorkflowHistory [39916293]\tLink: WorkflowTask [37476]\tEvent Source: Workflow Transition\tAction: Create New Task\tDate: 2021-07-01 10:35:26\tUser: Mary Anderson\tDetails: null",
						"Source: WorkflowTask [37476]\tLink: WorkflowTask [37476]\tEvent Source: Entity Insert\tAction: Insert\tDate: 2021-07-01 10:35:26\tUser: Mary Anderson\tDetails: Seattle Traders")
				.execute();
	}
}
