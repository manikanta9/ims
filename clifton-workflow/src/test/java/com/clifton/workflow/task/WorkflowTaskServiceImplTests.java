package com.clifton.workflow.task;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.SecurityTestsUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.priority.SystemPriority;
import com.clifton.system.priority.SystemPriorityService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author vgomelsky
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class WorkflowTaskServiceImplTests implements ApplicationContextAware {

	private static final short SYSTEM_PRIORITY_HIGH = 1;
	private static final short SYSTEM_PRIORITY_LOW = 3;

	private ApplicationContext applicationContext;
	@Resource
	private ContextHandler contextHandler;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private SystemPriorityService systemPriorityService;
	@Resource
	private WorkflowDefinitionService workflowDefinitionService;
	@Resource
	private WorkflowTaskService workflowTaskService;
	@Resource
	private SystemSchemaService systemSchemaService;
	@Resource
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////             Workflow Task Definition Tests            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveWorkflowTaskDefinition_RequireEntityListURLWhenLinkedEntityTableIsDefined() {
		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();
		definition.setLinkedEntityTable(new SystemTable());

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowTaskService.saveWorkflowTaskDefinition(definition));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Linked Entity List URL is required when Linked Entity Table is defined."));
	}


	@Test
	public void testSaveWorkflowTaskDefinition_RequireUnlockWorkflowStatusWhenLockEntityTableIsDefined() {
		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();
		definition.setLockList(new ArrayList<>());
		definition.getLockList().add(new WorkflowTaskDefinitionLock());

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowTaskService.saveWorkflowTaskDefinition(definition));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Unlock Workflow Status is required when Lock Entity Table is defined."));
	}


	@Test
	public void testSaveWorkflowTaskDefinition_SameAssigneeAndApproverAreAllowed() {
		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();
		definition.setSameAssigneeAndApproverAllowed(false);
		definition.setAssigneeUser(new SecurityUser());
		definition.setApproverUser(new SecurityUser());

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowTaskService.saveWorkflowTaskDefinition(definition));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Task Assignee and Approver cannot be the same user for this Task Definition."));
	}


	@Test
	public void testSaveWorkflowTaskDefinition_RequireApproverWhenSecondaryApproverIsDefined() {
		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();
		definition.setSecondaryApproverUser(new SecurityUser());

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowTaskService.saveWorkflowTaskDefinition(definition));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Approver User is required when Secondary Approver User is defined."));
	}


	@Test
	public void testSaveWorkflowTaskDefinition_ApproverAndSecondaryApproverCannotBeTheSame() {
		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();
		definition.setApproverUser(new SecurityUser());
		definition.setSecondaryApproverUser(new SecurityUser());

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowTaskService.saveWorkflowTaskDefinition(definition));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Approver User and Secondary Approver User cannot be the same."));
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Workflow Task Tests                 /////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveWorkflowTask_TaskAndDefinitionPrioritiesMustBeTheSame() {
		WorkflowTask task = new WorkflowTask();
		task.setDefinition(newWorkflowTaskDefinition(getSystemPriority(SYSTEM_PRIORITY_HIGH)));
		task.setPriority(getSystemPriority(SYSTEM_PRIORITY_LOW));

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowTaskService.saveWorkflowTask(task));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("When Priority is defined on Task Definition, then Task Priority must be the same as this on the definition: "));
	}


	@Test
	public void testSaveWorkflowTask_AssigneeUserIsRequired() {
		WorkflowTask task = new WorkflowTask();
		task.setDefinition(newWorkflowTaskDefinition(null));

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowTaskService.saveWorkflowTask(task));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Assignee User or Group is required for each Task."));
	}


	@Test
	public void testSaveWorkflowTask_OriginalDueDateCannotBeChanged() {
		// set current user
		SecurityUser user = new SecurityUser();
		user.setId(new Integer(7).shortValue());
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);

		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();
		definition.setWorkflow(this.workflowDefinitionService.getWorkflowByName("Test Workflow 1"));
		definition.setSameAssigneeAndApproverAllowed(true);
		definition.setChangeToDueDateAllowed(true);
		definition.setCategory(new WorkflowTaskCategory());
		this.workflowTaskService.saveWorkflowTaskDefinition(definition);

		// create a task due today
		WorkflowTask task = new WorkflowTask();
		task.setDefinition(definition);
		task.setAssigneeUser(user);
		task.setApproverUser(user);
		task = this.workflowTaskService.saveWorkflowTask(task);

		// update due date which is allowed
		task.setDueDate(DateUtils.addDays(new Date(), 5));
		task = this.workflowTaskService.saveWorkflowTask(task);

		// update to original due date must fail
		task.setOriginalDueDate(DateUtils.addDays(new Date(), 1));

		final WorkflowTask finalTask = task;
		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowTaskService.saveWorkflowTask(finalTask));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Original Due Date cannot be changed."));
	}


	@Test
	public void testSaveWorkflowTask_DescriptionRequiredForAutoGenerated_INSERT() {
		createWorkflowTaskForAutoGeneratedDefinitionWithoutDescription();
		Assertions.assertTrue(true, "Successfully Create Auto-Generated Task without Description.");
	}


	@Test
	public void testSaveWorkflowTask_DescriptionRequiredForAutoGenerated_UPDATE() {
		WorkflowTask task = createWorkflowTaskForAutoGeneratedDefinitionWithoutDescription();
		task.setName("test");

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.workflowTaskService.saveWorkflowTask(task));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Description field is required for tasks of selected definition: null"));
	}


	@Test
	public void testDeleteEntity_OpenWorkflowTask_WithLock() {
		SecurityUser referenceUser = this.securityUserService.getSecurityUser((short) 1);
		this.securityUserService.saveSecurityUser(referenceUser);


		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();
		definition.setCategory(new WorkflowTaskCategory());
		definition.getCategory().setDefinitionLockingAllowed(true);

		definition.setWorkflow(this.workflowDefinitionService.getWorkflowByName("Test Workflow 1"));
		definition.setLinkedEntityListURL("securityUser.json");
		definition.setTaskDescriptionRequired(false);
		definition.setSameAssigneeAndApproverAllowed(true);

		SystemTable table = this.systemSchemaService.getSystemTableByName("SecurityUser");
		definition.setLinkedEntityTable(table);

		definition.setFinalWorkflowStatus(this.workflowDefinitionService.getWorkflowStatus((short) 101));
		this.workflowTaskService.saveWorkflowTaskDefinition(definition);

		WorkflowTaskDefinitionLock lock = new WorkflowTaskDefinitionLock();

		lock.setDefinition(definition);
		lock.setLockEntityTable(table);
		lock.setLinkedEntityBeanFieldPath("id");
		List<WorkflowTaskDefinitionLock> lockList = new ArrayList<>();
		lockList.add(lock);

		definition.setLockList(lockList);
		definition.setUnlockWorkflowStatus(this.workflowDefinitionService.getWorkflowStatus((short) 100));
		// This line is needed to trigger registration of the Observer class
		definition.setActive(true);
		this.workflowTaskService.saveWorkflowTaskDefinition(definition);

		WorkflowTask task = new WorkflowTask();
		task.setAssigneeUser(new SecurityUser());
		task.setApproverUser(new SecurityUser());
		task.setDefinition(definition);
		task.setLinkedEntityLabel("Security User");
		task.setLinkedEntityFkFieldId(BeanUtils.getIdentityAsLong(referenceUser));
		this.workflowTaskService.saveWorkflowTask(task);

		try {
			ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> this.securityUserService.deleteSecurityUser(referenceUser.getId()));
			MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Cannot delete item because it has open workflow tasks."));
		}
		// Cleanup to remove locking from definition so that testDeleteEntity_OpenWorkflowTask is able to run
		finally {
			this.workflowTaskService.deleteWorkflowTask(task.getId());
			this.workflowTaskService.deleteWorkflowTaskDefinition(definition.getId());
		}
	}


	@Test
	public void testDeleteEntity_OpenWorkflowTask() {
		SecurityUser referenceUser = new SecurityUser();
		this.securityUserService.saveSecurityUser(referenceUser);

		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();

		definition.setCategory(new WorkflowTaskCategory());
		definition.setWorkflow(this.workflowDefinitionService.getWorkflowByName("Test Workflow 1"));
		definition.setLinkedEntityListURL("securityUser.json");
		definition.setTaskDescriptionRequired(false);
		definition.setSameAssigneeAndApproverAllowed(true);

		SystemTable table = this.systemSchemaService.getSystemTableByName("SecurityUser");
		definition.setLinkedEntityTable(table);

		definition.setFinalWorkflowStatus(this.workflowDefinitionService.getWorkflowStatus((short) 101));
		this.workflowTaskService.saveWorkflowTaskDefinition(definition);
		// This line is needed to trigger registration of the Observer class
		definition.setActive(true);
		this.workflowTaskService.saveWorkflowTaskDefinition(definition);

		WorkflowTask task = new WorkflowTask();
		task.setAssigneeUser(new SecurityUser());
		task.setApproverUser(new SecurityUser());
		task.setDefinition(definition);
		task.setLinkedEntityLabel("Security User");
		task.setLinkedEntityFkFieldId(BeanUtils.getIdentityAsLong(referenceUser));
		this.workflowTaskService.saveWorkflowTask(task);

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> this.securityUserService.deleteSecurityUser(referenceUser.getId()));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Cannot delete item because it has open workflow tasks."));
	}


	protected WorkflowTask createWorkflowTaskForAutoGeneratedDefinitionWithoutDescription() {
		SystemBeanType generatorType = new SystemBeanType();
		generatorType.setId(1);
		SystemBean generatorBean = new SystemBean();
		generatorBean.setType(generatorType);
		this.systemBeanService.saveSystemBeanNoValidation(generatorBean);

		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();
		definition.setCategory(new WorkflowTaskCategory());
		definition.setWorkflow(this.workflowDefinitionService.getWorkflowByName("Test Workflow 1"));
		definition.setTaskDescriptionRequired(true);
		definition.setTaskGeneratorBean(generatorBean);
		definition.setSameAssigneeAndApproverAllowed(true);
		this.workflowTaskService.saveWorkflowTaskDefinition(definition);

		WorkflowTask task = new WorkflowTask();
		task.setAssigneeUser(new SecurityUser());
		task.setApproverUser(new SecurityUser());
		task.setDefinition(definition);

		SecurityTestsUtils.setSecurityContextPrincipal(this.applicationContext, "testUser");

		this.workflowTaskService.saveWorkflowTask(task);
		return task;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemPriority getSystemPriority(short id) {
		return this.systemPriorityService.getSystemPriority(id);
	}


	private WorkflowTaskDefinition newWorkflowTaskDefinition(SystemPriority priority) {
		WorkflowTaskDefinition definition = new WorkflowTaskDefinition();
		definition.setPriority(priority);
		definition.setCategory(new WorkflowTaskCategory());
		this.workflowTaskService.saveWorkflowTaskDefinition(definition);
		return definition;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
