package com.clifton.workflow.task.step.comparison;

import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.step.WorkflowTaskStepService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author vgomelsky
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public abstract class BaseWorkflowTaskStepComparisonTests {

	public static final int TASK_WITH_NO_STEPS = 1;
	public static final int TASK_WITH_ALL_STEPS_COMPLETED = 2;
	public static final int TASK_WITH_NOT_COMPLETED_STEPS = 3;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Resource
	private WorkflowTaskService workflowTaskService;
	@Resource
	private WorkflowTaskStepService workflowTaskStepService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public WorkflowTaskStepService getWorkflowTaskStepService() {
		return this.workflowTaskStepService;
	}
}
