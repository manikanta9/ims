SELECT 'WorkflowTask' AS entityTableName, WorkflowTaskID AS entityID
FROM WorkflowTask
WHERE WorkflowTaskID IN (37476, 37776)
UNION
SELECT 'WorkflowHistory', WorkflowHistoryID
FROM WorkflowHistory h
	 INNER JOIN SystemTable t ON h.SystemTableID = t.SystemTableID
WHERE t.TableName = 'WorkflowTask'
  AND h.FkFieldID IN (37476, 37776)
UNION
SELECT 'WorkflowTransition', WorkflowTransitionID
FROM WorkflowTransition t
	 INNER JOIN WorkflowState es ON t.EndWorkflowStateID = es.WorkflowStateID
	 INNER JOIN Workflow w ON es.WorkflowID = w.WorkflowID
WHERE w.WorkflowID = (SELECT TOP 1 WorkflowID
					  FROM WorkflowState wts
						   INNER JOIN WorkflowTask task ON wts.WorkflowStateID = task.WOrkflowStateID
					  WHERE task.WorkflowTaskID IN (37476, 37776))
UNION
SELECT 'SystemNote', SystemNoteID
FROM SystemNote n
	 INNER JOIN SystemNoteType nt ON n.SystemNoteTypeID = nt.SystemNoteTypeID
	 INNER JOIN SystemTable t ON nt.SystemTableID = t.SystemTableID
WHERE t.TableName = 'WorkflowTask'
  AND n.FKFieldID IN (37476, 37776)
UNION
SELECT 'SecurityUser', SecurityUserID
FROM SecurityUser
WHERE SecurityUserName IN ('manderson', 'vgomelsky', 'systemuser', 'MichaelMc')
UNION
SELECT 'SystemTable', SystemTableID
FROM SystemTable
WHERE TableName IN ('WorkflowHistory', 'WorkflowTask', 'SystemNote');
