package com.clifton.workflow.task.step.comparison;

import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author vgomelsky
 */
public class WorkflowTaskHasIncompleteStepComparisonTests extends BaseWorkflowTaskStepComparisonTests {


	@Test
	public void testNoSteps() {
		WorkflowTaskHasIncompleteStepComparison comparisonWithIncompleteSteps = new WorkflowTaskHasIncompleteStepComparison();
		comparisonWithIncompleteSteps.setWorkflowTaskStepService(getWorkflowTaskStepService());

		ComparisonContext context = new SimpleComparisonContext();

		Assertions.assertFalse(comparisonWithIncompleteSteps.evaluate(getWorkflowTaskService().getWorkflowTask(TASK_WITH_NO_STEPS), context));
		Assertions.assertEquals("(No Incomplete Steps found for Task: Task with No Steps)", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}


	@Test
	public void testOneStepThatIsCompleted() {
		WorkflowTaskHasIncompleteStepComparison comparisonWithIncompleteSteps = new WorkflowTaskHasIncompleteStepComparison();
		comparisonWithIncompleteSteps.setWorkflowTaskStepService(getWorkflowTaskStepService());

		ComparisonContext context = new SimpleComparisonContext();

		Assertions.assertFalse(comparisonWithIncompleteSteps.evaluate(getWorkflowTaskService().getWorkflowTask(TASK_WITH_ALL_STEPS_COMPLETED), context));
		Assertions.assertEquals("(No Incomplete Steps found for Task: Task with All Steps Completed)", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}


	@Test
	public void testSecondStepIncomplete() {
		WorkflowTaskHasIncompleteStepComparison comparisonWithIncompleteSteps = new WorkflowTaskHasIncompleteStepComparison();
		comparisonWithIncompleteSteps.setWorkflowTaskStepService(getWorkflowTaskStepService());

		ComparisonContext context = new SimpleComparisonContext();

		Assertions.assertTrue(comparisonWithIncompleteSteps.evaluate(getWorkflowTaskService().getWorkflowTask(TASK_WITH_NOT_COMPLETED_STEPS), context));
		Assertions.assertEquals("(Incomplete Task Step found: Step 2)", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}
}
