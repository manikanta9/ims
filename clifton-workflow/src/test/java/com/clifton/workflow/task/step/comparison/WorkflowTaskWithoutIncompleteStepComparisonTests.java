package com.clifton.workflow.task.step.comparison;

import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author vgomelsky
 */
public class WorkflowTaskWithoutIncompleteStepComparisonTests extends BaseWorkflowTaskStepComparisonTests {

	@Test
	public void testNoSteps() {
		WorkflowTaskWithoutIncompleteStepComparison comparisonWithoutIncompleteSteps = new WorkflowTaskWithoutIncompleteStepComparison();
		comparisonWithoutIncompleteSteps.setWorkflowTaskStepService(getWorkflowTaskStepService());

		ComparisonContext context = new SimpleComparisonContext();

		Assertions.assertTrue(comparisonWithoutIncompleteSteps.evaluate(getWorkflowTaskService().getWorkflowTask(TASK_WITH_NO_STEPS), context));
		Assertions.assertEquals("(No Incomplete Steps found for Task: Task with No Steps)", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testOneStepThatIsCompleted() {
		WorkflowTaskWithoutIncompleteStepComparison comparisonWithoutIncompleteSteps = new WorkflowTaskWithoutIncompleteStepComparison();
		comparisonWithoutIncompleteSteps.setWorkflowTaskStepService(getWorkflowTaskStepService());

		ComparisonContext context = new SimpleComparisonContext();

		Assertions.assertTrue(comparisonWithoutIncompleteSteps.evaluate(getWorkflowTaskService().getWorkflowTask(TASK_WITH_ALL_STEPS_COMPLETED), context));
		Assertions.assertEquals("(No Incomplete Steps found for Task: Task with All Steps Completed)", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testSecondStepIncomplete() {
		WorkflowTaskWithoutIncompleteStepComparison comparisonWithoutIncompleteSteps = new WorkflowTaskWithoutIncompleteStepComparison();
		comparisonWithoutIncompleteSteps.setWorkflowTaskStepService(getWorkflowTaskStepService());

		ComparisonContext context = new SimpleComparisonContext();

		Assertions.assertFalse(comparisonWithoutIncompleteSteps.evaluate(getWorkflowTaskService().getWorkflowTask(TASK_WITH_NOT_COMPLETED_STEPS), context));
		Assertions.assertEquals("(Incomplete Task Step found: Step 2)", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}
}
