package com.clifton.workflow;


import com.clifton.workflow.definition.Workflow;


@WorkflowConfig(required = false, workflowBeanPropertyName = "myWorkflow")
public class WorkflowTestEntity4 extends BaseWorkflowAwareEntity<Integer> {

	private Workflow myWorkflow;


	public Workflow getMyWorkflow() {
		return this.myWorkflow;
	}


	public void setMyWorkflow(Workflow myWorkflow) {
		this.myWorkflow = myWorkflow;
	}
}
