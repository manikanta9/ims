Clifton.workflow.history.HistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow History Record',
	iconCls: 'workflow',
	width: 700,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Workflow history records a workflow state change that occurred in the past.  Effective Start Dates define when that workflow state is effective on.  When editing effective starts, you can also update the description to note your changes. If you clear the effective start, its value will be set to the actual start date.',
		url: 'workflowHistory.json',
		getSaveURL: function() {
			return 'workflowHistoryEffectiveStartDateSave.json';
		},
		labelFieldName: 'id',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Workflow', name: 'endWorkflowState.workflow.name', xtype: 'linkfield', detailIdField: 'endWorkflowState.workflow.id', detailPageClass: 'Clifton.workflow.definition.WorkflowWindow'},
			{
				xtype: 'columnpanel',
				columns: [
					{
						rows: [
							{fieldLabel: 'Linked Table', name: 'table.name', xtype: 'linkfield', detailIdField: 'table.id', detailPageClass: 'Clifton.system.schema.TableWindow'},
							{fieldLabel: 'Start Workflow State', name: 'startWorkflowState.name', xtype: 'linkfield', detailPageClass: 'Clifton.workflow.definition.StateWindow', detailIdField: 'startWorkflowState.id'},
							{fieldLabel: 'Start Date', name: 'endStateStartDate', xtype: 'datefield', format: 'm/d/Y H:i:s', readOnly: true},
							{fieldLabel: 'Effective Start Date', xtype: 'datefield', name: 'effectiveEndStateStartDate'}
						],
						config: {labelWidth: 130}
					}, {
						rows: [
							{
								fieldLabel: 'FK Field ID', name: 'fkFieldId', xtype: 'linkfield', detailIdField: 'fkFieldId',
								getDetailPageClass: function(fp) {
									return TCG.getValue('table.detailScreenClass', fp.getForm().formValues);
								}
							},
							{fieldLabel: 'End Workflow State', name: 'endWorkflowState.name', xtype: 'linkfield', detailPageClass: 'Clifton.workflow.definition.StateWindow', detailIdField: 'endWorkflowState.id'},
							{fieldLabel: 'End Date', name: 'endStateEndDate', xtype: 'datefield', format: 'm/d/Y H:i:s', readOnly: true},
							{fieldLabel: 'Effective End Date', name: 'effectiveEndStateEndDate', xtype: 'datefield', format: 'm/d/Y H:i:s', readOnly: true}
						],
						config: {labelWidth: 130}
					}
				]
			},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
		]
	}]
});
