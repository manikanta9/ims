Clifton.workflow.assignment.AssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Assignment',
	iconCls: 'workflow',
	height: 300,

	items: [{
		xtype: 'formpanel',
		instructions: 'A workflow assignment links a workflow to a given table. When a WorkflowAware entity is created for the table, the assignment will determine the workflow it will follow.',
		url: 'workflowAssignment.json',
		labelFieldName: 'workflow.name',
		labelWidth: 140,

		items: [
			{fieldLabel: 'Table', name: 'table.name', hiddenName: 'table.id', displayField: 'name', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', disableAddNewItem: true, detailPageClass: 'Clifton.system.schema.TableWindow'},
			{fieldLabel: 'Workflow', name: 'workflow.name', hiddenName: 'workflow.id', displayField: 'name', xtype: 'combo', url: 'workflowListFind.json', disableAddNewItem: true, detailPageClass: 'Clifton.workflow.definition.WorkflowWindow'},
			{fieldLabel: 'Assignment Condition', name: 'condition.name', hiddenName: 'condition.id', displayField: 'name', xtype: 'system-condition-combo'}
		]
	}]
});
