Clifton.workflow.WorkflowSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'workflowSetupWindow',
	title: 'Workflow Setup',
	iconCls: 'workflow',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Workflows',
				items: [{
					name: 'workflowListFind',
					xtype: 'gridpanel',
					instructions: 'A workflow defines the life-cycle for a given entity (database row).  It is a collection of states and transitions that define valid flows from one workflow state to the next.',
					wikiPage: 'IT/Workflow',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Assignment Table', xtype: 'toolbar-combo', name: 'assignmentTableId', width: 200, url: 'systemTableListFind.json?restrictionList=[{"field":"limitToWorkflowAssignmentTables","comparison":"EQUALS","value":true}]'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Workflow Name', width: 100, dataIndex: 'name'},
						{header: 'Documentation URL', width: 100, dataIndex: 'documentationURL'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Active', width: 25, dataIndex: 'active', type: 'boolean'},
						{
							header: 'Save History End Date', width: 45, dataIndex: 'endDateSavedOnHistory', type: 'boolean',
							tooltip: 'As transitions are performed and workflow history is recorded, a workflow history record is inserted with the start date and time that the entity was put into that workflow state.  The previous history record is also retrieved to set the end date on it.  If this option is unchecked, then only new history records are inserted and previous history records are not updated. (These can be filled in later with a batch job).  Turning this off can be ideal for high volume entities that go through multiple workflow transitions.'
						}
					],
					editor: {
						detailPageClass: 'Clifton.workflow.definition.WorkflowWindow',
						copyURL: 'workflowCopy.json',
						copyIdParameter: 'workflowId'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const table = TCG.getChildByName(this.getTopToolbar(), 'assignmentTableId');
						if (TCG.isNotBlank(table.getValue())) {
							return {assignmentTableId: table.getValue()};
						}
					}
				}]
			},


			{
				title: 'Workflow Assignments',
				items: [{
					name: 'workflowAssignmentListFind',
					xtype: 'gridpanel',
					instructions: 'A workflow assignment links a workflow to a given table. When a WorkflowAware entity is created for the table, the assignment will determine the workflow it will follow.',
					groupField: 'table.name',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Workflows" : "Workflow"]})',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Table', width: 100, dataIndex: 'table.name', filter: {searchFieldName: 'tableName'}, hidden: true},
						{header: 'Workflow', width: 150, dataIndex: 'workflow.name', filter: {searchFieldName: 'workflowName'}},
						{header: 'Assignment Condition', width: 200, dataIndex: 'condition.name', filter: {type: 'combo', searchFieldName: 'conditionId', url: 'systemConditionListFind.json'}},
						{header: 'Active', width: 30, dataIndex: 'workflow.active', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.workflow.assignment.AssignmentWindow',
						deleteURL: 'workflowAssignmentDelete.json'
					}
				}]
			},


			{
				title: 'Transitions',
				items: [{
					name: 'workflowTransitionListFind',
					xtype: 'gridpanel',
					instructions: 'Global list of all workflow transitions in the system. Workflow transitions define valid transitions between workflow states.',
					viewNames: ['Default View', 'Entry Screen'],
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 20, dataIndex: 'id'},
						{header: 'Workflow', width: 130, dataIndex: 'endWorkflowState.workflow.name', filter: {searchFieldName: 'workflowName'}},
						{header: 'Transition Name', width: 100, dataIndex: 'name'},
						{header: 'Transition Description', width: 200, dataIndex: 'description', hidden: 'true'},
						{header: 'Start State', width: 80, dataIndex: 'startWorkflowState.name', filter: {searchFieldName: 'startWorkflowStateName'}},
						{header: 'Start Status', width: 80, dataIndex: 'startWorkflowState.status.name', filter: {searchFieldName: 'startWorkflowStatusName'}, hidden: true},
						{header: 'End State', width: 80, dataIndex: 'endWorkflowState.name', filter: {searchFieldName: 'endWorkflowStateName'}},
						{header: 'End Status', width: 80, dataIndex: 'endWorkflowState.status.name', filter: {searchFieldName: 'startWorkflowStatusName'}, hidden: true},
						{header: 'Transition Condition', width: 150, dataIndex: 'condition.name', viewNames: ['Default View']},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int', useNull: true, hidden: true, defaultSortColumn: true, tooltip: 'Sort Order'},
						{header: 'Priority Order', width: 50, dataIndex: 'priorityOrder', type: 'int', useNull: true, hidden: true},
						{
							header: 'Auto', width: 30, dataIndex: 'automatic', type: 'boolean', viewNames: ['Default View'],
							tooltip: 'An automatic transition is one in which the system would move the entity from the start to end state based upon specified conditions.'
						},
						{
							header: 'Delayed', width: 30, dataIndex: 'automaticDelayed', type: 'boolean', hidden: true,
							tooltip: 'Delayed transitions are automatic transitions which do not execute until the end of the current request being handled. Otherwise, automatic transitions execute during the initial entity save. This adjustment to execution order has the advantage of allowing the transition to take into account all other actions that have taken place during the request, such as other entity saves in a multi-save request. Additionally, a failure in a delayed transition will not roll-back all actions prior to that transition as they would be for standard automatic transitions.'
						},
						{
							header: 'System', width: 30, dataIndex: 'systemDefined', type: 'boolean', viewNames: ['Default View'],
							tooltip: 'System define transitions cannot be triggered by users. They must be executed by the system.'
						},
						{
							header: 'Read', width: 30, dataIndex: 'readAccessSufficientToExecute', type: 'boolean', hidden: true,
							tooltip: 'Read Access Sufficient To Execute.  By default, users must have WRITE access to the table of the entity they are transitioning.  If checked (rare), users are allowed to execute with READ access.  Conditions can be used to further restrict who can execute a specific transition.'
						},
						{
							header: 'Not Transactional', width: 50, dataIndex: 'notExecutedInTransaction', type: 'boolean', hidden: true,
							tooltip: 'By default all transitions are executed in a transaction (including corresponding actions and auto-transitions). In rare cases you want to disable this to prevent database locking (workflow transition with an action that generates a report that needs the same row).'
						},
						{header: 'UI Screen', width: 30, dataIndex: 'entryDetailScreenClassPopulated', type: 'boolean', viewNames: ['Default View']},
						{header: 'Actions', width: 30, dataIndex: 'actionsExist', type: 'boolean', viewNames: ['Default View']},
						{header: 'Entry Screen', tooltip: 'A detail screen class is defined which will open when the transition is selected for an entity from the workflow toolbar', width: 100, dataIndex: 'entryDetailScreenClass', viewNames: ['Entry Screen'], hidden: true},
						{header: 'Default Data', tooltip: 'Default data for the entry detail screen', width: 100, dataIndex: 'entryDefaultData', viewNames: ['Entry Screen'], hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.workflow.transition.TransitionWindow'
					}
				}]
			},


			{
				title: 'Transition History',
				items: [{
					name: 'workflowHistoryListFind',
					xtype: 'gridpanel',
					instructions: 'Global list of all workflow transitions in the system. Workflow transitions define valid transitions between workflow states.',
					topToolbarSearchParameter: 'description',
					reloadOnRender: false,
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'Workflow', width: 100, dataIndex: 'endWorkflowState.workflow.name', filter: {searchFieldName: 'workflowName'}},
						{header: 'Start State', width: 80, dataIndex: 'startWorkflowState.name', filter: {searchFieldName: 'startWorkflowStateName'}},
						{header: 'Start Status', width: 60, dataIndex: 'startWorkflowState.status.name', filter: {type: 'combo', searchFieldName: 'startWorkflowStatusId', url: 'workflowStatusListFind.json'}},
						{header: 'End State', width: 80, dataIndex: 'endWorkflowState.name', filter: {searchFieldName: 'endWorkflowStateName'}},
						{header: 'End Status', width: 60, dataIndex: 'endWorkflowState.status.name', filter: {type: 'combo', searchFieldName: 'endWorkflowStatusId', url: 'workflowStatusListFind.json'}},
						{
							header: 'User', width: 45, dataIndex: 'createUserId', filter: {type: 'combo', searchFieldName: 'createUserId', url: 'securityUserListFind.json', displayField: 'label'},
							renderer: Clifton.security.renderSecurityUser, tooltip: 'User that performed this transition. Same as "Created By"'
						},
						// Boolean Flags for if the Effective Dates are different
						{header: 'Effective Start Used', dataIndex: 'effectiveStartUsed', type: 'boolean', hidden: true, filter: false},
						{header: 'Effective End Used', dataIndex: 'effectiveEndUsed', type: 'boolean', hidden: true, filter: false},
						// Actual Dates (Hidden)
						{header: 'Actual Start Date', width: 75, dataIndex: 'endStateStartDate', hidden: true},
						{header: 'Actual End Date', width: 75, dataIndex: 'endStateEndDate', hidden: true},
						// Start Date
						{
							header: 'Start Date', width: 75, dataIndex: 'effectiveEndStateStartDate', defaultSortColumn: 'true', defaultSortDirection: 'desc',
							renderer: function(v, p, r) {
								const value = TCG.renderDate(v);
								if (r.data['effectiveStartUsed'] === true) {
									return '<div style="COLOR: #ff0000;" qtip=\'Actual Start: ' + TCG.renderDate(r.data['endStateStartDate']) + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						// End Date
						{
							header: 'End Date', width: 75, dataIndex: 'effectiveEndStateEndDate',
							renderer: function(v, p, r) {
								const value = TCG.renderDate(v);
								if (r.data['effectiveEndUsed'] === true) {
									return '<div style="COLOR: #ff0000;" qtip=\'Actual End: ' + TCG.renderDate(r.data['endStateEndDate']) + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						{header: 'Duration', width: 80, dataIndex: 'timeInEndStateFormatted', filter: false, sortable: false},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'FKFieldID', width: 30, dataIndex: 'fkFieldId', hidden: true},
						{
							header: 'Entity', width: 35, dataIndex: 'table.detailScreenClass', filter: false, sortable: false,
							renderer: function(clz, args, r) {
								if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.fkFieldId)) {
									return TCG.renderActionColumn('view', 'View', 'View Entity that performed this Workflow transition');
								}
								return 'N/A';
							},
							eventListeners: {
								click: function(column, grid, rowIndex, event) {
									const row = grid.getStore().getAt(rowIndex);
									const gridPanel = grid.ownerGridPanel;
									gridPanel.editor.openDetailPage(row.json.table.detailScreenClass, gridPanel, row.json.fkFieldId, row);
								}
							}
						}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'System Table', width: 180, xtype: 'combo', name: 'table', url: 'systemTableListFind.json?defaultDataSource=true'},
							{fieldLabel: 'Description', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const table = TCG.getChildByName(this.getTopToolbar(), 'table');
						if (TCG.isBlank(table.getValue())) {
							TCG.showError('Required System Table filter must be selected.');
							return false;
						}
						return {
							tableId: table.getValue()
						};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.workflow.history.HistoryWindow'
					}
				}]
			},


			{
				title: 'Transition Actions',
				items: [{
					name: 'workflowTransitionActionListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Workflow transition actions are actions that can be performed prior to or after a transition is executed.  The following lists all of the workflow transition actions in the system.',
					additionalPropertiesToRequest: 'workflowTransition.id',
					columns: [
						{header: 'ID', width: 20, dataIndex: 'id', hidden: true},
						{header: 'Workflow Name', width: 150, dataIndex: 'workflowTransition.endWorkflowState.workflow.name', filter: {searchFieldName: 'workflowName'}},
						{header: 'Transition Name', width: 100, dataIndex: 'workflowTransition.name', filter: {searchFieldName: 'workflowTransitionName'}},
						{header: 'Start State', width: 80, dataIndex: 'workflowTransition.startWorkflowState.name', filter: {searchFieldName: 'startWorkflowStateName'}},
						{header: 'Start Status', width: 80, dataIndex: 'workflowTransition.startWorkflowState.status.name', filter: {searchFieldName: 'startWorkflowStatusName'}, hidden: true},
						{header: 'End State', width: 80, dataIndex: 'workflowTransition.endWorkflowState.name', filter: {searchFieldName: 'endWorkflowStateName'}},
						{header: 'End Status', width: 80, dataIndex: 'workflowTransition.endWorkflowState.status.name', filter: {searchFieldName: 'endWorkflowStatusName'}, hidden: true},
						{header: 'Action Bean', width: 170, dataIndex: 'actionSystemBean.name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Before', width: 30, dataIndex: 'beforeTransition', type: 'boolean'},
						{header: 'Separate Transaction', width: 40, dataIndex: 'separateTransaction', type: 'boolean'},
						{header: 'Order', width: 25, dataIndex: 'order', type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.workflow.transition.TransitionWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.workflowTransition.id;
						}
					}
				}]
			},


			{
				title: 'Statuses',
				items: [{
					name: 'workflowStatusListFind',
					xtype: 'gridpanel',
					instructions: 'Each workflow state is always associated with a workflow status. Multiple states can share the same status. Example of status are Open, Closed, On Hold, etc. Status are shared across all workflows and can be used for simple categorization of entities. Status can also be used to determine actions allowed for a given entity, i.e. (can\'t edit Closed entities).',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Status Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 400, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.workflow.definition.StatusWindow'
					}
				}]
			},


			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 140,
						loadValidation: false,
						height: 230,
						buttonAlign: 'right',
						instructions: 'For those workflows that choose to turn off saving workflow history end date, the following can be used to back fill the missing workflow history end dates.',
						items: [
							{fieldLabel: 'Weekdays Back', xtype: 'spinnerfield', name: 'weekdaysBack', minValue: 1, maxValue: 90, value: 1, allowBlank: false, qtip: 'This option allows filtering history records with null end dates to those last updated on or after weekdays back. Enter as a positive value.'},
							{fieldLabel: 'Workflow', xtype: 'combo', url: 'workflowListFind.json?endDateSavedOnHistory=false', name: 'workflow.name', hiddenName: 'workflow.id'}
						],
						buttons: [
							{
								text: 'Populate End Date(s)',
								iconCls: 'run',
								width: 150,
								handler: function() {
									const owner = TCG.getParentFormPanel(this);
									const grid = TCG.getChildByName(TCG.getParentTabPanel(this), 'runnerConfigListFind');
									const form = owner.getForm();
									form.submit(Ext.applyIf({
										url: 'workflowHistoryEndDateListForCommandPopulate.json',
										waitMsg: 'Processing...',
										success: function(form, action) {
											Ext.Msg.alert('Processing Started', action.result.status.message, function() {
												grid.reload.defer(300, grid);
											});
										}
									}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
								}
							}
						]
					},
					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'WORKFLOW-HISTORY-END-DATE-POPULATE',
						instantRunner: true,
						instructions: 'The following runners are being processed right now, or having recently completed.',
						title: 'Workflow History End Date Population',
						columnOverrides: [{dataIndex: 'type', hidden: true}],
						flex: 1
					}
				]
			}]
	}]

});



