Ext.ns('Clifton.workflow', 'Clifton.workflow.assignment', 'Clifton.workflow.definition', 'Clifton.workflow.task', 'Clifton.workflow.task.step', 'Clifton.workflow.task.template', 'Clifton.workflow.transition', 'Clifton.workflow.transition.action', 'Clifton.workflow.transition.entry', 'Clifton.workflow.history');


TCG.data.ErrorWindows['WorkflowTaskLockValidationException'] = 'Clifton.workflow.task.EntityLockedErrorWindow';


Clifton.workflow.task.LOCK_EVENT_SCOPES = [
	['INSERT', 'INSERT', 'Only when Linked Entity is Inserted.'],
	['UPDATE', 'UPDATE', 'Only when Linked Entity is Updated.'],
	['DELETE', 'DELETE', 'Only when Linked Entity is Deleted.'],
	['SAVE', 'SAVE', 'Only when Linked Entity is Inserted or Updated.']
];

Clifton.workflow.transition.action.EMAIL_TEXT_CUSTOM_COLUMNS_SYNTAX_DESCRIPTION = TCG.trimWhitespace(`
	<br/><br/>
	Placeholder values are available to use in freemarker, e.g. <code>\${bean.name}</code>:
	<br/>
	<ul class="c-list">
		<li><b><code>bean</code></b>: Corresponds to the workflow-aware entity, such as a trade or a workflow task (for workflow task definitions)</li>
		<li><b><code>linkedBean</code></b>: If the <code>bean</code> is a workflow task, then this will correspond to the entity associated with the task</li>
	</ul>
	<br/>
	Custom column values may be accessed through CustomJsonStringUtils methods:
	<br/>
	<ul class="c-list">
		<li><b><code>CustomJsonStringUtils.getColumnText(CustomJsonString customColumns, String columnName)</code></b>: Access the text of custom column values stored as a value-text pair
			<ul class="c-list">
				<li>E.g. <code>\${CustomJsonStringUtils.getColumnText(bean.customColumns, "tradeDate")}</code></li>
			</ul>
		</li>
		<li><b><code>CustomJsonStringUtils.getProperty(CustomJsonString customColumns, String propertyPath)</code></b>: Access other custom column values, including those that are stored as nested maps or lists </li>
			<ul class="c-list">
				<li>E.g. <code>\${CustomJsonStringUtils.getProperty(bean.customColumns, "accountList.entityList")}</code></li>
				<li>E.g., to access a list of values: <code>&lt#list CustomJsonStringUtils.getProperty(bean.customColumns, "accountList.entityList") as accountList>Column Value: \${accountList.clientAccount} &lt/#list&gt</code></li>
			</ul>
	</ul>
	<br/>
`);


TCG.app.Application.registerApplicationEventListener('workflowTasksAutoCreated', function(taskIds) {
	for (let i = 0; i < taskIds.length; i++) {
		const className = 'Clifton.workflow.task.WorkflowTaskWindow';
		TCG.createComponent(className, {
			id: TCG.getComponentId(className, taskIds[i]),
			params: {id: taskIds[i]},
			openerCt: this
		});
	}
});


Clifton.system.InfoWindow_AdditionalTabs[Clifton.system.InfoWindow_AdditionalTabs.length] = {
	title: 'Workflow Tasks',
	items: [{
		xtype: 'workflow-task-grid',
		getTableName: function() {
			return this.getWindow().table.name;
		},
		getEntityId: function() {
			return this.getWindow().idFieldValue;
		}
	}]
};

Clifton.system.InfoWindow_AdditionalTabs[Clifton.system.InfoWindow_AdditionalTabs.length] = {
	title: 'Task Definitions',
	items: [{
		name: 'workflowTaskDefinitionListFind',
		xtype: 'gridpanel',
		instructions: 'The following Workflow Task Definition are linked to or lock this entity. Note that task\'s Lock Scope Condition may exclude this specific entity.',
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{
				header: 'Task Category',
				width: 70,
				dataIndex: 'category.name',
				filter: {searchFieldName: 'categoryId', type: 'combo', url: 'workflowTaskCategoryListFind.json'},
				hidden: true
			},
			{header: 'Task Definition', width: 100, dataIndex: 'name'},
			{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
			{header: 'Task Workflow', width: 100, dataIndex: 'workflow.name'},
			{header: 'Task Assignee', width: 50, dataIndex: 'assigneeUser.label', hidden: true},
			{header: 'Task Approver', width: 40, dataIndex: 'approverUser.label'},
			{header: 'Secondary Approver', width: 50, dataIndex: 'secondaryApproverUser.label'},
			{header: 'Task Priority', width: 40, dataIndex: 'priority.name'},
			{header: 'Locked', width: 30, dataIndex: 'locked', type: 'boolean'},
			{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'}
		],
		getLoadParams: function() {
			return {linkedOrLockEntityTableName: this.getWindow().table.name};
		},
		editor: {
			detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow',
			drillDownOnly: true
		}
	}]
};


Clifton.system.schema.TableWindowAdditionalTabs[Clifton.system.schema.TableWindowAdditionalTabs.length] = {
	title: 'Tasks',
	items: [{
		xtype: 'workflow-task-grid',
		tableName: 'SystemTable'
	}]
};

Clifton.security.user.UserWindowAdditionalTabs[Clifton.security.user.UserWindowAdditionalTabs.length] = {
	title: 'Tasks',
	items: [{
		xtype: 'workflow-task-grid',
		tableName: 'SecurityUser'
	}]
};

Clifton.security.user.GroupWindowAdditionalTabs[Clifton.security.user.GroupWindowAdditionalTabs.length] = {
	title: 'Tasks',
	items: [{
		xtype: 'workflow-task-grid',
		tableName: 'SecurityGroup'
	}]
};


Clifton.workflow.showWikiPage = function(url) {
	if (TCG.isFalse(url) || TCG.isBlank(url)) {
		TCG.showError('Documentation URL is not defined for this Workflow.', 'No Help WIKI Page');
	}
	else {
		TCG.openWIKI(url, 'Workflow Help');
	}
};


Clifton.workflow.StateCombo = Ext.extend(TCG.form.ComboBox, {
	tableName: 'REQUIRED-TABLE-NAME',
	hiddenName: 'workflowState.id',
	name: 'workflowState.name',
	url: 'workflowStateNextAllowedListForCommand.json',
	loadAll: true, // next possible state list is always small
	requeryOnFormUpdate: true, // next states will be different after current state is changed
	disableUntilTheFormIsCreated: true, // should not be able to specify initial state: set automatically
	excludeHidden: true,
	excludeSystemDefined: true,

	beforequery: function(queryEvent) {
		const s = queryEvent.combo.store;
		s.setBaseParam('id', this.getEntityId());
		s.setBaseParam('tableName', this.tableName);
		s.setBaseParam('excludeHidden', this.excludeHidden);
		s.setBaseParam('excludeSystemDefined', this.excludeSystemDefined);
	},
	getEntityId: function() {
		return TCG.getParentFormPanel(this).getIdFieldValue();
	}
});
Ext.reg('workflow-state-combo', Clifton.workflow.StateCombo);


Clifton.workflow.TransitionCombo = Ext.extend(TCG.form.ComboBox, {
	tableName: 'REQUIRED-TABLE-NAME',
	hiddenName: 'workflowState.id',
	name: 'workflowState.name',
	url: 'workflowTransitionNextListByEntity.json',
	valueField: 'endStateId',
	loadAll: true, // next possible state list is always small
	requeryOnFormUpdate: true, // next states will be different after current state is changed
	disableUntilTheFormIsCreated: true, // should not be able to specify initial state: set automatically

	beforequery: function(queryEvent) {
		const s = queryEvent.combo.store;
		s.setBaseParam('id', this.getEntityId());
		s.setBaseParam('tableName', this.tableName);
	},
	getEntityId: function() {
		return TCG.getParentFormPanel(this).getIdFieldValue();
	}
});
Ext.reg('workflow-transition-combo', Clifton.workflow.TransitionCombo);


Clifton.workflow.HistoryGridPanel = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	name: 'workflowHistoryListFind',
	instructions: 'The following workflow history has been recorded for this entity.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true, defaultSortColumn: true, defaultSortDirection: 'desc'},
		{
			header: 'Workflow State',
			width: 100,
			dataIndex: 'endWorkflowState.name',
			filter: {searchFieldName: 'endWorkflowStateName'},
			tooltip: 'End Workflow State for this Workflow Transition'
		},
		{header: 'Workflow Status', width: 75, dataIndex: 'endWorkflowState.status.name', hidden: true, tooltip: 'End Workflow Status for this Workflow Transition'},
		{header: 'Start Date', width: 75, dataIndex: 'createDate'},
		{header: 'End Date', width: 75, dataIndex: 'endStateEndDate'},
		{header: 'Duration', width: 100, dataIndex: 'timeInEndStateFormatted'},
		{header: 'Description', width: 200, dataIndex: 'description'}
	],
	getLoadParams: function() {
		return {
			tableName: this.tableName,
			entityId: this.getEntityId()
		};
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	}
});
Ext.reg('workflow-history-grid', Clifton.workflow.HistoryGridPanel);


Clifton.workflow.Toolbar = Ext.extend(TCG.toolbar.Toolbar, {
	tableName: 'REQUIRED-TABLE-NAME',
	confirmTransition: true,
	finalState: 'SET-TO-SKIP-CHECK-FOR-NEXT-STATES', // set to true to avoid any transitions using toolbar
	finalWorkflowMessage: 'NOTE: no transitions available from final workflow state',
	enableOpenSessionInView: false,
	additionalSubmitParameters: {},
	optionalWorkflow: false, // for cases where workflow isn't required (ReconcileTradeMatch) if missing on the bean will hide the toolbar
	reloadFormPanelAfterTransition: false, // option to also reload the form after a transition (portfolio runs have warning status fields on form that need to be updated after transitions)
	hideSystemDefinedTransitions: false, // option to hide buttons for system defined transitions
	allowLocking: false, // Set to true if the entity supports being "locked" - implements SystemLockAware interface
	useLockNotes: false, // If allows locking, set to true if the entity uses lock notes
	allowTransitionOnModifiedForm: false,
	initComponent: function() {
		Clifton.workflow.Toolbar.superclass.initComponent.apply(this, arguments);
		// static items also serve as place-holders for constant height
		this.addDefaultButtons();
	},
	addDefaultButtons: function() {
		this.add({
			iconCls: 'lock',
			name: 'lock',
			tooltip: 'Lock this ' + this.tableName + '.',
			hidden: true,
			handler: this.lockUnlockEntity.createDelegate(this, [true])
		});
		this.add({xtype: 'tbseparator', name: 'lockSeparator'});
		this.add({
			iconCls: 'unlock',
			name: 'unlock',
			hidden: true,
			tooltip: 'Unlock this ' + this.tableName + '.',
			handler: this.lockUnlockEntity.createDelegate(this, [false])
		});
		this.add({xtype: 'tbseparator', name: 'unlockSeparator'});
		this.add({
			iconCls: 'pencil',
			name: 'lockNoteEdit',
			tooltip: 'Edit Lock Note',
			hidden: true,
			handler: this.lockUnlockEntity.createDelegate(this, [true], [true])
		});
		this.add({xtype: 'tbseparator', name: 'lockNoteEditSeparator'});

		this.add({
			iconCls: 'workflow',
			name: 'workflow',
			tooltip: 'Show workflow history for this entity.',
			handler: this.showWorkflowHistory.createDelegate(this)
		});
		this.add('-');
	},

	onRender: function() {
		Clifton.workflow.Toolbar.superclass.onRender.apply(this, arguments);
		this.getFormPanel().on('afterload', this.refreshWorkflowToolbar, this);
		this.addEvents({
			'afterTransition': true, // fired after transition was executed
			'afterWorkflowToolbarRefresh': true
		});
	},

	getFormPanel: function() {
		return this.ownerCt.items.get(0);
	},


	isInFinalState: function(data) {
		return (this.finalState === true) || (this.finalState === data.workflowState.name);
	},

	refreshWorkflowToolbar: function() {
		const data = this.getFormPanel().getForm().formValues;
		let lockMessage = this.refreshLockingToolbar(data);
		if (this.optionalWorkflow === true && !data.workflowState) {
			this.currentStateId = undefined;
			//Hide toolbar items - Not required and not used for this entity
			for (let i = 0; i < this.items.length; i++) {
				this.items.get(i).setVisible(false);
			}
			this.setVisible(false);
		}
		else {
			this.setVisible(true);
			if (!lockMessage && this.currentStateId === data.workflowState.id) { // no changes
				return;
			}
			if (lockMessage || this.currentStateId !== undefined) { // redraw toolbar: all but lock, unlock, and workflow buttons
				for (let i = this.items.length - 1; i >= 0; i--) {
					this.remove(this.items.get(i), true);
				}
				this.addDefaultButtons();
				lockMessage = this.refreshLockingToolbar(data);
			}
			// Don't set current state if currently locked - because after unlocking we need to redraw toolbar
			this.currentStateId = (lockMessage ? 0 : data.workflowState.id);

			// get available transitions first
			const trans = (this.isInFinalState(data) || lockMessage)
				? false
				: TCG.data.getData(`workflowTransitionNextListByEntity.json?requestedMaxDepth=3&id=${data.id}&tableName=${this.tableName}`, this);
			if (trans) {
				for (let i = 1; i < trans.length; i++) {
					const t = trans[i];
					if ((t.systemDefined && this.hideSystemDefinedTransitions) || t.hidden) {
						// Skip
					}
					else {
						this.add({
							iconCls: 'run',
							text: t.name,
							tooltip: t.description,
							disabled: t.systemDefined,
							transition: t, // save for future reference
							handler: this.executeTransition.createDelegate(this, [t])
						});
						this.add('-');
					}
				}
			}
			else {
				if (lockMessage) {
					this.add(lockMessage);
				}
				else {
					this.add(this.getFinalWorkflowMessage());
				}
			}
		}

		this.addAdditionalButtons(this);

		// if there are no items for the toolbar, we'll want to hide the whole thing here
		const visibleItem = this.items.find(function(item) {
			return !item.hidden;
		});
		this.setVisible(visibleItem);

		if (this.currentStateId) {
			this.add('->');
			this.add({text: 'Workflow Status: ', disabled: true});
			this.add({text: data.workflowStatus.name, tooltip: data.workflowStatus.description});
			this.add('-');
			this.add({text: 'Workflow State: ', disabled: true});
			this.add({text: data.workflowState.name, tooltip: data.workflowState.description});
		}
		this.doLayout();

		this.fireEvent('afterWorkflowToolbarRefresh', this);
	},

	addAdditionalButtons: function(toolbar) {
		//ability to add default buttons after dynamically generated buttons but before right hand side status
	},

	refreshLockingToolbar: function(data) {
		let lockMessage = undefined;
		// Default All to NOT Visible
		let showLock = false;
		let showUnlock = false;
		let showLockNote = false;

		if (this.allowLocking === true) {
			if (data.lockedByUser) {
				// entity is locked
				lockMessage = 'NOTE: This ' + this.tableName + ' is Locked By ' + data.lockedByUser.displayName + '. No Workflow Transitions Available.';
				if (TCG.isNotBlank(data.lockNote)) {
					lockMessage += '<br><b>Lock Note:</b>&nbsp;' + data.lockNote;
				}
				// if the entity is locked by the current user (or current user is an admin) - show unlock button
				if (data.lockedByUser.id === TCG.getCurrentUser().id || TCG.getResponseText('securityCurrentUserIsAdmin.json', this) === 'true') {
					// Unlock - Hide Lock Button and Show Unlock Button
					// ONLY allow editing lock note if locked by current user - admins can still unlock
					showLockNote = (data.lockedByUser.id === TCG.getCurrentUser().id);
					showUnlock = true;
				}
				// else hide all
			}
			else {
				// Lock - Show Lock Button and Hide Unlock Button
				showLock = true;
			}
		}
		// Else all remain hidden

		// Update Visibility
		TCG.getChildByName(this, 'lock').setVisible(showLock);
		TCG.getChildByName(this, 'lockSeparator').setVisible(showLock);
		TCG.getChildByName(this, 'lockNoteEdit').setVisible(showLockNote);
		TCG.getChildByName(this, 'lockNoteEditSeparator').setVisible(showLockNote);
		TCG.getChildByName(this, 'unlock').setVisible(showUnlock);
		TCG.getChildByName(this, 'unlockSeparator').setVisible(showUnlock);
		return lockMessage;
	},

	getFinalWorkflowMessage: function() {
		return this.finalWorkflowMessage;
	},

	getTransition: function(index) {
		return this.items.get(2 + index).transition;
	},

	executeTransition: function(transition) {
		const tb = this;
		const w = tb.getFormPanel().getWindow();
		if (w.isModified()) {
			if (this.allowTransitionOnModifiedForm === false) {
				TCG.showError('You must save the form that was modified before performing workflow transition.');
			}
			else {
				Ext.Msg.confirm('Transition - Lost Unsaved Changes', 'The form has been modified and if you continue you will lose unsaved changes. Would you like to perform the following workflow transition anyway: ' + transition.name, function(a) {
					if (a === 'yes') {
						tb.doTransition(transition);
					}
				});
			}
		}
		else {
			if (this.confirmTransition) {
				const confirmWindow = new TCG.app.Window({
					title: 'Workflow Transition - ' + transition.name,
					iconCls: 'workflow',
					width: 550,
					height: 200,
					modal: true,
					minimizable: false,
					maximizable: false,
					resizable: false,
					border: true,
					bodyStyle: 'padding: 20px 20px 0;',
					openerCt: this,

					items: [
						{xtype: 'label', html: 'Would you like to perform the following workflow transition: <b>' + transition.name + '</b><br/><br/>'},
						{xtype: 'textarea', value: transition.description, height: 60, width: 490, readOnly: true}
					],
					buttons: [
						{
							text: 'Transition',
							tooltip: 'Perform selected transition but do not close the window.',
							width: 120,
							handler: function() {
								confirmWindow.win.closeWindow();
								tb.doTransition(transition);
							}
						},
						{
							text: 'Transition and Close',
							tooltip: 'Perform selected transition and immediately close the window.',
							width: 120,
							handler: function() {
								confirmWindow.win.closeWindow();
								tb.doTransition(transition, function() {
									w.closeWindow(); // close on success only
								});

							}
						},
						{
							text: 'Cancel',
							tooltip: 'Do not perform selected transition.',
							width: 120,
							handler: function() {
								confirmWindow.win.closeWindow();
							}
						}
					],
					defaultButton: 0,
					windowOnShow: function() {
						this.focus(); // allow keyboard navigation selection: unfortunately visual highlight only happens after the user hits tab once (anywhere/anytime after our app is loaded)
					}
				});
			}
		}
	},

	validateTransition: function(entity, stateName, userScope) {
		// Override to validate item before performing transitions.
		// See trade-workflow-toolbar for a sample, which validates traders of the trade to approve.
		return '';
	},

	doTransition: async function(transition, callBack) {
		const tb = this;
		const fp = tb.getFormPanel();
		const f = fp.getForm();


		const item = f.formValues;
		const stateName = transition.endWorkflowState.name;
		const preValidationResult = await Promise.resolve(tb.validateTransition(item, stateName, fp))
			.then(validationMessage => {
				if (TCG.isNotBlank(validationMessage)) {
					return TCG.showConfirm(`Please review transition of selected item to <b>${stateName}</b>.<br/><br/>${validationMessage}<br/><br/>Would you like to bypass the above and continue?<br/>`, 'Workflow Transition Validation', {
						fn: (response, resolve) => resolve(response),
						minWidth: 600
					});
				}
				return 'yes';
			});


		if (preValidationResult === 'yes') {
			let params = this.additionalSubmitParameters ? this.additionalSubmitParameters : {};
			params = Ext.apply(params, {
				tableName: tb.tableName,
				id: f.formValues.id,
				workflowTransitionId: transition.id
			});

			if (transition.entryDetailScreenClass && TCG.isNotBlank(transition.entryDetailScreenClass)) {
				const className = transition.entryDetailScreenClass;
				let dd = {transitionParams: params, transitionBean: f.formValues};
				dd = Ext.apply(dd, Ext.decode(transition.entryDefaultData));
				const cmpId = TCG.getComponentId(className);
				TCG.createComponent(className, {
					modal: true,
					id: cmpId,
					defaultData: dd,
					openerCt: tb, // for modal window will call reloadAfterTransition() on close if transition was performed
					callBack: callBack // If clicking Transition and close can still honor the call back to close the window
				});
			}
			else {
				// Disable the toolbar to prevent long running transitions to allow the user to try to click another
				// transition button while it's still processing the last one.
				tb.setDisabled(true);
				const url = 'workflowTransitionExecuteByTransition.json' + (tb.enableOpenSessionInView === true ? '?enableOpenSessionInView=true' : '');
				const loader = new TCG.data.JsonLoader({
					waitMsg: 'Executing Transition',
					waitTarget: fp,
					params: params,
					timeout: tb.timeout ? tb.timeout : 40000,
					onLoad: function(record, conf) {
						// Re-enable the toolbar on success or failure of the transition
						tb.setDisabled(false);
						tb.reloadAfterTransition(record);
						if (callBack) {
							callBack.call(fp);
						}
					},
					onFailure: function() {
						// Re-enable the toolbar on success or failure of the transition
						tb.setDisabled(false);
						tb.reloadAfterTransitionFailure();
						return false; // return true to cancel standard processing/error message
					}
				});
				loader.load(url);
			}
		}
	},

// Called above after Transition is executed.  Also called from
// opened window when transition uses an entry window to perform some
// action and then execute the transition - returns the result here for reload
	reloadAfterTransition: function(record) {
		const tb = this;
		const fp = tb.getFormPanel();
		const f = fp.getForm();
		const w = fp.getWindow();
		tb.fireEvent('afterTransition', record);
		f.setValues(record, true);
		w.savedSinceOpen = true;
		fp.updateTitle();
		if (this.reloadFormPanelAfterTransition === true && fp.reload) {
			fp.reload();
		}
		fp.fireEvent('afterload', fp, false);
	},

// By default - will force formpanel reload if selected (PIOS runs)
	reloadAfterTransitionFailure: function() {
		const tb = this;
		const fp = tb.getFormPanel();
		if (this.reloadFormPanelAfterTransition === true && fp.reload) {
			fp.reload();
		}
		// Return false to still handle error message processing
		return false;
	},

	lockUnlockEntity: function(lock, lockNoteOnly) {
		const tb = this;
		const entityId = this.getFormPanel().getForm().formValues.id;
		if (TCG.isNull(entityId)) {
			TCG.showError(this.tableName + ' must be created first.');
		}

		let msg = 'Unlocking ';
		let url = 'systemLockAwareEntityUnlock.json';
		if (lock === true) {
			msg = 'Locking ';
			if (this.lockNoteOnly) {
				msg = 'Updating Lock Note for ';
			}
			url = 'systemLockAwareEntityLock.json';

			if (this.useLockNotes === true) {
				Ext.Msg.show({
					title: 'Lock Note',
					msg: 'Please enter a note for reasoning behind locking the selected ' + this.tableName + ':',
					width: 300,
					buttons: Ext.MessageBox.OKCANCEL,
					// Default the value to the existing lock note (if already locked and editing)
					value: TCG.getValue('lockNote', tb.getFormPanel().getForm().formValues),
					multiline: true,
					fn: function(btn, text) {
						if (btn === 'ok') {
							if (TCG.isBlank(text)) {
								TCG.showError('A note is required.', 'No Note Entered');
								return;
							}
							msg += tb.tableName;
							tb.executeLockUnlockEntity(url, entityId, msg, text);
						}
					}
				});
				return;
			}
		}
		msg += this.tableName;
		tb.executeLockUnlockEntity(url, entityId, msg);
	},
	executeLockUnlockEntity: function(url, entityId, msg, lockNote) {
		const tb = this;
		const loader = new TCG.data.JsonLoader({
			waitMsg: msg,
			waitTarget: this.getFormPanel(),
			params: {tableName: this.tableName, fkFieldId: entityId, lockNote: lockNote},
			onLoad: function(record, conf) {
				tb.reloadAfterTransition(record);
			},
			onFailure: function() {
				tb.reloadAfterTransitionFailure();
				return false; // return true to cancel standard processing/error message
			}
		});
		loader.load(url);
	},

	showWorkflowHistory: function() {
		this.doShowWorkflowHistory(this.tableName, this.getFormPanel());
	},

	doShowWorkflowHistory: function(tableName, fp) {
		const entityId = fp.getForm().formValues.id;
		if (TCG.isNull(entityId)) {
			TCG.showError(tableName + ' must be created first.');
		}
		else {
			new TCG.app.CloseWindow({
				modal: true,
				allowOpenFromModal: true,
				width: 1100,
				height: 500,
				iconCls: 'workflow',
				title: 'Workflow History for ' + (fp.getWindow().title || tableName + ' ' + entityId),
				openerCt: this,
				enableShowInfo: false,

				items: [{
					xtype: 'tabpanel',
					items: [
						{
							title: 'Workflow History',
							items: [{
								xtype: 'workflow-history-effective-grid',
								tableName: tableName,
								getEntityId: function() {
									return entityId;
								}
							}]
						},
						{
							title: tableName + ' Life Cycle',
							items: [{
								xtype: 'system-lifecycle-grid',
								tableName: tableName,
								getEntityId: function() {
									return entityId;
								}
							}]
						}
					]
				}]
			});
		}
	}
});
Ext.reg('workflow-toolbar', Clifton.workflow.Toolbar);


Clifton.workflow.HistoryEffectiveDatesGridPanel = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	name: 'workflowHistoryListFind',
	instructions: 'The following workflow history has been recorded for this entity.',
	additionalPropertiesToRequest: 'id|endWorkflowState.workflow.documentationURL',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true, defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Workflow ID', width: 25, dataIndex: 'endWorkflowState.workflow.id', hidden: true},
		{
			header: 'Workflow State',
			width: 100,
			dataIndex: 'endWorkflowState.name',
			filter: {searchFieldName: 'endWorkflowStateName'},
			tooltip: 'End Workflow State for this Workflow Transition'
		},
		{header: 'Workflow Status', width: 75, dataIndex: 'endWorkflowState.status.name', hidden: true, tooltip: 'End Workflow Status for this Workflow Transition'},

		// Boolean Flags for if the Effective Dates are different
		{header: 'Effective Start Used', dataIndex: 'effectiveStartUsed', type: 'boolean', hidden: true, filter: false},
		{header: 'Effective End Used', dataIndex: 'effectiveEndUsed', type: 'boolean', hidden: true, filter: false},

		// Actual Dates (Hidden)
		{header: 'Actual Start Date', width: 75, dataIndex: 'endStateStartDate', hidden: true},
		{header: 'Actual End Date', width: 75, dataIndex: 'endStateEndDate', hidden: true},

		// Start Date
		{
			header: 'Start Date', width: 75, dataIndex: 'effectiveEndStateStartDate',
			renderer: function(v, p, r) {
				const value = TCG.renderDate(v);
				if (r.data['effectiveStartUsed'] === true) {
					return '<div style="COLOR: #ff0000;" qtip=\'Actual Start: ' + TCG.renderDate(r.data['endStateStartDate']) + '\'>' + value + '</div>';
				}
				return value;
			}
		},

		// End Date
		{
			header: 'End Date', width: 75, dataIndex: 'effectiveEndStateEndDate',
			renderer: function(v, p, r) {
				const value = TCG.isNotBlank(v) ? TCG.renderDate(v) : TCG.renderDate(r.data['endStateEndDate']);
				if (r.data['effectiveEndUsed'] === true) {
					return '<div style="COLOR: #ff0000;" qtip=\'Actual End: ' + TCG.renderDate(r.data['endStateEndDate']) + '\'>' + value + '</div>';
				}
				return value;
			}
		},
		{header: 'Duration', width: 100, dataIndex: 'timeInEndStateFormatted', filter: false, sortable: false},
		{header: 'Description', width: 250, dataIndex: 'description'}
	],
	editor: {
		detailPageClass: 'Clifton.workflow.history.HistoryWindow',
		addEditButtons: function(t, gp) {
			t.add({
				text: 'Workflow',
				tooltip: 'Open workflow used by this entity',
				iconCls: 'workflow',
				handler: function() {
					const store = gp.grid.getStore();
					if (store.getCount() > 0) {
						const workflowId = store.getAt(0).get('endWorkflowState.workflow.id');
						const className = 'Clifton.workflow.definition.WorkflowWindow';
						const cmpId = TCG.getComponentId(className, workflowId);
						TCG.createComponent(className, {
							id: cmpId,
							params: {id: workflowId},
							openerCt: gp,
							defaultIconCls: gp.getWindow().iconCls
						});
					}
					else {
						TCG.showError('At least one workflow transition must be present in order to determine workflow.', 'No History');
					}
				}
			});
			t.add('-');
		}
	},
	getLoadParams: function() {
		return {
			tableName: this.tableName,
			entityId: this.getEntityId()
		};
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},
	getWikiPage: function(gridPanel) {
		const store = gridPanel.grid.getStore();
		let url = '';
		if (store.getCount() > 0) {
			url = TCG.getValue('json.endWorkflowState.workflow.documentationURL', store.getAt(0));
		}
		return url;
	},
	openWikiPage: function(gridPanel) {
		const url = gridPanel.getWikiPage(gridPanel);
		Clifton.workflow.showWikiPage(url);
	}
});
Ext.reg('workflow-history-effective-grid', Clifton.workflow.HistoryEffectiveDatesGridPanel);


Clifton.workflow.WorkflowAwareEntityGridEditor = Ext.extend(TCG.grid.GridEditor, {
	enableOpenSessionInView: false,
	requestedPropertiesToExclude: null,
	requestedMaxDepth: null,
	timeout: void 0, // placeholder to easily override transition execution request timeout
	executeMultipleInBatch: false, // If true and transitioning multiple - the entity IDs will all be submitted to the server for transitioning instead of unique requests per entity
	batchAsynchronous: false, // If true and executeMultipleInBatch=true, the batch will be run asynchronous so the request returns immediately
	batchInSingleTransaction: false, // If true and executeMultipleInBatch=true, all transitions will be in a single transaction; if one fails, they all fail
	batchStopOnFirstError: false, // If true and executeMultipleInBatch=true, the transitioning will stop on first error. If batchInSingleTransaction=true, all transitions will be rolled back
	batchAlwaysShowStatus: false, // If true and executeMultipleInBatch=true, the status message will always be shown if available on the response. Default is to only show status if details exist.
	skipDifferentFromStates: false, // If true and transitioning multiple - user will still get message but will continue transitioning all that are from the first from state
	excludeAutomatic: false, // Option to exclude Automatic transitions, i.e. in some cases we have the same transition one is auto with a condition and the other is manual
	// Configuration can be set at the GridEditor or GridPanel level for easier use
	// Checks the editor first, if not defined, checks the grid panel
	// Example
	// tableName: 'BillingInvoice'
	// transitionWorkflowStateList: [
	//             { stateName: 'Void', iconCls: 'cancel', buttonText: 'Void Invoice', buttonTooltip: 'Void Invoices'},
	//	           { stateName: 'Void and Create Corrected', iconCls: 'undo', buttonText: 'Void and Create Corrected Invoice', buttonTooltip: 'Void and Create Corrected Invoices'}
	//  ]

	// Can also override transitionList method on the gridPanel for custom transition support.  See Billing's InvoiceBlotter for the custom implementation example

	addEditButtons: function(toolbar, gridPanel) {
		TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

		if (gridPanel.addEditButtons) {
			gridPanel.addEditButtons(toolbar, gridPanel);
		}

		let transList = this.transitionWorkflowStateList;
		if (!transList) {
			transList = gridPanel.transitionWorkflowStateList;
		}

		if (transList && transList.length > 0) {
			for (let i = 0; i < transList.length; i++) {
				this.addTransitionToolbarButton(toolbar, transList[i], this, gridPanel);
			}
		}
	},
	addTransitionToolbarButton: function(t, state, gridEditor, gridPanel) {
		t.add({
			text: state.buttonText,
			tooltip: state.buttonTooltip,
			iconCls: state.iconCls,
			scope: this,
			handler: function() {
				const editor = gridEditor;
				const grid = gridPanel.grid;
				const sm = grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select a row to transition.', 'No Row(s) Selected');
				}
				else {
					const ut = sm.getSelections();
					if (state.requireConfirm) {
						Ext.Msg.confirm('Confirm Transition: ' + state.buttonText, 'Are you sure you want to execute "' + state.buttonTooltip + '" workflow transition?', function(a) {
							if (a === 'yes') {
								editor.transitionList(ut, state.stateName, editor, gridPanel);
							}
						});
					}
					else {
						editor.transitionList(ut, state.stateName, editor, gridPanel);
					}
				}
			}
		});
		t.add('-');
	},
	sortList: function(rows, stateName, gridEditor, gridPanel) {
		// Override for special sorting (See Trades where order of Trades submitted is important)
		return rows;
	},
	validateFromState: function(rows, stateName, gridEditor, gridPanel) {
		let errorMsg = '';
		if (rows[0].json.workflowState && rows[0].json.workflowState.name) {
			let fromState = undefined;
			let newStateName = undefined;
			for (let i = 0; i < rows.length; i++) {
				if (!fromState) {
					fromState = rows[i].json.workflowState.name;
					newStateName = this.getCorrectToStateName(fromState, stateName, rows[i]);
					if (fromState === newStateName) {
						fromState = undefined;
						rows.remove(rows[i]);
						i--;
						continue;
					}
					stateName = newStateName;
				}
				else {
					if (rows[i].json.workflowState.name === stateName) {
						rows.remove(rows[i]);
						i--;
						continue;
					}
					if (rows[i].json.workflowState.name !== fromState && fromState !== stateName) {
						if (this.skipDifferentFromStates === true) {
							errorMsg = 'Found multiple from workflow states.  Only those in ' + fromState + ' will be transitioned to [' + newStateName + '].  The rest will be skipped.';
							rows.remove(rows[i]);
							i--;
							continue;
						}
						TCG.showError('When transitioning multiple records, please keep your selection to those that are in the same workflow state. Found two records in different states [' + fromState + ', ' + rows[i].json.workflowState.name + ']', 'Invalid Selection');
						return undefined;
					}
				}
			}
		}
		if (errorMsg.length > 0) {
			TCG.showError(errorMsg, 'Invalid Selection');
		}
		return stateName;
	},
	getCorrectToStateName: function(fromState, toState, record) {
		// By Default - assumes toState is correct as is
		// See Portfolio Run - Trade Creation tab for sample where one button (Reject or Approve) can handle different transitions.
		return toState;
	},
	/**
	 * A hook for a grid to validate rows to be transitioned before transitions begin.
	 * Return a validation error message to display to the user upon failure. The result can be marked up with HTML.
	 * If validation is successful without issue, return null or an empty string.
	 *
	 * @param rows the rows to be transitioned
	 * @param stateName the state the rows will be transitioned to
	 * @param gridPanel the grid this editor applies to
	 * @returns {string} a validation error message to display to the user or null
	 */
	validateRowsToBeTransitioned: function(rows, stateName, gridPanel) {
		// Override to validate rows before performing transitions.
		// See PendingTradesGridPanel for a sample, which validates traders of selected trades to approve.
		return '';
	},
	transitionList: async function(rows, stateName, gridEditor, gridPanel) {
		rows = gridEditor.sortList(rows, stateName, gridEditor, gridPanel);
		// Ensure all selected rows are in the same state so transition will be the same for all
		// Returns stateName so for cases where one button can handle different transitions
		// based on selected from state, can use the correct one based on selections
		stateName = gridEditor.validateFromState(rows, stateName, gridEditor, gridPanel);
		// Returns undefined if invalid selection
		if (!stateName) {
			return;
		}
		// get transition
		let tableName = gridEditor.tableName;
		if (!tableName || tableName === 'OVERRIDE_ME') {
			tableName = gridPanel.tableName;
		}
		if (!tableName || tableName === 'OVERRIDE_ME') {
			TCG.showError('Missing Required tableName property', 'Missing Information');
			return;
		}
		const transition = this.findWorkflowTransition(tableName, rows[0].json.id, stateName);
		if (TCG.isNull(transition)) {
			TCG.showError('Cannot find transition to [' + stateName + '] workflow state for selected row with id = ' + rows[0].json.id, 'Illegal Workflow State');
		}
		else {
			const preValidationResult = await Promise.resolve(gridEditor.validateRowsToBeTransitioned(rows, stateName, gridPanel))
				.then(validationMessage => {
					if (TCG.isNotBlank(validationMessage)) {
						return TCG.showConfirm(`Please review transition of selected row(s) to <b>${stateName}</b>.<br/><br/>${validationMessage}<br/><br/>Would you like to bypass the above and continue?<br/>`, 'Workflow Transition Validation', {
							fn: (response, resolve) => resolve(response),
							minWidth: 600
						});
					}
					return 'yes';
				});
			if (preValidationResult === 'yes') {
				if (rows.length > 1 && gridEditor.executeMultipleInBatch === true) {
					gridEditor.transitionBatch(rows, transition, gridPanel, tableName);
				}
				else {
					// wait for transition to complete before starting next transition
					const transitionCount = 0;
					gridEditor.transitionRow(rows, transitionCount, transition, gridPanel, tableName);
				}
			}
		}
	},

	findWorkflowTransition: function(tableName, id, stateName) {
		const transitions = TCG.data.getData('workflowTransitionNextListByEntity.json?tableName=' + tableName + '&id=' + id, this);
		let transition = null;
		for (let i = 0; i < transitions.length; i++) {
			if (TCG.isEquals(stateName, transitions[i].endWorkflowState.name) && (!TCG.isTrue(this.excludeAutomatic) || TCG.isFalse(transitions[i].automatic))) {
				transition = transitions[i];
				break;
			}
		}
		return transition;
	},

	transitionRow: function(rows, transitionCount, transition, gridPanel, tableName) {
		const gridEditor = this;
		const rowId = gridEditor.getRowIdToTransition(tableName, rows, transitionCount);
		// Used for special handling overrides where this entity may be skipped here
		// Example, Billing Invoices go through the list and transition all groups first, then
		// transition individual invoices.  If the invoice group id is set that invoice is skipped for individual transition
		if (!rowId) {
			transitionCount++;
			if (transitionCount === rows.length) { // refresh after all rows were transitioned
				gridPanel.reload();
			}
			else {
				gridEditor.transitionRow(rows, transitionCount, transition, gridPanel, tableName);
			}
		}
		else {
			const loader = new TCG.data.JsonLoader({
				waitMsg: 'Executing Transition',
				waitTarget: gridPanel,
				timeout: gridEditor.timeout,
				params: {
					tableName: tableName,
					id: rowId,
					workflowTransitionId: transition.id,

					enableOpenSessionInView: gridEditor.enableOpenSessionInView,
					requestedPropertiesToExclude: gridEditor.requestedPropertiesToExclude,
					requestedMaxDepth: gridEditor.requestedMaxDepth
				},
				onLoad: function(record, conf) {
					transitionCount++;
					if (transitionCount === rows.length) { // refresh after all rows were transitioned
						gridPanel.reload();
					}
					else {
						gridEditor.transitionRow(rows, transitionCount, transition, gridPanel, tableName);
					}
				}
			});
			const url = 'workflowTransitionExecuteByTransition.json';
			loader.load(url);
		}
	},
	getRowIdToTransition: function(tableName, rows, currentIndex) {
		return rows[currentIndex].json.id;
	},

	transitionBatch: function(rows, transition, gridPanel, tableName) {
		const gridEditor = this;
		const rowIds = rows.map(row => row.json.id);
		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Executing Transitions',
			waitTarget: gridPanel,
			timeout: gridEditor.timeout,
			params: {
				tableName: tableName,
				ids: JSON.stringify(rowIds),
				workflowTransitionId: transition.id,
				asynchronous: gridEditor.batchAsynchronous,
				executeInSingleTransaction: gridEditor.batchInSingleTransaction,
				stopOnFirstError: gridEditor.batchStopOnFirstError,

				enableOpenSessionInView: gridEditor.enableOpenSessionInView,
				requestedPropertiesToExclude: gridEditor.requestedPropertiesToExclude,
				requestedMaxDepth: gridEditor.requestedMaxDepth
			},
			root: 'status',
			onLoad: function(status, conf) {
				if (status && (gridEditor.batchAlwaysShowStatus || (status.detailList && status.detailList.length > 0) || (status.errorList && status.errorList.length > 0))) {
					// show status window only if there are details to show or it is defined to show always
					TCG.createComponent('Clifton.core.StatusWindow', {
						title: 'Batch ' + tableName + ' Transition Status',
						defaultData: {status: status}
					});
				}
				gridPanel.reload();
			}
		});
		const url = 'workflowTransitionExecuteForCommand.json';
		loader.load(url);
	}
});
Ext.preg('workflowAwareEntity-grideditor', Clifton.workflow.WorkflowAwareEntityGridEditor);


Clifton.workflow.WorkflowTransitionListWindow = Ext.extend(TCG.grid.GridEditor, {
	//Override the title in implementing js
	title: 'Transition Workflow Records',
	iconCls: 'run',
	height: 200,
	width: 510,
	modal: true,
	timeout: 90000,
	tableName: undefined,
	recordIds: undefined,
	transitionOptions: undefined,
	transitionWindowHeight: 200,
	transitionWindowTitle: 'Workflow Transition',
	transitionWindowInstructions: 'Select Transition for selected records',
	transitionEntityPath: undefined, // Can be used if the entity that is being transition is a nested property (i.e. instead of id, workflowState.id, workflowState.name, it uses transitionEntityPath.id, transitionEntityPath.workflowState.id, etc)
	excludeTransitionsWithEntryScreens: false, // Can be used to exclude transitions from the list that use entry screen windows which aren't used in bulk transitions

	addBeforeTransitionButtons: function(toolbar, gridPanel) {
		//override in implementing js if needed
	},
	addAfterTransitionButtons: function(toolbar, gridPanel) {
		//override in implementing js if needed
	},
	addEditButtons: function(toolBar, gridPanel) {
		TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
		const gridEditor = this;
		gridEditor.addBeforeTransitionButtons(toolBar, gridPanel);
		toolBar.add({
			text: 'Workflow Transition',
			tooltip: 'Transition selected rows to new workflow state.  Note: All selected rows must be in the same workflow state currently.',
			iconCls: 'run',
			scope: this,
			handler: function() {
				const editor = gridEditor;
				const grid = gridPanel.grid;
				const sm = grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select at least one row to transition.', 'No Row(s) Selected');
				}
				else {
					const ut = sm.getSelections();
					editor.transitionRows(ut, editor, gridPanel);
				}
			}
		});
		toolBar.add('-');
		gridEditor.addAfterTransitionButtons(toolBar, gridPanel);
	},

	noTransitionsErrorMessage: 'Could not find any available transitions.',
	transitionRows: function(rows, editor, gridPanel) {
		// Validate all in the same current workflow state
		const recordIds = [];
		let wsId = undefined;
		let wsName = undefined;
		for (let i = 0; i < rows.length; i++) {
			const rec = rows[i].json;
			const recordId = this.transitionEntityPath ? TCG.getValue(this.transitionEntityPath + '.id', rec) : rec.id;
			if (i === 0) {
				wsId = this.transitionEntityPath ? TCG.getValue(this.transitionEntityPath + '.workflowState.id', rec) : rec.workflowState.id;
				wsName = this.transitionEntityPath ? TCG.getValue(this.transitionEntityPath + '.workflowState.name', rec) : rec.workflowState.name;
			}
			else {
				if (recordId && wsId !== this.transitionEntityPath ? TCG.getValue(this.transitionEntityPath + '.workflowState.id', rec) : rec.workflowState.id) {
					TCG.showError('When executing a workflow transition across multiple records, they all must be part of the same workflow and in the same workflow state.  Found at least 2 different [' + wsName + ', ' + this.transitionEntityPath ? TCG.getValue(this.transitionEntityPath + '.workflowState.name', rec) : rec.workflowState.name + '].');
					return;
				}
			}
			if (recordId) {
				recordIds.push(recordId);
			}
		}

		const transitionOptions = TCG.data.getData('workflowTransitionNextListByEntity.json?tableName=' + editor.tableName + '&id=' + recordIds[0], gridPanel);
		const selectableTransitionOptions = [];
		if (!transitionOptions) {
			TCG.showError(this.noTransitionsErrorMessage, 'No Available Transitions');
			return;
		}
		for (let i = 0; i < transitionOptions.length; i++) {
			const trans = transitionOptions[i];
			// No id means it returned it's current state as a possible transition, which doesn't apply
			// System Defined we skip system you can transition via UI
			if (TCG.isNotBlank(trans.id) && trans.systemDefined !== true) {
				// If there is a detail screen class - i.e. enter a note when transitioning - allows preventing bulk transitions
				if (TCG.isFalse(this.excludeTransitionsWithEntryScreens) || TCG.isBlank(trans.entryDetailScreenClass)) {
					// Array Store: name, id, description
					const selectableTrans = [trans.name, trans.id, trans.description];
					selectableTransitionOptions.push(selectableTrans);
				}
			}
		}
		if (selectableTransitionOptions.length === 0) {
			TCG.showError(this.noTransitionsErrorMessage, 'No Available Transitions');
			return;
		}

		TCG.createComponent('TCG.app.OKCancelWindow', {
			title: editor.transitionWindowTitle,
			height: editor.transitionWindowHeight,
			tableName: editor.tableName,
			recordIds: recordIds,
			transitionOptions: selectableTransitionOptions,
			openerCt: gridPanel,
			timeout: editor.timeout,
			iconCls: 'run',
			okButtonText: 'Transition',
			okButtonTooltip: 'Execute selected workflow transition',
			items: [{
				xtype: 'formpanel',
				loadValidation: false,
				instructions: editor.transitionWindowInstructions,
				labelWidth: 130,
				items: [
					{
						fieldLabel: 'Workflow Transition', name: 'transitionName', hiddenName: 'transitionId', xtype: 'combo', mode: 'local',
						displayField: 'name',
						valueField: 'id',
						tooltipField: 'description',
						store: {
							xtype: 'arraystore',
							fields: ['name', 'id', 'description'],
							data: [] //will be overridden
						},
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const fp = combo.getParentForm();
								const window = fp.getWindow();

								const data = window.transitionOptions;
								combo.getStore().loadData(data, false);
							}
						}
					}
				]
			}],
			saveForm: function(closeOnSuccess, forms, form, panel, params) {
				const win = this;
				win.closeOnSuccess = closeOnSuccess;
				win.modifiedFormCount = forms.length;

				const failures = [];
				const transitionCount = 0;
				const transition = form.findField('transitionId').getValue();
				this.transitionRecord(win.recordIds, transitionCount, transition, form, win.tableName, failures);
			},
			transitionRecord: function(recordIds, transitionCount, transitionId, form, tableName, failures) {
				const win = this;
				const recordId = recordIds[transitionCount];

				const loader = new TCG.data.JsonLoader({
					timeout: win.timeout,
					waitMsg: 'Executing Transition for Id: ' + recordId,
					waitTarget: form,
					params: {
						tableName: tableName,
						id: recordId,
						workflowTransitionId: transitionId,
						enableOpenSessionInView: true
					},
					onLoad: function(record, conf) {
						transitionCount++;
						if (transitionCount === recordIds.length) { // set saved since open and close window to refresh grid
							if (failures.length > 0) { // set saved since open and close window to refresh grid
								const data = {message: 'Some records failed to transition properly.', detailList: []};
								for (let i = 0; i < failures.length; i++) {
									data.detailList.push({category: 'Error', note: failures[i]});
								}
								TCG.createComponent('Clifton.core.StatusWindow', {
									defaultData: {status: data}
								});
							}
							win.savedSinceOpen = true;
							win.closeWindow();
						}
						else {
							win.transitionRecord(recordIds, transitionCount, transitionId, form, tableName, failures);
						}
					},
					onFailure: function(obj, result) {
						failures.push(result.message);
						this.onLoad.call(this.scope);
						return true;
					}
				});
				const url = 'workflowTransitionExecuteByTransition.json';
				loader.load(url);
			}
		});
	}
});
Ext.preg('workflow-transition-grid', Clifton.workflow.WorkflowTransitionListWindow);


Clifton.workflow.task.TaskListGridPanel = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	name: 'workflowTaskListFind',
	instructions: 'The following workflow tasks have been created for (linked to) this entity.',
	additionalPropertiesToRequest: 'priority.cssStyle',
	topToolbarSearchParameter: 'searchPattern',
	addTaskCategoryFilter: false,
	getTopToolbarFilters: function(toolbar) {
		const filters = TCG.grid.GridPanel.prototype.getTopToolbarFilters.call(this, arguments);
		if (this.addTaskCategoryFilter) {
			filters.push({fieldLabel: 'Task Category', xtype: 'toolbar-combo', width: 150, url: 'workflowTaskCategoryListFind.json', linkedFilter: 'definition.category.name'});
		}
		return filters;
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{
			header: 'Task Category',
			width: 45,
			dataIndex: 'definition.category.name',
			filter: {searchFieldName: 'categoryId', type: 'combo', url: 'workflowTaskCategoryListFind.json'},
			hidden: true
		},
		{
			header: 'Task Definition',
			width: 80,
			dataIndex: 'definition.name',
			filter: {searchFieldName: 'definitionId', type: 'combo', url: 'workflowTaskDefinitionListFind.json?linkedEntityTablePresent=true'}
		},
		{
			header: 'Name or Description', width: 90, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'},
			renderer: function(v, c, r) {
				return v || r.json.description;
			}
		},
		{header: 'Task Name', width: 100, dataIndex: 'name', hidden: true},
		{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
		{header: 'Workflow State', width: 40, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Workflow Status', width: 40, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
		{
			header: 'Assignee User',
			width: 30,
			dataIndex: 'assigneeUser.label',
			filter: {searchFieldName: 'assigneeUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'},
			hidden: true
		},
		{
			header: 'Assignee Group',
			width: 30,
			dataIndex: 'assigneeGroup.label',
			filter: {searchFieldName: 'assigneeGroupId', type: 'combo', url: 'securityGroupListFind.json'},
			hidden: true
		},
		{header: 'Assignee', width: 30, dataIndex: 'assigneeLabel', tooltip: 'Assignee User for this Task or, if not specified, Assignee User Group'},
		{
			header: 'Approver User',
			width: 30,
			dataIndex: 'approverUser.label',
			filter: {searchFieldName: 'approverUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'},
			hidden: true
		},
		{
			header: 'Approver Group',
			width: 30,
			dataIndex: 'approverGroup.label',
			filter: {searchFieldName: 'approverGroupId', type: 'combo', url: 'securityGroupListFind.json'},
			hidden: true
		},
		{header: 'Approver', width: 30, dataIndex: 'approverLabel', tooltip: 'Approver User for this Task or, if not specified, Approver User Group'},
		{
			header: 'Priority', width: 25, dataIndex: 'priority.name', filter: {searchFieldName: 'priorityId', type: 'combo', url: 'systemPriorityListFind.json'},
			renderer: function(v, c, r) {
				const style = r.json.priority.cssStyle;
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return v;
			}
		},
		{header: 'Original Due Date', width: 35, dataIndex: 'originalDueDate', hidden: true},
		{
			header: 'Due Date', width: 30, dataIndex: 'dueDate', defaultSortColumn: true, defaultSortDirection: 'DESC',
			renderer: function(v, metaData, r) {
				return Clifton.workflow.task.renderDueDate(v, metaData, r);
			}
		},
		{
			header: '<div class="checked" style="WIDTH:16px;">&nbsp;</div>',
			exportHeader: 'Completed',
			width: 12,
			tooltip: 'Completed Task',
			dataIndex: 'completedTask',
			type: 'boolean',
			nonPersistentField: true,
			renderer: function(v, c, r) {
				return TCG.renderBoolean(v, r.data.resolution);
			}
		}
	],
	getTopToolbarInitialLoadParams: function() {
		return {
			linkedEntityTableName: this.getTableName(),
			linkedEntityFkFieldId: this.getEntityId()
		};
	},
	getTableName: function() {
		return this.tableName;
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},
	editor: {
		detailPageClass: 'Clifton.workflow.task.WorkflowTaskWindow',
		allowToDeleteMultiple: true,
		addToolbarAddButton: function(toolBar, gridPanel) {
			toolBar.add(new TCG.form.ComboBox({
				name: 'taskDefinition',
				pageSize: 30,
				url: 'workflowTaskDefinitionListFind.json?active=true&taskGeneratorBeanPresent=false',
				limitRequestedProperties: false,
				width: 150,
				listWidth: 230,
				maxHeight: 550,
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						combo.store.baseParams = {
							linkedEntityTableName: gridPanel.getTableName()
						};
					}
				}
			}));
			toolBar.add({
				text: 'Add',
				tooltip: 'Create a new Workflow Task for selected Task Definition',
				iconCls: 'add',
				scope: this,
				handler: function() {
					const definition = TCG.getChildByName(toolBar, 'taskDefinition');
					const definitionId = definition.getValue();
					if (TCG.isBlank(definitionId)) {
						TCG.showError('You must first select Task Definition from the list.');
					}
					else {
						const taskLoader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							params: {
								taskDefinitionId: definitionId,
								linkedEntityId: gridPanel.getEntityId()
							},
							onLoad: function(record, conf) {
								TCG.createComponent('Clifton.workflow.task.WorkflowTaskWindow', {
									openerCt: gridPanel,
									defaultData: record
								});
							}
						});
						taskLoader.load('workflowTaskForDefinitionPopulate.json');
					}
				}
			});
			toolBar.add('-');
		}
	}
});
Ext.reg('workflow-task-grid', Clifton.workflow.task.TaskListGridPanel);


Clifton.workflow.task.AdvancedTaskListGridPanel = Ext.extend(Clifton.system.schema.GridPanelWithCustomJsonFields, {
	name: 'workflowTaskListFind',
	tableName: 'WorkflowTask',
	defaultCategoryName: undefined, // override to default a tab to specific category - i.e. 'Compliance Tasks'
	instructions: 'A workflow task represents a specific instance of a task that is being transition through corresponding workflow. For example, a specific request to update asset class targets for a specific client account. Tasks can be used to track an ad-hoc or regularly scheduled business process as well as to "lock" changes to entities	in the system and only allow changes according to corresponding business process defined via workflow and often containing 	independent approvals and reviews.',
	wikiPage: 'IT/Workflow',
	additionalPropertiesToRequest: 'priority.cssStyle',
	groupField: 'definition.category.name',
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Tasks" : "Task"]})',
	topToolbarSearchParameter: 'searchPattern',
	getTopToolbarFilters: function(toolbar) {
		const filters = TCG.grid.GridPanel.prototype.getTopToolbarFilters.call(this, arguments);
		filters.push({
			fieldLabel: 'Task Category',
			xtype: 'toolbar-combo',
			name: 'categoryId',
			width: 150,
			url: 'workflowTaskCategoryListFind.json',
			linkedFilter: 'definition.category.name'
		});
		filters.push({fieldLabel: 'View As', name: 'viewAsUserId', width: 120, xtype: 'toolbar-combo', url: 'securityUserListFind.json?disabled=false', displayField: 'label'});
		filters.push({
			fieldLabel: 'Display',
			xtype: 'combo',
			name: 'displayFilters',
			width: 140,
			minListWidth: 140,
			emptyText: 'My Open Tasks',
			forceSelection: true,
			mode: 'local',
			displayField: 'name',
			valueField: 'value',
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name', 'description'],
				data: [
					['ALL', 'All Tasks', 'Display all Workflow Tasks'],
					['ALL_OPEN', 'Open Tasks', 'Display all Workflow Tasks that have not been closed'],
					['ALL_COMPLETED', 'Open Completed', 'Display all Workflow Tasks that have been closed'],
					['MY_ALL', 'My Tasks', 'Display all Workflow Tasks that have been Assigned to me or are Approved by me (if user specific assignment is missing, group assignment will be used)'],
					['MY_OPEN', 'My Open Tasks', 'Display Workflow Tasks that have been Assigned to me or are Approved by me (if user specific assignment is missing, group assignment will be used) and have not been closed'],
					['MY_WAITING', 'My Waiting Tasks', 'Display Workflow Tasks that are waiting for me (if user specific assignment is missing, group assignment will be used) to be Completed or Approved'],
					['MY_ASSIGNEE_ALL', 'All Assigned to Me', 'Display all Workflow Tasks that have been Assigned to me (if user specific assignment is missing, group assignment will be used)'],
					['MY_ASSIGNEE_OPEN', 'Open Assigned to Me', 'Display Workflow Tasks that have been Assigned to me (if user specific assignment is missing, group assignment will be used) and have not been closed'],
					['MY_APPROVER_ALL', 'All Approved by Me', 'Display all Workflow Tasks that are Approved by me (if user specific assignment is missing, group assignment will be used)'],
					['MY_APPROVER_OPEN', 'Open Approved by Me', 'Display Workflow Tasks that are Approved by me (if user specific assignment is missing, group assignment will be used) and have not been closed']
				]
			}),
			listeners: {
				select: function(field) {
					const gp = TCG.getParentByClass(field, Ext.Panel);
					const v = field.getValue();
					const user = gp.getRunAsUser();
					gp.assigneeOrApproverUserId = undefined;
					gp.waitingForAssigneeOrApproverUserId = undefined;
					if (v === 'MY_ALL') {
						gp.clearFilter('assigneeUser.label', true);
						gp.clearFilter('approverUser.label', true);
						gp.clearFilter('completedTask', true);
						gp.assigneeOrApproverUserId = user.id;
					}
					else if (v === 'MY_OPEN') {
						gp.clearFilter('assigneeUser.label', true);
						gp.clearFilter('approverUser.label', true);
						gp.setFilterValue('completedTask', false);
						gp.assigneeOrApproverUserId = user.id;
					}
					else if (v === 'MY_WAITING') {
						gp.clearFilter('assigneeUser.label', true);
						gp.clearFilter('approverUser.label', true);
						gp.clearFilter('completedTask', true);
						gp.waitingForAssigneeOrApproverUserId = user.id;
					}
					else if (v === 'MY_ASSIGNEE_ALL') {
						gp.setFilterValue('assigneeUser.label', {'equals': {value: user.id, text: user.label}});
						gp.clearFilter('approverUser.label', true);
						gp.clearFilter('completedTask', true);
					}
					else if (v === 'MY_ASSIGNEE_OPEN') {
						gp.setFilterValue('assigneeUser.label', {'equals': {value: user.id, text: user.label}});
						gp.clearFilter('approverUser.label', true);
						gp.setFilterValue('completedTask', false);
					}
					else if (v === 'MY_APPROVER_ALL') {
						gp.setFilterValue('approverUser.label', {'equals': {value: user.id, text: user.label}});
						gp.clearFilter('assigneeUser.label', true);
						gp.clearFilter('completedTask', true);
					}
					else if (v === 'MY_APPROVER_OPEN') {
						gp.setFilterValue('approverUser.label', {'equals': {value: user.id, text: user.label}});
						gp.clearFilter('assigneeUser.label', true);
						gp.setFilterValue('completedTask', false);
					}
					else if (v === 'ALL_OPEN') {
						gp.clearFilter('assigneeUser.label', true);
						gp.clearFilter('approverUser.label', true);
						gp.setFilterValue('completedTask', false);
					}
					else if (v === 'ALL_COMPLETED') {
						gp.clearFilter('assigneeUser.label', true);
						gp.clearFilter('approverUser.label', true);
						gp.setFilterValue('completedTask', true);
					}
					else {
						gp.clearFilter('assigneeUser.label', true);
						gp.clearFilter('approverUser.label', true);
						gp.clearFilter('completedTask', true);
					}
					gp.reload();
				}
			}
		});
		return filters;
	},
	nonCustomColumns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{
			header: 'Task Category',
			width: 45,
			dataIndex: 'definition.category.name',
			filter: {searchFieldName: 'categoryId', type: 'combo', url: 'workflowTaskCategoryListFind.json'},
			hidden: true
		},
		{header: 'Task Definition', width: 100, dataIndex: 'definition.name', filter: {searchFieldName: 'definitionId', type: 'combo', url: 'workflowTaskDefinitionListFind.json'}},
		{header: 'Entity Label', width: 80, dataIndex: 'linkedEntityLabel'},
		{
			header: 'Name or Description', width: 100, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'},
			renderer: function(v, c, r) {
				return v || r.json.description;
			}
		},
		{header: 'Task Name', width: 100, dataIndex: 'name', hidden: true},
		{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
		{header: 'Workflow State', width: 40, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Workflow Status', width: 40, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}},
		{
			header: 'Assignee User',
			width: 45,
			dataIndex: 'assigneeUser.label',
			filter: {searchFieldName: 'assigneeUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'},
			hidden: true
		},
		{
			header: 'Assignee Group',
			width: 45,
			dataIndex: 'assigneeGroup.label',
			filter: {searchFieldName: 'assigneeGroupId', type: 'combo', url: 'securityGroupListFind.json'},
			hidden: true
		},
		{header: 'Assignee', width: 45, dataIndex: 'assigneeLabel', tooltip: 'Assignee User for this Task or, if not specified, Assignee User Group'},
		{
			header: 'Approver User',
			width: 45,
			dataIndex: 'approverUser.label',
			filter: {searchFieldName: 'approverUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'},
			hidden: true
		},
		{
			header: 'Approver Group',
			width: 45,
			dataIndex: 'approverGroup.label',
			filter: {searchFieldName: 'approverGroupId', type: 'combo', url: 'securityGroupListFind.json'},
			hidden: true
		},
		{header: 'Approver', width: 45, dataIndex: 'approverLabel', tooltip: 'Approver User for this Task or, if not specified, Approver User Group'},
		{
			header: 'Priority', width: 25, dataIndex: 'priority.name', filter: {searchFieldName: 'priorityId', type: 'combo', url: 'systemPriorityListFind.json'},
			renderer: function(v, c, r) {
				const style = r.json.priority.cssStyle;
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return v;
			}
		},
		{header: 'Original Due Date', width: 35, dataIndex: 'originalDueDate', hidden: true},
		{
			header: 'Due Date', width: 35, dataIndex: 'dueDate', defaultSortColumn: true,
			renderer: function(v, metaData, r) {
				return Clifton.workflow.task.renderDueDate(v, metaData, r);
			}
		},
		{
			header: '<div class="checked" style="WIDTH:16px;">&nbsp;</div>',
			exportHeader: 'Completed',
			width: 12,
			tooltip: 'Completed Task',
			dataIndex: 'completedTask',
			type: 'boolean',
			sortable: false,
			renderer: function(v, c, r) {
				return TCG.renderBoolean(v, r.data.resolution);
			}
		}
	],
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const dd = this.getWindow().defaultData;
		if (firstLoad) { // default to "My Open Tasks"
			if (dd && dd.forceReload) {
				this.clearFilters(true);
			}
			if (dd && dd.displayFilter) {
				const t = this.getTopToolbar();
				const displayFilters = TCG.getChildByName(t, 'displayFilters');
				displayFilters.setValue(dd.displayFilter);
				displayFilters.fireEvent('select', displayFilters);
			}
			else {
				this.assigneeOrApproverUserId = true;
				this.setFilterValue('completedTask', false);
			}
		}
		const params = {readUncommittedRequested: true};
		if (this.waitingForAssigneeOrApproverUserId) {
			params.waitingForAssigneeOrApproverUserId = this.getRunAsUser().id;
		}
		else if (this.assigneeOrApproverUserId) {
			params.assigneeOrApproverUserId = this.getRunAsUser().id;
		}
		if (firstLoad) {
			const grid = this;
			if (dd) {
				if (dd.createDate) {
					this.setFilterValue('createDate', {'on': dd.createDate}, false, true);
					this.setColumnHidden('createDate', false);
				}
				if (dd.updateDate) {
					this.setFilterValue('updateDate', {'on': dd.updateDate}, false, true);
					this.setColumnHidden('updateDate', false);
				}
			}
			if (grid.defaultCategoryName) {
				const catCombo = TCG.getChildByName(grid.getTopToolbar(), 'categoryId');
				if (catCombo) {
					return TCG.data.getDataPromiseUsingCaching('workflowTaskCategoryByName.json?name=' + grid.defaultCategoryName, grid, 'workflow.task.category.' + this.defaultCategoryName)
						.then(function(cat) {
							if (cat && cat.id) {
								catCombo.setValue({value: cat.id, text: cat.name});
								grid.setFilterValue(catCombo.linkedFilter, {value: cat.id, text: cat.name});
							}
							return params;
						});
				}
			}
		}
		return params;
	},
	getRunAsUser: function() {
		const u = TCG.getChildByName(this.getTopToolbar(), 'viewAsUserId');
		return TCG.isNotBlank(u.getValue()) ? {id: u.getValue(), label: u.lastSelectionText} : TCG.getCurrentUser();
	},
	editor: {
		getDetailPageClass: function(grid, row) {
			return (row) ? 'Clifton.workflow.task.WorkflowTaskWindow' : 'Clifton.workflow.task.WorkflowTaskWizardWindow';
		},
		allowToDeleteMultiple: true
	}
});
Ext.reg('workflow-task-advanced-grid', Clifton.workflow.task.AdvancedTaskListGridPanel);


Clifton.workflow.task.renderDueDate = function(v, metaData, r) {
	const data = r.data;
	if (data.completedTask !== true) {
		const today = new Date();
		today.setHours(0, 0, 0, 0);
		if (v < today) {
			metaData.css = 'ruleViolationBig';
			metaData.attr = 'qtip="This Task is Overdue"';
		}
		else if (v.getTime() === today.getTime()) {
			metaData.css = 'ruleViolation';
			metaData.attr = 'qtip="This Task is Due Today"';
		}
		else if (data.originalDueDate && data.originalDueDate.getTime() !== data.dueDate.getTime()) {
			metaData.css = 'amountAdjusted';
			metaData.attr = 'qtip="Due Date for this Task was updated. See Original Due Date and Audit Trail for details."';
		}
	}
	else if (data.originalDueDate && data.originalDueDate.getTime() !== data.dueDate.getTime()) {
		metaData.css = 'amountAdjusted';
		metaData.attr = 'qtip="Due Date for this completed Task was updated. See Original Due Date and Audit Trail for details."';
	}
	return TCG.renderDate(v);
};


// Manage workflow tasks subscription
{
	let numActiveTasks = 0;
	TCG.websocket.registerSubscription('workflow-task-count', '/user/topic/workflow/task/count', taskResponse => {
		const taskMessage = Ext.util.JSON.decode(taskResponse.body);
		if (taskMessage.reset) {
			numActiveTasks = 0;
		}
		numActiveTasks += taskMessage.numAdded;
		const bgColor = (numActiveTasks > 0) ? '#cc3333' : '#606060';
		const user = TCG.getCurrentUser();
		const msg = user.contact.label + `&nbsp;<span style="background: ${bgColor}; color: #ffffff; font-weight: bold; border-radius: 10px; text-align: center; padding: 2px 5px 2px 5px; cursor: pointer;" qtip="${numActiveTasks} Task(s) Waiting to be Completed or Approved by ${user.displayName}">${numActiveTasks}</span>`;
		Ext.fly('welcomeMessage').update(msg);
	});
}
