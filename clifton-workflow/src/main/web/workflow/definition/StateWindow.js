Clifton.workflow.definition.StateWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow State',
	iconCls: 'workflow',
	width: 750,
	height: 550,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Workflow State',
				items: [{
					xtype: 'formpanel',
					url: 'workflowState.json',
					instructions: 'A workflow state defines where an entity is in a workflow.' +
						'<ul class="c-list">' +
						'<li>Optional "Entity Update Condition" and "Entity Delete Condition" are executed on updates or deletes respectively to WorkflowAware entities and will raise a validation exception if the condition evaluates to false.</li>' +
						'<li>Entity Conditions are executed only if there is no transition being performed.  If there is a transition, the transition condition (if exists) will be used to evaluate the entity instead.</li>' +
						'</ul>',
					labelWidth: 140,
					items: [
						{fieldLabel: 'Workflow', name: 'workflow.name', xtype: 'linkfield', detailIdField: 'workflow.id', detailPageClass: 'Clifton.workflow.definition.WorkflowWindow'},
						{fieldLabel: 'State Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 120},
						{fieldLabel: 'Workflow Status', name: 'status.name', hiddenName: 'status.id', xtype: 'combo', url: 'workflowStatusListFind.json?disabled=false', detailPageClass: 'Clifton.workflow.definition.StatusWindow'},
						{fieldLabel: 'Entity Update Condition', name: 'entityUpdateCondition.name', hiddenName: 'entityUpdateCondition.id', xtype: 'system-condition-combo', qtip: 'Condition to evaluate when determining if updating a Workflow managed entity is allowed'},
						{fieldLabel: 'Entity Delete Condition', name: 'entityDeleteCondition.name', hiddenName: 'entityDeleteCondition.id', xtype: 'system-condition-combo', qtip: 'Condition to evaluate when determining if deleting a Workflow managed entity is allowed'},
						{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', minValue: 0, maxValue: 1000},
						{fieldLabel: '', name: 'disabled', xtype: 'checkbox', boxLabel: 'Disabled', tooltip: 'If checked, the state is disabled and there cannot be any transitions to or from this state. Useful for workflows that are updated, but we cannot delete the unused state because it is referenced by workflow history.'}
					]
				}]
			},

			{
				title: 'Transitions',
				items: [{
					name: 'workflowTransitionListFind',
					xtype: 'gridpanel',
					instructions: 'The following transitions exist for this workflow. Workflow transitions define valid transitions between workflow states.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Transition Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: 'true'},
						{header: 'Start State', width: 100, dataIndex: 'startWorkflowState.name'},
						{header: 'Start Status', width: 100, dataIndex: 'startWorkflowState.status.name', hidden: true},
						{header: 'End State', width: 100, dataIndex: 'endWorkflowState.name'},
						{header: 'End Status', width: 100, dataIndex: 'endWorkflowState.status.name', hidden: true},
						{header: 'Transition Condition', width: 170, dataIndex: 'condition.name', hidden: true},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int', useNull: true, defaultSortColumn: true, tooltip: 'Sort Order'},
						{header: 'Priority Order', width: 50, dataIndex: 'priorityOrder', type: 'int', useNull: true, hidden: true},
						{
							header: 'Auto', width: 50, dataIndex: 'automatic', type: 'boolean',
							tooltip: 'An automatic transition is one in which the system would move the entity from the start to end state based upon specified conditions.'
						},
						{
							header: 'Delayed', width: 50, dataIndex: 'automaticDelayed', type: 'boolean', hidden: true,
							tooltip: 'Delayed transitions are automatic transitions which do not execute until the end of the current request being handled. Otherwise, automatic transitions execute during the initial entity save. This adjustment to execution order has the advantage of allowing the transition to take into account all other actions that have taken place during the request, such as other entity saves in a multi-save request. Additionally, a failure in a delayed transition will not roll-back all actions prior to that transition as they would be for standard automatic transitions.'
						},
						{
							header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean',
							tooltip: 'System define transitions cannot be triggered by users. They must be executed by the system.'
						},
						{
							header: 'Read', width: 50, dataIndex: 'readAccessSufficientToExecute', type: 'boolean', hidden: true,
							tooltip: 'Read Access Sufficient To Execute.  By default, users must have WRITE access to the table of the entity they are transitioning.  If checked (rare), users are allowed to execute with READ access.  Conditions can be used to further restrict who can execute a specific transition.'
						},
						{
							header: 'Not Transactional', width: 50, dataIndex: 'notExecutedInTransaction', type: 'boolean', hidden: true,
							tooltip: 'By default all transitions are executed in a transaction (including corresponding actions and auto-transitions). In rare cases you want to disable this to prevent database locking (workflow transition with an action that generates a report that needs the same row).'
						},
						{header: 'UI Screen', width: 50, dataIndex: 'entryDetailScreenClassPopulated', type: 'boolean', hidden: true},
						{header: 'Actions', width: 50, dataIndex: 'actionsExist', type: 'boolean'},
						{header: 'Entry Screen', tooltip: 'A detail screen class is defined which will open when the transition is selected for an entity from the workflow toolbar', width: 100, dataIndex: 'entryDetailScreenClass', hidden: true},
						{header: 'Default Data', tooltip: 'Default data for the entry detail screen', width: 100, dataIndex: 'entryDefaultData', hidden: true}

					],
					editor: {
						detailPageClass: 'Clifton.workflow.transition.TransitionWindow',
						getDefaultData: function(gridPanel) {
							return {
								endWorkflowState: {
									workflow: TCG.getValue('workflow', gridPanel.getWindow().getMainForm().formValues)
								}
							};
						}
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 200, minListWidth: 150, mode: 'local', value: 'ALL', displayField: 'name', valueField: 'value',
								store: new Ext.data.ArrayStore({
									fields: ['value', 'name'],
									data: [['START', 'Transitions From State'], ['END', 'Transitions To State'], ['ALL', 'All Transitions To/From State']]
								})

							}
						];

					},
					getLoadParams: function(firstLoad) {
						const displayType = TCG.getChildByName(this.getTopToolbar(), 'displayType').getValue();
						if (displayType === 'START') {
							return {'startWorkflowStateId': this.getWindow().getMainFormId()};
						}
						else if (displayType === 'END') {
							return {'endWorkflowStateId': this.getWindow().getMainFormId()};
						}
						else {
							return {'startOrEndWorkflowStateId': this.getWindow().getMainFormId()};
						}
					}
				}]
			}

		]
	}]

});
