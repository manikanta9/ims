Clifton.workflow.definition.WorkflowWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Details',
	iconCls: 'workflow',
	width: 950,
	height: 450,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Workflow',
				items: [{
					xtype: 'formpanel',
					instructions: 'A workflow defines the life-cycle for a given entity (database row).  It is a collection of states and transitions that define valid flows from one workflow state to the next. A workflow should not be active until it is setup and ready for use.',
					url: 'workflow.json',
					labelWidth: 140,
					getWarningMessage: function(f) {
						return f.findField('active').getValue() ? undefined : 'To enable this workflow you must check the <i>Active</i> checkbox.';
					},
					items: [
						{fieldLabel: 'Workflow Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Documentation URL', xtype: 'compositefield',
							items: [
								{name: 'documentationURL', xtype: 'textfield', flex: 1},
								{
									xtype: 'button', iconCls: 'help', width: 30,
									handler: function() {
										const fp = TCG.getParentFormPanel(this);
										Clifton.workflow.showWikiPage(fp.getFormValue('documentationURL'));
									}
								}
							]
						},
						{boxLabel: 'Active', name: 'active', xtype: 'checkbox'},
						{
							boxLabel: 'Save History End Date', name: 'endDateSavedOnHistory', xtype: 'checkbox',
							qtip: 'As transitions are performed and workflow history is recorded, a workflow history record is inserted with the start date and time that the entity was put into that workflow state.  The previous history record is also retrieved to set the end date on it.  If this option is unchecked, then only new history records are inserted and previous history records are not updated. (These can be filled in later with a batch job).  Turning this off can be ideal for high volume entities that go through multiple workflow transitions.'
						}
					]
				}]
			},


			{
				title: 'States',
				items: [{
					name: 'workflowStateListFind',
					xtype: 'gridpanel',
					instructions: 'The following states are tied to this workflow.  At any given time, an entity can only be in one state.  The only way that an entity can go from one state to another is by executing a valid workflow transition.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'State Name', width: 130, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Workflow Status', width: 90, dataIndex: 'status.name'},
						{header: 'Entity Update Condition', width: 170, dataIndex: 'entityUpdateCondition.name'},
						{header: 'Entity Delete Condition', width: 170, dataIndex: 'entityDeleteCondition.name'},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int', useNull: true, defaultSortColumn: true},
						{header: 'Disabled', width: 50, dataIndex: 'disabled', type: 'boolean'}
					],
					editor: {
						deleteURL: 'workflowStateDelete.json',
						detailPageClass: 'Clifton.workflow.definition.StateWindow',
						getDefaultData: function(gridPanel) {
							return {
								workflow: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('disabled', false);
						}
						return {'workflowId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Transitions',
				items: [{
					name: 'workflowTransitionListByWorkflow',
					xtype: 'gridpanel',
					instructions: 'The following transitions exist for this workflow. Workflow transitions define valid transitions between workflow states.',
					viewNames: ['Default View', 'Entry Screen'],
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Transition Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: 'true'},
						{header: 'Start State', width: 100, dataIndex: 'startWorkflowState.name'},
						{header: 'Start Status', width: 100, dataIndex: 'startWorkflowState.status.name', hidden: true},
						{header: 'End State', width: 100, dataIndex: 'endWorkflowState.name'},
						{header: 'End Status', width: 100, dataIndex: 'endWorkflowState.status.name', hidden: true},
						{header: 'Transition Condition', width: 170, dataIndex: 'condition.name', viewNames: ['Default View']},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int', useNull: true, hidden: true, defaultSortColumn: true, tooltip: 'Sort Order'},
						{header: 'Priority Order', width: 50, dataIndex: 'priorityOrder', type: 'int', useNull: true, hidden: true},
						{
							header: 'Auto', width: 50, dataIndex: 'automatic', type: 'boolean', viewNames: ['Default View'],
							tooltip: 'An automatic transition is one in which the system would move the entity from the start to end state based upon specified conditions.'
						},
						{
							header: 'Delayed', width: 50, dataIndex: 'automaticDelayed', type: 'boolean', hidden: true,
							tooltip: 'Delayed transitions are automatic transitions which do not execute until the end of the current request being handled. Otherwise, automatic transitions execute during the initial entity save. This adjustment to execution order has the advantage of allowing the transition to take into account all other actions that have taken place during the request, such as other entity saves in a multi-save request. Additionally, a failure in a delayed transition will not roll-back all actions prior to that transition as they would be for standard automatic transitions.'
						},
						{
							header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean', viewNames: ['Default View'],
							tooltip: 'System define transitions cannot be triggered by users. They must be executed by the system.'
						},
						{
							header: 'Read', width: 50, dataIndex: 'readAccessSufficientToExecute', type: 'boolean', hidden: true,
							tooltip: 'Read Access Sufficient To Execute.  By default, users must have WRITE access to the table of the entity they are transitioning.  If checked (rare), users are allowed to execute with READ access.  Conditions can be used to further restrict who can execute a specific transition.'
						},
						{
							header: 'Not Transactional', width: 50, dataIndex: 'notExecutedInTransaction', type: 'boolean', hidden: true,
							tooltip: 'By default all transitions are executed in a transaction (including corresponding actions and auto-transitions). In rare cases you want to disable this to prevent database locking (workflow transition with an action that generates a report that needs the same row).'
						},
						{header: 'UI Screen', width: 50, dataIndex: 'entryDetailScreenClassPopulated', type: 'boolean', viewNames: ['Default View']},
						{header: 'Actions', width: 50, dataIndex: 'actionsExist', type: 'boolean', viewNames: ['Default View']},
						{header: 'Entry Screen', tooltip: 'A detail screen class is defined which will open when the transition is selected for an entity from the workflow toolbar', width: 100, dataIndex: 'entryDetailScreenClass', viewNames: ['Entry Screen'], hidden: true},
						{header: 'Default Data', tooltip: 'Default data for the entry detail screen', width: 100, dataIndex: 'entryDefaultData', viewNames: ['Entry Screen'], hidden: true}
					],
					editor: {
						deleteURL: 'workflowTransitionDelete.json',
						detailPageClass: 'Clifton.workflow.transition.TransitionWindow',
						getDefaultData: function(gridPanel) {
							return {
								endWorkflowState: {
									workflow: gridPanel.getWindow().getMainForm().formValues
								}
							};
						}
					},
					getLoadParams: function() {
						return {'workflowId': this.getWindow().getMainFormId()};
					},
					getWikiPage: function(gridPanel) {
						const url = gridPanel.getWindow().getMainFormPanel().getFormValue('documentationURL');
						return TCG.isBlank(url) ? false : url;
					},
					openWikiPage: function(gridPanel) {
						const url = gridPanel.getWikiPage(gridPanel);
						Clifton.workflow.showWikiPage(url);
					}
				}]
			},


			{
				title: 'Assignments',
				items: [{
					name: 'workflowAssignmentListByWorkflow',
					xtype: 'gridpanel',
					instructions: 'A workflow assignment links a workflow to a given table. When a WorkflowAware entity is created for the table, the assignment will determine the workflow it will follow.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Table', width: 100, dataIndex: 'table.name'},
						{header: 'Workflow', width: 150, dataIndex: 'workflow.name', hidden: true},
						{header: 'Assignment Condition', width: 250, dataIndex: 'condition.name'},
						{header: 'Active', width: 50, dataIndex: 'workflow.active', type: 'boolean', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.workflow.assignment.AssignmentWindow',
						deleteURL: 'workflowAssignmentDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								workflow: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {'workflowId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'Workflow'
				}]
			}

		]
	}]

});
