Clifton.workflow.definition.StatusWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Status',
	iconCls: 'workflow',

	items: [{
		xtype: 'formpanel',
		url: 'workflowStatus.json',
		instructions: 'Workflow status defines a generic classification for various workflow states, i.e. Open, Closed. Each workflow state is always associated with a workflow status. Multiple states can share the same status. Example of status are Open, Closed, On Hold, etc. Status are shared across all workflows and can be used for simple categorization of entities. Status can also be used to determine actions allowed for a given entity, i.e. (cannot edit Closed entities).',
		labelWidth: 120,
		items: [
			{fieldLabel: 'Status Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
		]
	}]
});
