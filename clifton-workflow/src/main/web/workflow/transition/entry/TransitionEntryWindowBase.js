Clifton.workflow.transition.entry.TransitionEntryWindowBase = Ext.extend(TCG.app.DetailWindow, {
	okButtonText: 'Save and Execute Transition',
	okButtonTooltip: 'Save your changes (if any) and execute transition',
	cancelButtonText: 'Cancel',
	cancelButtonTooltip: 'Close this window without saving or executing the transition',
	applyButtonText: 'Execute Transition Only',
	applyButtonTooltip: 'Do not save but still execute the transition',
	applyDisabled: false,
	forceModified: true, // always considers all forms as modified
	doNotWarnOnCloseModified: true // use true if don't want to warn when closing a modified window
});
