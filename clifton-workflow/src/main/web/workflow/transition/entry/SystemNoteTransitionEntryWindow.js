TCG.use('Clifton.system.note.NoteWindowBase');
TCG.use('Clifton.system.note.SingleNoteWindow');


Clifton.workflow.transition.entry.SystemNoteTransitionEntryWindow = Ext.extend(Clifton.system.note.SingleNoteWindow, {
	callBack: undefined, // When transition and close - call back can be used to close the original window - defined by the opener

	init: function() {
		// change defaultData to a method so can convert to each forms default data from the transition
		this.transitionParams = this.defaultData.transitionParams;
		const dd = this.defaultData;
		this.defaultData = this.getDefaultData(dd);

		Clifton.workflow.transition.entry.SystemNoteTransitionEntryWindow.superclass.init.apply(this, arguments);

		this.win.closeWindow = this.win.closeWindow.createInterceptor(this.beforeCloseWindowCall);
		this.win.saveForm = this.win.saveForm.createSequence(this.executeTransition);
	},

	getDefaultData: function(dd) {
		let tableName = dd.tableName;
		if (!tableName) {
			tableName = dd.transitionParams.tableName;
		}

		let fkId = dd.transitionParams.id;
		if (dd.noteEntityIdParam) {
			fkId = TCG.getValue(dd.noteEntityIdParam, dd);
		}

		const tbl = TCG.data.getData('systemTableByName.json?tableName=' + tableName, this, 'system.table.' + tableName);
		let nt = {table: tbl};
		if (dd.noteTypeName) {
			nt = TCG.data.getData('systemNoteTypeByTableAndName.json?tableId=' + tbl.id + '&noteTypeName=' + dd.noteTypeName, this);
		}
		return {
			noteType: nt,
			fkFieldId: fkId
		};
	},

	executeTransition: function() {
		const transitionParams = this.transitionParams;

		const win = this;
		const fp = this.getMainFormPanel();
		const url = 'workflowTransitionExecuteByTransition.json';
		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Executing Transition',
			waitTarget: fp,
			params: transitionParams,
			onLoad: function(record, conf) {
				win.openerCt.reloadAfterTransition(record);
				if (win.callBack) {
					win.callBack.call(fp);
				}
			}
		});
		loader.load(url);
	},

	beforeCloseWindowCall: function() {
		// not used right now
		// before close window call - if not saved since open check if user still wants to perform transition!!;
	},

	setTitle: function(title, iconCls) {
		title = 'Workflow Transition Entry';
		TCG.Window.superclass.setTitle.call(this, title, iconCls);
	}
});


