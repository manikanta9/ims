Clifton.workflow.transition.TransitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Transition',
	iconCls: 'workflow',
	width: 650,
	height: 600,

	items: [{
		xtype: 'formpanel',
		url: 'workflowTransition.json',
		instructions: 'A transition defines the flow from one workflow state to another, under what circumstances that move can happen, and additional actions that should be performed.',
		labelWidth: 120,

		items: [
			{name: 'endWorkflowState.workflow.id', xtype: 'hidden', submitValue: false},
			{fieldLabel: 'Transition Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true, height: 50},
			{
				fieldLabel: 'Start State', name: 'startWorkflowState.name', hiddenName: 'startWorkflowState.id', xtype: 'combo', url: 'workflowStateListFind.json', detailPageClass: 'Clifton.workflow.definition.StateWindow',
				beforequery: function(queryEvent) {
					queryEvent.combo.store.setBaseParam('workflowId', queryEvent.combo.getParentForm().getForm().findField('endWorkflowState.workflow.id').getValue());
				}
			},
			{
				fieldLabel: 'End State', name: 'endWorkflowState.name', hiddenName: 'endWorkflowState.id', xtype: 'combo', url: 'workflowStateListFind.json', detailPageClass: 'Clifton.workflow.definition.StateWindow',
				beforequery: function(queryEvent) {
					queryEvent.combo.store.setBaseParam('workflowId', queryEvent.combo.getParentForm().getForm().findField('endWorkflowState.workflow.id').getValue());
				}
			},
			{fieldLabel: 'Transition Condition', name: 'condition.name', hiddenName: 'condition.id', xtype: 'system-condition-combo', qtip: 'Optional System Condition that must evaluate to true in order to execute this Transition'},
			{fieldLabel: 'Sort Order', name: 'order', xtype: 'integerfield', qtip: 'Used for display purposes only to show transitions in logical order. See "Priority Order" for execution order configuration.'},
			{fieldLabel: 'Priority Order', name: 'priorityOrder', xtype: 'integerfield', doNotClearIfRequiredSet: true, requiredFields: ['automatic'], qtip: 'For rare cases where more than one automatic transition can apply, priority order can be used for first one wins (smallest order).  Should be used in rare cases where multiple automatic transitions can occur and setting up conditions can be difficult.'},
			{boxLabel: 'Automatic transition that is executed immediately after start state', name: 'automatic', xtype: 'checkbox'},
			{boxLabel: 'Delay automatic transitions until response serialization', name: 'automaticDelayed', xtype: 'checkbox', requiredFields: ['automatic'], qtip: 'When enabled, automatic transitions will delayed, queuing up all automatic transitions during request handling. Once the request handler has completed, all queued transitions will be executed together. This is useful in such cases as trade validation where the transition from "Draft" to "Validated" should wait until <i>all</i> trades from the current request have been saved and are available during validation logic.'},
			{boxLabel: 'This transition cannot be executed from UI (only by the system)', name: 'systemDefined', xtype: 'checkbox'},
			{boxLabel: 'This transition cannot be viewed from UI', name: 'hidden', xtype: 'checkbox', qtip: 'This flag indicates the transitions is hidden and not executable by the user from the UI. It differs from the system defined flag because it does not require system execution. There are rare cases where an automatic transition should not be seen from the UI as there are alternative manual transitions.'},
			{
				boxLabel: 'Read Access Sufficient to Execute this Transition', name: 'readAccessSufficientToExecute', xtype: 'checkbox',
				qtip: 'By default, users must have WRITE access to the table of the entity they are transitioning.  If checked (rare), users are allowed to execute with READ access.  Conditions can be used to further restrict who can execute a specific transition.'
			},
			{boxLabel: 'Do not execute this transition in a database transaction', name: 'notExecutedInTransaction', xtype: 'checkbox'},
			{
				xtype: 'fieldset-checkbox',
				title: 'UI Detail Screen',
				checkboxName: 'useTransitionEntryScreen',
				instructions: 'Transition entry screen is a window that opens when the user clicks a transition in the workflow toolbar.  These windows open and allow users to enter additional information prior to performing the transition.  For example, users can enter a note.  The entry screen cannot be used for automatic or system defined transitions.',
				items: [
					{fieldLabel: 'Detail Screen Class', name: 'entryDetailScreenClass', mutuallyExclusiveFields: ['automatic', 'systemDefined'], requiredFields: ['startWorkflowState.id', 'endWorkflowState.id']},
					{fieldLabel: 'Default Data', name: 'entryDefaultData', qtip: 'Default data for the detail screen. For example, for a Note window can default to use specific note type.', requiredFields: ['entryDetailScreenClass']}
				]
			},
			{
				xtype: 'fieldset-checkbox',
				title: 'Actions',
				checkboxName: 'actionsExist',
				instructions: 'Workflow Actions are actions that a workflow transition may trigger.  Actions are usually triggered by a transition and are defaulted to run after transitions occur (i.e. send an email, process another bean, etc.), however actions can be performed before transitioning an entity as well (usually involves validation)',
				items: [{
					xtype: 'formgrid',
					storeRoot: 'actionList',
					dtoClass: 'com.clifton.workflow.transition.WorkflowTransitionAction',
					columnsConfig: [
						{
							header: 'Action System Bean', width: 300, dataIndex: 'actionSystemBean.name', idDataIndex: 'actionSystemBean.id',
							editor: {xtype: 'combo', url: 'systemBeanListFind.json?groupName=Workflow Transition Action', allowBlank: false, detailPageClass: 'Clifton.system.bean.BeanWindow'}
						},
						{header: 'Before', width: 70, dataIndex: 'beforeTransition', type: 'boolean', editor: {xtype: 'checkbox'}},
						{
							header: 'Separate Transaction', width: 70, dataIndex: 'separateTransaction', type: 'boolean', editor: {xtype: 'checkbox'},
							tooltip: 'Specifies whether this action should be executed outside of the transaction that performs the transition or not. If it is executed outside and before and transition fails, then the action will not be rolled back. If it is executed outside and after and the action fails, then the transition will still be committed.'
						},
						{header: 'Order', width: 70, dataIndex: 'order', type: 'int', editor: {xtype: 'spinnerfield', minValue: 1, maxValue: 100, allowBlank: false}}
					]
				}]
			}
		]
	}]
});
