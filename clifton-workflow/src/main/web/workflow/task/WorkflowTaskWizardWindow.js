Clifton.workflow.task.WorkflowTaskWizardWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Workflow Task Wizard',
	iconCls: 'task',
	okButtonText: 'Create',
	width: 700,
	height: 300,

	items: [{
		xtype: 'formpanel',
		instructions: 'Select the Task Definition for the task that you would like to create. For tasks that are linked to an entity, you will also need to select the corresponding entity.',
		labelWidth: 140,
		items: [
			{
				fieldLabel: 'Task Definition', name: 'definition.name', hiddenName: 'definition.id', xtype: 'combo', displayField: 'labelLong', limitRequestedProperties: false, url: 'workflowTaskDefinitionListFind.json?active=true&taskGeneratorBeanPresent=false', detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow', allowBlank: false,
				listeners: {
					beforeselect: function(combo, record) {
						const fp = combo.getParentForm();
						const f = fp.getForm();
						const data = record.json;
						f.formValues = {definition: data};
						let o = f.findField('linkedEntityLabel');
						if (o) {
							fp.remove(o);
						}
						if (data.linkedEntityTable) {
							const url = data.linkedEntityListURL;
							fp.add({fieldLabel: data.linkedEntityTable.label, name: 'linkedEntityLabel', hiddenName: 'linkedEntityFkFieldId', xtype: 'combo', displayField: 'label', url: url, loadAll: !TCG.isUrlWithPagination(url), allowBlank: false});
							try {
								fp.doLayout();
							}
							catch (e) {
								// strange error due to removal of elements with allowBlank = false
							}
							if (data.linkedEntityFkFieldId) {
								o = f.findField('linkedEntityLabel');
								o.setValue({value: data.linkedEntityFkFieldId, text: data.linkedEntityLabel});
							}
						}
					}
				}
			}
		]
	}],

	saveWindow: function(closeOnSuccess, forceSubmit) {
		const w = this;
		const fp = w.getMainFormPanel();
		if (TCG.isNotNull(fp.getFirstInValidField())) {
			TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
		}
		else {
			const definition = fp.getFormValue('definition');
			const params = {taskDefinitionId: definition.id};
			if (definition.linkedEntityTable) {
				const o = fp.getForm().findField('linkedEntityFkFieldId').getValueObject();
				params.linkedEntityId = o.id;
			}

			const taskLoader = new TCG.data.JsonLoader({
				waitTarget: fp,
				params: params,
				onLoad: function(record, conf) {
					TCG.createComponent('Clifton.workflow.task.WorkflowTaskWindow', {
						openerCt: w.openerCt,
						defaultData: record
					});
					w.closeWindow();
				}
			});
			taskLoader.load('workflowTaskForDefinitionPopulate.json');
		}
	}
});
