Clifton.workflow.task.WorkflowTaskDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Task Definition',
	iconCls: 'task',
	width: 1000,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'workflowTaskDefinition.json',
					getSaveURL: function() {
						return 'workflowTaskDefinitionSave.json?requestedMaxDepth=4';
					},
					labelWidth: 165,

					updateFieldVisibility: function(formPanel) {
						if (TCG.isFalse(formPanel.getFormValue('category.definitionLockingAllowed'))) {
							TCG.getChildByName(formPanel, 'lockingConfiguration').hide();
						}
						else {
							TCG.getChildByName(formPanel, 'lockingConfiguration').show();
						}
						if (TCG.isFalse(formPanel.getFormValue('category.generateOnEntityCreationAllowed'))) {
							formPanel.hideField('generateOnEntityCreation');
						}
						else {
							formPanel.showField('generateOnEntityCreation');
						}
					},
					listeners: {
						afterload: function(formPanel) {
							formPanel.updateFieldVisibility(formPanel);
						}
					},

					items: [
						{
							fieldLabel: 'Task Category', name: 'category.name', hiddenName: 'category.id', xtype: 'combo', url: 'workflowTaskCategoryListFind.json', limitRequestedProperties: false, detailPageClass: 'Clifton.workflow.task.WorkflowTaskCategoryWindow',
							listeners: {
								select: function(combo, record) {
									const fp = combo.getParentForm();
									const f = fp.getForm();
									f.formValues = f.formValues || {};
									f.formValues.category = record.json;
									fp.updateFieldVisibility(fp);
								}
							}
						},
						{fieldLabel: 'Definition Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true, height: 50},
						{fieldLabel: 'Active', name: 'active', xtype: 'checkbox'},

						{xtype: 'sectionheaderfield', header: 'Workflow Configuration'},
						{fieldLabel: 'Task Workflow', name: 'workflow.name', hiddenName: 'workflow.id', xtype: 'combo', url: 'workflowListFind.json?assignmentTableName=WorkflowTask', detailPageClass: 'Clifton.workflow.definition.WorkflowWindow', qtip: 'Each Workflow Task of this definition will transition through this workflow.'},
						{
							fieldLabel: 'Final Workflow Status', name: 'finalWorkflowStatus.name', hiddenName: 'finalWorkflowStatus.id', xtype: 'combo', url: 'workflowStatusListFind.json', detailPageClass: 'Clifton.workflow.definition.StatusWindow',
							requiredFields: ['workflow.name'],
							qtip: 'When a task is completed, it transitions into this "Final" Workflow Status. Workflow tasks in this workflow status are considered done and are no longer used to determine if an entity is locked. There cannot be more than one non final tasks for the same entity at the same time',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									combo.clearAndReset();
									combo.store.baseParams = {
										workflowId: fp.getFormValue('workflow.id', true)
									};
								}
							}
						},
						{
							fieldLabel: 'Assignee Workflow Status', name: 'assigneeWorkflowStatus.name', hiddenName: 'assigneeWorkflowStatus.id', xtype: 'combo', url: 'workflowStatusListFind.json', detailPageClass: 'Clifton.workflow.definition.StatusWindow',
							requiredFields: ['workflow.name'],
							qtip: 'When a task is in this Workflow Status, then it is waiting on the Assignee to do the work. Usually this is "Open" workflow status. This field can be used to find assignee to do list: (Assignee = User AND Workflow Status = Assignee Workflow Status) and approver\'s to do list: (Approver = User AND Workflow Status NOT IN (Assignee Workflow Status, Final Workflow Status)',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									combo.clearAndReset();
									combo.store.baseParams = {
										workflowId: fp.getFormValue('workflow.id', true)
									};
								}
							}
						},

						{xtype: 'sectionheaderfield', header: 'Approver and Assignee User Configuration'},
						{fieldLabel: 'Task Assignee User', name: 'assigneeUser.label', hiddenName: 'assigneeUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json?disabled=false', qtip: 'Optional user that all tasks of this definition should be assigned to. When set, this user will be used to default task Assignee during task creation.'},
						{fieldLabel: 'Task Assignee Group', name: 'assigneeGroup.label', hiddenName: 'assigneeGroup.id', xtype: 'combo', displayField: 'label', url: 'securityGroupListFind.json?disabled=false', qtip: 'Optional user group that all tasks of this definition should be assigned to. When set, this user group will be used to default task Assignee during task creation.'},
						{fieldLabel: '', boxLabel: 'Require "Task Assignee User" to belong to "Task Assignee Group"', name: 'assigneeRequiredToBeInAssigneeGroup', xtype: 'checkbox', requiredFields: ['assigneeGroup.id']},

						{
							fieldLabel: 'Approver Retriever', name: 'approverUserRetrieverBean.name', hiddenName: 'approverUserRetrieverBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Workflow Task Approver Retriever', detailPageClass: 'Clifton.system.bean.BeanWindow',
							qtip: 'Optional System Bean that can retrieve Approver for the task based con custom logic. This logic takes precedence over "Task Approver" and "Secondary Approver" user fields.',
							getDefaultData: function() {
								return {type: {group: {name: 'Workflow Task Approver Retriever'}}};
							}
						},
						{fieldLabel: 'Task Approver User', name: 'approverUser.label', hiddenName: 'approverUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json?disabled=false', qtip: 'Optional user that is responsible for approving/reviewing corresponding workflow tasks. When set, this field will be used to default task Approver during task creation.', detailPageClass: 'Clifton.security.user.UserWindow', disableAddNewItem: true},
						{fieldLabel: 'Secondary Approver User', name: 'secondaryApproverUser.label', hiddenName: 'secondaryApproverUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json?disabled=false', qtip: 'Optional user that will be used when the Assignee is the same person as primary Approver.', requiredFields: ['approverUser.label'], detailPageClass: 'Clifton.security.user.UserWindow', disableAddNewItem: true},
						{fieldLabel: 'Task Approver Group', name: 'approverGroup.label', hiddenName: 'approverGroup.id', xtype: 'combo', displayField: 'label', url: 'securityGroupListFind.json?disabled=false', qtip: 'Optional user group that is responsible for approving/reviewing corresponding workflow tasks. When set, this field will be used to default task Approver during task creation.', detailPageClass: 'Clifton.security.user.UserWindow', disableAddNewItem: true},
						{fieldLabel: '', boxLabel: 'Require "Task Approver User" to belong to "Task Approver Group"', name: 'approverRequiredToBeInApproverGroup', xtype: 'checkbox', requiredFields: ['approverGroup.id']},
						{fieldLabel: '', boxLabel: 'Allow "Task Assignee" and "Task Approver" to be the same person', name: 'sameAssigneeAndApproverAllowed', xtype: 'checkbox', qtip: 'It is generally considered to be bad practice to allow Approve and Assignee to be the same person. However, in rare temporary instances (there is only one person who can do the job) this may be allowed.'},

						{xtype: 'sectionheaderfield', header: 'Task Configuration'},
						{fieldLabel: 'Task Priority', name: 'priority.name', hiddenName: 'priority.id', xtype: 'combo', url: 'systemPriorityListFind.json'},
						{fieldLabel: 'Task Modify Condition', name: 'taskEntityModifyCondition.name', hiddenName: 'taskEntityModifyCondition.id', xtype: 'system-condition-combo', qtip: 'Optionally restrict who can create and modify tasks for this definition. This condition can also restrict who is allowed to be Assignee and Approver for the task, etc.<br/><b>NOTE: this condition will not apply to Admin users.</b>'},
						{fieldLabel: 'Max Due Date Days', name: 'generateDueDateDaysFromToday', xtype: 'spinnerfield', minValue: 0, qtip: 'When specified, will not allow for a new task to be generated with Due Date that is more than this positive number of days into the future. This option is used both during task creation and during automated task generation.'},
						{fieldLabel: '', boxLabel: 'Allow changes to Due Date (Original Due Date field can never be modified)', name: 'changeToDueDateAllowed', xtype: 'checkbox'},
						{fieldLabel: '', boxLabel: 'Allow Task Steps for Tasks of this Definition', name: 'taskStepAllowed', xtype: 'checkbox'},
						{fieldLabel: '', boxLabel: 'Allow "Task Name" field for all Tasks of this Task Definition (hide if unchecked)', name: 'taskNameAllowed', xtype: 'checkbox'},
						{fieldLabel: '', boxLabel: 'Require "Task Name" field for all Tasks of this Task Definition', name: 'taskNameRequired', xtype: 'checkbox'},
						{fieldLabel: '', boxLabel: 'Require "Description" field for all Tasks of this Task Definition', name: 'taskDescriptionRequired', xtype: 'checkbox'},

						{xtype: 'sectionheaderfield', header: 'Auto Generation Configuration'},
						{
							fieldLabel: 'Task Generator Bean', name: 'taskGeneratorBean.name', hiddenName: 'taskGeneratorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Workflow Task Generator', detailPageClass: 'Clifton.system.bean.BeanWindow',
							qtip: 'For ad-hoc tasks, this field will be blank. It will be populated for periodic and other task types and will reference a System Bean that contains the logic used to generate corresponding tasks and steps.',
							getDefaultData: function() {
								return {type: {group: {name: 'Workflow Task Generator'}}};
							}
						},
						{fieldLabel: 'Task Name Template', name: 'taskNameTemplate', xtype: 'textarea', height: 20, grow: true, qtip: 'For auto-generated tasks that allow name, one can optionally use this template field to automatically generate task name. "task" and "linkedEntity" beans are placed in the context and can be used during name generation. For example, "January 2016" based on task\'s due date.', requiredFields: ['taskGeneratorBean.name', 'taskNameAllowed']},
						{fieldLabel: '', boxLabel: 'Auto-create new Workflow Task on Entity Creation', name: 'generateOnEntityCreation', xtype: 'checkbox'},

						{xtype: 'sectionheaderfield', header: 'Linked Entity Configuration'},
						{fieldLabel: 'Linked Table', name: 'linkedEntityTable.name', hiddenName: 'linkedEntityTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', qtip: 'Identifies the table that will have entities that WorkflowTask objects will be created for. For example, "InvestmentAccount" for requesting changes to all of account\'s asset classes.'},
						{fieldLabel: 'Linked Entity Window Class', name: 'linkedEntityWindowClass', requiredFields: ['linkedEntityTable.name'], qtip: 'Optionally can be used to override linkedEntityTable.detailPageClass'},
						{fieldLabel: 'Linked Entity URL', name: 'linkedEntityListURL', requiredFields: ['linkedEntityTable.name']},
						{
							fieldLabel: 'Linked Entity', name: 'linkedEntityLabel', hiddenName: 'linkedEntityFkFieldId', xtype: 'combo', submitValue: true, displayField: 'label', url: 'REPLACE_ME_DYNAMICALLY', requiredFields: ['linkedEntityListURL'],
							qtip: 'Optionally link all task for this definition to this entity. For example, unique monthly tasks for a specific client.',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const url = combo.getParentForm().getFormValue('linkedEntityListURL');
									if (url) {
										combo.setUrl(url);
									}
								}
							}
						},


						{xtype: 'sectionheaderfield', header: 'Linked Window Configuration'},
						{fieldLabel: 'Linked Window Name', name: 'linkedWindowName', qtip: 'Optionally specifies the window name/class/default data for easy navigation to related window. For example, for a task to review Compliance Rule Runs, this link can directly take the user to corresponding section.'},
						{fieldLabel: 'Linked Window Class', name: 'linkedWindowClass', requiredFields: ['linkedWindowName']},
						{fieldLabel: 'Linked Window Default Data', name: 'linkedWindowDefaultData', requiredFields: ['linkedWindowClass'], qtip: 'Task form panel is available via "TCG.tempFormPanel" variable. For example:<br/><br/>{<br/>runDate: Clifton.calendar.getBusinessDayFromDate(<br/>TCG.tempFormPanel.getFormValue(\'dueDate\', true), -1)<br/>}'},

						{
							xtype: 'fieldset-checkbox',
							title: 'Locking Configuration',
							name: 'lockingConfiguration',
							instructions: 'An observer will be registered for each "Table to Lock" DAO and corresponding Conditions will be used to manage locking.',
							checkboxName: 'locked',
							anchor: '0',
							defaults: {
								anchor: '-24'
							},
							labelWidth: 154,
							items: [
								{
									fieldLabel: 'Unlock Workflow Status', name: 'unlockWorkflowStatus.name', hiddenName: 'unlockWorkflowStatus.id', xtype: 'combo', url: 'workflowStatusListFind.json', detailPageClass: 'Clifton.workflow.definition.StatusWindow', qtip: 'Locked entities can be modified only when they are in this Workflow Status',
									requiredFields: ['workflow.name'],
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const fp = combo.getParentForm();
											combo.clearAndReset();
											combo.store.baseParams = {
												workflowId: fp.getFormValue('workflow.id', true)
											};
										}
									}
								},
								{
									xtype: 'formgrid',
									storeRoot: 'lockList',
									dtoClass: 'com.clifton.workflow.task.WorkflowTaskDefinitionLock',
									frame: false,
									columnsConfig: [
										{header: 'ID', dataIndex: 'id', width: 60, hidden: true},
										{header: 'Table to Lock', dataIndex: 'lockEntityTable.name', idDataIndex: 'lockEntityTable.id', width: 170, editor: {xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', allowBlank: false}, tooltip: 'Usually the same as Linked Table but could also be its child table'},
										{header: 'Linked Entity Bean Field Path', dataIndex: 'linkedEntityBeanFieldPath', width: 160, editor: {xtype: 'textfield', allowBlank: false}, tooltip: 'The value is usually \'id\' but when lockEntityTable is defined, it will specify the path from lock entity to the linked entity. For example, \'account.id\' for the linkedEntityTable example above. One can also specify comma-delimited paths to enable COALESCE logic when one of multiple path results in not null entity.'},
										{header: 'Event Scope', dataIndex: 'eventScope', width: 100, editor: {xtype: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.workflow.task.LOCK_EVENT_SCOPES}}, tooltip: 'Optionally restrict locks to specific Linked Entity modify events: INSERT, UPDATE, DELETE.'},
										{header: 'Lock Scope Condition', dataIndex: 'lockScopeCondition.name', idDataIndex: 'lockScopeCondition.id', width: 250, editor: {xtype: 'system-condition-combo'}, tooltip: 'If not set, all entities in the table will be locked.  If set, only tables where this scope condition evaluates to true will be locked.'},
										{header: 'Lock Entity Modify Condition', dataIndex: 'lockEntityModifyCondition.name', idDataIndex: 'lockEntityModifyCondition.id', width: 200, editor: {xtype: 'system-condition-combo'}, tooltip: 'Dynamically determines if the entity can be modified while in the unlockWorkflowStatus status given it is current field values. If null, then all fields may be modified and if set, then the condition must evaluate to true.'}
									]
								}
							]
						}
					]
				}]
			},


			{
				title: 'Step Definitions',
				items: [{
					name: 'workflowTaskStepDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'The list of Workflow Task Step Definitions for this Workflow Task Definition.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Name', width: 150, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Linked Table', width: 100, dataIndex: 'linkedEntityTable.name', filter: {searchFieldName: 'linkedEntityTableId', type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true'}},
						{header: 'Assignee', width: 50, dataIndex: 'assigneeUser.label', filter: {searchFieldName: 'assigneeUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}},
						{header: 'Order', width: 30, dataIndex: 'order', type: 'int'}
					],
					getLoadParams: function() {
						return {
							taskDefinitionId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.workflow.task.step.WorkflowTaskStepDefinitionWindow',
						addFromTemplateURL: 'workflowTaskStepDefinition.json',
						getDefaultData: function(gridPanel) {
							return {
								taskDefinition: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Custom Task Fields',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'The following custom columns are associated with workflow tasks for tasks of this definition.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 150, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int'},
						{header: 'Required', width: 50, dataIndex: 'required', type: 'boolean'},
						{header: 'Global', width: 50, dataIndex: 'linkedToAllRows', type: 'boolean', tooltip: 'Global custom fields are not task definition specific: apply to all rows'},
						{header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getLoadParams: function() {
						return {
							linkedValue: this.getWindow().getMainFormId(),
							columnGroupName: 'Workflow Task Custom Fields',
							includeNullLinkedValue: true
						};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						addFromTemplateURL: 'systemColumn.json',
						deleteURL: 'systemColumnDelete.json',
						getDefaultData: function(gridPanel) { // defaults group
							return TCG.data.getDataPromiseUsingCaching('systemColumnGroupByName.json?name=Workflow Task Custom Fields', gridPanel, 'system.schema.group.Workflow Task Custom Fields')
								.then(function(columnGroup) {
									const values = gridPanel.getWindow().getMainForm().formValues;
									return {
										columnGroup: columnGroup,
										linkedValue: values.id,
										linkedLabel: values.name,
										table: columnGroup.table
									};
								});
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'system-note-grid',
					tableName: 'WorkflowTaskDefinition',
					showInternalInfo: false,
					showPrivateInfo: false,
					defaultActiveFilter: false,
					showDisplayFilter: false
				}]
			},


			{
				title: 'Tasks',
				items: [{
					name: 'workflowTaskListFind',
					xtype: 'gridpanel-custom-json-fields',
					tableName: 'WorkflowTask',
					instructions: 'A list of all tasks for selected task definition.',
					additionalPropertiesToRequest: 'priority.cssStyle',
					topToolbarSearchParameter: 'searchPattern',
					nonCustomColumns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Task Category', width: 45, dataIndex: 'definition.category.name', filter: {searchFieldName: 'categoryId', type: 'combo', url: 'workflowTaskCategoryListFind.json'}, hidden: true},
						{header: 'Task Definition', width: 85, dataIndex: 'definition.name', filter: {searchFieldName: 'definitionId', type: 'combo', url: 'workflowTaskDefinitionListFind.json'}, hidden: true},
						{header: 'Entity Label', width: 80, dataIndex: 'linkedEntityLabel'},
						{
							header: 'Name or Description', width: 80, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'},
							renderer: function(v, c, r) {
								return v || r.json.description;
							}
						},
						{header: 'Task Name', width: 100, dataIndex: 'name', hidden: true},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Workflow State', width: 40, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Workflow Status', width: 40, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}},
						{header: 'Assignee User', width: 30, dataIndex: 'assigneeUser.label', filter: {searchFieldName: 'assigneeUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}, hidden: true},
						{header: 'Assignee Group', width: 35, dataIndex: 'assigneeGroup.label', filter: {searchFieldName: 'assigneeGroupId', type: 'combo', url: 'securityGroupListFind.json'}, hidden: true},
						{header: 'Assignee', width: 35, dataIndex: 'assigneeLabel', tooltip: 'Assignee User for this Task or, if not specified, Assignee User Group'},
						{header: 'Approver User', width: 30, dataIndex: 'approverUser.label', filter: {searchFieldName: 'approverUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}, hidden: true},
						{header: 'Approver Group', width: 35, dataIndex: 'approverGroup.label', filter: {searchFieldName: 'approverGroupId', type: 'combo', url: 'securityGroupListFind.json'}, hidden: true},
						{header: 'Approver', width: 35, dataIndex: 'approverLabel', tooltip: 'Approver User for this Task or, if not specified, Approver User Group'},
						{
							header: 'Priority', width: 25, dataIndex: 'priority.name', filter: {searchFieldName: 'priorityId', type: 'combo', url: 'systemPriorityListFind.json'},
							renderer: function(v, c, r) {
								const style = r.json.priority.cssStyle;
								if (TCG.isNotBlank(style)) {
									c.attr = 'style="' + style + '"';
								}
								return v;
							}
						},
						{header: 'Original Due Date', width: 35, dataIndex: 'originalDueDate', hidden: true},
						{
							header: 'Due Date', width: 30, dataIndex: 'dueDate', defaultSortColumn: true, defaultSortDirection: 'desc',
							renderer: function(v, metaData, r) {
								return Clifton.workflow.task.renderDueDate(v, metaData, r);
							}
						},
						{
							header: '<div class="checked" style="WIDTH:16px;">&nbsp;</div>', exportHeader: 'Completed', width: 12, tooltip: 'Completed Task', dataIndex: 'completedTask', type: 'boolean', nonPersistentField: true,
							renderer: function(v, c, r) {
								return TCG.renderBoolean(v, r.data.resolution);
							}
						}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('dueDate', {'after': new Date().add(Date.DAY, -90)});
						}
						return {
							definitionId: this.getWindow().getMainFormId(),
							readUncommittedRequested: true
						};
					},
					getLinkedValueFilter: function() {
						if (this.getWindow().params) {
							return this.getWindow().params.id; // limit to custom fields associated with selected task definition
						}
						return false;
					},
					editor: {
						detailPageClass: 'Clifton.workflow.task.WorkflowTaskWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
