Clifton.workflow.task.WorkflowTaskSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'workflowTaskSetupWindow',
	title: 'Workflow Task Setup',
	iconCls: 'task',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Workflow Tasks',
				reloadOnTabChange: true,
				items: [{
					xtype: 'workflow-task-advanced-grid'
				}]
			},


			{
				title: 'Tasks Steps',
				items: [{
					name: 'workflowTaskStepListFind',
					xtype: 'gridpanel',
					instructions: 'Workflow Task Step is a single step of a specific task. Many workflow tasks do not have any steps. However, for more complex tasks it might be important to keep track of individual steps that could be assigned to different people.',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						const filters = TCG.grid.GridPanel.prototype.getTopToolbarFilters.call(this, arguments);
						filters.push({fieldLabel: 'Task Category', xtype: 'toolbar-combo', width: 150, url: 'workflowTaskCategoryListFind.json', linkedFilter: 'task.definition.category.name'});
						filters.push({
							fieldLabel: 'Display', xtype: 'combo', name: 'displayFilters', width: 140, minListWidth: 140, emptyText: 'Open Assigned to Me', forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value',
							store: new Ext.data.ArrayStore({
								fields: ['value', 'name', 'description'],
								data: [
									['ALL', 'All Steps', 'Display all Workflow Task Steps'],
									['ALL_OPEN', 'Open Steps', 'Display all Workflow Task Steps that have not been completed'],
									['MY_ASSIGNEE_ALL', 'All Assigned to Me', 'Display all Workflow Task Steps that have been Assigned to me'],
									['MY_ASSIGNEE_OPEN', 'Open Assigned to Me', 'Display Workflow Task Steps that have been Assigned and have not been completed']
								]
							}),
							listeners: {
								select: function(field) {
									const gp = TCG.getParentByClass(field, Ext.Panel);
									const v = field.getValue();
									if (v === 'MY_ASSIGNEE_ALL') {
										const user = TCG.getCurrentUser();
										gp.setFilterValue('assigneeUser.label', {'equals': {value: user.id, text: user.label}});
										gp.clearFilter('completedStep', true);
									}
									else if (v === 'MY_ASSIGNEE_OPEN') {
										const user = TCG.getCurrentUser();
										gp.setFilterValue('assigneeUser.label', {'equals': {value: user.id, text: user.label}});
										gp.setFilterValue('completedStep', false);
									}
									else if (v === 'ALL_OPEN') {
										gp.clearFilter('assigneeUser.label', true);
										gp.setFilterValue('completedStep', false);
									}
									else {
										gp.clearFilter('assigneeUser.label', true);
										gp.clearFilter('completedStep', true);
									}
									gp.reload();
								}
							}
						});
						return filters;
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Task Category', width: 45, dataIndex: 'task.definition.category.name', filter: {searchFieldName: 'taskCategoryId', type: 'combo', url: 'workflowTaskCategoryListFind.json'}},
						{header: 'Step Definition', width: 70, dataIndex: 'stepDefinition.name', filter: {searchFieldName: 'stepDefinitionName'}},
						{header: 'Task Entity', width: 70, dataIndex: 'taskLinkedEntityLabel'},
						{header: 'Step Entity', width: 70, dataIndex: 'linkedEntityLabel'},
						{header: 'Step Name', width: 70, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Assignee', width: 30, dataIndex: 'assigneeUser.label', filter: {searchFieldName: 'assigneeUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}},
						{header: 'Original Due Date', width: 35, dataIndex: 'originalDueDate', hidden: true},
						{
							header: 'Due Date', width: 30, dataIndex: 'dueDate', defaultSortColumn: true,
							renderer: function(v, metaData, r) {
								return Clifton.workflow.task.renderDueDate(v, metaData, r);
							}
						},
						{header: 'Completion Date', width: 35, dataIndex: 'completionDate', hidden: true},
						{
							header: '<div class="checked" style="WIDTH:16px;">&nbsp;</div>', exportHeader: 'Completed', width: 12, tooltip: 'Completed Step', dataIndex: 'completedStep', type: 'boolean',
							renderer: function(v, c, r) {
								return TCG.renderBoolean(v, r.data.resolution);
							}
						}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) { // default to "Open Assigned to Me"
							const user = TCG.getCurrentUser();
							this.setFilterValue('assigneeUser.label', {'equals': {value: user.id, text: user.label}});
							this.setFilterValue('completedStep', false);
						}
						return {};
					},
					editor: {
						detailPageClass: 'Clifton.workflow.task.step.WorkflowTaskStepWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Task Definitions',
				items: [{
					name: 'workflowTaskDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'Workflow Task Definition is used to define attributes and logic used to generate Workflow Task entities.',
					topToolbarSearchParameter: 'searchPattern',
					groupField: 'category.name',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Definitions" : "Definition"]})',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Task Category', width: 70, dataIndex: 'category.name', filter: {searchFieldName: 'categoryId', type: 'combo', url: 'workflowTaskCategoryListFind.json'}, hidden: true},
						{header: 'Task Definition', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Task Workflow', width: 100, dataIndex: 'workflow.name'},
						{header: 'Final Workflow Status', width: 50, dataIndex: 'finalWorkflowStatus.name', hidden: true},
						{header: 'Assignee Workflow Status', width: 50, dataIndex: 'assigneeWorkflowStatus.name', hidden: true},
						{header: 'Generator Bean', width: 100, dataIndex: 'taskGeneratorBean.name', filter: {searchFieldName: 'taskGeneratorBeanId', type: 'combo', url: 'systemBeanListFind.json?groupName=Workflow Task Generator'}},

						{header: 'Task Assignee User', width: 50, dataIndex: 'assigneeUser.label', filter: {searchFieldName: 'assigneeUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}, hidden: true},
						{header: 'Task Assignee Group', width: 50, dataIndex: 'assigneeGroup.label', filter: {searchFieldName: 'assigneeGroupId', type: 'combo', url: 'securityGroupListFind.json', displayField: 'label'}, hidden: true},
						{header: 'Assignee User must belong to Assignee Group', width: 50, dataIndex: 'assigneeRequiredToBeInAssigneeGroup', type: 'boolean', hidden: true},
						{header: 'Task Approver User', width: 50, dataIndex: 'approverUser.label', filter: {searchFieldName: 'approverUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}, hidden: true},
						{header: 'Secondary Approver User', width: 50, dataIndex: 'secondaryApproverUser.label', filter: {searchFieldName: 'secondaryApproverUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}, hidden: true},
						{header: 'Task Approver Group', width: 50, dataIndex: 'approverGroup.label', filter: {searchFieldName: 'approverGroupId', type: 'combo', url: 'securityGroupListFind.json'}, hidden: true},
						{header: 'Approver User must belong to Approver Group', width: 50, dataIndex: 'approverRequiredToBeInApproverGroup', type: 'boolean', hidden: true},

						{header: 'Task Priority', width: 50, dataIndex: 'priority.name', hidden: true},
						{header: 'Task Modify Condition', width: 50, dataIndex: 'taskEntityModifyCondition.name', hidden: true},
						{header: 'Max Due Date Days', width: 50, dataIndex: 'generateDueDateDaysFromToday', hidden: true},
						{header: 'Allow changes to Due Date', width: 50, dataIndex: 'changeToDueDateAllowed', type: 'boolean', hidden: true},
						{header: 'Require Name', width: 50, dataIndex: 'taskNameRequired', type: 'boolean', hidden: true},
						{header: 'Require Description', width: 50, dataIndex: 'taskDescriptionRequired', type: 'boolean', hidden: true},
						{header: 'Task Name Template', width: 70, dataIndex: 'taskNameTemplate', hidden: true},
						{header: 'Auto-Create', width: 50, dataIndex: 'generateOnEntityCreation', type: 'boolean', hidden: true, tooltip: 'Auto-create new Workflow Task on Entity Creation'},
						{header: 'Allow Steps', width: 50, dataIndex: 'taskStepAllowed', type: 'boolean', hidden: true},

						{header: 'Linked Table', width: 70, dataIndex: 'linkedEntityTable.name', filter: {searchFieldName: 'linkedEntityTableId', type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true'}},
						{header: 'Linked Entity', width: 100, dataIndex: 'linkedEntityLabel', hidden: true},
						{header: 'Linked Entity ID', width: 100, dataIndex: 'linkedEntityFkFieldId', hidden: true},
						{header: 'Linked Window', width: 100, dataIndex: 'linkedWindowName', hidden: true},
						{header: 'Locked', width: 30, dataIndex: 'locked', type: 'boolean'},
						{header: 'Specific', width: 30, dataIndex: 'forSpecificEntity', type: 'boolean', tooltip: 'This Definition is Applicable to Only One Specific Entity'},
						{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'}
					],
					getTopToolbarFilters: function(toolbar) {
						const filters = TCG.grid.GridPanel.prototype.getTopToolbarFilters.call(this, arguments);
						filters.push({fieldLabel: 'Task Category', xtype: 'toolbar-combo', width: 150, url: 'workflowTaskCategoryListFind.json', linkedFilter: 'category.name'});
						return filters;
					},
					editor: {
						detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow',
						addFromTemplateURL: 'workflowTaskDefinition.json',
						addToolbarAddButton: function(toolBar, gridPanel) {
							TCG.grid.GridEditor.prototype.addToolbarAddButton.call(this, toolBar, gridPanel);
							const addButton = toolBar.findBy(function(o) {
								return o.xtype === 'splitbutton' && o.text === 'Add';
							})[0];
							addButton.menu.add('-', {
								text: 'From Definition Template',
								iconCls: 'copy',
								menu: new Ext.menu.Menu({
									layout: 'fit',
									style: {overflow: 'visible'},
									width: 250,
									items: [
										{
											xtype: 'combo', name: 'group', url: 'workflowTaskDefinitionTemplateListFind.json',
											getListParent: function() {
												return this.el.up('.x-menu');
											},
											listeners: {
												'select': function(combo) {
													TCG.data.getDataPromise('workflowTaskDefinitionTemplate.json?id=' + combo.getValue(), combo)
														.then(function(template) {
															TCG.createComponent('Clifton.workflow.task.template.TaskDefinitionFromTemplateWindow', {
																defaultData: {
																	template: template,
																	taskDefinition: template.templateDefinition,
																	name: template.defaultName || '',
																	description: template.defaultDescription || ''
																},
																openerCt: gridPanel
															});
														});
													combo.reset();
													addButton.hideMenu();
												}
											}
										}
									]
								})
							});
						}
					}
				}]
			},


			{
				title: 'Definition Locks',
				items: [{
					name: 'workflowTaskDefinitionLockListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all table/entity locks managed by corresponding Workflow Task Definitions.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Task Definition', width: 150, dataIndex: 'definition.name', filter: {searchFieldName: 'definitionName'}, hidden: true},
						{header: 'Table to Lock', width: 120, dataIndex: 'lockEntityTable.name', filter: {searchFieldName: 'lockEntityTableNameLike'}, defaultSortColumn: true},
						{header: 'Linked Table', width: 100, dataIndex: 'definition.linkedEntityTable.name', filter: {searchFieldName: 'linkedEntityTableNameLike'}},
						{header: 'Linked Entity Bean Field Path', width: 100, dataIndex: 'linkedEntityBeanFieldPath'},
						{header: 'Event Scope', width: 50, dataIndex: 'eventScope', filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.workflow.task.LOCK_EVENT_SCOPES}}},
						{header: 'Lock Scope Condition', width: 200, dataIndex: 'lockScopeCondition.name', filter: {type: 'combo', searchFieldName: 'lockScopeConditionId', url: 'systemConditionListFind.json'}},
						{header: 'Lock Entity Modify Condition', width: 140, dataIndex: 'lockEntityModifyCondition.name', filter: {type: 'combo', searchFieldName: 'lockEntityModifyConditionId', url: 'systemConditionListFind.json'}},
						{header: 'Active', width: 30, dataIndex: 'definition.active', filter: {searchFieldName: 'active'}, type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionLockWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Definition Templates',
				items: [{
					name: 'workflowTaskDefinitionTemplateListFind',
					xtype: 'gridpanel',
					instructions: 'Workflow Task Definition Templates are used to simplify creation of similar Task Definitions. All fields, except for those that can be changed, will be copied from the corresponding template definition. They can also be used to allow Task Definition creation for users that would not otherwise be allowed this. It is safe to allow this with templates because only few "safe" fields can be changed.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Template Name', width: 100, dataIndex: 'name'},
						{header: 'Template Definition', width: 100, dataIndex: 'templateDefinition.name', filter: false},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'Default Name', width: 100, dataIndex: 'defaultName', hidden: true},
						{header: 'Default Description', width: 100, dataIndex: 'defaultDescription', hidden: true},
						{header: 'Definition Create Condition', width: 150, dataIndex: 'taskDefinitionCreateCondition.name', filter: false}
					],
					editor: {
						detailPageClass: 'Clifton.workflow.task.template.WorkflowTaskDefinitionTemplateWindow'
					}
				}]
			},


			{
				title: 'Task Categories',
				items: [{
					name: 'workflowTaskCategoryListFind',
					xtype: 'gridpanel',
					instructions: 'Workflow Task Categories are used to group related task definitions. For example, Accounting Tasks, Compliance Tasks, Operations Tasks, Trading Tasks, etc.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'Definition Modify Condition', width: 150, dataIndex: 'definitionEntityModifyCondition.name', filter: false},
						{header: 'Locking Allowed', width: 50, dataIndex: 'definitionLockingAllowed', type: 'boolean'},
						{header: 'Auto-Create Allowed', width: 50, dataIndex: 'generateOnEntityCreationAllowed', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.workflow.task.WorkflowTaskCategoryWindow'
					}
				}]
			},


			{
				title: 'Task Generator',
				items: [{
					xtype: 'formpanel',
					items: [{
						xtype: 'fieldset',
						title: 'Workflow Task Generation',
						instructions: 'Use this section to generate tasks for Task Definitions that support Task automated generation. These tasks are usually generated by corresponding Batch Job.',
						items: [
							{fieldLabel: 'Task Definition', hiddenName: 'taskDefinitionId', xtype: 'combo', url: 'workflowTaskDefinitionListFind.json?taskGeneratorBeanPresent=true', detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow', allowBlank: false},
							{fieldLabel: 'From Due Date', name: 'fromDate', xtype: 'datefield', allowBlank: false},
							{fieldLabel: 'To Due Date', name: 'toDate', xtype: 'datefield', allowBlank: false}
						],
						buttons: [{
							text: 'Generate Tasks',
							width: 165,
							xtype: 'button',
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								if (TCG.isNotNull(f.getFirstInValidField())) {
									TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
								}
								else {
									const params = {
										taskDefinitionId: f.getFormValue('taskDefinitionId'),
										fromDate: f.getForm().findField('fromDate').value,
										toDate: f.getForm().findField('toDate').value
									};

									Ext.Msg.confirm('Generate Tasks', 'Would you like to generate Workflow Tasks using selected filters?', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitTarget: f,
												waitMsg: 'Generating Tasks...',
												root: 'workflowTaskList',
												params: params,
												onLoad: function(records, conf) {
													TCG.showInfo('Created ' + records.length + ' Workflow Tasks', 'Workflow Task Generation');
												}
											});
											loader.load('workflowTaskListGenerate.json?requestedPropertiesRoot=workflowTaskList&requestedProperties=id');
										}
									});
								}
							}
						}]
					}]
				}]
			}
		]
	}]
});


