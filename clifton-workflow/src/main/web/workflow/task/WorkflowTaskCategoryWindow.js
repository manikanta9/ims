Clifton.workflow.task.WorkflowTaskCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Task Category',
	iconCls: 'task',
	width: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'workflowTaskCategory.json',
					instructions: 'Workflow Task Categories are used to group related task definitions. For example, Accounting Tasks, Compliance Tasks, Operations Tasks, Trading Tasks, etc.',
					labelWidth: 150,

					items: [
						{fieldLabel: 'Category Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Definition Modify Condition', name: 'definitionEntityModifyCondition.name', hiddenName: 'definitionEntityModifyCondition.id', xtype: 'system-entity-modify-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=WorkflowTaskDefinition', requireConditionForNonAdmin: true},
						{boxLabel: 'Allow locking (Lock Configuration) workflow tasks for category\'s task definitions', name: 'definitionLockingAllowed', xtype: 'checkbox'},
						{boxLabel: 'Allow auto-generation of workflow tasks for category\'s task definitions', name: 'generateOnEntityCreationAllowed', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Task Definitions',
				items: [{
					name: 'workflowTaskDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'The list of Workflow Task Definitions for this Workflow Task Category.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Name', width: 150, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Auto-Create', width: 40, dataIndex: 'generateOnEntityCreation', type: 'boolean', tooltip: 'Auto-create new Workflow Task on Entity Creation'},
						{header: 'Locked', width: 30, dataIndex: 'locked', type: 'boolean'},
						{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function() {
						return {
							categoryId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
