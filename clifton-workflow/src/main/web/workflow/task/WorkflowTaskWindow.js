Clifton.workflow.task.WorkflowTaskWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Task',
	iconCls: 'task',
	width: 1000,
	height: 600,
	enableRefreshWindow: true,

	gridTabWithCount: 'Notes', // show notes count in  the tab

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'WorkflowTask'
				},
				items: [{
					xtype: 'formpanel-custom-json-fields',
					url: 'workflowTask.json',
					labelWidth: 125,
					columnGroupName: 'Workflow Task Custom Fields',
					loadDefaultDataAfterRender: true,
					getWarningMessage: function(f) {
						return TCG.isTrue(this.getFormValue('completedTask')) ? 'This task was completed and can no longer be modified.' : undefined;
					},
					prepareDefaultData: function(dd) {
						const definition = TCG.getValue('definition', dd);
						const linkedTable = definition.linkedEntityTable;
						const linkedWindow = this.getForm().items.find(function(e) {
							return e.fieldLabel === 'Linked Window';
						});
						if (!definition.linkedWindowName) {
							linkedWindow.hide();
						}
						const linkedEntity = this.getForm().items.find(function(e) {
							return (e.fieldLabel && (e.name === 'linkedEntityLabel'));
						});
						if (TCG.isNull(linkedTable)) {
							linkedEntity.hide();
						}
						else {
							linkedEntity.setFieldLabel(linkedTable.label + ':');
							linkedEntity.detailPageClass = definition.linkedEntityWindowClass || linkedTable.detailScreenClass;
						}
						if (!TCG.isTrue(definition.taskNameAllowed)) {
							this.hideField('name');
						}
						if (TCG.isTrue(definition.changeToDueDateAllowed) && dd.dueDate !== dd.originalDueDate) {
							this.showField('originalDueDate');
						}

						// additional tabs:
						const tabs = this.getWindow().items.get(0);
						if (TCG.isTrue(definition.taskStepAllowed) && !TCG.isTrue(tabs.workflowTaskWindowStepsTab)) {
							tabs.add(Clifton.workflow.task.WorkflowTaskWindowStepsTab);
							tabs.workflowTaskWindowStepsTab = true;
						}

						if (definition.locked && !TCG.isTrue(tabs.workflowTaskWindowAuditTrailTab)) {
							tabs.add(Clifton.workflow.task.WorkflowTaskWindowAuditTrailTab);
							tabs.workflowTaskWindowAuditTrailTab = true;
						}

						if (!TCG.isTrue(this.cancelForceFormModified)) {
							this.getForm().findField('description').originalValue = 'FORCE_TRIGGER_CHANGE';
						}

						return dd;
					},
					listeners: {
						afterload: function() {
							this.cancelForceFormModified = true;
							this.prepareDefaultData(this.getForm().formValues);
						}
					},
					updateTaskUser: function(userCombo) {
						const user = TCG.getCurrentUser();
						if (user.id === userCombo.getValue()) {
							TCG.showError('No change was made because you are already identified as Workflow Task Assignee/Approver.', 'Invalid User');
						}
						else {
							const w = this.getWindow();
							const autoSave = !w.isModified();
							userCombo.setValue({value: user.id, text: user.label});
							if (autoSave) {
								w.saveWindow(false);
							}
						}
					},
					items: [
						{name: 'completedTask', hidden: true},
						{fieldLabel: 'Task Definition', name: 'definition.name', detailIdField: 'definition.id', xtype: 'linkfield', detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow'},
						{name: 'definition.description', xtype: 'textarea', height: 62, grow: true, readOnly: true},
						{
							fieldLabel: 'Linked Window', name: 'definition.linkedWindowName', detailIdField: 'linkedWindowFkFieldId', detailPageWhenNoId: true, xtype: 'linkfield', submitValue: true,
							getDefaultData: function(fp) {
								let dd = fp.getFormValue('definition.linkedWindowDefaultData');
								if (dd) {
									TCG.tempFormPanel = fp; // make form panel available in scripts
									dd = Ext.decode(dd);
								}
								return dd;
							},
							getDetailPageClass: function(fp) {
								return fp.getFormValue('definition.linkedWindowClass');
							}
						},
						{fieldLabel: 'Linked Entity', name: 'linkedEntityLabel', detailIdField: 'linkedEntityFkFieldId', xtype: 'linkfield', submitValue: true, qtip: 'Identifies the entity that this task is for. Corresponding Table is defined by WorkflowTaskDefinition.'},
						{fieldLabel: 'Task Name', name: 'name'},
						{fieldLabel: 'Task Description', name: 'description', xtype: 'textarea', grow: true, height: 50},
						{
							xtype: 'columnpanel',
							columns: [
								{
									config: {columnWidth: 0.28},
									rows: [
										{fieldLabel: 'Task Priority', name: 'priority.name', hiddenName: 'priority.id', xtype: 'combo', url: 'systemPriorityListFind.json'},
										{fieldLabel: 'Due Date', name: 'dueDate', xtype: 'datefield'},
										{fieldLabel: 'Original Due Date', name: 'originalDueDate', xtype: 'datefield', hidden: true, readOnly: true}
									]
								},
								{
									config: {columnWidth: 0.34, labelWidth: 100},
									rows: [
										{
											fieldLabel: 'Assignee User', xtype: 'compositefield', name: 'assigneeUser',
											items: [
												{name: 'assigneeUser.label', hiddenName: 'assigneeUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json?disabled=false', disableAddNewItem: true, detailPageClass: 'Clifton.security.user.UserWindow', flex: 1},
												{
													xtype: 'button', iconCls: 'user', width: 26, tooltip: 'Set Assignee User for this Workflow Task to me (currently logged user)',
													handler: function(btn) {
														TCG.getParentFormPanel(this).updateTaskUser(btn.findParentByType('compositefield').items.get(0));
													}
												}
											]
										},
										{
											fieldLabel: 'Approver User', xtype: 'compositefield', name: 'approverUser',
											items: [
												{name: 'approverUser.label', hiddenName: 'approverUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json?disabled=false', disableAddNewItem: true, detailPageClass: 'Clifton.security.user.UserWindow', flex: 1},
												{
													xtype: 'button', iconCls: 'user', width: 26, tooltip: 'Set Approver User for this Workflow Task to me (currently logged user)',
													handler: function(btn) {
														TCG.getParentFormPanel(this).updateTaskUser(btn.findParentByType('compositefield').items.get(0));
													}
												}
											]
										}
									]
								},
								{
									config: {columnWidth: 0.38, labelWidth: 100},
									rows: [
										{fieldLabel: 'Assignee Group', name: 'assigneeGroup.label', detailIdField: 'assigneeGroup.id', xtype: 'linkfield', detailPageClass: 'Clifton.security.user.GroupWindow'},
										{fieldLabel: 'Approver Group', name: 'approverGroup.label', detailIdField: 'approverGroup.id', xtype: 'linkfield', detailPageClass: 'Clifton.security.user.GroupWindow'}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'}
					]
				}]
			},

			{
				title: 'Notes',
				items: [{
					xtype: 'system-note-grid',
					tableName: 'WorkflowTask',
					showInternalInfo: false,
					showPrivateInfo: false,
					defaultActiveFilter: false,
					showDisplayFilter: false
				}]
			},


			{
				title: 'Task Life Cycle',
				items: [{
					xtype: 'system-lifecycle-grid',
					tableName: 'WorkflowTask'
				}]
			}
		]
	}]
});


Clifton.workflow.task.WorkflowTaskWindowStepsTab = {
	title: 'Steps',
	items: [{
		name: 'workflowTaskStepListFind',
		xtype: 'gridpanel',
		instructions: 'The list of Workflow Task Steps for this Workflow Task. Steps cannot be created or deleted manually: they always come from corresponding Task Definition.',
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Step Definition', width: 150, dataIndex: 'stepDefinition.name', filter: {searchFieldName: 'stepDefinitionName'}},
			{header: 'Step Name', width: 150, dataIndex: 'name'},
			{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
			{header: 'Assignee', width: 60, dataIndex: 'assigneeUser.label', filter: {searchFieldName: 'assigneeUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}},
			{header: 'Order', width: 40, dataIndex: 'stepDefinition.order', type: 'int', filter: {searchFieldName: 'order'}, defaultSortColumn: true},
			{header: 'Original Due Date', width: 40, dataIndex: 'originalDueDate', hidden: true},
			{
				header: 'Due Date', width: 40, dataIndex: 'dueDate',
				renderer: function(v, metaData, r) {
					return Clifton.workflow.task.renderDueDate(v, metaData, r);
				}
			},
			{header: 'Completion Date', width: 40, dataIndex: 'completionDate', hidden: true},
			{
				header: '<div class="checked" style="WIDTH:16px;">&nbsp;</div>', exportHeader: 'Completed', width: 15, tooltip: 'Completed Step', dataIndex: 'completedStep', type: 'boolean',
				renderer: function(v, c, r) {
					return TCG.renderBoolean(v, r.data.resolution);
				}
			}
		],
		getLoadParams: function() {
			return {
				taskId: this.getWindow().getMainFormId()
			};
		},
		editor: {
			detailPageClass: 'Clifton.workflow.task.step.WorkflowTaskStepWindow',
			drillDownOnly: true
		}
	}]
};


Clifton.workflow.task.WorkflowTaskWindowAuditTrailTab = {
	title: 'Audit Trail (Locked Entity)',
	items: [{
		xtype: 'system-audit-grid',
		name: 'workflowTaskAuditEventList',
		instructions: 'Audit Log contains information about field level changes (insert/update/delete) for all entities locked by this task. Double click on the row to open corresponding detail window.',
		pageSize: 1000,
		parentTable: true,
		remoteSort: false,

		getTopToolbarFilters: function(toolbar) {
			return [
				{
					fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 100, minListWidth: 100, mode: 'local', value: 'TASK', displayField: 'name', valueField: 'value',
					store: new Ext.data.ArrayStore({
						fields: ['value', 'name', 'description'],
						data: [['TASK', 'Task History', 'Display only audit trail for linked entity for events from the time this task was created until the time it was completed.'], ['ALL', 'Full History', 'Display all audit trail events for linked entity.']]
					})
				}
			];
		},
		getLoadParams: function(firstPage) {
			const displayType = TCG.getChildByName(this.getTopToolbar(), 'displayType');
			return {
				workflowTaskId: this.getEntityId(),
				restrictTime: (displayType.getValue() === 'TASK')
			};
		}
	}]
};
