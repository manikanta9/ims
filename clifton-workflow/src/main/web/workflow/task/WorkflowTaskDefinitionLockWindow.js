// not the actual window but looks up the definition for the lock and opens that window
Clifton.workflow.task.WorkflowTaskDefinitionLockWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'workflowTaskDefinitionLock.json',

	openEntityWindow: function(config, entity) {
		if (entity) {
			const w = this;
			TCG.data.getDataPromise('workflowTaskDefinition.json?id=' + entity.definition.id, this).then(
				function(definition) {
					config.params.id = definition.id;
					w.doOpenEntityWindow(config, definition, 'Clifton.workflow.task.WorkflowTaskDefinitionWindow');
				}
			);
		}
	}
});
