Clifton.workflow.task.step.WorkflowTaskStepDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Task Step Definition',
	iconCls: 'task',
	width: 700,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'workflowTaskStepDefinition.json',
					labelWidth: 165,
					items: [
						{fieldLabel: 'Task Definition', name: 'taskDefinition.name', detailIdField: 'taskDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow'},
						{fieldLabel: 'Definition Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true, height: 50},

						{xtype: 'sectionheaderfield', header: 'Step Configuration'},
						{fieldLabel: 'Step Order', name: 'order', xtype: 'spinnerfield'},
						{fieldLabel: 'Step Assignee', name: 'assigneeUser.label', hiddenName: 'assigneeUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json?disabled=false', qtip: 'Optional user that all steps of this definition should be assigned to. When set, this user will be used to default the step Assignee during task creation.'},
						{
							fieldLabel: 'Due Date Generation', name: 'stepDueDateGenerationOption', displayField: 'name', hiddenName: 'stepDueDateGenerationOption', valueField: 'value', xtype: 'combo', mode: 'local',
							store: {xtype: 'arraystore', fields: ['name', 'value', 'description'], data: Clifton.calendar.date.DATE_GENERATION_OPTIONS},
							qtip: 'Optional setting that specifies how to calculate the value of Step\'s Due Date in relation to Task\'s Due Date.'
						},
						{fieldLabel: 'Days From Due Date', name: 'stepDueDateDaysFromTaskDueDate', xtype: 'integerfield', qtip: 'The number associated with \'Due Date Generation Option\' (if applicable).', dependentFields: ['stepDueDateGenerationOption']},
						{fieldLabel: '', boxLabel: 'Allow changes to Due Date (Original Due Date field can never be modified)', name: 'changeToDueDateAllowed', xtype: 'checkbox'},
						{fieldLabel: '', boxLabel: 'Require "Step Name" field for all Steps of this Step Definition', name: 'stepNameRequired', xtype: 'checkbox'},

						{xtype: 'sectionheaderfield', header: 'Linked Entity Configuration'},
						{fieldLabel: 'Linked Table', name: 'linkedEntityTable.name', hiddenName: 'linkedEntityTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', qtip: 'Identifies the table that will have entities that WorkflowTask objects will be created for. For example, "InvestmentAccount" for requesting changes to account\'s asset classes.'},
						{fieldLabel: 'Linked Entity URL', name: 'linkedEntityListURL', requiredFields: ['linkedEntityTable.name']}
					]
				}]
			},


			{
				title: 'Custom Step Fields',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'The following custom columns are associated with workflow task steps for tasks of this definition.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int'},
						{header: 'Required', width: 50, dataIndex: 'required', type: 'boolean'},
						{header: 'Global', width: 50, dataIndex: 'linkedToAllRows', type: 'boolean', tooltip: 'Global custom fields are not task definition specific: apply to all rows'},
						{header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getLoadParams: function() {
						return {
							linkedValue: this.getWindow().getMainFormId(),
							columnGroupName: 'Workflow Task Step Custom Fields',
							includeNullLinkedValue: true
						};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						addFromTemplateURL: 'systemColumn.json',
						deleteURL: 'systemColumnDelete.json',
						getDefaultData: function(gridPanel) { // defaults group
							const grp = TCG.data.getData('systemColumnGroupByName.json?name=Workflow Task Step Custom Fields', gridPanel, 'system.schema.group.Workflow Task Step Custom Fields');
							const values = gridPanel.getWindow().getMainForm().formValues;
							return {
								columnGroup: grp,
								linkedValue: values.id,
								linkedLabel: values.name,
								table: grp.table
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			}
		]
	}]
});
