Clifton.workflow.task.step.WorkflowTaskStepWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Task Step',
	iconCls: 'task',
	width: 700,
	height: 450,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel-custom-fields',
					url: 'workflowTaskStep.json',
					columnGroupName: 'Workflow Task Step Custom Fields',
					getWarningMessage: function(f) {
						return TCG.isTrue(this.getFormValue('task.completedTask')) ? 'This task was completed and can no longer be modified.' : undefined;
					},
					listeners: {
						afterload: function() {
							const linkedTable = this.getFormValue('stepDefinition.linkedEntityTable');
							const linkedEntity = this.getForm().items.find(function(e) {
								return e.fieldLabel === 'Linked Entity';
							});
							if (TCG.isNull(linkedTable)) {
								linkedEntity.hide();
							}
							else if (linkedTable.detailScreenClass) {
								linkedEntity.detailPageClass = linkedTable.detailScreenClass;
							}
							if (this.getFormValue('dueDate') !== this.getFormValue('originalDueDate')) {
								this.showField('originalDueDate');
							}
						}
					},
					items: [
						{name: 'completedTask', hidden: true},
						{fieldLabel: 'Task Definition', name: 'task.definition.name', detailIdField: 'task.definition.id', xtype: 'linkfield', detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow'},
						{fieldLabel: 'Workflow Task', name: 'task.label', detailIdField: 'task.id', xtype: 'linkfield', detailPageClass: 'Clifton.workflow.task.WorkflowTaskWindow'},
						{fieldLabel: 'Step Definition', name: 'stepDefinition.name', detailIdField: 'stepDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.workflow.task.step.WorkflowTaskStepDefinitionWindow'},
						{fieldLabel: 'Linked Entity', name: 'linkedEntityLabel', detailIdField: 'linkedEntityFkFieldId', xtype: 'linkfield', submitValue: true},
						{fieldLabel: 'Step Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true, height: 50},
						{
							xtype: 'columnpanel',
							columns: [
								{
									rows: [
										{fieldLabel: 'Due Date', name: 'dueDate', xtype: 'datefield'},
										{fieldLabel: 'Completion Date', name: 'completionDate', xtype: 'datefield'}
									]
								},
								{
									rows: [
										{fieldLabel: 'Original Due Date', name: 'originalDueDate', xtype: 'datefield', hidden: true, readOnly: true},
										{fieldLabel: 'Step Assignee', name: 'assigneeUser.label', hiddenName: 'assigneeUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json?disabled=false', disableAddNewItem: true}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'}
					]
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'system-note-grid',
					tableName: 'WorkflowTaskStep',
					showInternalInfo: false,
					showPrivateInfo: false,
					defaultActiveFilter: false,
					showDisplayFilter: false
				}]
			}
		]
	}]
});
