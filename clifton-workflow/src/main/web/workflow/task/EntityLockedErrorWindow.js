Clifton.workflow.task.EntityLockedErrorWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Entity Lock',
	iconCls: 'task',
	width: 700,
	height: 300,
	modal: true,
	minimizable: false,
	maximizable: false,
	resizable: false,
	buttonAlign: 'center',

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		items: [],
		listeners: {
			afterrender: function(fp) {
				const w = fp.getWindow();
				const data = w.data;

				w.setTitle(data.causeEntityLabel);
				fp.add({xtype: 'label', html: '<b>' + data.message + '</b><br/><br/>Click "Create Task" button if you want to create the following Workflow Task that allows you to change "' + data.causeEntityLabel + '".<br/><br/>'});

				let url = 'workflowTaskDefinitionLockListFind.json?active=true&limitUsingTaskEntityModifyCondition=true';
				url += data.causeEntityId ? '&lockEntityTableName=' + data.causeEntityTable + '&lockEntityId=' + data.causeEntityId : '&linkedEntityTableName=' + data.linkedEntityTable;
				if (data.eventType) {
					url += '&eventType=' + data.eventType;
				}
				const params = {};
				if (data.causeEntityClass && data.causeEntity) {
					// submit back updated (but not persisted yet) entity in order to evaluate "Fields Modified" comparisons correctly
					params.lockEntityClassName = data.causeEntityClass;
					params.lockEntityJson = JSON.stringify(data.causeEntity);
				}

				TCG.data.getDataPromise(url, fp, {params: params})
					.then(function(lockList) {
						if (lockList) {
							if (lockList.length === 1) {
								data.taskDefinition = lockList[0].definition;
								if (data.causeEntityId) {
									url = 'workflowTaskForDefinitionForLockEntityPopulate.json?taskDefinitionId=' + data.taskDefinition.id + '&lockEntityId=' + data.causeEntityId + '&lockEntityTableName=' + data.causeEntityTable;
								}
								else {
									url = 'workflowTaskForDefinitionPopulate.json?taskDefinitionId=' + data.taskDefinition.id + '&linkedEntityId=' + data.linkedEntityId;
								}
								return TCG.data.getDataPromise(url, fp);
							}
							data.lockList = lockList;
						}
					})
					.then(function(task) {
						if (task) {
							data.task = task;
							fp.add({fieldLabel: 'Task Definition', name: 'definition.name', detailIdField: 'definition.id', xtype: 'linkfield', detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow'});
							fp.add({fieldLabel: 'Linked Entity', name: 'linkedEntityLabel', detailIdField: 'linkedEntityFkFieldId', xtype: 'linkfield', detailPageClass: task.definition.linkedEntityTable.detailScreenClass});
							fp.getForm().setValues(task, true);
							fp.doLayout();
						}
						else if (data.lockList) {
							const definitions = [];
							let sameEntity = true;
							for (let i = 0; i < data.lockList.length; i++) {
								const lock = data.lockList[i];
								if (lock.linkedEntityBeanFieldPath !== 'id') {
									sameEntity = false;
								}
								definitions.push(lock.definition);
							}
							fp.add({
								fieldLabel: 'Task Definition', name: 'definition.name', detailIdField: 'definition.id', xtype: 'combo', mode: 'local',
								store: {
									xtype: 'jsonstore',
									fields: ['id', 'name', 'description'],
									data: definitions
								},
								listeners: {
									beforeselect: function(combo, record) {
										if (data.causeEntityId) {
											url = 'workflowTaskForDefinitionForLockEntityPopulate.json?taskDefinitionId=' + record.id + '&lockEntityId=' + data.causeEntityId + '&lockEntityTableName=' + data.causeEntityTable;
										}
										else {
											url = 'workflowTaskForDefinitionPopulate.json?taskDefinitionId=' + record.id + '&linkedEntityId=' + data.linkedEntityId;
										}
										TCG.data.getDataPromise(url, combo)
											.then(function(task) {
												data.task = task;
												fp.getForm().setValues(task, true);
											});
									}
								}
							});
							fp.add({fieldLabel: 'Linked Entity', name: 'linkedEntityLabel', detailIdField: 'linkedEntityFkFieldId', xtype: 'linkfield'});
							if (sameEntity) {
								fp.getForm().setValues({
									linkedEntityLabel: data.causeEntityLabel,
									linkedEntityFkFieldId: data.causeEntityId
								}, true);
							}
							fp.doLayout();
						}
						else {
							TCG.showError('Cannot find active Workflow Task Definition Lock for ' + data.causeEntityTable);
						}
					});
			}
		}
	}],

	buttons: [
		{
			text: 'Cancel',
			width: 120,
			handler: function() {
				const w = this.findParentBy(function(o) {
					return o.baseCls === 'x-window';
				});
				w.closeWindow();
			}
		},
		{
			text: 'Create Task',
			tooltip: 'Create new Workflow Task that unlocks this entity.',
			width: 120,
			handler: function() {
				const w = this.findParentBy(function(o) {
					return o.baseCls === 'x-window';
				});
				const data = w.data;
				if (data.task) {
					w.closeWindow();
					TCG.createComponent('Clifton.workflow.task.WorkflowTaskWindow', {defaultData: data.task});
				}
				else {
					TCG.showError('Task Definition is required');
				}
			}
		}
	]
});
