Clifton.workflow.task.template.WorkflowTaskDefinitionTemplateWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Task Definition Template',
	iconCls: 'task',
	height: 500,
	width: 700,

	items: [{
		xtype: 'formpanel',
		url: 'workflowTaskDefinitionTemplate.json',
		instructions: 'Templates are used to simplify creation of similar task definition as well as to allow "safe" creation of task definitions for non-admin users.',
		labelWidth: 150,

		items: [
			{fieldLabel: 'Template Definition', name: 'templateDefinition.name', hiddenName: 'templateDefinition.id', xtype: 'combo', url: 'workflowTaskDefinitionListFind.json', detailPageClass: 'Clifton.workflow.task.WorkflowTaskDefinitionWindow'},
			{fieldLabel: 'Template Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Definition Create Condition', name: 'taskDefinitionCreateCondition.name', hiddenName: 'taskDefinitionCreateCondition.id', xtype: 'system-condition-combo'},
			{fieldLabel: 'Default Name', name: 'defaultName'},
			{fieldLabel: 'Default Description', name: 'defaultDescription', xtype: 'textarea', grow: true, height: 50}
		]
	}]
});
