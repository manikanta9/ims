Clifton.workflow.task.template.TaskDefinitionFromTemplateWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Workflow Task Definition',
	iconCls: 'task',
	width: 750,
	height: 500,

	items: [{
		xtype: 'formpanel',
		instructions: 'Create a new Workflow Task Definition from the following Template.',
		labelWidth: 150,
		defaultDataIsReal: true,
		loadValidation: false,
		getSaveURL: function() {
			return 'workflowTaskDefinitionFromTemplateCreate.json';
		},

		listeners: {
			afterrender: function(fp) {
				const w = fp.getWindow();
				const template = fp.getDefaultData(w).template;
				fp.setFormValue('templateId', template.id, true);
				if (TCG.isNull(template.templateDefinition.taskGeneratorBean)) {
					fp.hideField('taskGeneratorBean');
				}
				else {
					fp.getForm().findField('taskGeneratorBean').allowBlank = false;

				}
				if (TCG.isNull(template.templateDefinition.linkedEntityLabel)) {
					fp.hideField('linkedEntityLabel');
				}
				else {
					fp.getForm().findField('linkedEntityLabel').allowBlank = false;
				}
				w.setTitle(template.name);
			},
			aftercreate: function(panel, closeOnSuccess) {
				const config = {
					defaultDataIsReal: true,
					defaultData: panel.getForm().formValues,
					openerCt: panel.ownerCt.openerCt
				};
				TCG.createComponent('Clifton.workflow.task.WorkflowTaskDefinitionWindow', config);
				if (!closeOnSuccess) {
					panel.getWindow().closeWindow();
				}
			}
		},

		items: [
			{name: 'templateId', xtype: 'hidden'},
			{fieldLabel: 'Template', name: 'template.name', xtype: 'linkfield', detailIdField: 'template.id', submitDetailField: false, detailPageClass: 'Clifton.workflow.task.template.WorkflowTaskDefinitionTemplateWindow'},
			{name: 'template.description', xtype: 'textarea', readOnly: true, height: 50, grow: true, submitValue: false},
			{fieldLabel: 'Definition Name', name: 'name', allowBlank: false},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70, grow: true, allowBlank: false},
			{
				fieldLabel: 'Task Generator Bean', name: 'taskGeneratorBean', hiddenName: 'taskGeneratorBeanId', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Workflow Task Generator', detailPageClass: 'Clifton.system.bean.BeanWindow',
				qtip: 'For ad-hoc tasks, this field will be blank. It will be populated for periodic and other task types and will reference a System Bean that contains the logic used to generate corresponding tasks and steps.',
				getDefaultData: function() {
					return {type: {group: {name: 'Workflow Task Generator'}}};
				}
			},
			{
				fieldLabel: 'Linked Entity', name: 'linkedEntityLabel', hiddenName: 'linkedEntityFkFieldId', xtype: 'combo', submitValue: true, displayField: 'label', url: 'REPLACE_ME_DYNAMICALLY',
				qtip: 'Optionally link all task for this definition to this entity. For example, unique monthly tasks for a specific client.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.getParentForm();
						const url = fp.getDefaultData(fp.getWindow()).template.templateDefinition.linkedEntityListURL;
						if (url) {
							combo.setUrl(url);
						}
					}
				}
			}
		]
	}]
});
