package com.clifton.workflow;


import com.clifton.core.beans.IdentityObject;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;


/**
 * The <code>WorkflowAware</code> interface marks DTO objects that support workflows.
 * Every DTO that supports workflows must always belong to a specific workflow state and be in a specific workflow status.
 * <p>
 * use {@link @WorkflowConfig annotation to drive how assignments are done for this entity}
 *
 * @author vgomelsky
 */
public interface WorkflowAware extends IdentityObject {

	/**
	 * Returns current workflow state that the DTO is in.
	 */
	public WorkflowState getWorkflowState();


	/**
	 * Sets the new workflow state.
	 */
	public void setWorkflowState(WorkflowState workflowState);


	/**
	 * Returns current workflow status that the DTO is in.
	 */
	public WorkflowStatus getWorkflowStatus();


	/**
	 * Sets the new workflow status.
	 */
	public void setWorkflowStatus(WorkflowStatus workflowStatus);
}
