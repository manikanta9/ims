package com.clifton.workflow;


import java.util.Date;


/**
 * The <code>BaseNamedWorkflowEffectiveDateAwareEntity</code> ...
 *
 * @author Mary Anderson
 */
public class BaseNamedWorkflowEffectiveDateAwareEntity extends BasedNamedWorkflowAwareEntity implements WorkflowEffectiveDateAware {

	private Date workflowStateEffectiveStartDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getWorkflowStateEffectiveStartDate() {
		return this.workflowStateEffectiveStartDate;
	}


	@Override
	public void setWorkflowStateEffectiveStartDate(Date workflowStateEffectiveStartDate) {
		this.workflowStateEffectiveStartDate = workflowStateEffectiveStartDate;
	}
}
