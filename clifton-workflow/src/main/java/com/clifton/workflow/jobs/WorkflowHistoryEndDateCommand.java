package com.clifton.workflow.jobs;

import com.clifton.core.util.status.Status;


/**
 * @author AbhinayaM
 */
public class WorkflowHistoryEndDateCommand {

	private boolean synchronous;

	private Integer weekdaysBack;

	private Short workflowId;

	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRunId() {
		return "WF-" + (getWorkflowId() == null ? "ALL" : getWorkflowId()) + "-DAYS-" + getWeekdaysBack();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Short getWorkflowId() {
		return this.workflowId;
	}


	public void setWorkflowId(Short workflowId) {
		this.workflowId = workflowId;
	}


	public Integer getWeekdaysBack() {
		return this.weekdaysBack;
	}


	public void setWeekdaysBack(Integer weekdaysBack) {
		this.weekdaysBack = weekdaysBack;
	}
}
