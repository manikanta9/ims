package com.clifton.workflow.jobs;

import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.workflow.history.WorkflowHistoryService;

import java.util.Map;


/**
 * @author AbhinayaM
 */
public class WorkflowHistoryEndDatePopulatorJob implements Task, StatusHolderObjectAware<Status> {


	private WorkflowHistoryService workflowHistoryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private StatusHolderObject<Status> statusHolderObject;

	private Integer weekdaysBack;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		WorkflowHistoryEndDateCommand command = new WorkflowHistoryEndDateCommand();
		command.setWeekdaysBack(getWeekdaysBack());
		command.setStatus(getStatusHolderObject().getStatus());
		command.setSynchronous(true);
		return getWorkflowHistoryService().populateWorkflowHistoryEndDateListForCommand(command);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getters and Setters                             ////////
	////////////////////////////////////////////////////////////////////////////


	public StatusHolderObject<Status> getStatusHolderObject() {
		return this.statusHolderObject;
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	public Integer getWeekdaysBack() {
		return this.weekdaysBack;
	}


	public void setWeekdaysBack(Integer weekdaysBack) {
		this.weekdaysBack = weekdaysBack;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}
}
