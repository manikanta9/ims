package com.clifton.workflow.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableDateRangeSystemHierarchyItemSearchForm;


/**
 * <code>BaseWorkflowAwareDateRangeSystemHierarchyItemSearchForm</code> provide search parameters for:
 * {@link com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm}
 * {@link com.clifton.core.dataaccess.search.form.DateRangeAwareSearchForm}
 * {@link com.clifton.system.hierarchy.assignment.search.BaseSystemHierarchyItemSearchForm}
 * {@link BaseWorkflowAwareSearchForm}
 *
 * @author manderson
 */
public abstract class BaseWorkflowAwareDateRangeSystemHierarchyItemSearchForm extends BaseAuditableDateRangeSystemHierarchyItemSearchForm implements WorkflowAwareSearchForm {

	/////////  Workflow Status Filters ///////////////

	@SearchField(searchField = "workflowStatus.id")
	private Short workflowStatusId;

	@SearchField(searchField = "workflowStatus.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Short workflowStatusIdOrNull;

	@SearchField(searchField = "workflowStatus.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] workflowStatusIds;

	@SearchField(searchField = "name", searchFieldPath = "workflowStatus", comparisonConditions = ComparisonConditions.EQUALS)
	private String workflowStatusNameEquals;

	@SearchField(searchField = "name", searchFieldPath = "workflowStatus", comparisonConditions = {ComparisonConditions.BEGINS_WITH, ComparisonConditions.LIKE})
	private String workflowStatusName;

	@SearchField(searchFieldPath = "workflowStatus", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] workflowStatusNames;

	@SearchField(searchField = "name", searchFieldPath = "workflowStatus", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL, leftJoin = true)
	private String workflowStatusNameOrNull;


	@SearchField(searchField = "workflowStatus.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short excludeWorkflowStatusId;

	@SearchField(searchField = "workflowStatus.id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Short[] excludeWorkflowStatusIds;

	@SearchField(searchField = "name", searchFieldPath = "workflowStatus", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeWorkflowStatusName;

	@SearchField(searchField = "name", searchFieldPath = "workflowStatus", comparisonConditions = ComparisonConditions.NOT_IN)
	private String[] excludeWorkflowStatusNames;


	/////////  Workflow State Filters ///////////////

	@SearchField(searchField = "workflowState.id")
	private Short workflowStateId;

	@SearchField(searchField = "workflowState.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Short workflowStateIdOrNull;

	@SearchField(searchField = "workflowState.id", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL)
	private Short[] workflowStateIdsOrNull;

	@SearchField(searchField = "workflowState.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] workflowStateIds;

	@SearchField(searchField = "name", searchFieldPath = "workflowState", comparisonConditions = ComparisonConditions.EQUALS)
	private String workflowStateNameEquals;

	@SearchField(searchField = "name", searchFieldPath = "workflowState", comparisonConditions = {ComparisonConditions.BEGINS_WITH, ComparisonConditions.LIKE})
	private String workflowStateName;

	@SearchField(searchField = "name", searchFieldPath = "workflowState", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL, leftJoin = true)
	private String workflowStateNameOrNull;

	@SearchField(searchFieldPath = "workflowState", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] workflowStateNames;

	@SearchField(searchField = "name", searchFieldPath = "workflowState", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL, leftJoin = true)
	private String[] workflowStateNamesOrNull;

	@SearchField(searchField = "name", searchFieldPath = "workflowState", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeWorkflowStateName;

	@SearchField(searchField = "name", searchFieldPath = "workflowState", comparisonConditions = ComparisonConditions.NOT_IN)
	private String[] excludeWorkflowStateNames;

	@SearchField(searchField = "workflowState.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short excludeWorkflowStateId;

	@SearchField(searchField = "workflowState.id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Short[] excludeWorkflowStateIds;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Short getWorkflowStatusId() {
		return this.workflowStatusId;
	}


	@Override
	public void setWorkflowStatusId(Short workflowStatusId) {
		this.workflowStatusId = workflowStatusId;
	}


	@Override
	public Short getWorkflowStatusIdOrNull() {
		return this.workflowStatusIdOrNull;
	}


	@Override
	public void setWorkflowStatusIdOrNull(Short workflowStatusIdOrNull) {
		this.workflowStatusIdOrNull = workflowStatusIdOrNull;
	}


	@Override
	public Short[] getWorkflowStatusIds() {
		return this.workflowStatusIds;
	}


	@Override
	public void setWorkflowStatusIds(Short[] workflowStatusIds) {
		this.workflowStatusIds = workflowStatusIds;
	}


	@Override
	public String getWorkflowStatusNameEquals() {
		return this.workflowStatusNameEquals;
	}


	@Override
	public void setWorkflowStatusNameEquals(String workflowStatusNameEquals) {
		this.workflowStatusNameEquals = workflowStatusNameEquals;
	}


	@Override
	public String getWorkflowStatusName() {
		return this.workflowStatusName;
	}


	@Override
	public void setWorkflowStatusName(String workflowStatusName) {
		this.workflowStatusName = workflowStatusName;
	}


	@Override
	public String[] getWorkflowStatusNames() {
		return this.workflowStatusNames;
	}


	@Override
	public void setWorkflowStatusNames(String[] workflowStatusNames) {
		this.workflowStatusNames = workflowStatusNames;
	}


	@Override
	public String getWorkflowStatusNameOrNull() {
		return this.workflowStatusNameOrNull;
	}


	@Override
	public void setWorkflowStatusNameOrNull(String workflowStatusNameOrNull) {
		this.workflowStatusNameOrNull = workflowStatusNameOrNull;
	}


	@Override
	public Short getExcludeWorkflowStatusId() {
		return this.excludeWorkflowStatusId;
	}


	@Override
	public void setExcludeWorkflowStatusId(Short excludeWorkflowStatusId) {
		this.excludeWorkflowStatusId = excludeWorkflowStatusId;
	}


	@Override
	public Short[] getExcludeWorkflowStatusIds() {
		return this.excludeWorkflowStatusIds;
	}


	@Override
	public void setExcludeWorkflowStatusIds(Short[] excludeWorkflowStatusIds) {
		this.excludeWorkflowStatusIds = excludeWorkflowStatusIds;
	}


	@Override
	public String getExcludeWorkflowStatusName() {
		return this.excludeWorkflowStatusName;
	}


	@Override
	public void setExcludeWorkflowStatusName(String excludeWorkflowStatusName) {
		this.excludeWorkflowStatusName = excludeWorkflowStatusName;
	}


	@Override
	public String[] getExcludeWorkflowStatusNames() {
		return this.excludeWorkflowStatusNames;
	}


	@Override
	public void setExcludeWorkflowStatusNames(String[] excludeWorkflowStatusNames) {
		this.excludeWorkflowStatusNames = excludeWorkflowStatusNames;
	}


	@Override
	public Short getWorkflowStateId() {
		return this.workflowStateId;
	}


	@Override
	public void setWorkflowStateId(Short workflowStateId) {
		this.workflowStateId = workflowStateId;
	}


	@Override
	public Short getWorkflowStateIdOrNull() {
		return this.workflowStateIdOrNull;
	}


	@Override
	public void setWorkflowStateIdOrNull(Short workflowStateIdOrNull) {
		this.workflowStateIdOrNull = workflowStateIdOrNull;
	}


	@Override
	public Short[] getWorkflowStateIdsOrNull() {
		return this.workflowStateIdsOrNull;
	}


	@Override
	public void setWorkflowStateIdsOrNull(Short[] workflowStateIdsOrNull) {
		this.workflowStateIdsOrNull = workflowStateIdsOrNull;
	}


	@Override
	public Short[] getWorkflowStateIds() {
		return this.workflowStateIds;
	}


	@Override
	public void setWorkflowStateIds(Short[] workflowStateIds) {
		this.workflowStateIds = workflowStateIds;
	}


	@Override
	public String getWorkflowStateNameEquals() {
		return this.workflowStateNameEquals;
	}


	@Override
	public void setWorkflowStateNameEquals(String workflowStateNameEquals) {
		this.workflowStateNameEquals = workflowStateNameEquals;
	}


	@Override
	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	@Override
	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	@Override
	public String getWorkflowStateNameOrNull() {
		return this.workflowStateNameOrNull;
	}


	@Override
	public void setWorkflowStateNameOrNull(String workflowStateNameOrNull) {
		this.workflowStateNameOrNull = workflowStateNameOrNull;
	}


	@Override
	public String[] getWorkflowStateNames() {
		return this.workflowStateNames;
	}


	@Override
	public void setWorkflowStateNames(String[] workflowStateNames) {
		this.workflowStateNames = workflowStateNames;
	}


	@Override
	public String[] getWorkflowStateNamesOrNull() {
		return this.workflowStateNamesOrNull;
	}


	@Override
	public void setWorkflowStateNamesOrNull(String[] workflowStateNamesOrNull) {
		this.workflowStateNamesOrNull = workflowStateNamesOrNull;
	}


	@Override
	public String getExcludeWorkflowStateName() {
		return this.excludeWorkflowStateName;
	}


	@Override
	public void setExcludeWorkflowStateName(String excludeWorkflowStateName) {
		this.excludeWorkflowStateName = excludeWorkflowStateName;
	}


	@Override
	public String[] getExcludeWorkflowStateNames() {
		return this.excludeWorkflowStateNames;
	}


	@Override
	public void setExcludeWorkflowStateNames(String[] excludeWorkflowStateNames) {
		this.excludeWorkflowStateNames = excludeWorkflowStateNames;
	}


	@Override
	public Short getExcludeWorkflowStateId() {
		return this.excludeWorkflowStateId;
	}


	@Override
	public void setExcludeWorkflowStateId(Short excludeWorkflowStateId) {
		this.excludeWorkflowStateId = excludeWorkflowStateId;
	}


	@Override
	public Short[] getExcludeWorkflowStateIds() {
		return this.excludeWorkflowStateIds;
	}


	@Override
	public void setExcludeWorkflowStateIds(Short[] excludeWorkflowStateIds) {
		this.excludeWorkflowStateIds = excludeWorkflowStateIds;
	}
}
