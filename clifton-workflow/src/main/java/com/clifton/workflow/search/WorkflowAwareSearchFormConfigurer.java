package com.clifton.workflow.search;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.List;


public class WorkflowAwareSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final WorkflowAwareSearchForm workflowAwareSearchForm;
	private WorkflowDefinitionService workflowDefinitionService;

	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////


	public WorkflowAwareSearchFormConfigurer(BaseEntitySearchForm searchForm, WorkflowDefinitionService workflowDefinitionService) {
		this(searchForm, workflowDefinitionService, false);
	}


	/**
	 * Allows the child class to call configureSearchForm after their services are set up
	 */
	public WorkflowAwareSearchFormConfigurer(BaseEntitySearchForm searchForm, WorkflowDefinitionService workflowDefinitionService, boolean doNotConfigure) {
		super(searchForm, true);

		// Validate the SearchForm implements WorkflowAwareSearchForm
		if (!(searchForm instanceof WorkflowAwareSearchForm)) {
			throw new IllegalArgumentException("In order to use the WorkflowAware Search Form Configurer, the search form must implement the WorkflowAwareSearchForm interface.");
		}
		this.workflowAwareSearchForm = (WorkflowAwareSearchForm) searchForm;
		this.workflowDefinitionService = workflowDefinitionService;
		if (!doNotConfigure) {
			configureSearchForm();
		}
	}


	@Override
	protected void configureSearchForm() {
		convertWorkflowStatusFields();
		convertWorkflowStateFields();

		super.configureSearchForm();
	}


	private void convertWorkflowStatusFields() {
		// Workflow Status Name Equals - Convert to WorkflowStatusID Equals
		String name = getRestrictionValue(this.workflowAwareSearchForm.getWorkflowStatusNameEquals(), "workflowStatusNameEquals");
		if (!StringUtils.isEmpty(name)) {
			this.workflowAwareSearchForm.setWorkflowStatusId(getWorkflowStatusIdForName(name));
			this.workflowAwareSearchForm.setWorkflowStatusNameEquals(null);
			this.workflowAwareSearchForm.removeSearchRestriction("workflowStatusNameEquals");
		}
		name = getRestrictionValue(this.workflowAwareSearchForm.getWorkflowStatusNameOrNull(), "workflowStatusNameOrNull");
		if (!StringUtils.isEmpty(name)) {
			this.workflowAwareSearchForm.setWorkflowStatusIdOrNull(getWorkflowStatusIdForName(name));
			this.workflowAwareSearchForm.setWorkflowStatusNameOrNull(null);
			this.workflowAwareSearchForm.removeSearchRestriction("workflowStatusNameOrNull");
		}
		String[] names = getRestrictionValue(this.workflowAwareSearchForm.getWorkflowStatusNames(), "workflowStatusNames");
		if (names != null && names.length > 0) {
			Short[] statusIds = new Short[names.length];
			for (int i = 0; i < names.length; i++) {
				statusIds[i] = getWorkflowStatusIdForName(names[i]);
			}
			this.workflowAwareSearchForm.setWorkflowStatusIds(statusIds);
			this.workflowAwareSearchForm.setWorkflowStatusNames(null);
			this.workflowAwareSearchForm.removeSearchRestriction("workflowStatusNames");
		}
		// Exclude Workflow Status Name - Convert to Exclude WorkflowStatusID Equals
		name = getRestrictionValue(this.workflowAwareSearchForm.getExcludeWorkflowStatusName(), "excludeWorkflowStatusName");
		if (!StringUtils.isEmpty(name)) {
			this.workflowAwareSearchForm.setExcludeWorkflowStatusId(getWorkflowStatusIdForName(name));
			this.workflowAwareSearchForm.setExcludeWorkflowStatusName(null);
			this.workflowAwareSearchForm.removeSearchRestriction("excludeWorkflowStatusName");
		}
		// Exclude Workflow status names - Convert to Exclude Workflow Status IDs
		names = getRestrictionValue(this.workflowAwareSearchForm.getExcludeWorkflowStatusNames(), "excludeWorkflowStatusNames");
		if (names != null && names.length > 0) {
			Short[] statusIds = new Short[names.length];
			for (int i = 0; i < names.length; i++) {
				statusIds[i] = getWorkflowStatusIdForName(names[i]);
			}
			this.workflowAwareSearchForm.setExcludeWorkflowStatusIds(statusIds);
			this.workflowAwareSearchForm.setExcludeWorkflowStatusNames(null);
			this.workflowAwareSearchForm.removeSearchRestriction("excludeWorkflowStatusNames");
		}
	}


	private void convertWorkflowStateFields() {
		// Workflow State Name Equals = Convert to Workflow State ID Equals (Or In)
		String name = getRestrictionValue(this.workflowAwareSearchForm.getWorkflowStateNameEquals(), "workflowStateNameEquals");
		if (!StringUtils.isEmpty(name)) {
			applyWorkflowStateIdFilter(getWorkflowStateIdsForName(name), false, false);
			this.workflowAwareSearchForm.setWorkflowStateNameEquals(null);
			this.workflowAwareSearchForm.removeSearchRestriction("workflowStateNameEquals");
		}
		name = getRestrictionValue(this.workflowAwareSearchForm.getWorkflowStateNameOrNull(), "workflowStateNameOrNull");
		if (!StringUtils.isEmpty(name)) {
			applyWorkflowStateIdFilter(getWorkflowStateIdsForName(name), false, true);
			this.workflowAwareSearchForm.setWorkflowStateNameOrNull(null);
			this.workflowAwareSearchForm.removeSearchRestriction("workflowStateNameOrNull");
		}
		// Workflow State Name In to Workflow State ID In
		String[] names = getRestrictionValue(this.workflowAwareSearchForm.getWorkflowStateNames(), "workflowStateNames");
		if (names != null && names.length > 0) {
			Short[] stateIds = new Short[0];
			for (String workflowStateName : names) {
				stateIds = ArrayUtils.addAll(stateIds, getWorkflowStateIdsForName(workflowStateName));
			}
			applyWorkflowStateIdFilter(stateIds, false, false);
			this.workflowAwareSearchForm.setWorkflowStateNames(null);
			this.workflowAwareSearchForm.removeSearchRestriction("workflowStateNames");
		}
		names = getRestrictionValue(this.workflowAwareSearchForm.getWorkflowStateNamesOrNull(), "workflowStateNamesOrNull");
		if (names != null && names.length > 0) {
			Short[] stateIds = new Short[0];
			for (String workflowStateName : names) {
				stateIds = ArrayUtils.addAll(stateIds, getWorkflowStateIdsForName(workflowStateName));
			}
			applyWorkflowStateIdFilter(stateIds, false, true);
			this.workflowAwareSearchForm.setWorkflowStateNamesOrNull(null);
			this.workflowAwareSearchForm.removeSearchRestriction("workflowStateNamesOrNull");
		}
		// Exclude Workflow State Name to Exclude Workflow State ID
		name = getRestrictionValue(this.workflowAwareSearchForm.getExcludeWorkflowStateName(), "excludeWorkflowStateName");
		if (!StringUtils.isEmpty(name)) {
			Short[] stateIds = getWorkflowStateIdsForName(this.workflowAwareSearchForm.getExcludeWorkflowStateName());
			applyWorkflowStateIdFilter(stateIds, true, false);
			this.workflowAwareSearchForm.setExcludeWorkflowStateName(null);
			this.workflowAwareSearchForm.removeSearchRestriction("excludeWorkflowStateName");
		}
		// Exclude Workflow State Name NOT In to Workflow State ID NOT In
		names = getRestrictionValue(this.workflowAwareSearchForm.getExcludeWorkflowStateNames(), "excludeWorkflowStateNames");
		if (names != null && names.length > 0) {
			Short[] stateIds = new Short[0];
			for (String workflowStateName : names) {
				stateIds = ArrayUtils.addAll(stateIds, getWorkflowStateIdsForName(workflowStateName));
			}
			applyWorkflowStateIdFilter(stateIds, true, false);
			this.workflowAwareSearchForm.setExcludeWorkflowStateNames(null);
			this.workflowAwareSearchForm.removeSearchRestriction("excludeWorkflowStateNames");
		}
	}


	private void applyWorkflowStateIdFilter(Short[] stateIds, boolean exclude, boolean orNull) {
		if (stateIds != null && stateIds.length > 0) {
			if (stateIds.length == 1) {
				if (exclude) {
					this.workflowAwareSearchForm.setExcludeWorkflowStateId(stateIds[0]);
				}
				else if (orNull) {
					this.workflowAwareSearchForm.setWorkflowStateIdOrNull(stateIds[0]);
				}
				else {
					this.workflowAwareSearchForm.setWorkflowStateId(stateIds[0]);
				}
			}
			else {
				if (exclude) {
					this.workflowAwareSearchForm.setExcludeWorkflowStateIds(stateIds);
				}
				else if (orNull) {
					this.workflowAwareSearchForm.setWorkflowStateIdsOrNull(stateIds);
				}
				else {
					this.workflowAwareSearchForm.setWorkflowStateIds(stateIds);
				}
			}
		}
	}


	/**
	 * Returns the restriction value - will return value if not null/empty
	 * Otherwise will look up value based on fieldName from the restriction list
	 * Note: Pass in the searchForm property value to prevent using reflection
	 */
	private String getRestrictionValue(String value, String fieldName) {
		if (!StringUtils.isEmpty(value)) {
			return value;
		}
		else if (this.workflowAwareSearchForm.containsSearchRestriction(fieldName)) {
			return (String) this.workflowAwareSearchForm.getSearchRestriction(fieldName).getValue();
		}
		return null;
	}


	@SuppressWarnings("rawtypes")
	private String[] getRestrictionValue(String[] value, String fieldName) {
		if (value != null && value.length > 0) {
			return value;
		}
		else if (this.workflowAwareSearchForm.containsSearchRestriction(fieldName)) {
			Object restrictionValue = this.workflowAwareSearchForm.getSearchRestriction(fieldName).getValue();
			if (restrictionValue != null) {
				if (restrictionValue instanceof String[]) {
					return (String[]) restrictionValue;
				}
				else if (restrictionValue instanceof List) {
					String[] result = new String[((List) restrictionValue).size()];
					((List) restrictionValue).toArray(result);
					return result;
				}
				throw new RuntimeException("Unable to convert restriction value to String[] for field name [" + fieldName + "] and value [" + restrictionValue + "] for class [" + restrictionValue.getClass().getName() + "].  Expecting only String[] or ArrayList.");
			}
		}
		return null;
	}


	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////


	private Short[] getWorkflowStateIdsForName(String workflowStateName) {
		List<WorkflowState> stateList = this.workflowDefinitionService.getWorkflowStateListByName(workflowStateName);
		if (!CollectionUtils.isEmpty(stateList)) {
			return BeanUtils.getBeanIdentityArray(stateList, Short.class);
		}
		return null;
	}


	private Short getWorkflowStatusIdForName(String workflowStatusName) {
		WorkflowStatus status = this.workflowDefinitionService.getWorkflowStatusByName(workflowStatusName);
		if (status != null) {
			return status.getId();
		}
		return null;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}
}
