package com.clifton.workflow.search;

import com.clifton.core.dataaccess.search.form.RestrictiveSearchForm;


/**
 * @author manderson
 */
public interface WorkflowAwareSearchForm extends RestrictiveSearchForm {


	public Short getWorkflowStatusId();


	public void setWorkflowStatusId(Short workflowStatusId);


	public Short[] getWorkflowStatusIds();


	public void setWorkflowStatusIds(Short[] workflowStatusIds);


	public String getWorkflowStatusNameEquals();


	public void setWorkflowStatusNameEquals(String workflowStatusNameEquals);


	public String getWorkflowStatusName();


	public void setWorkflowStatusName(String workflowStatusName);


	public String[] getWorkflowStatusNames();


	public void setWorkflowStatusNames(String[] workflowStatusNames);


	public Short getExcludeWorkflowStatusId();


	public void setExcludeWorkflowStatusId(Short excludeWorkflowStatusId);


	public Short[] getExcludeWorkflowStatusIds();


	public void setExcludeWorkflowStatusIds(Short[] excludeWorkflowStatusIds);


	public String getExcludeWorkflowStatusName();


	public void setExcludeWorkflowStatusName(String excludeWorkflowStatusName);


	public String[] getExcludeWorkflowStatusNames();


	public void setExcludeWorkflowStatusNames(String[] excludeWorkflowStatusNames);


	public Short getWorkflowStateId();


	public void setWorkflowStateId(Short workflowStateId);


	public Short[] getWorkflowStateIds();


	public void setWorkflowStateIds(Short[] workflowStateIds);


	public String getWorkflowStateNameEquals();


	public void setWorkflowStateNameEquals(String workflowStateNameEquals);


	public String getWorkflowStateName();


	public void setWorkflowStateName(String workflowStateName);


	public String[] getWorkflowStateNames();


	public void setWorkflowStateNames(String[] workflowStateNames);


	public String getExcludeWorkflowStateName();


	public void setExcludeWorkflowStateName(String excludeWorkflowStateName);


	public String[] getExcludeWorkflowStateNames();


	public void setExcludeWorkflowStateNames(String[] excludeWorkflowStateNames);


	public Short getExcludeWorkflowStateId();


	public void setExcludeWorkflowStateId(Short excludeWorkflowStateId);


	public Short[] getExcludeWorkflowStateIds();


	public void setExcludeWorkflowStateIds(Short[] excludeWorkflowStateIds);


	public Short getWorkflowStatusIdOrNull();


	public void setWorkflowStatusIdOrNull(Short workflowStatusIdOrNull);


	public String getWorkflowStatusNameOrNull();


	public void setWorkflowStatusNameOrNull(String workflowStatusNameOrNull);


	public Short getWorkflowStateIdOrNull();


	public void setWorkflowStateIdOrNull(Short workflowStateIdOrNull);


	public Short[] getWorkflowStateIdsOrNull();


	public void setWorkflowStateIdsOrNull(Short[] workflowStateIdsOrNull);


	public String getWorkflowStateNameOrNull();


	public void setWorkflowStateNameOrNull(String workflowStateNameOrNull);


	public String[] getWorkflowStateNamesOrNull();


	public void setWorkflowStateNamesOrNull(String[] workflowStateNamesOrNull);
}
