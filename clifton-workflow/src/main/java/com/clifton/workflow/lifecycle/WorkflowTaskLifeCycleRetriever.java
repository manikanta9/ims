package com.clifton.workflow.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.system.audit.SystemAuditLifeCycleRetriever;
import com.clifton.system.lifecycle.SystemLifeCycleEvent;
import com.clifton.system.lifecycle.SystemLifeCycleEventContext;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.audit.WorkflowTaskAuditService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class WorkflowTaskLifeCycleRetriever extends SystemAuditLifeCycleRetriever {

	private WorkflowTaskAuditService workflowTaskAuditService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof WorkflowTask;
	}


	@Override
	public List<SystemLifeCycleEvent> getSystemLifeCycleEventListForEntity(SystemLifeCycleEventContext lifeCycleEventContext) {
		List<SystemLifeCycleEvent> lifeCycleEventList = new ArrayList<>();

		WorkflowTask task = (WorkflowTask) lifeCycleEventContext.getCurrentProcessingEntity();
		if (task.getLinkedEntityFkFieldId() != null) {
			DataTable auditHistory = getWorkflowTaskAuditService().getWorkflowTaskAuditEventList(task.getId(), true);
			addAuditEvents(lifeCycleEventList, lifeCycleEventContext, auditHistory);
		}

		return lifeCycleEventList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskAuditService getWorkflowTaskAuditService() {
		return this.workflowTaskAuditService;
	}


	public void setWorkflowTaskAuditService(WorkflowTaskAuditService workflowTaskAuditService) {
		this.workflowTaskAuditService = workflowTaskAuditService;
	}
}
