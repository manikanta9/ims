package com.clifton.workflow.lifecycle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.lifecycle.SystemLifeCycleEvent;
import com.clifton.system.lifecycle.SystemLifeCycleEventContext;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleEventRetriever;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author manderson
 */
@Component
public class WorkflowLifeCycleRetriever implements SystemLifeCycleEventRetriever {

	private SecurityUserService securityUserService;

	private SystemSchemaService systemSchemaService;

	private WorkflowHistoryService workflowHistoryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof WorkflowAware;
	}


	@Override
	public List<SystemLifeCycleEvent> getSystemLifeCycleEventListForEntity(SystemLifeCycleEventContext lifeCycleEventContext) {
		List<WorkflowHistory> historyList = getWorkflowHistoryService().getWorkflowHistoryListByWorkflowAwareEntity((WorkflowAware) lifeCycleEventContext.getCurrentProcessingEntity(), true);
		return CollectionUtils.getConverted(historyList, history -> convertToSystemLifeCycleEvent(history, lifeCycleEventContext));
	}


	private SystemLifeCycleEvent convertToSystemLifeCycleEvent(WorkflowHistory history, SystemLifeCycleEventContext lifeCycleEventContext) {
		SystemLifeCycleEvent event = new SystemLifeCycleEvent();
		event.setLinkedSystemTable(history.getTable());
		event.setLinkedFkFieldId(history.getFkFieldId());
		event.setSourceSystemTable(lifeCycleEventContext.getSystemTableByName(getSystemSchemaService(), "WorkflowHistory"));
		event.setSourceFkFieldId(BeanUtils.getIdentityAsLong(history));
		event.setEventDate(history.getEffectiveEndStateStartDate());
		event.setEventSource("Workflow Transition");
		event.setEventAction(getActionNameForWorkflowHistory(history));
		event.setEventDetails(getEventDetailsFromWorkflowHistory(history));
		event.setEventUser(getSecurityUserForWorkflowHistory(history, lifeCycleEventContext));
		return event;
	}


	private SecurityUser getSecurityUserForWorkflowHistory(WorkflowHistory workflowHistory, SystemLifeCycleEventContext lifeCycleEventContext) {
		if (StringUtils.contains(workflowHistory.getDescription(), "[SYSTEM]")) {
			return lifeCycleEventContext.getSecurityUserByName(getSecurityUserService(), SecurityUser.SYSTEM_USER);
		}
		return lifeCycleEventContext.getSecurityUserById(getSecurityUserService(), workflowHistory.getCreateUserId());
	}


	private String getActionNameForWorkflowHistory(WorkflowHistory workflowHistory) {
		String description = workflowHistory.getDescription();
		if (StringUtils.contains(description, "did [")) {
			int startIndex = description.indexOf("did [") + 5;
			return StringUtils.substring(description, startIndex, description.indexOf("]", startIndex));
		}
		// Otherwise return the end workflow state name
		return workflowHistory.getEndWorkflowState().getName();
	}


	private String getEventDetailsFromWorkflowHistory(WorkflowHistory workflowHistory) {
		String description = workflowHistory.getDescription();
		if (StringUtils.contains(description, "because [")) {
			return "because" + StringUtils.substringAfterFirst(description, "because");
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}
}
