package com.clifton.workflow.history;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>WorkflowHistorySearchForm</code> defines search rules for WorkflowHistory objects.
 *
 * @author manderson
 */
public class WorkflowHistorySearchForm extends BaseAuditableEntitySearchForm {

	/**
	 * NOTE: tableId is required only - If table name is passed - SearchConfigurer will convert it to tableId before searching
	 */
	@SearchField(searchField = "name", searchFieldPath = "table", comparisonConditions = ComparisonConditions.EQUALS)
	private String tableName;

	@SearchField(searchField = "table.id")
	private Short tableId;

	@SearchField(searchField = "fkFieldId", comparisonConditions = ComparisonConditions.EQUALS)
	private Long entityId;

	@SearchField(searchField = "workflow.id", searchFieldPath = "endWorkflowState")
	private Short workflowId;

	@SearchField(searchField = "workflow.id", searchFieldPath = "endWorkflowState")
	private Short[] workflowIds;

	@SearchField(searchField = "name", searchFieldPath = "endWorkflowState.workflow")
	private String workflowName;

	@SearchField(searchField = "startWorkflowState.id")
	private Short startWorkflowStateId;

	@SearchField(searchField = "name", searchFieldPath = "startWorkflowState")
	private String startWorkflowStateName;

	@SearchField(searchField = "status.id", searchFieldPath = "startWorkflowState")
	private Short startWorkflowStatusId;


	@SearchField(searchField = "endWorkflowState.id")
	private Short endWorkflowStateId;

	@SearchField(searchField = "name", searchFieldPath = "endWorkflowState")
	private String endWorkflowStateName;

	@SearchField(searchField = "name", searchFieldPath = "endWorkflowState", comparisonConditions = ComparisonConditions.EQUALS)
	private String endWorkflowStateNameEquals;

	@SearchField(searchField = "status.id", searchFieldPath = "endWorkflowState")
	private Short endWorkflowStatusId;

	@SearchField(searchField = "endStateStartDate")
	private Date endStateStartDate;

	@SearchField(searchField = "endStateEndDate")
	private Date endStateEndDate;

	@SearchField(searchField = "effectiveEndStateStartDate")
	private Date effectiveEndStateStartDate;

	@SearchField(searchField = "effectiveEndStateEndDate")
	private Date effectiveEndStateEndDate;


	@SearchField(searchField = "description")
	private String description;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		SearchUtils.validateRequiredFilter(this, "tableName", "tableId", "description", "workflowId", "workflowIds");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public Long getEntityId() {
		return this.entityId;
	}


	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}


	public Short getWorkflowId() {
		return this.workflowId;
	}


	public void setWorkflowId(Short workflowId) {
		this.workflowId = workflowId;
	}


	public String getWorkflowName() {
		return this.workflowName;
	}


	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}


	public Short getStartWorkflowStateId() {
		return this.startWorkflowStateId;
	}


	public void setStartWorkflowStateId(Short startWorkflowStateId) {
		this.startWorkflowStateId = startWorkflowStateId;
	}


	public String getStartWorkflowStateName() {
		return this.startWorkflowStateName;
	}


	public void setStartWorkflowStateName(String startWorkflowStateName) {
		this.startWorkflowStateName = startWorkflowStateName;
	}


	public Short getStartWorkflowStatusId() {
		return this.startWorkflowStatusId;
	}


	public void setStartWorkflowStatusId(Short startWorkflowStatusId) {
		this.startWorkflowStatusId = startWorkflowStatusId;
	}


	public Short getEndWorkflowStateId() {
		return this.endWorkflowStateId;
	}


	public void setEndWorkflowStateId(Short endWorkflowStateId) {
		this.endWorkflowStateId = endWorkflowStateId;
	}


	public String getEndWorkflowStateName() {
		return this.endWorkflowStateName;
	}


	public void setEndWorkflowStateName(String endWorkflowStateName) {
		this.endWorkflowStateName = endWorkflowStateName;
	}


	public String getEndWorkflowStateNameEquals() {
		return this.endWorkflowStateNameEquals;
	}


	public void setEndWorkflowStateNameEquals(String endWorkflowStateNameEquals) {
		this.endWorkflowStateNameEquals = endWorkflowStateNameEquals;
	}


	public Short getEndWorkflowStatusId() {
		return this.endWorkflowStatusId;
	}


	public void setEndWorkflowStatusId(Short endWorkflowStatusId) {
		this.endWorkflowStatusId = endWorkflowStatusId;
	}


	public Date getEndStateStartDate() {
		return this.endStateStartDate;
	}


	public void setEndStateStartDate(Date endStateStartDate) {
		this.endStateStartDate = endStateStartDate;
	}


	public Date getEndStateEndDate() {
		return this.endStateEndDate;
	}


	public void setEndStateEndDate(Date endStateEndDate) {
		this.endStateEndDate = endStateEndDate;
	}


	public Date getEffectiveEndStateStartDate() {
		return this.effectiveEndStateStartDate;
	}


	public void setEffectiveEndStateStartDate(Date effectiveEndStateStartDate) {
		this.effectiveEndStateStartDate = effectiveEndStateStartDate;
	}


	public Date getEffectiveEndStateEndDate() {
		return this.effectiveEndStateEndDate;
	}


	public void setEffectiveEndStateEndDate(Date effectiveEndStateEndDate) {
		this.effectiveEndStateEndDate = effectiveEndStateEndDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short[] getWorkflowIds() {
		return this.workflowIds;
	}


	public void setWorkflowIds(Short[] workflowIds) {
		this.workflowIds = workflowIds;
	}
}
