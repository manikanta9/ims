package com.clifton.workflow.history;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.workflow.WorkflowEffectiveDateAware;
import com.clifton.workflow.definition.WorkflowState;

import java.util.Date;


/**
 * The <code>WorkflowHistory</code> class defines
 * historical state changes for an entity as well as a
 * system generated readable description
 *
 * @author manderson
 */
public class WorkflowHistory extends BaseEntity<Integer> {

	private WorkflowState startWorkflowState;
	private WorkflowState endWorkflowState;

	private SystemTable table;
	@SoftLinkField(tableBeanPropertyName = "table")
	private Long fkFieldId;

	private Date endStateStartDate;
	private Date endStateEndDate;

	/**
	 * Used by {@link WorkflowEffectiveDateAware} beans
	 * and allow specifying effective start/end dates.
	 */
	private Date effectiveEndStateStartDate;
	private Date effectiveEndStateEndDate;

	private String description;

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////


	public String getTimeInEndStateFormatted() {
		if (getEffectiveEndStateStartDate() == null) {
			return null;
		}
		// If still in this state, then give time in state up to now
		Date endDate = ObjectUtils.coalesce(getEffectiveEndStateEndDate(), getEndStateEndDate());
		if (endDate == null) {
			endDate = new Date();
		}

		return DateUtils.getTimeDifferenceShort(endDate, getEffectiveEndStateStartDate());
	}


	public boolean isEffectiveStartUsed() {
		// Do not include time to note if effective start is actually used, not actually overridden we just don't include the time piece
		return DateUtils.compare(getEffectiveEndStateStartDate(), getEndStateStartDate(), false) != 0;
	}


	public boolean isEffectiveEndUsed() {
		// Do not include time to note if effective end is actually used, not actually overridden we just don't include the time piece

		return ((getEffectiveEndStateEndDate() != null) && DateUtils.compare(getEffectiveEndStateEndDate(), getEndStateEndDate(), false) != 0);
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getEffectiveEndStateStartDate(), getEffectiveEndStateEndDate(), true);
	}

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////


	public WorkflowState getStartWorkflowState() {
		return this.startWorkflowState;
	}


	public void setStartWorkflowState(WorkflowState startWorkflowState) {
		this.startWorkflowState = startWorkflowState;
	}


	public WorkflowState getEndWorkflowState() {
		return this.endWorkflowState;
	}


	public void setEndWorkflowState(WorkflowState endWorkflowState) {
		this.endWorkflowState = endWorkflowState;
	}


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Date getEndStateEndDate() {
		return this.endStateEndDate;
	}


	public void setEndStateEndDate(Date endStateEndDate) {
		this.endStateEndDate = endStateEndDate;
	}


	public Date getEndStateStartDate() {
		return this.endStateStartDate;
	}


	public void setEndStateStartDate(Date endStateStartDate) {
		this.endStateStartDate = endStateStartDate;
	}


	public Date getEffectiveEndStateStartDate() {
		return this.effectiveEndStateStartDate;
	}


	public void setEffectiveEndStateStartDate(Date effectiveEndStateStartDate) {
		this.effectiveEndStateStartDate = effectiveEndStateStartDate;
	}


	public Date getEffectiveEndStateEndDate() {
		return this.effectiveEndStateEndDate;
	}


	public void setEffectiveEndStateEndDate(Date effectiveEndStateEndDate) {
		this.effectiveEndStateEndDate = effectiveEndStateEndDate;
	}
}
