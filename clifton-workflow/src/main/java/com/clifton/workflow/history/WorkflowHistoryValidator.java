package com.clifton.workflow.history;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;


/**
 * The <code>WorkflowHistoryValidator</code> ...
 * <p>
 * On Inserts and updates, validates effective dates are set, if not, sets them to current start/end dates
 * If they are set, validates that the effective end does not precede the effective start
 *
 * @author Mary Anderson
 */
@Component
public class WorkflowHistoryValidator extends SelfRegisteringDaoValidator<WorkflowHistory> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(WorkflowHistory bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		setEffectiveValuesWhereNull(bean);
		validateEndDateNotBeforeStartDate(bean);
	}


	private void setEffectiveValuesWhereNull(WorkflowHistory bean) {
		if (bean.getEffectiveEndStateEndDate() == null && bean.getEndStateEndDate() != null) {
			bean.setEffectiveEndStateEndDate(bean.getEndStateEndDate());
		}
		if (bean.getEffectiveEndStateStartDate() == null && bean.getEndStateStartDate() != null) {
			bean.setEffectiveEndStateStartDate(bean.getEndStateStartDate());
		}
	}


	private void validateEndDateNotBeforeStartDate(WorkflowHistory bean) {
		if (bean.getEffectiveEndStateStartDate() != null && bean.getEffectiveEndStateEndDate() != null) {
			/*
			 * Use four-millisecond threshold to satisfy SQL Server datetime millisecond rounding:
			 * https://docs.microsoft.com/en-us/sql/t-sql/functions/date-and-time-data-types-and-functions-transact-sql?view=sql-server-2017
			 */
			int endStartDateCompare = DateUtils.compareWithThreshold(bean.getEffectiveEndStateEndDate(), bean.getEffectiveEndStateStartDate(), ChronoUnit.MILLIS, 4);
			if (endStartDateCompare < 0) {
				// End date is before start date; throw exception
				throw new ValidationException(String.format("Effective Start Date [%s] cannot be before Effective End Date [%s] for Workflow State [%s]. If the Workflow State is being back dated, previous Workflow State(s) may need to be back dated first.", DateUtils.fromDate(bean.getEffectiveEndStateStartDate(), DateUtils.DATE_FORMAT_FULL_PRECISE), DateUtils.fromDate(bean.getEffectiveEndStateEndDate(), DateUtils.DATE_FORMAT_FULL_PRECISE), bean.getEndWorkflowState().getName()));
			}
			else if (endStartDateCompare == 0 && DateUtils.compare(bean.getEffectiveEndStateEndDate(), bean.getEffectiveEndStateStartDate(), true) < 0) {
				// Dates are equal within threshold, but end date is earlier than start date; move end date back to match start date exactly
				syncStartDateAndEndDate(bean);
			}
		}
	}


	private void syncStartDateAndEndDate(WorkflowHistory bean) {
		if (DateUtils.compare(bean.getEndStateEndDate(), bean.getEffectiveEndStateEndDate(), true) == 0) {
			bean.setEndStateEndDate(bean.getEffectiveEndStateStartDate());
		}
		bean.setEffectiveEndStateEndDate(bean.getEffectiveEndStateStartDate());
	}
}
