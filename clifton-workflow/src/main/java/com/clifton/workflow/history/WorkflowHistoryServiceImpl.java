package com.clifton.workflow.history;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.status.StatusWithCounts;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.WorkflowEffectiveDateAware;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.search.WorkflowSearchForm;
import com.clifton.workflow.jobs.WorkflowHistoryEndDateCommand;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>WorkflowHistoryServiceImpl</code> class provides default implementation of {@linkplain WorkflowHistoryService}.
 *
 * @author manderson
 */
@Service
public class WorkflowHistoryServiceImpl<T extends WorkflowAware> implements WorkflowHistoryService {

	private AdvancedUpdatableDAO<WorkflowHistory, Criteria> workflowHistoryDAO;

	private DaoLocator daoLocator;

	private SystemSchemaService systemSchemaService;

	private RunnerHandler runnerHandler;

	private WorkflowDefinitionService workflowDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////        Workflow History Business Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowHistory getWorkflowHistory(int id) {
		return getWorkflowHistoryDAO().findByPrimaryKey(id);
	}


	@Override
	public WorkflowHistory getWorkflowHistoryLast(WorkflowAware dtoInstance) {
		WorkflowHistorySearchForm searchForm = getWorkflowHistorySearchForm(dtoInstance, false);
		searchForm.setLimit(1);
		List<WorkflowHistory> list = getWorkflowHistoryList(searchForm);
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}


	@Override
	public List<WorkflowHistory> getWorkflowHistoryList(WorkflowHistorySearchForm searchForm) {
		// If Table ID is not Set - Try to Get it From Table Name Filter
		if (searchForm.getTableId() == null && !searchForm.containsSearchRestriction("tableId")) {
			String tableName = searchForm.getTableName();
			if (StringUtils.isEmpty(tableName) && searchForm.containsSearchRestriction("tableName")) {
				tableName = (String) searchForm.getSearchRestriction("tableName").getValue();
			}
			if (!StringUtils.isEmpty(tableName)) {
				SystemTable table = getSystemSchemaService().getSystemTableByName(searchForm.getTableName());
				searchForm.setTableId(table.getId());
			}
		}

		// Remove Table Name Restriction
		searchForm.setTableName(null);
		if (searchForm.containsSearchRestriction("tableName")) {
			searchForm.removeSearchRestriction("tableName");
		}

		// Default Sorting if none defined
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("effectiveEndStateStartDate:DESC#endStateStartDate:DESC");
		}
		// Get the List
		List<WorkflowHistory> result = getWorkflowHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		// If searching for a specific entity and more than one result in the list and the workflow on the object may not save end dates on history - attempt to populate them before returning
		if (searchForm.getEntityId() != null && CollectionUtils.getSize(result) > 1 && !result.get(0).getEndWorkflowState().getWorkflow().isEndDateSavedOnHistory()) {
			//populate End dates to display on UI
			populateWorkflowHistoryEndDatesForList(result);
		}
		return result;
	}


	@Override
	public List<WorkflowHistory> getWorkflowHistoryListByWorkflowAwareEntity(WorkflowAware dtoInstance, boolean ascending) {
		WorkflowHistorySearchForm searchForm = getWorkflowHistorySearchForm(dtoInstance, ascending);
		return getWorkflowHistoryList(searchForm);
	}


	private WorkflowHistorySearchForm getWorkflowHistorySearchForm(WorkflowAware dtoInstance, boolean ascending) {
		WorkflowHistorySearchForm searchForm = new WorkflowHistorySearchForm();
		searchForm.setEntityId(BeanUtils.getIdentityAsLong(dtoInstance));
		searchForm.setTableName(getSystemTableNameForEntity(dtoInstance));
		searchForm.setOrderBy(ascending ? "effectiveEndStateStartDate:ASC#endStateStartDate:ASC#id:ASC" : "effectiveEndStateStartDate:DESC#endStateStartDate:DESC#id:DESC");
		return searchForm;
	}


	/**
	 * @see WorkflowHistoryValidator
	 */
	@Override
	public void saveWorkflowHistoryList(List<WorkflowHistory> beanList) {
		getWorkflowHistoryDAO().saveList(beanList);
	}


	@SuppressWarnings("unchecked")
	private String getSystemTableNameForEntity(WorkflowAware dtoInstance) {
		return getDaoLocator().locate((Class<T>) dtoInstance.getClass()).getConfiguration().getTableName();
	}


	/**
	 * @see WorkflowHistoryValidator
	 */
	@Override
	public void deleteWorkflowHistoryByWorkflowAwareEntity(WorkflowAware dtoInstance) {
		getWorkflowHistoryDAO().deleteList(getWorkflowHistoryListByWorkflowAwareEntity(dtoInstance, true));
	}


	/**
	 * @see WorkflowHistoryValidator
	 */
	@Override
	@Transactional
	public void saveWorkflowHistoryEffectiveStartDate(WorkflowHistory bean) {
		int id = bean.getId();
		Date effectiveStartDate = bean.getEffectiveEndStateStartDate();
		String description = bean.getDescription();

		// Need to update current history, and previous one (end date)
		WorkflowHistory history = getWorkflowHistory(id);
		WorkflowHistory previous = null;

		WorkflowHistorySearchForm searchForm = new WorkflowHistorySearchForm();
		searchForm.setEntityId(history.getFkFieldId());
		searchForm.setTableName(history.getTable().getName());
		// Need to sort by both, because effective dates are date only and can be the same for multiple history records
		searchForm.setOrderBy("effectiveEndStateStartDate:DESC#endStateStartDate:DESC");
		List<WorkflowHistory> list = getWorkflowHistoryList(searchForm);

		// If changing for the current state - just need to update the effective date on the bean itself (will handle history updates)
		// Only need to directly find/update historical records if not in the current state.
		WorkflowHistory currentHistory = CollectionUtils.getFirstElement(list);
		if (history.equals(currentHistory)) {
			UpdatableDAO<WorkflowEffectiveDateAware> dao = (UpdatableDAO<WorkflowEffectiveDateAware>) getDaoLocator().<WorkflowEffectiveDateAware>locate(history.getTable().getName());
			WorkflowEffectiveDateAware dtoBean = dao.findByPrimaryKey(history.getFkFieldId());
			dtoBean.setWorkflowStateEffectiveStartDate(effectiveStartDate);
			dao.save(dtoBean);
		}
		else {
			// Ordered descending - once find this history record, the next one in the list is the previous one
			boolean found = false;
			for (WorkflowHistory his : CollectionUtils.getIterable(list)) {
				if (found && his.getEndWorkflowState().equals(history.getStartWorkflowState())) {
					previous = his;
					break;
				}
				if (his.equals(history)) {
					found = true;
				}
			}

			if (effectiveStartDate == null) {
				effectiveStartDate = history.getEndStateStartDate();
			}

			List<WorkflowHistory> saveList = new ArrayList<>();
			history.setEffectiveEndStateStartDate(effectiveStartDate);
			history.setDescription(description);
			saveList.add(history);

			if (previous != null) {
				previous.setEffectiveEndStateEndDate(effectiveStartDate);
				saveList.add(previous);
			}
			saveWorkflowHistoryList(saveList);
		}
	}


	@Override
	public void copyWorkflowHistoryList(WorkflowAware source, WorkflowAware destination) {
		ValidationUtils.assertFalse(source.isNewBean() || destination.isNewBean(), "Both Workflow Aware entities must be saved.");

		List<WorkflowHistory> originalWorkflowHistoryList = getWorkflowHistoryListByWorkflowAwareEntity(source, true);
		if (!CollectionUtils.isEmpty(originalWorkflowHistoryList)) {
			List<WorkflowHistory> newTradeWorkflowHistoryList = new ArrayList<>(originalWorkflowHistoryList.size());
			for (WorkflowHistory workflowHistory : CollectionUtils.getIterable(originalWorkflowHistoryList)) {
				// The clone will not have ID set, so all we need to do is update the FkFieldId for the new record.
				WorkflowHistory newTradeHistoryRecord = BeanUtils.cloneBean(workflowHistory, false, false);
				newTradeHistoryRecord.setFkFieldId(BeanUtils.getIdentityAsLong(destination));
				newTradeHistoryRecord.setDescription("Copied from WorkflowHistory: " + workflowHistory);
				newTradeWorkflowHistoryList.add(newTradeHistoryRecord);
			}
			// Make sure history is flushed in case the Trade is saved again within Transaction
			saveWorkflowHistoryList(newTradeWorkflowHistoryList);
		}
	}


	@Override
	public Status populateWorkflowHistoryEndDateListForCommand(WorkflowHistoryEndDateCommand command) {
		ValidationUtils.assertNotNull(command.getWeekdaysBack(), "Weekdays Back is required");
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : StatusWithCounts.ofMessage("Synchronously running for " + command);
			doPopulateWorkflowHistoryEndDateListForCommand(command, status);
			return status;
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + DateUtils.fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("WORKFLOW-HISTORY-END-DATE-POPULATE", runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					doPopulateWorkflowHistoryEndDateListForCommand(command, statusHolder.getStatus());
				}
				catch (Throwable e) {
					getStatus().setMessage("Error populating workflow history end dates for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error populating workflow history end dates for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);

		return Status.ofMessage("Started workflow history end date population: " + runId + ". Processing will be completed shortly.");
	}


	private void doPopulateWorkflowHistoryEndDateListForCommand(WorkflowHistoryEndDateCommand command, Status status) {
		Date now = DateUtils.clearTime(new Date());
		Date fromDate = DateUtils.addWeekDays(now, -command.getWeekdaysBack());

		WorkflowHistorySearchForm searchForm = new WorkflowHistorySearchForm();
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, fromDate));
		searchForm.addSearchRestriction(new SearchRestriction("endStateEndDate", ComparisonConditions.IS_NULL, null));
		if (command.getWorkflowId() != null) {
			searchForm.setWorkflowId(command.getWorkflowId());
		}
		else {
			WorkflowSearchForm workflowSearchForm = new WorkflowSearchForm();
			workflowSearchForm.setEndDateSavedOnHistory(false);
			List<Workflow> workflowList = getWorkflowDefinitionService().getWorkflowList(workflowSearchForm);
			if (CollectionUtils.isEmpty(workflowList)) {
				status.setMessage("Nothing was updated as there are no workflows to run against.");
				status.setActionPerformed(false);
				return;
			}
			if (workflowList.size() <= 1) {
				searchForm.setWorkflowId(workflowList.get(0).getId());
			}
			else {
				searchForm.setWorkflowIds(BeanUtils.getBeanIdentityArray(workflowList, Short.class));
			}
		}
		List<WorkflowHistory> historyList = getWorkflowHistoryList(searchForm);

		int entitySkipCount = 0;
		int entityProcessCount = 0;
		int entityFailCount = 0;
		int historyUpdatedCount = 0;
		Map<String, List<WorkflowHistory>> workflowHistoryMap = BeanUtils.getBeansMap(historyList, history ->
				BeanUtils.createKeyFromBeans(history.getTable(), history.getFkFieldId()));

		for (Map.Entry<String, List<WorkflowHistory>> workflowHistoryEntry : workflowHistoryMap.entrySet()) {
			entityProcessCount++;
			int size = CollectionUtils.getSize(workflowHistoryEntry.getValue());
			if (size > 1) {
				try {
					List<WorkflowHistory> workflowHistoryList = workflowHistoryEntry.getValue();
					List<WorkflowHistory> updatedList = populateWorkflowHistoryEndDatesForList(workflowHistoryList);
					saveWorkflowHistoryList(updatedList);
					historyUpdatedCount += CollectionUtils.getSize(updatedList);
				}
				catch (Exception e) {
					entityFailCount++;
					status.addError("Error populating workflow history end dates for " + workflowHistoryEntry.getKey() + ": " + ExceptionUtils.getOriginalMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error populating workflow history end dates for " + workflowHistoryEntry.getKey(), e);
				}
			}
			else {
				// skip histories with only one record without End Date: the very last record doesn't have the End Date
				entitySkipCount++;
			}
		}

		status.setMessageWithErrors("Entities Processed: " + entityProcessCount + "; Successful: " + (entityProcessCount - status.getErrorCount() - entitySkipCount) + "; Skipped " + entitySkipCount + "; Failed: " + entityFailCount + "; Updated " + historyUpdatedCount + " Histories.", 10);
	}


	/**
	 * This method populates missing workflow history end dates based on the given list of history.
	 * It returns only the history records that were updated (skips the last record.
	 */
	private List<WorkflowHistory> populateWorkflowHistoryEndDatesForList(List<WorkflowHistory> historyList) {
		List<WorkflowHistory> updatedList = new ArrayList<>();
		List<WorkflowHistory> sortedList = historyList.stream().sorted(Comparator.comparingInt(WorkflowHistory::getId)).collect(Collectors.toList());
		for (int i = 0; i < sortedList.size() - 1; i++) {
			WorkflowHistory history = sortedList.get(i);
			if (history.getEndStateEndDate() == null) {
				history.setEndStateEndDate(sortedList.get(i + 1).getEndStateStartDate());
				updatedList.add(history);
			}
		}
		return updatedList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<WorkflowHistory, Criteria> getWorkflowHistoryDAO() {
		return this.workflowHistoryDAO;
	}


	public void setWorkflowHistoryDAO(AdvancedUpdatableDAO<WorkflowHistory, Criteria> workflowHistoryDAO) {
		this.workflowHistoryDAO = workflowHistoryDAO;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}
}



