package com.clifton.workflow.history;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.jobs.WorkflowHistoryEndDateCommand;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>WorkflowHistoryService</code> defines the methods
 * for reading from and writing to the WorkflowHistory table.
 *
 * @author manderson
 */
public interface WorkflowHistoryService {

	////////////////////////////////////////////////////////////////////////////
	////////        Workflow History Business Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowHistory getWorkflowHistory(int id);


	/**
	 * Returns the last history record saved for an entity
	 */
	public WorkflowHistory getWorkflowHistoryLast(WorkflowAware dtoInstance);


	public List<WorkflowHistory> getWorkflowHistoryList(WorkflowHistorySearchForm searchForm);


	/**
	 * Returns all {@link WorkflowHistory} records for the given entity
	 *
	 * @param ascending - If true returns oldest to newest records
	 */
	public List<WorkflowHistory> getWorkflowHistoryListByWorkflowAwareEntity(WorkflowAware dtoInstance, boolean ascending);


	public void saveWorkflowHistoryList(List<WorkflowHistory> beanList);


	/**
	 * Deletes all {@link WorkflowHistory} records tied to a given
	 * {@link WorkflowAware}
	 */
	public void deleteWorkflowHistoryByWorkflowAwareEntity(WorkflowAware dtoInstance);


	/**
	 * Updates the effective start date for given history record
	 * If effectiveStartDate parameter is null, will copy the actual start from the history record as
	 * the effective start.
	 * <p>
	 * Also allows updating the history description so change made can be noted as to why
	 * <p>
	 * NOTE: Although it takes the full bean as a parameter, because of special logic - the only fields actually updated are the effective start & description
	 */
	@SecureMethod(dynamicTableNameBeanPath = "table.name", permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void saveWorkflowHistoryEffectiveStartDate(WorkflowHistory bean);


	/**
	 * Copies the WorkflowHistory from the source WorkflowAware entity to the destination WorkflowAware entity.
	 * The WorkflowHistory for the destination will be identical to the source's with updated FK Identity.
	 * <p>
	 * This should be used with caution. It is intended for copying/splitting WorkflowAware entities.
	 */
	@DoNotAddRequestMapping
	public void copyWorkflowHistoryList(WorkflowAware source, WorkflowAware destination);


	@RequestMapping("workflowHistoryEndDateListForCommandPopulate.json")
	@SecureMethod(dtoClass = WorkflowHistory.class, permissions = SecurityPermission.PERMISSION_READ)
	public Status populateWorkflowHistoryEndDateListForCommand(WorkflowHistoryEndDateCommand command);
}
