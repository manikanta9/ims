package com.clifton.workflow;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.context.Context;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventContext;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.lock.SystemLockAware;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.workflow.assignment.WorkflowAssignment;
import com.clifton.workflow.assignment.WorkflowAssignmentService;
import com.clifton.workflow.assignment.WorkflowAssignmentUtils;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionAction;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.WorkflowTransitionUtils;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>WorkflowManagerObserver</code> handles all DAO Validation and Workflow related events
 * attached to any {@link WorkflowAware} bean.
 * <p>
 * This includes:
 * Assigning WorkflowState/Status on bean INSERT
 * Validating Workflow Transition changes on bean UPDATE
 * Validating EntityModifyConditions on bean UPDATE
 * Processing any Automatic Transitions on bean INSERT/UPDATE
 * Inserting Workflow History records on bean INSERT/UPDATE
 * Deleting Workflow History on bean DELETE
 *
 * @author manderson
 */
@Component
public class WorkflowManagerObserver<T extends WorkflowAware> extends BaseDaoEventObserver<T> {

	private static final String DAO_EVENT_CONTEXT_WORKFLOW_HISTORY_LIST_KEY = "DAO_EVENT_CONTEXT_WORKFLOW_HISTORY_LIST_KEY";
	private static final String DAO_EVENT_CONTEXT_TRANSITION_ACTION_LIST_KEY = "DAO_EVENT_CONTEXT_WORKFLOW_TRANSITION_ACTION_LIST_KEY";
	private static final String DAO_EVENT_CONTEXT_LAST_WORKFLOW_HISTORY_KEY = "DAO_EVENT_CONTEXT_LAST_WORKFLOW_HISTORY_KEY";
	private static final String DAO_EVENT_CONTEXT_DEPTH_KEY = "DAO_EVENT_CONTEXT_DEPTH_KEY";
	private static final String DAO_EVENT_CONTEXT_INSIDE_OF_SAME_TRANSITION_KEY = "DAO_EVENT_CONTEXT_INSIDE_OF_SAME_TRANSITION_KEY";

	private SystemBeanService systemBeanService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemSchemaService systemSchemaService;
	private WorkflowAssignmentService workflowAssignmentService;
	private WorkflowHistoryService workflowHistoryService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowManagerObserver() {
		super();
		// The default order is 0. Make sure this runs after any other configured observer for an entity because it is capable of triggering nested transitions/saves.
		setOrder(Integer.MAX_VALUE);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isUniqueOrderingRequired() {
		// In conjunction with the value of order, ensure this observer is the last ordered observer
		return true;
	}


	@Override
	protected void beforeTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		int depth = changeWorkflowObserverDepth(bean, true);
		if (!isInsideOfSameTransition(bean)) {
			T originalBean = getOriginalBean(dao, bean);
			assignWorkflow(event, bean, originalBean);
			// If after assigning, the workflow isn't set then it wasn't required - don't do the rest of this...
			if (bean.getWorkflowState() == null) {
				return;
			}
			getDaoEventContext().setBeanAttribute(bean, DAO_EVENT_CONTEXT_INSIDE_OF_SAME_TRANSITION_KEY + depth, bean.getWorkflowState());
			validateEntityModifyCondition(dao, event, bean, originalBean);

			// If deleting, no transition necessary
			if (DaoEventTypes.DELETE != event) {
				// create and store lists of items before transaction starts to avoid/minimize deadlocks
				List<WorkflowHistory> historyList = new ArrayList<>();
				List<WorkflowTransitionAction> actionList = new ArrayList<>();

				// Put all actions into the context to be executed later
				getDaoEventContext().setBeanAttribute(bean, DAO_EVENT_CONTEXT_WORKFLOW_HISTORY_LIST_KEY, historyList);
				getDaoEventContext().setBeanAttribute(bean, DAO_EVENT_CONTEXT_TRANSITION_ACTION_LIST_KEY, actionList);

				// process this transition
				processWorkflowTransition(dao, event, bean, originalBean, historyList, actionList);

				// process pre-transition, pre-transaction actions
				processTransitionActions(bean, event, true, true);
			}
		}
	}


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// No Workflow State set means entity doesn't have a workflow (now optional)
		if (bean.getWorkflowState() == null) {
			return;
		}

		if (!isInsideOfSameTransition(bean)) {
			// process pre-transition, same-transaction actions
			processTransitionActions(bean, event, true, false);
		}
	}


	@Override
	protected void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// No Workflow State set means entity doesn't have a workflow (now optional)
		if (bean.getWorkflowState() == null) {
			return;
		}

		// Only process Workflow history if record was actually created.
		if (e == null && !isInsideOfSameTransition(bean)) {
			if (event.isInsert()) {
				// need to update DaoEventContext keys because the bean now has id
				getDaoEventContext().updateFromNewTo(bean);
			}

			// Propagated newly-saved bean as original bean value for child DAO calls
			executeWithPropagatedOriginalBean(bean, () -> {
				processWorkflowHistory(event, bean);
				// process post-transition, same-transaction actions
				processTransitionActions(bean, event, false, false);
			});
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// No Workflow State set means entity doesn't have a workflow (now optional)
		if (bean.getWorkflowState() == null) {
			return;
		}

		if (e == null && !isInsideOfSameTransition(bean)) {
			// Update the original bean snapshot; adds it for new bean and updates it with current state for updates
			// The snapshot is created to ensure the object is detached--not the same one used by Hibernate
			// The snapshot must be updated prior to processing after transition actions occurring in the same thread because actions can update the bean
			getDaoEventContext().setOriginalBean(createBeanSnapshot(bean));

			processTransitionActions(bean, event, false, true);

			// Process any next automatic transition, if present and allowed
			bean = processAutomaticWorkflowTransitions((UpdatableDAO<T>) dao, bean);
		}
		changeWorkflowObserverDepth(bean, false);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns current depth of workflow observer processing. This is used for circular workflow transitions.
	 */
	private int getWorkflowObserverDepth(T bean) {
		Integer result = (Integer) getDaoEventContext().getBeanAttribute(bean, DAO_EVENT_CONTEXT_DEPTH_KEY);
		return (result == null) ? 0 : result;
	}


	/**
	 * Increments or decrements the depth by 1 based on "add" parameter.
	 */
	private int changeWorkflowObserverDepth(T bean, boolean add) {
		int depth = getWorkflowObserverDepth(bean);
		depth = add ? (depth + 1) : depth - 1;
		getDaoEventContext().setBeanAttribute(bean, DAO_EVENT_CONTEXT_DEPTH_KEY, depth);
		return depth;
	}


	/**
	 * Returns true if the observer is invoked inside of the same transition (circular call when a transition action saves the bean).
	 * Make sure not to transition again.
	 */
	private boolean isInsideOfSameTransition(T bean) {
		int depth = getWorkflowObserverDepth(bean);
		if (depth > 1) {
			Object previousState = getDaoEventContext().getBeanAttribute(bean, DAO_EVENT_CONTEXT_INSIDE_OF_SAME_TRANSITION_KEY + (depth - 1));
			if (previousState != null && previousState.equals(bean.getWorkflowState())) {
				return true;
			}
		}
		return false;
	}


	/**
	 * For inserts sets the initial {@link WorkflowState} and {@link WorkflowStatus} for the given
	 * bean prior to the bean being saved in the database based upon the valid {@link WorkflowAssignment}s.
	 * <p>
	 * For existing beans, ensures the {@link WorkflowState} and {@link WorkflowStatus} are set on the bean.
	 * If not, will set it to the original bean value.  If not, will assign a value based on valid {@link WorkflowAssignment}s.
	 * <p>
	 * Also makes sure the correct {@link WorkflowStatus} is set on the bean
	 */
	private void assignWorkflow(DaoEventTypes event, T bean, T originalBean) {
		// Only assign if not DELETE AND bean is not null
		if (DaoEventTypes.DELETE != event && bean != null) {
			if (bean.getWorkflowState() == null) {
				if (originalBean != null && originalBean.getWorkflowState() != null) {
					// If Workflow was previously selected, but not on the bean
					// set it to the original state/status
					bean.setWorkflowState(originalBean.getWorkflowState());
					bean.setWorkflowStatus(bean.getWorkflowState().getStatus());
				}
				else {
					// Else set the state/status based upon assignments
					getWorkflowAssignmentService().assignWorkflow(bean);
				}
			}
			else {
				// Ensure the correct Status is selected on the bean.
				// UI only changes State, so this needs to be set
				bean.setWorkflowStatus(bean.getWorkflowState().getStatus());
			}
		}
	}


	/**
	 * Validates if a {@link WorkflowAware} can be modified while in a given state based upon the state's entity modify {@link SystemCondition} that is set.
	 * If no condition is set, the entity can be modified.  Otherwise it is evaluated against the original bean.
	 * <p>
	 * Entity Modify Conditions are evaluated only if there is NO transition.  If there is a transition, any conditions related to that condition
	 * will be used to validate the bean.
	 * <p>
	 * State changes are still allowed and determined if ok by the validateTransition method
	 */
	private void validateEntityModifyCondition(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, T originalBean) {
		// Modify Conditions are validated against the original bean's state.
		// Only validate modify conditions if NOT Insert && there is an originalBean and original workflow state
		if (originalBean != null && originalBean.getWorkflowState() != null) {
			if (DaoEventTypes.UPDATE == event) {
				// AND if there is NO transition and the originalBean state has an entity modify condition populated
				if (originalBean.getWorkflowState().equals(bean.getWorkflowState()) && originalBean.getWorkflowState().getEntityUpdateCondition() != null) {
					// Check for same state transition
					WorkflowTransition sameStateTransition = WorkflowTransitionUtils.getSameStateTransition() != null ? getWorkflowTransitionService().getWorkflowTransition(WorkflowTransitionUtils.getSameStateTransition()) : null;
					if (sameStateTransition == null || !sameStateTransition.getEndWorkflowState().equals(bean.getWorkflowState())) {
						String errorMessage = getSystemConditionEvaluationHandler().getConditionFalseMessage(originalBean.getWorkflowState().getEntityUpdateCondition(), bean);
						if (!StringUtils.isEmpty(errorMessage)) {
							throw new ValidationException("Cannot edit " + dao.getConfiguration().getTableName() + " in Workflow State [" + originalBean.getWorkflowState().getName() + "] because: " + errorMessage);
						}
					}
				}
			}
			// In some cases, a modify condition cannot be effectively evaluated for deletes. Thus, a delete condition can be used for deletes specifically.
			if (DaoEventTypes.DELETE == event) {
				if (originalBean.getWorkflowState().getEntityDeleteCondition() != null) {
					String errorMessage = getSystemConditionEvaluationHandler().getConditionFalseMessage(originalBean.getWorkflowState().getEntityDeleteCondition(), bean);
					if (!StringUtils.isEmpty(errorMessage)) {
						throw new ValidationException("Cannot delete " + dao.getConfiguration().getTableName() + " in Workflow State [" + originalBean.getWorkflowState().getName() + "] because: " + errorMessage);
					}
				}
			}
		}
	}


	/**
	 * Processes any automatic workflow transitions for the given entity from its current state.
	 *
	 * @see WorkflowTransitionUtils#isAutomaticWorkflowTransitionsDisabled()
	 */
	private T processAutomaticWorkflowTransitions(UpdatableDAO<T> dao, T bean) {
		// Guard-clause: Workflow transitions disabled
		if (WorkflowTransitionUtils.isAutomaticWorkflowTransitionsDisabled()) {
			return bean;
		}

		WorkflowTransition nextTransition = getWorkflowTransitionService().getWorkflowTransitionNext(bean, new StringBuilder());
		// Guard-clause: No automatic transitions exist from the current state
		if (nextTransition == null) {
			return bean;
		}

		if (nextTransition.isAutomaticDelayed() && RequestContextHolder.getRequestAttributes() != null) {
			// Delay automatic transitions until the end of the current request if a request is active
			WorkflowTransitionUtils.queueDelayedTransition(bean, nextTransition, dao);
			return bean;
		}
		else {
			// Immediately apply transitions
			bean.setWorkflowState(nextTransition.getEndWorkflowState());
			bean.setWorkflowStatus(nextTransition.getEndWorkflowState().getStatus());
			return dao.save(bean);
		}
	}


	/**
	 * Processes transition for the given entity.  If a transition exists, validates it first.
	 * <p>
	 * Records all {@link WorkflowHistory} history records in the context for save later in the afterMethodCall
	 * Records all {@link WorkflowTransitionAction} beans in the context for executing later in the beforeTransactionMethodCall/afterTransactionMethodCall the before/after transition actions
	 */
	private void processWorkflowTransition(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, T originalBean, List<WorkflowHistory> historyList, List<WorkflowTransitionAction> actionList) {

		WorkflowTransition sameStateTransition = WorkflowTransitionUtils.getSameStateTransition() != null ? getWorkflowTransitionService().getWorkflowTransition(WorkflowTransitionUtils.getSameStateTransition()) : null;
		boolean allowSameStateTransition = sameStateTransition != null && sameStateTransition.getEndWorkflowState().equals(bean.getWorkflowState()) && (originalBean != null && originalBean.getWorkflowState().equals(bean.getWorkflowState()));

		if (!allowSameStateTransition && originalBean != null && originalBean.getWorkflowState() != null && originalBean.getWorkflowState().equals(bean.getWorkflowState())) {
			if (!originalBean.getWorkflowStatus().equals(bean.getWorkflowStatus())) {
				// Should never get to this because we set it in assignWorkflow, but just in case...
				throw new IllegalStateException("Illegal State Occurred:  State didn't change for the bean, however the status did.");
			}
			// No transition - still need to check for any automatic transitions: after transaction
		}
		else {
			if (bean instanceof SystemLockAware) {
				if (((SystemLockAware) bean).getLockedByUser() != null) {
					throw new ValidationException("Cannot transition " + BeanUtils.getLabel(bean) + " to " + bean.getWorkflowState().getName() + " Workflow State because it is currently locked by [" + ((SystemLockAware) bean).getLockedByUser().getDisplayName() + "].");
				}
			}

			// Validate the existing transition
			StringBuilder message = new StringBuilder();

			WorkflowState start = (originalBean == null ? null : originalBean.getWorkflowState());
			// If Changing Workflow - Validate this is allowed
			if (start != null && !start.getWorkflow().equals(bean.getWorkflowState().getWorkflow())) {
				if (!WorkflowAssignmentUtils.isReassignmentAllowed()) {
					throw new ValidationException("Cannot perform Workflow reassignment from [" + start.getWorkflow().getName() + "] to [" + bean.getWorkflowState().getWorkflow().getName()
							+ "] because reassignment execution mode is not enabled.");
				}
				// If allowed - change start state to null so we get the initial start state transition
				start = null;
			}

			WorkflowTransition transition = getWorkflowTransitionService().getWorkflowTransitionForEntity(bean, start, bean.getWorkflowState(), message);

			if (transition == null) {
				throw new ValidationException("Cannot transition to Workflow State [" + bean.getWorkflowState().getName() + "] because " + message);
			}

			if (transition.isSystemDefined() && !WorkflowTransitionUtils.isSystemDefinedAllowed()) {
				throw new ValidationException("Cannot perform system defined Workflow Transition to Workflow State [" + bean.getWorkflowState().getName()
						+ "] because system defined execution mode is not enabled.");
			}

			// save transition actions for future execution
			if (transition.isBeforeActionsExist() || transition.isAfterActionsExist()) {
				List<WorkflowTransitionAction> actions = getWorkflowTransitionService().getWorkflowTransitionActionListForTransitionId(transition.getId());
				if (!CollectionUtils.isEmpty(actions)) {
					actionList.addAll(actions);
				}
			}

			// Create History Record
			WorkflowHistory lastHistory = null;
			if (transition.getEndWorkflowState() != null && transition.getEndWorkflowState().getWorkflow().isEndDateSavedOnHistory()) {
				lastHistory = (WorkflowHistory) getDaoEventContext().getBeanAttribute(bean, DAO_EVENT_CONTEXT_LAST_WORKFLOW_HISTORY_KEY);
				if (lastHistory != null) {
					if (!MathUtils.isEqual(lastHistory.getFkFieldId(), BeanUtils.getIdentityAsLong(bean))) {
						lastHistory = null;
					}
				}

				// Look it up if it's an update and the last history isn't in the context
				if (event.isUpdate() && lastHistory == null) {
					lastHistory = getWorkflowHistoryService().getWorkflowHistoryLast(bean);
				}
			}
			WorkflowHistory history = populateWorkflowHistory(dao, transition, message.toString());
			if (bean instanceof WorkflowEffectiveDateAware) {
				WorkflowEffectiveDateAware dateAwareBean = (WorkflowEffectiveDateAware) bean;
				if (dateAwareBean.getWorkflowStateEffectiveStartDate() == null) {
					dateAwareBean.setWorkflowStateEffectiveStartDate(DateUtils.clearTime(history.getEndStateStartDate()));
				}
				history.setEffectiveEndStateStartDate(dateAwareBean.getWorkflowStateEffectiveStartDate());
			}
			else if (history.getEffectiveEndStateStartDate() == null) {
				history.setEffectiveEndStateStartDate(history.getEndStateStartDate());
			}
			historyList.add(history);
			// Put this new history in the context as the last history record
			getDaoEventContext().setBeanAttribute(bean, DAO_EVENT_CONTEXT_LAST_WORKFLOW_HISTORY_KEY, history);

			// Add lastHistory to the list of histories to be inserted/updated
			if (lastHistory != null) {
				lastHistory.setEndStateEndDate(history.getEndStateStartDate());
				lastHistory.setEffectiveEndStateEndDate(history.getEffectiveEndStateStartDate());
				historyList.add(lastHistory);
			}
		}

		// If no transitions - check if effective start date was updated, and if so - need to update last two historical records
		if (originalBean != null && CollectionUtils.isEmpty(historyList) && bean instanceof WorkflowEffectiveDateAware) {
			WorkflowEffectiveDateAware originalDateAwareBean = (WorkflowEffectiveDateAware) originalBean;
			WorkflowEffectiveDateAware dateAwareBean = (WorkflowEffectiveDateAware) bean;
			if (DateUtils.compare(originalDateAwareBean.getWorkflowStateEffectiveStartDate(), dateAwareBean.getWorkflowStateEffectiveStartDate(), false) != 0) {
				// Get Workflow History in Descending Order
				List<WorkflowHistory> list = getWorkflowHistoryService().getWorkflowHistoryListByWorkflowAwareEntity(bean, false);
				if (!CollectionUtils.isEmpty(list)) {
					WorkflowHistory current = list.get(0);
					if (dateAwareBean.getWorkflowStateEffectiveStartDate() == null) {
						dateAwareBean.setWorkflowStateEffectiveStartDate(DateUtils.clearTime(current.getEndStateStartDate()));
					}
					current.setEffectiveEndStateStartDate(dateAwareBean.getWorkflowStateEffectiveStartDate());
					historyList.add(current);

					if (list.size() >= 2) {
						WorkflowHistory last = list.get(1);
						last.setEffectiveEndStateEndDate(dateAwareBean.getWorkflowStateEffectiveStartDate());
						historyList.add(last);
					}
				}
			}
		}
	}


	/**
	 * If any {@link WorkflowTransitionAction}(s) should be processed (saved in the context)
	 */
	@SuppressWarnings("unchecked")
	private T processTransitionActions(T bean, DaoEventTypes event, boolean beforeTransition, boolean separateTransaction) {
		List<WorkflowTransitionAction> actionList = (List<WorkflowTransitionAction>) getDaoEventContext().getBeanAttribute(bean, DAO_EVENT_CONTEXT_TRANSITION_ACTION_LIST_KEY);
		if ((DaoEventTypes.DELETE != event) && (actionList == null)) {
			throw new RuntimeException("[" + DAO_EVENT_CONTEXT_TRANSITION_ACTION_LIST_KEY + "] bean attribute is missing from the event context for [" + bean + "].");
		}
		int size = CollectionUtils.getSize(actionList);
		if (size > 0) {
			int i = 0;
			T beanSnapshot = createBeanSnapshot(bean);
			while (i < size) {
				WorkflowTransitionAction action = actionList.get(i);
				if (action.isBeforeTransition() == beforeTransition && action.isSeparateTransaction() == separateTransaction) {
					WorkflowTransitionActionHandler<T> handler = (WorkflowTransitionActionHandler<T>) getSystemBeanService().getBeanInstance(action.getActionSystemBean());
					T actionResult = handler.processAction(bean, action.getWorkflowTransition());
					bean = actionResult != null ? actionResult : bean;
					actionList.remove(i); // remove to avoid double processing
					size--;
					i--;
				}
				i++;
			}
			if (separateTransaction && !CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualProperties(bean, beanSnapshot, false))) {
				UpdatableDAO<T> dao = (UpdatableDAO<T>) getDaoLocator().locate((Class<T>) bean.getClass());
				bean = dao.save(bean);
			}
		}
		return bean;
	}


	/**
	 * For INSERT and UPDATE, saves (if any) {@link WorkflowHistory} records created during the before methods (that were saved in the context)
	 * <p>
	 * For DELETE, deletes all history records for the deleted bean
	 * <p>
	 * This MUST be done AFTER the initial save because inserts do not have an ID set for history records.
	 */
	@SuppressWarnings("unchecked")
	private void processWorkflowHistory(DaoEventTypes event, T bean) {
		// If deleting, only need to delete any WorkflowHistory records...
		if (DaoEventTypes.DELETE == event) {
			getWorkflowHistoryService().deleteWorkflowHistoryByWorkflowAwareEntity(bean);
		}
		else {
			// INSERTS & UPDATES - Insert the History records
			// Lookup History Records in the Context
			List<WorkflowHistory> historyList = (List<WorkflowHistory>) getDaoEventContext().getBeanAttribute(bean, DAO_EVENT_CONTEXT_WORKFLOW_HISTORY_LIST_KEY);
			if (!CollectionUtils.isEmpty(historyList)) {
				// Set all beans with the appropriate FKFieldID
				for (WorkflowHistory history : historyList) {
					history.setFkFieldId(BeanUtils.getIdentityAsLong(bean));
				}
				getWorkflowHistoryService().saveWorkflowHistoryList(historyList);
				historyList.clear();
			}
		}
	}


	private WorkflowHistory populateWorkflowHistory(ReadOnlyDAO<T> dao, WorkflowTransition transition, String message) {
		if (transition == null) {
			// No transition
			return null;
		}
		WorkflowHistory history = new WorkflowHistory();
		history.setEndStateStartDate(new Date());
		history.setStartWorkflowState(transition.getStartWorkflowState());
		history.setEndWorkflowState(transition.getEndWorkflowState());
		history.setTable(getSystemSchemaService().getSystemTableByName(dao.getConfiguration().getTableName()));
		history.setDescription(generateWorkflowHistoryDescription(transition, message));

		return history;
	}


	/**
	 * Generates a String representation of the {@link WorkflowHistory} entity.
	 * i.e. Who Made Change + What Change was made + When Change was made
	 * and sets it as its description value.
	 */
	private String generateWorkflowHistoryDescription(WorkflowTransition transition, String message) {
		LabeledObject user = (LabeledObject) getContextHandler().getBean(Context.USER_BEAN_NAME);
		AssertUtils.assertNotNull(user, "Current user is missing");
		StringBuilder sb = new StringBuilder(64);

		// Manual transition
		if (!transition.isAutomatic()) {
			sb.append("[");
			sb.append(StringUtils.coalesce(true, user.getLabel()));
			sb.append("]");
		}
		else {
			sb.append("[SYSTEM]");
		}
		sb.append(" did [");
		sb.append(transition.getName());
		sb.append("] on [");
		sb.append(DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT));
		sb.append("]");
		if (transition.isAutomatic() && transition.getCondition() != null) {
			sb.append(" because [");
			sb.append(message);
			sb.append("]");
		}
		return sb.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Execute the given operations with a propagated {@link DaoEventContext} "original bean" property.
	 * <p>
	 * DAO observers called from within this observer may require access to the new state of the bean. Since the old state of the bean is already stored in the {@link ThreadLocal}
	 * cache for the "original bean", observers will not naturally be able to obtain the new state of the bean through use of the {@link BaseDaoEventObserver#getOriginalBean(
	 *ReadOnlyDAO, IdentityObject)} method. In order to circumvent this, this method manually propagates the cached "original bean" for child operations. Once the child
	 * operations are complete, the former "original bean" is restored so that other observers at the current depth will continue to behave properly.
	 * <p>
	 * This is especially useful for child transitions. Transitions can occur recursively, meaning that any number of historic "original bean" entities may exist.
	 *
	 * @param bean the bean currently being modified
	 */
	private void executeWithPropagatedOriginalBean(T bean, Runnable runnable) {
		T previousOriginalBean = getDaoEventContext().getOriginalBean(bean);
		getDaoEventContext().setOriginalBean(createBeanSnapshot(bean));
		try {
			runnable.run();
		}
		finally {
			getDaoEventContext().setOriginalBean(previousOriginalBean);
		}
	}


	/**
	 * Creates a snapshot of the bean for handling multiple workflow transitions within the
	 * same transaction. The bean may be modified by actions, and we need to be sure the
	 * current state is not lost between saves and flushes to the DB.
	 */
	private T createBeanSnapshot(T bean) {
		// Cloning the bean with deep is costly when doing bulk operations such as trade uploads.
		// We can use the same property values because the new object reference is enough to have a detached instance.
		return BeanUtils.cloneBean(bean, false, true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public WorkflowAssignmentService getWorkflowAssignmentService() {
		return this.workflowAssignmentService;
	}


	public void setWorkflowAssignmentService(WorkflowAssignmentService workflowAssignmentService) {
		this.workflowAssignmentService = workflowAssignmentService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
