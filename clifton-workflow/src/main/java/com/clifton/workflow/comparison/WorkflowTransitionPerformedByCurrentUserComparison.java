package com.clifton.workflow.comparison;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.user.SecurityUserService;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>WorkflowTransitionPerformedByCurrentUserComparison</code> can be used to limit transition execution
 * to users that did or didn't perform specified workflow transition previously on the same bean.
 * <p>
 * Example: Performance Summaries.  We have some users that play dual roles as Analysts and PMs (Greg).  If, as an analyst
 * he approved the performance summary, then he cannot also approve the same summary as a PM.
 *
 * @author manderson
 */
public class WorkflowTransitionPerformedByCurrentUserComparison implements Comparison<WorkflowAware> {

	private SecurityUserService securityUserService;

	private WorkflowHistoryService workflowHistoryService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Transition on the bean the current user should/should not have performed
	 */
	private String transitionName;

	/**
	 * Option to check if the current user did or did not perform the transition
	 */
	private boolean didNotPerform;

	/**
	 * If transition hasn't happened at all,
	 * returns false if set, else returns true
	 */
	private boolean falseIfTransitionHasNotHappened;

	/**
	 * If the transition has happened multiple times (back and forth)
	 * this option can be used to check if the user did/didn't perform the transition ever
	 * or just the last time.
	 */
	private boolean checkLastTransitionOnly;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(WorkflowAware wBean, ComparisonContext context) {
		boolean result;
		String message;

		// Transitions are not unique by name and there could be multiple ways to get to the same place
		// i.e. Reject from different states.  Commonly we give similar transitions names that apply to them

		// Get Full Workflow History in descending order
		List<WorkflowHistory> historyList = getWorkflowHistoryService().getWorkflowHistoryListByWorkflowAwareEntity(wBean, false);

		// If No History...
		if (CollectionUtils.isEmpty(historyList)) {
			message = "Transition [" + getTransitionName() + "] has not occurred yet.";
			result = !isFalseIfTransitionHasNotHappened();
		}
		else {
			List<Short> userIdList = getUsersThatPerformedTransition(wBean, historyList);

			if (CollectionUtils.isEmpty(userIdList)) {
				message = "Transition [" + getTransitionName() + "] has not occurred yet.";
				result = !isFalseIfTransitionHasNotHappened();
			}
			else {
				Short currentUserId = getSecurityUserService().getSecurityUserCurrent().getId();
				if (userIdList.contains(currentUserId)) {
					message = "Current user performed " + (isCheckLastTransitionOnly() ? "most recent " : "") + "workflow transition [" + getTransitionName() + "]";
					result = !isDidNotPerform();
				}
				else {
					message = "Current user did not perform " + (isCheckLastTransitionOnly() ? "most recent " : "") + "workflow transition [" + getTransitionName() + "]";
					result = isDidNotPerform();
				}
			}
		}

		if (context != null) {
			if (result) {
				context.recordTrueMessage(message);
			}
			else {
				context.recordFalseMessage(message);
			}
		}

		return result;
	}


	private List<Short> getUsersThatPerformedTransition(WorkflowAware bean, List<WorkflowHistory> historyList) {
		Workflow workflow = bean.getWorkflowState().getWorkflow();
		List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionListByWorkflow(workflow.getId());
		transitionList = BeanUtils.filter(transitionList, WorkflowTransition::getName, getTransitionName());
		if (CollectionUtils.isEmpty(transitionList)) {
			throw new ValidationException("Unable to evaluate CurrentUserExecutedWorkflowTransitionComparison because there are no transitions with name [" + getTransitionName() + "] for workflow "
					+ workflow.getName());
		}

		// Get List of users ids that performed this/these transitions
		List<Short> userIdList = new ArrayList<>();
		for (WorkflowHistory history : historyList) {
			for (WorkflowTransition transition : transitionList) {
				if (transition.getStartWorkflowState() != null && transition.getStartWorkflowState().equals(history.getStartWorkflowState())) {
					if (transition.getEndWorkflowState().equals(history.getEndWorkflowState())) {
						userIdList.add(history.getCreateUserId());
						// If last transition only - return that one since the list is in descending order already
						if (isCheckLastTransitionOnly()) {
							return userIdList;
						}
					}
				}
			}
		}
		return userIdList;
	}


	///////////////////////////////////////////////////////////////////////////
	//////////              Getter and Setter Methods               ///////////
	///////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public String getTransitionName() {
		return this.transitionName;
	}


	public void setTransitionName(String transitionName) {
		this.transitionName = transitionName;
	}


	public boolean isDidNotPerform() {
		return this.didNotPerform;
	}


	public void setDidNotPerform(boolean didNotPerform) {
		this.didNotPerform = didNotPerform;
	}


	public boolean isCheckLastTransitionOnly() {
		return this.checkLastTransitionOnly;
	}


	public void setCheckLastTransitionOnly(boolean checkLastTransitionOnly) {
		this.checkLastTransitionOnly = checkLastTransitionOnly;
	}


	public boolean isFalseIfTransitionHasNotHappened() {
		return this.falseIfTransitionHasNotHappened;
	}


	public void setFalseIfTransitionHasNotHappened(boolean falseIfTransitionHasNotHappened) {
		this.falseIfTransitionHasNotHappened = falseIfTransitionHasNotHappened;
	}
}
