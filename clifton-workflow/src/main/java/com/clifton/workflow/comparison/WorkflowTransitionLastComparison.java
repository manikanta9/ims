package com.clifton.workflow.comparison;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;

import java.util.List;


/**
 * The <code>WorkflowTransitionLastComparison</code> can be used to determine what the last transition was and return true/false depending on that.
 *
 * <p>
 * Example: Order Allocation Workflow.  When in Pending state, if no other changes (not been rejected) it would auto approve.  However, Traders can reject and send back to PMs (Pending)
 * So, we don't want to auto approve if the last transition was Reject or Return to PM
 *
 * @author manderson
 */
public class WorkflowTransitionLastComparison implements Comparison<WorkflowAware> {

	private WorkflowHistoryService workflowHistoryService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Transition Name we are looking for
	 */
	private String transitionName;

	/**
	 * By Default, returns true if the last transition matches
	 * Can return true instead if it doesn't equal
	 * <p>
	 * i.e. Last Transition Was Not
	 */
	private boolean trueIfNotEquals;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(WorkflowAware wBean, ComparisonContext context) {
		boolean result;
		String message;

		// Get Full Workflow History in descending order and take the last one
		WorkflowHistory history = CollectionUtils.getFirstElement(getWorkflowHistoryService().getWorkflowHistoryListByWorkflowAwareEntity(wBean, false));

		if (history == null) {
			message = "No history available: Transition [" + getTransitionName() + "] has not occurred yet.";
			result = isTrueIfNotEquals();
		}
		else {
			result = isLastTransition(history);
			message = "Last Transition was " + (!result ? "not " : "") + "[" + getTransitionName() + "]";
			if (isTrueIfNotEquals()) {
				result = !result;
			}
		}

		if (context != null) {
			if (result) {
				context.recordTrueMessage(message);
			}
			else {
				context.recordFalseMessage(message);
			}
		}

		return result;
	}


	private boolean isLastTransition(WorkflowHistory history) {
		Workflow workflow = history.getEndWorkflowState().getWorkflow();
		// Get all Transitions from the Workflow
		List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionListByWorkflow(workflow.getId());
		// Then filter based on same start and end state
		transitionList = BeanUtils.filter(transitionList, workflowTransition -> {
			if (CompareUtils.isEqual(history.getStartWorkflowState(), workflowTransition.getStartWorkflowState())) {
				return CompareUtils.isEqual(history.getEndWorkflowState(), workflowTransition.getEndWorkflowState());
			}
			return false;
		});
		// NOTE: If more than one do we just check workflow history description rather than just a contains?

		// Then filter based on transition name
		transitionList = BeanUtils.filter(transitionList, WorkflowTransition::getName, getTransitionName());

		return (!CollectionUtils.isEmpty(transitionList));
	}

	///////////////////////////////////////////////////////////////////////////
	//////////              Getter and Setter Methods               ///////////
	///////////////////////////////////////////////////////////////////////////


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public String getTransitionName() {
		return this.transitionName;
	}


	public void setTransitionName(String transitionName) {
		this.transitionName = transitionName;
	}


	public boolean isTrueIfNotEquals() {
		return this.trueIfNotEquals;
	}


	public void setTrueIfNotEquals(boolean trueIfNotEquals) {
		this.trueIfNotEquals = trueIfNotEquals;
	}
}
