package com.clifton.workflow.comparison;


import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>WorkflowStatePreviouslyVisited</code> can be used to determine if a work flow state
 * has been previously visited.  We can define a min and max(optional) value to define the range the condition is
 * evaluated on.
 *
 * @author apopp
 */
public class WorkflowStatePreviouslyVisitedComparison implements Comparison<WorkflowAware>, ValidationAware {

	private WorkflowHistoryService workflowHistoryService;

	/**
	 * Name of state to check against
	 */
	private String stateName;

	/**
	 * Min amount of times this state needs to have been visited for true
	 */
	private Integer minVisitedCount; //required

	/**
	 * When not null, this max defines a range of truth for visited count based on min to max (inclusive).
	 */
	private Integer maxVisitedCount; //not required - nullable

	/**
	 * When checking for previously visited, find after the last time entity was in selected state
	 * i.e. Previously Approved, but not Rejected since then
	 */
	private String visitedSinceStateName;

	/**
	 * When checking for previously visited, find after the last time entity was in selected status
	 * i.e. Previously Approved, but not in Draft status since then
	 */
	private String visitedSinceStatusName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getStateName(), "Workflow State name is required.");
		ValidationUtils.assertNotNull(getMinVisitedCount(), "Min Visited Count is required.  Use 0 to indicate the state should NOT have been visited.");
		if (!StringUtils.isEmpty(getVisitedSinceStateName())) {
			ValidationUtils.assertEmpty(getVisitedSinceStatusName(), "Either Visited since state OR status is allowed, but not both.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(WorkflowAware wBean, ComparisonContext context) {
		// Note: Getting History is descending order is important for visited since logic
		List<WorkflowHistory> historyList = getWorkflowHistoryService().getWorkflowHistoryListByWorkflowAwareEntity(wBean, false);
		historyList = filterForVisitedSince(historyList);
		int visitedCnt = getVisitCount(historyList);

		String message;
		if (visitedCnt == 0) {
			message = "The state [" + getStateName() + "] has not been visited" + getVisitedSinceLabel();
		}
		else {
			message = "The state [" + getStateName() + "] has been visited " + visitedCnt + " times" + getVisitedSinceLabel();
		}

		if (getMinVisitedCount() > 1) {
			message += " and the min visit count is " + getMinVisitedCount();
		}
		if (getMaxVisitedCount() != null) {
			message += " and the max visit count is " + getMaxVisitedCount();
		}

		boolean result = calculateResult(visitedCnt);

		if (context != null) {
			if (result) {
				context.recordTrueMessage("(" + message + ")");
			}
			else {
				context.recordFalseMessage("(" + message + ")");
			}
		}
		return result;
	}


	/**
	 * This condition is based on the supplied parameters.
	 * <p>
	 * If there is no max supplied the result will be based on how the supplied min value
	 * compares to the historically calculated value.
	 * <p>
	 * If max is not null then the result will be based on how the historically calculated value
	 * compares to the range from min to max.
	 */
	private boolean calculateResult(int visitedCnt) {
		if (getMaxVisitedCount() == null) {
			return getMinVisitedCount() <= visitedCnt;
		}
		return visitedCnt >= getMinVisitedCount() && visitedCnt <= getMaxVisitedCount();
	}


	private int getVisitCount(List<WorkflowHistory> historyList) {
		int cnt = 0;

		for (WorkflowHistory history : CollectionUtils.getIterable(historyList)) {
			if (history.getEndWorkflowState().getName().equals(getStateName())) {
				cnt++;
			}
		}

		return cnt;
	}


	protected List<WorkflowHistory> filterForVisitedSince(List<WorkflowHistory> historyList) {
		// No history or visited since, then nothing to filter
		if (CollectionUtils.isEmpty(historyList) || (StringUtils.isEmpty(getVisitedSinceStateName()) && StringUtils.isEmpty(getVisitedSinceStatusName()))) {
			return historyList;
		}

		List<WorkflowHistory> visitedSinceList = new ArrayList<>();
		boolean found = false;
		for (WorkflowHistory history : CollectionUtils.getIterable(historyList)) {
			if (!StringUtils.isEmpty(getVisitedSinceStateName()) && StringUtils.isEqualIgnoreCase(getVisitedSinceStateName(), history.getEndWorkflowState().getName())) {
				found = true;
				break;
			}
			if (!StringUtils.isEmpty(getVisitedSinceStatusName()) && StringUtils.isEqualIgnoreCase(getVisitedSinceStatusName(), history.getEndWorkflowState().getStatus().getName())) {
				found = true;
				break;
			}
			visitedSinceList.add(history);
		}
		if (found) {
			return visitedSinceList;
		}
		return null;
	}


	protected String getVisitedSinceLabel() {
		String visitedSince = "";
		if (!StringUtils.isEmpty(getVisitedSinceStateName())) {
			visitedSince += "State [" + getVisitedSinceStateName() + "]";
		}
		else if (!StringUtils.isEmpty(getVisitedSinceStatusName())) {
			visitedSince += "Status [" + getVisitedSinceStatusName() + "]";
		}
		return (StringUtils.isEmpty(visitedSince)) ? "" : " since " + visitedSince;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////              Getter and Setter Methods               ///////////
	///////////////////////////////////////////////////////////////////////////


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}


	public String getStateName() {
		return this.stateName;
	}


	public void setStateName(String stateName) {
		this.stateName = stateName;
	}


	public Integer getMinVisitedCount() {
		return this.minVisitedCount;
	}


	public void setMinVisitedCount(Integer minVisitedCount) {
		this.minVisitedCount = minVisitedCount;
	}


	public Integer getMaxVisitedCount() {
		return this.maxVisitedCount;
	}


	public void setMaxVisitedCount(Integer maxVisitedCount) {
		this.maxVisitedCount = maxVisitedCount;
	}


	public String getVisitedSinceStateName() {
		return this.visitedSinceStateName;
	}


	public void setVisitedSinceStateName(String visitedSinceStateName) {
		this.visitedSinceStateName = visitedSinceStateName;
	}


	public String getVisitedSinceStatusName() {
		return this.visitedSinceStatusName;
	}


	public void setVisitedSinceStatusName(String visitedSinceStatusName) {
		this.visitedSinceStatusName = visitedSinceStatusName;
	}
}
