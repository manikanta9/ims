package com.clifton.workflow;


import com.clifton.core.beans.BaseEntity;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.io.Serializable;


/**
 * The <code>BaseWorkflowAwareEntity</code> class provides simple {@link WorkflowAware} implementation and should be extended
 * by DTO's that need workflow support.
 *
 * @author vgomelsky
 */
@WorkflowConfig
public class BaseWorkflowAwareEntity<T extends Serializable> extends BaseEntity<T> implements WorkflowAware {

	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	@Override
	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	@Override
	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	@Override
	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}
}
