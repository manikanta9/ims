package com.clifton.workflow.definition;


import com.clifton.workflow.definition.search.WorkflowSearchForm;
import com.clifton.workflow.definition.search.WorkflowStateSearchForm;
import com.clifton.workflow.definition.search.WorkflowStatusSearchForm;
import com.clifton.workflow.transition.WorkflowTransitionCommand;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The <code>WorkflowDefinitionService</code> interface defines methods for managing workflows, states, & status.
 *
 * @author manderson
 */
public interface WorkflowDefinitionService {

	////////////////////////////////////////////////////////////////////////////
	////////            Workflow Business Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	public Workflow getWorkflow(short id);


	public Workflow getWorkflowByName(String name);


	public List<Workflow> getWorkflowList();


	public List<Workflow> getWorkflowList(WorkflowSearchForm searchForm);


	public Workflow saveWorkflow(Workflow bean);


	/**
	 * Deletes the specified workflow, its states and transitions.
	 */
	public void deleteWorkflow(short id);


	/**
	 * Handles copying the workflow with the given ID.  Also copies all of it's states & transitions.
	 */
	public Workflow copyWorkflow(short workflowId, String name);

	////////////////////////////////////////////////////////////////////////////
	////////           Workflow State Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowState getWorkflowState(short id);


	public WorkflowState getWorkflowStateByName(short workflowId, String name);


	public List<WorkflowState> getWorkflowStateListByWorkflow(final short workflowId);


	public List<WorkflowState> getWorkflowStateListByWorkflowName(final String workflowName);


	public List<WorkflowState> getWorkflowStateListByName(final String stateName);


	public List<WorkflowState> getWorkflowStateList(WorkflowStateSearchForm searchForm);


	/**
	 * Returns a list of next allowed states for a given entity (determined by the tableName & id field)
	 * that are MANUAL transitions only.  If the entity doesn't exist yet, returns null.
	 */
	public List<WorkflowState> getWorkflowStateNextAllowedList(String tableName, Long id);


	/**
	 * Similar to getWorkflowStateNextAllowedList, but takes a command which allows excluding hidden and/or system defined transitions
	 */
	@ModelAttribute("data")
	public List<WorkflowState> getWorkflowStateNextAllowedListForCommand(WorkflowTransitionCommand workflowTransitionCommand);


	public WorkflowState saveWorkflowState(WorkflowState bean);


	/**
	 * Deletes the specified workflow state as well as all transitions into/out of this state.
	 */
	public void deleteWorkflowState(short id);

	////////////////////////////////////////////////////////////////////////////
	////////          Workflow Status Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowStatus getWorkflowStatus(short id);


	public WorkflowStatus getWorkflowStatusByName(String name);


	public List<WorkflowStatus> getWorkflowStatusList(WorkflowStatusSearchForm searchForm);


	public WorkflowStatus saveWorkflowStatus(WorkflowStatus bean);


	public void deleteWorkflowStatus(short id);
}
