package com.clifton.workflow.definition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class WorkflowStatusSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String name;

	@SearchField(sortField = "name")
	private String description;

	@SearchField(searchField = "stateList.workflow.assignmentList.table.name", comparisonConditions = ComparisonConditions.EXISTS)
	private String assignmentTableName;

	@SearchField(searchField = "stateList.workflow.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short workflowId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getAssignmentTableName() {
		return this.assignmentTableName;
	}


	public void setAssignmentTableName(String assignmentTableName) {
		this.assignmentTableName = assignmentTableName;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getWorkflowId() {
		return this.workflowId;
	}


	public void setWorkflowId(Short workflowId) {
		this.workflowId = workflowId;
	}
}
