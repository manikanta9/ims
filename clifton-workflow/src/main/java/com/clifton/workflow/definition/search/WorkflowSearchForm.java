package com.clifton.workflow.definition.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


/**
 * @author vgomelsky
 */
public class WorkflowSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private String documentationURL;

	@SearchField
	private Boolean active;

	@SearchField(searchField = "assignmentList.table.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short assignmentTableId;

	@SearchField(searchField = "assignmentList.table.name", comparisonConditions = ComparisonConditions.EXISTS)
	private String assignmentTableName;

	@SearchField
	private Boolean endDateSavedOnHistory;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDocumentationURL() {
		return this.documentationURL;
	}


	public void setDocumentationURL(String documentationURL) {
		this.documentationURL = documentationURL;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Short getAssignmentTableId() {
		return this.assignmentTableId;
	}


	public void setAssignmentTableId(Short assignmentTableId) {
		this.assignmentTableId = assignmentTableId;
	}


	public String getAssignmentTableName() {
		return this.assignmentTableName;
	}


	public void setAssignmentTableName(String assignmentTableName) {
		this.assignmentTableName = assignmentTableName;
	}


	public Boolean getEndDateSavedOnHistory() {
		return this.endDateSavedOnHistory;
	}


	public void setEndDateSavedOnHistory(Boolean endDateSavedOnHistory) {
		this.endDateSavedOnHistory = endDateSavedOnHistory;
	}
}

