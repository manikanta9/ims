package com.clifton.workflow.definition.validation;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.history.WorkflowHistory;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;


/**
 * The <code>WorkflowValidator</code> class handles all validation prior to editing a {@link Workflow}
 *
 * @author manderson
 */
@Component
public class WorkflowValidator extends SelfRegisteringDaoValidator<Workflow> {

	private AdvancedReadOnlyDAO<WorkflowHistory, Criteria> workflowDAO;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(Workflow bean, DaoEventTypes config) throws ValidationException {
		// Validate in-activating an active Workflow
		if (DaoEventTypes.UPDATE == config && !bean.isActive()) {
			Workflow originalBean = getOriginalBean(bean);
			if (originalBean.isActive() && isWorkflowUsed(bean.getId())) {
				throw new ValidationException("Cannot inactivate Workflow [" + bean.getName() + "] because it has already been used and has history records tied to it.");
			}
		}

		// Validate deleting an active workflow
		if (DaoEventTypes.DELETE == config) {
			Workflow originalBean = getOriginalBean(bean);
			if (originalBean.isActive()) {
				throw new ValidationException("Cannot delete an active Workflow [" + bean.getName() + "].");
			}
		}
	}


	private boolean isWorkflowUsed(final short workflowId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			DetachedCriteria sub = DetachedCriteria.forClass(WorkflowHistory.class, "wh");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("endWorkflowState", "ews");
			sub.add(Restrictions.eqProperty("ews.workflow.id", criteria.getAlias() + ".id"));
			criteria.add(Restrictions.eq("id", workflowId));
			criteria.add(Subqueries.exists(sub));
		};
		return (CollectionUtils.getSize(getWorkflowDAO().findBySearchCriteria(searchConfigurer)) > 0);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<WorkflowHistory, Criteria> getWorkflowDAO() {
		return this.workflowDAO;
	}


	public void setWorkflowDAO(AdvancedReadOnlyDAO<WorkflowHistory, Criteria> workflowDAO) {
		this.workflowDAO = workflowDAO;
	}
}
