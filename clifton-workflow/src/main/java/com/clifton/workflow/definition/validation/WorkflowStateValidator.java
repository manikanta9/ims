package com.clifton.workflow.definition.validation;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.search.WorkflowTransitionSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>WorkflowStateValidator</code> class handles all validation prior to editing a {@link WorkflowState}
 *
 * @author manderson
 */
@Component
public class WorkflowStateValidator extends SelfRegisteringDaoValidator<WorkflowState> {

	private SecurityAuthorizationService securityAuthorizationService;

	private WorkflowTransitionService workflowTransitionService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(@SuppressWarnings("unused") WorkflowState bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		// DO NOTHING - USE METHOD WITH THE DAO
	}


	@Override
	public void validate(WorkflowState bean, @SuppressWarnings("unused") DaoEventTypes config, ReadOnlyDAO<WorkflowState> dao) throws ValidationException {
		Workflow workflow;

		boolean validateOrder = false;
		if (bean.isNewBean()) {
			workflow = bean.getWorkflow();
			if (bean.getOrder() != null) {
				validateOrder = true;
			}
		}
		else {
			WorkflowState originalBean = getOriginalBean(bean);
			workflow = originalBean.getWorkflow();
			if (!bean.getWorkflow().equals(workflow)) {
				// Throwing an IllegalStateException here because user should NEVER be allowed to get to this state
				throw new IllegalStateException("Cannot change workflow for workflow state [" + bean.getName() + "]");
			}
			if (bean.getOrder() != null && (originalBean.getOrder() == null || !MathUtils.isEqual(bean.getOrder(), originalBean.getOrder()))) {
				validateOrder = true;
			}
		}
		if (validateOrder) {
			WorkflowState orderState = CollectionUtils.getFirstElement(dao.findByFields(new String[]{"workflow.id", "order"}, new Object[]{bean.getWorkflow().getId(), bean.getOrder()}));
			if (orderState != null) {
				throw new FieldValidationException("Workflow State [" + orderState.getName() + "] is already assigned to order [" + orderState.getOrder() + "].  Order should be unique per workflow.",
						"order");
			}
		}
		if (workflow.isActive()) {
			if (!getSecurityAuthorizationService().isSecurityUserAdmin()) {
				throw new ValidationException("Workflow State [" + bean.getName() + "] belongs to an active workflow [" + workflow.getName()
						+ "] and cannot be edited unless your are an Administrator.");
			}
		}

		if (config.isUpdate()) {
			WorkflowState originalBean = getOriginalBean(bean);
			if (!originalBean.isDisabled() && bean.isDisabled()) {
				WorkflowTransitionSearchForm searchForm = new WorkflowTransitionSearchForm();
				searchForm.setStartOrEndWorkflowStateId(bean.getId());

				List<WorkflowTransition> workflowTransitions = getWorkflowTransitionService().getWorkflowTransitionList(searchForm);

				if (!CollectionUtils.isEmpty(workflowTransitions)) {
					throw new ValidationException("Workflow States can only be disabled if there are no transitions to or from that state.");
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
