package com.clifton.workflow.definition.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class WorkflowStateSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "workflow.id")
	private Short workflowId;

	@SearchField(searchField = "name", searchFieldPath = "workflow", comparisonConditions = ComparisonConditions.EQUALS)
	private String workflowName;

	@SearchField(searchField = "status.id")
	private Short statusId;

	@SearchField
	private Integer order;

	@SearchField(searchField = "entityUpdateCondition.id")
	private Integer entityUpdateConditionId;

	@SearchField(searchField = "entityDeleteCondition.id")
	private Integer entityDeleteConditionId;

	@SearchField
	private Boolean disabled;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getWorkflowId() {
		return this.workflowId;
	}


	public void setWorkflowId(Short workflowId) {
		this.workflowId = workflowId;
	}


	public String getWorkflowName() {
		return this.workflowName;
	}


	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}


	public Short getStatusId() {
		return this.statusId;
	}


	public void setStatusId(Short statusId) {
		this.statusId = statusId;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Integer getEntityUpdateConditionId() {
		return this.entityUpdateConditionId;
	}


	public void setEntityUpdateConditionId(Integer entityUpdateConditionId) {
		this.entityUpdateConditionId = entityUpdateConditionId;
	}


	public Integer getEntityDeleteConditionId() {
		return this.entityDeleteConditionId;
	}


	public void setEntityDeleteConditionId(Integer entityDeleteConditionId) {
		this.entityDeleteConditionId = entityDeleteConditionId;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
}
