package com.clifton.workflow.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * A <code>WorkflowStatus</code> object defines a status that states are tied to.
 *
 * @author manderson
 */
@CacheByName
public class WorkflowStatus extends NamedEntity<Short> {

	public static final String STATUS_ACTIVE = "Active";

	public static final String STATUS_APPROVED = "Approved";

	public static final String STATUS_CANCELED = "Cancelled";

	public static final String STATUS_CLOSED = "Closed";

	public static final String STATUS_INACTIVE = "Inactive";

	public static final String STATUS_INVALID = "Invalid";

	public static final String STATUS_NON_OPERATIONAL = "Non-Operational";

	public static final String STATUS_OPEN = "Open";

	public static final String STATUS_PENDING = "Pending";

	public static final String STATUS_DRAFT = "Draft";

	public static final String STATUS_REVIEW = "Review";
}
