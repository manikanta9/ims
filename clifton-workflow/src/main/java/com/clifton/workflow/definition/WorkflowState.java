package com.clifton.workflow.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;


/**
 * A <code>WorkflowState</code> object defines the steps of a workflow that
 * entities can be in.
 *
 * @author manderson
 */
public class WorkflowState extends NamedEntity<Short> {

	private Workflow workflow;
	private WorkflowStatus status;

	/**
	 * Optional numerical order of this state.
	 */
	private Integer order;

	/**
	 * Dynamically determines if the entity can be updated while in this state given it's current field values.
	 * If null, then the entity can be modified.
	 * If not null, then evaluates the condition to determine if changes are allowed. If changes disallowed, exclude any workflow transition changes.
	 */
	private SystemCondition entityUpdateCondition;

	/**
	 * Dynamically determines if the entity can be deleted while in this state.
	 * If null, then the entity can be deleted.
	 * If not null, then evaluates the condition to determine if deletes are allowed. If delete is disallowed, exclude any workflow transition changes.
	 */
	private SystemCondition entityDeleteCondition;

	/**
	 *  Disabled states cannot be used (i.e. cannot have any transitions to or from the state) . Should be used for Workflow States that are no longer used, but we can't delete them because they have been used historically.
	 */
	private boolean disabled;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Override
	public String getLabel() {
		return isDisabled() ? getName() + "[DISABLED]" : getName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowStatus getStatus() {
		return this.status;
	}


	public void setStatus(WorkflowStatus status) {
		this.status = status;
	}


	public Workflow getWorkflow() {
		return this.workflow;
	}


	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public SystemCondition getEntityUpdateCondition() {
		return this.entityUpdateCondition;
	}


	public void setEntityUpdateCondition(SystemCondition entityUpdateCondition) {
		this.entityUpdateCondition = entityUpdateCondition;
	}


	public SystemCondition getEntityDeleteCondition() {
		return this.entityDeleteCondition;
	}


	public void setEntityDeleteCondition(SystemCondition entityDeleteCondition) {
		this.entityDeleteCondition = entityDeleteCondition;
	}


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
}
