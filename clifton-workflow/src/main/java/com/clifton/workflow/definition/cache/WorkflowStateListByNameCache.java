package com.clifton.workflow.definition.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.workflow.definition.WorkflowState;
import org.springframework.stereotype.Component;


/**
 * The WorkflowStateListByNameCache caches a list of WorkflowStates for a Given Workflow State Name.  It is independent of Workflows
 * and since Workflow States with the same name are often small, it is also independent of table names.
 *
 * @author manderson
 */
@Component
public class WorkflowStateListByNameCache extends SelfRegisteringSingleKeyDaoListCache<WorkflowState, String> {

	@Override
	protected String getBeanKeyProperty() {
		return "name";
	}


	@Override
	protected String getBeanKeyValue(WorkflowState bean) {
		return bean.getName();
	}
}


