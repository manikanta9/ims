package com.clifton.workflow.definition.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.workflow.definition.WorkflowState;
import org.springframework.stereotype.Component;


/**
 * The <code>WorkflowStateCache</code> class provides caching and retrieval from cache of WorkflowState objects
 * by key: WorkflowID_WorkflowStateName
 *
 * @author manderson
 */
@Component
public class WorkflowStateCache extends SelfRegisteringCompositeKeyDaoCache<WorkflowState, Short, String> {


	@Override
	protected String getBeanKey1Property() {
		return "workflow.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "name";
	}


	@Override
	protected Short getBeanKey1Value(WorkflowState bean) {
		if (bean.getWorkflow() != null) {
			return bean.getWorkflow().getId();
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(WorkflowState bean) {
		return bean.getName();
	}
}
