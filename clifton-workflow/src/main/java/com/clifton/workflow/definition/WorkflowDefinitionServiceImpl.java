package com.clifton.workflow.definition;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.workflow.definition.search.WorkflowSearchForm;
import com.clifton.workflow.definition.search.WorkflowStateSearchForm;
import com.clifton.workflow.definition.search.WorkflowStatusSearchForm;
import com.clifton.workflow.definition.validation.WorkflowStateValidator;
import com.clifton.workflow.definition.validation.WorkflowValidator;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionAction;
import com.clifton.workflow.transition.WorkflowTransitionCommand;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>WorkflowDefinitionServiceImpl</code> class provides default implementation of {@linkplain WorkflowDefinitionService}.
 *
 * @author manderson
 */
@Service
public class WorkflowDefinitionServiceImpl implements WorkflowDefinitionService {

	private AdvancedUpdatableDAO<Workflow, Criteria> workflowDAO;
	private AdvancedUpdatableDAO<WorkflowState, Criteria> workflowStateDAO;
	private AdvancedUpdatableDAO<WorkflowStatus, Criteria> workflowStatusDAO;

	private WorkflowTransitionService workflowTransitionService;

	private DaoCompositeKeyCache<WorkflowState, Short, String> workflowStateCache;
	private DaoSingleKeyListCache<WorkflowState, String> workflowStateListByNameCache;
	private DaoNamedEntityCache<WorkflowStatus> workflowStatusCache;


	private DaoNamedEntityCache<Workflow> workflowCache;

	////////////////////////////////////////////////////////////////////////////
	////////            Workflow Business Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Workflow getWorkflow(short id) {
		return getWorkflowDAO().findByPrimaryKey(id);
	}


	@Override
	public Workflow getWorkflowByName(String name) {
		return getWorkflowCache().getBeanForKeyValueStrict(getWorkflowDAO(), name);
	}


	@Override
	public List<Workflow> getWorkflowList() {
		return getWorkflowDAO().findAll();
	}


	@Override
	public List<Workflow> getWorkflowList(WorkflowSearchForm searchForm) {
		return getWorkflowDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see WorkflowValidator
	 */
	@Override
	public Workflow saveWorkflow(Workflow bean) {
		// All new workflows must be created as inactive
		if (bean.isNewBean()) {
			bean.setActive(false);
		}

		return getWorkflowDAO().save(bean);
	}


	/**
	 * @see WorkflowValidator
	 */
	@Override
	@Transactional
	public void deleteWorkflow(short id) {
		// Delete All Transitions
		getWorkflowTransitionService().deleteWorkflowTransitionList(getWorkflowTransitionService().getWorkflowTransitionListByWorkflow(id));

		// Delete all states
		getWorkflowStateDAO().deleteList(getWorkflowStateListByWorkflow(id));

		// Finally, delete the workflow
		getWorkflowDAO().delete(id);
	}


	@Override
	@Transactional
	public Workflow copyWorkflow(short workflowId, String name) {
		// Workflow - Get the Workflow to copy, reset the ID, set the new name, & insert into the database
		Workflow workflow = BeanUtils.cloneBean(getWorkflow(workflowId), false, false);
		workflow.setName(name);
		saveWorkflow(workflow);

		// Workflow States - Get each state in source workflow, reset ID, set to new Workflow, & insert into the database
		// Track mapping old States to new States in a HashMap
		Map<WorkflowState, WorkflowState> stateMap = new HashMap<>();
		List<WorkflowState> stateList = getWorkflowStateListByWorkflow(workflowId);
		for (WorkflowState state : CollectionUtils.getIterable(stateList)) {
			// Clone the state so we aren't using the same reference
			WorkflowState newState = BeanUtils.cloneBean(state, false, false);
			newState.setWorkflow(workflow);
			saveWorkflowState(newState);
			stateMap.put(state, newState);
		}

		// Workflow Transition - Get each transition for the workflow, reset the ID, update the State IDs, & insert into the database
		List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionListByWorkflow(workflowId);
		for (WorkflowTransition transition : CollectionUtils.getIterable(transitionList)) {
			WorkflowTransition newTransition = BeanUtils.cloneBean(transition, false, false);
			if (newTransition.getStartWorkflowState() != null) {
				newTransition.setStartWorkflowState(stateMap.get(transition.getStartWorkflowState()));
			}
			newTransition.setEndWorkflowState(stateMap.get(newTransition.getEndWorkflowState()));

			if (transition.isActionsExist()) {
				List<WorkflowTransitionAction> actionList = getWorkflowTransitionService().getWorkflowTransitionActionListForTransitionId(transition.getId());
				List<WorkflowTransitionAction> newActionList = new ArrayList<>();

				for (WorkflowTransitionAction action : CollectionUtils.getIterable(actionList)) {
					WorkflowTransitionAction newAction = BeanUtils.cloneBean(action, false, false);
					newAction.setWorkflowTransition(newTransition);
					newActionList.add(newAction);
				}

				newTransition.setActionList(newActionList);
			}
			getWorkflowTransitionService().saveWorkflowTransition(newTransition);
		}
		return workflow;
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Workflow State Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowState getWorkflowState(short id) {
		return getWorkflowStateDAO().findByPrimaryKey(id);
	}


	@Override
	public WorkflowState getWorkflowStateByName(short workflowId, String name) {
		return getWorkflowStateCache().getBeanForKeyValues(getWorkflowStateDAO(), workflowId, name);
	}


	@Override
	public List<WorkflowState> getWorkflowStateListByWorkflow(final short workflowId) {
		return getWorkflowStateDAO().findByField("workflow.id", workflowId);
	}


	@Override
	public List<WorkflowState> getWorkflowStateListByWorkflowName(final String workflowName) {
		WorkflowStateSearchForm searchForm = new WorkflowStateSearchForm();
		searchForm.setWorkflowName(workflowName);

		return getWorkflowStateList(searchForm);
	}


	@Override
	public List<WorkflowState> getWorkflowStateListByName(String stateName) {
		return getWorkflowStateListByNameCache().getBeanListForKeyValue(getWorkflowStateDAO(), stateName);
	}


	@Override
	public List<WorkflowState> getWorkflowStateList(WorkflowStateSearchForm searchForm) {
		return getWorkflowStateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<WorkflowState> getWorkflowStateNextAllowedList(String tableName, Long id) {
		return getEndWorkflowStateListForTransitionList(getWorkflowTransitionService().getWorkflowTransitionNextListByEntity(tableName, id));
	}


	@Override
	public List<WorkflowState> getWorkflowStateNextAllowedListForCommand(WorkflowTransitionCommand workflowTransitionCommand) {
		return getEndWorkflowStateListForTransitionList(getWorkflowTransitionService().getWorkflowTransitionNextList(workflowTransitionCommand));
	}


	private List<WorkflowState> getEndWorkflowStateListForTransitionList(List<WorkflowTransition> transitionList) {
		List<WorkflowState> list = new ArrayList<>();
		for (WorkflowTransition t : CollectionUtils.getIterable(transitionList)) {
			if (!list.contains(t.getEndWorkflowState())) {
				list.add(t.getEndWorkflowState());
			}
		}
		return list;
	}


	/**
	 * @see WorkflowStateValidator
	 */
	@Override
	public WorkflowState saveWorkflowState(WorkflowState bean) {
		return getWorkflowStateDAO().save(bean);
	}


	/**
	 * @see WorkflowStateValidator
	 */
	@Override
	@Transactional
	public void deleteWorkflowState(short id) {
		// Delete Workflow Transitions
		getWorkflowTransitionService().deleteWorkflowTransitionList(getWorkflowTransitionService().getWorkflowTransitionListByStartOrEndState(getWorkflowState(id)));

		// Delete Workflow State
		getWorkflowStateDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////          Workflow Status Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowStatus getWorkflowStatus(short id) {
		return getWorkflowStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public WorkflowStatus getWorkflowStatusByName(String name) {
		return getWorkflowStatusCache().getBeanForKeyValueStrict(getWorkflowStatusDAO(), name);
	}


	@Override
	public List<WorkflowStatus> getWorkflowStatusList(WorkflowStatusSearchForm searchForm) {
		return getWorkflowStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public WorkflowStatus saveWorkflowStatus(WorkflowStatus bean) {
		return getWorkflowStatusDAO().save(bean);
	}


	@Override
	public void deleteWorkflowStatus(short id) {
		getWorkflowStatusDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<Workflow, Criteria> getWorkflowDAO() {
		return this.workflowDAO;
	}


	public void setWorkflowDAO(AdvancedUpdatableDAO<Workflow, Criteria> workflowDAO) {
		this.workflowDAO = workflowDAO;
	}


	public AdvancedUpdatableDAO<WorkflowStatus, Criteria> getWorkflowStatusDAO() {
		return this.workflowStatusDAO;
	}


	public void setWorkflowStatusDAO(AdvancedUpdatableDAO<WorkflowStatus, Criteria> workflowStatusDAO) {
		this.workflowStatusDAO = workflowStatusDAO;
	}


	public AdvancedUpdatableDAO<WorkflowState, Criteria> getWorkflowStateDAO() {
		return this.workflowStateDAO;
	}


	public void setWorkflowStateDAO(AdvancedUpdatableDAO<WorkflowState, Criteria> workflowStateDAO) {
		this.workflowStateDAO = workflowStateDAO;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public DaoNamedEntityCache<WorkflowStatus> getWorkflowStatusCache() {
		return this.workflowStatusCache;
	}


	public void setWorkflowStatusCache(DaoNamedEntityCache<WorkflowStatus> workflowStatusCache) {
		this.workflowStatusCache = workflowStatusCache;
	}


	public DaoCompositeKeyCache<WorkflowState, Short, String> getWorkflowStateCache() {
		return this.workflowStateCache;
	}


	public void setWorkflowStateCache(DaoCompositeKeyCache<WorkflowState, Short, String> workflowStateCache) {
		this.workflowStateCache = workflowStateCache;
	}


	public DaoSingleKeyListCache<WorkflowState, String> getWorkflowStateListByNameCache() {
		return this.workflowStateListByNameCache;
	}


	public void setWorkflowStateListByNameCache(DaoSingleKeyListCache<WorkflowState, String> workflowStateListByNameCache) {
		this.workflowStateListByNameCache = workflowStateListByNameCache;
	}


	public DaoNamedEntityCache<Workflow> getWorkflowCache() {
		return this.workflowCache;
	}


	public void setWorkflowCache(DaoNamedEntityCache<Workflow> workflowCache) {
		this.workflowCache = workflowCache;
	}
}
