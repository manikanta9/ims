package com.clifton.workflow.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.workflow.transition.WorkflowTransition;

import java.util.List;


/**
 * The <code>Workflow</code> class defines the life-cycle for a given entity (database row).
 * It is a collection of states and transitions that define valid flows from one workflow state to the next.
 * A workflow should not be active until it is setup and ready for use.
 *
 * @author manderson
 */
@CacheByName
public class Workflow extends NamedEntity<Short> {

	/**
	 * Relative URL for this workflow's documentation (diagram, etc.).  Usually stored in a WIKI.
	 */
	private String documentationURL;

	private boolean active;


	@OneToManyEntity(serviceBeanName = "workflowDefinitionService", serviceMethodName = "getWorkflowStateListByWorkflow")
	private List<WorkflowState> stateList;

	@OneToManyEntity(serviceBeanName = "workflowTransitionService", serviceMethodName = "getWorkflowTransitionListByWorkflow")
	private List<WorkflowTransition> transitionList;

	/**
	 * If true then for each transition that occurs the previous history record is retrieved and end date is set as well as a new history record is created for the new workflow state.
	 * If false, then only new history records are created and previous history record is not retrieved or updated. Leave off for better performance (reduces the reads/writes during workflow transition processing).
	 * If false, there is a batch job that can be used to fill in the end dates separately if needed.
	 */
	private boolean endDateSavedOnHistory;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDocumentationURL() {
		return this.documentationURL;
	}


	public void setDocumentationURL(String documentationURL) {
		this.documentationURL = documentationURL;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public List<WorkflowState> getStateList() {
		return this.stateList;
	}


	public void setStateList(List<WorkflowState> stateList) {
		this.stateList = stateList;
	}


	public List<WorkflowTransition> getTransitionList() {
		return this.transitionList;
	}


	public void setTransitionList(List<WorkflowTransition> transitionList) {
		this.transitionList = transitionList;
	}


	public boolean isEndDateSavedOnHistory() {
		return this.endDateSavedOnHistory;
	}


	public void setEndDateSavedOnHistory(boolean endDateSavedOnHistory) {
		this.endDateSavedOnHistory = endDateSavedOnHistory;
	}
}
