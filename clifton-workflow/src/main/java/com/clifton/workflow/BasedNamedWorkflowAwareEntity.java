package com.clifton.workflow;


import com.clifton.core.beans.NamedEntity;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;


/**
 * The <code>BasedNamedWorkflowAwareEntity</code> provides simple {@link WorkflowAware} implementation
 * and extends {@link NamedEntity}.
 * Should be extended by DTO's that need workflow support and named entity support
 *
 * @author manderson
 */
@WorkflowConfig
public class BasedNamedWorkflowAwareEntity extends NamedEntity<Integer> implements WorkflowAware {

	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	@Override
	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	@Override
	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	@Override
	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}
}
