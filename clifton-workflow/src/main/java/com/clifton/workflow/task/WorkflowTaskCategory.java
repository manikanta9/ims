package com.clifton.workflow.task;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.condition.SystemCondition;


/**
 * The WorkflowTaskCategory class is used to group related task definitions.
 * For example, Accounting Tasks, Compliance Tasks, Operations Tasks, Trading Tasks, etc.
 *
 * @author vgomelsky
 */
@CacheByName
public class WorkflowTaskCategory extends NamedEntity<Short> {

	/**
	 * Optionally specifies Entity Modify Condition for all Workflow Task Definitions for this category.
	 */
	private SystemCondition definitionEntityModifyCondition;

	/**
	 * Specifies whether locking configuration is allowed for Workflow Task Definitions for this category.
	 * Incorrectly used locking is dangerous, because it can prevent changes to important system data.
	 * Task categories that can be created/updated by less advanced users, should not allow locking.
	 */
	private boolean definitionLockingAllowed;

	/**
	 * Specifies whether Workflow Task Definitions for this Category can be configured to allow Task generation on entity creation.
	 */
	private boolean generateOnEntityCreationAllowed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemCondition getDefinitionEntityModifyCondition() {
		return this.definitionEntityModifyCondition;
	}


	public void setDefinitionEntityModifyCondition(SystemCondition definitionEntityModifyCondition) {
		this.definitionEntityModifyCondition = definitionEntityModifyCondition;
	}


	public boolean isDefinitionLockingAllowed() {
		return this.definitionLockingAllowed;
	}


	public void setDefinitionLockingAllowed(boolean definitionLockingAllowed) {
		this.definitionLockingAllowed = definitionLockingAllowed;
	}


	public boolean isGenerateOnEntityCreationAllowed() {
		return this.generateOnEntityCreationAllowed;
	}


	public void setGenerateOnEntityCreationAllowed(boolean generateOnEntityCreationAllowed) {
		this.generateOnEntityCreationAllowed = generateOnEntityCreationAllowed;
	}
}
