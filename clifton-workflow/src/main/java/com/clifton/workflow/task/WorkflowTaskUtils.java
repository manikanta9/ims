package com.clifton.workflow.task;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * @author vgomelsky
 */
public class WorkflowTaskUtils {

	/**
	 * Returns LinkedEntity for the specified lockedEntity.  Linked entity is identified by the first matching path from the specified lock.
	 * The method throws {@link ValidationException} if the entity cannot be found.
	 */
	public static IdentityObject getLinkedEntityForLockedEntity(IdentityObject lockedEntity, WorkflowTaskDefinitionLock lock) {
		String path = lock.getLinkedEntityBeanFieldPath();
		if ("id".equals(path)) {
			return lockedEntity;
		}

		for (String propertyPath : lock.getLinkedEntityBeanFieldPaths()) {
			ValidationUtils.assertTrue(propertyPath.endsWith(".id"), "Task definition lock must have linked entity bean field path that ends on '.id': " + propertyPath);
			propertyPath = propertyPath.substring(0, propertyPath.length() - 3);
			IdentityObject entity = (IdentityObject) BeanUtils.getPropertyValue(lockedEntity, propertyPath);
			if (entity != null) {
				return entity;
			}
		}

		throw new ValidationException("Cannot find Workflow Task Linked Entity from Lock Entity " + lockedEntity + " using path = " + path);
	}
}
