package com.clifton.workflow.task.lock;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.search.WorkflowTaskSearchForm;

import java.util.Collections;
import java.util.List;


/**
 * Checks if there are any workflow tasks that satisfy the specified workflow status criteria. The resulting value will be negated if the negate property is set to true.
 * <p>
 * IMPORTANT: If includeWorkflowStatusIds and excludeWorkflowStatusIds are not disjoint - they contain one or more value that is the same - the result
 * of this comparison will always be False (not accounting for negation).
 *
 * @author theodorez
 */
public class WorkflowTaskLockExistsComparison implements Comparison<IdentityObject>, ValidationAware {

	/**
	 * If any task linked to the entity has one of the statuses specified here, it will be INCLUDED in the search result
	 * <p>
	 * Examples: (Note: these do not account for the negation attribute)
	 * If an entity has any tasks that match the type(s) specified here, the final result of this comparison will be TRUE.
	 * <p>
	 * If an entity has no tasks that match the type(s) specified, the final result of this comparison will be FALSE
	 */
	private List<Short> includeWorkflowStatusIds;

	/**
	 * If any task linked to the entity has one of the statuses specified here, it will be EXCLUDED in the search result.
	 * <p>
	 * Examples: (Note: these do not account for the negation attribute)
	 * If an entity ONLY has tasks of the type(s) specified here the final result of this comparison will be FALSE.
	 * <p>
	 * If an entity has ANY tasks that have a type(s) not specified here, the final result of this comparison will be TRUE.
	 */
	private List<Short> excludeWorkflowStatusIds;

	/**
	 * If this is set to true, the result of the inclusion and exclusion criteria will be reversed
	 * <p>
	 * I.e. if excluding CLOSED statuses on an entity with two tasks (ACTIVE and CLOSED), the result of which would return TRUE because the ACTIVE task would be returned.
	 * The negation would be FALSE, indicating not all tasks are CLOSED.
	 * <p>
	 * If including ACTIVE for the same entity (with no exclusion criteria), the result would be TRUE because the ACTIVE result would be returned.
	 * The negation would be FALSE, which would be used when you want to ensure that a specific status did not exist.
	 */
	private boolean doNotExpectResults;


	private DaoLocator daoLocator;

	private WorkflowTaskService workflowTaskService;
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		validate();

		WorkflowTaskSearchForm taskSearchForm = new WorkflowTaskSearchForm();

		if (!CollectionUtils.isEmpty(getIncludeWorkflowStatusIds())) {
			taskSearchForm.setWorkflowStatusIds(CollectionUtils.toArray(getIncludeWorkflowStatusIds(), Short.class));
		}

		if (!CollectionUtils.isEmpty(getExcludeWorkflowStatusIds())) {
			taskSearchForm.setExcludeWorkflowStatusIds(CollectionUtils.toArray(getExcludeWorkflowStatusIds(), Short.class));
		}

		taskSearchForm.setLinkedEntityFkFieldId(BeanUtils.getIdentityAsLong(bean));

		ReadOnlyDAO<?> dao = getDaoLocator().locate(bean);
		ValidationUtils.assertNotNull(dao, "No DAO found for bean: " + bean);
		taskSearchForm.setLinkedEntityTableName(dao.getConfiguration().getTableName());

		List<WorkflowTask> taskResults = getWorkflowTaskService().getWorkflowTaskList(taskSearchForm);
		//tasksFound is TRUE if tasks are found
		//tasksFound is FALSE if tasks are not found
		boolean tasksFound = !CollectionUtils.isEmpty(taskResults);

		//If we do not expect results
		//Tasks found should be empty (i.e. tasksFound should be false), and thus the final result should be true
		boolean finalResult = isDoNotExpectResults() ? !tasksFound : tasksFound;

		// record comparison result message
		if (context != null) {
			LogUtils.debug(LogCommand.ofMessageSupplier(getClass(), () -> {
				StringBuilder messageBuilder = new StringBuilder("WorkflowTaskLockExistsComparison for Identity Object ID: [").append(bean.getIdentity()).append("]");
				messageBuilder.append(" Tasks Found: ").append(tasksFound);
				messageBuilder.append(" Included Status Ids: [").append(StringUtils.collectionToCommaDelimitedString(getIncludeWorkflowStatusIds())).append("]");
				messageBuilder.append(" Excluded Status Ids: [").append(StringUtils.collectionToCommaDelimitedString(getExcludeWorkflowStatusIds())).append("]");
				messageBuilder.append(" Do Not Expect Results: ").append(isDoNotExpectResults());
				messageBuilder.append(" Final Result: ").append(finalResult);
				return messageBuilder.toString();
			}));

			String message = tasksFound ? "the following workflow task(s) were found: [" + StringUtils.join(taskResults, WorkflowTask::getLabel, "]") + "]" : "no workflow tasks were found.";
			if (finalResult) {
				context.recordTrueMessage(message);
			}
			else {
				context.recordFalseMessage((isDoNotExpectResults() ? "Results were NOT expected but " : "Results were expected but ") + message);
			}
		}

		return finalResult;
	}


	@Override
	public void validate() throws ValidationException {
		if (!CollectionUtils.isEmpty(getIncludeWorkflowStatusIds()) && !CollectionUtils.isEmpty(getExcludeWorkflowStatusIds())) {
			//Check to make sure the lists are disjoint - that is that there are no elements from the inclusion list that are also elements on the exclusion list
			ValidationUtils.assertTrue(Collections.disjoint(getIncludeWorkflowStatusIds(), getExcludeWorkflowStatusIds()), "Not allowed: the inclusion and exclusion lists contain at least one value that is the same.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<Short> getIncludeWorkflowStatusIds() {
		return this.includeWorkflowStatusIds;
	}


	public void setIncludeWorkflowStatusIds(List<Short> includeWorkflowStatusIds) {
		this.includeWorkflowStatusIds = includeWorkflowStatusIds;
	}


	public List<Short> getExcludeWorkflowStatusIds() {
		return this.excludeWorkflowStatusIds;
	}


	public void setExcludeWorkflowStatusIds(List<Short> excludeWorkflowStatusIds) {
		this.excludeWorkflowStatusIds = excludeWorkflowStatusIds;
	}


	public boolean isDoNotExpectResults() {
		return this.doNotExpectResults;
	}


	public void setDoNotExpectResults(boolean doNotExpectResults) {
		this.doNotExpectResults = doNotExpectResults;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}
}
