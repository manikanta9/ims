package com.clifton.workflow.task.generator;

import java.io.Serializable;
import java.util.Date;


/**
 * The TaskGeneratorCommand class specifies parameters that are used during {@link com.clifton.workflow.task.WorkflowTask} generation.
 *
 * @author vgomelsky
 */
public class TaskGeneratorCommand implements Serializable {

	private short taskDefinitionId;

	private Date fromDate;
	private Date toDate;

	/**
	 * If 'preview' is requested, the results will not be saved in the database: only returned back to the caller.
	 */
	private boolean preview;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TaskGeneratorCommand ofDateRange(short taskDefinitionId, Date fromDate, Date toDate) {
		TaskGeneratorCommand result = new TaskGeneratorCommand();
		result.setTaskDefinitionId(taskDefinitionId);
		result.setFromDate(fromDate);
		result.setToDate(toDate);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public short getTaskDefinitionId() {
		return this.taskDefinitionId;
	}


	public void setTaskDefinitionId(short taskDefinitionId) {
		this.taskDefinitionId = taskDefinitionId;
	}


	public Date getFromDate() {
		return this.fromDate;
	}


	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	public Date getToDate() {
		return this.toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	public boolean isPreview() {
		return this.preview;
	}


	public void setPreview(boolean preview) {
		this.preview = preview;
	}
}
