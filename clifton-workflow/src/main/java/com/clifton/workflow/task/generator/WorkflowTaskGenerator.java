package com.clifton.workflow.task.generator;

import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;

import java.util.Date;
import java.util.List;


/**
 * The WorkflowTaskGenerator interface must be implemented by classes that generate new {@link WorkflowTask} objects based on corresponding logic.
 * {@link com.clifton.workflow.task.WorkflowTaskDefinition#taskGeneratorBean} property defines specific task generator to use for a given
 * task definition.
 * <p>
 * For example, there could be a generator that creates periodic tasks based on a CalendarSchedule (every last business day of the month, etc.).
 *
 * @author vgomelsky
 */
public interface WorkflowTaskGenerator {

	/**
	 * Generates and returns a list of {@link WorkflowTask} objects for the specified command.
	 * The result is NOT saved in the database.
	 */
	public List<WorkflowTask> generate(WorkflowTaskDefinition taskDefinition, Date fromDate, Date toDate);
}
