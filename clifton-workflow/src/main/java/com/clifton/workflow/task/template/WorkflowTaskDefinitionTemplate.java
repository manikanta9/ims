package com.clifton.workflow.task.template;

import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.workflow.task.WorkflowTaskDefinition;


/**
 * The WorkflowTaskDefinitionTemplate class can be used to simplify creation of {@link WorkflowTaskDefinition} objects
 * by coping most fields from corresponding template definition.
 *
 * @author vgomelsky
 */
public class WorkflowTaskDefinitionTemplate extends NamedEntity<Short> {

	/**
	 * Uses the following task definition as a template for new definitions: will copy fields from the template.
	 */
	private WorkflowTaskDefinition templateDefinition;
	/**
	 * Evaluates to true for users that can create task definitions created from this template.
	 */
	private SystemCondition taskDefinitionCreateCondition;

	// optionally default/name description to these values
	private String defaultName;
	private String defaultDescription;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskDefinition getTemplateDefinition() {
		return this.templateDefinition;
	}


	public void setTemplateDefinition(WorkflowTaskDefinition templateDefinition) {
		this.templateDefinition = templateDefinition;
	}


	public SystemCondition getTaskDefinitionCreateCondition() {
		return this.taskDefinitionCreateCondition;
	}


	public void setTaskDefinitionCreateCondition(SystemCondition taskDefinitionCreateCondition) {
		this.taskDefinitionCreateCondition = taskDefinitionCreateCondition;
	}


	public String getDefaultName() {
		return this.defaultName;
	}


	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}


	public String getDefaultDescription() {
		return this.defaultDescription;
	}


	public void setDefaultDescription(String defaultDescription) {
		this.defaultDescription = defaultDescription;
	}
}
