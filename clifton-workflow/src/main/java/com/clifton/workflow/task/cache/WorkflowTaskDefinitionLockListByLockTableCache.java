package com.clifton.workflow.task.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.workflow.task.WorkflowTaskDefinitionLock;
import org.springframework.stereotype.Component;


/**
 * The WorkflowTaskDefinitionLockListByLockTableCache cache caches lists of WorkflowTaskDefinitionLock objects by corresponding lock table id.
 *
 * @author vgomelsky
 */
@Component
public class WorkflowTaskDefinitionLockListByLockTableCache extends SelfRegisteringSingleKeyDaoListCache<WorkflowTaskDefinitionLock, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "lockEntityTable.id";
	}


	@Override
	protected Short getBeanKeyValue(WorkflowTaskDefinitionLock bean) {
		if (bean.getLockEntityTable() != null) {
			return bean.getLockEntityTable().getId();
		}
		return null;
	}
}
