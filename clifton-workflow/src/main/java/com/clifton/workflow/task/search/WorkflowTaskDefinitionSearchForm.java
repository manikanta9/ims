package com.clifton.workflow.task.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class WorkflowTaskDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "category.id")
	private Short categoryId;

	@SearchField(searchField = "workflow.id")
	private Short workflowId;

	@SearchField(searchField = "finalWorkflowStatus.id")
	private Short finalWorkflowStatusId;

	@SearchField(searchField = "taskGeneratorBean.id")
	private Integer taskGeneratorBeanId;

	@SearchField(searchField = "taskGeneratorBean.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean taskGeneratorBeanPresent;

	@SearchField(searchField = "taskGeneratorBean.type.id")
	private Integer taskGeneratorBeanTypeId;

	@SearchField
	private Integer generateDueDateDaysFromToday;

	@SearchField
	private Boolean generateOnEntityCreation;

	@SearchField(searchField = "linkedEntityTable.id")
	private Short linkedEntityTableId;

	@SearchField(searchField = "name", searchFieldPath = "linkedEntityTable", comparisonConditions = ComparisonConditions.EQUALS)
	private String linkedEntityTableName;

	@SearchField(searchField = "linkedEntityTable.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean linkedEntityTablePresent;

	@SearchField
	private String linkedEntityWindowClass;

	@SearchField
	private Long linkedEntityFkFieldId;

	@SearchField
	private String linkedEntityLabel;

	@SearchField(searchField = "linkedEntityFkFieldId", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean forSpecificEntity;

	@SearchField
	private String linkedEntityListURL;

	@SearchField(searchField = "unlockWorkflowStatus.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean locked;

	// custom filter based that uses EXISTS on definition's lock
	private String lockEntityTableName;

	// custom filter based that uses EXISTS on definition's lock or link
	private String linkedOrLockEntityTableName;

	@SearchField(searchField = "unlockWorkflowStatus.id")
	private Short unlockWorkflowStatusId;

	@SearchField(searchField = "taskEntityModifyCondition.id")
	private Integer taskEntityModifyConditionId;

	@SearchField(searchField = "assigneeUser.id")
	private Short assigneeUserId;

	@SearchField(searchField = "assigneeGroup.id")
	private Short assigneeGroupId;

	@SearchField
	private Boolean assigneeRequiredToBeInAssigneeGroup;

	@SearchField(searchField = "approverUser.id")
	private Short approverUserId;

	@SearchField(searchField = "secondaryApproverUser.id")
	private Short secondaryApproverUserId;

	@SearchField(searchField = "approverGroup.id")
	private Short approverGroupId;

	@SearchField
	private Boolean approverRequiredToBeInApproverGroup;

	@SearchField
	private Boolean sameAssigneeAndApproverAllowed;

	@SearchField(searchField = "priority.id")
	private Short priorityId;

	@SearchField
	private String taskNameTemplate;

	@SearchField
	private Boolean taskNameAllowed;

	@SearchField
	private Boolean taskNameRequired;

	@SearchField
	private Boolean taskDescriptionRequired;

	@SearchField
	private Boolean active;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public Short getWorkflowId() {
		return this.workflowId;
	}


	public void setWorkflowId(Short workflowId) {
		this.workflowId = workflowId;
	}


	public Integer getTaskGeneratorBeanId() {
		return this.taskGeneratorBeanId;
	}


	public void setTaskGeneratorBeanId(Integer taskGeneratorBeanId) {
		this.taskGeneratorBeanId = taskGeneratorBeanId;
	}


	public Boolean getTaskGeneratorBeanPresent() {
		return this.taskGeneratorBeanPresent;
	}


	public void setTaskGeneratorBeanPresent(Boolean taskGeneratorBeanPresent) {
		this.taskGeneratorBeanPresent = taskGeneratorBeanPresent;
	}


	public Integer getTaskGeneratorBeanTypeId() {
		return this.taskGeneratorBeanTypeId;
	}


	public void setTaskGeneratorBeanTypeId(Integer taskGeneratorBeanTypeId) {
		this.taskGeneratorBeanTypeId = taskGeneratorBeanTypeId;
	}


	public Integer getGenerateDueDateDaysFromToday() {
		return this.generateDueDateDaysFromToday;
	}


	public void setGenerateDueDateDaysFromToday(Integer generateDueDateDaysFromToday) {
		this.generateDueDateDaysFromToday = generateDueDateDaysFromToday;
	}


	public Boolean getGenerateOnEntityCreation() {
		return this.generateOnEntityCreation;
	}


	public void setGenerateOnEntityCreation(Boolean generateOnEntityCreation) {
		this.generateOnEntityCreation = generateOnEntityCreation;
	}


	public Short getLinkedEntityTableId() {
		return this.linkedEntityTableId;
	}


	public void setLinkedEntityTableId(Short linkedEntityTableId) {
		this.linkedEntityTableId = linkedEntityTableId;
	}


	public String getLinkedEntityTableName() {
		return this.linkedEntityTableName;
	}


	public void setLinkedEntityTableName(String linkedEntityTableName) {
		this.linkedEntityTableName = linkedEntityTableName;
	}


	public Boolean getLinkedEntityTablePresent() {
		return this.linkedEntityTablePresent;
	}


	public void setLinkedEntityTablePresent(Boolean linkedEntityTablePresent) {
		this.linkedEntityTablePresent = linkedEntityTablePresent;
	}


	public String getLinkedEntityWindowClass() {
		return this.linkedEntityWindowClass;
	}


	public void setLinkedEntityWindowClass(String linkedEntityWindowClass) {
		this.linkedEntityWindowClass = linkedEntityWindowClass;
	}


	public Long getLinkedEntityFkFieldId() {
		return this.linkedEntityFkFieldId;
	}


	public void setLinkedEntityFkFieldId(Long linkedEntityFkFieldId) {
		this.linkedEntityFkFieldId = linkedEntityFkFieldId;
	}


	public String getLinkedEntityLabel() {
		return this.linkedEntityLabel;
	}


	public void setLinkedEntityLabel(String linkedEntityLabel) {
		this.linkedEntityLabel = linkedEntityLabel;
	}


	public Boolean getForSpecificEntity() {
		return this.forSpecificEntity;
	}


	public void setForSpecificEntity(Boolean forSpecificEntity) {
		this.forSpecificEntity = forSpecificEntity;
	}


	public String getLinkedEntityListURL() {
		return this.linkedEntityListURL;
	}


	public void setLinkedEntityListURL(String linkedEntityListURL) {
		this.linkedEntityListURL = linkedEntityListURL;
	}


	public Short getUnlockWorkflowStatusId() {
		return this.unlockWorkflowStatusId;
	}


	public void setUnlockWorkflowStatusId(Short unlockWorkflowStatusId) {
		this.unlockWorkflowStatusId = unlockWorkflowStatusId;
	}


	public Boolean getLocked() {
		return this.locked;
	}


	public void setLocked(Boolean locked) {
		this.locked = locked;
	}


	public String getLockEntityTableName() {
		return this.lockEntityTableName;
	}


	public void setLockEntityTableName(String lockEntityTableName) {
		this.lockEntityTableName = lockEntityTableName;
	}


	public String getLinkedOrLockEntityTableName() {
		return this.linkedOrLockEntityTableName;
	}


	public void setLinkedOrLockEntityTableName(String linkedOrLockEntityTableName) {
		this.linkedOrLockEntityTableName = linkedOrLockEntityTableName;
	}


	public Short getFinalWorkflowStatusId() {
		return this.finalWorkflowStatusId;
	}


	public void setFinalWorkflowStatusId(Short finalWorkflowStatusId) {
		this.finalWorkflowStatusId = finalWorkflowStatusId;
	}


	public Integer getTaskEntityModifyConditionId() {
		return this.taskEntityModifyConditionId;
	}


	public void setTaskEntityModifyConditionId(Integer taskEntityModifyConditionId) {
		this.taskEntityModifyConditionId = taskEntityModifyConditionId;
	}


	public Short getAssigneeUserId() {
		return this.assigneeUserId;
	}


	public void setAssigneeUserId(Short assigneeUserId) {
		this.assigneeUserId = assigneeUserId;
	}


	public Short getAssigneeGroupId() {
		return this.assigneeGroupId;
	}


	public void setAssigneeGroupId(Short assigneeGroupId) {
		this.assigneeGroupId = assigneeGroupId;
	}


	public Boolean getAssigneeRequiredToBeInAssigneeGroup() {
		return this.assigneeRequiredToBeInAssigneeGroup;
	}


	public void setAssigneeRequiredToBeInAssigneeGroup(Boolean assigneeRequiredToBeInAssigneeGroup) {
		this.assigneeRequiredToBeInAssigneeGroup = assigneeRequiredToBeInAssigneeGroup;
	}


	public Short getApproverUserId() {
		return this.approverUserId;
	}


	public void setApproverUserId(Short approverUserId) {
		this.approverUserId = approverUserId;
	}


	public Short getSecondaryApproverUserId() {
		return this.secondaryApproverUserId;
	}


	public void setSecondaryApproverUserId(Short secondaryApproverUserId) {
		this.secondaryApproverUserId = secondaryApproverUserId;
	}


	public Short getApproverGroupId() {
		return this.approverGroupId;
	}


	public void setApproverGroupId(Short approverGroupId) {
		this.approverGroupId = approverGroupId;
	}


	public Boolean getApproverRequiredToBeInApproverGroup() {
		return this.approverRequiredToBeInApproverGroup;
	}


	public void setApproverRequiredToBeInApproverGroup(Boolean approverRequiredToBeInApproverGroup) {
		this.approverRequiredToBeInApproverGroup = approverRequiredToBeInApproverGroup;
	}


	public Boolean getSameAssigneeAndApproverAllowed() {
		return this.sameAssigneeAndApproverAllowed;
	}


	public void setSameAssigneeAndApproverAllowed(Boolean sameAssigneeAndApproverAllowed) {
		this.sameAssigneeAndApproverAllowed = sameAssigneeAndApproverAllowed;
	}


	public Short getPriorityId() {
		return this.priorityId;
	}


	public void setPriorityId(Short priorityId) {
		this.priorityId = priorityId;
	}


	public String getTaskNameTemplate() {
		return this.taskNameTemplate;
	}


	public void setTaskNameTemplate(String taskNameTemplate) {
		this.taskNameTemplate = taskNameTemplate;
	}


	public Boolean getTaskNameAllowed() {
		return this.taskNameAllowed;
	}


	public void setTaskNameAllowed(Boolean taskNameAllowed) {
		this.taskNameAllowed = taskNameAllowed;
	}


	public Boolean getTaskNameRequired() {
		return this.taskNameRequired;
	}


	public void setTaskNameRequired(Boolean taskNameRequired) {
		this.taskNameRequired = taskNameRequired;
	}


	public Boolean getTaskDescriptionRequired() {
		return this.taskDescriptionRequired;
	}


	public void setTaskDescriptionRequired(Boolean taskDescriptionRequired) {
		this.taskDescriptionRequired = taskDescriptionRequired;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}
}
