package com.clifton.workflow.task.template;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareObserver;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.generator.WorkflowTaskGenerator;
import com.clifton.workflow.task.template.search.WorkflowTaskDefinitionTemplateSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class WorkflowTaskDefinitionTemplateServiceImpl implements WorkflowTaskDefinitionTemplateService {

	private AdvancedUpdatableDAO<WorkflowTaskDefinitionTemplate, Criteria> workflowTaskDefinitionTemplateDAO;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemBeanService systemBeanService;
	private WorkflowTaskService workflowTaskService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTaskDefinitionTemplate getWorkflowTaskDefinitionTemplate(short id) {
		return getWorkflowTaskDefinitionTemplateDAO().findByPrimaryKey(id);
	}


	@Override
	public List<WorkflowTaskDefinitionTemplate> getWorkflowTaskDefinitionTemplateList(WorkflowTaskDefinitionTemplateSearchForm searchForm) {
		return getWorkflowTaskDefinitionTemplateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public WorkflowTaskDefinitionTemplate saveWorkflowTaskDefinitionTemplate(WorkflowTaskDefinitionTemplate template) {
		return getWorkflowTaskDefinitionTemplateDAO().save(template);
	}


	@Override
	public void deleteWorkflowTaskDefinitionTemplate(short id) {
		getWorkflowTaskDefinitionTemplateDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTaskDefinition createWorkflowTaskDefinitionFromTemplate(DefinitionCreationTemplate creationTemplate) {
		WorkflowTaskDefinitionTemplate template = getWorkflowTaskDefinitionTemplate(creationTemplate.getTemplateId());
		ValidationUtils.assertNotNull(template, "Cannot find Workflow Task Definition Template with id = " + creationTemplate.getTemplateId());

		// copy properties from template definition
		WorkflowTaskDefinition newDefinition = new WorkflowTaskDefinition();
		BeanUtils.copyPropertiesExceptAudit(template.getTemplateDefinition(), newDefinition);
		newDefinition.setId(null);

		// overwrite user input properties
		newDefinition.setName(creationTemplate.getName());
		newDefinition.setDescription(creationTemplate.getDescription());
		if (template.getTemplateDefinition().getLinkedEntityFkFieldId() != null) {
			ValidationUtils.assertNotNull(creationTemplate.getLinkedEntityFkFieldId(), "Linked entity is required because it is specified on the template.");
			newDefinition.setLinkedEntityFkFieldId(creationTemplate.getLinkedEntityFkFieldId());
			newDefinition.setLinkedEntityLabel(creationTemplate.getLinkedEntityLabel());
		}

		if (creationTemplate.getTaskGeneratorBeanId() != null) {
			ValidationUtils.assertNotNull(newDefinition.getTaskGeneratorBean(), "Cannot overwrite Task Generator Bean because it is not set on the template definition");
			SystemBean generatorBean = getSystemBeanService().getSystemBean(creationTemplate.getTaskGeneratorBeanId());
			ValidationUtils.assertNotNull(generatorBean, "Cannot find Task Generator Bean with id = " + creationTemplate.getTaskGeneratorBeanId());
			ValidationUtils.assertTrue(generatorBean.getType().getGroup().getInterfaceClassName().equals(WorkflowTaskGenerator.class.getName()), "Task Generator Bean must be an instance of WorkflowTaskGenerator interface.");
			newDefinition.setTaskGeneratorBean(generatorBean);
		}

		// validate user access to this template
		String errorMessage = getSystemConditionEvaluationHandler().getConditionFalseMessage(template.getTaskDefinitionCreateCondition(), newDefinition);
		if (!StringUtils.isEmpty(errorMessage)) {
			throw new AccessDeniedException("Cannot create Workflow Task Definition from Template: " + errorMessage);
		}

		// disable entity modify observer: security is already handled by this method
		return DaoUtils.executeWithSpecificObserversDisabled(() -> getWorkflowTaskService().saveWorkflowTaskDefinition(newDefinition), SystemEntityModifyConditionAwareObserver.class);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<WorkflowTaskDefinitionTemplate, Criteria> getWorkflowTaskDefinitionTemplateDAO() {
		return this.workflowTaskDefinitionTemplateDAO;
	}


	public void setWorkflowTaskDefinitionTemplateDAO(AdvancedUpdatableDAO<WorkflowTaskDefinitionTemplate, Criteria> workflowTaskDefinitionTemplateDAO) {
		this.workflowTaskDefinitionTemplateDAO = workflowTaskDefinitionTemplateDAO;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}
}
