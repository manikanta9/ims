package com.clifton.workflow.task.subscription;

import com.clifton.core.util.CollectionUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.search.WorkflowTaskSearchForm;
import org.springframework.stereotype.Controller;

import java.util.List;


@Controller
public class WorkflowTaskSubscriptionServiceImpl implements WorkflowTaskSubscriptionService {

	private WorkflowTaskService workflowTaskService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTaskSubscriptionMessage getWorkflowTaskSubscriptionMessageForUser(SecurityUser user) {
		WorkflowTaskSearchForm searchForm = new WorkflowTaskSearchForm();
		searchForm.setWaitingForAssigneeOrApproverUserId(user.getId());
		List<WorkflowTask> workflowTaskList = getWorkflowTaskService().getWorkflowTaskList(searchForm);
		return WorkflowTaskSubscriptionMessage.reset(CollectionUtils.getSize(workflowTaskList));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}
}
