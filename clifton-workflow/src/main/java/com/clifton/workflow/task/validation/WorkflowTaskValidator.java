package com.clifton.workflow.task.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUserService;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.search.WorkflowTaskSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The WorkflowTaskValidator class validates WorkflowTask integrity on insert, update and delete.
 * It does not allow completed tasks to be deleted or invalid state for a task to be saved (based on corresponding definition rules).
 *
 * @author vgomelsky
 */
@Component
public class WorkflowTaskValidator extends SelfRegisteringDaoValidator<WorkflowTask> {

	private SecurityUserService securityUserService;
	private WorkflowTaskService workflowTaskService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(WorkflowTask task, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			ValidationUtils.assertFalse(task.isCompletedTask(), "Cannot delete a Workflow Task that was completed.");
		}
		else {
			validateWorkflowTaskSave(task);

			if (config.isUpdate()) {
				WorkflowTask originalTask = getOriginalBean(task);

				if (originalTask == null) {
					throw new IllegalStateException("Cannot update Workflow Task " + task.getLabel() + " because the original Task cannot be found.");
				}

				ValidationUtils.assertEquals(task.getDefinition().getId(), originalTask.getDefinition().getId(), "Cannot change Task Definition for existing Task.", "definition");
				ValidationUtils.assertEquals(task.getAssigneeGroup(), originalTask.getAssigneeGroup(), "Cannot change Assignee User Group for existing Task.", "assigneeGroup");
				ValidationUtils.assertEquals(task.getApproverGroup(), originalTask.getApproverGroup(), "Cannot change Approver User Group for existing Task.", "approverGroup");
				if (DateUtils.compare(task.getOriginalDueDate(), originalTask.getOriginalDueDate(), false) != 0) {
					throw new FieldValidationException("Original Due Date cannot be changed.", "originalDueDate");
				}
			}
			else if (config.isInsert()) {
				ValidationUtils.assertEquals(task.getAssigneeGroup(), task.getDefinition().getAssigneeGroup(), "Assignee User Group for a new Task must be the same as this from corresponding Task Definition.", "assigneeGroup");
				ValidationUtils.assertEquals(task.getApproverGroup(), task.getDefinition().getApproverGroup(), "Approver User Group for a new Task must be the same as this from corresponding Task Definition", "approverGroup");
			}
		}
	}


	private void validateWorkflowTaskSave(WorkflowTask task) {
		WorkflowTaskDefinition definition = getWorkflowTaskService().getWorkflowTaskDefinition(task.getDefinition().getId());
		if (definition.getPriority() != null) {
			ValidationUtils.assertEquals(definition.getPriority(), task.getPriority(), "When Priority is defined on Task Definition, then Task Priority must be the same as this on the definition: " + definition.getPriority(), "priority");
		}
		ValidationUtils.assertNotNull(ObjectUtils.coalesce(task.getAssigneeUser(), task.getAssigneeGroup()), "Assignee User or Group is required for each Task.", "assigneeUser");
		if (!definition.isSameAssigneeAndApproverAllowed() && task.getAssigneeUser() != null && task.getApproverUser() != null) {
			ValidationUtils.assertNotEquals(task.getAssigneeUser(), task.getApproverUser(), "Task Assignee and Approver cannot be the same user for this Task Definition.", "assigneeUser");
		}
		if (task.getAssigneeUser() != null && definition.isAssigneeRequiredToBeInAssigneeGroup()) {
			ValidationUtils.assertNotNull(definition.getAssigneeGroup(), "Assignee Group on the Task Definition is required when 'Assignee is Required to be in Assignee Group' option is selected.");
			ValidationUtils.assertTrue(getSecurityUserService().isSecurityUserInGroup(task.getAssigneeUser().getId(), definition.getAssigneeGroup().getName()), "Task Assignee User must belong to Assignee Group from corresponding Task Definition.");
		}
		if (task.getApproverUser() != null && definition.isApproverRequiredToBeInApproverGroup()) {
			ValidationUtils.assertNotNull(definition.getApproverGroup(), "Approver Group on the Task Definition is required when 'Approver is Required to be in Approver Group' option is selected.");
			ValidationUtils.assertTrue(getSecurityUserService().isSecurityUserInGroup(task.getApproverUser().getId(), definition.getApproverGroup().getName()), "Task Approver User must belong to Approver Group from corresponding Task Definition.");
		}

		if (definition.getGenerateDueDateDaysFromToday() != null) {
			Date maxDate = DateUtils.clearTime(DateUtils.addDays(new Date(), definition.getGenerateDueDateDaysFromToday()));
			ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(maxDate, DateUtils.clearTime(task.getOriginalDueDate())), "Original Due Date for task [" + task + "] cannot be more than " + definition.getGenerateDueDateDaysFromToday() + " days in the future.", "dueDate");
		}

		if (!definition.isChangeToDueDateAllowed()) {
			ValidationUtils.assertEquals(task.getDueDate(), task.getOriginalDueDate(), "Original Due Date and Due Date must be the same for Tasks of Task Definitions that do not allow change to Due Date.", "dueDate");
		}

		if (!task.isNewBean()) {
			ValidationUtils.assertFalse(task.isCompletedTask(), "Cannot update a Workflow Task that was completed.");
			if (definition.isTaskDescriptionRequired()) {
				ValidationUtils.assertNotEmpty(task.getDescription(), "Description field is required for tasks of selected definition: " + definition.getName(), "description");
			}
		}

		if (!definition.isTaskNameAllowed()) {
			ValidationUtils.assertTrue(StringUtils.isEmpty(task.getName()), "Name field is not allowed for tasks of selected definition: " + definition.getName(), "name");
		}
		if (definition.isTaskNameRequired() && !(task.isNewBean() && definition.getTaskGeneratorBean() != null)) {
			ValidationUtils.assertNotEmpty(task.getName(), "Name field is required for tasks of selected definition: " + definition.getName(), "name");
		}

		if (definition.getLinkedEntityTable() != null) {
			ValidationUtils.assertNotNull(task.getLinkedEntityFkFieldId(), "Linked Entity ID must be defined on Workflow Task that has Linked Entity Table on the Task Definition.", "linkedEntityFkFieldId");
			ValidationUtils.assertNotNull(task.getLinkedEntityLabel(), "Linked Entity Label must be defined on Workflow Task that has Linked Entity Table on the Task Definition.", "linkedEntityLabel");

			if (definition.getLinkedEntityFkFieldId() != null) {
				ValidationUtils.assertEquals(task.getLinkedEntityFkFieldId(), definition.getLinkedEntityFkFieldId(), "Task Linked Entity ID must equals to this on the Task Definition", "linkedEntityFkFieldId");
				// NOTE the label may change overtime: do not require for it to be the same
			}

			// cannot have more than one "locking" task for the same definition/entity that is not in the final state
			WorkflowTaskSearchForm searchForm = new WorkflowTaskSearchForm();
			searchForm.setDefinitionId(definition.getId());
			searchForm.setLinkedEntityFkFieldId(task.getLinkedEntityFkFieldId());
			searchForm.setExcludeWorkflowStatusId(definition.getFinalWorkflowStatus().getId());
			searchForm.setLocked(true);
			List<WorkflowTask> existingList = getWorkflowTaskService().getWorkflowTaskList(searchForm);
			if (!CollectionUtils.isEmpty(existingList)) {
				WorkflowTask existingTask = CollectionUtils.getOnlyElementStrict(existingList);
				if (!existingTask.equals(task)) {
					throw new ValidationException("Cannot have more than one non Closed locking task for the same entity and the same task definition.");
				}
			}
		}
		else {
			ValidationUtils.assertNull(task.getLinkedEntityFkFieldId(), "Linked Entity cannot be defined on Workflow Task that has no Linked Entity Table on the Task Definition.");
			if (definition.getTaskGeneratorBean() != null) {
				// cannot have more than one task for the same definition on the same date
				WorkflowTaskSearchForm searchForm = new WorkflowTaskSearchForm();
				searchForm.setDefinitionId(definition.getId());
				searchForm.setLinkedEntityFkFieldId(task.getLinkedEntityFkFieldId());
				searchForm.setOriginalDueDate(task.getOriginalDueDate());
				List<WorkflowTask> existingList = getWorkflowTaskService().getWorkflowTaskList(searchForm);
				if (!CollectionUtils.isEmpty(existingList)) {
					WorkflowTask existingTask = CollectionUtils.getOnlyElementStrict(existingList);
					if (!existingTask.equals(task)) {
						throw new ValidationException("Cannot have more than one auto-generated task due on the same Original Due Date for the same task definition: " + definition);
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}
}
