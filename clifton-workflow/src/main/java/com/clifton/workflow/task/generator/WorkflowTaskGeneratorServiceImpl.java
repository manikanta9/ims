package com.clifton.workflow.task.generator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.search.WorkflowTaskSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class WorkflowTaskGeneratorServiceImpl implements WorkflowTaskGeneratorService {

	private SystemBeanService systemBeanService;
	private WorkflowTaskService workflowTaskService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public List<WorkflowTask> generateWorkflowTaskList(TaskGeneratorCommand command) {
		WorkflowTaskDefinition definition = getWorkflowTaskService().getWorkflowTaskDefinition(command.getTaskDefinitionId());
		ValidationUtils.assertNotNull(definition, "Cannot find workflow task definition with id = " + command.getTaskDefinitionId());

		SystemBean generatorBean = definition.getTaskGeneratorBean();
		ValidationUtils.assertNotNull(generatorBean, "Task Generator Bean is not defined for workflow task definition: " + definition);

		WorkflowTaskGenerator taskGenerator = (WorkflowTaskGenerator) getSystemBeanService().getBeanInstance(generatorBean);

		List<WorkflowTask> taskList = taskGenerator.generate(definition, command.getFromDate(), command.getToDate());

		// skip tasks that already exist
		List<WorkflowTask> result = new ArrayList<>();
		for (WorkflowTask task : CollectionUtils.getIterable(taskList)) {
			WorkflowTaskSearchForm searchForm = new WorkflowTaskSearchForm();
			searchForm.setDefinitionId(definition.getId());
			searchForm.setOriginalDueDate(task.getOriginalDueDate()); // use Original Due Date which cannot be changed
			searchForm.setLinkedEntityFkFieldId(task.getLinkedEntityFkFieldId());
			List<WorkflowTask> existingList = getWorkflowTaskService().getWorkflowTaskList(searchForm);
			if (CollectionUtils.isEmpty(existingList)) {
				result.add(task);
			}
		}

		if (!command.isPreview() && !CollectionUtils.isEmpty(result)) {
			// save results in the database and return newly created objects
			for (int i = 0; i < result.size(); i++) {
				WorkflowTask task = result.get(i);
				task = getWorkflowTaskService().saveWorkflowTask(task);
				result.set(i, task);
			}
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}
}
