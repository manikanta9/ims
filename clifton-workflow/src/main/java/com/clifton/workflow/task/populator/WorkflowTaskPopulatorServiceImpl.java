package com.clifton.workflow.task.populator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.WorkflowTaskDefinitionLock;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.WorkflowTaskUtils;
import com.clifton.workflow.task.populator.approver.WorkflowTaskApproverRetriever;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author vgomelsky
 */
@Service
public class WorkflowTaskPopulatorServiceImpl implements WorkflowTaskPopulatorService {

	private ContextHandler contextHandler;
	private DaoLocator daoLocator;
	private SystemBeanService systemBeanService;
	private TemplateConverter templateConverter;
	private WorkflowTaskService workflowTaskService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTask populateWorkflowTaskForDefinition(short taskDefinitionId, Integer linkedEntityId) {
		WorkflowTaskDefinition definition = getWorkflowTaskService().getWorkflowTaskDefinition(taskDefinitionId);
		ValidationUtils.assertNotNull(definition, "Cannot find Workflow Task Definition with id = " + taskDefinitionId);

		IdentityObject linkedEntity = null;
		if (linkedEntityId != null) {
			SystemTable linkedTable = definition.getLinkedEntityTable();
			ValidationUtils.assertNotNull(linkedTable, "Linked Table is required for Workflow Task Definition with id = " + taskDefinitionId);

			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(linkedTable.getName());
			linkedEntity = dao.findByPrimaryKey(linkedEntityId);
			ValidationUtils.assertNotNull(linkedEntity, "Cannot find linked entity Workflow Task Definition " + definition + " with id = " + linkedEntityId);
		}

		return populateFromDefinitionForEntity(definition, linkedEntity);
	}


	@Override
	public WorkflowTask populateWorkflowTaskForDefinitionForLockEntity(short taskDefinitionId, int lockEntityId, String lockEntityTableName) {
		WorkflowTaskDefinition definition = getWorkflowTaskService().getWorkflowTaskDefinition(taskDefinitionId);
		ValidationUtils.assertNotNull(definition, "Cannot find Workflow Task Definition with id = " + taskDefinitionId);

		SystemTable linkedTable = definition.getLinkedEntityTable();
		ValidationUtils.assertNotNull(linkedTable, "Linked Table is required for Workflow Task Definition with id = " + taskDefinitionId);

		IdentityObject lockEntity = getLockEntity(lockEntityTableName, lockEntityId);

		WorkflowTaskDefinitionLock lock = definition.getTaskDefinitionLock(lockEntityTableName);
		ValidationUtils.assertNotNull(lock, "Cannot find lock for " + definition + " task definition and lock table = " + lockEntityTableName);

		IdentityObject linkedEntity = WorkflowTaskUtils.getLinkedEntityForLockedEntity(lockEntity, lock);
		return populateFromDefinitionForEntity(definition, linkedEntity);
	}


	@Override
	public WorkflowTask populateFromDefinitionForEntity(WorkflowTaskDefinition definition, IdentityObject linkedEntity) {
		return populateFromDefinitionForEntityForDueDate(definition, linkedEntity, null);
	}


	@Override
	public WorkflowTask populateFromDefinitionForEntityForDueDate(WorkflowTaskDefinition definition, IdentityObject linkedEntity, Date dueDate) {
		WorkflowTask task = new WorkflowTask();
		task.setDefinition(definition);
		task.setOriginalDueDate((dueDate != null) ? dueDate : DateUtils.addDays(DateUtils.clearTime(new Date()), ObjectUtils.coalesce(definition.getGenerateDueDateDaysFromToday(), 0)));
		task.setDueDate(task.getOriginalDueDate());
		task.setPriority(definition.getPriority());
		task.setLinkedEntityFkFieldId(definition.getLinkedEntityFkFieldId());
		task.setLinkedEntityLabel(definition.getLinkedEntityLabel());

		task.setAssigneeUser(definition.getAssigneeUser());
		if (task.getAssigneeUser() == null && definition.getTaskGeneratorBean() == null) {
			// do not set current user for auto-generated (scheduled) tasks
			task.setAssigneeUser((SecurityUser) getContextHandler().getBean(ContextHandler.USER_BEAN_NAME));
		}
		task.setAssigneeGroup(definition.getAssigneeGroup());

		populateTaskApprover(task, linkedEntity);

		if (linkedEntity != null) {
			populateLinkedEntity(task, linkedEntity);
		}
		populateTaskNameFromTemplate(task, linkedEntity);

		return task;
	}


	@Override
	public void populateLinkedEntity(WorkflowTask task, IdentityObject linkedEntity) {
		task.setLinkedEntityFkFieldId(BeanUtils.getIdentityAsLong(linkedEntity));
		task.setLinkedEntityLabel(BeanUtils.getLabel(linkedEntity));
	}


	private void populateTaskApprover(WorkflowTask task, IdentityObject linkedEntity) {
		WorkflowTaskDefinition definition = task.getDefinition();

		SecurityUser approverUser = null;
		if (definition.getApproverUserRetrieverBean() != null) {
			WorkflowTaskApproverRetriever approverRetriever = (WorkflowTaskApproverRetriever) getSystemBeanService().getBeanInstance(definition.getApproverUserRetrieverBean());
			approverUser = approverRetriever.getWorkflowTaskApprover(definition, linkedEntity);
		}

		if (approverUser == null || approverUser.equals(task.getAssigneeUser())) {
			approverUser = definition.getApproverUser();
			if (approverUser == null || approverUser.equals(task.getAssigneeUser())) {
				approverUser = definition.getSecondaryApproverUser();
			}
		}
		task.setApproverUser(approverUser);
		task.setApproverGroup(definition.getApproverGroup());
	}


	private void populateTaskNameFromTemplate(WorkflowTask task, IdentityObject linkedEntity) {
		String nameTemplate = task.getDefinition().getTaskNameTemplate();
		if (StringUtils.isEmpty(task.getName()) && !StringUtils.isEmpty(nameTemplate)) {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("task", task);
			if (linkedEntity != null) {
				parameters.put("linkedEntity", linkedEntity);
			}
			String name = getTemplateConverter().convert(TemplateConfig.ofTemplateWithUtilsClasses(nameTemplate, parameters));
			task.setName(name);
		}
	}


	@Override
	public IdentityObject getLockEntity(String lockEntityTableName, int lockEntityId) {
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(lockEntityTableName);
		IdentityObject lockEntity = dao.findByPrimaryKey(lockEntityId);
		ValidationUtils.assertNotNull(lockEntity, "Cannot find lock entity id = " + lockEntityId + " and table = " + lockEntityTableName);
		return lockEntity;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}
}
