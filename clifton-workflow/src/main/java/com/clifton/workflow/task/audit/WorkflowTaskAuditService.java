package com.clifton.workflow.task.audit;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.audit.SystemAuditEvent;


/**
 * The WorkflowTaskAuditService interface defines methods for retrieving {@link com.clifton.system.audit.SystemAuditEvent}
 * information for all entities locked by a specific {@link com.clifton.workflow.task.WorkflowTask}.
 *
 * @author vgomelsky
 */
public interface WorkflowTaskAuditService {

	/**
	 * Retrieves audit event information for each entity locked by corresponding {@link com.clifton.workflow.task.WorkflowTaskDefinition}.
	 * Combines results into a single {@link DataTable} before returning them (more than one table can be locked by a single task definition).
	 *
	 * @param workflowTaskId
	 * @param restrictTime   if true, will only return audit events that were recorded after this task was created
	 */
	@SecureMethod(dtoClass = SystemAuditEvent.class)
	public DataTable getWorkflowTaskAuditEventList(int workflowTaskId, boolean restrictTime);
}
