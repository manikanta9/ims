package com.clifton.workflow.task.step.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


/**
 * @author vgomelsky
 */
public class WorkflowTaskStepDefinitionSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "taskDefinition.id")
	private Short taskDefinitionId;

	@SearchField(searchField = "linkedEntityTable.id")
	private Short linkedEntityTableId;

	@SearchField(searchField = "linkedEntityTable.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean linkedEntityTablePresent;

	@SearchField
	private String linkedEntityListURL;

	@SearchField(searchField = "assigneeUser.id")
	private Short assigneeUserId;

	@SearchField
	private Short order;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getTaskDefinitionId() {
		return this.taskDefinitionId;
	}


	public void setTaskDefinitionId(Short taskDefinitionId) {
		this.taskDefinitionId = taskDefinitionId;
	}


	public Short getOrder() {
		return this.order;
	}


	public void setOrder(Short order) {
		this.order = order;
	}


	public Short getLinkedEntityTableId() {
		return this.linkedEntityTableId;
	}


	public void setLinkedEntityTableId(Short linkedEntityTableId) {
		this.linkedEntityTableId = linkedEntityTableId;
	}


	public Boolean getLinkedEntityTablePresent() {
		return this.linkedEntityTablePresent;
	}


	public void setLinkedEntityTablePresent(Boolean linkedEntityTablePresent) {
		this.linkedEntityTablePresent = linkedEntityTablePresent;
	}


	public String getLinkedEntityListURL() {
		return this.linkedEntityListURL;
	}


	public void setLinkedEntityListURL(String linkedEntityListURL) {
		this.linkedEntityListURL = linkedEntityListURL;
	}


	public Short getAssigneeUserId() {
		return this.assigneeUserId;
	}


	public void setAssigneeUserId(Short assigneeUserId) {
		this.assigneeUserId = assigneeUserId;
	}

}
