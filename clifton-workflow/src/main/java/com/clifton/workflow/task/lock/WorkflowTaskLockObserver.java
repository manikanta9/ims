package com.clifton.workflow.task.lock;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.WorkflowTaskDefinitionLock;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.WorkflowTaskUtils;
import com.clifton.workflow.task.populator.WorkflowTaskPopulatorService;
import com.clifton.workflow.task.search.WorkflowTaskSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * The WorkflowTaskLockObserver class is a DAO observer that is automatically registered for DAO's with entities
 * that should be locked based on corresponding {@link com.clifton.workflow.task.WorkflowTaskDefinition}.
 * Locking means that the entity cannot be modified unless there is a corresponding tasks in a workflow status
 * that allows edits to the entity or a sub-set of entity fields.
 * <p>
 * These observers are automatically registered/unregistered when changes are made to corresponding workflow task definitions.
 * These observers are automatically loaded for corresponding workflow task definitions on application server start ups.
 *
 * @author vgomelsky
 */
@Component
public class WorkflowTaskLockObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private static final String SCHEDULED_TASK_LIST = "WORKFLOW_TASK_LIST_SCHEDULED_FOR_CREATION";
	private static final String TASK_AUTO_CREATE_EVENT_KEY = "workflowTasksAutoCreated";


	private SecurityAuthorizationService securityAuthorizationService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private WorkflowTaskPopulatorService workflowTaskPopulatorService;
	private WorkflowTaskService workflowTaskService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskLockObserver() {
		super();
		setOrder(-900); // run this observer earlier (field auditing is -1000)
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		List<WorkflowTaskDefinitionLock> lockList = getLocksForEntity(dao.getConfiguration().getTableName(), bean, event);

		if (event.isInsert()) {
			// on insert, some definitions may auto-generate new WorkflowTask objects
			// need to delay WorkflowTask creation until after the linked entity is created so that the task is linked to new id
			scheduleNewTaskCreationOnInsert(lockList, bean);
		}

		if (!CollectionUtils.isEmpty(lockList)) {
			List<WorkflowTask> taskList = getOpenTasksForEntity(lockList, bean);

			if (CollectionUtils.isEmpty(taskList)) {
				throw newWorkflowTaskLockValidationException(event, bean, null);
			}

			WorkflowTask allowTask = null;
			for (WorkflowTask task : taskList) {
				WorkflowTaskDefinition definition = task.getDefinition();
				// check that task's workflow state allows edits
				if (task.getWorkflowStatus().equals(definition.getUnlockWorkflowStatus())) {
					boolean allowed = true;
					// check if current user is the assignee of the task
					if (task.getAssigneeUser() != null) {
						SecurityUser currentUser = (SecurityUser) getContextHandler().getBean(ContextHandler.USER_BEAN_NAME);
						allowed = (task.getAssigneeUser().equals(currentUser));
					}

					// check any additional restrictions (fields that can be modified, etc.)
					if (allowed) {
						WorkflowTaskDefinitionLock definitionLock = null;
						for (WorkflowTaskDefinitionLock lock : lockList) {
							if (definition.equals(lock.getDefinition())) {
								definitionLock = lock;
								break;
							}
						}
						if (definitionLock == null) {
							throw new java.lang.IllegalStateException("Cannot find Lock for " + definition);
						}
						else if (definitionLock.getLockEntityModifyCondition() != null) {
							allowed = getSystemConditionEvaluationHandler().isConditionTrue(definitionLock.getLockEntityModifyCondition(), bean);
						}
					}

					if (allowed) {
						allowTask = task;
						break;
					}
				}
			}

			if (allowTask == null) {
				if (CollectionUtils.isEmpty(taskList)) {
					throw newWorkflowTaskLockValidationException(event, bean, null);
				}
				throw new ValidationException("Cannot " + event + " [" + BeanUtils.getLabel(bean) + "] because none of currently open Workflow Tasks for this entity allows this: " + taskList);
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (event.isInsert()) {
			if (e == null) {
				// create scheduled tasks, if any, now that we have bean created
				@SuppressWarnings("unchecked")
				List<WorkflowTask> taskList = (List<WorkflowTask>) getContextHandler().getBean(SCHEDULED_TASK_LIST);
				if (!CollectionUtils.isEmpty(taskList)) {
					// also signal any interested parties (like UI) that a new task was created
					@SuppressWarnings("unchecked")
					MultiValueMap<String, Object> eventMap = (MultiValueMap<String, Object>) getContextHandler().getBean(ContextHandler.APPLICATION_EVENTS_KEY);
					if (eventMap == null) {
						eventMap = new MultiValueHashMap<>(false);
						getContextHandler().setBean(ContextHandler.APPLICATION_EVENTS_KEY, eventMap);
					}

					for (WorkflowTask task : taskList) {
						if (task.getDefinition().getLinkedEntityTable().getName().equals(ContextConventionUtils.getTableNameFromClass(bean.getClass()))) {
							// only need to re-populate if linked to the bean itself that was just inserted
							// otherwise locked entity was inserted that is linked to an existing bean
							getWorkflowTaskPopulatorService().populateLinkedEntity(task, bean);
						}

						task = getWorkflowTaskService().createWorkflowTaskAfterLockedEntityInsert(task);
						eventMap.put(TASK_AUTO_CREATE_EVENT_KEY, task.getId());
					}
				}
			}
			getContextHandler().removeBean(SCHEDULED_TASK_LIST);
		}
	}


	/**
	 * Processes the specified lock list and removes each lock when task creation is scheduled.
	 * For these definition, where configured, schedule creation of new WorkflowTask objects (need to wait until after the bean was created so that we have id).
	 */
	private void scheduleNewTaskCreationOnInsert(List<WorkflowTaskDefinitionLock> lockList, T bean) {
		List<WorkflowTask> taskList = null;

		boolean unlockTaskFound = false;
		WorkflowTaskLockValidationException notFoundException = null;
		int skippedEntitiesCount = 0;
		for (Iterator<WorkflowTaskDefinitionLock> iterator = lockList.iterator(); iterator.hasNext(); ) {
			WorkflowTaskDefinitionLock lock = iterator.next();
			WorkflowTaskDefinition definition = lock.getDefinition();
			IdentityObject linkedEntity = WorkflowTaskUtils.getLinkedEntityForLockedEntity(bean, lock);

			if (definition.getTaskEntityModifyCondition() != null) {
				// do not apply this to Admin users: can see all definitions similar to Entity Modify Condition logic
				if (!getSecurityAuthorizationService().isSecurityUserAdmin()) {
					// most conditions are security based (current user is a member of specific group)
					WorkflowTask task = getWorkflowTaskPopulatorService().populateFromDefinitionForEntity(definition, linkedEntity);
					if (!getSystemConditionEvaluationHandler().isConditionTrue(definition.getTaskEntityModifyCondition(), task)) {
						skippedEntitiesCount++;
						continue; // skip definition that the user cannot modify
					}
				}
			}

			if (linkedEntity.isNewBean()) {
				if (definition.isGenerateOnEntityCreation()) {
					// schedule task creation for new entity
					if (taskList == null) {
						taskList = new ArrayList<>();
					}
					taskList.add(getWorkflowTaskPopulatorService().populateFromDefinitionForEntity(definition, linkedEntity));
					iterator.remove();
				}
			}
			else {
				// when linked entity is different from lock entity (already exists), first check if it already has an open task
				WorkflowTaskSearchForm searchForm = new WorkflowTaskSearchForm();
				searchForm.setDefinitionId(definition.getId());
				searchForm.setLinkedEntityFkFieldId(BeanUtils.getIdentityAsLong(linkedEntity));
				searchForm.setExcludeWorkflowStatusId(definition.getFinalWorkflowStatus().getId());
				List<WorkflowTask> existingTaskList = getWorkflowTaskService().getWorkflowTaskList(searchForm);
				if (CollectionUtils.isEmpty(existingTaskList)) {
					// no open tasks: throw a special validation exception offering to create the task
					notFoundException = newWorkflowTaskLockValidationException(DaoEventTypes.INSERT, bean, linkedEntity);
				}
				else {
					for (WorkflowTask task : CollectionUtils.getIterable(existingTaskList)) {
						// check if task's workflow state allows edits
						if (!task.getWorkflowStatus().equals(definition.getUnlockWorkflowStatus())) {
							throw new ValidationException("Cannot create [" + BeanUtils.getLabel(bean) + "] because there is an open task linked to it that is currently locked: " + task);
						}
						// check if current user is the assignee of the task
						if (task.getAssigneeUser() != null) {
							SecurityUser currentUser = (SecurityUser) getContextHandler().getBean(ContextHandler.USER_BEAN_NAME);
							if (!task.getAssigneeUser().equals(currentUser)) {
								throw new ValidationException("Cannot create [" + BeanUtils.getLabel(bean) + "] because there is an open task linked to it that is assigned to a different user: " + task);
							}
						}
					}
					unlockTaskFound = true;
					break;
				}
			}
		}


		if (!unlockTaskFound) {
			if (!CollectionUtils.isEmpty(taskList)) {
				getContextHandler().setBean(SCHEDULED_TASK_LIST, taskList);
				// remove remaining locks, if any, because we found and scheduled unlocking task(s)
				lockList.clear();
			}
			else if (notFoundException != null) {
				// throw exception only if no tasks can be scheduled
				throw notFoundException;
			}
			else if (skippedEntitiesCount > 0 && CollectionUtils.getSize(lockList) == skippedEntitiesCount) {
				// all locks were skipped because entity modify condition for each task definition does not allow this
				if (skippedEntitiesCount == 1) {
					WorkflowTaskDefinition definition = lockList.get(0).getDefinition();
					throw new ValidationException("Cannot create [" + BeanUtils.getLabel(bean) +
							"] because it is locked by [" + definition.getLabel() +
							"] Workflow Task Definition and Task Entity Modify Condition [" + definition.getTaskEntityModifyCondition().getLabel() +
							"] prevents you from doing this.");
				}
				throw new ValidationException("Cannot create [" + BeanUtils.getLabel(bean) +
						"] because it is locked by Workflow Task Definition(s) with Task Entity Modify Condition(s) that prevent you from doing this.");
			}
		}
	}


	/**
	 * Returns a List of locks applicable to the specified entity: limits it to active definitions and matches on scope.
	 */
	private List<WorkflowTaskDefinitionLock> getLocksForEntity(String lockedTableName, T lockedBean, DaoEventTypes event) {
		List<WorkflowTaskDefinitionLock> result = new ArrayList<>();

		// filter active based on scope
		List<WorkflowTaskDefinitionLock> lockList = getWorkflowTaskService().getWorkflowTaskDefinitionLockListForTable(lockedTableName);
		for (WorkflowTaskDefinitionLock lock : CollectionUtils.getIterable(lockList)) {
			if (lock.getDefinition().isActive()) {
				if (lock.getEventScope() != null && !lock.getEventScope().isSuperSetOf(event)) {
					continue;  // skip locks when event is not part of event scope
				}
				if (lock.getLockScopeCondition() != null) {
					if (!getSystemConditionEvaluationHandler().isConditionTrue(lock.getLockScopeCondition(), lockedBean)) {
						continue;// skip definition for out out scope entity
					}
				}
				result.add(lock);
			}
		}

		return result;
	}


	private List<WorkflowTask> getOpenTasksForEntity(List<WorkflowTaskDefinitionLock> lockList, T lockedBean) {
		List<WorkflowTask> result = null;

		boolean found = false;
		for (WorkflowTaskDefinitionLock lock : CollectionUtils.getIterable(lockList)) {
			// get not closed tasks for the definition that match bean's field using definition's path
			Number linkedEntityId = null;
			for (String propertyPath : lock.getLinkedEntityBeanFieldPaths()) {
				linkedEntityId = (Number) BeanUtils.getPropertyValue(lockedBean, propertyPath);
				if (linkedEntityId != null) {
					found = true;
					break;
				}
			}
			if (linkedEntityId != null) {
				WorkflowTaskSearchForm taskSearchForm = new WorkflowTaskSearchForm();
				taskSearchForm.setDefinitionId(lock.getDefinition().getId());
				taskSearchForm.setLinkedEntityFkFieldId(linkedEntityId.longValue());
				taskSearchForm.setExcludeWorkflowStatusId(lock.getDefinition().getFinalWorkflowStatus().getId());
				List<WorkflowTask> taskList = getWorkflowTaskService().getWorkflowTaskList(taskSearchForm);

				if (result == null) {
					result = taskList;
				}
				else if (taskList != null) {
					result.addAll(taskList);
				}
			}
		}

		if (!found) {
			throw new ValidationException("Cannot find linked entity for Workflow Task Definition Locks " + lockList + " for locked bean " + lockedBean);
		}

		return result;
	}


	private WorkflowTaskLockValidationException newWorkflowTaskLockValidationException(DaoEventTypes event, IdentityObject lockedEntity, IdentityObject linkedEntity) {
		return new WorkflowTaskLockValidationException("Cannot " + event + " [" + BeanUtils.getLabel(lockedEntity) + "] because there are no open Workflow Tasks that unlock this entity.", lockedEntity, linkedEntity, event);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public WorkflowTaskPopulatorService getWorkflowTaskPopulatorService() {
		return this.workflowTaskPopulatorService;
	}


	public void setWorkflowTaskPopulatorService(WorkflowTaskPopulatorService workflowTaskPopulatorService) {
		this.workflowTaskPopulatorService = workflowTaskPopulatorService;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}
}
