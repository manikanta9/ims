package com.clifton.workflow.task.subscription;

import com.clifton.workflow.task.WorkflowTask;


/**
 * The message DTO for sending subscription information regarding {@link WorkflowTask} changes.
 *
 * @author MikeH
 * @see WorkflowTaskSubscriptionService
 */
public class WorkflowTaskSubscriptionMessage {

	/**
	 * A static message indicating that the number of items has been decremented.
	 */
	public static final WorkflowTaskSubscriptionMessage DECREMENT = WorkflowTaskSubscriptionMessage.add(-1);
	/**
	 * A static message indicating that the number of items has been incremented.
	 */
	public static final WorkflowTaskSubscriptionMessage INCREMENT = WorkflowTaskSubscriptionMessage.add(1);

	/**
	 * If {@code true}, this message indicates that the client-side data should be reset before processing the remainder of this message.
	 */
	private final boolean reset;
	/**
	 * The number of workflow tasks added.
	 */
	private final int numAdded;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskSubscriptionMessage(boolean reset, int numAdded) {
		this.reset = reset;
		this.numAdded = numAdded;
	}


	public static WorkflowTaskSubscriptionMessage reset(int numAdded) {
		return new WorkflowTaskSubscriptionMessage(true, numAdded);
	}


	public static WorkflowTaskSubscriptionMessage add(int numAdded) {
		return new WorkflowTaskSubscriptionMessage(false, numAdded);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getNumAdded() {
		return this.numAdded;
	}


	public boolean isReset() {
		return this.reset;
	}
}
