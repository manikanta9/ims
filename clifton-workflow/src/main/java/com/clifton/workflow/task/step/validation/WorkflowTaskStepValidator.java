package com.clifton.workflow.task.step.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.task.step.WorkflowTaskStep;
import com.clifton.workflow.task.step.WorkflowTaskStepDefinition;
import com.clifton.workflow.task.step.WorkflowTaskStepService;
import org.springframework.stereotype.Component;


/**
 * The WorkflowTaskStepValidator class validates WorkflowTaskStep integrity on insert, update and delete.
 * It does not allow steps for completed tasks to be deleted or invalid state for a task step to be saved (based on corresponding definition rules).
 *
 * @author vgomelsky
 */
@Component
public class WorkflowTaskStepValidator extends SelfRegisteringDaoValidator<WorkflowTaskStep> {

	private WorkflowTaskStepService workflowTaskStepService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(WorkflowTaskStep step, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			ValidationUtils.assertFalse(step.getTask().isCompletedTask(), "Cannot delete a Workflow Task Step for a completed Task.");
		}
		else {
			WorkflowTaskStepDefinition definition = getWorkflowTaskStepService().getWorkflowTaskStepDefinition(step.getStepDefinition().getId());
			ValidationUtils.assertNotNull(definition, "Cannot find Workflow Step Definition for " + step);

			if (!step.isNewBean()) {
				ValidationUtils.assertFalse(step.getTask().isCompletedTask(), "Cannot update Task Step for a completed Workflow Task.");
			}

			if (definition.isStepNameRequired()) {
				ValidationUtils.assertNotEmpty(step.getName(), "Name field is required for steps of selected definition: " + definition.getName(), "name");
			}

			if (!definition.isChangeToDueDateAllowed()) {
				ValidationUtils.assertEquals(step.getDueDate(), step.getOriginalDueDate(), "Original Due Date and Due Date must be the same for Tasks of Step Definitions that do not allow change to Due Date.", "dueDate");
			}

			if (config.isUpdate()) {
				WorkflowTaskStep originalStep = getOriginalBean(step);

				if (originalStep == null) {
					throw new IllegalStateException("Cannot update Workflow Task Step " + step.getLabel() + " because the original Task Step cannot be found.");
				}

				ValidationUtils.assertEquals(step.getStepDefinition().getId(), originalStep.getStepDefinition().getId(), "Cannot change Task Step Definition for existing Step.", "stepDefinition");
				ValidationUtils.assertEquals(step.getTask().getId(), originalStep.getTask().getId(), "Cannot change Task for existing Step.", "task");
				if (DateUtils.compare(step.getOriginalDueDate(), originalStep.getOriginalDueDate(), false) != 0) {
					throw new FieldValidationException("Original Due Date cannot be changed.", "originalDueDate");
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskStepService getWorkflowTaskStepService() {
		return this.workflowTaskStepService;
	}


	public void setWorkflowTaskStepService(WorkflowTaskStepService workflowTaskStepService) {
		this.workflowTaskStepService = workflowTaskStepService;
	}
}
