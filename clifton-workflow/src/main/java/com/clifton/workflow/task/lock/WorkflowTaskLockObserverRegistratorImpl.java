package com.clifton.workflow.task.lock;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.workflow.task.WorkflowTaskDefinitionLock;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.search.WorkflowTaskDefinitionLockSearchForm;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class WorkflowTaskLockObserverRegistratorImpl implements WorkflowTaskLockObserverRegistrator, CurrentContextApplicationListener<ContextRefreshedEvent> {

	private DaoLocator daoLocator;


	private WorkflowTaskLockObserver<IdentityObject> workflowTaskLockObserver;
	private WorkflowTaskService workflowTaskService;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			// get active task definition locks
			WorkflowTaskDefinitionLockSearchForm searchForm = new WorkflowTaskDefinitionLockSearchForm();
			searchForm.setActive(true);
			List<WorkflowTaskDefinitionLock> lockList = getWorkflowTaskService().getWorkflowTaskDefinitionLockList(searchForm);

			// get corresponding DAO and register observer
			for (WorkflowTaskDefinitionLock lock : CollectionUtils.getIterable(lockList)) {
				validateLock(lock);
				registerLockObserver(lock.getLockEntityTable().getName());
			}
		}
		catch (Throwable e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error registering DAO Workflow Task Lock Observers: " + event, e);
		}
	}


	private void validateLock(WorkflowTaskDefinitionLock lock) {
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(lock.getLockEntityTable().getName());
		Class<?> beanClass = dao.getConfiguration().getBeanClass();
		for (String propertyPath : lock.getLinkedEntityBeanFieldPaths()) {
			if (!BeanUtils.isPropertyPresent(beanClass, propertyPath)) {
				// just log this warning and register the lock anyway so that the user knows when it fails
				LogUtils.error(getClass(), "Invalid Workflow Task Definition Lock " + lock + " table: " + dao.getConfiguration().getTableName() + " path: " + propertyPath);
			}
		}
	}


	@Override
	public void registerLockObserver(String tableName) {
		// get DAO that's being locked
		ObserverableDAO<IdentityObject> dao = (ObserverableDAO<IdentityObject>) getDaoLocator().locate(tableName);

		dao.registerEventObserver(DaoEventTypes.INSERT, getWorkflowTaskLockObserver());
		dao.registerEventObserver(DaoEventTypes.UPDATE, getWorkflowTaskLockObserver());
		dao.registerEventObserver(DaoEventTypes.DELETE, getWorkflowTaskLockObserver());
	}


	@Override
	public void unregisterLockObserver(String tableName) {
		// get DAO that's being unlocked
		ObserverableDAO<IdentityObject> dao = (ObserverableDAO<IdentityObject>) getDaoLocator().locate(tableName);

		dao.unregisterEventObserver(DaoEventTypes.INSERT, getWorkflowTaskLockObserver());
		dao.unregisterEventObserver(DaoEventTypes.UPDATE, getWorkflowTaskLockObserver());
		dao.unregisterEventObserver(DaoEventTypes.DELETE, getWorkflowTaskLockObserver());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskLockObserver<IdentityObject> getWorkflowTaskLockObserver() {
		return this.workflowTaskLockObserver;
	}


	public void setWorkflowTaskLockObserver(WorkflowTaskLockObserver<IdentityObject> workflowTaskLockObserver) {
		this.workflowTaskLockObserver = workflowTaskLockObserver;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
