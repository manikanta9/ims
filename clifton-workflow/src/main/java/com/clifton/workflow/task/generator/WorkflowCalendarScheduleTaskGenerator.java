package com.clifton.workflow.task.generator;

import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.populator.WorkflowTaskPopulatorService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The WorkflowCalendarScheduleTaskGenerator class is responsible for generation of new {@link WorkflowTask} objects
 * with due dates based on a custom {@link com.clifton.calendar.schedule.api.Schedule}.
 *
 * @author vgomelsky
 */
public class WorkflowCalendarScheduleTaskGenerator implements WorkflowTaskGenerator {

	private Integer dueDateScheduleId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ScheduleApiService scheduleApiService;
	private WorkflowTaskPopulatorService workflowTaskPopulatorService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<WorkflowTask> generate(WorkflowTaskDefinition taskDefinition, Date fromDate, Date toDate) {
		validate(taskDefinition, fromDate, toDate);

		List<WorkflowTask> result = new ArrayList<>();

		List<? extends IdentityObject> linkedEntityList = getLinkedEntityList();
		if (!CollectionUtils.isEmpty(linkedEntityList)) {
			Date maxDate = null;
			if (taskDefinition.getGenerateDueDateDaysFromToday() != null) {
				maxDate = DateUtils.clearTime(DateUtils.addDays(new Date(), taskDefinition.getGenerateDueDateDaysFromToday()));
			}

			// ignore scheduler's time: from start of from date till the end to date
			fromDate = DateUtils.clearTime(fromDate);
			toDate = DateUtils.getEndOfDay(toDate);

			List<Date> dueDateList = getScheduleApiService().getScheduleOccurrences(ScheduleOccurrenceCommand.forOccurrencesBetween(getDueDateScheduleId(), fromDate, toDate));
			for (Date dueDate : CollectionUtils.getIterable(dueDateList)) {
				// skip generation for dates that are after max allowed date for this definition
				if (maxDate == null || DateUtils.isDateAfterOrEqual(maxDate, DateUtils.clearTime(dueDate))) {
					for (IdentityObject linkedEntity : linkedEntityList) {
						WorkflowTask task = getWorkflowTaskPopulatorService().populateFromDefinitionForEntityForDueDate(taskDefinition, linkedEntity, dueDate);
						result.add(task);
					}
				}
			}
		}

		return result;
	}


	/**
	 * @throws ValidationException if this generator has invalid or missing parameters or configuration
	 */
	protected void validate(WorkflowTaskDefinition taskDefinition, Date fromDate, Date toDate) {
		ValidationUtils.assertNotNull(getDueDateScheduleId(), "Required field 'Due Date Schedule' is not defined.");
		ValidationUtils.assertNotNull(taskDefinition, "Required field 'taskDefinition' is not defined.");
		ValidationUtils.assertNotNull(fromDate, "Required field 'From Date' is not specified.");
		ValidationUtils.assertNotNull(toDate, "Required field 'To Date' is not specified.");
	}


	/**
	 * Returns a list of Linked Entities.  A task will be created for each entity.
	 * If the list is empty, no tasks will be created. The list can have one null entity to create one non-entity specific task.
	 */
	protected List<? extends IdentityObject> getLinkedEntityList() {
		List<IdentityObject> result = new ArrayList<>();
		// this generator is not entity specific but it could be extended by those that are: a task per account in a group, etc.
		result.add(null);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDueDateScheduleId() {
		return this.dueDateScheduleId;
	}


	public void setDueDateScheduleId(Integer dueDateScheduleId) {
		this.dueDateScheduleId = dueDateScheduleId;
	}


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}


	public WorkflowTaskPopulatorService getWorkflowTaskPopulatorService() {
		return this.workflowTaskPopulatorService;
	}


	public void setWorkflowTaskPopulatorService(WorkflowTaskPopulatorService workflowTaskPopulatorService) {
		this.workflowTaskPopulatorService = workflowTaskPopulatorService;
	}
}
