package com.clifton.workflow.task.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.CustomJsonStringAwareSearchForm;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.util.Date;


/**
 * @author vgomelsky
 */
public class WorkflowTaskSearchForm extends BaseWorkflowAwareSearchForm implements CustomJsonStringAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,description,linkedEntityLabel,definition.name", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "definition.id")
	private Short definitionId;

	@SearchField(searchField = "definition.category.id")
	private Short categoryId;

	@SearchField
	private Long linkedEntityFkFieldId;

	@SearchField
	private String linkedEntityLabel;

	@SearchField(searchFieldPath = "definition.linkedEntityTable", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String linkedEntityTableName;

	@SearchField(searchField = "assigneeUser.id")
	private Short assigneeUserId;

	@SearchField(searchField = "assigneeGroup.id")
	private Short assigneeGroupId;

	@SearchField(searchField = "assigneeUser.displayName,assigneeGroup.name", leftJoin = true)
	private String assigneeLabel;

	@SearchField(searchField = "approverUser.id")
	private Short approverUserId;

	@SearchField(searchField = "approverGroup.id")
	private Short approverGroupId;

	@SearchField(searchField = "approverUser.displayName,approverGroup.name", leftJoin = true)
	private String approverLabel;

	@SearchField(searchField = "assigneeUser.id,approverUser.id")
	private Short assigneeOrApproverUserOnlyId;

	@SearchField(searchField = "priority.id")
	private Short priorityId;

	@SearchField
	private Date dueDate;

	@SearchField
	private Date originalDueDate;

	@SearchField(searchField = "definition.unlockWorkflowStatus.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean locked;

	@SearchField(searchField = "definition.unlockWorkflowStatus.id")
	private Short unlockWorkflowStatusId;


	// custom filter: a task is "Completed" if it is in the final workflow state as per its definition
	private Boolean completedTask;

	// (Assignee User = User) OR (Assignee User IS NULL AND Assignee Group IN User's Group(s)) OR (Approver User = User) OR (Approver User IS NULL AND Approver Group IN User's Group(s))
	private Short assigneeOrApproverUserId;

	// ((Assignee User = User OR (Assignee User IS NULL AND Assignee Group IN User's Group(s))) AND Workflow Status = Assignee Workflow Status)
	private Short waitingForAssigneeUserId;
	// ((Approver User = User OR (Approver User IS NULL AND Approver Group IN User's Group(s))) AND Workflow Status NOT IN (Assignee Workflow Status, Final Workflow Status)
	private Short waitingForApproverUserId;
	// ((Assignee User = User OR (Assignee User IS NULL AND Assignee Group IN User's Group(s))) AND Workflow Status = Assignee Workflow Status)
	// OR ((Approver User = User OR (Approver User IS NULL AND Approver Group IN User's Group(s))) AND Workflow Status NOT IN (Assignee Workflow Status, Final Workflow Status)
	private Short waitingForAssigneeOrApproverUserId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public Long getLinkedEntityFkFieldId() {
		return this.linkedEntityFkFieldId;
	}


	public void setLinkedEntityFkFieldId(Long linkedEntityFkFieldId) {
		this.linkedEntityFkFieldId = linkedEntityFkFieldId;
	}


	public String getLinkedEntityLabel() {
		return this.linkedEntityLabel;
	}


	public void setLinkedEntityLabel(String linkedEntityLabel) {
		this.linkedEntityLabel = linkedEntityLabel;
	}


	public String getLinkedEntityTableName() {
		return this.linkedEntityTableName;
	}


	public void setLinkedEntityTableName(String linkedEntityTableName) {
		this.linkedEntityTableName = linkedEntityTableName;
	}


	public Short getAssigneeUserId() {
		return this.assigneeUserId;
	}


	public void setAssigneeUserId(Short assigneeUserId) {
		this.assigneeUserId = assigneeUserId;
	}


	public Short getAssigneeGroupId() {
		return this.assigneeGroupId;
	}


	public void setAssigneeGroupId(Short assigneeGroupId) {
		this.assigneeGroupId = assigneeGroupId;
	}


	public String getAssigneeLabel() {
		return this.assigneeLabel;
	}


	public void setAssigneeLabel(String assigneeLabel) {
		this.assigneeLabel = assigneeLabel;
	}


	public Short getApproverUserId() {
		return this.approverUserId;
	}


	public void setApproverUserId(Short approverUserId) {
		this.approverUserId = approverUserId;
	}


	public Short getApproverGroupId() {
		return this.approverGroupId;
	}


	public void setApproverGroupId(Short approverGroupId) {
		this.approverGroupId = approverGroupId;
	}


	public String getApproverLabel() {
		return this.approverLabel;
	}


	public void setApproverLabel(String approverLabel) {
		this.approverLabel = approverLabel;
	}


	public Short getAssigneeOrApproverUserId() {
		return this.assigneeOrApproverUserId;
	}


	public void setAssigneeOrApproverUserId(Short assigneeOrApproverUserId) {
		this.assigneeOrApproverUserId = assigneeOrApproverUserId;
	}


	public Short getAssigneeOrApproverUserOnlyId() {
		return this.assigneeOrApproverUserOnlyId;
	}


	public void setAssigneeOrApproverUserOnlyId(Short assigneeOrApproverUserOnlyId) {
		this.assigneeOrApproverUserOnlyId = assigneeOrApproverUserOnlyId;
	}


	public Short getPriorityId() {
		return this.priorityId;
	}


	public void setPriorityId(Short priorityId) {
		this.priorityId = priorityId;
	}


	public Date getDueDate() {
		return this.dueDate;
	}


	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}


	public Date getOriginalDueDate() {
		return this.originalDueDate;
	}


	public void setOriginalDueDate(Date originalDueDate) {
		this.originalDueDate = originalDueDate;
	}


	public Boolean getLocked() {
		return this.locked;
	}


	public void setLocked(Boolean locked) {
		this.locked = locked;
	}


	public Short getUnlockWorkflowStatusId() {
		return this.unlockWorkflowStatusId;
	}


	public void setUnlockWorkflowStatusId(Short unlockWorkflowStatusId) {
		this.unlockWorkflowStatusId = unlockWorkflowStatusId;
	}


	public Boolean getCompletedTask() {
		return this.completedTask;
	}


	public void setCompletedTask(Boolean completedTask) {
		this.completedTask = completedTask;
	}


	public Short getWaitingForAssigneeUserId() {
		return this.waitingForAssigneeUserId;
	}


	public void setWaitingForAssigneeUserId(Short waitingForAssigneeUserId) {
		this.waitingForAssigneeUserId = waitingForAssigneeUserId;
	}


	public Short getWaitingForApproverUserId() {
		return this.waitingForApproverUserId;
	}


	public void setWaitingForApproverUserId(Short waitingForApproverUserId) {
		this.waitingForApproverUserId = waitingForApproverUserId;
	}


	public Short getWaitingForAssigneeOrApproverUserId() {
		return this.waitingForAssigneeOrApproverUserId;
	}


	public void setWaitingForAssigneeOrApproverUserId(Short waitingForAssigneeOrApproverUserId) {
		this.waitingForAssigneeOrApproverUserId = waitingForAssigneeOrApproverUserId;
	}
}
