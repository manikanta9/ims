package com.clifton.workflow.task.lock;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.WorkflowTaskDefinitionLock;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.linkedentity.WorkflowTaskLinkedEntityObserver;
import com.clifton.workflow.task.linkedentity.WorkflowTaskLinkedEntityObserverRegistrator;
import com.clifton.workflow.task.search.WorkflowTaskDefinitionLockSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The WorkflowTaskManagingObserver class is responsible for registering and unregistering {@link WorkflowTaskLockObserver}
 * and/or {@link WorkflowTaskLinkedEntityObserver} objects for corresponding DAO's when
 * {@link WorkflowTaskDefinitionLock} or {@link WorkflowTaskDefinition} objects are updated.
 *
 * @author vgomelsky
 */
@Component
public class WorkflowTaskManagingObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private WorkflowTaskLinkedEntityObserverRegistrator workflowTaskLinkedEntityObserverRegistrator;
	private WorkflowTaskLockObserverRegistrator workflowTaskLockObserverRegistrator;

	private WorkflowTaskService workflowTaskService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// get original bean so that we can get the old value of lock table
		if (event.isUpdate()) {
			super.getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			if (bean instanceof WorkflowTaskDefinition) {
				WorkflowTaskDefinition definition = (WorkflowTaskDefinition) bean;
				WorkflowTaskDefinition originalDefinition = (WorkflowTaskDefinition) getOriginalBean(dao, bean);
				if (event.isInsert()) {
					registerWorkflowTaskLinkedEntityObserver(definition);
				}
				else if (event.isDelete()) {
					unregisterWorkflowTaskLinkedEntityObserver(definition);
				}
				if (originalDefinition != null) {
					if (event.isUpdate() && !CompareUtils.isEqual(originalDefinition.getLinkedEntityTable(), definition.getLinkedEntityTable())) {
						unregisterWorkflowTaskLinkedEntityObserver(originalDefinition);
						registerWorkflowTaskLinkedEntityObserver(definition);
					}
					if (definition.isActive() != originalDefinition.isActive()) {
						for (WorkflowTaskDefinitionLock lock : CollectionUtils.getIterable(getWorkflowTaskService().getWorkflowTaskDefinitionLockListForDefinition(definition.getId()))) {
							registerOrUnregisterLockObserver(lock);
						}
					}
				}
			}
			else if (bean instanceof WorkflowTaskDefinitionLock) {
				registerOrUnregisterLockObserver((WorkflowTaskDefinitionLock) bean);
				WorkflowTaskDefinitionLock originalBean = (WorkflowTaskDefinitionLock) getOriginalBean(dao, bean);
				if (originalBean != null) {
					registerOrUnregisterLockObserver(originalBean);
				}
			}
			else {
				throw new IllegalArgumentException("Unsupported class for bean: " + bean);
			}
		}
	}


	private void registerOrUnregisterLockObserver(WorkflowTaskDefinitionLock bean) {
		SystemTable lockTable = bean.getLockEntityTable();
		if (lockTable != null) {
			// get all locks for the table
			WorkflowTaskDefinitionLockSearchForm searchForm = new WorkflowTaskDefinitionLockSearchForm();
			searchForm.setLockEntityTableId(lockTable.getId());
			searchForm.setActive(true);
			List<WorkflowTaskDefinitionLock> lockList = getWorkflowTaskService().getWorkflowTaskDefinitionLockList(searchForm);
			// register or unregister
			if (CollectionUtils.isEmpty(lockList)) {
				getWorkflowTaskLockObserverRegistrator().unregisterLockObserver(lockTable.getName());
			}
			else {
				getWorkflowTaskLockObserverRegistrator().registerLockObserver(lockTable.getName());
			}
		}
	}


	private void registerWorkflowTaskLinkedEntityObserver(WorkflowTaskDefinition bean) {
		SystemTable linkedTable = bean.getLinkedEntityTable();
		if (linkedTable != null) {
			getWorkflowTaskLinkedEntityObserverRegistrator().registerLinkedEntityObserver(linkedTable.getName());
		}
	}


	private void unregisterWorkflowTaskLinkedEntityObserver(WorkflowTaskDefinition bean) {
		SystemTable linkedTable = bean.getLinkedEntityTable();
		if (linkedTable != null) {
			getWorkflowTaskLinkedEntityObserverRegistrator().unregisterLinkedEntityObserver(linkedTable.getName());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskLockObserverRegistrator getWorkflowTaskLockObserverRegistrator() {
		return this.workflowTaskLockObserverRegistrator;
	}


	public void setWorkflowTaskLockObserverRegistrator(WorkflowTaskLockObserverRegistrator workflowTaskLockObserverRegistrator) {
		this.workflowTaskLockObserverRegistrator = workflowTaskLockObserverRegistrator;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}


	public WorkflowTaskLinkedEntityObserverRegistrator getWorkflowTaskLinkedEntityObserverRegistrator() {
		return this.workflowTaskLinkedEntityObserverRegistrator;
	}


	public void setWorkflowTaskLinkedEntityObserverRegistrator(WorkflowTaskLinkedEntityObserverRegistrator workflowTaskLinkedEntityObserverRegistrator) {
		this.workflowTaskLinkedEntityObserverRegistrator = workflowTaskLinkedEntityObserverRegistrator;
	}
}
