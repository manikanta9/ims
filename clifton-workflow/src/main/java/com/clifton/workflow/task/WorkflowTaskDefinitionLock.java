package com.clifton.workflow.task;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;

import java.util.Objects;


/**
 * The WorkflowTaskDefinitionLock class allows locking one or more tables (one entity per table) for a given task definition.
 * It defines configuration and parameters for what and when should be locked.
 *
 * @author vgomelsky
 */
public class WorkflowTaskDefinitionLock extends BaseEntity<Short> {

	private WorkflowTaskDefinition definition;

	/**
	 * Optional locking mechanism used by observers: DAO for this table will have an observer that controls whether an entity can be modified.
	 * For example, to allow changes to all asset classes with a single task, lockEntityTable = "InvestmentAccountAssetClass"
	 * while linkedEntityListURL = "InvestmentAccount".
	 */
	private SystemTable lockEntityTable;

	/**
	 * The value is usually "id" but when lockEntityTable is defined, it will specify the path from lock entity to the linked entity.
	 * For example, "account.id" for the linkedEntityTable example above.
	 * <p>
	 * One can optionally specify multiple paths that are comma delimited. If this is the case, the system will use COALESCE logic
	 * to find the first not-null value.  For example, "startWorkflowState.workflow.id,endWorkflowState.workflow.id'.
	 */
	private String linkedEntityBeanFieldPath;


	/**
	 * Optionally specify what DAO event(s) should this lock apply to: INSERT, UPDATE, DELETE, ...
	 */
	private DaoEventTypes eventScope;

	/**
	 * If not set, all entities in the table will be locked.  If set, only tables where this scope condition evaluates to true will be locked.
	 * For example, "type.ourAccount == false" condition to limit scope to Holding Accounts.
	 */
	private SystemCondition lockScopeCondition;


	/**
	 * Dynamically determines if the entity can be modified while in the unlockWorkflowStatus status given it's current field values.
	 * If null, then all fields may be modified and if set, then the condition must evaluate to true.
	 */
	private SystemCondition lockEntityModifyCondition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		WorkflowTaskDefinitionLock that = (WorkflowTaskDefinitionLock) o;
		return Objects.equals(this.definition, that.definition) && Objects.equals(this.lockEntityTable, that.lockEntityTable) && Objects.equals(this.linkedEntityBeanFieldPath, that.linkedEntityBeanFieldPath) && this.eventScope == that.eventScope && Objects.equals(this.lockScopeCondition, that.lockScopeCondition) && Objects.equals(this.lockEntityModifyCondition, that.lockEntityModifyCondition);
	}


	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), this.definition, this.lockEntityTable, this.linkedEntityBeanFieldPath, this.eventScope, this.lockScopeCondition, this.lockEntityModifyCondition);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(WorkflowTaskDefinition definition) {
		this.definition = definition;
	}


	public SystemTable getLockEntityTable() {
		return this.lockEntityTable;
	}


	public void setLockEntityTable(SystemTable lockEntityTable) {
		this.lockEntityTable = lockEntityTable;
	}


	public String getLinkedEntityBeanFieldPath() {
		return this.linkedEntityBeanFieldPath;
	}


	public void setLinkedEntityBeanFieldPath(String linkedEntityBeanFieldPath) {
		this.linkedEntityBeanFieldPath = linkedEntityBeanFieldPath;
	}


	public DaoEventTypes getEventScope() {
		return this.eventScope;
	}


	public void setEventScope(DaoEventTypes eventScope) {
		this.eventScope = eventScope;
	}


	/**
	 * One can optionally specify multiple paths that are comma delimited. If this is the case, the system will use COALESCE logic
	 * to find the first not-null value.  For example, "startWorkflowState.workflow.id,endWorkflowState.workflow.id'.
	 */
	public String[] getLinkedEntityBeanFieldPaths() {
		return (this.linkedEntityBeanFieldPath == null) ? null : this.linkedEntityBeanFieldPath.split(",");
	}


	public SystemCondition getLockScopeCondition() {
		return this.lockScopeCondition;
	}


	public void setLockScopeCondition(SystemCondition lockScopeCondition) {
		this.lockScopeCondition = lockScopeCondition;
	}


	public SystemCondition getLockEntityModifyCondition() {
		return this.lockEntityModifyCondition;
	}


	public void setLockEntityModifyCondition(SystemCondition lockEntityModifyCondition) {
		this.lockEntityModifyCondition = lockEntityModifyCondition;
	}
}
