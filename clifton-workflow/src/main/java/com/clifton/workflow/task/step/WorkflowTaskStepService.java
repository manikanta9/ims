package com.clifton.workflow.task.step;

import com.clifton.workflow.task.step.search.WorkflowTaskStepDefinitionSearchForm;
import com.clifton.workflow.task.step.search.WorkflowTaskStepDependencySearchForm;
import com.clifton.workflow.task.step.search.WorkflowTaskStepSearchForm;

import java.util.List;


/**
 * The WorkflowTaskStepService interface defines methods for working with workflow steps and related objects.
 *
 * @author vgomelsky
 */
public interface WorkflowTaskStepService {

	////////////////////////////////////////////////////////////////////////////
	////////        Workflow Task Step Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskStep getWorkflowTaskStep(int id);


	public List<WorkflowTaskStep> getWorkflowTaskStepListByTask(int workflowTaskId);


	public List<WorkflowTaskStep> getWorkflowTaskStepList(WorkflowTaskStepSearchForm searchForm);


	public WorkflowTaskStep saveWorkflowTaskStep(WorkflowTaskStep step);


	public void deleteWorkflowTaskStep(int id);


	////////////////////////////////////////////////////////////////////////////
	////////   Workflow Task Step Definition Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskStepDefinition getWorkflowTaskStepDefinition(short id);


	public List<WorkflowTaskStepDefinition> getWorkflowTaskStepDefinitionListByTaskDefinition(short taskDefinitionId);


	public List<WorkflowTaskStepDefinition> getWorkflowTaskStepDefinitionList(WorkflowTaskStepDefinitionSearchForm searchForm);


	public WorkflowTaskStepDefinition saveWorkflowTaskStepDefinition(WorkflowTaskStepDefinition stepDefinition);


	public void deleteWorkflowTaskStepDefinition(short id);


	////////////////////////////////////////////////////////////////////////////
	////////    Workflow Task Step Dependency Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskStepDependency getWorkflowTaskStepDependency(short id);


	public List<WorkflowTaskStepDependency> getWorkflowTaskStepDependencyList(WorkflowTaskStepDependencySearchForm searchForm);


	public WorkflowTaskStepDependency saveWorkflowTaskStepDependency(WorkflowTaskStepDependency dependency);


	public void deleteWorkflowTaskStepDependency(short id);
}
