package com.clifton.workflow.task.populator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;


/**
 * The WorkflowTaskPopulatorService interface defines methods that populate WorkflowTask objects based on
 * the specified arguments.
 *
 * @author vgomelsky
 */
public interface WorkflowTaskPopulatorService {

	/**
	 * Creates and returns fully populated WorkflowTask object based on the meta-data on the specified definition.
	 *
	 * @param linkedEntityId optional linked entity id when applicable to the specified task definition
	 */
	@RequestMapping("workflowTaskForDefinitionPopulate")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_READ)
	public WorkflowTask populateWorkflowTaskForDefinition(short taskDefinitionId, Integer linkedEntityId);


	/**
	 * Creates and returns fully populated WorkflowTask object based on the meta-data on the specified definition.
	 * Looks up linked entity from the specified lock entity.
	 */
	@RequestMapping("workflowTaskForDefinitionForLockEntityPopulate")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_READ)
	public WorkflowTask populateWorkflowTaskForDefinitionForLockEntity(short taskDefinitionId, int lockEntityId, String lockEntityTableName);


	/**
	 * Creates and returns fully populated WorkflowTask object for the specified entity based on the meta-data on the specified definition.
	 */
	@DoNotAddRequestMapping
	public WorkflowTask populateFromDefinitionForEntity(WorkflowTaskDefinition definition, IdentityObject linkedEntity);


	/**
	 * Creates and returns fully populated WorkflowTask object for the specified entity based on the meta-data on the specified definition
	 * that is due on the specified due date.
	 */
	@DoNotAddRequestMapping
	public WorkflowTask populateFromDefinitionForEntityForDueDate(WorkflowTaskDefinition definition, IdentityObject linkedEntity, Date dueDate);


	/**
	 * Populates linked entity id and label fields for the specified task.
	 */
	public void populateLinkedEntity(WorkflowTask task, IdentityObject linkedEntity);


	/**
	 * Returns lock entity for the specified parameters. Throws ValidationException if it cannot be located.
	 */
	@DoNotAddRequestMapping
	public IdentityObject getLockEntity(String lockEntityTableName, int lockEntityId);
}
