package com.clifton.workflow.task.generator;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.workflow.task.WorkflowTask;

import java.util.List;


/**
 * The WorkflowTaskGeneratorService interface defines methods that can be used to generate {@link WorkflowTask}
 * objects based on the specified parameters.
 *
 * @author vgomelsky
 */
public interface WorkflowTaskGeneratorService {

	@SecureMethod(permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public List<WorkflowTask> generateWorkflowTaskList(TaskGeneratorCommand command);
}
