package com.clifton.workflow.task.generator;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.search.WorkflowTaskDefinitionSearchForm;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The WorkflowTaskGeneratorJob class generates {@link com.clifton.workflow.task.WorkflowTask} objects based on
 * the specified parameters.
 *
 * @author vgomelsky
 */
public class WorkflowTaskGeneratorJob implements Task, ValidationAware {

	private Integer daysBackFromToday;
	private Integer daysForwardFromToday;

	/**
	 * Optionally limits to tasks for a single definition.
	 */
	private Short taskDefinitionId;
	/**
	 * Optionally limits to tasks of specific generator bean type.
	 */
	private Integer taskGeneratorBeanTypeId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private WorkflowTaskService workflowTaskService;
	private WorkflowTaskGeneratorService workflowTaskGeneratorService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getDaysBackFromToday(), "Required parameter 'daysBackFromToday' must be set");
		ValidationUtils.assertNotNull(getDaysForwardFromToday(), "Required parameter 'daysForwardFromToday' must be set");

		ValidationUtils.assertTrue(getDaysBackFromToday() > 0, "Required parameter 'daysBackFromToday' must be greater than 0");
		ValidationUtils.assertTrue(getDaysForwardFromToday() >= 0, "Required parameter 'daysForwardFromToday' must be greater than or equal to 0");
	}


	@Override
	public Status run(Map<String, Object> context) {
		WorkflowTaskDefinitionSearchForm searchForm = new WorkflowTaskDefinitionSearchForm();
		searchForm.setTaskGeneratorBeanPresent(true);
		searchForm.setActive(true);
		searchForm.setId(getTaskDefinitionId());
		searchForm.setTaskGeneratorBeanTypeId(getTaskGeneratorBeanTypeId());
		List<WorkflowTaskDefinition> definitionList = getWorkflowTaskService().getWorkflowTaskDefinitionList(searchForm);

		Date now = DateUtils.clearTime(new Date());
		Date fromDate = DateUtils.addDays(now, -getDaysBackFromToday());
		Date toDate = DateUtils.addDays(new Date(), getDaysForwardFromToday());

		int taskCount = 0;
		int errorCount = 0;
		Status status = Status.ofMessage("Starting WorkflowTaskGeneratorJob");
		for (WorkflowTaskDefinition definition : CollectionUtils.getIterable(definitionList)) {
			try {
				Date toDateOverride = getMaxToDate(definition, toDate);
				List<WorkflowTask> taskList = getWorkflowTaskGeneratorService().generateWorkflowTaskList(TaskGeneratorCommand.ofDateRange(definition.getId(), fromDate, toDateOverride));
				taskCount += CollectionUtils.getSize(taskList);
			}
			catch (Throwable e) {
				errorCount++;
				status.addError(definition.getName() + ": " + e.getMessage());
				LogUtils.error(getClass(), "Error generating workflow tasks for " + definition, e);
			}
		}

		status.setMessage("Tasks Generated: " + taskCount + "; Errors: " + errorCount);
		return status;
	}


	private Date getMaxToDate(WorkflowTaskDefinition definition, Date toDate) {
		if (definition.getGenerateDueDateDaysFromToday() != null) {
			Date maxDate = DateUtils.addDays(DateUtils.clearTime(new Date()), definition.getGenerateDueDateDaysFromToday());
			if (maxDate.before(toDate)) {
				return maxDate;
			}
		}
		return toDate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDaysBackFromToday() {
		return this.daysBackFromToday;
	}


	public void setDaysBackFromToday(Integer daysBackFromToday) {
		this.daysBackFromToday = daysBackFromToday;
	}


	public Integer getDaysForwardFromToday() {
		return this.daysForwardFromToday;
	}


	public void setDaysForwardFromToday(Integer daysForwardFromToday) {
		this.daysForwardFromToday = daysForwardFromToday;
	}


	public Short getTaskDefinitionId() {
		return this.taskDefinitionId;
	}


	public void setTaskDefinitionId(Short taskDefinitionId) {
		this.taskDefinitionId = taskDefinitionId;
	}


	public Integer getTaskGeneratorBeanTypeId() {
		return this.taskGeneratorBeanTypeId;
	}


	public void setTaskGeneratorBeanTypeId(Integer taskGeneratorBeanTypeId) {
		this.taskGeneratorBeanTypeId = taskGeneratorBeanTypeId;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}


	public WorkflowTaskGeneratorService getWorkflowTaskGeneratorService() {
		return this.workflowTaskGeneratorService;
	}


	public void setWorkflowTaskGeneratorService(WorkflowTaskGeneratorService workflowTaskGeneratorService) {
		this.workflowTaskGeneratorService = workflowTaskGeneratorService;
	}
}
