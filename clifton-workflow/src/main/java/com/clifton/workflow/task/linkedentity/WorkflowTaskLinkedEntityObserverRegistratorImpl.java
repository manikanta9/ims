package com.clifton.workflow.task.linkedentity;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.search.WorkflowTaskDefinitionSearchForm;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author mitchellf
 */
@Component
public class WorkflowTaskLinkedEntityObserverRegistratorImpl implements WorkflowTaskLinkedEntityObserverRegistrator, CurrentContextApplicationListener<ContextRefreshedEvent> {


	private DaoLocator daoLocator;


	private WorkflowTaskLinkedEntityObserver<IdentityObject> workflowTaskLinkedEntityObserver;
	private WorkflowTaskService workflowTaskService;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			// get active task definition locks
			WorkflowTaskDefinitionSearchForm searchForm = new WorkflowTaskDefinitionSearchForm();
			searchForm.setActive(true);
			searchForm.setLinkedEntityTablePresent(true);
			List<WorkflowTaskDefinition> definitionList = getWorkflowTaskService().getWorkflowTaskDefinitionList(searchForm);

			// get corresponding DAO and register observer
			for (WorkflowTaskDefinition definition : CollectionUtils.getIterable(definitionList)) {
				registerLinkedEntityObserver(definition.getLinkedEntityTable().getName());
			}
		}
		catch (Throwable e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error registering DAO Workflow Task Associated Entity Observers: " + event, e);
		}
	}


	@Override
	public void registerLinkedEntityObserver(String tableName) {
		ObserverableDAO<IdentityObject> dao = (ObserverableDAO<IdentityObject>) getDaoLocator().locate(tableName);
		dao.registerEventObserver(DaoEventTypes.DELETE, getWorkflowTaskLinkedEntityObserver());
	}


	@Override
	public void unregisterLinkedEntityObserver(String tableName) {
		ObserverableDAO<IdentityObject> dao = (ObserverableDAO<IdentityObject>) getDaoLocator().locate(tableName);
		dao.unregisterEventObserver(DaoEventTypes.DELETE, getWorkflowTaskLinkedEntityObserver());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public WorkflowTaskLinkedEntityObserver<IdentityObject> getWorkflowTaskLinkedEntityObserver() {
		return this.workflowTaskLinkedEntityObserver;
	}


	public void setWorkflowTaskLinkedEntityObserver(WorkflowTaskLinkedEntityObserver<IdentityObject> workflowTaskLinkedEntityObserver) {
		this.workflowTaskLinkedEntityObserver = workflowTaskLinkedEntityObserver;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
