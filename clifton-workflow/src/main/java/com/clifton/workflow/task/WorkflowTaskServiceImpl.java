package com.clifton.workflow.task;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.JsonStrategy;
import com.clifton.core.converter.json.jackson.strategy.JacksonDefaultStrategy;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.workflow.task.populator.WorkflowTaskPopulatorService;
import com.clifton.workflow.task.search.WorkflowTaskCategorySearchForm;
import com.clifton.workflow.task.search.WorkflowTaskDefinitionLockSearchForm;
import com.clifton.workflow.task.search.WorkflowTaskDefinitionSearchForm;
import com.clifton.workflow.task.search.WorkflowTaskSearchForm;
import com.clifton.workflow.task.step.WorkflowTaskStep;
import com.clifton.workflow.task.step.WorkflowTaskStepDefinition;
import com.clifton.workflow.task.step.WorkflowTaskStepService;
import com.clifton.workflow.task.validation.WorkflowTaskValidator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class WorkflowTaskServiceImpl implements WorkflowTaskService {

	private AdvancedUpdatableDAO<WorkflowTask, Criteria> workflowTaskDAO;
	private AdvancedUpdatableDAO<WorkflowTaskCategory, Criteria> workflowTaskCategoryDAO;
	private AdvancedUpdatableDAO<WorkflowTaskDefinition, Criteria> workflowTaskDefinitionDAO;
	private AdvancedUpdatableDAO<WorkflowTaskDefinitionLock, Criteria> workflowTaskDefinitionLockDAO;

	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private JsonHandler<JsonStrategy> jsonHandler;
	private SecurityAuthorizationService securityAuthorizationService;
	private SecurityUserService securityUserService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemSchemaService systemSchemaService;
	private WorkflowTaskPopulatorService workflowTaskPopulatorService;
	private WorkflowTaskStepService workflowTaskStepService;

	private DaoNamedEntityCache<WorkflowTaskCategory> workflowTaskCategoryCache;
	private DaoSingleKeyListCache<WorkflowTaskDefinitionLock, Short> workflowTaskDefinitionLockListByLockTableCache;


	////////////////////////////////////////////////////////////////////////////
	////////       Workflow Task Category Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTaskCategory getWorkflowTaskCategory(short id) {
		return getWorkflowTaskCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public WorkflowTaskCategory getWorkflowTaskCategoryByName(String name) {
		return getWorkflowTaskCategoryCache().getBeanForKeyValueStrict(getWorkflowTaskCategoryDAO(), name);
	}


	@Override
	public List<WorkflowTaskCategory> getWorkflowTaskCategoryList(WorkflowTaskCategorySearchForm searchForm) {
		return getWorkflowTaskCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public WorkflowTaskCategory saveWorkflowTaskCategory(WorkflowTaskCategory category) {
		return getWorkflowTaskCategoryDAO().save(category);
	}


	@Override
	public void deleteWorkflowTaskCategory(short id) {
		getWorkflowTaskCategoryDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////       Workflow Task Definition Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public WorkflowTaskDefinition getWorkflowTaskDefinition(short id) {
		WorkflowTaskDefinition definition = getWorkflowTaskDefinitionDAO().findByPrimaryKey(id);
		if (definition != null && definition.isLocked()) {
			definition.setLockList(getWorkflowTaskDefinitionLockListForDefinition(id));
		}
		return definition;
	}


	@Override
	public List<WorkflowTaskDefinition> getWorkflowTaskDefinitionList(WorkflowTaskDefinitionSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getLockEntityTableName() != null) {
					criteria.add(existsTaskDefinitionLockForTable(searchForm.getLockEntityTableName(), criteria));
				}
				if (searchForm.getLinkedOrLockEntityTableName() != null) {
					criteria.add(
							Restrictions.or(
									Restrictions.eq(getPathAlias("linkedEntityTable", criteria) + ".name", searchForm.getLinkedOrLockEntityTableName()),
									existsTaskDefinitionLockForTable(searchForm.getLinkedOrLockEntityTableName(), criteria)
							)
					);
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				// when sorting is not defined, default to category name and then definition name
				if (StringUtils.isEmpty(getSortableSearchForm().getOrderBy())) {
					criteria.addOrder(Order.asc(getPathAlias("category", criteria) + ".name"));
					criteria.addOrder(Order.asc("name"));
					return true;
				}
				return super.configureOrderBy(criteria);
			}
		};
		return getWorkflowTaskDefinitionDAO().findBySearchCriteria(config);
	}


	private Criterion existsTaskDefinitionLockForTable(String tableName, Criteria definitionCriteria) {
		return Subqueries.exists(
				DetachedCriteria.forClass(WorkflowTaskDefinitionLock.class, "l")
						.setProjection(Projections.id())
						.add(Restrictions.eqProperty("definition.id", definitionCriteria.getAlias() + ".id"))
						.createAlias("lockEntityTable", "lt").add(Restrictions.eq("lt.name", tableName)));
	}


	@Override
	@Transactional
	public WorkflowTaskDefinition saveWorkflowTaskDefinition(WorkflowTaskDefinition definition) {
		// validate field combinations first
		if (definition.getLinkedEntityTable() != null) {
			ValidationUtils.assertNotEmpty(definition.getLinkedEntityListURL(), "Linked Entity List URL is required when Linked Entity Table is defined.", "linkedEntityListURL");
		}
		else {
			ValidationUtils.assertNull(definition.getLinkedEntityListURL(), "Linked Entity List URL cannot be populated when Linked Entity Table is not defined.", "linkedEntityListURL");
			ValidationUtils.assertNull(definition.getLinkedEntityFkFieldId(), "Linked Entity ID cannot be populated when Linked Entity Table is not defined.", "linkedEntityListURL");
			ValidationUtils.assertNull(definition.getLinkedEntityLabel(), "Linked Entity Label cannot be populated when Linked Entity Table is not defined.", "linkedEntityListURL");
		}

		if (CollectionUtils.isEmpty(definition.getLockList())) {
			ValidationUtils.assertNull(definition.getUnlockWorkflowStatus(), "Unlock Workflow Status cannot be selected when no Lock is defined.", "unlockWorkflowStatus");
		}
		else {
			ValidationUtils.assertNotNull(definition.getUnlockWorkflowStatus(), "Unlock Workflow Status is required when Lock Entity Table is defined.", "unlockWorkflowStatus");
			ValidationUtils.assertNotEquals(definition.getUnlockWorkflowStatus(), definition.getFinalWorkflowStatus(), "Unlock Workflow Status cannot be the same as the Workflow Status of Final Workflow Status.", "unlockWorkflowStatus");
			ValidationUtils.assertNull(definition.getTaskGeneratorBean(), "Task Generator Bean cannot be used with Locking task definitions.", "taskGeneratorBean");
		}
		if (definition.getGenerateDueDateDaysFromToday() != null) {
			ValidationUtils.assertTrue(definition.getGenerateDueDateDaysFromToday() >= 0, "Generate Due Date Days From Today must be positive or 0", "generateDueDateDaysFromToday");
		}
		if (!StringUtils.isEmpty(definition.getTaskNameTemplate())) {
			ValidationUtils.assertTrue(definition.isTaskNameAllowed(), "When Task Name Template is specified, task name must be allowed.");
			ValidationUtils.assertNotNull(definition.getTaskGeneratorBean(), "When Task Name Template is specified, Task Generator Bean must be defined.");
		}
		if (StringUtils.isEmpty(definition.getLinkedWindowName())) {
			ValidationUtils.assertNull(definition.getLinkedWindowClass(), "Linked Window Class is not allowed when Linked Window Name is not defined", "linkedWindowClass");
			ValidationUtils.assertNull(definition.getLinkedWindowDefaultData(), "Linked Window Default Data is not allowed when Linked Window Name is not defined", "linkedWindowName");
		}
		else {
			ValidationUtils.assertNotEmpty(definition.getLinkedWindowClass(), "Linked Window Class is required when Linked Window Name is defined", "linkedWindowClass");
		}

		if (definition.isGenerateOnEntityCreation()) {
			ValidationUtils.assertTrue(definition.getCategory().isGenerateOnEntityCreationAllowed(), "Workflow Task Definitions for selected Category do not allow auto-creation of new Workflow Tasks on Entity Creation.");
			ValidationUtils.assertNotEmpty(definition.getLockList(), "Lock Entity table(s) are required when 'Generate On Entity Creation' option is selected.");
			boolean found = false;
			for (WorkflowTaskDefinitionLock lock : definition.getLockList()) {
				if (lock.getLockEntityTable().equals(definition.getLinkedEntityTable())) {
					found = true;
					break;
				}
			}
			ValidationUtils.assertTrue(found, "When 'Generate On Entity Creation' option is selected, there must be a Lock Table that is the same as Linked Table.");
			ValidationUtils.assertNotNull(ObjectUtils.coalesce(definition.getApproverUser(), definition.getApproverGroup()), "Approver User or Group is required when 'Generate On Entity Creation' option is selected.", "approverUser");
			ValidationUtils.assertNotNull(ObjectUtils.coalesce(definition.getSecondaryApproverUser(), definition.getApproverGroup()), "Secondary Approver User or Group is required when 'Generate On Entity Creation' option is selected.", "secondaryApproverUser");
			ValidationUtils.assertNotNull(definition.getPriority(), "Priority is required when 'Generate On Entity Creation' option is selected.", "priority");
			if (definition.isTaskNameRequired()) {
				ValidationUtils.assertNotEmpty(definition.getTaskNameTemplate(), "Cannot require Task Name when 'Generate On Entity Creation' option is selected and 'Name Template' is not specified.", "taskNameTemplate");
			}
		}

		if (definition.isAssigneeRequiredToBeInAssigneeGroup()) {
			ValidationUtils.assertNotNull(definition.getAssigneeGroup(), "Assignee Group is required when 'Assignee is Required to be in Assignee Group' option is selected.", "assigneeGroup");
			if (definition.getAssigneeUser() != null) {
				ValidationUtils.assertTrue(getSecurityUserService().isSecurityUserInGroup(definition.getAssigneeUser().getId(), definition.getAssigneeGroup().getName()), "Task Assignee User must belong to Task Assignee Group.", "assigneeUser");
			}
		}
		if (definition.isApproverRequiredToBeInApproverGroup()) {
			ValidationUtils.assertNotNull(definition.getApproverGroup(), "Approver Group is required when 'Approver is Required to be in Approver Group' option is selected.", "approverGroup");
			if (definition.getApproverUser() != null) {
				ValidationUtils.assertTrue(getSecurityUserService().isSecurityUserInGroup(definition.getApproverUser().getId(), definition.getApproverGroup().getName()), "Task Approver User must belong to Task Approver Group.", "approverUser");
			}
			if (definition.getSecondaryApproverUser() != null) {
				ValidationUtils.assertTrue(getSecurityUserService().isSecurityUserInGroup(definition.getSecondaryApproverUser().getId(), definition.getApproverGroup().getName()), "Task Secondary Approver User must belong to Task Approver Group.", "secondaryApproverUser");
			}
		}

		if (!definition.isSameAssigneeAndApproverAllowed()) {
			if (definition.getAssigneeUser() != null && definition.getApproverUser() != null) {
				ValidationUtils.assertNotEquals(definition.getAssigneeUser(), definition.getApproverUser(), "Task Assignee and Approver cannot be the same user for this Task Definition.", "assigneeUser");
			}
		}

		if (definition.getSecondaryApproverUser() != null) {
			ValidationUtils.assertNotNull(definition.getApproverUser(), "Approver User is required when Secondary Approver User is defined.", "approverUser");
			ValidationUtils.assertNotEquals(definition.getApproverUser(), definition.getSecondaryApproverUser(), "Approver User and Secondary Approver User cannot be the same.", "secondaryApproverUser");
		}

		// now save the definition and locks
		List<WorkflowTaskDefinitionLock> originalLockList = null;
		List<WorkflowTaskDefinitionLock> newLockList = definition.getLockList();
		if (!definition.isNewBean()) {
			originalLockList = getWorkflowTaskDefinitionLockListForDefinition(definition.getId());
		}

		if (!definition.getCategory().isDefinitionLockingAllowed()) {
			ValidationUtils.assertEmpty(originalLockList, "Workflow Task Definitions for selected Category do not allow locking.");
			ValidationUtils.assertEmpty(newLockList, "Workflow Task Definitions for selected Category do not allow locking.");
		}

		definition = getWorkflowTaskDefinitionDAO().save(definition);

		for (WorkflowTaskDefinitionLock lock : CollectionUtils.getIterable(newLockList)) {
			lock.setDefinition(definition);
		}
		getWorkflowTaskDefinitionLockDAO().saveList(newLockList, originalLockList);
		definition.setLockList(newLockList);

		return definition;
	}


	@Override
	@Transactional
	public void deleteWorkflowTaskDefinition(short id) {
		getWorkflowTaskDefinitionLockDAO().deleteList(getWorkflowTaskDefinitionLockListForDefinition(id));
		getWorkflowTaskDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////     Workflow Task Definition Lock Business Methods    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTaskDefinitionLock getWorkflowTaskDefinitionLock(short id) {
		return getWorkflowTaskDefinitionLockDAO().findByPrimaryKey(id);
	}


	@Override
	public List<WorkflowTaskDefinitionLock> getWorkflowTaskDefinitionLockListForDefinition(short definitionId) {
		return getWorkflowTaskDefinitionLockDAO().findByField("definition.id", definitionId);
	}


	@Override
	public List<WorkflowTaskDefinitionLock> getWorkflowTaskDefinitionLockListForTable(String lockTableName) {
		return getWorkflowTaskDefinitionLockListByLockTableCache().getBeanListForKeyValue(getWorkflowTaskDefinitionLockDAO(), getSystemSchemaService().getSystemTableByName(lockTableName).getId());
	}


	@Override
	public List<WorkflowTaskDefinitionLock> getWorkflowTaskDefinitionLockList(WorkflowTaskDefinitionLockSearchForm searchForm) {
		List<WorkflowTaskDefinitionLock> lockList = getWorkflowTaskDefinitionLockDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));

		if (!CollectionUtils.isEmpty(lockList) && searchForm.getEventType() != null) {
			lockList = BeanUtils.filter(lockList, lock -> (lock.getEventScope() == null || lock.getEventScope().isSuperSetOf(searchForm.getEventType())));
		}

		if (!CollectionUtils.isEmpty(lockList) && searchForm.getLockEntityId() != null) {
			lockList = BeanUtils.filter(lockList, lock -> {
				if (lock.getLockScopeCondition() != null) {
					// most conditions are security based (current user is a member of specific group)
					IdentityObject lockEntity;
					if (StringUtils.isEmpty(searchForm.getLockEntityJson())) {
						lockEntity = getWorkflowTaskPopulatorService().getLockEntity(lock.getLockEntityTable().getName(), searchForm.getLockEntityId());
					}
					else {
						// reuse passed object: can be modified but not persisted yet (needed to evaluate property change conditions)
						lockEntity = (IdentityObject) getJsonHandler().fromJson(searchForm.getLockEntityJson(), CoreClassUtils.getClass(searchForm.getLockEntityClassName()), new JacksonDefaultStrategy());
					}
					return getSystemConditionEvaluationHandler().isConditionTrue(lock.getLockScopeCondition(), lockEntity);
				}
				return true;
			});
		}

		if (!CollectionUtils.isEmpty(lockList) && searchForm.isLimitUsingTaskEntityModifyCondition()) {
			lockList = BeanUtils.filter(lockList, lock -> {
				// do not apply this to Admin users: can see all definitions similar to Entity Modify Condition logic
				if (!getSecurityAuthorizationService().isSecurityUserAdmin()) {
					// most conditions are security based (current user is a member of specific group)
					IdentityObject linkedEntity = null;
					if (lock.getLockEntityTable().equals(lock.getDefinition().getLinkedEntityTable()) && searchForm.getLockEntityId() != null && searchForm.getLockEntityTableName() != null) {
						// locked and linked tables are the same
						linkedEntity = getWorkflowTaskPopulatorService().getLockEntity(searchForm.getLockEntityTableName(), searchForm.getLockEntityId());
					}
					WorkflowTask task = getWorkflowTaskPopulatorService().populateFromDefinitionForEntity(lock.getDefinition(), linkedEntity);
					// when a task definition is used to lock another task definition, use linked entity's modify condition
					WorkflowTaskDefinition evaluationDefinition = (linkedEntity instanceof WorkflowTaskDefinition) ? (WorkflowTaskDefinition) linkedEntity : lock.getDefinition();
					if (evaluationDefinition.getTaskEntityModifyCondition() != null) {
						return getSystemConditionEvaluationHandler().isConditionTrue(evaluationDefinition.getTaskEntityModifyCondition(), task);
					}
				}
				return true;
			});
		}

		return lockList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////          Workflow Task Business Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTask getWorkflowTask(int id) {
		return getWorkflowTaskDAO().findByPrimaryKey(id);
	}


	@Override
	public List<WorkflowTask> getWorkflowTaskList(WorkflowTaskSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getCompletedTask() != null) {
					String definitionAlias = super.getPathAlias("definition", criteria);
					if (searchForm.getCompletedTask()) {
						criteria.add(Restrictions.eqProperty("workflowStatus.id", definitionAlias + ".finalWorkflowStatus.id"));
					}
					else {
						criteria.add(Restrictions.neProperty("workflowStatus.id", definitionAlias + ".finalWorkflowStatus.id"));
					}
				}

				if (searchForm.getWaitingForAssigneeOrApproverUserId() != null) {
					// ((Assignee User = User OR (Assignee User IS NULL AND Assignee Group IN User's Group(s))) AND Workflow Status = Assignee Workflow Status)
					// OR ((Approver User = User OR (Approver User IS NULL AND Approver Group IN User's Group(s))) AND Workflow Status NOT IN (Assignee Workflow Status, Final Workflow Status)
					String definitionAlias = super.getPathAlias("definition", criteria);
					Criterion assigneeCriterion = Restrictions.and(
							createUserRestriction(searchForm.getWaitingForAssigneeOrApproverUserId(), true, false),
							Restrictions.eqProperty("workflowStatus.id", definitionAlias + ".assigneeWorkflowStatus.id")
					);
					Criterion approverCriterion = Restrictions.and(
							createUserRestriction(searchForm.getWaitingForAssigneeOrApproverUserId(), false, true),
							Restrictions.neProperty("workflowStatus.id", definitionAlias + ".finalWorkflowStatus.id"),
							Restrictions.neProperty("workflowStatus.id", definitionAlias + ".assigneeWorkflowStatus.id")
					);
					criteria.add(Restrictions.or(assigneeCriterion, approverCriterion));
				}
				else if (searchForm.getWaitingForAssigneeUserId() != null) {
					// ((Assignee User = User OR (Assignee User IS NULL AND Assignee Group IN User's Group(s))) AND Workflow Status = Assignee Workflow Status)
					String definitionAlias = super.getPathAlias("definition", criteria);
					criteria.add(Restrictions.and(
							createUserRestriction(searchForm.getWaitingForAssigneeUserId(), true, false),
							Restrictions.eqProperty("workflowStatus.id", definitionAlias + ".assigneeWorkflowStatus.id"))
					);
				}
				else if (searchForm.getWaitingForApproverUserId() != null) {
					// ((Approver User = User OR (Approver User IS NULL AND Approver Group IN User's Group(s))) AND Workflow Status NOT IN (Assignee Workflow Status, Final Workflow Status)
					String definitionAlias = super.getPathAlias("definition", criteria);
					criteria.add(Restrictions.and(
							createUserRestriction(searchForm.getWaitingForApproverUserId(), false, true),
							Restrictions.neProperty("workflowStatus.id", definitionAlias + ".finalWorkflowStatus.id"),
							Restrictions.neProperty("workflowStatus.id", definitionAlias + ".assigneeWorkflowStatus.id"))
					);
				}
				else if (searchForm.getAssigneeOrApproverUserId() != null) {
					criteria.add(createUserRestriction(searchForm.getAssigneeOrApproverUserId(), true, true));
				}
			}
		};

		return getWorkflowTaskDAO().findBySearchCriteria(searchConfigurer);
	}


	private Criterion createUserRestriction(short userId, boolean includeAssignee, boolean includeApprover) {
		List<Short> userGroupIds = BeanUtils.getBeanIdentityList(getSecurityUserService().getSecurityGroupListByUser(userId));
		if (includeAssignee && includeApprover) {
			return Restrictions.or(
					Restrictions.eq("assigneeUser.id", userId),
					Restrictions.and(Restrictions.isNull("assigneeUser.id"), Restrictions.in("assigneeGroup.id", userGroupIds)),
					Restrictions.eq("approverUser.id", userId),
					Restrictions.and(Restrictions.isNull("approverUser.id"), Restrictions.in("approverGroup.id", userGroupIds))
			);
		}
		if (includeAssignee) {
			return Restrictions.or(
					Restrictions.eq("assigneeUser.id", userId),
					Restrictions.and(Restrictions.isNull("assigneeUser.id"), Restrictions.in("assigneeGroup.id", userGroupIds))
			);
		}
		if (includeApprover) {
			return Restrictions.or(
					Restrictions.eq("approverUser.id", userId),
					Restrictions.and(Restrictions.isNull("approverUser.id"), Restrictions.in("approverGroup.id", userGroupIds))
			);
		}
		throw new IllegalArgumentException("Either includeAssignee or includeApprover or both flags must be set to true.");
	}


	@Override
	public WorkflowTask saveWorkflowTask(WorkflowTask task) {
		return saveWorkflowTaskImpl(task, false);
	}


	@Override
	public WorkflowTask createWorkflowTaskAfterLockedEntityInsert(WorkflowTask task) {
		ValidationUtils.assertTrue(task.isNewBean(), "createWorkflowTaskAfterLockedEntityInsert cannot be called to update an existing task. Workflow Task ID = " + task.getId());
		return saveWorkflowTaskImpl(task, true);
	}


	/**
	 * @see WorkflowTaskValidator
	 */
	@Transactional
	protected WorkflowTask saveWorkflowTaskImpl(WorkflowTask task, boolean calledOnLockedEntityInsert) {
		boolean newBean = task.isNewBean();

		if (!calledOnLockedEntityInsert) {
			// additional validation that should be ignored on entity insert or auto-task generation
			WorkflowTaskDefinition definition = getWorkflowTaskDefinition(task.getDefinition().getId());
			if (definition.isTaskDescriptionRequired() && !(task.isNewBean() && definition.getTaskGeneratorBean() != null)) {
				ValidationUtils.assertNotEmpty(task.getDescription(), "Description field is required for tasks of selected definition: " + definition.getName(), "description");
			}
		}

		task = getWorkflowTaskDAO().save(task);

		// create the steps for this task if necessary
		if (newBean) {
			List<WorkflowTaskStepDefinition> stepDefinitionList = getWorkflowTaskStepService().getWorkflowTaskStepDefinitionListByTaskDefinition(task.getDefinition().getId());
			for (WorkflowTaskStepDefinition stepDefinition : CollectionUtils.getIterable(stepDefinitionList)) {
				WorkflowTaskStep step = new WorkflowTaskStep();
				step.setTask(task);
				step.setStepDefinition(stepDefinition);
				step.setAssigneeUser(ObjectUtils.coalesce(stepDefinition.getAssigneeUser(), task.getAssigneeUser()));
				step.setDueDate(getCalendarDateGenerationHandler().generateDate(stepDefinition.getStepDueDateGenerationOption(), task.getDueDate(), stepDefinition.getStepDueDateDaysFromTaskDueDate()));
				step.setOriginalDueDate(step.getDueDate());
				if (step.getDueDate().after(task.getDueDate())) {
					throw new FieldValidationException("Step Due Date of " + DateUtils.fromDateSmart(step.getDueDate()) + " cannot be after corresponding Task Due Date of " + DateUtils.fromDateSmart(task.getDueDate()), "dueDate");
				}
				getWorkflowTaskStepService().saveWorkflowTaskStep(step);
			}
		}

		return task;
	}


	/**
	 * @see WorkflowTaskValidator
	 */
	@Override
	@Transactional
	public void deleteWorkflowTask(int id) {
		List<WorkflowTaskStep> stepList = getWorkflowTaskStepService().getWorkflowTaskStepListByTask(id);
		for (WorkflowTaskStep step : CollectionUtils.getIterable(stepList)) {
			getWorkflowTaskStepService().deleteWorkflowTaskStep(step.getId());
		}

		getWorkflowTaskDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<WorkflowTask, Criteria> getWorkflowTaskDAO() {
		return this.workflowTaskDAO;
	}


	public void setWorkflowTaskDAO(AdvancedUpdatableDAO<WorkflowTask, Criteria> workflowTaskDAO) {
		this.workflowTaskDAO = workflowTaskDAO;
	}


	public AdvancedUpdatableDAO<WorkflowTaskCategory, Criteria> getWorkflowTaskCategoryDAO() {
		return this.workflowTaskCategoryDAO;
	}


	public void setWorkflowTaskCategoryDAO(AdvancedUpdatableDAO<WorkflowTaskCategory, Criteria> workflowTaskCategoryDAO) {
		this.workflowTaskCategoryDAO = workflowTaskCategoryDAO;
	}


	public AdvancedUpdatableDAO<WorkflowTaskDefinition, Criteria> getWorkflowTaskDefinitionDAO() {
		return this.workflowTaskDefinitionDAO;
	}


	public void setWorkflowTaskDefinitionDAO(AdvancedUpdatableDAO<WorkflowTaskDefinition, Criteria> workflowTaskDefinitionDAO) {
		this.workflowTaskDefinitionDAO = workflowTaskDefinitionDAO;
	}


	public AdvancedUpdatableDAO<WorkflowTaskDefinitionLock, Criteria> getWorkflowTaskDefinitionLockDAO() {
		return this.workflowTaskDefinitionLockDAO;
	}


	public void setWorkflowTaskDefinitionLockDAO(AdvancedUpdatableDAO<WorkflowTaskDefinitionLock, Criteria> workflowTaskDefinitionLockDAO) {
		this.workflowTaskDefinitionLockDAO = workflowTaskDefinitionLockDAO;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public JsonHandler<JsonStrategy> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<JsonStrategy> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public WorkflowTaskPopulatorService getWorkflowTaskPopulatorService() {
		return this.workflowTaskPopulatorService;
	}


	public void setWorkflowTaskPopulatorService(WorkflowTaskPopulatorService workflowTaskPopulatorService) {
		this.workflowTaskPopulatorService = workflowTaskPopulatorService;
	}


	public WorkflowTaskStepService getWorkflowTaskStepService() {
		return this.workflowTaskStepService;
	}


	public void setWorkflowTaskStepService(WorkflowTaskStepService workflowTaskStepService) {
		this.workflowTaskStepService = workflowTaskStepService;
	}


	public DaoNamedEntityCache<WorkflowTaskCategory> getWorkflowTaskCategoryCache() {
		return this.workflowTaskCategoryCache;
	}


	public void setWorkflowTaskCategoryCache(DaoNamedEntityCache<WorkflowTaskCategory> workflowTaskCategoryCache) {
		this.workflowTaskCategoryCache = workflowTaskCategoryCache;
	}


	public DaoSingleKeyListCache<WorkflowTaskDefinitionLock, Short> getWorkflowTaskDefinitionLockListByLockTableCache() {
		return this.workflowTaskDefinitionLockListByLockTableCache;
	}


	public void setWorkflowTaskDefinitionLockListByLockTableCache(DaoSingleKeyListCache<WorkflowTaskDefinitionLock, Short> workflowTaskDefinitionLockListByLockTableCache) {
		this.workflowTaskDefinitionLockListByLockTableCache = workflowTaskDefinitionLockListByLockTableCache;
	}
}
