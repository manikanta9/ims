package com.clifton.workflow.task;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.priority.SystemPriority;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.task.populator.approver.WorkflowTaskApproverRetriever;

import java.util.List;


/**
 * The WorkflowTaskDefinition class is used to define attributes, life-cycle, and logic used to generate {@link WorkflowTask} entities.
 *
 * @author vgomelsky
 */
public class WorkflowTaskDefinition extends NamedEntityWithoutLabel<Short> implements SystemEntityModifyConditionAwareAdminRequired {

	private WorkflowTaskCategory category;

	/**
	 * Each WorkflowTask of this definition will transition through this workflow.
	 */
	private Workflow workflow;

	/**
	 * When a task is completed, it transitions into this "Final" Workflow Status.
	 * Workflow tasks in this workflow status are considered done and are no longer used to determine if an entity is locked.
	 * There cannot be more than one non final tasks for the same entity at the same time.
	 * Final status can correspond to Finished or Cancelled workflow state.
	 */
	private WorkflowStatus finalWorkflowStatus;

	/**
	 * When a task is in this Workflow Status, then it is waiting on the Assignee to do the work.
	 * Usually this is "Open" workflow status.
	 * This field can be used to find assignee to do list: (Assignee = User AND Workflow Status = Assignee Workflow Status)
	 * and approver's to do list: (Approver = User AND Workflow Status NOT IN (Assignee Workflow Status, Final Workflow Status)
	 */
	private WorkflowStatus assigneeWorkflowStatus;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * Identifies the table that will have entities that WorkflowTask objects will be created for.
	 * For example, "InvestmentAccount" for requesting changes to account's asset classes.
	 */
	private SystemTable linkedEntityTable;
	/**
	 * Optionally can be used to override linkedEntityTable.detailPageClass
	 */
	private String linkedEntityWindowClass;

	/**
	 * For example, investmentAccountListFind.json?ourAccount=true
	 */
	private String linkedEntityListURL;

	/**
	 * A task definition can optionally be created for a specific entity: quarterly task for a specific client, etc.
	 * If this is the case, then the tasks for this definition must hav ethe same linked entity id and label.
	 */
	@SoftLinkField(tableBeanPropertyName = "linkedEntityTable")
	private Long linkedEntityFkFieldId;
	private String linkedEntityLabel;

	/**
	 * Optionally specifies the window name/class/default data for easy navigation to related window.
	 * For example, for a task to review Compliance Rule Runs, this link can directly take the user to corresponding section.
	 */
	private String linkedWindowName;
	private String linkedWindowClass;
	private String linkedWindowDefaultData;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * For ad-hoc tasks, this field will be blank. It will be populated for periodic and other task types and will reference
	 * {@link com.clifton.workflow.task.generator.WorkflowTaskGenerator} bean that contains the logic used to generate
	 * corresponding tasks and steps.
	 */
	private SystemBean taskGeneratorBean;

	/**
	 * When specified, will not allow for a new task to be generated with Due Date that is more than this positive number of days into the future.
	 * This option is used both during task creation and during automated task generation.
	 */
	private Integer generateDueDateDaysFromToday;

	/**
	 * Specifies whether Task "Due Date" field can be changed. Note that Original Due Date field can never be modified.
	 */
	private boolean changeToDueDateAllowed;

	/**
	 * When Lock and Linked entities are the same, specifies whether a new WorkflowTask should be generated automatically on entity insert.
	 * For example, when a new InvestmentSecurity is created, automatically create a task requiring review and approval.
	 */
	private boolean generateOnEntityCreation;


	/**
	 * Optional user that all tasks of this definition should be assigned to.
	 * When set, this user will be used to default task Assignee during task creation.
	 */
	private SecurityUser assigneeUser;
	/**
	 * Optional user group that all tasks of tis definition should be assigned to.
	 * When set, this user will be used to populate task Assignee Group during task creation.
	 */
	private SecurityGroup assigneeGroup;

	/**
	 * When {@link #assigneeGroup} is specified, optionally requires {@link #assigneeUser} to belong to this group.
	 */
	private boolean assigneeRequiredToBeInAssigneeGroup;

	/**
	 * Optional bean that can retrieve Approver for the task based con custom logic. This logic takes precedence
	 * over approverUser and secondaryApprover user fields.
	 * The bean must implement {@link WorkflowTaskApproverRetriever} interface.
	 */
	private SystemBean approverUserRetrieverBean;
	/**
	 * Optional user that is responsible for approving/reviewing corresponding workflow tasks.
	 * When set, this field will be used to default task Approver during task creation.
	 */
	private SecurityUser approverUser;
	/**
	 * When "approverUser" is specified, one can also optionally define the Secondary Approver.
	 * This approver will be used when task Assignee is the same person as primary Approver.
	 */
	private SecurityUser secondaryApproverUser;
	/**
	 * Optional user group that is responsible for reviewing/approving corresponding workflow tasks.
	 * When set, this field will be used to populate task Assignee Group during task creation.
	 */
	private SecurityGroup approverGroup;

	/**
	 * When {@link #approverGroup} is specified, optionally requires {@link #approverUser} to belong to this group.
	 */
	private boolean approverRequiredToBeInApproverGroup;

	/**
	 * It's generally considered to be bad practice to allow Approve and Assignee to be the same person.
	 * However, in rare temporary instances (there is only one person who can do the job) this may be allowed.
	 */
	private boolean sameAssigneeAndApproverAllowed;


	/**
	 * All tasks for this definition will be assigned the following priority
	 */
	private SystemPriority priority;
	/**
	 * Optionally restrict who can create and modify tasks for this definition.
	 * This condition can also restrict who is allowed to be Assignee and Approver for the task, etc.
	 */
	private SystemCondition taskEntityModifyCondition;

	// specifies whether tasks of this definition can have steps
	private boolean taskStepAllowed;

	/**
	 * For auto-generated tasks that allow name, one can optionally use this template field to automatically generate task name.
	 * "task" and "linkedEntity" beans are placed in the context and can be used during name generation.
	 * For example, "January 2016" based on task's due date.
	 */
	private String taskNameTemplate;
	private boolean taskNameAllowed;
	private boolean taskNameRequired;
	private boolean taskDescriptionRequired;

	private boolean active;


	/**
	 * A single task definition may lock multiple tables. Specifies whether any locks are present and if so, then WorkflowTaskDefinitionLock
	 * entries will contain additional details.
	 * All locked entities will remain locked (read only) until corresponding task is transitioned to this unlock workflow status.
	 */
	private WorkflowStatus unlockWorkflowStatus;

	// lock list is required when unlockWorkflowStatus is populated
	@OneToManyEntity(serviceBeanName = "workflowTaskService", serviceMethodName = "getWorkflowTaskDefinitionLockListForDefinition")
	private List<WorkflowTaskDefinitionLock> lockList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Can be used to restrict access to specific user groups as well as a sub-set of fields (name/description, etc.)
	 */
	@Override
	public SystemCondition getEntityModifyCondition() {
		return (this.category == null) ? null : this.category.getDefinitionEntityModifyCondition();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true for a definition that is linked to a specific entity: all task must be for that entity only.
	 */
	public boolean isForSpecificEntity() {
		return (getLinkedEntityFkFieldId() != null);
	}


	/**
	 * Returns true if this task definition locks one or more tables.
	 */
	public boolean isLocked() {
		return (getUnlockWorkflowStatus() != null);
	}


	/**
	 * Returns the {@link WorkflowTaskDefinitionLock} from this task definition that locks the table with the specified name.
	 */
	public WorkflowTaskDefinitionLock getTaskDefinitionLock(String lockEntityTableName) {
		for (WorkflowTaskDefinitionLock lock : CollectionUtils.getIterable(getLockList())) {
			if (lock.getLockEntityTable().getName().equals(lockEntityTableName)) {
				return lock;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabelLong() {
		return getCategory() == null ? getName() : getName() + " (" + getCategory().getName() + ")";
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskCategory getCategory() {
		return this.category;
	}


	public void setCategory(WorkflowTaskCategory category) {
		this.category = category;
	}


	public Workflow getWorkflow() {
		return this.workflow;
	}


	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}


	public SystemBean getTaskGeneratorBean() {
		return this.taskGeneratorBean;
	}


	public void setTaskGeneratorBean(SystemBean taskGeneratorBean) {
		this.taskGeneratorBean = taskGeneratorBean;
	}


	public Integer getGenerateDueDateDaysFromToday() {
		return this.generateDueDateDaysFromToday;
	}


	public void setGenerateDueDateDaysFromToday(Integer generateDueDateDaysFromToday) {
		this.generateDueDateDaysFromToday = generateDueDateDaysFromToday;
	}


	public boolean isChangeToDueDateAllowed() {
		return this.changeToDueDateAllowed;
	}


	public void setChangeToDueDateAllowed(boolean changeToDueDateAllowed) {
		this.changeToDueDateAllowed = changeToDueDateAllowed;
	}


	public boolean isGenerateOnEntityCreation() {
		return this.generateOnEntityCreation;
	}


	public void setGenerateOnEntityCreation(boolean generateOnEntityCreation) {
		this.generateOnEntityCreation = generateOnEntityCreation;
	}


	public SystemTable getLinkedEntityTable() {
		return this.linkedEntityTable;
	}


	public void setLinkedEntityTable(SystemTable linkedEntityTable) {
		this.linkedEntityTable = linkedEntityTable;
	}


	public String getLinkedEntityWindowClass() {
		return this.linkedEntityWindowClass;
	}


	public void setLinkedEntityWindowClass(String linkedEntityWindowClass) {
		this.linkedEntityWindowClass = linkedEntityWindowClass;
	}


	public String getLinkedEntityListURL() {
		return this.linkedEntityListURL;
	}


	public void setLinkedEntityListURL(String linkedEntityListURL) {
		this.linkedEntityListURL = linkedEntityListURL;
	}


	public Long getLinkedEntityFkFieldId() {
		return this.linkedEntityFkFieldId;
	}


	public void setLinkedEntityFkFieldId(Long linkedEntityFkFieldId) {
		this.linkedEntityFkFieldId = linkedEntityFkFieldId;
	}


	public String getLinkedEntityLabel() {
		return this.linkedEntityLabel;
	}


	public void setLinkedEntityLabel(String linkedEntityLabel) {
		this.linkedEntityLabel = linkedEntityLabel;
	}


	public String getLinkedWindowName() {
		return this.linkedWindowName;
	}


	public void setLinkedWindowName(String linkedWindowName) {
		this.linkedWindowName = linkedWindowName;
	}


	public String getLinkedWindowClass() {
		return this.linkedWindowClass;
	}


	public void setLinkedWindowClass(String linkedWindowClass) {
		this.linkedWindowClass = linkedWindowClass;
	}


	public String getLinkedWindowDefaultData() {
		return this.linkedWindowDefaultData;
	}


	public void setLinkedWindowDefaultData(String linkedWindowDefaultData) {
		this.linkedWindowDefaultData = linkedWindowDefaultData;
	}


	public WorkflowStatus getUnlockWorkflowStatus() {
		return this.unlockWorkflowStatus;
	}


	public void setUnlockWorkflowStatus(WorkflowStatus unlockWorkflowStatus) {
		this.unlockWorkflowStatus = unlockWorkflowStatus;
	}


	public List<WorkflowTaskDefinitionLock> getLockList() {
		return this.lockList;
	}


	public void setLockList(List<WorkflowTaskDefinitionLock> lockList) {
		this.lockList = lockList;
	}


	public WorkflowStatus getFinalWorkflowStatus() {
		return this.finalWorkflowStatus;
	}


	public void setFinalWorkflowStatus(WorkflowStatus finalWorkflowStatus) {
		this.finalWorkflowStatus = finalWorkflowStatus;
	}


	public WorkflowStatus getAssigneeWorkflowStatus() {
		return this.assigneeWorkflowStatus;
	}


	public void setAssigneeWorkflowStatus(WorkflowStatus assigneeWorkflowStatus) {
		this.assigneeWorkflowStatus = assigneeWorkflowStatus;
	}


	public SystemCondition getTaskEntityModifyCondition() {
		return this.taskEntityModifyCondition;
	}


	public void setTaskEntityModifyCondition(SystemCondition taskEntityModifyCondition) {
		this.taskEntityModifyCondition = taskEntityModifyCondition;
	}


	public SecurityUser getAssigneeUser() {
		return this.assigneeUser;
	}


	public void setAssigneeUser(SecurityUser assigneeUser) {
		this.assigneeUser = assigneeUser;
	}


	public SecurityGroup getAssigneeGroup() {
		return this.assigneeGroup;
	}


	public void setAssigneeGroup(SecurityGroup assigneeGroup) {
		this.assigneeGroup = assigneeGroup;
	}


	public boolean isAssigneeRequiredToBeInAssigneeGroup() {
		return this.assigneeRequiredToBeInAssigneeGroup;
	}


	public void setAssigneeRequiredToBeInAssigneeGroup(boolean assigneeRequiredToBeInAssigneeGroup) {
		this.assigneeRequiredToBeInAssigneeGroup = assigneeRequiredToBeInAssigneeGroup;
	}


	public SystemBean getApproverUserRetrieverBean() {
		return this.approverUserRetrieverBean;
	}


	public void setApproverUserRetrieverBean(SystemBean approverUserRetrieverBean) {
		this.approverUserRetrieverBean = approverUserRetrieverBean;
	}


	public SecurityUser getApproverUser() {
		return this.approverUser;
	}


	public void setApproverUser(SecurityUser approverUser) {
		this.approverUser = approverUser;
	}


	public SecurityUser getSecondaryApproverUser() {
		return this.secondaryApproverUser;
	}


	public void setSecondaryApproverUser(SecurityUser secondaryApproverUser) {
		this.secondaryApproverUser = secondaryApproverUser;
	}


	public SecurityGroup getApproverGroup() {
		return this.approverGroup;
	}


	public void setApproverGroup(SecurityGroup approverGroup) {
		this.approverGroup = approverGroup;
	}


	public boolean isApproverRequiredToBeInApproverGroup() {
		return this.approverRequiredToBeInApproverGroup;
	}


	public void setApproverRequiredToBeInApproverGroup(boolean approverRequiredToBeInApproverGroup) {
		this.approverRequiredToBeInApproverGroup = approverRequiredToBeInApproverGroup;
	}


	public boolean isSameAssigneeAndApproverAllowed() {
		return this.sameAssigneeAndApproverAllowed;
	}


	public void setSameAssigneeAndApproverAllowed(boolean sameAssigneeAndApproverAllowed) {
		this.sameAssigneeAndApproverAllowed = sameAssigneeAndApproverAllowed;
	}


	public SystemPriority getPriority() {
		return this.priority;
	}


	public void setPriority(SystemPriority priority) {
		this.priority = priority;
	}


	public String getTaskNameTemplate() {
		return this.taskNameTemplate;
	}


	public void setTaskNameTemplate(String taskNameTemplate) {
		this.taskNameTemplate = taskNameTemplate;
	}


	public boolean isTaskNameAllowed() {
		return this.taskNameAllowed;
	}


	public void setTaskNameAllowed(boolean taskNameAllowed) {
		this.taskNameAllowed = taskNameAllowed;
	}


	public boolean isTaskNameRequired() {
		return this.taskNameRequired;
	}


	public void setTaskNameRequired(boolean taskNameRequired) {
		this.taskNameRequired = taskNameRequired;
	}


	public boolean isTaskDescriptionRequired() {
		return this.taskDescriptionRequired;
	}


	public void setTaskDescriptionRequired(boolean taskDescriptionRequired) {
		this.taskDescriptionRequired = taskDescriptionRequired;
	}


	public boolean isTaskStepAllowed() {
		return this.taskStepAllowed;
	}


	public void setTaskStepAllowed(boolean taskStepAllowed) {
		this.taskStepAllowed = taskStepAllowed;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}
}
