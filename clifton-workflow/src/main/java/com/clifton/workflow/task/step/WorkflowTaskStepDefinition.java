package com.clifton.workflow.task.step;

import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;
import com.clifton.workflow.task.WorkflowTaskDefinition;


/**
 * The WorkflowTaskStepDefinition class represents a single step definition of a specific tasks.
 * Many workflow tasks do not have any steps. However, for more complex tasks it might be
 * important to keep track of individual steps that could be assigned to different people.
 *
 * @author vgomelsky
 */
public class WorkflowTaskStepDefinition extends NamedEntityWithoutLabel<Short> implements SystemEntityModifyConditionAwareAdminRequired {

	private WorkflowTaskDefinition taskDefinition;

	/**
	 * Identifies optional table that will have entities that WorkflowTaskStep objects will be created for.
	 */
	private SystemTable linkedEntityTable;
	/**
	 * For example, investmentAccountListFind.json?ourAccount=true
	 */
	private String linkedEntityListURL;


	/**
	 * Optional user that all steps of this definition should be assigned to.
	 * When set, this user will be used to default step Assignee during task creation.
	 */
	private SecurityUser assigneeUser;

	/**
	 * The {@link DateGenerationOptions} used to generate due date value for the step.  This
	 * is used in conjunction with the stepDueDateDaysFromTaskDueDate property.
	 */
	private DateGenerationOptions stepDueDateGenerationOption;
	/**
	 * The number of days to use when generating step's due date.  Positive numbers go forward and negative go backward.
	 * Only negative numbers of 0 are allowed because a step can never be due after corresponding task.
	 */
	private Integer stepDueDateDaysFromTaskDueDate;

	/**
	 * Specifies whether Task "Due Date" field can be changed. Note that Original Due Date field can never be modified.
	 */
	private boolean changeToDueDateAllowed;

	private boolean stepNameRequired;

	/**
	 * Allows sorting task steps using custom ordering
	 */
	private short order;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Can be used to restrict access to specific user groups as well as a sub-set of fields (name/description, etc.)
	 */
	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getTaskDefinition() != null && getTaskDefinition().getCategory() != null) {
			return getTaskDefinition().getCategory().getDefinitionEntityModifyCondition();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskDefinition getTaskDefinition() {
		return this.taskDefinition;
	}


	public void setTaskDefinition(WorkflowTaskDefinition taskDefinition) {
		this.taskDefinition = taskDefinition;
	}


	public SystemTable getLinkedEntityTable() {
		return this.linkedEntityTable;
	}


	public void setLinkedEntityTable(SystemTable linkedEntityTable) {
		this.linkedEntityTable = linkedEntityTable;
	}


	public String getLinkedEntityListURL() {
		return this.linkedEntityListURL;
	}


	public void setLinkedEntityListURL(String linkedEntityListURL) {
		this.linkedEntityListURL = linkedEntityListURL;
	}


	public DateGenerationOptions getStepDueDateGenerationOption() {
		return this.stepDueDateGenerationOption;
	}


	public void setStepDueDateGenerationOption(DateGenerationOptions stepDueDateGenerationOption) {
		this.stepDueDateGenerationOption = stepDueDateGenerationOption;
	}


	public Integer getStepDueDateDaysFromTaskDueDate() {
		return this.stepDueDateDaysFromTaskDueDate;
	}


	public void setStepDueDateDaysFromTaskDueDate(Integer stepDueDateDaysFromTaskDueDate) {
		this.stepDueDateDaysFromTaskDueDate = stepDueDateDaysFromTaskDueDate;
	}


	public boolean isChangeToDueDateAllowed() {
		return this.changeToDueDateAllowed;
	}


	public void setChangeToDueDateAllowed(boolean changeToDueDateAllowed) {
		this.changeToDueDateAllowed = changeToDueDateAllowed;
	}


	public boolean isStepNameRequired() {
		return this.stepNameRequired;
	}


	public void setStepNameRequired(boolean stepNameRequired) {
		this.stepNameRequired = stepNameRequired;
	}


	public short getOrder() {
		return this.order;
	}


	public void setOrder(short order) {
		this.order = order;
	}


	public SecurityUser getAssigneeUser() {
		return this.assigneeUser;
	}


	public void setAssigneeUser(SecurityUser assigneeUser) {
		this.assigneeUser = assigneeUser;
	}
}
