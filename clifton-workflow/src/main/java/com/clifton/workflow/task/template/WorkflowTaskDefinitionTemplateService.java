package com.clifton.workflow.task.template;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.template.search.WorkflowTaskDefinitionTemplateSearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The WorkflowTaskDefinitionTemplateService interface defines methods for working with task definition templates as well as for
 * creating task definitions from corresponding templates.
 *
 * @author vgomelsky
 */
public interface WorkflowTaskDefinitionTemplateService {


	public WorkflowTaskDefinitionTemplate getWorkflowTaskDefinitionTemplate(short id);


	public List<WorkflowTaskDefinitionTemplate> getWorkflowTaskDefinitionTemplateList(WorkflowTaskDefinitionTemplateSearchForm searchForm);


	public WorkflowTaskDefinitionTemplate saveWorkflowTaskDefinitionTemplate(WorkflowTaskDefinitionTemplate template);


	public void deleteWorkflowTaskDefinitionTemplate(short id);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Create and return a new {@link WorkflowTaskDefinition} object from the specified template.
	 */
	@ModelAttribute("data")
	@SecureMethod(dtoClass = WorkflowTaskDefinitionTemplate.class, permissions = SecurityPermission.PERMISSION_EXECUTE)
	public WorkflowTaskDefinition createWorkflowTaskDefinitionFromTemplate(DefinitionCreationTemplate creationTemplate);
}
