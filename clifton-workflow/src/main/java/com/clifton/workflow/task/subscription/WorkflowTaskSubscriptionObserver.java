package com.clifton.workflow.task.subscription;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.websocket.WebSocketHandler;
import com.clifton.workflow.task.WorkflowTask;
import org.springframework.stereotype.Component;

import java.util.Collection;


/**
 * The DAO observer for {@link WorkflowTask} subscription operations.
 * <p>
 * This observer uses {@link WebSocketHandler WebSockets} to send messages to subscribed clients. Clients may subscribe through the channels listed in {@link
 * WorkflowTaskSubscriptionService}.
 *
 * @author MikeH
 */
@Component
public class WorkflowTaskSubscriptionObserver extends SelfRegisteringDaoObserver<WorkflowTask> {

	private WebSocketHandler webSocketHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		// Include all event updates; these may include insertions, deletions, or workflow state updates
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<WorkflowTask> dao, DaoEventTypes event, WorkflowTask bean) {
		// Cache reference to original bean
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<WorkflowTask> dao, DaoEventTypes event, WorkflowTask newTask, Throwable e) {
		// Do not run observer during exceptions
		if (e != null) {
			return;
		}

		// Differentiate the current and former active user for the task
		WorkflowTask originalTask = event.isUpdate() ? getOriginalBean(dao, newTask) : null;
		SecurityUser originalActiveUser = originalTask != null ? getActiveUser(originalTask) : null;
		SecurityUser newActiveUser = getActiveUser(newTask);

		// Send updates to subscribed users
		sendWorkflowTaskUpdates(originalActiveUser, newActiveUser, event);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sends workflow task detail updates to the specified users.
	 * <p>
	 * For each user affected by this operation, this method sends a {@link WorkflowTaskSubscriptionMessage STOMP message} indicating the change which applies to the user.
	 *
	 * @param originalUser the previous active user for the task
	 * @param newUser      the new active user for the task
	 * @param event        the type of task modification event which took place
	 */
	private void sendWorkflowTaskUpdates(SecurityUser originalUser, SecurityUser newUser, DaoEventTypes event) {
		// Guard-clause: No updates apply if the active user has not changed
		if (CompareUtils.isEqual(originalUser, newUser)) {
			return;
		}

		Collection<SecurityUser> subscribedUserList = getWebSocketHandler().getSubscribedUserList(WorkflowTaskSubscriptionService.CHANNEL_USER_TOPIC_WORKFLOW_TASK_COUNT);

		// Send updates to formerly-active user
		if (originalUser != null && subscribedUserList.contains(originalUser)) {
			getWebSocketHandler().sendMessageToUser(originalUser, WorkflowTaskSubscriptionService.CHANNEL_USER_TOPIC_WORKFLOW_TASK_COUNT, WorkflowTaskSubscriptionMessage.DECREMENT);
		}

		// Send updates to newly-active user
		if (newUser != null && subscribedUserList.contains(newUser)) {
			WorkflowTaskSubscriptionMessage message = event.isDelete()
					? WorkflowTaskSubscriptionMessage.DECREMENT
					: WorkflowTaskSubscriptionMessage.INCREMENT;
			getWebSocketHandler().sendMessageToUser(newUser, WorkflowTaskSubscriptionService.CHANNEL_USER_TOPIC_WORKFLOW_TASK_COUNT, message);
		}
	}


	/**
	 * Gets the active user for the given task. This is the user from whom the workflow task is currently pending activity.
	 *
	 * @param task the task for which to get the active user
	 * @return the active user for the task, or {@code null} if no user is active for the task
	 */
	private SecurityUser getActiveUser(WorkflowTask task) {
		final SecurityUser activeUser;
		if (CompareUtils.isEqual(task.getWorkflowStatus(), task.getDefinition().getAssigneeWorkflowStatus())) {
			// Assignee is active
			activeUser = task.getAssigneeUser();
		}
		else if (!CompareUtils.isEqual(task.getWorkflowStatus(), task.getDefinition().getFinalWorkflowStatus())) {
			// Approver is active
			activeUser = task.getApproverUser();
		}
		else {
			// No user is active
			activeUser = null;
		}
		return activeUser;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}


	public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = webSocketHandler;
	}
}
