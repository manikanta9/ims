package com.clifton.workflow.task.template.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class WorkflowTaskDefinitionTemplateSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;
	@SearchField
	private String description;

	@SearchField
	private String defaultName;
	@SearchField
	private String defaultDescription;

	@SearchField(searchField = "templateDefinition.id")
	private Short templateDefinitionId;

	@SearchField(searchField = "taskDefinitionCreateCondition.id")
	private Integer taskDefinitionCreateConditionId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDefaultName() {
		return this.defaultName;
	}


	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}


	public String getDefaultDescription() {
		return this.defaultDescription;
	}


	public void setDefaultDescription(String defaultDescription) {
		this.defaultDescription = defaultDescription;
	}


	public Short getTemplateDefinitionId() {
		return this.templateDefinitionId;
	}


	public void setTemplateDefinitionId(Short templateDefinitionId) {
		this.templateDefinitionId = templateDefinitionId;
	}


	public Integer getTaskDefinitionCreateConditionId() {
		return this.taskDefinitionCreateConditionId;
	}


	public void setTaskDefinitionCreateConditionId(Integer taskDefinitionCreateConditionId) {
		this.taskDefinitionCreateConditionId = taskDefinitionCreateConditionId;
	}
}
