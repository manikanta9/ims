package com.clifton.workflow.task.step;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.step.search.WorkflowTaskStepDefinitionSearchForm;
import com.clifton.workflow.task.step.search.WorkflowTaskStepDependencySearchForm;
import com.clifton.workflow.task.step.search.WorkflowTaskStepSearchForm;
import com.clifton.workflow.task.step.validation.WorkflowTaskStepValidator;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class WorkflowTaskStepServiceImpl implements WorkflowTaskStepService {

	private AdvancedUpdatableDAO<WorkflowTaskStep, Criteria> workflowTaskStepDAO;
	private AdvancedUpdatableDAO<WorkflowTaskStepDefinition, Criteria> workflowTaskStepDefinitionDAO;
	private AdvancedUpdatableDAO<WorkflowTaskStepDependency, Criteria> workflowTaskStepDependencyDAO;

	////////////////////////////////////////////////////////////////////////////
	////////        Workflow Task Step Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTaskStep getWorkflowTaskStep(int id) {
		return getWorkflowTaskStepDAO().findByPrimaryKey(id);
	}


	@Override
	public List<WorkflowTaskStep> getWorkflowTaskStepListByTask(int workflowTaskId) {
		return getWorkflowTaskStepDAO().findByField("task.id", workflowTaskId);
	}


	@Override
	public List<WorkflowTaskStep> getWorkflowTaskStepList(WorkflowTaskStepSearchForm searchForm) {
		return getWorkflowTaskStepDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see WorkflowTaskStepValidator
	 */
	@Override
	public WorkflowTaskStep saveWorkflowTaskStep(WorkflowTaskStep step) {
		return getWorkflowTaskStepDAO().save(step);
	}


	/**
	 * @see WorkflowTaskStepValidator
	 */
	@Override
	public void deleteWorkflowTaskStep(int id) {
		getWorkflowTaskStepDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////   Workflow Task Step Definition Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTaskStepDefinition getWorkflowTaskStepDefinition(short id) {
		return getWorkflowTaskStepDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<WorkflowTaskStepDefinition> getWorkflowTaskStepDefinitionListByTaskDefinition(short taskDefinitionId) {
		return getWorkflowTaskStepDefinitionDAO().findByField("taskDefinition.id", taskDefinitionId);
	}


	@Override
	public List<WorkflowTaskStepDefinition> getWorkflowTaskStepDefinitionList(WorkflowTaskStepDefinitionSearchForm searchForm) {
		return getWorkflowTaskStepDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public WorkflowTaskStepDefinition saveWorkflowTaskStepDefinition(WorkflowTaskStepDefinition stepDefinition) {
		WorkflowTaskDefinition taskDefinition = stepDefinition.getTaskDefinition();
		ValidationUtils.assertTrue(taskDefinition.isTaskStepAllowed(), "Task Steps are not allowed for this Task Definition.");

		return getWorkflowTaskStepDefinitionDAO().save(stepDefinition);
	}


	@Override
	public void deleteWorkflowTaskStepDefinition(short id) {
		getWorkflowTaskStepDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////    Workflow Task Step Dependency Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTaskStepDependency getWorkflowTaskStepDependency(short id) {
		return getWorkflowTaskStepDependencyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<WorkflowTaskStepDependency> getWorkflowTaskStepDependencyList(WorkflowTaskStepDependencySearchForm searchForm) {
		return getWorkflowTaskStepDependencyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public WorkflowTaskStepDependency saveWorkflowTaskStepDependency(WorkflowTaskStepDependency dependency) {
		return getWorkflowTaskStepDependencyDAO().save(dependency);
	}


	@Override
	public void deleteWorkflowTaskStepDependency(short id) {
		getWorkflowTaskStepDependencyDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<WorkflowTaskStep, Criteria> getWorkflowTaskStepDAO() {
		return this.workflowTaskStepDAO;
	}


	public void setWorkflowTaskStepDAO(AdvancedUpdatableDAO<WorkflowTaskStep, Criteria> workflowTaskStepDAO) {
		this.workflowTaskStepDAO = workflowTaskStepDAO;
	}


	public AdvancedUpdatableDAO<WorkflowTaskStepDefinition, Criteria> getWorkflowTaskStepDefinitionDAO() {
		return this.workflowTaskStepDefinitionDAO;
	}


	public void setWorkflowTaskStepDefinitionDAO(AdvancedUpdatableDAO<WorkflowTaskStepDefinition, Criteria> workflowTaskStepDefinitionDAO) {
		this.workflowTaskStepDefinitionDAO = workflowTaskStepDefinitionDAO;
	}


	public AdvancedUpdatableDAO<WorkflowTaskStepDependency, Criteria> getWorkflowTaskStepDependencyDAO() {
		return this.workflowTaskStepDependencyDAO;
	}


	public void setWorkflowTaskStepDependencyDAO(AdvancedUpdatableDAO<WorkflowTaskStepDependency, Criteria> workflowTaskStepDependencyDAO) {
		this.workflowTaskStepDependencyDAO = workflowTaskStepDependencyDAO;
	}
}
