package com.clifton.workflow.task.action;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.lock.WorkflowTaskLockObserver;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.io.Serializable;
import java.lang.reflect.Method;


/**
 * The WorkflowTaskDeleteLinkedEntityAction class is a workflow transition action that deletes the entity linked to corresponding workflow task.
 * Delete service and method names can be configured on the action or dynamically retrieved based on corresponding DTO configuration.
 *
 * @author vgomelsky
 */
public class WorkflowTaskDeleteLinkedEntityAction implements WorkflowTransitionActionHandler<WorkflowTask>, ValidationAware {

	/**
	 * The name of Spring managed bean that has the delete method.  Skip to retrieve dynamically based on DTO configuration.
	 */
	private String serviceBeanName;
	/**
	 * The name of the delete method on Spring managed bean defined by serviceBeanName.  Skip to retrieve dynamically based on DTO configuration.
	 */
	private String serviceDeleteMethodName;


	private DaoLocator daoLocator;
	private ApplicationContextService applicationContextService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		if (!StringUtils.isEmpty(getServiceDeleteMethodName())) {
			ValidationUtils.assertNotEmpty(getServiceBeanName(), "'serviceBeanName' must be specified when 'serviceDeleteMethodName' is specified.");
		}
	}


	@Override
	public WorkflowTask processAction(WorkflowTask task, WorkflowTransition transition) {
		SystemTable linkedTable = task.getDefinition().getLinkedEntityTable();
		ValidationUtils.assertNotNull(linkedTable, "Cannot delete Linked Entity for Workflow Task because no Linked Entity Table is defined on corresponding Workflow Task Definition: " + task.getDefinition());
		ValidationUtils.assertNotNull(task.getLinkedEntityFkFieldId(), "Cannot delete Linked Entity because its id is not defined on Workflow Task: " + task);

		ReadOnlyDAO<?> dao = getDaoLocator().locate(linkedTable.getName());
		DAOConfiguration<?> config = dao.getConfiguration();
		Serializable entityId = dao.convertToPrimaryKeyDataType(task.getLinkedEntityFkFieldId());

		// Get service bean
		String beanName = StringUtils.coalesce(getServiceBeanName(), config.getTable().getServiceBeanName());
		ValidationUtils.assertNotEmpty(beanName, "'serviceBeanName' is not specified on the workflow action or the table configuration for " + linkedTable.getName());
		Object deleteService = getApplicationContextService().getContextBean(beanName);
		AssertUtils.assertNotNull(deleteService, "Cannot find bean with name = '%s' in application context for deletes to table [%s].", beanName, config.getTableName());

		// Get service delete method
		String methodName = getServiceDeleteMethodName();
		if (StringUtils.isEmpty(methodName)) {
			// try to lookup the method dynamically based on table configuration
			Table table = config.getTable();
			methodName = table.getServiceDeleteMethodName();
			if (StringUtils.isEmpty(table.getServiceDeleteMethodName())) {
				methodName = ContextConventionUtils.getDeleteMethodNameFromClass(config.getBeanClass());
			}
		}
		ValidationUtils.assertNotEmpty(methodName, "'serviceDeleteMethodName' is not specified on the workflow action or the table configuration for " + linkedTable.getName());
		Method deleteMethod = MethodUtils.getMethod(deleteService.getClass(), methodName, ClassUtils.wrapperToPrimitive(entityId.getClass()));
		AssertUtils.assertNotNull(deleteMethod, "Cannot find method '%s' on bean '%s' for '%s' delete.", methodName, beanName, config.getTableName());

		DaoUtils.executeWithSpecificObserversDisabled(() -> MethodUtils.invoke(deleteMethod, deleteService, entityId), WorkflowTaskLockObserver.class);

		return task;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getServiceBeanName() {
		return this.serviceBeanName;
	}


	public void setServiceBeanName(String serviceBeanName) {
		this.serviceBeanName = serviceBeanName;
	}


	public String getServiceDeleteMethodName() {
		return this.serviceDeleteMethodName;
	}


	public void setServiceDeleteMethodName(String serviceDeleteMethodName) {
		this.serviceDeleteMethodName = serviceDeleteMethodName;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
