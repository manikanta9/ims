package com.clifton.workflow.task;

import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.date.DateUtils;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.priority.SystemPriority;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.workflow.BasedNamedWorkflowAwareEntity;
import com.clifton.workflow.WorkflowConfig;
import com.clifton.workflow.task.step.WorkflowTaskStep;

import java.util.Date;
import java.util.List;


/**
 * The WorkflowTask class represents a specific instance of a task that's being transition through corresponding workflow.
 * For example, a specific request to update asset class targets for a specific client account.
 * <p>
 * Tasks can be used to track an ad-hoc or regularly scheduled business process as well as to "lock" changes to entities
 * in the system and only allow changes according to corresponding business process defined via workflow and often containing
 * independent approvals and reviews.
 *
 * @author vgomelsky
 */
@WorkflowConfig(workflowBeanPropertyName = "definition.workflow")
public class WorkflowTask extends BasedNamedWorkflowAwareEntity implements SystemColumnCustomJsonAware, SystemEntityModifyConditionAware {

	public static final String CUSTOM_FIELDS_GROUP_NAME = "Workflow Task Custom Fields";
	public static final String TABLE_NAME = "WorkflowTask";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private WorkflowTaskDefinition definition;

	/**
	 * Identifies the entity that the task is for. Corresponding taskTable is defined by WorkflowTaskDefinition.
	 */
	@SoftLinkField(tableBeanPropertyName = "definition.linkedEntityTable")
	private Long linkedEntityFkFieldId;
	private String linkedEntityLabel;

	private SecurityUser assigneeUser;
	/**
	 * Assignee Group is only used when Assignee User is not populated. When Assignee User is populated, Assignee Group field will be ignored.
	 */
	private SecurityGroup assigneeGroup;

	private SecurityUser approverUser;
	/**
	 * Approver Group is only used when Approver User is not populated. When Approver User is populated, Approver Group field will be ignored.
	 */
	private SecurityGroup approverGroup;

	private SystemPriority priority;

	private Date dueDate;
	/**
	 * Initially, originalDueDate is always equal to dueDate. If corresponding WorkflowTaskDefinition allows changing
	 * the dueDate (changeToDueDateAllowed == true), then the user will be able to update dueDate. originalDueDate can
	 * never be changed.  It is used by task generator beans to determine if a task on that date already exists.
	 */
	private Date originalDueDate;


	@NonPersistentField
	private String columnGroupName;

	/**
	 * JSON String value representing the custom column values
	 */
	private CustomJsonString customColumns;


	private List<WorkflowTaskStep> stepList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getDefinition() != null) {
			return getDefinition().getTaskEntityModifyCondition();
		}
		return null;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(64);
		result.append('\'');
		result.append(getLabel());
		result.append('\'');
		if (getAssigneeUser() != null) {
			result.append(" assigned to ");
			result.append(getAssigneeUser().getLabel());
		}
		if (getDueDate() != null) {
			result.append(" due on ");
			result.append(DateUtils.fromDateShort(getDueDate()));
		}

		return result.toString();
	}


	@Override
	public String getLabel() {
		if (getName() != null) {
			return getName();
		}
		if (getLinkedEntityLabel() != null) {
			return getLinkedEntityLabel();
		}
		if (getDefinition() != null) {
			return getDefinition().getName();
		}
		return null;
	}


	/**
	 * COALESCE(Assignee User, Assignee Group).getLabel()
	 */
	public String getAssigneeLabel() {
		if (getAssigneeUser() != null) {
			return getAssigneeUser().getLabel();
		}
		if (getAssigneeGroup() != null) {
			return getAssigneeGroup().getLabel();
		}
		return null;
	}


	/**
	 * COALESCE(Approver User, Approver Group).getLabel()
	 */
	public String getApproverLabel() {
		if (getApproverUser() != null) {
			return getApproverUser().getLabel();
		}
		if (getApproverGroup() != null) {
			return getApproverGroup().getLabel();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A task is "Completed" if it is in the final workflow status as per its definition: Finished or Cancelled.
	 */
	public boolean isCompletedTask() {
		if (getDefinition() != null && getWorkflowStatus() != null) {
			return getWorkflowStatus().equals(getDefinition().getFinalWorkflowStatus());
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(WorkflowTaskDefinition definition) {
		this.definition = definition;
	}


	public Long getLinkedEntityFkFieldId() {
		return this.linkedEntityFkFieldId;
	}


	public void setLinkedEntityFkFieldId(Long linkedEntityFkFieldId) {
		this.linkedEntityFkFieldId = linkedEntityFkFieldId;
	}


	public String getLinkedEntityLabel() {
		return this.linkedEntityLabel;
	}


	public void setLinkedEntityLabel(String linkedEntityLabel) {
		this.linkedEntityLabel = linkedEntityLabel;
	}


	public SecurityUser getAssigneeUser() {
		return this.assigneeUser;
	}


	public void setAssigneeUser(SecurityUser assigneeUser) {
		this.assigneeUser = assigneeUser;
	}


	public SecurityGroup getAssigneeGroup() {
		return this.assigneeGroup;
	}


	public void setAssigneeGroup(SecurityGroup assigneeGroup) {
		this.assigneeGroup = assigneeGroup;
	}


	public SecurityUser getApproverUser() {
		return this.approverUser;
	}


	public void setApproverUser(SecurityUser approverUser) {
		this.approverUser = approverUser;
	}


	public SecurityGroup getApproverGroup() {
		return this.approverGroup;
	}


	public void setApproverGroup(SecurityGroup approverGroup) {
		this.approverGroup = approverGroup;
	}


	public SystemPriority getPriority() {
		return this.priority;
	}


	public void setPriority(SystemPriority priority) {
		this.priority = priority;
	}


	public Date getDueDate() {
		return this.dueDate;
	}


	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}


	public Date getOriginalDueDate() {
		return this.originalDueDate;
	}


	public void setOriginalDueDate(Date originalDueDate) {
		this.originalDueDate = originalDueDate;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public List<WorkflowTaskStep> getStepList() {
		return this.stepList;
	}


	public void setStepList(List<WorkflowTaskStep> stepList) {
		this.stepList = stepList;
	}


	public CustomJsonString getCustomColumns() {
		return this.customColumns;
	}


	public void setCustomColumns(CustomJsonString customColumns) {
		this.customColumns = customColumns;
	}
}
