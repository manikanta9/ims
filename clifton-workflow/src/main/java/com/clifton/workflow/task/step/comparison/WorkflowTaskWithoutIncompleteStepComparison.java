package com.clifton.workflow.task.step.comparison;

/**
 * /**
 * The WorkflowTaskWithoutIncompleteStepComparison class evaluates to true when the specified WorkflowTask does not have any steps that have not been completed.
 * The Comparison evaluates all steps for the task unless optional 'stepDefinitionName' is specified.
 *
 * @author vgomelsky
 */
public class WorkflowTaskWithoutIncompleteStepComparison extends WorkflowTaskHasIncompleteStepComparison {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
