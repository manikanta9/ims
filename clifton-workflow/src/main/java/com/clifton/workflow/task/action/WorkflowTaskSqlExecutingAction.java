package com.clifton.workflow.task.action;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.sql.SqlHandlerLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.ArrayList;
import java.util.List;


/**
 * Executes one or more sql statements separated by a semi-colon.
 * If the SystemNoteType is set for the workflow task, a SystemNote is generated for each query.
 * If the SystemNoteType is not set, we only log the results.
 * If one query fails, it all fails.
 * <p>
 * <p>
 *
 * @author KellyJ
 */
public class WorkflowTaskSqlExecutingAction implements WorkflowTransitionActionHandler<WorkflowTask>, ValidationAware {

	private static final String SQL_STATEMENT_DELIMITER = ";";

	private String dataSourceFieldName;
	private String sqlFieldName;
	private String resultSystemNoteTypeName;  // optional - if set, then it adds a SystemNote of this type with results of query execution


	private SqlHandlerLocator sqlHandlerLocator;

	private SystemColumnValueHandler systemColumnValueHandler;

	private SystemNoteService systemNoteService;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		if (!StringUtils.isEmpty(getResultSystemNoteTypeName())) {
			// make sure that the note type is valid for WorkflowTask: ValidationException will be thrown if it cannot be found
			getSystemNoteType();
		}
	}


	/**
	 * Executes one more more sql statements separated by a semi-colon.
	 * If one fails they all fail.
	 */
	@Override
	public WorkflowTask processAction(WorkflowTask task, WorkflowTransition transition) {
		// required fields
		Integer dataSourceId = (Integer) getSystemColumnValue(task, getDataSourceFieldName());
		String sql = (String) getSystemColumnValue(task, getSqlFieldName());
		ValidationUtils.assertNotNull(dataSourceId, "Data Source is required.");
		ValidationUtils.assertNotEmpty(sql, "One or more sql statements are required.");

		// split out each statement and run them separately so we can preserve the execution messages
		String[] statements = sql.split(SQL_STATEMENT_DELIMITER);
		List<String> errorMessages = new ArrayList<>();
		for (String statement : statements) {
			executeSqlStatement(statement, errorMessages, task, dataSourceId);
		}

		if (!CollectionUtils.isEmpty(errorMessages)) {
			ValidationUtils.fail(String.format("[%d] errors: %s", errorMessages.size(), errorMessages.toString()));
		}
		return task;
	}


	/**
	 * Executes a single sql statement and generates a system note if applicable
	 */
	protected void executeSqlStatement(String sql, List<String> errorMessages, WorkflowTask task, Integer dataSourceId) {
		String message = null;
		try {
			SystemDataSource dataSource = getSystemSchemaService().getSystemDataSource(dataSourceId.shortValue());
			ValidationUtils.assertNotNull(dataSource, String.format("Could not find data source [%d]", dataSourceId));

			long start = System.currentTimeMillis();
			int result = getSqlHandlerLocator().locate(dataSource.getName()).executeUpdate(sql);
			message = "Execution Time = " + (System.currentTimeMillis() - start) + "ms; Update Count = " + result;
		}
		catch (Throwable e) {
			message = ExceptionUtils.getDetailedMessage(e);
			errorMessages.add(message);
		}
		finally {
			// save execution results as a system note if applicable
			if (!StringUtils.isEmpty(getResultSystemNoteTypeName())) {
				SystemNote note = new SystemNote();
				note.setNoteType(getSystemNoteType());
				note.setText(message);
				note.setFkFieldId(BeanUtils.getIdentityAsLong(task));
				getSystemNoteService().saveSystemNote(note);
			}
		}
	}


	/**
	 * Returns {@link SystemNoteType} object for WorkflowTask table and the name configured on this bean.
	 *
	 * @throws ValidationException if the type cannot be found.
	 */
	private SystemNoteType getSystemNoteType() {
		String tableName = WorkflowTask.class.getSimpleName();
		SystemTable table = getSystemSchemaService().getSystemTableByName(tableName);
		ValidationUtils.assertNotNull(table, "Cannot find SystemTable with name:" + tableName);

		SystemNoteType noteType = getSystemNoteService().getSystemNoteTypeByTableAndName(table.getId(), getResultSystemNoteTypeName());
		ValidationUtils.assertNotNull(noteType, "Cannot find system note type for WorkflowTask table with name: " + getResultSystemNoteTypeName());

		return noteType;
	}


	private Object getSystemColumnValue(WorkflowTask bean, String fieldName) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(bean, WorkflowTask.CUSTOM_FIELDS_GROUP_NAME, fieldName, false);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getDataSourceFieldName() {
		return this.dataSourceFieldName;
	}


	public void setDataSourceFieldName(String dataSourceFieldName) {
		this.dataSourceFieldName = dataSourceFieldName;
	}


	public String getSqlFieldName() {
		return this.sqlFieldName;
	}


	public void setSqlFieldName(String sqlFieldName) {
		this.sqlFieldName = sqlFieldName;
	}


	public String getResultSystemNoteTypeName() {
		return this.resultSystemNoteTypeName;
	}


	public void setResultSystemNoteTypeName(String resultSystemNoteTypeName) {
		this.resultSystemNoteTypeName = resultSystemNoteTypeName;
	}


	public SqlHandlerLocator getSqlHandlerLocator() {
		return this.sqlHandlerLocator;
	}


	public void setSqlHandlerLocator(SqlHandlerLocator sqlHandlerLocator) {
		this.sqlHandlerLocator = sqlHandlerLocator;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}
}
