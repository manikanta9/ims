package com.clifton.workflow.task.subscription;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.security.user.SecurityUser;
import com.clifton.workflow.task.WorkflowTask;
import org.springframework.messaging.simp.annotation.SubscribeMapping;


/**
 * The service interface for subscriptions to assigned {@link WorkflowTask} entities. This interface provides methods for subscribing to {@link WorkflowTask} changes.
 * <p>
 * Updates for the provided channels are sent to users through the {@link WorkflowTaskSubscriptionObserver} DAO observer.
 *
 * @author MikeH
 */
public interface WorkflowTaskSubscriptionService {

	/**
	 * The WebSocket channel for user-specific workflow task counts.
	 */
	public static final String CHANNEL_USER_TOPIC_WORKFLOW_TASK_COUNT = "/user/topic/workflow/task/count";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the info for workflow tasks waiting for an action by the given user. This method handles STOMP <tt>SUBSCRIBE</tt> frames for the {@link #CHANNEL_USER_TOPIC_WORKFLOW_TASK_COUNT}
	 * channel.
	 */
	@SecureMethod(dtoClass = WorkflowTask.class)
	@SubscribeMapping(CHANNEL_USER_TOPIC_WORKFLOW_TASK_COUNT)
	@DoNotAddRequestMapping
	public WorkflowTaskSubscriptionMessage getWorkflowTaskSubscriptionMessageForUser(SecurityUser user);
}
