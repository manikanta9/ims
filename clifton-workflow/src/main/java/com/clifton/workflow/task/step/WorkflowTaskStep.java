package com.clifton.workflow.task.step;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.workflow.task.WorkflowTask;

import java.util.Date;
import java.util.List;


/**
 * The WorkflowTaskStep class represents a single step of a specific task.
 * Many workflow tasks do not have any steps. However, for more complex tasks it might be
 * important to keep track of individual steps that could be assigned to different people.
 *
 * @author vgomelsky
 */
public class WorkflowTaskStep extends NamedEntity<Integer> implements SystemColumnCustomValueAware, SystemEntityModifyConditionAware {

	private WorkflowTaskStepDefinition stepDefinition;
	private WorkflowTask task;

	/**
	 * Identifies the entity that the task step is for. Corresponding table is defined by WorkflowTaskStepDefinition.
	 */
	@SoftLinkField(tableBeanPropertyName = "stepDefinition.linkedEntityTable")
	private Long linkedEntityFkFieldId;
	private String linkedEntityLabel;

	/**
	 * Usually steps have the same assignee as the task. However, in some situations one
	 * may want to assign a specific step to a different person.
	 */
	private SecurityUser assigneeUser;

	/**
	 * Step's due date cannot be after this of the task. Step definition defines logic for calculating
	 * step due date relative to corresponding task's due date.
	 */
	private Date dueDate;
	/**
	 * Initially, originalDueDate is always equal to dueDate. If corresponding WorkflowTaskStepDefinition allows changing
	 * the dueDate (changeToDueDateAllowed == true), then the user will be able to update dueDate. originalDueDate can never be changed.
	 */
	private Date originalDueDate;

	/**
	 * Indicates if and when this step was completed
	 */
	private Date completionDate;


	/**
	 * A List of custom column values for this task (field are assigned and vary by task category)
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		WorkflowTask workflowTask = getTask();
		if (workflowTask != null) {
			if (workflowTask.getDefinition() != null) {
				return workflowTask.getDefinition().getTaskEntityModifyCondition();
			}
		}
		return null;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(64);
		result.append('\'');
		result.append(getLabel());
		result.append('\'');
		if (getAssigneeUser() != null) {
			result.append(" assigned to ");
			result.append(getAssigneeUser().getLabel());
		}
		if (getDueDate() != null) {
			result.append(" due on ");
			result.append(DateUtils.fromDateShort(getDueDate()));
		}

		return result.toString();
	}


	@Override
	public String getLabel() {
		if (getName() != null) {
			return getName();
		}
		if (getLinkedEntityLabel() != null) {
			return getLinkedEntityLabel();
		}
		if (getStepDefinition() != null) {
			return getStepDefinition().getName();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A step is "Completed" if it has completionDate set.
	 */

	public boolean isCompletedStep() {
		return (getCompletionDate() != null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskStepDefinition getStepDefinition() {
		return this.stepDefinition;
	}


	public void setStepDefinition(WorkflowTaskStepDefinition stepDefinition) {
		this.stepDefinition = stepDefinition;
	}


	public WorkflowTask getTask() {
		return this.task;
	}


	public void setTask(WorkflowTask task) {
		this.task = task;
	}


	public Long getLinkedEntityFkFieldId() {
		return this.linkedEntityFkFieldId;
	}


	public void setLinkedEntityFkFieldId(Long linkedEntityFkFieldId) {
		this.linkedEntityFkFieldId = linkedEntityFkFieldId;
	}


	public String getLinkedEntityLabel() {
		return this.linkedEntityLabel;
	}


	public void setLinkedEntityLabel(String linkedEntityLabel) {
		this.linkedEntityLabel = linkedEntityLabel;
	}


	public SecurityUser getAssigneeUser() {
		return this.assigneeUser;
	}


	public void setAssigneeUser(SecurityUser assigneeUser) {
		this.assigneeUser = assigneeUser;
	}


	public Date getDueDate() {
		return this.dueDate;
	}


	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}


	public Date getOriginalDueDate() {
		return this.originalDueDate;
	}


	public void setOriginalDueDate(Date originalDueDate) {
		this.originalDueDate = originalDueDate;
	}


	public Date getCompletionDate() {
		return this.completionDate;
	}


	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}
}
