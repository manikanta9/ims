package com.clifton.workflow.task.audit;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.UpdatableEntity;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.audit.SystemAuditEvent;
import com.clifton.system.audit.SystemAuditEventSearchForm;
import com.clifton.system.audit.SystemAuditService;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.WorkflowTaskDefinitionLock;
import com.clifton.workflow.task.WorkflowTaskService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author vgomelsky
 */
@Service
public class WorkflowTaskAuditServiceImpl implements WorkflowTaskAuditService {

	private DaoLocator daoLocator;
	private SecurityUserService securityUserService;
	private SystemAuditService systemAuditService;
	private SystemColumnService systemColumnService;
	private WorkflowTaskService workflowTaskService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable getWorkflowTaskAuditEventList(int workflowTaskId, boolean restrictTime) {
		WorkflowTask task = getWorkflowTaskService().getWorkflowTask(workflowTaskId);
		ValidationUtils.assertNotNull(task, "Cannot find Workflow Task with id = " + workflowTaskId);

		DataTable result = null;
		if (task.getLinkedEntityFkFieldId() != null) {
			Date fromDate = DateUtils.addSeconds(task.getCreateDate(), -5); // in insert, the task is created after the entity: subtract 5 seconds to catch inserted entity
			Date toDate = task.isCompletedTask() ? task.getUpdateDate() : null;

			// check if linked entity exists or if it were deleted
			WorkflowTaskDefinition definition = task.getDefinition();
			ReadOnlyDAO<IdentityObject> linkedEntityDao = getDaoLocator().locate(definition.getLinkedEntityTable().getName());
			IdentityObject linkedEntity = linkedEntityDao.findByPrimaryKey(task.getLinkedEntityFkFieldId());
			if (linkedEntity == null) {
				// linked entity may have been deleted: get audit trail only for the linked entity
				result = getSystemAuditEventListForEntity(definition.getLinkedEntityTable().getName(), task.getLinkedEntityFkFieldId(), restrictTime, fromDate, toDate);
			}
			else {
				// for existing linked entity: get all locks
				List<WorkflowTaskDefinitionLock> lockList = getWorkflowTaskService().getWorkflowTaskDefinitionLockListForDefinition(definition.getId());
				Set<UpdatableEntity> insertedLockedEntityList = new HashSet<>();
				for (WorkflowTaskDefinitionLock lock : CollectionUtils.getIterable(lockList)) {
					// for each lock, get corresponding locked entity(ies), their audit events, and append them to the result
					String lockTableName = lock.getLockEntityTable().getName();
					ReadOnlyDAO<IdentityObject> lockEntityDao = getDaoLocator().locate(lockTableName);

					if (definition.getLinkedEntityTable().equals(lock.getLockEntityTable())) {
						// linked entity is the one that was locked
						DataTable entityResult = getSystemAuditEventListForEntity(lockTableName, task.getLinkedEntityFkFieldId(), restrictTime, fromDate, toDate);
						result = mergeDataTables(result, entityResult);

						// also capture newly inserted entity
						IdentityObject lockedEntity = lockEntityDao.findByPrimaryKey(task.getLinkedEntityFkFieldId());
						if (lockedEntity instanceof UpdatableEntity) {
							if (!restrictTime || DateUtils.isDateBetween(((UpdatableEntity) lockedEntity).getCreateDate(), fromDate, toDate, true)) {
								insertedLockedEntityList.add((UpdatableEntity) lockedEntity);
							}
						}
					}
					else {
						// locked table is different from linked table: find all dependent locked entities that still exist (haven't been deleted)
						Set<Long> lockedIds = new HashSet<>();
						for (String propertyPath : lock.getLinkedEntityBeanFieldPaths()) {
							List<IdentityObject> lockedList = lockEntityDao.findByField(propertyPath, linkedEntityDao.convertToPrimaryKeyDataType(task.getLinkedEntityFkFieldId()));
							for (IdentityObject lockedEntity : CollectionUtils.getIterable(lockedList)) {
								lockedIds.add(((Number) lockedEntity.getIdentity()).longValue());

								// also capture inserted entities (not in audit rail)
								if (lockedEntity instanceof UpdatableEntity) {
									if (!restrictTime || DateUtils.isDateBetween(((UpdatableEntity) lockedEntity).getCreateDate(), fromDate, toDate, true)) {
										insertedLockedEntityList.add((UpdatableEntity) lockedEntity);
									}
								}
							}
						}
						if (!lockedIds.isEmpty()) {
							DataTable entityResult = getSystemAuditEventListForEntities(lockTableName, lockedIds.toArray(new Long[]{}), restrictTime, fromDate, toDate);
							result = mergeDataTables(result, entityResult);
						}

						// check audit trail for deleted linked entities
						String parentFieldName = lockEntityDao.getConfiguration().getParentFieldName();
						Map<Number, Map<Number, DataRow>> matchedDeletedRows = new HashMap<>(); // <Entity PK, <Column ID, Row>> - a row for each column for a deleted entity
						for (String propertyPath : lock.getLinkedEntityBeanFieldPaths()) {
							if (propertyPath.equals(parentFieldName)) {
								// parent matching always works because it's not dependent on entity existence
								DataTable deletedRows = getSystemAuditEventList(lockTableName, SystemAuditEvent.AUDIT_DELETE, new Long[]{task.getLinkedEntityFkFieldId()}, true, restrictTime, fromDate, toDate);
								result = mergeDataTables(result, deletedRows);
							}
							else if (propertyPath.endsWith(".id")) {
								propertyPath = propertyPath.substring(0, propertyPath.length() - 3);
								// Locked Entity can be multiple levels deep when it's deleted: the link to Linked Entity maybe lost.
								// The following logic tries to recover it (if possible) from audit trail as well as existing entities along the path that haven't been deleted yet.
								// EXAMPLE: Action => Transition => State => Workflow :: "workflowTransition.endWorkflowState.workflow"
								int nextStart = propertyPath.indexOf('.');
								String nextPath = (nextStart == -1) ? propertyPath : propertyPath.substring(0, nextStart);
								propertyPath = (nextStart == -1) ? null : propertyPath.substring(nextStart + 1);

								DataTable allDeletedRows = getSystemAuditEventList(lockTableName, SystemAuditEvent.AUDIT_DELETE, null, false, restrictTime, fromDate, toDate);
								if (allDeletedRows == null || allDeletedRows.getTotalRowCount() == 0) {
									// no audit trail: nothing to recover
								}
								else {
									String lockColumnName = lockEntityDao.getConfiguration().getDBFieldName(nextPath);
									SystemColumn fkColumn = getSystemColumnService().getSystemColumnStandardByName(lockTableName, lockColumnName);
									ValidationUtils.assertNotNull(fkColumn, "Cannot find System Column '" + lockColumnName + "' on lockeTable '" + lockTableName + "' for lock: " + lock);
									for (int i = 0; i < allDeletedRows.getTotalRowCount(); i++) {
										DataRow row = allDeletedRows.getRow(i);
										if (MathUtils.isEqual(fkColumn.getId(), (Number) row.getValue("ColumnID"))) {
											Long fkValue = getForeignKeyIdFromDeletedRow(row);
											// for lock table, get next table using nextPath
											@SuppressWarnings("unchecked")
											Class<IdentityObject> nextClass = (Class<IdentityObject>) BeanUtils.getPropertyType(lockEntityDao.getConfiguration().getBeanClass(), nextPath);
											ReadOnlyDAO<IdentityObject> nextDao = getDaoLocator().locate(nextClass);
											if (nextDao.getConfiguration().getTableName().equals(task.getDefinition().getLinkedEntityTable().getName())) {
												// if same as Linked Table, then add delete id
												if (MathUtils.isEqual(fkValue, task.getLinkedEntityFkFieldId())) {
													addDeletedRowToResult(matchedDeletedRows, row);
												}
											}
											else if (propertyPath != null) {
												// find the entity by primary key and if found use remaining path to check if id matches
												IdentityObject nextEntity = nextDao.findByPrimaryKey(fkValue);
												if (nextEntity != null) {
													if (MathUtils.isEqual((Number) BeanUtils.getPropertyValue(nextEntity, propertyPath + ".id"), task.getLinkedEntityFkFieldId())) {
														addDeletedRowToResult(matchedDeletedRows, row);
													}
												}
												else {
													// TODO: check audit trail again: repeat the process (keep moving along the path until we find if the entity is related or not)
													// This is becoming too complex and the benefit of implementing this is likely not worth it. For example, if one deletes
													// a workflow transition and its action, current logic will catch the transition but not the action in results.
													break;
												}
											}
										}
									}
									if (!matchedDeletedRows.isEmpty()) {
										// add all audit trail rows for matched entity, not just the primary key
										for (int i = 0; i < allDeletedRows.getTotalRowCount(); i++) {
											DataRow row = allDeletedRows.getRow(i);
											//noinspection SuspiciousMethodCalls
											if (matchedDeletedRows.containsKey(row.getValue("MainPKFieldID"))) {
												addDeletedRowToResult(matchedDeletedRows, row);
											}
										}
									}
								}
							}
						}
						for (Map<Number, DataRow> rows : matchedDeletedRows.values()) {
							result = createDataTableIfNull(result);
							for (DataRow row : rows.values()) {
								result.addRow(new DataRowImpl(result, ((DataRowImpl) row).getData()));
							}
						}
					}
				}

				result = addInsertedEntitiesToDataTable(result, insertedLockedEntityList);
			}
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////


	private DataTable getSystemAuditEventListForEntity(String tableName, Long entityId, boolean restrictTime, Date fromDate, Date toDate) {
		return getSystemAuditEventListForEntities(tableName, new Long[]{entityId}, restrictTime, fromDate, toDate);
	}


	private DataTable getSystemAuditEventListForEntities(String tableName, Long[] entityIds, boolean restrictTime, Date fromDate, Date toDate) {
		return getSystemAuditEventList(tableName, null, entityIds, false, restrictTime, fromDate, toDate);
	}


	private DataTable getSystemAuditEventList(String tableName, Integer auditTypeId, Long[] entityIds, boolean matchParentEntity, boolean restrictTime, Date fromDate, Date toDate) {
		SystemAuditEventSearchForm searchForm = new SystemAuditEventSearchForm();
		searchForm.setTableName(tableName);
		searchForm.setAuditTypeId(auditTypeId);
		if (entityIds != null) {
			if (entityIds.length == 1) {
				if (matchParentEntity) {
					searchForm.setParentEntityId(entityIds[0]);
				}
				else {
					searchForm.setEntityId(entityIds[0]);
				}
			}
			else {
				ValidationUtils.assertFalse(matchParentEntity, "Cannot match parent audit entity for multiple id's");
				searchForm.setEntityIds(entityIds);
			}
		}
		if (restrictTime) {
			// start 3 seconds earlier because in some cases the task is created immediately after the entity is inserted (needs entity id)
			searchForm.addSearchRestriction("auditDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, DateUtils.addSeconds(fromDate, -3));
			if (toDate != null) {
				searchForm.addSearchRestriction("auditDate", ComparisonConditions.LESS_THAN_OR_EQUALS, toDate);
			}
		}
		return getSystemAuditService().getSystemAuditEventList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DataTable mergeDataTables(DataTable dataTable, DataTable moreResults) {
		if (dataTable == null) {
			return moreResults;
		}
		if (moreResults != null) {
			for (int i = 0; i < moreResults.getTotalRowCount(); i++) {
				dataTable.addRow(new DataRowImpl(dataTable, ((DataRowImpl) moreResults.getRow(i)).getData()));
			}
		}
		return dataTable;
	}


	private DataTable addInsertedEntitiesToDataTable(DataTable dataTable, Set<UpdatableEntity> insertedEntityList) {
		if (CollectionUtils.isEmpty(insertedEntityList)) {
			return dataTable;
		}

		dataTable = createDataTableIfNull(dataTable);

		Map<Column, SystemColumn> columnCache = new HashMap<>();
		for (UpdatableEntity entity : insertedEntityList) {
			Serializable id = ((IdentityObject) entity).getIdentity();
			SecurityUser user = getSecurityUserService().getSecurityUser(entity.getCreateUserId());
			SystemColumn column = getPrimaryKeyColumn((IdentityObject) entity, columnCache);
			DataRow row = new DataRowImpl(dataTable, new Object[]{
					column.getId(), id, null, entity.getCreateDate(), (user == null) ? "User ID = " + entity.getCreateUserId() : user.getUserName(),
					"insert", column.getTable().getLabel(), column.getLabel(), BeanUtils.getLabel(entity), null, "DOUBLE CLICK FOR MORE INFO"
			});
			dataTable.addRow(row);
		}

		return dataTable;
	}


	private void addDeletedRowToResult(Map<Number, Map<Number, DataRow>> matchedDeletedRows, DataRow row) {
		Number pk = (Number) row.getValue("MainPKFieldID");
		Map<Number, DataRow> deletedRows = matchedDeletedRows.computeIfAbsent(pk, k -> new HashMap<>());
		deletedRows.put((Number) row.getValue("ColumnID"), row);
	}


	private DataTable createDataTableIfNull(DataTable dataTable) {
		if (dataTable == null) {
			DataColumn[] columnList = {
					new DataColumnImpl("ColumnID", Types.INTEGER),
					new DataColumnImpl("MainPKFieldID", Types.INTEGER),
					new DataColumnImpl("ParentPKFieldID", Types.INTEGER),
					new DataColumnImpl("Date", Types.DATE),
					new DataColumnImpl("User", Types.NVARCHAR),
					new DataColumnImpl("Action", Types.NVARCHAR),
					new DataColumnImpl("Table", Types.NVARCHAR),
					new DataColumnImpl("Field", Types.NVARCHAR),
					new DataColumnImpl("Label", Types.NVARCHAR),
					new DataColumnImpl("Old Value", Types.NVARCHAR),
					new DataColumnImpl("New Value", Types.NVARCHAR)
			};
			dataTable = new PagingDataTableImpl(columnList);
		}
		return dataTable;
	}


	private SystemColumn getPrimaryKeyColumn(IdentityObject entity, Map<Column, SystemColumn> columnCache) {
		Column column = getDaoLocator().locate(entity).getConfiguration().getPrimaryKeyColumn();
		SystemColumn result = columnCache.get(column);
		if (result == null) {
			result = getSystemColumnService().getSystemColumnStandardByName(column.getTableName(), column.getName());
			ValidationUtils.assertNotNull(result, "Cannot find column [" + column.getTableName() + "].[" + column.getName() + "]");
		}
		return result;
	}


	/**
	 * 'Old Value' for Foreign Keys on delete is usually in the following format: {id=???,label=???}
	 * Try to extract the id if possible and return Long representation of it.  Return null otherwise.
	 */
	private Long getForeignKeyIdFromDeletedRow(DataRow row) {
		Long result = null;

		String oldValue = (String) row.getValue("Old Value");
		if (oldValue != null && oldValue.startsWith("{id=")) {
			String stringResult = oldValue.substring(4); // cut "{id="
			int endIndex = stringResult.indexOf(',');
			if (endIndex < 0) {
				endIndex = stringResult.indexOf('}');
			}
			if (endIndex > 0) {
				stringResult = stringResult.substring(0, endIndex);
			}
			result = Long.parseLong(stringResult);
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemAuditService getSystemAuditService() {
		return this.systemAuditService;
	}


	public void setSystemAuditService(SystemAuditService systemAuditService) {
		this.systemAuditService = systemAuditService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}
}
