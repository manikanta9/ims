package com.clifton.workflow.task.linkedentity;

/**
 * This class is responsible for registering/unregistering observers for linked entities of workflow tasks based on configured workflow task definition linked tables
 *
 * @author mitchellf
 */
public interface WorkflowTaskLinkedEntityObserverRegistrator {

	/**
	 * Registers {@link WorkflowTaskLinkedEntityObserver} for INSERT, UPDATE, DELETE events of the DAO for the specified table.
	 */
	public void registerLinkedEntityObserver(String tableName);


	/**
	 * Unregisters {@link WorkflowTaskLinkedEntityObserver} for INSERT, UPDATE, DELETE events of the DAO for the specified table.
	 */
	public void unregisterLinkedEntityObserver(String tableName);
}
