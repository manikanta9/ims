package com.clifton.workflow.task.step.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * @author vgomelsky
 */
public class WorkflowTaskStepSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,description,linkedEntityLabel,task.linkedEntityLabel", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "task.id")
	private Integer taskId;

	@SearchField(searchField = "stepDefinition.id")
	private Short stepDefinitionId;

	@SearchField(searchFieldPath = "stepDefinition", searchField = "name")
	private String stepDefinitionName;

	@SearchField(searchField = "task.definition.id")
	private Short taskDefinitionId;

	@SearchField(searchField = "task.definition.category.id")
	private Short taskCategoryId;

	@SearchField
	private String linkedEntityLabel;

	@SearchField(searchFieldPath = "task", searchField = "linkedEntityLabel")
	private String taskLinkedEntityLabel;

	@SearchField(searchFieldPath = "stepDefinition", searchField = "order")
	private Short order;

	@SearchField(searchField = "assigneeUser.id")
	private Short assigneeUserId;

	@SearchField
	private Date dueDate;

	@SearchField
	private Date originalDueDate;

	@SearchField
	private Date completionDate;

	@SearchField(searchField = "completionDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean completedStep;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getStepDefinitionId() {
		return this.stepDefinitionId;
	}


	public void setStepDefinitionId(Short stepDefinitionId) {
		this.stepDefinitionId = stepDefinitionId;
	}


	public String getStepDefinitionName() {
		return this.stepDefinitionName;
	}


	public void setStepDefinitionName(String stepDefinitionName) {
		this.stepDefinitionName = stepDefinitionName;
	}


	public Short getTaskDefinitionId() {
		return this.taskDefinitionId;
	}


	public void setTaskDefinitionId(Short taskDefinitionId) {
		this.taskDefinitionId = taskDefinitionId;
	}


	public Short getTaskCategoryId() {
		return this.taskCategoryId;
	}


	public void setTaskCategoryId(Short taskCategoryId) {
		this.taskCategoryId = taskCategoryId;
	}


	public String getLinkedEntityLabel() {
		return this.linkedEntityLabel;
	}


	public void setLinkedEntityLabel(String linkedEntityLabel) {
		this.linkedEntityLabel = linkedEntityLabel;
	}


	public String getTaskLinkedEntityLabel() {
		return this.taskLinkedEntityLabel;
	}


	public void setTaskLinkedEntityLabel(String taskLinkedEntityLabel) {
		this.taskLinkedEntityLabel = taskLinkedEntityLabel;
	}


	public Short getOrder() {
		return this.order;
	}


	public void setOrder(Short order) {
		this.order = order;
	}


	public Integer getTaskId() {
		return this.taskId;
	}


	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}


	public Short getAssigneeUserId() {
		return this.assigneeUserId;
	}


	public void setAssigneeUserId(Short assigneeUserId) {
		this.assigneeUserId = assigneeUserId;
	}


	public Date getDueDate() {
		return this.dueDate;
	}


	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}


	public Date getOriginalDueDate() {
		return this.originalDueDate;
	}


	public void setOriginalDueDate(Date originalDueDate) {
		this.originalDueDate = originalDueDate;
	}


	public Date getCompletionDate() {
		return this.completionDate;
	}


	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}


	public Boolean getCompletedStep() {
		return this.completedStep;
	}


	public void setCompletedStep(Boolean completedStep) {
		this.completedStep = completedStep;
	}
}
