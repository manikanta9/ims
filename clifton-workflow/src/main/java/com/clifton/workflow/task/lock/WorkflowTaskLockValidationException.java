package com.clifton.workflow.task.lock;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.validation.CauseEntityAware;
import com.clifton.core.validation.CauseEventTypeAware;
import com.clifton.core.validation.LinkedEntityAware;
import com.clifton.core.util.validation.ValidationException;


/**
 * The WorkflowTaskLockValidationException should be thrown when an edit is attempted on a locked entity.
 * It can be used to identify this specific case and allows one to customize corresponding logic.
 * <p>
 * For example, in UI we can display a link to a screen that allows one to create a task that unlocks the entity.
 *
 * @author vgomelsky
 */
public class WorkflowTaskLockValidationException extends ValidationException implements CauseEntityAware, CauseEventTypeAware, LinkedEntityAware {

	private final IdentityObject causeEntity;
	private final DaoEventTypes causeEvent;
	private final IdentityObject linkedEntity;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskLockValidationException(String message, IdentityObject causeEntity, IdentityObject linkedEntity, DaoEventTypes causeEvent) {
		super(message);
		this.causeEntity = causeEntity;
		this.causeEvent = causeEvent;
		this.linkedEntity = linkedEntity;
	}


	public WorkflowTaskLockValidationException(String message, IdentityObject causeEntity, DaoEventTypes causeEvent) {
		this(message, causeEntity, null, causeEvent);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IdentityObject getCauseEntity() {
		return this.causeEntity;
	}


	@Override
	public DaoEventTypes getCauseEvent() {
		return this.causeEvent;
	}


	@Override
	public IdentityObject getLinkedEntity() {
		return this.linkedEntity;
	}
}
