package com.clifton.workflow.task.search;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class WorkflowTaskDefinitionLockSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "lockEntityTable.name,linkedEntityBeanFieldPath")
	private String searchPattern;

	@SearchField(searchField = "definition.id")
	private Short definitionId;

	@SearchField(searchFieldPath = "definition", searchField = "name")
	private String definitionName;

	@SearchField(searchFieldPath = "definition", searchField = "active")
	private Boolean active;

	@SearchField(searchField = "lockEntityTable.id")
	private Short lockEntityTableId;

	@SearchField(searchField = "name", searchFieldPath = "lockEntityTable", comparisonConditions = ComparisonConditions.EQUALS)
	private String lockEntityTableName;

	@SearchField(searchField = "name", searchFieldPath = "lockEntityTable", comparisonConditions = ComparisonConditions.LIKE)
	private String lockEntityTableNameLike;

	@SearchField(searchField = "lockEntityTable.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean lockEntityTablePresent;

	@SearchField
	private String linkedEntityBeanFieldPath;

	@SearchField
	private DaoEventTypes eventScope;

	/**
	 * Custom filter that matches if the specified event type is a subset of event scope.
	 */
	private DaoEventTypes eventType;

	@SearchField(searchField = "lockScopeCondition.id")
	private Integer lockScopeConditionId;

	@SearchField(searchField = "lockEntityModifyCondition.id")
	private Integer lockEntityModifyConditionId;

	@SearchField(searchField = "name", searchFieldPath = "definition.linkedEntityTable", comparisonConditions = ComparisonConditions.EQUALS)
	private String linkedEntityTableName;

	@SearchField(searchField = "definition.linkedEntityTable.name", comparisonConditions = ComparisonConditions.LIKE)
	private String linkedEntityTableNameLike;

	// optionally limits results to those that pass taskEntityModifyCondition on corresponding task definition
	// most conditions are security based (current user is a member of specific group) so the task information is likely not needed
	private boolean limitUsingTaskEntityModifyCondition;
	// used together with lockEntityTableName for locks that have lockScopeConditionId populated to limit locks based on this condition
	private Integer lockEntityId;

	// For cases when Lock Scope Condition depends on bean field(s) modifications, we need access to updated bean
	// that has not been saved yet. Use this bean instead of retrieving from DB instead to evaluate the condition correctly.
	private String lockEntityClassName;
	private String lockEntityJson;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Short getLockEntityTableId() {
		return this.lockEntityTableId;
	}


	public void setLockEntityTableId(Short lockEntityTableId) {
		this.lockEntityTableId = lockEntityTableId;
	}


	public String getLockEntityTableName() {
		return this.lockEntityTableName;
	}


	public void setLockEntityTableName(String lockEntityTableName) {
		this.lockEntityTableName = lockEntityTableName;
	}


	public String getLockEntityTableNameLike() {
		return this.lockEntityTableNameLike;
	}


	public void setLockEntityTableNameLike(String lockEntityTableNameLike) {
		this.lockEntityTableNameLike = lockEntityTableNameLike;
	}


	public Boolean getLockEntityTablePresent() {
		return this.lockEntityTablePresent;
	}


	public void setLockEntityTablePresent(Boolean lockEntityTablePresent) {
		this.lockEntityTablePresent = lockEntityTablePresent;
	}


	public String getLinkedEntityBeanFieldPath() {
		return this.linkedEntityBeanFieldPath;
	}


	public void setLinkedEntityBeanFieldPath(String linkedEntityBeanFieldPath) {
		this.linkedEntityBeanFieldPath = linkedEntityBeanFieldPath;
	}


	public DaoEventTypes getEventScope() {
		return this.eventScope;
	}


	public void setEventScope(DaoEventTypes eventScope) {
		this.eventScope = eventScope;
	}


	public DaoEventTypes getEventType() {
		return this.eventType;
	}


	public void setEventType(DaoEventTypes eventType) {
		this.eventType = eventType;
	}


	public Integer getLockScopeConditionId() {
		return this.lockScopeConditionId;
	}


	public void setLockScopeConditionId(Integer lockScopeConditionId) {
		this.lockScopeConditionId = lockScopeConditionId;
	}


	public Integer getLockEntityModifyConditionId() {
		return this.lockEntityModifyConditionId;
	}


	public void setLockEntityModifyConditionId(Integer lockEntityModifyConditionId) {
		this.lockEntityModifyConditionId = lockEntityModifyConditionId;
	}


	public String getLinkedEntityTableName() {
		return this.linkedEntityTableName;
	}


	public void setLinkedEntityTableName(String linkedEntityTableName) {
		this.linkedEntityTableName = linkedEntityTableName;
	}


	public String getLinkedEntityTableNameLike() {
		return this.linkedEntityTableNameLike;
	}


	public void setLinkedEntityTableNameLike(String linkedEntityTableNameLike) {
		this.linkedEntityTableNameLike = linkedEntityTableNameLike;
	}


	public boolean isLimitUsingTaskEntityModifyCondition() {
		return this.limitUsingTaskEntityModifyCondition;
	}


	public void setLimitUsingTaskEntityModifyCondition(boolean limitUsingTaskEntityModifyCondition) {
		this.limitUsingTaskEntityModifyCondition = limitUsingTaskEntityModifyCondition;
	}


	public Integer getLockEntityId() {
		return this.lockEntityId;
	}


	public void setLockEntityId(Integer lockEntityId) {
		this.lockEntityId = lockEntityId;
	}


	public String getLockEntityClassName() {
		return this.lockEntityClassName;
	}


	public void setLockEntityClassName(String lockEntityClassName) {
		this.lockEntityClassName = lockEntityClassName;
	}


	public String getLockEntityJson() {
		return this.lockEntityJson;
	}


	public void setLockEntityJson(String lockEntityJson) {
		this.lockEntityJson = lockEntityJson;
	}
}
