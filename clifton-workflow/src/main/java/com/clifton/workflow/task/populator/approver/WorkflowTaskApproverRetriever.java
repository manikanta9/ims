package com.clifton.workflow.task.populator.approver;

import com.clifton.core.beans.IdentityObject;
import com.clifton.security.user.SecurityUser;
import com.clifton.workflow.task.WorkflowTaskDefinition;


/**
 * The WorkflowTaskApproverRetriever interface can be implemented to provide custom WorkflowTask Approver selection logic.
 * For example, the approver is last Portfolio Manager who approved latest main Portfolio Run for a given Client Account.
 *
 * @author vgomelsky
 */
public interface WorkflowTaskApproverRetriever {

	/**
	 * Returns the user who should be approving workflow tasks for the specified definition.
	 * The method can return null meaning that specific Approver fields from WorkflowTaskDefinition should be used instead.
	 */
	public SecurityUser getWorkflowTaskApprover(WorkflowTaskDefinition definition, IdentityObject linkedEntity);
}
