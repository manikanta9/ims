package com.clifton.workflow.task.step.comparison;

import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.step.WorkflowTaskStep;
import com.clifton.workflow.task.step.WorkflowTaskStepService;

import java.util.ArrayList;
import java.util.List;


/**
 * The WorkflowTaskHasIncompleteStepComparison class evaluates to true when the specified WorkflowTask has at least one step that has not been completed.
 * The Comparison evaluates all steps for the task unless optional 'stepDefinitionName' is specified.
 *
 * @author vgomelsky
 */
public class WorkflowTaskHasIncompleteStepComparison implements Comparison<WorkflowTask> {

	private WorkflowTaskStepService workflowTaskStepService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * Optional task step definition name. If specified, only this step vs all steps will be evaluated.
	 */
	private String stepDefinitionName;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	protected boolean isReverse() {
		return false;
	}


	@Override
	public boolean evaluate(WorkflowTask task, ComparisonContext context) {
		List<WorkflowTaskStep> stepList = getWorkflowTaskStepService().getWorkflowTaskStepListByTask(task.getId());
		List<WorkflowTaskStep> incompleteStepList = new ArrayList<>();
		for (WorkflowTaskStep step : CollectionUtils.getIterable(stepList)) {
			if (!step.isCompletedStep()) {
				if (StringUtils.isEmpty(getStepDefinitionName()) || getStepDefinitionName().equals(step.getStepDefinition().getName())) {
					incompleteStepList.add(step);
				}
			}
		}

		boolean result = isReverse() ? incompleteStepList.isEmpty() : !incompleteStepList.isEmpty();
		if (context != null) {
			String msg;
			if (isReverse() ? !result : result) {
				msg = ("(Incomplete Task Step found: " + incompleteStepList.get(0).getLabel() + ")");
			}
			else {
				msg = ("(No Incomplete Steps found for Task: " + task.getLabel() + ")");
			}

			if (result) {
				context.recordTrueMessage(msg);
			}
			else {
				context.recordFalseMessage(msg);
			}
		}
		return result;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////              Getter and Setter Methods               ///////////
	///////////////////////////////////////////////////////////////////////////


	public WorkflowTaskStepService getWorkflowTaskStepService() {
		return this.workflowTaskStepService;
	}


	public void setWorkflowTaskStepService(WorkflowTaskStepService workflowTaskStepService) {
		this.workflowTaskStepService = workflowTaskStepService;
	}


	public String getStepDefinitionName() {
		return this.stepDefinitionName;
	}


	public void setStepDefinitionName(String stepDefinitionName) {
		this.stepDefinitionName = stepDefinitionName;
	}
}
