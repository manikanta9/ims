package com.clifton.workflow.task.template;

import com.clifton.core.dataaccess.dao.NonPersistentObject;


/**
 * The DefinitionCreationTemplate class defines the task definition that will be used as a template for new task definition
 * as well as the fields that can be overridden on new definition.
 *
 * @author vgomelsky
 */
@NonPersistentObject
public class DefinitionCreationTemplate {

	private short templateId;


	private String name;
	private String description;

	private Integer taskGeneratorBeanId;

	private Long linkedEntityFkFieldId;
	private String linkedEntityLabel;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public short getTemplateId() {
		return this.templateId;
	}


	public void setTemplateId(short templateId) {
		this.templateId = templateId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getTaskGeneratorBeanId() {
		return this.taskGeneratorBeanId;
	}


	public void setTaskGeneratorBeanId(Integer taskGeneratorBeanId) {
		this.taskGeneratorBeanId = taskGeneratorBeanId;
	}


	public Long getLinkedEntityFkFieldId() {
		return this.linkedEntityFkFieldId;
	}


	public void setLinkedEntityFkFieldId(Long linkedEntityFkFieldId) {
		this.linkedEntityFkFieldId = linkedEntityFkFieldId;
	}


	public String getLinkedEntityLabel() {
		return this.linkedEntityLabel;
	}


	public void setLinkedEntityLabel(String linkedEntityLabel) {
		this.linkedEntityLabel = linkedEntityLabel;
	}
}
