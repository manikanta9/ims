package com.clifton.workflow.task.linkedentity;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.search.WorkflowTaskSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * This is a DAO observer that is automatically registered for DAOs with entities corresponding to an existing {@link com.clifton.workflow.task.WorkflowTaskDefinition}.
 *
 * @author mitchellf
 */
@Component
public class WorkflowTaskLinkedEntityObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private WorkflowTaskService workflowTaskService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {

		if (event.isDelete()) {
			// find list of open workflow tasks
			WorkflowTaskSearchForm taskSearchForm = new WorkflowTaskSearchForm();
			taskSearchForm.setLinkedEntityFkFieldId(BeanUtils.getIdentityAsLong(bean));
			taskSearchForm.setCompletedTask(Boolean.FALSE);
			List<WorkflowTask> taskList = getWorkflowTaskService().getWorkflowTaskList(taskSearchForm);
			ValidationUtils.assertEmpty(taskList, "Cannot delete item because it has open workflow tasks.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskService getWorkflowTaskService() {
		return this.workflowTaskService;
	}


	public void setWorkflowTaskService(WorkflowTaskService workflowTaskService) {
		this.workflowTaskService = workflowTaskService;
	}
}
