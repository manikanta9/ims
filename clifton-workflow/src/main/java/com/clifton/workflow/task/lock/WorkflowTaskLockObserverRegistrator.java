package com.clifton.workflow.task.lock;

/**
 * The WorkflowTaskLockObserverRegistrator interface defines methods for registering and unregistering DAO observers responsible for
 * locking workflow task entities.
 *
 * @author vgomelsky
 */
public interface WorkflowTaskLockObserverRegistrator {

	/**
	 * Registers {@link WorkflowTaskLockObserver} for INSERT, UPDATE, DELETE events of the DAO for the specified table.
	 */
	public void registerLockObserver(String tableName);


	/**
	 * Unregisters {@link WorkflowTaskLockObserver} for INSERT, UPDATE, DELETE events of the DAO for the specified table.
	 */
	public void unregisterLockObserver(String tableName);
}
