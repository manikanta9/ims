package com.clifton.workflow.task.step.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class WorkflowTaskStepDependencySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "stepDefinition.id")
	private Short stepDefinitionId;

	@SearchField(searchField = "requiredStepDefinition.id")
	private Short requiredStepDefinitionId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getStepDefinitionId() {
		return this.stepDefinitionId;
	}


	public void setStepDefinitionId(Short stepDefinitionId) {
		this.stepDefinitionId = stepDefinitionId;
	}


	public Short getRequiredStepDefinitionId() {
		return this.requiredStepDefinitionId;
	}


	public void setRequiredStepDefinitionId(Short requiredStepDefinitionId) {
		this.requiredStepDefinitionId = requiredStepDefinitionId;
	}
}
