package com.clifton.workflow.task.step;

import com.clifton.core.beans.BaseEntity;


/**
 * The WorkflowTaskStepDependency class defines a dependency between two different steps for the same task.
 * A single step may depend on one or more other steps. A dependent step cannot be completed until all other
 * steps that it depends on have been completed.
 *
 * @author vgomelsky
 */
public class WorkflowTaskStepDependency extends BaseEntity<Short> {

	private WorkflowTaskStepDefinition stepDefinition;

	// this step must be completed before 'stepDefinition" step can be completed
	private WorkflowTaskStepDefinition requiredStepDefinition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskStepDefinition getStepDefinition() {
		return this.stepDefinition;
	}


	public void setStepDefinition(WorkflowTaskStepDefinition stepDefinition) {
		this.stepDefinition = stepDefinition;
	}


	public WorkflowTaskStepDefinition getRequiredStepDefinition() {
		return this.requiredStepDefinition;
	}


	public void setRequiredStepDefinition(WorkflowTaskStepDefinition requiredStepDefinition) {
		this.requiredStepDefinition = requiredStepDefinition;
	}
}
