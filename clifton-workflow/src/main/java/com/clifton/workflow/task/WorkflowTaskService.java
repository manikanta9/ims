package com.clifton.workflow.task;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.workflow.task.search.WorkflowTaskCategorySearchForm;
import com.clifton.workflow.task.search.WorkflowTaskDefinitionLockSearchForm;
import com.clifton.workflow.task.search.WorkflowTaskDefinitionSearchForm;
import com.clifton.workflow.task.search.WorkflowTaskSearchForm;

import java.util.List;


/**
 * The WorkflowTaskService interface defines methods that help setup and retrieve
 * {@link WorkflowTask} and related definition objects.
 *
 * @author vgomelsky
 */
public interface WorkflowTaskService {

	////////////////////////////////////////////////////////////////////////////
	////////       Workflow Task Category Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskCategory getWorkflowTaskCategory(short id);


	public WorkflowTaskCategory getWorkflowTaskCategoryByName(String name);


	public List<WorkflowTaskCategory> getWorkflowTaskCategoryList(WorkflowTaskCategorySearchForm searchForm);


	public WorkflowTaskCategory saveWorkflowTaskCategory(WorkflowTaskCategory category);


	public void deleteWorkflowTaskCategory(short id);


	////////////////////////////////////////////////////////////////////////////
	////////       Workflow Task Definition Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskDefinition getWorkflowTaskDefinition(short id);


	public List<WorkflowTaskDefinition> getWorkflowTaskDefinitionList(WorkflowTaskDefinitionSearchForm searchForm);


	public WorkflowTaskDefinition saveWorkflowTaskDefinition(WorkflowTaskDefinition definition);


	public void deleteWorkflowTaskDefinition(short id);


	////////////////////////////////////////////////////////////////////////////
	////////     Workflow Task Definition Lock Business Methods    /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTaskDefinitionLock getWorkflowTaskDefinitionLock(short id);


	public List<WorkflowTaskDefinitionLock> getWorkflowTaskDefinitionLockListForDefinition(short definitionId);


	public List<WorkflowTaskDefinitionLock> getWorkflowTaskDefinitionLockListForTable(String lockTableName);


	public List<WorkflowTaskDefinitionLock> getWorkflowTaskDefinitionLockList(WorkflowTaskDefinitionLockSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////          Workflow Task Business Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTask getWorkflowTask(int id);


	public List<WorkflowTask> getWorkflowTaskList(WorkflowTaskSearchForm searchForm);


	public WorkflowTask saveWorkflowTask(WorkflowTask task);


	/**
	 * Same functionality as {@link #saveWorkflowTask(WorkflowTask)} method but bypasses some validation:
	 * ignores required description in this case but will enforce on future updates.
	 */
	@DoNotAddRequestMapping
	public WorkflowTask createWorkflowTaskAfterLockedEntityInsert(WorkflowTask task);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_FULL_CONTROL) // TODO: revisit whether this should be allowed at all
	public void deleteWorkflowTask(int id);
}
