package com.clifton.workflow.transition.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.workflow.transition.WorkflowTransition;
import org.springframework.stereotype.Component;


/**
 * @author DipinC
 */
@Component
public class WorkflowTransitionListByWorkflowCache extends SelfRegisteringSingleKeyDaoListCache<WorkflowTransition, Short>{


	@Override
	protected String getBeanKeyProperty() {
		return "endWorkflowState.workflow.id";
	}

	@Override
	protected Short getBeanKeyValue(WorkflowTransition bean) {
		if (bean.getEndWorkflowState() != null && bean.getEndWorkflowState().getWorkflow() != null) {
			return bean.getEndWorkflowState().getWorkflow().getId();
		}
		return null;
	}

}
