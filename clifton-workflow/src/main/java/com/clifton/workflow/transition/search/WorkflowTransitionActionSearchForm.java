package com.clifton.workflow.transition.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.workflow.transition.WorkflowTransitionAction;


/**
 * The search form for querying {@link WorkflowTransitionAction} entities.
 *
 * @author MikeH
 */
public class WorkflowTransitionActionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "actionSystemBean.name,actionSystemBean.description")
	private String searchPattern;

	@SearchField(searchField = "actionSystemBean.name")
	private String name;

	@SearchField(searchField = "actionSystemBean.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField(searchField = "actionSystemBean.description")
	private String description;

	@SearchField(searchFieldPath = "workflowTransition", searchField = "endWorkflowState.workflow.name")
	private String workflowName;

	@SearchField(searchFieldPath = "workflowTransition", searchField = "endWorkflowState.workflow.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String workflowNameEquals;

	@SearchField(searchField = "workflowTransition.id")
	private Integer workflowTransitionId;

	@SearchField(searchField = "workflowTransition.name")
	private String workflowTransitionName;

	@SearchField(searchField = "workflowTransition.startWorkflowState.id")
	private Short startWorkflowStateId;

	@SearchField(searchField = "workflowTransition.startWorkflowState.name")
	private String startWorkflowStateName;

	@SearchField(searchField = "workflowTransition.startWorkflowState.status.name")
	private String startWorkflowStatusName;

	@SearchField(searchField = "workflowTransition.endWorkflowState.id")
	private Short endWorkflowStateId;

	@SearchField(searchField = "workflowTransition.endWorkflowState.name")
	private String endWorkflowStateName;

	@SearchField(searchField = "workflowTransition.endWorkflowState.status.name")
	private String endWorkflowStatusName;

	@SearchField
	private Integer order;

	@SearchField
	private Boolean beforeTransition;

	@SearchField
	private Boolean separateTransaction;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getWorkflowNameEquals() {
		return this.workflowNameEquals;
	}


	public void setWorkflowNameEquals(String workflowNameEquals) {
		this.workflowNameEquals = workflowNameEquals;
	}


	public Integer getWorkflowTransitionId() {
		return this.workflowTransitionId;
	}


	public void setWorkflowTransitionId(Integer workflowTransitionId) {
		this.workflowTransitionId = workflowTransitionId;
	}


	public String getWorkflowTransitionName() {
		return this.workflowTransitionName;
	}


	public void setWorkflowTransitionName(String workflowTransitionName) {
		this.workflowTransitionName = workflowTransitionName;
	}


	public Short getStartWorkflowStateId() {
		return this.startWorkflowStateId;
	}


	public void setStartWorkflowStateId(Short startWorkflowStateId) {
		this.startWorkflowStateId = startWorkflowStateId;
	}


	public Short getEndWorkflowStateId() {
		return this.endWorkflowStateId;
	}


	public void setEndWorkflowStateId(Short endWorkflowStateId) {
		this.endWorkflowStateId = endWorkflowStateId;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Boolean getBeforeTransition() {
		return this.beforeTransition;
	}


	public void setBeforeTransition(Boolean beforeTransition) {
		this.beforeTransition = beforeTransition;
	}


	public Boolean getSeparateTransaction() {
		return this.separateTransaction;
	}


	public void setSeparateTransaction(Boolean separateTransaction) {
		this.separateTransaction = separateTransaction;
	}


	public String getWorkflowName() {
		return this.workflowName;
	}


	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}


	public String getStartWorkflowStateName() {
		return this.startWorkflowStateName;
	}


	public void setStartWorkflowStateName(String startWorkflowStateName) {
		this.startWorkflowStateName = startWorkflowStateName;
	}


	public String getStartWorkflowStatusName() {
		return this.startWorkflowStatusName;
	}


	public void setStartWorkflowStatusName(String startWorkflowStatusName) {
		this.startWorkflowStatusName = startWorkflowStatusName;
	}


	public String getEndWorkflowStateName() {
		return this.endWorkflowStateName;
	}


	public void setEndWorkflowStateName(String endWorkflowStateName) {
		this.endWorkflowStateName = endWorkflowStateName;
	}


	public String getEndWorkflowStatusName() {
		return this.endWorkflowStatusName;
	}


	public void setEndWorkflowStatusName(String endWorkflowStatusName) {
		this.endWorkflowStatusName = endWorkflowStatusName;
	}
}
