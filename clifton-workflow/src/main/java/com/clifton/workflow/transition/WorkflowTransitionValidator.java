package com.clifton.workflow.transition;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.springframework.stereotype.Component;


/**
 * The <code>WorkflowTransitionValidator</code> class handles all validation prior to saving a {@link WorkflowTransition}
 *
 * @author manderson
 */
@Component
public class WorkflowTransitionValidator extends SelfRegisteringDaoValidator<WorkflowTransition> {

	private SecurityAuthorizationService securityAuthorizationService;
	private WorkflowTransitionService workflowTransitionService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(WorkflowTransition bean, DaoEventTypes config) throws ValidationException {
		if (bean.getEndWorkflowState() == null) {
			throw new FieldValidationException("Workflow Transition End State is required", "endWorkflowState");
		}

		if (bean.getEndWorkflowState().getWorkflow().isActive()) {
			if (!getSecurityAuthorizationService().isSecurityUserAdmin()) {
				throw new ValidationException("Cannot edit Workflow Transition [" + bean.getName() + "] unless your are an Administrator because it is tied to an active Workflow ["
						+ bean.getEndWorkflowState().getWorkflow().getName() + "].");
			}
		}

		if (config.isDelete()) {
			return;
		}
		if (config.isInsert() || config.isUpdate()) {
			if (bean.getStartWorkflowState() != null) {
				ValidationUtils.assertFalse(bean.getStartWorkflowState().isDisabled(), "Cannot add a transition from state [" + bean.getStartWorkflowState().getName() + "], because it is disabled");
			}
			ValidationUtils.assertFalse(bean.getEndWorkflowState().isDisabled(), "Cannot add a transition to state [" + bean.getEndWorkflowState().getName() + "], because it is disabled");
		}

		// Only need to validate field values if performing an insert or update.
		if (bean.isAutomaticDelayed()) {
			ValidationUtils.assertTrue(bean.isAutomatic(), "Transitions can only be delayed when they are automatic transitions.", "automaticDelayed");
		}

		// Transition Entry screens don't make sense to use if the transition is:
		// automatic or system defined
		// or is an initial state assignment (i.e. start state is null)
		// since these are not processed by a user clicking the transition button in the toolbar
		if (bean.isUseTransitionEntryScreen()) {
			ValidationUtils.assertFalse(bean.isAutomatic(), "Transition Entry Detail Screens cannot be used for automatic transitions");
			ValidationUtils.assertFalse(bean.isSystemDefined(), "Transition Entry Detail Screens cannot be used for system defined transitions");
			ValidationUtils.assertNotNull(bean.getStartWorkflowState(), "Transition Entry Detail Screens cannot be used for initial state assignments (i.e. Start State must be defined).");
		}

		if (bean.getStartWorkflowState() != null) {
			if (!bean.getStartWorkflowState().getWorkflow().equals(bean.getEndWorkflowState().getWorkflow())) {
				throw new ValidationException("Workflow Transition Start & End States must belong to the same Workflow");
			}

			if (bean.getStartWorkflowState().equals(bean.getEndWorkflowState())) {
				ValidationUtils.assertFalse(bean.isAutomatic(), "Workflow Transitions cannot be automatic if the Start and End States are the same.", "endWorkflowState");
			}
		}
		else {
			// Verify only one initial state (i.e. transition where start state is null)
			WorkflowTransition initialTransition = getWorkflowTransitionService().getWorkflowTransitionFirst(bean.getEndWorkflowState().getWorkflow());
			if (initialTransition != null && !initialTransition.equals(bean)) {
				throw new ValidationException("Only one initial transition is allowed per workflow.  There already exists an initial transition [" + initialTransition.getName() + "] for Workflow ["
						+ bean.getEndWorkflowState().getWorkflow().getName() + "]");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
