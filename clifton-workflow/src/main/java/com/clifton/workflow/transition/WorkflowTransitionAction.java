package com.clifton.workflow.transition;


import com.clifton.core.beans.BaseEntity;
import com.clifton.system.bean.SystemBean;
import com.clifton.workflow.WorkflowAware;


/**
 * The <code>WorkflowTransitionAction</code> class defines an action {@link SystemBean} that should be executed
 * when a particular {@link WorkflowAware} is transitioned from one state to another.
 *
 * @author Mary Anderson
 */
public class WorkflowTransitionAction extends BaseEntity<Integer> {

	private WorkflowTransition workflowTransition;
	private SystemBean actionSystemBean;

	/**
	 * If true, action will be executed before entity is transitioned.
	 */
	private boolean beforeTransition;

	/**
	 * Specifies whether this action should be executed outside of the transaction that performs the transition or not.
	 * If it's executed outside and before and transition fails, then the action will not be rolled back.
	 * If it's executed outside and after and the action fails, then the transition will still be committed.
	 */
	private boolean separateTransaction;

	private int order;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTransition getWorkflowTransition() {
		return this.workflowTransition;
	}


	public void setWorkflowTransition(WorkflowTransition workflowTransition) {
		this.workflowTransition = workflowTransition;
	}


	public SystemBean getActionSystemBean() {
		return this.actionSystemBean;
	}


	public void setActionSystemBean(SystemBean actionSystemBean) {
		this.actionSystemBean = actionSystemBean;
	}


	public boolean isBeforeTransition() {
		return this.beforeTransition;
	}


	public void setBeforeTransition(boolean beforeTransition) {
		this.beforeTransition = beforeTransition;
	}


	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public boolean isSeparateTransaction() {
		return this.separateTransaction;
	}


	public void setSeparateTransaction(boolean separateTransaction) {
		this.separateTransaction = separateTransaction;
	}
}
