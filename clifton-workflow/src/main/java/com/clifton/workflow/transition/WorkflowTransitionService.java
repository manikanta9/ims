package com.clifton.workflow.transition;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.search.WorkflowTransitionActionSearchForm;
import com.clifton.workflow.transition.search.WorkflowTransitionSearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The WorkflowTransitionService class defines methods for working with WorkflowTransition objects.
 *
 * @author manderson
 */
public interface WorkflowTransitionService {

	////////////////////////////////////////////////////////////////////////////
	////////       Workflow Transition Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowTransition getWorkflowTransition(int id);


	/**
	 * Returns the valid transition associated with moving the {@link WorkflowAware} dtoInstance from the given state
	 * to the given state.  If there is a valid automatic transition, then it assumes that is the transition,
	 * otherwise if there are no valid automatic transitions, then it assumes any valid manual transitions.
	 */
	public WorkflowTransition getWorkflowTransitionForEntity(WorkflowAware dtoInstance, WorkflowState fromState, WorkflowState toState);


	/**
	 * Returns the valid transition associated with moving the {@link WorkflowAware} dtoInstance from the given state
	 * to the given state.  If there is a valid automatic transition, then it assumes that is the transition,
	 * otherwise if there are no valid automatic transitions, then it assumes any valid manual transitions.
	 * <p>
	 * Also appends to messages any true reasons why or why not a validate transition (if transition exists with condition),
	 */
	public WorkflowTransition getWorkflowTransitionForEntity(WorkflowAware dtoInstance, WorkflowState fromState, WorkflowState toState, StringBuilder messages);


	/**
	 * Returns, if applicable, the next transition that is automatic for the
	 * given bean.  Appends to message the reason why the condition passed.
	 */
	public WorkflowTransition getWorkflowTransitionNext(WorkflowAware dtoInstance, StringBuilder message);


	/**
	 * Returns the first transition for a given workflow.
	 */
	public WorkflowTransition getWorkflowTransitionFirst(Workflow workflow);


	public List<WorkflowTransition> getWorkflowTransitionList(WorkflowTransitionSearchForm searchForm);


	/**
	 * Returns all WorkflowTransitions with the given state as their start state. If automatic is true then returns only
	 * automatic transitions, otherwise returns all manual transitions
	 */
	public List<WorkflowTransition> getWorkflowTransitionListFromState(WorkflowState startState, boolean automatic);


	/**
	 * Returns all next transitions available for the given entity.
	 * Also includes the current state as a "non" transition option in the list.
	 */
	public List<WorkflowTransition> getWorkflowTransitionNextListByEntity(String tableName, Long id);


	@ModelAttribute("data")
	public List<WorkflowTransition> getWorkflowTransitionNextList(WorkflowTransitionCommand command);


	public List<WorkflowTransition> getWorkflowTransitionListToEndState(WorkflowState endState);


	/**
	 * Retrieves the full list of transitions which may be used to transition from the given start state to the given end state.
	 */
	public List<WorkflowTransition> getWorkflowTransitionListFromStartToEndState(WorkflowState startState, WorkflowState endState);


	public List<WorkflowTransition> getWorkflowTransitionListByStartOrEndState(WorkflowState state);


	public List<WorkflowTransition> getWorkflowTransitionListByWorkflow(final short workflowId);


	public WorkflowTransition saveWorkflowTransition(WorkflowTransition bean);


	public void deleteWorkflowTransition(int id);


	public void deleteWorkflowTransitionList(List<WorkflowTransition> deleteList);


	/**
	 * Transitions the specified entity to the end workflow state of the specified transition.
	 * Finds the entity for the specified table and id, sets new workflow state for it
	 * and saves the entity which will execute corresponding workflow transition.
	 * <p>
	 * Can optionally bypass transaction if notExecutedInTransaction = true.
	 * <p>
	 * NOTE: this method will fail on system defined transitions. Use 'executeWorkflowTransitionSystemDefined' for system defined transitions.
	 * <p>
	 * The method applies custom security which requires UPDATE permission to the entity being transitioned.
	 */
	@RequestMapping("workflowTransitionExecuteByTransition")
	@SecureMethod(dynamicTableNameUrlParameter = "tableName", permissionResolverBeanName = "workflowTransitionSecureMethodPermissionResolver")
	public WorkflowAware executeWorkflowTransitionByTransition(String tableName, long id, int workflowTransitionId);


	/**
	 * Executes workflow transition by transition, but uses the given entity for saving
	 * This is used for same state transition saves where we can't just save the bean with the new state to trigger the transition but have to call the transition directly
	 */
	@DoNotAddRequestMapping
	public <T extends WorkflowAware> T executeWorkflowAwareSaveWithSameStateTransition(T entity, int workflowTransitionId);


	/**
	 * Executes workflow transition by transition, but uses the given entity for saving
	 * This is used for same state SYSTEM DEFINED transition saves where we can't just save the bean with the new state to trigger the transition but have to call the transition directly
	 */
	@DoNotAddRequestMapping
	public <T extends WorkflowAware> T executeWorkflowAwareSaveWithSameStateTransitionSystemDefined(T entity, int workflowTransitionId);


	/**
	 * Transitions the specified entity to the specified workflow state.
	 * Finds the entity for the specified table and id, sets new workflow state for it
	 * and saves the entity which will execute corresponding workflow transition.
	 * <p>
	 * ALWAYS EXECUTES IN A TRANSACTION
	 * <p>
	 * NOTE: this method will fail on system defined transitions. Use 'executeWorkflowTransitionSystemDefined' for system defined transitions.
	 * <p>
	 * The method applies custom security which requires UPDATE permission to the entity being transitioned.
	 */
	@RequestMapping("workflowTransitionExecute")
	@SecureMethod(dynamicTableNameUrlParameter = "tableName")
	public WorkflowAware executeWorkflowTransition(String tableName, long id, short newWorkflowStateId);


	/**
	 * Transitions the specified entity to the specified workflow state. Allows transition of system defined transitions.
	 */
	@DoNotAddRequestMapping
	public WorkflowAware executeWorkflowTransitionSystemDefined(String tableName, long id, short newWorkflowStateId);


	/**
	 * Transitions one or more entities of the same table to the end workflow state of the specified transition for the provided command.
	 * If more than one entity ID is specified, each entity is transitioned similar to calling {@link #executeWorkflowTransitionByTransition(String, long, int)} or
	 * {@link #executeWorkflowTransition(String, long, short)} for each, depending on whether {@link WorkflowTransitionCommand#workflowTransitionId} or
	 * {@link WorkflowTransitionCommand#newWorkflowStateId} is defined.
	 * <p>
	 * Returns the list of ending states of each entity provided.
	 */
	@RequestMapping("workflowTransitionExecuteForCommand")
	@SecureMethod(dynamicTableNameUrlParameter = "tableName", permissionResolverBeanName = "workflowTransitionSecureMethodPermissionResolver")
	public Status executeWorkflowTransitionForCommand(WorkflowTransitionCommand command);

	////////////////////////////////////////////////////////////////////////////
	///////      Workflow Transition Action Business Methods            ////////
	////////////////////////////////////////////////////////////////////////////


	public List<WorkflowTransitionAction> getWorkflowTransitionActionListForTransitionId(int workflowTransitionId);


	public List<WorkflowTransitionAction> getWorkflowTransitionActionList(WorkflowTransitionActionSearchForm searchForm);


	public List<WorkflowTransition> getWorkflowTransitionListForCommand(WorkflowTransitionSearchCommand command);
}
