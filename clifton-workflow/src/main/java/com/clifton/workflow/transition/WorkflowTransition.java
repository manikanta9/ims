package com.clifton.workflow.transition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.workflow.definition.WorkflowState;

import java.util.List;


/**
 * The WorkflowTransition class defines the flow from one state to another and under what circumstances that move can happen.
 *
 * @author manderson
 */
public class WorkflowTransition extends NamedEntity<Integer> {

	private WorkflowState startWorkflowState;
	private WorkflowState endWorkflowState;

	/**
	 * By default users must have WRITE access to the table of the entity they are transitioning
	 * In some rare cases, users having READ Access is sufficient.  This is usually used in combination
	 * with a condition selection to further restrict those users.
	 */
	private boolean readAccessSufficientToExecute;

	/**
	 * Optional condition that must evaluate to TRUE when set in order to execute this transition.
	 */
	private SystemCondition condition;
	/**
	 * An automatic transition is one in which the system would move the entity from the start to end state based upon specified conditions.
	 */
	private boolean automatic;
	/**
	 * If {@code true}, then {@link #automatic} transitions will be delayed until the end of the current request <i>if</i> an in-progress request is active.
	 *
	 * @see WorkflowDelayedTransitionInterceptor
	 */
	private boolean automaticDelayed;

	/**
	 * Used with automatic transitions, for the rare cases where more than one can apply and setting up conditions can be difficult (see Reconciliation: Affirm, Confirm-Ops, Confirm-Docs workflow)
	 * priority order can be set to indicate first one in wins.  If multiple automatic transitions exist, first one in wins will only occur if all transitions have an order and then
	 * the one with the smallest order will be used.
	 */
	private Short priorityOrder;

	/**
	 * System define transitions cannot be triggered by users. They must be executed by the system.
	 * TODO: how do we enforce them other than disabling UI? Template with thread local?
	 */
	private boolean systemDefined;


	/**
	 * By default all transitions are executed in a transaction (including corresponding actions and auto-transitions).
	 * If rare cases you want to disable this to prevent database locking (workflow transition with an action that generates a report that needs the same row)
	 */
	private boolean notExecutedInTransaction;

	/**
	 * Used to hide the transition from being visible on the client.
	 * In rare cases a transition is automatic under specific conditions and is executed in a chain of transitions triggered by the user, so the system executed flag does not work.
	 * An example is the Trade Workflow's 'Auto Book and Post Trade' transition.
	 */
	private boolean hidden;

	/**
	 * Set automatically by the system.  Used when transitioning entities to determine if
	 * we need to lookup actions.
	 */
	private boolean beforeActionsExist;
	private boolean afterActionsExist;

	/**
	 * Optional sort order of this transition (used in UI).
	 */
	private Integer order;

	/**
	 * Used for opening a custom window (defined by the entryDetailPageClass)
	 * to have users perform an action before executing the transition.  For example,
	 * may want a user to enter a note and then perform the transition.  Default data
	 * can be used to pre-populate some of the data on the window - i.e. which note type
	 */
	private String entryDetailScreenClass;
	private String entryDefaultData;

	@OneToManyEntity(serviceBeanName = "workflowTransitionService", serviceMethodName = "getWorkflowTransitionActionListForTransitionId")
	private List<WorkflowTransitionAction> actionList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isActionsExist() {
		return isBeforeActionsExist() || isAfterActionsExist();
	}


	public boolean isEntryDetailScreenClassPopulated() {
		return (getEntryDetailScreenClass() != null);
	}


	public Short getEndStateId() {
		if (getEndWorkflowState() != null) {
			return getEndWorkflowState().getId();
		}
		return null;
	}


	public boolean isUseTransitionEntryScreen() {
		return !StringUtils.isEmpty(getEntryDetailScreenClass());
	}


	public boolean isSameStateTransition() {
		return CompareUtils.isEqual(getStartWorkflowState(), getEndWorkflowState());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowState getStartWorkflowState() {
		return this.startWorkflowState;
	}


	public void setStartWorkflowState(WorkflowState startWorkflowState) {
		this.startWorkflowState = startWorkflowState;
	}


	public WorkflowState getEndWorkflowState() {
		return this.endWorkflowState;
	}


	public void setEndWorkflowState(WorkflowState endWorkflowState) {
		this.endWorkflowState = endWorkflowState;
	}


	public boolean isReadAccessSufficientToExecute() {
		return this.readAccessSufficientToExecute;
	}


	public void setReadAccessSufficientToExecute(boolean readAccessSufficientToExecute) {
		this.readAccessSufficientToExecute = readAccessSufficientToExecute;
	}


	public SystemCondition getCondition() {
		return this.condition;
	}


	public void setCondition(SystemCondition condition) {
		this.condition = condition;
	}


	public boolean isAutomatic() {
		return this.automatic;
	}


	public void setAutomatic(boolean automatic) {
		this.automatic = automatic;
	}


	public boolean isAutomaticDelayed() {
		return this.automaticDelayed;
	}


	public void setAutomaticDelayed(boolean automaticDelayed) {
		this.automaticDelayed = automaticDelayed;
	}


	public Short getPriorityOrder() {
		return this.priorityOrder;
	}


	public void setPriorityOrder(Short priorityOrder) {
		this.priorityOrder = priorityOrder;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isNotExecutedInTransaction() {
		return this.notExecutedInTransaction;
	}


	public void setNotExecutedInTransaction(boolean notExecutedInTransaction) {
		this.notExecutedInTransaction = notExecutedInTransaction;
	}


	public boolean isHidden() {
		return this.hidden;
	}


	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}


	public boolean isBeforeActionsExist() {
		return this.beforeActionsExist;
	}


	public void setBeforeActionsExist(boolean beforeActionsExist) {
		this.beforeActionsExist = beforeActionsExist;
	}


	public boolean isAfterActionsExist() {
		return this.afterActionsExist;
	}


	public void setAfterActionsExist(boolean afterActionsExist) {
		this.afterActionsExist = afterActionsExist;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public String getEntryDetailScreenClass() {
		return this.entryDetailScreenClass;
	}


	public void setEntryDetailScreenClass(String entryDetailScreenClass) {
		this.entryDetailScreenClass = entryDetailScreenClass;
	}


	public String getEntryDefaultData() {
		return this.entryDefaultData;
	}


	public void setEntryDefaultData(String entryDefaultData) {
		this.entryDefaultData = entryDefaultData;
	}


	public List<WorkflowTransitionAction> getActionList() {
		return this.actionList;
	}


	public void setActionList(List<WorkflowTransitionAction> actionList) {
		this.actionList = actionList;
	}
}
