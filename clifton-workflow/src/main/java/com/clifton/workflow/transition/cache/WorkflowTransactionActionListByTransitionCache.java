package com.clifton.workflow.transition.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionAction;
import org.springframework.stereotype.Component;


/**
 * The <code>WorkflowTransactionActionListByTransitionCache</code> caches the list of
 * {@link WorkflowTransitionAction} beans for a {@link WorkflowTransition} - stores empty
 * list for transitions without actions
 *
 * @author manderson
 */
@Component
public class WorkflowTransactionActionListByTransitionCache extends SelfRegisteringSingleKeyDaoListCache<WorkflowTransitionAction, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "workflowTransition.id";
	}


	@Override
	protected Integer getBeanKeyValue(WorkflowTransitionAction bean) {
		if (bean.getWorkflowTransition() != null) {
			return bean.getWorkflowTransition().getId();
		}
		return null;
	}
}
