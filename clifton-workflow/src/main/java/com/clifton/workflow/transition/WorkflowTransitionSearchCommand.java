package com.clifton.workflow.transition;


/**
 * @author DipinC
 */
public class WorkflowTransitionSearchCommand {


	private String workflowName;
	private String workflowTransitionName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getWorkflowName() {
		return this.workflowName;
	}


	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}


	public String getWorkflowTransitionName() {
		return this.workflowTransitionName;
	}


	public void setWorkflowTransitionName(String workflowTransitionName) {
		this.workflowTransitionName = workflowTransitionName;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static WorkflowTransitionSearchCommand of(String workflowName, String transitionName) {
		WorkflowTransitionSearchCommand workflowTransitionSearchCommand = new WorkflowTransitionSearchCommand();
		workflowTransitionSearchCommand.workflowName = workflowName;
		workflowTransitionSearchCommand.workflowTransitionName = transitionName;
		return workflowTransitionSearchCommand;
	}
}
