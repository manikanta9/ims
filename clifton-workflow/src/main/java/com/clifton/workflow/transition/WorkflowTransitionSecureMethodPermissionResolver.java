package com.clifton.workflow.transition;

import com.clifton.core.security.authorization.SecureMethodPermissionResolver;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.MapUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;


/**
 * WorkflowTransitionSecureMethodPermissionResolver returns permission level required in order to execute a specific transition
 * (where the transition id is passed as a parameter in the request).
 *
 * @author manderson
 */
@Component
public class WorkflowTransitionSecureMethodPermissionResolver implements SecureMethodPermissionResolver {

	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public int getRequiredPermissions(Method method, Map<String, ?> config, String securityResourceName) {
		WorkflowTransition transition = getWorkflowTransition(config);
		if (transition != null && transition.isReadAccessSufficientToExecute()) {
			return SecurityPermission.PERMISSION_READ;
		}
		return SecurityPermission.PERMISSION_WRITE;
	}

	////////////////////////////////////////////////////////////////////////////////


	private WorkflowTransition getWorkflowTransition(Map<String, ?> config) {
		Integer transitionId = MapUtils.getParameterAsInteger("workflowTransitionId", config);
		if (transitionId != null) {
			return getWorkflowTransitionService().getWorkflowTransition(transitionId);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////             Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
