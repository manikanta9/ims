package com.clifton.workflow.transition;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.concurrent.synchronize.SynchronizableBuilder;
import com.clifton.core.util.concurrent.synchronize.handler.SynchronizationHandler;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.search.WorkflowTransitionActionSearchForm;
import com.clifton.workflow.transition.search.WorkflowTransitionSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * The <code>WorkflowTransitionServiceImpl</code> class provides default implementation of {@linkplain WorkflowTransitionService}.
 *
 * @author manderson
 */
@Service
public class WorkflowTransitionServiceImpl implements WorkflowTransitionService {

	private AdvancedUpdatableDAO<WorkflowTransition, Criteria> workflowTransitionDAO;
	private AdvancedUpdatableDAO<WorkflowTransitionAction, Criteria> workflowTransitionActionDAO;

	private DaoSingleKeyListCache<WorkflowTransitionAction, Integer> workflowTransactionActionListByTransitionCache;

	private DaoSingleKeyListCache<WorkflowTransition, Short> workflowTransitionListByWorkflowCache;


	private RunnerHandler runnerHandler;

	private SynchronizationHandler synchronizationHandler;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private WorkflowDefinitionService workflowDefinitionService;

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowTransition getWorkflowTransition(int id) {
		WorkflowTransition trans = getWorkflowTransitionDAO().findByPrimaryKey(id);
		if (trans != null) {
			trans.setActionList(getWorkflowTransitionActionListForTransitionId(trans.getId()));
		}
		return trans;
	}


	@Override
	public WorkflowTransition getWorkflowTransitionForEntity(WorkflowAware dtoInstance, WorkflowState fromState, WorkflowState toState) {
		return getWorkflowTransitionForEntity(dtoInstance, fromState, toState, null);
	}


	@Override
	public WorkflowTransition getWorkflowTransitionForEntity(WorkflowAware dtoInstance, WorkflowState fromState, WorkflowState toState, StringBuilder messages) {
		WorkflowTransition validTransition = null;

		List<WorkflowTransition> transitionList;
		if (fromState == null) {
			transitionList = CollectionUtils.createList(getWorkflowTransitionFirst(toState.getWorkflow()));
		}
		else {
			transitionList = getTransitionListFromState(fromState);
		}
		if (CollectionUtils.isEmpty(transitionList)) {
			if (messages != null) {
				messages.append("no transition exists from [").append(fromState == null ? "" : fromState.getName()).append("] to [").append(toState.getName()).append("]");
			}
			return null;
		}

		// While iterating possible transitions to apply, track any condition evaluation result messages.
		// If we find a valid transition, we only want to append the message for that transition.
		// If a valid transition is not found, then append all available failed messages so they can be reported.
		Map<Integer, String> transitionToConditionMessageMap = new HashMap<>();
		for (WorkflowTransition transition : transitionList) {
			if (!transition.getEndWorkflowState().equals(toState)) {
				continue;
			}
			// If conditions don't pass, skip it
			if (transition.getCondition() != null) {
				EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(transition.getCondition(), dtoInstance);
				// Track Result message
				transitionToConditionMessageMap.put(transition.getId(), result.getMessage());
				// If Result is False, continue to next transition
				if (!result.isResult()) {
					continue;
				}
			}
			//  There can only be one valid automatic transition.  If there is a validTransition that is NOT automatic, don't use that one and use the automatic one instead
			if (transition.isAutomatic()) {
				if (validTransition != null && validTransition.isAutomatic()) {
					throw new IllegalStateException("More than one valid automatic transition from " + (fromState == null ? "[]" : fromState.getName()) + " to " + toState + " for bean " + dtoInstance);
				}
				validTransition = transition;
			}
			// If it is a manual transition, only set the validTransition to it if none exists. (don't want to overwrite a valid automatic transition)
			else if (validTransition == null) {
				validTransition = transition;
			}
		}

		if (messages != null) {
			if (validTransition == null) {
				if (transitionToConditionMessageMap.isEmpty()) {
					messages.append("cannot find a valid transition from [").append(fromState == null ? "" : fromState.getName()).append("] to [").append(toState.getName()).append("]");
				}
				else {
					// append failed transition messages since we could not find a valid one
					transitionToConditionMessageMap.values().forEach(messages::append);
				}
			}
			else {
				Optional.ofNullable(transitionToConditionMessageMap.get(validTransition.getId()))
						.ifPresent(messages::append);
			}
		}
		return validTransition;
	}


	@Override
	public WorkflowTransition getWorkflowTransitionNext(WorkflowAware dtoInstance, StringBuilder message) {
		List<WorkflowTransition> transitionList = getWorkflowTransitionListFromState(dtoInstance.getWorkflowState(), true);

		// remove transitions that don't match required condition

		if (!CollectionUtils.isEmpty(transitionList)) {
			for (Iterator<WorkflowTransition> iterator = transitionList.iterator(); iterator.hasNext(); ) {
				WorkflowTransition transition = iterator.next();
				// if no condition is set then this transition is allowed
				if (transition.getCondition() != null) {
					EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(transition.getCondition(), dtoInstance);
					if (result == null || !result.isResult()) {
						iterator.remove();
					}
					else {
						message.append(result.getMessage());
					}
				}
			}
		}

		if (CollectionUtils.isEmpty(transitionList)) {
			return null;
		}
		if (CollectionUtils.getSize(transitionList) > 1) {
			// If Priority Order Exists on all, then return first one (smallest order) - Otherwise throw an exception
			WorkflowTransition first = null;
			for (WorkflowTransition transition : transitionList) {
				// One without order - clear first and  break out of loop
				if (transition.getPriorityOrder() == null) {
					first = null;
					break;
				}
				else if (first == null || MathUtils.isLessThan(transition.getPriorityOrder(), first.getPriorityOrder())) {
					first = transition;
				}
			}
			if (first == null) {
				throw new ValidationException("There are [" + CollectionUtils.getSize(transitionList) + "] automatic transitions [" + BeanUtils.getPropertyValues(transitionList, "name", ",") + "] allowed for bean " + dtoInstance);
			}
			return first;
		}
		return CollectionUtils.getOnlyElement(transitionList);
	}


	@Override
	public List<WorkflowTransition> getWorkflowTransitionList(WorkflowTransitionSearchForm searchForm) {
		return getWorkflowTransitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<WorkflowTransition> getWorkflowTransitionNextListByEntity(String tableName, Long id) {
		return getWorkflowTransitionNextList(WorkflowTransitionCommand.of(tableName, id));
	}


	@Override
	public List<WorkflowTransition> getWorkflowTransitionListForCommand(WorkflowTransitionSearchCommand command) {
		ValidationUtils.assertNotNull(command, "Command object is null.");
		ValidationUtils.assertNotNull(command.getWorkflowName(), "WorkflowName is required");
		List<WorkflowTransition> wrkflowTransitions = this.workflowTransitionListByWorkflowCache.getBeanListForKeyValue(getWorkflowTransitionDAO(), getWorkflowDefinitionService().getWorkflowByName(command.getWorkflowName()).getId());
		//filtering for WorkflowTransition Name if provided
		return Optional.ofNullable(wrkflowTransitions)
				.orElseGet(Collections::emptyList).stream()
				.filter(x -> (command.getWorkflowTransitionName() != null && x.getName() != null && x.getName().equals(command.getWorkflowTransitionName())))
				.collect(Collectors.toList());
	}


	@Override
	public List<WorkflowTransition> getWorkflowTransitionNextList(WorkflowTransitionCommand command) {
		if (command.getId() == null) {
			return null;
		}
		ReadOnlyDAO<WorkflowAware> dao = getDaoLocator().locate(command.getTableName());
		ValidationUtils.assertNotNull(dao, "Invalid table name: " + command.getTableName(), "tableName");
		WorkflowAware bean = dao.findByPrimaryKey(command.getId());
		if (bean == null || bean.getWorkflowState() == null) {
			return null;
		}
		List<WorkflowTransition> transitionList = getTransitionListFromState(bean.getWorkflowState());
		if (!CollectionUtils.isEmpty(transitionList)) {
			if (command.isExcludeSystemDefined()) {
				transitionList = BeanUtils.filter(transitionList, t -> !t.isSystemDefined());
			}
			if (command.isExcludeHidden()) {
				transitionList = BeanUtils.filter(transitionList, t -> !t.isHidden());
			}
			if (command.isEvaluateCondition()) {
				transitionList = BeanUtils.filter(transitionList, transition -> {
					if (transition.getCondition() == null) {
						return true;
					}
					return getSystemConditionEvaluationHandler().isConditionTrue(transition.getCondition(), bean);
				});
			}
			if (!command.isExcludeNoTransition()) {
				// Add No Transition as an option
				WorkflowTransition transition = new WorkflowTransition();
				transition.setEndWorkflowState(bean.getWorkflowState());
				transition.setName(bean.getWorkflowState().getLabel());
				transitionList.add(0, transition);
			}
		}
		return transitionList;
	}


	@Override
	public List<WorkflowTransition> getWorkflowTransitionListFromState(WorkflowState startState, boolean automatic) {
		List<WorkflowTransition> list = getTransitionListFromState(startState);
		return BeanUtils.filter(list, WorkflowTransition::isAutomatic, automatic);
	}


	private List<WorkflowTransition> getTransitionListFromState(WorkflowState startState) {
		AssertUtils.assertNotNull(startState, "Start State cannot be null");

		List<WorkflowTransition> transitionList = getWorkflowTransitionListByWorkflow(startState.getWorkflow().getId());
		return CollectionUtils.getFiltered(transitionList, transition -> startState.equals(transition.getStartWorkflowState()));
	}


	@Override
	public List<WorkflowTransition> getWorkflowTransitionListToEndState(WorkflowState endState) {
		AssertUtils.assertNotNull(endState, "End State cannot be null");

		List<WorkflowTransition> transitionList = getWorkflowTransitionListByWorkflow(endState.getWorkflow().getId());
		return CollectionUtils.getFiltered(transitionList, transition -> endState.equals(transition.getEndWorkflowState()));
	}


	@Override
	public List<WorkflowTransition> getWorkflowTransitionListFromStartToEndState(WorkflowState startState, WorkflowState endState) {
		ValidationUtils.assertEquals(startState.getWorkflow(), endState.getWorkflow(), "The start state and end state must use the same workflow when retrieving the workflow state transition list.");
		List<WorkflowTransition> transitionList = getWorkflowTransitionListToEndState(endState);
		return CollectionUtils.getFiltered(transitionList, transition -> startState.equals(transition.getStartWorkflowState()));
	}


	@Override
	public List<WorkflowTransition> getWorkflowTransitionListByStartOrEndState(final WorkflowState state) {
		return CollectionUtils.combineCollections(getTransitionListFromState(state), getWorkflowTransitionListToEndState(state));
	}


	@Override
	public WorkflowTransition getWorkflowTransitionFirst(final Workflow workflow) {
		List<WorkflowTransition> transitionList = getWorkflowTransitionListByWorkflow(workflow.getId());
		return CollectionUtils.getOnlyElement(CollectionUtils.getFiltered(transitionList, transition -> transition.getStartWorkflowState() == null));
	}


	@Override
	public List<WorkflowTransition> getWorkflowTransitionListByWorkflow(final short workflowId) {
		//  Since End Workflow State is required and start & end states belong to the same workflow
		//  will determine list of transitions based upon the link from the end state to it's workflow.
		//Switch to Cache
		return getWorkflowTransitionListByWorkflowCache().getBeanListForKeyValue(getWorkflowTransitionDAO(), workflowId);
	}


	/**
	 * @see WorkflowTransitionValidator
	 */
	@Override
	@Transactional
	public WorkflowTransition saveWorkflowTransition(WorkflowTransition bean) {
		List<WorkflowTransitionAction> actionList = bean.getActionList();
		bean.setBeforeActionsExist(false);
		bean.setAfterActionsExist(false);
		for (WorkflowTransitionAction action : CollectionUtils.getIterable(actionList)) {
			action.setWorkflowTransition(bean);
			if (action.isBeforeTransition()) {
				bean.setBeforeActionsExist(true);
			}
			else {
				bean.setAfterActionsExist(true);
			}
		}
		bean = getWorkflowTransitionDAO().save(bean);
		getWorkflowTransitionActionDAO().saveList(actionList, bean.isNewBean() ? null : getWorkflowTransitionActionListForTransitionId(bean.getId()));
		bean.setActionList(actionList);
		return bean;
	}


	/**
	 * @see WorkflowTransitionValidator
	 */
	@Override
	@Transactional
	public void deleteWorkflowTransition(int id) {
		getWorkflowTransitionActionDAO().deleteList(getWorkflowTransitionActionListForTransitionId(id));
		getWorkflowTransitionDAO().delete(id);
	}


	/**
	 * @see WorkflowTransitionValidator
	 */
	@Override
	@Transactional
	public void deleteWorkflowTransitionList(List<WorkflowTransition> deleteList) {
		for (WorkflowTransition trans : CollectionUtils.getIterable(deleteList)) {
			if (trans.isBeforeActionsExist() || trans.isAfterActionsExist()) {
				getWorkflowTransitionActionDAO().deleteList(getWorkflowTransitionActionListForTransitionId(trans.getId()));
			}
		}
		getWorkflowTransitionDAO().deleteList(deleteList);
	}


	@Override
	public WorkflowAware executeWorkflowTransitionByTransition(String tableName, long id, int workflowTransitionId) {
		WorkflowTransition transition = getWorkflowTransition(workflowTransitionId);
		ValidationUtils.assertNotNull(transition, "Cannot find workflow transition with id = " + workflowTransitionId, "workflowTransitionId");

		// The transition will be validated in the workflow observer by finding and validating the transition for the current and new states of the bean.
		// If there are multiple transitions with the same state combination, only one applicable (condition null or evaluating to true) will be used.
		return doExecuteWorkflowTransition(tableName, id, transition.getEndStateId(), false, !transition.isNotExecutedInTransaction(), transition.isSameStateTransition() ? transition.getId() : null);
	}


	@Override
	public <T extends WorkflowAware> T executeWorkflowAwareSaveWithSameStateTransition(T entity, int workflowTransitionId) {
		return executeWorkflowAwareSaveWithSameStateTransitionImpl(entity, workflowTransitionId, false);
	}


	@Override
	public <T extends WorkflowAware> T executeWorkflowAwareSaveWithSameStateTransitionSystemDefined(T entity, int workflowTransitionId) {
		return executeWorkflowAwareSaveWithSameStateTransitionImpl(entity, workflowTransitionId, true);
	}


	private <T extends WorkflowAware> T executeWorkflowAwareSaveWithSameStateTransitionImpl(T entity, int workflowTransitionId, boolean allowSystemDefined) {
		WorkflowTransition transition = getWorkflowTransition(workflowTransitionId);
		ValidationUtils.assertNotNull(transition, "Cannot find workflow transition with id = " + workflowTransitionId, "workflowTransitionId");
		ValidationUtils.assertNotNull(entity.getWorkflowState(), "Provided entity " + BeanUtils.getLabel(entity) + " does not have a current workflow state defined");
		ValidationUtils.assertTrue(transition.isSameStateTransition(), "Selected transition [" + transition.getLabel() + "] is not a same state transition.  Use the standard save or transition execution options to perform this transition and save.");
		ValidationUtils.assertTrue(transition.getEndWorkflowState().equals(entity.getWorkflowState()), "Cannot execute same state transition [" + transition.getLabel() + "] because entity is currently in state [" + entity.getWorkflowState().getName() + "], and not in expected state [" + transition.getEndWorkflowState().getName() + "]");

		final UpdatableDAO<T> dao = (UpdatableDAO<T>) getDaoLocator().locate(entity);
		ValidationUtils.assertNotNull(dao, "Cannot find dao for entity: " + entity);
		final List<T> resultList = new ArrayList<>();
		if (allowSystemDefined) {
			WorkflowTransitionUtils.executeSystemDefinedWithSameStateTransition(workflowTransitionId, () -> resultList.add(dao.save(entity)));
		}
		else {
			WorkflowTransitionUtils.executeSameStateTransition(workflowTransitionId, () -> resultList.add(dao.save(entity)));
		}
		return resultList.get(0);
	}


	@Override
	public WorkflowAware executeWorkflowTransition(String tableName, long id, short newWorkflowStateId) {
		// execute in a transaction and don't allow system defined
		return doExecuteWorkflowTransition(tableName, id, newWorkflowStateId, false, true, null);
	}


	@Override
	public WorkflowAware executeWorkflowTransitionSystemDefined(String tableName, long id, short newWorkflowStateId) {
		// execute in a transaction and DO allow system defined
		return doExecuteWorkflowTransition(tableName, id, newWorkflowStateId, true, true, null);
	}


	private WorkflowAware doExecuteWorkflowTransition(String tableName, long id, short newWorkflowStateId, boolean allowSystemDefined, boolean transactional, Integer allowSameStateTransitionId) {
		final UpdatableDAO<WorkflowAware> dao = (UpdatableDAO<WorkflowAware>) getDaoLocator().<WorkflowAware>locate(tableName);
		ValidationUtils.assertNotNull(dao, "Invalid table name: " + tableName, "tableName");

		WorkflowAware bean = null;
		if (!transactional || dao.getConfiguration().isVersionEnabled()) {
			// get the bean before starting transaction: safe to do this because row versioning will take care of transaction isolation
			bean = dao.findByPrimaryKey(id);
			ValidationUtils.assertNotNull(bean, "Cannot find bean with id = " + id + " for table: " + tableName, "id");
		}

		if (transactional) {
			return doExecuteWorkflowTransitionInTransaction(dao, bean, id, newWorkflowStateId, allowSystemDefined, allowSameStateTransitionId);
		}
		return doExecuteWorkflowTransitionForBean(dao, bean, newWorkflowStateId, allowSystemDefined, allowSameStateTransitionId);
	}


	@Transactional(timeout = 90)
	protected WorkflowAware doExecuteWorkflowTransitionInTransaction(UpdatableDAO<WorkflowAware> dao, WorkflowAware bean, long id, short newWorkflowStateId, boolean allowSystemDefined, Integer allowSameStateTransitionId) {
		if (bean == null) {
			// lookup inside of a transaction
			bean = dao.findByPrimaryKey(id);
			ValidationUtils.assertNotNull(bean, "Cannot find bean with id = " + id + " for table: " + dao.getConfiguration().getTableName(), "id");
		}
		return doExecuteWorkflowTransitionForBean(dao, bean, newWorkflowStateId, allowSystemDefined, allowSameStateTransitionId);
	}


	private WorkflowAware doExecuteWorkflowTransitionForBean(final UpdatableDAO<WorkflowAware> dao, final WorkflowAware bean, short newWorkflowStateId, boolean allowSystemDefined, Integer allowSameStateTransitionId) {
		// move the entity to new state
		WorkflowState newWorkflowState = getWorkflowDefinitionService().getWorkflowState(newWorkflowStateId);
		ValidationUtils.assertNotNull(newWorkflowState, "Cannot find workflow state for id = " + newWorkflowStateId, "newWorkflowStateId");

		String lockMessage = "Transitioning " + ContextConventionUtils.getTableNameFromClass(bean.getClass()) + " with ID " + bean.getIdentity() + " from state \"" + bean.getWorkflowState().getName() + "\" to state \"" + newWorkflowState.getName() + "\"";
		String lockKey = bean.getClass().getSimpleName() + '-' + bean.getIdentity();
		return getSynchronizationHandler().call(
				SynchronizableBuilder.forLocking("WORKFLOW-TRANSITION", lockMessage, lockKey)
						.buildWithCallableAction(() -> {
							// If the new state is same as the current state, then a transition will not be performed unless it's flagged to do so.
							// Otherwise do not attempt to update the bean because nothing will be done and we don't want to update last updated by/on
							if (bean.getWorkflowState() != null && bean.getWorkflowState().equals(newWorkflowState) && allowSameStateTransitionId == null) {
								return bean;
							}

							bean.setWorkflowState(newWorkflowState);

							final List<WorkflowAware> resultList = new ArrayList<>();
							if (allowSystemDefined) {
								if (allowSameStateTransitionId != null) {
									WorkflowTransitionUtils.executeSystemDefinedWithSameStateTransition(allowSameStateTransitionId, () -> resultList.add(dao.save(bean)));
								}
								else {
									WorkflowTransitionUtils.executeSystemDefined(() -> resultList.add(dao.save(bean)));
								}
							}
							else {
								if (allowSameStateTransitionId != null) {
									WorkflowTransitionUtils.executeSameStateTransition(allowSameStateTransitionId, () -> resultList.add(dao.save(bean)));
								}
								else {
									resultList.add(dao.save(bean));
								}
							}
							return resultList.get(0);
						}));
	}


	@Override
	public Status executeWorkflowTransitionForCommand(WorkflowTransitionCommand command) {
		Supplier<Status> statusSupplier = () -> command.isExecuteInSingleTransaction()
				? doExecuteWorkflowTransitionForCommandInTransaction(command)
				: doExecuteWorkflowTransitionForCommand(command);

		if (!command.isAsynchronous()) {
			return statusSupplier.get();
		}

		// asynchronous run support; initialize the status object before submitting the task so the status object can be returned
		Status status = command.getStatus();
		if (status == null) {
			status = Status.ofMessage("Executing Workflow Transition(s) for " + command.getRunId());
			command.setStatus(status);
		}
		getRunnerHandler().runNow(new AbstractStatusAwareRunner("WORKFLOW-TRANSITION", command.getRunId(), new Date(), command) {
			@Override
			public void run() {
				statusSupplier.get();
			}
		});
		return status;
	}


	@Transactional(timeout = 90)
	protected Status doExecuteWorkflowTransitionForCommandInTransaction(WorkflowTransitionCommand command) {
		return doExecuteWorkflowTransitionForCommand(command);
	}


	private Status doExecuteWorkflowTransitionForCommand(WorkflowTransitionCommand command) {
		Status status = command.getStatus() == null ? Status.ofMessage("Executing Workflow Transition(s) for " + command.getRunId()) : command.getStatus();

		final UpdatableDAO<WorkflowAware> dao = (UpdatableDAO<WorkflowAware>) getDaoLocator().<WorkflowAware>locate(command.getTableName());
		ValidationUtils.assertNotNull(dao, "Invalid table name: " + command.getTableName(), "tableName");

		WorkflowState newWorkflowState;
		WorkflowTransition transition = null;
		boolean allowSameStateTransition = false;  // Only set to true if a specific transition is specified that is a same state transition
		if (command.getNewWorkflowStateId() != null) {
			newWorkflowState = getWorkflowDefinitionService().getWorkflowState(command.getNewWorkflowStateId());
			ValidationUtils.assertNotNull(newWorkflowState, "Cannot find workflow state for id = " + command.getNewWorkflowStateId(), "newWorkflowStateId");
		}
		else {
			transition = getWorkflowTransition(command.getWorkflowTransitionId());
			ValidationUtils.assertNotNull(transition, "Cannot find workflow transition with id = " + command.getWorkflowTransitionId(), "workflowTransitionId");
			newWorkflowState = transition.getEndWorkflowState();
			allowSameStateTransition = transition.isSameStateTransition();
		}

		Long[] ids = ArrayUtils.add(command.getIds(), command.getId());

		int attemptedCount = 0;
		int successCount = 0;
		if (ids != null) {
			// Transition each entity in its own transaction.
			for (Long id : ids) {
				attemptedCount++;

				try {
					WorkflowAware bean = dao.findByPrimaryKey(id);
					if (bean == null) {
						status.addError("Cannot find bean with id = " + id + " for table: " + dao.getConfiguration().getTable().getName());
					}
					else if (bean.getWorkflowState().equals(newWorkflowState) && !allowSameStateTransition) {
						status.addSkipped("Workflow Transition skipped (already in state and not a same state transition) for bean: " + bean);
					}
					else {
						WorkflowTransition validTransition = getWorkflowTransitionForEntity(bean, bean.getWorkflowState(), newWorkflowState);
						if (validTransition != null) {
							if (transition == null || !transition.isNotExecutedInTransaction()) {
								doExecuteWorkflowTransitionInTransaction(dao, bean, id, newWorkflowState.getId(), command.isAllowExecuteSystemDefined(), allowSameStateTransition ? command.getWorkflowTransitionId() : null);
							}
							else {
								doExecuteWorkflowTransitionForBean(dao, bean, newWorkflowState.getId(), command.isAllowExecuteSystemDefined(), allowSameStateTransition ? command.getWorkflowTransitionId() : null);
							}
							successCount++;
						}
						else {
							status.addSkipped("Workflow Transition skipped (invalid transition) for bean: " + bean);
						}
					}
				}
				catch (Exception e) {
					status.addError(ExceptionUtils.getDetailedMessage(e));
					if (command.isStopOnFirstError()) {
						break;
					}
				}
			}
		}
		status.setMessage("Total Transitions to process: " + ArrayUtils.getLength(ids) + ". Processed " + attemptedCount + ": " + successCount + " Successful. " + (status.getSkippedCount() > 0 ? status.getSkippedCount() + " Skipped. " : "") + (status.getErrorCount() > 0 ? status.getErrorCount() + "Errors." : ""));
		status.setActionPerformed(successCount > 0);
		if (command.isStopOnFirstError()) {
			status.appendToMessage(" Stopped processing on first error.");
		}
		return status;
	}

	////////////////////////////////////////////////////////////////////////////
	///////      Workflow Transition Action Business Methods            ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<WorkflowTransitionAction> getWorkflowTransitionActionListForTransitionId(int workflowTransitionId) {
		return getWorkflowTransactionActionListByTransitionCache().getBeanListForKeyValue(getWorkflowTransitionActionDAO(), workflowTransitionId);
	}


	@Override
	public List<WorkflowTransitionAction> getWorkflowTransitionActionList(WorkflowTransitionActionSearchForm searchForm) {
		return getWorkflowTransitionActionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<WorkflowTransition, Criteria> getWorkflowTransitionDAO() {
		return this.workflowTransitionDAO;
	}


	public void setWorkflowTransitionDAO(AdvancedUpdatableDAO<WorkflowTransition, Criteria> workflowTransitionDAO) {
		this.workflowTransitionDAO = workflowTransitionDAO;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public SynchronizationHandler getSynchronizationHandler() {
		return this.synchronizationHandler;
	}


	public void setSynchronizationHandler(SynchronizationHandler synchronizationHandler) {
		this.synchronizationHandler = synchronizationHandler;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public AdvancedUpdatableDAO<WorkflowTransitionAction, Criteria> getWorkflowTransitionActionDAO() {
		return this.workflowTransitionActionDAO;
	}


	public void setWorkflowTransitionActionDAO(AdvancedUpdatableDAO<WorkflowTransitionAction, Criteria> workflowTransitionActionDAO) {
		this.workflowTransitionActionDAO = workflowTransitionActionDAO;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public DaoSingleKeyListCache<WorkflowTransitionAction, Integer> getWorkflowTransactionActionListByTransitionCache() {
		return this.workflowTransactionActionListByTransitionCache;
	}


	public void setWorkflowTransactionActionListByTransitionCache(DaoSingleKeyListCache<WorkflowTransitionAction, Integer> workflowTransactionActionListByTransitionCache) {
		this.workflowTransactionActionListByTransitionCache = workflowTransactionActionListByTransitionCache;
	}


	public DaoSingleKeyListCache<WorkflowTransition, Short> getWorkflowTransitionListByWorkflowCache() {
		return this.workflowTransitionListByWorkflowCache;
	}


	public void setWorkflowTransitionListByWorkflowCache(DaoSingleKeyListCache<WorkflowTransition, Short> workflowTransitionListByWorkflowCache) {
		this.workflowTransitionListByWorkflowCache = workflowTransitionListByWorkflowCache;
	}
}
