package com.clifton.workflow.transition.action;

import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreStringUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.email.Email;
import com.clifton.core.util.email.EmailHandler;
import com.clifton.core.util.email.MimeContentTypes;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.transition.WorkflowTransition;

import java.util.List;


/**
 * The {@link EmailSendingWorkflowActionHandler} is used to generate and send an email after a Workflow Transition.
 *
 * @author lnaylor
 */
public class EmailSendingWorkflowActionHandler<T extends WorkflowAware> implements WorkflowTransitionActionHandler<T>, ValidationAware {

	private EmailHandler emailHandler;

	private TemplateConverter templateConverter;

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<String> toEmails;
	private List<String> ccEmails;
	private List<String> bccEmails;

	private String fromEmail;

	/**
	 * Used to send messages on behalf of the from email
	 */
	private String senderEmail;

	private String subject;
	private String text;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public T processAction(T bean, WorkflowTransition transition) {
		Email email = new Email(getFromEmail(),
				getSenderEmail(),
				CollectionUtils.toArrayOrNull(getToEmails(), String.class),
				CollectionUtils.toArrayOrNull(getCcEmails(), String.class),
				CollectionUtils.toArrayOrNull(getBccEmails(), String.class),
				getStringFromTemplate(getSubject(), bean),
				getStringFromTemplate(getText(), bean),
				MimeContentTypes.TEXT_HTML);
		getEmailHandler().send(email);
		return bean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertTrue(CoreStringUtils.isEmailValid(getFromEmail()), "Email Address " + getFromEmail() + " is not valid.");
		if (!StringUtils.isEmpty(getSenderEmail())) {
			ValidationUtils.assertTrue(CoreStringUtils.isEmailValid(getSenderEmail()), "Email Address " + getSenderEmail() + " is not valid.");
		}
		validateEmailAddressList(getToEmails());
		validateEmailAddressList(getCcEmails());
		validateEmailAddressList(getBccEmails());
		ValidationUtils.assertNotEmpty(getSubject(), "Subject cannot be empty");
		ValidationUtils.assertNotEmpty(getText(), "Message body cannot be empty");
	}


	private void validateEmailAddressList(List<String> emailAddressList) {
		if (!CollectionUtils.isEmpty(emailAddressList)) {
			emailAddressList.forEach(emailAddress -> ValidationUtils.assertTrue(CoreStringUtils.isEmailValid(emailAddress), "Email Address " + emailAddress + " is not valid."));
		}
	}


	private String getStringFromTemplate(String template, T bean) {
		TemplateConfig templateConfig = TemplateConfig.ofTemplateWithUtilsClasses(template);

		templateConfig.addBeanToContext("bean", bean);

		//add linked entity to template context
		if (bean instanceof WorkflowTask) {
			WorkflowTask workflowTask = (WorkflowTask) bean;
			if (workflowTask.getLinkedEntityFkFieldId() != null) {
				ReadOnlyDAO<?> fkDao = getDaoLocator().locate(workflowTask.getDefinition().getLinkedEntityTable().getName());
				Object fkEntity = fkDao.findByPrimaryKey(workflowTask.getLinkedEntityFkFieldId());
				templateConfig.addBeanToContext("linkedBean", fkEntity);
			}
		}

		return getTemplateConverter().convert(templateConfig);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public EmailHandler getEmailHandler() {
		return this.emailHandler;
	}


	public void setEmailHandler(EmailHandler emailHandler) {
		this.emailHandler = emailHandler;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public List<String> getToEmails() {
		return this.toEmails;
	}


	public void setToEmails(List<String> toEmails) {
		this.toEmails = toEmails;
	}


	public List<String> getCcEmails() {
		return this.ccEmails;
	}


	public void setCcEmails(List<String> ccEmails) {
		this.ccEmails = ccEmails;
	}


	public List<String> getBccEmails() {
		return this.bccEmails;
	}


	public void setBccEmails(List<String> bccEmails) {
		this.bccEmails = bccEmails;
	}


	public String getFromEmail() {
		return this.fromEmail;
	}


	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}


	public String getSenderEmail() {
		return this.senderEmail;
	}


	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}
}
