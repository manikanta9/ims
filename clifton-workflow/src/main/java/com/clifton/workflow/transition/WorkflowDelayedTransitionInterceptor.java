package com.clifton.workflow.transition;

import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.web.view.JsonView;
import com.clifton.workflow.WorkflowAware;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

import java.util.Map;
import java.util.stream.Collectors;


/**
 * The {@link WorkflowDelayedTransitionInterceptor} is a {@link WebRequestInterceptor} which intercepts servlet requests in order to execute any existing {@link
 * WorkflowTransition#automaticDelayed delayed automatic workflow transitions} before serialization and to attach them to the response {@link JsonSession#entityConversionMap entity
 * conversion map}. This ensures that delayed transitions are executed before and included in the serialized response.
 *
 * @author mikeh
 */
public class WorkflowDelayedTransitionInterceptor implements WebRequestInterceptor {

	@Override
	public void preHandle(WebRequest request) {
		// Do nothing
	}


	@Override
	public void postHandle(WebRequest request, ModelMap model) {
		// After request is handled but before response is serialized
		request.setAttribute(JsonView.ENTITY_CONVERSION_MAP_ATTRIBUTE_NAME, WorkflowTransitionUtils.executeAndGetDelayedTransitionEntityConversionMap(), WebRequest.SCOPE_REQUEST);
	}


	@Override
	public void afterCompletion(WebRequest request, Exception ex) {
		// Clear any remaining transitions
		Map<WorkflowAware, WorkflowTransition> remainingTransitionMap = WorkflowTransitionUtils.getDelayedTransitionMap();
		if (!remainingTransitionMap.isEmpty()) {
			String errorMessage = remainingTransitionMap.entrySet().stream()
					.map(entry -> String.format("Entity [%s] and transition [%s].", entry.getKey(), entry.getValue()))
					.collect(Collectors.joining("\n\t- ", "Delayed automatic workflow transitions have been unexpectedly orphaned.\n\t- ", ""));
			LogUtils.error(WorkflowDelayedTransitionInterceptor.class, errorMessage);
		}
		WorkflowTransitionUtils.clearDelayedTransitions();
	}
}
