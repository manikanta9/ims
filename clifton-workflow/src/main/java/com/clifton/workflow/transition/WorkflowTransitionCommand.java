package com.clifton.workflow.transition;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;

import java.io.Serializable;


/**
 * <code>WorkflowTransitionCommand</code> is a command object used for listing and/or executing {@link WorkflowTransition}(s)
 * of {@link com.clifton.workflow.WorkflowAware} entities of the same type/table.
 *
 * @author vgomelsky
 */
public class WorkflowTransitionCommand implements Serializable, StatusHolderObject<Status> {

	/**
	 * Table name of the entity ID or IDs to transition
	 */
	private String tableName;
	/**
	 * A single entity ID or array of IDs to either look up transitions or execute transitions for.
	 * <p>
	 * The ID or IDs must be valid identities of the specified table name.
	 */
	private Long id;
	private Long[] ids;

	// Transition filter properties

	/**
	 * When listing applicable {@link WorkflowTransition}s for a {@link com.clifton.workflow.WorkflowAware} {@link #tableName} and {@link #id} match, a no-op transition is included by default.
	 * Specifying true for this value will exclude the no-op transition.
	 */
	private boolean excludeNoTransition;
	/**
	 * {@link WorkflowTransition} filter to exclude system defined transitions when listing applicable transitions for an entity based on {@link #tableName} and {@link #id}. Defaults to false.
	 */
	private boolean excludeSystemDefined;
	/**
	 * {@link WorkflowTransition} filter to exclude hidden transitions when listing applicable transitions for an entity based on {@link #tableName} and {@link #id}. Defaults to false.
	 */
	private boolean excludeHidden;
	/**
	 * {@link WorkflowTransition} filter to evaluate conditions when listing applicable transitions for an entity based on {@link #tableName} and {@link #id}.
	 * Transitions with no condition and those with conditions evaluating to true will be included. Defaults to false.
	 */
	private boolean evaluateCondition;

	// Transition execution properties

	/**
	 * WorkflowTransition to use for transitioning each of {@link #id} and/or {@link #ids} of {@link #tableName} to.
	 * <p>
	 * If both {@link #workflowTransitionId} and {@link #newWorkflowStateId} are defined, {@link #newWorkflowStateId} will take precedence.
	 */
	private Integer workflowTransitionId;
	/**
	 * New WorkflowState to transition each of {@link #id} and/or {@link #ids} of {@link #tableName} to.
	 * <p>
	 * If both {@link #workflowTransitionId} and {@link #newWorkflowStateId} are defined, {@link #newWorkflowStateId} will take precedence.
	 */
	private Short newWorkflowStateId;

	/**
	 * Allows execution of system defined transitions. Defaults to false.
	 */
	private boolean allowExecuteSystemDefined;
	/**
	 * If true, the execution of the service this command is provided to will run asynchronous. Defaults to false; synchronous processing.
	 */
	private boolean asynchronous;
	/**
	 * If true, the transitions of entities included in the command will execute in the same transaction so if one fails they all fail.
	 * Defaults to false, so each transition is attempted independent of the others.
	 */
	private boolean executeInSingleTransaction;
	/**
	 * If true, the execution of multiple transitions will stop after receiving the first failed transition. Defaults to false, so each transition is attempted.
	 */
	private boolean stopOnFirstError;

	/**
	 * Optional status object to track processing of the command. If the status is not defined, a new one will be used.
	 */
	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static WorkflowTransitionCommand of(String tableName, Long id) {
		WorkflowTransitionCommand result = new WorkflowTransitionCommand();
		result.setTableName(tableName);
		result.setId(id);
		return result;
	}


	public static WorkflowTransitionCommand of(String tableName, Long id, Integer workflowTransitionId) {
		WorkflowTransitionCommand result = of(tableName, id);
		result.setWorkflowTransitionId(workflowTransitionId);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status getStatus() {
		return this.status;
	}


	@Override
	public String toString() {
		return getRunId();
	}


	public String getRunId() {
		StringBuilder runId = new StringBuilder("Table_").append(getTableName());
		if (getWorkflowTransitionId() != null) {
			runId.append("-TransitionId_").append(getWorkflowTransitionId());
		}
		if (getId() != null || getIds() != null) {
			runId.append("-Ids_").append(StringUtils.join(ArrayUtils.add(getIds(), getId()), Object::toString, ","));
		}
		return runId.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long[] getIds() {
		return this.ids;
	}


	public void setIds(Long[] ids) {
		this.ids = ids;
	}


	public boolean isExcludeNoTransition() {
		return this.excludeNoTransition;
	}


	public void setExcludeNoTransition(boolean excludeNoTransition) {
		this.excludeNoTransition = excludeNoTransition;
	}


	public boolean isExcludeSystemDefined() {
		return this.excludeSystemDefined;
	}


	public void setExcludeSystemDefined(boolean excludeSystemDefined) {
		this.excludeSystemDefined = excludeSystemDefined;
	}


	public boolean isExcludeHidden() {
		return this.excludeHidden;
	}


	public void setExcludeHidden(boolean excludeHidden) {
		this.excludeHidden = excludeHidden;
	}


	public boolean isEvaluateCondition() {
		return this.evaluateCondition;
	}


	public void setEvaluateCondition(boolean evaluateCondition) {
		this.evaluateCondition = evaluateCondition;
	}


	public Integer getWorkflowTransitionId() {
		return this.workflowTransitionId;
	}


	public void setWorkflowTransitionId(Integer workflowTransitionId) {
		this.workflowTransitionId = workflowTransitionId;
	}


	public Short getNewWorkflowStateId() {
		return this.newWorkflowStateId;
	}


	public void setNewWorkflowStateId(Short newWorkflowStateId) {
		this.newWorkflowStateId = newWorkflowStateId;
	}


	public boolean isAllowExecuteSystemDefined() {
		return this.allowExecuteSystemDefined;
	}


	public void setAllowExecuteSystemDefined(boolean allowExecuteSystemDefined) {
		this.allowExecuteSystemDefined = allowExecuteSystemDefined;
	}


	public boolean isAsynchronous() {
		return this.asynchronous;
	}


	public void setAsynchronous(boolean asynchronous) {
		this.asynchronous = asynchronous;
	}


	public boolean isExecuteInSingleTransaction() {
		return this.executeInSingleTransaction;
	}


	public void setExecuteInSingleTransaction(boolean executeInSingleTransaction) {
		this.executeInSingleTransaction = executeInSingleTransaction;
	}


	public boolean isStopOnFirstError() {
		return this.stopOnFirstError;
	}


	public void setStopOnFirstError(boolean stopOnFirstError) {
		this.stopOnFirstError = stopOnFirstError;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
