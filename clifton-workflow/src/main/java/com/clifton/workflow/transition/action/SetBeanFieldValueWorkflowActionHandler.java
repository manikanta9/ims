package com.clifton.workflow.transition.action;


import com.clifton.core.beans.BeanUtils;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.transition.WorkflowTransition;


/**
 * The <code>SetBeanPropertyValueWorkflowActionHandler</code> sets specified bean property to specified value
 * <p>
 * Example: On Reconciliation workflow - clicking Confirm transition will automatically set the Confirmed flag to true,
 * and setting it to false on unconfirms.
 *
 * @author Mary Anderson
 */
public class SetBeanFieldValueWorkflowActionHandler<T extends WorkflowAware> implements WorkflowTransitionActionHandler<T> {

	private String beanFieldName;
	private String value;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public T processAction(T bean, WorkflowTransition transition) {
		BeanUtils.setPropertyValue(bean, getBeanFieldName(), getValue());
		// We are purposely NOT saving the bean here because the bean will be saved by the WorkflowManagerObserver after all actions are processed.
		// This prevents the bean from being saved multiple times when this handler is invoked for more than one field.
		return bean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBeanFieldName() {
		return this.beanFieldName;
	}


	public void setBeanFieldName(String beanFieldName) {
		this.beanFieldName = beanFieldName;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}
}
