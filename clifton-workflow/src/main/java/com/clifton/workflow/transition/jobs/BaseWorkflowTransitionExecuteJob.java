package com.clifton.workflow.transition.jobs;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.search.WorkflowAwareSearchForm;
import com.clifton.workflow.transition.WorkflowTransitionCommand;
import com.clifton.workflow.transition.WorkflowTransitionService;

import java.util.List;
import java.util.Map;


/**
 * The <code>BaseWorkflowTransitionExecuteJob</code> class can be extended and used to create a batch job that attempts to transition entities through a transition based on selected criteria.
 * <p>
 * For example - Archiving Billing Definitions - there is a condition on the transition that defines when this can happen, but it would only get triggered by editing the definition so
 * instead we'll have a job that will periodically check for billing definitions eligible for archiving and attempt to move them
 *
 * @author manderson
 */
public abstract class BaseWorkflowTransitionExecuteJob<T extends WorkflowAware, S extends WorkflowAwareSearchForm> implements Task, ValidationAware, StatusHolderObjectAware<Status> {


	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////


	private StatusHolderObject<Status> statusHolderObject;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * Workflow for this transition - used to find end state id to transition to
	 */
	private Short workflowId;

	/**
	 * Will include in search for entities that are currently IN this state
	 */
	private String startWorkflowStateName;

	/**
	 * Used to transition the entity to this workflow state
	 */
	private String endWorkflowStateName;

	/**
	 * Determines if entity can't move if it will either be recorded as a SKIP or ERROR
	 */
	private boolean errorIfTransitionCannotSucceed;

	/**
	 * Allows execution of system defined transitions. Defaults to false.
	 */
	private boolean allowExecuteSystemDefined;


	/**
	 * If true, the transitions of entities included in the command will execute in the same transaction so if one fails they all fail.
	 * Defaults to false, so each transition is attempted independent of the others.
	 */
	private boolean executeInSingleTransaction;

	/**
	 * If true, the execution of multiple transitions will stop after receiving the first failed transition. Defaults to false, so each transition is attempted.
	 */
	private boolean stopOnFirstError;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getWorkflowId(), "Workflow is required.");
		ValidationUtils.assertNotNull(getStartWorkflowStateName(), "Start Workflow State Is Required.");
		ValidationUtils.assertNotNull(getEndWorkflowStateName(), "End Workflow State Is Required.");
		ValidationUtils.assertNotNull(getWorkflowDefinitionService().getWorkflowStateByName(getWorkflowId(), getEndWorkflowStateName()), "Cannot find End Workflow State with name " + getEndWorkflowStateName() + ".");
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolderObject.getStatus();

		WorkflowState newWorkflowState = getWorkflowDefinitionService().getWorkflowStateByName(getWorkflowId(), getEndWorkflowStateName());
		ValidationUtils.assertNotNull(newWorkflowState, "Cannot find workflow state to transition to.");

		S searchForm = configureWorkflowAwareSearchForm();
		searchForm.setWorkflowStateNameEquals(getStartWorkflowStateName());

		List<T> workflowAwareEntityList = getWorkflowAwareEntityList(searchForm);
		if (!CollectionUtils.isEmpty(workflowAwareEntityList)) {
			WorkflowTransitionCommand transitionCommand = new WorkflowTransitionCommand();
			transitionCommand.setTableName(getWorkflowAwareEntityTableName());
			transitionCommand.setIds(workflowAwareEntityList.stream().map(BeanUtils::getIdentityAsLong).toArray(Long[]::new));
			transitionCommand.setNewWorkflowStateId(newWorkflowState.getId());
			transitionCommand.setAllowExecuteSystemDefined(isAllowExecuteSystemDefined());
			transitionCommand.setExecuteInSingleTransaction(isExecuteInSingleTransaction());
			transitionCommand.setStopOnFirstError(isStopOnFirstError());
			transitionCommand.setStatus(status);

			status = getWorkflowTransitionService().executeWorkflowTransitionForCommand(transitionCommand);
		}
		else {
			status.setMessage("No entities matching criteria available to transition");
		}
		return status;
	}


	protected abstract String getWorkflowAwareEntityTableName();


	/**
	 * Configure the search form - if needed for more than just the current workflow state name which is automatically added
	 */
	protected abstract S configureWorkflowAwareSearchForm();


	/**
	 * Get the list of entities
	 */
	protected abstract List<T> getWorkflowAwareEntityList(S searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public Short getWorkflowId() {
		return this.workflowId;
	}


	public void setWorkflowId(Short workflowId) {
		this.workflowId = workflowId;
	}


	public String getStartWorkflowStateName() {
		return this.startWorkflowStateName;
	}


	public void setStartWorkflowStateName(String startWorkflowStateName) {
		this.startWorkflowStateName = startWorkflowStateName;
	}


	public String getEndWorkflowStateName() {
		return this.endWorkflowStateName;
	}


	public void setEndWorkflowStateName(String endWorkflowStateName) {
		this.endWorkflowStateName = endWorkflowStateName;
	}


	public boolean isAllowExecuteSystemDefined() {
		return this.allowExecuteSystemDefined;
	}


	public void setAllowExecuteSystemDefined(boolean allowExecuteSystemDefined) {
		this.allowExecuteSystemDefined = allowExecuteSystemDefined;
	}


	public boolean isExecuteInSingleTransaction() {
		return this.executeInSingleTransaction;
	}


	public void setExecuteInSingleTransaction(boolean executeInSingleTransaction) {
		this.executeInSingleTransaction = executeInSingleTransaction;
	}


	public boolean isStopOnFirstError() {
		return this.stopOnFirstError;
	}


	public void setStopOnFirstError(boolean stopOnFirstError) {
		this.stopOnFirstError = stopOnFirstError;
	}


	public boolean isErrorIfTransitionCannotSucceed() {
		return this.errorIfTransitionCannotSucceed;
	}


	public void setErrorIfTransitionCannotSucceed(boolean errorIfTransitionCannotSucceed) {
		this.errorIfTransitionCannotSucceed = errorIfTransitionCannotSucceed;
	}
}
