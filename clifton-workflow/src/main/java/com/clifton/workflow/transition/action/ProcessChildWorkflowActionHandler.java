package com.clifton.workflow.transition.action;


import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;

import java.util.List;


/**
 * The <code>ProcessChildWorkflowAction</code> looks up all children of specified bean and processes
 * each one to see if any automatic workflow transitions are available.
 *
 * @author Mary Anderson
 */
public class ProcessChildWorkflowActionHandler<T extends WorkflowAware> implements WorkflowTransitionActionHandler<T> {

	private WorkflowTransitionService workflowTransitionService;
	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public T processAction(T bean, WorkflowTransition transition) {
		UpdatableDAO<T> dao = (UpdatableDAO<T>) getDaoLocator().locate((Class<T>) bean.getClass());
		List<T> childList = dao.findByField("parent.id", bean.getIdentity());
		for (T child : CollectionUtils.getIterable(childList)) {
			if (getWorkflowTransitionService().getWorkflowTransitionNext(child, new StringBuilder()) != null) {
				// If a next auto transition is available, save the bean so that the transition is applied.
				dao.save(child);
			}
		}
		return bean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
