package com.clifton.workflow.transition;


import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowState;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * The <code>WorkflowTransitionUtils</code> class contains utility methods for workflow transitions.
 *
 * @author vgomelsky
 */
public class WorkflowTransitionUtils {

	private static final ThreadLocal<Boolean> allowSystemDefined = new ThreadLocal<>();
	private static final ThreadLocal<Boolean> disableAutomaticWorkflowTransitions = new ThreadLocal<>();

	/**
	 * This is used to note a transition is occurring when the transition start and end state are the same. Without this set the transition will not get triggered
	 * The specified transition id must be populated
	 */
	private static final ThreadLocal<Integer> sameStateTransition = new ThreadLocal<>();

	/**
	 * The thread-local map of {@link WorkflowAware} entities to objects representing their current pending delayed {@link WorkflowTransition}. The order of entities for which
	 * delayed transitions are necessary is maintained.
	 *
	 * @implNote This map is sensitive to mutations. Mutable access should be restricted to this class.
	 */
	private static final ThreadLocal<LinkedHashMap<WorkflowAware, WorkflowDelayedTransition<?>>> delayedTransitionMap = ThreadLocal.withInitial(LinkedHashMap::new);

	////////////////////////////////////////////////////////////////////////////////
	//////////             System Defined Workflow Transitions            //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if execution of same state transition is occurring
	 * Without this same state transitions will NOT be triggered
	 */
	public static boolean isSystemDefinedAllowed() {
		return Boolean.TRUE.equals(allowSystemDefined.get());
	}


	private static void allowSystemDefined() {
		allowSystemDefined.set(Boolean.TRUE);
	}


	private static void disallowSystemDefined() {
		allowSystemDefined.remove();
	}


	/**
	 * Executes the specified callback in the mode that allows system defined transitions. Guarantees to allow execution of system
	 * defined transitions before callback execution and disallowing after.
	 */
	public static void executeSystemDefined(WorkflowTransitionExecutor callback) {
		try {
			allowSystemDefined();
			callback.execute();
		}
		finally {
			disallowSystemDefined();
		}
	}


	/**
	 * Executes the specified callback in the mode that allows system defined transitions and a same state transition. Guarantees to allow execution of system
	 * defined transitions before callback execution and disallowing after.
	 */
	public static void executeSystemDefinedWithSameStateTransition(int transitionId, WorkflowTransitionExecutor callback) {
		try {
			allowSystemDefined();
			allowSameStateTransition(transitionId);
			callback.execute();
		}
		finally {
			disallowSystemDefined();
			clearSameStateTransition();
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                Same State Workflow Transitions             //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if execution of system defined transitions is allowed.
	 * By default system defined transitions are not allowed.
	 */
	public static Integer getSameStateTransition() {
		return sameStateTransition.get();
	}


	private static void allowSameStateTransition(int transitionId) {
		sameStateTransition.set(transitionId);
	}


	private static void clearSameStateTransition() {
		sameStateTransition.remove();
	}


	/**
	 * Executes the specified callback in the mode that allows a specific same state transition. Guarantees to allow execution of same
	 * state transitions before callback execution and clearing after.
	 */
	public static void executeSameStateTransition(int transitionId, WorkflowTransitionExecutor callback) {
		try {
			allowSameStateTransition(transitionId);
			callback.execute();
		}
		finally {
			clearSameStateTransition();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The <code>WorkflowTransitionExecutor</code> is a callback interface to be used with system defined or same state transitions enabled.
	 */
	public interface WorkflowTransitionExecutor {

		public void execute();
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                Disable Workflow Transitions                //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if automatic transitions are disabled
	 */
	public static boolean isAutomaticWorkflowTransitionsDisabled() {
		return Boolean.TRUE.equals(disableAutomaticWorkflowTransitions.get());
	}


	/**
	 * Executes the specified callback with subsequent automatic workflow transitions disabled. Still allows the workflow assignment to be performed. Can be used for cases of
	 * automatic transitions that depend on data added after the bean is inserted to stop that transition. Then the dependent data can be added, and then you need to call the
	 * transitions manually afterwards. Examples: Trades that have Trade Allocations, and Rule Violations depend on the allocations. Need to Insert the Trade, then Insert the Trade
	 * Allocations, the Validate the Trade
	 */
	public static <T> T executeWithAutomaticWorkflowTransitionsDisabledAndReturn(Supplier<T> task) {
		try {
			disableAutomaticWorkflowTransitions.set(Boolean.TRUE);
			return task.get();
		}
		finally {
			disableAutomaticWorkflowTransitions.remove();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Delayed Workflow Transitions                    ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the existing map of {@link WorkflowAware} entities to {@link WorkflowTransition} entities for all pending delayed transitions.
	 */
	public static Map<WorkflowAware, WorkflowTransition> getDelayedTransitionMap() {
		return delayedTransitionMap.get().entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().transition));
	}


	/**
	 * Executes any pending delayed {@link WorkflowTransition} for the given <code>entity</code>. Any subsequent transitions queued for the given entity while processing an
	 * existing delayed transition will also be automatically executed before this method returns.
	 * <p>
	 * This method will return the resulting transitioned entity after the final transition in the chain has been executed.
	 */
	public static <T extends WorkflowAware> T executeDelayedTransitions(T entity) {
		return executeDelayedTransitions(entity, new HashMap<>());
	}


	/**
	 * Executes all pending delayed {@link WorkflowTransition transitions} and returns a map {@link WorkflowAware} entities to their target {@link WorkflowAware} transitioned
	 * results.
	 */
	public static Map<WorkflowAware, WorkflowAware> executeAndGetDelayedTransitionEntityConversionMap() {
		Map<WorkflowAware, WorkflowAware> entityConversionMap = new HashMap<>();
		Map<WorkflowAware, WorkflowDelayedTransition<?>> transitionMap = delayedTransitionMap.get();
		Map<WorkflowAware, Collection<WorkflowState>> entityToVisitedStateListMap = new HashMap<>();
		// Since down-stack mutation is expected, manual iteration (without standard iterators) must be performed
		while (!transitionMap.isEmpty()) {
			Map.Entry<WorkflowAware, WorkflowDelayedTransition<?>> firstElement = CollectionUtils.getFirstElementStrict(transitionMap.entrySet());
			WorkflowAware entity = firstElement.getKey();
			entityConversionMap.put(entity, executeDelayedTransitions(entity, entityToVisitedStateListMap));
		}
		return entityConversionMap;
	}


	/**
	 * Queues a new delayed {@link WorkflowTransition} for the given entity using the provided {@link UpdatableDAO DAO}. This delayed transition will be executed after the current
	 * request has finished processing.
	 * <p>
	 * {@link WorkflowTransition} objects <i>must not</i> be queued when a request context is not active for the current thread.
	 */
	public static <T extends WorkflowAware> void queueDelayedTransition(T entity, WorkflowTransition transition, UpdatableDAO<T> dao) {
		if (RequestContextHolder.getRequestAttributes() == null) {
			throw new RuntimeException("Automatic workflow transitions cannot be delayed outside of the request context.");
		}
		// LinkedHashMap order is not affected by re-insertions; if a transition already exists for the given entity, it will be replaced in-place with this one
		delayedTransitionMap.get().put(entity, new WorkflowDelayedTransition<>(entity, transition, dao));
	}


	/**
	 * Removes any pending delayed {@link WorkflowTransition transitions}.
	 */
	public static void clearDelayedTransitions() {
		delayedTransitionMap.remove();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static <T extends WorkflowAware> T executeDelayedTransitions(T entity, Map<WorkflowAware, Collection<WorkflowState>> visitedEntityStateMap) {
		// Track previously-executed transitions to prevent infinite loops
		Collection<WorkflowState> visitedStateList = visitedEntityStateMap.computeIfAbsent(entity, key -> new LinkedHashSet<>());
		// Execute delayed transitions until transitions for the entity are exhausted
		T result = entity;
		while (delayedTransitionMap.get().containsKey(result)) {
			// Prevent infinite loops
			WorkflowState delayedTransitionStartState = delayedTransitionMap.get().get(result).transition.getStartWorkflowState();
			AssertUtils.assertTrue(visitedStateList.add(delayedTransitionStartState), "Repeated automatic transitions found for entity [%s]. Visited transitions: %s.", result, CollectionUtils.toString(visitedStateList, 10));

			@SuppressWarnings("unchecked")
			WorkflowDelayedTransition<T> delayedTransition = (WorkflowDelayedTransition<T>) delayedTransitionMap.get().remove(result);
			if (delayedTransition.isPending(result)) {
				// Store current state for logging
				T currentEntity = result;
				String currentStartStateName = result.getWorkflowState() != null ? result.getWorkflowState().getName() : "<No State>";
				// Temporarily replace delayed transition queue so that adjustments can be rolled back on exception
				LinkedHashMap<WorkflowAware, WorkflowDelayedTransition<?>> savedTransitionMap = delayedTransitionMap.get();
				delayedTransitionMap.set(new LinkedHashMap<>());
				try {
					// Execute transition
					result = delayedTransition.doTransition(currentEntity);
					savedTransitionMap.putAll(delayedTransitionMap.get());
				}
				catch (Exception e) {
					LogUtils.error(LogCommand.ofThrowableAndMessage(WorkflowTransitionUtils.class, e, () -> String.format("An error occurred while attempting to execute delayed automatic workflow transitions. Entity type [%s] and ID [%s]. Before state: [%s]. Transition: [%s].", currentEntity.getClass().getName(), currentEntity.getIdentity(), currentStartStateName, delayedTransition.transition.getName())));
				}
				finally {
					delayedTransitionMap.set(savedTransitionMap);
				}
			}
			else {
				// Transition does not match expected state; report error and continue
				String entityStateName = result.getWorkflowState() != null ? result.getWorkflowState().getName() : "<NO STATE>";
				String startStateName = delayedTransitionStartState != null ? delayedTransitionStartState.getName() : "<NO STATE>";
				String endStateName = delayedTransition.transition.getEndWorkflowState().getName();
				LogUtils.error(WorkflowTransitionUtils.class, String.format("Invalid pending transition found for entity [%s] from state [%s] to state [%s]. The current entity state ([%s]) does not match the expected state ([%s]).", result, startStateName, endStateName, entityStateName, startStateName));
			}
		}
		// Allow ThreadLocal garbage collection
		if (delayedTransitionMap.get().isEmpty()) {
			delayedTransitionMap.remove();
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The {@link WorkflowDelayedTransition} type represents a delayed {@link WorkflowTransition} attempt. This encapsulates the entity to be transitioned and all properties
	 * necessary to describe the transition. The transition itself may be executed by the {@link #doTransition(WorkflowAware)} method.
	 */
	private static class WorkflowDelayedTransition<T extends WorkflowAware> {

		private final T entity;
		private final WorkflowTransition transition;
		private final UpdatableDAO<T> dao;


		private WorkflowDelayedTransition(T entity, WorkflowTransition transition, UpdatableDAO<T> dao) {
			AssertUtils.assertEquals(entity.getWorkflowState(), transition.getStartWorkflowState(), "The delayed transition entity workflow state [%s] must match the transition start workflow state [%s].", entity.getWorkflowState(), transition.getStartWorkflowState());
			this.entity = entity;
			this.transition = transition;
			this.dao = dao;
		}


		private boolean isPending(T entity) {
			return Objects.equals(entity.getWorkflowState(), this.transition.getStartWorkflowState());
		}


		@Transactional
		private T doTransition(T entity) {
			AssertUtils.assertNotNull(entity, "The entity to be transitioned is null.");
			AssertUtils.assertEquals(entity, this.entity, "The entity to be transitioned ([%s]) must match the initial stored entity ([%s]) when executing delayed transitions.", entity, this.entity);
			entity.setWorkflowState(this.transition.getEndWorkflowState());
			entity.setWorkflowStatus(this.transition.getEndWorkflowState().getStatus());
			return this.dao.save(entity);
		}
	}
}
