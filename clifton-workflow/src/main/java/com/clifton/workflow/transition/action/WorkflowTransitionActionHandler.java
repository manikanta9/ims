package com.clifton.workflow.transition.action;


import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.transition.WorkflowTransition;


/**
 * The <code>WorkflowTransitionActionHandler</code> interface must be implemented by workflow actions
 * that can be performed when performing a particular workflow transition.
 * <p/>
 * An action can represent any code that is executed before or after a workflow transition.
 *
 * @author Mary Anderson
 */
public interface WorkflowTransitionActionHandler<T extends WorkflowAware> {

	/**
	 * Processes the action for the specified entity and workflow transition. If updates are performed on the bean, there is no need to save. The calling code is responsible for
	 * managing updates of the bean for saving because there may be many actions performed, and updates should be batched together to facilitate better performance.
	 *
	 * @param bean       the entity being transitioned
	 * @param transition the transition currently in process
	 */
	public T processAction(T bean, WorkflowTransition transition);
}
