package com.clifton.workflow.transition.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class WorkflowTransitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,description,endWorkflowState.workflow.name,endWorkflowState.name,startWorkflowState.name", leftJoin = true) // start state is optional
	private String searchPattern;

	@SearchField(searchField = "name")
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField(searchField = "description")
	private String description;

	@SearchField(searchField = "endWorkflowState.workflow.name")
	private String workflowName;

	@SearchField(searchField = "endWorkflowState.workflow.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String workflowNameEquals;


	@SearchField(searchField = "startWorkflowState.id")
	private Short startWorkflowStateId;

	@SearchField(searchField = "startWorkflowState.name")
	private String startWorkflowStateName;

	@SearchField(searchField = "startWorkflowState.status.name")
	private String startWorkflowStatusName;

	@SearchField(searchField = "endWorkflowState.id")
	private Short endWorkflowStateId;

	@SearchField(searchField = "endWorkflowState.name")
	private String endWorkflowStateName;

	@SearchField(searchField = "endWorkflowState.status.name")
	private String endWorkflowStatusName;

	@SearchField
	private Boolean readAccessSufficientToExecute;

	@SearchField(searchField = "condition.id")
	private Integer conditionId;

	@SearchField
	private Boolean automatic;

	@SearchField
	private Boolean automaticDelayed;

	@SearchField
	private Boolean beforeActionsExist;

	@SearchField
	private Boolean afterActionsExist;

	@SearchField(searchField = "beforeActionsExist,afterActionsExist", searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Boolean actionsExist;

	@SearchField
	private Short priorityOrder;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Boolean notExecutedInTransaction;

	@SearchField
	private Integer order;

	@SearchField
	private String entryDetailScreenClass;

	@SearchField(searchField = "entryDetailScreenClass", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean entryDetailScreenClassPopulated;

	@SearchField
	private String entryDefaultData;

	@SearchField(searchField = "startWorkflowState.id,endWorkflowState.id", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Short startOrEndWorkflowStateId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getWorkflowName() {
		return this.workflowName;
	}


	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}


	public String getWorkflowNameEquals() {
		return this.workflowNameEquals;
	}


	public void setWorkflowNameEquals(String workflowNameEquals) {
		this.workflowNameEquals = workflowNameEquals;
	}


	public Short getStartWorkflowStateId() {
		return this.startWorkflowStateId;
	}


	public void setStartWorkflowStateId(Short startWorkflowStateId) {
		this.startWorkflowStateId = startWorkflowStateId;
	}


	public String getStartWorkflowStateName() {
		return this.startWorkflowStateName;
	}


	public void setStartWorkflowStateName(String startWorkflowStateName) {
		this.startWorkflowStateName = startWorkflowStateName;
	}


	public String getStartWorkflowStatusName() {
		return this.startWorkflowStatusName;
	}


	public void setStartWorkflowStatusName(String startWorkflowStatusName) {
		this.startWorkflowStatusName = startWorkflowStatusName;
	}


	public Short getEndWorkflowStateId() {
		return this.endWorkflowStateId;
	}


	public void setEndWorkflowStateId(Short endWorkflowStateId) {
		this.endWorkflowStateId = endWorkflowStateId;
	}


	public String getEndWorkflowStateName() {
		return this.endWorkflowStateName;
	}


	public void setEndWorkflowStateName(String endWorkflowStateName) {
		this.endWorkflowStateName = endWorkflowStateName;
	}


	public String getEndWorkflowStatusName() {
		return this.endWorkflowStatusName;
	}


	public void setEndWorkflowStatusName(String endWorkflowStatusName) {
		this.endWorkflowStatusName = endWorkflowStatusName;
	}


	public Boolean getReadAccessSufficientToExecute() {
		return this.readAccessSufficientToExecute;
	}


	public void setReadAccessSufficientToExecute(Boolean readAccessSufficientToExecute) {
		this.readAccessSufficientToExecute = readAccessSufficientToExecute;
	}


	public Integer getConditionId() {
		return this.conditionId;
	}


	public void setConditionId(Integer conditionId) {
		this.conditionId = conditionId;
	}


	public Boolean getAutomatic() {
		return this.automatic;
	}


	public void setAutomatic(Boolean automatic) {
		this.automatic = automatic;
	}


	public Boolean getAutomaticDelayed() {
		return this.automaticDelayed;
	}


	public void setAutomaticDelayed(Boolean automaticDelayed) {
		this.automaticDelayed = automaticDelayed;
	}


	public Boolean getBeforeActionsExist() {
		return this.beforeActionsExist;
	}


	public void setBeforeActionsExist(Boolean beforeActionsExist) {
		this.beforeActionsExist = beforeActionsExist;
	}


	public Boolean getAfterActionsExist() {
		return this.afterActionsExist;
	}


	public void setAfterActionsExist(Boolean afterActionsExist) {
		this.afterActionsExist = afterActionsExist;
	}


	public Boolean getActionsExist() {
		return this.actionsExist;
	}


	public void setActionsExist(Boolean actionsExist) {
		this.actionsExist = actionsExist;
	}


	public Short getPriorityOrder() {
		return this.priorityOrder;
	}


	public void setPriorityOrder(Short priorityOrder) {
		this.priorityOrder = priorityOrder;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getNotExecutedInTransaction() {
		return this.notExecutedInTransaction;
	}


	public void setNotExecutedInTransaction(Boolean notExecutedInTransaction) {
		this.notExecutedInTransaction = notExecutedInTransaction;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public String getEntryDetailScreenClass() {
		return this.entryDetailScreenClass;
	}


	public void setEntryDetailScreenClass(String entryDetailScreenClass) {
		this.entryDetailScreenClass = entryDetailScreenClass;
	}


	public Boolean getEntryDetailScreenClassPopulated() {
		return this.entryDetailScreenClassPopulated;
	}


	public void setEntryDetailScreenClassPopulated(Boolean entryDetailScreenClassPopulated) {
		this.entryDetailScreenClassPopulated = entryDetailScreenClassPopulated;
	}


	public String getEntryDefaultData() {
		return this.entryDefaultData;
	}


	public void setEntryDefaultData(String entryDefaultData) {
		this.entryDefaultData = entryDefaultData;
	}


	public Short getStartOrEndWorkflowStateId() {
		return this.startOrEndWorkflowStateId;
	}


	public void setStartOrEndWorkflowStateId(Short startOrEndWorkflowStateId) {
		this.startOrEndWorkflowStateId = startOrEndWorkflowStateId;
	}
}
