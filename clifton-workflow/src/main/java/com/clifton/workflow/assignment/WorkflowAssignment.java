package com.clifton.workflow.assignment;


import com.clifton.core.beans.BaseEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.definition.Workflow;


/**
 * The <code>WorkflowAssignment</code> class links DTO's to corresponding workflows.
 *
 * @author vgomelsky
 */
public class WorkflowAssignment extends BaseEntity<Short> {

	private Workflow workflow;
	private SystemTable table;
	/**
	 * Optional system condition that can determine whether this assignment should be used.
	 */
	private SystemCondition condition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Workflow getWorkflow() {
		return this.workflow;
	}


	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public SystemCondition getCondition() {
		return this.condition;
	}


	public void setCondition(SystemCondition condition) {
		this.condition = condition;
	}
}
