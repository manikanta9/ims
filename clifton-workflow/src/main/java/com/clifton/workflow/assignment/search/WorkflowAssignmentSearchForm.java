package com.clifton.workflow.assignment.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class WorkflowAssignmentSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "table.name,workflow.name")
	private String searchPattern;

	@SearchField(searchField = "table.name")
	private String tableName;

	@SearchField(searchField = "workflow.name")
	private String workflowName;

	@SearchField(searchField = "condition.id")
	private Integer conditionId;

	@SearchField(searchField = "workflow.active")
	private Boolean active;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getWorkflowName() {
		return this.workflowName;
	}


	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}


	public Integer getConditionId() {
		return this.conditionId;
	}


	public void setConditionId(Integer conditionId) {
		this.conditionId = conditionId;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}
}
