package com.clifton.workflow.assignment.validation;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.assignment.WorkflowAssignment;
import org.springframework.stereotype.Component;


/**
 * The <code>WorkflowAssignmentValidator</code> class handles all validation prior to editing a {@link WorkflowAssignment}
 *
 * @author manderson
 */
@Component
public class WorkflowAssignmentValidator extends SelfRegisteringDaoValidator<WorkflowAssignment> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		// Run validation against inserts and updates only
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(WorkflowAssignment bean, DaoEventTypes config) throws ValidationException {
		WorkflowAssignment originalBean = null;
		if (config.isUpdate()) {
			originalBean = getOriginalBean(bean);
		}
		// Only validate new bean or if the original table selection has changed
		if (originalBean == null || !originalBean.getTable().equals(bean.getTable())) {
			// Validate the Table's DTO for a WorkflowAssignment
			ReadOnlyDAO<?> dao = getDaoLocator().locate(bean.getTable().getName());
			Class<?> clazz = dao.getConfiguration().getBeanClass();
			if (!WorkflowAware.class.isAssignableFrom(clazz)) {
				throw new FieldValidationException("The class associated with table [" + bean.getTable().getName() + "] does not have support for workflows.", "table.name");
			}
		}
	}
}
