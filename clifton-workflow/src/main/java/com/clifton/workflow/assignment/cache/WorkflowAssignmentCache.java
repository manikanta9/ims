package com.clifton.workflow.assignment.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.workflow.assignment.WorkflowAssignment;
import org.springframework.stereotype.Component;


/**
 * The <code>SystemNoteTypeCache</code> stores a list of note types for a table
 *
 * @author manderson
 */
@Component
public class WorkflowAssignmentCache extends SelfRegisteringSingleKeyDaoListCache<WorkflowAssignment, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "table.id";
	}


	@Override
	protected Short getBeanKeyValue(WorkflowAssignment bean) {
		if (bean.getTable() != null) {
			return bean.getTable().getId();
		}
		return null;
	}
}
