package com.clifton.workflow.assignment.search;

import com.clifton.core.dataaccess.search.SearchFormRestrictionConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.BooleanUtils;
import com.clifton.system.schema.search.SystemTableSearchForm;
import com.clifton.workflow.assignment.WorkflowAssignment;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;


/**
 * The WorkflowAssignmentSystemTableSearchFormRestrictionConfigurer class defines cross-project search restriction for {@link com.clifton.system.schema.SystemTable}
 * objects that are referenced by workflow assignments.
 *
 * @author vgomelsky
 */
@Component
public class WorkflowAssignmentSystemTableSearchFormRestrictionConfigurer implements SearchFormRestrictionConfigurer {


	@Override
	public Class<?> getSearchFormClass() {
		return SystemTableSearchForm.class;
	}


	@Override
	public String getSearchFieldName() {
		return "limitToWorkflowAssignmentTables";
	}


	@Override
	public void configureCriteria(Criteria criteria, SearchRestriction restriction) {
		if (BooleanUtils.isTrue(restriction.getValue())) {
			DetachedCriteria subquery = DetachedCriteria.forClass(WorkflowAssignment.class, "wa");
			subquery.setProjection(Projections.property("id"));
			subquery.add(Restrictions.eqProperty(subquery.getAlias() + ".table.id", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.exists(subquery));
		}
	}
}
