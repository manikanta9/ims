package com.clifton.workflow.assignment;


/**
 * The <code>WorkflowAssignmentUtils</code> class contains utility methods for workflow assignments and allowing
 * a reassignment.
 *
 * @author manderson
 */
public class WorkflowAssignmentUtils {

	private static final ThreadLocal<Boolean> allowReassignment = new ThreadLocal<>();


	/**
	 * Returns true if an entity can change workflow if there is already a workflow
	 * associated with it.  Set only by the reassignWorkflow method in WorkflowAssignmentService
	 * <p>
	 * If not allowed - Transition to the other workflow would fail because it's not a valid transition
	 */
	public static boolean isReassignmentAllowed() {
		return Boolean.TRUE.equals(allowReassignment.get());
	}


	private static void allowReassignment() {
		allowReassignment.set(Boolean.TRUE);
	}


	private static void disallowReassignment() {
		allowReassignment.remove();
	}


	/**
	 * Executes the specified callback in the mode that allows reassignments. Guarantees to allow execution of reassignment
	 * before callback execution and disallowing after.
	 */
	public static void executeReassignment(WorkflowAssignmentExecutor callback) {
		try {
			allowReassignment();
			callback.execute();
		}
		finally {
			disallowReassignment();
		}
	}


	/**
	 * The <code>WorkflowAssignmentExecutor</code> is a callback interface to be used with reassignments enabled.
	 *
	 * @author manderson
	 */
	public interface WorkflowAssignmentExecutor {

		public void execute();
	}
}
