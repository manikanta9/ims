package com.clifton.workflow.assignment;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.assignment.search.WorkflowAssignmentSearchForm;
import com.clifton.workflow.definition.Workflow;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>WorkflowAssignmentService</code> interface defines methods for working with workflow assignments.
 *
 * @author vgomelsky
 */
public interface WorkflowAssignmentService {

	////////////////////////////////////////////////////////////////////////////
	/////////            Workflow Assignment Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowAssignment getWorkflowAssignment(short id);


	public List<WorkflowAssignment> getWorkflowAssignmentList(WorkflowAssignmentSearchForm searchForm);


	public List<WorkflowAssignment> getWorkflowAssignmentListByWorkflow(short workflowId);


	public List<WorkflowAssignment> getWorkflowAssignmentListByTableName(final String tableName, boolean includeInactive);


	public WorkflowAssignment saveWorkflowAssignment(WorkflowAssignment bean);


	public void deleteWorkflowAssignment(short id);


	/**
	 * Assigns an active {@link Workflow} that should be associated with the specified DTO instance.
	 * Sets the first workflow state and status on the specified DTO.
	 * Returns assigned workflow.
	 * <p>
	 * For conditional workflow assignment the dtoInstance can be used for property comparisons, etc.
	 */
	public Workflow assignWorkflow(WorkflowAware dtoInstance);


	/**
	 * For cases where the workflow isn't required - was null, or can easily be changed, users
	 * may want the ability to "reassign" workflow on an entity.
	 * Example: Trade Reconciliation - As new workflows are supported, existing reconciliation matches may not have a workflow
	 * and since it's not required they never will since they are only assigned on inserts.  Second example would be a workflow change on
	 * the match type - some existing reconciliation records users may want to change to use the new workflow (likely anything that hasn't been confirmed yet)
	 */
	@RequestMapping("workflowReassign")
	@SecureMethod(dynamicTableNameUrlParameter = "tableName")
	public void reassignWorkflow(String tableName, Integer fkFieldId);


	/**
	 * Auto assigns the start workflow state for a given table where the {@link WorkflowAware}'s {@link com.clifton.workflow.definition.WorkflowState} is not defined.
	 * <p>
	 * Used during migrations when doing batch inserts, or when assigning a workflow for a table that has existing data
	 */
	@DoNotAddRequestMapping
	public void assignWorkflowByTable(short tableId);
}
