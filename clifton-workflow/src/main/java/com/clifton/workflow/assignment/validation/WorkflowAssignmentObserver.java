package com.clifton.workflow.assignment.validation;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.assignment.WorkflowAssignment;
import com.clifton.workflow.assignment.WorkflowAssignmentService;
import com.clifton.workflow.definition.WorkflowState;
import org.springframework.stereotype.Component;


/**
 * The <code>WorkflowAssignmentObserver</code> class is a {@link DaoEventObserver} that
 * assigns the first {@link WorkflowState} to all entities in the table that do not have a state defined.
 * Used for new assignments & changing table assignments.
 * <p/>
 * the {@link WorkflowAssignmentObserver} observes changes to {@link WorkflowAssignment} {@link SystemTable}
 *
 * @author manderson
 */
@Component
public class WorkflowAssignmentObserver extends BaseDaoEventObserver<WorkflowAssignment> {

	private WorkflowAssignmentService workflowAssignmentService;


	@Override
	@SuppressWarnings("unused")
	public void beforeMethodCallImpl(ReadOnlyDAO<WorkflowAssignment> dao, DaoEventTypes event, WorkflowAssignment bean) {
		// NOTHING FOR NOW
	}


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<WorkflowAssignment> dao, DaoEventTypes event, WorkflowAssignment bean, Throwable e) {
		WorkflowAssignment original = null;
		if (DaoEventTypes.UPDATE == event) {
			original = getOriginalBean(dao, bean);
		}
		// Additional Items to Consider: RESTRICT CHANGING TABLES, HANDLE ACTIVATING/INACTIVATING WORKFLOWS
		if (original == null || !original.getTable().equals(bean.getTable())) {
			getWorkflowAssignmentService().assignWorkflowByTable(bean.getTable().getId());
		}
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public WorkflowAssignmentService getWorkflowAssignmentService() {
		return this.workflowAssignmentService;
	}


	public void setWorkflowAssignmentService(WorkflowAssignmentService workflowAssignmentService) {
		this.workflowAssignmentService = workflowAssignmentService;
	}
}
