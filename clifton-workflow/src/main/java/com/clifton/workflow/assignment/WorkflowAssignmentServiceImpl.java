package com.clifton.workflow.assignment;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.WorkflowConfig;
import com.clifton.workflow.assignment.search.WorkflowAssignmentSearchForm;
import com.clifton.workflow.assignment.validation.WorkflowAssignmentObserver;
import com.clifton.workflow.assignment.validation.WorkflowAssignmentValidator;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The <code>WorkflowAssignmentServiceImpl</code> class provides basic implementation of {@link WorkflowAssignmentService} methods.
 *
 * @author vgomelsky
 */
@Service
public class WorkflowAssignmentServiceImpl<T extends WorkflowAware> implements WorkflowAssignmentService {

	private AdvancedUpdatableDAO<WorkflowAssignment, Criteria> workflowAssignmentDAO;

	private DaoLocator daoLocator;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemSchemaService systemSchemaService;
	private WorkflowTransitionService workflowTransitionService;

	private DaoSingleKeyListCache<WorkflowAssignment, Short> workflowAssignmentCache;

	////////////////////////////////////////////////////////////////////////////
	////////        Workflow Assignment Business Methods              //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WorkflowAssignment getWorkflowAssignment(short id) {
		return getWorkflowAssignmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<WorkflowAssignment> getWorkflowAssignmentList(WorkflowAssignmentSearchForm searchForm) {
		return getWorkflowAssignmentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<WorkflowAssignment> getWorkflowAssignmentListByWorkflow(short workflowId) {
		return getWorkflowAssignmentDAO().findByField("workflow.id", workflowId);
	}


	@Override
	public List<WorkflowAssignment> getWorkflowAssignmentListByTableName(final String tableName, boolean includeInactive) {
		List<WorkflowAssignment> list = getWorkflowAssignmentCache().getBeanListForKeyValue(getWorkflowAssignmentDAO(), getSystemSchemaService().getSystemTableByName(tableName).getId());
		if (!includeInactive) {
			list = BeanUtils.filter(list, workflowAssignment -> workflowAssignment.getWorkflow().isActive());
		}
		return list;
	}


	/**
	 * @see WorkflowAssignmentValidator
	 * @see WorkflowAssignmentObserver
	 */
	@Override
	public WorkflowAssignment saveWorkflowAssignment(WorkflowAssignment bean) {
		return getWorkflowAssignmentDAO().save(bean);
	}


	/**
	 * @see WorkflowAssignmentValidator
	 * @see WorkflowAssignmentObserver
	 */
	@Override
	public void deleteWorkflowAssignment(short id) {
		getWorkflowAssignmentDAO().delete(id);
	}


	@Override
	public Workflow assignWorkflow(WorkflowAware dtoInstance) {
		return assignWorkflowImpl(dtoInstance, false);
	}


	@Override
	@Transactional
	public void reassignWorkflow(String tableName, Integer fkFieldId) {
		UpdatableDAO<WorkflowAware> dao = (UpdatableDAO<WorkflowAware>) getDaoLocator().<WorkflowAware>locate(tableName);
		WorkflowAware entity = dao.findByPrimaryKey(fkFieldId);
		Workflow original = entity.getWorkflowState() != null ? entity.getWorkflowState().getWorkflow() : null;
		Workflow workflow = assignWorkflowImpl(entity, true);

		// Reset Workflow on the Bean only if it's populated and different than what it was
		if (workflow != null && !workflow.equals(original)) {
			saveWorkflowReassignment(entity, dao);
		}
	}


	private void saveWorkflowReassignment(final WorkflowAware dtoInstance, final UpdatableDAO<WorkflowAware> dao) {
		// This is the ONLY time one can reassign a workflow on an existing bean
		WorkflowAssignmentUtils.executeReassignment(() -> dao.save(dtoInstance));
	}


	@SuppressWarnings("unchecked")
	private Workflow assignWorkflowImpl(WorkflowAware dtoInstance, boolean reset) {
		AssertUtils.assertNotNull(dtoInstance, "Cannot determine a workflow assigned to a null instance");

		WorkflowConfig config = AnnotationUtils.getAnnotation(dtoInstance.getClass(), WorkflowConfig.class);
		// If Workflow Isn't Required - Do not Attempt to Assign a Workflow to an existing bean - only assigned on new beans
		// Unless reset = true
		if (config != null && !config.required() && !reset) {
			if (!dtoInstance.isNewBean()) {
				return null;
			}
		}

		String tableName = getDaoLocator().locate((Class<T>) dtoInstance.getClass()).getConfiguration().getTableName();

		List<WorkflowAssignment> assignmentList = getWorkflowAssignmentListByTableName(tableName, false);
		if (CollectionUtils.isEmpty(assignmentList)) {
			// If workflow is option - return null
			if (config != null && !config.required()) {
				return null;
			}
			// Otherwise, throw an exception if because workflow is Required and there is nothing available
			throw new ValidationException("There are no active workflows assigned to '" + tableName + "' table.");
		}

		Workflow workflow = null;

		if (config != null && !StringUtils.isEmpty(config.workflowBeanPropertyName())) {
			Object workflowObj = BeanUtils.getPropertyValue(dtoInstance, config.workflowBeanPropertyName(), false);
			if (workflowObj != null) {
				if (workflowObj instanceof Workflow) {
					workflow = (Workflow) workflowObj;
				}
				else {
					throw new IllegalArgumentException("Workflow Bean Property Name defined under @WorkflowConfig annotation for class [" + dtoInstance.getClass().getName()
							+ "] should reference a Workflow object, but instead is returning an object of type [" + workflowObj.getClass().getName() + "]");
				}
			}
			// If we have a specific workflow to use - filter out assignment list by it
			if (workflow != null) {
				ValidationUtils.assertTrue(workflow.isActive(), "Workflow [" + workflow.getName() + "] cannot be assigned because it is not currently active.");
				assignmentList = BeanUtils.filter(assignmentList, WorkflowAssignment::getWorkflow, workflow);
				if (CollectionUtils.isEmpty(assignmentList)) {
					throw new ValidationException("Cannot use selected Workflow [" + workflow.getName() + "].  It does not have any assignments to '" + tableName + "' table.");
				}
			}
			// Otherwise set assignment list to null - should not assign (validates required later)
			else {
				assignmentList = null;
			}
		}

		// filter through assignments that match required condition or no condition at all
		WorkflowAssignment realAssignment = null;

		for (WorkflowAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
			if (assignment.getCondition() != null) {
				if (getSystemConditionEvaluationHandler().isConditionTrue(assignment.getCondition(), dtoInstance)) {
					if (realAssignment != null && realAssignment.getCondition() != null) {
						throw new ValidationException("Cannot select one workflow from assigned to " + tableName + " at least 2 conditions passed [" + realAssignment.getCondition().getName()
								+ ", and " + assignment.getCondition().getName() + "]");
					}
					realAssignment = assignment;
				}
			}
			else if (realAssignment == null) {
				realAssignment = assignment;
			}
			// If we already know the workflow and it passed at least one condition (or there isn't a condition)
			// don't keep checking other assignment conditions because they'd result in nothing or the same workflow anyway
			if (realAssignment != null && workflow != null) {
				break;
			}
		}

		if (realAssignment == null) {
			if (config == null || config.required()) {
				throw new ValidationException("There is no valid workflow assigned to '" + tableName + "' table.");
			}
			workflow = null;
		}
		else {
			workflow = realAssignment.getWorkflow();

			WorkflowTransition firstTransition = getWorkflowTransitionService().getWorkflowTransitionFirst(workflow);
			if (firstTransition == null) {
				throw new ValidationException("Cannot determine start state for workflow: '" + workflow.getName() + "'");
			}
			dtoInstance.setWorkflowState(firstTransition.getEndWorkflowState());
			dtoInstance.setWorkflowStatus(firstTransition.getEndWorkflowState().getStatus());
		}
		return workflow;
	}


	@Override
	public void assignWorkflowByTable(short tableId) {
		SystemTable table = getSystemSchemaService().getSystemTable(tableId);
		AdvancedUpdatableDAO<WorkflowAware, Criteria> dao = (AdvancedUpdatableDAO<WorkflowAware, Criteria>) getDaoLocator().<WorkflowAware>locate(table.getName());

		// First - Pull Just One to Check if WorkflowConfig is defined and optional.  If it's optional don't need to attempt
		// to assign where missing on existing entities
		WorkflowAware firstDtoInstance = CollectionUtils.getFirstElement(dao.findBySearchCriteria((HibernateSearchConfigurer) criteria -> criteria.setMaxResults(1)));

		if (firstDtoInstance != null) {
			WorkflowConfig config = AnnotationUtils.getAnnotation(firstDtoInstance.getClass(), WorkflowConfig.class);
			// Only attempt assignments of Workflow States for initial entities if the workflow isn't setup as optional
			// Optional workflows attempt workflow assignments on inserts only - never updates
			if (config == null || config.required()) {
				// At this point we need to assign where missing
				List<WorkflowAware> dtoList = dao.findByField("workflowState.id", new Object[]{null});
				for (WorkflowAware dtoInstance : CollectionUtils.getIterable(dtoList)) {
					if (dtoInstance.getWorkflowState() == null) {
						// During save will update the state
						dao.save(dtoInstance);
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public AdvancedUpdatableDAO<WorkflowAssignment, Criteria> getWorkflowAssignmentDAO() {
		return this.workflowAssignmentDAO;
	}


	public void setWorkflowAssignmentDAO(AdvancedUpdatableDAO<WorkflowAssignment, Criteria> workflowAssignmentDAO) {
		this.workflowAssignmentDAO = workflowAssignmentDAO;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public DaoSingleKeyListCache<WorkflowAssignment, Short> getWorkflowAssignmentCache() {
		return this.workflowAssignmentCache;
	}


	public void setWorkflowAssignmentCache(DaoSingleKeyListCache<WorkflowAssignment, Short> workflowAssignmentCache) {
		this.workflowAssignmentCache = workflowAssignmentCache;
	}
}
