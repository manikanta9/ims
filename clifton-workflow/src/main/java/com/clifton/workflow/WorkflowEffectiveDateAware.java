package com.clifton.workflow;


import java.util.Date;


/**
 * The <code>WorkflowEffectiveDateAware</code> extends {@link WorkflowAware} interface
 * and added support for editing effective start dates.
 *
 * @author Mary Anderson
 */
public interface WorkflowEffectiveDateAware extends WorkflowAware {

	public Date getWorkflowStateEffectiveStartDate();


	public void setWorkflowStateEffectiveStartDate(Date workflowStateEffectiveStartDate);
}
