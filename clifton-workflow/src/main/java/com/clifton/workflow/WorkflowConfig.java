package com.clifton.workflow;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>WorkflowConfig</code> annotation can be used to define how an entity
 * determines the workflow assignments, if workflows are required, etc.
 * <p>
 * Workflow assignments for the table marked with this annotation must be defined even though
 * annotation explicitly identifies the workflow.  In this case, assignments will be used as
 * a filter for available workflows for the annotated DTO.
 *
 * @author manderson
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface WorkflowConfig {

	/**
	 * Specifies whether a workflow assignment is required for each entity
	 * Defaults to True
	 * <p>
	 * When NOT required, assignments are attempted to be made on "CREATE" only
	 */
	boolean required() default true;


	/**
	 * If populated, the workflow assigned to an entity will be determined by getting the
	 * property off of the bean itself and not solely from WorkflowAssignments.
	 * <p>
	 * Note: Workflow returned from the bean property name must still be assigned to the table
	 * And if there is a condition on the assignment it must still pass that condition.
	 * <p>
	 * i.e. ReconcileTradeMatch workflow will be determined by the workflow selected on its "matchType.workflow" property
	 */
	String workflowBeanPropertyName() default "";
}
