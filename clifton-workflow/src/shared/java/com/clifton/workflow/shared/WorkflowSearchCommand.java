package com.clifton.workflow.shared;

import java.io.Serializable;


/**
 * The WorkflowSearchCommand class defines common search filters that can be used with Workflow Aware objects.
 * It is intended to be extended by other search commands for Workflow Aware objects.
 *
 * @author vgomelsky
 */
public class WorkflowSearchCommand implements Serializable {

	/////////  Workflow Status Filters ///////////////

	private Short workflowStatusId;

	private Short workflowStatusIdOrNull;

	private Short[] workflowStatusIds;

	private String workflowStatusNameEquals;

	private String workflowStatusName;

	private String[] workflowStatusNames;

	private String workflowStatusNameOrNull;


	private Short excludeWorkflowStatusId;

	private Short[] excludeWorkflowStatusIds;

	private String excludeWorkflowStatusName;

	private String[] excludeWorkflowStatusNames;


	/////////  Workflow State Filters ///////////////

	private Short workflowStateId;

	private Short workflowStateIdOrNull;

	private Short[] workflowStateIdsOrNull;

	private Short[] workflowStateIds;

	private String workflowStateNameEquals;

	private String workflowStateName;

	private String workflowStateNameOrNull;

	private String[] workflowStateNames;

	private String[] excludeWorkflowStateNames;

	private String[] workflowStateNamesOrNull;

	private String excludeWorkflowStateName;

	private Short excludeWorkflowStateId;

	private Short[] excludeWorkflowStateIds;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getWorkflowStatusId() {
		return this.workflowStatusId;
	}


	public void setWorkflowStatusId(Short workflowStatusId) {
		this.workflowStatusId = workflowStatusId;
	}


	public Short getWorkflowStatusIdOrNull() {
		return this.workflowStatusIdOrNull;
	}


	public void setWorkflowStatusIdOrNull(Short workflowStatusIdOrNull) {
		this.workflowStatusIdOrNull = workflowStatusIdOrNull;
	}


	public Short[] getWorkflowStatusIds() {
		return this.workflowStatusIds;
	}


	public void setWorkflowStatusIds(Short[] workflowStatusIds) {
		this.workflowStatusIds = workflowStatusIds;
	}


	public String getWorkflowStatusNameEquals() {
		return this.workflowStatusNameEquals;
	}


	public void setWorkflowStatusNameEquals(String workflowStatusNameEquals) {
		this.workflowStatusNameEquals = workflowStatusNameEquals;
	}


	public String getWorkflowStatusName() {
		return this.workflowStatusName;
	}


	public void setWorkflowStatusName(String workflowStatusName) {
		this.workflowStatusName = workflowStatusName;
	}


	public String[] getWorkflowStatusNames() {
		return this.workflowStatusNames;
	}


	public void setWorkflowStatusNames(String[] workflowStatusNames) {
		this.workflowStatusNames = workflowStatusNames;
	}


	public String getWorkflowStatusNameOrNull() {
		return this.workflowStatusNameOrNull;
	}


	public void setWorkflowStatusNameOrNull(String workflowStatusNameOrNull) {
		this.workflowStatusNameOrNull = workflowStatusNameOrNull;
	}


	public Short getExcludeWorkflowStatusId() {
		return this.excludeWorkflowStatusId;
	}


	public void setExcludeWorkflowStatusId(Short excludeWorkflowStatusId) {
		this.excludeWorkflowStatusId = excludeWorkflowStatusId;
	}


	public Short[] getExcludeWorkflowStatusIds() {
		return this.excludeWorkflowStatusIds;
	}


	public void setExcludeWorkflowStatusIds(Short[] excludeWorkflowStatusIds) {
		this.excludeWorkflowStatusIds = excludeWorkflowStatusIds;
	}


	public String getExcludeWorkflowStatusName() {
		return this.excludeWorkflowStatusName;
	}


	public void setExcludeWorkflowStatusName(String excludeWorkflowStatusName) {
		this.excludeWorkflowStatusName = excludeWorkflowStatusName;
	}


	public String[] getExcludeWorkflowStatusNames() {
		return this.excludeWorkflowStatusNames;
	}


	public void setExcludeWorkflowStatusNames(String[] excludeWorkflowStatusNames) {
		this.excludeWorkflowStatusNames = excludeWorkflowStatusNames;
	}


	public Short getWorkflowStateId() {
		return this.workflowStateId;
	}


	public void setWorkflowStateId(Short workflowStateId) {
		this.workflowStateId = workflowStateId;
	}


	public Short getWorkflowStateIdOrNull() {
		return this.workflowStateIdOrNull;
	}


	public void setWorkflowStateIdOrNull(Short workflowStateIdOrNull) {
		this.workflowStateIdOrNull = workflowStateIdOrNull;
	}


	public Short[] getWorkflowStateIdsOrNull() {
		return this.workflowStateIdsOrNull;
	}


	public void setWorkflowStateIdsOrNull(Short[] workflowStateIdsOrNull) {
		this.workflowStateIdsOrNull = workflowStateIdsOrNull;
	}


	public Short[] getWorkflowStateIds() {
		return this.workflowStateIds;
	}


	public void setWorkflowStateIds(Short[] workflowStateIds) {
		this.workflowStateIds = workflowStateIds;
	}


	public String getWorkflowStateNameEquals() {
		return this.workflowStateNameEquals;
	}


	public void setWorkflowStateNameEquals(String workflowStateNameEquals) {
		this.workflowStateNameEquals = workflowStateNameEquals;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	public String getWorkflowStateNameOrNull() {
		return this.workflowStateNameOrNull;
	}


	public void setWorkflowStateNameOrNull(String workflowStateNameOrNull) {
		this.workflowStateNameOrNull = workflowStateNameOrNull;
	}


	public String[] getWorkflowStateNames() {
		return this.workflowStateNames;
	}


	public void setWorkflowStateNames(String[] workflowStateNames) {
		this.workflowStateNames = workflowStateNames;
	}


	public String[] getExcludeWorkflowStateNames() {
		return this.excludeWorkflowStateNames;
	}


	public void setExcludeWorkflowStateNames(String[] excludeWorkflowStateNames) {
		this.excludeWorkflowStateNames = excludeWorkflowStateNames;
	}


	public String[] getWorkflowStateNamesOrNull() {
		return this.workflowStateNamesOrNull;
	}


	public void setWorkflowStateNamesOrNull(String[] workflowStateNamesOrNull) {
		this.workflowStateNamesOrNull = workflowStateNamesOrNull;
	}


	public String getExcludeWorkflowStateName() {
		return this.excludeWorkflowStateName;
	}


	public void setExcludeWorkflowStateName(String excludeWorkflowStateName) {
		this.excludeWorkflowStateName = excludeWorkflowStateName;
	}


	public Short getExcludeWorkflowStateId() {
		return this.excludeWorkflowStateId;
	}


	public void setExcludeWorkflowStateId(Short excludeWorkflowStateId) {
		this.excludeWorkflowStateId = excludeWorkflowStateId;
	}


	public Short[] getExcludeWorkflowStateIds() {
		return this.excludeWorkflowStateIds;
	}


	public void setExcludeWorkflowStateIds(Short[] excludeWorkflowStateIds) {
		this.excludeWorkflowStateIds = excludeWorkflowStateIds;
	}
}
