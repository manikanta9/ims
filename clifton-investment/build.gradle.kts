import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant


val apiVariant = registerVariant("api", upstreamForMain = true)
val sharedVariant = registerVariant("shared", upstreamForMain = true)


dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	apiVariant.api(sharedVariant())

	sharedVariant.api(project(":clifton-business")) { usingVariant("shared") }
	sharedVariant.api(project(":clifton-core")) { usingVariant("shared") }
	sharedVariant.api(project(":clifton-core:clifton-core-utils"))
	api(project(":clifton-system"))
	api(project(":clifton-system:clifton-system-query"))
	api(project(":clifton-business"))
	api(project(":clifton-business:clifton-business-contact"))
	api(project(":clifton-business:clifton-business-contract"))
	api(project(":clifton-business:clifton-business-client"))
	api(project(":clifton-report"))
	api(project(":clifton-rule"))
	api(project(":clifton-instruction"))
	api(project(":clifton-export-messaging"))
	api(project(":clifton-calendar:clifton-calendar-schedule"))


	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-business")))
	testFixturesApi(testFixtures(project(":clifton-business:clifton-business-contact")))
	testFixturesApi(testFixtures(project(":clifton-business:clifton-business-contract")))
	testFixturesApi(testFixtures(project(":clifton-business:clifton-business-client")))
	testFixturesApi(testFixtures(project(":clifton-report")))
	testFixturesApi(testFixtures(project(":clifton-rule")))
}
