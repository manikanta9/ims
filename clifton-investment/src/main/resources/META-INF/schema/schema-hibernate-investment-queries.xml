<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping SYSTEM "http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd">
<hibernate-mapping>

	<class name="com.clifton.investment.instrument.structure.InvestmentInstrumentSecurityStructure" mutable="false" polymorphism="explicit">
		<subselect>
			SELECT uuid = NEWID()
			, st.InvestmentInstrumentStructureID AS 'InvestmentInstrumentStructureID'
			, s.InvestmentSecurityID AS 'InvestmentSecurityID'
			, CASE WHEN EXISTS (SELECT * FROM InvestmentSecurityStructureWeight w INNER JOIN InvestmentInstrumentStructureWeight iw ON w.InvestmentInstrumentStructureWeightID = iw.InvestmentInstrumentStructureWeightID WHERE iw.InvestmentInstrumentStructureID = st.InvestmentInstrumentStructureID AND w.InvestmentSecurityID = s.InvestmentSecurityID) THEN 1 ELSE 0 END AS 'IsPopulated'
			FROM InvestmentInstrumentStructure st
			INNER JOIN InvestmentSecurity s ON st.InvestmentInstrumentID = s.InvestmentInstrumentID
		</subselect>
		<synchronize table="InvestmentInstrumentStructure" />
		<synchronize table="InvestmentSecurity" />
		<id name="uuid">
			<generator class="assigned" />
		</id>
		<many-to-one embed-xml="false" lazy="false" name="structure" column="InvestmentInstrumentStructureID" class="com.clifton.investment.instrument.structure.InvestmentInstrumentStructure" />
		<many-to-one embed-xml="false" lazy="false" name="security" column="InvestmentSecurityID" class="com.clifton.investment.instrument.InvestmentSecurity" />
		<property name="populated" column="IsPopulated" />
	</class>

	<class name="com.clifton.investment.manager.balance.stale.InvestmentManagerAccountBalanceStaleSummary" mutable="false" polymorphism="explicit">
		<subselect>
			<![CDATA[
			SELECT uuid = NEWID(), x.BusinessCompanyID,x.BalanceDate,SUM(CASE WHEN ABS(AdjustedDifference) <= 1 THEN 1 ELSE 0 END) "StaleCount", AVG(a.StaleCount) "AverageStaleCount"
			FROM
			(
				SELECT ca.CustodianBusinessCompanyID "BusinessCompanyID",b.BalanceDate,(b.OriginalCashValue+b.OriginalSecuritiesValue) - (b2.OriginalCashValue+b2.OriginalSecuritiesValue) "OriginalDifference",
					(b.AdjustedCashValue+b.AdjustedSecuritiesValue) - (b2.AdjustedCashValue+b2.AdjustedSecuritiesValue) "AdjustedDifference"
				FROM InvestmentManagerAccountBalance b
					INNER JOIN InvestmentManagerAccount a ON a.InvestmentManagerAccountID = b.InvestmentManagerAccountID
					INNER JOIN InvestmentManagerCustodianAccount ca ON ca.InvestmentManagerCustodianAccountID = a.CustodianAccountID
					LEFT JOIN InvestmentManagerAccountBalance b2 ON b2.InvestmentManagerAccountID = b.InvestmentManagerAccountID
						AND b2.BalanceDate = (
							SELECT TOP 1 BalanceDate FROM InvestmentManagerAccountBalance b3
							WHERE b3.InvestmentManagerAccountID = b.InvestmentManagerAccountID AND b3.BalanceDate < b.BalanceDate
							ORDER BY BalanceDate DESC
						)
				WHERE b.BalanceDate=? AND a.ManagerType LIKE ? AND (? = 1 OR b.AdjustedCashValue > ?)
			) x
			INNER JOIN
			(
				SELECT BusinessCompanyID, AVG(StaleCount) "StaleCount"
				FROM (
					SELECT x.BusinessCompanyID,x.BalanceDate,SUM(CASE WHEN ABS(AdjustedDifference) <= 1 THEN 1 ELSE 0 END) "StaleCount"
					FROM
					(
						SELECT ca.CustodianBusinessCompanyID "BusinessCompanyID", b.BalanceDate,(b.OriginalCashValue+b.OriginalSecuritiesValue) - (b2.OriginalCashValue+b2.OriginalSecuritiesValue) "OriginalDifference",
							(b.AdjustedCashValue+b.AdjustedSecuritiesValue) - (b2.AdjustedCashValue+b2.AdjustedSecuritiesValue) "AdjustedDifference"
						FROM InvestmentManagerAccountBalance b
							INNER JOIN InvestmentManagerAccount a ON a.InvestmentManagerAccountID = b.InvestmentManagerAccountID
							INNER JOIN InvestmentManagerCustodianAccount ca ON ca.InvestmentManagerCustodianAccountID = a.CustodianAccountID
							LEFT JOIN InvestmentManagerAccountBalance b2 ON b2.InvestmentManagerAccountID = b.InvestmentManagerAccountID
								AND b2.BalanceDate = (
									SELECT TOP 1 BalanceDate FROM InvestmentManagerAccountBalance b3
									WHERE b3.InvestmentManagerAccountID = b.InvestmentManagerAccountID AND b3.BalanceDate < b.BalanceDate
									ORDER BY BalanceDate DESC
									)
						WHERE b.BalanceDate >= ? AND b.BalanceDate <= ? AND a.ManagerType LIKE ?
							AND (? = 1 OR b.AdjustedCashValue > ?)
					) x
					GROUP BY BusinessCompanyID, BalanceDate
				)x
				GROUP BY BusinessCompanyID
			) a ON a.BusinessCompanyID = x.BusinessCompanyID
			GROUP BY x.BusinessCompanyID,x.BalanceDate
			]]>
		</subselect>
		<synchronize table="InvestmentManagerAccountBalance" />
		<synchronize table="InvestmentManagerAccount" />
		<synchronize table="InvestmentManagerCustodianAccount" />
		<id name="uuid">
			<generator class="assigned" />
		</id>
		<many-to-one embed-xml="false" lazy="false" name="custodianCompany" column="BusinessCompanyID" class="com.clifton.business.company.BusinessCompany" />
		<property name="balanceDate" column="BalanceDate" type="date" />
		<property name="staleCount" column="StaleCount" />
		<property name="averageStaleCount" column="AverageStaleCount" />
	</class>

	<class name="com.clifton.investment.manager.balance.stale.InvestmentManagerAccountBalanceStaleDetail" mutable="false" polymorphism="explicit">
		<subselect>
			<![CDATA[
			SELECT (b.OriginalCashValue+b.OriginalSecuritiesValue) - (b2.OriginalCashValue+b2.OriginalSecuritiesValue) "OriginalDifference",
				(b.AdjustedCashValue+b.AdjustedSecuritiesValue) - (b2.AdjustedCashValue+b2.AdjustedSecuritiesValue) "AdjustedDifference",b.*
			FROM InvestmentManagerAccountBalance b
				LEFT JOIN InvestmentManagerAccountBalance b2 ON b2.InvestmentManagerAccountID = b.InvestmentManagerAccountID
					AND b2.BalanceDate = (
						SELECT TOP 1 BalanceDate FROM InvestmentManagerAccountBalance b3
						WHERE b3.InvestmentManagerAccountID = b.InvestmentManagerAccountID AND b3.BalanceDate < b.BalanceDate
						ORDER BY BalanceDate DESC
					)
			WHERE b.BalanceDate=?
			]]>
		</subselect>
		<synchronize table="InvestmentManagerAccountBalance" />
		<synchronize table="InvestmentManagerAccount" />
		<synchronize table="RuleViolationStatus" />
		<id name="id" column="InvestmentManagerAccountBalanceID" type="int">
			<generator class="identity" />
		</id>
		<many-to-one embed-xml="false" lazy="false" name="managerAccount" column="InvestmentManagerAccountID" class="com.clifton.investment.manager.InvestmentManagerAccount" />
		<property name="balanceDate" column="BalanceDate" type="date" />
		<many-to-one embed-xml="false" lazy="false" name="violationStatus" column="RuleViolationStatusID" class="com.clifton.rule.violation.status.RuleViolationStatus" />
		<property name="cashValue" column="OriginalCashValue" type="big_decimal" precision="19" scale="2" />
		<property name="securitiesValue" column="OriginalSecuritiesValue" type="big_decimal" precision="19" scale="2" />
		<property name="adjustedCashValue" column="AdjustedCashValue" type="big_decimal" precision="19" scale="2" />
		<property name="adjustedSecuritiesValue" column="AdjustedSecuritiesValue" type="big_decimal" precision="19" scale="2" />
		<property name="marketOnCloseCashValue" column="MarketOnCloseCashValue" type="big_decimal" precision="19" scale="2" />
		<property name="marketOnCloseSecuritiesValue" column="MarketOnCloseSecuritiesValue" type="big_decimal" precision="19" scale="2" />
		<property name="note" column="BalanceNote" />
		<property name="createUserId" column="CreateUserID" update="false" />
		<property name="createDate" column="CreateDate" update="false" />
		<property name="updateUserId" column="UpdateUserID" />
		<property name="updateDate" column="UpdateDate" />
		<property name="rv" column="rv" insert="false" update="false" />
		<property name="difference" column="OriginalDifference" type="big_decimal" precision="19" scale="2" />
		<property name="adjustedDifference" column="AdjustedDifference" type="big_decimal" precision="19" scale="2" />
	</class>

	<class name="com.clifton.investment.instrument.event.InvestmentSecurityEventExtended" mutable="false" polymorphism="explicit">
		<!-- uuid is a GUID that's not used anywhere by is required by Hibernate: must have a PK -->
		<subselect>
			<![CDATA[
			SELECT
				uuid = NEWID(),
				e.*,
				ep.InvestmentSecurityEventPayoutID "PayoutID",
				ep.BeforeEventValue "PayoutBeforeEventValue",
				ep.AfterEventValue "PayoutAfterEventValue",
				ep.PayoutSecurityID,
				ep.AdditionalPayoutDate,
				ep.AdditionalPayoutValue,
				ep.AdditionalPayoutValue2,
				ep.ProrationRate,
				ep.PayoutDescription,
				ep.InvestmentSecurityEventPayoutID,
				ep.InvestmentSecurityEventPayoutTypeID,
				ep.PayoutNumber,
				ep.ElectionNumber,
				ep.FractionalSharesMethod,
				ep.IsDefaultElection,
				ep.IsDeleted
			FROM InvestmentSecurityEvent e
				LEFT JOIN InvestmentSecurityEventPayout ep ON ep.InvestmentSecurityEventID = e.InvestmentSecurityEventID
			]]>
		</subselect>

		<synchronize table="InvestmentSecurityEvent" />
		<synchronize table="InvestmentSecurityEventPayout" />

		<id name="uuid">
			<generator class="assigned" />
		</id>
		<!-- Event Fields -->
		<property name="id" column="InvestmentSecurityEventID" type="int" />
		<many-to-one embed-xml="false" lazy="false" name="type" column="InvestmentSecurityEventTypeID" class="com.clifton.investment.instrument.event.InvestmentSecurityEventType" />
		<many-to-one embed-xml="false" lazy="false" name="status" column="InvestmentSecurityEventStatusID" class="com.clifton.investment.instrument.event.InvestmentSecurityEventStatus" />
		<many-to-one embed-xml="false" lazy="false" name="security" column="InvestmentSecurityID" class="com.clifton.investment.instrument.InvestmentSecurity" />
		<many-to-one embed-xml="false" lazy="false" name="additionalSecurity" column="NewInvestmentSecurityID" class="com.clifton.investment.instrument.InvestmentSecurity" />
		<property name="corporateActionIdentifier" column="CorporateActionIdentifier" />
		<property name="beforeEventValue" column="BeforeEventValue" type="big_decimal" precision="28" scale="15" />
		<property name="afterEventValue" column="AfterEventValue" type="big_decimal" precision="28" scale="15" />
		<property name="additionalEventValue" column="AdditionalEventValue" type="big_decimal" precision="28" scale="15" />
		<property name="declareDate" column="EventDeclareDate" type="date" />
		<property name="exDate" column="EventExDate" type="date" />
		<property name="recordDate" column="EventRecordDate" type="date" />
		<property name="eventDate" column="EventDate" type="date" />
		<property name="additionalDate" column="AdditionalDate" type="date" />
		<property name="actualSettlementDate" column="ActualSettlementDate" type="date" />
		<property name="eventDescription" column="EventDescription" length="500" />
		<property name="bookingOrderOverride" column="BookingOrderOverride" />
		<property name="voluntary" column="IsVoluntary" />
		<property name="taxable" column="IsTaxable" />
		<property name="createUserId" column="CreateUserID" update="false" />
		<property name="createDate" column="CreateDate" update="false" />
		<property name="updateUserId" column="UpdateUserID" />
		<property name="updateDate" column="UpdateDate" />
		<property name="rv" column="rv" insert="false" update="false" />

		<!-- Payout Fields -->
		<many-to-one embed-xml="false" lazy="false" name="payout" column="PayoutID" class="com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout" />
		<many-to-one embed-xml="false" lazy="false" name="payoutType" column="InvestmentSecurityEventPayoutTypeID" class="com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType" />
		<property name="electionNumber" column="ElectionNumber" />
		<property name="payoutNumber" column="PayoutNumber" />
		<many-to-one embed-xml="false" lazy="false" name="payoutSecurity" column="PayoutSecurityID" class="com.clifton.investment.instrument.InvestmentSecurity" />
		<property name="payoutBeforeEventValue" column="PayoutBeforeEventValue" type="big_decimal" precision="28" scale="15" />
		<property name="payoutAfterEventValue" column="PayoutAfterEventValue" type="big_decimal" precision="28" scale="15" />
		<property name="additionalPayoutValue" column="AdditionalPayoutValue" type="big_decimal" precision="28" scale="15" />
		<property name="additionalPayoutValue2" column="AdditionalPayoutValue2" type="big_decimal" precision="28" scale="15" />
		<property name="prorationRate" column="ProrationRate" type="big_decimal" precision="28" scale="15" />
		<property name="additionalPayoutDate" column="AdditionalPayoutDate" type="date" />
		<property name="fractionalSharesMethod" column="FractionalSharesMethod" type="com_clifton_investment_instrument_event_FractionalShares" length="50" />
		<property name="payoutDescription" column="PayoutDescription" length="500" />
		<property name="defaultElection" column="IsDefaultElection" />
		<property name="deleted" column="IsDeleted" />
	</class>
</hibernate-mapping>
