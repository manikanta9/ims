package com.clifton.investment.manager.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;


/**
 * @author nickk
 */
public abstract class BaseInvestmentManagerAccountAllocationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id", searchFieldPath = "managerAccountAssignment")
	private Integer investmentManagerAccountId;

	@SearchField(searchField = "referenceTwo.id", searchFieldPath = "managerAccountAssignment")
	private Integer investmentAccountId;

	@SearchField(searchField = "inactive", searchFieldPath = "managerAccountAssignment")
	private Boolean inactiveAssignment;

	@SearchField
	private Boolean privateAssignment;

	@SearchField
	private BigDecimal allocationPercent;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentManagerAccountId() {
		return this.investmentManagerAccountId;
	}


	public void setInvestmentManagerAccountId(Integer investmentManagerAccountId) {
		this.investmentManagerAccountId = investmentManagerAccountId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Boolean getInactiveAssignment() {
		return this.inactiveAssignment;
	}


	public void setInactiveAssignment(Boolean inactiveAssignment) {
		this.inactiveAssignment = inactiveAssignment;
	}


	public Boolean getPrivateAssignment() {
		return this.privateAssignment;
	}


	public void setPrivateAssignment(Boolean privateAssignment) {
		this.privateAssignment = privateAssignment;
	}


	public BigDecimal getAllocationPercent() {
		return this.allocationPercent;
	}


	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}
}
