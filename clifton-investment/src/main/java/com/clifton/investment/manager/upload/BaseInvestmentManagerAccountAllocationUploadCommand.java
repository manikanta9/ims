package com.clifton.investment.manager.upload;

import com.clifton.core.dataaccess.file.upload.FileUploadCommand;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.manager.InvestmentManagerAccount;

import java.util.HashMap;
import java.util.Map;


/**
 * @author nickk
 */
public abstract class BaseInvestmentManagerAccountAllocationUploadCommand<T extends BaseInvestmentManagerAccountAllocationUploadTemplate> extends FileUploadCommand<T> {


	/**
	 * When false, requires that manager allocations for each manager in the upload results in 100% asset class allocations
	 */
	private boolean allowPartiallyAllocatedManagers;


	/**
	 * When false, only the allocation percentages of existing manager allocations can be made,
	 * However, if true, then existing allocations, not in the file for the manager are removed, and new ones are added.
	 */
	private boolean allowAddingOrRemovingAllocations;


	/**
	 * True indicates that if not all rows can be uploaded,
	 * upload the successful rows and ignore the unsuccessful rows
	 * else don't upload any rows
	 */
	private boolean partialUploadAllowed;

	////////////////////////////////////////////////////////////////////////////////

	// Used during processing to track Accounts for each client account number and manager account number so we don't have to retrieve each time
	private final Map<String, InvestmentAccount> clientAccountNumberMap = new HashMap<>();
	private final Map<String, InvestmentManagerAccount> managerAccountNumberMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////


	public void clearResults() {
		setUploadResultsString("");
		getClientAccountNumberMap().clear();
		getManagerAccountNumberMap().clear();
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isAllowPartiallyAllocatedManagers() {
		return this.allowPartiallyAllocatedManagers;
	}


	public void setAllowPartiallyAllocatedManagers(boolean allowPartiallyAllocatedManagers) {
		this.allowPartiallyAllocatedManagers = allowPartiallyAllocatedManagers;
	}


	public boolean isAllowAddingOrRemovingAllocations() {
		return this.allowAddingOrRemovingAllocations;
	}


	public void setAllowAddingOrRemovingAllocations(boolean allowAddingOrRemovingAllocations) {
		this.allowAddingOrRemovingAllocations = allowAddingOrRemovingAllocations;
	}


	public boolean isPartialUploadAllowed() {
		return this.partialUploadAllowed;
	}


	public void setPartialUploadAllowed(boolean partialUploadAllowed) {
		this.partialUploadAllowed = partialUploadAllowed;
	}


	public Map<String, InvestmentAccount> getClientAccountNumberMap() {
		return this.clientAccountNumberMap;
	}


	public Map<String, InvestmentManagerAccount> getManagerAccountNumberMap() {
		return this.managerAccountNumberMap;
	}
}
