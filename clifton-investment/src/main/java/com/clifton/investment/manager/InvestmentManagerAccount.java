package com.clifton.investment.manager;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.custodian.InvestmentManagerCustodianAccount;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentManagerAccount</code> class represents client's investment manager account
 * that we need to track information about (daily cash/security) balances.
 * <p>
 * This information maybe obtained daily from custodian or manually for some managers.
 *
 * @author vgomelsky
 */
public class InvestmentManagerAccount extends BaseEntity<Integer> implements LabeledObject, SystemColumnCustomValueAware {

	public enum ManagerTypes {
		STANDARD, PROXY, ROLLUP, LINKED
	}

	private BusinessClient client;
	private BusinessCompany managerCompany;

	/**
	 * Base CCY of the Manager Account - This is usually the same as the base currency of the account that uses this manager, but if different
	 * portfolio processing or various uses for a specific account would convert the manager balance to the client account base CCY
	 */
	private InvestmentSecurity baseCurrency;

	/**
	 * Our account number for this account: enforce 6 characters via UI
	 */
	private String accountNumber;
	private String accountName;

	// optional custodian information if reporting is provided by custodian
	private InvestmentManagerCustodianAccount custodianAccount; // must be unique per company and account number
	private boolean zeroBalanceIfNoDownload;

	/**
	 * Cash Allocation Attributes
	 * NONE = cashPercent = null
	 * CASH = cashPercent = 100
	 * SECURITIES = cashPercent = 0
	 * MANAGER = determine cash percentage based upon selected manager
	 * CUSTOM_PERCENT = user entered custom cash percent value
	 * CUSTOM_VALUE = User entered custom static cash value
	 * IGNORE_CASH = ignoreCash = true (adjustments revert original value)
	 * IGNORE_SECURITIES = ignoreSecurities = true (adjustments revert original value)
	 */
	public enum CashAdjustmentType {
		NONE, CASH, SECURITIES, CUSTOM_VALUE, CUSTOM_PERCENT, MANAGER, IGNORE_CASH, IGNORE_SECURITIES
	}

	private CashAdjustmentType cashAdjustmentType;

	private BigDecimal customCashValue;
	private Date customCashValueUpdateDate;

	private InvestmentManagerAccount cashPercentManager;

	private List<InvestmentManagerAccountAdjustmentM2M> adjustmentM2MList;

	private boolean inactive;

	private ManagerTypes managerType = ManagerTypes.STANDARD;

	private LinkedManagerTypes linkedManagerType;

	/**
	 * Linked Managers are tied to another Investment Account for the same client
	 * and can have different attributes.  The attributes that apply to each linked manager type
	 * are defined by custom fields Linked Manager column group.
	 */
	private InvestmentAccount linkedInvestmentAccount;

	// Used for reporting purposes
	private InvestmentSecurity benchmarkSecurity;

	/**
	 * By default, the benchmark security will use the Duration MarketDataField value for valuation.
	 * This property allows the field to be overridden on a per manager account basis to be used by the benchmark security.
	 */
	private String durationFieldNameOverride;

	/**
	 * Proxy Info
	 */
	private boolean proxyValueGrowthInverted;
	private BigDecimal proxyValue;
	private Date proxyValueDate;

	// Proxy Manager Balances can be auto adjusted based on proxy value date and the security's performance
	private InvestmentSecurity proxyBenchmarkSecurity;
	// Used for reporting to display this name instead of the security ticker or name
	// Allows for reuse of the same custom security allocations but display the name that the client/consultant wants to see
	private String proxyBenchmarkSecurityDisplayName;
	// Proxy Manager Balances can be auto created based on a security and quantity
	private BigDecimal proxySecurityQuantity;
	private InvestmentInterestRateIndex proxyBenchmarkRateIndex;
	/**
	 * Proxy balances will be used for Securities balances only.
	 * Custodial accounts will still be used for Cash Balances.
	 */
	private boolean proxySecuritiesOnly;

	/**
	 * Used for Managers with Proxy Benchmark whose price isn't available every date, i.e. 2 day lag
	 */
	private boolean proxyToLastKnownPrice;

	/**
	 * Rollup Managers can have child managers whose balances are "rolled up" into the
	 * main manager.
	 */
	private List<InvestmentManagerAccountRollup> rollupManagerList;

	/**
	 * The rollup type the manager should use to aggregate balances.
	 * <p>
	 * SUM - Sums all balances of child manager accounts.
	 * MIN - Takes only the minimum balance of the child manager accounts.
	 * MAX - Takes only the maximum balance of the child manager accounts.
	 */
	private AggregationOperations rollupAggregationType;

	/**
	 * Custom field support
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;


	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getLabelUsingAccountNumber(getAccountNumber());
	}


	public String getLabelShort() {
		if (getAccountName() != null) {
			return getAccountNumber() + ": " + getAccountName();
		}
		return getAccountNumber();
	}


	public String getLabelCustodian() {
		if (getCustodianAccount() != null) {
			return getLabelUsingAccountNumber(getCustodianAccount().getNumber());
		}
		return getLabelUsingAccountNumber(getAccountNumber());
	}


	private String getLabelUsingAccountNumber(String accountNum) {
		if (getAccountName() != null) {
			return accountNum + ": " + getAccountName();
		}
		if (getManagerCompany() != null) {
			return accountNum + ": " + getManagerCompany().getName();
		}
		return accountNum;
	}


	public BigDecimal getCashPercent() {
		if (CashAdjustmentType.SECURITIES == getCashAdjustmentType()) {
			return BigDecimal.ZERO;
		}
		if (CashAdjustmentType.CASH == getCashAdjustmentType()) {
			return MathUtils.BIG_DECIMAL_ONE_HUNDRED;
		}
		if (CashAdjustmentType.CUSTOM_PERCENT == getCashAdjustmentType()) {
			return getCustomCashValue();
		}
		return null;
	}


	public boolean isCustodian() {
		return getCustodianAccount() != null;
	}


	public String getCustodianLabel() {
		if (getCustodianAccount() != null) {
			return getCustodianAccount().getNumber() + ": " + getCustodianAccount().getCompany().getLabel();
		}
		return null;
	}


	public boolean isManagerTypeSelected() {
		return ManagerTypes.STANDARD != getManagerType();
	}


	public boolean isProxyManager() {
		return ManagerTypes.PROXY == getManagerType();
	}


	public void setProxyManager(boolean proxyManager) {
		if (proxyManager) {
			setManagerType(ManagerTypes.PROXY);
		}
	}


	public boolean isRollupManager() {
		return ManagerTypes.ROLLUP == getManagerType();
	}


	public void setRollupManager(boolean rollupManager) {
		if (rollupManager) {
			setManagerType(ManagerTypes.ROLLUP);
		}
	}


	public boolean isLinkedManager() {
		return ManagerTypes.LINKED == getManagerType();
	}


	public boolean isLinkedManagerProcessingDependent() {
		if (isLinkedManager() && getLinkedManagerType() != null) {
			return getLinkedManagerType().isProcessingDependent();
		}
		return false;
	}


	public String getLinkedManagerTypeLabel() {
		if (getLinkedManagerType() != null) {
			return getLinkedManagerType().getLabel();
		}
		return null;
	}


	public void setLinkedManager(boolean linkedManager) {
		if (linkedManager) {
			setManagerType(ManagerTypes.LINKED);
		}
	}


	public boolean isCashAllocation() {
		return CashAdjustmentType.NONE != getCashAdjustmentType();
	}


	public boolean isAdjustmentM2MExists() {
		return !CollectionUtils.isEmpty(getAdjustmentM2MList());
	}


	public String getAutoGeneratedAccountNumber() {
		if (isNewBean()) {
			return null;
		}
		return String.format("M%05d", getId());
	}


	public boolean isAccountNumberAutoGenerated() {
		if (getAccountNumber() != null) {
			return getAccountNumber().equals(getAutoGeneratedAccountNumber());
		}
		return false;
	}


	public boolean isProxyAdjustmentAppliedToManager() {
		if (isProxyManager()) {
			// Benchmark Security is populated and manager balance is not determined by the number of shares, but proxied based on the security's performance
			if (getProxyBenchmarkSecurity() != null && MathUtils.isNullOrZero(getProxySecurityQuantity())) {
				return true;
			}
			// Otherwise balance is adjusted based on given index
			return (getProxyBenchmarkRateIndex() != null);
		}
		return false;
	}


	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////


	public BusinessCompany getManagerCompany() {
		return this.managerCompany;
	}


	public void setManagerCompany(BusinessCompany managerCompany) {
		this.managerCompany = managerCompany;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public InvestmentManagerCustodianAccount getCustodianAccount() {
		return this.custodianAccount;
	}


	public void setCustodianAccount(InvestmentManagerCustodianAccount custodianAccount) {
		this.custodianAccount = custodianAccount;
	}


	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public boolean isProxyValueGrowthInverted() {
		return this.proxyValueGrowthInverted;
	}


	public void setProxyValueGrowthInverted(boolean proxyValueGrowthInverted) {
		this.proxyValueGrowthInverted = proxyValueGrowthInverted;
	}


	public BigDecimal getProxyValue() {
		return this.proxyValue;
	}


	public void setProxyValue(BigDecimal proxyValue) {
		this.proxyValue = proxyValue;
	}


	public Date getProxyValueDate() {
		return this.proxyValueDate;
	}


	public void setProxyValueDate(Date proxyValueDate) {
		this.proxyValueDate = proxyValueDate;
	}


	public InvestmentSecurity getProxyBenchmarkSecurity() {
		return this.proxyBenchmarkSecurity;
	}


	public void setProxyBenchmarkSecurity(InvestmentSecurity proxyBenchmarkSecurity) {
		this.proxyBenchmarkSecurity = proxyBenchmarkSecurity;
	}


	public BusinessClient getClient() {
		return this.client;
	}


	public void setClient(BusinessClient client) {
		this.client = client;
	}


	public List<InvestmentManagerAccountRollup> getRollupManagerList() {
		return this.rollupManagerList;
	}


	public void setRollupManagerList(List<InvestmentManagerAccountRollup> rollupManagerList) {
		this.rollupManagerList = rollupManagerList;
	}


	public AggregationOperations getRollupAggregationType() {
		return this.rollupAggregationType;
	}


	public void setRollupAggregationType(AggregationOperations rollupAggregationType) {
		this.rollupAggregationType = rollupAggregationType;
	}


	public InvestmentManagerAccount getCashPercentManager() {
		return this.cashPercentManager;
	}


	public void setCashPercentManager(InvestmentManagerAccount cashPercentManager) {
		this.cashPercentManager = cashPercentManager;
	}


	public List<InvestmentManagerAccountAdjustmentM2M> getAdjustmentM2MList() {
		return this.adjustmentM2MList;
	}


	public void setAdjustmentM2MList(List<InvestmentManagerAccountAdjustmentM2M> adjustmentM2MList) {
		this.adjustmentM2MList = adjustmentM2MList;
	}


	public boolean isProxySecuritiesOnly() {
		return this.proxySecuritiesOnly;
	}


	public void setProxySecuritiesOnly(boolean proxySecuritiesOnly) {
		this.proxySecuritiesOnly = proxySecuritiesOnly;
	}


	public BigDecimal getProxySecurityQuantity() {
		return this.proxySecurityQuantity;
	}


	public void setProxySecurityQuantity(BigDecimal proxySecurityQuantity) {
		this.proxySecurityQuantity = proxySecurityQuantity;
	}


	public boolean isZeroBalanceIfNoDownload() {
		return this.zeroBalanceIfNoDownload;
	}


	public void setZeroBalanceIfNoDownload(boolean zeroBalanceIfNoDownload) {
		this.zeroBalanceIfNoDownload = zeroBalanceIfNoDownload;
	}


	public ManagerTypes getManagerType() {
		return this.managerType;
	}


	public void setManagerType(ManagerTypes managerType) {
		this.managerType = managerType;
	}


	public InvestmentAccount getLinkedInvestmentAccount() {
		return this.linkedInvestmentAccount;
	}


	public void setLinkedInvestmentAccount(InvestmentAccount linkedInvestmentAccount) {
		this.linkedInvestmentAccount = linkedInvestmentAccount;
	}


	public InvestmentInterestRateIndex getProxyBenchmarkRateIndex() {
		return this.proxyBenchmarkRateIndex;
	}


	public void setProxyBenchmarkRateIndex(InvestmentInterestRateIndex proxyBenchmarkRateIndex) {
		this.proxyBenchmarkRateIndex = proxyBenchmarkRateIndex;
	}


	public LinkedManagerTypes getLinkedManagerType() {
		return this.linkedManagerType;
	}


	public void setLinkedManagerType(LinkedManagerTypes linkedManagerType) {
		this.linkedManagerType = linkedManagerType;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	public BigDecimal getCustomCashValue() {
		return this.customCashValue;
	}


	public void setCustomCashValue(BigDecimal customCashValue) {
		this.customCashValue = customCashValue;
	}


	public Date getCustomCashValueUpdateDate() {
		return this.customCashValueUpdateDate;
	}


	public void setCustomCashValueUpdateDate(Date customCashValueUpdateDate) {
		this.customCashValueUpdateDate = customCashValueUpdateDate;
	}


	public CashAdjustmentType getCashAdjustmentType() {
		return this.cashAdjustmentType;
	}


	public void setCashAdjustmentType(CashAdjustmentType cashAdjustmentType) {
		this.cashAdjustmentType = cashAdjustmentType;
	}


	public boolean isProxyToLastKnownPrice() {
		return this.proxyToLastKnownPrice;
	}


	public void setProxyToLastKnownPrice(boolean proxyToLastKnownPrice) {
		this.proxyToLastKnownPrice = proxyToLastKnownPrice;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public String getDurationFieldNameOverride() {
		return this.durationFieldNameOverride;
	}


	public void setDurationFieldNameOverride(String durationFieldNameOverride) {
		this.durationFieldNameOverride = durationFieldNameOverride;
	}


	public String getProxyBenchmarkSecurityDisplayName() {
		return this.proxyBenchmarkSecurityDisplayName;
	}


	public void setProxyBenchmarkSecurityDisplayName(String proxyBenchmarkSecurityDisplayName) {
		this.proxyBenchmarkSecurityDisplayName = proxyBenchmarkSecurityDisplayName;
	}


	public InvestmentSecurity getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(InvestmentSecurity baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
}
