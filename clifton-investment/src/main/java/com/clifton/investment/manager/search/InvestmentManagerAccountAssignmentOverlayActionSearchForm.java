package com.clifton.investment.manager.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import com.clifton.investment.manager.ManagerCashOverlayActionTypes;

import java.math.BigDecimal;


/**
 * The <code>InvestmentManagerAccountAssignmentOverlayActionSearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentManagerAccountAssignmentOverlayActionSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "managerAccountAssignment.id")
	private Integer managerAccountAssignmentId;

	@SearchField(searchField = "referenceTwo.id", searchFieldPath = "managerAccountAssignment")
	private Integer investmentAccountId;

	@SearchField
	private ManagerCashOverlayActionTypes overlayActionType;

	@SearchField
	private BigDecimal overlayActionAmount;

	@SearchField
	private BigDecimal overlayActionPercent;

	@SearchField
	private Boolean applyDifferenceToSecurities;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getManagerAccountAssignmentId() {
		return this.managerAccountAssignmentId;
	}


	public void setManagerAccountAssignmentId(Integer managerAccountAssignmentId) {
		this.managerAccountAssignmentId = managerAccountAssignmentId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public ManagerCashOverlayActionTypes getOverlayActionType() {
		return this.overlayActionType;
	}


	public void setOverlayActionType(ManagerCashOverlayActionTypes overlayActionType) {
		this.overlayActionType = overlayActionType;
	}


	public BigDecimal getOverlayActionAmount() {
		return this.overlayActionAmount;
	}


	public void setOverlayActionAmount(BigDecimal overlayActionAmount) {
		this.overlayActionAmount = overlayActionAmount;
	}


	public BigDecimal getOverlayActionPercent() {
		return this.overlayActionPercent;
	}


	public void setOverlayActionPercent(BigDecimal overlayActionPercent) {
		this.overlayActionPercent = overlayActionPercent;
	}


	public Boolean getApplyDifferenceToSecurities() {
		return this.applyDifferenceToSecurities;
	}


	public void setApplyDifferenceToSecurities(Boolean applyDifferenceToSecurities) {
		this.applyDifferenceToSecurities = applyDifferenceToSecurities;
	}
}
