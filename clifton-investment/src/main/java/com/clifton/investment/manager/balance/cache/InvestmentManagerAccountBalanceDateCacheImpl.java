package com.clifton.investment.manager.balance.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>InvestmentManagerAccountBalanceDateCacheImpl</code> caches key ManagerAccountID_BalanceDate to the balance record id that represents it
 * <p>
 * Clears the record from the cache ONLY when balance record is deleted - since it stores id values and manger/balance date can't change update changes are unnecessary to track
 *
 * @author manderson
 */
@Component
public class InvestmentManagerAccountBalanceDateCacheImpl extends SelfRegisteringSimpleDaoCache<InvestmentManagerAccountBalance, String, Integer> implements InvestmentManagerAccountBalanceDateCache {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.DELETE;
	}


	@Override
	public Integer getInvestmentManagerAccountBalanceId(Integer managerAccountId, Date balanceDate) {
		return getCacheHandler().get(getCacheName(), getBeanKeyForManagerAccountAndDate(managerAccountId, balanceDate));
	}


	@Override
	public void storeInvestmentManagerAccountBalanceId(InvestmentManagerAccountBalance balance) {
		getCacheHandler().put(getCacheName(), getBeanKeyForManagerAccountAndDate(balance.getManagerAccount().getId(), balance.getBalanceDate()), balance.getId());
	}


	private String getBeanKeyForManagerAccountAndDate(int managerAccountId, Date date) {
		return managerAccountId + "_" + DateUtils.fromDateShort(date);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentManagerAccountBalance> dao, @SuppressWarnings("unused") DaoEventTypes event,
	                                InvestmentManagerAccountBalance bean, Throwable e) {
		if (e == null) {
			getCacheHandler().remove(getCacheName(), getBeanKeyForManagerAccountAndDate(bean.getManagerAccount().getId(), bean.getBalanceDate()));
		}
	}
}
