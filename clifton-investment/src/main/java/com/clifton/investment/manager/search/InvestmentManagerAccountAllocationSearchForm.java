package com.clifton.investment.manager.search;


import com.clifton.core.dataaccess.search.SearchField;


/**
 * The <code>InvestmentManagerAccountAllocationSearchForm</code> class defines search configuration for
 * InvestmentManagerAccountAllocation objects.
 *
 * @author vgomelsky
 */
public class InvestmentManagerAccountAllocationSearchForm extends BaseInvestmentManagerAccountAllocationSearchForm {

	@SearchField(searchField = "assetClass.id")
	private Short assetClassId;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Short getAssetClassId() {
		return this.assetClassId;
	}


	public void setAssetClassId(Short assetClassId) {
		this.assetClassId = assetClassId;
	}
}
