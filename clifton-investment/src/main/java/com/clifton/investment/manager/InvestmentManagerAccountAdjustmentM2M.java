package com.clifton.investment.manager;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;


/**
 * The <code>InvestmentManagerAccountAdjustmentM2M</code> class holds the information necessary to calculate
 * mark to market adjustments on specific manager accounts.  The user may specify a start date from which to begin
 * aggregating M2M calculations, or they may specify number of business days.
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountAdjustmentM2M extends ManyToManyEntity<InvestmentManagerAccount, InvestmentAccount, Integer> {

	public enum ManagerM2MAdjustmentTypes {

		// Futures Only
		// BECAUSE FOREIGN FUTURES INCLUDE CURRENCY M2M WITHIN IT - NEED TO CALCULATE OFF OF LOCAL AND USE FX RATE TO GET FOREIGN FUTURES WITHOUT THE CURRENCY M2M
		// NO AFFECT ON THE DOMESTIC FUTURES, BECAUSE THE FX RATE WILL BE 1
		// ((OTE Local + Realized Local - Previous OTE Local - Commission Local) * FX Rate)
		GAIN_LOSS_FUTURES_ONLY("Gain/Loss (Futures Only)", true, false), //
		GAIN_LOSS_FUTURES_ONLY_EXCLUDE_HOLIDAYS("Gain/Loss (Futures Only) - Excludes Holidays", true, true),
		// Actual M2M transfer amount that was entered for that account on given date(s) - if reconciled, else uses OurMarkAmount
		TRANSFERRED_M2M("Actual M2M Transfer Amount", false, false), //
		TRANSFERRED_M2M_EXCLUDE_HOLIDAYS("Actual M2M Transfer Amount", false, true), //
		// Our Calculated M2M Value
		OUR_M2M("Our M2M Amount", false, false), //
		OUR_M2M_EXCLUDE_HOLIDAYS("Our M2M Amount", false, true),
		;


		ManagerM2MAdjustmentTypes(String label, boolean gainLoss, boolean excludeHoliday) {
			this.label = label;
			this.gainLoss = gainLoss;
			this.excludeHoliday = excludeHoliday;
		}


		private final String label;
		// Used to note if it's calculated g/l or pulling precalculated m2m values
		private final boolean gainLoss;
		private final boolean excludeHoliday;


		public String getLabel() {
			return this.label;
		}


		public boolean isExcludeHoliday() {
			return this.excludeHoliday;
		}


		public boolean isGainLoss() {
			return this.gainLoss;
		}
	}

	private ManagerM2MAdjustmentTypes adjustmentType;
	private Integer daysOfM2M;
	private Date startDate;


	public String getAdjustmentTypeLabel() {
		if (getAdjustmentType() != null) {
			return getAdjustmentType().getLabel();
		}
		return null;
	}


	public Integer getDaysOfM2M() {
		return this.daysOfM2M;
	}


	public void setDaysOfM2M(Integer daysOfM2M) {
		this.daysOfM2M = daysOfM2M;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public ManagerM2MAdjustmentTypes getAdjustmentType() {
		return this.adjustmentType;
	}


	public void setAdjustmentType(ManagerM2MAdjustmentTypes adjustmentType) {
		this.adjustmentType = adjustmentType;
	}
}
