package com.clifton.investment.manager;


/**
 * The <code>ManagerCustomCashOverlayTypes</code> defines how the system should overlay manager cash for customized rules and allows
 * for overlaying cash to specific asset classes, cash types, and percentages or amounts
 *
 * @author manderson
 */
public enum ManagerCustomCashOverlayTypes {

	NONE, // No custom rules used
	CUSTOM_PERCENT(true), // Overlay Cash using specified percentages, anything left over is ignored
	CUSTOM_AMOUNT(false); // Overlay Cash using specified amounts, anything left over is ignored


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, then custom rules are applied
	 */
	private final boolean applyCustomRules;


	/**
	 * If true, then values supplied are percentages, else specific amounts
	 */
	private final boolean applyValuesAsPercentage;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	ManagerCustomCashOverlayTypes() {
		this(false, false);
	}


	ManagerCustomCashOverlayTypes(boolean applyValuesAsPercentage) {
		this(true, applyValuesAsPercentage);
	}


	ManagerCustomCashOverlayTypes(boolean applyCustomRules, boolean applyValuesAsPercentage) {
		this.applyCustomRules = applyCustomRules;
		this.applyValuesAsPercentage = applyValuesAsPercentage;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isApplyCustomRules() {
		return this.applyCustomRules;
	}


	public boolean isApplyValuesAsPercentage() {
		return this.applyValuesAsPercentage;
	}
}
