package com.clifton.investment.manager;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceServiceImpl;
import com.clifton.investment.manager.search.InvestmentManagerAccountAllocationSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountAssignmentOverlayActionSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountGroupSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountRollupSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountService</code> ...
 *
 * @author Mary Anderson
 */
public interface InvestmentManagerAccountService {

	//////////////////////////////////////////////////////////////////////////////
	////////            Investment Manager Account  Methods               ////////
	//// ALL ACCOUNT METHODS ALSO GET/SAVE/DELETE ASSOCIATED ROLLUP MANAGERS! ////
	/////  ALL ACCOUNT METHODS ALSO GET/SAVE/DELETE ASSOCIATED M2M ADJUSTS! //////
	//////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccount getInvestmentManagerAccount(int id);


	public List<InvestmentManagerAccount> getInvestmentManagerAccountList(InvestmentManagerAccountSearchForm searchForm);


	/**
	 * Called from {@link InvestmentManagerAccountBalanceServiceImpl} to find all manager account missing balances on a given day.
	 */
	@DoNotAddRequestMapping
	public List<InvestmentManagerAccount> getInvestmentManagerAccountListBySearchConfig(HibernateSearchFormConfigurer searchConfig);


	public InvestmentManagerAccount saveInvestmentManagerAccount(InvestmentManagerAccount bean);


	/**
	 * Updates manager account to inactive, as well as an assignments for the manager account
	 * Used by batch job to de-activate all manager accounts for terminated clients so we don't continue to process them
	 */
	@DoNotAddRequestMapping
	public InvestmentManagerAccount saveInvestmentManagerAccountInactive(InvestmentManagerAccount managerAccount);


	/**
	 * Special save method for uploads to handle auto-generated manager account numbers
	 */
	@DoNotAddRequestMapping
	public InvestmentManagerAccount saveInvestmentManagerAccountFromUpload(InvestmentManagerAccount bean);


	/**
	 * Used during Rollup Manager save to verify that a rollup manager does not have any children that also depend on this manager. i.e. Rollups of Rollups
	 * Also used during Linked Manager loads for Linked Manager Types that have the option to select a managerChanged to Public so Linked Managers that have a dependency on Managers (through custom fields) can verify circular dependencies before trying to load and getting stuck in an infinite loop
	 */
	@DoNotAddRequestMapping
	public void validateInvestmentManagerAccountRollupDependency(InvestmentManagerAccount parent, List<InvestmentManagerAccountRollup> rollupList);


	public void deleteInvestmentManagerAccount(int id);


	////////////////////////////////////////////////////////////////////////////
	/////        Investment Manager Account Assignment Methods            //////
	//// ALL ASSIGNMENT METHODS ALSO GET/SAVE/DELETE ASSOCIATED ALLOCATIONS!////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountAssignment getInvestmentManagerAccountAssignment(int id);


	public InvestmentManagerAccountAssignment getInvestmentManagerAccountAssignmentByManagerAndAccount(int managerAccountId, int accountId);


	public List<InvestmentManagerAccountAssignment> getInvestmentManagerAccountAssignmentListByManager(int managerAccountId, Boolean active);


	public List<InvestmentManagerAccountAssignment> getInvestmentManagerAccountAssignmentListByAccount(int accountId, Boolean active);


	public InvestmentManagerAccountAssignment saveInvestmentManagerAccountAssignment(InvestmentManagerAccountAssignment managerAccountAssignment);


	public void saveInvestmentManagerAccountAssignmentList(List<InvestmentManagerAccountAssignment> managerAccountAssignmentList);


	public void deleteInvestmentManagerAccountAssignment(int id);


	////////////////////////////////////////////////////////////////////////////
	//////       Investment Manager Account Allocation Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentManagerAccountAllocation> getInvestmentManagerAccountAllocationList(InvestmentManagerAccountAllocationSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////  Investment Manager Account Assignment Overlay Action  Methods  ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountAssignmentOverlayAction getInvestmentManagerAccountAssignmentOverlayAction(int id);


	public List<InvestmentManagerAccountAssignmentOverlayAction> getInvestmentManagerAccountAssignmentOverlayActionList(InvestmentManagerAccountAssignmentOverlayActionSearchForm searchForm);


	public InvestmentManagerAccountAssignmentOverlayAction saveInvestmentManagerAccountAssignmentOverlayAction(InvestmentManagerAccountAssignmentOverlayAction bean);


	public void deleteInvestmentManagerAccountAssignmentOverlayAction(int id);


	////////////////////////////////////////////////////////////////////////////
	///////           Investment Manager Account Rollup Methods           //////
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentManagerAccountRollup> getInvestmentManagerAccountRollupListByParent(int parentId);


	public List<InvestmentManagerAccountRollup> getInvestmentManagerAccountRollupListByChild(int childId);


	public List<InvestmentManagerAccountRollup> getInvestmentManagerAccountRollupList(InvestmentManagerAccountRollupSearchForm searchForm);


	@SecureMethod(dtoClass = InvestmentManagerAccountRollup.class)
	public void saveInvestmentManagerAccountRollupPercentageList(Integer[] rollupIds, BigDecimal allocationPercent);


	////////////////////////////////////////////////////////////////////////////
	///////           Investment Manager Account Group Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountGroup getInvestmentManagerAccountGroup(int id);


	public List<InvestmentManagerAccountGroup> getInvestmentManagerAccountGroupListByInvestmentAccount(int investmentAccountId, boolean includeInactive);


	public List<InvestmentManagerAccountGroup> getInvestmentManagerAccountGroupList(InvestmentManagerAccountGroupSearchForm searchForm);


	public InvestmentManagerAccountGroup saveInvestmentManagerAccountGroup(InvestmentManagerAccountGroup bean);


	public void deleteInvestmentManagerAccountGroup(int id);
}
