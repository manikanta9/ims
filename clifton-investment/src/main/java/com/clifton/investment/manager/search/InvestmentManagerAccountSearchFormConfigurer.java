package com.clifton.investment.manager.search;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientService;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;

import java.util.Date;


/**
 * The <code>InvestmentManagerAccountSearchFormConfigurer</code> ...
 *
 * @author manderson
 */
public class InvestmentManagerAccountSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final InvestmentManagerAccountSearchForm investmentManagerAccountSearchForm;

	private final BusinessClientService businessClientService;


	public InvestmentManagerAccountSearchFormConfigurer(InvestmentManagerAccountSearchForm searchForm, BusinessClientService businessClientService) {
		super(searchForm);
		this.investmentManagerAccountSearchForm = searchForm;
		this.businessClientService = businessClientService;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		InvestmentManagerAccountSearchForm searchForm = this.investmentManagerAccountSearchForm;

		if (searchForm.getInvestmentAccountGroupId() != null) {
			DetachedCriteria exists = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "ga");
			exists.setProjection(Projections.property("id"));
			exists.createAlias("ga.referenceOne", "g");
			exists.createAlias("ga.referenceTwo", "a");
			exists.createAlias("a.businessClient", "bc");
			exists.add(Restrictions.eqProperty("bc.id", getPathAlias("client", criteria) + ".id"));
			exists.add(Restrictions.eq("g.id", searchForm.getInvestmentAccountGroupId()));
			criteria.add(Subqueries.exists(exists));
		}

		if (searchForm.getClientActive() != null) {
			if (searchForm.getClientActive()) {
				LogicalExpression startDateFilter = Restrictions.or(Restrictions.isNull(getPathAlias("client", criteria) + ".inceptionDate"),
						Restrictions.le(getPathAlias("client", criteria) + ".inceptionDate", new Date()));
				LogicalExpression endDateFilter = Restrictions.or(Restrictions.isNull(getPathAlias("client", criteria) + ".terminateDate"),
						Restrictions.ge(getPathAlias("client", criteria) + ".terminateDate", new Date()));
				criteria.add(Restrictions.and(startDateFilter, endDateFilter));
			}
			else {
				criteria.add(Restrictions.or(Restrictions.gt(getPathAlias("client", criteria) + ".inceptionDate", new Date()),
						Restrictions.lt(getPathAlias("client", criteria) + ".terminateDate", new Date())));
			}
		}

		if (searchForm.getExcludeClientAccountWorkflowStateId() != null) {
			// not exists investment account that this manager account is assigned to in FILTER (for example, "Active") workflow state
			DetachedCriteria notExists = DetachedCriteria.forClass(InvestmentManagerAccountAssignment.class, "aa");
			notExists.setProjection(Projections.property("id"));
			notExists.createAlias("aa.referenceOne", "ima");
			notExists.createAlias("aa.referenceTwo", "ia");
			notExists.add(Restrictions.eqProperty("ima.id", criteria.getAlias() + ".id"));
			notExists.add(Restrictions.eq("ia.workflowState.id", searchForm.getExcludeClientAccountWorkflowStateId()));
			criteria.add(Subqueries.notExists(notExists));
		}

		if (searchForm.getClientIdOrRelatedClient() != null) {
			// Look up the Client to See if it is a Child or a Parent
			BusinessClient client = this.businessClientService.getBusinessClient(searchForm.getClientIdOrRelatedClient());
			if (client != null) {
				// Exact Client Match
				SimpleExpression clientRestriction = Restrictions.eq("client.id", searchForm.getClientIdOrRelatedClient());

				// If Client has a parent, then include where client id = client's parent id
				if (client.getParent() != null) {
					criteria.add(Restrictions.or(clientRestriction, Restrictions.eq("client.id", client.getParent().getId())));
				}
				// Otherwise if it is a parent client, include where client.parent.id = id 
				else if (client.getCategory().isChildrenAllowed()) {
					String p1 = getPathAlias("client.parent", criteria, JoinType.LEFT_OUTER_JOIN);
					criteria.add(Restrictions.or(clientRestriction, Restrictions.eq(p1 + ".id", client.getId())));
				}
				// Else - there are no related clients - just filter on the client id field
				else {
					criteria.add(clientRestriction);
				}
			}
		}
	}
}
