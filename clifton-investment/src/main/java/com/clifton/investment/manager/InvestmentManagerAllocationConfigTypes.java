package com.clifton.investment.manager;

/**
 * @author manderson
 */
public enum InvestmentManagerAllocationConfigTypes {

	CASH_ALLOCATION,
	OVERLAY_ALLOCATION,
	SECURITIES_ALLOCATION
}
