package com.clifton.investment.manager.balance.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.util.BooleanUtils;
import com.clifton.investment.manager.InvestmentManagerAccount.ManagerTypes;
import com.clifton.investment.manager.LinkedManagerTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentManagerAccountBalanceSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountBalanceSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "managerAccount.id")
	private Integer managerAccountId;

	@SearchField(searchField = "managerAccount.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] managerAccountIds;

	@SearchField
	private Date balanceDate;

	@SearchField(searchFieldPath = "managerAccount", searchField = "managerCompany.id")
	private Integer managerCompanyId;

	@SearchField(searchFieldPath = "managerAccount.managerCompany", searchField = "name")
	private String managerCompanyName;

	@SearchField(searchFieldPath = "managerAccount", searchField = "custodianAccount.id")
	private Integer custodianAccountId;

	@SearchField(searchFieldPath = "managerAccount.custodianAccount", searchField = "number")
	private String custodianAccountNumber;

	@SearchField(searchFieldPath = "managerAccount.custodianAccount", searchField = "company.id")
	private Integer custodianCompanyId;

	@SearchField(searchFieldPath = "managerAccount.custodianAccount.company", searchField = "name")
	private String custodianCompanyName;

	@SearchField(searchFieldPath = "managerAccount", searchField = "accountNumber,accountName", sortField = "accountNumber")
	private String accountNameNumber;

	@SearchField(searchFieldPath = "managerAccount", searchField = "client.id")
	private Integer clientId;

	@SearchField(searchFieldPath = "managerAccount", searchField = "baseCurrency.id", sortField = "baseCurrency.symbol")
	private Integer baseCurrencyId;

	@SearchField(searchField = "violationStatus.id")
	private Short violationStatusId;

	@SearchField(searchField = "violationStatus.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] violationStatusIds;

	@SearchField
	private BigDecimal cashValue;

	@SearchField
	private BigDecimal securitiesValue;

	@SearchField
	private BigDecimal adjustedCashValue;

	@SearchField
	private BigDecimal adjustedSecuritiesValue;

	@SearchField(searchFieldPath = "violationStatus", searchField = "name")
	private String[] violationStatusNames;

	// CUSTOM FILTERS SET VALUES BELOW
	private Boolean proxyManager;
	private Boolean rollupManager;
	private Boolean linkedManager;

	@SearchField(searchFieldPath = "managerAccount", searchField = "managerType", comparisonConditions = {ComparisonConditions.EQUALS})
	private ManagerTypes managerType;

	@SearchField(searchFieldPath = "managerAccount", searchField = "linkedManagerType", comparisonConditions = {ComparisonConditions.EQUALS})
	private LinkedManagerTypes linkedManagerType;

	@SearchField(searchFieldPath = "managerAccount", searchField = "managerType", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private ManagerTypes excludeProxyManagers;

	@SearchField(searchFieldPath = "managerAccount", searchField = "managerType", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private ManagerTypes excludeRollupManagers;

	@SearchField(searchFieldPath = "managerAccount", searchField = "managerType", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private ManagerTypes excludeLinkedManagers;

	// custom filter that needs to take into account manager account assignments
	private Integer clientInvestmentAccountGroupId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	@Override
	public void validate() {
		// when Rule Violation Status filter is used, at least one more other filter must be specified
		SearchUtils.validateRequiredFilter(this, "violationStatusId", "violationStatusIds", "violationStatusNames");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ManagerTypes getManagerType() {
		if (this.managerType == null) {
			if (BooleanUtils.isTrue(getProxyManager())) {
				return ManagerTypes.PROXY;
			}
			if (BooleanUtils.isTrue(getRollupManager())) {
				return ManagerTypes.ROLLUP;
			}
			if (BooleanUtils.isTrue(getLinkedManager())) {
				return ManagerTypes.LINKED;
			}
		}
		return this.managerType;
	}


	public void setManagerType(ManagerTypes managerType) {
		this.managerType = managerType;
	}


	public ManagerTypes getExcludeProxyManagers() {
		if (this.excludeProxyManagers == null) {
			if (getProxyManager() != null && !getProxyManager()) {
				return ManagerTypes.PROXY;
			}
		}
		return this.excludeProxyManagers;
	}


	public ManagerTypes getExcludeRollupManagers() {
		if (this.excludeRollupManagers == null) {
			if (getRollupManager() != null && !getRollupManager()) {
				return ManagerTypes.ROLLUP;
			}
		}
		return this.excludeRollupManagers;
	}


	public ManagerTypes getExcludeLinkedManagers() {
		if (this.excludeLinkedManagers == null) {
			if (getLinkedManager() != null && !getLinkedManager()) {
				return ManagerTypes.LINKED;
			}
		}
		return this.excludeLinkedManagers;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getManagerAccountId() {
		return this.managerAccountId;
	}


	public void setManagerAccountId(Integer managerAccountId) {
		this.managerAccountId = managerAccountId;
	}


	public Integer[] getManagerAccountIds() {
		return this.managerAccountIds;
	}


	public void setManagerAccountIds(Integer[] managerAccountIds) {
		this.managerAccountIds = managerAccountIds;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public String getManagerCompanyName() {
		return this.managerCompanyName;
	}


	public void setManagerCompanyName(String managerCompanyName) {
		this.managerCompanyName = managerCompanyName;
	}


	public String getAccountNameNumber() {
		return this.accountNameNumber;
	}


	public void setAccountNameNumber(String accountNameNumber) {
		this.accountNameNumber = accountNameNumber;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Integer getManagerCompanyId() {
		return this.managerCompanyId;
	}


	public void setManagerCompanyId(Integer managerCompanyId) {
		this.managerCompanyId = managerCompanyId;
	}


	public Integer getCustodianCompanyId() {
		return this.custodianCompanyId;
	}


	public void setCustodianCompanyId(Integer custodianCompanyId) {
		this.custodianCompanyId = custodianCompanyId;
	}


	public String getCustodianCompanyName() {
		return this.custodianCompanyName;
	}


	public void setCustodianCompanyName(String custodianCompanyName) {
		this.custodianCompanyName = custodianCompanyName;
	}


	public Boolean getProxyManager() {
		return this.proxyManager;
	}


	public void setProxyManager(Boolean proxyManager) {
		this.proxyManager = proxyManager;
	}


	public Boolean getRollupManager() {
		return this.rollupManager;
	}


	public void setRollupManager(Boolean rollupManager) {
		this.rollupManager = rollupManager;
	}


	public Boolean getLinkedManager() {
		return this.linkedManager;
	}


	public void setLinkedManager(Boolean linkedManager) {
		this.linkedManager = linkedManager;
	}


	public LinkedManagerTypes getLinkedManagerType() {
		return this.linkedManagerType;
	}


	public void setLinkedManagerType(LinkedManagerTypes linkedManagerType) {
		this.linkedManagerType = linkedManagerType;
	}


	public Short getViolationStatusId() {
		return this.violationStatusId;
	}


	public void setViolationStatusId(Short violationStatusId) {
		this.violationStatusId = violationStatusId;
	}


	public Short[] getViolationStatusIds() {
		return this.violationStatusIds;
	}


	public void setViolationStatusIds(Short[] violationStatusIds) {
		this.violationStatusIds = violationStatusIds;
	}


	public String[] getViolationStatusNames() {
		return this.violationStatusNames;
	}


	public void setViolationStatusNames(String[] violationStatusNames) {
		this.violationStatusNames = violationStatusNames;
	}


	public BigDecimal getCashValue() {
		return this.cashValue;
	}


	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}


	public BigDecimal getSecuritiesValue() {
		return this.securitiesValue;
	}


	public void setSecuritiesValue(BigDecimal securitiesValue) {
		this.securitiesValue = securitiesValue;
	}


	public BigDecimal getAdjustedCashValue() {
		return this.adjustedCashValue;
	}


	public void setAdjustedCashValue(BigDecimal adjustedCashValue) {
		this.adjustedCashValue = adjustedCashValue;
	}


	public BigDecimal getAdjustedSecuritiesValue() {
		return this.adjustedSecuritiesValue;
	}


	public void setAdjustedSecuritiesValue(BigDecimal adjustedSecuritiesValue) {
		this.adjustedSecuritiesValue = adjustedSecuritiesValue;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public String getCustodianAccountNumber() {
		return this.custodianAccountNumber;
	}


	public void setCustodianAccountNumber(String custodianAccountNumber) {
		this.custodianAccountNumber = custodianAccountNumber;
	}


	public Integer getCustodianAccountId() {
		return this.custodianAccountId;
	}


	public void setCustodianAccountId(Integer custodianAccountId) {
		this.custodianAccountId = custodianAccountId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public void setExcludeProxyManagers(ManagerTypes excludeProxyManagers) {
		this.excludeProxyManagers = excludeProxyManagers;
	}


	public void setExcludeRollupManagers(ManagerTypes excludeRollupManagers) {
		this.excludeRollupManagers = excludeRollupManagers;
	}


	public void setExcludeLinkedManagers(ManagerTypes excludeLinkedManagers) {
		this.excludeLinkedManagers = excludeLinkedManagers;
	}
}
