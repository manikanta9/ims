package com.clifton.investment.manager;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentManagerAccountAssignmentOverlayAction</code> defines overlay cash actions
 * that should be taken for a specific manager assignment when overlaying cash for a date range
 * e.g. Adjust Cash to Overlay, Limit Cash to Overlay
 *
 * @author manderson
 */
public class InvestmentManagerAccountAssignmentOverlayAction extends BaseEntity<Integer> {

	private InvestmentManagerAccountAssignment managerAccountAssignment;

	private ManagerCashOverlayActionTypes overlayActionType;

	private BigDecimal overlayActionAmount;

	/**
	 * Enter a percent of total market value and the system will calculate the value daily based on total market value
	 * for the account (PIOS - Manager Allocations Tab)
	 * <p/>
	 * For Adjustments, Instead of entering a static value, can enter a percentage (mutually exclusive) of total market value and the system will calculate the value daily based on total market value
	 * for the account (PIOS - Manager Allocations Tab)
	 * <p/>
	 * For Limits, Can be entered in addition to amount, where the amount is the absolute limit, and percentage is the percentage of the ManagerAllocation total (NOTE: NOT TMV like for adjustments)
	 * 1. If percent value is higher than amount, value is capped at the amount entered
	 * 2. If percent value is less than amount, value is capped at percent value
	 */
	private BigDecimal overlayActionPercent;

	private boolean applyDifferenceToSecurities;

	private Date startDate;
	private Date endDate;


	//////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	//////////////////////////////////////////////


	public InvestmentManagerAccountAssignment getManagerAccountAssignment() {
		return this.managerAccountAssignment;
	}


	public void setManagerAccountAssignment(InvestmentManagerAccountAssignment managerAccountAssignment) {
		this.managerAccountAssignment = managerAccountAssignment;
	}


	public ManagerCashOverlayActionTypes getOverlayActionType() {
		return this.overlayActionType;
	}


	public void setOverlayActionType(ManagerCashOverlayActionTypes overlayActionType) {
		this.overlayActionType = overlayActionType;
	}


	public BigDecimal getOverlayActionAmount() {
		return this.overlayActionAmount;
	}


	public void setOverlayActionAmount(BigDecimal overlayActionAmount) {
		this.overlayActionAmount = overlayActionAmount;
	}


	public BigDecimal getOverlayActionPercent() {
		return this.overlayActionPercent;
	}


	public void setOverlayActionPercent(BigDecimal overlayActionPercent) {
		this.overlayActionPercent = overlayActionPercent;
	}


	public boolean isApplyDifferenceToSecurities() {
		return this.applyDifferenceToSecurities;
	}


	public void setApplyDifferenceToSecurities(boolean applyDifferenceToSecurities) {
		this.applyDifferenceToSecurities = applyDifferenceToSecurities;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
