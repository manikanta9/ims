package com.clifton.investment.manager.balance.stale;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseSimpleEntity;

import java.util.Date;


public class InvestmentManagerAccountBalanceStaleSummary extends BaseSimpleEntity<String> {

	private String uuid; // PK field: not used but needed by Hibernate

	private BusinessCompany custodianCompany;
	private Date balanceDate;

	/**
	 * The number of stale accounts on the balance date.
	 */
	private int staleCount;

	/**
	 * The average number of stale accounts on the balance date.
	 */
	private int averageStaleCount;


	public BusinessCompany getCustodianCompany() {
		return this.custodianCompany;
	}


	public void setCustodianCompany(BusinessCompany custodianCompany) {
		this.custodianCompany = custodianCompany;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public int getStaleCount() {
		return this.staleCount;
	}


	public void setStaleCount(int staleCount) {
		this.staleCount = staleCount;
	}


	public int getAverageStaleCount() {
		return this.averageStaleCount;
	}


	public void setAverageStaleCount(int averageStaleCount) {
		this.averageStaleCount = averageStaleCount;
	}


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
