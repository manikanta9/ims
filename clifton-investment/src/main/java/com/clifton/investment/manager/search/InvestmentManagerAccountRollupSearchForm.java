package com.clifton.investment.manager.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;


/**
 * The <code>InvestmentManagerAccountRollupSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountRollupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchFieldPath = "referenceOne", searchField = "client.id")
	private Integer clientId;

	@SearchField(searchFieldPath = "referenceOne", searchField = "accountNumber,accountName", sortField = "accountNumber")
	private String parentManagerAccountNumberName;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "accountNumber,accountName", sortField = "accountNumber")
	private String childManagerAccountNumberName;

	@SearchField(searchFieldPath = "referenceOne", searchField = "inactive")
	private Boolean inactive;

	@SearchField(searchFieldCustomType = SearchFieldCustomTypes.OR, searchField = "referenceOne.id,referenceTwo.id")
	private Integer parentOrChildManagerAccountId;

	@SearchField
	private BigDecimal allocationPercent;


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public BigDecimal getAllocationPercent() {
		return this.allocationPercent;
	}


	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}


	public String getParentManagerAccountNumberName() {
		return this.parentManagerAccountNumberName;
	}


	public void setParentManagerAccountNumberName(String parentManagerAccountNumberName) {
		this.parentManagerAccountNumberName = parentManagerAccountNumberName;
	}


	public String getChildManagerAccountNumberName() {
		return this.childManagerAccountNumberName;
	}


	public void setChildManagerAccountNumberName(String childManagerAccountNumberName) {
		this.childManagerAccountNumberName = childManagerAccountNumberName;
	}


	public Integer getParentOrChildManagerAccountId() {
		return this.parentOrChildManagerAccountId;
	}


	public void setParentOrChildManagerAccountId(Integer parentOrChildManagerAccountId) {
		this.parentOrChildManagerAccountId = parentOrChildManagerAccountId;
	}
}
