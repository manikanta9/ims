package com.clifton.investment.manager.cache;

import com.clifton.investment.manager.InvestmentManagerAccountRollup;

import java.util.List;


/**
 * The <code>InvestmentManagerAccountRollupCacheImpl</code> caches
 * rollup relationships for a manager as either the parent or the child.
 * <p>
 * Cache stores ID values of the {@link InvestmentManagerAccountRollup} and when retrieved looks up by id values
 * <p>
 * Calling methods can then filter out on parent or child = manager account id to get the list of parents or the list of children
 * <p>
 * Cache is cleared for parent and child when rollup is updated
 * <p>
 * NOTE: This cache is pretty big, because stores empty lists for managers that are neither a parent or a child.
 * so that we don't attempt to do look ups again each time
 *
 * @author Mary Anderson
 */
public interface InvestmentManagerAccountRollupCache {

	public Integer[] getInvestmentManagerAccountRollupList(int managerAccountId);


	public void storeInvestmentManagerAccountRollupList(int managerAccountId, List<InvestmentManagerAccountRollup> list);
}
