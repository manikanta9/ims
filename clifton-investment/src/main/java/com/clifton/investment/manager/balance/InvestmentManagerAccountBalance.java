package com.clifton.investment.manager.balance;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.rule.violation.RuleViolationAware;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountBalance</code> class holds daily balances for manager accounts.
 *
 * @author vgomelsky
 */
public class InvestmentManagerAccountBalance extends BaseEntity<Integer> implements LabeledObject, RuleViolationAware {

	private InvestmentManagerAccount managerAccount;
	private Date balanceDate;

	private BigDecimal cashValue;
	private BigDecimal securitiesValue;

	// adjusted amounts are same as non-adjusted when there are no adjustments
	// adjustment details are captured by InvestmentManagerAccountBalanceAdjustment(s) and may include m2m, manual overrides, etc.
	// this does NOT include MOC adjustments
	private BigDecimal adjustedCashValue;
	private BigDecimal adjustedSecuritiesValue;

	// adjusted amounts are same as non-adjusted when there are no adjustments
	// MOC Balances = Adjusted Value +/- MOC Adjustments.  If no MOC Adjustments
	// then the MOC value is the same as the adjusted value.
	private BigDecimal marketOnCloseCashValue;
	private BigDecimal marketOnCloseSecuritiesValue;

	private RuleViolationStatus violationStatus;
	private String note;

	private List<InvestmentManagerAccountBalanceAdjustment> adjustmentList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalance() {
		// default constructor
	}


	public InvestmentManagerAccountBalance(InvestmentManagerAccount managerAccount, Date balanceDate) {
		this.managerAccount = managerAccount;
		this.balanceDate = balanceDate;
		// Set Zero Values to start (required fields, but each type may only set cash or securities)
		this.cashValue = BigDecimal.ZERO;
		this.securitiesValue = BigDecimal.ZERO;
	}


	@Override
	public String getLabel() {
		if (getManagerAccount() != null && getBalanceDate() != null) {
			return getManagerAccount().getLabel() + " (" + DateUtils.fromDate(getBalanceDate(), DateUtils.DATE_FORMAT_INPUT) + ") ";
		}
		return null;
	}


	public boolean isAdjusted() {
		return isCashAdjustment() || isSecuritiesAdjustment() || isMocAdjustment();
	}


	public boolean isCashAdjustment() {
		if (getAdjustedCashValue() == null) {
			return false;
		}
		return !(getAdjustedCashValue().equals(getCashValue()));
	}


	public boolean isSecuritiesAdjustment() {
		if (getAdjustedSecuritiesValue() == null) {
			return false;
		}
		return !(getAdjustedSecuritiesValue().equals(getSecuritiesValue()));
	}


	public boolean isMocAdjustment() {
		if (getMarketOnCloseTotalValue() == null) {
			return false;
		}
		return !(getMarketOnCloseCashValue().equals(getAdjustedCashValue()) && getMarketOnCloseSecuritiesValue().equals(getAdjustedSecuritiesValue()));
	}


	public BigDecimal getTotalValue() {
		if (this.cashValue != null) {
			return (this.securitiesValue == null) ? this.cashValue : this.cashValue.add(this.securitiesValue);
		}
		return this.securitiesValue;
	}


	public BigDecimal getAdjustedTotalValue() {
		if (this.adjustedCashValue != null) {
			return (this.adjustedSecuritiesValue == null) ? this.adjustedCashValue : this.adjustedCashValue.add(this.adjustedSecuritiesValue);
		}
		return this.adjustedSecuritiesValue;
	}


	public BigDecimal getMarketOnCloseTotalValue() {
		if (this.marketOnCloseCashValue != null) {
			return (this.marketOnCloseSecuritiesValue == null) ? this.marketOnCloseCashValue : this.marketOnCloseCashValue.add(this.marketOnCloseSecuritiesValue);
		}
		return this.marketOnCloseSecuritiesValue;
	}


	public void addAdjustment(InvestmentManagerAccountBalanceAdjustment adjustment) {
		if (getAdjustmentList() == null) {
			this.adjustmentList = new ArrayList<>();
		}
		this.adjustmentList.add(adjustment);
	}


	public void recalculateAdjustedValues() {
		// Start with original values, then adjust as necessary
		this.adjustedCashValue = this.cashValue;
		this.adjustedSecuritiesValue = this.securitiesValue;
		this.marketOnCloseCashValue = this.cashValue;
		this.marketOnCloseSecuritiesValue = this.securitiesValue;

		for (InvestmentManagerAccountBalanceAdjustment adj : CollectionUtils.getIterable(getAdjustmentList())) {
			if (!MathUtils.isNullOrZero(adj.getCashValue())) {
				if (!adj.getAdjustmentType().isMarketOnClose()) {
					this.adjustedCashValue = MathUtils.add(this.adjustedCashValue, adj.getCashValue());
				}
				this.marketOnCloseCashValue = MathUtils.add(this.marketOnCloseCashValue, adj.getCashValue());
			}
			if (!MathUtils.isNullOrZero(adj.getSecuritiesValue())) {
				if (!adj.getAdjustmentType().isMarketOnClose()) {
					this.adjustedSecuritiesValue = MathUtils.add(this.adjustedSecuritiesValue, adj.getSecuritiesValue());
				}
				this.marketOnCloseSecuritiesValue = MathUtils.add(this.marketOnCloseSecuritiesValue, adj.getSecuritiesValue());
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccount getManagerAccount() {
		return this.managerAccount;
	}


	public void setManagerAccount(InvestmentManagerAccount managerAccount) {
		this.managerAccount = managerAccount;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public BigDecimal getCashValue() {
		return this.cashValue;
	}


	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}


	public BigDecimal getSecuritiesValue() {
		return this.securitiesValue;
	}


	public void setSecuritiesValue(BigDecimal securitiesValue) {
		this.securitiesValue = securitiesValue;
	}


	public BigDecimal getAdjustedCashValue() {
		return this.adjustedCashValue;
	}


	public void setAdjustedCashValue(BigDecimal adjustedCashValue) {
		this.adjustedCashValue = adjustedCashValue;
	}


	public BigDecimal getAdjustedSecuritiesValue() {
		return this.adjustedSecuritiesValue;
	}


	public void setAdjustedSecuritiesValue(BigDecimal adjustedSecuritiesValue) {
		this.adjustedSecuritiesValue = adjustedSecuritiesValue;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public List<InvestmentManagerAccountBalanceAdjustment> getAdjustmentList() {
		return this.adjustmentList;
	}


	public void setAdjustmentList(List<InvestmentManagerAccountBalanceAdjustment> adjustmentList) {
		this.adjustmentList = adjustmentList;
	}


	public BigDecimal getMarketOnCloseCashValue() {
		return this.marketOnCloseCashValue;
	}


	public void setMarketOnCloseCashValue(BigDecimal marketOnCloseCashValue) {
		this.marketOnCloseCashValue = marketOnCloseCashValue;
	}


	public BigDecimal getMarketOnCloseSecuritiesValue() {
		return this.marketOnCloseSecuritiesValue;
	}


	public void setMarketOnCloseSecuritiesValue(BigDecimal marketOnCloseSecuritiesValue) {
		this.marketOnCloseSecuritiesValue = marketOnCloseSecuritiesValue;
	}


	@Override
	public RuleViolationStatus getViolationStatus() {
		return this.violationStatus;
	}


	@Override
	public void setViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
	}
}
