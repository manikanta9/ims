package com.clifton.investment.manager.upload;

import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentManagerAccountAllocationUploadCommand</code> handles custom uploads for {@link InvestmentManagerAccountAllocationUploadTemplate} objects
 * that are saved as {@link com.clifton.investment.manager.InvestmentManagerAccountAllocation}
 * <p>
 * Note: Upload is really a list of allocations, but we apply them to the manager and save at that level
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountAllocationUploadCommand extends BaseInvestmentManagerAccountAllocationUploadCommand<InvestmentManagerAccountAllocationUploadTemplate> {


	public static final String[] REQUIRED_PROPERTIES = {"clientAccountNumber", "managerAccountNumber", "assetClassName", "allocationPercent"};

	////////////////////////////////////////////////////////////////////////////////

	Map<Integer, List<InvestmentAccountAssetClass>> clientAccountAssetClassListMap;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<InvestmentManagerAccountAllocationUploadTemplate> getUploadBeanClass() {
		return InvestmentManagerAccountAllocationUploadTemplate.class;
	}


	@Override
	public String[] getRequiredProperties() {
		return REQUIRED_PROPERTIES;
	}


	@Override
	public void clearResults() {
		super.clearResults();
		this.clientAccountAssetClassListMap = new HashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public Map<Integer, List<InvestmentAccountAssetClass>> getClientAccountAssetClassListMap() {
		return this.clientAccountAssetClassListMap;
	}
}
