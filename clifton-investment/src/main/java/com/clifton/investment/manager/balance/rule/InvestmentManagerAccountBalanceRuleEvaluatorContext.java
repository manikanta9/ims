package com.clifton.investment.manager.balance.rule;

import com.clifton.rule.evaluator.BaseRuleEvaluatorContext;


/**
 * Used by all Investment Manager Balance Rule evaluation and contains common information (easily stored/retrieved from context) and re-used across various rule evaluations
 *
 * @author stevenf on 7/31/2015.
 */
public class InvestmentManagerAccountBalanceRuleEvaluatorContext extends BaseRuleEvaluatorContext {

	// TODO
}
