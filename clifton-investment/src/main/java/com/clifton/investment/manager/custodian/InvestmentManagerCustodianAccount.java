package com.clifton.investment.manager.custodian;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>InvestmentManagerCustodianAccount</code> class represents a Custodian Account for an external money manager.
 * Some of these accounts will be custodian accounts directly managed by us.  In this case, we can link them to corresponding
 * Holding Accounts.
 */
public class InvestmentManagerCustodianAccount extends BaseEntity<Integer> {

	/**
	 * The base currency to be used with this Custodian Account (Optional)
	 */
	private InvestmentSecurity baseCurrency;

	/**
	 * The Issuing Company of the Custodian Account
	 */
	private BusinessCompany company;

	/**
	 * The Custodian Account Number
	 */
	private String number;

	/**
	 * Link to corresponding Holding Account
	 */
	private InvestmentAccount holdingAccount;

	/**
	 * Specifies whether this account provides "Custody Only" services or is a "Full Service" account.
	 * Full service accounts report assets from other accounts: for example Futures Broker.
	 */
	private boolean custodyOnlyAccount;

	/**
	 * A sweep account is a bank account that automatically transfers amounts that exceed, or fall short of, a certain level into a higher interest-earning
	 * investment option at the close of each business day. Commonly, the excess cash is swept into money market funds.
	 */
	private boolean sweepAccount;

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return getNumber() + " (" + getCompany().getName() + ")";
	}

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getCompany() {
		return this.company;
	}


	public void setCompany(BusinessCompany company) {
		this.company = company;
	}


	public String getNumber() {
		return this.number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public InvestmentSecurity getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(InvestmentSecurity baseCurrency) {
		this.baseCurrency = baseCurrency;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public boolean isCustodyOnlyAccount() {
		return this.custodyOnlyAccount;
	}


	public void setCustodyOnlyAccount(boolean custodyOnlyAccount) {
		this.custodyOnlyAccount = custodyOnlyAccount;
	}


	public boolean isSweepAccount() {
		return this.sweepAccount;
	}


	public void setSweepAccount(boolean sweepAccount) {
		this.sweepAccount = sweepAccount;
	}
}
