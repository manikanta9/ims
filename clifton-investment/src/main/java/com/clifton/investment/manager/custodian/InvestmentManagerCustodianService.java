package com.clifton.investment.manager.custodian;

import com.clifton.investment.manager.custodian.search.InvestmentManagerCustodianAccountSearchForm;

import java.util.List;


/**
 * The <code>InvestmentManagerCustodianService</code> defines methods for interacting
 * with Manager Custodian objects
 */
public interface InvestmentManagerCustodianService {

	/////////////////////////////////////////////////////////////////////////////////////
	//////                InvestmentManagerCustodianAccount Methods                 /////
	/////////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerCustodianAccount getInvestmentManagerCustodianAccount(int id);


	public InvestmentManagerCustodianAccount getInvestmentManagerCustodianAccountByNumberAndCompany(String number, Integer companyId);


	public List<InvestmentManagerCustodianAccount> getInvestmentManagerCustodianAccountList(InvestmentManagerCustodianAccountSearchForm searchForm);


	public InvestmentManagerCustodianAccount saveInvestmentManagerCustodianAccount(InvestmentManagerCustodianAccount bean);


	public void deleteInvestmentManagerCustodianAccount(int id);
}
