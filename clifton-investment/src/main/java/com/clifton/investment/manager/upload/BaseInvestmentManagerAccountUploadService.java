package com.clifton.investment.manager.upload;

import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;


/**
 * @author nickk
 */
public abstract class BaseInvestmentManagerAccountUploadService {

	private InvestmentAccountService investmentAccountService;
	private InvestmentManagerAccountService investmentManagerAccountService;

	private FileUploadHandler fileUploadHandler;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	protected <T extends BaseInvestmentManagerAccountAllocationUploadTemplate> InvestmentAccount getClientAccount(BaseInvestmentManagerAccountAllocationUploadCommand<T> uploadCommand, T uploadTemplate) {
		InvestmentAccount clientAccount = uploadCommand.getClientAccountNumberMap().computeIfAbsent(uploadTemplate.getClientAccountNumber(), clientNumber -> {
			InvestmentAccountSearchForm clientAccountSearchForm = new InvestmentAccountSearchForm();
			clientAccountSearchForm.setOurAccount(true);
			clientAccountSearchForm.setNumberEquals(clientNumber);
			return CollectionUtils.getOnlyElement(getInvestmentAccountService().getInvestmentAccountList(clientAccountSearchForm));
		});
		if (clientAccount == null) {
			uploadCommand.appendResult("Cannot find client account with number [" + uploadTemplate.getClientAccountNumber() + "].");
			return null;
		}
		return clientAccount;
	}


	protected <T extends BaseInvestmentManagerAccountAllocationUploadTemplate> InvestmentManagerAccount getManagerAccount(BaseInvestmentManagerAccountAllocationUploadCommand<T> uploadCommand, T uploadTemplate) {
		InvestmentManagerAccount managerAccount = uploadCommand.getManagerAccountNumberMap().computeIfAbsent(uploadTemplate.getManagerAccountNumber(), managerNumber -> {
			InvestmentManagerAccountSearchForm managerAccountSearchForm = new InvestmentManagerAccountSearchForm();
			managerAccountSearchForm.setAccountNumberEquals(managerNumber);
			return CollectionUtils.getOnlyElement(getInvestmentManagerAccountService().getInvestmentManagerAccountList(managerAccountSearchForm));
		});
		if (managerAccount == null) {
			uploadCommand.appendResult("Cannot find manager account with number [" + uploadTemplate.getManagerAccountNumber() + "].");
			return null;
		}
		return managerAccount;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public FileUploadHandler getFileUploadHandler() {
		return this.fileUploadHandler;
	}


	public void setFileUploadHandler(FileUploadHandler fileUploadHandler) {
		this.fileUploadHandler = fileUploadHandler;
	}
}
