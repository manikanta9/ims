package com.clifton.investment.manager.transaction;

import com.clifton.investment.manager.transaction.search.InvestmentManagerCustodianTransactionSearchForm;

import java.util.List;


public interface InvestmentManagerCustodianTransactionService {

	public InvestmentManagerCustodianTransaction getInvestmentManagerCustodianTransaction(int id);


	public List<InvestmentManagerCustodianTransaction> getInvestmentManagerCustodianTransactionList(InvestmentManagerCustodianTransactionSearchForm searchForm);


	public InvestmentManagerCustodianTransaction saveInvestmentManagerCustodianTransaction(InvestmentManagerCustodianTransaction bean);


	public void deleteInvestmentManagerCustodianTransaction(int id);


	public void deleteInvestmentManagerCustodianTransactionList(List<InvestmentManagerCustodianTransaction> beans);
}
