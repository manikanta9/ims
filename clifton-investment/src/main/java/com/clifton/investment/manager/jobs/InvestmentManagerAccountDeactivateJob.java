package com.clifton.investment.manager.jobs;

import com.clifton.business.client.BusinessClient;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * InvestmentManagerAccountDeactivateJob is used to de-activate manager accounts (and their assignments) in bulk.
 * Used currently by batch job to de-activate manager accounts once the client is terminated
 *
 * @author manderson
 */
public class InvestmentManagerAccountDeactivateJob implements Task {

	private InvestmentManagerAccountService investmentManagerAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		// Find Activate Managers with Terminated Clients
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		searchForm.setInactive(false);
		searchForm.setClientActive(false);
		List<InvestmentManagerAccount> managerAccountList = getInvestmentManagerAccountService().getInvestmentManagerAccountList(searchForm);

		Status status = Status.ofMessage("Starting manager account de-activation: " + CollectionUtils.getSize(managerAccountList));
		status.setActionPerformed(false);

		if (!CollectionUtils.isEmpty(managerAccountList)) {
			List<BusinessClient> clientList = new ArrayList<>();
			int successCount = 0, errorCount = 0;
			for (InvestmentManagerAccount managerAccount : managerAccountList) {
				try {
					getInvestmentManagerAccountService().saveInvestmentManagerAccountInactive(managerAccount);
					successCount++;
					BusinessClient client = managerAccount.getClient();
					if (!clientList.contains(client)) {
						clientList.add(client);
						status.addWarning("Deactivated managers (and their assignments) for " + client.getName() + " (Terminated " + DateUtils.fromDateShort(client.getTerminateDate()) + ")");
					}
				}
				catch (Exception e) {
					errorCount++;
					status.addError(managerAccount.getLabel() + ": " + ExceptionUtils.getOriginalMessage(e));
				}
			}

			status.setMessage("Successfully Deactivated: " + successCount + "; Failed to Deactivate: " + errorCount);
			status.setActionPerformed(true);
		}

		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////            Getter and Setter Methods               ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}
}
