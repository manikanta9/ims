package com.clifton.investment.manager.cache;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentManagerAccountRollupCacheImpl</code> caches
 * rollup relationships for a manager as either the parent or the child.
 * <p>
 * Cache stores ID values of the {@link InvestmentManagerAccountRollup} and when retrieved looks up by id values
 * <p>
 * Calling methods can then filter out on parent or child = manager account id to get the list of parents or the list of children
 * <p>
 * Cache is cleared for parent and child when rollup is updated
 * <p>
 * NOTE: This cache is pretty big, because stores empty lists for managers that are neither a parent or a child.
 * so that we don't attempt to do look ups again each time
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentManagerAccountRollupCacheImpl extends SelfRegisteringSimpleDaoCache<InvestmentManagerAccountRollup, Integer, Integer[]> implements InvestmentManagerAccountRollupCache {


	@Override
	public Integer[] getInvestmentManagerAccountRollupList(int managerAccountId) {
		return getCacheHandler().get(getCacheName(), managerAccountId);
	}


	@Override
	public void storeInvestmentManagerAccountRollupList(int managerAccountId, List<InvestmentManagerAccountRollup> list) {
		Integer[] ids = BeanUtils.getBeanIdentityArray(list, Integer.class);
		getCacheHandler().put(getCacheName(), managerAccountId, ids);
	}


	private void clearInvestmentManagerAccountRollupCache(int managerAccountId) {
		getCacheHandler().remove(getCacheName(), managerAccountId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<InvestmentManagerAccountRollup> dao, DaoEventTypes event, InvestmentManagerAccountRollup bean, Throwable e) {
		if (e == null) {
			boolean clear = false;
			if (event.isUpdate()) {
				InvestmentManagerAccountRollup originalBean = getOriginalBean(dao, bean);
				if (originalBean != null && !CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualProperties(originalBean, bean, false))) {
					clear = true;
					// Clear the original reference's cache, not just the new one
					clearInvestmentManagerAccountRollupCache(originalBean.getReferenceOne().getId());
					clearInvestmentManagerAccountRollupCache(originalBean.getReferenceTwo().getId());
				}
			}
			if (clear || event.isInsert() || event.isDelete()) {
				clearInvestmentManagerAccountRollupCache(bean.getReferenceOne().getId());
				clearInvestmentManagerAccountRollupCache(bean.getReferenceTwo().getId());
			}
		}
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<InvestmentManagerAccountRollup> dao, DaoEventTypes event, InvestmentManagerAccountRollup bean) {
		if (!event.isInsert()) {
			getOriginalBean(dao, bean);
		}
	}
}
