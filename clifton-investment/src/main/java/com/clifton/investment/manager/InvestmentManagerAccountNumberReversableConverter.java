package com.clifton.investment.manager;


import com.clifton.core.converter.reversable.ReversableConverter;

import java.math.BigDecimal;
import java.util.UUID;


/**
 * The <code>InvestmentManagerAccountNumberReversableConverter</code> is used for manager account uploads
 * and allows not populating the account number and the system will return the system uuid.
 * <p/>
 * Special upload method is then used to update to actual system defined manager account number, which uses the id
 * after create is successful.
 *
 * @author manderson
 */
public class InvestmentManagerAccountNumberReversableConverter implements ReversableConverter<Object, String> {

	public static final String MANAGER_ACCOUNT_NUMBER_UPLOAD_PREFIX = "UPLOAD_";


	@Override
	public String convert(Object from) {
		if (from == null) {
			return MANAGER_ACCOUNT_NUMBER_UPLOAD_PREFIX + UUID.randomUUID().toString();
		}
		if (from instanceof BigDecimal) {
			return ((BigDecimal) from).toPlainString();
		}
		return from.toString();
	}


	@Override
	public String reverseConvert(String to) {
		return to;
	}
}
