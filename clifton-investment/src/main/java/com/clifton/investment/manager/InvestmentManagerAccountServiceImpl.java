package com.clifton.investment.manager;


import com.clifton.business.client.BusinessClientService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.manager.cache.InvestmentManagerAccountRollupCache;
import com.clifton.investment.manager.search.InvestmentManagerAccountAllocationSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountAssignmentOverlayActionSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountGroupSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountRollupSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchFormConfigurer;
import com.clifton.system.audit.auditor.SystemAuditDaoUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * The <code>InvestmentManagerAccountServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class InvestmentManagerAccountServiceImpl implements InvestmentManagerAccountService {

	private AdvancedUpdatableDAO<InvestmentManagerAccount, Criteria> investmentManagerAccountDAO;
	private AdvancedUpdatableDAO<InvestmentManagerAccountAllocation, Criteria> investmentManagerAccountAllocationDAO;

	private AdvancedUpdatableDAO<InvestmentManagerAccountAssignment, Criteria> investmentManagerAccountAssignmentDAO;
	private AdvancedUpdatableDAO<InvestmentManagerAccountAssignmentOverlayAction, Criteria> investmentManagerAccountAssignmentOverlayActionDAO;
	private AdvancedUpdatableDAO<InvestmentManagerAccountRollup, Criteria> investmentManagerAccountRollupDAO;

	private AdvancedUpdatableDAO<InvestmentManagerAccountAdjustmentM2M, Criteria> investmentManagerAccountAdjustmentM2MDAO;

	private AdvancedUpdatableDAO<InvestmentManagerAccountGroup, Criteria> investmentManagerAccountGroupDAO;
	private AdvancedUpdatableDAO<InvestmentManagerAccountGroupAllocation, Criteria> investmentManagerAccountGroupAllocationDAO;

	private BusinessClientService businessClientService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	private DaoSingleKeyListCache<InvestmentManagerAccountAdjustmentM2M, Integer> investmentManagerAccountAdjustmentM2MListByManagerAccountCache;
	private DaoSingleKeyListCache<InvestmentManagerAccountAllocation, Integer> investmentManagerAccountAllocationListByAssignmentCache;
	private DaoSingleKeyListCache<InvestmentManagerAccountGroup, Integer> investmentManagerAccountGroupListByInvestmentAccountCache;
	private DaoSingleKeyListCache<InvestmentManagerAccountGroupAllocation, Integer> investmentManagerAccountGroupAllocationListByGroupCache;
	private InvestmentManagerAccountRollupCache investmentManagerAccountRollupCache;


	////////////////////////////////////////////////////////////////////////////
	////////          Investment Manager Account Methods               /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerAccount getInvestmentManagerAccount(int id) {
		InvestmentManagerAccount account = getInvestmentManagerAccountDAO().findByPrimaryKey(id);
		if (account != null) {
			if (account.isRollupManager()) {
				account.setRollupManagerList(getInvestmentManagerAccountRollupListByParent(id));
			}
			account.setAdjustmentM2MList(getInvestmentManagerAccountAdjustmentM2MListByManager(id));
		}
		return account;
	}


	@Override
	public List<InvestmentManagerAccount> getInvestmentManagerAccountList(final InvestmentManagerAccountSearchForm searchForm) {
		return getInvestmentManagerAccountDAO().findBySearchCriteria(new InvestmentManagerAccountSearchFormConfigurer(searchForm, getBusinessClientService()));
	}


	/**
	 * Called from InvestmentManagerAccountBalanceServiceImpl to find all manager account missing balances on a given day.
	 */
	@Override
	public List<InvestmentManagerAccount> getInvestmentManagerAccountListBySearchConfig(HibernateSearchFormConfigurer searchConfig) {
		return getInvestmentManagerAccountDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	@Transactional
	public InvestmentManagerAccount saveInvestmentManagerAccountFromUpload(InvestmentManagerAccount bean) {
		boolean updateNumber = false;
		if (bean.isNewBean() && bean.getAccountNumber().startsWith(InvestmentManagerAccountNumberReversableConverter.MANAGER_ACCOUNT_NUMBER_UPLOAD_PREFIX)) {
			updateNumber = true;
		}

		// save the account
		bean = getInvestmentManagerAccountDAO().save(bean);
		// If we need to update the auto-generated account number after an insert
		if (updateNumber) {
			bean = updateInvestmentManagerAccountToAutoGeneratedNumber(bean);
		}
		return bean;
	}


	@Override
	@Transactional
	public InvestmentManagerAccount saveInvestmentManagerAccount(InvestmentManagerAccount bean) {
		// When Manager Numbers are entered by Users, a 6 digit value is enforced
		// Otherwise, for inserts - make it UUID value for insert uniqueness
		// and after insert change it to M + ID and do not enforce 6 digits
		boolean updateNumber = false;
		if (StringUtils.isEmpty(bean.getAccountNumber())) {
			if (bean.isNewBean()) {
				updateNumber = true;
				bean.setAccountNumber(UUID.randomUUID().toString());
			}
			else {
				bean.setAccountNumber(bean.getAutoGeneratedAccountNumber());
			}
		}
		else if (!bean.isAccountNumberAutoGenerated()) {
			ValidationUtils.assertTrue(bean.getAccountNumber().length() == 6, "Account number [" + bean.getAccountNumber()
					+ "] is invalid. User Defined Account numbers must be 6 characters long. To have the system generate a unique account number for you, please leave this field blank.");
		}

		List<InvestmentManagerAccountRollup> rollups = bean.getRollupManagerList();
		List<InvestmentManagerAccountAdjustmentM2M> adjustments = bean.getAdjustmentM2MList();

		if (bean.isRollupManager()) {
			if (CollectionUtils.isEmpty(rollups)) {
				throw new ValidationException("Rollup managers require at least one child manager selected.  Please select a child manager, or uncheck rollup manager field.");
			}
		}

		// get existing lists so that we know what to delete if necessary
		List<InvestmentManagerAccountRollup> oldRollups = null;
		List<InvestmentManagerAccountAdjustmentM2M> oldAdjustments = null;
		if (!bean.isNewBean()) {
			oldRollups = getInvestmentManagerAccountRollupListByParent(bean.getId());
			oldAdjustments = getInvestmentManagerAccountAdjustmentM2MListByManager(bean.getId());
		}

		// save the account and its allocations
		bean = getInvestmentManagerAccountDAO().save(bean);
		// If we need to update the auto-generated account number after an insert
		if (updateNumber) {
			updateInvestmentManagerAccountToAutoGeneratedNumber(bean);
		}

		// Rollups - Delete if not a rollup manager, otherwise save.
		if (!bean.isRollupManager()) {
			if (!CollectionUtils.isEmpty(oldRollups)) {
				getInvestmentManagerAccountRollupDAO().deleteList(oldRollups);
			}
		}
		else {
			for (InvestmentManagerAccountRollup rollup : rollups) {
				rollup.setReferenceOne(bean);

				ValidationUtils.assertNotNull(rollup.getAllocationPercent(), "Allocation percent is required for each rollup manager account.", "allocationPercent");
				ValidationUtils.assertTrue(bean.getClient().equals(rollup.getReferenceTwo().getClient()), "Rollup managers can only contain child manager accounts for the same client.");
				ValidationUtils.assertTrue(bean.getBaseCurrency().equals(rollup.getReferenceTwo().getBaseCurrency()), "Rollup managers can only contain child manager accounts for the same base currency.");
			}
			// Validate circular dependencies
			validateInvestmentManagerAccountRollupDependency(bean, rollups);

			getInvestmentManagerAccountRollupDAO().saveList(rollups, oldRollups);
		}

		for (InvestmentManagerAccountAdjustmentM2M adj : CollectionUtils.getIterable(adjustments)) {
			adj.setReferenceOne(bean);

			ValidationUtils.assertMutuallyExclusive("Days of M2M or Start Date must be chosen (but not both)", adj.getDaysOfM2M(), adj.getStartDate());

			if (adj.getDaysOfM2M() != null) {
				ValidationUtils.assertTrue(0 < adj.getDaysOfM2M(), "Days for M2M adjustments must be greater than 0");
			}
			else {
				boolean businessDay = getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(adj.getStartDate()));
				ValidationUtils.assertTrue(businessDay, "Please choose a valid business day for Start Date.");
			}

			ValidationUtils.assertNotNull(adj.getReferenceTwo(), "Account selection is required for each M2M Adjustments.");
			ValidationUtils.assertTrue(bean.getClient().equals(adj.getReferenceTwo().getBusinessClient()), "Account selection for M2M Adjustments must belong to the same client.");
			ValidationUtils.assertTrue(bean.getBaseCurrency().equals(adj.getReferenceTwo().getBaseCurrency()), "Account selection for M2M Adjustments must use the same base currency as the manager.");
		}
		getInvestmentManagerAccountAdjustmentM2MDAO().saveList(adjustments, oldAdjustments);
		bean.setRollupManagerList(rollups);
		bean.setAdjustmentM2MList(adjustments);
		return bean;
	}


	private InvestmentManagerAccount updateInvestmentManagerAccountToAutoGeneratedNumber(final InvestmentManagerAccount bean) {
		bean.setAccountNumber(bean.getAutoGeneratedAccountNumber());

		final UpdatableDAO<InvestmentManagerAccount> dao = this.investmentManagerAccountDAO;
		return SystemAuditDaoUtils.executeWithAuditingDisabled(new String[]{"accountNumber"}, () -> dao.save(bean));
	}


	@Override
	// Changed to Public so Linked Managers that have a dependency on Managers (through custom fields) can verify circular dependencies before trying to load and getting stuck in an infinite loop
	public void validateInvestmentManagerAccountRollupDependency(InvestmentManagerAccount parent, List<InvestmentManagerAccountRollup> rollupList) {
		// New Beans cannot possibly have a circular dependency
		if (parent == null || parent.isNewBean()) {
			return;
		}
		for (InvestmentManagerAccountRollup rollup : CollectionUtils.getIterable(rollupList)) {
			if (rollup.getReferenceTwo().equals(parent)) {
				throw new ValidationException("Rollup Manager selection will result in a circular dependency.  Manager [" + rollup.getReferenceOne().getAccountNumber() + "] already depends on ["
						+ parent.getAccountNumber() + "].");
			}
			if (rollup.getReferenceTwo().isRollupManager()) {
				validateInvestmentManagerAccountRollupDependency(parent, getInvestmentManagerAccountRollupListByParent(rollup.getReferenceTwo().getId()));
			}
		}
	}


	@Override
	@Transactional
	public InvestmentManagerAccount saveInvestmentManagerAccountInactive(InvestmentManagerAccount managerAccount) {
		// Get active assignments for the manager account (if any) and de-activate them first
		List<InvestmentManagerAccountAssignment> assignmentList = getInvestmentManagerAccountAssignmentListByManager(managerAccount.getId(), true);
		for (InvestmentManagerAccountAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
			assignment.setInactive(true);
			getInvestmentManagerAccountAssignmentDAO().save(assignment);
		}
		// De-activate the manager account
		managerAccount.setInactive(true);
		return getInvestmentManagerAccountDAO().save(managerAccount);
	}


	@Override
	@Transactional
	public void deleteInvestmentManagerAccount(int id) {
		// First Delete all Allocations
		InvestmentManagerAccount account = getInvestmentManagerAccount(id);
		if (account != null) {
			getInvestmentManagerAccountRollupDAO().deleteList(account.getRollupManagerList());
		}
		// Then delete the account
		getInvestmentManagerAccountDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////        Investment Manager Account Assignment Methods            //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerAccountAssignment getInvestmentManagerAccountAssignment(int id) {
		InvestmentManagerAccountAssignment assign = getInvestmentManagerAccountAssignmentDAO().findByPrimaryKey(id);
		populateInvestmentManagerAccountAssignment(assign);
		return assign;
	}


	@Override
	public InvestmentManagerAccountAssignment getInvestmentManagerAccountAssignmentByManagerAndAccount(int managerAccountId, int accountId) {
		InvestmentManagerAccountAssignment assign = getInvestmentManagerAccountAssignmentDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"},
				new Object[]{managerAccountId, accountId});
		populateInvestmentManagerAccountAssignment(assign);
		return assign;
	}


	private void populateInvestmentManagerAccountAssignment(InvestmentManagerAccountAssignment assign) {
		if (assign != null) {
			assign.setAllocationList(getInvestmentManagerAccountAllocationList(assign.getId(), false));
			if (ManagerCustomCashOverlayTypes.NONE != assign.getCustomCashOverlayType()) {
				assign.setCustomCashOverlayAllocationList(getInvestmentManagerAccountAllocationList(assign.getId(), true));
			}
		}
	}


	@Override
	public List<InvestmentManagerAccountAssignment> getInvestmentManagerAccountAssignmentListByManager(int managerAccountId, Boolean active) {
		if (active == null) {
			return getInvestmentManagerAccountAssignmentDAO().findByField("referenceOne.id", managerAccountId);
		}
		return getInvestmentManagerAccountAssignmentDAO().findByFields(new String[]{"referenceOne.id", "inactive"}, new Object[]{managerAccountId, !active});
	}


	@Override
	public List<InvestmentManagerAccountAssignment> getInvestmentManagerAccountAssignmentListByAccount(int accountId, Boolean active) {
		if (active == null) {
			return getInvestmentManagerAccountAssignmentDAO().findByField("referenceTwo.id", accountId);
		}
		return getInvestmentManagerAccountAssignmentDAO().findByFields(new String[]{"referenceTwo.id", "inactive"}, new Object[]{accountId, !active});
	}


	@Override
	@Transactional
	public InvestmentManagerAccountAssignment saveInvestmentManagerAccountAssignment(InvestmentManagerAccountAssignment managerAccountAssignment) {
		if (!managerAccountAssignment.isInactive()) {
			ValidationUtils.assertFalse(managerAccountAssignment.getReferenceOne().isInactive(), "This assignment cannot be marked as Active, because the manager [" + managerAccountAssignment.getReferenceOne().getLabel()
					+ "] is currently flagged as inactive.", "inactive");

			if (managerAccountAssignment.getReferenceOne().getLinkedInvestmentAccount() != null && managerAccountAssignment.getReferenceOne().getLinkedInvestmentAccount().equals(managerAccountAssignment.getReferenceTwo())) {
				if (managerAccountAssignment.getReferenceOne().getLinkedManagerType().isProcessingDependent()) {
					throw new ValidationException("You cannot assign linked manager [" + managerAccountAssignment.getReferenceOne().getLabel() + "] to account [" + managerAccountAssignment.getReferenceTwo().getLabel()
							+ "] because that account needs to process its Portfolio Run in order to calculate the manager balance.");
				}
			}
		}

		BusinessServiceProcessingType serviceProcessingType = managerAccountAssignment.getReferenceTwo().getServiceProcessingType();
		if (serviceProcessingType == null) {
			throw new ValidationException("Missing service processing type for Account [" + managerAccountAssignment.getReferenceTwo().getLabel() + "].  Manager Assignments are dependent on the service processing type of the client account.");
		}
		// Processing Type is a required field on the Service Types
		if (!serviceProcessingType.getProcessingType().isManagerAssignmentsSupported()) {
			throw new ValidationException("Service Processing Type [" + serviceProcessingType.getName() + "] is selected for Account [" + managerAccountAssignment.getReferenceTwo().getLabel() + "].  This processing type does not support manager assignments.");
		}

		// Portfolio Runs and LDI Support Different Cash Types
		if (serviceProcessingType.getProcessingType().isManagerAssignmentCashTypeRequired()) {
			ValidationUtils.assertNotNull(managerAccountAssignment.getCashType(), "Cash Type is required.");
			ValidationUtils.assertTrue(managerAccountAssignment.getCashType().getServiceProcessingType() == serviceProcessingType.getProcessingType(), "Selected Cash Type [" + managerAccountAssignment.getCashType().getLabel()
					+ "] is not supported for Service Processing Type [" + serviceProcessingType.getName() + "].");
			ValidationUtils.assertTrue(managerAccountAssignment.getCashType().isSelectableOption(), "Cash Type selection [" + managerAccountAssignment.getCashType().getLabel() + "] is not a valid selection");
		}
		else {
			managerAccountAssignment.setCashType(null);
		}

		// LDI, Duration Management
		if (!serviceProcessingType.getProcessingType().isAssetClassesSupported()) {
			managerAccountAssignment.setCashOverlayType(null);

			// Nothing is used for Security Targets except to just assign the manager account to the client account
			// More attributes could be moved to the enum, but waiting until we know more about manager assignments for the security target processing
			if (ArrayUtils.anyMatch(new ServiceProcessingTypes[]{ServiceProcessingTypes.LDI, ServiceProcessingTypes.DURATION_MANAGEMENT}, o -> o == serviceProcessingType.getProcessingType())) {
				// Require the Benchmark Security - as long as it's active and not just for Margin (LDI)
				if (!managerAccountAssignment.isInactive() && (managerAccountAssignment.getCashType() == null || ManagerCashTypes.MARGIN != managerAccountAssignment.getCashType())) {
					ValidationUtils.assertNotNull(managerAccountAssignment.getBenchmarkSecurity(), "Benchmark Security selection is required for LDI or Duration Management Manager Assignments.");
				}
			}
			managerAccountAssignment = getInvestmentManagerAccountAssignmentDAO().save(managerAccountAssignment);
		}
		// Else Overlay, Fully Funded, Structured Options
		else {
			// Cash Overlay Type selection is Required
			ValidationUtils.assertNotNull(managerAccountAssignment.getCashOverlayType(), "Cash Overlay Type is required.");

			// Validate if TargetAllocationPercent entered - Rebalance Min/Max fields are also entered
			if (managerAccountAssignment.getTargetAllocationPercent() == null) {
				managerAccountAssignment.setRebalanceAllocationFixed(false);
				managerAccountAssignment.setRebalanceAllocationMin(null);
				managerAccountAssignment.setRebalanceAllocationMax(null);
			}
			else {
				ValidationUtils.assertNotNull(managerAccountAssignment.getRebalanceAllocationMin(), "Minimum trigger for rebalance allocation is required when target allocation percentage is entered.",
						"rebalanceAllocationMin");
				ValidationUtils.assertNotNull(managerAccountAssignment.getRebalanceAllocationMax(), "Maximum trigger for rebalance allocation is required when target allocation percentage is entered.",
						"rebalanceAllocationMax");
			}

			// Get a list of valid asset classes -- tied to account & currently active
			List<InvestmentAccountAssetClass> assetClassList = getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassListByAccount(managerAccountAssignment.getReferenceTwo().getId());
			// Exclude Rollup Asset Classes from selectable options
			assetClassList = BeanUtils.filter(assetClassList, InvestmentAccountAssetClass::isRollupAssetClass, false);
			Object[] validAssetClasses = BeanUtils.getPropertyValues(assetClassList, "assetClass.id");
			List<InvestmentManagerAccountAllocation> allocations = validateInvestmentManagerAccountAllocationList(managerAccountAssignment.getAllocationList(), managerAccountAssignment, false, validAssetClasses);
			// As long as NOT null and false (i.e. explicitly false) then run this validation
			if (managerAccountAssignment.getAllowPartialAllocation() != null && !managerAccountAssignment.getAllowPartialAllocation()) {
				BigDecimal totalAllocation = CoreMathUtils.sumProperty(managerAccountAssignment.getAllocationList(), InvestmentManagerAccountAllocation::getAllocationPercent);
				ValidationUtils.assertTrue(MathUtils.isEqual(totalAllocation, MathUtils.BIG_DECIMAL_ONE_HUNDRED), "Total Asset Class Allocations for Manager Balance is [" + CoreMathUtils.formatNumberDecimal(totalAllocation) + "] which does not equal 100%.  Please adjust your percentages, or check the <i>Do Not Enforce 100% Allocation<i> option.");
			}
			List<InvestmentManagerAccountAllocation> customCashAllocations = validateInvestmentManagerAccountAllocationList(managerAccountAssignment.getCustomCashOverlayAllocationList(), managerAccountAssignment, true, validAssetClasses);

			// get existing lists so that we know what to delete if necessary
			List<InvestmentManagerAccountAllocation> oldAllocations = null;
			List<InvestmentManagerAccountAllocation> oldCustomCashAllocations = null;
			if (!managerAccountAssignment.isNewBean()) {
				oldAllocations = getInvestmentManagerAccountAllocationList(managerAccountAssignment.getId(), false);
				oldCustomCashAllocations = getInvestmentManagerAccountAllocationList(managerAccountAssignment.getId(), true);
			}

			// save the assignment and its allocations
			managerAccountAssignment = getInvestmentManagerAccountAssignmentDAO().save(managerAccountAssignment);
			getInvestmentManagerAccountAllocationDAO().saveList(allocations, oldAllocations);
			getInvestmentManagerAccountAllocationDAO().saveList(customCashAllocations, oldCustomCashAllocations);
			managerAccountAssignment.setAllocationList(allocations);
			managerAccountAssignment.setCustomCashOverlayAllocationList(customCashAllocations);
		}
		return managerAccountAssignment;
	}


	@Override
	@Transactional
	public void saveInvestmentManagerAccountAssignmentList(List<InvestmentManagerAccountAssignment> managerAccountAssignmentList) {
		for (InvestmentManagerAccountAssignment managerAccountAssignment : CollectionUtils.getIterable(managerAccountAssignmentList)) {
			saveInvestmentManagerAccountAssignment(managerAccountAssignment);
		}
	}


	@Override
	@Transactional
	public void deleteInvestmentManagerAccountAssignment(int id) {
		// First Delete all Allocations
		InvestmentManagerAccountAssignment assign = getInvestmentManagerAccountAssignment(id);
		if (assign != null) {
			getInvestmentManagerAccountAllocationDAO().deleteList(assign.getAllocationList());
			getInvestmentManagerAccountAllocationDAO().deleteList(assign.getCustomCashOverlayAllocationList());
		}
		// Then delete the assignment
		getInvestmentManagerAccountAssignmentDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////       Investment Manager Account Allocation Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentManagerAccountAllocation> getInvestmentManagerAccountAllocationList(InvestmentManagerAccountAllocationSearchForm searchForm) {
		return getInvestmentManagerAccountAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private List<InvestmentManagerAccountAllocation> getInvestmentManagerAccountAllocationList(int assignmentId, boolean customCashOverlay) {
		List<InvestmentManagerAccountAllocation> allocationList = getInvestmentManagerAccountAllocationListByAssignmentCache().getBeanListForKeyValue(getInvestmentManagerAccountAllocationDAO(), assignmentId);
		return BeanUtils.filter(allocationList, InvestmentManagerAccountAllocation::isCustomCashOverlay, customCashOverlay);
	}


	private List<InvestmentManagerAccountAllocation> validateInvestmentManagerAccountAllocationList(List<InvestmentManagerAccountAllocation> allocationList,
	                                                                                                InvestmentManagerAccountAssignment assignment, boolean customCashOverlay, Object[] validAssetClassIds) {
		if (!customCashOverlay) {
			ValidationUtils.assertNotEmpty(allocationList, "Investment Manager Accounts must have at least one allocation.");
		}
		else {
			// If not custom allocation overlay type - return null as the custom asset class allocation list (will be deleted)
			if (ManagerCustomCashOverlayTypes.NONE == assignment.getCustomCashOverlayType()) {
				assignment.setCustomCashAllocationType(null);
				return null;
			}
			ValidationUtils.assertNotEmpty(allocationList, "Investment Manager Accounts with custom cash overlay allocations must have at least one asset call allocation entered.");
			ValidationUtils.assertNotNull(assignment.getCustomCashAllocationType(), "Custom Cash allocation type MUST be selected when custom cash overlay is used.");
		}
		for (InvestmentManagerAccountAllocation alloc : allocationList) {
			ValidationUtils.assertNotNull(alloc.getAssetClass(), "There is a record in the allocation list without an asset class.  Please select an asset class or delete that row.");
			if (!customCashOverlay) {
				// Only applies to custom cash overlay
				alloc.setCashType(null);
			}
			else {
				// Cash Type selection is Required for custom cash overlay
				ValidationUtils.assertNotNull(alloc.getCashType(), "Custom Cash Overlay Asset Class [" + alloc.getAssetClass().getName() + "] is missing required Cash Type");
				ValidationUtils.assertTrue(alloc.getCashType().isSelectableOption(), "Custom Cash Overlay Asset Class [" + alloc.getAssetClass().getName() + "] Cash Type selection ["
						+ alloc.getCashType().getLabel() + "] is not a valid selection");
				if (!alloc.isApplyBenchmarkAdjustment()) {
					ValidationUtils.assertNull(alloc.getBenchmarkSecurity(), "Benchmark Security cannot be selected if not applying benchmark adjustment.");
					ValidationUtils.assertNull(alloc.getBenchmarkAdjustmentOffSetCashOverlayType(), "Benchmark Adjustment Offset Cash Overlay Type cannot be selected if not applying benchmark adjustment");
				}
				else {
					if (alloc.getBenchmarkAdjustmentOffSetCashOverlayType() == null) {
						alloc.setBenchmarkAdjustmentOffSetCashOverlayType(ManagerCashOverlayTypes.NONE); // Use Default Rules if Not Selected
					}
					ValidationUtils.assertTrue(alloc.getBenchmarkAdjustmentOffSetCashOverlayType() == ManagerCashOverlayTypes.NONE || alloc.getBenchmarkAdjustmentOffSetCashOverlayType() == ManagerCashOverlayTypes.FUND, "Benchmark Adjustment Offset Cash Overlay Type selections can either be None or Fund.");
				}
			}
			if (customCashOverlay) {
				ValidationUtils.assertNotNull(alloc.getCustomAllocationValue(), "Custom Cash Overlay Asset Class [" + alloc.getAssetClass().getName()
						+ "] is missing required " + (assignment.getCustomCashOverlayType().isApplyValuesAsPercentage() ? "Allocation Percent" : "Allocation Value"));
				ValidationUtils.assertFalse(assignment.getCustomCashOverlayType().isApplyValuesAsPercentage() && MathUtils.isGreaterThan(MathUtils.abs(alloc.getCustomAllocationValue()), BigDecimal.valueOf(9999.99)),
						"Custom Cash Overlay Asset Class [" + alloc.getAssetClass().getName() + "] percentage is not allowed to be over 9,999.99%");
			}
			else {
				ValidationUtils.assertNotNull(alloc.getAllocationPercent(), "Asset Class [" + alloc.getAssetClass().getName() + "] is missing required Allocation Percent");
			}
			ValidationUtils.assertTrue(ArrayUtils.contains(validAssetClassIds, alloc.getAssetClass().getId()), "Selected Asset Class [" + alloc.getAssetClass().getName()
					+ "] is invalid because it has not been tied to the investment account [" + assignment.getReferenceTwo().getLabel()
					+ "] or is assigned to the account, but as a rollup asset class");
			alloc.setManagerAccountAssignment(assignment);
			alloc.setCustomCashOverlay(customCashOverlay);
		}
		return allocationList;
	}


	////////////////////////////////////////////////////////////////////////////
	/////       Investment Manager Account Adjustment M2M Methods         //////
	////////////////////////////////////////////////////////////////////////////


	private List<InvestmentManagerAccountAdjustmentM2M> getInvestmentManagerAccountAdjustmentM2MListByManager(int managerAccountId) {
		return getInvestmentManagerAccountAdjustmentM2MListByManagerAccountCache().getBeanListForKeyValue(getInvestmentManagerAccountAdjustmentM2MDAO(), managerAccountId);
	}


	////////////////////////////////////////////////////////////////////////////
	////  Investment Manager Account Assignment Overlay Action  Methods  ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerAccountAssignmentOverlayAction getInvestmentManagerAccountAssignmentOverlayAction(int id) {
		return getInvestmentManagerAccountAssignmentOverlayActionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentManagerAccountAssignmentOverlayAction> getInvestmentManagerAccountAssignmentOverlayActionList(InvestmentManagerAccountAssignmentOverlayActionSearchForm searchForm) {
		return getInvestmentManagerAccountAssignmentOverlayActionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	// Validation: {@link InvestmentManagerAccountAssignmentOverlayActionValidator}
	@Override
	public InvestmentManagerAccountAssignmentOverlayAction saveInvestmentManagerAccountAssignmentOverlayAction(InvestmentManagerAccountAssignmentOverlayAction bean) {
		return getInvestmentManagerAccountAssignmentOverlayActionDAO().save(bean);
	}


	@Override
	public void deleteInvestmentManagerAccountAssignmentOverlayAction(int id) {
		getInvestmentManagerAccountAssignmentOverlayActionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////           Investment Manager Account Rollup Methods           //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentManagerAccountRollup> getInvestmentManagerAccountRollupListByParent(int parentId) {
		return getInvestmentManagerAccountRollupListByParentOrChild(parentId, true);
	}


	@Override
	public List<InvestmentManagerAccountRollup> getInvestmentManagerAccountRollupListByChild(int childId) {
		return getInvestmentManagerAccountRollupListByParentOrChild(childId, false);
	}


	/**
	 * Helper method for parent/child rollup look ups.  Cache stores full list where manager is a parent or a child, this method gets/sets from the cache and
	 * returns filtered list based on parent flag (if parent = true, returns list where manager is the parent, else child)
	 */
	private List<InvestmentManagerAccountRollup> getInvestmentManagerAccountRollupListByParentOrChild(int managerId, boolean parent) {
		List<InvestmentManagerAccountRollup> rollupList = null;
		Integer[] rollupIds = getInvestmentManagerAccountRollupCache().getInvestmentManagerAccountRollupList(managerId);
		// Nothing cached yet - look it up
		if (rollupIds == null) {
			InvestmentManagerAccountRollupSearchForm searchForm = new InvestmentManagerAccountRollupSearchForm();
			searchForm.setParentOrChildManagerAccountId(managerId);
			rollupList = getInvestmentManagerAccountRollupList(searchForm);
			getInvestmentManagerAccountRollupCache().storeInvestmentManagerAccountRollupList(managerId, rollupList);
		}
		// Otherwise lookup rollups by id to get real list (only if length of array > 0
		else if (rollupIds.length > 0) {
			rollupList = getInvestmentManagerAccountRollupDAO().findByPrimaryKeys(rollupIds);
		}
		// Filter out the list where manager = parent or = child depending on which one looking for.
		if (!CollectionUtils.isEmpty(rollupList)) {
			return BeanUtils.filter(rollupList, rollup -> (parent ? rollup.getReferenceOne().getId() : rollup.getReferenceTwo().getId()), managerId);
		}
		return rollupList;
	}


	@Override
	public List<InvestmentManagerAccountRollup> getInvestmentManagerAccountRollupList(final InvestmentManagerAccountRollupSearchForm searchForm) {
		return getInvestmentManagerAccountRollupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public void saveInvestmentManagerAccountRollupPercentageList(Integer[] rollupIds, BigDecimal allocationPercent) {
		if (rollupIds == null || rollupIds.length == 0) {
			throw new ValidationException("No Rollups(s) selected to update.");
		}

		ValidationUtils.assertNotNull(allocationPercent, "Allocation percent is required.");
		if (MathUtils.isGreaterThan(allocationPercent, 100)) {
			throw new ValidationException("Allocation percentages cannot exceed 100%.");
		}

		List<InvestmentManagerAccountRollup> list = new ArrayList<>();
		for (Integer rollupId : rollupIds) {
			InvestmentManagerAccountRollup r = getInvestmentManagerAccountRollupDAO().findByPrimaryKey(rollupId);
			r.setAllocationPercent(allocationPercent);
			list.add(r);
		}

		getInvestmentManagerAccountRollupDAO().saveList(list);
	}


	////////////////////////////////////////////////////////////////////////////
	///////           Investment Manager Account Group Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerAccountGroup getInvestmentManagerAccountGroup(int id) {
		InvestmentManagerAccountGroup bean = getInvestmentManagerAccountGroupDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setAllocationList(getInvestmentManagerAccountGroupAllocationListByGroup(bean.getId()));
		}
		return bean;
	}


	@Override
	public List<InvestmentManagerAccountGroup> getInvestmentManagerAccountGroupListByInvestmentAccount(int investmentAccountId, boolean includeInactive) {
		List<InvestmentManagerAccountGroup> managerGroupList = getInvestmentManagerAccountGroupListByInvestmentAccountCache().getBeanListForKeyValue(getInvestmentManagerAccountGroupDAO(), investmentAccountId);
		if (!includeInactive && !CollectionUtils.isEmpty(managerGroupList)) {
			return BeanUtils.filter(managerGroupList, InvestmentManagerAccountGroup::isInactive, false);
		}
		return managerGroupList;
	}


	@Override
	public List<InvestmentManagerAccountGroup> getInvestmentManagerAccountGroupList(InvestmentManagerAccountGroupSearchForm searchForm) {
		return getInvestmentManagerAccountGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Transactional
	@Override
	public InvestmentManagerAccountGroup saveInvestmentManagerAccountGroup(InvestmentManagerAccountGroup bean) {
		List<InvestmentManagerAccountGroupAllocation> allocations = bean.getAllocationList();
		// Don't validate if it's inactive
		if (!bean.isInactive()) {
			ValidationUtils.assertNotEmpty(allocations, "Investment manager account groups must have at least one allocation.");
			BigDecimal totalWeight = BigDecimal.ZERO;
			for (InvestmentManagerAccountGroupAllocation allocation : allocations) {
				allocation.setReferenceOne(bean);

				if (MathUtils.isNullOrZero(allocation.getTargetAllocation())) {
					throw new ValidationException("Target is required for each allocation");
				}

				totalWeight = totalWeight.add(allocation.getTargetAllocation());
			}
			if (!MathUtils.isEqual(totalWeight, MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
				throw new FieldValidationException("The sum of all target allocation is " + totalWeight + " but must be 100%", "targetAllocation");
			}
		}

		// get existing allocations so that we know what to delete if necessary
		List<InvestmentManagerAccountGroupAllocation> oldAllocations = null;
		if (!bean.isNewBean()) {
			oldAllocations = getInvestmentManagerAccountGroupAllocationListByGroup(bean.getId());
		}

		// save the group and its allocations
		bean = getInvestmentManagerAccountGroupDAO().save(bean);
		getInvestmentManagerAccountGroupAllocationDAO().saveList(allocations, oldAllocations);
		bean.setAllocationList(allocations);
		return bean;
	}


	@Override
	@Transactional
	public void deleteInvestmentManagerAccountGroup(int id) {
		getInvestmentManagerAccountGroupAllocationDAO().deleteList(getInvestmentManagerAccountGroupAllocationListByGroup(id));
		getInvestmentManagerAccountGroupDAO().delete(id);
	}


	private List<InvestmentManagerAccountGroupAllocation> getInvestmentManagerAccountGroupAllocationListByGroup(int groupId) {
		return getInvestmentManagerAccountGroupAllocationListByGroupCache().getBeanListForKeyValue(getInvestmentManagerAccountGroupAllocationDAO(), groupId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentManagerAccount, Criteria> getInvestmentManagerAccountDAO() {
		return this.investmentManagerAccountDAO;
	}


	public void setInvestmentManagerAccountDAO(AdvancedUpdatableDAO<InvestmentManagerAccount, Criteria> investmentManagerAccountDAO) {
		this.investmentManagerAccountDAO = investmentManagerAccountDAO;
	}


	public AdvancedUpdatableDAO<InvestmentManagerAccountAllocation, Criteria> getInvestmentManagerAccountAllocationDAO() {
		return this.investmentManagerAccountAllocationDAO;
	}


	public void setInvestmentManagerAccountAllocationDAO(AdvancedUpdatableDAO<InvestmentManagerAccountAllocation, Criteria> investmentManagerAccountAllocationDAO) {
		this.investmentManagerAccountAllocationDAO = investmentManagerAccountAllocationDAO;
	}


	public AdvancedUpdatableDAO<InvestmentManagerAccountAssignment, Criteria> getInvestmentManagerAccountAssignmentDAO() {
		return this.investmentManagerAccountAssignmentDAO;
	}


	public void setInvestmentManagerAccountAssignmentDAO(AdvancedUpdatableDAO<InvestmentManagerAccountAssignment, Criteria> investmentManagerAccountAssignmentDAO) {
		this.investmentManagerAccountAssignmentDAO = investmentManagerAccountAssignmentDAO;
	}


	public AdvancedUpdatableDAO<InvestmentManagerAccountRollup, Criteria> getInvestmentManagerAccountRollupDAO() {
		return this.investmentManagerAccountRollupDAO;
	}


	public void setInvestmentManagerAccountRollupDAO(AdvancedUpdatableDAO<InvestmentManagerAccountRollup, Criteria> investmentManagerAccountRollupDAO) {
		this.investmentManagerAccountRollupDAO = investmentManagerAccountRollupDAO;
	}


	public AdvancedUpdatableDAO<InvestmentManagerAccountAdjustmentM2M, Criteria> getInvestmentManagerAccountAdjustmentM2MDAO() {
		return this.investmentManagerAccountAdjustmentM2MDAO;
	}


	public void setInvestmentManagerAccountAdjustmentM2MDAO(AdvancedUpdatableDAO<InvestmentManagerAccountAdjustmentM2M, Criteria> investmentManagerAccountAdjustmentM2MDAO) {
		this.investmentManagerAccountAdjustmentM2MDAO = investmentManagerAccountAdjustmentM2MDAO;
	}


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public AdvancedUpdatableDAO<InvestmentManagerAccountGroup, Criteria> getInvestmentManagerAccountGroupDAO() {
		return this.investmentManagerAccountGroupDAO;
	}


	public void setInvestmentManagerAccountGroupDAO(AdvancedUpdatableDAO<InvestmentManagerAccountGroup, Criteria> investmentManagerAccountGroupDAO) {
		this.investmentManagerAccountGroupDAO = investmentManagerAccountGroupDAO;
	}


	public AdvancedUpdatableDAO<InvestmentManagerAccountGroupAllocation, Criteria> getInvestmentManagerAccountGroupAllocationDAO() {
		return this.investmentManagerAccountGroupAllocationDAO;
	}


	public void setInvestmentManagerAccountGroupAllocationDAO(AdvancedUpdatableDAO<InvestmentManagerAccountGroupAllocation, Criteria> investmentManagerAccountGroupAllocationDAO) {
		this.investmentManagerAccountGroupAllocationDAO = investmentManagerAccountGroupAllocationDAO;
	}


	public InvestmentManagerAccountRollupCache getInvestmentManagerAccountRollupCache() {
		return this.investmentManagerAccountRollupCache;
	}


	public void setInvestmentManagerAccountRollupCache(InvestmentManagerAccountRollupCache investmentManagerAccountRollupCache) {
		this.investmentManagerAccountRollupCache = investmentManagerAccountRollupCache;
	}


	public DaoSingleKeyListCache<InvestmentManagerAccountAdjustmentM2M, Integer> getInvestmentManagerAccountAdjustmentM2MListByManagerAccountCache() {
		return this.investmentManagerAccountAdjustmentM2MListByManagerAccountCache;
	}


	public void setInvestmentManagerAccountAdjustmentM2MListByManagerAccountCache(DaoSingleKeyListCache<InvestmentManagerAccountAdjustmentM2M, Integer> investmentManagerAccountAdjustmentM2MListByManagerAccountCache) {
		this.investmentManagerAccountAdjustmentM2MListByManagerAccountCache = investmentManagerAccountAdjustmentM2MListByManagerAccountCache;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}


	public AdvancedUpdatableDAO<InvestmentManagerAccountAssignmentOverlayAction, Criteria> getInvestmentManagerAccountAssignmentOverlayActionDAO() {
		return this.investmentManagerAccountAssignmentOverlayActionDAO;
	}


	public void setInvestmentManagerAccountAssignmentOverlayActionDAO(AdvancedUpdatableDAO<InvestmentManagerAccountAssignmentOverlayAction, Criteria> investmentManagerAccountAssignmentOverlayActionDAO) {
		this.investmentManagerAccountAssignmentOverlayActionDAO = investmentManagerAccountAssignmentOverlayActionDAO;
	}


	public DaoSingleKeyListCache<InvestmentManagerAccountGroup, Integer> getInvestmentManagerAccountGroupListByInvestmentAccountCache() {
		return this.investmentManagerAccountGroupListByInvestmentAccountCache;
	}


	public void setInvestmentManagerAccountGroupListByInvestmentAccountCache(DaoSingleKeyListCache<InvestmentManagerAccountGroup, Integer> investmentManagerAccountGroupListByInvestmentAccountCache) {
		this.investmentManagerAccountGroupListByInvestmentAccountCache = investmentManagerAccountGroupListByInvestmentAccountCache;
	}


	public DaoSingleKeyListCache<InvestmentManagerAccountGroupAllocation, Integer> getInvestmentManagerAccountGroupAllocationListByGroupCache() {
		return this.investmentManagerAccountGroupAllocationListByGroupCache;
	}


	public void setInvestmentManagerAccountGroupAllocationListByGroupCache(DaoSingleKeyListCache<InvestmentManagerAccountGroupAllocation, Integer> investmentManagerAccountGroupAllocationListByGroupCache) {
		this.investmentManagerAccountGroupAllocationListByGroupCache = investmentManagerAccountGroupAllocationListByGroupCache;
	}


	public DaoSingleKeyListCache<InvestmentManagerAccountAllocation, Integer> getInvestmentManagerAccountAllocationListByAssignmentCache() {
		return this.investmentManagerAccountAllocationListByAssignmentCache;
	}


	public void setInvestmentManagerAccountAllocationListByAssignmentCache(DaoSingleKeyListCache<InvestmentManagerAccountAllocation, Integer> investmentManagerAccountAllocationListByAssignmentCache) {
		this.investmentManagerAccountAllocationListByAssignmentCache = investmentManagerAccountAllocationListByAssignmentCache;
	}
}
