package com.clifton.investment.manager.balance;


import com.clifton.core.beans.BaseEntity;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.math.BigDecimal;


/**
 * The <code>InvestmentManagerAccountBalanceAdjustment</code> class handles optional
 * adjustments that maybe necessary to manager account balances.
 *
 * @author vgomelsky
 */
public class InvestmentManagerAccountBalanceAdjustment extends BaseEntity<Integer> {

	private InvestmentManagerAccountBalance managerAccountBalance;
	private InvestmentManagerAccountBalanceAdjustmentType adjustmentType;

	private BigDecimal cashValue;
	private BigDecimal securitiesValue;

	private String note;

	/**
	 * Source information for the adjustment.
	 * Example: Adjustments that are created from Events will point to the
	 * InvestmentEventManagerAdjustment record that it was created from
	 */
	private SystemTable sourceSystemTable;

	@SoftLinkField(tableBeanPropertyName = "sourceSystemTable", ownerBeanPropertyName = "managerAccountBalance")
	private Integer sourceFkFieldId;


	////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////


	/**
	 * Automatically loaded adjustments are those that are for a System Defined Adjustment Type
	 * OR
	 * Manual Adjustment types that have a Source Table/Source FK Field ID.
	 */
	public boolean isAutoLoaded() {
		if (getSourceSystemTable() != null) {
			return true;
		}
		return getAdjustmentType() != null && getAdjustmentType().isSystemDefined();
	}


	public boolean isNextDayAdjustmentApplied() {
		return (getAdjustmentType() != null && getAdjustmentType().getNextDayAdjustmentType() != null);
	}


	////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalance getManagerAccountBalance() {
		return this.managerAccountBalance;
	}


	public void setManagerAccountBalance(InvestmentManagerAccountBalance managerAccountBalance) {
		this.managerAccountBalance = managerAccountBalance;
	}


	public InvestmentManagerAccountBalanceAdjustmentType getAdjustmentType() {
		return this.adjustmentType;
	}


	public void setAdjustmentType(InvestmentManagerAccountBalanceAdjustmentType adjustmentType) {
		this.adjustmentType = adjustmentType;
	}


	public BigDecimal getCashValue() {
		return this.cashValue;
	}


	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public BigDecimal getSecuritiesValue() {
		return this.securitiesValue;
	}


	public void setSecuritiesValue(BigDecimal securitiesValue) {
		this.securitiesValue = securitiesValue;
	}


	public SystemTable getSourceSystemTable() {
		return this.sourceSystemTable;
	}


	public void setSourceSystemTable(SystemTable sourceSystemTable) {
		this.sourceSystemTable = sourceSystemTable;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}
}
