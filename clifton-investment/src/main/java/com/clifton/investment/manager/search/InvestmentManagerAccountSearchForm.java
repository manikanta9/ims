package com.clifton.investment.manager.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccount.ManagerTypes;
import com.clifton.investment.manager.LinkedManagerTypes;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentManagerAccountSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "accountNumber,accountName,managerCompany.name,custodianAccount.number", sortField = "accountNumber", leftJoin = true)
	private String searchPattern;

	@SearchField(searchField = "id")
	private Integer managerAccountId;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] managerAccountIds;

	@SearchField(searchField = "client.id")
	private Integer clientId;

	@SearchField(searchField = "client.company.id,client.clientRelationship.company.id", leftJoin = true)
	private Integer clientOrClientRelationshipCompanyId;

	// Will include the client's parent's accounts (if a child), or the children client's accounts
	// ClientID = ? OR Client's Parent ID = ? OR (LOOKUP CLIENT'S PARENT ID AND IF NOT NULL - THAT VALUE = ClientID)
	private Integer clientIdOrRelatedClient;

	@SearchField(searchFieldPath = "client", searchField = "name")
	private String clientName;


	// Custom Search Field
	private Boolean clientActive;

	// Custom Search Field
	private Short excludeClientAccountWorkflowStateId;

	@SearchField(searchField = "managerCompany.id")
	private Integer managerCompanyId;

	@SearchField(searchField = "managerCompany.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] managerCompanyIds;

	@SearchField(searchField = "name", searchFieldPath = "managerCompany")
	private String managerCompanyName;

	@SearchField(searchField = "ourCompany", searchFieldPath = "managerCompany.type")
	private Boolean managerCompanyOurCompany;

	@SearchField(searchField = "baseCurrency.id", sortField = "baseCurrency.symbol")
	private Integer baseCurrencyId;

	@SearchField
	private String accountNumber;

	@SearchField(searchField = "accountNumber", comparisonConditions = ComparisonConditions.EQUALS)
	private String accountNumberEquals;

	@SearchField
	private String accountName;

	@SearchField(searchField = "accountNumber,accountName", sortField = "accountNumber")
	private String accountNameNumber;

	@SearchField(searchFieldPath = "custodianAccount", searchField = "company.id")
	private Integer custodianCompanyId;


	@SearchField(searchFieldPath = "custodianAccount", searchField = "number", comparisonConditions = {ComparisonConditions.EQUALS})
	private String custodianAccountNumber;

	@SearchField(searchField = "custodianAccount.id")
	private Integer custodianAccountId;

	@SearchField
	private Boolean zeroBalanceIfNoDownload;

	@SearchField
	private InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType;

	@SearchField
	private BigDecimal customCashValue;

	@SearchField
	private Date customCashValueUpdateDate;

	@SearchField(searchField = "inactive")
	private Boolean inactive;

	@SearchField(searchField = "managerType", comparisonConditions = {ComparisonConditions.EQUALS})
	private ManagerTypes managerType;

	@SearchField(searchField = "linkedManagerType", comparisonConditions = {ComparisonConditions.EQUALS})
	private LinkedManagerTypes linkedManagerType;

	@SearchField(searchField = "managerType", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private ManagerTypes excludeProxyManagers;

	@SearchField(searchField = "managerType", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private ManagerTypes excludeRollupManagers;

	@SearchField(searchField = "managerType", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private ManagerTypes excludeLinkedManagers;

	@SearchField(searchFieldPath = "linkedInvestmentAccount", searchField = "number,name", sortField = "number")
	private String linkedInvestmentAccountLabel;

	@SearchField(searchFieldPath = "benchmarkSecurity", searchField = "symbol,name", sortField = "symbol")
	private String benchmarkSecurityLabel;

	@SearchField
	private String durationFieldNameOverride;

	@SearchField
	private Boolean proxyValueGrowthInverted;

	@SearchField
	private BigDecimal proxyValue;

	@SearchField
	private BigDecimal proxySecurityQuantity;

	@SearchField
	private Date proxyValueDate;

	@SearchField(searchFieldPath = "proxyBenchmarkSecurity", searchField = "symbol,name", sortField = "symbol")
	private String proxyBenchmarkSecurityLabel;

	@SearchField
	private String proxyBenchmarkSecurityDisplayName;

	@SearchField(searchFieldPath = "proxyBenchmarkRateIndex", searchField = "symbol,name", sortField = "symbol")
	private String proxyBenchmarkRateIndexLabel;

	@SearchField
	private Boolean proxySecuritiesOnly;

	@SearchField
	private Boolean proxyToLastKnownPrice;


	@SearchField(searchField = "assignmentList.referenceTwo.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer assignmentAccountId;

	@SearchField(searchField = "assignmentList.referenceTwo.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer[] assignmentAccountIds;

	@SearchField(searchField = "assignmentList.inactive", comparisonConditions = ComparisonConditions.EXISTS)
	private Boolean assignmentInactive;

	// Custom Search Field
	private Integer investmentAccountGroupId;

	@SearchField(searchFieldPath = "custodianAccount", searchField = "sweepAccount")
	private Boolean sweepAccount;

	@SearchField(searchField = "rollupAggregationType")
	private String rollupAggregationType;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	@Override
	public String getDaoTableName() {
		return "InvestmentManagerAccount";
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public void setProxyManager(Boolean proxyManager) {
		if (proxyManager != null) {
			if (proxyManager) {
				setManagerType(ManagerTypes.PROXY);
				setExcludeProxyManagers(null);
			}
			else {
				setExcludeProxyManagers(ManagerTypes.PROXY);
			}
		}
	}


	public void setRollupManager(Boolean rollupManager) {
		if (rollupManager != null) {
			if (rollupManager) {
				setManagerType(ManagerTypes.ROLLUP);
				setExcludeRollupManagers(null);
			}
			else {
				setExcludeRollupManagers(ManagerTypes.ROLLUP);
			}
		}
	}


	public void setLinkedManager(Boolean linkedManager) {
		if (linkedManager != null) {
			if (linkedManager) {
				setManagerType(ManagerTypes.LINKED);
				setExcludeLinkedManagers(null);
			}
			else {
				setExcludeLinkedManagers(ManagerTypes.LINKED);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getManagerAccountId() {
		return this.managerAccountId;
	}


	public void setManagerAccountId(Integer managerAccountId) {
		this.managerAccountId = managerAccountId;
	}


	public Integer[] getManagerAccountIds() {
		return this.managerAccountIds;
	}


	public void setManagerAccountIds(Integer[] managerAccountIds) {
		this.managerAccountIds = managerAccountIds;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Integer getClientIdOrRelatedClient() {
		return this.clientIdOrRelatedClient;
	}


	public void setClientIdOrRelatedClient(Integer clientIdOrRelatedClient) {
		this.clientIdOrRelatedClient = clientIdOrRelatedClient;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public Boolean getClientActive() {
		return this.clientActive;
	}


	public void setClientActive(Boolean clientActive) {
		this.clientActive = clientActive;
	}


	public Short getExcludeClientAccountWorkflowStateId() {
		return this.excludeClientAccountWorkflowStateId;
	}


	public void setExcludeClientAccountWorkflowStateId(Short excludeClientAccountWorkflowStateId) {
		this.excludeClientAccountWorkflowStateId = excludeClientAccountWorkflowStateId;
	}


	public Integer getManagerCompanyId() {
		return this.managerCompanyId;
	}


	public void setManagerCompanyId(Integer managerCompanyId) {
		this.managerCompanyId = managerCompanyId;
	}


	public Integer[] getManagerCompanyIds() {
		return this.managerCompanyIds;
	}


	public void setManagerCompanyIds(Integer[] managerCompanyIds) {
		this.managerCompanyIds = managerCompanyIds;
	}


	public String getManagerCompanyName() {
		return this.managerCompanyName;
	}


	public void setManagerCompanyName(String managerCompanyName) {
		this.managerCompanyName = managerCompanyName;
	}


	public Boolean getManagerCompanyOurCompany() {
		return this.managerCompanyOurCompany;
	}


	public void setManagerCompanyOurCompany(Boolean managerCompanyOurCompany) {
		this.managerCompanyOurCompany = managerCompanyOurCompany;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getAccountNumberEquals() {
		return this.accountNumberEquals;
	}


	public void setAccountNumberEquals(String accountNumberEquals) {
		this.accountNumberEquals = accountNumberEquals;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getAccountNameNumber() {
		return this.accountNameNumber;
	}


	public void setAccountNameNumber(String accountNameNumber) {
		this.accountNameNumber = accountNameNumber;
	}


	public Integer getCustodianCompanyId() {
		return this.custodianCompanyId;
	}


	public void setCustodianCompanyId(Integer custodianCompanyId) {
		this.custodianCompanyId = custodianCompanyId;
	}


	public String getCustodianAccountNumber() {
		return this.custodianAccountNumber;
	}


	public void setCustodianAccountNumber(String custodianAccountNumber) {
		this.custodianAccountNumber = custodianAccountNumber;
	}


	public Integer getCustodianAccountId() {
		return this.custodianAccountId;
	}


	public void setCustodianAccountId(Integer custodianAccountId) {
		this.custodianAccountId = custodianAccountId;
	}


	public Boolean getZeroBalanceIfNoDownload() {
		return this.zeroBalanceIfNoDownload;
	}


	public void setZeroBalanceIfNoDownload(Boolean zeroBalanceIfNoDownload) {
		this.zeroBalanceIfNoDownload = zeroBalanceIfNoDownload;
	}


	public InvestmentManagerAccount.CashAdjustmentType getCashAdjustmentType() {
		return this.cashAdjustmentType;
	}


	public void setCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType) {
		this.cashAdjustmentType = cashAdjustmentType;
	}


	public BigDecimal getCustomCashValue() {
		return this.customCashValue;
	}


	public void setCustomCashValue(BigDecimal customCashValue) {
		this.customCashValue = customCashValue;
	}


	public Date getCustomCashValueUpdateDate() {
		return this.customCashValueUpdateDate;
	}


	public void setCustomCashValueUpdateDate(Date customCashValueUpdateDate) {
		this.customCashValueUpdateDate = customCashValueUpdateDate;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public ManagerTypes getManagerType() {
		return this.managerType;
	}


	public void setManagerType(ManagerTypes managerType) {
		this.managerType = managerType;
	}


	public LinkedManagerTypes getLinkedManagerType() {
		return this.linkedManagerType;
	}


	public void setLinkedManagerType(LinkedManagerTypes linkedManagerType) {
		this.linkedManagerType = linkedManagerType;
	}


	public ManagerTypes getExcludeProxyManagers() {
		return this.excludeProxyManagers;
	}


	public void setExcludeProxyManagers(ManagerTypes excludeProxyManagers) {
		this.excludeProxyManagers = excludeProxyManagers;
	}


	public ManagerTypes getExcludeRollupManagers() {
		return this.excludeRollupManagers;
	}


	public void setExcludeRollupManagers(ManagerTypes excludeRollupManagers) {
		this.excludeRollupManagers = excludeRollupManagers;
	}


	public ManagerTypes getExcludeLinkedManagers() {
		return this.excludeLinkedManagers;
	}


	public void setExcludeLinkedManagers(ManagerTypes excludeLinkedManagers) {
		this.excludeLinkedManagers = excludeLinkedManagers;
	}


	public String getLinkedInvestmentAccountLabel() {
		return this.linkedInvestmentAccountLabel;
	}


	public void setLinkedInvestmentAccountLabel(String linkedInvestmentAccountLabel) {
		this.linkedInvestmentAccountLabel = linkedInvestmentAccountLabel;
	}


	public String getBenchmarkSecurityLabel() {
		return this.benchmarkSecurityLabel;
	}


	public void setBenchmarkSecurityLabel(String benchmarkSecurityLabel) {
		this.benchmarkSecurityLabel = benchmarkSecurityLabel;
	}


	public String getDurationFieldNameOverride() {
		return this.durationFieldNameOverride;
	}


	public void setDurationFieldNameOverride(String durationFieldNameOverride) {
		this.durationFieldNameOverride = durationFieldNameOverride;
	}


	public Boolean getProxyValueGrowthInverted() {
		return this.proxyValueGrowthInverted;
	}


	public void setProxyValueGrowthInverted(Boolean proxyValueGrowthInverted) {
		this.proxyValueGrowthInverted = proxyValueGrowthInverted;
	}


	public BigDecimal getProxyValue() {
		return this.proxyValue;
	}


	public void setProxyValue(BigDecimal proxyValue) {
		this.proxyValue = proxyValue;
	}


	public BigDecimal getProxySecurityQuantity() {
		return this.proxySecurityQuantity;
	}


	public void setProxySecurityQuantity(BigDecimal proxySecurityQuantity) {
		this.proxySecurityQuantity = proxySecurityQuantity;
	}


	public Date getProxyValueDate() {
		return this.proxyValueDate;
	}


	public void setProxyValueDate(Date proxyValueDate) {
		this.proxyValueDate = proxyValueDate;
	}


	public String getProxyBenchmarkSecurityLabel() {
		return this.proxyBenchmarkSecurityLabel;
	}


	public void setProxyBenchmarkSecurityLabel(String proxyBenchmarkSecurityLabel) {
		this.proxyBenchmarkSecurityLabel = proxyBenchmarkSecurityLabel;
	}


	public String getProxyBenchmarkSecurityDisplayName() {
		return this.proxyBenchmarkSecurityDisplayName;
	}


	public void setProxyBenchmarkSecurityDisplayName(String proxyBenchmarkSecurityDisplayName) {
		this.proxyBenchmarkSecurityDisplayName = proxyBenchmarkSecurityDisplayName;
	}


	public String getProxyBenchmarkRateIndexLabel() {
		return this.proxyBenchmarkRateIndexLabel;
	}


	public void setProxyBenchmarkRateIndexLabel(String proxyBenchmarkRateIndexLabel) {
		this.proxyBenchmarkRateIndexLabel = proxyBenchmarkRateIndexLabel;
	}


	public Boolean getProxySecuritiesOnly() {
		return this.proxySecuritiesOnly;
	}


	public void setProxySecuritiesOnly(Boolean proxySecuritiesOnly) {
		this.proxySecuritiesOnly = proxySecuritiesOnly;
	}


	public Boolean getProxyToLastKnownPrice() {
		return this.proxyToLastKnownPrice;
	}


	public void setProxyToLastKnownPrice(Boolean proxyToLastKnownPrice) {
		this.proxyToLastKnownPrice = proxyToLastKnownPrice;
	}


	public Integer getAssignmentAccountId() {
		return this.assignmentAccountId;
	}


	public void setAssignmentAccountId(Integer assignmentAccountId) {
		this.assignmentAccountId = assignmentAccountId;
	}


	public Integer[] getAssignmentAccountIds() {
		return this.assignmentAccountIds;
	}


	public void setAssignmentAccountIds(Integer[] assignmentAccountIds) {
		this.assignmentAccountIds = assignmentAccountIds;
	}


	public Boolean getAssignmentInactive() {
		return this.assignmentInactive;
	}


	public void setAssignmentInactive(Boolean assignmentInactive) {
		this.assignmentInactive = assignmentInactive;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Boolean getSweepAccount() {
		return this.sweepAccount;
	}


	public void setSweepAccount(Boolean sweepAccount) {
		this.sweepAccount = sweepAccount;
	}


	public Integer getClientOrClientRelationshipCompanyId() {
		return this.clientOrClientRelationshipCompanyId;
	}


	public void setClientOrClientRelationshipCompanyId(Integer clientOrClientRelationshipCompanyId) {
		this.clientOrClientRelationshipCompanyId = clientOrClientRelationshipCompanyId;
	}


	public String getRollupAggregationType() {
		return this.rollupAggregationType;
	}


	public void setRollupAggregationType(String rollupAggregationType) {
		this.rollupAggregationType = rollupAggregationType;
	}
}
