package com.clifton.investment.manager.balance.cache;

import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;

import java.util.Date;


/**
 * The <code>InvestmentManagerAccountBalanceDateCacheImpl</code> caches key ManagerAccountID_BalanceDate to the balance record id that represents it
 * <p>
 * Clears the record from the cache ONLY when balance record is deleted - since it stores id values and manger/balance date can't change update changes are unnecessary to track
 *
 * @author manderson
 */
public interface InvestmentManagerAccountBalanceDateCache {

	public Integer getInvestmentManagerAccountBalanceId(Integer managerAccountId, Date balanceDate);


	public void storeInvestmentManagerAccountBalanceId(InvestmentManagerAccountBalance balance);
}
