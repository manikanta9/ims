package com.clifton.investment.manager;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.bean.SystemBean;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountAssignment</code> links Client {@link InvestmentManagerAccount}s to {@link InvestmentAccount}s
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountAssignment extends ManyToManyEntity<InvestmentManagerAccount, InvestmentAccount, Integer> {

	public static final String TABLE_NAME = "InvestmentManagerAccountAssignment";

	private String displayName;
	private boolean inactive;

	/**
	 * Default cash bucket cash is overlaid into.  Can be overridden by custom cash allocations
	 */
	private ManagerCashTypes cashType = ManagerCashTypes.MANAGER;

	/**
	 * How cash is overlaid into the selected bucket(s) - Default Rules
	 * Additional custom rules can be defined
	 */
	private ManagerCashOverlayTypes cashOverlayType = ManagerCashOverlayTypes.NONE;

	/**
	 * How custom rules are applied (percentage, values, etc.)
	 */
	private ManagerCustomCashOverlayTypes customCashOverlayType = ManagerCustomCashOverlayTypes.NONE;

	private ManagerCustomCashAllocationTypes customCashAllocationType;

	/**
	 * Custom Cash Overlay Allocations
	 */
	private List<InvestmentManagerAccountAllocation> customCashOverlayAllocationList;

	/**
	 * Allocations
	 */
	private List<InvestmentManagerAccountAllocation> allocationList;

	/**
	 * NOTE: Not saved in the database, but used when entering allocations on an assignment from UI
	 * if we should enforce that the total = 100%
	 * Will only do the verification if the value is false, if it's NULL or true, then no additional validation
	 */
	@NonPersistentField
	private Boolean allowPartialAllocation;

	/**
	 * Even though we cannot rebalance at manager level, some clients want to establish targets for specific managers and do physical rebalancing (performed by client; not us) when actual manager allocation deviates from the target more than the threshold.
	 * We only need to capture this information and display a warning in client's PIOS report when manager target's outside of a threshold.
	 * <p/>
	 * All fields are optional, but if TargetAllocationPercent is populated, then the min/max fields will be required.
	 */
	private BigDecimal targetAllocationPercent;
	private boolean rebalanceAllocationFixed;
	private BigDecimal rebalanceAllocationMin;
	private BigDecimal rebalanceAllocationMax;

	/**
	 * Used for LDI Managers Only
	 */
	private InvestmentSecurity benchmarkSecurity;

	/**
	 * Used during portfolio run processing to allocate the securities balance to the replication's AdditionalExposure.
	 * If not selected, the securities balance is not allocated to the replications.
	 * <p/>
	 * This is currently used for the PPA Commodity Fund where the Commodities Asset class contains Commodity Swaps, but the account also holds DJP ETN whose market value
	 * needs to be allocated to the replications matching by the DJ-UBS structured index weights.  i.e. Natural Gas is allocated ~ 14% of DJP Market Value
	 */
	private SystemBean securitiesBalanceReplicationAllocatorBean;


	public String getManagerLabel() {
		if (!StringUtils.isEmpty(getDisplayName())) {
			return getDisplayName();
		}
		else if (getReferenceOne() != null) {
			if (!StringUtils.isEmpty(getReferenceOne().getAccountName())) {
				return getReferenceOne().getAccountName();
			}
			if (getReferenceOne().getManagerCompany() != null) {
				return getReferenceOne().getManagerCompany().getName();
			}
		}
		return null;
	}


	public String getManagerAccountLabel() {
		if (getReferenceOne() != null) {
			return getReferenceOne().getAccountNumber() + ": " + getManagerLabel();
		}
		return getManagerLabel();
	}


	public String getCashTypeLabel() {
		if (getCashType() != null) {
			return getCashType().getLabel();
		}
		return null;
	}


	public String getCashOverlayTypeLabel() {
		if (getCashOverlayType() != null) {
			return getCashOverlayType().getLabel();
		}
		return null;
	}


	public boolean isOverlayCash() {
		return ManagerCashOverlayTypes.NONE != getCashOverlayType() || ManagerCustomCashOverlayTypes.NONE != getCustomCashOverlayType();
	}


	public boolean isOverlayCashCustom() {
		return ManagerCustomCashOverlayTypes.NONE != getCustomCashOverlayType();
	}


	public boolean isTargetAllocationExists() {
		return getTargetAllocationPercent() != null;
	}


	public boolean isAllocateSecuritiesBalanceToReplications() {
		return getSecuritiesBalanceReplicationAllocatorBean() != null;
	}


	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	public List<InvestmentManagerAccountAllocation> getAllocationList() {
		return this.allocationList;
	}


	public void setAllocationList(List<InvestmentManagerAccountAllocation> allocationList) {
		this.allocationList = allocationList;
	}


	public String getDisplayName() {
		return this.displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public ManagerCashOverlayTypes getCashOverlayType() {
		return this.cashOverlayType;
	}


	public void setCashOverlayType(ManagerCashOverlayTypes cashOverlayType) {
		this.cashOverlayType = cashOverlayType;
	}


	public ManagerCustomCashOverlayTypes getCustomCashOverlayType() {
		return this.customCashOverlayType;
	}


	public void setCustomCashOverlayType(ManagerCustomCashOverlayTypes customCashOverlayType) {
		this.customCashOverlayType = customCashOverlayType;
	}


	public ManagerCustomCashAllocationTypes getCustomCashAllocationType() {
		return this.customCashAllocationType;
	}


	public void setCustomCashAllocationType(ManagerCustomCashAllocationTypes customCashAllocationType) {
		this.customCashAllocationType = customCashAllocationType;
	}


	public List<InvestmentManagerAccountAllocation> getCustomCashOverlayAllocationList() {
		return this.customCashOverlayAllocationList;
	}


	public void setCustomCashOverlayAllocationList(List<InvestmentManagerAccountAllocation> customCashOverlayAllocationList) {
		this.customCashOverlayAllocationList = customCashOverlayAllocationList;
	}


	public BigDecimal getTargetAllocationPercent() {
		return this.targetAllocationPercent;
	}


	public void setTargetAllocationPercent(BigDecimal targetAllocationPercent) {
		this.targetAllocationPercent = targetAllocationPercent;
	}


	public boolean isRebalanceAllocationFixed() {
		return this.rebalanceAllocationFixed;
	}


	public void setRebalanceAllocationFixed(boolean rebalanceAllocationFixed) {
		this.rebalanceAllocationFixed = rebalanceAllocationFixed;
	}


	public BigDecimal getRebalanceAllocationMin() {
		return this.rebalanceAllocationMin;
	}


	public void setRebalanceAllocationMin(BigDecimal rebalanceAllocationMin) {
		this.rebalanceAllocationMin = rebalanceAllocationMin;
	}


	public BigDecimal getRebalanceAllocationMax() {
		return this.rebalanceAllocationMax;
	}


	public void setRebalanceAllocationMax(BigDecimal rebalanceAllocationMax) {
		this.rebalanceAllocationMax = rebalanceAllocationMax;
	}


	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public ManagerCashTypes getCashType() {
		return this.cashType;
	}


	public void setCashType(ManagerCashTypes cashType) {
		this.cashType = cashType;
	}


	public SystemBean getSecuritiesBalanceReplicationAllocatorBean() {
		return this.securitiesBalanceReplicationAllocatorBean;
	}


	public void setSecuritiesBalanceReplicationAllocatorBean(SystemBean securitiesBalanceReplicationAllocatorBean) {
		this.securitiesBalanceReplicationAllocatorBean = securitiesBalanceReplicationAllocatorBean;
	}


	public Boolean getAllowPartialAllocation() {
		return this.allowPartialAllocation;
	}


	public void setAllowPartialAllocation(Boolean allowPartialAllocation) {
		this.allowPartialAllocation = allowPartialAllocation;
	}
}
