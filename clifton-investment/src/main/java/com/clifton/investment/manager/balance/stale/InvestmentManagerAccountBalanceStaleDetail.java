package com.clifton.investment.manager.balance.stale;


import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


public class InvestmentManagerAccountBalanceStaleDetail extends InvestmentManagerAccountBalance {

	private static final BigDecimal STALE_THRESHOLD = BigDecimal.ONE;

	private BigDecimal difference;
	private BigDecimal adjustedDifference;


	public boolean isStale() {
		return MathUtils.compare(MathUtils.abs(getDifference()), STALE_THRESHOLD) <= 0;
	}


	public BigDecimal getDifference() {
		return this.difference;
	}


	public void setDifference(BigDecimal difference) {
		this.difference = difference;
	}


	public BigDecimal getAdjustedDifference() {
		return this.adjustedDifference;
	}


	public void setAdjustedDifference(BigDecimal adjustedDifference) {
		this.adjustedDifference = adjustedDifference;
	}
}
