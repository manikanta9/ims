package com.clifton.investment.manager.upload;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;


/**
 * The <code>InvestmentManagerAccountUploadService</code> contains methods for uploading data from an external file into manager related tables
 *
 * @author manderson
 */
public interface InvestmentManagerAccountUploadService {


	////////////////////////////////////////////////////////////////////////////
	///////     Investment Manager Account Allocation Upload Methods     ///////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = InvestmentManagerAccountAllocation.class)
	public void uploadInvestmentManagerAccountAllocationUploadFile(InvestmentManagerAccountAllocationUploadCommand uploadCommand);
}
