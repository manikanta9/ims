package com.clifton.investment.manager.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.setup.InvestmentAssetClass;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>InvestmentManagerAccountUploadServiceImpl</code>
 *
 * @author manderson
 */
@Service
public class InvestmentManagerAccountUploadServiceImpl extends BaseInvestmentManagerAccountUploadService implements InvestmentManagerAccountUploadService {

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	////////////////////////////////////////////////////////////////////////////
	///////     Investment Manager Account Allocation Upload Methods     ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadInvestmentManagerAccountAllocationUploadFile(InvestmentManagerAccountAllocationUploadCommand uploadCommand) {
		uploadCommand.clearResults();
		List<InvestmentManagerAccountAllocationUploadTemplate> beanList = getFileUploadHandler().convertFileUploadFileToBeanList(uploadCommand);
		ValidationUtils.assertNotEmpty(beanList, "No manager allocations found to update.");

		boolean errors = false;
		Map<String, List<InvestmentManagerAccountAllocationUploadTemplate>> managerAssignmentUpdateMap = BeanUtils.getBeansMap(beanList, InvestmentManagerAccountAllocationUploadTemplate::getManagerAssignmentKey);

		List<InvestmentManagerAccountAssignment> saveManagerAssignmentList = new ArrayList<>();

		for (Map.Entry<String, List<InvestmentManagerAccountAllocationUploadTemplate>> stringListEntry : managerAssignmentUpdateMap.entrySet()) {
			List<InvestmentManagerAccountAllocationUploadTemplate> allocationUploadTemplateList = stringListEntry.getValue();
			// Returns the Active Manager Account Assignment based on the first allocation in the list
			// If returns null, then there was an error and the uploadCommand results will be updated with why
			InvestmentManagerAccountAssignment managerAccountAssignment = getManagerAccountAssignment(uploadCommand, allocationUploadTemplateList.get(0));
			if (managerAccountAssignment == null) {
				errors = true;
				continue;
			}

			Set<String> processedAssetClasses = new HashSet<>();
			Set<String> managerAssignmentErrors = new HashSet<>();

			// Assignment already has existing allocations populated
			List<InvestmentManagerAccountAllocation> saveAllocationList = new ArrayList<>();
			for (InvestmentManagerAccountAllocationUploadTemplate newAllocation : allocationUploadTemplateList) {
				String assetClassName = newAllocation.getAssetClassName();
				if (processedAssetClasses.contains(assetClassName)) {
					managerAssignmentErrors.add("Multiple rows allocated to asset class [" + assetClassName + "].");
					continue;
				}
				processedAssetClasses.add(assetClassName);
				// Find the existing allocation if any
				InvestmentManagerAccountAllocation existingAllocation = CollectionUtils.getOnlyElement(BeanUtils.filter(managerAccountAssignment.getAllocationList(), investmentManagerAccountAllocation -> investmentManagerAccountAllocation.getAssetClass().getName(), assetClassName));
				if (existingAllocation == null) {
					if (!uploadCommand.isAllowAddingOrRemovingAllocations()) {
						managerAssignmentErrors.add("There is not an existing allocation for asset class [" + assetClassName + "], and the option to insert new allocations is turned off.");
						continue;
					}
					// Otherwise create it as a new allocation and add it to the saveList
					InvestmentAssetClass assetClass = getInvestmentAssetClass(uploadCommand, managerAssignmentErrors, managerAccountAssignment.getReferenceTwo(), assetClassName);
					if (assetClass == null) {
						continue;
					}
					InvestmentManagerAccountAllocation managerAccountAllocation = new InvestmentManagerAccountAllocation();
					managerAccountAllocation.setManagerAccountAssignment(managerAccountAssignment);
					managerAccountAllocation.setAssetClass(assetClass);
					managerAccountAllocation.setAllocationPercent(newAllocation.getAllocationPercent());
					saveAllocationList.add(managerAccountAllocation);
				}
				else {
					existingAllocation.setAllocationPercent(newAllocation.getAllocationPercent());
					saveAllocationList.add(existingAllocation);
				}
			}

			// If Not Allowed to Removed existing allocations - see what is missing from the list and add it back into the save list
			if (!uploadCommand.isAllowAddingOrRemovingAllocations()) {
				for (InvestmentManagerAccountAllocation existingAllocation : CollectionUtils.getIterable(managerAccountAssignment.getAllocationList())) {
					if (!processedAssetClasses.contains(existingAllocation.getAssetClass().getName())) {
						saveAllocationList.add(existingAllocation);
					}
				}
			}
			if (CollectionUtils.isEmpty(managerAssignmentErrors) && !uploadCommand.isAllowPartiallyAllocatedManagers()) {
				BigDecimal totalAllocated = CoreMathUtils.sumProperty(saveAllocationList, InvestmentManagerAccountAllocation::getAllocationPercent);
				if (MathUtils.isNotEqual(totalAllocated, MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
					managerAssignmentErrors.add("The total allocated is " + CoreMathUtils.formatNumberDecimal(totalAllocated) + " but you have chosen to enforce total to 100.  Please double check your file.  If you are not allowing removal of existing allocations - those asset classes not included in the file will be included at their existing percentage.");
				}
			}

			if (!CollectionUtils.isEmpty(managerAssignmentErrors)) {
				errors = true;
				uploadCommand.appendResult("Manager Assignment [" + managerAccountAssignment.getLabel() + "] Errors: " + StringUtils.collectionToCommaDelimitedString(managerAssignmentErrors));
			}
			else {
				// Update the Assignment with the New List
				managerAccountAssignment.setAllocationList(saveAllocationList);
				saveManagerAssignmentList.add(managerAccountAssignment);
			}
		}

		if (!errors || uploadCommand.isPartialUploadAllowed()) {
			getInvestmentManagerAccountService().saveInvestmentManagerAccountAssignmentList(saveManagerAssignmentList);
			uploadCommand.prependResult(CollectionUtils.getSize(saveManagerAssignmentList) + " Manager Assignments Updated.");
		}
		else {
			uploadCommand.prependResult("No Manager Assignments Updated.");
		}
	}


	/**
	 * Returns the {@link InvestmentManagerAccountAssignment} to use for the template row.  Uses a map for client accounts and manager accounts for common retrievals to prevent going to the database
	 * each time.  The assignment look up is not cached in a map because there can't be more than one unique assignment per file.
	 * Note: All existing allocations are already populated on the assignment bean that is returned.
	 */
	private InvestmentManagerAccountAssignment getManagerAccountAssignment(InvestmentManagerAccountAllocationUploadCommand uploadCommand, InvestmentManagerAccountAllocationUploadTemplate uploadTemplate) {
		InvestmentAccount clientAccount = getClientAccount(uploadCommand, uploadTemplate);
		InvestmentManagerAccount managerAccount = getManagerAccount(uploadCommand, uploadTemplate);
		if (clientAccount == null || managerAccount == null) {
			return null;
		}
		InvestmentManagerAccountAssignment managerAccountAssignment = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentByManagerAndAccount(managerAccount.getId(), clientAccount.getId());
		if (managerAccountAssignment == null) {
			uploadCommand.appendResult("Cannot find manager assignment for client account [" + clientAccount.getNumber() + "] and manager account [" + managerAccount.getAccountNumber() + "]");
		}
		return managerAccountAssignment;
	}


	/**
	 * Returns the {@link InvestmentAssetClass} to use for the given account and asset class.  Checks against valid asset class selections for the account
	 * which are stored in a map for easy retrieval.
	 */
	private InvestmentAssetClass getInvestmentAssetClass(InvestmentManagerAccountAllocationUploadCommand uploadCommand, Set<String> managerAssignmentErrors, InvestmentAccount clientAccount, String assetClassName) {
		List<InvestmentAccountAssetClass> accountAssetClassList = uploadCommand.getClientAccountAssetClassListMap().get(clientAccount.getId());
		if (accountAssetClassList == null) {
			// Get a list of valid asset classes -- tied to account & currently active
			accountAssetClassList = getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassListByAccount(clientAccount.getId());
			// Exclude Rollup Asset Classes from selectable options
			accountAssetClassList = BeanUtils.filter(accountAssetClassList, InvestmentAccountAssetClass::isRollupAssetClass, false);
			uploadCommand.getClientAccountAssetClassListMap().put(clientAccount.getId(), accountAssetClassList);
		}
		InvestmentAccountAssetClass accountAssetClass = CollectionUtils.getOnlyElement(BeanUtils.filter(accountAssetClassList, investmentAccountAssetClass -> investmentAccountAssetClass.getAssetClass().getName(), assetClassName));
		if (accountAssetClass == null) {
			managerAssignmentErrors.add("Cannot find active non-rollup asset class associated with this account with name [" + assetClassName + "]");
		}
		return (accountAssetClass != null ? accountAssetClass.getAssetClass() : null);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////           Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}
}
