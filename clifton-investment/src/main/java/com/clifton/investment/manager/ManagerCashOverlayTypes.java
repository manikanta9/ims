package com.clifton.investment.manager;


/**
 * The <code>ManagerCashOverlayType</code> defines how the system should overlay manager cash
 * i.e. Allocate using Fund Cash Weights (does not necessarily mean that manager cash is put into fund cash bucket - that is defined by the ManagerCashType option)
 *
 * @author manderson
 */
public enum ManagerCashOverlayTypes {

	NONE("None"), // Not Overlaid
	FUND("Fund Cash Weights"), // Using Account Fund Cash % ages
	MANAGER("Manager Asset Class Allocation"), // Using Manager Asset Class(es) and % ages
	MINIMIZE_IMBALANCES("Minimize Imbalances");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String label;


	ManagerCashOverlayTypes(String label) {
		this.label = label;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}
}
