package com.clifton.investment.manager.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentManagerAccountAllocationListByAssignmentCache</code> stores the list of {@link InvestmentManagerAccountAllocation} objects
 * by assignment id.
 *
 * @author manderson
 */
@Component
public class InvestmentManagerAccountAllocationListByAssignmentCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentManagerAccountAllocation, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "managerAccountAssignment.id";
	}


	@Override
	protected Integer getBeanKeyValue(InvestmentManagerAccountAllocation bean) {
		if (bean.getManagerAccountAssignment() != null) {
			return bean.getManagerAccountAssignment().getId();
		}
		return null;
	}
}
