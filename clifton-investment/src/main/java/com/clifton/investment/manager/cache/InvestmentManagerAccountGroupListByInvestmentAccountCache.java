package com.clifton.investment.manager.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.manager.InvestmentManagerAccountGroup;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentManagerAccountGroupListByInvestmentAccountCache</code> caches a list of {@link InvestmentManagerAccountGroup} objects by Investment Account.
 * There are very few accounts that use manager account groups, so for most accounts this list will be empty
 *
 * @author manderson
 */
@Component
public class InvestmentManagerAccountGroupListByInvestmentAccountCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentManagerAccountGroup, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "investmentAccount.id";
	}


	@Override
	protected Integer getBeanKeyValue(InvestmentManagerAccountGroup bean) {
		if (bean.getInvestmentAccount() != null) {
			return bean.getInvestmentAccount().getId();
		}
		return null;
	}
}
