package com.clifton.investment.manager;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * <code>BaseInvestmentManagerAllocation</code> is contains common data for a manager allocation
 * and can be extended for type specific relationships.
 *
 * @author nickk
 */
public abstract class BaseInvestmentManagerAllocation extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentManagerAccountAssignment managerAccountAssignment;

	private BigDecimal allocationPercent;

	/**
	 * Will be copied on runs to indicate if the allocation is included in reports
	 */
	private boolean privateAssignment;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return new StringBuilder("[")
				.append((getManagerAccountAssignment() == null) ? "UNKNOWN" : this.managerAccountAssignment.getLabel())
				.append("] - [").append(getTypeSpecificLabelSuffix()).append(']')
				.toString();
	}


	protected abstract String getTypeSpecificLabelSuffix();


	public BigDecimal getAbsAllocationPercent() {
		return MathUtils.abs(getAllocationPercent());
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountAssignment getManagerAccountAssignment() {
		return this.managerAccountAssignment;
	}


	public void setManagerAccountAssignment(InvestmentManagerAccountAssignment managerAccountAssignment) {
		this.managerAccountAssignment = managerAccountAssignment;
	}


	public BigDecimal getAllocationPercent() {
		return this.allocationPercent;
	}


	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}


	public boolean isPrivateAssignment() {
		return this.privateAssignment;
	}


	public void setPrivateAssignment(boolean privateAssignment) {
		this.privateAssignment = privateAssignment;
	}
}
