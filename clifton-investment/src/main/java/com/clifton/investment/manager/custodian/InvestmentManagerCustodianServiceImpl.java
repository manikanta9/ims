package com.clifton.investment.manager.custodian;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.custodian.search.InvestmentManagerCustodianAccountSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InvestmentManagerCustodianServiceImpl implements InvestmentManagerCustodianService {

	private AdvancedUpdatableDAO<InvestmentManagerCustodianAccount, Criteria> investmentManagerCustodianAccountDAO;
	private DaoCompositeKeyCache<InvestmentManagerCustodianAccount, Integer, String> investmentManagerCustodianAccountCache;

	/////////////////////////////////////////////////////////////////////////////////////
	//////                InvestmentManagerCustodianAccount Methods                 /////
	/////////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerCustodianAccount getInvestmentManagerCustodianAccount(int id) {
		return getInvestmentManagerCustodianAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentManagerCustodianAccount getInvestmentManagerCustodianAccountByNumberAndCompany(String number, Integer companyId) {
		ValidationUtils.assertNotNull(number, "Custodian account 'number' cannot be null, it is required to look up a custodian account.");
		return getInvestmentManagerCustodianAccountCache().getBeanForKeyValues(getInvestmentManagerCustodianAccountDAO(), companyId, number);
	}


	@Override
	public List<InvestmentManagerCustodianAccount> getInvestmentManagerCustodianAccountList(InvestmentManagerCustodianAccountSearchForm searchForm) {
		return getInvestmentManagerCustodianAccountDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentManagerCustodianAccount saveInvestmentManagerCustodianAccount(InvestmentManagerCustodianAccount bean) {
		return getInvestmentManagerCustodianAccountDAO().save(bean);
	}


	@Override
	public void deleteInvestmentManagerCustodianAccount(int id) {
		getInvestmentManagerCustodianAccountDAO().delete(id);
	}


	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentManagerCustodianAccount, Criteria> getInvestmentManagerCustodianAccountDAO() {
		return this.investmentManagerCustodianAccountDAO;
	}


	public void setInvestmentManagerCustodianAccountDAO(AdvancedUpdatableDAO<InvestmentManagerCustodianAccount, Criteria> investmentManagerCustodianAccountDAO) {
		this.investmentManagerCustodianAccountDAO = investmentManagerCustodianAccountDAO;
	}


	public DaoCompositeKeyCache<InvestmentManagerCustodianAccount, Integer, String> getInvestmentManagerCustodianAccountCache() {
		return this.investmentManagerCustodianAccountCache;
	}


	public void setInvestmentManagerCustodianAccountCache(DaoCompositeKeyCache<InvestmentManagerCustodianAccount, Integer, String> investmentManagerCustodianAccountCache) {
		this.investmentManagerCustodianAccountCache = investmentManagerCustodianAccountCache;
	}
}
