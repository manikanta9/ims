package com.clifton.investment.manager.balance.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentManagerAccountBalanceAdjustmentTypeSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountBalanceAdjustmentTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Boolean cashAdjustmentAllowed;

	@SearchField
	private Boolean securitiesAdjustmentAllowed;

	@SearchField
	private Boolean marketOnClose;

	@SearchField
	private Boolean noteRequired;

	@SearchField(searchField = "nextDayAdjustmentType.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean nextDayAdjustmentExists;

	@SearchField(searchFieldPath = "nextDayAdjustmentType", searchField = "name")
	private String nextDayAdjustmentTypeName;


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getCashAdjustmentAllowed() {
		return this.cashAdjustmentAllowed;
	}


	public void setCashAdjustmentAllowed(Boolean cashAdjustmentAllowed) {
		this.cashAdjustmentAllowed = cashAdjustmentAllowed;
	}


	public Boolean getSecuritiesAdjustmentAllowed() {
		return this.securitiesAdjustmentAllowed;
	}


	public void setSecuritiesAdjustmentAllowed(Boolean securitiesAdjustmentAllowed) {
		this.securitiesAdjustmentAllowed = securitiesAdjustmentAllowed;
	}


	public Boolean getMarketOnClose() {
		return this.marketOnClose;
	}


	public void setMarketOnClose(Boolean marketOnClose) {
		this.marketOnClose = marketOnClose;
	}


	public Boolean getNoteRequired() {
		return this.noteRequired;
	}


	public void setNoteRequired(Boolean noteRequired) {
		this.noteRequired = noteRequired;
	}


	public Boolean getNextDayAdjustmentExists() {
		return this.nextDayAdjustmentExists;
	}


	public void setNextDayAdjustmentExists(Boolean nextDayAdjustmentExists) {
		this.nextDayAdjustmentExists = nextDayAdjustmentExists;
	}


	public String getNextDayAdjustmentTypeName() {
		return this.nextDayAdjustmentTypeName;
	}


	public void setNextDayAdjustmentTypeName(String nextDayAdjustmentTypeName) {
		this.nextDayAdjustmentTypeName = nextDayAdjustmentTypeName;
	}
}
