package com.clifton.investment.manager;


import com.clifton.core.beans.NamedEntity;
import com.clifton.investment.account.InvestmentAccount;

import java.util.List;


/**
 * The <code>InvestmentManagerAccountGroup</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountGroup extends NamedEntity<Integer> {


	private InvestmentAccount investmentAccount;

	private boolean inactive;

	private List<InvestmentManagerAccountGroupAllocation> allocationList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}


	public List<InvestmentManagerAccountGroupAllocation> getAllocationList() {
		return this.allocationList;
	}


	public void setAllocationList(List<InvestmentManagerAccountGroupAllocation> allocationList) {
		this.allocationList = allocationList;
	}
}
