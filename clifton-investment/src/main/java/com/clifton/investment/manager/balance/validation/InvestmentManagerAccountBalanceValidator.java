package com.clifton.investment.manager.balance.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentManagerAccountBalanceValidator</code>
 * ensures original cash/securities values cannot be changed (must use adjustments)
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentManagerAccountBalanceValidator extends SelfRegisteringDaoValidator<InvestmentManagerAccountBalance> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	public void validate(InvestmentManagerAccountBalance bean, DaoEventTypes config) throws ValidationException {
		if (config.isUpdate()) {
			InvestmentManagerAccountBalance originalBean = getOriginalBean(bean);

			if (originalBean == null) {
				throw new IllegalStateException("Cannot update manager balance " + bean.getLabel() + " because the original bean has been deleted (manager balance has been fully reloaded)");
			}

			if (MathUtils.isNotEqual(bean.getCashValue(), originalBean.getCashValue()) || MathUtils.isNotEqual(bean.getSecuritiesValue(), originalBean.getSecuritiesValue())) {
				throw new FieldValidationException(
						"The original cash and/or securities values entered on balance creation cannot be changed.  In order to change the account balance for a given date, please enter adjustments accordingly.",
						"cashValue");
			}
			if (DateUtils.compare(originalBean.getBalanceDate(), bean.getBalanceDate(), false) != 0) {
				throw new FieldValidationException("The balance date originally entered cannot be changed.  A new balance record should be created for entering balances for a different date.",
						"balanceDate");
			}
		}
	}
}
