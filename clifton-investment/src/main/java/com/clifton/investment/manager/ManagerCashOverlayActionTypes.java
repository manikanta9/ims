package com.clifton.investment.manager;


/**
 * The <code>ManagerCashOverlayActionTypes</code> defines the overlay action types
 * that can be associated with an {@link InvestmentManagerAccountAssignmentOverlayAction}
 * <p/>
 * i.e. Adjustments to adjust cash to be overlaid, Limit to cap cash to be overlaid
 *
 * @author manderson
 */
public enum ManagerCashOverlayActionTypes {

	ADJUSTMENT(false), LIMIT(true);

	/**
	 * If true (for limits) users can enter both an amount and percent value
	 * If false (for adjustments) can enter ONLY one or the other
	 */
	private final boolean allowAmountAndPercent;


	ManagerCashOverlayActionTypes(boolean allowAmountAndPercent) {
		this.allowAmountAndPercent = allowAmountAndPercent;
	}


	public boolean isAllowAmountAndPercent() {
		return this.allowAmountAndPercent;
	}
}
