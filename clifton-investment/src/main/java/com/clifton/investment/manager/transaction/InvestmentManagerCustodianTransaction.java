package com.clifton.investment.manager.transaction;

import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.manager.custodian.InvestmentManagerCustodianAccount;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentManagerCustodianTransaction</code> represents a cashflow event for a given
 * <code>InvestmentManagerCustodianAccount</code>
 *
 * @author theodorez
 */
public class InvestmentManagerCustodianTransaction extends BaseEntity<Integer> {

	private InvestmentManagerCustodianAccount custodianAccount;
	private Date transactionDate;
	private BigDecimal transactionAmount;
	private String description;
	/**
	 * Optional Soft Link to incoming data set
	 */
	private Integer sourceDataIdentifier;

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getTransactionAmountAbsoluteValue() {
		return MathUtils.abs(this.transactionAmount);
	}


	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerCustodianAccount getCustodianAccount() {
		return this.custodianAccount;
	}


	public void setCustodianAccount(InvestmentManagerCustodianAccount custodianAccount) {
		this.custodianAccount = custodianAccount;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}


	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getSourceDataIdentifier() {
		return this.sourceDataIdentifier;
	}


	public void setSourceDataIdentifier(Integer sourceDataIdentifier) {
		this.sourceDataIdentifier = sourceDataIdentifier;
	}
}
