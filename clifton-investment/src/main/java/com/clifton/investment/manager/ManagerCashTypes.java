package com.clifton.investment.manager;


import com.clifton.business.service.ServiceProcessingTypes;


/**
 * The <code>ManagerCashType</code> defines the different cash buckets that manager's cash can be put into
 *
 * @author manderson
 */
public enum ManagerCashTypes {

	// Overlay Manager Cash Types
	// Selectable Options for Setup and Processing
	MANAGER("Manager", ServiceProcessingTypes.PORTFOLIO_RUNS, true), //
	FUND("Fund", ServiceProcessingTypes.PORTFOLIO_RUNS, true), //
	TRANSITION("Transition", ServiceProcessingTypes.PORTFOLIO_RUNS, true), //
	// Non-Selectable Options - Used during Processing Only.  i.e. Values are not explicit, they are calculated during process, but still are valid "Cash Buckets" for Portfolio Run Results
	CLIENT_DIRECTED("Client Directed", ServiceProcessingTypes.PORTFOLIO_RUNS, false), // 
	REBALANCE("Rebalance", ServiceProcessingTypes.PORTFOLIO_RUNS, false), // 
	REBALANCE_ATTRIBUTION("Rebalance Attribution", ServiceProcessingTypes.PORTFOLIO_RUNS, false), //

	// LDI Manager Cash Types
	ASSETS("Assets", ServiceProcessingTypes.LDI, true), //
	LIABILITIES("Liabilities", ServiceProcessingTypes.LDI, true), //
	MARGIN("Margin", ServiceProcessingTypes.LDI, true); //


	ManagerCashTypes(String label, ServiceProcessingTypes serviceProcessingType, boolean selectableOption) {
		this.label = label;
		this.serviceProcessingType = serviceProcessingType;
		this.selectableOption = selectableOption;
	}


	private final String label;

	private final ServiceProcessingTypes serviceProcessingType;

	private final boolean selectableOption;


	public String getLabel() {
		return this.label;
	}


	public boolean isSelectableOption() {
		return this.selectableOption;
	}


	public ServiceProcessingTypes getServiceProcessingType() {
		return this.serviceProcessingType;
	}
}
