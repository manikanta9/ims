package com.clifton.investment.manager.custodian.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.investment.manager.custodian.InvestmentManagerCustodianAccount;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentManagerCustodianAccountCache</code> class provides caching and cache retrieval of
 * <code>InvestmentManagerCustodianAccount</code> by key: CompanyID_CustodianAccountNumber
 *
 * @author theodorez
 */
@Component
public class InvestmentManagerCustodianAccountCache extends SelfRegisteringCompositeKeyDaoCache<InvestmentManagerCustodianAccount, Integer, String> {


	@Override
	protected String getBeanKey1Property() {
		return "company.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "number";
	}


	@Override
	protected Integer getBeanKey1Value(InvestmentManagerCustodianAccount bean) {
		if (bean.getCompany() != null) {
			return bean.getCompany().getId();
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(InvestmentManagerCustodianAccount bean) {
		return bean.getNumber();
	}
}
