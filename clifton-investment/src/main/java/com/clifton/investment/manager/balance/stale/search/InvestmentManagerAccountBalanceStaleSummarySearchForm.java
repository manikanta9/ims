package com.clifton.investment.manager.balance.stale.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.investment.manager.InvestmentManagerAccount.ManagerTypes;

import java.util.Date;


public class InvestmentManagerAccountBalanceStaleSummarySearchForm extends BaseEntitySearchForm {

	// CUSTOM FILTER
	private Date balanceDate;

	// CUSTOM FILTER
	private Integer numberOfDaysForAverage;

	@SearchField(searchField = "custodianCompany.id")
	private Integer custodianCompanyId;

	@SearchField(searchFieldPath = "custodianCompany", searchField = "name")
	private String custodianCompanyName;

	// CUSTOM FILTER
	private ManagerTypes managerType;

	@SearchField
	private Integer staleCount;

	@SearchField
	private Integer averageStaleCount;

	// CUSTOM FILTER
	private Boolean adjustedCashValueGreaterThanZero;


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public Integer getCustodianCompanyId() {
		return this.custodianCompanyId;
	}


	public void setCustodianCompanyId(Integer custodianCompanyId) {
		this.custodianCompanyId = custodianCompanyId;
	}


	public String getCustodianCompanyName() {
		return this.custodianCompanyName;
	}


	public void setCustodianCompanyName(String custodianCompanyName) {
		this.custodianCompanyName = custodianCompanyName;
	}


	public ManagerTypes getManagerType() {
		return this.managerType;
	}


	public void setManagerType(ManagerTypes managerType) {
		this.managerType = managerType;
	}


	public Integer getNumberOfDaysForAverage() {
		return this.numberOfDaysForAverage;
	}


	public void setNumberOfDaysForAverage(Integer numberOfDaysForAverage) {
		this.numberOfDaysForAverage = numberOfDaysForAverage;
	}


	public Integer getStaleCount() {
		return this.staleCount;
	}


	public void setStaleCount(Integer staleCount) {
		this.staleCount = staleCount;
	}


	public Integer getAverageStaleCount() {
		return this.averageStaleCount;
	}


	public void setAverageStaleCount(Integer averageStaleCount) {
		this.averageStaleCount = averageStaleCount;
	}


	public Boolean getAdjustedCashValueGreaterThanZero() {
		return this.adjustedCashValueGreaterThanZero;
	}


	public void setAdjustedCashValueGreaterThanZero(Boolean adjustedCashValueGreaterThanZero) {
		this.adjustedCashValueGreaterThanZero = adjustedCashValueGreaterThanZero;
	}
}
