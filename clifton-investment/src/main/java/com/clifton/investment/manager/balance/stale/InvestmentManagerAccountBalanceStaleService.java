package com.clifton.investment.manager.balance.stale;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.stale.search.InvestmentManagerAccountBalanceStaleDetailSearchForm;
import com.clifton.investment.manager.balance.stale.search.InvestmentManagerAccountBalanceStaleSummarySearchForm;

import java.util.List;


public interface InvestmentManagerAccountBalanceStaleService {

	////////////////////////////////////////////////////////////////////////////
	//////           Investment Manager Account Balance Stale             ////// 
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = InvestmentManagerAccountBalance.class)
	public List<InvestmentManagerAccountBalanceStaleSummary> getInvestmentManagerAccountBalanceStaleSummaryList(InvestmentManagerAccountBalanceStaleSummarySearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	//////          Investment Manager Account Balance Extended           ////// 
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = InvestmentManagerAccountBalance.class)
	public List<InvestmentManagerAccountBalanceStaleDetail> getInvestmentManagerAccountBalanceStaleDetailList(InvestmentManagerAccountBalanceStaleDetailSearchForm searchForm);
}
