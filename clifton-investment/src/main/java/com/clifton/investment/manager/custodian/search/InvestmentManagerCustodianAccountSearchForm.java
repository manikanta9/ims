package com.clifton.investment.manager.custodian.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class InvestmentManagerCustodianAccountSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "number,company.name")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField
	private String number;

	@SearchField(searchField = "baseCurrency.id", sortField = "baseCurrency.symbol")
	private Integer baseCurrencyId;

	@SearchField(searchField = "company.id", sortField = "company.name")
	private Integer companyId;

	@SearchField(searchField = "holdingAccount.id")
	private Integer holdingAccountId;

	@SearchField
	private Boolean custodyOnlyAccount;

	@SearchField
	private Boolean sweepAccount;

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNumber() {
		return this.number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public Integer getCompanyId() {
		return this.companyId;
	}


	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Boolean getCustodyOnlyAccount() {
		return this.custodyOnlyAccount;
	}


	public void setCustodyOnlyAccount(Boolean custodyOnlyAccount) {
		this.custodyOnlyAccount = custodyOnlyAccount;
	}


	public Boolean getSweepAccount() {
		return this.sweepAccount;
	}


	public void setSweepAccount(Boolean sweepAccount) {
		this.sweepAccount = sweepAccount;
	}
}
