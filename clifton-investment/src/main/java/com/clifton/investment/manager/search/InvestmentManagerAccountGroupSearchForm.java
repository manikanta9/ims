package com.clifton.investment.manager.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class InvestmentManagerAccountGroupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "investmentAccount.id", sortField = "investmentAccount.number")
	private Integer investmentAccountId;

	@SearchField(searchField = "name,description,investmentAccount.name,investmentAccount.number")
	private String searchPattern;

	@SearchField
	private Boolean inactive;

	@SearchField
	private String name;

	@SearchField
	private String description;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
