package com.clifton.investment.manager.balance.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountBalanceAdjustmentCopyNextDayCacheImpl</code> class provides caching and retrieval from cache of InvestmentManagerAccountBalanceAdjustment objects
 * for a specific date where the adjustment type is flagged to be copied forward to the following day.
 * <p>
 * There are not very many of these daily, but we need them  in order to process the next day's balance.  Instead of looking them up
 * for each manager, lookup all at once and cache it.  Cache is automatically cleared if an adjustment for that date that is set to copy forward
 * is inserted, updated, or deleted.  Because this is not two business days ago, changes to the balance are very unlikely.
 *
 * @author manderson
 */
@Component
public class InvestmentManagerAccountBalanceAdjustmentCopyNextDayCacheImpl extends
		SelfRegisteringSimpleDaoCache<InvestmentManagerAccountBalanceAdjustment, Date, List<InvestmentManagerAccountBalanceAdjustment>> implements InvestmentManagerAccountBalanceAdjustmentCopyNextDayCache {


	@Override
	public List<InvestmentManagerAccountBalanceAdjustment> getInvestmentManagerAccountBalanceAdjustmentList(Date balanceDate) {
		List<InvestmentManagerAccountBalanceAdjustment> cachedList = getCacheHandler().get(getCacheName(), balanceDate);
		List<InvestmentManagerAccountBalanceAdjustment> result = null;
		// need to clone the list in case it gets modified
		if (!CollectionUtils.isEmpty(cachedList)) {
			result = new ArrayList<>(cachedList);
		}
		return result;
	}


	@Override
	public void storeInvestmentManagerAccountBalanceAdjustmentList(Date balanceDate, List<InvestmentManagerAccountBalanceAdjustment> list) {
		// Need to create a new list, because the reference to the list itself is modified by other methods which affects the reference in the cache.
		List<InvestmentManagerAccountBalanceAdjustment> newList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(list)) {
			newList.addAll(list);
		}
		getCacheHandler().put(getCacheName(), balanceDate, newList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentManagerAccountBalanceAdjustment> dao, @SuppressWarnings("unused") DaoEventTypes event,
	                                InvestmentManagerAccountBalanceAdjustment bean, Throwable e) {
		if (e == null) {
			if (bean.getAdjustmentType().getNextDayAdjustmentType() != null) {
				getCacheHandler().remove(getCacheName(), bean.getManagerAccountBalance().getBalanceDate());
			}
		}
	}
}
