package com.clifton.investment.manager.balance.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class InvestmentManagerAccountBalanceAdjustmentSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchFieldPath = "managerAccountBalance", searchField = "balanceDate")
	private Date balanceDate;

	@SearchField(searchFieldPath = "adjustmentType", searchField = "nextDayAdjustmentType.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean nextDayAdjustmentExists;


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public Boolean getNextDayAdjustmentExists() {
		return this.nextDayAdjustmentExists;
	}


	public void setNextDayAdjustmentExists(Boolean nextDayAdjustmentExists) {
		this.nextDayAdjustmentExists = nextDayAdjustmentExists;
	}
}
