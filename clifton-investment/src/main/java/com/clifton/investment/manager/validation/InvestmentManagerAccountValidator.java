package com.clifton.investment.manager.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccount.CashAdjustmentType;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountValidator</code>
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentManagerAccountValidator extends SelfRegisteringDaoValidator<InvestmentManagerAccount> {

	private InvestmentManagerAccountService investmentManagerAccountService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentManagerAccount bean, DaoEventTypes config) throws ValidationException {

		InvestmentManagerAccount originalBean = null;
		if (config.isUpdate()) {
			originalBean = getOriginalBean(bean);
		}

		if (!bean.isProxyManager()) {
			bean.setProxyValue(null);
			bean.setProxyValueGrowthInverted(false);
			bean.setProxyValueDate(null);
			bean.setProxySecuritiesOnly(false);
		}
		else {
			ValidationUtils.assertFalse(bean.getProxyValue() == null && bean.getProxySecurityQuantity() == null,
					"Proxy Managers must have a value or Benchmark and quantity entered. Please enter Proxy info or uncheck the Proxy Manager checkbox.", "proxyValue");
		}

		if (!bean.isLinkedManager()) {
			bean.setLinkedInvestmentAccount(null);
			bean.setLinkedManagerType(null);
		}
		else {
			ValidationUtils.assertNotNull(bean.getLinkedManagerType(), "A linked manager type selection is required for linked managers", "linkedManagerType");
			ValidationUtils.assertNotNull(bean.getLinkedInvestmentAccount(), "An investment account selection is required for linked managers", "linkedInvestmentAccount.id");
			if (!bean.getLinkedManagerType().isDifferentBaseCurrencyAllowed()) {
				ValidationUtils.assertTrue(bean.getLinkedInvestmentAccount().getBaseCurrency().equals(bean.getBaseCurrency()), "Linked Manager Type [" + bean.getLinkedManagerType().getLabel() + "] does not support Manager Balances in a different Base CCY than the linked account (" + bean.getLinkedInvestmentAccount().getBaseCurrency().getLabel() + ")");
			}

			// As long as this manager is not inactive, make sure the linked type is not inactive
			// NOTE: We don't want to lose history, so if the manager is inactive, it's OK they are using an inactive linked manager type since manager balances will never attempt to load for them
			if (!bean.isInactive()) {
				ValidationUtils.assertFalse(bean.getLinkedManagerType().isInactive(), "Selected Linked Manager Type [" + bean.getLinkedManagerType().getLabel()
						+ "] is inactive.  You cannot select an inactive linked type for an active manager account.");
			}

			// Validate that the Linked Investment Account is also not assigned to this manager - only for linked types that depend on PIOS runs
			// Would only be set on updates - not inserts
			if (config.isUpdate()) {
				if (bean.isLinkedManagerProcessingDependent()) {
					InvestmentManagerAccountAssignment assignment = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentByManagerAndAccount(bean.getId(),
							bean.getLinkedInvestmentAccount().getId());
					if (assignment != null && !assignment.isInactive()) {
						throw new FieldValidationException("You cannot link this manager to client account [" + bean.getLinkedInvestmentAccount().getLabel()
								+ "] because this manager is already assigned to this account and this type of linked manager requires a dependent PIOS run.");
					}
				}
			}
		}

		// Proxy Securities Only Can only be selected if Custodian Account Number is populated
		if (bean.isProxySecuritiesOnly() && bean.getCustodianAccount() == null) {
			throw new FieldValidationException("You can proxy securities only if there is a Custodian Account # entered (which will be used for Cash balances).", "proxySecuritiesOnly");
		}

		// If Proxy Adjustment applies - ensure proxy value date is entered so we know when to get performance % change from
		if (bean.isProxyAdjustmentAppliedToManager()) {
			ValidationUtils.assertNotNull(bean.getProxyValueDate(),
					"This manager will apply a balance adjustment based on the selected security or index.  Proxy Last Updated Date is required in order to calculate this adjustment.",
					"proxyValueDate");
		}

		// Proxy Date cannot be in the future and must be a weekday
		if (bean.getProxyValueDate() != null) {
			if (bean.getProxyValueDate().after(new Date())) {
				throw new FieldValidationException("Proxy Date cannot be in the future [" + DateUtils.fromDate(bean.getProxyValueDate(), DateUtils.DATE_FORMAT_INPUT) + "].", "proxyValueDate");
			}

			int dayOfWeek = DateUtils.getDayOfWeek(bean.getProxyValueDate());
			if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
				throw new FieldValidationException("Proxy Date must be a weekday.  The date you have entered [" + DateUtils.fromDate(bean.getProxyValueDate(), DateUtils.DATE_FORMAT_INPUT)
						+ "] falls on a " + (dayOfWeek == Calendar.SATURDAY ? "Saturday." : "Sunday"), "proxyValueDate");
			}

			if (bean.getProxyBenchmarkSecurity() == null && bean.getProxySecurityQuantity() != null) {
				throw new FieldValidationException("Cannot enter a quantity for Proxy Managers unless a benchmark has been selected.", "proxySecurityQuantity");
			}

			if (bean.getProxySecurityQuantity() != null && bean.getProxyValue() != null) {
				throw new FieldValidationException("Cannot enter a Proxy Value and Quantity. Can enter a value or quantity, but not both.", "proxyValue");
			}
		}


		// If inactivating - make sure no assignments
		if (originalBean != null && !originalBean.isInactive() && bean.isInactive()) {
			List<InvestmentManagerAccountAssignment> assignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByManager(bean.getId(), true);
			if (!CollectionUtils.isEmpty(assignmentList)) {
				throw new ValidationException("You cannot de-activate a manager account that has active assignments.  Please de-activate all active assignments ["
						+ BeanUtils.getPropertyValues(assignmentList, "referenceTwo.number", ",") + "] before de-activating the manager.");
			}
		}

		// Validate if manager selected for cash allocation is selected, it is not the same as the current manager
		if (bean.getCashPercentManager() != null && bean.getCashPercentManager().equals(bean)) {
			throw new FieldValidationException("You cannot select the current manager account to use cash allocation percentage from.", "cashPercentManager");
		}

		// Validate for Custom Cash that Update Date is entered, else clear it
		if (CashAdjustmentType.CUSTOM_PERCENT == bean.getCashAdjustmentType() || CashAdjustmentType.CUSTOM_VALUE == bean.getCashAdjustmentType()) {
			if (!bean.isInactive()) {
				if (bean.getCustomCashValueUpdateDate() == null || bean.getCustomCashValueUpdateDate().after(new Date())) {
					throw new FieldValidationException("Custom cash value update date is required for custom cash allocation percentages or values and cannot be in the future.");
				}
			}
		}
		else {
			bean.setCustomCashValue(null);
			bean.setCustomCashValueUpdateDate(null);
		}

		if (bean.isRollupManager()) {
			ValidationUtils.assertNotNull(bean.getRollupAggregationType(), "A RollupManager requires a value for property RollupAggregationType (AggregationOperation).");
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////          Getter and Setter Methods               ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}
}
