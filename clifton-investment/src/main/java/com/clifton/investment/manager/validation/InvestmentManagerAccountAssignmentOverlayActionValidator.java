package com.clifton.investment.manager.validation;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.InvestmentManagerAccountAssignmentOverlayAction;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentManagerAccountAssignmentOverlayActionValidator</code> ...
 *
 * @author manderson
 */
@Component
public class InvestmentManagerAccountAssignmentOverlayActionValidator extends SelfRegisteringDaoValidator<InvestmentManagerAccountAssignmentOverlayAction> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(InvestmentManagerAccountAssignmentOverlayAction bean, DaoEventTypes config) throws ValidationException {
		// NOTHING HERE - USES METHOD WITH DAO
	}


	@Override
	public void validate(InvestmentManagerAccountAssignmentOverlayAction bean, DaoEventTypes config, ReadOnlyDAO<InvestmentManagerAccountAssignmentOverlayAction> dao) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			// Type and at least one of amount or percent is required
			ValidationUtils.assertNotNull(bean.getOverlayActionType(), "Overlay Action Type is required.");
			ValidationUtils.assertTrue(bean.getOverlayActionAmount() != null || bean.getOverlayActionPercent() != null, "Missing Amount or Percent entry.");

			// Some types allow both type & percent, if not - validate only one is entered
			if (!bean.getOverlayActionType().isAllowAmountAndPercent()) {
				ValidationUtils.assertMutuallyExclusive("Either an amount or percent value is allowed, but not both", bean.getOverlayActionAmount(), bean.getOverlayActionPercent());
			}

			// Check for overlapping Dates and Type for Manager Account Assignment
			List<InvestmentManagerAccountAssignmentOverlayAction> existingList = dao.findByFields(new String[]{"managerAccountAssignment.id", "overlayActionType"}, new Object[]{
					bean.getManagerAccountAssignment().getId(), bean.getOverlayActionType()});

			for (InvestmentManagerAccountAssignmentOverlayAction existing : CollectionUtils.getIterable(existingList)) {
				if (existing.equals(bean)) {
					continue;
				}
				if (DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), existing.getStartDate(), existing.getEndDate())) {
					throw new ValidationException("There already exists an overlay action of type [" + bean.getOverlayActionType().name() + "] for this manager assignment and overlapping date range "
							+ DateUtils.fromDateRange(existing.getStartDate(), existing.getEndDate(), true, true) + ".");
				}
			}
		}
	}
}
