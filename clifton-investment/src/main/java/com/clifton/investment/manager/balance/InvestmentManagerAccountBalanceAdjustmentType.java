package com.clifton.investment.manager.balance;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>InvestmentManagerAccountBalanceAdjustmentType</code> ...
 *
 * @author Mary Anderson
 */
@CacheByName
public class InvestmentManagerAccountBalanceAdjustmentType extends NamedEntity<Short> {

	///////////////////////////////////////////////////////////

	public static final String M2M = "Adding M2M";
	public static final String CASH_PERCENT = "Cash As % of Total";
	public static final String CUSTOM_CASH_VALUE = "Custom Cash Value";
	public static final String SECURITIES_AS_CASH = "Use Securities As Cash Value";
	public static final String CASH_AS_SECURITIES = "Use Cash As Securities Value";
	public static final String IGNORE_CASH = "Ignore Cash Value";
	public static final String IGNORE_SECURITIES = "Ignore Securities Value";
	public static final String PROXY_ADJUSTMENTS = "Proxy Performance";
	public static final String ROLLUP_MOC = "Rollup MOC";

	///////////////////////////////////////////////////////////

	/**
	 * When populated, adjustments are copied to the following day and applied
	 * as this selected adjustment type.
	 * <p>
	 * i.e. MOC adjustments are automatically copied to the following day and set to type
	 * Previous Day's MOC Adjustment
	 */
	private InvestmentManagerAccountBalanceAdjustmentType nextDayAdjustmentType;

	private boolean systemDefined;
	private boolean cashAdjustmentAllowed;
	private boolean securitiesAdjustmentAllowed;
	private boolean marketOnClose;
	private boolean noteRequired;

	private String cssStyle;


	@Override
	public String getLabel() {
		String append = "";
		if (isCashAdjustmentAllowed()) {
			if (!isSecuritiesAdjustmentAllowed()) {
				append = " (Cash Only)";
			}
		}
		else {
			append = " (Securities Only)";
		}
		return getName() + append;
	}


	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isCashAdjustmentAllowed() {
		return this.cashAdjustmentAllowed;
	}


	public void setCashAdjustmentAllowed(boolean cashAdjustmentAllowed) {
		this.cashAdjustmentAllowed = cashAdjustmentAllowed;
	}


	public boolean isSecuritiesAdjustmentAllowed() {
		return this.securitiesAdjustmentAllowed;
	}


	public void setSecuritiesAdjustmentAllowed(boolean securitiesAdjustmentAllowed) {
		this.securitiesAdjustmentAllowed = securitiesAdjustmentAllowed;
	}


	public boolean isNoteRequired() {
		return this.noteRequired;
	}


	public void setNoteRequired(boolean noteRequired) {
		this.noteRequired = noteRequired;
	}


	public boolean isMarketOnClose() {
		return this.marketOnClose;
	}


	public void setMarketOnClose(boolean marketOnClose) {
		this.marketOnClose = marketOnClose;
	}


	public InvestmentManagerAccountBalanceAdjustmentType getNextDayAdjustmentType() {
		return this.nextDayAdjustmentType;
	}


	public void setNextDayAdjustmentType(InvestmentManagerAccountBalanceAdjustmentType nextDayAdjustmentType) {
		this.nextDayAdjustmentType = nextDayAdjustmentType;
	}


	public String getCssStyle() {
		return this.cssStyle;
	}


	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}
}
