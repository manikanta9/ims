package com.clifton.investment.manager.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.manager.InvestmentManagerAccountGroupAllocation;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentManagerAccountGroupAllocationListByGroupCache</code> caches a list of {@link InvestmentManagerAccountGroupAllocation} objects by group
 *
 * @author manderson
 */
@Component
public class InvestmentManagerAccountGroupAllocationListByGroupCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentManagerAccountGroupAllocation, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "referenceOne.id";
	}


	@Override
	protected Integer getBeanKeyValue(InvestmentManagerAccountGroupAllocation bean) {
		if (bean.getReferenceOne() != null) {
			return bean.getReferenceOne().getId();
		}
		return null;
	}
}
