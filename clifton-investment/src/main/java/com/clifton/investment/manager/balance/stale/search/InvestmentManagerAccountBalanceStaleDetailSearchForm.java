package com.clifton.investment.manager.balance.stale.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;

import java.math.BigDecimal;


public class InvestmentManagerAccountBalanceStaleDetailSearchForm extends InvestmentManagerAccountBalanceSearchForm {

	@SearchField(comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN_OR_EQUALS})
	private BigDecimal adjustedDifference;


	public BigDecimal getAdjustedDifference() {
		return this.adjustedDifference;
	}


	public void setAdjustedDifference(BigDecimal adjustedDifference) {
		this.adjustedDifference = adjustedDifference;
	}
}
