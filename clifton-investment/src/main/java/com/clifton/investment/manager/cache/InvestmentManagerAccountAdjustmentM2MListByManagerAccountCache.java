package com.clifton.investment.manager.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.manager.InvestmentManagerAccountAdjustmentM2M;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentManagerAccountAdjustmentM2MListByManagerAccountCache</code> class provides caching and retrieval from cache of InvestmentManagerAccountAdjustmentM2M objects by manager account
 * <p>
 * These are always associated/retrieved/saved/deleted with InvestmentManagerAccount, so the manager id is the key and the list of M2M Adjustments is stored
 * so when updates are done on the Manager itself the list is automatically wiped.
 *
 * @author manderson
 */
@Component
public class InvestmentManagerAccountAdjustmentM2MListByManagerAccountCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentManagerAccountAdjustmentM2M, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "referenceOne.id";
	}


	@Override
	protected Integer getBeanKeyValue(InvestmentManagerAccountAdjustmentM2M bean) {
		if (bean.getReferenceOne() != null) {
			return bean.getReferenceOne().getId();
		}
		return null;
	}
}
