package com.clifton.investment.manager.transaction.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class InvestmentManagerCustodianTransactionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "custodianAccount.number,description,custodianAccount.company.name")
	private String searchPattern;

	@SearchField(searchField = "custodianAccount.id")
	private Integer custodianAccountId;

	@SearchField(searchField = "custodianAccount.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] custodianAccountIds;

	@SearchField(searchField = "custodianAccount.number")
	private String custodianAccountNumber;

	@SearchField(searchField = "custodianAccount.company.id")
	private Integer custodianCompanyId;

	@SearchField(searchFieldPath = "custodianAccount.company", searchField = "name")
	private String custodianCompanyName;

	@SearchField
	private Date transactionDate;

	@SearchField
	private BigDecimal transactionAmount;

	@SearchField
	private String description;

	@SearchField
	private Integer sourceDataIdentifier;

	@SearchField(searchField = "sourceDataIdentifier", comparisonConditions = ComparisonConditions.IN)
	private Integer[] sourceDataIdentifierList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		if (restrictManagerCustodianTransactionQuery() && this.getRestrictionList() != null) {
			SearchFormUtils.validateDateRangeLength(this, "transactionDate", 7, "A date range, not exceeding 7 days, must be selected if not searching by Custodian Account Number, Single Transaction Date, or Source Data Identifier.");
		}
	}


	private boolean restrictManagerCustodianTransactionQuery() {
		//Restrict searching if NOT querying by specific transactionDate, accountNumber, or by Source Data Identifiers
		return (this.transactionDate == null && !isSearchRestrictionSetAny("custodianAccountNumber", "custodianAccountId", "sourceDataIdentifier", "sourceDataIdentifierList"));
	}


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getCustodianAccountId() {
		return this.custodianAccountId;
	}


	public void setCustodianAccountId(Integer custodianAccountId) {
		this.custodianAccountId = custodianAccountId;
	}


	public Integer[] getCustodianAccountIds() {
		return this.custodianAccountIds;
	}


	public void setCustodianAccountIds(Integer[] custodianAccountIds) {
		this.custodianAccountIds = custodianAccountIds;
	}


	public String getCustodianAccountNumber() {
		return this.custodianAccountNumber;
	}


	public void setCustodianAccountNumber(String custodianAccountNumber) {
		this.custodianAccountNumber = custodianAccountNumber;
	}


	public Integer getCustodianCompanyId() {
		return this.custodianCompanyId;
	}


	public void setCustodianCompanyId(Integer custodianCompanyId) {
		this.custodianCompanyId = custodianCompanyId;
	}


	public String getCustodianCompanyName() {
		return this.custodianCompanyName;
	}


	public void setCustodianCompanyName(String custodianCompanyName) {
		this.custodianCompanyName = custodianCompanyName;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}


	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getSourceDataIdentifier() {
		return this.sourceDataIdentifier;
	}


	public void setSourceDataIdentifier(Integer sourceDataIdentifier) {
		this.sourceDataIdentifier = sourceDataIdentifier;
	}


	public Integer[] getSourceDataIdentifierList() {
		return this.sourceDataIdentifierList;
	}


	public void setSourceDataIdentifierList(Integer[] sourceDataIdentifierList) {
		this.sourceDataIdentifierList = sourceDataIdentifierList;
	}
}
