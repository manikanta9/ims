package com.clifton.investment.manager.transaction;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateOrderBySqlFormula;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.transaction.search.InvestmentManagerCustodianTransactionSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InvestmentManagerCustodianTransactionServiceImpl implements InvestmentManagerCustodianTransactionService {

	private AdvancedUpdatableDAO<InvestmentManagerCustodianTransaction, Criteria> investmentManagerCustodianTransactionDAO;

	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerCustodianTransaction getInvestmentManagerCustodianTransaction(int id) {
		return getInvestmentManagerCustodianTransactionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentManagerCustodianTransaction> getInvestmentManagerCustodianTransactionList(InvestmentManagerCustodianTransactionSearchForm searchForm) {
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public boolean configureOrderBy(Criteria criteria) {
				// Special support for calculated fields
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(getSortableSearchForm().getOrderBy());
				if (CollectionUtils.isEmpty(orderByList)) {
					criteria.addOrder(Order.asc("transactionDate"));
					criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(ABS(TransactionAmount))" + " DESC"));
					return true;
				}
				for (OrderByField field : orderByList) {
					String name = getOrderByFieldName(field, criteria);
					boolean asc = OrderByDirections.ASC == field.getDirection();
					if ("transactionAmountAbsoluteValue".equals(name)) {
						criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(ABS(TransactionAmount))" + (asc ? "ASC" : "DESC")));
					}
					else {
						criteria.addOrder(asc ? Order.asc(name) : Order.desc(name));
					}
				}
				return true;
			}
		};
		return getInvestmentManagerCustodianTransactionDAO().findBySearchCriteria(configurer);
	}


	@Override
	public InvestmentManagerCustodianTransaction saveInvestmentManagerCustodianTransaction(InvestmentManagerCustodianTransaction bean) {
		return getInvestmentManagerCustodianTransactionDAO().save(bean);
	}


	@Override
	public void deleteInvestmentManagerCustodianTransaction(int id) {
		getInvestmentManagerCustodianTransactionDAO().delete(id);
	}


	@Override
	public void deleteInvestmentManagerCustodianTransactionList(List<InvestmentManagerCustodianTransaction> beans) {
		getInvestmentManagerCustodianTransactionDAO().deleteList(beans);
	}

	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentManagerCustodianTransaction, Criteria> getInvestmentManagerCustodianTransactionDAO() {
		return this.investmentManagerCustodianTransactionDAO;
	}


	public void setInvestmentManagerCustodianTransactionDAO(AdvancedUpdatableDAO<InvestmentManagerCustodianTransaction, Criteria> investmentManagerCustodianTransactionDAO) {
		this.investmentManagerCustodianTransactionDAO = investmentManagerCustodianTransactionDAO;
	}
}
