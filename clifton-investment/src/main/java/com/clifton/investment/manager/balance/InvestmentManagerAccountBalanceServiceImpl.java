package com.clifton.investment.manager.balance;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.calendar.InvestmentEventManagerAdjustment;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.cache.InvestmentManagerAccountBalanceAdjustmentCopyNextDayCache;
import com.clifton.investment.manager.balance.cache.InvestmentManagerAccountBalanceDateCache;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceAdjustmentSearchForm;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceAdjustmentTypeSearchForm;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.schema.SystemSchemaService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountBalanceServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class InvestmentManagerAccountBalanceServiceImpl implements InvestmentManagerAccountBalanceService {

	public static final String INVESTMENT_EVENT_MANAGER_ADJUSTMENT_TABLE = "InvestmentEventManagerAdjustment";
	public static final String INVESTMENT_MANAGER_BALANCE_ADJUSTMENT_TABLE = "InvestmentManagerAccountBalanceAdjustment";

	private AdvancedUpdatableDAO<InvestmentManagerAccountBalance, Criteria> investmentManagerAccountBalanceDAO;
	private AdvancedUpdatableDAO<InvestmentManagerAccountBalanceAdjustment, Criteria> investmentManagerAccountBalanceAdjustmentDAO;

	private AdvancedUpdatableDAO<InvestmentManagerAccountBalanceAdjustmentType, Criteria> investmentManagerAccountBalanceAdjustmentTypeDAO;

	private DaoNamedEntityCache<InvestmentManagerAccountBalanceAdjustmentType> investmentManagerAccountBalanceAdjustmentTypeCache;
	private InvestmentManagerAccountBalanceAdjustmentCopyNextDayCache investmentManagerAccountBalanceAdjustmentCopyNextDayCache;
	private InvestmentManagerAccountBalanceDateCache investmentManagerAccountBalanceDateCache;

	private CalendarBusinessDayService calendarBusinessDayService;
	private ContextHandler contextHandler;

	private InvestmentManagerAccountService investmentManagerAccountService;

	private RuleViolationStatusService ruleViolationStatusService;

	private SecurityUserService securityUserService;
	private SystemSchemaService systemSchemaService;


	////////////////////////////////////////////////////////////////////////////
	////////           Investment Manager Account Methods                ///////
	//////			Relies on InvestmentManagerAccountBalance              /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentManagerAccount> getInvestmentManagerAccountMissingBalanceList(InvestmentManagerAccountSearchForm searchForm, final Date balanceDate) {
		ValidationUtils.assertNotNull(balanceDate, "Balance Date is Required", "balanceDate");
		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				DetachedCriteria notExists = DetachedCriteria.forClass(InvestmentManagerAccountBalance.class, "b");
				notExists.setProjection(Projections.property("id"));
				notExists.createAlias("b.managerAccount", "bm");
				notExists.add(Restrictions.eqProperty("bm.id", criteria.getAlias() + ".id"));
				notExists.add(Restrictions.eq("balanceDate", balanceDate));
				criteria.add(Subqueries.notExists(notExists));
			}
		};
		return getInvestmentManagerAccountService().getInvestmentManagerAccountListBySearchConfig(searchConfig);
	}


	////////////////////////////////////////////////////////////////////////////
	//////        Investment Manager Account Balance Methods             ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerAccountBalance getInvestmentManagerAccountBalance(int id) {
		return getInvestmentManagerAccountBalanceImpl(id, true);
	}


	private InvestmentManagerAccountBalance getInvestmentManagerAccountBalanceImpl(int id, boolean populated) {
		InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceDAO().findByPrimaryKey(id);
		if (populated) {
			populateAdjustmentsOnBalance(balance);
		}
		return balance;
	}


	@Override
	public InvestmentManagerAccountBalance getInvestmentManagerAccountBalanceByManagerAndDate(int managerAccountId, Date balanceDate, boolean populateAdjustments) {
		return getInvestmentManagerAccountBalanceByManagerAndDate(managerAccountId, balanceDate, populateAdjustments, false);
	}


	@Override
	public InvestmentManagerAccountBalance getInvestmentManagerAccountBalanceByManagerAndDateFlexible(int managerAccountId, Date balanceDate, boolean populateAdjustments) {
		return getInvestmentManagerAccountBalanceByManagerAndDate(managerAccountId, balanceDate, populateAdjustments, true);
	}


	private InvestmentManagerAccountBalance getInvestmentManagerAccountBalanceByManagerAndDate(int managerAccountId, Date balanceDate, boolean populateAdjustments, boolean flexible) {
		// Seek cache for balance ID
		InvestmentManagerAccountBalanceDateCache cache = getInvestmentManagerAccountBalanceDateCache();
		final Integer balanceId = cache.getInvestmentManagerAccountBalanceId(managerAccountId, balanceDate);

		// If balance ID is found, seek balance entity directly
		InvestmentManagerAccountBalance balance = null;
		if (balanceId != null) {
			balance = getInvestmentManagerAccountBalanceImpl(balanceId, false);
		}

		// If balance does not exist, query based on provided fields
		if (balance == null) {
			if (flexible) {
				// Find latest balance as of the given date
				InvestmentManagerAccountBalanceSearchForm searchForm = new InvestmentManagerAccountBalanceSearchForm();
				searchForm.setManagerAccountId(managerAccountId);
				searchForm.addSearchRestriction("balanceDate", ComparisonConditions.LESS_THAN_OR_EQUALS, balanceDate);
				searchForm.setLimit(1);
				searchForm.setOrderBy("balanceDate:DESC");
				List<InvestmentManagerAccountBalance> balanceList = getInvestmentManagerAccountBalanceList(searchForm);
				balance = CollectionUtils.getFirstElement(balanceList);
			}
			else {
				// Find balance on given date
				balance = getInvestmentManagerAccountBalanceDAO().findOneByFields(new String[]{"managerAccount.id", "balanceDate"}, new Object[]{managerAccountId, balanceDate});
			}

			// Update cache
			if (balance != null) {
				cache.storeInvestmentManagerAccountBalanceId(balance);
			}
		}

		// Populate adjustments
		if (balance != null && populateAdjustments) {
			populateAdjustmentsOnBalance(balance);
		}
		return balance;
	}


	private void populateAdjustmentsOnBalance(InvestmentManagerAccountBalance balance) {
		if (balance != null && balance.isAdjusted()) {
			balance.setAdjustmentList(getInvestmentManagerAccountBalanceAdjustmentDAO().findByField("managerAccountBalance.id", balance.getId()));
		}
	}


	@Override
	public List<InvestmentManagerAccountBalance> getInvestmentManagerAccountBalanceList(InvestmentManagerAccountBalanceSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getClientInvestmentAccountGroupId() != null) {
					String sql = "EXISTS (SELECT * FROM InvestmentManagerAccountAssignment ma " +
							"INNER JOIN InvestmentAccountGroupAccount ga ON ma.InvestmentAccountID = ga.InvestmentAccountID " +
							"WHERE ma.InvestmentManagerAccountID = {alias}.InvestmentManagerAccountID " +
							"AND ga.InvestmentAccountGroupID = " + searchForm.getClientInvestmentAccountGroupId() + ")";
					criteria.add(Restrictions.sqlRestriction(sql));
				}
			}
		};
		return getInvestmentManagerAccountBalanceDAO().findBySearchCriteria(searchConfig);
	}


	private void validateInvestmentManagerAccountBalanceAdjustments(InvestmentManagerAccountBalance bean) {
		List<InvestmentManagerAccountBalanceAdjustment> list = bean.getAdjustmentList();
		for (InvestmentManagerAccountBalanceAdjustment adj : CollectionUtils.getIterable(list)) {
			if (adj == null || adj.getAdjustmentType() == null) {
				throw new ValidationException("Please enter adjustment type for each line item, or remove all blank lines.");
			}
			adj.setManagerAccountBalance(bean);
			String adjustmentLabel = adj.getAdjustmentType().getName() + (!StringUtils.isEmpty(adj.getNote()) ? " (" + adj.getNote() + ")" : "");
			if (adj.getAdjustmentType().isNoteRequired() && StringUtils.isEmpty(adj.getNote())) {
				throw new ValidationException("Note is required for adjustments with type [" + adj.getAdjustmentType().getName() + "]");
			}
			if (adj.getCashValue() == null) {
				adj.setCashValue(BigDecimal.ZERO);
			}
			if (adj.getSecuritiesValue() == null) {
				adj.setSecuritiesValue(BigDecimal.ZERO);
			}
			if (MathUtils.isEqual(adj.getCashValue(), 0) && MathUtils.isEqual(0, adj.getSecuritiesValue())) {
				throw new ValidationException("A cash and/or securities value for adjustment [" + adjustmentLabel + "] is required.");
			}
			if (!MathUtils.isEqual(adj.getCashValue(), 0)) {
				ValidationUtils.assertTrue(adj.getAdjustmentType().isCashAdjustmentAllowed(), "Cash adjustments for [" + adjustmentLabel + "] are not allowed.");
			}
			if (!MathUtils.isEqual(0, adj.getSecuritiesValue())) {
				ValidationUtils.assertTrue(adj.getAdjustmentType().isSecuritiesAdjustmentAllowed(), "Securities adjustments for [" + adjustmentLabel
						+ "] are not allowed.");
			}
		}
	}


	private void roundInvestmentManagerAccountBalanceValues(InvestmentManagerAccountBalance bean) {
		// round to 2 decimals (max for currencies)
		bean.setCashValue(MathUtils.round(bean.getCashValue(), 2));
		bean.setSecuritiesValue(MathUtils.round(bean.getSecuritiesValue(), 2));
		bean.setAdjustedCashValue(MathUtils.round(bean.getAdjustedCashValue(), 2));
		bean.setAdjustedSecuritiesValue(MathUtils.round(bean.getAdjustedSecuritiesValue(), 2));
		bean.setMarketOnCloseCashValue(MathUtils.round(bean.getMarketOnCloseCashValue(), 2));
		bean.setMarketOnCloseSecuritiesValue(MathUtils.round(bean.getMarketOnCloseSecuritiesValue(), 2));
	}


	@Override
	@Transactional
	public InvestmentManagerAccountBalance saveInvestmentManagerAccountBalanceReload(InvestmentManagerAccountBalance existingBean, InvestmentManagerAccountBalance bean, boolean forceReload) {
		ValidationUtils.assertTrue(bean.isNewBean(), "Save Investment Manager Balance For Reloads can only save for new balance records, not used for udpates.");
		ValidationUtils.assertEmpty(bean.getAdjustmentList(), "Balance Record cannot have adjustments when initially loading a new balance record.");

		// round to 2 decimals (max for currencies) - round before comparison to verify comparing existing and new to same ccy precision
		roundInvestmentManagerAccountBalanceValues(bean);

		boolean createNewBalance = true;
		if (existingBean != null) {
			// Make sure adjustments are available on existing bean
			populateAdjustmentsOnBalance(existingBean);

			// If cash and securities are the same, reuse it - but still want to re-process adjustments if necessary so can load rollups
			if (MathUtils.isEqual(existingBean.getCashValue(), bean.getCashValue()) && MathUtils.isEqual(existingBean.getSecuritiesValue(), bean.getSecuritiesValue())) {
				if (forceReload) {
					createNewBalance = false;
					bean = existingBean;
					bean.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.UNPROCESSED));
				}
				// If not forcing a reload, keep the same manager balance that was there
				else {
					return existingBean;
				}
			}
			else {
				final InvestmentManagerAccountBalance newBean = bean;
				DaoUtils.executeWithPostUpdateFlushEnabled(() -> {
					newBean.setAdjustmentList(BeanUtils.filter(existingBean.getAdjustmentList(), adjustment -> !adjustment.isAutoLoaded()));
					deleteInvestmentManagerAccountBalance(existingBean.getId());
				});
			}
		}

		// Before Saves - Recalculate adjusted balances
		bean.recalculateAdjustedValues();

		// save the balance and its adjustments
		List<InvestmentManagerAccountBalanceAdjustment> adjustmentList = bean.getAdjustmentList();
		bean = getInvestmentManagerAccountBalanceDAO().save(bean);

		if (createNewBalance && !CollectionUtils.isEmpty(adjustmentList)) {
			// If copying manual adjustments - get the original create user - set context as user and reset id for inserts
			// save the current user
			Object currentUser = getContextHandler().getBean(Context.USER_BEAN_NAME);
			SecurityUser adjustmentCreator = null;
			try {
				for (InvestmentManagerAccountBalanceAdjustment adj : adjustmentList) {
					adj.setId(null);
					adj.setManagerAccountBalance(bean);
					if (adj.getCreateUserId() > 0) {
						adjustmentCreator = getSecurityUserService().getSecurityUser(adj.getCreateUserId());
					}
					// If can't determine who the original creator was, use current user
					if (adjustmentCreator == null) {
						getContextHandler().setBean(Context.USER_BEAN_NAME, currentUser);
					}
					else {
						getContextHandler().setBean(Context.USER_BEAN_NAME, adjustmentCreator);
					}
					getInvestmentManagerAccountBalanceAdjustmentDAO().save(adj);
				}
			}
			finally {
				// restore the current user
				if (currentUser == null) {
					getContextHandler().removeBean(Context.USER_BEAN_NAME);
				}
				else {
					getContextHandler().setBean(Context.USER_BEAN_NAME, currentUser);
				}
			}
		}

		// Reset Adjustment List on Bean before returning
		bean.setAdjustmentList(adjustmentList);
		return bean;
	}


	@Override
	@Transactional
	public InvestmentManagerAccountBalance saveInvestmentManagerAccountBalance(InvestmentManagerAccountBalance bean) {
		// Validate Balance Date isn't set in the future - usually we process for previous day - but for validation we'll block anything after today
		if (DateUtils.compare(bean.getBalanceDate(), new Date(), false) > 0) {
			throw new ValidationException("Invalid Date Entered [" + DateUtils.fromDateShort(bean.getBalanceDate()) + "]: Cannot enter a balance for dates in the future.");
		}
		if (!DateUtils.isWeekday(bean.getBalanceDate())) {
			throw new ValidationException("Invalid Date Entered [" + DateUtils.fromDateShort(bean.getBalanceDate()) + "]: Date selected falls on a weekend.  Weekday balances only are allowed.");
		}

		// Validate Adjustments
		validateInvestmentManagerAccountBalanceAdjustments(bean);

		// get existing adjustments so that we know what to delete if necessary (get them before recalculating bean properties)
		List<InvestmentManagerAccountBalanceAdjustment> oldAdjustments = null;
		if (!bean.isNewBean()) {
			oldAdjustments = getInvestmentManagerAccountBalanceAdjustmentDAO().findByField("managerAccountBalance.id", bean.getId());
		}

		// Round everything to 2 decimals
		roundInvestmentManagerAccountBalanceValues(bean);

		// Before Saves - Recalculate adjusted balances
		bean.recalculateAdjustedValues();

		// save the balance and its adjustments
		List<InvestmentManagerAccountBalanceAdjustment> newAdjustments = bean.getAdjustmentList();
		bean = getInvestmentManagerAccountBalanceDAO().save(bean);
		getInvestmentManagerAccountBalanceAdjustmentDAO().saveList(newAdjustments, oldAdjustments);
		bean.setAdjustmentList(newAdjustments);
		return bean;
	}


	/**
	 * NOTE: Use save in URL so that binding knows to fully populate the given bean since we are saving changes and copying previous manual adjustments
	 * Otherwise, fields like warning status are missed and then when this is saved it's pushed back to unprocessed when it shouldn't be since we are just adding manual adjustments
	 *
	 * @param bean
	 */
	@Override
	public InvestmentManagerAccountBalance saveInvestmentManagerAccountBalanceAndCopyPreviousAdjustments(InvestmentManagerAccountBalance bean) {
		Date previousDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(bean.getBalanceDate()), -1);
		InvestmentManagerAccountBalance previousBean = getInvestmentManagerAccountBalanceByManagerAndDate(bean.getManagerAccount().getId(), previousDate, true);
		ValidationUtils.assertNotNull(previousBean, "Unable to copy previous manual adjustments from manager [" + bean.getManagerAccount().getLabel()
				+ "] because there is no balance record for the previous business day [" + DateUtils.fromDateShort(previousDate) + "].");
		boolean found = false;
		for (InvestmentManagerAccountBalanceAdjustment adj : CollectionUtils.getIterable(previousBean.getAdjustmentList())) {
			// Filter out system defined adjustment types (i.e. system generated) and those with a source table = Calendar Event Manager Adjustment Table (i.e. generated from calendar events)
			if (!adj.getAdjustmentType().isSystemDefined() && (adj.getSourceSystemTable() == null || !INVESTMENT_EVENT_MANAGER_ADJUSTMENT_TABLE.equals(adj.getSourceSystemTable().getName()))) {
				found = true;
				InvestmentManagerAccountBalanceAdjustment newAdj = BeanUtils.cloneBean(adj, false, false);
				newAdj.setId(null);
				// Remove Source Table/FKFieldID - Manual Copy - (add copy to note so it's clear it wasn't auto applied)
				if (adj.getSourceSystemTable() != null) {
					newAdj.setNote(newAdj.getNote() + " (Copy)");
					newAdj.setSourceSystemTable(null);
					newAdj.setSourceFkFieldId(null);
				}
				newAdj.setManagerAccountBalance(bean);
				bean.addAdjustment(newAdj);
			}
		}
		ValidationUtils.assertTrue(found, "Unable to copy previous manual adjustments.  Manager Balance on [" + DateUtils.fromDateShort(previousDate) + "] has no manual adjustments to copy.");
		return saveInvestmentManagerAccountBalance(bean);
	}


	@Override
	public void deleteInvestmentManagerAccountBalance(int id) {
		deleteInvestmentManagerAccountBalanceImpl(getInvestmentManagerAccountBalance(id));
	}


	@Transactional
	protected void deleteInvestmentManagerAccountBalanceImpl(InvestmentManagerAccountBalance balance) {
		if (balance != null) {
			// First Delete all Adjustments
			getInvestmentManagerAccountBalanceAdjustmentDAO().deleteList(balance.getAdjustmentList());
			// Then delete the balance
			getInvestmentManagerAccountBalanceDAO().delete(balance.getId());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////  Investment Manager Account Balance Adjustment Lookup Methods  //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerAccountBalanceAdjustment getInvestmentManagerAccountBalanceAdjustment(int id) {
		return getInvestmentManagerAccountBalanceAdjustmentDAO().findByPrimaryKey(id);
	}


	/**
	 * Called from Event Observer to add create an adjustment from an event adjustment
	 * Or from Manager processing to create a manager adjustment from the event adjustment
	 * NOTE: Does not update database, just returns the new bean
	 */
	@Override
	public InvestmentManagerAccountBalanceAdjustment createInvestmentManagerAccountBalanceAdjustmentFromEvent(InvestmentEventManagerAdjustment eventAdjustment) {
		InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
		BeanUtils.copyProperties(eventAdjustment, adjustment);
		adjustment.setId(null);
		String notePrefix = "Adjustment Created from Event [" + eventAdjustment.getInvestmentEvent().getLabel() + "]";
		if (adjustment.getNote() != null) {
			adjustment.setNote(notePrefix + ": " + adjustment.getNote());
		}
		else {
			adjustment.setNote(notePrefix);
		}
		adjustment.setSourceSystemTable(getSystemSchemaService().getSystemTableByName(INVESTMENT_EVENT_MANAGER_ADJUSTMENT_TABLE));
		adjustment.setSourceFkFieldId(eventAdjustment.getId());
		return adjustment;
	}


	@Override
	public List<InvestmentManagerAccountBalanceAdjustment> getInvestmentManagerAccountBalanceAdjustmentList(InvestmentManagerAccountBalanceAdjustmentSearchForm searchForm) {
		return getInvestmentManagerAccountBalanceAdjustmentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentManagerAccountBalanceAdjustment> getInvestmentManagerAccountBalanceAdjustmentCopyNextDayList(Date balanceDate) {
		List<InvestmentManagerAccountBalanceAdjustment> adjList = getInvestmentManagerAccountBalanceAdjustmentCopyNextDayCache().getInvestmentManagerAccountBalanceAdjustmentList(balanceDate);
		if (adjList == null) {
			InvestmentManagerAccountBalanceAdjustmentSearchForm searchForm = new InvestmentManagerAccountBalanceAdjustmentSearchForm();
			searchForm.setBalanceDate(balanceDate);
			searchForm.setNextDayAdjustmentExists(true);
			adjList = getInvestmentManagerAccountBalanceAdjustmentList(searchForm);
			if (adjList == null) {
				adjList = new ArrayList<>();
			}
			getInvestmentManagerAccountBalanceAdjustmentCopyNextDayCache().storeInvestmentManagerAccountBalanceAdjustmentList(balanceDate, adjList);
		}
		return adjList;
	}


	@Override
	public void saveInvestmentManagerAccountBalanceAdjustment(InvestmentManagerAccountBalanceAdjustment bean) {
		InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalance(bean.getManagerAccountBalance().getId());
		balance.addAdjustment(bean);
		saveInvestmentManagerAccountBalance(balance);
	}


	////////////////////////////////////////////////////////////////////////////
	//////  Investment Manager Account Balance Adjustment Type Methods   ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerAccountBalanceAdjustmentType getInvestmentManagerAccountBalanceAdjustmentType(short id) {
		return getInvestmentManagerAccountBalanceAdjustmentTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentManagerAccountBalanceAdjustmentType getInvestmentManagerAccountBalanceAdjustmentTypeByName(String name) {
		return getInvestmentManagerAccountBalanceAdjustmentTypeCache().getBeanForKeyValueStrict(getInvestmentManagerAccountBalanceAdjustmentTypeDAO(), name);
	}


	@Override
	public List<InvestmentManagerAccountBalanceAdjustmentType> getInvestmentManagerAccountBalanceAdjustmentTypeList(InvestmentManagerAccountBalanceAdjustmentTypeSearchForm searchForm) {
		return getInvestmentManagerAccountBalanceAdjustmentTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	// See {@link InvestmentManagerAccountBalanceAdjustmentTypeValidator}
	@Override
	public InvestmentManagerAccountBalanceAdjustmentType saveInvestmentManagerAccountBalanceAdjustmentType(InvestmentManagerAccountBalanceAdjustmentType bean) {
		return getInvestmentManagerAccountBalanceAdjustmentTypeDAO().save(bean);
	}


	// See {@link InvestmentManagerAccountBalanceAdjustmentTypeValidator}
	@Override
	public void deleteInvestmentManagerAccountBalanceAdjustmentType(short id) {
		getInvestmentManagerAccountBalanceAdjustmentTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentManagerAccountBalance, Criteria> getInvestmentManagerAccountBalanceDAO() {
		return this.investmentManagerAccountBalanceDAO;
	}


	public void setInvestmentManagerAccountBalanceDAO(AdvancedUpdatableDAO<InvestmentManagerAccountBalance, Criteria> investmentManagerAccountBalanceDAO) {
		this.investmentManagerAccountBalanceDAO = investmentManagerAccountBalanceDAO;
	}


	public AdvancedUpdatableDAO<InvestmentManagerAccountBalanceAdjustment, Criteria> getInvestmentManagerAccountBalanceAdjustmentDAO() {
		return this.investmentManagerAccountBalanceAdjustmentDAO;
	}


	public void setInvestmentManagerAccountBalanceAdjustmentDAO(AdvancedUpdatableDAO<InvestmentManagerAccountBalanceAdjustment, Criteria> investmentManagerAccountBalanceAdjustmentDAO) {
		this.investmentManagerAccountBalanceAdjustmentDAO = investmentManagerAccountBalanceAdjustmentDAO;
	}


	public AdvancedUpdatableDAO<InvestmentManagerAccountBalanceAdjustmentType, Criteria> getInvestmentManagerAccountBalanceAdjustmentTypeDAO() {
		return this.investmentManagerAccountBalanceAdjustmentTypeDAO;
	}


	public void setInvestmentManagerAccountBalanceAdjustmentTypeDAO(AdvancedUpdatableDAO<InvestmentManagerAccountBalanceAdjustmentType, Criteria> investmentManagerAccountBalanceAdjustmentTypeDAO) {
		this.investmentManagerAccountBalanceAdjustmentTypeDAO = investmentManagerAccountBalanceAdjustmentTypeDAO;
	}


	public DaoNamedEntityCache<InvestmentManagerAccountBalanceAdjustmentType> getInvestmentManagerAccountBalanceAdjustmentTypeCache() {
		return this.investmentManagerAccountBalanceAdjustmentTypeCache;
	}


	public void setInvestmentManagerAccountBalanceAdjustmentTypeCache(DaoNamedEntityCache<InvestmentManagerAccountBalanceAdjustmentType> investmentManagerAccountBalanceAdjustmentTypeCache) {
		this.investmentManagerAccountBalanceAdjustmentTypeCache = investmentManagerAccountBalanceAdjustmentTypeCache;
	}


	public InvestmentManagerAccountBalanceAdjustmentCopyNextDayCache getInvestmentManagerAccountBalanceAdjustmentCopyNextDayCache() {
		return this.investmentManagerAccountBalanceAdjustmentCopyNextDayCache;
	}


	public void setInvestmentManagerAccountBalanceAdjustmentCopyNextDayCache(InvestmentManagerAccountBalanceAdjustmentCopyNextDayCache investmentManagerAccountBalanceAdjustmentCopyNextDayCache) {
		this.investmentManagerAccountBalanceAdjustmentCopyNextDayCache = investmentManagerAccountBalanceAdjustmentCopyNextDayCache;
	}


	public InvestmentManagerAccountBalanceDateCache getInvestmentManagerAccountBalanceDateCache() {
		return this.investmentManagerAccountBalanceDateCache;
	}


	public void setInvestmentManagerAccountBalanceDateCache(InvestmentManagerAccountBalanceDateCache investmentManagerAccountBalanceDateCache) {
		this.investmentManagerAccountBalanceDateCache = investmentManagerAccountBalanceDateCache;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
