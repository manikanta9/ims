package com.clifton.investment.manager.upload;

import java.math.BigDecimal;


/**
 * <code>BaseInvestmentManagerAccountAllocationUploadTemplate</code>> is a simplified base class that is used to easily upload new manager
 * allocation weights for existing manager assignments
 *
 * @author nickk
 */
public abstract class BaseInvestmentManagerAccountAllocationUploadTemplate {

	private String clientAccountNumber;
	private String managerAccountNumber;

	private BigDecimal allocationPercent;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used to build the list of allocations for each manager assignment which is unique by client account and manager account
	 */
	public String getManagerAssignmentKey() {
		return getClientAccountNumber() + "_" + getManagerAccountNumber();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public String getManagerAccountNumber() {
		return this.managerAccountNumber;
	}


	public void setManagerAccountNumber(String managerAccountNumber) {
		this.managerAccountNumber = managerAccountNumber;
	}


	public BigDecimal getAllocationPercent() {
		return this.allocationPercent;
	}


	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}
}
