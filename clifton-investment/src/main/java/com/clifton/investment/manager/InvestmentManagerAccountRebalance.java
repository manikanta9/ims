package com.clifton.investment.manager;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * @author mitchellf
 */
public class InvestmentManagerAccountRebalance extends BaseEntity<Integer> {

	private InvestmentManagerAccountGroup managerAccountGroup;

	private InvestmentManagerAccount managerAccount;

	private BigDecimal actualAllocation;
	private BigDecimal targetAllocation;
	private BigDecimal rebalanceTriggerMin;
	private BigDecimal rebalanceTriggerMax;

	private BigDecimal actualAllocationPercent;
	private BigDecimal targetAllocationPercent;
	private BigDecimal rebalanceTriggerMinPercent;
	private BigDecimal rebalanceTriggerMaxPercent;

	// Used during processing on the bean - not stored in the database
	@NonPersistentField
	private InvestmentManagerAccountGroupAllocation groupAllocation;

	/////////////////////////////////////////////////
	/////////////////////////////////////////////////


	public BigDecimal getActualDeviationFromTarget() {
		return MathUtils.subtract(getActualAllocation(), getTargetAllocation());
	}


	public BigDecimal getActualDeviationFromTargetPercent() {
		return MathUtils.subtract(getActualAllocationPercent(), getTargetAllocationPercent());
	}


	public boolean isOutsideOfRebalanceTriggers() {
		return !MathUtils.isInRange(getActualDeviationFromTarget(), getRebalanceTriggerMin(), getRebalanceTriggerMax());
	}

	/////////////////////////////////////////////////
	/////////////////////////////////////////////////


	public InvestmentManagerAccountGroup getManagerAccountGroup() {
		return this.managerAccountGroup;
	}


	public void setManagerAccountGroup(InvestmentManagerAccountGroup managerAccountGroup) {
		this.managerAccountGroup = managerAccountGroup;
	}


	public InvestmentManagerAccount getManagerAccount() {
		return this.managerAccount;
	}


	public void setManagerAccount(InvestmentManagerAccount managerAccount) {
		this.managerAccount = managerAccount;
	}


	public BigDecimal getActualAllocation() {
		return this.actualAllocation;
	}


	public void setActualAllocation(BigDecimal actualAllocation) {
		this.actualAllocation = actualAllocation;
	}


	public BigDecimal getTargetAllocation() {
		return this.targetAllocation;
	}


	public void setTargetAllocation(BigDecimal targetAllocation) {
		this.targetAllocation = targetAllocation;
	}


	public BigDecimal getRebalanceTriggerMin() {
		return this.rebalanceTriggerMin;
	}


	public void setRebalanceTriggerMin(BigDecimal rebalanceTriggerMin) {
		this.rebalanceTriggerMin = rebalanceTriggerMin;
	}


	public BigDecimal getRebalanceTriggerMax() {
		return this.rebalanceTriggerMax;
	}


	public void setRebalanceTriggerMax(BigDecimal rebalanceTriggerMax) {
		this.rebalanceTriggerMax = rebalanceTriggerMax;
	}


	public BigDecimal getActualAllocationPercent() {
		return this.actualAllocationPercent;
	}


	public void setActualAllocationPercent(BigDecimal actualAllocationPercent) {
		this.actualAllocationPercent = actualAllocationPercent;
	}


	public BigDecimal getTargetAllocationPercent() {
		return this.targetAllocationPercent;
	}


	public void setTargetAllocationPercent(BigDecimal targetAllocationPercent) {
		this.targetAllocationPercent = targetAllocationPercent;
	}


	public BigDecimal getRebalanceTriggerMinPercent() {
		return this.rebalanceTriggerMinPercent;
	}


	public void setRebalanceTriggerMinPercent(BigDecimal rebalanceTriggerMinPercent) {
		this.rebalanceTriggerMinPercent = rebalanceTriggerMinPercent;
	}


	public BigDecimal getRebalanceTriggerMaxPercent() {
		return this.rebalanceTriggerMaxPercent;
	}


	public void setRebalanceTriggerMaxPercent(BigDecimal rebalanceTriggerMaxPercent) {
		this.rebalanceTriggerMaxPercent = rebalanceTriggerMaxPercent;
	}


	public InvestmentManagerAccountGroupAllocation getGroupAllocation() {
		return this.groupAllocation;
	}


	public void setGroupAllocation(InvestmentManagerAccountGroupAllocation groupAllocation) {
		this.groupAllocation = groupAllocation;
	}
}
