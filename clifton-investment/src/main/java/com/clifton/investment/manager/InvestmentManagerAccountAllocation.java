package com.clifton.investment.manager;


import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentAssetClass;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentManagerAccountAllocation</code> class allows allocating assets to one or more
 * asset classes (usually one).
 *
 * @author vgomelsky
 */
public class InvestmentManagerAccountAllocation extends BaseInvestmentManagerAllocation {


	private InvestmentAssetClass assetClass;


	////////////////////////////////////////////////////////////////////////////////
	////////////              Custom Cash Overlay Options             //////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If set, then this is an asset class allocation for custom cash overlay
	 */
	private boolean customCashOverlay;

	/**
	 * Either a percent or amount - determined by the assignment's {@link ManagerCustomCashOverlayTypes} value
	 */
	private BigDecimal customAllocationValue;

	/**
	 * The date the custom allocation value was entered as of.  Used when applying benchmark adjustment to the allocation value
	 */
	private Date customAllocationValueDate;

	/**
	 * If true, will adjust the allocation value based on the benchmark performance since the custom allocation value date.
	 * If a specific security is selected here, that benchmark will be used.  Otherwise, the asset class
	 */
	private boolean applyBenchmarkAdjustment;


	private InvestmentSecurity benchmarkSecurity;

	/**
	 * When using benchmark adjustments, the offset to the adjustment can be
	 * applied using the default (rules) - if defined or overlaid to Fund Cash using Fund Cash Weights
	 */
	private ManagerCashOverlayTypes benchmarkAdjustmentOffSetCashOverlayType;

	/**
	 * What cash bucket this cash is overlaid into
	 */
	private ManagerCashTypes cashType;


	private String note;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getTypeSpecificLabelSuffix() {
		return getAssetClass() == null ? "UNKNOWN" : getAssetClass().getLabel();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getCashTypeLabel() {
		if (getCashType() != null) {
			return getCashType().getLabel();
		}
		return null;
	}


	public InvestmentAssetClass getAssetClass() {
		return this.assetClass;
	}


	public void setAssetClass(InvestmentAssetClass assetClass) {
		this.assetClass = assetClass;
	}


	public boolean isCustomCashOverlay() {
		return this.customCashOverlay;
	}


	public void setCustomCashOverlay(boolean customCashOverlay) {
		this.customCashOverlay = customCashOverlay;
	}


	public ManagerCashTypes getCashType() {
		return this.cashType;
	}


	public void setCashType(ManagerCashTypes cashType) {
		this.cashType = cashType;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public BigDecimal getCustomAllocationValue() {
		return this.customAllocationValue;
	}


	public void setCustomAllocationValue(BigDecimal customAllocationValue) {
		this.customAllocationValue = customAllocationValue;
	}


	public Date getCustomAllocationValueDate() {
		return this.customAllocationValueDate;
	}


	public void setCustomAllocationValueDate(Date customAllocationValueDate) {
		this.customAllocationValueDate = customAllocationValueDate;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public boolean isApplyBenchmarkAdjustment() {
		return this.applyBenchmarkAdjustment;
	}


	public void setApplyBenchmarkAdjustment(boolean applyBenchmarkAdjustment) {
		this.applyBenchmarkAdjustment = applyBenchmarkAdjustment;
	}


	public ManagerCashOverlayTypes getBenchmarkAdjustmentOffSetCashOverlayType() {
		return this.benchmarkAdjustmentOffSetCashOverlayType;
	}


	public void setBenchmarkAdjustmentOffSetCashOverlayType(ManagerCashOverlayTypes benchmarkAdjustmentOffSetCashOverlayType) {
		this.benchmarkAdjustmentOffSetCashOverlayType = benchmarkAdjustmentOffSetCashOverlayType;
	}
}
