package com.clifton.investment.manager.balance;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.calendar.InvestmentEventManagerAdjustment;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceAdjustmentSearchForm;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceAdjustmentTypeSearchForm;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountBalanceService</code> ...
 *
 * @author Mary Anderson
 */
public interface InvestmentManagerAccountBalanceService {

	////////////////////////////////////////////////////////////////////////////
	////////           Investment Manager Account Methods                ///////
	//////			Relies on InvestmentManagerAccountBalance              /////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = InvestmentManagerAccountBalance.class)
	public List<InvestmentManagerAccount> getInvestmentManagerAccountMissingBalanceList(InvestmentManagerAccountSearchForm searchForm, final Date balanceDate);


	////////////////////////////////////////////////////////////////////////////
	//////        Investment Manager Account Balance Methods             /////// 
	///// ALL BALANCE METHODS ALSO GET/SAVE/DELETE ASSOCIATED ADJUSTMENTS! /////	
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalance getInvestmentManagerAccountBalance(int id);


	/**
	 * Lookup ManagerAccountBalance for a specific manager and date.  Optionally populate adjustments only if the actual adjustments are needed
	 * Uses ManagerAccountBalanceDate cache that stores for specific manager and date the balance id record that represents it
	 */
	public InvestmentManagerAccountBalance getInvestmentManagerAccountBalanceByManagerAndDate(int managerAccountId, Date balanceDate, boolean populateAdjustments);


	/**
	 * Looks up the latest manager account balance for the given manager as of the given date, or <tt>null</tt> if no balance exists for the manager. This is a <i>flexible</i>
	 * lookup, meaning that the <i>latest</i> balance will be retrieved as of the given date.
	 *
	 * @param managerAccountId    the manager account ID whose balance should be retrieved
	 * @param balanceDate         the date for which the balance must be on or before
	 * @param populateAdjustments <tt>true</tt> if the {@link InvestmentManagerAccountBalance#adjustmentList adjustments} should be populated, or <tt>false</tt> otherwise
	 * @return the manager account balance, or <tt>null</tt> if no balance was found
	 */
	public InvestmentManagerAccountBalance getInvestmentManagerAccountBalanceByManagerAndDateFlexible(int managerAccountId, Date balanceDate, boolean populateAdjustments);


	public List<InvestmentManagerAccountBalance> getInvestmentManagerAccountBalanceList(InvestmentManagerAccountBalanceSearchForm searchForm);


	public InvestmentManagerAccountBalance saveInvestmentManagerAccountBalance(InvestmentManagerAccountBalance bean);


	/**
	 * Used to generate new balance records.  If existingBean is not null will delete that balance record (first copying manual adjustments to the new balance record)
	 * then save the new balance record
	 *
	 * @param existingBean
	 * @param bean
	 * @param forceReload  - If the original securities/cash value is the same, and forceReload is true, will clear processing for the balance.  If forceReload is false, will not update it.
	 *                     Force Reload is used as false, for example, for linked manager balances which are dependent on PIOS runs and processed prior to the run being processed to only reload if there was an actual change.  If no change, not necessary to re-process the balance
	 */
	@DoNotAddRequestMapping
	public InvestmentManagerAccountBalance saveInvestmentManagerAccountBalanceReload(InvestmentManagerAccountBalance existingBean, InvestmentManagerAccountBalance bean, boolean forceReload);


	/**
	 * Used as a shortcut in UI for users to automatically copy manual adjustments from one day to the next.
	 * <p/>
	 * Need this because a bank may delay for a few days before showing the adjustment.
	 */
	public InvestmentManagerAccountBalance saveInvestmentManagerAccountBalanceAndCopyPreviousAdjustments(InvestmentManagerAccountBalance bean);


	/**
	 * Deletes the balance for the manager account and balanceDate and returns a copy of the manual adjustments that were on the balance
	 * so can be kept for reloads
	 */
	//public List<InvestmentManagerAccountBalanceAdjustment> deleteInvestmentManagerAccountBalanceByManagerAndDate(int managerAccountId, Date balanceDate);
	public void deleteInvestmentManagerAccountBalance(int id);


	////////////////////////////////////////////////////////////////////////////
	//////  Investment Manager Account Balance Adjustment Lookup Methods  ////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceAdjustment getInvestmentManagerAccountBalanceAdjustment(int id);


	@DoNotAddRequestMapping
	public InvestmentManagerAccountBalanceAdjustment createInvestmentManagerAccountBalanceAdjustmentFromEvent(InvestmentEventManagerAdjustment eventAdjustment);


	public List<InvestmentManagerAccountBalanceAdjustment> getInvestmentManagerAccountBalanceAdjustmentList(InvestmentManagerAccountBalanceAdjustmentSearchForm searchForm);


	/**
	 * Returns a list of adjustments across all managers for a balance date that should be copied forward to the next day automatically.
	 * <p/>
	 * This method is called by manager balance adjustment processing to get the full list.  There are relatively few that are copied forward automatically, so we
	 * get the full list at once so as not to lookup for each manager.
	 */
	@DoNotAddRequestMapping
	public List<InvestmentManagerAccountBalanceAdjustment> getInvestmentManagerAccountBalanceAdjustmentCopyNextDayList(Date balanceDate);


	/**
	 * Used for uploads to save the adjustment - will get the balance and add the adjustment to the balance
	 * and then save the balance record which performs all validation and recalculations
	 */
	public void saveInvestmentManagerAccountBalanceAdjustment(InvestmentManagerAccountBalanceAdjustment bean);


	////////////////////////////////////////////////////////////////////////////
	//////  Investment Manager Account Balance Adjustment Type Methods   /////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceAdjustmentType getInvestmentManagerAccountBalanceAdjustmentType(short id);


	public InvestmentManagerAccountBalanceAdjustmentType getInvestmentManagerAccountBalanceAdjustmentTypeByName(String name);


	public List<InvestmentManagerAccountBalanceAdjustmentType> getInvestmentManagerAccountBalanceAdjustmentTypeList(InvestmentManagerAccountBalanceAdjustmentTypeSearchForm searchForm);


	public InvestmentManagerAccountBalanceAdjustmentType saveInvestmentManagerAccountBalanceAdjustmentType(InvestmentManagerAccountBalanceAdjustmentType bean);


	public void deleteInvestmentManagerAccountBalanceAdjustmentType(short id);
}
