package com.clifton.investment.manager;


import com.clifton.core.beans.ManyToManyEntity;

import java.math.BigDecimal;


/**
 * The <code>InvestmentManagerAccountGroupAllocation</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountGroupAllocation extends ManyToManyEntity<InvestmentManagerAccountGroup, InvestmentManagerAccount, Integer> {

	private BigDecimal targetAllocation;

	private boolean rebalanceAllocationFixed;
	private BigDecimal rebalanceAllocationMin;
	private BigDecimal rebalanceAllocationMax;


	public BigDecimal getTargetAllocation() {
		return this.targetAllocation;
	}


	public void setTargetAllocation(BigDecimal targetAllocation) {
		this.targetAllocation = targetAllocation;
	}


	public boolean isRebalanceAllocationFixed() {
		return this.rebalanceAllocationFixed;
	}


	public void setRebalanceAllocationFixed(boolean rebalanceAllocationFixed) {
		this.rebalanceAllocationFixed = rebalanceAllocationFixed;
	}


	public BigDecimal getRebalanceAllocationMin() {
		return this.rebalanceAllocationMin;
	}


	public void setRebalanceAllocationMin(BigDecimal rebalanceAllocationMin) {
		this.rebalanceAllocationMin = rebalanceAllocationMin;
	}


	public BigDecimal getRebalanceAllocationMax() {
		return this.rebalanceAllocationMax;
	}


	public void setRebalanceAllocationMax(BigDecimal rebalanceAllocationMax) {
		this.rebalanceAllocationMax = rebalanceAllocationMax;
	}
}
