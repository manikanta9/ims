package com.clifton.investment.manager.balance;

import java.math.BigDecimal;
import java.util.function.Function;


/**
 * A selection of investment manager account balance types. These types may be used to extract a specific value type from a {@link
 * InvestmentManagerAccountBalance} entity.
 *
 * @author MikeH
 */
public enum InvestmentManagerAccountBalanceTypes {
	CASH(InvestmentManagerAccountBalance::getCashValue),
	CASH_ADJUSTED(InvestmentManagerAccountBalance::getAdjustedCashValue),
	CASH_MARKET_ON_CLOSE(InvestmentManagerAccountBalance::getMarketOnCloseCashValue),
	SECURITIES(InvestmentManagerAccountBalance::getSecuritiesValue),
	SECURITIES_ADJUSTED(InvestmentManagerAccountBalance::getAdjustedSecuritiesValue),
	SECURITIES_MARKET_ON_CLOSE(InvestmentManagerAccountBalance::getMarketOnCloseSecuritiesValue),
	TOTAL(InvestmentManagerAccountBalance::getTotalValue),
	TOTAL_ADJUSTED(InvestmentManagerAccountBalance::getAdjustedTotalValue),
	TOTAL_MARKET_ON_CLOSE(InvestmentManagerAccountBalance::getMarketOnCloseTotalValue);

	Function<InvestmentManagerAccountBalance, BigDecimal> accountBalanceRetriever;


	InvestmentManagerAccountBalanceTypes(Function<InvestmentManagerAccountBalance, BigDecimal> accountBalanceRetriever) {
		this.accountBalanceRetriever = accountBalanceRetriever;
	}


	/**
	 * Retrieves the balance value for this type from the given {@link InvestmentManagerAccountBalance} entity.
	 *
	 * @param managerAccountBalance the entity for which to retrieve the balance value
	 * @return the balance value
	 */
	public BigDecimal getValue(InvestmentManagerAccountBalance managerAccountBalance) {
		return this.accountBalanceRetriever.apply(managerAccountBalance);
	}
}
