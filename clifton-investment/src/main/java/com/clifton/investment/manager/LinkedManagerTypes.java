package com.clifton.investment.manager;


/**
 * The <code>LinkedManagerType</code> defines the different types of ManagerType = LINKED
 */
public enum LinkedManagerTypes {

	/**
	 * PIOS Processing Dependent Linked Managers
	 */

	// Comes from Manager Allocations (for managers assigned to a different account - cannot be assigned to the same account)
	MANAGER_MARKET_VALUE("Manager Market Value", true), // Cash & Securities
	MANAGER_CASH_MARKET_VALUE("Manager Cash Market Value", true), // Cash Only
	MANAGER_SECURITIES_MARKET_VALUE("Manager Securities Market Value", true), // Securities Only

	// Securities Only: comes from Overlay Exposure (our overlay positions)
	ACCOUNT_SYNTHETIC_EXPOSURE("Account Synthetic Exposure", true), // Product Overlay Asset Class Replication: Overlay Exposure Value

	// Securities Only: comes from Overlay Target (our overlay positions)
	ACCOUNT_OVERLAY_TARGET("Account Overlay Target", true), // Product Overlay Asset Class Replication: Overlay Target Adjusted Value

	/**
	 * System Calculated Linked Managers (No dependency on PIOS Runs)
	 */

	// Securities Only: comes from the exposure of our options positions held in the specified account.  Calculated as Contracts * Underlying Index Price * Delta * Option Price Multiplier.
	OPTION_POSITIONS_EXPOSURE("Option Positions Exposure", false), //

	TOTAL_MARKET_VALUE("Total Market Value", false), // CASH_MARKET_VALUE + POSITIONS_MARKET_VALUE
	CASH_MARKET_VALUE("Cash Market Value", false), // Cash Only: comes from the General Ledger cash balance held in the specified client account and broker account (optional)
	POSITIONS_MARKET_VALUE("Positions Market Value", false, true), // Securities Only: comes from the market value of our positions held in the specified account.
	POSITIONS_MARKET_VALUE_DURATION_ADJUSTED("Positions Market Value (Duration Adjusted)", false), // Securities Only: market value * security duration / benchmark duration.

	POSITIONS_NOTIONAL("Positions Notional", false, true), // Securities Only: comes from the notional value of our positions held in the specified account.
	POSITIONS_REPLICATION_EXPOSURE("Positions Replication Exposure", false), // Securities Only: comes from the calculated exposure of our positions held in the specified account using selected replication type (useful for setting balance to exposure in same account without run dependency).

	POSITIONS_QUANTITY_UNADJUSTED("Positions Quantity (Unadjusted)", false, true), // Securities Only: comes from the unadjusted quantity of our positions held in the specified account. Note that quantity can have special meaning - i.e. for CCY Forwards it is the amount in the underlying CCY of the forward

	SECURITY_GAIN_LOSS("Security Gain/Loss", false), // Securities: comes from the cumulative Gain/Loss from matching positions for the selected account since the given start date.  Balances on that start date are always zero.

	// Inactive Option - Receivables no longer supported by position.  Use General Ledger Balance type with GL Account options and optional currency
	OUTSTANDING_RECEIVABLES("Outstanding Receivables", false, 1, true, false), // Cash Only: comes from the market value of the receivable positions held in the specified account.

	ACCRUED_INTEREST_REPOS("Accrued Interest For REPOs", false), // Cash Only: comes from the interest from REPOs for the selected Client Account and optionally holding account. Open vs. Term interest is calculated based on its type.

	MANAGER_BALANCE_PROPRIETARY_FUND_PORTION("Manager Balance (Proprietary Fund Portion)", false, 100), // Cash and/or Securities: Selected Manager's Balance * Market Value of Fund Holdings in Client Account / Total Market Value of the Fund

	GENERAL_LEDGER_BALANCE("General Ledger Balance", false), // 'This manager\'s balance (Cash Only) comes from the General Ledger account balance held in the specified client account and broker account (optional) for a specific GL account/account type/account group.'
	PENDING_GENERAL_LEDGER_BALANCE("Pending General Ledger Balance", false), // 'This manager\'s balance (Cash Only) comes from the pending General Ledger account balance held in the specified client account and broker account (optional) for a specific GL account with Transaction Date <= Balance Date and Settlement Date > Balance Date'

	PENDING_EVENT_JOURNAL_BALANCE("Pending Event Journal Balance", false); // This manager\'s balance (Cash Only) comes from unbooked accounting event journals for the selected account/type/investment group options.

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Optional order can help the system put linked managers into a better processing order in case linked managers depend on each other
	 * Although the system can auto load the dependent balance, do we really need this???
	 */
	private final int order;

	/**
	 * Determines the type of Linked manager it really is - if it depends on PIOS processing and needs a linked run,
	 * or if it is just a system calculation that can be loaded before PIOS runs.
	 */
	private final boolean processingDependent;

	private final String label;

	private final boolean inactive;

	/**
	 * If true, then the linked manager type supports the manager being in a different base CCY than the linked client account
	 */
	private final boolean differentBaseCurrencyAllowed;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	LinkedManagerTypes(String label, boolean processingDependent) {
		this(label, processingDependent, 1);
	}


	LinkedManagerTypes(String label, boolean processingDependent, boolean differentBaseCurrencyAllowed) {
		this(label, processingDependent, 1, false, differentBaseCurrencyAllowed);
	}


	LinkedManagerTypes(String label, boolean processingDependent, int order) {
		this(label, processingDependent, order, false, false);
	}


	LinkedManagerTypes(String label, boolean processingDependent, int order, boolean inactive, boolean differentBaseCurrencyAllowed) {
		this.label = label;
		this.processingDependent = processingDependent;
		this.order = order;
		this.inactive = inactive;
		this.differentBaseCurrencyAllowed = differentBaseCurrencyAllowed;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public int getOrder() {
		return this.order;
	}


	public boolean isProcessingDependent() {
		return this.processingDependent;
	}


	public String getLabel() {
		return this.label;
	}


	public boolean isInactive() {
		return this.inactive;
	}


	public boolean isDifferentBaseCurrencyAllowed() {
		return this.differentBaseCurrencyAllowed;
	}
}
