package com.clifton.investment.manager.upload;

/**
 * The InvestmentManagerAccountAllocationUploadTemplate is a simplified class that is used to easily upload new manager allocation weights for existing manager assignments
 * Created by Mary Anderson
 */
public class InvestmentManagerAccountAllocationUploadTemplate extends BaseInvestmentManagerAccountAllocationUploadTemplate {

	private String assetClassName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getAssetClassName() {
		return this.assetClassName;
	}


	public void setAssetClassName(String assetClassName) {
		this.assetClassName = assetClassName;
	}
}
