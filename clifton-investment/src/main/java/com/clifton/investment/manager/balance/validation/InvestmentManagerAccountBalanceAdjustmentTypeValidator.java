package com.clifton.investment.manager.balance.validation;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustmentType;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentManagerAccountBalanceAdjustmentTypeValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentManagerAccountBalanceAdjustmentTypeValidator extends SelfRegisteringDaoValidator<InvestmentManagerAccountBalanceAdjustmentType> {

	private ReadOnlyDAO<InvestmentManagerAccountBalanceAdjustment> investmentManagerAccountBalanceAdjustmentDAO;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(InvestmentManagerAccountBalanceAdjustmentType bean, DaoEventTypes config) throws ValidationException {
		// Uses DAO Method instead
	}


	@Override
	public void validate(InvestmentManagerAccountBalanceAdjustmentType bean, DaoEventTypes config, ReadOnlyDAO<InvestmentManagerAccountBalanceAdjustmentType> dao) throws ValidationException {
		if (config.isInsert()) {
			if (bean.isSystemDefined()) {
				throw new FieldValidationException("You cannot enter a new system defined adjustment type.", "systemDefined");
			}
		}
		else {
			InvestmentManagerAccountBalanceAdjustmentType original = getOriginalBean(bean);
			if (original.isSystemDefined()) {
				throw new ValidationException("You cannot edit or delete a system defined adjustment type.");
			}
			if (config.isUpdate()) {
				boolean validateUsedAsNextDay = false;
				if (original.isCashAdjustmentAllowed() && !bean.isCashAdjustmentAllowed()) {
					if (!CollectionUtils.isEmpty(getInvestmentManagerAccountBalanceAdjustmentDAO().findByFields(new String[]{"adjustmentType.id", "cashAdjustment"},
							new Object[]{bean.getId(), true}))) {
						throw new FieldValidationException("You cannot prohibit cash adjustments for this type as there already exists cash adjustments tied to this type.", "cashAdjustmentsAllowed");
					}
					validateUsedAsNextDay = true;
				}
				if (original.isSecuritiesAdjustmentAllowed() && !bean.isSecuritiesAdjustmentAllowed()) {
					if (!CollectionUtils.isEmpty(getInvestmentManagerAccountBalanceAdjustmentDAO().findByFields(new String[]{"adjustmentType.id", "cashAdjustment"},
							new Object[]{bean.getId(), false}))) {
						throw new FieldValidationException("You cannot prohibit securities adjustments for this type as there already exists securities adjustments tied to this type.",
								"securitiesAdjustmentsAllowed");
					}
					validateUsedAsNextDay = true;
				}
				if (validateUsedAsNextDay) {
					// Find any cases where this entry is used as a next day adjustment and validate copy from/to both support required cash/securities
					List<InvestmentManagerAccountBalanceAdjustmentType> usedByList = dao.findByField("nextDayAdjustmentType.id", bean.getId());
					for (InvestmentManagerAccountBalanceAdjustmentType usedBy : CollectionUtils.getIterable(usedByList)) {
						validateNextDayAdjustmentTypeSelection(usedBy, bean);
					}
				}
			}
		}
		if (config.isInsert() || config.isUpdate()) {
			ValidationUtils.assertTrue(bean.isCashAdjustmentAllowed() || bean.isSecuritiesAdjustmentAllowed(), "At least one of Cash and/or Securities adjustments must be allowed.");
			validateNextDayAdjustmentTypeSelection(bean, bean.getNextDayAdjustmentType());
		}
	}


	private void validateNextDayAdjustmentTypeSelection(InvestmentManagerAccountBalanceAdjustmentType adjustmentType, InvestmentManagerAccountBalanceAdjustmentType nextDayAdjustmentType) {
		if (adjustmentType != null && nextDayAdjustmentType != null) {
			ValidationUtils.assertFalse(nextDayAdjustmentType.isSystemDefined(), "Adjustment Type [" + nextDayAdjustmentType.getName()
					+ "] cannot be used as a next day adjustment because it is system defined.");
			if (adjustmentType.isCashAdjustmentAllowed()) {
				ValidationUtils.assertTrue(nextDayAdjustmentType.isCashAdjustmentAllowed(), "Adjustment Type [" + adjustmentType.getName() + "] allows cash adjustments.  Next Day Adjustment Type ["
						+ nextDayAdjustmentType.getName() + "] must also allow cash adjustments.");
			}
			if (adjustmentType.isSecuritiesAdjustmentAllowed()) {
				ValidationUtils.assertTrue(nextDayAdjustmentType.isSecuritiesAdjustmentAllowed(), "Adjustment Type [" + adjustmentType.getName()
						+ "] allows securities adjustments.  Next Day Adjustment Type [" + nextDayAdjustmentType.getName() + "] must also allow securities adjustments.");
			}
		}
	}


	public ReadOnlyDAO<InvestmentManagerAccountBalanceAdjustment> getInvestmentManagerAccountBalanceAdjustmentDAO() {
		return this.investmentManagerAccountBalanceAdjustmentDAO;
	}


	public void setInvestmentManagerAccountBalanceAdjustmentDAO(ReadOnlyDAO<InvestmentManagerAccountBalanceAdjustment> investmentManagerAccountBalanceAdjustmentDAO) {
		this.investmentManagerAccountBalanceAdjustmentDAO = investmentManagerAccountBalanceAdjustmentDAO;
	}
}
