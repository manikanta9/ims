package com.clifton.investment.manager.balance.cache;

import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountBalanceAdjustmentCopyNextDayCacheImpl</code> class provides caching and retrieval from cache of InvestmentManagerAccountBalanceAdjustment objects
 * for a specific date where the adjustment type is flagged to be copied forward to the following day.
 * <p>
 * There are not very many of these daily, but we need them  in order to process the next day's balance.  Instead of looking them up
 * for each manager, lookup all at once and cache it.  Cache is automatically cleared if an adjustment for that date that is set to copy forward
 * is inserted, updated, or deleted.  Because this is not two business days ago, changes to the balance are very unlikely.
 *
 * @author manderson
 */
public interface InvestmentManagerAccountBalanceAdjustmentCopyNextDayCache {


	public List<InvestmentManagerAccountBalanceAdjustment> getInvestmentManagerAccountBalanceAdjustmentList(Date balanceDate);


	public void storeInvestmentManagerAccountBalanceAdjustmentList(Date balanceDate, List<InvestmentManagerAccountBalanceAdjustment> list);
}
