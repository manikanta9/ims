package com.clifton.investment.manager;


import com.clifton.core.beans.ManyToManyEntity;

import java.math.BigDecimal;


/**
 * The <code>InvestmentManagerAccountRollup</code> defines the {@link InvestmentManagerAccount} that is a child manager account
 * of a parent account.  The parent account must be selected as IsRollup = true;
 *
 * @author Mary Anderson
 */
public class InvestmentManagerAccountRollup extends ManyToManyEntity<InvestmentManagerAccount, InvestmentManagerAccount, Integer> {

	/**
	 * Specifies the portion of the child account balance to allocate to this manager.
	 * Usually it's 100%.
	 */
	private BigDecimal allocationPercent;


	public BigDecimal getAllocationPercent() {
		return this.allocationPercent;
	}


	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}
}
