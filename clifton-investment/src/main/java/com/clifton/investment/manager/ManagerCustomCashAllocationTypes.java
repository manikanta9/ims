package com.clifton.investment.manager;


/**
 * The <code>ManagerCustomCashAllocationTypes</code> defines how the system should ALLOCATE manager cash when custom cash overlay is utilized
 *
 * @author manderson
 */
public enum ManagerCustomCashAllocationTypes {

	MANAGER_ASSET_CLASSES(true, false), // Allocate Cash to Manager Asset Classes
	CUSTOM_CASH_ASSET_CLASSES(false, true), // Allocate Cash to Custom Cash Asset Classes Only (anything left over is ignored)
	CUSTOM_CASH_ASSET_CLASSES_APPLY_REMAINDER(true, true); // Allocate Cash to Custom Asset Classes and Remainder to Manager Asset Classes


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, then cash is allocated to manager asset classes (if custom cash asset classes is also used, then this will only be used for the remainder)
	 */
	private final boolean allocateUsingManagerAssetClasses;


	/**
	 * If true, then cash is allocated to custom cash asset classes
	 */
	private final boolean allocateUsingCustomCashAssetClasses;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	ManagerCustomCashAllocationTypes(boolean allocateUsingManagerAssetClasses, boolean allocateUsingCustomCashAssetClasses) {
		this.allocateUsingManagerAssetClasses = allocateUsingManagerAssetClasses;
		this.allocateUsingCustomCashAssetClasses = allocateUsingCustomCashAssetClasses;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isAllocateUsingManagerAssetClasses() {
		return this.allocateUsingManagerAssetClasses;
	}


	public boolean isAllocateUsingCustomCashAssetClasses() {
		return this.allocateUsingCustomCashAssetClasses;
	}
}
