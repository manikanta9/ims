package com.clifton.investment.manager.balance.stale;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.SubselectParameterExpression;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.balance.stale.search.InvestmentManagerAccountBalanceStaleDetailSearchForm;
import com.clifton.investment.manager.balance.stale.search.InvestmentManagerAccountBalanceStaleSummarySearchForm;
import org.hibernate.Criteria;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Service
public class InvestmentManagerAccountBalanceStaleServiceImpl implements InvestmentManagerAccountBalanceStaleService {

	private AdvancedReadOnlyDAO<InvestmentManagerAccountBalanceStaleSummary, Criteria> investmentManagerAccountBalanceStaleSummaryDAO;
	private AdvancedReadOnlyDAO<InvestmentManagerAccountBalanceStaleDetail, Criteria> investmentManagerAccountBalanceStaleDetailDAO;

	private CalendarBusinessDayService calendarBusinessDayService;


	////////////////////////////////////////////////////////////////////////////
	//////           Investment Manager Account Balance Stale             //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentManagerAccountBalanceStaleSummary> getInvestmentManagerAccountBalanceStaleSummaryList(final InvestmentManagerAccountBalanceStaleSummarySearchForm searchForm) {
		ValidationUtils.assertNotNull(searchForm.getBalanceDate(), "BalanceDate is required for InvestmentManagerAccountBalanceStaleSummary list find.");

		final Date businessDay = searchForm.getBalanceDate();
		final Date averageEndBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(searchForm.getBalanceDate()),
				searchForm.getNumberOfDaysForAverage() != null ? -1 * searchForm.getNumberOfDaysForAverage() : -90);
		searchForm.setBalanceDate(null);
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(new SubselectParameterExpression(new DateType(), businessDay));
				addManagerType(criteria);
				addAdjustCashFilter(criteria);
				criteria.add(new SubselectParameterExpression(new DateType(), averageEndBusinessDay));
				criteria.add(new SubselectParameterExpression(new DateType(), businessDay));
				addManagerType(criteria);
				addAdjustCashFilter(criteria);

				super.configureCriteria(criteria);
			}


			private void addManagerType(Criteria criteria) {
				if (searchForm.getManagerType() == null) {
					criteria.add(new SubselectParameterExpression(new StringType(), "%"));
				}
				else {
					criteria.add(new SubselectParameterExpression(new StringType(), searchForm.getManagerType().toString()));
				}
			}


			private void addAdjustCashFilter(Criteria criteria) {
				if ((searchForm.getAdjustedCashValueGreaterThanZero() != null) && Boolean.TRUE.equals(searchForm.getAdjustedCashValueGreaterThanZero())) {
					criteria.add(new SubselectParameterExpression(new BooleanType(), false));
				}
				else {
					criteria.add(new SubselectParameterExpression(new BooleanType(), true));
				}
				criteria.add(new SubselectParameterExpression(new BigDecimalType(), BigDecimal.ZERO));
			}
		};
		return getInvestmentManagerAccountBalanceStaleSummaryDAO().findBySearchCriteria(config);
	}


	////////////////////////////////////////////////////////////////////////////
	//////          Investment Manager Account Balance Extended           //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentManagerAccountBalanceStaleDetail> getInvestmentManagerAccountBalanceStaleDetailList(InvestmentManagerAccountBalanceStaleDetailSearchForm searchForm) {
		ValidationUtils.assertNotNull(searchForm.getBalanceDate(), "BalanceDate is required for InvestmentManagerAccountBalanceStaleDetail list find.");
		final Date businessDay = searchForm.getBalanceDate();
		//final Date prevBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(searchForm.getBalanceDate(), -1);
		searchForm.setBalanceDate(null);
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				//criteria.add(new SubselectParameterExpression(new DateType(), prevBusinessDay));
				criteria.add(new SubselectParameterExpression(new DateType(), businessDay));

				super.configureCriteria(criteria);
			}
		};
		return getInvestmentManagerAccountBalanceStaleDetailDAO().findBySearchCriteria(config);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<InvestmentManagerAccountBalanceStaleDetail, Criteria> getInvestmentManagerAccountBalanceStaleDetailDAO() {
		return this.investmentManagerAccountBalanceStaleDetailDAO;
	}


	public void setInvestmentManagerAccountBalanceStaleDetailDAO(AdvancedReadOnlyDAO<InvestmentManagerAccountBalanceStaleDetail, Criteria> investmentManagerAccountBalanceStaleDetailDAO) {
		this.investmentManagerAccountBalanceStaleDetailDAO = investmentManagerAccountBalanceStaleDetailDAO;
	}


	public AdvancedReadOnlyDAO<InvestmentManagerAccountBalanceStaleSummary, Criteria> getInvestmentManagerAccountBalanceStaleSummaryDAO() {
		return this.investmentManagerAccountBalanceStaleSummaryDAO;
	}


	public void setInvestmentManagerAccountBalanceStaleSummaryDAO(AdvancedReadOnlyDAO<InvestmentManagerAccountBalanceStaleSummary, Criteria> investmentManagerAccountBalanceStaleSummaryDAO) {
		this.investmentManagerAccountBalanceStaleSummaryDAO = investmentManagerAccountBalanceStaleSummaryDAO;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
