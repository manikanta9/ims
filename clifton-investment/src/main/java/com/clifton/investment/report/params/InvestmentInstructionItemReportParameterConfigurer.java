package com.clifton.investment.report.params;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.params.ReportConfigParameter;
import com.clifton.report.params.ReportParameterConfigurer;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentAccountGroupReportParameterConfigurer</code> return the report params for an account group
 * (Returns a list of account ids)
 *
 * @author manderson
 */
@Component
public class InvestmentInstructionItemReportParameterConfigurer implements ReportParameterConfigurer<InvestmentInstructionItem> {

	private InvestmentInstructionService investmentInstructionService;

	private SecurityUserService securityUserService;


	@Override
	public String getParameterName() {
		return "investmentInstructionItemIds";
	}


	@Override
	public List<InvestmentInstructionItem> getReportParameterValueList(ReportConfigParameter configParameter, @SuppressWarnings("unused") Date reportDate) {
		if (configParameter == null || StringUtils.isEmpty(configParameter.getValue())) {
			return null;
		}
		String instructionItemIdsString = configParameter.getValue();
		String[] instructionItemIds = instructionItemIdsString.split(",");

		if (instructionItemIds.length == 0) {
			return null;
		}

		List<InvestmentInstructionItem> items = new ArrayList<>();
		List<String> reportGroupingKeys = new ArrayList<>();
		for (String instructionItemId : instructionItemIds) {
			int id = Integer.parseInt(instructionItemId.trim());
			InvestmentInstructionItem item = getInvestmentInstructionService().getInvestmentInstructionItem(id);
			if (reportGroupingKeys.contains(item.getReportGroupingUniqueKey())) {
				continue;
			}
			else {
				reportGroupingKeys.add(item.getReportGroupingUniqueKey());
			}
			items.add(getInvestmentInstructionService().getInvestmentInstructionItem(id));
		}
		return items;
	}


	@Override
	public void populateReportParameterMap(Map<String, Object> parameterMap, InvestmentInstructionItem value) {
		parameterMap.put("RecipientCompanyID", value.getInstruction().getRecipientCompany().getId());
		parameterMap.put("InstructionID", value.getInstruction().getId());
		parameterMap.put("InstructionDefinitionID", value.getInstruction().getDefinition().getId());
		parameterMap.put("FKFieldID", value.getFkFieldId());
		parameterMap.put("InstructionDate", DateUtils.fromDateShort(value.getInstruction().getInstructionDate()));
		parameterMap.put("RunAsUserID", getSecurityUserService().getSecurityUserCurrent().getId());
	}


	@SuppressWarnings("unused")
	@Override
	public void populateReportPostingFileConfiguration(ReportPostingFileConfiguration postConfig, InvestmentInstructionItem value) {
		// NO POSTING SUPPORTED
		throw new IllegalStateException("Posting Not Supported for Settlement Instruction Reports.");
	}


	@Override
	public Integer getReportIdForValue(@SuppressWarnings("unused") ReportExportConfiguration exportConfig, InvestmentInstructionItem value) {
		if (value.getInstruction().getDefinition().getReport() == null) {
			throw new ValidationException("Instruction Definition [" + value.getInstruction().getDefinition().getName() + "] is missing a report selection.");
		}
		return value.getInstruction().getDefinition().getReport().getId();
	}


	@Override
	public String getLabelForValue(InvestmentInstructionItem value) {
		return value.getFkFieldLabel();
	}


	/**
	 * If left null, the value from the ReportExportConfiguration (typically this is what is sent in from the UI) is used
	 */
	@Override
	public String generateFileNameWithoutExtension(InvestmentInstructionItem value, Date reportDate) {
		return null;
	}


	@Override
	public FileWrapper getAttachments(InvestmentInstructionItem value, ReportExportConfiguration configuration) {
		return null;
	}

	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
