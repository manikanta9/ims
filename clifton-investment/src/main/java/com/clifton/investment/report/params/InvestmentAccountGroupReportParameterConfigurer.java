package com.clifton.investment.report.params;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.params.ReportConfigParameter;
import com.clifton.report.params.ReportParameterConfigurer;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentAccountGroupReportParameterConfigurer</code> return the report params for an account group
 * (Returns a list of account ids)
 *
 * @author manderson
 */
@Component
public class InvestmentAccountGroupReportParameterConfigurer implements ReportParameterConfigurer<InvestmentAccount> {

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getParameterName() {
		return "investmentAccountGroupId";
	}


	@Override
	public List<InvestmentAccount> getReportParameterValueList(ReportConfigParameter configParameter, @SuppressWarnings("unused") Date reportDate) {
		if (configParameter == null || StringUtils.isEmpty(configParameter.getValue())) {
			return null;
		}
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOrderBy("number:ASC");
		searchForm.setInvestmentAccountGroupId(new Integer(configParameter.getValue()));
		List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		ValidationUtils.assertTrue(CollectionUtils.getSize(accountList) < 101, "Cannot run a report set for more than 100 accounts, there are currently [" + CollectionUtils.getSize(accountList)
				+ "] in the selected group.");
		return accountList;
	}


	@Override
	public void populateReportParameterMap(Map<String, Object> parameterMap, InvestmentAccount value) {
		parameterMap.put("ClientAccountID", value.getId());
	}


	@Override
	public Integer getReportIdForValue(ReportExportConfiguration exportConfig, @SuppressWarnings("unused") InvestmentAccount value) {
		return exportConfig.getReportId();
	}


	@Override
	public void populateReportPostingFileConfiguration(ReportPostingFileConfiguration postConfig, InvestmentAccount value) {
		postConfig.setPostEntitySourceTableName(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME);
		postConfig.setPostEntitySourceFkFieldId(value.getId());
	}


	@Override
	public String getLabelForValue(InvestmentAccount value) {
		return "Account: " + value.getLabel();
	}


	/**
	 * If left null, the value from the ReportExportConfiguration (typically this is what is sent in from the UI) is used
	 */
	@Override
	public String generateFileNameWithoutExtension(InvestmentAccount value, Date reportDate) {
		return null;
	}


	@Override
	public FileWrapper getAttachments(InvestmentAccount value, ReportExportConfiguration configuration) {
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
