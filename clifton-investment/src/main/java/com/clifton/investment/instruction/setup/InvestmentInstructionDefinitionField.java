package com.clifton.investment.instruction.setup;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>InvestmentInstructionDefinitionField</code> defines a specific field on the instruction
 * Some fields can be dynamically populated from the record the instruction is for, others are hardcoded values,
 * i.e. Bank, ABA #, Beneficiary, Further Credit
 * <p>
 * Dynamic fields (like include For Further Credit (FFC) line with holding account number at the end of instructions is defined by the report selection.
 *
 * @author manderson
 */
public class InvestmentInstructionDefinitionField extends NamedEntity<Integer> {

	private InvestmentInstructionDefinition definition;

	/**
	 * String value of the field - i.e. Bank name, ABA number, etc.
	 */
	private String value;

	private Short fieldOrder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(InvestmentInstructionDefinition definition) {
		this.definition = definition;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public Short getFieldOrder() {
		return this.fieldOrder;
	}


	public void setFieldOrder(Short fieldOrder) {
		this.fieldOrder = fieldOrder;
	}
}
