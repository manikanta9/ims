package com.clifton.investment.instruction.run.messaging;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.messaging.InstructionMessagingResponseHandler;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.instruction.messaging.message.InstructionMessagingStatusMessage;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;
import com.clifton.investment.instruction.run.search.InvestmentInstructionRunInstructionItemSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Handles the response from SWIFT messaging server
 *
 * @author mwacker
 */
@Component("instructionMessagingResponseHandler")
public class SwiftInvestmentInstructionMessagingResponseHandlerImpl implements InstructionMessagingResponseHandler {

	private InvestmentInstructionStatusService investmentInstructionStatusService;
	private InvestmentInstructionService investmentInstructionService;
	private InvestmentInstructionRunService investmentInstructionRunService;
	private SystemSchemaUtilHandler systemSchemaUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void process(InstructionMessagingStatusMessage message) {
		String uniqueStringIdentifier = message.getUniqueStringIdentifier();
		uniqueStringIdentifier = InstructionGenerator.getItemEntityUniqueStringIdentifier(uniqueStringIdentifier);
		IdentityObject dtoObject = getSystemSchemaUtilHandler().getEntityFromUniqueId(uniqueStringIdentifier);

		InvestmentInstructionItem item;
		if (dtoObject instanceof InvestmentInstructionItem) {
			item = (InvestmentInstructionItem) dtoObject;
		}
		else {
			// deprecated
			Set<InvestmentInstructionItem> itemList = new HashSet<>(getInvestmentInstructionService().getInvestmentInstructionItemListForEntityObjectAndType(dtoObject, InvestmentInstructionRunTypes.SWIFT));
			item = CollectionUtils.getOnlyElement(itemList);
			ValidationUtils.assertNotNull(itemList, "No instruction items were found for instruction entity with uniqueStringIdentifier [" + uniqueStringIdentifier + "].  Message: " + message);
		}
		ValidationUtils.assertNotNull(item, "No instruction item was found for instruction entity with uniqueStringIdentifier [" + uniqueStringIdentifier + "].  Message: " + message);

		if (item != null) {
			InvestmentInstructionRun run = getInvestmentInstructionRunService().getInvestmentInstructionRunForItemLatestByType(item.getId(), InvestmentInstructionRunTypes.SWIFT);

			String messageResult = message.getMessage();
			InvestmentInstructionStatus existingRunStatus = run.getRunStatus();

			if (message.getStatus() == InstructionStatuses.PROCESSED && !existingRunStatus.isFail()) {
				setStatus(run, InvestmentInstructionStatusNames.COMPLETED, messageResult);
			}

			if (message.getStatus() == null || message.getStatus() == InstructionStatuses.FAILED) {
				setStatus(run, InvestmentInstructionStatusNames.ERROR, messageResult);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setStatus(InvestmentInstructionRun run, InvestmentInstructionStatusNames status, String messageResult) {
		setRunItemStatus(run, status);
		setRunStatus(run, status, messageResult);
		getInvestmentInstructionService().refreshInvestmentInstructionStatus(run.getInvestmentInstruction(), null);
	}


	private void setRunStatus(InvestmentInstructionRun run, InvestmentInstructionStatusNames status, String messageResult) {
		StringBuilder message = new StringBuilder();
		if (run.getHistoryDescription() != null) {
			message.append(run.getHistoryDescription());
			message.append("\n\n");
		}
		message.append("INTEGRATION RESULT:\n");
		message.append(messageResult);

		run.setRunStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(status));
		run.setHistoryDescription(StringUtils.formatStringUpToNCharsWithDots(message.toString(), DataTypes.DESCRIPTION_LONG.getLength(), true));
		getInvestmentInstructionRunService().saveInvestmentInstructionRun(run);
	}


	private void setRunItemStatus(InvestmentInstructionRun run, InvestmentInstructionStatusNames status) {
		InvestmentInstructionRunInstructionItemSearchForm searchForm = new InvestmentInstructionRunInstructionItemSearchForm();
		searchForm.setInstructionRunId(run.getId());
		List<InvestmentInstructionRunInstructionItem> runItems = getInvestmentInstructionRunService().getInvestmentInstructionRunInstructionItemList(searchForm);

		InvestmentInstructionStatus investmentInstructionStatus = getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(status);
		for (InvestmentInstructionRunInstructionItem runItem : runItems) {
			InvestmentInstructionItem item = runItem.getReferenceTwo();
			setItemStatus(item, investmentInstructionStatus);
			getInvestmentInstructionService().saveInvestmentInstructionItem(item);
		}

		getInvestmentInstructionService().refreshInvestmentInstructionStatus(run.getInvestmentInstruction(), null);
	}


	/**
	 * 'COMPLETED' a cancelled item to 'CANCELLED'.
	 */
	private void setItemStatus(InvestmentInstructionItem item, InvestmentInstructionStatus status) {
		InvestmentInstructionStatusNames mappedStatusName = InvestmentInstructionStatusNames.getCancelledStatus(item, status.getInvestmentInstructionStatusName());
		InvestmentInstructionStatus mappedStatus = mappedStatusName != status.getInvestmentInstructionStatusName()
				? getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(mappedStatusName) : status;
		item.setStatus(mappedStatus);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionStatusService getInvestmentInstructionStatusService() {
		return this.investmentInstructionStatusService;
	}


	public void setInvestmentInstructionStatusService(InvestmentInstructionStatusService investmentInstructionStatusService) {
		this.investmentInstructionStatusService = investmentInstructionStatusService;
	}


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public InvestmentInstructionRunService getInvestmentInstructionRunService() {
		return this.investmentInstructionRunService;
	}


	public void setInvestmentInstructionRunService(InvestmentInstructionRunService investmentInstructionRunService) {
		this.investmentInstructionRunService = investmentInstructionRunService;
	}


	public SystemSchemaUtilHandler getSystemSchemaUtilHandler() {
		return this.systemSchemaUtilHandler;
	}


	public void setSystemSchemaUtilHandler(SystemSchemaUtilHandler systemSchemaUtilHandler) {
		this.systemSchemaUtilHandler = systemSchemaUtilHandler;
	}
}
