package com.clifton.investment.instruction.group;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instruction.InvestmentInstruction;


/**
 * @author manderson
 */
public interface InvestmentInstructionGroupService {

	@DoNotAddRequestMapping
	public InvestmentInstructionItemGroup getInvestmentInstructionItemGroupByName(String name, boolean addIfMissing);


	/**
	 * Rebuilds groupings on an instruction
	 *
	 * @param instructionId
	 */
	@SecureMethod(dtoClass = InvestmentInstruction.class)
	public void processInvestmentInstrumentItemGroupsForInstruction(int instructionId);


	/**
	 * To be used by batch job for weekly maintenance to clear out unused groupings
	 * This should rarely happen - only if instructions were generated incorrectly and then later removed
	 */
	public void deleteInvestmentInstructionItemGroupListByUnused();
}
