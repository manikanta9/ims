package com.clifton.investment.instruction.run;

import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.investment.instruction.InvestmentInstructionItem;


/**
 * Many-To-Many link between Investment Instruction Runs and Investment Instruction Items
 * <p>
 * A Run may have many items and an Item may be tied to several runs (For example if there are errors that prevent
 * the run from successfully completing).  Also some run types (SWIFT) only support one instruction item per run.
 *
 * @author theodorez
 */
public class InvestmentInstructionRunInstructionItem extends ManyToManyEntity<InvestmentInstructionRun, InvestmentInstructionItem, Integer> {
	// Nothing in here
}
