package com.clifton.investment.instruction.delivery;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.Instruction;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryBusinessCompanySearchForm;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryFieldSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


@Component
public class InvestmentInstructionDeliveryUtilHandlerImpl implements InvestmentInstructionDeliveryUtilHandler {

	private static final String DELIVERY_TYPE_EXECUTING_BROKER = "Executing Broker";

	private InvestmentInstructionDeliveryBusinessCompanyService investmentInstructionDeliveryBusinessCompanyService;
	private InvestmentInstructionDeliveryService investmentInstructionDeliveryService;
	private InvestmentAccountGroupService investmentAccountGroupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionDeliveryType getDeliveryType(Instruction instruction) {
		return Optional.of(instruction)
				.filter(InvestmentInstructionItem.class::isInstance)
				.map(InvestmentInstructionItem.class::cast)
				.map(i -> i.getInstruction().getDefinition().getDeliveryType())
				.orElse(null);
	}


	@Override
	public InvestmentInstructionDeliveryType getExecutingBrokerDeliveryType() {
		return getInvestmentInstructionDeliveryService().getInvestmentInstructionDeliveryTypeByName(DELIVERY_TYPE_EXECUTING_BROKER);
	}


	@Override
	public InvestmentInstructionDelivery getInvestmentInstructionDelivery(Instruction instruction, BusinessCompany businessCompany, InvestmentSecurity deliveryCurrency) {
		ValidationUtils.assertTrue(InvestmentInstructionItem.class.isAssignableFrom(instruction.getClass()), "The instruction parameter must be derived from Investment Instruction Item.");
		InvestmentInstructionItem investmentInstructionItem = (InvestmentInstructionItem) instruction;
		ValidationUtils.assertNotNull(investmentInstructionItem.getInstruction(), "Investment Instruction is required.");
		ValidationUtils.assertNotNull(investmentInstructionItem.getInstruction().getDefinition(), "Investment Instruction Definition is required.");
		ValidationUtils.assertNotNull(investmentInstructionItem.getInstruction().getDefinition().getDeliveryType(), () ->
				String.format("Investment Instruction Definition [%s] requires a delivery type.", investmentInstructionItem.getInstruction().getDefinition().getName()));
		return getInvestmentInstructionDelivery(getDeliveryType(instruction), businessCompany, deliveryCurrency);
	}


	@Override
	public InvestmentInstructionDelivery getInvestmentInstructionDelivery(InvestmentInstructionDeliveryType type, BusinessCompany businessCompany, InvestmentSecurity deliveryCurrency) {
		AssertUtils.assertNotNull(type, "Instruction delivery type is required.");
		AssertUtils.assertNotNull(businessCompany, "Instruction delivery business company is required.");
		AssertUtils.assertNotNull(deliveryCurrency, "Instruction delivery currency is required.");

		return getInvestmentInstructionDeliveryService().getInvestmentInstructionDeliveryByCompanyAndTypeAndCurrency(businessCompany,
				type, deliveryCurrency);
	}


	@Override
	public List<String> getAdditionalPartyDeliveryFieldBicList(InstructionDeliveryFieldCommand deliveryFieldCommand) {
		List<String> results = new ArrayList<>();
		InvestmentInstructionDeliveryType deliveryType = getDeliveryType(deliveryFieldCommand.getInstruction());
		if (Objects.nonNull(deliveryType) && Objects.nonNull(deliveryFieldCommand.getDeliveryCompany())
				&& Objects.nonNull(deliveryFieldCommand.getDeliveryCurrency()) && Objects.nonNull(deliveryFieldCommand.getDeliveryAccount())) {
			InvestmentInstructionDeliveryBusinessCompanySearchForm searchForm = new InvestmentInstructionDeliveryBusinessCompanySearchForm();
			searchForm.setBusinessCompanyId(deliveryFieldCommand.getDeliveryCompany().getId());
			searchForm.setTypeId(deliveryType.getId());
			searchForm.setDeliveryCurrencyId(deliveryFieldCommand.getDeliveryCurrency().getId());
			List<InvestmentInstructionDeliveryBusinessCompany> deliveryBusinessCompanies = getInvestmentInstructionDeliveryBusinessCompanyService().getInvestmentInstructionDeliveryBusinessCompanyList(searchForm);
			if (Objects.nonNull(deliveryBusinessCompanies) && deliveryBusinessCompanies.size() == 1) {
				List<InvestmentInstructionDeliveryField> additionalPartyFields = getAdditionalPartyDeliveryFieldList(deliveryBusinessCompanies.get(0).getReferenceOne(), deliveryType, deliveryFieldCommand);
				results.addAll(CollectionUtils.getStream(additionalPartyFields)
						.map(InvestmentInstructionDeliveryField::getValue)
						.filter(v -> !StringUtils.isEmpty(v))
						.distinct()
						.collect(Collectors.toList())
				);
			}
		}
		return results;
	}


	@Override
	public List<InvestmentInstructionDeliveryField> getAdditionalPartyDeliveryFieldList(InvestmentInstructionDelivery deliveryInstruction, InvestmentInstructionDeliveryType deliveryType, InstructionDeliveryFieldCommand deliveryFieldCommand) {
		AssertUtils.assertNotNull(deliveryInstruction, "Delivery Instruction is required.");
		InvestmentInstructionDeliveryFieldType interestedPartyFields = getInvestmentInstructionDeliveryService().getInvestmentInstructionDeliveryFieldTypeByName(InvestmentInstructionDeliveryServiceImpl.FIELD_TYPE_INTERESTED_PARTIES);
		AssertUtils.assertNotNull(interestedPartyFields, "Delivery Field Type " + InvestmentInstructionDeliveryServiceImpl.FIELD_TYPE_INTERESTED_PARTIES + " could not be found.");
		InvestmentInstructionDeliveryFieldSearchForm searchForm = new InvestmentInstructionDeliveryFieldSearchForm();
		// required search values
		searchForm.setTypeId(interestedPartyFields.getId());
		searchForm.setDeliveryId(deliveryInstruction.getId());
		// optional selection criteria
		ObjectUtils.doIfPresent(deliveryType, dt -> searchForm.setDeliveryTypeIdOrNull(dt.getId()));
		ObjectUtils.doIfPresent(deliveryFieldCommand.getDeliveryAccount(), da -> searchForm.setHoldingAccountIdOrNull(da.getId()));
		ObjectUtils.doIfPresent(deliveryFieldCommand.getDeliveryClientAccount(), ca -> searchForm.setClientAccountIdOrNull(ca.getId()));
		List<InvestmentInstructionDeliveryField> fieldList = getInvestmentInstructionDeliveryService().getInvestmentInstructionDeliveryFieldList(searchForm);
		// if field defines an account group, check group membership.
		return CollectionUtils.getStream(fieldList)
				.filter(fld -> Objects.isNull(fld.getHoldingAccountGroup()) ||
						getInvestmentAccountGroupService().isInvestmentAccountInGroup(deliveryFieldCommand.getDeliveryAccount().getId(), fld.getHoldingAccountGroup().getName()))
				.collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionDeliveryBusinessCompanyService getInvestmentInstructionDeliveryBusinessCompanyService() {
		return this.investmentInstructionDeliveryBusinessCompanyService;
	}


	public void setInvestmentInstructionDeliveryBusinessCompanyService(InvestmentInstructionDeliveryBusinessCompanyService investmentInstructionDeliveryBusinessCompanyService) {
		this.investmentInstructionDeliveryBusinessCompanyService = investmentInstructionDeliveryBusinessCompanyService;
	}


	public InvestmentInstructionDeliveryService getInvestmentInstructionDeliveryService() {
		return this.investmentInstructionDeliveryService;
	}


	public void setInvestmentInstructionDeliveryService(InvestmentInstructionDeliveryService investmentInstructionDeliveryService) {
		this.investmentInstructionDeliveryService = investmentInstructionDeliveryService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}
}
