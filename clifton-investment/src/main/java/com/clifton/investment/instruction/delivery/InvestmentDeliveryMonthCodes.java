package com.clifton.investment.instruction.delivery;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;


/**
 * The <code>InvestmentDeliveryMonthCodes</code>
 * <p/>
 * 'Delivery Month': A key characteristic of a futures contract that designates when the contract expires and
 * when the underlying asset must be delivered. The exchange on the futures contract is traded will also
 * establish a delivery location and a date within the delivery month when the delivery can take place.
 * Not all futures contracts require physical delivery of a commodity, and many are settled in cash.
 * <p/>
 * Also referred to as "contract month."
 * <p/>
 * On the ticker, delivery month is indicated by a letter.
 *
 * @author manderson
 */
public enum InvestmentDeliveryMonthCodes {

	F(1, "January"), //
	G(2, "February"), //
	H(3, "March"), //
	J(4, "April"), //
	K(5, "May"), //
	M(6, "June"), //
	N(7, "July"), //
	Q(8, "August"), //
	U(9, "September"), //
	V(10, "October"), //
	X(11, "November"), //
	Z(12, "December"); //

	/**
	 * Uses same int value for month numbers that
	 * {@link com.clifton.calendar.setup.CalendarMonth} uses:
	 * equal to the {@link java.util.Calendar} int
	 * representation of the month + 1. Reason behind this is that Java
	 * Calendar starts months at 0 and we start at 1.
	 */
	private final int monthNumber;

	/**
	 * Name of the month for visual appearance
	 */
	private final String monthName;


	InvestmentDeliveryMonthCodes(int monthNumber, String monthName) {
		this.monthNumber = monthNumber;
		this.monthName = monthName;
	}


	///////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////


	public static boolean isValidCode(String code) {
		InvestmentDeliveryMonthCodes codeEnum;
		try {
			codeEnum = InvestmentDeliveryMonthCodes.valueOf(code);
		}
		catch (Exception e) {
			return false;
		}
		return codeEnum != null;
	}


	public static InvestmentDeliveryMonthCodes valueOfMonthName(String month) {

		ValidationUtils.assertFalse(StringUtils.isEmpty(month), "Month Name is required to get delivery month code.");
		month = month.trim();
		for (InvestmentDeliveryMonthCodes cd : InvestmentDeliveryMonthCodes.values()) {
			if (month.equalsIgnoreCase(cd.getMonthName())) {
				return cd;
			}
		}
		throw new ValidationException("Missing Month Code for Month Name: " + month + ". Please use valid month names only.");
	}


	public static InvestmentDeliveryMonthCodes valueOfMonthNumber(int month) {
		for (InvestmentDeliveryMonthCodes cd : InvestmentDeliveryMonthCodes.values()) {
			if (month == cd.getMonthNumber()) {
				return cd;
			}
		}
		throw new ValidationException("Missing Month Code for Calendar Month Number: " + month);
	}


	public static InvestmentDeliveryMonthCodes valueOfDate(Date date) {
		ValidationUtils.assertNotNull(date, "Date is required to get delivery month code.");
		return valueOfMonthNumber(DateUtils.getMonthOfYear(date));
	}


	///////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////


	public String getMonthName() {
		return this.monthName;
	}


	public int getMonthNumber() {
		return this.monthNumber;
	}
}
