package com.clifton.investment.instruction;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.run.runner.InvestmentInstructionRunnerService;
import com.clifton.investment.instruction.setup.InvestmentInstructionContactGroup;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionContactSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author mwacker
 */
@Service
public class InvestmentInstructionSendingServiceImpl implements InvestmentInstructionSendingService {

	private InvestmentInstructionService investmentInstructionService;
	private InvestmentInstructionSetupService investmentInstructionSetupService;
	private InvestmentInstructionRunnerService investmentInstructionRunnerService;
	private InvestmentInstructionStatusService investmentInstructionStatusService;

	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////


	@Override
	public void sendInvestmentInstructionList(int[] instructionIds, Date wireDate) {
		for (int id : instructionIds) {
			InvestmentInstruction instruction = getInvestmentInstructionService().getInvestmentInstruction(id);
			InvestmentInstructionStatus instructionStatus = instruction.getStatus();
			//Instruction must not have already been sent (i.e. have ERROR or OPEN status)
			if (instructionStatus.isSendable()) {
				handleInvestmentInstructionItemSending(instruction.getItemList(), wireDate);
			}
		}
	}


	@Override
	public void sendInvestmentInstruction(int id, Date wireDate) {
		sendInvestmentInstructionList(new int[]{id}, wireDate);
	}


	@Override
	public void sendInvestmentInstructionItemList(int[] itemIds, Date wireDate) {
		List<InvestmentInstructionItem> items = new ArrayList<>();
		for (int id : itemIds) {
			InvestmentInstructionItem item = getInvestmentInstructionService().getInvestmentInstructionItem(id);
			ValidationUtils.assertNotNull(item, String.format("Instruction item [%s] could not be fetched.", id));
			items.add(item);
		}
		handleInvestmentInstructionItemSending(items, wireDate);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void handleInvestmentInstructionItemSending(List<InvestmentInstructionItem> items, Date wireDate) {
		if (!CollectionUtils.isEmpty(items)) {
			validateInstructionPriorToSending(items.get(0).getInstruction());
			validateInstructionItemsPriorToSend(items);
			if (items.iterator().next().getInstruction().getDefinition().isRequiresWireDate()) {
				ValidationUtils.assertNotNull(wireDate, () -> String.format("Wire Date required for processing instruction [%s].", items.iterator().next().getInstruction().getId()));
			}
		}
		try {
			List<InvestmentInstructionItem> filteredItems = getFilteredInvestmentInstructionItemsList(items, wireDate);
			getInvestmentInstructionRunnerService().sendItems(filteredItems);
		}
		catch (Throwable t) {
			LogUtils.error(InvestmentInstructionSendingServiceImpl.class, "Error sending instructions.", t);
		}
	}


	@Transactional
	protected List<InvestmentInstructionItem> getFilteredInvestmentInstructionItemsList(List<InvestmentInstructionItem> items, Date wireDate) {
		List<InvestmentInstructionItem> filteredItems = new ArrayList<>();
		getInvestmentInstructionService().populateInvestmentInstructionItemFkFieldLabel(items);
		for (InvestmentInstructionItem item : CollectionUtils.getIterable(items)) {
			InvestmentInstructionStatus itemStatus = item.getStatus();
			// items must be in OPEN or ERROR or PENDING_CANCEL status to be sent
			if (itemStatus.isSendable() && item.getReady()) {
				if (Objects.nonNull(item.getStatus()) && item.getStatus().isCancel()) {
					item.setStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.SENT_CANCEL));
				}
				else {
					item.setStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.SEND));
				}
				ObjectUtils.doIfPresent(wireDate, item::setWireDate);
				filteredItems.add(getInvestmentInstructionService().saveInvestmentInstructionItem(item));
			}
		}
		return filteredItems;
	}


	private void validateInstructionPriorToSending(InvestmentInstruction instruction) {
		ValidationUtils.assertFalse(instruction.isRegenerating(), "Cannot send instruction " + instruction.getLabel() + " because it is locked for Instruction Generation");
		InvestmentInstructionDefinition definition = instruction.getDefinition();
		InvestmentInstructionContactGroup group = definition.getContactGroup();
		//A definition must have a group specified to use the automated sending functionality
		ValidationUtils.assertNotNull(group, "A contact group must be specified for the definition: " + definition.getLabel());

		//The group selected must have recipients for the instruction recipient company to use the automated sending functionality
		InvestmentInstructionContactSearchForm searchForm = new InvestmentInstructionContactSearchForm();
		searchForm.setContactGroupId(group.getId());
		searchForm.setCompanyId(instruction.getRecipientCompany().getId());
		ValidationUtils.assertNotEmpty(getInvestmentInstructionSetupService().getInvestmentInstructionContactList(searchForm), "The contact group ( " + group.getLabel() + " ) selected on definition: " + definition.getLabel() + " does not have any recipients for the company of this instruction (" + instruction.getRecipientCompany().getLabel() + ")");
	}


	/**
	 * Replacement messages cannot be sent until its cancellation has been fully processed.
	 */
	private void validateInstructionItemsPriorToSend(List<InvestmentInstructionItem> items) {
		final List<InvestmentInstructionItem> violations = CollectionUtils.asNonNullList(items).stream()
				.map(i ->
						Optional.ofNullable(i.getReferencedInvestmentInstructionItem())
								.flatMap(r ->
										Optional.ofNullable(r.getStatus())
												.flatMap(s -> Optional.of(s.getInvestmentInstructionStatusName()))
												.flatMap(n -> n == InvestmentInstructionStatusNames.CANCELED ? Optional.empty() : Optional.of(r))
								).orElse(null)
				)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		ValidationUtils.assertEmpty(violations, "The replacement message cannot be sent until its corresponding cancel message is [CANCELLED] successfully.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public InvestmentInstructionSetupService getInvestmentInstructionSetupService() {
		return this.investmentInstructionSetupService;
	}


	public void setInvestmentInstructionSetupService(InvestmentInstructionSetupService investmentInstructionSetupService) {
		this.investmentInstructionSetupService = investmentInstructionSetupService;
	}


	public InvestmentInstructionRunnerService getInvestmentInstructionRunnerService() {
		return this.investmentInstructionRunnerService;
	}


	public void setInvestmentInstructionRunnerService(InvestmentInstructionRunnerService investmentInstructionRunnerService) {
		this.investmentInstructionRunnerService = investmentInstructionRunnerService;
	}


	public InvestmentInstructionStatusService getInvestmentInstructionStatusService() {
		return this.investmentInstructionStatusService;
	}


	public void setInvestmentInstructionStatusService(InvestmentInstructionStatusService investmentInstructionStatusService) {
		this.investmentInstructionStatusService = investmentInstructionStatusService;
	}
}
