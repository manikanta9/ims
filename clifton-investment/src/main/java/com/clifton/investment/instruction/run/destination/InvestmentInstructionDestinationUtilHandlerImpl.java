package com.clifton.investment.instruction.run.destination;


import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;


@Component
public class InvestmentInstructionDestinationUtilHandlerImpl implements InvestmentInstructionDestinationUtilHandler {

	private SecurityUserService securityUserService;
	private BusinessContactService businessContactService;

	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	@Override
	public BusinessContact getSenderContact(InvestmentInstructionRun run) {
		BusinessContact contact = null;
		SecurityUser user = getSecurityUserService().getSecurityUser(run.getUpdateUserId());

		if (user != null) {
			Integer contactId = user.getContact() != null ? user.getContact().getId() : user.getContactIdentifier();
			if (contactId != null) {
				contact = getBusinessContactService().getBusinessContact(contactId);
			}
		}
		return contact;
	}


	@Override
	public BusinessContact getFromContact(InvestmentInstructionRun run, BusinessContact defaultContact) {
		InvestmentInstructionDefinition definition = run.getInvestmentInstruction().getDefinition();
		BusinessContact contact = definition.getFromContact() != null ? definition.getFromContact() : definition.getCategory().getFromContact();
		return contact != null ? contact : defaultContact;
	}

	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}
}
