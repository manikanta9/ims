package com.clifton.investment.instruction.run.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithTimeSearchForm;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;

import java.util.Date;


/**
 * @author theodorez
 */
public class InvestmentInstructionRunSearchForm extends BaseAuditableEntityDateRangeWithTimeSearchForm {

	@SearchField(searchFieldPath = "investmentInstruction", searchField = "definition.name,definition.category.name,recipientCompany.name")
	private String searchPattern;

	@SearchField(searchField = "investmentInstruction.id")
	private Integer instructionId;

	@SearchField(searchField = "investmentInstruction.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] instructionIds;

	@SearchField(searchField = "runStatus.id")
	private Short runStatus;

	@SearchField(searchField = "scheduledDate")
	private Date runScheduledDate;

	@SearchField(searchField = "runType")
	private InvestmentInstructionRunTypes runType;

	@SearchField(searchField = "runType", comparisonConditions = {ComparisonConditions.IN})
	private InvestmentInstructionRunTypes[] runTypeList;

	@SearchField(searchField = "name", searchFieldPath = "runStatus", comparisonConditions = {ComparisonConditions.IN})
	private String[] instructionRunStatusList;

	@SearchField(searchField = "instructionItemList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer itemId;

	@SearchField(searchField = "instructionItemList.fkFieldId", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer itemFkFieldId;

	@SearchField(searchFieldPath = "investmentInstruction.definition.category", searchField = "table.id")
	private Short tableId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInstructionId() {
		return this.instructionId;
	}


	public void setInstructionId(Integer instructionId) {
		this.instructionId = instructionId;
	}


	public Integer[] getInstructionIds() {
		return this.instructionIds;
	}


	public void setInstructionIds(Integer[] instructionIds) {
		this.instructionIds = instructionIds;
	}


	public Short getRunStatus() {
		return this.runStatus;
	}


	public void setRunStatus(Short runStatus) {
		this.runStatus = runStatus;
	}


	public Date getRunScheduledDate() {
		return this.runScheduledDate;
	}


	public void setRunScheduledDate(Date runScheduledDate) {
		this.runScheduledDate = runScheduledDate;
	}


	public InvestmentInstructionRunTypes getRunType() {
		return this.runType;
	}


	public void setRunType(InvestmentInstructionRunTypes runType) {
		this.runType = runType;
	}


	public InvestmentInstructionRunTypes[] getRunTypeList() {
		return this.runTypeList;
	}


	public void setRunTypeList(InvestmentInstructionRunTypes[] runTypeList) {
		this.runTypeList = runTypeList;
	}


	public String[] getInstructionRunStatusList() {
		return this.instructionRunStatusList;
	}


	public void setInstructionRunStatusList(String[] instructionRunStatusList) {
		this.instructionRunStatusList = instructionRunStatusList;
	}


	public Integer getItemId() {
		return this.itemId;
	}


	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getItemFkFieldId() {
		return this.itemFkFieldId;
	}


	public void setItemFkFieldId(Integer itemFkFieldId) {
		this.itemFkFieldId = itemFkFieldId;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}
}
