package com.clifton.investment.instruction.group;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.group.populator.InvestmentInstructionItemGroupPopulator;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class InvestmentInstructionGroupServiceImpl implements InvestmentInstructionGroupService {

	private AdvancedUpdatableDAO<InvestmentInstructionItemGroup, Criteria> investmentInstructionItemGroupDAO;

	private DaoLocator daoLocator;
	private InvestmentInstructionService investmentInstructionService;
	private SystemBeanService systemBeanService;


	@Override
	public InvestmentInstructionItemGroup getInvestmentInstructionItemGroupByName(String name, boolean addIfMissing) {
		// Max Length of 500 -
		name = StringUtils.formatStringUpToNCharsWithDots(name, DataTypes.DESCRIPTION.getLength(), true);
		InvestmentInstructionItemGroup group = getInvestmentInstructionItemGroupDAO().findOneByField("name", name);
		if (group == null && addIfMissing) {
			group = new InvestmentInstructionItemGroup();
			group.setName(name);
			getInvestmentInstructionItemGroupDAO().save(group);
		}
		return group;
	}


	@Override
	public void processInvestmentInstrumentItemGroupsForInstruction(int instructionId) {
		InvestmentInstruction instruction = getInvestmentInstructionService().getInvestmentInstruction(instructionId);
		if (instruction.getDefinition().getItemGroupPopulatorBean() == null) {
			// Clear All Groupings on the Instruction
			for (InvestmentInstructionItem item : CollectionUtils.getIterable(instruction.getItemList())) {
				item.setItemGroup(null);
			}
		}
		else {
			InvestmentInstructionItemGroupPopulator populator = (InvestmentInstructionItemGroupPopulator) getSystemBeanService().getBeanInstance(instruction.getDefinition().getItemGroupPopulatorBean());
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(instruction.getDefinition().getCategory().getTable().getName());
			Map<String, InvestmentInstructionItemGroup> groupNameMap = new HashMap<>();
			// Clear All Groupings on the Instruction
			for (InvestmentInstructionItem item : CollectionUtils.getIterable(instruction.getItemList())) {
				item.setItemGroup(null);
				IdentityObject bean = dao.findByPrimaryKey(item.getFkFieldId());
				if (bean != null) {
					String groupName = populator.populateInvestmentInstructionItemGroup(bean);
					if (groupNameMap.containsKey(groupName)) {
						item.setItemGroup(groupNameMap.get(groupName));
					}
					else {
						InvestmentInstructionItemGroup itemGroup = getInvestmentInstructionItemGroupByName(groupName, true);
						item.setItemGroup(itemGroup);
						groupNameMap.put(groupName, itemGroup);
					}
				}
			}
		}
		getInvestmentInstructionService().saveInvestmentInstruction(instruction, true);
	}


	/**
	 * To be used by batch job for weekly maintenance to clear out unused groupings
	 * This should rarely happen - only if instructions were generated incorrectly and then later removed
	 */
	@Override
	public void deleteInvestmentInstructionItemGroupListByUnused() {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentInstructionItem.class, "item");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("itemGroup.id", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.notExists(sub));
		};
		List<InvestmentInstructionItemGroup> itemGroupList = getInvestmentInstructionItemGroupDAO().findBySearchCriteria(searchConfigurer);
		getInvestmentInstructionItemGroupDAO().deleteList(itemGroupList);
	}


	////////////////////////////////////////////////////////////
	//////         Getter and Setter Methods           /////////
	////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstructionItemGroup, Criteria> getInvestmentInstructionItemGroupDAO() {
		return this.investmentInstructionItemGroupDAO;
	}


	public void setInvestmentInstructionItemGroupDAO(AdvancedUpdatableDAO<InvestmentInstructionItemGroup, Criteria> investmentInstructionItemGroupDAO) {
		this.investmentInstructionItemGroupDAO = investmentInstructionItemGroupDAO;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
