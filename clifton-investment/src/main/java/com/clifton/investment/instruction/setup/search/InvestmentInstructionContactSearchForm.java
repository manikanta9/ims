package com.clifton.investment.instruction.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithTimeSearchForm;


/**
 * @author theodorez
 */
public class InvestmentInstructionContactSearchForm extends BaseAuditableEntityDateRangeWithTimeSearchForm {

	@SearchField(searchField = "destinationSystemBean.description,company.name,company.companyLegalName,company.alternateCompanyName,company.alias")
	private String searchPattern;

	@SearchField(searchField = "group.id")
	private Integer contactGroupId;

	@SearchField(searchField = "company.id")
	private Integer companyId;

	@SearchField(searchFieldPath = "company", searchField = "name,alias")
	private String companyLabel;

	@SearchField(searchFieldPath = "destinationSystemBean.type", searchField = "name")
	private String destinationSystemBeanTypeName;

	@SearchField(searchFieldPath = "destinationSystemBean", searchField = "description")
	private String destinationSystemBeanDescription;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getContactGroupId() {
		return this.contactGroupId;
	}


	public void setContactGroupId(Integer contactGroupId) {
		this.contactGroupId = contactGroupId;
	}


	public Integer getCompanyId() {
		return this.companyId;
	}


	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	public String getCompanyLabel() {
		return this.companyLabel;
	}


	public void setCompanyLabel(String companyLabel) {
		this.companyLabel = companyLabel;
	}


	public String getDestinationSystemBeanTypeName() {
		return this.destinationSystemBeanTypeName;
	}


	public void setDestinationSystemBeanTypeName(String destinationSystemBeanTypeName) {
		this.destinationSystemBeanTypeName = destinationSystemBeanTypeName;
	}


	public String getDestinationSystemBeanDescription() {
		return this.destinationSystemBeanDescription;
	}


	public void setDestinationSystemBeanDescription(String destinationSystemBeanDescription) {
		this.destinationSystemBeanDescription = destinationSystemBeanDescription;
	}
}
