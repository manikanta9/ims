package com.clifton.investment.instruction.setup;


import com.clifton.business.contact.BusinessContact;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>InvestmentInstructionCategory</code> classifies the {@link InvestmentInstructionDefinition}s we have in the system
 * and associates them with tables, bean selectors, etc.
 *
 * @author manderson
 */
@CacheByName
public class InvestmentInstructionCategory extends NamedEntity<Short> {

	public static final String CATEGORY_SELECTOR_SYSTEM_BEAN_GROUP = "Investment Instruction Selector";

	/**
	 * Table this category applies to, i.e. Trade, AccountingM2MDaily
	 */
	private SystemTable table;

	/**
	 * When viewing category specific screens - instead of just seeing "Date", can be more specific about the date that it is
	 * because we often instruct on the following business day - i.e. Trade Date, Event Date, Mark Date
	 */
	private String dateLabel;

	/**
	 * Defines the list of {@link SystemBean}s that can be selected on the {@link InvestmentInstructionDefinition} that is used
	 * to filter which records are applied the definition as well as the logic to select recipient company for each record.
	 * <p>
	 * These bean types are specific to a category, and we can create a new instance of the class to use to find "missing" instructions.
	 */
	private SystemBeanType selectorBeanType;

	/**
	 * Is instruction required for every entity in this category
	 */
	private boolean required;

	/**
	 * Classification: Cash vs. Securities
	 */
	private boolean cash;

	/**
	 * Classification: Client vs. Counterparty
	 */
	private boolean client;

	/**
	 * The contact that instructions will be sent from
	 */
	private BusinessContact fromContact;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public boolean isRequired() {
		return this.required;
	}


	public void setRequired(boolean required) {
		this.required = required;
	}


	public boolean isCash() {
		return this.cash;
	}


	public void setCash(boolean cash) {
		this.cash = cash;
	}


	public boolean isClient() {
		return this.client;
	}


	public void setClient(boolean client) {
		this.client = client;
	}


	public SystemBeanType getSelectorBeanType() {
		return this.selectorBeanType;
	}


	public void setSelectorBeanType(SystemBeanType selectorBeanType) {
		this.selectorBeanType = selectorBeanType;
	}


	public String getDateLabel() {
		return this.dateLabel;
	}


	public void setDateLabel(String dateLabel) {
		this.dateLabel = dateLabel;
	}


	public BusinessContact getFromContact() {
		return this.fromContact;
	}


	public void setFromContact(BusinessContact fromContact) {
		this.fromContact = fromContact;
	}
}
