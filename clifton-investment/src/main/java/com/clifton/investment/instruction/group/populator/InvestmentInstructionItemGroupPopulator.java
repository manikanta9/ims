package com.clifton.investment.instruction.group.populator;

import com.clifton.core.beans.IdentityObject;


/**
 * The <code>InvestmentInstructionItemGroupPopulator</code> interface takes an entity (i.e. a Trade) and returns the grouping label. How the grouping is defined is based on the implementation of the interface.
 * For example, Trades could be groups by Client Account and Holding Account.  This is useful when the reports group multiple entities on the same report and send them together with a net amount.
 * <p>
 * These grouping implementations are created by development for users as they need to match how the selected report handles grouping.  For example, if the report includes the Client Account
 * in the header of the report, then grouping options must include the Client Account.
 *
 * @author manderson
 */
public interface InvestmentInstructionItemGroupPopulator {


	public String populateInvestmentInstructionItemGroup(IdentityObject entity);
}
