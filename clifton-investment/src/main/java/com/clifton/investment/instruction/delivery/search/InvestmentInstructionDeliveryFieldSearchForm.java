package com.clifton.investment.instruction.delivery.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class InvestmentInstructionDeliveryFieldSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField(searchField = "delivery.id")
	private Integer deliveryId;

	@SearchField(searchField = "delivery.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullDeliveryId;

	@SearchField(searchField = "deliveryType.id")
	private Short deliveryTypeId;

	@SearchField(searchField = "deliveryType.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Short deliveryTypeIdOrNull;

	@SearchField(searchField = "holdingAccountGroup.id")
	private Integer holdingAccountGroupId;

	@SearchField(searchField = "holdingAccountGroup.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] holdingAccountGroupIds;

	@SearchField(searchField = "holdingAccountGroup.id", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL)
	private Integer[] holdingAccountGroupIdsOrNull;

	@SearchField(searchField = "holdingAccount.id")
	private Integer holdingAccountId;

	@SearchField(searchField = "holdingAccount.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Integer holdingAccountIdOrNull;

	@SearchField(searchField = "clientAccount.id")
	private Integer clientAccountId;

	@SearchField(searchField = "clientAccount.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Integer clientAccountIdOrNull;

	@SearchField
	private Short fieldOrder;

	@SearchField
	private String value;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Integer getDeliveryId() {
		return this.deliveryId;
	}


	public void setDeliveryId(Integer deliveryId) {
		this.deliveryId = deliveryId;
	}


	public Boolean getNullDeliveryId() {
		return this.nullDeliveryId;
	}


	public void setNullDeliveryId(Boolean nullDeliveryId) {
		this.nullDeliveryId = nullDeliveryId;
	}


	public Short getDeliveryTypeId() {
		return this.deliveryTypeId;
	}


	public void setDeliveryTypeId(Short deliveryTypeId) {
		this.deliveryTypeId = deliveryTypeId;
	}


	public Short getDeliveryTypeIdOrNull() {
		return this.deliveryTypeIdOrNull;
	}


	public void setDeliveryTypeIdOrNull(Short deliveryTypeIdOrNull) {
		this.deliveryTypeIdOrNull = deliveryTypeIdOrNull;
	}


	public Integer getHoldingAccountGroupId() {
		return this.holdingAccountGroupId;
	}


	public void setHoldingAccountGroupId(Integer holdingAccountGroupId) {
		this.holdingAccountGroupId = holdingAccountGroupId;
	}


	public Integer[] getHoldingAccountGroupIds() {
		return this.holdingAccountGroupIds;
	}


	public void setHoldingAccountGroupIds(Integer[] holdingAccountGroupIds) {
		this.holdingAccountGroupIds = holdingAccountGroupIds;
	}


	public Integer[] getHoldingAccountGroupIdsOrNull() {
		return this.holdingAccountGroupIdsOrNull;
	}


	public void setHoldingAccountGroupIdsOrNull(Integer[] holdingAccountGroupIdsOrNull) {
		this.holdingAccountGroupIdsOrNull = holdingAccountGroupIdsOrNull;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer getHoldingAccountIdOrNull() {
		return this.holdingAccountIdOrNull;
	}


	public void setHoldingAccountIdOrNull(Integer holdingAccountIdOrNull) {
		this.holdingAccountIdOrNull = holdingAccountIdOrNull;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer getClientAccountIdOrNull() {
		return this.clientAccountIdOrNull;
	}


	public void setClientAccountIdOrNull(Integer clientAccountIdOrNull) {
		this.clientAccountIdOrNull = clientAccountIdOrNull;
	}


	public Short getFieldOrder() {
		return this.fieldOrder;
	}


	public void setFieldOrder(Short fieldOrder) {
		this.fieldOrder = fieldOrder;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}
}
