package com.clifton.investment.instruction.delivery;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.NamedEntity;


/**
 * The <code>InvestmentInstructionDelivery</code> class defines cash/currency/security delivery instructions.
 * Delivery instructions are usually setup for brokers and contain information about broker's
 * bank account (usually a different company but may be the same).  All clients wire money to the same
 * bank account and include client account number in the wire so that funds can be allocated accordingly.
 * <p/>
 * NOTE: at this time we only wire mark to market (most of the time in base currency of the client).
 * This section may need to be enhanced in the future.
 *
 * @author vgomelsky
 */
public class InvestmentInstructionDelivery extends NamedEntity<Integer> {

	private BusinessCompany deliveryCompany;

	private String deliveryAccountNumber;
	private String deliveryAccountName;
	private String deliveryABA;
	private String deliverySwiftCode;
	private String deliveryText;
	/**
	 * Some banks require additional line on delivery instructions (international?)
	 */
	private String deliveryForFurtherCredit;

	/**
	 * If true, generate a separate wire transfer for collateral (don't include it in mark to market letter, etc.)
	 */
	private boolean separateWireForCollateral;

	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////


	public BusinessCompany getDeliveryCompany() {
		return this.deliveryCompany;
	}


	public void setDeliveryCompany(BusinessCompany deliveryCompany) {
		this.deliveryCompany = deliveryCompany;
	}


	public String getDeliveryAccountNumber() {
		return this.deliveryAccountNumber;
	}


	public void setDeliveryAccountNumber(String deliveryAccountNumber) {
		this.deliveryAccountNumber = deliveryAccountNumber;
	}


	public String getDeliveryAccountName() {
		return this.deliveryAccountName;
	}


	public void setDeliveryAccountName(String deliveryAccountName) {
		this.deliveryAccountName = deliveryAccountName;
	}


	public String getDeliveryABA() {
		return this.deliveryABA;
	}


	public void setDeliveryABA(String deliveryABA) {
		this.deliveryABA = deliveryABA;
	}


	public String getDeliverySwiftCode() {
		return this.deliverySwiftCode;
	}


	public void setDeliverySwiftCode(String deliverySwiftCode) {
		this.deliverySwiftCode = deliverySwiftCode;
	}


	public String getDeliveryText() {
		return this.deliveryText;
	}


	public void setDeliveryText(String deliveryText) {
		this.deliveryText = deliveryText;
	}


	public String getDeliveryForFurtherCredit() {
		return this.deliveryForFurtherCredit;
	}


	public void setDeliveryForFurtherCredit(String deliveryForFurtherCredit) {
		this.deliveryForFurtherCredit = deliveryForFurtherCredit;
	}


	public boolean isSeparateWireForCollateral() {
		return this.separateWireForCollateral;
	}


	public void setSeparateWireForCollateral(boolean separateWireForCollateral) {
		this.separateWireForCollateral = separateWireForCollateral;
	}
}
