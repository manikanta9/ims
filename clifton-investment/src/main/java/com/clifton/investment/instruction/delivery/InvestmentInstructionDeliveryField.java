package com.clifton.investment.instruction.delivery;


import com.clifton.core.beans.NamedEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;


/**
 * The <code>InvestmentInstructionDeliveryField</code> defines a specific field on the instruction
 * Some fields can be dynamically populated from the record the instruction is for, others are hardcoded values,
 * i.e. Bank, ABA #, Beneficiary, Further Credit
 * <p>
 * Dynamic fields (like include For Further Credit (FFC) line with holding account number at the end of instructions is defined by the report selection.
 */
public class InvestmentInstructionDeliveryField extends NamedEntity<Integer> {

	private InvestmentInstructionDelivery delivery;

	private InvestmentInstructionDeliveryFieldType type;
	private InvestmentInstructionDeliveryType deliveryType;
	private InvestmentAccountGroup holdingAccountGroup;
	private InvestmentAccount holdingAccount;
	private InvestmentAccount clientAccount;

	/**
	 * String value of the field - i.e. Bank name, ABA number, etc.
	 */
	private String value;

	private Short fieldOrder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionDelivery getDelivery() {
		return this.delivery;
	}


	public void setDelivery(InvestmentInstructionDelivery delivery) {
		this.delivery = delivery;
	}


	public InvestmentInstructionDeliveryFieldType getType() {
		return this.type;
	}


	public void setType(InvestmentInstructionDeliveryFieldType type) {
		this.type = type;
	}


	public InvestmentInstructionDeliveryType getDeliveryType() {
		return this.deliveryType;
	}


	public void setDeliveryType(InvestmentInstructionDeliveryType deliveryType) {
		this.deliveryType = deliveryType;
	}


	public InvestmentAccountGroup getHoldingAccountGroup() {
		return this.holdingAccountGroup;
	}


	public void setHoldingAccountGroup(InvestmentAccountGroup holdingAccountGroup) {
		this.holdingAccountGroup = holdingAccountGroup;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public Short getFieldOrder() {
		return this.fieldOrder;
	}


	public void setFieldOrder(Short fieldOrder) {
		this.fieldOrder = fieldOrder;
	}
}
