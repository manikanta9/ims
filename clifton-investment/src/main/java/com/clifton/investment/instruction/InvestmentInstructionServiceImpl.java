package com.clifton.investment.instruction;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.InstructionService;
import com.clifton.instruction.export.InstructionExportHandler;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.generator.InstructionGeneratorLocator;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.InstructionPreviewMessage;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;
import com.clifton.investment.instruction.run.search.InvestmentInstructionRunInstructionItemSearchForm;
import com.clifton.investment.instruction.search.InvestmentInstructionItemSearchForm;
import com.clifton.investment.instruction.search.InvestmentInstructionSearchForm;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelector;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionContactSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


/**
 * The <code>InvestmentInstructionServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class InvestmentInstructionServiceImpl<M extends InstructionMessage, E extends IdentityObject> implements InvestmentInstructionService {

	private AdvancedUpdatableDAO<InvestmentInstruction, Criteria> investmentInstructionDAO;
	private AdvancedUpdatableDAO<InvestmentInstructionItem, Criteria> investmentInstructionItemDAO;

	private DaoLocator daoLocator;

	private InvestmentInstructionRunService investmentInstructionRunService;
	private InvestmentInstructionSetupService investmentInstructionSetupService;
	private InvestmentInstructionStatusService investmentInstructionStatusService;

	private SystemBeanService systemBeanService;
	private SystemSchemaService systemSchemaService;

	private InstructionExportHandler instructionExportHandler;
	private InstructionService<M> instructionService;

	private InstructionGeneratorLocator<M, E, InstructionDeliveryFieldCommand> instructionGeneratorLocator;


	////////////////////////////////////////////////////////////////////////////
	//////////                       Instruction                      //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstruction getInvestmentInstruction(int id) {
		return getInvestmentInstructionImpl(id, true);
	}


	private InvestmentInstruction getInvestmentInstructionImpl(int id, boolean populateItemLabel) {
		InvestmentInstruction instruction = getInvestmentInstructionDAO().findByPrimaryKey(id);
		if (instruction != null) {
			instruction.setItemList(getInvestmentInstructionItemListByInstruction(id));
			if (populateItemLabel) {
				// Does additional lookup on the actual entities in the instruction item to populate label
				// i.e. instead of just saying AccountingEventJournalDetailID: 12345 it will use the event label, i.e. Equity Leg Payment for Security x, etc.
				populateInvestmentInstructionItemFkFieldLabel(instruction.getItemList());
			}
		}
		return instruction;
	}


	@Override
	public List<InvestmentInstruction> getInvestmentInstructionList(InvestmentInstructionSearchForm searchForm) {
		return getInvestmentInstructionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional(readOnly = true) // avoid numerous separate transactions and enable first level caching to avoid extra DB look ups: same event journal for each detail, etc.
	public List<InvestmentInstruction> getInvestmentInstructionListPopulated(InvestmentInstructionSearchForm searchForm) {
		List<InvestmentInstruction> instructionList = getInvestmentInstructionList(searchForm);
		for (InvestmentInstruction instruction : CollectionUtils.getIterable(instructionList)) {
			List<InvestmentInstructionItem> itemList = getInvestmentInstructionItemListByInstruction(instruction.getId());
			instruction.setAllItemsReady(areAllItemsReadyWithOptionalPopulate(itemList, true));
		}

		return instructionList;
	}


	@Override
	public InvestmentInstruction saveInvestmentInstruction(InvestmentInstruction bean, boolean doNotPopulateFkFieldLabel) {
		bean = saveInvestmentInstructionImpl(bean);
		// Re-Populate FKField Labels
		if (!doNotPopulateFkFieldLabel) {
			populateInvestmentInstructionItemFkFieldLabel(bean.getItemList());
		}
		return bean;
	}


	@Transactional
	protected InvestmentInstruction saveInvestmentInstructionImpl(InvestmentInstruction bean) {
		List<InvestmentInstructionItem> newItems = bean.getItemList();

		if (!CollectionUtils.isEmpty(newItems)) {
			for (InvestmentInstructionItem item : CollectionUtils.getIterable(newItems)) {
				item.setInstruction(bean);
			}
		}

		List<InvestmentInstructionItem> oldItemsToBeReplaced = new ArrayList<>();
		if (!bean.isNewBean()) {
			List<InvestmentInstructionItem> oldItems = getInvestmentInstructionItemListByInstruction(bean.getId());

			for (InvestmentInstructionItem oldItem : oldItems) {
				//Skip the old item if it is in the new items list (want it to get updated)
				if (CollectionUtils.contains(newItems, oldItem)) {
					continue;
				}

				//If the item status is open, delete the item (open items should not have any runs associated with them)
				if (oldItem.getStatus().getInvestmentInstructionStatusName() == InvestmentInstructionStatusNames.OPEN) {
					oldItemsToBeReplaced.add(oldItem);
					continue;
				}

				InvestmentInstructionRunInstructionItemSearchForm runSearchForm = new InvestmentInstructionRunInstructionItemSearchForm();
				runSearchForm.setInstructionItemId(oldItem.getId());

				//Check if there are any runs associated with the old item
				if (!CollectionUtils.isEmpty(getInvestmentInstructionRunService().getInvestmentInstructionRunInstructionItemList(runSearchForm))) {
					oldItem.setStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.CANCELED));
					saveInvestmentInstructionItem(oldItem);
				}
				else {
					oldItemsToBeReplaced.add(oldItem);
				}
			}
		}

		InvestmentInstructionStatus instructionStatus = getInvestmentInstructionStatus(newItems);

		bean.setStatus(instructionStatus);
		bean = getInvestmentInstructionDAO().save(bean);
		getInvestmentInstructionItemDAO().saveList(newItems, oldItemsToBeReplaced);
		bean.setItemList(newItems);
		return bean;
	}


	@Override
	public InvestmentInstruction saveInvestmentInstructionComplete(int id, Date wireDate) {
		final InvestmentInstruction investmentInstruction = getInvestmentInstructionImpl(id, false);
		if (investmentInstruction != null) {
			if (investmentInstruction.getDefinition().isRequiresWireDate()) {
				ValidationUtils.assertNotNull(wireDate, () -> String.format("Wire Date required for processing instruction [%s]", id));
			}
			for (InvestmentInstructionItem item : CollectionUtils.getIterable(investmentInstruction.getItemList())) {
				item.setStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.COMPLETED));
				ObjectUtils.doIfPresent(wireDate, item::setWireDate);
			}
			// also saves the instruction's item list
			return saveInvestmentInstructionImpl(investmentInstruction);
		}
		return investmentInstruction;
	}


	@Override
	@Transactional
	public void saveInvestmentInstructionListComplete(Integer[] instructionIds, Date wireDate) {
		if (instructionIds != null && instructionIds.length > 0) {
			for (Integer instructionId : instructionIds) {
				saveInvestmentInstructionComplete(instructionId, wireDate);
			}
		}
	}


	@Override
	public void refreshInvestmentInstructionStatus(InvestmentInstruction bean, List<InvestmentInstructionItem> items) {
		if (items == null) {
			items = bean.getItemList();
			if (items == null) {
				items = getInvestmentInstructionItemListByInstruction(bean.getId());
			}
			else {
				items.addAll(getInvestmentInstructionItemListByInstruction(bean.getId()));
			}
		}
		InvestmentInstructionStatus instructionStatus = getInvestmentInstructionStatus(items);

		bean.setStatus(instructionStatus);
		getInvestmentInstructionDAO().save(bean);
	}


	@Override
	@Transactional
	public void deleteInvestmentInstruction(int id) {
		getInvestmentInstructionItemDAO().deleteList(getInvestmentInstructionItemListByInstruction(id));
		// Do Not Necessarily Need to Manually Delete The Instruction
		// The Last Item in the List, when deleted will trigger deleting the instruction.
		if (getInvestmentInstruction(id) != null) {
			getInvestmentInstructionDAO().delete(id);
		}
	}


	@Override
	@Transactional
	public void setInvestmentInstructionRegeneratingFlag(InvestmentInstructionCategory category, Date date) {
		InvestmentInstructionSearchForm searchForm = new InvestmentInstructionSearchForm();
		searchForm.setInstructionCategoryId(category.getId());
		searchForm.setInstructionDate(date);
		for (InvestmentInstruction investmentInstruction : CollectionUtils.getIterable(getInvestmentInstructionList(searchForm))) {
			investmentInstruction.setRegenerating(true);
			getInvestmentInstructionDAO().save(investmentInstruction);
		}
	}


	@Override
	@Transactional
	public void clearInvestmentInstructionRegeneratingFlag(InvestmentInstructionCategory category, Date date) {
		InvestmentInstructionSearchForm searchForm = new InvestmentInstructionSearchForm();
		searchForm.setInstructionCategoryId(category.getId());
		searchForm.setInstructionDate(date);
		for (InvestmentInstruction investmentInstruction : CollectionUtils.getIterable(getInvestmentInstructionList(searchForm))) {
			investmentInstruction.setRegenerating(false);
			getInvestmentInstructionDAO().save(investmentInstruction);
		}
	}

	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////


	private InvestmentInstructionStatus getInvestmentInstructionStatus(List<InvestmentInstructionItem> items) {
		InvestmentInstructionStatus instructionStatus = getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.CANCELED);

		if (!CollectionUtils.isEmpty(items)) {
			instructionStatus = items.get(0).getStatus();

			for (InvestmentInstructionItem item : CollectionUtils.getIterable(items)) {
				//set the instruction status to ERROR if any of its items have an ERROR status
				InvestmentInstructionStatus itemStatus = item.getStatus();

				if (itemStatus.isFail()) {
					return getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR);
				}
				else if (instructionStatus.compareStatusCode(itemStatus) > 0) {
					instructionStatus = item.getStatus();
				}
			}
		}

		return instructionStatus;
	}


	private boolean areAllItemsReadyWithOptionalPopulate(List<InvestmentInstructionItem> items, boolean returnReadyInsteadOfPopulate) {
		boolean allReady = true;
		if (!CollectionUtils.isEmpty(items)) {
			Map<String, ReadOnlyDAO<IdentityObject>> daoMap = new HashMap<>();
			Map<Integer, InvestmentInstructionSelector<IdentityObject>> selectorBeanMap = new HashMap<>();

			for (InvestmentInstructionItem item : CollectionUtils.getIterable(items)) {
				InvestmentInstructionDefinition instructionDefinition = item.getInstruction().getDefinition();

				ReadOnlyDAO<IdentityObject> dao = getDaoByTableName(instructionDefinition.getCategory().getTable().getName(), daoMap);
				IdentityObject entity = dao.findByPrimaryKey(item.getFkFieldId());

				if (entity != null) {
					InvestmentInstructionSelector<IdentityObject> selector = getInstructionSelectorBean(instructionDefinition.getSelectorBean(), selectorBeanMap);

					boolean ready = selector.isInstructionItemReady(item, entity);
					if (!ready) {
						allReady = false;
						if (returnReadyInsteadOfPopulate) {
							return allReady;
						}
					}
					item.setReady(ready);
					item.setFkFieldLabel(selector.getInstructionItemLabel(entity));
				}
			}
		}
		return allReady;
	}


	private ReadOnlyDAO<IdentityObject> getDaoByTableName(String tableName, Map<String, ReadOnlyDAO<IdentityObject>> daoMap) {
		ReadOnlyDAO<IdentityObject> dao = daoMap.get(tableName);
		if (dao == null) {
			dao = getDaoLocator().locate(tableName);
			ValidationUtils.assertNotNull(dao, "Cannot find DAO for table: " + tableName);
			daoMap.put(tableName, dao);
		}
		return dao;
	}


	@SuppressWarnings("unchecked")
	private InvestmentInstructionSelector<IdentityObject> getInstructionSelectorBean(SystemBean selectorBean, Map<Integer, InvestmentInstructionSelector<IdentityObject>> selectorBeanMap) {
		return selectorBeanMap.computeIfAbsent(selectorBean.getId(), k -> (InvestmentInstructionSelector<IdentityObject>) getSystemBeanService().getBeanInstance(selectorBean));
	}

	////////////////////////////////////////////////////////////////////////////
	///////////                   Instruction Item                    //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionItem getInvestmentInstructionItem(int id) {
		return getInvestmentInstructionItemDAO().findByPrimaryKey(id);
	}


	@Override
	public <T extends IdentityObject> List<InvestmentInstructionItem> getInvestmentInstructionItemListForEntityObject(T dtoObject) {
		ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(dtoObject);
		return getInvestmentInstructionItemListForEntityList(dao.getConfiguration().getTable().getName(), new Integer[]{(int) dtoObject.getIdentity()}, false);
	}


	@Override
	public <T extends IdentityObject> List<InvestmentInstructionItem> getInvestmentInstructionItemListForEntityObjectAndType(T dtoObject, InvestmentInstructionRunTypes runType) {
		ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(dtoObject);
		return getInvestmentInstructionItemListForEntityList(dao.getConfiguration().getTable().getName(), new Integer[]{(int) dtoObject.getIdentity()}, false, runType);
	}


	@Override
	public List<InvestmentInstructionItem> getInvestmentInstructionItemListForEntity(String tableName, int fkFieldId) {
		return getInvestmentInstructionItemListForEntityList(tableName, new Integer[]{fkFieldId}, false);
	}


	@Override
	public List<InvestmentInstructionItem> getInvestmentInstructionItemListForEntityList(String tableName, Integer[] fkFieldIds, boolean populateFkFieldLabel) {
		return getInvestmentInstructionItemListForEntityList(tableName, fkFieldIds, populateFkFieldLabel, null);
	}


	@Override
	public List<InvestmentInstructionItem> getInvestmentInstructionItemList(InvestmentInstructionItemSearchForm searchForm) {
		return getInvestmentInstructionItemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentInstructionItem> getInvestmentInstructionItemListPopulated(InvestmentInstructionItemSearchForm searchForm) {
		List<InvestmentInstructionItem> items = getInvestmentInstructionItemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		populateInvestmentInstructionItemFkFieldLabel(items);
		return items;
	}


	@Override
	public InvestmentInstructionItem getInvestmentInstructionItemPopulated(int id) {
		InvestmentInstructionItem result = getInvestmentInstructionItem(id);
		if (result != null) {
			List<InvestmentInstructionItem> items = new ArrayList<>();
			items.add(result);
			populateInvestmentInstructionItemFkFieldLabel(items);
			result = items.get(0);
		}
		return result;
	}


	@Override
	public InstructionPreviewMessage getInvestmentInstructionPreview(int fkFieldId, String tableName, Integer definitionId) {
		AssertUtils.assertNotNull(getDaoLocator(), "daoLocator is missing: cannot locate dao for table " + tableName);
		String errorMessage = String.format("Could not get generate instruction messages for [%s].[%s]", tableName, fkFieldId);
		InstructionPreviewMessage results = new InstructionPreviewMessage(errorMessage, errorMessage);

		SystemTable systemTable = getSystemSchemaService().getSystemTableByName(tableName);
		PreviewOnlyInstructionItem previewOnlyInstructionItem = new PreviewOnlyInstructionItem(systemTable, fkFieldId);
		if (Objects.nonNull(definitionId)) {
			InvestmentInstructionDefinition definition = getInvestmentInstructionSetupService().getInvestmentInstructionDefinition(definitionId);
			InvestmentInstruction instruction = new InvestmentInstruction();
			instruction.setDefinition(definition);
			instruction.setInstructionDate(new Date());
			previewOnlyInstructionItem.setInstruction(instruction);
		}
		List<M> messageList = getInstructionService().getInstructionMessage(previewOnlyInstructionItem, false);
		if (!CollectionUtils.isEmpty(messageList)) {
			String message = getInstructionExportHandler().previewExportMessage(messageList);
			String formattedMessage = getInstructionExportHandler().previewFormattedExportMessage(messageList);
			results = new InstructionPreviewMessage(message, formattedMessage);
		}
		return results;
	}


	@Override
	public void populateInvestmentInstructionItemFkFieldLabel(List<InvestmentInstructionItem> itemList) {
		areAllItemsReadyWithOptionalPopulate(itemList, false);
	}


	@Override
	public InvestmentInstructionItem saveInvestmentInstructionItem(InvestmentInstructionItem item) {
		return getInvestmentInstructionItemDAO().save(item);
	}


	@Override
	@Transactional
	public void saveInvestmentInstructionItemComplete(InvestmentInstructionItem item, InvestmentInstruction instruction) {
		item.setStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.COMPLETED));
		saveInvestmentInstructionItem(item);
		//save the instruction to update its status based on the instruction item change
		//get the investment instruction by ID because the instruction object on the item is not fully populated
		saveInvestmentInstructionImpl(instruction);
	}


	@Override
	@Transactional
	public void saveInvestmentInstructionItemListComplete(Integer[] itemIds, Date wireDate) {
		if (itemIds != null && itemIds.length > 0) {
			InvestmentInstructionItem instructionItem = getInvestmentInstructionItem(itemIds[0]);
			if (instructionItem != null) {
				//hold onto the instruction so we don't have to lookup every time
				final int instructionId = instructionItem.getInstruction().getId();
				InvestmentInstruction instructionHolder = getInvestmentInstruction(instructionId);
				if (instructionHolder.getDefinition().isRequiresWireDate()) {
					ValidationUtils.assertNotNull(wireDate, () -> String.format("Wire Date required for processing instruction [%s].", instructionId));
				}
				if (wireDate != null) {
					instructionItem.setWireDate(wireDate);
				}
				saveInvestmentInstructionItemComplete(instructionItem, instructionHolder);

				for (int i = 1; i < itemIds.length; i++) {
					int itemId = itemIds[i];
					instructionItem = getInvestmentInstructionItem(itemId);
					if (instructionItem != null) {
						if (!Objects.equals(instructionHolder.getId(), instructionItem.getInstruction().getId())) {
							instructionHolder = getInvestmentInstruction(getInvestmentInstructionItem(itemId).getInstruction().getId());
						}
						if (wireDate != null) {
							instructionItem.setWireDate(wireDate);
						}
						saveInvestmentInstructionItemComplete(instructionItem, instructionHolder);
					}
				}
			}
		}
	}


	@Override
	@Transactional
	public void saveInvestmentInstructionItemError(InvestmentInstructionItem item, InvestmentInstruction instruction) {
		item.setStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR));
		saveInvestmentInstructionItem(item);
		//save the instruction to update its status based on the instruction item change
		//get the investment instruction by ID because the instruction object on the item is not fully populated
		saveInvestmentInstructionImpl(instruction);
	}


	@Override
	@Transactional
	public int saveInvestmentInstructionItemListError(Integer[] itemIds) {
		int updateCount = 0;
		if (itemIds != null && itemIds.length > 0) {
			InvestmentInstructionItem instructionItem = getInvestmentInstructionItem(itemIds[0]);
			if (instructionItem != null) {
				//hold onto the instruction so we don't have to lookup every time
				InvestmentInstruction instructionHolder = getInvestmentInstruction(instructionItem.getInstruction().getId());
				saveInvestmentInstructionItemError(instructionItem, instructionHolder);
				updateCount++;

				for (int i = 1; i < itemIds.length; i++) {
					int itemId = itemIds[i];
					instructionItem = getInvestmentInstructionItem(itemId);
					if (instructionItem != null) {
						if (!Objects.equals(instructionHolder.getId(), instructionItem.getInstruction().getId())) {
							instructionHolder = getInvestmentInstruction(getInvestmentInstructionItem(itemId).getInstruction().getId());
						}
						saveInvestmentInstructionItemError(instructionItem, instructionHolder);
						updateCount++;
					}
				}
			}
		}
		return updateCount;
	}


	@Override
	public int saveInvestmentInstructionRunListError(Integer[] runIds) {
		int updateCount = 0;
		if (runIds != null && runIds.length > 0) {
			final InvestmentInstructionStatus errorStatus = getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR);
			for (Integer runId : runIds) {
				InvestmentInstructionRun instructionRun = getInvestmentInstructionRunService().getInvestmentInstructionRun(runId);
				if (instructionRun != null) {
					instructionRun.setRunStatus(errorStatus);
					getInvestmentInstructionRunService().saveInvestmentInstructionRun(instructionRun);
					updateCount++;
				}
			}
		}
		return updateCount;
	}


	@Override
	@Transactional
	public void deleteInvestmentInstructionItem(int id) {
		boolean deleteInstruction = false;
		InvestmentInstructionItem itemToDelete = getInvestmentInstructionItem(id);
		if (itemToDelete != null && itemToDelete.getInstruction() != null) {
			List<InvestmentInstructionItem> list = getInvestmentInstructionItemDAO().findByField("instruction.id", itemToDelete.getInstruction().getId());

			if (CollectionUtils.isEmpty(list)) {
				deleteInstruction = true;
			}
			else if (list.size() == 1) {
				InvestmentInstructionItem item = list.get(0);
				if (item.equals(itemToDelete)) {
					deleteInstruction = true;
				}
			}
		}
		getInvestmentInstructionItemDAO().delete(itemToDelete);
		if (deleteInstruction) {
			deleteInvestmentInstruction(itemToDelete.getInstruction().getId());
		}
	}


	@Override
	@Transactional
	public void cancelInvestmentInstructionItemList(Integer[] itemIds) {
		Set<InvestmentInstruction> investmentInstructionList = new HashSet<>();
		for (int itemId : CollectionUtils.createList(itemIds)) {
			InvestmentInstructionItem cancelledItem = getInvestmentInstructionItem(itemId);
			validateForCancellation(itemId);

			InvestmentInstructionItem newItem = new InvestmentInstructionItem();
			newItem.setStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.OPEN));
			newItem.setInstruction(cancelledItem.getInstruction());
			newItem.setItemGroup(cancelledItem.getItemGroup());
			newItem.setFkFieldId(cancelledItem.getFkFieldId());
			newItem.setReferencedInvestmentInstructionItem(cancelledItem);
			saveInvestmentInstructionItem(newItem);

			cancelledItem.setStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.PENDING_CANCEL));
			saveInvestmentInstructionItem(cancelledItem);
			investmentInstructionList.add(cancelledItem.getInstruction());
		}
		ValidationUtils.assertFalse(investmentInstructionList.size() > 1,
				"All investment Instruction Items should belong to the same Instruction " + investmentInstructionList);
		investmentInstructionList.forEach(instruction -> {
			instruction.setStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.PENDING_CANCEL));
			getInvestmentInstructionDAO().save(instruction);
		});
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<InvestmentInstructionItem> getInvestmentInstructionItemListForEntityList(String tableName, Integer[] fkFieldIds, boolean populateFkFieldLabel, InvestmentInstructionRunTypes runType) {
		ValidationUtils.assertNotEmpty(tableName, "Table Name is Required");
		ValidationUtils.assertNotNull(fkFieldIds, "FK Field IDs is required");
		ValidationUtils.assertFalse(fkFieldIds.length == 0, "At least one FK Field ID is required.");
		SystemTable table = getSystemSchemaService().getSystemTableByName(tableName);
		InvestmentInstructionItemSearchForm searchForm = new InvestmentInstructionItemSearchForm();
		searchForm.setTableId(table.getId());
		if (fkFieldIds.length == 1) {
			searchForm.setFkFieldId(fkFieldIds[0]);
		}
		else {
			searchForm.setFkFieldIdList(fkFieldIds);
		}
		searchForm.setRunType(runType);

		List<InvestmentInstructionItem> itemList = getInvestmentInstructionItemList(searchForm);
		if (!CollectionUtils.isEmpty(itemList) && populateFkFieldLabel) {
			populateInvestmentInstructionItemFkFieldLabel(itemList);
		}
		return itemList;
	}


	private List<InvestmentInstructionItem> getInvestmentInstructionItemListByInstruction(int id) {
		return getInvestmentInstructionItemDAO().findByField("instruction.id", id);
	}


	@SuppressWarnings("unchecked")
	private void validateForCancellation(int itemId) {
		InvestmentInstructionItem cancelledItem = getInvestmentInstructionItem(itemId);
		ValidationUtils.assertNotNull(cancelledItem, () -> String.format("Investment Instruction Item [%s] cannot be found.", itemId));

		InvestmentInstructionStatus itemStatus = cancelledItem.getStatus();
		ValidationUtils.assertTrue(itemStatus.isCompleted() && !itemStatus.isFail(), () ->
				String.format("Investment Instruction Item [%s] cannot be cancelled with status [%s].", cancelledItem.getId(), itemStatus.getLabel()));

		InvestmentInstruction investmentInstruction = cancelledItem.getInstruction();
		InvestmentInstructionDefinition investmentInstructionDefinition = investmentInstruction.getDefinition();

		InvestmentInstructionContactSearchForm searchForm = new InvestmentInstructionContactSearchForm();
		searchForm.setActive(true);
		ObjectUtils.doIfPresent(investmentInstructionDefinition.getContactGroup(), g -> searchForm.setContactGroupId(g.getId()));
		searchForm.setCompanyId(investmentInstruction.getRecipientCompany().getId());
		searchForm.setDestinationSystemBeanTypeName(InvestmentInstructionRunTypes.SWIFT.getDestinationGeneratorBeanType());
		List<InvestmentInstructionContact> investmentInstructionContactList = this.getInvestmentInstructionSetupService().getInvestmentInstructionContactList(searchForm);
		ValidationUtils.assertFalse(investmentInstructionContactList.isEmpty(), () -> String.format("Investment Definition [%s] Contact list does not include a SWIFT contact for company [%s].",
				investmentInstructionDefinition.getName(), investmentInstruction.getRecipientCompany().getLabel()));

		ValidationUtils.assertNotNull(cancelledItem.getSystemTable(), () -> String.format("Instruction Item [%s] does not have a System table.", itemId));
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(cancelledItem.getSystemTable().getName());
		ValidationUtils.assertNotNull(dao, () -> String.format("Cannot find DAO for [%s].", cancelledItem.getSystemTable().getName()));
		IdentityObject identityObject = dao.findByPrimaryKey(cancelledItem.getFkFieldId());
		ValidationUtils.assertNotNull(identityObject, () -> String.format("Cannot find [%s] entity with ID [%s].", cancelledItem.getSystemTable().getName(), cancelledItem.getFkFieldId()));

		InstructionGenerator<M, E, InstructionDeliveryFieldCommand> generator = null;
		try {
			generator = getInstructionGeneratorLocator().locate((Class<E>) identityObject.getClass());
		}
		catch (Exception e) {
			// swallow exception
		}
		ValidationUtils.assertNotNull(generator, () -> String.format("Message generator not found for [%s] with ID [%s]", identityObject.getClass().getName(), identityObject.getIdentity()));
		if (generator != null) {
			final String generatorName = generator.getClass().getName();
			ValidationUtils.assertTrue(generator.isCancellationSupported(), () -> String.format("Message generator [%s] does not support cancellations.", generatorName));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstruction, Criteria> getInvestmentInstructionDAO() {
		return this.investmentInstructionDAO;
	}


	public void setInvestmentInstructionDAO(AdvancedUpdatableDAO<InvestmentInstruction, Criteria> investmentInstructionDAO) {
		this.investmentInstructionDAO = investmentInstructionDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInstructionItem, Criteria> getInvestmentInstructionItemDAO() {
		return this.investmentInstructionItemDAO;
	}


	public void setInvestmentInstructionItemDAO(AdvancedUpdatableDAO<InvestmentInstructionItem, Criteria> investmentInstructionItemDAO) {
		this.investmentInstructionItemDAO = investmentInstructionItemDAO;
	}


	public InvestmentInstructionSetupService getInvestmentInstructionSetupService() {
		return this.investmentInstructionSetupService;
	}


	public void setInvestmentInstructionSetupService(InvestmentInstructionSetupService investmentInstructionSetupService) {
		this.investmentInstructionSetupService = investmentInstructionSetupService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public InvestmentInstructionStatusService getInvestmentInstructionStatusService() {
		return this.investmentInstructionStatusService;
	}


	public void setInvestmentInstructionStatusService(InvestmentInstructionStatusService investmentInstructionStatusService) {
		this.investmentInstructionStatusService = investmentInstructionStatusService;
	}


	public InvestmentInstructionRunService getInvestmentInstructionRunService() {
		return this.investmentInstructionRunService;
	}


	public void setInvestmentInstructionRunService(InvestmentInstructionRunService investmentInstructionRunService) {
		this.investmentInstructionRunService = investmentInstructionRunService;
	}


	public InstructionExportHandler getInstructionExportHandler() {
		return this.instructionExportHandler;
	}


	public void setInstructionExportHandler(InstructionExportHandler instructionExportHandler) {
		this.instructionExportHandler = instructionExportHandler;
	}


	public InstructionService<M> getInstructionService() {
		return this.instructionService;
	}


	public void setInstructionService(InstructionService<M> instructionService) {
		this.instructionService = instructionService;
	}


	public InstructionGeneratorLocator<M, E, InstructionDeliveryFieldCommand> getInstructionGeneratorLocator() {
		return this.instructionGeneratorLocator;
	}


	public void setInstructionGeneratorLocator(InstructionGeneratorLocator<M, E, InstructionDeliveryFieldCommand> instructionGeneratorLocator) {
		this.instructionGeneratorLocator = instructionGeneratorLocator;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private class PreviewOnlyInstructionItem extends InvestmentInstructionItem {

		private SystemTable systemTable;
		private Integer fkFieldId;


		PreviewOnlyInstructionItem(SystemTable systemTable, Integer fkFieldId) {
			this.systemTable = systemTable;
			this.fkFieldId = fkFieldId;
			this.setId(1);
		}


		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			if (!super.equals(o)) {
				return false;
			}
			PreviewOnlyInstructionItem that = (PreviewOnlyInstructionItem) o;
			return Objects.equals(this.systemTable, that.systemTable) &&
					Objects.equals(this.fkFieldId, that.fkFieldId);
		}


		@Override
		public int hashCode() {

			return Objects.hash(super.hashCode(), this.systemTable, this.fkFieldId);
		}


		@Override
		public SystemTable getSystemTable() {
			return this.systemTable;
		}


		@Override
		public Integer getFkFieldId() {
			return this.fkFieldId;
		}
	}
}
