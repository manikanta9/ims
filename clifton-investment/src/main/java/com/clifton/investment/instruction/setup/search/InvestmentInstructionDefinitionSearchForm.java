package com.clifton.investment.instruction.setup.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.SystemHierarchyItemSearchForm;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;


/**
 * The <code>InvestmentInstructionDefinitionSearchForm</code> ...
 * <p>
 * NOTE: BECAUSE OF THE TWO SEARCH FORM IMPLEMENTATIONS FOR WORKFLOW AND HIERARCHY ITEMS, THE CONFIGURER IS USED ONLY FOR HIERARCHY AS THE WORKFLOW AWARE
 * SEARCH FORM CONFIGURER WILL JUST CONVERT NAME PROPERTIES TO IDS IT'S NOT CRITICAL TO USE
 *
 * @author manderson
 */
public class InvestmentInstructionDefinitionSearchForm extends BaseWorkflowAwareSearchForm implements SystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "category.id")
	private Short instructionCategoryId;

	@SearchField(searchField = "name", searchFieldPath = "category")
	private String instructionCategoryName;

	@SearchField(searchField = "name", searchFieldPath = "report")
	private String reportName;

	@SearchField(searchField = "name", searchFieldPath = "itemGroupPopulatorBean")
	private String itemGroupPopulatorBeanName;

	@SearchField(searchFieldPath = "category.table", searchField = "name")
	private String tableName;

	@SearchField(searchFieldPath = "category.table", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] tableNames;

	@SearchField(searchFieldPath = "category", searchField = "table.id")
	private Short tableId;

	@SearchField(searchFieldPath = "category.selectorBeanType", searchField = "name")
	private String selectorBeanTypeName;

	@SearchField(searchFieldPath = "category", searchField = "required")
	private Boolean required;

	@SearchField(searchFieldPath = "category", searchField = "cash")
	private Boolean cash;

	@SearchField(searchFieldPath = "category", searchField = "client")
	private Boolean client;

	@SearchField(searchField = "contactGroup.id")
	private Integer contactGroupId;

	@SearchField(searchField = "contactGroup.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] contactGroupIds;

	@SearchField(searchField = "deliveryType.id")
	private Short deliveryTypeId;

	@SearchField
	private String name;

	/*
	 * Field members for com.clifton.system.hierarchy.assignment.search.SystemHierarchyItemSearchForm implementation. See
	 * com.clifton.system.hierarchy.assignment.search.BaseSystemHierarchyItemSearchForm for field details.
	 */

	private String categoryName;
	private String categoryTableName;
	private Short categoryHierarchyId;
	private Short[] categoryHierarchyIds;
	private String categoryHierarchyName;
	private String[] categoryHierarchyNames;
	private String categoryLinkFieldPath;
	private Boolean categoryIncludeAllIfNull;

	private String category2Name;
	private String category2TableName;
	private Short category2HierarchyId;
	private Short[] category2HierarchyIds;
	private String category2HierarchyName;
	private String[] category2HierarchyNames;
	private String category2LinkFieldPath;
	private Boolean category2IncludeAllIfNull;

	private String category3Name;
	private String category3TableName;
	private Short category3HierarchyId;
	private Short[] category3HierarchyIds;
	private String category3HierarchyName;
	private String[] category3HierarchyNames;
	private String category3LinkFieldPath;
	private Boolean category3IncludeAllIfNull;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "InvestmentInstructionDefinition";
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getInstructionCategoryId() {
		return this.instructionCategoryId;
	}


	public void setInstructionCategoryId(Short instructionCategoryId) {
		this.instructionCategoryId = instructionCategoryId;
	}


	public String getInstructionCategoryName() {
		return this.instructionCategoryName;
	}


	public void setInstructionCategoryName(String instructionCategoryName) {
		this.instructionCategoryName = instructionCategoryName;
	}


	public String getReportName() {
		return this.reportName;
	}


	public void setReportName(String reportName) {
		this.reportName = reportName;
	}


	public String getItemGroupPopulatorBeanName() {
		return this.itemGroupPopulatorBeanName;
	}


	public void setItemGroupPopulatorBeanName(String itemGroupPopulatorBeanName) {
		this.itemGroupPopulatorBeanName = itemGroupPopulatorBeanName;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String[] getTableNames() {
		return this.tableNames;
	}


	public void setTableNames(String[] tableNames) {
		this.tableNames = tableNames;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public String getSelectorBeanTypeName() {
		return this.selectorBeanTypeName;
	}


	public void setSelectorBeanTypeName(String selectorBeanTypeName) {
		this.selectorBeanTypeName = selectorBeanTypeName;
	}


	public Boolean getRequired() {
		return this.required;
	}


	public void setRequired(Boolean required) {
		this.required = required;
	}


	public Boolean getCash() {
		return this.cash;
	}


	public void setCash(Boolean cash) {
		this.cash = cash;
	}


	public Boolean getClient() {
		return this.client;
	}


	public void setClient(Boolean client) {
		this.client = client;
	}


	public Integer getContactGroupId() {
		return this.contactGroupId;
	}


	public void setContactGroupId(Integer contactGroupId) {
		this.contactGroupId = contactGroupId;
	}


	public Short getDeliveryTypeId() {
		return this.deliveryTypeId;
	}


	public void setDeliveryTypeId(Short deliveryTypeId) {
		this.deliveryTypeId = deliveryTypeId;
	}


	public Integer[] getContactGroupIds() {
		return this.contactGroupIds;
	}


	public void setContactGroupIds(Integer[] contactGroupIds) {
		this.contactGroupIds = contactGroupIds;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String getCategoryName() {
		return this.categoryName;
	}


	@Override
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	@Override
	public String getCategoryTableName() {
		return this.categoryTableName;
	}


	@Override
	public void setCategoryTableName(String categoryTableName) {
		this.categoryTableName = categoryTableName;
	}


	@Override
	public Short getCategoryHierarchyId() {
		return this.categoryHierarchyId;
	}


	@Override
	public void setCategoryHierarchyId(Short categoryHierarchyId) {
		this.categoryHierarchyId = categoryHierarchyId;
	}


	@Override
	public Short[] getCategoryHierarchyIds() {
		return this.categoryHierarchyIds;
	}


	@Override
	public void setCategoryHierarchyIds(Short[] categoryHierarchyIds) {
		this.categoryHierarchyIds = categoryHierarchyIds;
	}


	@Override
	public String getCategoryHierarchyName() {
		return this.categoryHierarchyName;
	}


	@Override
	public void setCategoryHierarchyName(String categoryHierarchyName) {
		this.categoryHierarchyName = categoryHierarchyName;
	}


	@Override
	public String[] getCategoryHierarchyNames() {
		return this.categoryHierarchyNames;
	}


	@Override
	public void setCategoryHierarchyNames(String[] categoryHierarchyNames) {
		this.categoryHierarchyNames = categoryHierarchyNames;
	}


	@Override
	public String getCategoryLinkFieldPath() {
		return this.categoryLinkFieldPath;
	}


	@Override
	public void setCategoryLinkFieldPath(String categoryLinkFieldPath) {
		this.categoryLinkFieldPath = categoryLinkFieldPath;
	}


	@Override
	public Boolean getCategoryIncludeAllIfNull() {
		return this.categoryIncludeAllIfNull;
	}


	@Override
	public void setCategoryIncludeAllIfNull(Boolean categoryIncludeAllIfNull) {
		this.categoryIncludeAllIfNull = categoryIncludeAllIfNull;
	}


	@Override
	public String getCategory2Name() {
		return this.category2Name;
	}


	@Override
	public void setCategory2Name(String category2Name) {
		this.category2Name = category2Name;
	}


	@Override
	public String getCategory2TableName() {
		return this.category2TableName;
	}


	@Override
	public void setCategory2TableName(String category2TableName) {
		this.category2TableName = category2TableName;
	}


	@Override
	public Short getCategory2HierarchyId() {
		return this.category2HierarchyId;
	}


	@Override
	public void setCategory2HierarchyId(Short category2HierarchyId) {
		this.category2HierarchyId = category2HierarchyId;
	}


	@Override
	public Short[] getCategory2HierarchyIds() {
		return this.category2HierarchyIds;
	}


	@Override
	public void setCategory2HierarchyIds(Short[] category2HierarchyIds) {
		this.category2HierarchyIds = category2HierarchyIds;
	}


	@Override
	public String getCategory2HierarchyName() {
		return this.category2HierarchyName;
	}


	@Override
	public void setCategory2HierarchyName(String category2HierarchyName) {
		this.category2HierarchyName = category2HierarchyName;
	}


	@Override
	public String[] getCategory2HierarchyNames() {
		return this.category2HierarchyNames;
	}


	@Override
	public void setCategory2HierarchyNames(String[] category2HierarchyNames) {
		this.category2HierarchyNames = category2HierarchyNames;
	}


	@Override
	public String getCategory2LinkFieldPath() {
		return this.category2LinkFieldPath;
	}


	@Override
	public void setCategory2LinkFieldPath(String category2LinkFieldPath) {
		this.category2LinkFieldPath = category2LinkFieldPath;
	}


	@Override
	public Boolean getCategory2IncludeAllIfNull() {
		return this.category2IncludeAllIfNull;
	}


	@Override
	public void setCategory2IncludeAllIfNull(Boolean category2IncludeAllIfNull) {
		this.category2IncludeAllIfNull = category2IncludeAllIfNull;
	}


	@Override
	public String getCategory3Name() {
		return this.category3Name;
	}


	@Override
	public void setCategory3Name(String category3Name) {
		this.category3Name = category3Name;
	}


	@Override
	public String getCategory3TableName() {
		return this.category3TableName;
	}


	@Override
	public void setCategory3TableName(String category3TableName) {
		this.category3TableName = category3TableName;
	}


	@Override
	public Short getCategory3HierarchyId() {
		return this.category3HierarchyId;
	}


	@Override
	public void setCategory3HierarchyId(Short category3HierarchyId) {
		this.category3HierarchyId = category3HierarchyId;
	}


	@Override
	public Short[] getCategory3HierarchyIds() {
		return this.category3HierarchyIds;
	}


	@Override
	public void setCategory3HierarchyIds(Short[] category3HierarchyIds) {
		this.category3HierarchyIds = category3HierarchyIds;
	}


	@Override
	public String getCategory3HierarchyName() {
		return this.category3HierarchyName;
	}


	@Override
	public void setCategory3HierarchyName(String category3HierarchyName) {
		this.category3HierarchyName = category3HierarchyName;
	}


	@Override
	public String[] getCategory3HierarchyNames() {
		return this.category3HierarchyNames;
	}


	@Override
	public void setCategory3HierarchyNames(String[] category3HierarchyNames) {
		this.category3HierarchyNames = category3HierarchyNames;
	}


	@Override
	public String getCategory3LinkFieldPath() {
		return this.category3LinkFieldPath;
	}


	@Override
	public void setCategory3LinkFieldPath(String category3LinkFieldPath) {
		this.category3LinkFieldPath = category3LinkFieldPath;
	}


	@Override
	public Boolean getCategory3IncludeAllIfNull() {
		return this.category3IncludeAllIfNull;
	}


	@Override
	public void setCategory3IncludeAllIfNull(Boolean category3IncludeAllIfNull) {
		this.category3IncludeAllIfNull = category3IncludeAllIfNull;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
