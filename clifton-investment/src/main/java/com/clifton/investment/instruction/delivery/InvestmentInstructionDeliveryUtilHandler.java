package com.clifton.investment.instruction.delivery;

import com.clifton.business.company.BusinessCompany;
import com.clifton.instruction.Instruction;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.List;


public interface InvestmentInstructionDeliveryUtilHandler {

	/**
	 * Get the Delivery Type from the {@link com.clifton.instruction.Instruction}
	 */
	public InvestmentInstructionDeliveryType getDeliveryType(Instruction instruction);


	/**
	 * Get the 'Executing Broker' Delivery type.
	 */
	public InvestmentInstructionDeliveryType getExecutingBrokerDeliveryType();


	/**
	 * Get the list of Delivery Instructions using the delivery type specified on the instruction's definition
	 */
	public InvestmentInstructionDelivery getInvestmentInstructionDelivery(Instruction instruction, BusinessCompany businessCompany, InvestmentSecurity deliveryCurrency);


	/**
	 * Get the list of Delivery Instructions for a specific delivery type.
	 */
	public InvestmentInstructionDelivery getInvestmentInstructionDelivery(InvestmentInstructionDeliveryType type, BusinessCompany businessCompany, InvestmentSecurity deliveryCurrency);


	/**
	 * Get additional party BICs from the Delivery Instruction Fields using the provided instruction, company and currency.  The parameters are required, otherwise, an empty
	 * list is returned.  No exceptions are thrown, the caller must perform assertions based on the expected results.
	 */
	public List<String> getAdditionalPartyDeliveryFieldBicList(InstructionDeliveryFieldCommand deliveryFieldCommand);


	/**
	 * Get Delivery fields with type 'Additional Party' from the provided delivery.  The parameters are required, otherwise, an empty list is returned.  No exceptions are thrown,
	 * the caller must perform assertions based on the expected results.
	 */
	public List<InvestmentInstructionDeliveryField> getAdditionalPartyDeliveryFieldList(InvestmentInstructionDelivery deliveryInstruction, InvestmentInstructionDeliveryType deliveryType, InstructionDeliveryFieldCommand deliveryFieldCommand);
}
