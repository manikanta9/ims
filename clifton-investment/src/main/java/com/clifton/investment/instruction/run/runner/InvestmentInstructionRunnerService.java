package com.clifton.investment.instruction.run.runner;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.investment.instruction.InvestmentInstructionItem;

import java.util.List;


/**
 * @author theodorez
 */
public interface InvestmentInstructionRunnerService {

	@DoNotAddRequestMapping
	public void sendItems(List<InvestmentInstructionItem> items);
}
