package com.clifton.investment.instruction.delivery;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryBusinessCompanySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class InvestmentInstructionDeliveryBusinessCompanyServiceImpl implements InvestmentInstructionDeliveryBusinessCompanyService {

	private AdvancedUpdatableDAO<InvestmentInstructionDeliveryBusinessCompany, Criteria> investmentInstructionDeliveryBusinessCompanyDAO;

	private InvestmentInstructionDeliveryService investmentInstructionDeliveryService;

	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionDeliveryBusinessCompany getInvestmentInstructionDeliveryBusinessCompany(int id) {
		return getInvestmentInstructionDeliveryBusinessCompanyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInstructionDeliveryBusinessCompany> getInvestmentInstructionDeliveryBusinessCompanyList(InvestmentInstructionDeliveryBusinessCompanySearchForm searchForm) {
		return getInvestmentInstructionDeliveryBusinessCompanyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentInstructionDeliveryBusinessCompany saveInvestmentInstructionDeliveryBusinessCompany(InvestmentInstructionDeliveryBusinessCompany investmentInstructionDeliveryBusinessCompany) {
		AssertUtils.assertNotNull(investmentInstructionDeliveryBusinessCompany, "Investment Instruction Delivery Business Company entity is required.");

		ValidationUtils.assertNotNull(investmentInstructionDeliveryBusinessCompany.getReferenceOne(), "Investment Instruction Delivery entity is required.");
		ValidationUtils.assertNotNull(investmentInstructionDeliveryBusinessCompany.getReferenceTwo(), "Business Company is required.");
		ValidationUtils.assertNotNull(investmentInstructionDeliveryBusinessCompany.getDeliveryCurrency(), "Currency is required.");
		ValidationUtils.assertNotNull(investmentInstructionDeliveryBusinessCompany.getInvestmentInstructionDeliveryType(), "Delivery Type is required.");

		return getInvestmentInstructionDeliveryBusinessCompanyDAO().save(investmentInstructionDeliveryBusinessCompany);
	}


	@Override
	@Transactional
	public void deleteInvestmentInstructionDeliveryBusinessCompany(int id) {
		InvestmentInstructionDeliveryBusinessCompany linkage = getInvestmentInstructionDeliveryBusinessCompanyDAO().findByPrimaryKey(id);
		AssertUtils.assertNotNull(linkage, "Cannot find bean to delete with primary key: " + id);
		InvestmentInstructionDelivery investmentInstructionDelivery = linkage.getReferenceOne();
		AssertUtils.assertNotNull(investmentInstructionDelivery, "Investment Instruction Delivery cannot be found");
		getInvestmentInstructionDeliveryBusinessCompanyDAO().delete(id);

		InvestmentInstructionDeliveryBusinessCompanySearchForm searchForm = new InvestmentInstructionDeliveryBusinessCompanySearchForm();
		searchForm.setInvestmentInstructionDelivery(investmentInstructionDelivery.getId());
		List<InvestmentInstructionDeliveryBusinessCompany> deliveries = getInvestmentInstructionDeliveryBusinessCompanyList(searchForm);
		if (deliveries.isEmpty()) {
			getInvestmentInstructionDeliveryService().deleteInvestmentInstructionDelivery(investmentInstructionDelivery.getId());
		}
	}


	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstructionDeliveryBusinessCompany, Criteria> getInvestmentInstructionDeliveryBusinessCompanyDAO() {
		return this.investmentInstructionDeliveryBusinessCompanyDAO;
	}


	public void setInvestmentInstructionDeliveryBusinessCompanyDAO(AdvancedUpdatableDAO<InvestmentInstructionDeliveryBusinessCompany, Criteria> investmentInstructionDeliveryBusinessCompanyDAO) {
		this.investmentInstructionDeliveryBusinessCompanyDAO = investmentInstructionDeliveryBusinessCompanyDAO;
	}


	public InvestmentInstructionDeliveryService getInvestmentInstructionDeliveryService() {
		return this.investmentInstructionDeliveryService;
	}


	public void setInvestmentInstructionDeliveryService(InvestmentInstructionDeliveryService investmentInstructionDeliveryService) {
		this.investmentInstructionDeliveryService = investmentInstructionDeliveryService;
	}
}
