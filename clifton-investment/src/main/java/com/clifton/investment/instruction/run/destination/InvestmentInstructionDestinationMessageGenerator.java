package com.clifton.investment.instruction.run.destination;

import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;

import java.util.Map;


/**
 * Generates the destination for instructions to be sent via messaging (SWIFT messaging for example)
 */
public class InvestmentInstructionDestinationMessageGenerator implements InvestmentInstructionDestinationGenerator {


	@Override
	public Map<ExportMapKeys, String> generateDestination(InvestmentInstructionRun run) {
		throw new RuntimeException("Message generator is a place holder that does not implement actual generation of instruction destinations.");
	}


	@Override
	public Class<?> getParentClass() {
		return null;
	}


	@Override
	public String getDestinationName() {
		return "SWIFT";
	}


	@Override
	public String getDestinationLabel() {
		return "SWIFT messaging destination.";
	}
}
