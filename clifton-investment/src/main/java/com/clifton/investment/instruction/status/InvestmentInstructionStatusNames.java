package com.clifton.investment.instruction.status;

import com.clifton.investment.instruction.InvestmentInstructionItem;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Statuses used by Investment Instructions, Items, and Runs.
 *
 * @author theodorez
 */
public enum InvestmentInstructionStatusNames {
	ERROR,
	OPEN,
	SEND,
	SENDING,
	SENT,
	COMPLETED,
	CANCELED,
	PENDING_CANCEL,
	SENT_CANCEL,
	ERROR_CANCEL;

	static final Map<InvestmentInstructionStatusNames, InvestmentInstructionStatusNames> cancellationStatusMappings = Stream.of(
			new AbstractMap.SimpleEntry<>(OPEN, OPEN),
			new AbstractMap.SimpleEntry<>(SEND, SENT_CANCEL),
			new AbstractMap.SimpleEntry<>(SENDING, SENT_CANCEL),
			new AbstractMap.SimpleEntry<>(SENT, SENT_CANCEL),
			new AbstractMap.SimpleEntry<>(COMPLETED, CANCELED),
			new AbstractMap.SimpleEntry<>(CANCELED, CANCELED),
			new AbstractMap.SimpleEntry<>(ERROR, ERROR_CANCEL),
			new AbstractMap.SimpleEntry<>(PENDING_CANCEL, PENDING_CANCEL),
			new AbstractMap.SimpleEntry<>(SENT_CANCEL, SENT_CANCEL),
			new AbstractMap.SimpleEntry<>(ERROR_CANCEL, ERROR_CANCEL)
	).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));


	/**
	 * Get the corresponding cancellation status for the provided instruction item and intended status.
	 */
	public static InvestmentInstructionStatusNames getCancelledStatus(InvestmentInstructionItem item, InvestmentInstructionStatusNames statusName) {
		return Objects.nonNull(item) && Objects.nonNull(item.getStatus()) && item.getStatus().isCancel() ? cancellationStatusMappings.get(statusName) : statusName;
	}
}
