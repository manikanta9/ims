package com.clifton.investment.instruction.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The InvestmentInstructionSelectionSearchForm is a customized class used for code filtering when finding missing instructions for a particular category and
 * date.
 * <p>
 * Created by Mary
 */
@SearchForm(hasOrmDtoClass = false)
public class InvestmentInstructionSelectionSearchForm extends BaseAuditableEntitySearchForm {

	/**
	 * Category and Date are Required Properties, although if Date is null will use previous business day
	 */
	private short categoryId;
	private Date date;

	/////////////////////////////////////////////////
	// Note: Filters are applied via code, but sorting we'll map to specific fields to make it automated

	@SearchField(sortField = "clientAccount.number")
	private String clientAccountLabel;

	private String holdingAccountLabel;

	private String securityLabel;

	private Boolean booked;

	private String description;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(short categoryId) {
		this.categoryId = categoryId;
	}


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getClientAccountLabel() {
		return this.clientAccountLabel;
	}


	public void setClientAccountLabel(String clientAccountLabel) {
		this.clientAccountLabel = clientAccountLabel;
	}


	public String getHoldingAccountLabel() {
		return this.holdingAccountLabel;
	}


	public void setHoldingAccountLabel(String holdingAccountLabel) {
		this.holdingAccountLabel = holdingAccountLabel;
	}


	public String getSecurityLabel() {
		return this.securityLabel;
	}


	public void setSecurityLabel(String securityLabel) {
		this.securityLabel = securityLabel;
	}


	public Boolean getBooked() {
		return this.booked;
	}


	public void setBooked(Boolean booked) {
		this.booked = booked;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
