package com.clifton.investment.instruction.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author theodorez
 */
public class InvestmentInstructionContactGroupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "id")
	private Integer contactGroupId;

	@SearchField(searchField = "name")
	private String contactGroupName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getContactGroupId() {
		return this.contactGroupId;
	}


	public void setContactGroupId(Integer contactGroupId) {
		this.contactGroupId = contactGroupId;
	}


	public String getContactGroupName() {
		return this.contactGroupName;
	}


	public void setContactGroupName(String contactGroupName) {
		this.contactGroupName = contactGroupName;
	}
}
