package com.clifton.investment.instruction.process;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.investment.instruction.search.InvestmentInstructionSelectionSearchForm;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentInstructionProcessorServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class InvestmentInstructionProcessorServiceImpl implements InvestmentInstructionProcessorService {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentInstructionProcessor investmentInstructionProcessor;
	private InvestmentInstructionSetupService investmentInstructionSetupService;

	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status processInvestmentInstructionList(final short categoryId, final Date date, boolean synchronous) {
		StatusHolder statusHolder = new StatusHolder("Investment Instruction Processing");
		if (synchronous) {
			processInvestmentInstructionListImpl(categoryId, date, statusHolder);
			return statusHolder.getStatus();
		}

		// asynchronous run support
		String runId = "CATEGORY_" + categoryId + "_" + DateUtils.fromDateShort(date);
		final Date now = new Date();
		Runner runner = new AbstractStatusAwareRunner("INVESTMENT-INSTRUCTIONS", runId, now, statusHolder) {

			@Override
			public void run() {
				processInvestmentInstructionListImpl(categoryId, date, statusHolder);
			}
		};
		getRunnerHandler().runNow(runner);
		return Status.ofMessage("Started loading instructions for selected category and date. Processing will be completed shortly.");
	}


	private void processInvestmentInstructionListImpl(short categoryId, Date date, StatusHolder statusHolder) {
		getInvestmentInstructionProcessor().processInvestmentInstructionList(getInvestmentInstructionSetupService().getInvestmentInstructionCategory(categoryId), date, statusHolder.getStatus());
	}


	@Override
	public List<InvestmentInstructionSelection> getInvestmentInstructionSelectionMissingList(InvestmentInstructionSelectionSearchForm searchForm) {
		return getInvestmentInstructionProcessor().getInvestmentInstructionSelectionMissingList(searchForm);
	}


	@Override
	public List<InvestmentInstructionSelection> getInvestmentInstructionSelectionList(int definitionId, Date date) {
		if (date == null) {
			date = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);
		}
		InvestmentInstructionDefinition definition = getInvestmentInstructionSetupService().getInvestmentInstructionDefinition(definitionId);
		return getInvestmentInstructionProcessor().getInvestmentInstructionSelectionList(definition, date);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionSetupService getInvestmentInstructionSetupService() {
		return this.investmentInstructionSetupService;
	}


	public void setInvestmentInstructionSetupService(InvestmentInstructionSetupService investmentInstructionSetupService) {
		this.investmentInstructionSetupService = investmentInstructionSetupService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentInstructionProcessor getInvestmentInstructionProcessor() {
		return this.investmentInstructionProcessor;
	}


	public void setInvestmentInstructionProcessor(InvestmentInstructionProcessor investmentInstructionProcessor) {
		this.investmentInstructionProcessor = investmentInstructionProcessor;
	}
}
