package com.clifton.investment.instruction.setup.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentInstructionCategorySearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentInstructionCategorySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchFieldPath = "table", searchField = "name")
	private String tableName;

	@SearchField(searchField = "table.id")
	private Short tableId;

	@SearchField(searchFieldPath = "selectorBeanType", searchField = "name")
	private String selectorBeanTypeName;

	@SearchField
	private Boolean required;

	@SearchField
	private Boolean cash;

	@SearchField
	private Boolean client;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public String getSelectorBeanTypeName() {
		return this.selectorBeanTypeName;
	}


	public void setSelectorBeanTypeName(String selectorBeanTypeName) {
		this.selectorBeanTypeName = selectorBeanTypeName;
	}


	public Boolean getRequired() {
		return this.required;
	}


	public void setRequired(Boolean required) {
		this.required = required;
	}


	public Boolean getCash() {
		return this.cash;
	}


	public void setCash(Boolean cash) {
		this.cash = cash;
	}


	public Boolean getClient() {
		return this.client;
	}


	public void setClient(Boolean client) {
		this.client = client;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
