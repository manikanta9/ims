package com.clifton.investment.instruction.run;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.run.search.InvestmentInstructionRunInstructionItemSearchForm;
import com.clifton.investment.instruction.run.search.InvestmentInstructionRunSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author theodorez
 */
@Service
public class InvestmentInstructionRunServiceImpl implements InvestmentInstructionRunService {

	private AdvancedUpdatableDAO<InvestmentInstructionRun, Criteria> investmentInstructionRunDAO;
	private AdvancedUpdatableDAO<InvestmentInstructionRunInstructionItem, Criteria> investmentInstructionRunInstructionItemDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionRun getInvestmentInstructionRun(int id) {
		return getInvestmentInstructionRunDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInstructionRun> getInvestmentInstructionRunForItemListLatest(int[] itemIds) {
		if (!CollectionUtils.isEmptyCollectionOrArray(itemIds)) {
			return Arrays.stream(itemIds).
					mapToObj(this::getInvestmentInstructionRunForItemLatest).
					distinct().
					collect(Collectors.toList());
		}
		return Collections.emptyList();
	}


	@Override
	public InvestmentInstructionRun getInvestmentInstructionRunForItemLatest(int id) {
		return getInvestmentInstructionRunForItemLatestByType(id, null);
	}


	@Override
	public InvestmentInstructionRun getInvestmentInstructionRunForItemLatestByType(int itemId, InvestmentInstructionRunTypes runType) {
		ValidationUtils.assertNotNull(itemId, "Item ID must be specified");
		InvestmentInstructionRunSearchForm runSearchForm = new InvestmentInstructionRunSearchForm();
		runSearchForm.setItemId(itemId);
		runSearchForm.setLimit(1);
		runSearchForm.setRunType(runType);
		runSearchForm.setOrderBy("startDate:desc");

		return CollectionUtils.getOnlyElement(getInvestmentInstructionRunList(runSearchForm));
	}


	@Override
	public List<InvestmentInstructionRun> getInvestmentInstructionRunList(InvestmentInstructionRunSearchForm investmentInstructionRunSearchForm) {
		return getInvestmentInstructionRunDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(investmentInstructionRunSearchForm));
	}


	@Override
	public InvestmentInstructionRun saveInvestmentInstructionRun(InvestmentInstructionRun bean) {
		return getInvestmentInstructionRunDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentInstructionRunInstructionItem> getInvestmentInstructionRunInstructionItemList(InvestmentInstructionRunInstructionItemSearchForm searchForm) {
		return getInvestmentInstructionRunInstructionItemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentInstructionRunInstructionItem getInvestmentInstructionRunInstructionItem(int id) {
		return getInvestmentInstructionRunInstructionItemDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentInstructionRunInstructionItem saveInvestmentInstructionRunInstructionItem(InvestmentInstructionRunInstructionItem investmentInstructionRunInstructionItem) {
		return getInvestmentInstructionRunInstructionItemDAO().save(investmentInstructionRunInstructionItem);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstructionRun, Criteria> getInvestmentInstructionRunDAO() {
		return this.investmentInstructionRunDAO;
	}


	public void setInvestmentInstructionRunDAO(AdvancedUpdatableDAO<InvestmentInstructionRun, Criteria> investmentInstructionRunDAO) {
		this.investmentInstructionRunDAO = investmentInstructionRunDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInstructionRunInstructionItem, Criteria> getInvestmentInstructionRunInstructionItemDAO() {
		return this.investmentInstructionRunInstructionItemDAO;
	}


	public void setInvestmentInstructionRunInstructionItemDAO(AdvancedUpdatableDAO<InvestmentInstructionRunInstructionItem, Criteria> investmentInstructionRunInstructionItemDAO) {
		this.investmentInstructionRunInstructionItemDAO = investmentInstructionRunInstructionItemDAO;
	}
}
