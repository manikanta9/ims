package com.clifton.investment.instruction.setup;


import com.clifton.investment.instruction.setup.search.InvestmentInstructionCategorySearchForm;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionContactGroupSearchForm;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionContactSearchForm;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionDefinitionSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


public interface InvestmentInstructionSetupService {

	///////////////////////////////////////////////////////////////
	//////////           Instruction Category            //////////
	///////////////////////////////////////////////////////////////


	public InvestmentInstructionCategory getInvestmentInstructionCategory(short id);


	public InvestmentInstructionCategory getInvestmentInstructionCategoryByName(String name);


	public List<InvestmentInstructionCategory> getInvestmentInstructionCategoryList(InvestmentInstructionCategorySearchForm searchForm);


	public List<InvestmentInstructionCategory> getInvestmentInstructionCategoryListByDestinationType(String destinationType);


	public InvestmentInstructionCategory saveInvestmentInstructionCategory(InvestmentInstructionCategory bean);


	public void deleteInvestmentInstructionCategory(short id);


	///////////////////////////////////////////////////////////////
	//////////          Instruction Definition           //////////
	///////////////////////////////////////////////////////////////


	/**
	 * Also populates list of {@link InvestmentInstructionDefinitionField}
	 */
	public InvestmentInstructionDefinition getInvestmentInstructionDefinition(int id);


	public List<InvestmentInstructionDefinition> getInvestmentInstructionDefinitionList(InvestmentInstructionDefinitionSearchForm searchForm);


	/**
	 * Also saves list of {@link InvestmentInstructionDefinitionField}
	 */
	public InvestmentInstructionDefinition saveInvestmentInstructionDefinition(InvestmentInstructionDefinition bean);


	public InvestmentInstructionDefinition copyInvestmentInstructionDefinition(int id, String name);


	/**
	 * Creates a copy of the definition setting the new one as existing parent
	 */
	@RequestMapping("investmentInstructionDefinitionRevise")
	public InvestmentInstructionDefinition reviseInvestmentInstructionDefinition(int id);


	public void deleteInvestmentInstructionDefinition(int id);

	///////////////////////////////////////////////////////////////
	//////////       Instruction Definition Field        //////////
	///////////////////////////////////////////////////////////////


	public InvestmentInstructionDefinitionField getInvestmentInstructionDefinitionField(int id);


	public List<InvestmentInstructionDefinitionField> getInvestmentInstructionDefinitionFieldListByDefinition(int definitionId);


	public InvestmentInstructionDefinitionField saveInvestmentInstructionDefinitionField(InvestmentInstructionDefinitionField bean);


	public void deleteInvestmentInstructionDefinitionField(int id);

	///////////////////////////////////////////////////////////////
	//////////          Instruction Definition Contact   //////////
	///////////////////////////////////////////////////////////////


	public InvestmentInstructionContact saveInvestmentInstructionContact(InvestmentInstructionContact bean);


	public InvestmentInstructionContact getInvestmentInstructionContact(int id);


	public List<InvestmentInstructionContact> getInvestmentInstructionContactListAll();


	public List<InvestmentInstructionContact> getInvestmentInstructionContactList(InvestmentInstructionContactSearchForm searchForm);


	public void deleteInvestmentInstructionContact(int id);


	///////////////////////////////////////////////////////////////
	//////////   Instruction Definition Contact Groups   //////////
	///////////////////////////////////////////////////////////////


	public InvestmentInstructionContactGroup saveInvestmentInstructionContactGroup(InvestmentInstructionContactGroup bean);


	public InvestmentInstructionContactGroup getInvestmentInstructionContactGroup(int id);


	public List<InvestmentInstructionContactGroup> getInvestmentInstructionContactGroupListAll();


	public List<InvestmentInstructionContactGroup> getInvestmentInstructionContactGroupList(InvestmentInstructionContactGroupSearchForm searchForm);


	public void deleteInvestmentInstructionContactGroup(InvestmentInstructionContactGroup bean);
}
