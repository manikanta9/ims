package com.clifton.investment.instruction.run.destination;

import com.clifton.business.contact.BusinessContact;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;


/**
 * Parent generator responsible for combining multiple email generators into one output
 */
public class InvestmentInstructionDestinationParentEmailGenerator implements InvestmentInstructionDestinationParentGenerator {

	private List<InvestmentInstructionDestinationGenerator> childGeneratorList = new ArrayList<>();

	private InvestmentInstructionDestinationUtilHandler investmentInstructionDestinationUtilHandler;
	private TemplateConverter templateConverter; //Template converter for converting freemarker text

	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	@Override
	public Map<ExportMapKeys, String> generate(InvestmentInstructionRun run) {
		ValidationUtils.assertNotEmpty(getChildGeneratorList(), "There are no child generates to process.");
		Map<ExportMapKeys, String> destinationMap = new EnumMap<>(ExportMapKeys.class);
		StringBuilder recipients = new StringBuilder();

		BusinessContact senderContact = getInvestmentInstructionDestinationUtilHandler().getSenderContact(run);
		BusinessContact fromContact = getInvestmentInstructionDestinationUtilHandler().getFromContact(run, senderContact);

		//FOR TESTING PURPOSES ONLY
		String smtpHostUrl = null;
		String smtpHostPort = null;

		List<String> recipientEmails = new ArrayList<>();
		for (InvestmentInstructionDestinationGenerator generator : CollectionUtils.getIterable(getChildGeneratorList())) {
			Map<ExportMapKeys, String> childMap = generator.generateDestination(run);
			recipientEmails.add(childMap.get(ExportMapKeys.EMAIL_TO));

			//FOR TESTING PURPOSES ONLY - Get the SMTP Information off the child if it is not set
			if (smtpHostUrl == null && smtpHostPort == null && !StringUtils.isEmpty(childMap.get(ExportMapKeys.SMTP_HOSTNAME)) && !StringUtils.isEmpty(childMap.get(ExportMapKeys.SMTP_PORT))) {
				smtpHostUrl = childMap.get(ExportMapKeys.SMTP_HOSTNAME);
				smtpHostPort = childMap.get(ExportMapKeys.SMTP_PORT);
			}
		}

		//FOR TESTING PURPOSES ONLY
		if (!StringUtils.isEmpty(smtpHostUrl) && !StringUtils.isEmpty(smtpHostPort)) {
			destinationMap.put(ExportMapKeys.SMTP_HOSTNAME, smtpHostUrl);
			destinationMap.put(ExportMapKeys.SMTP_PORT, smtpHostPort);
		}

		recipients.append(StringUtils.join(recipientEmails, ";"));

		if (fromContact != null && fromContact.getEmailAddress() != null && !recipientEmails.contains(fromContact.getEmailAddress())) {
			destinationMap.put(ExportMapKeys.EMAIL_CC, fromContact.getEmailAddress());

			if (senderContact != null && senderContact.getEmailAddress() != null) {
				destinationMap.put(ExportMapKeys.EMAIL_SENDER, senderContact.getEmailAddress());
				if (senderContact.getNameLabel() != null) {
					destinationMap.put(ExportMapKeys.EMAIL_SENDER_NAME, senderContact.getNameLabel());
				}
			}
		}

		destinationMap.put(ExportMapKeys.EMAIL_TO, recipients.toString());
		//Empty string will result in the default email address being applied on the Integration Server
		destinationMap.put(ExportMapKeys.EMAIL_FROM, (fromContact != null) ? fromContact.getEmailAddress() : (senderContact != null) ? senderContact.getEmailAddress() : "");
		destinationMap.put(ExportMapKeys.EMAIL_FROM_NAME, (fromContact != null) ? fromContact.getNameLabel() : (senderContact != null) ? senderContact.getNameLabel() : "");

		populateSubjectAndMessageContent(destinationMap, senderContact, run);

		return destinationMap;
	}


	@Override
	public void addChildGenerator(InvestmentInstructionDestinationGenerator generator) {
		this.childGeneratorList.add(generator);
	}

	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	private void populateSubjectAndMessageContent(Map<ExportMapKeys, String> destinationMap, BusinessContact senderContact, InvestmentInstructionRun run) {
		String messageSubject = run.getInvestmentInstruction().getDefinition().getMessageSubjectContent();
		String messageBody = run.getInvestmentInstruction().getDefinition().getMessageBodyContent();
		destinationMap.put(ExportMapKeys.MESSAGE_SUBJECT, messageSubject != null ? processTemplate(messageSubject, senderContact, run.getInvestmentInstruction()) : getDefaultMessageSubjectText(destinationMap, run));
		destinationMap.put(ExportMapKeys.MESSAGE_TEXT, messageBody != null ? processTemplate(messageBody, senderContact, run.getInvestmentInstruction()) : getDefaultMessageBodyText(destinationMap, senderContact));
	}


	private String processTemplate(String template, BusinessContact senderContact, InvestmentInstruction instruction) {
		if (template != null) {
			TemplateConfig templateConfig = initializeTemplateConfig(template, senderContact, instruction);
			template = getTemplateConverter().convert(templateConfig);
		}
		return template;
	}


	private TemplateConfig initializeTemplateConfig(String text, BusinessContact senderContact, InvestmentInstruction instruction) {
		TemplateConfig config = new TemplateConfig(text != null ? text : "");
		if (text != null) {
			addToContext(config, "senderContact", senderContact);
			addToContext(config, "instruction", instruction);
		}
		return config;
	}


	private void addToContext(TemplateConfig config, String accessor, Object object) {
		if (object != null) {
			config.addBeanToContext(accessor, object);
		}
	}


	private String getDefaultMessageSubjectText(Map<ExportMapKeys, String> destinationMap, InvestmentInstructionRun run) {
		if (destinationMap.get(ExportMapKeys.FAX_RECIPIENT) != null) {
			return ExportDestinationTypes.PHONE + " to " + destinationMap.get(ExportMapKeys.FAX_RECIPIENT);
		}
		else {
			return run.getInvestmentInstruction().getLabel();
		}
	}


	private String getDefaultMessageBodyText(Map<ExportMapKeys, String> destinationMap, BusinessContact senderContact) {
		boolean html = destinationMap.get(ExportMapKeys.FAX_RECIPIENT) == null;
		String lineBreak = !html ? System.lineSeparator() : "<br />";

		StringBuilder text = new StringBuilder();
		text.append("Please see the attached file for investment instruction information.");
		text.append(lineBreak);
		text.append(lineBreak);
		text.append(lineBreak);

		if (senderContact != null) {
			if (senderContact.getFirstName() != null && senderContact.getLastName() != null) {
				text.append(senderContact.getFirstName());
				text.append(" ");
				text.append(senderContact.getLastName());
				text.append(lineBreak);
			}

			if (senderContact.getTitle() != null) {
				text.append(senderContact.getTitle());
				text.append(lineBreak);
			}

			if (senderContact.getPhoneNumber() != null) {
				text.append("T ");
				text.append(senderContact.getPhoneNumber());
				text.append(lineBreak);
			}

			if (senderContact.getFaxNumber() != null) {
				text.append("F ");
				text.append(senderContact.getFaxNumber());
				text.append(lineBreak);
			}

			if (senderContact.getEmailAddress() != null) {
				text.append("E ");
				if (html) {
					text.append("<a href=\"mailto:");
				}
				text.append(senderContact.getEmailAddress());
				if (html) {
					text.append("\">");
					text.append(senderContact.getEmailAddress());
					text.append("</a>");
				}
				text.append(lineBreak);
			}
		}
		if (html) {
			text.append("<a href=\"http://www.parametricportfolio.com\">");
		}
		text.append("www.parametricportfolio.com");
		if (html) {
			text.append("</a>");
		}

		return text.toString();
	}


	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////
	@Override
	public List<InvestmentInstructionDestinationGenerator> getChildGeneratorList() {
		return this.childGeneratorList;
	}


	@Override
	public void setChildGeneratorList(List<InvestmentInstructionDestinationGenerator> childGeneratorList) {
		this.childGeneratorList = childGeneratorList;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public InvestmentInstructionDestinationUtilHandler getInvestmentInstructionDestinationUtilHandler() {
		return this.investmentInstructionDestinationUtilHandler;
	}


	public void setInvestmentInstructionDestinationUtilHandler(InvestmentInstructionDestinationUtilHandler investmentInstructionDestinationUtilHandler) {
		this.investmentInstructionDestinationUtilHandler = investmentInstructionDestinationUtilHandler;
	}
}
