package com.clifton.investment.instruction.run.destination;

import com.clifton.business.contact.BusinessContact;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;

import java.util.EnumMap;
import java.util.Map;


/**
 * Generates the destinations for instructions to be sent via fax
 */
public class InvestmentInstructionDestinationFaxGenerator extends AbstractInvestmentInstructionDestinationGenerator {

	private InvestmentInstructionDestinationUtilHandler investmentInstructionDestinationUtilHandler;


	@Override
	public Map<ExportMapKeys, String> generateDestination(InvestmentInstructionRun run) {
		BusinessContact senderContact = getInvestmentInstructionDestinationUtilHandler().getSenderContact(run);

		BusinessContact recipientContact = getRecipientContact();
		ValidationUtils.assertNotNull(recipientContact.getFaxNumber(), "Fax number required for contact: " + recipientContact);
		Map<ExportMapKeys, String> destinationMap = new EnumMap<>(ExportMapKeys.class);
		destinationMap.put(ExportMapKeys.FAX_RECIPIENT, recipientContact.getFaxNumber());
		destinationMap.put(ExportMapKeys.FAX_RECIPIENT_NAME, recipientContact.getNameLabel());
		destinationMap.put(ExportMapKeys.MESSAGE_TEXT, ExportDestinationTypes.PHONE + " to " + destinationMap.get(ExportMapKeys.FAX_RECIPIENT));

		destinationMap.put(ExportMapKeys.FAX_SENDER, (senderContact != null) ? senderContact.getEmailAddress() : null);
		destinationMap.put(ExportMapKeys.FAX_SENDER_NAME, (senderContact != null) ? senderContact.getNameLabel() : null);

		return destinationMap;
	}


	@Override
	public String getDestinationName() {
		StringBuilder result = new StringBuilder(100);
		result.append(super.getDestinationName());
		result.append(": FAX");
		return result.toString();
	}

	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	public InvestmentInstructionDestinationUtilHandler getInvestmentInstructionDestinationUtilHandler() {
		return this.investmentInstructionDestinationUtilHandler;
	}


	public void setInvestmentInstructionDestinationUtilHandler(InvestmentInstructionDestinationUtilHandler investmentInstructionDestinationUtilHandler) {
		this.investmentInstructionDestinationUtilHandler = investmentInstructionDestinationUtilHandler;
	}
}
