package com.clifton.investment.instruction.selector;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.search.InvestmentInstructionItemSearchForm;
import com.clifton.investment.instruction.search.InvestmentInstructionSelectionSearchForm;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public abstract class BaseInvestmentInstructionSelector<T extends IdentityObject> implements InvestmentInstructionSelector<T> {

	private InvestmentInstructionService investmentInstructionService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Optionally exposed on those Selector Beans that allow customizing when an item is ready to send.
	 * If not populated, uses the default logic if the entity is booked
	 */
	private Integer instructionItemReadyConditionId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final List<InvestmentInstructionSelection> getInstructionSelectionMissingList(InvestmentInstructionCategory category, Date date, InvestmentInstructionSelectionSearchForm searchForm) {
		List<T> list = getEntityList(category, null, date);
		List<InvestmentInstructionSelection> selectionList = new ArrayList<>();

		if (!CollectionUtils.isEmpty(list)) {
			List<Integer> existingList = getExistingItemList(category, date);
			for (T entity : CollectionUtils.getIterable(list)) {
				if (!isInvestmentInstructionItemExists(entity, existingList)) {
					InvestmentInstructionSelection selection = new InvestmentInstructionSelection(category, (Integer) entity.getIdentity());
					populateInstructionSelectionDetails(selection, entity);
					if (isInvestmentInstructionSelectionIncluded(selection, searchForm)) {
						try {
							selection.setRecipientCompany(getRecipient(entity));
						}
						catch (ValidationException e) {
							// If no specific definition, then looking for missing and we'll give user error message associated with
							selection.setErrorMessage(e.getMessage());
						}
						selectionList.add(selection);
					}
				}
			}
		}
		return selectionList;
	}


	@Override
	public final List<InvestmentInstructionSelection> getInstructionSelectionList(InvestmentInstructionDefinition definition, Date date) {
		List<T> list = getEntityList(definition.getCategory(), definition, date);
		List<InvestmentInstructionSelection> selectionList = new ArrayList<>();
		for (T entity : CollectionUtils.getIterable(list)) {
			InvestmentInstructionSelection selection = new InvestmentInstructionSelection(definition, null, (Integer) entity.getIdentity(), getInstructionItemLabel(entity), entity);
			try {
				// Look up the Recipient within a try/catch.  If the system can't determine the recipient
				// it will show up on the failed list, instead of stopping all instructions for the category from being generated
				selection.setRecipientCompany(getRecipient(entity));
			}
			catch (ValidationException e) {
				selection.setErrorMessage(e.getMessage());
			}
			selectionList.add(selection);
		}
		return selectionList;
	}


	/**
	 * Returns the list of entities (Trades, CollateralBalance, AccountingEventJournalDetail, etc.)
	 */
	public abstract List<T> getEntityList(InvestmentInstructionCategory category, InvestmentInstructionDefinition definition, Date date);


	/**
	 * Returns the recipient for the instruction.  Using the Custodial Account Issuer or Holding Account Issuer
	 */
	public abstract BusinessCompany getRecipient(T entity);


	@Override
	public boolean isInstructionItemReady(InvestmentInstructionItem instructionItem, T entity) {
		// If there is no Ready Condition Defined - Use Booked status to determine if it's ready
		if (getInstructionItemReadyConditionId() == null) {
			return isInstructionItemBooked(instructionItem.getInstruction().getDefinition().getCategory(), entity);
		}
		// Current use cases (M2M Transfers and Cash Collateral Transfers) only need the related entity to compare fields against (Booked = True, Transfer Amount = 0, etc.)
		// If we need the item information, should create a new context to store that information and pass around
		return getSystemConditionEvaluationHandler().evaluateCondition(getInstructionItemReadyConditionId(), entity).isResult();
	}


	////////////////////////////////////////////////////////////////////
	//////////                 Helper Methods                ///////////
	////////////////////////////////////////////////////////////////////


	public List<Integer> getExistingItemList(InvestmentInstructionCategory category, Date date) {
		InvestmentInstructionItemSearchForm searchForm = new InvestmentInstructionItemSearchForm();
		searchForm.setCategoryId(category.getId());
		searchForm.setInstructionDate(date);
		List<InvestmentInstructionItem> existingList = getInvestmentInstructionService().getInvestmentInstructionItemList(searchForm);
		if (!CollectionUtils.isEmpty(existingList)) {
			return Arrays.asList(BeanUtils.getPropertyValues(existingList, "fkFieldId", Integer.class));
		}
		return new ArrayList<>();
	}


	protected boolean isInvestmentInstructionItemExists(T entity, List<Integer> existingList) {
		if (existingList.contains(BeanUtils.getIdentityAsInteger(entity))) {
			return true;
		}
		return false;
	}


	private boolean isInvestmentInstructionSelectionIncluded(InvestmentInstructionSelection selection, InvestmentInstructionSelectionSearchForm searchForm) {
		if (!SearchFormUtils.applyStringFilter(selection.getClientAccount() == null ? null : selection.getClientAccount().getLabel(), searchForm.getClientAccountLabel())) {
			return false;
		}
		if (!SearchFormUtils.applyStringFilter(selection.getHoldingAccount() == null ? null : selection.getHoldingAccount().getLabel(), searchForm.getHoldingAccountLabel())) {
			return false;
		}
		if (!SearchFormUtils.applyStringFilter(selection.getSecurity() == null ? null : selection.getSecurity().getLabel(), searchForm.getSecurityLabel())) {
			return false;
		}
		if (!SearchFormUtils.applyBooleanFilter(selection.isBooked(), searchForm.getBooked())) {
			return false;
		}
		if (!SearchFormUtils.applyStringFilter(selection.getDescription(), searchForm.getDescription())) {
			if (selection.getDescription() == null || !selection.getDescription().contains(searchForm.getDescription())) {
				return false;
			}
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////
	////////             Getter and Setter Methods           ///////////
	////////////////////////////////////////////////////////////////////


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public Integer getInstructionItemReadyConditionId() {
		return this.instructionItemReadyConditionId;
	}


	public void setInstructionItemReadyConditionId(Integer instructionItemReadyConditionId) {
		this.instructionItemReadyConditionId = instructionItemReadyConditionId;
	}
}
