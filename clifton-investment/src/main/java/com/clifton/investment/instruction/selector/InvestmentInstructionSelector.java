package com.clifton.investment.instruction.selector;


import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.search.InvestmentInstructionSelectionSearchForm;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentInstructionDefinitionSelector</code> interface
 *
 * @author manderson
 */
public interface InvestmentInstructionSelector<T extends IdentityObject> {

	/**
	 * Return a list of {@link InvestmentInstructionSelection} that includes entities that do not have an instruction item associated with them
	 */
	public List<InvestmentInstructionSelection> getInstructionSelectionMissingList(InvestmentInstructionCategory category, Date date, InvestmentInstructionSelectionSearchForm searchForm);


	/**
	 * Return a list of {@link InvestmentInstructionSelection}
	 */
	public List<InvestmentInstructionSelection> getInstructionSelectionList(InvestmentInstructionDefinition definition, Date date);


	/**
	 * Returns the instruction item label used in the UI to make the item easily understood by
	 * the user.  Most cases, this would be the result of getLabel(), but some cases the entity may not be a
	 * labeled object, or additional information is necessary
	 */
	public String getInstructionItemLabel(T entity);


	/**
	 * Returns true if the instruction item has been booked.
	 * <p>
	 * Note: We include the category because could be different logic based on what we are looking at: i.e. Collateral Cash vs. Collateral Position Transfers
	 */
	public boolean isInstructionItemBooked(InvestmentInstructionCategory category, T entity);


	/**
	 * Returns true if the instruction item is ready to be sent.
	 * Ready usually indicates that the item has been booked and is OK to send. In some cases, where the entity can't be booked (0 transfer amount) it could mean that the item has been reconciled. This can be determined by the definition if an option besides booked is available
	 * Used in the UI to make it easier to see ready status of the item
	 * because in some cases we want to generate the letter even if unbooked, but not send them until they are booked, a.k.a. Ready.
	 * <p>
	 * The entity is the referencing entity for the item (based on Table and FKField)
	 */
	public boolean isInstructionItemReady(InvestmentInstructionItem instructionItem, T entity);


	/**
	 * Used for Missing instructions - populates basic information like client account, holding account, etc. that can be used to easily find items missing instructions.
	 * As opposed to getInstructionItemLabel which just creates a short string of information
	 */
	public void populateInstructionSelectionDetails(InvestmentInstructionSelection selection, T entity);
}
