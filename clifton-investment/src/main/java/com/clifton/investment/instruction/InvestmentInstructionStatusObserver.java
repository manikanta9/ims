package com.clifton.investment.instruction;

import com.clifton.core.cache.TimedEvictionCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;
import com.clifton.investment.instruction.run.search.InvestmentInstructionRunInstructionItemSearchForm;
import com.clifton.investment.instruction.run.search.InvestmentInstructionRunSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


/**
 * <code>InvestmentInstructionStatusObserver</code> listen for changes to the {@link InvestmentInstructionItem#getStatus()}.
 * Add to the timeout cache:
 * - Item status is updated or created with status {@link InvestmentInstructionStatusNames#SENT}
 * Remove from the timeout cache:
 * - Item status was updated to
 * -- CANCELED
 * -- COMPLETED
 * -- ERROR
 * -- PENDING_CANCEL
 * -- ERROR_CANCEL
 *
 * @author TerryS
 */
@Component
public class InvestmentInstructionStatusObserver extends SelfRegisteringDaoObserver<InvestmentInstructionItem> {

	private static final List<InvestmentInstructionRunTypes> RUN_TYPES_LIST = Arrays.asList(InvestmentInstructionRunTypes.FAX, InvestmentInstructionRunTypes.SWIFT);
	private static final List<InvestmentInstructionStatusNames> ACKNOWLEDGED_STATUSES = Arrays.asList(
			InvestmentInstructionStatusNames.CANCELED,
			InvestmentInstructionStatusNames.COMPLETED,
			InvestmentInstructionStatusNames.ERROR,
			InvestmentInstructionStatusNames.PENDING_CANCEL,
			InvestmentInstructionStatusNames.ERROR_CANCEL
	);


	private InvestmentInstructionRunService investmentInstructionRunService;
	private TimedEvictionCache<InvestmentInstructionRunInstructionItem> investmentInstructionTimedStatusCache;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<InvestmentInstructionItem> dao, DaoEventTypes event, InvestmentInstructionItem item, Throwable e) {
		// not an error
		if (e == null) {
			// updates
			if (event.isUpdate()) {
				InvestmentInstructionItem originalBean = getOriginalBean(dao, item);
				// status was updated.
				if (originalBean != null && item != null && originalBean.getStatus() != item.getStatus() && item.getStatus() != null) {
					// status updated to SENT
					if (item.getStatus().getInvestmentInstructionStatusName() == InvestmentInstructionStatusNames.SENT) {
						sendSentStatusEvent(item);
					}
					else if (ACKNOWLEDGED_STATUSES.contains(item.getStatus().getInvestmentInstructionStatusName())) {
						// status updated to any other 'complete' status, send acknowledgement
						sendAcknowledgedStatusEvent(item);
					}
				}
			}
			// creates
			else if (item != null && item.getStatus() != null && item.getStatus().getInvestmentInstructionStatusName() == InvestmentInstructionStatusNames.SENT) {
				// create with status SENT
				sendSentStatusEvent(item);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void sendAcknowledgedStatusEvent(InvestmentInstructionItem item) {
		try {
			AssertUtils.assertNotNull(item, "Investment Instruction item instance is required.");
			AssertUtils.assertNotEquals(item.getStatus().getInvestmentInstructionStatusName(), InvestmentInstructionStatusNames.SENT, "Cannot acknowledge a SENT instruction.");

			List<InvestmentInstructionRun> runList = getInvestmentInstructionRunList(item);
			CollectionUtils.asNonNullList(runList).forEach(r -> doSendStatusEvent(r, item, true));
		}
		catch (Exception e) {
			LogUtils.error(InvestmentInstructionStatusObserver.class, "Could not send 'Acknowledgement' event for investment instruction item " + item.getId(), e);
		}
	}


	private void sendSentStatusEvent(InvestmentInstructionItem item) {
		try {
			AssertUtils.assertNotNull(item, "Investment Instruction item instance is required.");
			AssertUtils.assertEquals(item.getStatus().getInvestmentInstructionStatusName(), InvestmentInstructionStatusNames.SENT, "can only acknowledge a SENT instruction.");

			List<InvestmentInstructionRun> runList = getInvestmentInstructionRunList(item);
			CollectionUtils.asNonNullList(runList).forEach(r -> doSendStatusEvent(r, item, false));
		}
		catch (Exception e) {
			LogUtils.error(InvestmentInstructionStatusObserver.class, "Could not send 'Sent' event for investment instruction item " + item.getId(), e);
		}
	}


	private List<InvestmentInstructionRun> getInvestmentInstructionRunList(InvestmentInstructionItem item) {
		AssertUtils.assertNotNull(item, "Investment Instruction item instance is required.");

		InvestmentInstructionRunSearchForm investmentInstructionRunSearchForm = new InvestmentInstructionRunSearchForm();
		investmentInstructionRunSearchForm.setInstructionId(item.getInstruction().getId());
		investmentInstructionRunSearchForm.setItemId(item.getId());
		// MUST filter run types
		investmentInstructionRunSearchForm.setRunTypeList(CollectionUtils.toArray(RUN_TYPES_LIST, InvestmentInstructionRunTypes.class));

		return getInvestmentInstructionRunService().getInvestmentInstructionRunList(investmentInstructionRunSearchForm);
	}


	private void doSendStatusEvent(InvestmentInstructionRun run, InvestmentInstructionItem item, boolean acknowledgement) {
		AssertUtils.assertNotNull(run, "Investment Instruction run instance is required.");
		AssertUtils.assertNotNull(item, "Investment Instruction item instance is required.");
		AssertUtils.assertTrue(RUN_TYPES_LIST.contains(run.getRunType()), String.format("[%s] must be one of the following run types [%s].", run.getRunType(), RUN_TYPES_LIST));

		InvestmentInstructionRunInstructionItemSearchForm searchForm = new InvestmentInstructionRunInstructionItemSearchForm();
		searchForm.setInstructionRunId(run.getId());
		searchForm.setInstructionItemId(item.getId());
		List<InvestmentInstructionRunInstructionItem> runItemList = getInvestmentInstructionRunService().getInvestmentInstructionRunInstructionItemList(searchForm);
		for (InvestmentInstructionRunInstructionItem runItem : runItemList) {
			if (acknowledgement) {
				getInvestmentInstructionTimedStatusCache().remove(runItem.getIdentity());
			}
			else {
				getInvestmentInstructionTimedStatusCache().add(runItem);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionRunService getInvestmentInstructionRunService() {
		return this.investmentInstructionRunService;
	}


	public void setInvestmentInstructionRunService(InvestmentInstructionRunService investmentInstructionRunService) {
		this.investmentInstructionRunService = investmentInstructionRunService;
	}


	public TimedEvictionCache<InvestmentInstructionRunInstructionItem> getInvestmentInstructionTimedStatusCache() {
		return this.investmentInstructionTimedStatusCache;
	}


	public void setInvestmentInstructionTimedStatusCache(TimedEvictionCache<InvestmentInstructionRunInstructionItem> investmentInstructionTimedStatusCache) {
		this.investmentInstructionTimedStatusCache = investmentInstructionTimedStatusCache;
	}
}
