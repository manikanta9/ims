package com.clifton.investment.instruction.run.destination;

import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;


/**
 * This base generator is responsible for generating email and/or fax destinations
 */
public abstract class AbstractInvestmentInstructionDestinationGenerator implements InvestmentInstructionDestinationGenerator {


	private BusinessContactService businessContactService;


	/**
	 * The contact that will receive the email or fax
	 */
	private Integer recipientContactId;


	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////


	@Override
	public Class<?> getParentClass() {
		return null;
	}


	@Override
	public String getDestinationName() {
		StringBuilder result = new StringBuilder(100);
		result.append(getDestinationLabel());
		return result.toString();
	}


	@Override
	public String getDestinationLabel() {
		return getRecipientContact().getLabelShort();
	}


	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////


	protected BusinessContact getRecipientContact() {
		return getBusinessContactService().getBusinessContact(getRecipientContactId());
	}

	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////


	public Integer getRecipientContactId() {
		return this.recipientContactId;
	}


	public void setRecipientContactId(Integer recipientContactId) {
		this.recipientContactId = recipientContactId;
	}


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}
}
