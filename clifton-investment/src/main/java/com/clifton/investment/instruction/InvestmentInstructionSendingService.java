package com.clifton.investment.instruction;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;


/**
 * @author mwacker
 */
public interface InvestmentInstructionSendingService {

	@RequestMapping("investmentInstructionListSend")
	@SecureMethod(dtoClass = InvestmentInstructionItem.class, permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void sendInvestmentInstructionList(int[] instructionIds, Date wireDate);


	@RequestMapping("investmentInstructionSend")
	@SecureMethod(dtoClass = InvestmentInstructionItem.class, permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void sendInvestmentInstruction(int id, Date wireDate);


	@RequestMapping("investmentInstructionItemListSend")
	public void sendInvestmentInstructionItemList(int[] itemIds, Date wireDate);
}
