package com.clifton.investment.instruction.report;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instruction.InvestmentInstruction;

import java.util.Date;


/**
 * The <code>InvestmentInstructionReportService</code> ...
 *
 * @author manderson
 */
public interface InvestmentInstructionReportService {

	///////////////////////////////////////////////////////////////
	/////////             Instruction Reports             /////////
	///////////////////////////////////////////////////////////////


	/**
	 * Returns a concatenated report for each item held in the instruction
	 */
	@SecureMethod(dtoClass = InvestmentInstruction.class)
	public FileWrapper downloadInvestmentInstructionReport(int instructionId);


	/**
	 * Returns a concatenated report for each item held in all of the selected instructions
	 *
	 * @param instructionStatusId Optional status filter that is passed to the report for those that group multiple items together but we only want
	 *                            the items in a given status
	 */
	@SecureMethod(dtoClass = InvestmentInstruction.class)
	public FileWrapper downloadInvestmentInstructionReportList(Integer[] instructionIds, Integer instructionStatusId);


	/**
	 * Returns the report for a specific item held in the instruction
	 */
	@SecureMethod(dtoClass = InvestmentInstruction.class)
	public FileWrapper downloadInvestmentInstructionItemReport(int instructionItemId);


	/**
	 * Returns the report for a list of instruction items
	 *
	 * @param instructionItemIds
	 * @param instructionStatusId Optional status filter that is passed to the report for those that group multiple items together but we only want
	 *                            the items in a given status
	 */
	@SecureMethod(dtoClass = InvestmentInstruction.class)
	public FileWrapper downloadInvestmentInstructionItemReportList(Integer[] instructionItemIds, Integer instructionStatusId);


	/**
	 * Used during setup, allows previewing the report for a specific entity from the preview entities
	 */
	@SecureMethod(dtoClass = InvestmentInstruction.class)
	public FileWrapper downloadInvestmentInstructionItemReportPreview(int instructionDefinitionId, int fkFieldId, Date instructionDate, int recipientCompanyId);
}
