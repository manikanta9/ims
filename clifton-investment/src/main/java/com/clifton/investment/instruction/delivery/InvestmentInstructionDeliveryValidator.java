package com.clifton.investment.instruction.delivery;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * The <code>InvestmentInstructionDeliveryValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentInstructionDeliveryValidator extends SelfRegisteringDaoValidator<InvestmentInstructionDelivery> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentInstructionDelivery bean, DaoEventTypes config, ReadOnlyDAO<InvestmentInstructionDelivery> dao) throws ValidationException {
		ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getDeliveryABA())
				&& StringUtils.isEmpty(bean.getDeliverySwiftCode()), "ABA and/or Swift Code is required.", "deliveryABA");

		List<InvestmentInstructionDelivery> duplicates = dao.findByFields(new String[]{"name"}, new Object[]{bean.getName()});
		ValidationUtils.assertTrue(duplicates.stream().filter(i -> !Objects.equals(i.getId(), bean.getId())).collect(Collectors.toList()).isEmpty(), ()
				-> String.format("Duplicate Investment Instruction Delivery Names [%s] are not allowed", bean.getName()));
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(InvestmentInstructionDelivery bean, DaoEventTypes config) throws ValidationException {
		// uses dao method
	}
}
