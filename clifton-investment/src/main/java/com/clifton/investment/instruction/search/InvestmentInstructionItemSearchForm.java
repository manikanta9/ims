package com.clifton.investment.instruction.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseUpdatableEntitySearchForm;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;

import java.util.Date;


/**
 * The <code>InvestmentInstructionItemSearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentInstructionItemSearchForm extends BaseUpdatableEntitySearchForm {

	@SearchField(searchField = "instruction.id")
	private Integer instructionId;

	@SearchField(searchField = "instruction.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] instructionIds;

	@SearchField(searchField = "instructionDate", searchFieldPath = "instruction")
	private Date instructionDate;

	@SearchField
	private Integer fkFieldId;

	@SearchField(searchField = "fkFieldId", comparisonConditions = ComparisonConditions.IN)
	private Integer[] fkFieldIdList;

	@SearchField(searchFieldPath = "instruction", searchField = "definition.id")
	private Integer definitionId;

	@SearchField(searchFieldPath = "instruction.definition", searchField = "category.id")
	private Short categoryId;

	@SearchField(searchFieldPath = "instruction.definition.category", searchField = "table.id")
	private Short tableId;

	@SearchField(searchField = "status.id")
	private Short itemStatus;

	@SearchField(searchField = "instructionRunList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer runId;

	@SearchField(searchField = "name", searchFieldPath = "status", comparisonConditions = {ComparisonConditions.IN})
	private String[] instructionItemStatusList;

	@SearchField(searchField = "instructionRunList.runType", comparisonConditions = ComparisonConditions.EXISTS)
	private InvestmentInstructionRunTypes runType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInstructionId() {
		return this.instructionId;
	}


	public void setInstructionId(Integer instructionId) {
		this.instructionId = instructionId;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public Date getInstructionDate() {
		return this.instructionDate;
	}


	public void setInstructionDate(Date instructionDate) {
		this.instructionDate = instructionDate;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public Integer[] getFkFieldIdList() {
		return this.fkFieldIdList;
	}


	public void setFkFieldIdList(Integer[] fkFieldIdList) {
		this.fkFieldIdList = fkFieldIdList;
	}


	public Integer[] getInstructionIds() {
		return this.instructionIds;
	}


	public void setInstructionIds(Integer[] instructionIds) {
		this.instructionIds = instructionIds;
	}


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Short getItemStatus() {
		return this.itemStatus;
	}


	public void setItemStatus(Short itemStatus) {
		this.itemStatus = itemStatus;
	}


	public String[] getInstructionItemStatusList() {
		return this.instructionItemStatusList;
	}


	public void setInstructionItemStatusList(String[] instructionItemStatusList) {
		this.instructionItemStatusList = instructionItemStatusList;
	}


	public InvestmentInstructionRunTypes getRunType() {
		return this.runType;
	}


	public void setRunType(InvestmentInstructionRunTypes runType) {
		this.runType = runType;
	}
}
