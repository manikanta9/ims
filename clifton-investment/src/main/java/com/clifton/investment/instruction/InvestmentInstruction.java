package com.clifton.investment.instruction;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentInstruction</code> is the result instruction for a specific
 * {@link InvestmentInstructionDefinition}, {@link BusinessCompany} recipient,  and date
 *
 * @author manderson
 */
public class InvestmentInstruction extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentInstructionDefinition definition;

	/**
	 * Company that is receiving this instruction, generally the issuing company
	 * of the custodian or broker account.
	 */
	private BusinessCompany recipientCompany;


	private Date instructionDate;


	/**
	 * Status indicator
	 */
	private InvestmentInstructionStatus status;

	/**
	 * List of items (specific entities for the table under the definition category)
	 * that apply to this instruction - i.e. list of Trade Ids, M2M records, etc.
	 */
	private List<InvestmentInstructionItem> itemList;

	/**
	 * Not stored in the database. Specifies whether an instruction has unbooked items associated with it or not.
	 * Ready usually indicates the item is booked - but some definition selectors allow other conditions (Booked or (Amount = 0 and Reconciled))
	 */
	@NonPersistentField
	private Boolean allItemsReady;

	/**
	 * Indicates whether the instruction is locked for regeneration
	 * <p>
	 * NOTE: Stored in DB
	 */
	private boolean regenerating;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder lbl = new StringBuilder(16);
		if (getDefinition() != null) {
			if (getDefinition().getCategory() != null) {
				lbl.append(getDefinition().getCategory().getLabel());
			}
			lbl.append(" - ").append(getDefinition().getLabel());
		}
		if (getRecipientCompany() != null) {
			lbl.append(" To: ").append(getRecipientCompany().getName());
		}
		lbl.append(" On:").append(DateUtils.fromDateShort(getInstructionDate()));
		return lbl.toString();
	}


	public String getCategoryRecipientLabel() {
		if (getDefinition() != null && getDefinition().getCategory() != null) {
			return getDefinition().getCategory().getName() + " " + getRecipientLabel();
		}
		return getRecipientLabel();
	}


	public String getRecipientLabel() {
		StringBuilder lbl = new StringBuilder(16);
		if (getRecipientCompany() != null) {
			lbl.append(getRecipientCompany().getName());
		}

		return lbl.toString();
	}


	////////////////////////////////////////////////////////////////////////////


	public String getSelectionKey() {
		if (getDefinition() != null && getRecipientCompany() != null) {
			return getDefinition().getId() + "_" + getRecipientCompany().getId();
		}
		return null;
	}


	public void addItem(InvestmentInstructionItem item) {
		if (this.itemList == null) {
			this.itemList = new ArrayList<>();
		}
		this.itemList.add(item);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(InvestmentInstructionDefinition definition) {
		this.definition = definition;
	}


	public BusinessCompany getRecipientCompany() {
		return this.recipientCompany;
	}


	public void setRecipientCompany(BusinessCompany recipientCompany) {
		this.recipientCompany = recipientCompany;
	}


	public Date getInstructionDate() {
		return this.instructionDate;
	}


	public void setInstructionDate(Date instructionDate) {
		this.instructionDate = instructionDate;
	}


	public List<InvestmentInstructionItem> getItemList() {
		return this.itemList;
	}


	public void setItemList(List<InvestmentInstructionItem> itemList) {
		this.itemList = itemList;
	}


	public InvestmentInstructionStatus getStatus() {
		return this.status;
	}


	public void setStatus(InvestmentInstructionStatus status) {
		this.status = status;
	}


	public Boolean getAllItemsReady() {
		return this.allItemsReady;
	}


	public void setAllItemsReady(Boolean allItemsReady) {
		this.allItemsReady = allItemsReady;
	}


	public boolean isRegenerating() {
		return this.regenerating;
	}


	public void setRegenerating(boolean regenerating) {
		this.regenerating = regenerating;
	}
}
