package com.clifton.investment.instruction.setup;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.bean.SystemBean;

import java.util.Date;


/**
 * Defines what contact an instruction will be sent to for a given Company.
 * When an instruction is sent via Instruction Run, the system will send it to all contact's from the group
 * defined on instruction definition that match instruction's recipient company.
 *
 * @author theodorez
 */
public class InvestmentInstructionContact extends BaseEntity<Integer> {

	private InvestmentInstructionContactGroup group;
	/**
	 * The Company that the contact is associated with.
	 * <p>
	 * Because a Definition may send instructions to multiple companies the recipient contacts
	 * are determined by  the combination of Definition and Company.
	 */
	private BusinessCompany company;

	/**
	 * For historical purposes we keep track of the dates that instructions were sent to various contacts.
	 * <p>
	 * Ex: We are sending instructions to Person A who then leaves the recipient company and we stop sending instructions to that person after the specified end date.
	 */
	private Date startDate;
	private Date endDate;

	/**
	 * The system bean that implements the InvestmentInstructionDestinationGenerator interface
	 */
	private SystemBean destinationSystemBean;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getCompany() {
		return this.company;
	}


	public void setCompany(BusinessCompany company) {
		this.company = company;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public InvestmentInstructionContactGroup getGroup() {
		return this.group;
	}


	public void setGroup(InvestmentInstructionContactGroup group) {
		this.group = group;
	}


	public SystemBean getDestinationSystemBean() {
		return this.destinationSystemBean;
	}


	public void setDestinationSystemBean(SystemBean destinationSystemBean) {
		this.destinationSystemBean = destinationSystemBean;
	}
}
