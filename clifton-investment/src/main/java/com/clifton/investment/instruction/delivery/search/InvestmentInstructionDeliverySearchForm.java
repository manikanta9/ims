package com.clifton.investment.instruction.delivery.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentInstructionDeliverySearchForm</code> class defines search configuration for InvestmentInstructionDelivery objects.
 *
 * @author vgomelsky
 */
public class InvestmentInstructionDeliverySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "deliveryCompany.id")
	private Integer deliveryCompanyId;

	@SearchField
	private String deliveryAccountNumber;

	@SearchField
	private String deliveryAccountName;

	@SearchField
	private String deliveryABA;

	@SearchField
	private String deliverySwiftCode;

	@SearchField
	private String deliveryForFurtherCredit;

	// custom search configuration
	private Integer businessCompanyId;
	private Short typeId;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getDeliveryCompanyId() {
		return this.deliveryCompanyId;
	}


	public void setDeliveryCompanyId(Integer deliveryCompanyId) {
		this.deliveryCompanyId = deliveryCompanyId;
	}


	public String getDeliveryAccountNumber() {
		return this.deliveryAccountNumber;
	}


	public void setDeliveryAccountNumber(String deliveryAccountNumber) {
		this.deliveryAccountNumber = deliveryAccountNumber;
	}


	public String getDeliveryAccountName() {
		return this.deliveryAccountName;
	}


	public void setDeliveryAccountName(String deliveryAccountName) {
		this.deliveryAccountName = deliveryAccountName;
	}


	public String getDeliveryABA() {
		return this.deliveryABA;
	}


	public void setDeliveryABA(String deliveryABA) {
		this.deliveryABA = deliveryABA;
	}


	public String getDeliverySwiftCode() {
		return this.deliverySwiftCode;
	}


	public void setDeliverySwiftCode(String deliverySwiftCode) {
		this.deliverySwiftCode = deliverySwiftCode;
	}


	public String getDeliveryForFurtherCredit() {
		return this.deliveryForFurtherCredit;
	}


	public void setDeliveryForFurtherCredit(String deliveryForFurtherCredit) {
		this.deliveryForFurtherCredit = deliveryForFurtherCredit;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
