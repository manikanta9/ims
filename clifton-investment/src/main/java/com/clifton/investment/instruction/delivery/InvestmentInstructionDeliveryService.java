package com.clifton.investment.instruction.delivery;


import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryFieldSearchForm;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryFieldTypeSearchForm;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliverySearchForm;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryTypeSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.List;


public interface InvestmentInstructionDeliveryService {

	//////////////////////////////////////////////////////////////////////////// 
	///////      Investment Instruction Delivery Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionDelivery getInvestmentInstructionDelivery(int id);


	public List<InvestmentInstructionDelivery> getInvestmentInstructionDeliveryList(final InvestmentInstructionDeliverySearchForm searchForm);


	public InvestmentInstructionDelivery getInvestmentInstructionDeliveryByCompanyAndTypeAndCurrency(BusinessCompany company, InvestmentInstructionDeliveryType type, InvestmentSecurity deliveryCurrency);


	public InvestmentInstructionDelivery saveInvestmentInstructionDelivery(InvestmentInstructionDelivery bean);


	public void deleteInvestmentInstructionDelivery(int id);


	////////////////////////////////////////////////////////////////////////////
	///////    Investment Instruction Delivery Type Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionDeliveryType getInvestmentInstructionDeliveryType(Short id);


	public List<InvestmentInstructionDeliveryType> getInvestmentInstructionDeliveryTypeList(InvestmentInstructionDeliveryTypeSearchForm searchForm);


	public InvestmentInstructionDeliveryType getInvestmentInstructionDeliveryTypeByName(String deliveryType);


	public InvestmentInstructionDeliveryType saveInvestmentInstructionDeliveryType(InvestmentInstructionDeliveryType bean);


	public void deleteInvestmentInstructionDeliveryType(Short id);


	////////////////////////////////////////////////////////////////////////////
	///////    Investment Instruction Delivery Field Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionDeliveryField getInvestmentInstructionDeliveryField(int id);


	public List<InvestmentInstructionDeliveryField> getInvestmentInstructionDeliveryFieldList(InvestmentInstructionDeliveryFieldSearchForm searchForm);


	public InvestmentInstructionDeliveryField saveInvestmentInstructionDeliveryField(InvestmentInstructionDeliveryField bean);


	public void deleteInvestmentInstructionDeliveryField(int id);


	////////////////////////////////////////////////////////////////////////////
	///////    Investment Instruction Delivery Field Type Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionDeliveryFieldType getInvestmentInstructionDeliveryFieldType(short id);


	public InvestmentInstructionDeliveryFieldType getInvestmentInstructionDeliveryFieldTypeByName(String fieldTypeName);


	public List<InvestmentInstructionDeliveryFieldType> getInvestmentInstructionDeliveryFieldTypeList(InvestmentInstructionDeliveryFieldTypeSearchForm searchForm);
}
