package com.clifton.investment.instruction.run.messaging;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.message.ExportMessagingContent;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.export.messaging.message.ExportMessagingMessageList;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.destination.InvestmentInstructionDestinationGenerator;
import com.clifton.investment.instruction.run.destination.InvestmentInstructionDestinationParentGenerator;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Converts an investment instruction run into a message that can be sent to integration for transmission
 *
 * @author theodorez
 */
@Service
public class InvestmentInstructionRunMessagingConverterServiceImpl implements InvestmentInstructionRunMessagingConverterService {

	private String sourceSystemName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ApplicationContextService applicationContextService;
	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportMessagingMessage toExportMessagingMessage(InvestmentInstructionRun run, List<InvestmentInstructionContact> recipientContacts) {
		ExportMessagingMessage message = initializeMessage(run);
		populateInstructionDestinations(message, run, recipientContacts);
		return message;
	}


	@Override
	public ExportMessagingMessageList toExportMessagingMessageList(List<ExportMessagingMessage> messages, FileWrapper file, String rootDirectory) {
		ExportMessagingMessageList messageList = new ExportMessagingMessageList();
		messageList.setMessageList(messages);
		messageList.getContentList().add(populateContent(file, rootDirectory));
		messageList.setSourceSystemName(getSourceSystemName());
		messageList.setBaseArchivePath("Operations");
		return messageList;
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	private void populateInstructionDestinations(ExportMessagingMessage message, InvestmentInstructionRun run, List<InvestmentInstructionContact> recipientContacts) {
		Map<Class<?>, InvestmentInstructionDestinationParentGenerator> parentGeneratorMap = new HashMap<>();
		for (InvestmentInstructionContact contact : CollectionUtils.getIterable(recipientContacts)) {
			InvestmentInstructionDestinationGenerator generator = (InvestmentInstructionDestinationGenerator) getSystemBeanService().getBeanInstance(contact.getDestinationSystemBean());
			if (generator.getParentClass() != null) {
				ValidationUtils.assertTrue(InvestmentInstructionDestinationParentGenerator.class.isAssignableFrom(generator.getParentClass()), "The parent class does not implement InvestmentInstructionDestinationParentGenerator.class");
				InvestmentInstructionDestinationParentGenerator parentGenerator = parentGeneratorMap.get(generator.getParentClass());
				if (parentGenerator == null) {
					parentGenerator = (InvestmentInstructionDestinationParentGenerator) BeanUtils.newInstance(generator.getParentClass());
					getApplicationContextService().autowireBean(parentGenerator);
					parentGeneratorMap.put(parentGenerator.getClass(), parentGenerator);
				}
				parentGenerator.addChildGenerator(generator);
			}
			else {
				message.getDestinationList().add(generateDestination(run, generator.generateDestination(run)));
			}
		}

		for (InvestmentInstructionDestinationParentGenerator parentGenerator : new ArrayList<>(parentGeneratorMap.values())) {
			message.getDestinationList().add(generateDestination(run, parentGenerator.generate(run)));
		}

		//Add the recipient company name to the message's additional properties map
		//to pass along to the archiving process on the integration server to be used in the folder structure
		message.getProperties().put("RECIPIENT_COMPANY_NAME", run.getInvestmentInstruction().getRecipientCompany().getName());
	}


	private ExportMessagingContent populateContent(FileWrapper file, String rootDirectory) {
		ExportMessagingContent content = new ExportMessagingContent();

		//get the current path
		String path = FileUtils.getFilePath(file.getFile().getPath());

		//create the new file
		FileContainer newFile = FileContainerFactory.getFileContainer((rootDirectory != null ? rootDirectory : path) + File.separator + file.getFileName());

		//change the file name
		try {
			FileUtils.copyFile(FileContainerFactory.getFileContainer(file.getFile()), newFile);
		}
		catch (IOException ioe) {
			LogUtils.error(getClass(), "Failed to copy file [" + file.getFile().getPath() + "].", ioe);
		}

		//delete the original file
		try {
			FileUtils.delete(file.getFile());
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "Failed to delete file [" + file.getFile().getPath() + "].", e);
		}

		String completePath = newFile.getAbsolutePath();
		//delete the file in the temp directory after it is sent
		content.setDeleteSourceFile(true);
		content.setFileNameWithPath(FileUtils.getRelativePath(completePath, rootDirectory));
		return content;
	}


	private ExportMessagingDestination generateDestination(InvestmentInstructionRun run, Map<ExportMapKeys, String> destinationMap) {
		ExportMessagingDestination destination = new ExportMessagingDestination();
		destination.setType(run.getRunType().getDestinationType());
		destination.getPropertyList().putAll(destinationMap);
		return destination;
	}


	private ExportMessagingMessage initializeMessage(InvestmentInstructionRun run) {
		ExportMessagingMessage message = new ExportMessagingMessage();
		message.setSourceSystemIdentifier(run.getId().toString());
		return message;
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public String getSourceSystemName() {
		return this.sourceSystemName;
	}


	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
