package com.clifton.investment.instruction.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;

import java.util.Date;


/**
 * The <code>InvestmentInstructionSearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentInstructionSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "category.id", searchFieldPath = "definition")
	private Short instructionCategoryId;

	@SearchField(searchField = "name", searchFieldPath = "definition.category")
	private String instructionCategoryName;

	@SearchField(searchField = "name", searchFieldPath = "definition.category.table")
	private String tableName;

	@SearchField(searchField = "table.id", searchFieldPath = "definition.category")
	private Short tableId;

	@SearchField(searchField = "name", searchFieldPath = "definition")
	private String definitionName;

	@SearchField(searchField = "definition.id")
	private Integer definitionId;

	@SearchField(searchField = "recipientCompany.id")
	private Integer recipientCompanyId;

	@SearchField(searchField = "name", searchFieldPath = "recipientCompany")
	private String recipientCompanyName;

	@SearchField
	private Date instructionDate;

	/**
	 * Status indicator
	 */
	@SearchField(searchField = "name", searchFieldPath = "status", comparisonConditions = {ComparisonConditions.IN})
	private String[] instructionStatusList;

	/////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "InvestmentInstruction";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getInstructionCategoryId() {
		return this.instructionCategoryId;
	}


	public void setInstructionCategoryId(Short instructionCategoryId) {
		this.instructionCategoryId = instructionCategoryId;
	}


	public String getInstructionCategoryName() {
		return this.instructionCategoryName;
	}


	public void setInstructionCategoryName(String instructionCategoryName) {
		this.instructionCategoryName = instructionCategoryName;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Integer getRecipientCompanyId() {
		return this.recipientCompanyId;
	}


	public void setRecipientCompanyId(Integer recipientCompanyId) {
		this.recipientCompanyId = recipientCompanyId;
	}


	public String getRecipientCompanyName() {
		return this.recipientCompanyName;
	}


	public void setRecipientCompanyName(String recipientCompanyName) {
		this.recipientCompanyName = recipientCompanyName;
	}


	public Date getInstructionDate() {
		return this.instructionDate;
	}


	public void setInstructionDate(Date instructionDate) {
		this.instructionDate = instructionDate;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public String[] getInstructionStatusList() {
		return this.instructionStatusList;
	}


	public void setInstructionStatusList(String[] instructionStatusList) {
		this.instructionStatusList = instructionStatusList;
	}
}
