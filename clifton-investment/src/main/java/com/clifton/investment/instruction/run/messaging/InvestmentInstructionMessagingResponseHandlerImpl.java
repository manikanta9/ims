package com.clifton.investment.instruction.run.messaging;

import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.export.messaging.ExportMessagingResponseHandler;
import com.clifton.export.messaging.ExportStatuses;
import com.clifton.export.messaging.message.ExportMessagingStatusMessage;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;
import com.clifton.investment.instruction.run.search.InvestmentInstructionRunInstructionItemSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;

import java.util.List;


/**
 * Handles the response from integration and updates the Run and Item statuses accordingly
 *
 * @author theodorez
 */
public class InvestmentInstructionMessagingResponseHandlerImpl implements ExportMessagingResponseHandler {

	private String sourceSystemName;

	private InvestmentInstructionStatusService investmentInstructionStatusService;
	private InvestmentInstructionService investmentInstructionService;
	private InvestmentInstructionRunService investmentInstructionRunService;


	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////


	@Override
	public void process(ExportMessagingStatusMessage message) {
		int runId = Integer.parseInt(message.getSourceSystemIdentifier());
		String messageResult = message.getMessage();
		InvestmentInstructionRun run = getInvestmentInstructionRunService().getInvestmentInstructionRun(runId);

		if (message.getStatus() == ExportStatuses.PROCESSED) {
			setRunStatus(run, InvestmentInstructionStatusNames.COMPLETED, messageResult);
		}

		if (message.getStatus() == null || message.getStatus() == ExportStatuses.FAILED) {
			setRunStatus(run, InvestmentInstructionStatusNames.ERROR, messageResult);
		}

		// only update if this run is not detached, a detached run should not affect the instruction items
		if (!run.isDetached()) {
			refreshRunItemStatus(run);
			refreshInvestmentInstructionStatus(run);
		}
	}


	private void setRunStatus(InvestmentInstructionRun run, InvestmentInstructionStatusNames status, String messageResult) {
		StringBuilder message = new StringBuilder();
		if (run.getHistoryDescription() != null) {
			message.append(run.getHistoryDescription());
			message.append("\n\n");
		}
		message.append("INTEGRATION RESULT:\n");
		message.append(messageResult);

		run.setRunStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(status));
		run.setHistoryDescription(StringUtils.formatStringUpToNCharsWithDots(message.toString(), DataTypes.DESCRIPTION_LONG.getLength(), true));
		getInvestmentInstructionRunService().saveInvestmentInstructionRun(run);
	}


	private void refreshRunItemStatus(InvestmentInstructionRun run) {
		InvestmentInstructionRunInstructionItemSearchForm searchForm = new InvestmentInstructionRunInstructionItemSearchForm();
		searchForm.setInstructionRunId(run.getId());
		List<InvestmentInstructionRunInstructionItem> runItems = getInvestmentInstructionRunService().getInvestmentInstructionRunInstructionItemList(searchForm);

		for (InvestmentInstructionRunInstructionItem runItem : runItems) {
			InvestmentInstructionItem item = runItem.getReferenceTwo();
			InvestmentInstructionStatus itemStatus = getInvestmentInstructionItemStatus(item);
			InvestmentInstructionStatusNames mappedName = InvestmentInstructionStatusNames.getCancelledStatus(item, itemStatus.getInvestmentInstructionStatusName());
			itemStatus = mappedName != itemStatus.getInvestmentInstructionStatusName()
					? getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(mappedName) : itemStatus;
			item.setStatus(itemStatus);
			getInvestmentInstructionService().saveInvestmentInstructionItem(item);
		}
	}


	protected void refreshInvestmentInstructionStatus(InvestmentInstructionRun run) {
		getInvestmentInstructionService().refreshInvestmentInstructionStatus(run.getInvestmentInstruction(), null);
	}


	/**
	 * Determines the appropriate status for the item given its most recent runs
	 */
	private InvestmentInstructionStatus getInvestmentInstructionItemStatus(InvestmentInstructionItem item) {
		//Using the map of most recent run for each type, calculate the status that should be applied to the item
		InvestmentInstructionStatus itemStatus = null;

		//For each run type get the absolute latest run
		for (InvestmentInstructionRunTypes runType : InvestmentInstructionRunTypes.values()) {
			InvestmentInstructionRun run = getInvestmentInstructionRunService().getInvestmentInstructionRunForItemLatestByType(item.getId(), runType);
			if (run != null) {
				InvestmentInstructionStatus runStatus = run.getRunStatus();
				if (itemStatus != null && itemStatus.isFail()) {
					return getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR);
				}
				else if (itemStatus == null || itemStatus.compareStatusCode(runStatus) > 0) {
					itemStatus = runStatus;
				}
			}
		}

		return itemStatus;
	}


	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////


	@Override
	public String getSourceSystemName() {
		return this.sourceSystemName;
	}


	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}


	public InvestmentInstructionStatusService getInvestmentInstructionStatusService() {
		return this.investmentInstructionStatusService;
	}


	public void setInvestmentInstructionStatusService(InvestmentInstructionStatusService investmentInstructionStatusService) {
		this.investmentInstructionStatusService = investmentInstructionStatusService;
	}


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public InvestmentInstructionRunService getInvestmentInstructionRunService() {
		return this.investmentInstructionRunService;
	}


	public void setInvestmentInstructionRunService(InvestmentInstructionRunService investmentInstructionRunService) {
		this.investmentInstructionRunService = investmentInstructionRunService;
	}
}
