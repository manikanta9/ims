package com.clifton.investment.instruction.run;

import com.clifton.investment.instruction.run.search.InvestmentInstructionRunInstructionItemSearchForm;
import com.clifton.investment.instruction.run.search.InvestmentInstructionRunSearchForm;

import java.util.List;


/**
 * @author theodorez
 */
public interface InvestmentInstructionRunService {


	public List<InvestmentInstructionRun> getInvestmentInstructionRunList(InvestmentInstructionRunSearchForm investmentInstructionRunSearchForm);


	public InvestmentInstructionRun getInvestmentInstructionRun(int id);


	public InvestmentInstructionRun saveInvestmentInstructionRun(InvestmentInstructionRun bean);


	public List<InvestmentInstructionRun> getInvestmentInstructionRunForItemListLatest(int[] itemIds);


	public InvestmentInstructionRun getInvestmentInstructionRunForItemLatest(int id);


	public InvestmentInstructionRun getInvestmentInstructionRunForItemLatestByType(int itemId, InvestmentInstructionRunTypes runType);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentInstructionRunInstructionItem> getInvestmentInstructionRunInstructionItemList(InvestmentInstructionRunInstructionItemSearchForm searchForm);


	public InvestmentInstructionRunInstructionItem getInvestmentInstructionRunInstructionItem(int id);


	public InvestmentInstructionRunInstructionItem saveInvestmentInstructionRunInstructionItem(InvestmentInstructionRunInstructionItem investmentInstructionRunInstructionItem);
}
