package com.clifton.investment.instruction.delivery;

import com.clifton.business.company.BusinessCompany;
import com.clifton.instruction.Instruction;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * <code>InstructionDeliveryFieldCommand</code> Selection Criteria necessary to select specific delivery instructions
 * from a source entity such as trade, collateral, m2m.
 */
public interface InstructionDeliveryFieldCommand {

	public Instruction getInstruction();


	public BusinessCompany getDeliveryCompany();


	public InvestmentAccount getDeliveryAccount();


	public InvestmentSecurity getDeliveryCurrency();


	public InvestmentAccount getDeliveryClientAccount();
}
