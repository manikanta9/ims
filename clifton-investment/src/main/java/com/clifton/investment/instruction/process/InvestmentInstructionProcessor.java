package com.clifton.investment.instruction.process;


import com.clifton.core.util.status.Status;
import com.clifton.investment.instruction.search.InvestmentInstructionSelectionSearchForm;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentInstructionProcessor</code> ...
 *
 * @author manderson
 */
public interface InvestmentInstructionProcessor {

	////////////////////////////////////////////////////////////////////////////////
	//////////////              Instruction Processing               ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public void processInvestmentInstructionList(InvestmentInstructionCategory category, Date date, Status status);


	////////////////////////////////////////////////////////////////////////////////
	//////////////              Instruction Selections               ///////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used to find items missing an instruction for a category
	 */
	public List<InvestmentInstructionSelection> getInvestmentInstructionSelectionMissingList(InvestmentInstructionSelectionSearchForm searchForm);


	/**
	 * Used by both the actual processing/generation of instructions as well as the preview feature
	 */
	public List<InvestmentInstructionSelection> getInvestmentInstructionSelectionList(InvestmentInstructionDefinition definition, Date date);
}
