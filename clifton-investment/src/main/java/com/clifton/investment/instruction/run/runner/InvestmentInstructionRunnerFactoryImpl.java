package com.clifton.investment.instruction.run.runner;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionContactSearchForm;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Creates new Instruction Runners that will create Runs and send Instruction Items
 *
 * @author theodorez
 */
@Component
public class InvestmentInstructionRunnerFactoryImpl implements InvestmentInstructionRunnerFactory {

	public static final String INSTRUCTION_SWIFT = "Instruction SWIFT";

	private ApplicationContextService applicationContextService;

	/**
	 * Where the file will be stored when sent to integration
	 */
	private String rootDirectory;

	private InvestmentInstructionSetupService investmentInstructionSetupService;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionRunner createInvestmentInstructionRunner(Date runDate, List<InvestmentInstructionItem> items) {
		InvestmentInstructionRunner result = null;
		List<InvestmentInstructionRunner> detachedRunnerList = new ArrayList<>();

		List<InvestmentInstructionContact> contactList = getInstructionContacts(items.get(0).getInstruction(), items.get(0).getStatus().isCancel());
		MultiValueMap<String, InvestmentInstructionContact> contactMap = BeanUtils.getMultiValueMapFromProperty(contactList, "destinationSystemBean.type.name");

		List<InvestmentInstructionRunTypes> existingRunTypeList = new ArrayList<>();
		List<InvestmentInstructionRunTypes> detachedRunTypeList = new ArrayList<>();
		Map<Class<?>, MultiValueMap<InvestmentInstructionRunTypes, InvestmentInstructionContact>> runnerClassToContactList = new HashMap<>();
		for (Map.Entry<String, Collection<InvestmentInstructionContact>> instructionContactEntry : contactMap.entrySet()) {
			InvestmentInstructionRunTypes runType = InvestmentInstructionRunTypes.getInstructionRunTypeFromBeanTypeName(instructionContactEntry.getKey());
			updateDetachedRunList(runType, existingRunTypeList, detachedRunTypeList);
			List<InvestmentInstructionContact> typeContactList = new ArrayList<>(instructionContactEntry.getValue());

			Class<?> runnerClass = runType.getRunnerClass() != null ? runType.getRunnerClass() : InvestmentInstructionRunner.class;
			MultiValueMap<InvestmentInstructionRunTypes, InvestmentInstructionContact> runTypeMap = runnerClassToContactList.get(runnerClass);
			if (runTypeMap == null) {
				runTypeMap = new MultiValueHashMap<>(false);
				runnerClassToContactList.put(runType.getRunnerClass(), runTypeMap);
			}
			runTypeMap.putAll(runType, typeContactList);
		}

		for (Map.Entry<Class<?>, MultiValueMap<InvestmentInstructionRunTypes, InvestmentInstructionContact>> entry : runnerClassToContactList.entrySet()) {
			Class<?> clazz = entry.getKey();
			MultiValueMap<InvestmentInstructionRunTypes, InvestmentInstructionContact> runnerContactMap = entry.getValue();

			if (!CollectionUtils.isEmpty(runnerContactMap)) {
				boolean detached = isDetachedRun(detachedRunTypeList, runnerContactMap);
				InvestmentInstructionRunner runner = (InvestmentInstructionRunner) BeanUtils.newInstance(clazz.getConstructors()[0], runDate, items, runnerContactMap, detached);
				getApplicationContextService().autowireBean(runner);
				runner.setRootDirectory(getRootDirectory());
				ValidationUtils.assertTrue(detached || result == null, "Only one main runner is supported.");
				if (!detached) {
					result = runner;
				}
				else {
					detachedRunnerList.add(runner);
				}
			}
		}
		if (result != null) {
			result.setDetachedRunnerList(detachedRunnerList);
		}
		return result;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	private boolean isDetachedRun(List<InvestmentInstructionRunTypes> detachedRunTypeList, MultiValueMap<InvestmentInstructionRunTypes, InvestmentInstructionContact> runnerContactMap) {
		for (InvestmentInstructionRunTypes type : runnerContactMap.keySet()) {
			if (detachedRunTypeList.contains(type)) {
				return true;
			}
		}
		return false;
	}


	private void updateDetachedRunList(@NotNull InvestmentInstructionRunTypes newRunType, List<InvestmentInstructionRunTypes> existingRunTypeList, List<InvestmentInstructionRunTypes> detachedRunTypeList) {
		for (InvestmentInstructionRunTypes runType : CollectionUtils.getIterable(existingRunTypeList)) {
			ValidationUtils.assertTrue(runType.isParallelRunSupported(newRunType), "Cannot create simultaneous runs for type [" + runType + "] and [" + newRunType + "].");
			InvestmentInstructionRunTypes detachedRunType = null;
			if (runType.isAllowedDetachedRunType(newRunType)) {
				detachedRunType = newRunType;
			}
			else if (newRunType.isAllowedDetachedRunType(runType)) {
				detachedRunType = runType;
			}
			if (detachedRunType != null && !detachedRunTypeList.contains(detachedRunType)) {
				detachedRunTypeList.add(detachedRunType);
			}
		}
		existingRunTypeList.add(newRunType);
	}


	private List<InvestmentInstructionContact> getInstructionContacts(InvestmentInstruction instruction, boolean isCancel) {
		InvestmentInstructionContactSearchForm contactSearchForm = new InvestmentInstructionContactSearchForm();
		contactSearchForm.setContactGroupId(instruction.getDefinition().getContactGroup().getId());
		contactSearchForm.setCompanyId(instruction.getRecipientCompany().getId());
		contactSearchForm.setActiveOnDate(new Date());
		if (isCancel) {
			contactSearchForm.setDestinationSystemBeanTypeName(INSTRUCTION_SWIFT);
		}
		return getInvestmentInstructionSetupService().getInvestmentInstructionContactList(contactSearchForm);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public String getRootDirectory() {
		return this.rootDirectory;
	}


	public void setRootDirectory(String rootDirectory) {
		this.rootDirectory = rootDirectory;
	}


	public InvestmentInstructionSetupService getInvestmentInstructionSetupService() {
		return this.investmentInstructionSetupService;
	}


	public void setInvestmentInstructionSetupService(InvestmentInstructionSetupService investmentInstructionSetupService) {
		this.investmentInstructionSetupService = investmentInstructionSetupService;
	}
}
