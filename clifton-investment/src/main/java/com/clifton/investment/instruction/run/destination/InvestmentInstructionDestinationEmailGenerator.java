package com.clifton.investment.instruction.run.destination;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;

import java.util.EnumMap;
import java.util.Map;


/**
 * Generates the destination for instructions to be sent via email
 */
public class InvestmentInstructionDestinationEmailGenerator extends AbstractInvestmentInstructionDestinationGenerator {

	/**
	 * Used for testing purposes ONLY
	 */
	private String smtpHostUrl;
	private Integer smtpHostPort;


	@Override
	public Map<ExportMapKeys, String> generateDestination(InvestmentInstructionRun run) {
		ValidationUtils.assertNotNull(getRecipientContact().getEmailAddress(), "Email address is required for contact: " + getRecipientContact());
		Map<ExportMapKeys, String> destinationMap = new EnumMap<>(ExportMapKeys.class);
		destinationMap.put(ExportMapKeys.EMAIL_TO, getRecipientContact().getEmailAddress());
		if (!StringUtils.isEmpty(getSmtpHostUrl()) && getSmtpHostPort() != null) {
			destinationMap.put(ExportMapKeys.SMTP_HOSTNAME, getSmtpHostUrl());
			destinationMap.put(ExportMapKeys.SMTP_PORT, getSmtpHostPort().toString());
		}
		return destinationMap;
	}


	@Override
	public String getDestinationName() {
		StringBuilder result = new StringBuilder(100);
		result.append(super.getDestinationName());
		result.append(": EMAIL");
		return result.toString();
	}


	@Override
	public Class<?> getParentClass() {
		return InvestmentInstructionDestinationParentEmailGenerator.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSmtpHostUrl() {
		return this.smtpHostUrl;
	}


	public void setSmtpHostUrl(String smtpHostUrl) {
		this.smtpHostUrl = smtpHostUrl;
	}


	public Integer getSmtpHostPort() {
		return this.smtpHostPort;
	}


	public void setSmtpHostPort(Integer smtpHostPort) {
		this.smtpHostPort = smtpHostPort;
	}
}
