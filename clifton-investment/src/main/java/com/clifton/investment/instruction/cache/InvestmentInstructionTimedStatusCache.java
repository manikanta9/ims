package com.clifton.investment.instruction.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.AbstractTimedEvictionCache;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.priority.SystemPriority;
import com.clifton.system.priority.SystemPriorityService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.websocket.WebSocketHandler;
import com.clifton.websocket.alert.WebsocketAlertMessage;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.Duration;
import java.util.List;
import java.util.Objects;


/**
 * <code>InvestmentInstructionTimedStatusCache</code> track {@link InvestmentInstructionRun#getRunStatus()} and issue transient notifications to the operations group members if an
 * instruction item hasn't progressed in at least {@link InvestmentInstructionTimedStatusCache#getEvictionTimeout()}.
 *
 * @author TerryS
 */
@Component
public class InvestmentInstructionTimedStatusCache extends AbstractTimedEvictionCache<IdentityObject> {

	private InvestmentInstructionRunService investmentInstructionRunService;
	private SecurityUserService securityUserService;
	private WebSocketHandler webSocketHandler;

	private SystemPriorityService systemPriorityService;
	private SystemSchemaService systemSchemaService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionTimedStatusCache() {
		super(Duration.ofSeconds(30), Duration.ofMinutes(15));
	}


	@Override
	public void process(Serializable identifier) {
		InvestmentInstructionRunInstructionItem investmentInstructionRunInstructionItem = getInvestmentInstructionRunService().getInvestmentInstructionRunInstructionItem((Integer) identifier);
		if (Objects.isNull(investmentInstructionRunInstructionItem)) {
			LogUtils.error(LogCommand.ofMessageSupplier(this.getClass(), () -> String.format("Could not create 'SENT' status timeout notification because [InvestmentInstructionRunInstructionItem] [%s] was not found", identifier)));
		}
		else {
			sendNotification(investmentInstructionRunInstructionItem);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Send a transient notification message for this timed-out Investment Instruction Item.
	 */
	private void sendNotification(final InvestmentInstructionRunInstructionItem investmentInstructionRunInstructionItem) {
		WebsocketAlertMessage notificationMessage = new WebsocketAlertMessage(
				"Investment Instruction Item Timeout",
				createSubjectLine(investmentInstructionRunInstructionItem),
				getSystemPriorityService().getSystemPriorityByName(SystemPriority.SYSTEM_PRIORITY_IMMEDIATE),
				getSystemSchemaService().getSystemTableByName(InvestmentInstructionItem.INVESTMENT_INSTRUCTION_ITEM_TABLE_NAME),
				investmentInstructionRunInstructionItem.getReferenceTwo().getId()
		);
		List<SecurityUser> recipientList = getOperationsSecurityGroupUserList();
		getWebSocketHandler().sendMessageToUserList(recipientList, WebsocketAlertMessage.CHANNEL_USER_TOPIC_IMMEDIATE_SYSTEM_TOAST_MESSAGES, notificationMessage);
	}


	/**
	 * example:  Investment Instruction Item for Trade: 12345 has been in [SENT] status for at least 15 minutes.
	 */
	private String createSubjectLine(final InvestmentInstructionRunInstructionItem investmentInstructionRunInstructionItem) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(investmentInstructionRunInstructionItem.getReferenceOne().getRunType());
		builder.append("] Instruction for ");
		builder.append(investmentInstructionRunInstructionItem.getReferenceTwo().getFkFieldLabel());
		builder.append(" has been in [");
		builder.append(investmentInstructionRunInstructionItem.getReferenceTwo().getStatus().getName());
		builder.append("] status for at least ");
		builder.append(getEvictionTimeout().toMinutes());
		builder.append(" minutes ");
		return builder.toString();
	}


	/**
	 * Get the unique list of users from the Operations Security group.
	 */
	private List<SecurityUser> getOperationsSecurityGroupUserList() {
		SecurityGroup securityGroup = getSecurityUserService().getSecurityGroupByName(SecurityGroup.OPERATIONS);
		AssertUtils.assertNotNull(securityGroup, "Could not retrieve the %s security group.", SecurityGroup.OPERATIONS);
		return getSecurityUserService().getSecurityUserListByGroup(securityGroup.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionRunService getInvestmentInstructionRunService() {
		return this.investmentInstructionRunService;
	}


	public void setInvestmentInstructionRunService(InvestmentInstructionRunService investmentInstructionRunService) {
		this.investmentInstructionRunService = investmentInstructionRunService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}


	public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = webSocketHandler;
	}


	public SystemPriorityService getSystemPriorityService() {
		return this.systemPriorityService;
	}


	public void setSystemPriorityService(SystemPriorityService systemPriorityService) {
		this.systemPriorityService = systemPriorityService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
