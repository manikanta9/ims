package com.clifton.investment.instruction;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instruction.search.InvestmentInstructionItemSearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * <code>InstructionUtilHandlerImpl</code> Investment-specific implementation of an instruction scoped interface.
 */
@Component
public class InstructionUtilHandlerImpl implements InstructionUtilHandler<InvestmentInstructionItem, InstructionDeliveryFieldCommand> {

	private SystemSchemaService systemSchemaService;
	private SystemSchemaUtilHandler systemSchemaUtilHandler;
	private InvestmentInstructionService investmentInstructionService;
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentInstructionItem> getInstructionList(String systemTableName, Integer fkFieldId) {
		AssertUtils.assertNotEmpty(systemTableName, "System Table is required.");
		AssertUtils.assertNotNull(fkFieldId, "Foreign Key Field Identifier is required.");

		SystemTable systemTable = getSystemSchemaService().getSystemTableByName(systemTableName);
		AssertUtils.assertNotNull(systemTable, "The system table could not be found [%s]", systemTableName);

		InvestmentInstructionItemSearchForm searchForm = new InvestmentInstructionItemSearchForm();
		searchForm.setTableId(systemTable.getId());
		searchForm.setFkFieldId(fkFieldId);
		return getInvestmentInstructionService().getInvestmentInstructionItemList(searchForm);
	}


	@Override
	public final String getTransactionReferenceNumber(List<IdentityObject> components) {
		SystemSchemaUtilHandler schemaUtilHandler = getSystemSchemaUtilHandler();
		return CollectionUtils.getStream(components)
				.filter(Objects::nonNull)
				.map(schemaUtilHandler::getEntityUniqueId)
				.collect(Collectors.joining(InstructionGenerator.REFERENCE_COMPONENT_DELIMITER));
	}


	@Override
	public List<String> getAdditionalRecipientBicList(InstructionDeliveryFieldCommand selectionProvider) {
		return getInvestmentInstructionDeliveryUtilHandler().getAdditionalPartyDeliveryFieldBicList(selectionProvider);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public SystemSchemaUtilHandler getSystemSchemaUtilHandler() {
		return this.systemSchemaUtilHandler;
	}


	public void setSystemSchemaUtilHandler(SystemSchemaUtilHandler systemSchemaUtilHandler) {
		this.systemSchemaUtilHandler = systemSchemaUtilHandler;
	}


	public InvestmentInstructionDeliveryUtilHandler getInvestmentInstructionDeliveryUtilHandler() {
		return this.investmentInstructionDeliveryUtilHandler;
	}


	public void setInvestmentInstructionDeliveryUtilHandler(InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler) {
		this.investmentInstructionDeliveryUtilHandler = investmentInstructionDeliveryUtilHandler;
	}
}
