package com.clifton.investment.instruction.process;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.group.InvestmentInstructionGroupService;
import com.clifton.investment.instruction.group.InvestmentInstructionItemGroup;
import com.clifton.investment.instruction.group.populator.InvestmentInstructionItemGroupPopulator;
import com.clifton.investment.instruction.search.InvestmentInstructionItemSearchForm;
import com.clifton.investment.instruction.search.InvestmentInstructionSelectionSearchForm;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelector;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionDefinitionSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class InvestmentInstructionProcessorImpl implements InvestmentInstructionProcessor {

	private ApplicationContextService applicationContextService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentInstructionService investmentInstructionService;
	private InvestmentInstructionGroupService investmentInstructionGroupService;
	private InvestmentInstructionSetupService investmentInstructionSetupService;
	private InvestmentInstructionStatusService investmentInstructionStatusService;

	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////////
	//////////////              Instruction Processing               ///////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void processInvestmentInstructionList(InvestmentInstructionCategory category, Date date, Status status) {
		String label = "Category [" + category.getName() + "] on " + DateUtils.fromDateShort(date);
		status.addMessage("Starting processing " + label);

		InvestmentInstructionItemSearchForm runningItemForm = new InvestmentInstructionItemSearchForm();
		runningItemForm.setCategoryId(category.getId());
		runningItemForm.setInstructionDate(date);
		runningItemForm.setInstructionItemStatusList(new String[]{
				InvestmentInstructionStatusNames.SEND.toString(),
				InvestmentInstructionStatusNames.SENDING.toString(),
				InvestmentInstructionStatusNames.SENT.toString(),
				InvestmentInstructionStatusNames.PENDING_CANCEL.toString(),
				InvestmentInstructionStatusNames.SENT_CANCEL.toString()
		});
		List<InvestmentInstructionItem> runningItemList = getInvestmentInstructionService().getInvestmentInstructionItemList(runningItemForm);
		if (!CollectionUtils.isEmpty(runningItemList)) {
			status.addMessage("Canceled processing for " + label + " Instructions are currently being sent.");
			for (InvestmentInstructionItem item : runningItemList) {
				status.addError("Instruction Item " + item.getFkFieldLabel() + " [" + item.getId() + "] for Instruction " + item.getInstruction().getLabel() + "[" + item.getInstruction().getId() + "] currently has status " + item.getStatus().getName());
			}
		}
		else {
			try {
				getInvestmentInstructionService().setInvestmentInstructionRegeneratingFlag(category, date);
				InvestmentInstructionDefinitionSearchForm searchForm = new InvestmentInstructionDefinitionSearchForm();
				searchForm.setInstructionCategoryId(category.getId());
				searchForm.setWorkflowStatusNameEquals(WorkflowStatus.STATUS_ACTIVE);
				List<InvestmentInstructionDefinition> definitionList = getInvestmentInstructionSetupService().getInvestmentInstructionDefinitionList(searchForm);

				int totalCount = 0;
				int failedCount = 0;

				if (CollectionUtils.isEmpty(definitionList)) {
					status.addMessage(label + ": No Active Instruction Definitions exist/No Instructions loaded");
				}
				else {
					// Stores list of instruction selections
					List<InvestmentInstructionSelection> definitionSelections = new ArrayList<>();

					// Stores FKFieldID mapped to the Definition to catch cases where an entity would apply to multiple definitions
					Map<Integer, InvestmentInstructionDefinition> entitySelectionMap = new HashMap<>();

					// Error map for those entities mapped to multiple definitions
					Map<Integer, String> errorMap = new HashMap<>();

					// Go through each definition and map list of selections to their corresponding definition.  Also generates a map of those that are being
					// applied to multiple so we can show their error messages.
					for (InvestmentInstructionDefinition definition : definitionList) {
						List<InvestmentInstructionSelection> selectionList = getInvestmentInstructionSelectionList(definition, date);

						if (CollectionUtils.isEmpty(selectionList)) {
							status.addSkipped("Definition [" + definition.getName() + "]: No matching records found.");
						}
						else {
							status.addMessage("Definition [" + definition.getName() + "]: " + selectionList.size() + " records found.");
							for (InvestmentInstructionSelection selection : CollectionUtils.getIterable(selectionList)) {
								totalCount++;
								if (!StringUtils.isEmpty(selection.getErrorMessage())) {
									failedCount++;
									status.addError(selection.getFkFieldLabel() + ": " + selection.getErrorMessage());
								}
								else if (entitySelectionMap.containsKey(selection.getFkFieldId())) {
									// Should never happen, but first verify if the definition is the same
									InvestmentInstructionDefinition existingDefinition = entitySelectionMap.get(selection.getFkFieldId());
									if (!definition.equals(existingDefinition)) {
										String errorMsg;
										if (errorMap.containsKey(selection.getFkFieldId())) {
											errorMsg = errorMap.get(selection.getFkFieldId());
											errorMsg += ", " + definition.getName();
										}
										else {
											failedCount++;
											errorMsg = selection.getFkFieldLabel() + " is mapped to multiple definitions: " + existingDefinition.getName() + ", " + definition.getName();
										}
										errorMap.put(selection.getFkFieldId(), errorMsg);
									}
								}
								else {
									entitySelectionMap.put(selection.getFkFieldId(), definition);
									definitionSelections.add(selection);
								}
							}
						}
					}

					// Get a List of all existing instruction items - will skip those that are there
					InvestmentInstructionItemSearchForm itemSearchForm = new InvestmentInstructionItemSearchForm();
					itemSearchForm.setCategoryId(category.getId());
					itemSearchForm.setInstructionDate(date);
					List<InvestmentInstructionItem> existingItemList = getInvestmentInstructionService().getInvestmentInstructionItemList(itemSearchForm);

					InvestmentInstructionStatus openStatus = getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.OPEN);

					Map<String, InvestmentInstruction> instructionMap = new HashMap<>();
					// Items can only be mapped to multiple if there are runs associated with them in which case all are marked as canceled except for the latest valid one
					Map<Integer, List<InvestmentInstructionItem>> existingItemMap = new HashMap<>();
					for (InvestmentInstructionItem item : CollectionUtils.getIterable(existingItemList)) {
						if (existingItemMap.containsKey(item.getFkFieldId())) {
							existingItemMap.get(item.getFkFieldId()).add(item);
						}
						else {
							existingItemMap.put(item.getFkFieldId(), CollectionUtils.createList(item));
						}
						instructionMap.put(item.getInstruction().getSelectionKey(), item.getInstruction());
					}


					for (InvestmentInstructionSelection selection : CollectionUtils.getIterable(definitionSelections)) {
						// If FkFieldID is mapped to an error - skip it
						if (errorMap.containsKey(selection.getFkFieldId())) {
							// Already tracking failed count - so nothing to increment here
							status.addError(errorMap.get(selection.getFkFieldId()));
							continue;
						}

						// Already exists
						InvestmentInstruction instruction = instructionMap.get(selection.getSelectionKey());

						List<InvestmentInstructionItem> itemList = existingItemMap.get(selection.getFkFieldId());
						InvestmentInstructionItem instructionItem = null;
						if (!CollectionUtils.isEmpty(itemList)) {
							if (itemList.size() == 1) {
								instructionItem = itemList.get(0);
							}
							else {
								for (InvestmentInstructionItem item : itemList) {
									if (!StringUtils.isEqual(InvestmentInstructionStatusNames.CANCELED.name(), item.getStatus().getName())) {
										instructionItem = item;
									}
								}
							}
						}

						// Create the instruction (if necessary) and item
						if (instruction == null) {
							instruction = new InvestmentInstruction();
							instruction.setDefinition(selection.getDefinition());
							instruction.setRecipientCompany(selection.getRecipientCompany());
							instruction.setInstructionDate(date);
							instruction.setStatus(openStatus);
						}
						// If instruction item changes instructions - then create a new one
						// Existing items not on the original instruction will either be automatically deleted, or if there are runs associated with them, will be marked as canceled.
						if (instructionItem == null || !instruction.equals(instructionItem.getInstruction())) {
							instructionItem = new InvestmentInstructionItem();
							instructionItem.setInstruction(instruction);
							instructionItem.setFkFieldId(selection.getFkFieldId());
							instructionItem.setStatus(openStatus);
						}
						instructionItem.setItemGroup(selection.getItemGroup());
						instruction.addItem(instructionItem);
						instructionMap.put(selection.getSelectionKey(), instruction);
					}

					// Create the instructions in the database
					saveInvestmentInstructionListImpl(instructionMap.values());
					status.setMessage(label + " Processing Complete. Total:  " + totalCount + "; Success: " + (totalCount - failedCount) + "; Failed: " + failedCount);
				}
			}
			finally {
				getInvestmentInstructionService().clearInvestmentInstructionRegeneratingFlag(category, date);
			}
		}
	}


	@Transactional
	protected void saveInvestmentInstructionListImpl(Collection<InvestmentInstruction> instructionList) {
		for (InvestmentInstruction instruction : CollectionUtils.getIterable(instructionList)) {
			// Saving the Instruction with No Items will automatically delete the instruction (observers)
			getInvestmentInstructionService().saveInvestmentInstruction(instruction, true);
		}
	}


	///////////////////////////////////////////////////
	///////       Instruction Selections       ////////
	///////////////////////////////////////////////////


	@Override
	public List<InvestmentInstructionSelection> getInvestmentInstructionSelectionMissingList(InvestmentInstructionSelectionSearchForm searchForm) {
		InvestmentInstructionCategory category = getInvestmentInstructionSetupService().getInvestmentInstructionCategory(searchForm.getCategoryId());
		Date date = searchForm.getDate();
		if (date == null) {
			date = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);
		}
		InvestmentInstructionSelector<?> selector = (InvestmentInstructionSelector<?>) BeanUtils.newInstance(CoreClassUtils.getClass(category.getSelectorBeanType().getClassName()));
		getApplicationContextService().autowireBean(selector);
		SearchFormUtils.convertSearchRestrictionList(searchForm);
		return selector.getInstructionSelectionMissingList(category, date, searchForm);
	}


	@Override
	public List<InvestmentInstructionSelection> getInvestmentInstructionSelectionList(InvestmentInstructionDefinition definition, Date date) {
		InvestmentInstructionSelector<?> selector = (InvestmentInstructionSelector<?>) getSystemBeanService().getBeanInstance(definition.getSelectorBean());
		List<InvestmentInstructionSelection> selections = selector.getInstructionSelectionList(definition, date);

		if (!CollectionUtils.isEmpty(selections) && definition.getItemGroupPopulatorBean() != null) {
			InvestmentInstructionItemGroupPopulator groupPopulator = (InvestmentInstructionItemGroupPopulator) getSystemBeanService().getBeanInstance(definition.getItemGroupPopulatorBean());
			Map<String, InvestmentInstructionItemGroup> groupMap = new HashMap<>();
			for (InvestmentInstructionSelection selection : selections) {
				String groupName = groupPopulator.populateInvestmentInstructionItemGroup(selection.getFkEntity());
				if (!StringUtils.isEmpty(groupName)) {
					if (groupMap.containsKey(groupName)) {
						selection.setItemGroup(groupMap.get(groupName));
					}
					else {
						InvestmentInstructionItemGroup itemGroup = getInvestmentInstructionGroupService().getInvestmentInstructionItemGroupByName(groupName, true);
						groupMap.put(itemGroup.getName(), itemGroup);
						selection.setItemGroup(itemGroup);
					}
				}
			}
		}
		return BeanUtils.sortWithFunction(selections, selection -> (selection.getRecipientCompany() == null ? null : selection.getRecipientCompany().getId()), true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentInstructionSetupService getInvestmentInstructionSetupService() {
		return this.investmentInstructionSetupService;
	}


	public void setInvestmentInstructionSetupService(InvestmentInstructionSetupService investmentInstructionSetupService) {
		this.investmentInstructionSetupService = investmentInstructionSetupService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public InvestmentInstructionStatusService getInvestmentInstructionStatusService() {
		return this.investmentInstructionStatusService;
	}


	public void setInvestmentInstructionStatusService(InvestmentInstructionStatusService investmentInstructionStatusService) {
		this.investmentInstructionStatusService = investmentInstructionStatusService;
	}


	public InvestmentInstructionGroupService getInvestmentInstructionGroupService() {
		return this.investmentInstructionGroupService;
	}


	public void setInvestmentInstructionGroupService(InvestmentInstructionGroupService investmentInstructionGroupService) {
		this.investmentInstructionGroupService = investmentInstructionGroupService;
	}
}
