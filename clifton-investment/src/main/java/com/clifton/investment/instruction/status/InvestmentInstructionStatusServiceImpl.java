package com.clifton.investment.instruction.status;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author theodorez
 */
@Service
public class InvestmentInstructionStatusServiceImpl implements InvestmentInstructionStatusService {

	private AdvancedUpdatableDAO<InvestmentInstructionStatus, Criteria> investmentInstructionStatusDAO;

	private DaoNamedEntityCache<InvestmentInstructionStatus> investmentInstructionStatusCache;


	///////////////////////////////////////////////////////////////
	///////////             Status                       //////////
	///////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionStatus getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames name) {
		ValidationUtils.assertNotNull(name, "getInvestmentInstructionStatusByName requires a name parameter");

		return getInvestmentInstructionStatusCache().getBeanForKeyValueStrict(getInvestmentInstructionStatusDAO(), name.name());
	}


	@Override
	public List<InvestmentInstructionStatus> getInvestmentInstructionStatusList() {
		return getInvestmentInstructionStatusDAO().findAll();
	}

	///////////////////////////////////////////////////////////////
	//////////          Getter & Setter Methods          //////////
	///////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstructionStatus, Criteria> getInvestmentInstructionStatusDAO() {
		return this.investmentInstructionStatusDAO;
	}


	public void setInvestmentInstructionStatusDAO(AdvancedUpdatableDAO<InvestmentInstructionStatus, Criteria> investmentInstructionStatusDAO) {
		this.investmentInstructionStatusDAO = investmentInstructionStatusDAO;
	}


	public DaoNamedEntityCache<InvestmentInstructionStatus> getInvestmentInstructionStatusCache() {
		return this.investmentInstructionStatusCache;
	}


	public void setInvestmentInstructionStatusCache(DaoNamedEntityCache<InvestmentInstructionStatus> investmentInstructionStatusCache) {
		this.investmentInstructionStatusCache = investmentInstructionStatusCache;
	}
}
