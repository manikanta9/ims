package com.clifton.investment.instruction.run.destination;

import com.clifton.business.contact.BusinessContact;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;


public interface InvestmentInstructionDestinationUtilHandler {

	/**
	 * Gets the "sender" contact (i.e. sent on behalf of) from an Instruction Run
	 */
	public BusinessContact getSenderContact(InvestmentInstructionRun run);


	/**
	 * Gets the "from" contact (i.e. who the email comes from) from an Instruction Run.
	 * <p>
	 * If no from contact can be found, will return the default contact
	 */
	public BusinessContact getFromContact(InvestmentInstructionRun run, BusinessContact defaultContact);
}
