package com.clifton.investment.instruction.report;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;
import com.clifton.report.params.ReportConfigParameter;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedHashMap;


/**
 * The <code>InvestmentInstructionReportServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class InvestmentInstructionReportServiceImpl implements InvestmentInstructionReportService {

	private InvestmentInstructionSetupService investmentInstructionSetupService;

	private ReportExportService reportExportService;
	private SecurityUserService securityUserService;


	///////////////////////////////////////////////////////////////
	/////////             Instruction Reports             /////////
	///////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadInvestmentInstructionReport(int instructionId) {
		return downloadInvestmentInstructionReportList(new Integer[]{instructionId}, null);
	}


	@Override
	public FileWrapper downloadInvestmentInstructionReportList(Integer[] instructionIds, Integer instructionStatusId) {
		LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
		if (instructionStatusId != null) {
			parameterMap.put("InstructionStatusID", instructionStatusId);
		}
		ReportExportConfiguration exportConfig = new ReportExportConfiguration(null, parameterMap, true, false);
		ReportConfigParameter param = new ReportConfigParameter();
		param.setName("investmentInstructionIds");
		param.setValue("" + ArrayUtils.toString(instructionIds));
		exportConfig.setAdditionalReportParameter(param);
		return getReportExportService().downloadReportFile(exportConfig);
	}


	@Override
	public FileWrapper downloadInvestmentInstructionItemReport(int instructionItemId) {
		return downloadInvestmentInstructionItemReportList(new Integer[]{instructionItemId}, null);
	}


	@Override
	public FileWrapper downloadInvestmentInstructionItemReportList(Integer[] instructionItemIds, Integer instructionStatusId) {
		LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
		if (instructionStatusId != null) {
			parameterMap.put("InstructionStatusID", instructionStatusId);
		}
		ReportExportConfiguration exportConfig = new ReportExportConfiguration(null, parameterMap, true, false);
		ReportConfigParameter param = new ReportConfigParameter();
		param.setName("investmentInstructionItemIds");
		param.setValue("" + ArrayUtils.toString(instructionItemIds));
		exportConfig.setAdditionalReportParameter(param);
		return getReportExportService().downloadReportFile(exportConfig);
	}


	@Override
	public FileWrapper downloadInvestmentInstructionItemReportPreview(int instructionDefinitionId, int fkFieldId, Date instructionDate, int recipientCompanyId) {
		InvestmentInstructionDefinition def = getInvestmentInstructionSetupService().getInvestmentInstructionDefinition(instructionDefinitionId);
		ValidationUtils.assertNotNull(def.getReport(), "Definition [" + def.getName() + "] is missing a report selection.");

		// Get which report to run
		Integer reportId = def.getReport().getId();
		LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();

		parameterMap.put("RecipientCompanyID", recipientCompanyId);
		parameterMap.put("InstructionDefinitionID", instructionDefinitionId);
		parameterMap.put("FKFieldID", fkFieldId);
		parameterMap.put("InstructionDate", DateUtils.fromDateShort(instructionDate));
		parameterMap.put("RunAsUserID", getSecurityUserService().getSecurityUserCurrent().getId());
		parameterMap.put("HidePageNumbers", true);
		return getReportExportService().getReportPDFFile(new ReportExportConfiguration(reportId, parameterMap, true, true));
	}


	///////////////////////////////////////////////////////////////
	//////////          Getter & Setter Methods          //////////
	///////////////////////////////////////////////////////////////


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public InvestmentInstructionSetupService getInvestmentInstructionSetupService() {
		return this.investmentInstructionSetupService;
	}


	public void setInvestmentInstructionSetupService(InvestmentInstructionSetupService investmentInstructionSetupService) {
		this.investmentInstructionSetupService = investmentInstructionSetupService;
	}
}
