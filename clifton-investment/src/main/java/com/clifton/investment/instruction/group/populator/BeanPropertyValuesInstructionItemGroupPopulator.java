package com.clifton.investment.instruction.group.populator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.schema.column.SystemColumnCustomAware;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.util.List;


/**
 * The <code>BeanPropertyValuesInstructionItemGroupPopulator</code> is a basic implementation of the item group populator that allows grouping by bean property values
 * of the entity.  Custom Field values are supported through a particular syntax.  Each grouping is associated with reports that support that grouping:
 *
 * @author manderson
 */
public class BeanPropertyValuesInstructionItemGroupPopulator implements InvestmentInstructionItemGroupPopulator {


	/**
	 * Syntax to indicate retrieval of a custom field value rather than a bean property value.
	 * Should be followed by:  :System Column Group Name:ColumnName
	 * Example: Accounting Event Journal Detail - include Security's Coupon Frequency Custom Field Value
	 * accountingTransaction.investmentSecurity-CustomField:Security Custom Fields:Coupon Frequency
	 */
	private static final String CUSTOM_FIELD_PREFIX = "-CustomField:";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<Integer> reportIds;

	private List<String> beanPropertyNames;

	/**
	 * When generating the grouping, use this value to separate each value in the result
	 * i.e. clientAccount.label,holdingAccount.number using separator of - would look like:
	 * 012345: Test Client Account-HA1234
	 */
	private String propertyValueSeparator = "-";


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String populateInvestmentInstructionItemGroup(IdentityObject entity) {
		StringBuilder groupName = new StringBuilder(16);
		for (int i = 0; i < CollectionUtils.getSize(getBeanPropertyNames()); i++) {
			String beanPropertyName = StringUtils.trim(getBeanPropertyNames().get(i));
			appendBeanPropertyValue(entity, beanPropertyName, groupName);
			if (i != getBeanPropertyNames().size() - 1) {
				groupName.append(getPropertyValueSeparator());
			}
		}
		return groupName.toString();
	}


	private void appendBeanPropertyValue(IdentityObject entity, String beanPropertyName, StringBuilder groupName) {
		Object result = null;
		if (!StringUtils.isEmpty(beanPropertyName)) {
			if (beanPropertyName.contains(CUSTOM_FIELD_PREFIX)) {
				String[] properties = beanPropertyName.split(CUSTOM_FIELD_PREFIX);
				try {
					String customAwareBeanPropertyPath = properties[0];
					String columnGroupName = properties[1].split(":")[0];
					String columnName = properties[1].split(":")[1];
					result = getCustomFieldValue(entity, customAwareBeanPropertyPath, columnGroupName, columnName);
				}
				catch (Exception e) {
					throw new ValidationException("Error converting beanPropertyName: " + beanPropertyName + " to custom field value. Expect value in format of entityPath" + CUSTOM_FIELD_PREFIX + "Column Group Name:Column Name", e);
				}
			}
			else {
				// Allow Blank Values (i.e. Executing Broker may not be populated on the trade during generation, but would be prior to the instruction being ready for sending)
				result = BeanUtils.getPropertyValue(entity, beanPropertyName, true);
			}
		}
		if (result != null) {
			if (result instanceof Boolean) {
				if (Boolean.TRUE.equals(result)) {
					int lastPropertyIndex = beanPropertyName.lastIndexOf('.');
					if (lastPropertyIndex == -1) {
						groupName.append(StringUtils.splitWords(beanPropertyName));
					}
					else {
						groupName.append(StringUtils.splitWords(beanPropertyName.substring(lastPropertyIndex + 1)));
					}
				}
			}
			else {
				groupName.append(result);
			}
		}
	}


	private Object getCustomFieldValue(IdentityObject entity, String customAwareBeanPropertyPath, String columnGroupName, String columnName) {
		Object beanEntity = BeanUtils.getPropertyValue(entity, customAwareBeanPropertyPath);
		if (beanEntity != null) {
			if (!(beanEntity instanceof SystemColumnCustomAware)) {
				throw new ValidationException("Invalid custom field value bean property name: [" + customAwareBeanPropertyPath + "].  Object found does not support custom fields.");
			}
			Object result = getSystemColumnValueHandler().getSystemColumnValueForEntity((SystemColumnCustomAware) beanEntity, columnGroupName, columnName, true);
			if (result instanceof Boolean) {
				if (Boolean.TRUE.equals(result)) {
					return columnName;
				}
			}
			else {
				return result;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public List<Integer> getReportIds() {
		return this.reportIds;
	}


	public void setReportIds(List<Integer> reportIds) {
		this.reportIds = reportIds;
	}


	public List<String> getBeanPropertyNames() {
		return this.beanPropertyNames;
	}


	public void setBeanPropertyNames(List<String> beanPropertyNames) {
		this.beanPropertyNames = beanPropertyNames;
	}


	public String getPropertyValueSeparator() {
		return this.propertyValueSeparator;
	}


	public void setPropertyValueSeparator(String propertyValueSeparator) {
		this.propertyValueSeparator = propertyValueSeparator;
	}
}
