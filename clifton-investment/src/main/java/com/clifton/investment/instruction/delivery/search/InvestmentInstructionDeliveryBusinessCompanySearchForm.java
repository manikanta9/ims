package com.clifton.investment.instruction.delivery.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * <code>InvestmentInstructionDeliveryBusinessCompanySearchForm</code>
 */
public class InvestmentInstructionDeliveryBusinessCompanySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer investmentInstructionDelivery;

	@SearchField(searchField = "referenceTwo.id")
	private Integer businessCompanyId;

	@SearchField(searchFieldPath = "referenceOne", searchField = "deliveryCompany.id")
	private Integer deliveryCompanyId;

	@SearchField(searchFieldPath = "referenceOne", searchField = "deliveryAccountNumber")
	private String deliveryAccountNumber;

	@SearchField(searchFieldPath = "referenceOne", searchField = "deliveryAccountName")
	private String deliveryAccountName;

	@SearchField(searchFieldPath = "referenceOne", searchField = "deliveryABA")
	private String deliveryABA;

	@SearchField(searchFieldPath = "referenceOne", searchField = "deliverySwiftCode")
	private String deliverySwiftCode;

	@SearchField(searchFieldPath = "referenceOne", searchField = "deliveryForFurtherCredit")
	private String deliveryForFurtherCredit;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "type.id")
	private Short businessCompanyTypeId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "name")
	private String businessCompanyName;

	@SearchField(searchField = "deliveryCurrency.id")
	private Integer deliveryCurrencyId;

	@SearchField(searchField = "investmentInstructionDeliveryType.id")
	private Short typeId;


	public Integer getInvestmentInstructionDelivery() {
		return this.investmentInstructionDelivery;
	}


	public void setInvestmentInstructionDelivery(Integer investmentInstructionDelivery) {
		this.investmentInstructionDelivery = investmentInstructionDelivery;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}


	public Integer getDeliveryCompanyId() {
		return this.deliveryCompanyId;
	}


	public void setDeliveryCompanyId(Integer deliveryCompanyId) {
		this.deliveryCompanyId = deliveryCompanyId;
	}


	public String getDeliveryAccountNumber() {
		return this.deliveryAccountNumber;
	}


	public void setDeliveryAccountNumber(String deliveryAccountNumber) {
		this.deliveryAccountNumber = deliveryAccountNumber;
	}


	public String getDeliveryAccountName() {
		return this.deliveryAccountName;
	}


	public void setDeliveryAccountName(String deliveryAccountName) {
		this.deliveryAccountName = deliveryAccountName;
	}


	public String getDeliveryABA() {
		return this.deliveryABA;
	}


	public void setDeliveryABA(String deliveryABA) {
		this.deliveryABA = deliveryABA;
	}


	public String getDeliverySwiftCode() {
		return this.deliverySwiftCode;
	}


	public void setDeliverySwiftCode(String deliverySwiftCode) {
		this.deliverySwiftCode = deliverySwiftCode;
	}


	public String getDeliveryForFurtherCredit() {
		return this.deliveryForFurtherCredit;
	}


	public void setDeliveryForFurtherCredit(String deliveryForFurtherCredit) {
		this.deliveryForFurtherCredit = deliveryForFurtherCredit;
	}


	public Short getBusinessCompanyTypeId() {
		return this.businessCompanyTypeId;
	}


	public void setBusinessCompanyTypeId(Short businessCompanyTypeId) {
		this.businessCompanyTypeId = businessCompanyTypeId;
	}


	public String getBusinessCompanyName() {
		return this.businessCompanyName;
	}


	public void setBusinessCompanyName(String businessCompanyName) {
		this.businessCompanyName = businessCompanyName;
	}


	public Integer getDeliveryCurrencyId() {
		return this.deliveryCurrencyId;
	}


	public void setDeliveryCurrencyId(Integer deliveryCurrencyId) {
		this.deliveryCurrencyId = deliveryCurrencyId;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}
}
