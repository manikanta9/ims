package com.clifton.investment.instruction.run.runner;

import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.report.InvestmentInstructionReportService;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;
import com.clifton.investment.instruction.run.messaging.InvestmentInstructionRunMessagingConverterService;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * Creates an InstructionRun and sends it to the integration server for transmission.
 *
 * @author theodorez
 */
public class InvestmentInstructionRunner extends AbstractStatusAwareRunner {

	private static final String RUNNER_TYPE = "INSTRUCTION";

	/**
	 * Expression to remove any invalid
	 */
	private static final String INVALID_FILENAME_PATTERN = ",";
	/**
	 * The instruction the run will be tied to. A run may only ever be tied to one instruction.
	 */
	private final InvestmentInstruction instruction;
	/**
	 * The created runs will be marked as detached.
	 */
	private final boolean createDetachedRun;
	/**
	 * The Instruction Items that are to be sent in the run.
	 */
	private List<InvestmentInstructionItem> items;
	/**
	 * A map of contacts for each run type.
	 */
	private MultiValueMap<InvestmentInstructionRunTypes, InvestmentInstructionContact> instructionContactMap;
	/**
	 * Where files are going to be stored when they are sent to integration
	 */
	private String rootDirectory;

	/**
	 * A list of detached runners to run after the item status has been set.
	 */
	private List<InvestmentInstructionRunner> detachedRunnerList;


	private InvestmentInstructionService investmentInstructionService;
	private InvestmentInstructionRunService investmentInstructionRunService;
	private InvestmentInstructionSetupService investmentInstructionSetupService;
	private InvestmentInstructionStatusService investmentInstructionStatusService;
	private InvestmentInstructionReportService investmentInstructionReportService;
	private InvestmentInstructionRunMessagingConverterService investmentInstructionRunMessagingConverterService;

	private TemplateConverter templateConverter;

	/**
	 * The sender used for putting the message on a queue which will then be picked up by the Integration service to send the export.
	 */
	private AsynchronousMessageHandler exportClientAsynchronousMessageHandler;

	///////////////////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionRunner(Date runDate, List<InvestmentInstructionItem> items, MultiValueMap<InvestmentInstructionRunTypes, InvestmentInstructionContact> instructionContactMap, boolean createDetachedRun) {
		//Note: the previous implementation used the run type as part of the run ID, this was due to a runner being created for each type.
		// This is no longer needed because the runner creates the runs with the appropriate types.
		super(RUNNER_TYPE,
				items.get(0).getInstruction().getId() + (createDetachedRun ? "_DETACHED" : ""),
				runDate);
		this.instruction = items.get(0).getInstruction();
		this.items = items;
		this.instructionContactMap = instructionContactMap;
		this.createDetachedRun = createDetachedRun;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public void run() {
		List<InvestmentInstructionRun> runList = new ArrayList<>();
		Map<Integer, ExportMessagingMessage> messageMap = new HashMap<>();
		HashMap<InvestmentInstructionRun, Throwable> errorMap = new HashMap<>();

		try {
			if (!getItems().isEmpty()) {
				setItemSendingStatus(getItems());

				// run any detached runners
				runDetachedRunnerList();

				FileWrapper instructionContent = generateInstructionContent();

				//Loop through all run types and see if there are contacts
				for (Map.Entry<InvestmentInstructionRunTypes, Collection<InvestmentInstructionContact>> runTypeContactEntry : this.instructionContactMap.entrySet()) {
					List<InvestmentInstructionContact> contactList = new ArrayList<>(runTypeContactEntry.getValue());
					if (!CollectionUtils.isEmpty(contactList)) {
						//Make the run and execute it
						initializeAndExecuteRun(runTypeContactEntry.getKey(), runList, messageMap, errorMap, contactList);
					}
				}
				if (errorMap.isEmpty()) {
					// TODO: a failure in the next method will not set the run to error status
					sendRunMessages(runList, messageMap, instructionContent);
				}
				else {
					handleErrorStatuses(runList, errorMap);
				}
			}
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Error occurred while running InvestmentInstructionRunner: " + e.getMessage(), e);
			setItemErrorStatus(getItems());
			refreshInvestmentInstructionStatus(this.instruction);
		}
		finally {
			for (InvestmentInstructionRun run : CollectionUtils.getIterable(runList)) {
				run.setEndDate(new Date());
				getInvestmentInstructionRunService().saveInvestmentInstructionRun(run);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	protected void runDetachedRunnerList() {
		for (InvestmentInstructionRunner detachedRunner : CollectionUtils.getIterable(getDetachedRunnerList())) {
			detachedRunner.run();
		}
	}


	protected void initializeAndExecuteRun(InvestmentInstructionRunTypes runType, List<InvestmentInstructionRun> runList, Map<Integer, ExportMessagingMessage> messageMap, HashMap<InvestmentInstructionRun, Throwable> errorMap, List<InvestmentInstructionContact> contactList) {
		InvestmentInstructionRun run = getInitializedRun(runType);
		try {
			runList.add(run);
			//populate the run items
			initInstructionRunItems(run, getItems());
			getStatus().setMessage("Sending instruction run with id = " + run.getId() + " to ActiveMQ queue on " + (getRunDate() == null ? "" : DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_SHORT)) + "");
			//Create the export message for the run (DESTINATION ONLY)
			ExportMessagingMessage messagingMessage = getInvestmentInstructionRunMessagingConverterService().toExportMessagingMessage(run, contactList);
			messageMap.put(run.getId(), messagingMessage);
		}
		catch (Throwable e) {
			//Thrown when the error map is processed
			errorMap.put(run, e);
		}
	}


	@Transactional
	protected void sendRunMessages(List<InvestmentInstructionRun> runs, Map<Integer, ExportMessagingMessage> messages, FileWrapper instructionContent) {
		ValidationUtils.assertEquals(runs.size(), messages.size(), "Number of runs must equal number of generated export messages.");
		//Set the status to SENT
		setItemSentStatus(getItems());

		for (InvestmentInstructionRun run : runs) {
			ExportMessagingMessage message = messages.get(run.getId());
			updateRunHistory(message, run);
			//Save the instruction to reevaluate the status
			refreshInvestmentInstructionStatus(run.getInvestmentInstruction());
			getStatus().setMessage("Sent Instruction Run with id = " + run.getId() + " to ActiveMQ on '" + (getRunDate() == null ? "" : DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_SHORT)));
		}

		InvestmentInstructionRun run = CollectionUtils.getFirstElement(runs);
		if (Objects.nonNull(run)) {
			FileWrapper finalContent = setInstructionContentName(run.getInvestmentInstruction(), instructionContent, new ArrayList<>(CollectionUtils.getKeys(messages)));
			//Put the message list on the queue
			getExportClientAsynchronousMessageHandler().send(getInvestmentInstructionRunMessagingConverterService().toExportMessagingMessageList(new ArrayList<>(messages.values()), finalContent, getRootDirectory()));
		}
	}


	private FileWrapper generateInstructionContent() {
		//get all the ids of the items
		Integer[] itemIds = new Integer[getItems().size()];
		for (int counter = 0; counter < getItems().size(); counter++) {
			itemIds[counter] = getItems().get(counter).getId();
		}
		Integer instructionStatusId = getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.SENDING).getId().intValue();
		//generate the report for the items (combined into one file)
		FileWrapper output = getInvestmentInstructionReportService().downloadInvestmentInstructionItemReportList(itemIds, instructionStatusId);
		ValidationUtils.assertNotNull(output, "No instruction item reports found for items: " + getItems());
		return output;
	}


	private FileWrapper setInstructionContentName(InvestmentInstruction instruction, FileWrapper instructionContent, List<Integer> runIds) {
		StringBuilder newFileNameSb = new StringBuilder();
		if (!CollectionUtils.isEmpty(runIds)) {
			String template = instruction.getDefinition().getFilenameTemplate();
			newFileNameSb.append(DateUtils.fromDate(instruction.getInstructionDate(), "yyyy.MM.dd"));
			newFileNameSb.append("_");
			if (!StringUtils.isEmpty(template)) {
				newFileNameSb.append(StringUtils.removeAll(FileUtils.replaceInvalidCharacters(getTemplateConverter().convert(prepareTemplateConfig(template, instruction)), "-"), INVALID_FILENAME_PATTERN));
			}
			else {
				newFileNameSb.append(StringUtils.removeAll(FileUtils.replaceInvalidCharacters(this.instruction.getRecipientCompany().getName(), "-"), INVALID_FILENAME_PATTERN));
			}
			newFileNameSb.append("_");
			newFileNameSb.append(StringUtils.collectionToDelimitedString(runIds, "-"));
			newFileNameSb.append(".pdf");

			FilePath newFile = FilePath.forPath(FileUtils.combinePaths(FileUtils.JAVA_TEMP_DIRECTORY, newFileNameSb.toString()));
			try {
				FileUtils.moveFile(instructionContent.getFile(), newFile);
			}
			catch (Exception e) {
				LogUtils.error(getClass(), "Failed to copy file [" + newFile.getPath() + "].", e);
			}
			return new FileWrapper(newFile, newFileNameSb.toString(), true);
		}
		return null;
	}


	private TemplateConfig prepareTemplateConfig(String template, InvestmentInstruction instruction) {
		InvestmentInstructionDefinition definition = instruction.getDefinition();
		TemplateConfig config = new TemplateConfig(template);
		config.addBeanToContext("INSTRUCTION", instruction);
		config.addBeanToContext("CATEGORY", definition.getCategory());
		config.addBeanToContext("DEFINITION", definition);
		config.addBeanToContext("DEFINITION_NAME", definition.getName());
		config.addBeanToContext("REPORT", definition.getReport());
		config.addBeanToContext("REPORT_NAME", definition.getReport().getName());
		config.addBeanToContext("RECIPIENT_COMPANY", instruction.getRecipientCompany());
		config.addBeanToContext("RECIPIENT_COMPANY_NAME", instruction.getRecipientCompany().getName());
		return config;
	}


	protected void handleErrorStatuses(List<InvestmentInstructionRun> runs, Map<InvestmentInstructionRun, Throwable> errorMap) {
		//The first run that has an error
		InvestmentInstructionRun firstSiblingErrorRun = errorMap.entrySet().iterator().next().getKey();

		for (InvestmentInstructionRun run : runs) {
			Throwable error = errorMap.get(run);
			if (error == null) {
				error = new Throwable("Error found in sibling run. ID: " + firstSiblingErrorRun.getId() + " TYPE: " + firstSiblingErrorRun.getRunType() + " ERROR: " + errorMap.get(firstSiblingErrorRun));
			}
			handleErrorStatus(run, error, getItems());
		}
	}


	protected void handleErrorStatus(InvestmentInstructionRun run, Throwable e, List<InvestmentInstructionItem> items) {

		//set items to error status
		getStatus().addError(e.getMessage());
		if (!CollectionUtils.isEmpty(items)) {
			setItemErrorStatus(items);
		}

		String message = StringUtils.formatStringUpToNCharsWithDots(e.getMessage(), 3997, true);
		run.setRunStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR));
		run.setHistoryDescription("Error occurred when sending instruction items: " + message);
		run.setEndDate(new Date());
		getInvestmentInstructionRunService().saveInvestmentInstructionRun(run);
		refreshInvestmentInstructionStatus(this.instruction);
		LogUtils.error(getClass(), "Error running instructions run " + run, e);
	}


	protected void refreshInvestmentInstructionStatus(InvestmentInstruction bean) {
		if (!isCreateDetachedRun()) {
			getInvestmentInstructionService().refreshInvestmentInstructionStatus(bean, null);
		}
	}


	protected void updateRunHistory(ExportMessagingMessage message, InvestmentInstructionRun run) {
		StringBuilder builder = new StringBuilder();
		builder.append("Message generated for ");
		builder.append(message.getDestinationList().size());
		builder.append(" destinations.");
		run.setHistoryDescription(builder.toString());
		getInvestmentInstructionRunService().saveInvestmentInstructionRun(run);
	}


	protected InvestmentInstructionRun getInitializedRun(InvestmentInstructionRunTypes runType) {
		InvestmentInstructionRun run = new InvestmentInstructionRun();
		run.setRunStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.OPEN));
		run.setInvestmentInstruction(this.instruction);
		run.setScheduledDate(getRunDate());
		run.setStartDate(new Date());
		run.setRunType(runType);
		run.setDetached(isCreateDetachedRun());
		getInvestmentInstructionRunService().saveInvestmentInstructionRun(run);
		return run;
	}


	protected void initInstructionRunItems(final InvestmentInstructionRun run, final List<InvestmentInstructionItem> items) {
		CollectionUtils.asNonNullList(items).stream()
				.map(i -> {
					InvestmentInstructionRunInstructionItem runItem = new InvestmentInstructionRunInstructionItem();
					runItem.setReferenceOne(run);
					runItem.setReferenceTwo(i);
					return runItem;
				})
				.forEach(ri -> getInvestmentInstructionRunService().saveInvestmentInstructionRunInstructionItem(ri));
	}


	protected void setItemErrorStatus(List<InvestmentInstructionItem> items) {
		if (!isCreateDetachedRun()) {
			InvestmentInstructionStatusNames itemStatus = InvestmentInstructionStatusNames.ERROR;
			setItemStatus(items, itemStatus);
		}
	}


	protected void setItemSendingStatus(List<InvestmentInstructionItem> items) {
		if (!isCreateDetachedRun()) {
			//set the status for the items in the run to "SENDING" - so that they will not be sent more than once
			InvestmentInstructionStatusNames itemStatus = InvestmentInstructionStatusNames.SENDING;
			setItemStatus(items, itemStatus);
		}
	}


	protected void setItemSentStatus(List<InvestmentInstructionItem> items) {
		if (!isCreateDetachedRun()) {
			//set the status for the items in the run to "SENT"
			InvestmentInstructionStatusNames itemStatus = InvestmentInstructionStatusNames.SENT;
			setItemStatus(items, itemStatus);
		}
	}


	@Transactional
	protected void setItemStatus(List<InvestmentInstructionItem> items, InvestmentInstructionStatusNames status) {
		InvestmentInstructionStatus itemStatus = getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(status);
		for (int i = 0; i < items.size(); i++) {
			InvestmentInstructionItem item = items.get(i);
			item.setStatus(getInstructionStatus(item, itemStatus));
			//save the item (to set the status)
			items.set(i, getInvestmentInstructionService().saveInvestmentInstructionItem(item));
		}
	}


	/**
	 * Get the potentially remapped cancellation status for the provided investment instruction item and status
	 */
	protected InvestmentInstructionStatus getInstructionStatus(final InvestmentInstructionItem item, final InvestmentInstructionStatus intendedStatus) {
		InvestmentInstructionStatusNames mappedStatusName = InvestmentInstructionStatusNames.getCancelledStatus(item, intendedStatus.getInvestmentInstructionStatusName());
		return mappedStatusName != intendedStatus.getInvestmentInstructionStatusName()
				? getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(mappedStatusName) : intendedStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionRunService getInvestmentInstructionRunService() {
		return this.investmentInstructionRunService;
	}


	public void setInvestmentInstructionRunService(InvestmentInstructionRunService investmentInstructionRunService) {
		this.investmentInstructionRunService = investmentInstructionRunService;
	}


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public InvestmentInstructionSetupService getInvestmentInstructionSetupService() {
		return this.investmentInstructionSetupService;
	}


	public void setInvestmentInstructionSetupService(InvestmentInstructionSetupService investmentInstructionSetupService) {
		this.investmentInstructionSetupService = investmentInstructionSetupService;
	}


	public InvestmentInstructionStatusService getInvestmentInstructionStatusService() {
		return this.investmentInstructionStatusService;
	}


	public void setInvestmentInstructionStatusService(InvestmentInstructionStatusService investmentInstructionStatusService) {
		this.investmentInstructionStatusService = investmentInstructionStatusService;
	}


	public InvestmentInstructionReportService getInvestmentInstructionReportService() {
		return this.investmentInstructionReportService;
	}


	public void setInvestmentInstructionReportService(InvestmentInstructionReportService investmentInstructionReportService) {
		this.investmentInstructionReportService = investmentInstructionReportService;
	}


	public AsynchronousMessageHandler getExportClientAsynchronousMessageHandler() {
		return this.exportClientAsynchronousMessageHandler;
	}


	public void setExportClientAsynchronousMessageHandler(AsynchronousMessageHandler exportClientAsynchronousMessageHandler) {
		this.exportClientAsynchronousMessageHandler = exportClientAsynchronousMessageHandler;
	}


	public InvestmentInstructionRunMessagingConverterService getInvestmentInstructionRunMessagingConverterService() {
		return this.investmentInstructionRunMessagingConverterService;
	}


	public void setInvestmentInstructionRunMessagingConverterService(InvestmentInstructionRunMessagingConverterService investmentInstructionRunMessagingConverterService) {
		this.investmentInstructionRunMessagingConverterService = investmentInstructionRunMessagingConverterService;
	}


	public String getRootDirectory() {
		return this.rootDirectory;
	}


	public void setRootDirectory(String rootDirectory) {
		this.rootDirectory = rootDirectory;
	}


	public List<InvestmentInstructionItem> getItems() {
		return this.items;
	}


	protected void setItems(List<InvestmentInstructionItem> items) {
		this.items = items;
	}


	public boolean isCreateDetachedRun() {
		return this.createDetachedRun;
	}


	public List<InvestmentInstructionRunner> getDetachedRunnerList() {
		return this.detachedRunnerList;
	}


	public void setDetachedRunnerList(List<InvestmentInstructionRunner> detachedRunnerList) {
		this.detachedRunnerList = detachedRunnerList;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}
}
