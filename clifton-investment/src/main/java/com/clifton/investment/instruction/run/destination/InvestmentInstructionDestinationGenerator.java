package com.clifton.investment.instruction.run.destination;

import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;

import java.util.Map;


/**
 * Responsible for creating the proper export map of values for the automated sending of instructions
 */
public interface InvestmentInstructionDestinationGenerator {

	public Map<ExportMapKeys, String> generateDestination(InvestmentInstructionRun run);


	/**
	 * Returns the parent generator class, if it exists.
	 *
	 * @return the parent generator class
	 */
	public Class<?> getParentClass();


	/**
	 * @return the string to be used for the bean name
	 */
	public String getDestinationName();


	/**
	 * @return the string to be displayed on the UI
	 */
	public String getDestinationLabel();
}
