package com.clifton.investment.instruction.delivery;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


@CacheByName
public class InvestmentInstructionDeliveryType extends NamedEntity<Short> {

	private boolean defaultDeliveryType;


	public boolean isDefaultDeliveryType() {
		return this.defaultDeliveryType;
	}


	public void setDefaultDeliveryType(boolean defaultDeliveryType) {
		this.defaultDeliveryType = defaultDeliveryType;
	}
}
