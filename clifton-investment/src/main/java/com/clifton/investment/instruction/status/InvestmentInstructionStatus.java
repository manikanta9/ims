package com.clifton.investment.instruction.status;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The InvestmentInstructionStatus class represents a status of a specific instruction or instruction run.
 * Available statuses: {@link InvestmentInstructionStatusNames}
 *
 * @author theodorez
 */
@CacheByName
public class InvestmentInstructionStatus extends NamedEntity<Short> {

	//Used to specify the ordering so that the correct status for parent items (instructions, runs)
	//Can be determined
	private Short statusCode;

	//Bit Flags for different states
	private boolean completed;
	private boolean fail;
	private boolean sendable;
	private boolean cancel;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionStatusNames getInvestmentInstructionStatusName() {
		return InvestmentInstructionStatusNames.valueOf(getName());
	}


	public int compareStatusCode(InvestmentInstructionStatus status) {
		return getStatusCode() - status.getStatusCode();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getStatusCode() {
		return this.statusCode;
	}


	public void setStatusCode(Short statusCode) {
		this.statusCode = statusCode;
	}


	public boolean isCompleted() {
		return this.completed;
	}


	public void setCompleted(boolean completed) {
		this.completed = completed;
	}


	public boolean isFail() {
		return this.fail;
	}


	public void setFail(boolean fail) {
		this.fail = fail;
	}


	public boolean isSendable() {
		return this.sendable;
	}


	public void setSendable(boolean sendable) {
		this.sendable = sendable;
	}


	public boolean isCancel() {
		return this.cancel;
	}


	public void setCancel(boolean cancel) {
		this.cancel = cancel;
	}
}
