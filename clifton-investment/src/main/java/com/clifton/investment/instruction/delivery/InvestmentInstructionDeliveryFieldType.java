package com.clifton.investment.instruction.delivery;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


@CacheByName
public class InvestmentInstructionDeliveryFieldType extends NamedEntity<Short> {

	/**
	 * Determines whether the associated {@link InvestmentInstructionDeliveryField#value} is shown on reports.
	 */
	private boolean fieldVisibleOnReport;


	public boolean isFieldVisibleOnReport() {
		return this.fieldVisibleOnReport;
	}


	public void setFieldVisibleOnReport(boolean fieldVisibleOnReport) {
		this.fieldVisibleOnReport = fieldVisibleOnReport;
	}
}
