package com.clifton.investment.instruction.run.runner;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.InstructionService;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * @author mwacker
 */
public class InvestmentInstructionSwiftRunner<M extends InstructionMessage> extends InvestmentInstructionRunner {

	private InstructionService<M> instructionService;
	private List<InvestmentInstructionContact> emailContactList;

	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionSwiftRunner(Date runDate, List<InvestmentInstructionItem> items, MultiValueMap<InvestmentInstructionRunTypes, InvestmentInstructionContact> instructionContactMap, boolean createDetachedRun) {
		super(runDate, items, instructionContactMap, createDetachedRun);

		setItems(items);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public void run() {
		// update all item statuses to SENDING first, if anything fails the process will stop here
		setItemSendingStatus(getItems());

		// run the detached runs (for example the run the send emails when sending SWIFT messages)
		runDetachedRunnerList();

		for (InvestmentInstructionItem item : CollectionUtils.getIterable(getItems())) {
			InvestmentInstructionRun run = null;
			try {
				run = initializeRun(InvestmentInstructionRunTypes.SWIFT, item);
				item = sendSwiftMessages(item, run);
			}
			catch (Exception e) {
				handleErrorStatus(run, e, null);
				item = updateItemStatus(item, InvestmentInstructionStatusNames.ERROR);
				getInvestmentInstructionService().refreshInvestmentInstructionStatus(item.getInstruction(), null);
				LogUtils.error(getClass(), "Error occurred while running InvestmentInstructionRunner for item [" + item + "]:", e);
			}
			finally {
				if (run != null) {
					run.setEndDate(new Date());
					getInvestmentInstructionRunService().saveInvestmentInstructionRun(run);
				}
			}
		}
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	protected InvestmentInstructionItem sendSwiftMessages(InvestmentInstructionItem item, InvestmentInstructionRun run) {
		// send the SWIFT message
		InvestmentInstructionStatusNames status = InvestmentInstructionStatusNames.SENT;
		boolean sent;
		if (item.getStatus().getInvestmentInstructionStatusName() == InvestmentInstructionStatusNames.SENT_CANCEL) {
			sent = getInstructionService().cancelInstruction(item);
			if (!sent) {
				status = InvestmentInstructionStatusNames.CANCELED;
			}
		}
		else {
			sent = getInstructionService().sendInstruction(item);
			if (!sent) {
				status = InvestmentInstructionStatusNames.COMPLETED;
			}
		}

		//Set the status to SENT or CANCELED / COMPLETED
		item = updateItemStatus(item, status);
		updateRunHistory(run, item, sent);

		//Save the instruction to reevaluate the status
		getInvestmentInstructionService().refreshInvestmentInstructionStatus(run.getInvestmentInstruction(), null);
		if (sent) {
			getStatus().setMessage("Sent Instruction Run with id = " + run.getId() + " to ActiveMQ on " + (getRunDate() == null ? "" : DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_SHORT)));
		}
		else {
			getStatus().setMessage("Skipped sending Instruction Run with id = " + run.getId() + "' because no message was generated.");
		}

		return item;
	}


	protected InvestmentInstructionRun initializeRun(InvestmentInstructionRunTypes runType, InvestmentInstructionItem item) {
		InvestmentInstructionRun run = getInitializedRun(runType);

		//populate the run items
		initInstructionRunItems(run, CollectionUtils.createList(item));
		getStatus().setMessage("Sending instruction run with id = " + run.getId() + " to ActiveMQ queue on " + (getRunDate() == null ? "" : DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_SHORT)));

		return run;
	}


	protected void updateRunHistory(InvestmentInstructionRun run, InvestmentInstructionItem item, boolean sent) {
		StringBuilder builder = new StringBuilder();
		builder.append("SWIFT message for [");
		builder.append(item.getSystemTable().getName()).append(":").append(item.getFkFieldId());
		if (sent) {
			builder.append("] was sent.");
		}
		else {
			builder.append("] was skipped because no message was generated.  Could be due to a $0 transfer amount.");
		}
		run.setHistoryDescription(builder.toString());
		if (!sent) {
			run.setRunStatus(getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.COMPLETED));
		}
		getInvestmentInstructionRunService().saveInvestmentInstructionRun(run);
	}


	@Transactional
	protected InvestmentInstructionItem updateItemStatus(InvestmentInstructionItem item, InvestmentInstructionStatusNames itemStatusName) {
		InvestmentInstructionStatusNames mappedStatusName = InvestmentInstructionStatusNames.getCancelledStatus(item, itemStatusName);
		InvestmentInstructionStatus mappedStatus = getInvestmentInstructionStatusService().getInvestmentInstructionStatusByName(mappedStatusName);
		item.setStatus(mappedStatus);
		return getInvestmentInstructionService().saveInvestmentInstructionItem(item);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	public InstructionService<M> getInstructionService() {
		return this.instructionService;
	}


	public void setInstructionService(InstructionService<M> instructionService) {
		this.instructionService = instructionService;
	}


	public List<InvestmentInstructionContact> getEmailContactList() {
		return this.emailContactList;
	}


	public void setEmailContactList(List<InvestmentInstructionContact> emailContactList) {
		this.emailContactList = emailContactList;
	}
}
