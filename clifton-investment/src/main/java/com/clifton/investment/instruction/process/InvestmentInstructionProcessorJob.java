package com.clifton.investment.instruction.process;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionCategorySearchForm;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentInstructionGeneratorJob</code> class generates missing instructions that fall in
 * the specified date range.
 *
 * @author Mary Anderson
 */
public class InvestmentInstructionProcessorJob implements Task, StatusHolderObjectAware<Status> {

	public static final Integer DEFAULT_DAYS_BACK = 1;

	private Integer daysBack = DEFAULT_DAYS_BACK;

	private boolean includeInstructionsForToday;

	private InvestmentInstructionProcessor investmentInstructionProcessor;
	private InvestmentInstructionSetupService investmentInstructionSetupService;


	private StatusHolderObject<Status> statusHolder;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Date endDate = DateUtils.clearTime(new Date());
		Date startDate = DateUtils.addDays(endDate, -getDaysBack());
		if (!isIncludeInstructionsForToday()) {
			endDate = DateUtils.addDays(endDate, -1);
		}
		Date currentDate = startDate;

		List<InvestmentInstructionCategory> categoryList = getInvestmentInstructionSetupService().getInvestmentInstructionCategoryList(new InvestmentInstructionCategorySearchForm());

		Status status = this.statusHolder.getStatus();
		while (DateUtils.isDateBetween(currentDate, startDate, endDate, false)) {
			for (InvestmentInstructionCategory category : CollectionUtils.getIterable(categoryList)) {
				try {
					getInvestmentInstructionProcessor().processInvestmentInstructionList(category, currentDate, status);
				}
				catch (Throwable e) {
					String errorMessage = "Error processing instructions for Category [" + category.getName() + "] on " + DateUtils.fromDateShort(currentDate) + ": " + ExceptionUtils.getOriginalMessage(e);
					status.addError(errorMessage);
					LogUtils.errorOrInfo(getClass(), errorMessage, e);
				}
			}

			// Process Next Day
			currentDate = DateUtils.addDays(currentDate, 1);
		}

		status.setMessage("Finished Processing of " + CollectionUtils.getSize(categoryList) + " categories fromm " + DateUtils.fromDateShort(startDate) + " to " + DateUtils.fromDateShort(endDate));
		return status;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDaysBack() {
		return this.daysBack;
	}


	public void setDaysBack(Integer daysBack) {
		this.daysBack = daysBack;
	}


	public boolean isIncludeInstructionsForToday() {
		return this.includeInstructionsForToday;
	}


	public void setIncludeInstructionsForToday(boolean includeInstructionsForToday) {
		this.includeInstructionsForToday = includeInstructionsForToday;
	}


	public InvestmentInstructionSetupService getInvestmentInstructionSetupService() {
		return this.investmentInstructionSetupService;
	}


	public void setInvestmentInstructionSetupService(InvestmentInstructionSetupService investmentInstructionSetupService) {
		this.investmentInstructionSetupService = investmentInstructionSetupService;
	}


	public InvestmentInstructionProcessor getInvestmentInstructionProcessor() {
		return this.investmentInstructionProcessor;
	}


	public void setInvestmentInstructionProcessor(InvestmentInstructionProcessor investmentInstructionProcessor) {
		this.investmentInstructionProcessor = investmentInstructionProcessor;
	}
}
