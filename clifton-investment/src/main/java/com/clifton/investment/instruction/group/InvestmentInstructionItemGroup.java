package com.clifton.investment.instruction.group;

import com.clifton.core.beans.BaseSimpleEntity;


/**
 * InvestmentInstructionItemGroup is used to group items together as they would be grouped in the reports to prevent duplicate report generation
 * For example, we may group items in a report by client account and holding account.  The reason for the grouping is often because the settlement amount is the net amount
 * These groupings are generated during processing and are a generic String so they can be re-used across instructions and even across categories.
 *
 * @author manderson
 */
public class InvestmentInstructionItemGroup extends BaseSimpleEntity<Integer> {

	private String name;


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
