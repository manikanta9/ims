package com.clifton.investment.instruction.delivery;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryBusinessCompanySearchForm;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryFieldSearchForm;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryFieldTypeSearchForm;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliverySearchForm;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryTypeSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;


@Service
public class InvestmentInstructionDeliveryServiceImpl implements InvestmentInstructionDeliveryService {

	public static final String FIELD_TYPE_INTERESTED_PARTIES = "Interested Parties";

	private AdvancedUpdatableDAO<InvestmentInstructionDelivery, Criteria> investmentInstructionDeliveryDAO;
	private AdvancedUpdatableDAO<InvestmentInstructionDeliveryType, Criteria> investmentInstructionDeliveryTypeDAO;
	private AdvancedUpdatableDAO<InvestmentInstructionDeliveryField, Criteria> investmentInstructionDeliveryFieldDAO;
	private AdvancedUpdatableDAO<InvestmentInstructionDeliveryFieldType, Criteria> investmentInstructionDeliveryFieldTypeDAO;

	private DaoNamedEntityCache<InvestmentInstructionDeliveryType> investmentInstructionDeliveryTypeCache;
	private DaoNamedEntityCache<InvestmentInstructionDeliveryFieldType> investmentInstructionDeliveryFieldTypeCache;

	private InvestmentInstructionDeliveryBusinessCompanyService investmentInstructionDeliveryBusinessCompanyService;


	////////////////////////////////////////////////////////////////////////////
	///////      Investment Instruction Delivery Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionDelivery getInvestmentInstructionDelivery(int id) {
		return getInvestmentInstructionDeliveryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInstructionDelivery> getInvestmentInstructionDeliveryList(final InvestmentInstructionDeliverySearchForm searchForm) {
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (Objects.nonNull(searchForm.getBusinessCompanyId()) || Objects.nonNull(searchForm.getTypeId())) {
					DetachedCriteria link = DetachedCriteria.forClass(InvestmentInstructionDeliveryBusinessCompany.class, "link");
					link.createAlias("referenceOne", "businessCompany");
					link.createAlias("referenceTwo", "delivery");
					link.setProjection(Projections.property("delivery.id"));
					if (Objects.nonNull(searchForm.getBusinessCompanyId())) {
						link.add(Restrictions.eq("businessCompany.id", searchForm.getBusinessCompanyId()));
					}
					if (Objects.nonNull(searchForm.getTypeId())) {
						link.add(Restrictions.eq("investmentInstructionDeliveryType.id", searchForm.getTypeId()));
					}
					criteria.add(Subqueries.propertyIn("id", link));
				}
			}
		};
		return getInvestmentInstructionDeliveryDAO().findBySearchCriteria(configurer);
	}


	@Override
	public InvestmentInstructionDelivery getInvestmentInstructionDeliveryByCompanyAndTypeAndCurrency(BusinessCompany company, InvestmentInstructionDeliveryType type, InvestmentSecurity deliveryCurrency) {
		AssertUtils.assertNotNull(company, "Company parameter is required.");
		AssertUtils.assertNotNull(type, "Type parameter is required.");
		AssertUtils.assertNotNull(deliveryCurrency, "Currency parameter is required.");

		InvestmentInstructionDeliveryBusinessCompanySearchForm searchForm = new InvestmentInstructionDeliveryBusinessCompanySearchForm();
		searchForm.setBusinessCompanyId(company.getId());
		searchForm.setTypeId(type.getId());
		searchForm.setDeliveryCurrencyId(deliveryCurrency.getId());
		List<InvestmentInstructionDeliveryBusinessCompany> deliveryBusinessCompanies = getInvestmentInstructionDeliveryBusinessCompanyService().getInvestmentInstructionDeliveryBusinessCompanyList(searchForm);

		ValidationUtils.assertFalse(deliveryBusinessCompanies.isEmpty(), () ->
				String.format("The Delivery Instruction could not be found for Broker [%s], Delivery Type [%s], and Delivery Currency [%s]", company.getName(), type.getName(), deliveryCurrency.getSymbol()));
		return CollectionUtils.getOnlyElementStrict(deliveryBusinessCompanies).getReferenceOne();
	}


	@Override
	public InvestmentInstructionDelivery saveInvestmentInstructionDelivery(InvestmentInstructionDelivery bean) {
		return getInvestmentInstructionDeliveryDAO().save(bean);
	}


	@Override
	public void deleteInvestmentInstructionDelivery(int id) {
		getInvestmentInstructionDeliveryDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////    Investment Instruction Delivery Type Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionDeliveryType getInvestmentInstructionDeliveryType(Short id) {
		return getInvestmentInstructionDeliveryTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInstructionDeliveryType> getInvestmentInstructionDeliveryTypeList(InvestmentInstructionDeliveryTypeSearchForm searchForm) {
		return getInvestmentInstructionDeliveryTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentInstructionDeliveryType getInvestmentInstructionDeliveryTypeByName(String deliveryType) {
		return getInvestmentInstructionDeliveryTypeCache().getBeanForKeyValue(getInvestmentInstructionDeliveryTypeDAO(), deliveryType);
	}


	@Override
	public InvestmentInstructionDeliveryType saveInvestmentInstructionDeliveryType(InvestmentInstructionDeliveryType bean) {
		return getInvestmentInstructionDeliveryTypeDAO().save(bean);
	}


	@Override
	public void deleteInvestmentInstructionDeliveryType(Short id) {
		getInvestmentInstructionDeliveryTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////    Investment Instruction Delivery Field Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionDeliveryField getInvestmentInstructionDeliveryField(int id) {
		return getInvestmentInstructionDeliveryFieldDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInstructionDeliveryField> getInvestmentInstructionDeliveryFieldList(InvestmentInstructionDeliveryFieldSearchForm searchForm) {
		return getInvestmentInstructionDeliveryFieldDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentInstructionDeliveryField saveInvestmentInstructionDeliveryField(InvestmentInstructionDeliveryField bean) {
		ValidationUtils.assertFalse(Objects.isNull(bean.getHoldingAccount()) && Objects.nonNull(bean.getClientAccount()),
				"The [Holding Account] is required when [Client Account] is specified.");
		ValidationUtils.assertTrue(Stream.of(bean.getDeliveryType(), bean.getHoldingAccountGroup(), bean.getHoldingAccount()).filter(Objects::nonNull).count() <= 1,
				"Only one Field Selection ( [Delivery Type] [Holding Account Group] [Holding Account] ) is allowed.");
		return getInvestmentInstructionDeliveryFieldDAO().save(bean);
	}


	@Override
	public void deleteInvestmentInstructionDeliveryField(int id) {
		getInvestmentInstructionDeliveryFieldDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	/////   Investment Instruction Delivery Field Type Business Methods  ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionDeliveryFieldType getInvestmentInstructionDeliveryFieldType(short id) {
		return getInvestmentInstructionDeliveryFieldTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentInstructionDeliveryFieldType getInvestmentInstructionDeliveryFieldTypeByName(String fieldTypeName) {
		return getInvestmentInstructionDeliveryFieldTypeCache().getBeanForKeyValue(getInvestmentInstructionDeliveryFieldTypeDAO(), fieldTypeName);
	}


	@Override
	public List<InvestmentInstructionDeliveryFieldType> getInvestmentInstructionDeliveryFieldTypeList(InvestmentInstructionDeliveryFieldTypeSearchForm searchForm) {
		return getInvestmentInstructionDeliveryFieldTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstructionDelivery, Criteria> getInvestmentInstructionDeliveryDAO() {
		return this.investmentInstructionDeliveryDAO;
	}


	public void setInvestmentInstructionDeliveryDAO(AdvancedUpdatableDAO<InvestmentInstructionDelivery, Criteria> investmentInstructionDeliveryDAO) {
		this.investmentInstructionDeliveryDAO = investmentInstructionDeliveryDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInstructionDeliveryType, Criteria> getInvestmentInstructionDeliveryTypeDAO() {
		return this.investmentInstructionDeliveryTypeDAO;
	}


	public void setInvestmentInstructionDeliveryTypeDAO(AdvancedUpdatableDAO<InvestmentInstructionDeliveryType, Criteria> investmentInstructionDeliveryTypeDAO) {
		this.investmentInstructionDeliveryTypeDAO = investmentInstructionDeliveryTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInstructionDeliveryField, Criteria> getInvestmentInstructionDeliveryFieldDAO() {
		return this.investmentInstructionDeliveryFieldDAO;
	}


	public void setInvestmentInstructionDeliveryFieldDAO(AdvancedUpdatableDAO<InvestmentInstructionDeliveryField, Criteria> investmentInstructionDeliveryFieldDAO) {
		this.investmentInstructionDeliveryFieldDAO = investmentInstructionDeliveryFieldDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInstructionDeliveryFieldType, Criteria> getInvestmentInstructionDeliveryFieldTypeDAO() {
		return this.investmentInstructionDeliveryFieldTypeDAO;
	}


	public void setInvestmentInstructionDeliveryFieldTypeDAO(AdvancedUpdatableDAO<InvestmentInstructionDeliveryFieldType, Criteria> investmentInstructionDeliveryFieldTypeDAO) {
		this.investmentInstructionDeliveryFieldTypeDAO = investmentInstructionDeliveryFieldTypeDAO;
	}


	public DaoNamedEntityCache<InvestmentInstructionDeliveryType> getInvestmentInstructionDeliveryTypeCache() {
		return this.investmentInstructionDeliveryTypeCache;
	}


	public void setInvestmentInstructionDeliveryTypeCache(DaoNamedEntityCache<InvestmentInstructionDeliveryType> investmentInstructionDeliveryTypeCache) {
		this.investmentInstructionDeliveryTypeCache = investmentInstructionDeliveryTypeCache;
	}


	public DaoNamedEntityCache<InvestmentInstructionDeliveryFieldType> getInvestmentInstructionDeliveryFieldTypeCache() {
		return this.investmentInstructionDeliveryFieldTypeCache;
	}


	public void setInvestmentInstructionDeliveryFieldTypeCache(DaoNamedEntityCache<InvestmentInstructionDeliveryFieldType> investmentInstructionDeliveryFieldTypeCache) {
		this.investmentInstructionDeliveryFieldTypeCache = investmentInstructionDeliveryFieldTypeCache;
	}


	public InvestmentInstructionDeliveryBusinessCompanyService getInvestmentInstructionDeliveryBusinessCompanyService() {
		return this.investmentInstructionDeliveryBusinessCompanyService;
	}


	public void setInvestmentInstructionDeliveryBusinessCompanyService(InvestmentInstructionDeliveryBusinessCompanyService investmentInstructionDeliveryBusinessCompanyService) {
		this.investmentInstructionDeliveryBusinessCompanyService = investmentInstructionDeliveryBusinessCompanyService;
	}
}
