package com.clifton.investment.instruction;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.instruction.messages.InstructionPreviewMessage;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;
import com.clifton.investment.instruction.search.InvestmentInstructionItemSearchForm;
import com.clifton.investment.instruction.search.InvestmentInstructionSearchForm;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentInstructionService</code> ...
 *
 * @author manderson
 */
public interface InvestmentInstructionService {

	////////////////////////////////////////////////////////////////////////////
	//////////                       Instruction                      //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstruction getInvestmentInstruction(int id);


	public List<InvestmentInstruction> getInvestmentInstructionList(InvestmentInstructionSearchForm searchForm);


	public List<InvestmentInstruction> getInvestmentInstructionListPopulated(InvestmentInstructionSearchForm searchForm);


	/**
	 * When saving from UI, we want to re-populate the Fk Field label for users, but
	 * from generation, we set that to true so we don't re-populate it.
	 */
	public InvestmentInstruction saveInvestmentInstruction(InvestmentInstruction bean, boolean doNotPopulateFkFieldLabel);


	public InvestmentInstruction saveInvestmentInstructionComplete(int id, Date wireDate);


	@SecureMethod(dtoClass = InvestmentInstruction.class)
	public void saveInvestmentInstructionListComplete(Integer[] instructionIds, Date wireDate);


	public void deleteInvestmentInstruction(int id);


	@DoNotAddRequestMapping
	public void refreshInvestmentInstructionStatus(InvestmentInstruction bean, List<InvestmentInstructionItem> items);


	@DoNotAddRequestMapping
	public void setInvestmentInstructionRegeneratingFlag(InvestmentInstructionCategory category, Date date);


	@DoNotAddRequestMapping
	public void clearInvestmentInstructionRegeneratingFlag(InvestmentInstructionCategory category, Date date);

	////////////////////////////////////////////////////////////////////////////
	///////////                   Instruction Item                    //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionItem getInvestmentInstructionItem(int id);


	public InvestmentInstructionItem getInvestmentInstructionItemPopulated(int id);


	/**
	 * Returns the list of instruction items for the entity.  Mostly there will be only one, but there could be one per category if there are
	 * multiple categories defined for the table.
	 */
	public <T extends IdentityObject> List<InvestmentInstructionItem> getInvestmentInstructionItemListForEntityObject(T dtoObject);


	/**
	 * Returns the list of instruction items of a specific run type for the entity.  Mostly there will be only one, but there could be one per category if there are
	 * multiple categories defined for the table.
	 */
	public <T extends IdentityObject> List<InvestmentInstructionItem> getInvestmentInstructionItemListForEntityObjectAndType(T dtoObject, InvestmentInstructionRunTypes runType);


	/**
	 * Returns the list of instruction items for the entity.  Mostly there will be only one, but there could be one per category if there are
	 * multiple categories defined for the table.
	 */
	public List<InvestmentInstructionItem> getInvestmentInstructionItemListForEntity(String tableName, int fkFieldId);


	/**
	 * Returns the list of instruction items for the entity list.
	 * Used for cases like Collateral Balance, where we display the list of instructions for any position transfer associated with the collateral balance
	 * Optionally populate the fkFieldLabel, which can be helpful for cases where we list multiple items
	 */
	public List<InvestmentInstructionItem> getInvestmentInstructionItemListForEntityList(String tableName, Integer[] fkFieldIds, boolean populateFkFieldLabel);


	/**
	 * Get the list of instruction items with the label properly populated
	 */
	public List<InvestmentInstructionItem> getInvestmentInstructionItemListPopulated(InvestmentInstructionItemSearchForm searchForm);


	public List<InvestmentInstructionItem> getInvestmentInstructionItemList(InvestmentInstructionItemSearchForm searchForm);


	public InvestmentInstructionItem saveInvestmentInstructionItem(InvestmentInstructionItem item);


	@DoNotAddRequestMapping
	public void saveInvestmentInstructionItemComplete(InvestmentInstructionItem item, InvestmentInstruction instruction);


	@SecureMethod(dtoClass = InvestmentInstructionItem.class)
	public void saveInvestmentInstructionItemListComplete(Integer[] itemIds, Date wireDate);


	@DoNotAddRequestMapping
	public void saveInvestmentInstructionItemError(InvestmentInstructionItem item, InvestmentInstruction instruction);


	@ModelAttribute("result")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public int saveInvestmentInstructionItemListError(Integer[] itemIds);


	@ModelAttribute("result")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public int saveInvestmentInstructionRunListError(Integer[] runIds);


	public void deleteInvestmentInstructionItem(int id);


	@RequestMapping("investmentInstructionItemListCancel")
	public void cancelInvestmentInstructionItemList(Integer[] itemIds);


	/**
	 * Generates a preview of an export message; some messages require the optional definition id.
	 */
	@SecureMethod(securityResource = "SWIFT")
	public InstructionPreviewMessage getInvestmentInstructionPreview(int fkFieldId, String tableName, Integer definitionId);


	@DoNotAddRequestMapping
	public void populateInvestmentInstructionItemFkFieldLabel(List<InvestmentInstructionItem> itemList);
}
