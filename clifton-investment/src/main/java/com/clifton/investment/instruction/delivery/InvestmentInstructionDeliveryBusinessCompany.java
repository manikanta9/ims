package com.clifton.investment.instruction.delivery;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * <code>InvestmentInstructionDeliveryBusinessCompany</code> link between {@link BusinessCompany} and {@link InvestmentInstructionDelivery}.
 */
public class InvestmentInstructionDeliveryBusinessCompany extends ManyToManyEntity<InvestmentInstructionDelivery, BusinessCompany, Integer> {

	private InvestmentInstructionDeliveryType investmentInstructionDeliveryType;
	private InvestmentSecurity deliveryCurrency;


	public InvestmentInstructionDeliveryType getInvestmentInstructionDeliveryType() {
		return this.investmentInstructionDeliveryType;
	}


	public void setInvestmentInstructionDeliveryType(InvestmentInstructionDeliveryType investmentInstructionDeliveryType) {
		this.investmentInstructionDeliveryType = investmentInstructionDeliveryType;
	}


	public InvestmentSecurity getDeliveryCurrency() {
		return this.deliveryCurrency;
	}


	public void setDeliveryCurrency(InvestmentSecurity deliveryCurrency) {
		this.deliveryCurrency = deliveryCurrency;
	}
}
