package com.clifton.investment.instruction.run;

import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;

import java.util.Date;


/**
 * A "Run" is an attempt to send an instruction(s).
 * <p>
 * {@link InvestmentInstructionRunInstructionItem} table identifies specific instruction items that
 * are part of the run. Certain run types (SWIFT) may support only one instruction item per run.
 * We may also have multiple runs of different types for the same instruction (Email and FAX).
 *
 * @author theodorez
 */

public class InvestmentInstructionRun extends BaseEntity<Integer> {

	/**
	 * The instruction the run is tied to. A run may only be tied to a single instruction.
	 */
	private InvestmentInstruction investmentInstruction;

	/**
	 * The type of run this is: Fax, FTP, Email, SWIFT
	 */
	private InvestmentInstructionRunTypes runType;

	/**
	 * The Status of the Run:  OPEN, SEND, SENDING, SENT, COMPLETED, CANCELED, ERROR
	 */
	private InvestmentInstructionStatus runStatus;

	/**
	 * Indicates that the run is detached, meaning that it will not be used to update the status
	 * of the InvestmentInstructionItem or InvestmentInstruction that the run was created for.
	 */
	private boolean detached;

	private Date scheduledDate; //The date the run is scheduled to be ran. As Runs are instantaneous this is often the same as the start date
	private Date startDate; //The date the run is sent
	private Date endDate; //The date the run finishes
	private String historyDescription; //A user-friendly string of the history of the Run

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstruction getInvestmentInstruction() {
		return this.investmentInstruction;
	}


	public void setInvestmentInstruction(InvestmentInstruction investmentInstruction) {
		this.investmentInstruction = investmentInstruction;
	}


	public InvestmentInstructionStatus getRunStatus() {
		return this.runStatus;
	}


	public void setRunStatus(InvestmentInstructionStatus runStatus) {
		this.runStatus = runStatus;
	}


	public Date getScheduledDate() {
		return this.scheduledDate;
	}


	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getHistoryDescription() {
		return this.historyDescription;
	}


	public void setHistoryDescription(String historyDescription) {
		this.historyDescription = historyDescription;
	}


	public InvestmentInstructionRunTypes getRunType() {
		return this.runType;
	}


	public void setRunType(InvestmentInstructionRunTypes runType) {
		this.runType = runType;
	}


	public boolean isDetached() {
		return this.detached;
	}


	public void setDetached(boolean detached) {
		this.detached = detached;
	}
}
