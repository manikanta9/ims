package com.clifton.investment.instruction;


import com.clifton.core.beans.BaseUpdatableOnlyEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.instruction.Instruction;
import com.clifton.investment.instruction.group.InvestmentInstructionItemGroup;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.util.Date;


/**
 * The <code>InvestmentInstructionItem</code> contains the information for the entities
 * associated with an instruction.  i.e. Trade ID, M2M ID, etc.
 *
 * @author manderson
 */
public class InvestmentInstructionItem extends BaseUpdatableOnlyEntity<Integer> implements Instruction {

	public static final String INVESTMENT_INSTRUCTION_ITEM_TABLE_NAME = "InvestmentInstructionItem";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentInstruction instruction;

	@SoftLinkField(tableBeanPropertyName = "instruction.definition.category.table")
	private Integer fkFieldId;

	/**
	 * Status Indicator
	 */
	private InvestmentInstructionStatus status;

	/**
	 * Optional grouping label used for UI display as well as indicator in reports as to which items
	 * are included in the same report
	 */
	private InvestmentInstructionItemGroup itemGroup;

	/**
	 * The investment instruction item this item is cancelling.
	 */
	private InvestmentInstructionItem referencedInvestmentInstructionItem;

	/**
	 * Date agreed upon with the counterparty to wire out/in cash, applies to Swap trades and resets.
	 */
	private Date wireDate;

	// Not saved - Populated for Display Purposes on Instruction Window ONLY - populated if entity implements LabeledObject - else uses TableName + FkFieldID
	@NonPersistentField
	private String fkFieldLabel;
	/**
	 * Ready usually indicates the item is booked - but some definition selectors allow other conditions (Booked or (Amount = 0 and Reconciled))
	 * NOTE: Not stored in DB
	 */
	@NonPersistentField
	private Boolean ready;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemTable getSystemTable() {
		if (getInstruction() != null) {
			return getInstruction().getDefinition().getCategory().getTable();
		}
		return null;
	}


	public String getFkFieldLabel() {
		if (!StringUtils.isEmpty(this.fkFieldLabel)) {
			return this.fkFieldLabel;
		}
		else if (getInstruction() != null) {
			return getInstruction().getDefinition().getCategory().getTable().getName() + ": " + getFkFieldId();
		}
		return null;
	}


	public String getLabel() {
		return "Item " + getId() + ( getInstruction() == null ? "" : " for " + getInstruction().getLabel());
	}


	public String getReportGroupingUniqueKey() {
		StringBuilder sb = new StringBuilder(16);
		if (getInstruction() != null) {
			sb.append(getInstruction().getId());
		}
		if (getItemGroup() != null) {
			sb.append("_G_");
			sb.append(getItemGroup().getId());
		}
		else {
			sb.append("_I_");
			sb.append(getId());
		}
		return sb.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setFkFieldLabel(String fkFieldLabel) {
		this.fkFieldLabel = fkFieldLabel;
	}


	public InvestmentInstruction getInstruction() {
		return this.instruction;
	}


	public void setInstruction(InvestmentInstruction instruction) {
		this.instruction = instruction;
	}


	@Override
	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public InvestmentInstructionStatus getStatus() {
		return this.status;
	}


	public void setStatus(InvestmentInstructionStatus status) {
		this.status = status;
	}


	public InvestmentInstructionItemGroup getItemGroup() {
		return this.itemGroup;
	}


	public void setItemGroup(InvestmentInstructionItemGroup itemGroup) {
		this.itemGroup = itemGroup;
	}


	public InvestmentInstructionItem getReferencedInvestmentInstructionItem() {
		return this.referencedInvestmentInstructionItem;
	}


	public void setReferencedInvestmentInstructionItem(InvestmentInstructionItem referencedInvestmentInstructionItem) {
		this.referencedInvestmentInstructionItem = referencedInvestmentInstructionItem;
	}


	public Date getWireDate() {
		return this.wireDate;
	}


	public void setWireDate(Date wireDate) {
		this.wireDate = wireDate;
	}


	public Boolean getReady() {
		return this.ready;
	}


	public void setReady(Boolean ready) {
		this.ready = ready;
	}
}
