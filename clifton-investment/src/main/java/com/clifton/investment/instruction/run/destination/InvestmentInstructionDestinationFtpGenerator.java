package com.clifton.investment.instruction.run.destination;

import com.clifton.core.util.ftp.FtpConfig;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.util.ExportMessagingUtils;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.security.secret.SecuritySecretService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.util.Map;


/**
 * This generator is responsible for generating an FTP destination
 */
public class InvestmentInstructionDestinationFtpGenerator implements InvestmentInstructionDestinationGenerator {

	private SecuritySecretService securitySecretService;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * The shared export destination that will be used to populate the map
	 */
	private Integer sharedFtpDestinationId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<ExportMapKeys, String> generateDestination(InvestmentInstructionRun run) {
		FtpConfig config = getDestinationFtpConfig();
		return ExportMessagingUtils.generateFtpDestinationMap(config
				, getSecuritySecretString(config.getPasswordSecretId())
				, getSecuritySecretString(config.getSshPrivateKeySecretId()));
	}


	@Override
	public Class<?> getParentClass() {
		return null;
	}


	@Override
	public String getDestinationName() {
		FtpConfig config = getDestinationFtpConfig();
		return config.getUrl() + ((config.getPort() != null) ? ":" + config.getPort() : "");
	}


	@Override
	public String getDestinationLabel() {
		return getDestinationName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getSecuritySecretString(Integer id) {
		if (id != null) {
			return getSecuritySecretService().decryptSecuritySecret(getSecuritySecretService().getSecuritySecret(id)).getSecretString();
		}
		return null;
	}


	private FtpConfig getDestinationFtpConfig() {
		Object bean = getSystemBeanService().getBeanInstance(getSharedFtpDestination());
		ValidationUtils.assertInstanceOf(FtpConfig.class, bean, "Shared FTP Destination (id=" + getSharedFtpDestinationId() + ") is not an instance of FtpConfig.");
		return (FtpConfig) bean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBean getSharedFtpDestination() {
		return getSystemBeanService().getSystemBean(getSharedFtpDestinationId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSharedFtpDestinationId() {
		return this.sharedFtpDestinationId;
	}


	public void setSharedFtpDestinationId(Integer sharedFtpDestinationId) {
		this.sharedFtpDestinationId = sharedFtpDestinationId;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}
}
