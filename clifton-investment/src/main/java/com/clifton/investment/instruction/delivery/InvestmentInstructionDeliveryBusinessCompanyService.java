package com.clifton.investment.instruction.delivery;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryBusinessCompanySearchForm;

import java.util.List;


/**
 * <code>InvestmentInstructionDeliveryBusinessCompanyService</code>
 */
public interface InvestmentInstructionDeliveryBusinessCompanyService {


	@SecureMethod(dtoClass = InvestmentInstructionDelivery.class)
	public InvestmentInstructionDeliveryBusinessCompany getInvestmentInstructionDeliveryBusinessCompany(int id);


	@SecureMethod(dtoClass = InvestmentInstructionDelivery.class)
	public List<InvestmentInstructionDeliveryBusinessCompany> getInvestmentInstructionDeliveryBusinessCompanyList(final InvestmentInstructionDeliveryBusinessCompanySearchForm searchForm);


	@SecureMethod(dtoClass = InvestmentInstructionDelivery.class)
	public InvestmentInstructionDeliveryBusinessCompany saveInvestmentInstructionDeliveryBusinessCompany(InvestmentInstructionDeliveryBusinessCompany investmentInstructionDeliveryBusinessCompany);


	@SecureMethod(dtoClass = InvestmentInstructionDelivery.class)
	public void deleteInvestmentInstructionDeliveryBusinessCompany(int id);
}
