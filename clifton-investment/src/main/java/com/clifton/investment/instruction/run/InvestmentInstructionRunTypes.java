package com.clifton.investment.instruction.run;

import com.clifton.core.util.CollectionUtils;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.investment.instruction.run.runner.InvestmentInstructionRunner;
import com.clifton.investment.instruction.run.runner.InvestmentInstructionSwiftRunner;

import java.util.List;


/**
 * @author theodorez
 */
public enum InvestmentInstructionRunTypes {
	EMAIL(ExportDestinationTypes.EMAIL, "Instruction Email", InvestmentInstructionRunner.class),
	FAX(ExportDestinationTypes.PHONE, "Instruction Fax", InvestmentInstructionRunner.class),
	FTP(ExportDestinationTypes.FTP, "Instruction FTP", InvestmentInstructionRunner.class),
	SWIFT(null, "Instruction SWIFT", InvestmentInstructionSwiftRunner.class, EMAIL, FTP);

	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Maps Instruction Run Types to Export Destination Types
	 */
	private final ExportDestinationTypes destinationType;

	private final String destinationGeneratorBeanType;

	/**
	 * A list of
	 */
	private final List<InvestmentInstructionRunTypes> allowedDetachedRunTypes;

	/**
	 * The runner class the will be created to send the instructions.
	 */
	private final Class<?> runnerClass;


	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	InvestmentInstructionRunTypes(ExportDestinationTypes destinationType, String destinationGeneratorBeanType, Class<?> runnerClass, InvestmentInstructionRunTypes... allowedDetachedRunTypes) {
		this.destinationType = destinationType;
		this.destinationGeneratorBeanType = destinationGeneratorBeanType;
		this.runnerClass = runnerClass;
		this.allowedDetachedRunTypes = CollectionUtils.createList(allowedDetachedRunTypes);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	public static InvestmentInstructionRunTypes getInstructionRunTypeFromBeanTypeName(String beanTypeName) {
		for (InvestmentInstructionRunTypes runType : InvestmentInstructionRunTypes.values()) {
			if (beanTypeName.equalsIgnoreCase(runType.destinationGeneratorBeanType)) {
				return runType;
			}
		}
		throw new RuntimeException("Cannot find InvestmentInstructionRunTypes for type [" + beanTypeName + "].");
	}


	public boolean isAllowedDetachedRunType(InvestmentInstructionRunTypes runType) {
		if (!CollectionUtils.isEmpty(getAllowedDetachedRunTypes()) && getAllowedDetachedRunTypes().contains(runType)) {
			return true;
		}
		return false;
	}


	public boolean isParallelRunSupported(InvestmentInstructionRunTypes runType) {
		if (!CollectionUtils.isEmpty(getAllowedDetachedRunTypes())) {
			return getAllowedDetachedRunTypes().contains(runType);
		}
		else if (!CollectionUtils.isEmpty(runType.getAllowedDetachedRunTypes())) {
			return runType.getAllowedDetachedRunTypes().contains(this);
		}
		return runType.getRunnerClass() == getRunnerClass();
	}


	public boolean isEmail() {
		return this == EMAIL;
	}


	public boolean isFax() {
		return this == FAX;
	}


	public boolean isFtp() {
		return this == FTP;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	public ExportDestinationTypes getDestinationType() {
		return this.destinationType;
	}


	public String getDestinationGeneratorBeanType() {
		return this.destinationGeneratorBeanType;
	}


	public Class<?> getRunnerClass() {
		return this.runnerClass;
	}


	public List<InvestmentInstructionRunTypes> getAllowedDetachedRunTypes() {
		return this.allowedDetachedRunTypes;
	}
}
