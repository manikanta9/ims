package com.clifton.investment.instruction.setup;

import com.clifton.core.beans.NamedEntity;


/**
 * Instructions are usually sent to multiple parties. Contact Groups define named collections of contacts that can be shared across multiple definitions.
 * An instruction is only sent to group\'s contacts that have recipient company match this on instruction.
 *
 * @author theodorez
 */
public class InvestmentInstructionContactGroup extends NamedEntity<Integer> {
	//Nothing here
}
