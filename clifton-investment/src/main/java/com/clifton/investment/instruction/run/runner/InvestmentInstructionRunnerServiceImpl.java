package com.clifton.investment.instruction.run.runner;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author theodorez
 */
@Service
public class InvestmentInstructionRunnerServiceImpl implements InvestmentInstructionRunnerService {

	private InvestmentInstructionRunnerFactory investmentInstructionRunnerFactory;
	private InvestmentInstructionRunService investmentInstructionRunService;

	private RunnerHandler runnerHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void sendItems(List<InvestmentInstructionItem> items) {
		if (!CollectionUtils.isEmpty(items)) {
			List<InvestmentInstructionItem> itemHolderList = new ArrayList<>();

			for (InvestmentInstructionItem item : items) {
				InvestmentInstructionRun latestRun = getInvestmentInstructionRunService().getInvestmentInstructionRunForItemLatest(item.getId());

				// no run or run status IN (ERROR, ERROR_CANCEL)
				if (latestRun == null || latestRun.getRunStatus().isFail()) {
					itemHolderList.add(item);
				}
				// send cancel / correct
				// run status == COMPLETED AND status == SENT_CANCEL
				else if (latestRun.getRunStatus().isCompleted() && item.getStatus().isCancel()
						&& InvestmentInstructionStatusNames.SENT_CANCEL == item.getStatus().getInvestmentInstructionStatusName()) {
					itemHolderList.add(item);
				}
				// not sure how we got here
				else {
					LogUtils.error(this.getClass(), String.format("The instruction item with status [%s] associated with run [%s] with status [%s] was not sent.", item.getStatus(),
							latestRun.getId(), latestRun.getRunStatus()));
				}
			}

			if (!itemHolderList.isEmpty()) {
				handleSend(itemHolderList);
			}
		}
	}


	private void handleSend(List<InvestmentInstructionItem> items) {
		if (!CollectionUtils.isEmpty(items)) {
			InvestmentInstructionRunner runner = getInvestmentInstructionRunnerFactory().createInvestmentInstructionRunner(new Date(), items);
			getRunnerHandler().runNow(runner);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionRunnerFactory getInvestmentInstructionRunnerFactory() {
		return this.investmentInstructionRunnerFactory;
	}


	public void setInvestmentInstructionRunnerFactory(InvestmentInstructionRunnerFactory investmentInstructionRunnerFactory) {
		this.investmentInstructionRunnerFactory = investmentInstructionRunnerFactory;
	}


	public InvestmentInstructionRunService getInvestmentInstructionRunService() {
		return this.investmentInstructionRunService;
	}


	public void setInvestmentInstructionRunService(InvestmentInstructionRunService investmentInstructionRunService) {
		this.investmentInstructionRunService = investmentInstructionRunService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
