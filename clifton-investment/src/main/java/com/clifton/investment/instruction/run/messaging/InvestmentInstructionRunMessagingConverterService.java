package com.clifton.investment.instruction.run.messaging;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.export.messaging.message.ExportMessagingMessageList;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;

import java.util.List;


/**
 * Methods for converting instruction runs to exports
 *
 * @author theodorez
 */
public interface InvestmentInstructionRunMessagingConverterService {

	/**
	 * Creates an export message from a run and files
	 */
	@DoNotAddRequestMapping
	public ExportMessagingMessage toExportMessagingMessage(InvestmentInstructionRun run, List<InvestmentInstructionContact> recipientContacts);


	@DoNotAddRequestMapping
	public ExportMessagingMessageList toExportMessagingMessageList(List<ExportMessagingMessage> messages, FileWrapper file, String rootDirectory);
}

