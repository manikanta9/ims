package com.clifton.investment.instruction.run.destination;

import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;

import java.util.List;
import java.util.Map;


/**
 * Used if a destination needs to be composed of the result of multiple generators.
 * <p>
 * Ex: Automated emails have multiple recipients that are calculated at time of transmission, each child generator returns a recipient
 * and the parent generator combines all recipients and adds other information to create a complete email destination map
 *
 * @author theodorez
 */
public interface InvestmentInstructionDestinationParentGenerator {

	public Map<ExportMapKeys, String> generate(InvestmentInstructionRun run);


	public void addChildGenerator(InvestmentInstructionDestinationGenerator generator);


	public List<InvestmentInstructionDestinationGenerator> getChildGeneratorList();


	public void setChildGeneratorList(List<InvestmentInstructionDestinationGenerator> childGeneratorList);
}
