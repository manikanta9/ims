package com.clifton.investment.instruction.setup;


import com.clifton.business.contact.BusinessContact;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.run.destination.InvestmentInstructionDestinationGenerator;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionCategorySearchForm;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionContactGroupSearchForm;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionContactSearchForm;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionDefinitionSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>InvestmentInstructionDefinitionServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class InvestmentInstructionSetupServiceImpl implements InvestmentInstructionSetupService {

	private AdvancedUpdatableDAO<InvestmentInstructionCategory, Criteria> investmentInstructionCategoryDAO;
	private AdvancedUpdatableDAO<InvestmentInstructionDefinition, Criteria> investmentInstructionDefinitionDAO;
	private AdvancedUpdatableDAO<InvestmentInstructionDefinitionField, Criteria> investmentInstructionDefinitionFieldDAO;
	private AdvancedUpdatableDAO<InvestmentInstructionContact, Criteria> investmentInstructionContactDAO;
	private AdvancedUpdatableDAO<InvestmentInstructionContactGroup, Criteria> investmentInstructionContactGroupDAO;

	private DaoNamedEntityCache<InvestmentInstructionCategory> investmentInstructionCategoryCache;

	private SystemBeanService systemBeanService;


	///////////////////////////////////////////////////////////////
	//////////           Instruction Category            //////////
	///////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionCategory getInvestmentInstructionCategory(short id) {
		return getInvestmentInstructionCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentInstructionCategory getInvestmentInstructionCategoryByName(String name) {
		return getInvestmentInstructionCategoryCache().getBeanForKeyValueStrict(getInvestmentInstructionCategoryDAO(), name);
	}


	@Override
	public List<InvestmentInstructionCategory> getInvestmentInstructionCategoryList(InvestmentInstructionCategorySearchForm searchForm) {
		return getInvestmentInstructionCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentInstructionCategory> getInvestmentInstructionCategoryListByDestinationType(String destinationType) {
		InvestmentInstructionContactSearchForm contactSearchForm = new InvestmentInstructionContactSearchForm();
		contactSearchForm.setDestinationSystemBeanTypeName(destinationType);
		contactSearchForm.setActive(true);
		List<InvestmentInstructionContact> contactList = getInvestmentInstructionContactList(contactSearchForm);
		InvestmentInstructionDefinitionSearchForm definitionSearchForm = new InvestmentInstructionDefinitionSearchForm();
		definitionSearchForm.setContactGroupIds(contactList.stream()
				.map(InvestmentInstructionContact::getGroup)
				.map(InvestmentInstructionContactGroup::getId)
				.distinct().toArray(Integer[]::new));
		return getInvestmentInstructionDefinitionList(definitionSearchForm).stream()
				.filter(InvestmentInstructionDefinition::isActive)
				.map(InvestmentInstructionDefinition::getCategory)
				.distinct()
				.collect(Collectors.toList());
	}


	@Override
	public InvestmentInstructionCategory saveInvestmentInstructionCategory(InvestmentInstructionCategory bean) {
		validateFromContact(bean.getFromContact());
		return getInvestmentInstructionCategoryDAO().save(bean);
	}


	@Override
	public void deleteInvestmentInstructionCategory(short id) {
		getInvestmentInstructionCategoryDAO().delete(id);
	}


	///////////////////////////////////////////////////////////////
	//////////          Instruction Definition           //////////
	///////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionDefinition getInvestmentInstructionDefinition(int id) {
		InvestmentInstructionDefinition bean = getInvestmentInstructionDefinitionDAO().findByPrimaryKey(id);
		if (bean != null) {
			// Need Properties Populated
			if (bean.getSelectorBean() != null) {
				bean.setSelectorBean(getSystemBeanService().getSystemBean(bean.getSelectorBean().getId()));
			}
			bean.setFieldList(getInvestmentInstructionDefinitionFieldListByDefinition(id));
		}
		return bean;
	}


	@Override
	public List<InvestmentInstructionDefinition> getInvestmentInstructionDefinitionList(InvestmentInstructionDefinitionSearchForm searchForm) {
		return getInvestmentInstructionDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * Note: Validation for read only is done here instead of in observers, because we really only care about
	 * the fields and bean properties being updated - you can still update names/descriptions if they are active
	 */
	@Override
	@Transactional
	public InvestmentInstructionDefinition saveInvestmentInstructionDefinition(InvestmentInstructionDefinition bean) {
		validateFromContact(bean.getFromContact());
		// If existing bean isn't not in an open status - you can only edit definition information - no fields or system bean properties
		if (!bean.isNewBean() && !bean.isOpen()) {
			getInvestmentInstructionDefinitionDAO().save(bean);
		}
		// Otherwise a full edit
		else {
			SystemBean selectorBean = bean.getSelectorBean();
			ValidationUtils.assertNotNull(selectorBean, "Missing required selector bean");
			boolean renameSelectorBean = bean.isNewBean();

			selectorBean.setType(bean.getCategory().getSelectorBeanType());
			selectorBean.setName(bean.getCalculatedSelectorBeanName());
			selectorBean.setDescription("Selector bean generated for Investment Instruction Definition: " + bean.getName() + ": "
					+ StringUtils.coalesce(true, bean.getDescription()));
			getSystemBeanService().saveSystemBean(selectorBean);

			// get existing lists so that we know what to delete if necessary
			List<InvestmentInstructionDefinitionField> oldFields = null;
			List<InvestmentInstructionDefinitionField> newFields = bean.getFieldList();
			if (!bean.isNewBean()) {
				oldFields = getInvestmentInstructionDefinitionFieldListByDefinition(bean.getId());
			}

			// save the definition
			bean = getInvestmentInstructionDefinitionDAO().save(bean);

			if (renameSelectorBean) {
				getSystemBeanService().renameSystemBean(selectorBean.getId(), bean.getCalculatedSelectorBeanName());
			}

			for (InvestmentInstructionDefinitionField field : CollectionUtils.getIterable(newFields)) {
				field.setDefinition(bean);
			}
			getInvestmentInstructionDefinitionFieldDAO().saveList(newFields, oldFields);
			bean.setFieldList(newFields);
		}
		return bean;
	}


	private void validateFromContact(BusinessContact contact) {
		if (contact != null) {
			ValidationUtils.assertTrue(!StringUtils.isEmpty(contact.getEmailAddress()) && contact.getEmailAddress().endsWith("@paraport.com"),
					"The From Contact selected must have a valid Parametric email address");
		}
	}


	@Override
	@Transactional
	public InvestmentInstructionDefinition copyInvestmentInstructionDefinition(int id, String name) {
		InvestmentInstructionDefinition copyFrom = getInvestmentInstructionDefinition(id);
		InvestmentInstructionDefinition newBean = BeanUtils.cloneBean(copyFrom, false, false);
		newBean.setId(null);
		newBean.setName(name);
		newBean.setWorkflowState(null);
		newBean.setWorkflowStatus(null);
		List<InvestmentInstructionDefinitionField> newFieldList = new ArrayList<>();
		for (InvestmentInstructionDefinitionField field : CollectionUtils.getIterable(copyFrom.getFieldList())) {
			InvestmentInstructionDefinitionField newField = BeanUtils.cloneBean(field, false, false);
			newField.setId(null);
			newField.setDefinition(newBean);
			newFieldList.add(newField);
		}
		newBean.setFieldList(newFieldList);

		// Copy System Bean - and set it on the new instruction
		if (newBean.getSelectorBean() != null) {
			newBean.setSelectorBean(getSystemBeanService().copySystemBean(newBean.getSelectorBean().getId(), newBean.getCalculatedSelectorBeanName(), null));
		}
		saveInvestmentInstructionDefinition(newBean);
		return newBean;
	}


	@Override
	@Transactional
	public InvestmentInstructionDefinition reviseInvestmentInstructionDefinition(int id) {
		InvestmentInstructionDefinition bean = getInvestmentInstructionDefinition(id);
		// Can Only Revise an "Active"
		if (!bean.isActive()) {
			throw new ValidationException("Can only revise Active definitions only, this definition is currently [" + bean.getWorkflowStatus().getName() + "].");
		}

		InvestmentInstructionDefinition newBean = copyInvestmentInstructionDefinition(id, "Revision Of: " + bean.getName());
		bean.setParent(newBean);
		newBean.setName(bean.getName());
		// Name Update Only
		getInvestmentInstructionDefinitionDAO().save(bean);
		getInvestmentInstructionDefinitionDAO().save(newBean);
		return newBean;
	}


	@Override
	@Transactional
	public void deleteInvestmentInstructionDefinition(int id) {
		InvestmentInstructionDefinition bean = getInvestmentInstructionDefinition(id);
		if (!bean.isOpen()) {
			throw new ValidationException("Cannot delete definition [" + bean.getName() + "] because it is not in an Open status.");
		}
		// Find if anything points to it - Should Only be One
		InvestmentInstructionDefinition revisionOf = getInvestmentInstructionDefinitionDAO().findOneByField("parent.id", id);
		if (revisionOf != null) {
			if (!revisionOf.isActive()) {
				throw new ValidationException("Invalid workflow state found.  Cannot delete revised definition because the definition that it revises is not currently active.");
			}
			revisionOf.setParent(null);
			getInvestmentInstructionDefinitionDAO().save(revisionOf);
		}
		getInvestmentInstructionDefinitionFieldDAO().deleteList(getInvestmentInstructionDefinitionFieldListByDefinition(id));
		getInvestmentInstructionDefinitionDAO().delete(id);
		getSystemBeanService().deleteSystemBean(bean.getSelectorBean().getId());
	}


	///////////////////////////////////////////////////////////////
	//////////       Instruction Definition Field        //////////
	///////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionDefinitionField getInvestmentInstructionDefinitionField(int id) {
		return getInvestmentInstructionDefinitionFieldDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInstructionDefinitionField> getInvestmentInstructionDefinitionFieldListByDefinition(int definitionId) {
		return getInvestmentInstructionDefinitionFieldDAO().findByField("definition.id", definitionId);
	}


	@Override
	public InvestmentInstructionDefinitionField saveInvestmentInstructionDefinitionField(InvestmentInstructionDefinitionField bean) {
		return getInvestmentInstructionDefinitionFieldDAO().save(bean);
	}


	@Override
	public void deleteInvestmentInstructionDefinitionField(int id) {
		getInvestmentInstructionDefinitionFieldDAO().delete(id);
	}

	///////////////////////////////////////////////////////////////
	//////////       Instruction Definition Contact        //////////
	///////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionContact saveInvestmentInstructionContact(InvestmentInstructionContact bean) {
		SystemBean destinationBean = bean.getDestinationSystemBean();
		if (destinationBean != null) {
			checkForDuplicateInstructionContact(bean);
			InvestmentInstructionDestinationGenerator generator = (InvestmentInstructionDestinationGenerator) getSystemBeanService().getBeanInstance(destinationBean, destinationBean.getPropertyList());
			boolean newBean = destinationBean.isNewBean();
			if (newBean) {
				destinationBean.setName(StringUtils.formatStringUpToNCharsWithDots(generator.getDestinationName(), DataTypes.NAME_LONG.getLength(), true));
			}
			else {
				destinationBean.setAutoGeneratedName(generator.getDestinationName());
			}
			destinationBean.setDescription(generator.getDestinationLabel());
			getSystemBeanService().saveSystemBean(destinationBean);
			if (newBean) {
				// didn't have id on create so need to rename
				getSystemBeanService().renameSystemBean(destinationBean.getId(), destinationBean.getAutoGeneratedName(generator.getDestinationName()));
			}
		}
		return getInvestmentInstructionContactDAO().save(bean);
	}


	/**
	 * A contact should be unique per combination of
	 * 1. Contact Group
	 * 2. Company
	 * 3. Properties of the destination system bean
	 */
	private void checkForDuplicateInstructionContact(InvestmentInstructionContact bean) {
		InvestmentInstructionContactSearchForm searchForm = new InvestmentInstructionContactSearchForm();
		searchForm.setContactGroupId(bean.getGroup().getId());
		searchForm.setCompanyId(bean.getCompany().getId());
		searchForm.setDestinationSystemBeanTypeName(bean.getDestinationSystemBean().getType().getName());
		List<InvestmentInstructionContact> contacts = getInvestmentInstructionContactList(searchForm);
		if (!CollectionUtils.isEmpty(contacts)) {
			SystemBean[] beansToCompare = BeanUtils.getPropertyValues(contacts, "destinationSystemBean", SystemBean.class);
			List<SystemBean> duplicateBeans = getSystemBeanService().getDuplicateBeans(bean.getDestinationSystemBean(), new ArrayList<>(Arrays.asList(beansToCompare)));
			ValidationUtils.assertTrue(CollectionUtils.isEmpty(duplicateBeans), "Cannot save the Instruction Contact. A duplicate already exists.");
		}
	}


	@Override
	@Transactional
	public void deleteInvestmentInstructionContact(int id) {
		SystemBean destinationSystemBean = getInvestmentInstructionContact(id).getDestinationSystemBean();
		getInvestmentInstructionContactDAO().delete(id);
		getSystemBeanService().deleteSystemBean(destinationSystemBean.getId());
	}


	@Override
	public InvestmentInstructionContact getInvestmentInstructionContact(int id) {
		InvestmentInstructionContact result = getInvestmentInstructionContactDAO().findByPrimaryKey(id);
		if (result != null) {
			SystemBean bean = result.getDestinationSystemBean();
			if (bean != null) {
				bean.setPropertyList(getSystemBeanService().getSystemBeanPropertyListByBeanId(bean.getId()));
			}
		}
		return result;
	}


	@Override
	public List<InvestmentInstructionContact> getInvestmentInstructionContactListAll() {
		return getInvestmentInstructionContactDAO().findAll();
	}


	@Override
	public List<InvestmentInstructionContact> getInvestmentInstructionContactList(InvestmentInstructionContactSearchForm searchForm) {
		return getInvestmentInstructionContactDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	///////////////////////////////////////////////////////////////
	//////////   Instruction Definition Contact Groups   //////////
	///////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstructionContactGroup saveInvestmentInstructionContactGroup(InvestmentInstructionContactGroup bean) {
		return getInvestmentInstructionContactGroupDAO().save(bean);
	}


	@Override
	public InvestmentInstructionContactGroup getInvestmentInstructionContactGroup(int id) {
		return getInvestmentInstructionContactGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInstructionContactGroup> getInvestmentInstructionContactGroupListAll() {
		return getInvestmentInstructionContactGroupDAO().findAll();
	}


	@Override
	public List<InvestmentInstructionContactGroup> getInvestmentInstructionContactGroupList(InvestmentInstructionContactGroupSearchForm searchForm) {
		return getInvestmentInstructionContactGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public void deleteInvestmentInstructionContactGroup(InvestmentInstructionContactGroup bean) {
		getInvestmentInstructionContactGroupDAO().delete(bean);
	}


	///////////////////////////////////////////////////////////////
	//////////          Getter & Setter Methods          //////////
	///////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstructionCategory, Criteria> getInvestmentInstructionCategoryDAO() {
		return this.investmentInstructionCategoryDAO;
	}


	public void setInvestmentInstructionCategoryDAO(AdvancedUpdatableDAO<InvestmentInstructionCategory, Criteria> investmentInstructionCategoryDAO) {
		this.investmentInstructionCategoryDAO = investmentInstructionCategoryDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInstructionDefinition, Criteria> getInvestmentInstructionDefinitionDAO() {
		return this.investmentInstructionDefinitionDAO;
	}


	public void setInvestmentInstructionDefinitionDAO(AdvancedUpdatableDAO<InvestmentInstructionDefinition, Criteria> investmentInstructionDefinitionDAO) {
		this.investmentInstructionDefinitionDAO = investmentInstructionDefinitionDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInstructionDefinitionField, Criteria> getInvestmentInstructionDefinitionFieldDAO() {
		return this.investmentInstructionDefinitionFieldDAO;
	}


	public void setInvestmentInstructionDefinitionFieldDAO(AdvancedUpdatableDAO<InvestmentInstructionDefinitionField, Criteria> investmentInstructionDefinitionFieldDAO) {
		this.investmentInstructionDefinitionFieldDAO = investmentInstructionDefinitionFieldDAO;
	}


	public DaoNamedEntityCache<InvestmentInstructionCategory> getInvestmentInstructionCategoryCache() {
		return this.investmentInstructionCategoryCache;
	}


	public void setInvestmentInstructionCategoryCache(DaoNamedEntityCache<InvestmentInstructionCategory> investmentInstructionCategoryCache) {
		this.investmentInstructionCategoryCache = investmentInstructionCategoryCache;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public AdvancedUpdatableDAO<InvestmentInstructionContact, Criteria> getInvestmentInstructionContactDAO() {
		return this.investmentInstructionContactDAO;
	}


	public void setInvestmentInstructionContactDAO(AdvancedUpdatableDAO<InvestmentInstructionContact, Criteria> investmentInstructionContactDAO) {
		this.investmentInstructionContactDAO = investmentInstructionContactDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInstructionContactGroup, Criteria> getInvestmentInstructionContactGroupDAO() {
		return this.investmentInstructionContactGroupDAO;
	}


	public void setInvestmentInstructionContactGroupDAO(AdvancedUpdatableDAO<InvestmentInstructionContactGroup, Criteria> investmentInstructionContactGroupDAO) {
		this.investmentInstructionContactGroupDAO = investmentInstructionContactGroupDAO;
	}
}
