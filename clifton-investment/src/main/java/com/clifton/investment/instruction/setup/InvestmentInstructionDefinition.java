package com.clifton.investment.instruction.setup;


import com.clifton.business.contact.BusinessContact;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.report.definition.Report;
import com.clifton.system.bean.SystemBean;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.List;
import java.util.UUID;


/**
 * The <code>InvestmentInstructionDefinition</code> class defines configuration information as well as selection logic for each settlement instruction.
 * The system generates instructions for each active definitions. It will apply {@link #selectorBean} logic to identify entities that instruction
 * items should be generated for as well as the logic to select recipient company for each instruction.
 *
 * @author manderson
 */
public class InvestmentInstructionDefinition extends NamedHierarchicalEntity<InvestmentInstructionDefinition, Integer> implements WorkflowAware {

	private InvestmentInstructionCategory category;


	/**
	 * Report that is used for this definition.
	 */
	private Report report;

	/**
	 * {@link SystemBean} of the type (defined by {@link InvestmentInstructionCategory#selectorBeanType}) used to filter records that apply to this definition.
	 * Example for M2M: Holding Account Issuer: Goldman Sachs of Acct Type: Futures Broker and Base CCY = USD
	 */
	private SystemBean selectorBean;

	/**
	 * {@link SystemBean} of the group for interface {@link com.clifton.investment.instruction.group.populator.InvestmentInstructionItemGroupPopulator}) used
	 * to group items together that would display on the same report.
	 * Selectable options are based on the selected report where the report is a property of the bean. i.e. we may group trades by client account and holding account on the report
	 * so from instruction items we want to see the grouping and also confirm when pulling the reports for the instruction as a whole we don't pull duplicate reports
	 */
	private SystemBean itemGroupPopulatorBean;

	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;

	/**
	 * List of fields that are displayed on the instruction
	 */
	private List<InvestmentInstructionDefinitionField> fieldList;

	/**
	 * The body content of the message that will be automatically sent out with the instruction
	 */
	private String messageBodyContent;

	/**
	 * Sending or Marking Complete instruction requires the Wire Date.
	 */
	private boolean requiresWireDate;

	/**
	 * The subject content of the message that will be automatically sent out with the instruction
	 */
	private String messageSubjectContent;


	private InvestmentInstructionContactGroup contactGroup;

	/**
	 * Overrides the fromContact on the instruction category
	 */
	private BusinessContact fromContact;

	/**
	 * Freemarker template used for constructing the filename of the file that is sent out
	 */
	private String filenameTemplate;


	private InvestmentInstructionDeliveryType deliveryType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCalculatedSelectorBeanName() {
		if (isNewBean()) {
			return UUID.randomUUID().toString();
		}
		return "InvestmentInstructionDefinition-" + getId();
	}


	public boolean isOpen() {
		if (getWorkflowStatus() != null) {
			return WorkflowStatus.STATUS_OPEN.equals(getWorkflowStatus().getName());
		}
		return false;
	}


	public boolean isActive() {
		if (getWorkflowStatus() != null) {
			return WorkflowStatus.STATUS_ACTIVE.equals(getWorkflowStatus().getName());
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionCategory getCategory() {
		return this.category;
	}


	public void setCategory(InvestmentInstructionCategory category) {
		this.category = category;
	}


	public Report getReport() {
		return this.report;
	}


	public void setReport(Report report) {
		this.report = report;
	}


	@Override
	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	@Override
	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	@Override
	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	@Override
	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}


	public SystemBean getSelectorBean() {
		return this.selectorBean;
	}


	public void setSelectorBean(SystemBean selectorBean) {
		this.selectorBean = selectorBean;
	}


	public SystemBean getItemGroupPopulatorBean() {
		return this.itemGroupPopulatorBean;
	}


	public void setItemGroupPopulatorBean(SystemBean itemGroupPopulatorBean) {
		this.itemGroupPopulatorBean = itemGroupPopulatorBean;
	}


	public List<InvestmentInstructionDefinitionField> getFieldList() {
		return this.fieldList;
	}


	public void setFieldList(List<InvestmentInstructionDefinitionField> fieldList) {
		this.fieldList = fieldList;
	}


	public String getMessageBodyContent() {
		return this.messageBodyContent;
	}


	public void setMessageBodyContent(String messageBodyContent) {
		this.messageBodyContent = messageBodyContent;
	}


	public boolean isRequiresWireDate() {
		return this.requiresWireDate;
	}


	public void setRequiresWireDate(boolean requiresWireDate) {
		this.requiresWireDate = requiresWireDate;
	}


	public String getMessageSubjectContent() {
		return this.messageSubjectContent;
	}


	public void setMessageSubjectContent(String messageSubjectContent) {
		this.messageSubjectContent = messageSubjectContent;
	}


	public InvestmentInstructionContactGroup getContactGroup() {
		return this.contactGroup;
	}


	public void setContactGroup(InvestmentInstructionContactGroup contactGroup) {
		this.contactGroup = contactGroup;
	}


	public BusinessContact getFromContact() {
		return this.fromContact;
	}


	public void setFromContact(BusinessContact fromContact) {
		this.fromContact = fromContact;
	}


	public String getFilenameTemplate() {
		return this.filenameTemplate;
	}


	public void setFilenameTemplate(String filenameTemplate) {
		this.filenameTemplate = filenameTemplate;
	}


	public InvestmentInstructionDeliveryType getDeliveryType() {
		return this.deliveryType;
	}


	public void setDeliveryType(InvestmentInstructionDeliveryType deliveryType) {
		this.deliveryType = deliveryType;
	}
}
