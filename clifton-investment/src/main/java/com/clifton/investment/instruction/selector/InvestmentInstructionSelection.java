package com.clifton.investment.instruction.selector;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.group.InvestmentInstructionItemGroup;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>InvestmentInstructionSelection</code> is a config/virtual object
 * that is populated and returned by the selector with all necessary information populated
 * in order to process/create {@link InvestmentInstruction} and {@link InvestmentInstructionItem}
 *
 * @author manderson
 */
public class InvestmentInstructionSelection {

	private final InvestmentInstructionCategory category;
	private final InvestmentInstructionDefinition definition;
	private BusinessCompany recipientCompany;

	private final Integer fkFieldId;
	private final String fkFieldLabel;
	private final IdentityObject fkEntity;

	private InvestmentInstructionItemGroup itemGroup;

	////////////////////////////////////////////////////////////////////////////////
	//  The following fields are used for "Missing Instructions" to give more
	//  information

	private InvestmentAccount clientAccount;
	private InvestmentAccount holdingAccount;
	private InvestmentSecurity security;
	private boolean booked;
	private String description;

	/**
	 * Used for "Missing Instructions" - if we can't figure out recipient
	 * will set error message on the item for review instead of throwing exception
	 */
	private String errorMessage;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionSelection(InvestmentInstructionCategory category, Integer fkFieldId) {
		this(category, null, null, fkFieldId, null, null);
	}


	public InvestmentInstructionSelection(InvestmentInstructionDefinition definition, BusinessCompany recipientCompany, Integer fkFieldId, String fkFieldLabel, IdentityObject fkEntity) {
		this(definition != null ? definition.getCategory() : null, definition, recipientCompany, fkFieldId, fkFieldLabel, fkEntity);
	}


	public InvestmentInstructionSelection(InvestmentInstructionCategory category, InvestmentInstructionDefinition definition, BusinessCompany recipientCompany, Integer fkFieldId, String fkFieldLabel, IdentityObject fkEntity) {
		this.definition = definition;
		this.category = category;
		this.fkFieldId = fkFieldId;
		this.fkFieldLabel = fkFieldLabel;
		this.recipientCompany = recipientCompany;
		this.fkEntity = fkEntity;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSelectionKey() {
		if (getDefinition() != null && getRecipientCompany() != null) {
			return getDefinition().getId() + "_" + getRecipientCompany().getId();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getRecipientCompany() {
		return this.recipientCompany;
	}


	public void setRecipientCompany(BusinessCompany recipientCompany) {
		this.recipientCompany = recipientCompany;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public String getFkFieldLabel() {
		return this.fkFieldLabel;
	}


	public InvestmentInstructionDefinition getDefinition() {
		return this.definition;
	}


	public InvestmentInstructionCategory getCategory() {
		return this.category;
	}


	public IdentityObject getFkEntity() {
		return this.fkEntity;
	}


	public InvestmentInstructionItemGroup getItemGroup() {
		return this.itemGroup;
	}


	public void setItemGroup(InvestmentInstructionItemGroup itemGroup) {
		this.itemGroup = itemGroup;
	}


	public String getErrorMessage() {
		return this.errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public boolean isBooked() {
		return this.booked;
	}


	public void setBooked(boolean booked) {
		this.booked = booked;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
