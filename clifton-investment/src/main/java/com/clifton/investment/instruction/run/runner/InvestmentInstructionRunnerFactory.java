package com.clifton.investment.instruction.run.runner;

import com.clifton.investment.instruction.InvestmentInstructionItem;

import java.util.Date;
import java.util.List;


/**
 * Factory to create investment instruction runners
 *
 * @author theodorez
 */
public interface InvestmentInstructionRunnerFactory {

	public InvestmentInstructionRunner createInvestmentInstructionRunner(Date runDate, List<InvestmentInstructionItem> items);
}
