package com.clifton.investment.instruction.process;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.search.InvestmentInstructionSelectionSearchForm;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentInstructionProcessorService</code> ...
 *
 * @author manderson
 */
public interface InvestmentInstructionProcessorService {

	/**
	 * For a given category and date will generate instructions for each definition in the
	 * category
	 * <p>
	 * Note - Instructions must be performed at the category level so that duplicates can be found within the category.
	 */
	@ModelAttribute("result")
	public Status processInvestmentInstructionList(short categoryId, Date date, boolean synchronous);


	////////////////////////////////////////////////////////////////////////////////
	//////////////              Instruction Selection                   ////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For a given category and date will generate list of entities and their recipients that are missing an instruction
	 */
	@SecureMethod(dtoClass = InvestmentInstruction.class)
	public List<InvestmentInstructionSelection> getInvestmentInstructionSelectionMissingList(InvestmentInstructionSelectionSearchForm searchForm);


	/**
	 * For a given definition and date will generate list of entities and their recipients that are used
	 * to process instructions, but can be used at this point for previewing.
	 */
	@SecureMethod(dtoClass = InvestmentInstruction.class)
	public List<InvestmentInstructionSelection> getInvestmentInstructionSelectionList(int definitionId, Date date);
}
