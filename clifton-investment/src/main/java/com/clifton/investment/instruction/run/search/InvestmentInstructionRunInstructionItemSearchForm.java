package com.clifton.investment.instruction.run.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;

import java.util.Date;


/**
 * @author theodorez
 */
public class InvestmentInstructionRunInstructionItemSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer instructionRunId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer instructionItemId;

	@SearchField(searchField = "referenceTwo.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] instructionItemIds;

	@SearchField(searchField = "referenceOne.runType")
	private InvestmentInstructionRunTypes runType;

	@SearchField(searchField = "referenceOne.startDate")
	private Date startDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInstructionRunId() {
		return this.instructionRunId;
	}


	public void setInstructionRunId(Integer instructionRunId) {
		this.instructionRunId = instructionRunId;
	}


	public Integer getInstructionItemId() {
		return this.instructionItemId;
	}


	public void setInstructionItemId(Integer instructionItemId) {
		this.instructionItemId = instructionItemId;
	}


	public Integer[] getInstructionItemIds() {
		return this.instructionItemIds;
	}


	public void setInstructionItemIds(Integer[] instructionItemIds) {
		this.instructionItemIds = instructionItemIds;
	}


	public InvestmentInstructionRunTypes getRunType() {
		return this.runType;
	}


	public void setRunType(InvestmentInstructionRunTypes runType) {
		this.runType = runType;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
}
