package com.clifton.investment.instruction.status;

import java.util.List;


/**
 * @author theodorez
 */
public interface InvestmentInstructionStatusService {

	public InvestmentInstructionStatus getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames name);


	public List<InvestmentInstructionStatus> getInvestmentInstructionStatusList();
}
