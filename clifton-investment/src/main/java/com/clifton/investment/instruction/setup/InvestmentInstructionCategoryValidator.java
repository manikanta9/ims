package com.clifton.investment.instruction.setup;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionDefinitionSearchForm;


/**
 * The <code>InvestmentInstructionCategoryValidator</code> validates
 * categories prior to saves:
 * 1.  SystemBeanType is associated with SystemBeanGroup "Investment Instruction Definition Selector"
 * 2.  Can't change selector bean type if there are definitions already associated with current type
 *
 * @author manderson
 */
public class InvestmentInstructionCategoryValidator extends SelfRegisteringDaoValidator<InvestmentInstructionCategory> {

	private InvestmentInstructionSetupService investmentInstructionDefinitionService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentInstructionCategory bean, DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertNotNull(bean.getSelectorBeanType(), "Selector Bean Type is required.");
		ValidationUtils.assertEquals(InvestmentInstructionCategory.CATEGORY_SELECTOR_SYSTEM_BEAN_GROUP, bean.getSelectorBeanType().getGroup().getName(),
				"Invalid bean type selected.  Category Selector Bean Type must be associated with System Bean Group [" + InvestmentInstructionCategory.CATEGORY_SELECTOR_SYSTEM_BEAN_GROUP + "].");

		// Can only change selector bean type if no definitions already associated with the category
		if (config.isUpdate()) {
			InvestmentInstructionCategory originalBean = getOriginalBean(bean);
			if (!CompareUtils.isEqual(originalBean.getSelectorBeanType(), bean.getSelectorBeanType())) {
				InvestmentInstructionDefinitionSearchForm searchForm = new InvestmentInstructionDefinitionSearchForm();
				searchForm.setInstructionCategoryId(bean.getId());
				if (!CollectionUtils.isEmpty(getInvestmentInstructionDefinitionService().getInvestmentInstructionDefinitionList(searchForm))) {
					throw new ValidationException("Cannot change selector bean type because there are already instruction definitions associated with this category.");
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionSetupService getInvestmentInstructionDefinitionService() {
		return this.investmentInstructionDefinitionService;
	}


	public void setInvestmentInstructionDefinitionService(InvestmentInstructionSetupService investmentInstructionDefinitionService) {
		this.investmentInstructionDefinitionService = investmentInstructionDefinitionService;
	}
}
