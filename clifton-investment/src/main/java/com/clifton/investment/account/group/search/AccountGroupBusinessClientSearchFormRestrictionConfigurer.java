package com.clifton.investment.account.group.search;

import com.clifton.business.client.search.BusinessClientSearchForm;
import com.clifton.core.dataaccess.search.SearchFormRestrictionConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;


/**
 * @author jonathanr
 */
@Component
public class AccountGroupBusinessClientSearchFormRestrictionConfigurer implements SearchFormRestrictionConfigurer {


	@Override
	public Class<BusinessClientSearchForm> getSearchFormClass() {
		return BusinessClientSearchForm.class;
	}


	@Override
	public String getSearchFieldName() {
		return "investmentAccountGroupId";
	}


	@Override
	public void configureCriteria(Criteria criteria, SearchRestriction restriction) {
		if (restriction.getValue() != null) {
			Integer groupId = Integer.valueOf((String) restriction.getValue());
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "link");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("link.referenceOne", "group");
			sub.createAlias("link.referenceTwo", "acct");
			sub.add(Restrictions.eqProperty("acct.businessClient", criteria.getAlias() + ".id"));
			sub.add(Restrictions.eq("group.id", groupId));
			criteria.add(Subqueries.exists(sub));
		}
	}
}
