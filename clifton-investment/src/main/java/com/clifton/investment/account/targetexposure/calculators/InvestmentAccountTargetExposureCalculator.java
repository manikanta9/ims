package com.clifton.investment.account.targetexposure.calculators;

import java.math.BigDecimal;


/**
 * <code>InvestmentAccountTargetExposureCalculator</code> is an interface that facilitates dynamic calculation of portfolio target exposure adjustments.
 * The calculated adjustments can be distributed across offsetting account asset class assignments, or for single account asset class.
 *
 * @author nickk
 */
public interface InvestmentAccountTargetExposureCalculator {

	/**
	 * Returns a calculated Target Exposure Adjustment percent for the provided {@link InvestmentAccountTargetExposureCalculatorConfig}
	 * used to adjust an account portfolio's target percentage.
	 */
	public BigDecimal calculate(InvestmentAccountTargetExposureCalculatorConfig config);


	/**
	 * Returns <code>true</code> to exclude offsetting Target Exposure Adjustments across a set of asset class assignments.
	 */
	public boolean isDoNotRequireOffsettingAdjustments();
}
