package com.clifton.investment.account.targetexposure;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.targetexposure.calculators.InvestmentAccountTargetExposureCalculator;
import com.clifton.investment.account.targetexposure.calculators.InvestmentAccountTargetExposureCalculatorConfig;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.rule.violation.RuleViolationUtil;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Component
public class InvestmentAccountTargetExposureHandlerImpl<T extends InvestmentTargetExposureAdjustmentAware<T, A>, A extends InvestmentTargetExposureAdjustmentAwareAssignment<T, A>> implements InvestmentAccountTargetExposureHandler<T, A> {

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateTargetExposureAdjustmentTypeOptions(T bean, String typeLabel, ReadOnlyDAO<A> assignmentDAO) {
		InvestmentAccount clientAccount = bean.getClientInvestmentAccount();
		ValidationUtils.assertNotNull(clientAccount, "Client Account is missing for this " + typeLabel);

		if (bean.getTargetExposureAdjustment() != null) {
			if (!validateTargetExposureAdjustment(bean, bean.getTargetExposureAdjustment(), typeLabel, assignmentDAO)) {
				bean.setTargetExposureAdjustment(null);
			}
		}
		if (bean.getCashAdjustment() != null) {
			if (!validateTargetExposureAdjustment(bean, bean.getCashAdjustment(), typeLabel, assignmentDAO)) {
				bean.setCashAdjustment(null);
			}
		}
	}


	/**
	 * Returns true if the adjustment is not null and a valid adjustment according to {@link InvestmentTargetExposureAdjustmentTypes#isTargetExposureUsed(InvestmentTargetExposureAdjustmentTypes)}
	 * If false is returned, the adjustment can be cleared from the {@link InvestmentTargetExposureAdjustmentAware} entity to avoid persisting unused data.
	 */
	private boolean validateTargetExposureAdjustment(T bean, InvestmentTargetExposureAdjustment adjustment, String typeLabel, ReadOnlyDAO<A> assignmentDAO) {
		if (adjustment != null) {
			InvestmentAccount clientAccount = bean.getClientInvestmentAccount();
			if (InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(adjustment.getTargetExposureAdjustmentType())) {
				// Make sure option is allowed for account
				if (adjustment.getTargetExposureAdjustmentType().getRestrictedProcessingTypes() != null && adjustment.getTargetExposureAdjustmentType().getRestrictedProcessingTypes().length > 0) {
					if (clientAccount.getServiceProcessingType() == null || !ArrayUtils.contains(adjustment.getTargetExposureAdjustmentType().getRestrictedProcessingTypes(), clientAccount.getServiceProcessingType().getProcessingType())) {
						throw new ValidationException("Selected account [" + clientAccount.getLabel() + "] does not support selected target adjustment type of [" + adjustment.getTargetExposureAdjustmentType().name());
					}
				}

				// Make sure not being used by another TFA
				if (!bean.isNewBean()) {
					validateTargetExposureAdjustmentAssignmentUnused(bean, typeLabel, "set as a Target Exposure Adjustment", assignmentDAO);
				}
				if (adjustment.getTargetExposureAdjustmentType().isAmountFieldRequired()) {
					ValidationUtils.assertNotNull(adjustment.getTargetExposureAmount(), "A value for [" + adjustment.getTargetExposureAdjustmentType().getAmountFieldName() + "] is required if the Target Exposure Adjustment type is [" + adjustment.getTargetExposureAdjustmentType().name() + "].",
							"targetExposureAmount");
				}
				else {
					adjustment.setTargetExposureAmount(null);
				}

				if (adjustment.getTargetExposureAdjustmentType().isManagerFieldRequired()) {
					ValidationUtils.assertNotNull(adjustment.getTargetExposureManagerAccount(), "A selected manager account is required if the Target Exposure Adjustment type is [" + adjustment.getTargetExposureAdjustmentType().name() + "].");
				}
				else {
					adjustment.setTargetExposureManagerAccount(null);
				}
				if (adjustment.getTargetExposureManagerAccount() != null) {
					if (!clientAccount.getBaseCurrency().equals(adjustment.getTargetExposureManagerAccount().getBaseCurrency())) {
						// We cannot Convert Manager Amount to the Account's Base Currency here (need market data project) so if this happens throw an exception
						throw new ValidationException("Selected target manager must use the same base currency as the client account.");
					}
				}

				if (adjustment.getTargetExposureAdjustmentType().isDynamicTargetCalculatorRequired()) {
					ValidationUtils.assertNotNull(adjustment.getDynamicTargetCalculatorBean(), "A selected dynamic target calculator is required if the Target Exposure Adjustment type is [" + adjustment.getTargetExposureAdjustmentType().name() + "].");
				}
				else {
					adjustment.setDynamicTargetCalculatorBean(null);
				}
			}
			else {
				adjustment.setTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes.NONE);
				adjustment.setTargetExposureAmount(null);
				adjustment.setTargetExposureManagerAccount(null);
				adjustment.setDynamicTargetCalculatorBean(null);
			}

			// Validate Assignments
			validateTargetExposureAdjustmentAssignmentList(bean, adjustment, typeLabel);
		}

		return adjustment != null && InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(adjustment.getTargetExposureAdjustmentType());
	}


	private void validateTargetExposureAdjustmentAssignmentList(T bean, InvestmentTargetExposureAdjustment adjustment, String typeLabel) {
		if (isTargetExposureAdjustmentAssignmentListUsed(adjustment)) {
			List<A> assignments = adjustment.isCashAdjustment() ? bean.getCashAdjustmentAssignmentList() : bean.getTargetAdjustmentAssignmentList();
			if (CollectionUtils.isEmpty(assignments)) {
				throw new ValidationException(
						"Target Exposure " + typeLabel + " Assignments require at least one child assignment selected.  Please select a child assignment, or uncheck target exposure selection.");
			}

			BigDecimal total = BigDecimal.ZERO;
			boolean propFound = false;
			for (A assign : assignments) {
				T referenceTwo = assign.getReferenceTwo();
				InvestmentTargetExposureAdjustment referenceTwoTargetAdjustment = adjustment.isCashAdjustment() ? referenceTwo.getCashAdjustment() : referenceTwo.getTargetExposureAdjustment();
				if (referenceTwo.equals(bean)) {
					throw new ValidationException("You cannot assign a Target Exposure Assignment Target to itself.");
				}
				else if (referenceTwoTargetAdjustment != null && InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(referenceTwoTargetAdjustment.getTargetExposureAdjustmentType())) {
					throw new ValidationException("You cannot assign a Target Exposure " + typeLabel + " Target to another Target Exposure " + typeLabel + " Target. ["
							+ referenceTwo.getLabel() + "] is marked as Target Exposure adjusted.");
				}
				else if (referenceTwo.isInactive()) {
					throw new ValidationException("You cannot assign a Target Exposure " + typeLabel + " Target to an inactive " + typeLabel + ". [" + referenceTwo.getLabel()
							+ "] is marked as inactive.");
				}
				if (assign.isProportionalAllocation()) {
					propFound = true;

					if (MathUtils.isNullOrZero(referenceTwo.getTargetPercent())) {
						throw new ValidationException("You cannot use a proportional allocation for " + typeLabel + " [" + referenceTwo.getLabel()
								+ "] because the target for that point is 0%.  Proportional allocation will always result in 0 value.  Please specify an actual percent.");
					}

					ValidationUtils.assertNull(
							assign.getAllocationPercent(),
							"Allocation percent can be entered only when the proportional option is not being used.  Please either uncheck the proportional checkbox, or clear the specified percentage for " + typeLabel + " assignment ["
									+ referenceTwo.getLabel() + "].", "allocationPercentage");
				}
				else {
					ValidationUtils.assertNotNull(assign.getAllocationPercent(), "Allocation percent is required for each assignment that is not flagged as proportional.", "allocationPercentage");
				}
				if (assign.getAllocationPercent() != null) {
					total = total.add(assign.getAllocationPercent());
				}
				assign.setReferenceOne(bean);
				ValidationUtils.assertTrue(bean.getClientInvestmentAccount().equals(referenceTwo.getClientInvestmentAccount()), "Target Exposure Adjustment Assignments must belong to the same Client Account.");
			}
			if (MathUtils.isGreaterThan(total, 100)) {
				throw new ValidationException("You cannot create assignments with a total allocation percentages greater than 100%.  Current total is [" + CoreMathUtils.formatNumberDecimal(total) + ".");
			}

			if (!propFound && MathUtils.isLessThan(total, 100)) {
				throw new ValidationException("At least one of the child assignments must be selected as a proportional distribution, or the total allocation percentages must equal 100%");
			}
		}
		else {
			// Clear the list for this adjustment, set to the other adjustment list
			List<A> assignments = adjustment.isCashAdjustment() ? bean.getTargetAdjustmentAssignmentList() : bean.getCashAdjustmentAssignmentList();
			bean.setTargetExposureAssignmentCombinedList(assignments);
		}
	}


	@Override
	public boolean isTargetExposureAdjustmentAssignmentListUsed(InvestmentTargetExposureAdjustment adjustment) {
		if (adjustment == null || !InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(adjustment.getTargetExposureAdjustmentType())) {
			return false;
		}

		if (adjustment.getDynamicTargetCalculatorBean() == null) {
			return true;
		}

		InvestmentAccountTargetExposureCalculator calculator = (InvestmentAccountTargetExposureCalculator) getSystemBeanService().getBeanInstance(adjustment.getDynamicTargetCalculatorBean());
		return !calculator.isDoNotRequireOffsettingAdjustments();
	}


	@Override
	public void validateTargetExposureAdjustmentAssignmentUnused(T bean, String typeLabel, String actionDescription, ReadOnlyDAO<A> assignmentDAO) {
		List<A> assignmentList = assignmentDAO.findByField("referenceTwo.id", bean.getIdentity());
		if (!CollectionUtils.isEmpty(assignmentList)) {
			throw new ValidationException("This " + typeLabel + " cannot be " + actionDescription + ", because it is being used for Target Exposure Adjustments for the following ["
					+ BeanUtils.getPropertyValues(assignmentList, "referenceOne.label", ","));
		}
	}


	@Override
	public BigDecimal calculateTargetExposureAdjustedAmount(T bean, InvestmentTargetExposureAdjustment adjustment, BigDecimal currentTargetAmount, BigDecimal currentActualAmount, Date date, BigDecimal totalPortfolioValue, boolean marketOnCloseAdjustmentsIncluded) {
		if (adjustment == null) {
			return BigDecimal.ZERO;
		}
		switch (adjustment.getTargetExposureAdjustmentType()) {
			case AFT: {
				// Adjust target percentages if AFT (After Follow Target) is used
				return currentTargetAmount;
			}
			case TFA: {
				// Adjust target percentages if TFA (Target Follows Actual) is used
				// The difference between target TFA allocation and actual allocation needs to be redistributed across other asset classes
				// Either proportional or weighted allocation distribution can be used for any subset of NONE (Unadjusted) asset classes
				// We do not change exposure for asset classes marked as TFA (some asset classes are very difficult to replicate: alternative investments, hedge funds, etc.)
				return currentActualAmount;
			}
			case CONSTANT: {
				// Adjust target asset class percentages if CUSTOM target amount is used
				// Set actual as specified value
				return adjustment.getTargetExposureAmount();
			}
			case PERCENT_OF_TARGET: {
				// Adjust target percentages if PERCENT_OF_TARGET target amount change is used
				// Set actual as specified value
				return MathUtils.add(currentTargetAmount,
						MathUtils.getPercentageOf(adjustment.getTargetExposureAmount(), currentTargetAmount, true));
			}
			case ABSOLUTE_PERCENT: {
				// Set actual as original target % +/- percent adjustment
				return MathUtils.round(
						MathUtils.getPercentageOf(MathUtils.add(bean.getTargetPercent(), adjustment.getTargetExposureAmount()),
								totalPortfolioValue, true), 2);
			}
			case MANAGER_MARKET_VALUE: {
				// Set actual as the manager balance for the selected manager on the balance date
				InvestmentManagerAccount managerAccount = adjustment.getTargetExposureManagerAccount();
				if (managerAccount == null) {
					throw new ValidationExceptionWithCause("InvestmentAccountAssetClass", (Integer) bean.getIdentity(), "[" + bean.getLabel()
							+ "] has selected " + InvestmentTargetExposureAdjustmentTypes.MANAGER_MARKET_VALUE
							+ " as the target exposure adjustment type, however the selected manager to set target to is missing.");
				}
				if (!bean.getClientInvestmentAccount().getBaseCurrency().equals(managerAccount.getBaseCurrency())) {
					// We cannot Convert Manager Amount to the Account's Base Currency here (need market data project) so if this happens throw an exception
					throw new ValidationExceptionWithCause("InvestmentAccountAssetClass", (Integer) bean.getIdentity(), "[" + bean.getLabel()
							+ "] has selected " + InvestmentTargetExposureAdjustmentTypes.MANAGER_MARKET_VALUE
							+ " as the target exposure adjustment type, however the selected manager uses a difference base ccy than the account.  Please use a manager whose balance is the same base CCY as the account.");
				}

				InvestmentManagerAccountBalance managerBalance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(managerAccount.getId(), date,
						false);
				// Pre-PIOS run processing should already enforce Linked Manager Balances have been loaded processed.  Not sure if we can trigger
				// it again if missing here because of circular dependencies...most times this will be linked or rollup, so should exist.
				if (!RuleViolationUtil.isRuleViolationAwareReadyForUse(managerBalance)) {
					throw new ValidationExceptionWithCause("InvestmentManagerAccount", managerAccount.getId(), "[" + bean.getLabel() + "] has selected "
							+ InvestmentTargetExposureAdjustmentTypes.MANAGER_MARKET_VALUE + " as the target exposure adjustment type, however the selected manager [" + managerAccount.getLabel()
							+ "] is missing or has an unprocessed balance on [" + DateUtils.fromDateShort(date) + "].");
				}
				return (marketOnCloseAdjustmentsIncluded ? managerBalance.getMarketOnCloseTotalValue() : managerBalance.getAdjustedTotalValue());
			}
			case DYNAMIC_TARGET_PERCENT: {
				// Use calculator bean to get percentage adjustment. Use the percent to calculate an amount of total portfolio value to add to the current target
				InvestmentAccountTargetExposureCalculator targetExposureCalculator = (InvestmentAccountTargetExposureCalculator) getSystemBeanService().getBeanInstance(adjustment.getDynamicTargetCalculatorBean());
				InvestmentAccountTargetExposureCalculatorConfig config = InvestmentAccountTargetExposureCalculatorConfig.forClientAccountOnBalanceDate(bean.getClientInvestmentAccount(), date);
				BigDecimal calculatedPercentage = targetExposureCalculator.calculate(config);
				BigDecimal calculatedAmount = MathUtils.round(MathUtils.getPercentageOf(calculatedPercentage, totalPortfolioValue, true), 2);
				return CoreMathUtils.sum(currentTargetAmount, calculatedAmount);
			}
			default: {
				return BigDecimal.ZERO;
			}
		}
	}


	@Override
	public Map<T, BigDecimal> calculateTargetExposureAdjustmentMap(T bean, InvestmentTargetExposureAdjustment adjustment, BigDecimal totalAdjustment, Map<Serializable, BigDecimal> proportionalMapOverride) {
		Map<T, BigDecimal> adjustmentMap = new HashMap<>();
		// First Determine Specific Percentages for Proportional Distribution
		List<A> assignList = adjustment.isCashAdjustment() ? bean.getCashAdjustmentAssignmentList() : bean.getTargetAdjustmentAssignmentList();
		Map<Boolean, List<A>> proportionalAssignmentListMap = BeanUtils.getBeansMap(assignList, InvestmentTargetExposureAdjustmentAwareAssignment::isProportionalAllocation);
		List<A> proportionalList = proportionalAssignmentListMap.get(Boolean.TRUE);
		assignList = proportionalAssignmentListMap.get(Boolean.FALSE);
		BigDecimal proportionalAdjustment = totalAdjustment;
		for (A assign : CollectionUtils.getIterable(assignList)) {
			BigDecimal adjust = MathUtils.round(MathUtils.getPercentageOf(assign.getAllocationPercent(), totalAdjustment, true), 2);
			// Put the adjustment into the map
			adjustmentMap.put(assign.getReferenceTwo(), adjust);
			// Remove amount from what's available for proportional adjustments
			proportionalAdjustment = MathUtils.subtract(totalAdjustment, adjust);
		}
		if (!CollectionUtils.isEmpty(proportionalList)) {
			BigDecimal proportionalTotal = CoreMathUtils.sumProperty(proportionalList, targetExposureAssignment -> targetExposureAssignment.getReferenceTwo() == null ? null : targetExposureAssignment.getReferenceTwo().getTargetPercent());
			if (proportionalMapOverride != null) {
				proportionalTotal = BigDecimal.ZERO;
				for (A assign : CollectionUtils.getIterable(proportionalList)) {
					proportionalTotal = MathUtils.add(proportionalMapOverride.get(assign.getReferenceTwo().getIdentity()), proportionalTotal);
				}
			}
			for (A assign : CollectionUtils.getIterable(proportionalList)) {
				BigDecimal percentage = CoreMathUtils.getPercentValue(proportionalMapOverride != null ? proportionalMapOverride.get(assign.getReferenceTwo().getIdentity()) : assign.getReferenceTwo().getTargetPercent(), proportionalTotal, true);
				BigDecimal adjust = MathUtils.round(MathUtils.getPercentageOf(percentage, proportionalAdjustment, true), 2);
				// Put adjustment into the map
				adjustmentMap.put(assign.getReferenceTwo(), adjust);
			}
		}
		return adjustmentMap;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
