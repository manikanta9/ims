package com.clifton.investment.account.securitytarget.history;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.history.search.InvestmentAccountSecurityTargetHistorySearchForm;

import java.util.List;


/**
 * A service designed to generate lists of InvestmentAccountSecurityTargetHistory.  The entities in the list are
 * not persisted, so this service does not have save and delete methods for these entities.
 *
 * @author davidi
 */
public interface InvestmentAccountSecurityTargetHistoryService {

	@SecureMethod(dtoClass = InvestmentAccountSecurityTarget.class)
	public List<InvestmentAccountSecurityTargetHistory> getInvestmentAccountSecurityTargetHistoryList(InvestmentAccountSecurityTargetHistorySearchForm searchForm);
}
