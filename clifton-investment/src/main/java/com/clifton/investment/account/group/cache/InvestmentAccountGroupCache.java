package com.clifton.investment.account.group.cache;

import com.clifton.investment.account.InvestmentAccount;

import java.util.List;


/**
 * The <code>InvestmentAccountGroupCache</code> contains a lazily built cache for
 * a mapping of account groups to a set of the accounts that fall within them.
 *
 * @author apopp
 */
public interface InvestmentAccountGroupCache {

	/**
	 * Note: Method is public so pre-load cache job can build the cache for groups
	 */
	public void buildGroupCache(final String groupName);


	public void clearGroupCache(final String groupName);


	public boolean isInvestmentAccountInGroup(final String groupName, final int accountId);


	public List<InvestmentAccount> getInvestmentAccountListByGroup(final String groupName);
}
