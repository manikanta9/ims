package com.clifton.investment.account.assetclass;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.assetclass.search.InvestmentAccountAssetClassBenchmarkSearchForm;
import com.clifton.investment.account.assetclass.search.InvestmentAccountAssetClassSearchForm;
import com.clifton.investment.replication.InvestmentReplication;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountAssetClassService</code> ...
 *
 * @author Mary Anderson
 */
public interface InvestmentAccountAssetClassService {

	////////////////////////////////////////////////////////////////////////////
	//////          Investment Account Asset Class Methods               ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClass getInvestmentAccountAssetClass(int id);


	public List<InvestmentAccountAssetClass> getInvestmentAccountAssetClassListByAccount(int accountId);


	public List<InvestmentAccountAssetClass> getInvestmentAccountAssetClassList(InvestmentAccountAssetClassSearchForm searchForm);


	/**
	 * Saves the specified asset class and updates, if necessary, rollup information.
	 */
	public InvestmentAccountAssetClass saveInvestmentAccountAssetClass(InvestmentAccountAssetClass bean);


	/**
	 * Only allows updates, no inserts, and will also retrieve the list of target exposure adjustment assignments prior to calling save on each individual record
	 * Used from the upload service which has additional restrictions
	 */
	@DoNotAddRequestMapping
	public void saveInvestmentAccountAssetClassList(List<InvestmentAccountAssetClass> beanList);


	/**
	 * Sets all asset classes with a secondary replication amount set equal to NULL
	 * Note: Used by a batch job that runs nightly to clear all input values
	 */
	@SecureMethod(dtoClass = InvestmentAccountAssetClass.class)
	public void clearInvestmentAccountAssetClassSecondaryAmount();


	/**
	 * DE-ACTIVATES the specified asset class and updates, if necessary, rollup information.
	 * NOTE: DELETE IS NOT SUPPORTED, WHEN USERS DELETE, IT ACTUALLY MARKS IT AS INACTIVE AND
	 * IS NO LONGER VISIBLE TO THE USER.  WHEN THE USER ADDS A NEW ONE WITH THE SAME ASSET CLASS
	 * AND AN INACTIVE ONE, THE INACTIVE ONE IS REACTIVATED AND THE PROPERTIES ARE COPIED OVER.
	 */
	public void deleteInvestmentAccountAssetClass(int id);


	////////////////////////////////////////////////////////////////////////////
	/////        Investment Account Asset Class Rollup Methods            //////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sets the parent on the child account asset class bean and saves it
	 */
	@SecureMethod(dtoClass = InvestmentAccountAssetClass.class)
	public void linkInvestmentAccountAssetClassRollup(int parentId, int childId);


	/**
	 * Clears the parent on the child account asset class bean and saves it
	 */
	@SecureMethod(dtoClass = InvestmentAccountAssetClass.class)
	public void deleteInvestmentAccountAssetClassRollup(int id);


	////////////////////////////////////////////////////////////////////////////
	/////         Investment Account Asset Class Assignment Methods        /////
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentAccountAssetClassAssignment> getInvestmentAccountAssetClassAssignmentListByParent(int parentId);


	////////////////////////////////////////////////////////////////////////////
	/////     Investment Account Asset Class Benchmark Business Methods    /////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassBenchmark getInvestmentAccountAssetClassBenchmark(int id);


	public List<InvestmentAccountAssetClassBenchmark> getInvestmentAccountAssetClassBenchmarkList(InvestmentAccountAssetClassBenchmarkSearchForm searchForm);


	public List<InvestmentAccountAssetClassBenchmark> getInvestmentAccountAssetClassBenchmarkListActiveMonthToDate(int assetClassId, Date date);


	public InvestmentAccountAssetClassBenchmark saveInvestmentAccountAssetClassBenchmark(InvestmentAccountAssetClassBenchmark bean);


	public void deleteInvestmentAccountAssetClassBenchmark(int id);


	////////////////////////////////////////////////////////////////////////////
	/////        Investment Account Asset Class Replication Methods        /////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Custom getter that creates a list of replications that are associated with the Account Asset Class as:
	 * 1. Primary Replication
	 * 2. Secondary Replication
	 * 3. Any Matching Replication used by the Primary or Secondary replication
	 * <p/>
	 * Currently used for explicit position allocation to assign a security quantity to a specific replication in the asset class if it is used by multiple (i.e. primary and secondary)
	 */
	public List<InvestmentReplication> getInvestmentReplicationListForAccountAssetClass(int accountAssetClassId);


	////////////////////////////////////////////////////////////////////////////////
	//////     Investment Account Asset Class Security Allowance Methods     ///////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Goes through the selected account's account asset classes and replications and returns object that contains which asset classes/replications that security
	 * is considered valid for.
	 *
	 * @param balanceDate - NOT required.  If blank defaults to previous week day which is generally if we are trading today, we are looking at previous business day's run.
	 */
	@DoNotAddRequestMapping
	public InvestmentAccountAssetClassSecurityAllowance getInvestmentAccountAssetClassSecurityAllowance(int clientAccountId, int securityId, Date balanceDate);
}
