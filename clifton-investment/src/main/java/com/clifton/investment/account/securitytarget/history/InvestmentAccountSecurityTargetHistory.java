package com.clifton.investment.account.securitytarget.history;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.AssertUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.core.util.MathUtils;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The InvestmentAccountSecurityTargetHistory entity is a class designed to show a historical entry for a security target.  It is a subclass of {@link InvestmentAccountSecurityTarget}
 * representing the historical.  The purpose for this entity is to allow for the calculation of fields used to track a underlying quantity or underlying notional differences
 * (depending on security target type) between the two historical {@link InvestmentAccountSecurityTarget} entities.
 *
 * @author davidi
 */
@NonPersistentObject
public class InvestmentAccountSecurityTargetHistory implements IdentityObject {

	private InvestmentAccountSecurityTarget securityTarget;

	private BigDecimal quantityOrNotionalDifference;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetHistory(InvestmentAccountSecurityTarget securityTarget, InvestmentAccountSecurityTarget previousSecurityTarget) {
		AssertUtils.assertNotNull(securityTarget, "The specified security target must be defined (not null).");
		setSecurityTarget(securityTarget);
		calculateQuantityOrNotionalDifference(previousSecurityTarget);
	}


	private void calculateQuantityOrNotionalDifference(InvestmentAccountSecurityTarget previousSecurityTarget) {
		boolean useQuantity = getSecurityTarget().getTargetUnderlyingQuantity() != null;

		if (previousSecurityTarget != null) {
			setQuantityOrNotionalDifference(useQuantity ? MathUtils.subtract(getSecurityTarget().getTargetUnderlyingQuantity(), previousSecurityTarget.getTargetUnderlyingQuantity()) :
					MathUtils.subtract(getSecurityTarget().getTargetUnderlyingNotional(), previousSecurityTarget.getTargetUnderlyingNotional()));
		}
		else {
			setQuantityOrNotionalDifference(useQuantity ? getSecurityTarget().getTargetUnderlyingQuantity() : getSecurityTarget().getTargetUnderlyingNotional());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTarget getSecurityTarget() {
		return this.securityTarget;
	}


	public void setSecurityTarget(InvestmentAccountSecurityTarget securityTarget) {
		this.securityTarget = securityTarget;
	}


	public BigDecimal getQuantityOrNotionalDifference() {
		return this.quantityOrNotionalDifference;
	}


	private void setQuantityOrNotionalDifference(BigDecimal quantityOrNotionalDifference) {
		this.quantityOrNotionalDifference = quantityOrNotionalDifference;
	}


	@Override
	public Serializable getIdentity() {
		return getSecurityTarget() != null ? getSecurityTarget().getId() : null;
	}


	@Override
	public boolean isNewBean() {
		return false;
	}
}
