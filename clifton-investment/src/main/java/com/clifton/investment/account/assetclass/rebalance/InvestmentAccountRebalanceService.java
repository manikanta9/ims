package com.clifton.investment.account.assetclass.rebalance;


import com.clifton.investment.account.assetclass.rebalance.search.InvestmentAccountRebalanceAssetClassHistorySearchForm;
import com.clifton.investment.account.assetclass.rebalance.search.InvestmentAccountRebalanceHistorySearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountRebalanceService</code> ...
 *
 * @author manderson
 */
public interface InvestmentAccountRebalanceService {

	//////////////////////////////////////////////////////////////////////////
	////////////        Investment Account Rebalance Methods      //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRebalance getInvestmentAccountRebalance(int id);


	@RequestMapping("investmentAccountRebalanceSaveConfig")
	public InvestmentAccountRebalance saveInvestmentAccountRebalance(InvestmentAccountRebalance bean);


	public void deleteInvestmentAccountRebalance(int id);


	////////////////////////////////////////////////////////////////////////////
	////////        Investment Account Rebalance History Methods      //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRebalanceHistory getInvestmentAccountRebalanceHistory(int id);


	/**
	 * Returns the InvestmentAccountRebalanceHistory record that is on or before given date.  If date is null, uses "today"
	 */
	public InvestmentAccountRebalanceHistory getInvestmentAccountRebalanceHistoryRecent(int investmentAccountId, Date date);


	public List<InvestmentAccountRebalanceHistory> getInvestmentAccountRebalanceHistoryList(InvestmentAccountRebalanceHistorySearchForm searchForm);


	public InvestmentAccountRebalanceHistory saveInvestmentAccountRebalanceHistory(InvestmentAccountRebalanceHistory bean);


	/**
	 * NOTE: Special logic that you can only delete the last history record for an account
	 */
	public void deleteInvestmentAccountRebalanceHistory(int id);


	////////////////////////////////////////////////////////////////////////////
	//////    Investment Account Asset Class Rebalance History Methods   ///////
	////////////////////////////////////////////////////////////////////////////

	// NOTE: SAVES ARE DONE THROUGH THE ACCOUNT HISTORY - NO DIRECT ASSET CLASS HISTORY SAVES/DELETES


	/**
	 * Returns the InvestmentAccountRebalanceAssetClassHistory record that is on or before given date.  If date is null, uses "today"
	 */
	public InvestmentAccountRebalanceAssetClassHistory getInvestmentAccountRebalanceAssetClassHistoryRecent(int investmentAccountAssetClassId, Date date);


	public List<InvestmentAccountRebalanceAssetClassHistory> getInvestmentAccountRebalanceAssetClassHistoryList(InvestmentAccountRebalanceAssetClassHistorySearchForm searchForm);
}
