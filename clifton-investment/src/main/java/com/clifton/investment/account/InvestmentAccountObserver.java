package com.clifton.investment.account;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountObserver</code>
 * <p/>
 * When Client Accounts workflow states are changed - updates, if necessary the Client's WorkflowState/Status
 *
 * @author manderson
 */
@Component
public class InvestmentAccountObserver extends SelfRegisteringDaoObserver<InvestmentAccount> {

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private BusinessClientService businessClientService;

	private RunnerHandler runnerHandler;


	public InvestmentAccountObserver() {
		super();
		// Setting order to below default so this Observer runs before those at default.
		setOrder(-100);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<InvestmentAccount> dao, DaoEventTypes event, InvestmentAccount bean) {
		if (!event.isInsert()) {
			// Get original bean so we have it for later
			getOriginalBean(dao, bean);
		}
		else {
			ValidationUtils.assertNull(bean.getBusinessClient().getTerminateDate(), "Cannot add a new account to a terminated client.");
		}

		// If event is delete - remove it from all account groups
		if (event.isDelete()) {
			InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
			searchForm.setAccountId(bean.getId());
			List<InvestmentAccountGroupAccount> groupAccountList = getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm);
			for (InvestmentAccountGroupAccount groupAccount : CollectionUtils.getIterable(groupAccountList)) {
				getInvestmentAccountGroupService().deleteInvestmentAccountGroupAccount(groupAccount);
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<InvestmentAccount> dao, DaoEventTypes event, InvestmentAccount bean, Throwable e) {
		if (e == null && !event.isDelete()) {
			if (isWorkflowRequired(bean)) {
				InvestmentAccount original = null;
				if (event.isUpdate()) {
					original = getOriginalBean(dao, bean);
				}
				boolean updateClient = false;
				BusinessClient client = getBusinessClientService().getBusinessClient(bean.getBusinessClient().getId());

				// If an insert, or change in workflow state
				if (original == null || !original.getWorkflowState().equals(bean.getWorkflowState())) {
					// If new workflow state is different than from the client's state, reset client to the min
					if (bean.getWorkflowState().getOrder() != null && (client.getWorkflowState() == null || !client.getWorkflowState().equals(bean.getWorkflowState()))) {
						// Get an Account with the Min Workflow State Order, if after the Client Workflow, update the Client
						// For Ordering - Active was set as the Minimum, so that any client that has at least ONE active account will appear as Active
						InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
						searchForm.setOurAccount(true);
						searchForm.setClientId(client.getId());

						List<InvestmentAccount> clientAccounts = getInvestmentAccountService().getInvestmentAccountList(searchForm);

						InvestmentAccount minAccount = CollectionUtils.getFirstElement(
								BeanUtils.sortWithFunction(clientAccounts, clientAccount -> clientAccount.getWorkflowState().getOrder(), true));
						if (minAccount != null && (client.getWorkflowState() == null || !client.getWorkflowState().equals(minAccount.getWorkflowState()))) {
							client.setWorkflowState(minAccount.getWorkflowState());
							client.setWorkflowStatus(minAccount.getWorkflowStatus());
							updateClient = true;
						}
					}
				}
				// If inception date set and missing on the client (Note: Clients then push it up to the Client Relationship where missing)
				if (bean.getInceptionDate() != null && client.getInceptionDate() == null) {
					updateClient = true;
					client.setInceptionDate(bean.getInceptionDate());
				}
				if (updateClient) {
					getBusinessClientService().saveBusinessClient(client);
				}
			}
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentAccount> dao, DaoEventTypes event, @SuppressWarnings("unused") InvestmentAccount bean, Throwable e) {
		if (e == null && (event.isInsert() || event.isUpdate())) {
			/*
			 * Rebuild the Account Groups after an account is added or updated so
			 * the groups' Account list is accurate.
			 */
			submitInvestmentAccountGroupRebuildTask();
		}
	}


	private void submitInvestmentAccountGroupRebuildTask() {
		// asynchronous support
		String runId = "ALL";
		final Date scheduledDate = DateUtils.addSeconds(new Date(), 30);

		Runner runner = new AbstractStatusAwareRunner("INVESTMENT-ACCOUNT-GROUP", runId, scheduledDate) {

			@Override
			public void run() {
				getStatus().mergeStatus(getInvestmentAccountGroupService().rebuildInvestmentAccountGroupSystemManaged());
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
	}


	/**
	 * Only applies to changes to "our accounts"
	 *
	 * @param account
	 */
	private boolean isWorkflowRequired(InvestmentAccount account) {
		if (account == null) {
			return false;
		}
		InvestmentAccountType type = account.getType();
		if (type == null) {
			return false;
		}
		if (account.getWorkflowState() == null) {
			return false;
		}
		return type.isOurAccount();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
