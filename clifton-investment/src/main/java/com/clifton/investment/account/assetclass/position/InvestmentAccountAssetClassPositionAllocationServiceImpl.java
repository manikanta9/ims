package com.clifton.investment.account.assetclass.position;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountAssetClassPositionAllocationServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class InvestmentAccountAssetClassPositionAllocationServiceImpl implements InvestmentAccountAssetClassPositionAllocationService {

	private AdvancedUpdatableDAO<InvestmentAccountAssetClassPositionAllocation, Criteria> investmentAccountAssetClassPositionAllocationDAO;


	////////////////////////////////////////////////////////////////////////////
	////   Investment Account Asset Class Position Allocation Methods       ////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountAssetClassPositionAllocation getInvestmentAccountAssetClassPositionAllocation(int id) {
		return getInvestmentAccountAssetClassPositionAllocationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountAssetClassPositionAllocation> getInvestmentAccountAssetClassPositionAllocationList(int investmentAccountId, Date activeOnDate) {
		InvestmentAccountAssetClassPositionAllocationSearchForm searchForm = new InvestmentAccountAssetClassPositionAllocationSearchForm();
		searchForm.setAccountId(investmentAccountId);
		searchForm.setActiveOnDate(activeOnDate);
		return getInvestmentAccountAssetClassPositionAllocationList(searchForm);
	}


	@Override
	public List<InvestmentAccountAssetClassPositionAllocation> getInvestmentAccountAssetClassPositionAllocationList(final InvestmentAccountAssetClassPositionAllocationSearchForm searchForm) {
		return getInvestmentAccountAssetClassPositionAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public void copyInvestmentAccountAssetClassPositionAllocation(int id, Date newStartDate, Date newEndDate, BigDecimal newQuantity) {
		InvestmentAccountAssetClassPositionAllocation copyFrom = getInvestmentAccountAssetClassPositionAllocation(id);
		boolean updateExisting = false;
		if (newStartDate != null && (copyFrom.getEndDate() == null || DateUtils.compare(newStartDate, copyFrom.getEndDate(), false) < 0)) {
			copyFrom.setEndDate(DateUtils.addDays(newStartDate, -1));
			updateExisting = true;
		}

		// Create a New Allocation Bean
		InvestmentAccountAssetClassPositionAllocation newBean = BeanUtils.cloneBean(copyFrom, false, false);
		newBean.setStartDate(newStartDate);
		newBean.setEndDate(newEndDate);
		newBean.setAllocationQuantity(newQuantity);

		// Do First Date Validation here before attempting to copy (Real Date Validation across all is done in the DAO Validator)
		if (DateUtils.isOverlapInDates(copyFrom.getStartDate(), copyFrom.getEndDate(), newStartDate, newEndDate)) {
			throw new ValidationException("Start/End Dates entered are invalid as they overlap with the Position Allocation you are copying from " + copyFrom.getLabel() + ".");
		}

		if (updateExisting) {
			// just updating the end date field - use dao method in case additional logic is added to save method - just need to update end date
			getInvestmentAccountAssetClassPositionAllocationDAO().save(copyFrom);
		}
		getInvestmentAccountAssetClassPositionAllocationDAO().save(newBean);
	}


	@Override
	public InvestmentAccountAssetClassPositionAllocation saveInvestmentAccountAssetClassPositionAllocation(InvestmentAccountAssetClassPositionAllocation bean) {
		return getInvestmentAccountAssetClassPositionAllocationDAO().save(bean);
	}


	@Override
	public void saveInvestmentAccountAssetClassPositionAllocationList(List<InvestmentAccountAssetClassPositionAllocation> beanList) {
		getInvestmentAccountAssetClassPositionAllocationDAO().saveList(beanList);
	}


	@Override
	public void deleteInvestmentAccountAssetClassPositionAllocation(int id) {
		getInvestmentAccountAssetClassPositionAllocationDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                Getter & Setter Methods                    /////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentAccountAssetClassPositionAllocation, Criteria> getInvestmentAccountAssetClassPositionAllocationDAO() {
		return this.investmentAccountAssetClassPositionAllocationDAO;
	}


	public void setInvestmentAccountAssetClassPositionAllocationDAO(AdvancedUpdatableDAO<InvestmentAccountAssetClassPositionAllocation, Criteria> investmentAccountAssetClassPositionAllocationDAO) {
		this.investmentAccountAssetClassPositionAllocationDAO = investmentAccountAssetClassPositionAllocationDAO;
	}
}
