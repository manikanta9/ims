package com.clifton.investment.account.mapping.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentAccountMappingSearchForm</code> class defines search configuration for InvestmentAccountMapping objects.
 *
 * @author davidi
 */
public class InvestmentAccountMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "investmentAccountMappingPurpose.name,investmentAccountMappingPurpose.description,investmentAccount.number,fieldValue")
	private String searchPattern;

	@SearchField(sortField = "fieldValue")
	private Integer id;

	@SearchField(searchField = "investmentAccountMappingPurpose.id")
	private Short investmentAccountMappingPurposeId;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Integer[] excludeIds;

	@SearchField(searchField = "investmentAccountMappingPurpose.name")
	private String mappingPurposeName;

	@SearchField(searchField = "investmentAccountMappingPurpose.description")
	private String mappingPurposeDescription;

	@SearchField
	private String fieldValue;

	@SearchField(searchField = "fieldValue", comparisonConditions = ComparisonConditions.EQUALS)
	private String fieldValueEquals;

	@SearchField(searchField = "investmentAccount.number")
	private String accountNumber;

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.type.ourAccount")
	private Boolean ourAccount;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Integer[] getExcludeIds() {
		return this.excludeIds;
	}


	public void setExcludeIds(Integer[] excludeIds) {
		this.excludeIds = excludeIds;
	}


	public String getMappingPurposeName() {
		return this.mappingPurposeName;
	}


	public void setMappingPurposeName(String mappingPurposeName) {
		this.mappingPurposeName = mappingPurposeName;
	}


	public String getMappingPurposeDescription() {
		return this.mappingPurposeDescription;
	}


	public void setMappingPurposeDescription(String mappingPurposeDescription) {
		this.mappingPurposeDescription = mappingPurposeDescription;
	}


	public String getFieldValue() {
		return this.fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public String getFieldValueEquals() {
		return this.fieldValueEquals;
	}


	public void setFieldValueEquals(String fieldValueEquals) {
		this.fieldValueEquals = fieldValueEquals;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Short getInvestmentAccountMappingPurposeId() {
		return this.investmentAccountMappingPurposeId;
	}


	public void setInvestmentAccountMappingPurposeId(Short investmentAccountMappingPurposeId) {
		this.investmentAccountMappingPurposeId = investmentAccountMappingPurposeId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getOurAccount() {
		return this.ourAccount;
	}


	public void setOurAccount(Boolean ourAccount) {
		this.ourAccount = ourAccount;
	}
}
