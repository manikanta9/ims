package com.clifton.investment.account.securitytarget.util;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;


/**
 * @author NickK
 */
@Component
public class InvestmentAccountSecurityTargetAdjustmentUtilHandlerImpl implements InvestmentAccountSecurityTargetAdjustmentUtilHandler {

	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;


	///////////////////////////////////////////////////////////////////////////
	///////////         Target Adjustment Business Methods         ////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountSecurityTarget adjustInvestmentAccountSecurityTargetQuantity(InvestmentAccountSecurityTarget securityTarget, BigDecimal quantity) {
		return setInvestmentAccountSecurityTargetQuantity(securityTarget, MathUtils.add(securityTarget.getTargetUnderlyingQuantity(), quantity));
	}


	@Override
	public InvestmentAccountSecurityTarget adjustInvestmentAccountSecurityTargetNotionalFromQuantity(InvestmentAccountSecurityTarget securityTarget, BigDecimal quantity, BigDecimal averagePrice) {
		InvestmentNotionalCalculatorTypes notionalCalculatorType = InvestmentCalculatorUtils.getNotionalCalculator(securityTarget.getTargetUnderlyingSecurity());
		BigDecimal notionalAdjustment = notionalCalculatorType.calculateNotional(averagePrice, securityTarget.getTargetUnderlyingSecurity().getPriceMultiplier(), quantity);
		return adjustInvestmentAccountSecurityTargetNotional(securityTarget, notionalAdjustment);
	}


	@Override
	public InvestmentAccountSecurityTarget adjustInvestmentAccountSecurityTargetNotional(InvestmentAccountSecurityTarget securityTarget, BigDecimal notional) {
		return setInvestmentAccountSecurityTargetNotional(securityTarget, MathUtils.add(securityTarget.getTargetUnderlyingNotional(), notional));
	}


	///////////////////////////////////////////////////////////////////////////
	///////////         Target Setting Business Methods         ///////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountSecurityTarget setInvestmentAccountSecurityTargetQuantity(InvestmentAccountSecurityTarget securityTarget, BigDecimal quantity) {
		ValidationUtils.assertNotNull(securityTarget.getTargetUnderlyingQuantity(), String.format("Unable to adjust an Investment Account Security Target's quantity to %s because the target quantity is not defined.", quantity));

		return quantity == null ? securityTarget : saveInvestmentAccountSecurityTarget(securityTarget, target -> target.setTargetUnderlyingQuantity(quantity));
	}


	@Override
	public InvestmentAccountSecurityTarget setInvestmentAccountSecurityTargetNotional(InvestmentAccountSecurityTarget securityTarget, BigDecimal notional) {
		ValidationUtils.assertNotNull(securityTarget.getTargetUnderlyingNotional(), String.format("Unable to adjust an Investment Account Security Target's notional to %s because the target notional is not defined.", notional));

		return (notional == null) ? securityTarget : saveInvestmentAccountSecurityTarget(securityTarget, target -> target.setTargetUnderlyingNotional(notional));
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTarget(InvestmentAccountSecurityTarget securityTarget, Consumer<InvestmentAccountSecurityTarget> securityTargetAdjustmentConsumer) {
		Date today = DateUtils.clearTime(new Date());
		UnaryOperator<InvestmentAccountSecurityTarget> securityTargetSaveOperation;
		if (DateUtils.isDateBefore(securityTarget.getStartDate(), today, false) && DateUtils.isDateAfter(securityTarget.getEndDate(), today)) {
			securityTargetSaveOperation = target -> {
				// clone the security target; if in transaction the provided target may be the hibernate cached version and the update will happen to each: the ended and new target
				InvestmentAccountSecurityTarget saveSafeSecurityTarget = BeanUtils.cloneBean(target, false, true);
				// apply adjustment
				securityTargetAdjustmentConsumer.accept(saveSafeSecurityTarget);
				// save
				return getInvestmentAccountSecurityTargetService().saveInvestmentAccountSecurityTargetFromExisting(saveSafeSecurityTarget.getId(), saveSafeSecurityTarget);
			};
		}
		else if (DateUtils.isDateAfterOrEqual(securityTarget.getStartDate(), today)) {
			securityTargetSaveOperation = target -> {
				// apply adjustment
				securityTargetAdjustmentConsumer.accept(target);
				// save
				return getInvestmentAccountSecurityTargetService().saveInvestmentAccountSecurityTarget(target);
			};
		}
		else {
			securityTargetSaveOperation = UnaryOperator.identity(); // noop
		}

		return securityTargetSaveOperation.apply(securityTarget);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}
}
