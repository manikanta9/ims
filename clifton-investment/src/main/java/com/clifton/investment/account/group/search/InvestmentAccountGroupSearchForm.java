package com.clifton.investment.account.group.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


public class InvestmentAccountGroupSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField(searchField = "accountType.id")
	private Short accountTypeId;


	@SearchField(searchField = "name", searchFieldPath = "accountType", comparisonConditions = ComparisonConditions.EQUALS)
	private String accountTypeName;

	@SearchField(searchField = "name", searchFieldPath = "accountType", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL, leftJoin = true)
	private String accountTypeNameEqualsOrNull;

	@SearchField(searchField = "name", searchFieldPath = "accountType", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String accountTypeNameNotEquals;

	@SearchField(searchField = "name", searchFieldPath = "accountType", comparisonConditions = ComparisonConditions.NOT_EQUALS_OR_IS_NULL, leftJoin = true)
	private String accountTypeNameNotEqualsOrNull;

	@SearchField
	private Boolean systemDefined;

	@SearchField(searchField = "name", searchFieldPath = "entityModifyCondition")
	private String entityModifyCondition;

	@SearchField(searchField = "rebuildSystemBean.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean systemManaged;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String typeName;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = ComparisonConditions.EQUALS)
	private String typeNameEquals;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField
	private String groupAlias;


	@SearchField(searchField = "accountList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return InvestmentAccountGroup.INVESTMENT_ACCOUNT_GROUP_TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public Short getAccountTypeId() {
		return this.accountTypeId;
	}


	public void setAccountTypeId(Short accountTypeId) {
		this.accountTypeId = accountTypeId;
	}


	public String getAccountTypeName() {
		return this.accountTypeName;
	}


	public void setAccountTypeName(String accountTypeName) {
		this.accountTypeName = accountTypeName;
	}


	public String getAccountTypeNameEqualsOrNull() {
		return this.accountTypeNameEqualsOrNull;
	}


	public void setAccountTypeNameEqualsOrNull(String accountTypeNameEqualsOrNull) {
		this.accountTypeNameEqualsOrNull = accountTypeNameEqualsOrNull;
	}


	public String getAccountTypeNameNotEquals() {
		return this.accountTypeNameNotEquals;
	}


	public void setAccountTypeNameNotEquals(String accountTypeNameNotEquals) {
		this.accountTypeNameNotEquals = accountTypeNameNotEquals;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getSystemManaged() {
		return this.systemManaged;
	}


	public void setSystemManaged(Boolean systemManaged) {
		this.systemManaged = systemManaged;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(String entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getGroupAlias() {
		return this.groupAlias;
	}


	public void setGroupAlias(String groupAlias) {
		this.groupAlias = groupAlias;
	}


	public String getAccountTypeNameNotEqualsOrNull() {
		return this.accountTypeNameNotEqualsOrNull;
	}


	public void setAccountTypeNameNotEqualsOrNull(String accountTypeNameNotEqualsOrNull) {
		this.accountTypeNameNotEqualsOrNull = accountTypeNameNotEqualsOrNull;
	}


	public String getTypeNameEquals() {
		return this.typeNameEquals;
	}


	public void setTypeNameEquals(String typeNameEquals) {
		this.typeNameEquals = typeNameEquals;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}
}
