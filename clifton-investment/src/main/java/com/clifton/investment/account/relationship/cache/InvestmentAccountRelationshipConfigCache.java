package com.clifton.investment.account.relationship.cache;

/**
 * @author manderson
 */
public interface InvestmentAccountRelationshipConfigCache {

	public InvestmentAccountRelationshipConfig getInvestmentAccountRelationshipConfig(Integer investmentAccountId);


	public void setInvestmentAccountRelationshipConfig(Integer investmentAccountId, InvestmentAccountRelationshipConfig config);
}
