package com.clifton.investment.account;


import com.clifton.business.contract.BusinessContractPartyRole;
import com.clifton.business.contract.BusinessContractType;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>InvestmentAccountType</code> class classifies InvestmentAccount objects.
 * Example: Client, Custodian, Futures Broker, OTC ISDA, REPO Account, etc.
 *
 * @author vgomelsky
 */
@CacheByName
public class InvestmentAccountType extends NamedEntity<Short> {

	public static final String BROKER = "Broker";
	public static final String CLIENT = "Client";
	public static final String COLLATERAL = "Collateral";
	public static final String CUSTODIAN = "Custodian";
	public static final String ESCROW_RECEIPT = "Escrow Receipt";
	public static final String EXTERNAL_MONEY_MANAGER = "External Money Manager";
	public static final String FUTURES_BROKER = "Futures Broker";
	public static final String HOLDING_FUTURES_BROKER = "Futures Broker (Hold)";
	public static final String INVESTOR_CAPITAL = "Investor Capital";
	public static final String MONEY_MARKET = "Money Market";
	public static final String OPTIONS_BROKER = "Options Broker";
	public static final String OTC_ISDA = "OTC ISDA";
	public static final String OTC_FX_LETTER = "OTC FX Letter";
	public static final String OTC_CLEARED = "OTC Cleared";
	public static final String PRIME_BROKER = "Prime Broker";
	public static final String REPO_ACCOUNT = "Repo Account";
	public static final String TRI_PARTY_COLLATERAL = "Tri Party Collateral";
	public static final String TRI_PARTY_REPO = "Tri-Party REPO";
	public static final String MARGIN = "Margin";
	public static final String DIRECTED_BROKER = "Directed Broker";
	public static final String OTC_CLS = "OTC CLS";
	public static final String PORTFOLIO_MARGIN = "Portfolio Margin";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Optional InvestmentAccount "number" field label. If not set defaults to "Account Number"
	 */
	private String accountNumberLabel;
	/**
	 * Optional InvestmentAccount "number2" field label. If not set, then number2 field is not applicable.
	 */
	private String accountNumber2Label;

	/**
	 * For account types that can be linked to a contract, identifies contract's category: IMA, ISDA, REPO, etc.
	 * Can be used independently or with contractType
	 */
	private String contractCategory;

	/**
	 * For account types that can be linked to a contract, identifies contract's type
	 * Can be used independently or with contractCategory
	 */
	private BusinessContractType contractType;

	/**
	 * Optionally restricts the issue of the account to the following party role for selected contract: Counterparty, Client, etc.
	 */
	private BusinessContractPartyRole issuerContractPartyRole;


	/**
	 * determines if this account type represents an account issued by "us" (Client Account as opposed to Holding Account)
	 */
	private boolean ourAccount;

	/**
	 * Either doesn't belong to our client or is used for a purpose not related to our services and
	 * should not be counted towards AUM, Billing, etc.
	 * For example, "Investor Capital" account used for Fund contributions/distributions before actual trading.
	 */
	private boolean excludedAccount;

	/**
	 * Specifies whether a contract (ISDA, etc.) is required for (associated with) accounts of this type
	 */
	private boolean contractRequired;

	/**
	 * Account aliases can be assigned by issuing companies and used as a reference during communication
	 */
	private boolean aliasAllowed;

	/**
	 * Specifies whether all securities in this account must be OTC securities. Only Collateral is allowed as non-OTC in this account.
	 */
	private boolean otc;

	/**
	 * Specifies if this account requires all trades to be executed with the account's issuing company
	 */
	private boolean executionWithIssuerRequired;

	/**
	 * Specifies whether the default exchange rate source company on Investment Accounts of this type must be the same as the issuer
	 * of the holding account. This is important for Futures and Cleared Swaps that must use issuer's FX in order to reconcile Mark to Market.
	 */
	private boolean defaultExchangeRateSourceCompanySameAsIssuer;

	/**
	 * Specifies that accounts of this type require positions to be closed by the same journal type that they were opened with (i.e. if a position was opened by a transfer is has to be
	 * closed by a transfer).
	 */
	private boolean requirePositionClosingFromSameJournalType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (isOurAccount()) {
			return getName() + " (Our Account)";
		}
		return getName();
	}


	public boolean isOurAccount() {
		return this.ourAccount;
	}


	public void setOurAccount(boolean ourAccount) {
		this.ourAccount = ourAccount;
	}


	public boolean isAliasAllowed() {
		return this.aliasAllowed;
	}


	public void setAliasAllowed(boolean aliasAllowed) {
		this.aliasAllowed = aliasAllowed;
	}


	public boolean isOtc() {
		return this.otc;
	}


	public void setOtc(boolean otc) {
		this.otc = otc;
	}


	public boolean isContractRequired() {
		return this.contractRequired;
	}


	public void setContractRequired(boolean contractRequired) {
		this.contractRequired = contractRequired;
	}


	public boolean isExcludedAccount() {
		return this.excludedAccount;
	}


	public void setExcludedAccount(boolean excluded) {
		this.excludedAccount = excluded;
	}


	public String getContractCategory() {
		return this.contractCategory;
	}


	public void setContractCategory(String contractCategory) {
		this.contractCategory = contractCategory;
	}


	public BusinessContractPartyRole getIssuerContractPartyRole() {
		return this.issuerContractPartyRole;
	}


	public void setIssuerContractPartyRole(BusinessContractPartyRole issuerContractPartyRole) {
		this.issuerContractPartyRole = issuerContractPartyRole;
	}


	public BusinessContractType getContractType() {
		return this.contractType;
	}


	public void setContractType(BusinessContractType contractType) {
		this.contractType = contractType;
	}


	public boolean isExecutionWithIssuerRequired() {
		return this.executionWithIssuerRequired;
	}


	public void setExecutionWithIssuerRequired(boolean executionWithIssuerRequired) {
		this.executionWithIssuerRequired = executionWithIssuerRequired;
	}


	public boolean isDefaultExchangeRateSourceCompanySameAsIssuer() {
		return this.defaultExchangeRateSourceCompanySameAsIssuer;
	}


	public void setDefaultExchangeRateSourceCompanySameAsIssuer(boolean defaultExchangeRateSourceCompanySameAsIssuer) {
		this.defaultExchangeRateSourceCompanySameAsIssuer = defaultExchangeRateSourceCompanySameAsIssuer;
	}


	public String getAccountNumberLabel() {
		return this.accountNumberLabel;
	}


	public void setAccountNumberLabel(String accountNumberLabel) {
		this.accountNumberLabel = accountNumberLabel;
	}


	public String getAccountNumber2Label() {
		return this.accountNumber2Label;
	}


	public void setAccountNumber2Label(String accountNumber2Label) {
		this.accountNumber2Label = accountNumber2Label;
	}


	public boolean isRequirePositionClosingFromSameJournalType() {
		return this.requirePositionClosingFromSameJournalType;
	}


	public void setRequirePositionClosingFromSameJournalType(boolean requirePositionClosingFromSameJournalType) {
		this.requirePositionClosingFromSameJournalType = requirePositionClosingFromSameJournalType;
	}
}
