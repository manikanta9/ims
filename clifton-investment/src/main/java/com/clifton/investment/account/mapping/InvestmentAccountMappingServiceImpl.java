package com.clifton.investment.account.mapping;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.mapping.search.InvestmentAccountMappingPurposeSearchForm;
import com.clifton.investment.account.mapping.search.InvestmentAccountMappingSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * This class provides the implementation for a the AccountMappingService used for mapping an InvestmentAccount entity to a value.
 * The value's meaning is determined  by the InvestmentAccountMappingPurpose. The service is currently used to map a HoldingAccount
 * to a specific value such as a broker's internal account number, a block account number, or a WEX Portfolio name.
 *
 * @author davidi
 */
@Service
public class InvestmentAccountMappingServiceImpl implements InvestmentAccountMappingService {

	private AdvancedUpdatableDAO<InvestmentAccountMapping, Criteria> investmentAccountMappingDAO;
	private AdvancedUpdatableDAO<InvestmentAccountMappingPurpose, Criteria> investmentAccountMappingPurposeDAO;


	////////////////////////////////////////////////////////////////////////////
	//////      Investment Account Mapping Business Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountMapping getInvestmentAccountMapping(Integer id) {
		return getInvestmentAccountMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountMapping> getInvestmentAccountMappingList(InvestmentAccountMappingSearchForm searchForm) {
		return getInvestmentAccountMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountMapping getInvestmentAccountMappingByAccountNumberAndPurpose(InvestmentAccount account, InvestmentAccountMappingPurpose mappingPurpose) {
		if (account == null || mappingPurpose == null) {
			return null;
		}

		InvestmentAccountMappingSearchForm searchForm = new InvestmentAccountMappingSearchForm();
		searchForm.setInvestmentAccountId(account.getId());
		searchForm.setInvestmentAccountMappingPurposeId(mappingPurpose.getId());

		// due to unique index in table, the list will contain a maximum of one item
		List<InvestmentAccountMapping> accountMappingList = getInvestmentAccountMappingList(searchForm);
		return CollectionUtils.getFirstElement(accountMappingList);
	}


	@Override
	public List<InvestmentAccountMapping> getInvestmentAccountMappingListByMappedValueAndPurpose(String mappedValue, InvestmentAccountMappingPurpose mappingPurpose) {
		ValidationUtils.assertNotNull(mappingPurpose, "The InvestmentAccountMappingPurpose is required.");

		InvestmentAccountMappingSearchForm searchForm = new InvestmentAccountMappingSearchForm();
		searchForm.setFieldValueEquals(mappedValue);
		searchForm.setInvestmentAccountMappingPurposeId(mappingPurpose.getId());
		return getInvestmentAccountMappingList(searchForm);
	}


	@Transactional
	@Override
	public InvestmentAccountMapping saveInvestmentAccountMapping(InvestmentAccountMapping investmentAccountMapping) {
		validateOneToOneMapping(investmentAccountMapping);
		return getInvestmentAccountMappingDAO().save(investmentAccountMapping);
	}


	@Override
	public void deleteInvestmentAccountMapping(int id) {
		getInvestmentAccountMappingDAO().delete(id);
	}


	/**
	 * A function that attempts to look up a translated account number based on the account number provided in the accountNumber parameter.
	 * The accountTranslationMappingPurposeName parameter identifies the purpose of the mapping.
	 *
	 * @param account        InvestmentAccount for which to lookup a translated value
	 * @param mappingPurpose the mapping purpose to be used in the mapping lookup process
	 * @return The mapped account value if found.  Null if no mapping is found.
	 */
	@Override
	public String getInvestmentAccountMappedValue(InvestmentAccount account, InvestmentAccountMappingPurpose mappingPurpose) {
		InvestmentAccountMapping mapping = getInvestmentAccountMappingByAccountNumberAndPurpose(account, mappingPurpose);
		return mapping == null ? null : mapping.getFieldValue();
	}


	/**
	 * Performs a lookup of a mapped account given the mapped value.  This is support only for accounts
	 * with a 1:1 mapping.  If the search returns multiple InvestmentAccountMapping entries, a Runtime Exception is thrown.
	 *
	 * @param mappedValue
	 * @param mappingPurpose the mapping purpose to be used in the mapping lookup process
	 * @return the account number mapped to the value for the specified mapping purpose. If a reverse mapping is not found, returns null.
	 */
	@Override
	public String getInvestmentAccountReverseMapping(String mappedValue, InvestmentAccountMappingPurpose mappingPurpose) {
		List<InvestmentAccountMapping> mappingList = getInvestmentAccountMappingListByMappedValueAndPurpose(mappedValue, mappingPurpose);

		if (CollectionUtils.getSize(mappingList) > 1) {
			String message = String.format("Reverse mapping for value '%s' and purpose '%s' returned multiple accounts.", mappedValue, mappingPurpose.getName());
			LogUtils.error(InvestmentAccountMappingService.class, message);
			throw new RuntimeException(message);
		}

		return CollectionUtils.isEmpty(mappingList) ? null : mappingList.get(0).getInvestmentAccount().getNumber();
	}


	////////////////////////////////////////////////////////////////////////////
	//////    Investment Account Mapping Purpose Business Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountMappingPurpose getInvestmentAccountMappingPurpose(Short id) {
		return getInvestmentAccountMappingPurposeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountMappingPurpose> getInvestmentAccountMappingPurposeList(InvestmentAccountMappingPurposeSearchForm searchForm) {
		return getInvestmentAccountMappingPurposeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountMappingPurpose saveInvestmentAccountMappingPurpose(InvestmentAccountMappingPurpose investmentAccountMappingPurpose) {
		return getInvestmentAccountMappingPurposeDAO().save(investmentAccountMappingPurpose);
	}


	@Override
	public void deleteInvestmentAccountMappingPurpose(InvestmentAccountMappingPurpose investmentAccountMappingPurpose) {
		getInvestmentAccountMappingPurposeDAO().delete(investmentAccountMappingPurpose);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates that an account mapping does not exist for the mapped value and purpose.
	 *
	 * @param mapping
	 */
	private void validateOneToOneMapping(InvestmentAccountMapping mapping) {
		if (mapping.getInvestmentAccountMappingPurpose().isOneAccountValueMappingPerPurpose()) {
			List<InvestmentAccountMapping> mappingList = getInvestmentAccountMappingListByMappedValueAndPurpose(mapping.getFieldValue(), mapping.getInvestmentAccountMappingPurpose());
			if (!CollectionUtils.isEmpty(mappingList)) {
				throw new ValidationException(
						String.format("A mapping to value '%s' already exists for account '%s'. Multiple mappings to the same value are not allowed for 1:1 mappings.",
								mapping.getFieldValue(), mappingList.get(0).getInvestmentAccount().getNumber()));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentAccountMapping, Criteria> getInvestmentAccountMappingDAO() {
		return this.investmentAccountMappingDAO;
	}


	public void setInvestmentAccountMappingDAO(AdvancedUpdatableDAO<InvestmentAccountMapping, Criteria> investmentAccountMappingDAO) {
		this.investmentAccountMappingDAO = investmentAccountMappingDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountMappingPurpose, Criteria> getInvestmentAccountMappingPurposeDAO() {
		return this.investmentAccountMappingPurposeDAO;
	}


	public void setInvestmentAccountMappingPurposeDAO(AdvancedUpdatableDAO<InvestmentAccountMappingPurpose, Criteria> investmentAccountMappingPurposeDAO) {
		this.investmentAccountMappingPurposeDAO = investmentAccountMappingPurposeDAO;
	}
}
