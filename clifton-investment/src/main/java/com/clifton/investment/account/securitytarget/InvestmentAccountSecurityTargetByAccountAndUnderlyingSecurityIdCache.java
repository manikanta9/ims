package com.clifton.investment.account.securitytarget;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyFlexibleDaoCache;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The {@link InvestmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache} is a flexible cache for storing {@link InvestmentAccountSecurityTarget} entities and their
 * applicability by date.
 *
 * @author MikeH
 */
@Component
public class InvestmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache extends SelfRegisteringCompositeKeyFlexibleDaoCache<InvestmentAccountSecurityTarget, Date, Integer, Integer> {

	@Override
	protected String getBeanKey1Property() {
		return "clientInvestmentAccount.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "targetUnderlyingSecurity.id";
	}


	@Override
	protected Integer getBeanKey1Value(InvestmentAccountSecurityTarget bean) {
		return BeanUtils.getBeanIdentity(bean.getClientInvestmentAccount());
	}


	@Override
	protected Integer getBeanKey2Value(InvestmentAccountSecurityTarget bean) {
		return BeanUtils.getBeanIdentity(bean.getTargetUnderlyingSecurity());
	}


	@Override
	protected String getBeanOrderProperty() {
		return "startDate";
	}


	@Override
	protected Date getBeanOrderValue(InvestmentAccountSecurityTarget bean) {
		return bean.getStartDate();
	}


	@Override
	protected String getBeanEndOrderProperty() {
		return "endDate";
	}


	@Override
	protected Date getBeanEndOrderValue(InvestmentAccountSecurityTarget bean) {
		return bean.getEndDate();
	}
}
