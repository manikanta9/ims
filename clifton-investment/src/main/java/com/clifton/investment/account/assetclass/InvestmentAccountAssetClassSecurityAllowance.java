package com.clifton.investment.account.assetclass;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.replication.InvestmentReplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentAccountAssetClassSecurityAllowance</code> class contains information regarding a specific client account and security and which asset classes and replications for those asset classes the security belongs to
 * <p>
 * This can be used to validate if a security applies to a given asset class/replication
 * and if there is contract splitting available.
 * <p>
 * Note: There are two maps, one is for those excluded from position count.  In those cases the same positions can be applied to both lists and they don't split across each other.
 *
 * @author manderson
 */
public class InvestmentAccountAssetClassSecurityAllowance {

	private final Map<InvestmentAccountAssetClass, List<InvestmentReplication>> accountAssetClassReplicationListMap = new HashMap<>();

	private final Map<InvestmentAccountAssetClass, List<InvestmentReplication>> accountAssetClassReplicationExcludedFromCountListMap = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the security applies to the given account asset class and replication.
	 */
	public boolean isValidSelection(InvestmentAccountAssetClass accountAssetClass, InvestmentReplication replication) {
		Map<InvestmentAccountAssetClass, List<InvestmentReplication>> map = this.accountAssetClassReplicationListMap;
		if (accountAssetClass.isReplicationPositionExcludedFromCount()) {
			map = this.accountAssetClassReplicationExcludedFromCountListMap;
		}
		List<InvestmentReplication> replicationList = map.get(accountAssetClass);
		return CollectionUtils.contains(replicationList, replication);
	}


	/**
	 * Returns true if the security can be split for the given asset class. This means that there is more than one replication within the asset class
	 * that is valid for the security, or multiple asset classes replications.
	 * <p>
	 * Splits are specific to whether or not the asset class isReplicationPositionExcludedFromCount flag is on or off, as then they can
	 * only be split based on that common option.
	 */
	public boolean isSecuritySplit(InvestmentAccountAssetClass accountAssetClass) {
		Map<InvestmentAccountAssetClass, List<InvestmentReplication>> map = this.accountAssetClassReplicationListMap;
		if (accountAssetClass.isReplicationPositionExcludedFromCount()) {
			map = this.accountAssetClassReplicationExcludedFromCountListMap;
		}
		return CollectionUtils.getSize(CollectionUtils.combineCollectionOfCollections(map.values())) >= 2;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void addInvestmentAccountAssetClassReplicationToMap(InvestmentAccountAssetClass accountAssetClass, InvestmentReplication replication) {
		Map<InvestmentAccountAssetClass, List<InvestmentReplication>> map = this.accountAssetClassReplicationListMap;
		if (accountAssetClass.isReplicationPositionExcludedFromCount()) {
			map = this.accountAssetClassReplicationExcludedFromCountListMap;
		}
		List<InvestmentReplication> replicationList = map.computeIfAbsent(accountAssetClass, k -> new ArrayList<>());
		replicationList.add(replication);
	}
}
