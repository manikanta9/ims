package com.clifton.investment.account.relationship;

import com.clifton.core.util.validation.ValidationException;


/**
 * The NoRelatedAccountFoundException class represent a {@link ValidationException} that should be thrown by
 * related investment account lookup for purpose logic when no related accounts are found.
 *
 * @author vgomelsky
 */
public class NoRelatedAccountFoundException extends ValidationException {

	public NoRelatedAccountFoundException(String message) {
		super(message);
	}
}
