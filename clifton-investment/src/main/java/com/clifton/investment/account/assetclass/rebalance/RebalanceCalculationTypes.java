package com.clifton.investment.account.assetclass.rebalance;


/**
 * The <code>RebalanceCalculationTypes</code> defines the calculation types used for account mini rebalancing (PIOS Runs)
 *
 * @author manderson
 */
public enum RebalanceCalculationTypes {

	PROPORTIONAL_REBALANCE_CASH("Proportionally Using Rebalance Cash", false, true, false, false) // Allocates rebalance cash rebalancing based on actual rebalance cash %ages
	, PROPORTIONAL_FUND_WEIGHT("Proportionally Using Fund Cash Weights", false, true, false, false) // Allocates rebalance cash rebalancing based on Fund Cash %ages 
	, MANUAL("Manual Rebalance", false, false, true, false) // Used (hopefully temporarily) for cases where analysts need to manually enter rebalance cash values for a rebalance
	, FULL("Full Rebalance", true, false, false, true); // System Calculated Full Rebalance which sets rebalance cash = Target - Actual - Effective Cash to bring all deviations back to zero


	RebalanceCalculationTypes(String label, boolean fullRebalance, boolean miniRebalance, boolean manualRebalance, boolean overriddenByMinimizeImbalances) {
		this.label = label;
		this.fullRebalance = fullRebalance;
		this.miniRebalance = miniRebalance;
		this.manualRebalance = manualRebalance;
		this.overriddenByMinimizeImbalances = overriddenByMinimizeImbalances;
	}


	private final String label;

	private final boolean miniRebalance;
	private final boolean manualRebalance;
	private final boolean fullRebalance; // Full Rebalance vs. Mini Rebalance

	private final boolean overriddenByMinimizeImbalances; // Full Rebalance functionality can be overridden by minimize imbalances calculator - if there is a minimize imbalances calculator, need to use that calculation


	public String getLabel() {
		return this.label;
	}


	public boolean isFullRebalance() {
		return this.fullRebalance;
	}


	public boolean isOverriddenByMinimizeImbalances() {
		return this.overriddenByMinimizeImbalances;
	}


	public boolean isMiniRebalance() {
		return this.miniRebalance;
	}


	public boolean isManualRebalance() {
		return this.manualRebalance;
	}
}
