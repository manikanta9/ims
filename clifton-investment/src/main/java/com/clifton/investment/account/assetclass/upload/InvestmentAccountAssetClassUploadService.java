package com.clifton.investment.account.assetclass.upload;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;


/**
 * @author manderson
 */
public interface InvestmentAccountAssetClassUploadService {


	/**
	 * Download a sample upload file.  If investment account is selected on the uploadCommand the results will be filtered to that account
	 * All inactive and rollup asset classes are excluded
	 */
	@SecureMethod(dtoClass = InvestmentAccountAssetClass.class)
	public DataTable downloadInvestmentAccountAssetClassUploadFileSample(InvestmentAccountAssetClassUploadCommand uploadCommand);


	/**
	 * Uploads {@link com.clifton.investment.account.assetclass.InvestmentAccountAssetClass} beans into the system
	 * <p>
	 * Supports UPDATES only to existing active non-rollup asset classes
	 */
	@SecureMethod(dtoClass = InvestmentAccountAssetClass.class)
	public void uploadInvestmentAccountAssetClassUploadFile(InvestmentAccountAssetClassUploadCommand uploadCommand);
}
