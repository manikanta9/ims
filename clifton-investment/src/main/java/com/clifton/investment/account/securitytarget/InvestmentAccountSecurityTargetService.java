package com.clifton.investment.account.securitytarget;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetFreezeSearchForm;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetFreezeTypeSearchForm;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * <code>InvestmentAccountSecurityTargetService</code> is a service for managing {@link InvestmentAccountSecurityTarget}s and
 * {@link InvestmentAccountSecurityTargetFreeze} objects for an {@link com.clifton.investment.account.InvestmentAccount}. Targets
 * for an underlying {@link com.clifton.investment.instrument.InvestmentSecurity} can be defined and used to calculate utilization
 * based on held positions, pending transactions, and frozen quantities.
 *
 * @author NickK
 */
public interface InvestmentAccountSecurityTargetService {

	////////////////////////////////////////////////////////////////////////////
	/////////    Investment Account Security Target Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTarget getInvestmentAccountSecurityTarget(int id);


	/**
	 * Retrieves the {@link InvestmentAccountSecurityTarget} for the given client account ID and underlying security ID that is applicable on the given date, or <code>null</code>
	 * if no such security target exists.
	 *
	 * @param accountId            the client account ID for the security target
	 * @param underlyingSecurityId the underlying security ID for the security target
	 * @param date                 the date on which the security target should be applicable
	 * @return the applicable security target, or <code>null</code> if no such security target exists
	 */
	public InvestmentAccountSecurityTarget getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(int accountId, int underlyingSecurityId, Date date);


	public List<InvestmentAccountSecurityTarget> getInvestmentAccountSecurityTargetList(InvestmentAccountSecurityTargetSearchForm searchForm);


	public InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTarget(InvestmentAccountSecurityTarget securityTarget);


	/**
	 * Saves the {@link InvestmentAccountSecurityTarget} without validating security target validation for zero values in the InvestmentAccountSecurityTarget's quantity or notional target fields.
	 * Designed to support the alternate URL required by {@link UserIgnorableValidationException} for bypassing this validation.
	 */
	public InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTargetIgnoringValidation(InvestmentAccountSecurityTarget securityTarget);


	/**
	 * Saves the provided new {@link InvestmentAccountSecurityTarget}.
	 * <p>
	 * <br/>- If an existing ID is provided and the existing record has a null end date, the existing target will be ended using the start date - 1 of the new target.
	 * <br/>- If an existing ID is not provided and the provided security target has an ID, the original will be looked up and ended using today - 1 day and the new target will start today.
	 * <br/>- If an existing ID is not provided and the provided security target is new, an existing active target is looked up using the new target's client account and underlying security.
	 * An existing active target will be ended using start date - 1 of the new target.
	 * <p>
	 * Any active {@link InvestmentAccountSecurityTargetFreeze} entities will be ended for the existing target and new frozen allocations will be created for the new target using the same start date.
	 */
	public InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTargetFromExisting(Integer existingId, InvestmentAccountSecurityTarget securityTarget);


	/**
	 * An alternate method that saves the provided new {@link InvestmentAccountSecurityTarget}. This method is used when the security target validation for zero quantity / notional target values in the InvestmentAccountSecurityTarget
	 * should be skipped.  Designed to support the alternate URL required by {@link UserIgnorableValidationException} for bypassing the validation.
	 * <p>
	 * <br/>- If an existing ID is provided and the existing record has a null end date, the existing target will be ended using the start date - 1 of the new target.
	 * <br/>- If an existing ID is not provided and the provided security target has an ID, the original will be looked up and ended using today - 1 day and the new target will start today.
	 * <br/>- If an existing ID is not provided and the provided security target is new, an existing active target is looked up using the new target's client account and underlying security.
	 * An existing active target will be ended using start date - 1 of the new target.
	 * <p>
	 * Any active {@link InvestmentAccountSecurityTargetFreeze} entities will be ended for the existing target and new frozen allocations will be created for the new target using the same start date.
	 */
	public InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTargetFromExistingIgnoringValidation(Integer existingId, InvestmentAccountSecurityTarget securityTarget);


	/**
	 * {@link InvestmentAccountSecurityTarget}s do not get deleted, instead existing targets will be ended.
	 * This service ends the security target with the provided ID by updating the end date to the current date - 1, so it is not active today.
	 */
	@RequestMapping("investmentAccountSecurityTargetEnd")
	public void endInvestmentAccountSecurityTarget(int id);


	/**
	 * Deletes the {@link InvestmentAccountSecurityTarget} with the provided ID. If the target to delete has been active for more than one day, an
	 * {@link com.clifton.core.validation.UserIgnorableValidationException} will be thrown for the user to override calling {@link #deleteInvestmentAccountSecurityTargetHistorical(int)}.
	 * <p>
	 * Any {@link InvestmentAccountSecurityTargetFreeze} entities associated with the deleted security target will also be deleted.
	 * <p>
	 * This service should be used rarely and with caution. The preferred service to use is {@link #endInvestmentAccountSecurityTarget(int)} so the
	 * target is still available in the system to historically view active security targets on a given day.
	 */
	public void deleteInvestmentAccountSecurityTarget(int id);


	/**
	 * Same as {@link #deleteInvestmentAccountSecurityTarget(int)} but allows deleting a historical {@link InvestmentAccountSecurityTarget}.
	 */
	@SecureMethod(dtoClass = InvestmentAccountSecurityTarget.class)
	public void deleteInvestmentAccountSecurityTargetHistorical(int id);

	////////////////////////////////////////////////////////////////////////////
	///    Investment Account Security Target Freeze Type Business Methods   ///
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetFreezeType getInvestmentAccountSecurityTargetFreezeType(short id);


	public InvestmentAccountSecurityTargetFreezeType getInvestmentAccountSecurityTargetFreezeTypeByName(String name);


	public List<InvestmentAccountSecurityTargetFreezeType> getInvestmentAccountSecurityTargetFreezeTypeList(InvestmentAccountSecurityTargetFreezeTypeSearchForm searchForm);


	public InvestmentAccountSecurityTargetFreezeType saveInvestmentAccountSecurityTargetFreezeType(InvestmentAccountSecurityTargetFreezeType securityTargetFreezeType);


	public void deleteInvestmentAccountSecurityTargetFreezeType(short id);


	////////////////////////////////////////////////////////////////////////////
	//////    Investment Account Security Target Freeze Business Methods   /////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetFreeze getInvestmentAccountSecurityTargetFreeze(int id);


	public List<InvestmentAccountSecurityTargetFreeze> getInvestmentAccountSecurityTargetFreezeList(InvestmentAccountSecurityTargetFreezeSearchForm searchForm);


	public InvestmentAccountSecurityTargetFreeze saveInvestmentAccountSecurityTargetFreeze(InvestmentAccountSecurityTargetFreeze securityTargetFreeze);


	/**
	 * {@link InvestmentAccountSecurityTargetFreeze}s do not get deleted, instead existing frozen allocations will be ended.
	 * This service ends the security target frozen allocation with the provided ID by updating the end date to the current date - 1, so it is not active today.
	 */
	@RequestMapping("investmentAccountSecurityTargetFreezeEnd")
	public void endInvestmentAccountSecurityTargetFreeze(int id);


	/**
	 * Deletes the {@link InvestmentAccountSecurityTargetFreeze} with the provided ID.
	 * <p>
	 * This service should be used rarely and with caution. The preferred service to use is {@link #endInvestmentAccountSecurityTargetFreeze(int)} so the
	 * freeze allocation is still available in the system to historically view active security targets utilization on a given day.
	 */
	public void deleteInvestmentAccountSecurityTargetFreeze(int id);
}
