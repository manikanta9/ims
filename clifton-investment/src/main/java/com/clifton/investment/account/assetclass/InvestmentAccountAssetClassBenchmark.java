package com.clifton.investment.account.assetclass;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * The <code>InvestmentAccountAssetClassBenchmark</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentAccountAssetClassBenchmark extends ManyToManyEntity<InvestmentAccountAssetClass, InvestmentSecurity, Integer> {

	private Date startDate;
	private Date endDate;


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	public String getDateLabel() {
		return DateUtils.fromDateRange(getStartDate(), getEndDate(), true, false);
	}


	public String getBenchmarkLabel() {
		if (getReferenceTwo() != null) {
			return getReferenceTwo().getLabel();
		}
		return "No Benchmark Security (Use Account Asset Class Return)";
	}


	public boolean isUseAccountAssetClassReturn() {
		return getReferenceTwo() == null;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
