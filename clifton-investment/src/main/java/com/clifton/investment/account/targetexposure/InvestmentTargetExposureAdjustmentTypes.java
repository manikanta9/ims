package com.clifton.investment.account.targetexposure;

import com.clifton.business.service.ServiceProcessingTypes;


/**
 * Target Exposure Adjustment Types is an enum that defines how an entity's target can be adjusted during run processing
 * The concept is universal to various portfolios and therefore accessible/defined via processing types.
 * Examples: For PIOS Target Exposure Adjustment Types apply to asset classes, LDI Target Adjustments apply to KRD Points
 * <p>
 * ANY OPTION THAT IS NOT NONE MUST HAVE ENTRIES IN RELATED MANY-TO-MANY TABLE TO APPLY DIFFERENCES TO AT LEAST ONE OTHER ENTITY
 *
 * @author manderson
 */
public enum InvestmentTargetExposureAdjustmentTypes {

	/**
	 * NONE: Target Exposure is Unadjusted
	 */
	NONE,
	/**
	 * CONSTANT: Target Exposure is adjusted based upon a Constant value
	 */
	CONSTANT(true, "Target Exposure Amount", false, false, null),
	/**
	 * TFA: Target Follows Actual
	 */
	TFA(false, null, false, false, null),
	/**
	 * AFT: Actual Follows Target
	 */
	AFT(false, null, false, false, new ServiceProcessingTypes[]{ServiceProcessingTypes.PORTFOLIO_RUNS}),
	/**
	 * PERCENT_OF_TARGET: This one is proportional and adjusts the target by x% of the actual target amount. So, if your target is 10% and 500,000 and you enter adjustment of 5% the target will be adjusted by 25000, or 0.5%. = New target of 525,000 or 10.5%
	 */
	PERCENT_OF_TARGET(true, "Percentage", false, false, null),
	/**
	 * ABSOLUTE_PERCENT: Absolute Percent would not take a percentage of the actual target, but adjust the target percentage by the given amount. So, if your target is 10% and 500,000 and you enter adjustment of 5% the target will be adjusted by 250m000, or 5%. = New target of 750,000 or 15%
	 */
	ABSOLUTE_PERCENT(true, "Percentage", false, false, null),
	/**
	 * MANAGER_MARKET_VALUE: Target Exposure is adjusted to a selected Manager Value (Cash & Securities)
	 */
	MANAGER_MARKET_VALUE(false, null, true, false, null),
	/**
	 * DYNAMIC_TARGET_PERCENT: Target Exposure is adjusted to the result of a system bean calculation
	 */
	DYNAMIC_TARGET_PERCENT(false, null, false, true, null);


	InvestmentTargetExposureAdjustmentTypes() {
		// USED FOR NONE (Unadjusted) TYPE - NO SETTINGS
	}


	InvestmentTargetExposureAdjustmentTypes(boolean amountFieldRequired, String amountFieldName, boolean managerFieldRequired, boolean dynamicTargetCalculatorRequired, ServiceProcessingTypes[] restrictedProcessingTypes) {
		this.amountFieldRequired = amountFieldRequired;
		this.amountFieldName = amountFieldName;
		this.managerFieldRequired = managerFieldRequired;
		this.dynamicTargetCalculatorRequired = dynamicTargetCalculatorRequired;
		this.restrictedProcessingTypes = restrictedProcessingTypes;
	}


	/////////////////////////////////////////////////////////////////////

	private boolean amountFieldRequired;
	private String amountFieldName;

	private boolean managerFieldRequired;

	private boolean dynamicTargetCalculatorRequired;

	/**
	 * When populated means that use of this type can be used ONLY if the entity is used by an account that uses a processing type within that array
	 * If null, then no restrictions
	 */
	private ServiceProcessingTypes[] restrictedProcessingTypes;


	/////////////////////////////////////////////////////////////////////


	public static boolean isTargetExposureUsed(InvestmentTargetExposureAdjustmentTypes type) {
		if (type != null && InvestmentTargetExposureAdjustmentTypes.NONE != type) {
			return true;
		}
		return false;
	}


	/////////////////////////////////////////////////////////////////////


	public boolean isAmountFieldRequired() {
		return this.amountFieldRequired;
	}


	public String getAmountFieldName() {
		return this.amountFieldName;
	}


	public boolean isManagerFieldRequired() {
		return this.managerFieldRequired;
	}


	public boolean isDynamicTargetCalculatorRequired() {
		return this.dynamicTargetCalculatorRequired;
	}


	public ServiceProcessingTypes[] getRestrictedProcessingTypes() {
		return this.restrictedProcessingTypes;
	}
}


