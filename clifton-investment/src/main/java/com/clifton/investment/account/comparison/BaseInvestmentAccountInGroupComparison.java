package com.clifton.investment.account.comparison;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;

import java.util.List;


/**
 * The <code>BaseInvestmentAccountInGroupComparison</code> class is a base class for comparing an investment account
 * against a list of account groups. Implementation classes can either evaluate based on whether the investment
 * account is in the list of groups (<code>InvestmentAccountInGroupComparison</code>) or whether the investment
 * account is not in the list of groups (<code>InvestmentAccountNotInGroupComparison</code>).
 *
 * @author lnaylor
 */
public abstract class BaseInvestmentAccountInGroupComparison implements Comparison<IdentityObject> {

	private InvestmentAccountGroupService investmentAccountGroupService;

	/**
	 * Id(s) of account group(s) to compare against the given bean.
	 */
	private List<Integer> accountGroupIdList;

	/**
	 * The bean property name on the bean that defines the investment account
	 */
	private String accountBeanPropertyName;

	/**
	 * If this flag is set to true, the comparison will evaluate to true if the investment account
	 * on the bean matches the comparison condition (InGroup or NotInGroup) for at least one of the
	 * account groups in accountGroupIdList. Otherwise, the comparison will only evaluate to true
	 * if the investment account on the bean matches the comparison condition for all of the account
	 * groups in accountGroupIdList.
	 */
	private boolean matchAnyGroup;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		InvestmentAccount account = (InvestmentAccount) BeanUtils.getPropertyValue(bean, getAccountBeanPropertyName());
		List<InvestmentAccountGroup> groupList = getInvestmentAccountGroupService().getInvestmentAccountGroupListByIds(getAccountGroupIdList());
		AssertUtils.assertNotEmpty(groupList, "Cannot find account group(s) with id(s) " + getAccountGroupIdList() + ".");
		boolean result = false;
		String groupListString = CollectionUtils.toString(CollectionUtils.getConverted(groupList, InvestmentAccountGroup::getName), 5);
		if (account != null) {
			for (InvestmentAccountGroup group : groupList) {
				boolean match = isRequireInGroup() == getInvestmentAccountGroupService().isInvestmentAccountInGroup(account.getId(), group.getName());
				if (!isMatchAnyGroup() && !match) {
					result = false;
					break;
				}
				else if (isMatchAnyGroup() && match) {
					result = true;
					break;
				}
				else {
					result = !isMatchAnyGroup();
				}
			}

			if (context != null) {
				if (result) {
					context.recordTrueMessage(getTrueMessage(account.getNumber(), groupListString));
				}
				else {
					context.recordFalseMessage(getFalseMessage(account.getNumber(), groupListString));
				}
			}
		}
		else if (context != null) {
			context.recordFalseMessage("(Investment Account Property Value is not set, therefore no account to verify in group(s) " + groupListString + ")");
		}
		return result;
	}


	/**
	 * This method should be overridden to return true if the child class comparison is checking that the investment account is
	 * in the given account groups (i.e. <code>InvestmentAccountInGroupComparison</code>). It should be overridden to return
	 * false if the child class comparison is checking that the investment account is not in the given account groups (i.e.
	 * <code>InvestmentAccountNotInGroupComparison</code>).
	 */
	protected abstract boolean isRequireInGroup();


	protected abstract String getTrueMessage(String accountNumber, String groupList);


	protected abstract String getFalseMessage(String accountNumber, String groupList);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public List<Integer> getAccountGroupIdList() {
		return this.accountGroupIdList;
	}


	public void setAccountGroupIdList(List<Integer> accountGroupIdList) {
		this.accountGroupIdList = accountGroupIdList;
	}


	public String getAccountBeanPropertyName() {
		return this.accountBeanPropertyName;
	}


	public void setAccountBeanPropertyName(String accountBeanPropertyName) {
		this.accountBeanPropertyName = accountBeanPropertyName;
	}


	public boolean isMatchAnyGroup() {
		return this.matchAnyGroup;
	}


	public void setMatchAnyGroup(boolean matchAnyGroup) {
		this.matchAnyGroup = matchAnyGroup;
	}
}
