package com.clifton.investment.account.assetclass;


import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentAwareAssignment;


/**
 * The <code>InvestmentAccountAssetClassAssignment</code> assigns parent/child relationships for {@link InvestmentAccountAssetClass} objects
 * when a Target Exposure Adjustment option is used (i.e. defines where the put the target the was adjusted from the parent and how to apply it)
 *
 * @author Mary Anderson
 */
public class InvestmentAccountAssetClassAssignment extends InvestmentTargetExposureAdjustmentAwareAssignment<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> {

	// NOTHING HERE
}
