package com.clifton.investment.account.relationship;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfig;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipMappingSearchForm;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipPurposeSearchForm;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountRelationshipService</code> interface defines methods for working with InvestmentAccount relationships.
 *
 * @author vgomelsky
 */
public interface InvestmentAccountRelationshipService {

	////////////////////////////////////////////////////////////////////////////
	/////////   Investment Account Relationship Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationship getInvestmentAccountRelationship(int id);


	public List<InvestmentAccountRelationship> getInvestmentAccountRelationshipList(InvestmentAccountRelationshipSearchForm searchForm);


	@RequestMapping("investmentAccountMainList")
	public List<InvestmentAccountRelationship> getMainInvestmentAccountList(int relatedAccountId, boolean activeOnly);


	@RequestMapping("investmentAccountRelatedList")
	public List<InvestmentAccountRelationship> getRelatedInvestmentAccountList(int mainAccountId, boolean activeOnly);


	/**
	 * Returns true if the main-related account relationship is valid on a given date
	 *
	 * @param mainAccountId
	 * @param relatedAccountId
	 * @param date
	 */
	@SecureMethod(dtoClass = InvestmentAccountRelationship.class)
	public boolean isInvestmentAccountRelationshipValid(int mainAccountId, int relatedAccountId, Date date);


	/**
	 * Returns true if the main-related account relationship is valid on a given date
	 *
	 * @param mainAccountId
	 * @param relatedAccountId
	 * @param purpose          the name of relationship purpose
	 * @param date
	 */
	public boolean isInvestmentAccountRelationshipForPurposeValid(int mainAccountId, int relatedAccountId, String purpose, Date date);


	public InvestmentAccountRelationship saveInvestmentAccountRelationship(InvestmentAccountRelationship accountRelationship);


	public void deleteInvestmentAccountRelationship(int id);


	////////////////////////////////////////////////////////////////////////////
	///////   Investment Account Relationship Purpose Business Methods   ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipPurpose getInvestmentAccountRelationshipPurpose(short id);


	public InvestmentAccountRelationshipPurpose getInvestmentAccountRelationshipPurposeByName(String name);


	public List<InvestmentAccountRelationshipPurpose> getInvestmentAccountRelationshipPurposeList(InvestmentAccountRelationshipPurposeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////   Investment Account Relationship Config Business Methods   ///////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = InvestmentAccountRelationship.class)
	public InvestmentAccountRelationshipConfig getInvestmentAccountRelationshipConfig(int investmentAccountId);


	/**
	 * Returns a list of related accounts for a given purpose and active date.
	 * If purpose is blank - will include all purposes.  If active date is null will not filter by active dates
	 *
	 * @param investmentAccountId
	 * @param securityId          optionally some relationships maybe limited to a set of securities: InvestmentSecurityGroup, will filter if securityId is passed
	 * @param purpose
	 * @param activeOnDate
	 */
	public List<InvestmentAccount> getInvestmentAccountRelatedListForPurpose(int investmentAccountId, Integer securityId, String purpose, Date activeOnDate);


	/**
	 * NOTE: use "getInvestmentAccountRelatedForPurposeStrict" method instead if you know both client and holding accounts.
	 * <p>
	 * Returns at most ONE related account for a given purpose and active date.
	 * If purpose is blank - will include all purposes.  If active date is null will not filter by active dates
	 * <p>
	 * NOTE: If more than one related account applies, an exception will be thrown.  Should only be used in cases
	 * where one account is really expected.
	 */
	public InvestmentAccount getInvestmentAccountRelatedForPurpose(int investmentAccountId, String purpose, Date activeOnDate);


	/**
	 * Returns at most ONE related account for the given purpose and active date.
	 * First looks up related accounts for the holdingAccountId and returns it if only one found.
	 * Then looks up related accounts for the clientAccountId and returns it if only one found and no holding accounts exist.
	 * If both relationships exist, then tries to find a single account that's in both and returns it if only one.
	 * <p>
	 * Throws exceptions in all other cases (can't find a single account).
	 * <p>
	 * NOTE: ONLY checks direct relationships - does not include relationships to the holding account through any sub-accounts
	 */
	public InvestmentAccount getInvestmentAccountRelatedForPurposeStrict(int clientAccountId, int holdingAccountId, int investmentSecurityId, String purpose, Date activeOnDate);


	/**
	 * Returns a list of {@link InvestmentAccountRelationship} entities for a given purpose and active date.
	 * <p>
	 * If no purpose is given, the resulting list will include all purposes. If no active date is provided, then the resulting list will include purposes from all points in
	 * history.
	 *
	 * @param investmentAccountId            the account ID from which relationships will be found
	 * @param purpose                        the purpose name for the relationships to find, or {@code null} to return relationships of all types
	 * @param activeOnDate                   the active date by which to filter resulting relationships, or {@code null} if all historic relationships should be returned
	 * @param includeSubAccountRelationships {@code true} to include relationships for sub-accounts, or {@code false} otherwise
	 * @return the list of account relationships
	 */
	public List<InvestmentAccountRelationship> getInvestmentAccountRelationshipListForPurpose(int investmentAccountId, String purpose, Date activeOnDate, boolean includeSubAccountRelationships);


	/**
	 * Uses current date as activeOnDate
	 *
	 * @param investmentAccountId
	 * @param purpose
	 * @param includeSubAccountRelationships - If true will look for any relationship for the account or any of its sub-accounts
	 */
	public List<InvestmentAccountRelationship> getInvestmentAccountRelationshipListForPurposeActive(int investmentAccountId, String purpose, boolean includeSubAccountRelationships);


	////////////////////////////////////////////////////////////////////////////
	///   Investment Account Relationship Purpose Mapping Business Methods   ///
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipMapping getInvestmentAccountRelationshipMapping(int id);


	@DoNotAddRequestMapping
	public InvestmentAccountRelationshipMapping getInvestmentAccountRelationshipMappingActiveFor(InvestmentAccountRelationship investmentAccountRelationship);


	public List<InvestmentAccountRelationshipMapping> getInvestmentAccountRelationshipMappingList(InvestmentAccountRelationshipMappingSearchForm searchForm);


	public InvestmentAccountRelationshipMapping saveInvestmentAccountRelationshipMapping(InvestmentAccountRelationshipMapping bean);


	public void deleteInvestmentAccountRelationshipMapping(int id);
}
