package com.clifton.investment.account.assetclass.rebalance.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentAccountRebalanceAssetClassHistorySearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentAccountRebalanceAssetClassHistorySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "accountAssetClass.id", sortField = "accountAssetClass.order")
	private Integer accountAssetClassId;

	@SearchField(searchFieldPath = "accountAssetClass", searchField = "account.id", sortField = "account.number")
	private Integer investmentAccountId;

	@SearchField(searchFieldPath = "accountAssetClass", searchField = "assetClass.id", sortField = "assetClass.name")
	private Short assetClassId;

	@SearchField(searchFieldPath = "rebalanceHistory", searchField = "rebalanceDate")
	private Date rebalanceDate;

	@SearchField(searchFieldPath = "rebalanceHistory", searchField = "createDate")
	private Date historyCreateDate;

	@SearchField(searchFieldPath = "rebalanceHistory", searchField = "rebalanceCalculationType")
	private RebalanceCalculationTypes rebalanceCalculationType;

	@SearchField
	private BigDecimal beforeRebalanceCash;

	@SearchField
	private BigDecimal afterRebalanceCash;


	public Integer getAccountAssetClassId() {
		return this.accountAssetClassId;
	}


	public void setAccountAssetClassId(Integer accountAssetClassId) {
		this.accountAssetClassId = accountAssetClassId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Short getAssetClassId() {
		return this.assetClassId;
	}


	public void setAssetClassId(Short assetClassId) {
		this.assetClassId = assetClassId;
	}


	public Date getRebalanceDate() {
		return this.rebalanceDate;
	}


	public void setRebalanceDate(Date rebalanceDate) {
		this.rebalanceDate = rebalanceDate;
	}


	public RebalanceCalculationTypes getRebalanceCalculationType() {
		return this.rebalanceCalculationType;
	}


	public void setRebalanceCalculationType(RebalanceCalculationTypes rebalanceCalculationType) {
		this.rebalanceCalculationType = rebalanceCalculationType;
	}


	public BigDecimal getBeforeRebalanceCash() {
		return this.beforeRebalanceCash;
	}


	public void setBeforeRebalanceCash(BigDecimal beforeRebalanceCash) {
		this.beforeRebalanceCash = beforeRebalanceCash;
	}


	public BigDecimal getAfterRebalanceCash() {
		return this.afterRebalanceCash;
	}


	public void setAfterRebalanceCash(BigDecimal afterRebalanceCash) {
		this.afterRebalanceCash = afterRebalanceCash;
	}


	public Date getHistoryCreateDate() {
		return this.historyCreateDate;
	}


	public void setHistoryCreateDate(Date historyCreateDate) {
		this.historyCreateDate = historyCreateDate;
	}
}
