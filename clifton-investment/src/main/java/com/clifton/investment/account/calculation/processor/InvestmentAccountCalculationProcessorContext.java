package com.clifton.investment.account.calculation.processor;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.calculation.InvestmentAccountCalculationType;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;


/**
 * The <code>InvestmentAccountCalculationProcessorContext</code> is a context object passed to the processor that can hold various objects used to
 * limit the number of additional retrievals that need to be done.
 *
 * @author manderson
 */
public class InvestmentAccountCalculationProcessorContext {

	private final InvestmentAccountCalculationType calculationType;

	/**
	 * Set because if we are only processing for a sub-set of accounts - then retrievals that are cached that load data for all accounts
	 * will still limit it to the accounts that would be included (i.e. getting Currency Balances, Cash balances)
	 */
	private final Integer investmentAccountId;

	private final Integer investmentAccountGroupId;

	private Date snapshotDate;

	private Map<Integer, InvestmentAccountCalculationSnapshot> existingAccountSnapshotMap;

	/**
	 * Used to cache objects that can be easily reused for common retrievals to speed up performance
	 * Cache is cleared for each date since it contains date specific information
	 * * i.e. for a date and datasource what is my FX rate
	 */
	private Map<String, Object> dateContext;

	/**
	 * Used to cache objects that can be easily reused for common retrievals to speed up performance
	 * Cache is NOT cleared for each date since it contains global information that isn't date specific
	 * i.e. for an account what datasource do I use
	 */
	private Map<String, Object> globalContext = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationProcessorContext(InvestmentAccountCalculationType calculationType, InvestmentAccountCalculationProcessorCommand command) {
		this.calculationType = calculationType;
		this.investmentAccountGroupId = command.getInvestmentAccountGroupId();
		this.investmentAccountId = command.getInvestmentAccountId();
	}


	public void setupContextForDate(Date snapshotDate, Set<InvestmentAccountCalculationSnapshot> existingSnapshotList) {
		this.snapshotDate = snapshotDate;
		this.existingAccountSnapshotMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(existingSnapshotList)) {
			this.existingAccountSnapshotMap = BeanUtils.getBeanMap(existingSnapshotList, snapshot -> snapshot.getInvestmentAccount().getId());
		}
		this.dateContext = new HashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Object getValueFromDateContextMap(String key, Supplier<Object> functionReturningValueIfNotInMap) {
		return CollectionUtils.getValue(this.dateContext, key, functionReturningValueIfNotInMap);
	}


	public Object getValueFromGlobalContextMap(String key, Supplier<Object> functionReturningValueIfNotInMap) {
		return CollectionUtils.getValue(this.globalContext, key, functionReturningValueIfNotInMap);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationType getCalculationType() {
		return this.calculationType;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public Map<Integer, InvestmentAccountCalculationSnapshot> getExistingAccountSnapshotMap() {
		return this.existingAccountSnapshotMap;
	}
}
