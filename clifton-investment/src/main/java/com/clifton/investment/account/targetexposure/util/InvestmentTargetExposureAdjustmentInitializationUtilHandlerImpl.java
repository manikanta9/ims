package com.clifton.investment.account.targetexposure.util;

import com.clifton.core.json.custom.util.CustomJsonObjectInitializationUtilHandler;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustment;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Component;


/**
 * @author nickk
 */
@Component
public class InvestmentTargetExposureAdjustmentInitializationUtilHandlerImpl implements CustomJsonObjectInitializationUtilHandler<InvestmentTargetExposureAdjustment> {

	private InvestmentManagerAccountService investmentManagerAccountService;
	private SystemBeanService systemBeanService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Class<InvestmentTargetExposureAdjustment> getCustomJsonObjectClass() {
		return InvestmentTargetExposureAdjustment.class;
	}


	@Override
	public void prepareObjectForJsonSerialization(InvestmentTargetExposureAdjustment targetExposureAdjustment) {
		if (targetExposureAdjustment.getTargetExposureManagerAccount() != null) {
			targetExposureAdjustment.setTargetExposureManagerAccountId(targetExposureAdjustment.getTargetExposureManagerAccount().getId());
		}
		if (targetExposureAdjustment.getDynamicTargetCalculatorBean() != null) {
			targetExposureAdjustment.setDynamicTargetCalculatorBeanId(targetExposureAdjustment.getDynamicTargetCalculatorBean().getId());
		}
	}


	@Override
	public void postInitializeObjectAfterJsonDeserialization(InvestmentTargetExposureAdjustment targetExposureAdjustment) {
		if (targetExposureAdjustment.getTargetExposureManagerAccountId() != null) {
			targetExposureAdjustment.setTargetExposureManagerAccount(getInvestmentManagerAccountService().getInvestmentManagerAccount(targetExposureAdjustment.getTargetExposureManagerAccountId()));
		}
		if (targetExposureAdjustment.getDynamicTargetCalculatorBeanId() != null) {
			targetExposureAdjustment.setDynamicTargetCalculatorBean(getSystemBeanService().getSystemBean(targetExposureAdjustment.getDynamicTargetCalculatorBeanId()));
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
