package com.clifton.investment.account.securitytarget.history.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseEntityDateRangeWithoutTimeSearchForm;

import java.util.Date;


/**
 * <code>InvestmentAccountSecurityTargetHistorySearchForm</code> represents search configuration for {@link com.clifton.investment.account.securitytarget.history.InvestmentAccountSecurityTargetHistory} entities.
 *
 * @author davidi
 */
@SearchForm(hasOrmDtoClass = false)
public class InvestmentAccountSecurityTargetHistorySearchForm extends BaseEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "securityTarget.clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "securityTarget.targetUnderlyingSecurity.id")
	private Integer targetUnderlyingSecurityId;

	@SearchField(searchField = "securityTarget.createDate", dateFieldIncludesTime = true)
	private Date createDate;

	@SearchField(searchField = "securityTarget.createUserId")
	private Short createUserId;

	@SearchField(searchField = "securityTarget.updateDate", dateFieldIncludesTime = true)
	private Date updateDate;

	@SearchField(searchField = "securityTarget.updateUserId")
	private Short updateUserId;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getTargetUnderlyingSecurityId() {
		return this.targetUnderlyingSecurityId;
	}


	public void setTargetUnderlyingSecurityId(Integer targetUnderlyingSecurityId) {
		this.targetUnderlyingSecurityId = targetUnderlyingSecurityId;
	}


	public Date getCreateDate() {
		return this.createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Short getCreateUserId() {
		return this.createUserId;
	}


	public void setCreateUserId(Short createUserId) {
		this.createUserId = createUserId;
	}


	public Date getUpdateDate() {
		return this.updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public Short getUpdateUserId() {
		return this.updateUserId;
	}


	public void setUpdateUserId(Short updateUserId) {
		this.updateUserId = updateUserId;
	}
}
