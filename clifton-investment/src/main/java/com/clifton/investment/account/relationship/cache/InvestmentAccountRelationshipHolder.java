package com.clifton.investment.account.relationship.cache;


import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>InvestmentAccountRelationshipHolder</code> contains the metadata for {@link InvestmentAccountRelationship}
 * and is used by {@link InvestmentAccountRelationshipConfig} which contains all relationship information for an account
 * <p/>
 * We store ids only so that the cache doesn't have to be cleared for dependent object changes.
 *
 * @author manderson
 */
public class InvestmentAccountRelationshipHolder implements Serializable {

	private final int id;

	private final Integer mainAccountId; // i.e. Reference One in the Relationship
	private final Integer relatedAccountId; // i.e. Reference Two in the Relationship

	private final Short purposeId;

	private Short securityGroupId;

	private Integer accountAssetClassId;

	private final Date startDate;
	private final Date endDate;


	public InvestmentAccountRelationshipHolder(InvestmentAccountRelationship relationship) {
		this.id = relationship.getId();
		this.mainAccountId = relationship.getReferenceOne().getId();
		this.relatedAccountId = relationship.getReferenceTwo().getId();
		this.purposeId = relationship.getPurpose().getId();

		if (relationship.getSecurityGroup() != null) {
			this.securityGroupId = relationship.getSecurityGroup().getId();
		}

		if (relationship.getAccountAssetClass() != null) {
			this.accountAssetClassId = relationship.getAccountAssetClass().getId();
		}

		this.startDate = relationship.getStartDate();
		this.endDate = relationship.getEndDate();
	}


	//////////////////////////////////////////////////////////////


	public boolean isActive() {
		return isActiveOnDate(new Date());
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	//////////////////////////////////////////////////////////////


	public Integer getMainAccountId() {
		return this.mainAccountId;
	}


	public Integer getRelatedAccountId() {
		return this.relatedAccountId;
	}


	public Short getPurposeId() {
		return this.purposeId;
	}


	public Short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public Integer getAccountAssetClassId() {
		return this.accountAssetClassId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public int getId() {
		return this.id;
	}
}
