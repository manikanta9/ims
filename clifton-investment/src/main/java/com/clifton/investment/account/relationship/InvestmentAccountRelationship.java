package com.clifton.investment.account.relationship;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;

import java.util.Date;


/**
 * The <code>InvestmentAccountRelationship</code> defines a relationship between 2 accounts.
 *
 * @author mwacker
 */
public class InvestmentAccountRelationship extends ManyToManyEntity<InvestmentAccount, InvestmentAccount, Integer> {

	/**
	 * referenceOne uses referenceTwo account for the following purpose
	 */
	private InvestmentAccountRelationshipPurpose purpose;
	/**
	 * Optional group of securities that this relationship applies to.  If set, security must belong to this group.
	 * For example, can have define relationships to 2 custodian accounts: one for PUT options and one for CALL options.
	 */
	private InvestmentSecurityGroup securityGroup;

	private Date startDate;
	private Date endDate;

	/**
	 * If set, then this related account applies specifically to the account asset class from
	 * reference One
	 */
	private InvestmentAccountAssetClass accountAssetClass;


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public InvestmentAccountRelationshipPurpose getPurpose() {
		return this.purpose;
	}


	public void setPurpose(InvestmentAccountRelationshipPurpose purpose) {
		this.purpose = purpose;
	}


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public void setAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
	}


	public InvestmentSecurityGroup getSecurityGroup() {
		return this.securityGroup;
	}


	public void setSecurityGroup(InvestmentSecurityGroup securityGroup) {
		this.securityGroup = securityGroup;
	}
}
