package com.clifton.investment.account.relationship;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.setup.InvestmentType;


/**
 * The <code>InvestmentAccountRelationshipPurpose</code> class defines the purpose (use case) for a
 * relationship between 2 investment accounts: Collateral, M2M, Trading: Futures, Trading: Bonds, etc.
 *
 * @author vgomelsky
 */
@CacheByName
public class InvestmentAccountRelationshipPurpose extends NamedEntity<Short> {

	public static final String CLIFTON_SUB_ACCOUNT_PURPOSE_NAME = "Clifton Sub-Account";
	public static final String COLLATERAL_ACCOUNT_PURPOSE_NAME = "Collateral";
	public static final String CUSTODIAN_ACCOUNT_PURPOSE_NAME = "Custodian";
	public static final String MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME = "Mark to Market";
	public static final String TRADING_FORWARDS_PURPOSE_NAME = "Trading: Forwards";
	public static final String TRADING_FUTURES_PURPOSE_NAME = "Trading: Futures";
	public static final String TRADING_OPTIONS_PURPOSE_NAME = "Trading: Options";
	public static final String TRADING_BONDS_PURPOSE_NAME = "Trading: Bonds";
	public static final String TRADING_BONDS_COLLATERAL_PURPOSE_NAME = TRADING_BONDS_PURPOSE_NAME + " Collateral";
	public static final String TRADING_PURPOSE_NAME_PREFIX = "Trading: ";

	/**
	 * Optional investment types that are allowed for this relationship.
	 */
	private InvestmentType investmentType;

	/**
	 * Optional matching collateral purpose. For example, "Trading: Bonds" references "Trading: Bonds Collateral".
	 */
	private InvestmentAccountRelationshipPurpose collateralPurpose;

	/**
	 * Optional matching trading executing purpose. For example, "Trading: Funds" relationship should have associated "Executing: Funds" relationship for the broker
	 */
	private InvestmentAccountRelationshipPurpose tradingExecutingPurpose;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipPurpose() {
		super();
	}


	public InvestmentAccountRelationshipPurpose(String name, short id) {
		super();
		setName(name);
		setId(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public InvestmentAccountRelationshipPurpose getCollateralPurpose() {
		return this.collateralPurpose;
	}


	public void setCollateralPurpose(InvestmentAccountRelationshipPurpose collateralPurpose) {
		this.collateralPurpose = collateralPurpose;
	}


	public InvestmentAccountRelationshipPurpose getTradingExecutingPurpose() {
		return this.tradingExecutingPurpose;
	}


	public void setTradingExecutingPurpose(InvestmentAccountRelationshipPurpose tradingExecutingPurpose) {
		this.tradingExecutingPurpose = tradingExecutingPurpose;
	}
}
