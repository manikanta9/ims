package com.clifton.investment.account.securitytarget.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


/**
 * <code>InvestmentAccountSecurityTargetSearchForm</code> represents search configuration for {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget} entities.
 *
 * @author NickK
 */
public class InvestmentAccountSecurityTargetSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "mainRelationshipAccountList.id", searchFieldPath = "clientInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer relatedAccountId;

	@SearchField(searchField = "mainRelationshipAccountList.issuingCompany.id", searchFieldPath = "clientInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer issuingCompanyId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchField = "targetUnderlyingSecurity.id")
	private Integer targetUnderlyingSecurityId;

	@SearchField(searchField = "targetUnderlyingSecurity.id")
	private Integer[] targetUnderlyingSecurityIds;

	// Custom Search Field - Based on selection the populated search form filters will be populated
	private String targetType;

	@SearchField
	private BigDecimal targetUnderlyingQuantity;

	@SearchField(searchField = "targetUnderlyingQuantity", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean targetUnderlyingQuantityPopulated;

	@SearchField
	private BigDecimal targetUnderlyingNotional;

	@SearchField(searchField = "targetUnderlyingNotional", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean targetUnderlyingNotionalPopulated;

	@SearchField(searchField = "targetUnderlyingManagerAccount.id")
	private Integer targetUnderlyingManagerAccountId;

	@SearchField(searchField = "targetUnderlyingManagerAccount.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean targetUnderlyingManagerAccountPopulated;

	@SearchField
	private BigDecimal targetUnderlyingMultiplier;

	@SearchField
	private Short trancheCount;

	@SearchField
	private Short cycleDurationWeeks;

	@SearchField
	private String securityTargetNote;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "clientInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchField = "teamSecurityGroup.id", searchFieldPath = "clientInvestmentAccount")
	private Short teamSecurityGroupId;

	@SearchField(searchField = "businessService.id", searchFieldPath = "clientInvestmentAccount")
	private Short businessServiceId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchFieldPath = "clientInvestmentAccount.businessService", searchField = "id,parent.id,parent.parent.id,parent.parent.parent.id", leftJoin = true, sortField = "name")
	private Short businessServiceOrParentId;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getRelatedAccountId() {
		return this.relatedAccountId;
	}


	public void setRelatedAccountId(Integer relatedAccountId) {
		this.relatedAccountId = relatedAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public Integer getTargetUnderlyingSecurityId() {
		return this.targetUnderlyingSecurityId;
	}


	public void setTargetUnderlyingSecurityId(Integer targetUnderlyingSecurityId) {
		this.targetUnderlyingSecurityId = targetUnderlyingSecurityId;
	}


	public Integer[] getTargetUnderlyingSecurityIds() {
		return this.targetUnderlyingSecurityIds;
	}


	public void setTargetUnderlyingSecurityIds(Integer[] targetUnderlyingSecurityIds) {
		this.targetUnderlyingSecurityIds = targetUnderlyingSecurityIds;
	}


	public BigDecimal getTargetUnderlyingQuantity() {
		return this.targetUnderlyingQuantity;
	}


	public void setTargetUnderlyingQuantity(BigDecimal targetUnderlyingQuantity) {
		this.targetUnderlyingQuantity = targetUnderlyingQuantity;
	}


	public BigDecimal getTargetUnderlyingNotional() {
		return this.targetUnderlyingNotional;
	}


	public void setTargetUnderlyingNotional(BigDecimal targetUnderlyingNotional) {
		this.targetUnderlyingNotional = targetUnderlyingNotional;
	}


	public Integer getTargetUnderlyingManagerAccountId() {
		return this.targetUnderlyingManagerAccountId;
	}


	public void setTargetUnderlyingManagerAccountId(Integer targetUnderlyingManagerAccountId) {
		this.targetUnderlyingManagerAccountId = targetUnderlyingManagerAccountId;
	}


	public BigDecimal getTargetUnderlyingMultiplier() {
		return this.targetUnderlyingMultiplier;
	}


	public void setTargetUnderlyingMultiplier(BigDecimal targetUnderlyingMultiplier) {
		this.targetUnderlyingMultiplier = targetUnderlyingMultiplier;
	}


	public Short getTrancheCount() {
		return this.trancheCount;
	}


	public void setTrancheCount(Short trancheCount) {
		this.trancheCount = trancheCount;
	}


	public Short getCycleDurationWeeks() {
		return this.cycleDurationWeeks;
	}


	public void setCycleDurationWeeks(Short cycleDurationWeeks) {
		this.cycleDurationWeeks = cycleDurationWeeks;
	}


	public String getSecurityTargetNote() {
		return this.securityTargetNote;
	}


	public void setSecurityTargetNote(String securityTargetNote) {
		this.securityTargetNote = securityTargetNote;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public String getTargetType() {
		return this.targetType;
	}


	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}


	public Boolean getTargetUnderlyingQuantityPopulated() {
		return this.targetUnderlyingQuantityPopulated;
	}


	public void setTargetUnderlyingQuantityPopulated(Boolean targetUnderlyingQuantityPopulated) {
		this.targetUnderlyingQuantityPopulated = targetUnderlyingQuantityPopulated;
	}


	public Boolean getTargetUnderlyingNotionalPopulated() {
		return this.targetUnderlyingNotionalPopulated;
	}


	public void setTargetUnderlyingNotionalPopulated(Boolean targetUnderlyingNotionalPopulated) {
		this.targetUnderlyingNotionalPopulated = targetUnderlyingNotionalPopulated;
	}


	public Boolean getTargetUnderlyingManagerAccountPopulated() {
		return this.targetUnderlyingManagerAccountPopulated;
	}


	public void setTargetUnderlyingManagerAccountPopulated(Boolean targetUnderlyingManagerAccountPopulated) {
		this.targetUnderlyingManagerAccountPopulated = targetUnderlyingManagerAccountPopulated;
	}


	public Integer getIssuingCompanyId() {
		return this.issuingCompanyId;
	}


	public void setIssuingCompanyId(Integer issuingCompanyId) {
		this.issuingCompanyId = issuingCompanyId;
	}
}
