package com.clifton.investment.account.calculation.processor;

import com.clifton.core.util.status.Status;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;

import java.util.Set;


/**
 * The <code>InvestmentAccountCalculationProcessor</code> interface defines the methods for processing {@link com.clifton.investment.account.calculation.InvestmentAccountCalculationType}
 *
 * @author manderson
 */
public interface InvestmentAccountCalculationProcessor {


	public Set<InvestmentAccountCalculationSnapshot> processInvestmentAccountCalculationSnapshots(InvestmentAccountCalculationProcessorCommand command, InvestmentAccountCalculationProcessorContext context, Status status);
}
