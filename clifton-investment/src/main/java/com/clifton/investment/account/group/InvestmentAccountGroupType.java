package com.clifton.investment.account.group;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>InvestmentAccountGroupType</code> class classifies InvestmentAccountGroup objects.
 *
 * @author mwacker
 */
public class InvestmentAccountGroupType extends NamedEntity<Short> {

	public static final String INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME = "Related Collateral Accounts";

	////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////

	/**
	 * Indicates if the account group type allows for primary a primary account to be selected.
	 */
	private boolean primaryAccountAllowed;
	/**
	 * Indicates that a single account can be in multiple groups of this type.
	 */
	private boolean accountAllowedInMultipleGroups;

	/**
	 * If set, cannot change the name via UI
	 */
	private boolean systemDefined;


	public boolean isPrimaryAccountAllowed() {
		return this.primaryAccountAllowed;
	}


	public void setPrimaryAccountAllowed(boolean primaryAccountAllowed) {
		this.primaryAccountAllowed = primaryAccountAllowed;
	}


	public boolean isAccountAllowedInMultipleGroups() {
		return this.accountAllowedInMultipleGroups;
	}


	public void setAccountAllowedInMultipleGroups(boolean accountAllowedInMultipleGroups) {
		this.accountAllowedInMultipleGroups = accountAllowedInMultipleGroups;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
}
