package com.clifton.investment.account.calculation;

import java.util.List;


/**
 * @author manderson
 */
public interface InvestmentAccountCalculationService {


	////////////////////////////////////////////////////////////////////////////////
	//////      Investment Account Calculation Type Business Methods         ///////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationType getInvestmentAccountCalculationType(short id);


	public InvestmentAccountCalculationType getInvestmentAccountCalculationTypeByName(String name);


	public List<InvestmentAccountCalculationType> getInvestmentAccountCalculationTypeList();


	public InvestmentAccountCalculationType saveInvestmentAccountCalculationType(InvestmentAccountCalculationType bean);


	public void deleteInvestmentAccountCalculationType(short id);
}
