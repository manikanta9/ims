package com.clifton.investment.account.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


public class InvestmentAccountBusinessServiceSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "businessClient.id")
	private Integer businessClientId;

	@SearchField(searchField = "businessService.id")
	private Short businessServiceId;

	@SearchField(searchField = "discretionType", searchFieldPath = "investmentAccount")
	private String investmentAccountDiscretionType;

	@SearchField(searchField = "clientAccountType", searchFieldPath = "investmentAccount")
	private String clientAccountType;


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public Integer getBusinessClientId() {
		return this.businessClientId;
	}


	public void setBusinessClientId(Integer businessClientId) {
		this.businessClientId = businessClientId;
	}


	public String getInvestmentAccountDiscretionType() {
		return this.investmentAccountDiscretionType;
	}


	public void setInvestmentAccountDiscretionType(String investmentAccountDiscretionType) {
		this.investmentAccountDiscretionType = investmentAccountDiscretionType;
	}


	public String getClientAccountType() {
		return this.clientAccountType;
	}


	public void setClientAccountType(String clientAccountType) {
		this.clientAccountType = clientAccountType;
	}
}
