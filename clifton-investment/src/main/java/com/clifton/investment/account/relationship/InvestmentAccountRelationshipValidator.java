package com.clifton.investment.account.relationship;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentAccountRelationshipValidator</code> ...
 * <p>
 * NOTE: SEE AccountingTransactionInvestmentAccountRelationshipValidator for additional validation
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentAccountRelationshipValidator extends SelfRegisteringDaoValidator<InvestmentAccountRelationship> {

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	@SuppressWarnings("unused")
	public void validate(InvestmentAccountRelationship bean, DaoEventTypes config) throws ValidationException {
		// DO NOTHING, USE THE METHOD WITH THE DAO
	}


	@Override
	public void validate(InvestmentAccountRelationship bean, DaoEventTypes config, ReadOnlyDAO<InvestmentAccountRelationship> dao) throws ValidationException {
		// Retrieve the Mapping that applies to this relationship and validate Entity Modify Condition
		InvestmentAccountRelationshipMapping mapping = validateInvestmentAccountRelationshipMapping(bean, config);
		// If not a delete, continue with additional validation:
		if (!config.isDelete()) {
			// self-referencing is allowed when a mapping allows it ("Cash Location Override" to keep it in the same account)

			// Date Range Check
			ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");

			// validate asset class selections
			if (bean.getAccountAssetClass() != null) {
				String prefix = "Account Asset Class [" + bean.getAccountAssetClass().getAssetClass().getLabel() + "]";
				ValidationUtils.assertTrue(mapping.isAssetClassAllowed(), "The mapping for selected account types and purpose does not allow Asset Class selection");
				ValidationUtils.assertTrue(bean.getAccountAssetClass().getAccount().equals(bean.getReferenceOne()), prefix
						+ " selected is not a valid selection for the main account.  Selected asset class must be from the same account.");
				// If the main account is a client account - make sure processing supports asset classes in order to select.  If an account had asset classes set up
				// and then processing type or service changed those asset classes are no longer used going forward so we don't want them selected
				if (bean.getReferenceOne().getServiceProcessingType() != null) {
					ValidationUtils.assertTrue(bean.getReferenceOne().getServiceProcessingType().getProcessingType().isAssetClassesSupported(),
							"Asset class selection is not allowed for main account [" + bean.getReferenceOne().getNumber() + "] because the account's processing does not support asset classes.");
				}
				ValidationUtils.assertFalse(bean.getAccountAssetClass().isRollupAssetClass(), prefix + " is a rollup asset class.  Rollup Asset class selections are not allowed.");
			}

			// TODO DO WE WANT ACTIVE/INACTIVE VALIDATION AS WELL?
			if (mapping.isOnlyOneAllowedFromMain()) {
				List<InvestmentAccountRelationship> list = dao.findByFields(new String[]{"referenceOne.id", "purpose.id"}, new Object[]{bean.getReferenceOne().getId(), bean.getPurpose().getId()});
				for (InvestmentAccountRelationship rel : CollectionUtils.getIterable(list)) {
					if (!rel.equals(bean) && rel.getReferenceTwo().getType().equals(bean.getReferenceTwo().getType())) { // skip itself
						ValidationUtils.assertFalse(
								DateUtils.isDateBetween(bean.getStartDate(), rel.getStartDate(), rel.getEndDate(), false)
										|| DateUtils.isDateBetween(bean.getEndDate(), rel.getStartDate(), rel.getEndDate(), false),
								"Cannot have more than one overlapping relationship from main account for selected account types and purpose. Found: " + rel.getLabel());
					}
				}
			}
			if (mapping.isOnlyOneAllowedToRelated()) {
				List<InvestmentAccountRelationship> list = dao.findByFields(new String[]{"referenceTwo.id", "purpose.id"}, new Object[]{bean.getReferenceTwo().getId(), bean.getPurpose().getId()});
				for (InvestmentAccountRelationship rel : CollectionUtils.getIterable(list)) {
					if (!rel.equals(bean) && rel.getReferenceOne().getType().equals(bean.getReferenceOne().getType())) { // skip itself
						ValidationUtils.assertFalse(
								DateUtils.isDateBetween(bean.getStartDate(), rel.getStartDate(), rel.getEndDate(), false)
										|| DateUtils.isDateBetween(bean.getEndDate(), rel.getStartDate(), rel.getEndDate(), false),
								"Cannot have more than one overlapping relationship to related account for selected account types and purpose. Found: " + rel.getLabel());
					}
				}
			}
		}
	}


	private InvestmentAccountRelationshipMapping validateInvestmentAccountRelationshipMapping(InvestmentAccountRelationship bean, DaoEventTypes eventType) {
		InvestmentAccountRelationshipMapping mapping = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipMappingActiveFor(bean);
		ValidationUtils.assertNotNull(mapping, "Cannot find mapping between '" + bean.getReferenceOne().getType().getName() + "' and '" + bean.getReferenceTwo().getType().getName()
				+ "' account types for purpose: " + bean.getPurpose().getName() + " and date range [" + DateUtils.fromDateRange(bean.getStartDate(), bean.getEndDate(), true) + "]");
		String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(mapping.getRelationshipEntityModifyCondition(), bean);
		if (!StringUtils.isEmpty(error)) {
			throw new ValidationException(eventType.toString() + " Not Allowed. You do not have permission to modify this Account Relationship because: " + error);
		}
		return mapping;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
