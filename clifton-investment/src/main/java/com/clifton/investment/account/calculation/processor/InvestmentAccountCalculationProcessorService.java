package com.clifton.investment.account.calculation.processor;

import com.clifton.core.util.status.Status;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * @author manderson
 */
public interface InvestmentAccountCalculationProcessorService {


	/**
	 * Processes account calculations for the given calculation type and date(s)
	 * Runs for all accounts - but can optional skip anything that exists
	 *
	 * @return message with execution status (number of accounts, snapshots, etc.)
	 */
	@ModelAttribute("result")
	public Status processInvestmentAccountCalculationSnapshotList(InvestmentAccountCalculationProcessorCommand command);


	/**
	 * Ability to preview snapshot and details for a specific account and date - nothing is saved
	 */
	@ModelAttribute("data")
	public InvestmentAccountCalculationSnapshot previewInvestmentAccountCalculationSnapshot(InvestmentAccountCalculationProcessorCommand command);
}
