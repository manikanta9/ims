package com.clifton.investment.account.calculation.processor;

import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.calculation.calculator.InvestmentAccountCalculationSnapshotCalculator;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
public class InvestmentAccountCalculationProcessorImpl implements InvestmentAccountCalculationProcessor {

	private InvestmentAccountCalculationSnapshotService investmentAccountCalculationSnapshotService;
	private InvestmentAccountService investmentAccountService;

	private SystemBeanService systemBeanService;

	private WorkflowHistoryService workflowHistoryService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * The property on the account to retrieve the SystemBean calculator for the account
	 */
	private String calculatorBeanPropertyName;

	/**
	 * If populated will filter accounts on the given category types.
	 * <p>
	 * For AUM we only include CLIENT and SISTER_CLIENT accounts
	 * We don't run for Virtual Clients or Funds
	 */
	private List<BusinessClientCategoryTypes> clientCategoryTypes;


	/**
	 * If populated, checks workflow history for the account to ensure the account was in the given state on the calculation date
	 * <p>
	 * For AUM - account must be "Active" on the calculation date
	 */
	private List<String> workflowStateNameList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<InvestmentAccountCalculationSnapshot> processInvestmentAccountCalculationSnapshots(InvestmentAccountCalculationProcessorCommand command, InvestmentAccountCalculationProcessorContext context, Status status) {
		Set<InvestmentAccountCalculationSnapshot> snapshotList = new HashSet<>();

		// Keep a map of the actual bean instances by id so we don't have to keep retrieving them
		Map<Integer, InvestmentAccountCalculationSnapshotCalculator> calculatorBeanInstanceMap = new HashMap<>();
		List<InvestmentAccount> accountList = getInvestmentAccountList(command, context);
		int count = 0;
		for (InvestmentAccount investmentAccount : CollectionUtils.getIterable(accountList)) {
			count++;
			status.setMessage("Calculating " + count + " of " + CollectionUtils.getSize(accountList) + " accounts on " + DateUtils.fromDateShort(context.getSnapshotDate()));
			try {
				InvestmentAccountCalculationSnapshot existingSnapshot = context.getExistingAccountSnapshotMap().get(investmentAccount.getId());
				if (existingSnapshot != null && !command.isReprocessExisting()) {
					status.addSkipped("Skipping Account " + investmentAccount.getLabel() + " because snapshot already exists and not re-processing.");
					// Remove it from the existing list as we don't need to save or remove it
					context.getExistingAccountSnapshotMap().remove(investmentAccount.getId());
				}
				else {
					InvestmentAccountCalculationSnapshotCalculator calculator = getInvestmentAccountCalculationSnapshotCalculatorForAccount(investmentAccount, calculatorBeanInstanceMap);
					if (calculator == null) {
						status.addWarning("Cannot calculate Account " + investmentAccount.getLabel() + " because there is no calculator defined to use.");
					}
					else {
						InvestmentAccountCalculationSnapshot snapshot = calculator.calculateInvestmentAccountCalculationSnapshot(investmentAccount, context);
						if (!CollectionUtils.isEmpty(snapshot.getDetailList()) && existingSnapshot != null) {
							// Get the original details and merge them (if possible) so we don't delete/re-insert
							List<InvestmentAccountCalculationSnapshotDetail> existingDetailList = getInvestmentAccountCalculationSnapshotService().getInvestmentAccountCalculationSnapshotDetailListForSnapshot(existingSnapshot.getId());
							mergeSnapshotDetailLists(snapshot.getDetailList(), existingDetailList);
						}
						snapshotList.add(snapshot);
					}
				}
			}
			catch (Throwable e) {
				String errorMessage = "Error processing Account: " + investmentAccount.getLabel() + " on " + DateUtils.fromDateShort(context.getSnapshotDate()) + ExceptionUtils.getOriginalMessage(e);
				status.addError(errorMessage);
				if (!CoreExceptionUtils.isValidationException(e)) {
					LogUtils.error(getClass(), errorMessage, e);
				}
			}
		}
		return snapshotList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected List<InvestmentAccount> getInvestmentAccountList(InvestmentAccountCalculationProcessorCommand command, InvestmentAccountCalculationProcessorContext context) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		if (!CollectionUtils.isEmpty(getClientCategoryTypes())) {
			searchForm.setClientCategoryTypes(CollectionUtils.toArray(getClientCategoryTypes(), BusinessClientCategoryTypes.class));
		}
		if (command.getInvestmentAccountId() != null) {
			searchForm.setId(command.getInvestmentAccountId());
		}
		if (command.getInvestmentAccountGroupId() != null) {
			searchForm.setInvestmentAccountGroupId(command.getInvestmentAccountGroupId());
		}

		List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		if (!CollectionUtils.isEmpty(getWorkflowStateNameList())) {
			accountList = BeanUtils.filter(accountList, account -> isInvestmentAccountWorkflowStateValid(account, context));
		}
		if (CollectionUtils.isEmpty(accountList)) {
			throw new ValidationException("There are no accounts that are valid to calculate for [" + DateUtils.fromDateShort(context.getSnapshotDate()) + "].");
		}
		return accountList;
	}


	@SuppressWarnings("unchecked")
	private boolean isInvestmentAccountWorkflowStateValid(InvestmentAccount account, InvestmentAccountCalculationProcessorContext context) {
		// Use current state if effective date before calculation date - This should be the case most of the time as we are processing latest values daily
		if (DateUtils.isDateAfterOrEqual(context.getSnapshotDate(), account.getWorkflowStateEffectiveStartDate())) {
			return CollectionUtils.contains(getWorkflowStateNameList(), account.getWorkflowState().getName());
		}

		String key = "ACCOUNT_WORKFLOW_HISTORY_" + account.getId();
		List<WorkflowHistory> workflowHistoryList = (List<WorkflowHistory>) context.getValueFromGlobalContextMap(key, () -> getWorkflowHistoryService().getWorkflowHistoryListByWorkflowAwareEntity(account, true));
		for (WorkflowHistory workflowHistory : CollectionUtils.getIterable(workflowHistoryList)) {
			// Compare against the end of the day
			if (workflowHistory.isActiveOnDate(DateUtils.getEndOfDay(context.getSnapshotDate()))) {
				return CollectionUtils.contains(getWorkflowStateNameList(), workflowHistory.getEndWorkflowState().getName());
			}
		}
		return false;
	}


	protected InvestmentAccountCalculationSnapshotCalculator getInvestmentAccountCalculationSnapshotCalculatorForAccount(InvestmentAccount investmentAccount, Map<Integer, InvestmentAccountCalculationSnapshotCalculator> calculatorBeanInstanceMap) {
		SystemBean bean = (SystemBean) BeanUtils.getPropertyValue(investmentAccount, getCalculatorBeanPropertyName());
		if (bean == null) {
			return null;
		}
		if (!calculatorBeanInstanceMap.containsKey(bean.getId())) {
			calculatorBeanInstanceMap.put(bean.getId(), (InvestmentAccountCalculationSnapshotCalculator) getSystemBeanService().getBeanInstance(bean));
		}
		return calculatorBeanInstanceMap.get(bean.getId());
	}


	/**
	 * Applies Ids from existing list to new list where they match up so we can perform updates instead of deletes/inserts
	 */
	private void mergeSnapshotDetailLists(List<InvestmentAccountCalculationSnapshotDetail> newDetailList, List<InvestmentAccountCalculationSnapshotDetail> existingDetailList) {
		Map<Integer, List<InvestmentAccountCalculationSnapshotDetail>> newLevelDetailMap = BeanUtils.getBeansMap(newDetailList, InvestmentAccountCalculationSnapshotDetail::getLevel);
		Map<Integer, List<InvestmentAccountCalculationSnapshotDetail>> existingLevelDetailMap = BeanUtils.getBeansMap(existingDetailList, InvestmentAccountCalculationSnapshotDetail::getLevel);

		int maxLevel = CoreMathUtils.getBeanWithMaxProperty(newDetailList, "level", true).getLevel();

		for (int i = 1; i <= maxLevel; i++) {
			if (i == 1) {
				mergeSnapshotDetailListsImpl(newLevelDetailMap.get(i), existingLevelDetailMap.get(i));
			}
			else {

				Map<Integer, List<InvestmentAccountCalculationSnapshotDetail>> newParentDetailMap = BeanUtils.getBeansMap(BeanUtils.filter(newLevelDetailMap.get(i), snapshotDetail -> !snapshotDetail.getParent().isNewBean()), snapshotDetail -> snapshotDetail.getParent().getId());
				Map<Integer, List<InvestmentAccountCalculationSnapshotDetail>> existingParentDetailMap = BeanUtils.getBeansMap(existingLevelDetailMap.get(i), snapshotDetail -> snapshotDetail.getParent().getId());
				for (Map.Entry<Integer, List<InvestmentAccountCalculationSnapshotDetail>> integerListEntry : newParentDetailMap.entrySet()) {
					mergeSnapshotDetailListsImpl(integerListEntry.getValue(), existingParentDetailMap.get(integerListEntry.getKey()));
				}
			}
		}
	}


	private void mergeSnapshotDetailListsImpl(List<InvestmentAccountCalculationSnapshotDetail> newDetailList, List<InvestmentAccountCalculationSnapshotDetail> existingDetailList) {
		Set<Integer> existingIdsUsed = new HashSet<>();
		if (!CollectionUtils.isEmpty(newDetailList) && !CollectionUtils.isEmpty(existingDetailList)) {
			for (InvestmentAccountCalculationSnapshotDetail newDetail : newDetailList) {
				for (InvestmentAccountCalculationSnapshotDetail existingDetail : existingDetailList) {
					if (!existingIdsUsed.contains(existingDetail.getId())) {
						// Is parent/asset class/security enough?  If there are multiple, the first one will match to the first and the second will get the second because we track ids we already used
						if (CompareUtils.isEqual(newDetail.getInvestmentAssetClass(), existingDetail.getInvestmentAssetClass()) && CompareUtils.isEqual(newDetail.getInvestmentSecurity(), existingDetail.getInvestmentSecurity())) {
							newDetail.setId(existingDetail.getId());
							existingIdsUsed.add(existingDetail.getId());
							break;
						}
					}
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationSnapshotService getInvestmentAccountCalculationSnapshotService() {
		return this.investmentAccountCalculationSnapshotService;
	}


	public void setInvestmentAccountCalculationSnapshotService(InvestmentAccountCalculationSnapshotService investmentAccountCalculationSnapshotService) {
		this.investmentAccountCalculationSnapshotService = investmentAccountCalculationSnapshotService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}


	public String getCalculatorBeanPropertyName() {
		return this.calculatorBeanPropertyName;
	}


	public void setCalculatorBeanPropertyName(String calculatorBeanPropertyName) {
		this.calculatorBeanPropertyName = calculatorBeanPropertyName;
	}


	public List<BusinessClientCategoryTypes> getClientCategoryTypes() {
		return this.clientCategoryTypes;
	}


	public void setClientCategoryTypes(List<BusinessClientCategoryTypes> clientCategoryTypes) {
		this.clientCategoryTypes = clientCategoryTypes;
	}


	public List<String> getWorkflowStateNameList() {
		return this.workflowStateNameList;
	}


	public void setWorkflowStateNameList(List<String> workflowStateNameList) {
		this.workflowStateNameList = workflowStateNameList;
	}
}
