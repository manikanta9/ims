package com.clifton.investment.account.relationship.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentAccountRelationshipPurposeSearchForm</code> class defines search
 * configuration for InvestmentAccountRelationshipPurpose objects.
 *
 * @author vgomelsky
 */
public class InvestmentAccountRelationshipPurposeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	// custom filters based on InvestmentAccountRelationshipMapping
	private Short mainAccountTypeId;
	private Short relatedAccountTypeId;


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getMainAccountTypeId() {
		return this.mainAccountTypeId;
	}


	public void setMainAccountTypeId(Short mainAccountTypeId) {
		this.mainAccountTypeId = mainAccountTypeId;
	}


	public Short getRelatedAccountTypeId() {
		return this.relatedAccountTypeId;
	}


	public void setRelatedAccountTypeId(Short relatedAccountTypeId) {
		this.relatedAccountTypeId = relatedAccountTypeId;
	}
}
