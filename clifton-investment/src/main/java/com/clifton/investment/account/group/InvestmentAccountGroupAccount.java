package com.clifton.investment.account.group;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>InvestmentAccountGroupAccount</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentAccountGroupAccount extends ManyToManyEntity<InvestmentAccountGroup, InvestmentAccount, Integer> implements SystemEntityModifyConditionAware {

	/**
	 * Indicated that this is the primary account for this group.
	 */
	private boolean primary;


	/**
	 * Entity inherits the parent's {@link InvestmentAccountGroup#getEntityModifyCondition()} if the parent group is not system managed.
	 * If the parent group is system managed, the modify condition is ignored (null) allowing the group to be rebuilt after individual {@link InvestmentAccount} additions.
	 */
	@Override
	public SystemCondition getEntityModifyCondition() {
		return isEntityModifyConditionApplicable() ? getReferenceOne().getEntityModifyCondition() : null;
	}



	private boolean isEntityModifyConditionApplicable() {
		return (getReferenceOne() != null) && !getReferenceOne().isSystemManaged();
	}


	public boolean isPrimary() {
		return this.primary;
	}


	public void setPrimary(boolean primary) {
		this.primary = primary;
	}
}
