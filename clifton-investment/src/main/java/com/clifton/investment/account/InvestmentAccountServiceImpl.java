package com.clifton.investment.account;


import com.clifton.business.client.BusinessClientService;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.search.InvestmentAccountBusinessServiceSearchForm;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.account.search.InvestmentAccountSearchFormConfigurer;
import com.clifton.investment.account.search.InvestmentAccountServiceObjectiveSearchForm;
import com.clifton.investment.account.search.InvestmentAccountTypeSearchForm;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The <code>InvestmentAccountServiceImpl</code> ...
 *
 * @author mwacker
 */
@Service
public class InvestmentAccountServiceImpl implements InvestmentAccountService {

	private AdvancedUpdatableDAO<InvestmentAccount, Criteria> investmentAccountDAO;
	private AdvancedReadOnlyDAO<InvestmentAccountType, Criteria> investmentAccountTypeDAO;
	private AdvancedUpdatableDAO<InvestmentAccountBusinessService, Criteria> investmentAccountBusinessServiceDAO;
	private AdvancedUpdatableDAO<InvestmentAccountServiceObjective, Criteria> investmentAccountServiceObjectiveDAO;

	private BusinessClientService businessClientService;
	private BusinessCompanyService businessCompanyService;
	private WorkflowDefinitionService workflowDefinitionService;

	private DaoSingleKeyListCache<InvestmentAccountBusinessService, Integer> investmentAccountBusinessServiceByAccountCache;
	private DaoNamedEntityCache<InvestmentAccountType> investmentAccountTypeCache;

	private static final String OUR_ACCOUNT_TYPE_CACHE_KEY = "OurAccountTypeCacheKey";


	////////////////////////////////////////////////////////////////////////////
	//////        Investment Account Type Business Methods               ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountType getInvestmentAccountType(short id) {
		return getInvestmentAccountTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentAccountType getInvestmentAccountTypeByName(String name) {
		return getInvestmentAccountTypeCache().getBeanForKeyValueStrict(getInvestmentAccountTypeDAO(), name);
	}


	@Override
	public List<InvestmentAccountType> getInvestmentAccountTypeList(InvestmentAccountTypeSearchForm searchForm) {
		return getInvestmentAccountTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountType getInvestmentAccountTypeByOurAccount() {
		InvestmentAccountType investmentAccountType = getInvestmentAccountTypeCache().getBeanForKeyValue(getInvestmentAccountTypeDAO(), OUR_ACCOUNT_TYPE_CACHE_KEY);

		if (investmentAccountType == null) {
			investmentAccountType = CollectionUtils.getOnlyElement(getInvestmentAccountTypeDAO().findByField("ourAccount", true));
			if (investmentAccountType != null) {
				getInvestmentAccountTypeCache().put(OUR_ACCOUNT_TYPE_CACHE_KEY, investmentAccountType);
			}
		}

		return investmentAccountType;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////        Investment Account Business Methods               /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getInvestmentAccountSimple(int id) {
		return getInvestmentAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentAccount getInvestmentAccount(int id) {
		InvestmentAccount result = getInvestmentAccountSimple(id);
		if (result != null && result.getBusinessService() != null) {
			result.setBusinessServiceList(getInvestmentAccountBusinessServiceListByInvestmentAccountId(id));
		}
		return result;
	}


	private List<InvestmentAccountBusinessService> getInvestmentAccountBusinessServiceListByInvestmentAccountId(Integer id) {
		return getInvestmentAccountBusinessServiceByAccountCache().getBeanListForKeyValue(getInvestmentAccountBusinessServiceDAO(), id);
	}


	@Override
	public InvestmentAccount getInvestmentAccountByNumber(String accountNumber) {
		return getInvestmentAccountDAO().findOneByField("number", accountNumber);
	}


	@Override
	public List<InvestmentAccount> getInvestmentAccountListByIds(List<Integer> ids) {
		if (!CollectionUtils.isEmpty(ids)) {
			return getInvestmentAccountDAO().findByPrimaryKeys(ids.toArray(new Integer[ids.size()]));
		}
		return null;
	}


	@Override
	public List<InvestmentAccount> getInvestmentAccountList(InvestmentAccountSearchForm searchForm) {
		return getInvestmentAccountDAO().findBySearchCriteria(new InvestmentAccountSearchFormConfigurer(searchForm, getBusinessClientService(), getWorkflowDefinitionService(),
				getBusinessCompanyService(), this));
	}


	/**
	 * See {@link InvestmentAccountValidator} for additional validation
	 */
	@Override
	@Transactional
	public InvestmentAccount saveInvestmentAccount(InvestmentAccount account) {
		/*
		 * Look up InvestmentAccountBusinessService from original if the InvestmentAccount is not new. This needs to be done prior
		 * to save to ensure invoking isNewBean() returns pre-persisted values since the DTO is mutable.
		 * NOTE: Look up the business service list by ID instead of by getting the original entity. Getting the original entity, when all this
		 * service uses is the business service list, causes updates to prematurely flush during workflow transitions in the observers. The
		 * premature flushes cause the save of the account with a workflow state update to fail in Hibernate with a stale object update.
		 */
		List<InvestmentAccountBusinessService> originalBusinessServiceList = account.isNewBean() ? null : getInvestmentAccountBusinessServiceListByInvestmentAccountId(account.getId());

		InvestmentAccount result = getInvestmentAccountDAO().save(account);
		getInvestmentAccountBusinessServiceDAO().saveList(account.getBusinessServiceList(), originalBusinessServiceList);
		// Reset it so we have the full business service list
		result.setBusinessServiceList(account.getBusinessServiceList());
		return result;
	}


	@Transactional
	@Override
	public void deleteInvestmentAccount(int id) {
		getInvestmentAccountDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////     Investment Account Business Service Business Methods      ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountBusinessService getInvestmentAccountBusinessService(int id) {
		return getInvestmentAccountBusinessServiceDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountBusinessService> getInvestmentAccountBusinessServiceList(final InvestmentAccountBusinessServiceSearchForm searchForm) {
		return getInvestmentAccountBusinessServiceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	//////  Investment Account Service Type Objective Business Methods    //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountServiceObjective getInvestmentAccountServiceObjective(int id) {
		return getInvestmentAccountServiceObjectiveDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountServiceObjective> getInvestmentAccountServiceObjectiveList(InvestmentAccountServiceObjectiveSearchForm searchForm) {
		return getInvestmentAccountServiceObjectiveDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountServiceObjective saveInvestmentAccountServiceObjective(InvestmentAccountServiceObjective bean) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getObjectiveDescription()), "There is no standard description for [" + bean.getReferenceTwo().getName()
				+ "]. Please enter an account specific description for this objective.");
		return getInvestmentAccountServiceObjectiveDAO().save(bean);
	}


	@Override
	public void deleteInvestmentAccountServiceObjective(int id) {
		getInvestmentAccountServiceObjectiveDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////          Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentAccount, Criteria> getInvestmentAccountDAO() {
		return this.investmentAccountDAO;
	}


	public void setInvestmentAccountDAO(AdvancedUpdatableDAO<InvestmentAccount, Criteria> investmentAccountDAO) {
		this.investmentAccountDAO = investmentAccountDAO;
	}


	public AdvancedReadOnlyDAO<InvestmentAccountType, Criteria> getInvestmentAccountTypeDAO() {
		return this.investmentAccountTypeDAO;
	}


	public void setInvestmentAccountTypeDAO(AdvancedReadOnlyDAO<InvestmentAccountType, Criteria> investmentAccountTypeDAO) {
		this.investmentAccountTypeDAO = investmentAccountTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountServiceObjective, Criteria> getInvestmentAccountServiceObjectiveDAO() {
		return this.investmentAccountServiceObjectiveDAO;
	}


	public void setInvestmentAccountServiceObjectiveDAO(AdvancedUpdatableDAO<InvestmentAccountServiceObjective, Criteria> investmentAccountServiceObjectiveDAO) {
		this.investmentAccountServiceObjectiveDAO = investmentAccountServiceObjectiveDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountBusinessService, Criteria> getInvestmentAccountBusinessServiceDAO() {
		return this.investmentAccountBusinessServiceDAO;
	}


	public void setInvestmentAccountBusinessServiceDAO(AdvancedUpdatableDAO<InvestmentAccountBusinessService, Criteria> investmentAccountBusinessServiceDAO) {
		this.investmentAccountBusinessServiceDAO = investmentAccountBusinessServiceDAO;
	}


	public DaoSingleKeyListCache<InvestmentAccountBusinessService, Integer> getInvestmentAccountBusinessServiceByAccountCache() {
		return this.investmentAccountBusinessServiceByAccountCache;
	}


	public void setInvestmentAccountBusinessServiceByAccountCache(DaoSingleKeyListCache<InvestmentAccountBusinessService, Integer> investmentAccountBusinessServiceByAccountCache) {
		this.investmentAccountBusinessServiceByAccountCache = investmentAccountBusinessServiceByAccountCache;
	}


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}


	public DaoNamedEntityCache<InvestmentAccountType> getInvestmentAccountTypeCache() {
		return this.investmentAccountTypeCache;
	}


	public void setInvestmentAccountTypeCache(DaoNamedEntityCache<InvestmentAccountType> investmentAccountTypeCache) {
		this.investmentAccountTypeCache = investmentAccountTypeCache;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}
}
