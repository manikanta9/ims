package com.clifton.investment.account.securitytarget.history;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.history.search.InvestmentAccountSecurityTargetHistorySearchForm;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class InvestmentAccountSecurityTargetHistoryServiceImpl implements InvestmentAccountSecurityTargetHistoryService {

	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	////////////////////////////////////////////////////////////////////////////
	//////  Investment Account Security Target History Business Methods  ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentAccountSecurityTargetHistory> getInvestmentAccountSecurityTargetHistoryList(InvestmentAccountSecurityTargetHistorySearchForm searchForm) {

		AssertUtils.assertNotNull(searchForm.getClientInvestmentAccountId(), "A client account ID must be specified.");
		AssertUtils.assertNotNull(searchForm.getTargetUnderlyingSecurityId(), "An underlying security ID must be specified.");

		// Create an InvestmentAccountSecurityTarget search form that extends the search range by 1 unit and moves back the start of the range by 1 unit to support difference calculations between entities.
		InvestmentAccountSecurityTargetSearchForm securityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();
		BeanUtils.copyProperties(searchForm, securityTargetSearchForm);

		securityTargetSearchForm.setClientInvestmentAccountId(searchForm.getClientInvestmentAccountId());
		securityTargetSearchForm.setTargetUnderlyingSecurityId(searchForm.getTargetUnderlyingSecurityId());

		int originalLimit = searchForm.getLimit();
		securityTargetSearchForm.setLimit(originalLimit + 1);
		securityTargetSearchForm.setStart(searchForm.getStart());

		securityTargetSearchForm.setOrderBy("startDate:desc");
		List<InvestmentAccountSecurityTarget> securityTargetList = getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetList(securityTargetSearchForm);

		ArrayList<InvestmentAccountSecurityTargetHistory> securityTargetHistoryList = new ArrayList<>();

		int entityCountLimit = originalLimit <= securityTargetList.size() ? originalLimit : securityTargetList.size();
		for (int index = 0; index < entityCountLimit; ++index) {
			InvestmentAccountSecurityTarget currentTarget = securityTargetList.get(index);
			// note that previousTarget is considered to have a start date before (previous) to the current target's start date, but its index position is at index+1 due to sorting in descending order.
			InvestmentAccountSecurityTarget previousTarget = index < entityCountLimit - 1 ? securityTargetList.get(index + 1) : null;
			securityTargetHistoryList.add(new InvestmentAccountSecurityTargetHistory(currentTarget, previousTarget));
		}

		int totalSize = securityTargetList instanceof PagingArrayList ? ((PagingArrayList<?>) securityTargetList).getTotalElementCount() : securityTargetHistoryList.size();
		PagingArrayList<InvestmentAccountSecurityTargetHistory> securityTargetHistoryPagingList = new PagingArrayList<>(securityTargetHistoryList, searchForm.getStart(), totalSize, searchForm.getLimit());
		return securityTargetHistoryPagingList;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}
}
