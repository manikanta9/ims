package com.clifton.investment.account.mapping.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentAccountMappingSearchForm</code> class defines search configuration for InvestmentAccountMapping objects.
 *
 * @author davidi
 */
public class InvestmentAccountMappingPurposeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description,fieldName")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String fieldName;

	@SearchField
	private Boolean oneAccountValueMappingPerPurpose;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPurposeDescription() {
		return this.description;
	}


	public void setPurposeDescription(String purposeDescription) {
		this.description = purposeDescription;
	}


	public String getFieldName() {
		return this.fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public Boolean getOneAccountValueMappingPerPurpose() {
		return this.oneAccountValueMappingPerPurpose;
	}


	public void setOneAccountValueMappingPerPurpose(Boolean oneAccountValueMappingPerPurpose) {
		this.oneAccountValueMappingPerPurpose = oneAccountValueMappingPerPurpose;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
