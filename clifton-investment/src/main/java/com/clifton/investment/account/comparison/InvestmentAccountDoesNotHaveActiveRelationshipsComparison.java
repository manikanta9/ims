package com.clifton.investment.account.comparison;


/**
 * The <code>InvestmentAccountDoesNotHaveActiveRelationships</code> returns false if the account has any active relationships...
 * Looks for the account as the main and the related account in relationships active today.  This allows it to work for Client & Holding Accounts
 *
 * @author manderson
 */
public class InvestmentAccountDoesNotHaveActiveRelationshipsComparison extends InvestmentAccountHasActiveRelationshipsComparison {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
