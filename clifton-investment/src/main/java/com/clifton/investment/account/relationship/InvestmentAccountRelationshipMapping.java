package com.clifton.investment.account.relationship;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.system.condition.SystemCondition;

import java.util.Date;


/**
 * The <code>InvestmentAccountRelationshipMapping</code> class defines allowed mappings between investment accounts.
 * <p/>
 * It defines what type of relationship is allowed between investment accounts of two types as well as whether
 * to allow only 1 to 1, 1 to many, many to 1 or many to many.
 *
 * @author vgomelsky
 */
public class InvestmentAccountRelationshipMapping extends BaseEntity<Integer> {

	private InvestmentAccountType mainAccountType;
	private InvestmentAccountType relatedAccountType;
	private InvestmentAccountRelationshipPurpose purpose;

	// specify whether to allow only: 1 to 1, 1 to many, many to 1, many to many
	private boolean onlyOneAllowedFromMain;
	private boolean onlyOneAllowedToRelated;

	/**
	 * Allow Asset Class selection for relationships of this type.
	 */
	private boolean assetClassAllowed;

	/**
	 * Optional note explaining this mapping
	 */
	private String note;

	/**
	 * Required Condition - Condition must pass to true when users
	 * are creating/updating/deleting relationships that apply to this mapping
	 */
	private SystemCondition relationshipEntityModifyCondition;

	private Date startDate;
	private Date endDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return isActiveOnDate(new Date());
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountType getMainAccountType() {
		return this.mainAccountType;
	}


	public void setMainAccountType(InvestmentAccountType mainAccountType) {
		this.mainAccountType = mainAccountType;
	}


	public InvestmentAccountType getRelatedAccountType() {
		return this.relatedAccountType;
	}


	public void setRelatedAccountType(InvestmentAccountType relatedAccountType) {
		this.relatedAccountType = relatedAccountType;
	}


	public InvestmentAccountRelationshipPurpose getPurpose() {
		return this.purpose;
	}


	public void setPurpose(InvestmentAccountRelationshipPurpose purpose) {
		this.purpose = purpose;
	}


	public boolean isOnlyOneAllowedFromMain() {
		return this.onlyOneAllowedFromMain;
	}


	public void setOnlyOneAllowedFromMain(boolean onlyOneAllowedFromMain) {
		this.onlyOneAllowedFromMain = onlyOneAllowedFromMain;
	}


	public boolean isOnlyOneAllowedToRelated() {
		return this.onlyOneAllowedToRelated;
	}


	public void setOnlyOneAllowedToRelated(boolean onlyOneAllowedToRelated) {
		this.onlyOneAllowedToRelated = onlyOneAllowedToRelated;
	}


	public boolean isAssetClassAllowed() {
		return this.assetClassAllowed;
	}


	public void setAssetClassAllowed(boolean assetClassAllowed) {
		this.assetClassAllowed = assetClassAllowed;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public SystemCondition getRelationshipEntityModifyCondition() {
		return this.relationshipEntityModifyCondition;
	}


	public void setRelationshipEntityModifyCondition(SystemCondition relationshipEntityModifyCondition) {
		this.relationshipEntityModifyCondition = relationshipEntityModifyCondition;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
