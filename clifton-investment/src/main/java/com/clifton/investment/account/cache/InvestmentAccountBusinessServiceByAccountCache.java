package com.clifton.investment.account.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.account.InvestmentAccountBusinessService;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentAccountBusinessServiceByAccountCache</code> stores the list of {@link InvestmentAccountBusinessService} objects
 * by account id.
 *
 * @author manderson
 */
@Component
public class InvestmentAccountBusinessServiceByAccountCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentAccountBusinessService, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "investmentAccount.id";
	}


	@Override
	protected Integer getBeanKeyValue(InvestmentAccountBusinessService bean) {
		if (bean.getInvestmentAccount() != null) {
			return bean.getInvestmentAccount().getId();
		}
		return null;
	}
}
