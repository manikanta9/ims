package com.clifton.investment.account.group;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.cache.InvestmentAccountGroupCache;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.account.group.search.InvestmentAccountGroupSearchForm;
import com.clifton.investment.account.group.search.InvestmentAccountGroupTypeSearchForm;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;


@Service
public class InvestmentAccountGroupServiceImpl implements InvestmentAccountGroupService {

	private AdvancedUpdatableDAO<InvestmentAccountGroup, Criteria> investmentAccountGroupDAO;
	private AdvancedUpdatableDAO<InvestmentAccountGroupAccount, Criteria> investmentAccountGroupAccountDAO;
	private AdvancedUpdatableDAO<InvestmentAccountGroupType, Criteria> investmentAccountGroupTypeDAO;

	private InvestmentAccountService investmentAccountService;
	private SystemBeanService systemBeanService;

	private InvestmentAccountGroupCache investmentAccountGroupCache;

	////////////////////////////////////////////////////////////////////////////
	///////           Investment Account Group Business Methods          ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountGroup getInvestmentAccountGroup(int id) {
		return getInvestmentAccountGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentAccountGroup getInvestmentAccountGroupByName(String name) {
		return getInvestmentAccountGroupDAO().findOneByField("name", name);
	}


	@Override
	public List<InvestmentAccountGroup> getInvestmentAccountGroupListByIds(List<Integer> ids) {
		if (!CollectionUtils.isEmpty(ids)) {
			return getInvestmentAccountGroupDAO().findByPrimaryKeys(ids.toArray(new Integer[0]));
		}
		return Collections.emptyList();
	}


	@Override
	public List<InvestmentAccountGroup> getInvestmentAccountGroupList(final InvestmentAccountGroupSearchForm searchForm) {
		return getInvestmentAccountGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountGroup saveInvestmentAccountGroup(InvestmentAccountGroup bean) {
		return getInvestmentAccountGroupDAO().save(bean);
	}


	@Override
	@Transactional
	public void deleteInvestmentAccountGroup(int id) {
		getInvestmentAccountGroupAccountDAO().deleteList(getInvestmentAccountGroupAccountList(new InvestmentAccountGroupAccountSearchForm(id)));
		getInvestmentAccountGroupDAO().delete(id);
	}


	@Override
	public Status rebuildInvestmentAccountGroup(int groupId) {
		InvestmentAccountGroup grp = getInvestmentAccountGroup(groupId);
		Status status = Status.ofEmptyMessage();
		rebuildInvestmentAccountGroupImpl(grp, status);
		return status;
	}


	@Override
	public Status rebuildInvestmentAccountGroupSystemManaged() {
		InvestmentAccountGroupSearchForm searchForm = new InvestmentAccountGroupSearchForm();
		Status status = Status.ofEmptyMessage();
		searchForm.setSystemManaged(true);
		List<InvestmentAccountGroup> grpList = getInvestmentAccountGroupList(searchForm);
		int success = 0;
		int failed = 0;
		for (InvestmentAccountGroup grp : CollectionUtils.getIterable(grpList)) {
			try {
				if (grp.isSystemManaged()) {
					rebuildInvestmentAccountGroupImpl(grp, status);
				}
				success++;
			}
			catch (ValidationException e) {
				failed++;
				status.addError(e.getMessage());
			}
		}
		status.setMessage(String.format("System Managed Investment Accounts rebuilt; Processed: [%d] Success: [%d] Failed: [%d]", grpList.size(), success, failed));
		return status;
	}


	private void rebuildInvestmentAccountGroupImpl(InvestmentAccountGroup group, Status status) {
		if (!group.isSystemManaged()) {
			String errorMessage = String.format("Investment Account Group [%s] is not system managed.  Accounts are manually maintained and cannot be automatically rebuilt.", group.getName());
			status.addError(errorMessage);
			throw new ValidationException(errorMessage);
		}
		EntityGroupRebuildAwareExecutor rebuildBean = (EntityGroupRebuildAwareExecutor) getSystemBeanService().getBeanInstance(group.getRebuildSystemBean());
		rebuildBean.executeRebuild(group, status);
		getInvestmentAccountGroupCache().clearGroupCache(group.getName());
	}


	////////////////////////////////////////////////////////////////////////////
	////           Investment Account Group Account Business Methods        ////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountGroupAccount getInvestmentAccountGroupAccount(int id) {
		return getInvestmentAccountGroupAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountGroupAccount> getInvestmentAccountGroupAccountList(InvestmentAccountGroupAccountSearchForm searchForm) {
		return getInvestmentAccountGroupAccountDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private InvestmentAccountGroupAccount getInvestmentAccountGroupAccountByGroupAndAccount(int groupId, int accountId) {
		return getInvestmentAccountGroupAccountDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{groupId, accountId});
	}


	@Override
	public InvestmentAccountGroupAccount getPrimaryInvestmentAccountGroupAccountByGroup(int groupId) {
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setGroupId(groupId);
		searchForm.setPrimary(true);
		return CollectionUtils.getOnlyElement(getInvestmentAccountGroupAccountList(searchForm));
	}


	@Override
	public List<InvestmentAccount> getInvestmentAccountListByGroup(String groupName) {
		return getInvestmentAccountGroupCache().getInvestmentAccountListByGroup(groupName);
	}


	@Override
	public boolean isInvestmentAccountInGroup(int accountId, String groupName) {
		return getInvestmentAccountGroupCache().isInvestmentAccountInGroup(groupName, accountId);
	}


	@Override
	public boolean isInvestmentAccountInGroupOfType(int accountId, String groupTypeName) {
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setAccountId(accountId);
		searchForm.setGroupTypeName(groupTypeName);
		InvestmentAccountGroupAccount groupAccount = CollectionUtils.getFirstElement(getInvestmentAccountGroupAccountList(searchForm));
		return (groupAccount != null);
	}


	private boolean isInvestmentAccountInGroupOfType(final int accountId, final short groupTypeId) {
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setAccountId(accountId);
		searchForm.setGroupTypeId(groupTypeId);
		InvestmentAccountGroupAccount groupAccount = CollectionUtils.getFirstElement(getInvestmentAccountGroupAccountList(searchForm));
		return (groupAccount != null);
	}


	@Override
	public void linkInvestmentAccountGroupToAccount(int groupId, int accountId) {
		InvestmentAccountGroup group = getInvestmentAccountGroup(groupId);
		if (!group.getType().isAccountAllowedInMultipleGroups()) {
			ValidationUtils.assertFalse(isInvestmentAccountInGroupOfType(accountId, group.getType().getId()), "Cannot link investment account [" + accountId + "] because group type ["
					+ group.getType().getLabel() + "] does not allow accounts to be assigned to multiple groups.");
		}
		InvestmentAccountGroupAccount groupAccount = new InvestmentAccountGroupAccount();
		groupAccount.setReferenceOne(group);
		groupAccount.setReferenceTwo(getInvestmentAccountService().getInvestmentAccount(accountId));
		saveInvestmentAccountGroupAccount(groupAccount);
	}


	@Override
	@Transactional
	public void linkInvestmentAccountGroupToAccountList(InvestmentAccountGroup group, List<InvestmentAccount> accountList) {
		if (group != null && !CollectionUtils.isEmpty(accountList)) {
			for (InvestmentAccount account : accountList) {
				InvestmentAccountGroupAccount groupAccount = new InvestmentAccountGroupAccount();
				groupAccount.setReferenceOne(group);
				groupAccount.setReferenceTwo(account);
				getInvestmentAccountGroupAccountDAO().save(groupAccount);
			}
			getInvestmentAccountGroupCache().clearGroupCache(group.getName());
		}
	}


	@Override
	@Transactional
	public InvestmentAccountGroupAccount saveInvestmentAccountGroupAccount(InvestmentAccountGroupAccount bean) {
		if (bean.isPrimary()) {
			ValidationUtils.assertTrue(bean.getReferenceOne().getType().isPrimaryAccountAllowed(), "Account group [" + bean.getReferenceOne().getLabel() + "] do not support setting primary accounts.");

			// if a primary exists, then set change if to not be the primary
			InvestmentAccountGroupAccount originalPrimary = getPrimaryInvestmentAccountGroupAccountByGroup(bean.getReferenceOne().getId());
			if (originalPrimary != null && !originalPrimary.equals(bean)) {
				originalPrimary.setPrimary(false);
				getInvestmentAccountGroupAccountDAO().save(originalPrimary);
			}
		}
		ValidationUtils.assertFalse(bean.getReferenceOne().isSystemManaged(), "Investment Account Group [" + bean.getReferenceOne().getName() + "] is system managed.  You cannot manually add accounts.");
		bean = getInvestmentAccountGroupAccountDAO().save(bean);
		getInvestmentAccountGroupCache().clearGroupCache(bean.getReferenceOne().getName());
		return bean;
	}


	@Override
	public void deleteInvestmentAccountGroupAccount(int groupId, int accountId) {
		InvestmentAccountGroup group = getInvestmentAccountGroup(groupId);
		if (group != null) {
			ValidationUtils.assertFalse(group.isSystemManaged(), "Investment Account Group [" + group.getName() + "] is system managed.  You cannot manually remove accounts.");
			getInvestmentAccountGroupCache().clearGroupCache(group.getName());
			getInvestmentAccountGroupAccountDAO().delete(getInvestmentAccountGroupAccountByGroupAndAccount(groupId, accountId));
		}
	}


	@Override
	public void deleteInvestmentAccountGroupAccount(InvestmentAccountGroupAccount bean) {
		if (bean != null) {
			getInvestmentAccountGroupAccountDAO().delete(bean);
			getInvestmentAccountGroupCache().clearGroupCache(bean.getReferenceOne().getName());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////           Investment Account Group Type Business Methods           ////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountGroupType getInvestmentAccountGroupType(short id) {
		return getInvestmentAccountGroupTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountGroupType> getInvestmentAccountGroupTypeList(InvestmentAccountGroupTypeSearchForm searchForm) {
		return getInvestmentAccountGroupTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountGroupType saveInvestmentAccountGroupType(InvestmentAccountGroupType bean) {
		return getInvestmentAccountGroupTypeDAO().save(bean);
	}


	@Override
	public void deleteInvestmentAccountGroupType(short id) {
		getInvestmentAccountGroupTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentAccountGroup, Criteria> getInvestmentAccountGroupDAO() {
		return this.investmentAccountGroupDAO;
	}


	public void setInvestmentAccountGroupDAO(AdvancedUpdatableDAO<InvestmentAccountGroup, Criteria> investmentAccountGroupDAO) {
		this.investmentAccountGroupDAO = investmentAccountGroupDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountGroupAccount, Criteria> getInvestmentAccountGroupAccountDAO() {
		return this.investmentAccountGroupAccountDAO;
	}


	public void setInvestmentAccountGroupAccountDAO(AdvancedUpdatableDAO<InvestmentAccountGroupAccount, Criteria> investmentAccountGroupAccountDAO) {
		this.investmentAccountGroupAccountDAO = investmentAccountGroupAccountDAO;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public AdvancedUpdatableDAO<InvestmentAccountGroupType, Criteria> getInvestmentAccountGroupTypeDAO() {
		return this.investmentAccountGroupTypeDAO;
	}


	public void setInvestmentAccountGroupTypeDAO(AdvancedUpdatableDAO<InvestmentAccountGroupType, Criteria> investmentAccountGroupTypeDAO) {
		this.investmentAccountGroupTypeDAO = investmentAccountGroupTypeDAO;
	}


	public InvestmentAccountGroupCache getInvestmentAccountGroupCache() {
		return this.investmentAccountGroupCache;
	}


	public void setInvestmentAccountGroupCache(InvestmentAccountGroupCache investmentAccountGroupCache) {
		this.investmentAccountGroupCache = investmentAccountGroupCache;
	}
}
