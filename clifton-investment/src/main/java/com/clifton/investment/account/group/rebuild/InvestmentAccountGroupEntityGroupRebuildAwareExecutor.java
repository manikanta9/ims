package com.clifton.investment.account.group.rebuild;

import com.clifton.system.bean.SystemBeanGroup;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;


/**
 * <code>InvestmentAccountGroupEntityGroupRebuildAwareExecutor</code> is an interface that extends {@link EntityGroupRebuildAwareExecutor}
 * but offers the same functionality. The reason for the new interface is because the SystemBean framework only allows
 * an interface to be referenced by one {@link SystemBeanGroup}.
 *
 * @author NickK
 */
public interface InvestmentAccountGroupEntityGroupRebuildAwareExecutor extends EntityGroupRebuildAwareExecutor {

}
