package com.clifton.investment.account.calculation.processor;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.calculation.InvestmentAccountCalculationService;
import com.clifton.investment.account.calculation.InvestmentAccountCalculationType;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotService;
import com.clifton.investment.account.calculation.snapshot.search.InvestmentAccountCalculationSnapshotSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.clifton.core.util.date.DateUtils.fromDate;
import static com.clifton.core.util.status.Status.ofMessage;


/**
 * @author manderson
 */
@Service
public class InvestmentAccountCalculationProcessorServiceImpl implements InvestmentAccountCalculationProcessorService {


	private InvestmentAccountCalculationService investmentAccountCalculationService;
	private InvestmentAccountCalculationSnapshotService investmentAccountCalculationSnapshotService;

	private RunnerHandler runnerHandler;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status processInvestmentAccountCalculationSnapshotList(final InvestmentAccountCalculationProcessorCommand command) {
		ValidationUtils.assertNotNull(command.getCalculationTypeId(), "Calculation Type is Required.");
		ValidationUtils.assertNotNull(command.getStartSnapshotDate(), "Start Snapshot Date is Required.");
		ValidationUtils.assertNotNull(command.getEndSnapshotDate(), "End Snapshot Date is Required.");
		ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(command.getEndSnapshotDate(), command.getStartSnapshotDate()), "Invalid Dates entered.  Start date must be on or before end date.");
		ValidationUtils.assertFalse(DateUtils.isDateAfter(command.getEndSnapshotDate(), new Date()), "Invalid Dates entered.  Snapshot dates cannot process for future dates.");

		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Synchronously running for " + command);
			doProcessInvestmentAccountCalculationSnapshots(command, status);
			return status;
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("INVESTMENT-ACCOUNT-CALCULATION-SNAPSHOTS", runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					doProcessInvestmentAccountCalculationSnapshots(command, statusHolder.getStatus());
				}
				catch (Throwable e) {
					getStatus().setMessage("Error rebuilding snapshots for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding snapshots for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);

		return Status.ofMessage("Started snapshots rebuild: " + runId + ". Processing will be completed shortly.");
	}


	@Override
	public InvestmentAccountCalculationSnapshot previewInvestmentAccountCalculationSnapshot(InvestmentAccountCalculationProcessorCommand command) {
		ValidationUtils.assertNotNull(command.getInvestmentAccountId(), "Account selection is required in order to preview.");
		ValidationUtils.assertNotNull(command.getStartSnapshotDate(), "Start Snapshot Date is required");
		ValidationUtils.assertNotNull(command.getEndSnapshotDate(), "End Snapshot Date is required");
		ValidationUtils.assertEquals(command.getStartSnapshotDate(), command.getEndSnapshotDate(), "Start and End Snapshot Dates must be equal for preview feature.");

		InvestmentAccountCalculationType calculationType = getInvestmentAccountCalculationService().getInvestmentAccountCalculationType(command.getCalculationTypeId());
		InvestmentAccountCalculationProcessorContext context = new InvestmentAccountCalculationProcessorContext(calculationType, command);
		context.setupContextForDate(command.getStartSnapshotDate(), null);
		InvestmentAccountCalculationProcessor processor = (InvestmentAccountCalculationProcessor) getSystemBeanService().getBeanInstance(calculationType.getProcessorSystemBean());

		Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Preview processing for " + command);
		Set<InvestmentAccountCalculationSnapshot> snapshotList = processor.processInvestmentAccountCalculationSnapshots(command, context, status);
		if (CollectionUtils.isEmpty(snapshotList)) {
			throw new ValidationException("Did not calculate any values");
		}
		if (CollectionUtils.getSize(snapshotList) > 1) {
			throw new ValidationException("Expected one result, but calculated " + snapshotList.size());
		}
		if (status.getErrorCount() > 1) {
			throw new ValidationException("Preview Failed: " + StringUtils.collectionToCommaDelimitedString(status.getErrorList(), StatusDetail::getNote));
		}
		InvestmentAccountCalculationSnapshot snapshot = CollectionUtils.getFirstElementStrict(snapshotList);
		// Add Ids to Fix Detail List Sorting and so it doesn't get marked as modified in UI
		int counter = -10;
		for (InvestmentAccountCalculationSnapshotDetail detail : CollectionUtils.getIterable(snapshot.getDetailList())) {
			detail.setId(counter);
			counter += -10;
		}
		snapshot.setDetailList(BeanUtils.sortWithFunction(snapshot.getDetailList(), snapshotDetail -> (snapshotDetail.isChild() ? (snapshotDetail.getParent().getId() + "_" + snapshotDetail.getId()) : snapshotDetail.getId()) + "_", true));
		return CollectionUtils.getFirstElement(snapshotList);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void doProcessInvestmentAccountCalculationSnapshots(InvestmentAccountCalculationProcessorCommand command, Status status) {
		InvestmentAccountCalculationType calculationType = getInvestmentAccountCalculationService().getInvestmentAccountCalculationType(command.getCalculationTypeId());
		InvestmentAccountCalculationProcessor processor = (InvestmentAccountCalculationProcessor) getSystemBeanService().getBeanInstance(calculationType.getProcessorSystemBean());

		InvestmentAccountCalculationProcessorContext context = new InvestmentAccountCalculationProcessorContext(calculationType, command);

		Date snapshotDate = getNextSnapshotDate(null, command.getStartSnapshotDate(), command.getEndSnapshotDate());
		int snapshotsProcessed = 0;
		long start = System.currentTimeMillis();
		// Used to accurately track how many were skipped because status details max out at 1000
		int originalExistingSize = 0;
		int newExistingSize = 0;

		while (snapshotDate != null) {
			InvestmentAccountCalculationSnapshotSearchForm searchForm = new InvestmentAccountCalculationSnapshotSearchForm();
			searchForm.setCalculationTypeId(calculationType.getId());
			searchForm.setSnapshotDate(snapshotDate);
			searchForm.setInvestmentAccountId(command.getInvestmentAccountId());
			searchForm.setInvestmentAccountGroupId(command.getInvestmentAccountGroupId());
			Set<InvestmentAccountCalculationSnapshot> existingSnapshotList = new HashSet<>(getInvestmentAccountCalculationSnapshotService().getInvestmentAccountCalculationSnapshotList(searchForm, false));
			originalExistingSize = originalExistingSize + CollectionUtils.getSize(existingSnapshotList);

			context.setupContextForDate(snapshotDate, existingSnapshotList);
			Set<InvestmentAccountCalculationSnapshot> snapshotList = processor.processInvestmentAccountCalculationSnapshots(command, context, status);
			// Retrieve the existing snapshot list from the context as anything that is skipped is removed so we don't have to save it
			existingSnapshotList = new HashSet<>(context.getExistingAccountSnapshotMap().values());
			newExistingSize = newExistingSize + CollectionUtils.getSize(existingSnapshotList);
			if (!CollectionUtils.isEmpty(snapshotList) || !CollectionUtils.isEmpty(existingSnapshotList)) {
				getInvestmentAccountCalculationSnapshotService().saveInvestmentAccountCalculationSnapshotList(snapshotList, existingSnapshotList);
			}
			snapshotsProcessed += CollectionUtils.getSize(snapshotList);
			status.setMessage("Total Snapshots Processed: " + snapshotsProcessed + ", Last Calculation Date: " + DateUtils.fromDateShort(snapshotDate));
			snapshotDate = getNextSnapshotDate(snapshotDate, command.getStartSnapshotDate(), command.getEndSnapshotDate());
		}
		int skipCount = originalExistingSize - newExistingSize;
		String duration = DateUtils.getTimeDifference(System.currentTimeMillis() - start, true);
		status.setMessageWithErrors("Processing Complete (" + duration + "). Total Snapshots Processed: " + snapshotsProcessed + (skipCount > 0 ? (", Skipped: " + skipCount) : "") + (status.getWarningCount() > 0 ? (", Warnings: " + status.getWarningCount()) : ""), 5);
	}


	private Date getNextSnapshotDate(Date snapshotDate, Date startDate, Date endDate) {
		// If calculationDate is null, then start with the first weekday as of the start date
		if (snapshotDate == null) {
			if (DateUtils.isWeekday(startDate) || DateUtils.isLastDayOfMonth(startDate)) {
				return startDate;
			}
			snapshotDate = startDate;
		}
		Date nextSnapshotDate = DateUtils.addDays(snapshotDate, 1);
		if (DateUtils.isDateAfter(nextSnapshotDate, endDate)) {
			return null;
		}
		if (DateUtils.isWeekday(nextSnapshotDate) || DateUtils.isLastDayOfMonth(nextSnapshotDate)) {
			return nextSnapshotDate;
		}
		return getNextSnapshotDate(nextSnapshotDate, startDate, endDate);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////               Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationService getInvestmentAccountCalculationService() {
		return this.investmentAccountCalculationService;
	}


	public void setInvestmentAccountCalculationService(InvestmentAccountCalculationService investmentAccountCalculationService) {
		this.investmentAccountCalculationService = investmentAccountCalculationService;
	}


	public InvestmentAccountCalculationSnapshotService getInvestmentAccountCalculationSnapshotService() {
		return this.investmentAccountCalculationSnapshotService;
	}


	public void setInvestmentAccountCalculationSnapshotService(InvestmentAccountCalculationSnapshotService investmentAccountCalculationSnapshotService) {
		this.investmentAccountCalculationSnapshotService = investmentAccountCalculationSnapshotService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
