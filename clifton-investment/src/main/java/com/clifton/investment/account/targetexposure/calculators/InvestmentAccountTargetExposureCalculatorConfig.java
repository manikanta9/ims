package com.clifton.investment.account.targetexposure.calculators;

import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;


/**
 * <code>InvestmentAccountTargetExposureCalculatorConfig</code> contains details used in calculating target exposure adjustments for an account asset class.
 *
 * @author nickk
 * @see InvestmentAccountTargetExposureCalculator
 */
public class InvestmentAccountTargetExposureCalculatorConfig {

	private InvestmentAccount clientAccount;
	private Date balanceDate;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static InvestmentAccountTargetExposureCalculatorConfig forClientAccountOnBalanceDate(InvestmentAccount clientAccount, Date balanceDate) {
		InvestmentAccountTargetExposureCalculatorConfig config = new InvestmentAccountTargetExposureCalculatorConfig();
		config.setClientAccount(clientAccount);
		config.setBalanceDate(balanceDate);
		return config;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}
}
