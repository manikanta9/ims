package com.clifton.investment.account.assetclass.rebalance;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>InvestmentAccountRebalanceAssetClassHistory</code> ...
 *
 * @author manderson
 */
public class InvestmentAccountRebalanceAssetClassHistory extends BaseSimpleEntity<Integer> {

	private InvestmentAccountRebalanceHistory rebalanceHistory;

	private InvestmentAccountAssetClass accountAssetClass;

	private BigDecimal beforeRebalanceCash;

	private BigDecimal afterRebalanceCash;


	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	public BigDecimal getAdjustmentValue() {
		return MathUtils.subtract(getAfterRebalanceCash(), getBeforeRebalanceCash());
	}


	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	public InvestmentAccountRebalanceHistory getRebalanceHistory() {
		return this.rebalanceHistory;
	}


	public void setRebalanceHistory(InvestmentAccountRebalanceHistory rebalanceHistory) {
		this.rebalanceHistory = rebalanceHistory;
	}


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public void setAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
	}


	public BigDecimal getBeforeRebalanceCash() {
		return this.beforeRebalanceCash;
	}


	public void setBeforeRebalanceCash(BigDecimal beforeRebalanceCash) {
		this.beforeRebalanceCash = beforeRebalanceCash;
	}


	public BigDecimal getAfterRebalanceCash() {
		return this.afterRebalanceCash;
	}


	public void setAfterRebalanceCash(BigDecimal afterRebalanceCash) {
		this.afterRebalanceCash = afterRebalanceCash;
	}
}
