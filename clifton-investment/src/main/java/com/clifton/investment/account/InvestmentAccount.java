package com.clifton.investment.account;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyPlatform;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.service.AccountingClosingMethods;
import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.shared.BaseAccount;
import com.clifton.investment.shared.ClientAccount;
import com.clifton.investment.shared.HoldingAccount;
import com.clifton.security.user.SecurityGroup;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.workflow.BaseNamedWorkflowEffectiveDateAwareEntity;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccount</code> class represents an investment account that can
 * be used to track investment positions and provide corresponding reporting.
 *
 * @author vgomelsky
 */
public class InvestmentAccount extends BaseNamedWorkflowEffectiveDateAwareEntity implements SystemColumnCustomValueAware {

	public static final String INVESTMENT_ACCOUNT_TABLE_NAME = "InvestmentAccount";

	// System Defined Custom Column Group Name on the Holding Account
	public static final String HOLDING_ACCOUNT_COLUMN_GROUP_NAME = "Holding Account Fields";

	// System Defined Dated Value Custom Column Group Name on the Client Account
	public static final String CLIENT_ACCOUNT_INFO_COLUMN_GROUP_NAME = "Client Account Info";

	// Client Account PIOS Processing Column Group
	public static final String CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME = "PIOS Processing";

	// Client Account Portfolio Setup Column Group
	public static final String CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME = "Portfolio Setup";

	// LDI Target Contract Calculator enable / disable Custom Column
	public static final String CLIENT_ACCOUNT_PORTFOLIO_RUN_LDI_TARGET_CONTRACT_CALCULATOR = "Portfolio Run LDI Target Contract Calculator";

	// LDI Cash Flow Custom Columns
	public static final String LDI_CASH_FLOW_DISCOUNT_CURVE = "Discount Curve";
	public static final String LDI_CASH_FLOW_DISCOUNT_CURVE_DAYS_TILL_OUTDATED_DATA = "Max Days Till Outdated Curve Data";
	public static final String LDI_CASH_FLOW_DV01_PERIODICITY = "DV01 Periodicity";
	public static final String LDI_CASH_FLOW_SHOCK_AMOUNT_BPS = "Curve Shock Amount (bps)";
	public static final String LDI_CASH_FLOW_DO_NOT_PROCESS = "Do Not Process Cash Flow History for LDI Runs";

	// Holding Account Custom Columns
	public static final String HOLDING_ACCOUNT_COLLATERAL_TRANSFER_ROUNDING = "Collateral Transfer Rounding";
	public static final String HOLDING_ACCOUNT_M2M_LOCAL_CURRENCY = "M2M in Local Currency";

	// Client Account Info Dated Custom Columns
	public static final String CLIENT_ACCOUNT_RELATIONSHIP_MANAGER_OVERRIDE = "RelationshipManagerCompanyContactID";

	// Client Account Fields Custom Columns
	public static final String CLIENT_ACCOUNT_COLLATERAL_EXCESS_TARGET_PERCENT_CUSTOM_COLUMN = "Collateral Excess Target Percent";

	// Client Account Portfolio Setup Fields Custom Column
	public static final String CLIENT_ACCOUNT_PORTFOLIO_CASH_FLOW_GROUP_TYPE = "Portfolio Cash Flow Group Type";

	// Grouped Margin Account Field Custom Column for PIOS and LDI
	public static final String GROUPED_MARGIN_ACCOUNT = "Grouped Margin Account";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private InvestmentAccountType type;

	/**
	 * The company that issued this account. Examples: specific bank, broker, etc.
	 */
	private BusinessCompany issuingCompany;

	/**
	 * The Clifton client that this account is tied to.
	 */
	private BusinessClient businessClient;

	/**
	 * Some account types are tied to corresponding business contracts: OTC Collateral is tied to ISDA, etc.
	 * The contract must be for the same client.
	 */
	private BusinessContract businessContract;

	/**
	 * The base currency if the account (as defined by InvestmentSecurityID)
	 */
	private InvestmentSecurity baseCurrency;

	/**
	 * The default company (uses parent name if applies) to use for retrieving FX rates.
	 * Applies to both Client and Holding Accounts and can be used to override default behaviour.
	 * <p>
	 * Whenever possible, holding account FX Source is used: COALESCE(Holding Account FX Source, Holding Account Issuer, Default: Goldman Sachs)
	 * <p>
	 * When holding account is not available (manager balances, billing, etc.), client account FX Source is used: COALESCE(Client Account FX Source, Default: Goldman Sachs).
	 * <p>
	 * For client account FX Source, the largest use case of this is currently Portfolio Runs.
	 * Also, Manager Balances (When Manager is in different CCY then Account) and Billing (When Billing CCY is different than Account).
	 */
	private BusinessCompany defaultExchangeRateSourceCompany;

	/**
	 * The business service that this investment account is used for.
	 * Could be empty.
	 */
	private BusinessService businessService;

	/**
	 * Optionally overrides Accounting Closing Method (FIFO, LIFO, etc.) specified for account's Business Service.
	 */
	private AccountingClosingMethods accountingClosingMethodOverride;


	/**
	 * Populated if different than the business service AUM calculator bean
	 * SystemBean must implement {@link com.clifton.investment.account.calculation.calculator.InvestmentAccountCalculationSnapshotCalculator}
	 */
	private SystemBean aumCalculatorOverrideBean;


	/**
	 * Multiple BusinessService(s) which are sub-services of selected main "businessService"
	 * can be associated with this account: Cash Securitization, Rebalancing, etc. for PIOS.
	 */
	private List<InvestmentAccountBusinessService> businessServiceList;

	/*
	 * The BusinessServiceProcessingType associated with the account.
	 */
	private BusinessServiceProcessingType serviceProcessingType;

	/**
	 * Security Group whose members are the team that handles this account
	 */
	private SecurityGroup teamSecurityGroup;

	/**
	 * Unique issuer account number assigned to this account by the issuer.
	 */
	private String number;
	/**
	 * Optional alternate account number. Some account types may have 2 different account numbers.
	 * For example, Custodian accounts will have "Custodian Number" and "Accounting Number" (number2).
	 */
	private String number2;

	/**
	 * Optional short name for accounts with long names. Used in label when set.
	 */
	private String shortName;

	/**
	 * Used for Holding Accounts to indicate that Trading for the account is directed by the Client
	 */
	private boolean clientDirected;

	/**
	 * Specifies whether this account is used for speculation rather than hedging (speculators usually have higher margin requirements)
	 */
	private boolean usedForSpeculation;

	/**
	 * This field applies to holding accounts and is usually equal to 1 (no additional requirements: use standard).
	 * However, a broker may deem a certain account to be more risky and impose additional requirement: 1.5, 2.5, 3.25, etc.
	 */
	private BigDecimal initialMarginMultiplier;

	/**
	 * Applies to Client Accounts Only
	 */
	private Date inceptionDate; // AKA Positions On Date
	private Date performanceInceptionDate; // Usually = Positions On Date, but When we started tracking performance for the account

	/**
	 * Client Account type - options can be found in System List Item "Client Account Types"
	 * Label changed to "Pooled Vehicle Type"
	 * Used for client accounts to classify accounts - i.e. 40 ACT Fund
	 */
	private String clientAccountType;

	/**
	 * Applies to Client Accounts only
	 * Designated unit to be credited with the "sale" of the product used by the client
	 * Examples: EVD - Eaton Vance Distributors, INST - Institutional Sales & Client Services Management
	 * See System List: "Client Account Channels" for full list
	 */
	private SystemListItem clientAccountChannel;


	/**
	 * Selected options using system lists to classify our client accounts
	 * Values are from EV and help define where they put our AUM and Projected Revenue (INVESTMENT-424)
	 */
	private SystemListItem externalPortfolioCode;
	private SystemListItem externalProductCode;

	/**
	 * "GIPS"  Discretion on the account - options can be found in System List Item "Client Account Discretion Options (GIPS)"
	 * Used for client accounts
	 */
	private String discretionType;

	/**
	 * Applies to Holding Accounts Only - company on the platform must be the same as the issuing company of the account
	 * Broker Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers’ strategies.
	 */
	private BusinessCompanyPlatform companyPlatform;


	/**
	 * Applies to Holding Accounts Only - Differentiates accounts where the client pays a periodic account fee rather than trade commissions.
	 * Is tied to "Platform" in IMS so that we may differentiate between clients who are vs. are not utilizing the platform WRAP feature
	 * (retail clients generally do (Institutional clients generally do not) participate in the WRAP feature/account structure on a platform.
	 */
	private boolean wrapAccount;

	/**
	 * A List of custom column values for the accounts
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;

	/**
	 * An optional field that can be conditionally populated based on the configured {@link BusinessServiceProcessingType#ACCOUNT_PURPOSE_REPORTING_ACCOUNT_NUMBER}
	 * for a client account's {@link BusinessServiceProcessingType}. The custom field is the purpose to use in finding a related account (expected to be one result).
	 */
	@NonPersistentField
	private InvestmentAccount processingTypePurposeAccount;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructor for testing convenience
	 */
	public InvestmentAccount(int id, String clientName) {
		this.setId(id);
		this.setName(clientName);
	}


	public InvestmentAccount() {
		//empty
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ClientAccount toClientAccount() {
		if (getType() != null && !getType().isOurAccount()) {
			throw new IllegalStateException("Cannot convert Investment Account that is not of 'Our Account' Type to a Client Account: " + getLabel());
		}
		ClientAccount result = new ClientAccount();
		populateBaseAccountFields(result);
		result.setShortName(getShortName());
		return result;
	}


	public HoldingAccount toHoldingAccount() {
		if (getType() != null && getType().isOurAccount()) {
			throw new IllegalStateException("Cannot convert Investment Account that is of 'Our Account' Type to a Holding Account: " + getLabel());
		}
		HoldingAccount result = new HoldingAccount();
		populateBaseAccountFields(result);
		result.setAccountNumber2(getNumber2());
		if (getType() != null) {
			result.setAccountType(getType().getName());
		}
		if (getIssuingCompany() != null) {
			result.setIssuer(getIssuingCompany().toCompany());
		}
		return result;
	}


	private void populateBaseAccountFields(BaseAccount account) {
		if (getId() != null) {
			account.setId(getId());
		}
		account.setAccountNumber(getNumber());
		account.setAccountName(getName());
		if (getWorkflowState() != null) {
			account.setWorkflowState(getWorkflowState().getName());
		}
		if (getBaseCurrency() != null) {
			account.setBaseCurrencySymbol(getBaseCurrency().getSymbol());
		}
		if (getDefaultExchangeRateSourceCompany() != null) {
			account.setDefaultExchangeRateSourceCompany(getDefaultExchangeRateSourceCompany().toCompany());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		result.append(this.number);
		result.append(": ");
		if (this.shortName != null) {
			result.append(this.shortName);
		}
		else if (StringUtils.isEmpty(getName())) {
			if (this.businessClient != null) {
				result.append(this.businessClient.getName());
				if (this.issuingCompany != null) {
					result.append(" - ");
					result.append(this.issuingCompany.getName());
				}
			}
		}
		else {
			result.append(getName());
		}
		if (this.type != null && !this.type.isOurAccount()) {
			result.append(" (");
			result.append(this.type.getName());
			result.append(')');
		}
		return result.toString();
	}


	public String getLabelShort() {
		StringBuilder result = new StringBuilder();
		if (this.shortName != null) {
			result.append(this.shortName);
		}
		else if (StringUtils.isEmpty(getName())) {
			if (this.businessClient != null) {
				result.append(this.businessClient.getName());
			}
		}
		else {
			result.append(getName());
		}
		return result.toString();
	}


	/**
	 * Returns true if the current WorkflowStatus.name for the account = Active
	 */
	public boolean isActive() {
		return getWorkflowStatus() != null && WorkflowStatus.STATUS_ACTIVE.equals(getWorkflowStatus().getName());
	}


	/**
	 * Returns true if this investment account is in terminated workflow state (Closed, Closed - Historical Correction, Terminated, Terminated - Historical Correction, Paused - Pending Termination)
	 * and the specified date is after the termination date (Workflow State Effective Start Date) for this account.
	 * Returns false otherwise.
	 */
	public boolean isAfterTerminationDate(Date date) {
		String name = getWorkflowState().getName();
		if (name.startsWith("Closed") || name.startsWith("Terminated") || name.contains("Termination")) {
			return date.after(getWorkflowStateEffectiveStartDate());
		}
		return false;
	}


	public SystemBean getAumCalculatorBean() {
		if (getAumCalculatorOverrideBean() != null) {
			return getAumCalculatorOverrideBean();
		}
		if (getBusinessService() != null) {
			return getBusinessService().getAumCalculatorBean();
		}
		return null;
	}


	public Date getStartDate() {
		return (getPerformanceInceptionDate() != null) ? getPerformanceInceptionDate() :
				(getInceptionDate() != null) ? getInceptionDate() : getWorkflowStateEffectiveStartDate();
	}


	// If the service on the account has a parent of Service Group type, will display the Service Group name, otherwise the service name
	public String getCoalesceBusinessServiceGroupName() {
		if (getBusinessService() != null) {
			return getBusinessService().getCoalesceBusinessServiceGroupName();
		}
		return null;
	}


	/**
	 * Returns account level override for the Accounting Closing Method.  If not defined, returns the default specified for the Business Service.
	 */
	public AccountingClosingMethods getAccountingClosingMethod() {
		if (getAccountingClosingMethodOverride() == null && getBusinessService() != null) {
			return getBusinessService().getAccountingClosingMethod();
		}
		return getAccountingClosingMethodOverride();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getIssuingCompany() {
		return this.issuingCompany;
	}


	public void setIssuingCompany(BusinessCompany issuingCompany) {
		this.issuingCompany = issuingCompany;
	}


	public String getNumber() {
		return this.number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public String getNumber2() {
		return this.number2;
	}


	public void setNumber2(String number2) {
		this.number2 = number2;
	}


	public InvestmentAccountType getType() {
		return this.type;
	}


	public void setType(InvestmentAccountType type) {
		this.type = type;
	}


	public BusinessClient getBusinessClient() {
		return this.businessClient;
	}


	public void setBusinessClient(BusinessClient businessClient) {
		this.businessClient = businessClient;
	}


	public BusinessService getBusinessService() {
		return this.businessService;
	}


	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}


	public AccountingClosingMethods getAccountingClosingMethodOverride() {
		return this.accountingClosingMethodOverride;
	}


	public void setAccountingClosingMethodOverride(AccountingClosingMethods accountingClosingMethodOverride) {
		this.accountingClosingMethodOverride = accountingClosingMethodOverride;
	}


	public InvestmentSecurity getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(InvestmentSecurity baseCurrency) {
		this.baseCurrency = baseCurrency;
	}


	public SecurityGroup getTeamSecurityGroup() {
		return this.teamSecurityGroup;
	}


	public void setTeamSecurityGroup(SecurityGroup teamSecurityGroup) {
		this.teamSecurityGroup = teamSecurityGroup;
	}


	public boolean isUsedForSpeculation() {
		return this.usedForSpeculation;
	}


	public void setUsedForSpeculation(boolean usedForSpeculation) {
		this.usedForSpeculation = usedForSpeculation;
	}


	public BigDecimal getInitialMarginMultiplier() {
		return this.initialMarginMultiplier;
	}


	public void setInitialMarginMultiplier(BigDecimal initialMarginMultiplier) {
		this.initialMarginMultiplier = initialMarginMultiplier;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	public BusinessContract getBusinessContract() {
		return this.businessContract;
	}


	public void setBusinessContract(BusinessContract businessContract) {
		this.businessContract = businessContract;
	}


	public List<InvestmentAccountBusinessService> getBusinessServiceList() {
		return this.businessServiceList;
	}


	public void setBusinessServiceList(List<InvestmentAccountBusinessService> businessServiceList) {
		this.businessServiceList = businessServiceList;
	}


	public BusinessServiceProcessingType getServiceProcessingType() {
		return this.serviceProcessingType;
	}


	public void setServiceProcessingType(BusinessServiceProcessingType serviceProcessingType) {
		this.serviceProcessingType = serviceProcessingType;
	}


	public Date getInceptionDate() {
		return this.inceptionDate;
	}


	public void setInceptionDate(Date inceptionDate) {
		this.inceptionDate = inceptionDate;
	}


	public Date getPerformanceInceptionDate() {
		return this.performanceInceptionDate;
	}


	public void setPerformanceInceptionDate(Date performanceInceptionDate) {
		this.performanceInceptionDate = performanceInceptionDate;
	}


	public String getDiscretionType() {
		return this.discretionType;
	}


	public void setDiscretionType(String discretionType) {
		this.discretionType = discretionType;
	}


	public String getClientAccountType() {
		return this.clientAccountType;
	}


	public void setClientAccountType(String clientAccountType) {
		this.clientAccountType = clientAccountType;
	}


	public SystemBean getAumCalculatorOverrideBean() {
		return this.aumCalculatorOverrideBean;
	}


	public void setAumCalculatorOverrideBean(SystemBean aumCalculatorOverrideBean) {
		this.aumCalculatorOverrideBean = aumCalculatorOverrideBean;
	}


	public String getShortName() {
		return this.shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public SystemListItem getExternalPortfolioCode() {
		return this.externalPortfolioCode;
	}


	public void setExternalPortfolioCode(SystemListItem externalPortfolioCode) {
		this.externalPortfolioCode = externalPortfolioCode;
	}


	public SystemListItem getExternalProductCode() {
		return this.externalProductCode;
	}


	public void setExternalProductCode(SystemListItem externalProductCode) {
		this.externalProductCode = externalProductCode;
	}


	public boolean isClientDirected() {
		return this.clientDirected;
	}


	public void setClientDirected(boolean clientDirected) {
		this.clientDirected = clientDirected;
	}


	public BusinessCompany getDefaultExchangeRateSourceCompany() {
		return this.defaultExchangeRateSourceCompany;
	}


	public void setDefaultExchangeRateSourceCompany(BusinessCompany defaultExchangeRateSourceCompany) {
		this.defaultExchangeRateSourceCompany = defaultExchangeRateSourceCompany;
	}


	public SystemListItem getClientAccountChannel() {
		return this.clientAccountChannel;
	}


	public void setClientAccountChannel(SystemListItem clientAccountChannel) {
		this.clientAccountChannel = clientAccountChannel;
	}


	public BusinessCompanyPlatform getCompanyPlatform() {
		return this.companyPlatform;
	}


	public void setCompanyPlatform(BusinessCompanyPlatform companyPlatform) {
		this.companyPlatform = companyPlatform;
	}


	public boolean isWrapAccount() {
		return this.wrapAccount;
	}


	public void setWrapAccount(boolean wrapAccount) {
		this.wrapAccount = wrapAccount;
	}


	public InvestmentAccount getProcessingTypePurposeAccount() {
		return this.processingTypePurposeAccount;
	}


	public void setProcessingTypePurposeAccount(InvestmentAccount processingTypePurposeAccount) {
		this.processingTypePurposeAccount = processingTypePurposeAccount;
	}
}
