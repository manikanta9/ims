package com.clifton.investment.account.targetexposure.calculators;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureService;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.search.InvestmentSecurityStructureAllocationSearchForm;

import java.math.BigDecimal;
import java.util.List;


/**
 * <code>InvestmentSecurityStructureWeightCalculator</code> is an {@link InvestmentAccountTargetExposureCalculator} that calculates a target percentage
 * by aggregating instrument or security weights of a structured index security.
 *
 * @author nickk
 */
public class InvestmentSecurityStructureWeightCalculator extends BaseInvestmentAccountTargetExposureCalculator implements ValidationAware {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentInstrumentStructureService investmentInstrumentStructureService;

	private Integer structuredIndexSecurityId;
	private Integer[] allocatedSecurityIds;
	private Integer[] allocatedInstrumentIds;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getStructuredIndexSecurityId(), "A Structured Index Security is required for this calculator.");
		ValidationUtils.assertTrue(!ArrayUtils.isEmpty(getAllocatedInstrumentIds()) || !ArrayUtils.isEmpty(getAllocatedSecurityIds()), "At least one allocation instrument or security is required.");
	}


	@Override
	public BigDecimal calculate(InvestmentAccountTargetExposureCalculatorConfig config) {
		List<InvestmentSecurityStructureAllocation> securityStructureAllocationList = getInvestmentSecurityStructureAllocationList(config);
		return CoreMathUtils.sumProperty(securityStructureAllocationList, InvestmentSecurityStructureAllocation::getAllocationWeight);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private List<InvestmentSecurityStructureAllocation> getInvestmentSecurityStructureAllocationList(InvestmentAccountTargetExposureCalculatorConfig config) {
		InvestmentSecurityStructureAllocationSearchForm structureAllocationSearchForm = new InvestmentSecurityStructureAllocationSearchForm();
		structureAllocationSearchForm.setSecurityId(getStructuredIndexSecurityId());
		structureAllocationSearchForm.setMeasureDate(config.getBalanceDate());
		if (getAllocatedInstrumentIds() != null) {
			if (getAllocatedInstrumentIds().length > 1) {
				structureAllocationSearchForm.setCurrentSecurityInstrumentIds(getAllocatedInstrumentIds());
			}
			else {
				structureAllocationSearchForm.setCurrentSecurityInstrumentId(getAllocatedInstrumentIds()[0]);
			}
		}
		if (getAllocatedSecurityIds() != null) {
			if (getAllocatedSecurityIds().length > 1) {
				structureAllocationSearchForm.setCurrentSecurityIds(getAllocatedSecurityIds());
			}
			else {
				structureAllocationSearchForm.setCurrentSecurityId(getAllocatedSecurityIds()[0]);
			}
		}
		return getInvestmentInstrumentStructureService().getInvestmentSecurityStructureAllocationList(structureAllocationSearchForm);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentInstrumentStructureService getInvestmentInstrumentStructureService() {
		return this.investmentInstrumentStructureService;
	}


	public void setInvestmentInstrumentStructureService(InvestmentInstrumentStructureService investmentInstrumentStructureService) {
		this.investmentInstrumentStructureService = investmentInstrumentStructureService;
	}


	public Integer getStructuredIndexSecurityId() {
		return this.structuredIndexSecurityId;
	}


	public void setStructuredIndexSecurityId(Integer structuredIndexSecurityId) {
		this.structuredIndexSecurityId = structuredIndexSecurityId;
	}


	public Integer[] getAllocatedSecurityIds() {
		return this.allocatedSecurityIds;
	}


	public void setAllocatedSecurityIds(Integer[] allocatedSecurityIds) {
		this.allocatedSecurityIds = allocatedSecurityIds;
	}


	public Integer[] getAllocatedInstrumentIds() {
		return this.allocatedInstrumentIds;
	}


	public void setAllocatedInstrumentIds(Integer[] allocatedInstrumentIds) {
		this.allocatedInstrumentIds = allocatedInstrumentIds;
	}
}
