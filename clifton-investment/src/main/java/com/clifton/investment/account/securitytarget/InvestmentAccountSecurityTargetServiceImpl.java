package com.clifton.investment.account.securitytarget;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyFlexibleCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetFreezeSearchForm;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetFreezeTypeSearchForm;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author NickK
 */
@Service
public class InvestmentAccountSecurityTargetServiceImpl implements InvestmentAccountSecurityTargetService {

	private AdvancedUpdatableDAO<InvestmentAccountSecurityTarget, Criteria> investmentAccountSecurityTargetDAO;
	private AdvancedUpdatableDAO<InvestmentAccountSecurityTargetFreezeType, Criteria> investmentAccountSecurityTargetFreezeTypeDAO;
	private AdvancedUpdatableDAO<InvestmentAccountSecurityTargetFreeze, Criteria> investmentAccountSecurityTargetFreezeDAO;

	private DaoNamedEntityCache<InvestmentAccountSecurityTargetFreezeType> investmentAccountSecurityTargetFreezeTypeCache;
	private DaoCompositeKeyFlexibleCache<InvestmentAccountSecurityTarget, Date, Integer, Integer> investmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache;


	////////////////////////////////////////////////////////////////////////////
	/////////    Investment Account Security Target Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountSecurityTarget getInvestmentAccountSecurityTarget(int id) {
		return getInvestmentAccountSecurityTargetDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentAccountSecurityTarget getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(int accountId, int underlyingSecurityId, Date date) {
		return getInvestmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache().getBeanForKeyValues(getInvestmentAccountSecurityTargetDAO(), date, accountId, underlyingSecurityId);
	}


	@Override
	public List<InvestmentAccountSecurityTarget> getInvestmentAccountSecurityTargetList(InvestmentAccountSecurityTargetSearchForm searchForm) {
		String targetTypeFilter = searchForm.getTargetType();
		if (StringUtils.isEmpty(targetTypeFilter) && searchForm.containsSearchRestriction("targetType")) {
			targetTypeFilter = (String) searchForm.getSearchRestriction("targetType").getValue();
		}
		if (!StringUtils.isEmpty(targetTypeFilter)) {
			switch (targetTypeFilter) {
				case InvestmentAccountSecurityTarget.TARGET_TYPE_MANAGER:
					searchForm.setTargetUnderlyingManagerAccountPopulated(true);
					break;
				case InvestmentAccountSecurityTarget.TARGET_TYPE_NOTIONAL:
					searchForm.setTargetUnderlyingNotionalPopulated(true);
					break;
				case InvestmentAccountSecurityTarget.TARGET_TYPE_QUANTITY:
					searchForm.setTargetUnderlyingQuantityPopulated(true);
					break;
				default:
					throw new ValidationException("Unknown security target type [" + targetTypeFilter + "].  Cannot apply filter");
			}
		}
		return getInvestmentAccountSecurityTargetDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTarget(InvestmentAccountSecurityTarget securityTarget) {
		if (securityTarget.getTargetType().equals(InvestmentAccountSecurityTarget.TARGET_TYPE_QUANTITY) && MathUtils.isEqual(securityTarget.getTargetUnderlyingQuantity(), BigDecimal.ZERO) ||
				securityTarget.getTargetType().equals(InvestmentAccountSecurityTarget.TARGET_TYPE_NOTIONAL) && MathUtils.isEqual(securityTarget.getTargetUnderlyingNotional(), BigDecimal.ZERO)) {
			throw new UserIgnorableValidationException(InvestmentAccountSecurityTargetService.class, "saveInvestmentAccountSecurityTargetIgnoringValidation", "The security target's quantity value should normally be greater than 0.00.");
		}

		// Validate that underlying security was not changed
		if (!securityTarget.isNewBean()) {
			InvestmentAccountSecurityTarget originalBean = getInvestmentAccountSecurityTarget(securityTarget.getId());
			ValidationUtils.assertEquals(originalBean.getTargetUnderlyingSecurity(), securityTarget.getTargetUnderlyingSecurity(), "Underlying security cannot be changed on a Security Target. As an alternative, you can end the existing target and create a new target with the new underlying security.");
		}

		return getInvestmentAccountSecurityTargetDAO().save(securityTarget);
	}


	@Override
	public InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTargetIgnoringValidation(InvestmentAccountSecurityTarget securityTarget) {
		return getInvestmentAccountSecurityTargetDAO().save(securityTarget);
	}


	@Override
	@Transactional
	public InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTargetFromExisting(Integer existingId, InvestmentAccountSecurityTarget securityTarget) {
		if (securityTarget.getTargetType().equals(InvestmentAccountSecurityTarget.TARGET_TYPE_QUANTITY) && MathUtils.isEqual(securityTarget.getTargetUnderlyingQuantity(), BigDecimal.ZERO) ||
				securityTarget.getTargetType().equals(InvestmentAccountSecurityTarget.TARGET_TYPE_NOTIONAL) && MathUtils.isEqual(securityTarget.getTargetUnderlyingNotional(), BigDecimal.ZERO)) {
			throw new UserIgnorableValidationException(InvestmentAccountSecurityTargetService.class, "saveInvestmentAccountSecurityTargetFromExistingIgnoringValidation", "The security target's quantity value should normally be greater than 0.00.");
		}

		return saveInvestmentAccountSecurityTargetFromExistingTarget(existingId, securityTarget);
	}


	@Override
	@Transactional
	public InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTargetFromExistingIgnoringValidation(Integer existingId, InvestmentAccountSecurityTarget securityTarget) {
		return saveInvestmentAccountSecurityTargetFromExistingTarget(existingId, securityTarget);
	}


	/**
	 * Saves the provided new {@link InvestmentAccountSecurityTarget}.  The validateNotionalOrQuantity flag, when set to True, will validate the security targets TargetUnderlyingQuantity or TargetUnderlyingNotional to ensure these values are not 0.00.
	 * <p>
	 * <br/>- If an existing ID is provided and the existing record has a null end date, the existing target will be ended using the start date - 1 of the new target.
	 * <br/>- If an existing ID is not provided and the provided security target has an ID, the original will be looked up and ended using today - 1 day and the new target will start today.
	 * <br/>- If an existing ID is not provided and the provided security target is new, an existing active target is looked up using the new target's client account and underlying security.
	 * An existing active target will be ended using start date - 1 of the new target.
	 * <p>
	 * Any active {@link InvestmentAccountSecurityTargetFreeze} entities will be ended for the existing target and new frozen allocations will be created for the new target using the same start date.
	 */
	private InvestmentAccountSecurityTarget saveInvestmentAccountSecurityTargetFromExistingTarget(Integer existingId, InvestmentAccountSecurityTarget securityTarget) {
		ValidationUtils.assertTrue(securityTarget.getClientInvestmentAccount() != null && securityTarget.getClientInvestmentAccount().getId() != null, "Client Account for the new Security Target is required");
		ValidationUtils.assertTrue(securityTarget.getTargetUnderlyingSecurity() != null && securityTarget.getTargetUnderlyingSecurity().getId() != null, "Underlying Security for the new Security Target is required");

		InvestmentAccountSecurityTarget existing;
		if (securityTarget.isNewBean()) {
			if (existingId == null) {
				// look up existing; only one active can exist for client and underlying security
				InvestmentAccountSecurityTargetSearchForm securityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();
				securityTargetSearchForm.setActive(Boolean.TRUE);
				securityTargetSearchForm.setClientInvestmentAccountId(securityTarget.getClientInvestmentAccount().getId());
				securityTargetSearchForm.setTargetUnderlyingSecurityId(securityTarget.getTargetUnderlyingSecurity().getId());
				List<InvestmentAccountSecurityTarget> existingSecurityTargetList = getInvestmentAccountSecurityTargetList(securityTargetSearchForm);
				existing = CollectionUtils.getFirstElement(existingSecurityTargetList);
			}
			else {
				// look up existing
				existing = getInvestmentAccountSecurityTarget(existingId);
			}
		}
		else {
			if (existingId == null) {
				existingId = securityTarget.getId();
			}
			else {
				ValidationUtils.assertEquals(existingId, securityTarget.getId(), "The Security Target to save already exists and its ID does not match the existing ID to end.");
			}
			// look up existing
			existing = getInvestmentAccountSecurityTarget(existingId);
			// clear the ID and update the start date to present time; existing target will be ended and new one will be created
			securityTarget.setId(null);
			securityTarget.setStartDate(DateUtils.clearTime(new Date()));
		}

		// validate and update existing before adding new target
		if (existing != null) {
			ValidationUtils.assertEquals(existing.getClientInvestmentAccount(), securityTarget.getClientInvestmentAccount(), "Saving an Investment Account Security Target from an existing requires the two targets to use the same Client Account");
			ValidationUtils.assertEquals(existing.getTargetUnderlyingSecurity(), securityTarget.getTargetUnderlyingSecurity(), "Saving an Investment Account Security Target from an existing requires the two targets to use the same Underlying Security");

			if (existing.getEndDate() == null) {
				existing.setEndDate(DateUtils.addDays(securityTarget.getStartDate(), -1));
				getInvestmentAccountSecurityTargetDAO().save(existing);
			}
		}

		// add new target
		return getInvestmentAccountSecurityTargetDAO().save(securityTarget);
	}


	@Override
	public void endInvestmentAccountSecurityTarget(int id) {
		InvestmentAccountSecurityTarget securityTarget = getInvestmentAccountSecurityTarget(id);
		Date yesterday = DateUtils.addDays(new Date(), -1);
		if (securityTarget != null && DateUtils.isDateAfter(securityTarget.getEndDate(), yesterday)) {
			securityTarget.setEndDate(yesterday);
			saveInvestmentAccountSecurityTarget(securityTarget);
		}
	}


	@Override
	public void deleteInvestmentAccountSecurityTarget(int id) {
		deleteInvestmentAccountSecurityTargetImpl(id, false);
	}


	@Override
	public void deleteInvestmentAccountSecurityTargetHistorical(int id) {
		deleteInvestmentAccountSecurityTargetImpl(id, true);
	}


	@Transactional
	protected void deleteInvestmentAccountSecurityTargetImpl(int id, boolean allowHistorical) {
		InvestmentAccountSecurityTarget securityTarget = getInvestmentAccountSecurityTarget(id);
		ValidationUtils.assertNotNull(securityTarget, "Unable to find Security Target with id=" + id);

		Date now = DateUtils.clearTime(new Date());
		if (DateUtils.compare(securityTarget.getStartDate(), now, false) < 0) {
			if (!allowHistorical) {
				throw new UserIgnorableValidationException(getClass(), "deleteInvestmentAccountSecurityTargetHistorical", "Cannot delete a Security Target that has been active for more than one day");
			}
		}

		getInvestmentAccountSecurityTargetDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///    Investment Account Security Target Freeze Type Business Methods   ///
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountSecurityTargetFreezeType getInvestmentAccountSecurityTargetFreezeType(short id) {
		return getInvestmentAccountSecurityTargetFreezeTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentAccountSecurityTargetFreezeType getInvestmentAccountSecurityTargetFreezeTypeByName(String name) {
		return getInvestmentAccountSecurityTargetFreezeTypeCache().getBeanForKeyValue(getInvestmentAccountSecurityTargetFreezeTypeDAO(), name);
	}


	@Override
	public List<InvestmentAccountSecurityTargetFreezeType> getInvestmentAccountSecurityTargetFreezeTypeList(InvestmentAccountSecurityTargetFreezeTypeSearchForm searchForm) {
		return getInvestmentAccountSecurityTargetFreezeTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountSecurityTargetFreezeType saveInvestmentAccountSecurityTargetFreezeType(InvestmentAccountSecurityTargetFreezeType securityTargetFreezeType) {
		return getInvestmentAccountSecurityTargetFreezeTypeDAO().save(securityTargetFreezeType);
	}


	@Override
	public void deleteInvestmentAccountSecurityTargetFreezeType(short id) {
		getInvestmentAccountSecurityTargetFreezeTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	//////    Investment Account Security Target Freeze Business Methods   /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountSecurityTargetFreeze getInvestmentAccountSecurityTargetFreeze(int id) {
		return getInvestmentAccountSecurityTargetFreezeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountSecurityTargetFreeze> getInvestmentAccountSecurityTargetFreezeList(InvestmentAccountSecurityTargetFreezeSearchForm searchForm) {
		return getInvestmentAccountSecurityTargetFreezeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountSecurityTargetFreeze saveInvestmentAccountSecurityTargetFreeze(InvestmentAccountSecurityTargetFreeze securityTargetFreeze) {
		// normalize the persisted quantity to be the underlying amount instead of the cause security's
		if (securityTargetFreeze.getFreezeUnderlyingQuantity() == null && securityTargetFreeze.getFreezeCauseSecurityQuantity() != null) {
			ValidationUtils.assertNotNull(securityTargetFreeze.getCauseInvestmentSecurity(), "The cause security for the quantity to freeze is required.");
			securityTargetFreeze.setFreezeUnderlyingQuantity(MathUtils.multiply(securityTargetFreeze.getFreezeCauseSecurityQuantity(), securityTargetFreeze.getCauseInvestmentSecurity().getPriceMultiplier()));
		}

		return getInvestmentAccountSecurityTargetFreezeDAO().save(securityTargetFreeze);
	}


	@Override
	public void endInvestmentAccountSecurityTargetFreeze(int id) {
		InvestmentAccountSecurityTargetFreeze freeze = getInvestmentAccountSecurityTargetFreeze(id);
		Date yesterday = DateUtils.addDays(new Date(), -1);
		if (freeze != null && DateUtils.isDateAfter(freeze.getEndDate(), yesterday)) {
			freeze.setEndDate(yesterday);
			saveInvestmentAccountSecurityTargetFreeze(freeze);
		}
	}


	@Override
	public void deleteInvestmentAccountSecurityTargetFreeze(int id) {
		getInvestmentAccountSecurityTargetFreezeDAO().delete(id);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentAccountSecurityTarget, Criteria> getInvestmentAccountSecurityTargetDAO() {
		return this.investmentAccountSecurityTargetDAO;
	}


	public void setInvestmentAccountSecurityTargetDAO(AdvancedUpdatableDAO<InvestmentAccountSecurityTarget, Criteria> investmentAccountSecurityTargetDAO) {
		this.investmentAccountSecurityTargetDAO = investmentAccountSecurityTargetDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountSecurityTargetFreezeType, Criteria> getInvestmentAccountSecurityTargetFreezeTypeDAO() {
		return this.investmentAccountSecurityTargetFreezeTypeDAO;
	}


	public void setInvestmentAccountSecurityTargetFreezeTypeDAO(AdvancedUpdatableDAO<InvestmentAccountSecurityTargetFreezeType, Criteria> investmentAccountSecurityTargetFreezeTypeDAO) {
		this.investmentAccountSecurityTargetFreezeTypeDAO = investmentAccountSecurityTargetFreezeTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountSecurityTargetFreeze, Criteria> getInvestmentAccountSecurityTargetFreezeDAO() {
		return this.investmentAccountSecurityTargetFreezeDAO;
	}


	public void setInvestmentAccountSecurityTargetFreezeDAO(AdvancedUpdatableDAO<InvestmentAccountSecurityTargetFreeze, Criteria> investmentAccountSecurityTargetFreezeDAO) {
		this.investmentAccountSecurityTargetFreezeDAO = investmentAccountSecurityTargetFreezeDAO;
	}


	public DaoNamedEntityCache<InvestmentAccountSecurityTargetFreezeType> getInvestmentAccountSecurityTargetFreezeTypeCache() {
		return this.investmentAccountSecurityTargetFreezeTypeCache;
	}


	public void setInvestmentAccountSecurityTargetFreezeTypeCache(DaoNamedEntityCache<InvestmentAccountSecurityTargetFreezeType> investmentAccountSecurityTargetFreezeTypeCache) {
		this.investmentAccountSecurityTargetFreezeTypeCache = investmentAccountSecurityTargetFreezeTypeCache;
	}


	public DaoCompositeKeyFlexibleCache<InvestmentAccountSecurityTarget, Date, Integer, Integer> getInvestmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache() {
		return this.investmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache;
	}


	public void setInvestmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache(DaoCompositeKeyFlexibleCache<InvestmentAccountSecurityTarget, Date, Integer, Integer> investmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache) {
		this.investmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache = investmentAccountSecurityTargetByAccountAndUnderlyingSecurityIdCache;
	}
}
