package com.clifton.investment.account.calculation.snapshot.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public class InvestmentAccountCalculationSnapshotDetailSearchForm extends BaseEntitySearchForm {


	// Used for sorting only to sort parents and children together
	@SearchField(searchField = "calculationSnapshot.id,parent.id,id", searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private Integer sortIdOrder;

	@SearchField(searchField = "calculationSnapshot.id")
	private Integer calculationSnapshotId;

	@SearchField(searchFieldPath = "calculationSnapshot", searchField = "calculationType.id")
	private Short calculationTypeId;

	@SearchField(searchFieldPath = "calculationSnapshot", searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "calculationSnapshot.investmentAccount.aumCalculatorOverrideBean.id,calculationSnapshot.investmentAccount.businessService.aumCalculatorBean.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private Integer investmentAccountAumCalculatorBeanId;

	@SearchField(searchFieldPath = "calculationSnapshot", searchField = "snapshotDate")
	private Date snapshotDate;

	@SearchField(searchFieldPath = "calculationSnapshot", searchField = "calculatedValueCurrency.id")
	private Integer calculatedValueCurrencyId;

	@SearchField(searchFieldPath = "calculationSnapshot", searchField = "fxRateToBase")
	private BigDecimal fxRateToBase;

	@SearchField(searchFieldPath = "calculationSnapshot.calculationType", searchField = "baseCurrency.id")
	private Integer baseCurrencyId;

	@SearchField(searchField = "investmentAssetClass.id")
	private Short investmentAssetClassId;

	@SearchField(searchFieldPath = "investmentSecurity.instrument.hierarchy", searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchFieldPath = "investmentSecurity.instrument.hierarchy", searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;

	@SearchField(searchFieldPath = "investmentSecurity.instrument.hierarchy", searchField = "investmentTypeSubType2.id")
	private Short investmentTypeSubType2Id;

	@SearchField(searchFieldPath = "investmentSecurity.instrument", searchField = "hierarchy.id")
	private Short investmentHierarchyId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentInstrumentId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal preliminaryCalculatedValue;

	@SearchField
	private BigDecimal calculatedValue;

	@SearchField(searchField = "calculatedValue,calculationSnapshot.fxRateToBase", searchFieldCustomType = SearchFieldCustomTypes.MATH_MULTIPLY)
	private BigDecimal calculatedValueBase;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "calculationSnapshot.investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.id", searchFieldPath = "investmentSecurity", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentSecurityGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;


	@SearchField(searchField = "name", searchFieldPath = "calculationSnapshot.investmentAccount.businessClient.clientRelationship")
	private String clientRelationshipName;

	@SearchField(searchField = "name", searchFieldPath = "calculationSnapshot.investmentAccount.businessClient")
	private String clientName;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "calculationSnapshot.investmentAccount.businessService.id,calculationSnapshot.investmentAccount.businessService.parent.id,calculationSnapshot.investmentAccount.businessService.parent.parent.id,calculationSnapshot.investmentAccount.businessService.parent.parent.parent.id", leftJoin = true, sortField = "calculationSnapshot.investmentAccount.businessService.name")
	private Short businessServiceOrParentId;

	@SearchField(searchField = "teamSecurityGroup.id", searchFieldPath = "calculationSnapshot.investmentAccount")
	private Short teamSecurityGroupId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getSortIdOrder() {
		return this.sortIdOrder;
	}


	public void setSortIdOrder(Integer sortIdOrder) {
		this.sortIdOrder = sortIdOrder;
	}


	public Integer getCalculationSnapshotId() {
		return this.calculationSnapshotId;
	}


	public void setCalculationSnapshotId(Integer calculationSnapshotId) {
		this.calculationSnapshotId = calculationSnapshotId;
	}


	public Short getCalculationTypeId() {
		return this.calculationTypeId;
	}


	public void setCalculationTypeId(Short calculationTypeId) {
		this.calculationTypeId = calculationTypeId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Integer getInvestmentAccountAumCalculatorBeanId() {
		return this.investmentAccountAumCalculatorBeanId;
	}


	public void setInvestmentAccountAumCalculatorBeanId(Integer investmentAccountAumCalculatorBeanId) {
		this.investmentAccountAumCalculatorBeanId = investmentAccountAumCalculatorBeanId;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public Integer getCalculatedValueCurrencyId() {
		return this.calculatedValueCurrencyId;
	}


	public void setCalculatedValueCurrencyId(Integer calculatedValueCurrencyId) {
		this.calculatedValueCurrencyId = calculatedValueCurrencyId;
	}


	public BigDecimal getFxRateToBase() {
		return this.fxRateToBase;
	}


	public void setFxRateToBase(BigDecimal fxRateToBase) {
		this.fxRateToBase = fxRateToBase;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public Short getInvestmentAssetClassId() {
		return this.investmentAssetClassId;
	}


	public void setInvestmentAssetClassId(Short investmentAssetClassId) {
		this.investmentAssetClassId = investmentAssetClassId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPreliminaryCalculatedValue() {
		return this.preliminaryCalculatedValue;
	}


	public void setPreliminaryCalculatedValue(BigDecimal preliminaryCalculatedValue) {
		this.preliminaryCalculatedValue = preliminaryCalculatedValue;
	}


	public BigDecimal getCalculatedValue() {
		return this.calculatedValue;
	}


	public void setCalculatedValue(BigDecimal calculatedValue) {
		this.calculatedValue = calculatedValue;
	}


	public BigDecimal getCalculatedValueBase() {
		return this.calculatedValueBase;
	}


	public void setCalculatedValueBase(BigDecimal calculatedValueBase) {
		this.calculatedValueBase = calculatedValueBase;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Short getInvestmentSecurityGroupId() {
		return this.investmentSecurityGroupId;
	}


	public void setInvestmentSecurityGroupId(Short investmentSecurityGroupId) {
		this.investmentSecurityGroupId = investmentSecurityGroupId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}
}
