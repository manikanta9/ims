package com.clifton.investment.account.relationship.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.util.Date;


/**
 * The <code>InvestmentAccountRelationshipSearchForm</code> class defines search configuration for InvestmentAccountRelationship objects.
 *
 * @author vgomelsky
 */
public class InvestmentAccountRelationshipSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer mainAccountId;

	@SearchField(searchField = "referenceOne.id")
	private Integer[] mainAccountIds;

	@SearchField(searchField = "number", searchFieldPath = "referenceOne")
	private String mainAccount;

	@SearchField(searchField = "type.id", searchFieldPath = "referenceOne")
	private Short mainAccountTypeId;

	@SearchField(searchField = "type.id", searchFieldPath = "referenceOne")
	private Short mainAccountTypeIds;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "referenceOne")
	private Integer mainAccountIssuingCompanyId;

	@SearchField(searchField = "businessClient.id", searchFieldPath = "referenceOne")
	private Integer mainAccountClientId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer relatedAccountId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer[] relatedAccountIds;

	@SearchField(searchField = "number", searchFieldPath = "referenceTwo")
	private String relatedAccount;

	@SearchField(searchField = "type.id", searchFieldPath = "referenceTwo")
	private Short relatedAccountTypeId;

	@SearchField(searchField = "type.id", searchFieldPath = "referenceTwo")
	private Short[] relatedAccountTypeIds;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "referenceTwo")
	private Integer relatedAccountIssuingCompanyId;

	@SearchField(searchField = "businessClient.id", searchFieldPath = "referenceTwo")
	private Integer relatedAccountClientId;

	@SearchField(searchField = "accountAssetClass.id")
	private Integer investmentAccountAssetClassId;

	@SearchField(searchFieldPath = "accountAssetClass", searchField = "displayName,assetClass.name")
	private String investmentAccountAssetClass;

	@SearchField(searchField = "purpose.id")
	private Short purposeId;

	@SearchField(searchFieldPath = "purpose", searchField = "name")
	private String purposeName;

	@SearchField(searchFieldPath = "purpose", searchField = "name")
	private String[] purposeNames;

	@SearchField(searchFieldPath = "purpose", searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchFieldPath = "referenceOne.workflowState", searchField = "name")
	private String mainAccountWorkflowStateName;

	@SearchField(searchFieldPath = "referenceOne", searchField = "workflowStateEffectiveStartDate")
	private Date mainAccountWorkflowStateEffectiveStart;

	@SearchField(searchFieldPath = "referenceTwo.workflowState", searchField = "name")
	private String relatedAccountWorkflowStateName;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "workflowStateEffectiveStartDate")
	private Date relatedAccountWorkflowStateEffectiveStart;

	@SearchField(searchFieldPath = "securityGroup", searchField = "name")
	private String securityGroupName;

	@SearchField(searchField = "referenceOne.id,referenceTwo.id", searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Integer mainOrRelatedAccountId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getMainAccountId() {
		return this.mainAccountId;
	}


	public void setMainAccountId(Integer mainAccountId) {
		this.mainAccountId = mainAccountId;
	}


	public Integer[] getMainAccountIds() {
		return this.mainAccountIds;
	}


	public void setMainAccountIds(Integer[] mainAccountIds) {
		this.mainAccountIds = mainAccountIds;
	}


	public String getMainAccount() {
		return this.mainAccount;
	}


	public void setMainAccount(String mainAccount) {
		this.mainAccount = mainAccount;
	}


	public Short getMainAccountTypeId() {
		return this.mainAccountTypeId;
	}


	public void setMainAccountTypeId(Short mainAccountTypeId) {
		this.mainAccountTypeId = mainAccountTypeId;
	}


	public Short getMainAccountTypeIds() {
		return this.mainAccountTypeIds;
	}


	public void setMainAccountTypeIds(Short mainAccountTypeIds) {
		this.mainAccountTypeIds = mainAccountTypeIds;
	}


	public Integer getMainAccountIssuingCompanyId() {
		return this.mainAccountIssuingCompanyId;
	}


	public void setMainAccountIssuingCompanyId(Integer mainAccountIssuingCompanyId) {
		this.mainAccountIssuingCompanyId = mainAccountIssuingCompanyId;
	}


	public Integer getMainAccountClientId() {
		return this.mainAccountClientId;
	}


	public void setMainAccountClientId(Integer mainAccountClientId) {
		this.mainAccountClientId = mainAccountClientId;
	}


	public Integer getRelatedAccountId() {
		return this.relatedAccountId;
	}


	public void setRelatedAccountId(Integer relatedAccountId) {
		this.relatedAccountId = relatedAccountId;
	}


	public Integer[] getRelatedAccountIds() {
		return this.relatedAccountIds;
	}


	public void setRelatedAccountIds(Integer[] relatedAccountIds) {
		this.relatedAccountIds = relatedAccountIds;
	}


	public String getRelatedAccount() {
		return this.relatedAccount;
	}


	public void setRelatedAccount(String relatedAccount) {
		this.relatedAccount = relatedAccount;
	}


	public Short getRelatedAccountTypeId() {
		return this.relatedAccountTypeId;
	}


	public void setRelatedAccountTypeId(Short relatedAccountTypeId) {
		this.relatedAccountTypeId = relatedAccountTypeId;
	}


	public Short[] getRelatedAccountTypeIds() {
		return this.relatedAccountTypeIds;
	}


	public void setRelatedAccountTypeIds(Short[] relatedAccountTypeIds) {
		this.relatedAccountTypeIds = relatedAccountTypeIds;
	}


	public Integer getRelatedAccountIssuingCompanyId() {
		return this.relatedAccountIssuingCompanyId;
	}


	public void setRelatedAccountIssuingCompanyId(Integer relatedAccountIssuingCompanyId) {
		this.relatedAccountIssuingCompanyId = relatedAccountIssuingCompanyId;
	}


	public Integer getRelatedAccountClientId() {
		return this.relatedAccountClientId;
	}


	public void setRelatedAccountClientId(Integer relatedAccountClientId) {
		this.relatedAccountClientId = relatedAccountClientId;
	}


	public Integer getInvestmentAccountAssetClassId() {
		return this.investmentAccountAssetClassId;
	}


	public void setInvestmentAccountAssetClassId(Integer investmentAccountAssetClassId) {
		this.investmentAccountAssetClassId = investmentAccountAssetClassId;
	}


	public String getInvestmentAccountAssetClass() {
		return this.investmentAccountAssetClass;
	}


	public void setInvestmentAccountAssetClass(String investmentAccountAssetClass) {
		this.investmentAccountAssetClass = investmentAccountAssetClass;
	}


	public Short getPurposeId() {
		return this.purposeId;
	}


	public void setPurposeId(Short purposeId) {
		this.purposeId = purposeId;
	}


	public String getPurposeName() {
		return this.purposeName;
	}


	public void setPurposeName(String purposeName) {
		this.purposeName = purposeName;
	}


	public String[] getPurposeNames() {
		return this.purposeNames;
	}


	public void setPurposeNames(String[] purposeNames) {
		this.purposeNames = purposeNames;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public String getMainAccountWorkflowStateName() {
		return this.mainAccountWorkflowStateName;
	}


	public void setMainAccountWorkflowStateName(String mainAccountWorkflowStateName) {
		this.mainAccountWorkflowStateName = mainAccountWorkflowStateName;
	}


	public Date getMainAccountWorkflowStateEffectiveStart() {
		return this.mainAccountWorkflowStateEffectiveStart;
	}


	public void setMainAccountWorkflowStateEffectiveStart(Date mainAccountWorkflowStateEffectiveStart) {
		this.mainAccountWorkflowStateEffectiveStart = mainAccountWorkflowStateEffectiveStart;
	}


	public String getRelatedAccountWorkflowStateName() {
		return this.relatedAccountWorkflowStateName;
	}


	public void setRelatedAccountWorkflowStateName(String relatedAccountWorkflowStateName) {
		this.relatedAccountWorkflowStateName = relatedAccountWorkflowStateName;
	}


	public Date getRelatedAccountWorkflowStateEffectiveStart() {
		return this.relatedAccountWorkflowStateEffectiveStart;
	}


	public void setRelatedAccountWorkflowStateEffectiveStart(Date relatedAccountWorkflowStateEffectiveStart) {
		this.relatedAccountWorkflowStateEffectiveStart = relatedAccountWorkflowStateEffectiveStart;
	}


	public String getSecurityGroupName() {
		return this.securityGroupName;
	}


	public void setSecurityGroupName(String securityGroupName) {
		this.securityGroupName = securityGroupName;
	}


	public Integer getMainOrRelatedAccountId() {
		return this.mainOrRelatedAccountId;
	}


	public void setMainOrRelatedAccountId(Integer mainOrRelatedAccountId) {
		this.mainOrRelatedAccountId = mainOrRelatedAccountId;
	}
}
