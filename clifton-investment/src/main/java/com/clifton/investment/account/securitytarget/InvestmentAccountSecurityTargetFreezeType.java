package com.clifton.investment.account.securitytarget;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * <code>InvestmentAccountSecurityTargetFreezeType</code> defines a type of {@link InvestmentAccountSecurityTargetFreeze}
 * with various configuration properties.
 *
 * @author NickK
 */
@CacheByName
public class InvestmentAccountSecurityTargetFreezeType extends NamedEntity<Short> {

	public static final String SECURITY_TARGET_TYPE_EARLY_CLOSE = "Early Close";

	private boolean systemDefined;
	private boolean noteRequired;
	private boolean causeRequired;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isNoteRequired() {
		return this.noteRequired;
	}


	public void setNoteRequired(boolean noteRequired) {
		this.noteRequired = noteRequired;
	}


	public boolean isCauseRequired() {
		return this.causeRequired;
	}


	public void setCauseRequired(boolean causeRequired) {
		this.causeRequired = causeRequired;
	}
}
