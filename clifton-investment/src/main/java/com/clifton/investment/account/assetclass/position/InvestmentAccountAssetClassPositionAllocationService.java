package com.clifton.investment.account.assetclass.position;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountAssetClassPositionAllocationService</code> ...
 *
 * @author manderson
 */
public interface InvestmentAccountAssetClassPositionAllocationService {

	////////////////////////////////////////////////////////////////////////////
	////   Investment Account Asset Class Position Allocation Methods       ////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassPositionAllocation getInvestmentAccountAssetClassPositionAllocation(int id);


	public List<InvestmentAccountAssetClassPositionAllocation> getInvestmentAccountAssetClassPositionAllocationList(int investmentAccountId, Date activeOnDate);


	public List<InvestmentAccountAssetClassPositionAllocation> getInvestmentAccountAssetClassPositionAllocationList(final InvestmentAccountAssetClassPositionAllocationSearchForm searchForm);


	public void copyInvestmentAccountAssetClassPositionAllocation(int id, Date newStartDate, Date newEndDate, BigDecimal newQuantity);


	public InvestmentAccountAssetClassPositionAllocation saveInvestmentAccountAssetClassPositionAllocation(InvestmentAccountAssetClassPositionAllocation bean);


	public void saveInvestmentAccountAssetClassPositionAllocationList(List<InvestmentAccountAssetClassPositionAllocation> beanList);


	public void deleteInvestmentAccountAssetClassPositionAllocation(int id);
}
