package com.clifton.investment.account.securitytarget;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccount;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>InvestmentAccountSecurityTarget</code> class defining a fixed quantity or notional target for an
 * underlying {@link InvestmentSecurity} and client {@link InvestmentAccount} for holding positions in a portfolio.
 *
 * @author NickK
 */
public class InvestmentAccountSecurityTarget extends BaseEntity<Integer> implements LabeledObject {

	public static final String TARGET_TYPE_QUANTITY = "Quantity";
	public static final String TARGET_TYPE_NOTIONAL = "Notional";
	public static final String TARGET_TYPE_MANAGER = "Manager";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private InvestmentAccount clientInvestmentAccount;

	private InvestmentSecurity targetUnderlyingSecurity;
	/**
	 * The underlying security target can be defined for a quantity or notional.
	 * Applicable held positions will need to be converted to the underlying amount using the security multiplier when calculating utilization of the target.
	 */
	private BigDecimal targetUnderlyingQuantity;
	private BigDecimal targetUnderlyingNotional;
	/**
	 * An optional manager account can be specified for linking the target quantity/notional to an manager account's balance.
	 */
	private InvestmentManagerAccount targetUnderlyingManagerAccount;
	/**
	 * An optional multiplier to use in conjunction of the defined quantity or notional target to adjust the
	 * actual target for the client account. The default is 1, which means 100% of the defined target is to be used.
	 */
	private BigDecimal targetUnderlyingMultiplier = BigDecimal.ONE;

	/**
	 * An optional number of slices the holdings towards this target should be split into.
	 * An example usage is for OARS where tranches are used to slice options holdings by expiration date.
	 * <p>
	 * Tranche count is set from the client/UI using a SystemList and SystemListItems.
	 */
	private Short trancheCount;

	/**
	 * The duration (in weeks) of a service's trading cycle.  An example would be the OARS service, that
	 * has 12 tranches with 3 tranches traded a week and has a cycle duration of 4 weeks.
	 * <p>
	 * Cycle duration weeks is set from the client/UI using a SystemList and SystemListItems.
	 */
	private Short cycleDurationWeeks;

	private String securityTargetNote;

	private Date startDate;
	private Date endDate;

	/**
	 * Trade dates to track the last time the client traded in each direction for this target. Applicable trades
	 * to track include trades for a security with the same underlying as this target.
	 * <p>
	 * These dates are used to easily see how active or stale a client's portfolio is.
	 */
	private Date lastBuyTradeDate;
	private Date lastSellTradeDate;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getClientInvestmentAccount() != null) {
			return getClientInvestmentAccount().getLabel() + ": " + getLabelShort();
		}
		return getLabelShort();
	}


	public String getLabelShort() {
		if (getTargetUnderlyingSecurity() != null) {
			return getTargetUnderlyingSecurity().getSymbol() + " " + DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true);
		}
		return DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true);
	}


	public String getTargetType() {
		if (getTargetUnderlyingManagerAccount() != null) {
			return TARGET_TYPE_MANAGER;
		}
		if (getTargetUnderlyingNotional() != null) {
			return TARGET_TYPE_NOTIONAL;
		}
		return TARGET_TYPE_QUANTITY;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentSecurity getTargetUnderlyingSecurity() {
		return this.targetUnderlyingSecurity;
	}


	public void setTargetUnderlyingSecurity(InvestmentSecurity targetUnderlyingSecurity) {
		this.targetUnderlyingSecurity = targetUnderlyingSecurity;
	}


	public BigDecimal getTargetUnderlyingQuantity() {
		return this.targetUnderlyingQuantity;
	}


	public void setTargetUnderlyingQuantity(BigDecimal targetUnderlyingQuantity) {
		this.targetUnderlyingQuantity = targetUnderlyingQuantity;
	}


	public BigDecimal getTargetUnderlyingNotional() {
		return this.targetUnderlyingNotional;
	}


	public void setTargetUnderlyingNotional(BigDecimal targetUnderlyingNotional) {
		this.targetUnderlyingNotional = targetUnderlyingNotional;
	}


	public InvestmentManagerAccount getTargetUnderlyingManagerAccount() {
		return this.targetUnderlyingManagerAccount;
	}


	public void setTargetUnderlyingManagerAccount(InvestmentManagerAccount targetUnderlyingManagerAccount) {
		this.targetUnderlyingManagerAccount = targetUnderlyingManagerAccount;
	}


	public BigDecimal getTargetUnderlyingMultiplier() {
		return this.targetUnderlyingMultiplier;
	}


	public void setTargetUnderlyingMultiplier(BigDecimal targetUnderlyingMultiplier) {
		this.targetUnderlyingMultiplier = targetUnderlyingMultiplier;
	}


	public Short getTrancheCount() {
		return this.trancheCount;
	}


	public void setTrancheCount(Short trancheCount) {
		this.trancheCount = trancheCount;
	}


	public Short getCycleDurationWeeks() {
		return this.cycleDurationWeeks;
	}


	public void setCycleDurationWeeks(Short cycleDurationWeeks) {
		this.cycleDurationWeeks = cycleDurationWeeks;
	}


	public String getSecurityTargetNote() {
		return this.securityTargetNote;
	}


	public void setSecurityTargetNote(String securityTargetNote) {
		this.securityTargetNote = securityTargetNote;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getLastBuyTradeDate() {
		return this.lastBuyTradeDate;
	}


	public void setLastBuyTradeDate(Date lastBuyTradeDate) {
		this.lastBuyTradeDate = lastBuyTradeDate;
	}


	public Date getLastSellTradeDate() {
		return this.lastSellTradeDate;
	}


	public void setLastSellTradeDate(Date lastSellTradeDate) {
		this.lastSellTradeDate = lastSellTradeDate;
	}
}
