package com.clifton.investment.account.group;


import com.clifton.core.beans.NamedEntity;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>InvestmentAccountGroup</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentAccountGroup extends NamedEntity<Integer> implements SystemEntityModifyConditionAware, EntityGroupRebuildAware {

	public static final String INVESTMENT_ACCOUNT_GROUP_TABLE_NAME = "InvestmentAccountGroup";

	/**
	 * The group type.
	 */
	private InvestmentAccountGroupType type;

	/**
	 * Optional field. If set will restrict accounts tied to this group
	 * by the specified type.
	 */
	private InvestmentAccountType accountType;

	/**
	 * If set, cannot change the name via UI
	 */
	private boolean systemDefined;

	/**
	 * A {@link SystemBean} that is used to rebuild the account group based on mappings in the InvestmentAccountGroupAccount table
	 */
	private SystemBean rebuildSystemBean;

	/**
	 * Additional security condition that when populated must evaluate to true in order for a non admin user to be able to edit this account group
	 */
	private SystemCondition entityModifyCondition;

	/**
	 * Alias used to identify the group.  For example, a group of collateral accounts are identified by an alias assigned by the broker.
	 */
	private String groupAlias;


	////////////////////////////////////////////////////////////////////////////
	////////////          InvestmentAccountGroup Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * System Managed means that users cannot add securities to the groups themselves
	 * it is maintained by executing the rebuildSystemQuery.
	 */
	public boolean isSystemManaged() {
		return getRebuildSystemBean() != null;
	}


	@Override
	public String getRebuildTableName() {
		return "InvestmentAccountGroupAccount";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountType getAccountType() {
		return this.accountType;
	}


	public void setAccountType(InvestmentAccountType accountType) {
		this.accountType = accountType;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	@Override
	public SystemBean getRebuildSystemBean() {
		return this.rebuildSystemBean;
	}


	public void setRebuildSystemBean(SystemBean rebuildSystemBean) {
		this.rebuildSystemBean = rebuildSystemBean;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public String getGroupAlias() {
		return this.groupAlias;
	}


	public void setGroupAlias(String groupAlias) {
		this.groupAlias = groupAlias;
	}


	public InvestmentAccountGroupType getType() {
		return this.type;
	}


	public void setType(InvestmentAccountGroupType type) {
		this.type = type;
	}
}
