package com.clifton.investment.account.calculation.snapshot.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public class InvestmentAccountCalculationSnapshotSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "calculationType.id")
	private Short calculationTypeId;

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.aumCalculatorOverrideBean.id,investmentAccount.businessService.aumCalculatorBean.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private Integer investmentAccountAumCalculatorBeanId;

	@SearchField
	private Date snapshotDate;

	@SearchField
	private BigDecimal calculatedValue;

	@SearchField(searchField = "calculatedValueCurrency.id")
	private Integer calculatedValueCurrencyId;

	@SearchField
	private BigDecimal fxRateToBase;

	@SearchField
	private BigDecimal calculatedValueBase;

	@SearchField(searchFieldPath = "calculationType", searchField = "baseCurrency.id")
	private Integer baseCurrencyId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchField = "name", searchFieldPath = "investmentAccount.businessClient.clientRelationship")
	private String clientRelationshipName;

	@SearchField(searchField = "name", searchFieldPath = "investmentAccount.businessClient")
	private String clientName;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "investmentAccount.businessService.id,investmentAccount.businessService.parent.id,investmentAccount.businessService.parent.parent.id,investmentAccount.businessService.parent.parent.parent.id", leftJoin = true, sortField = "investmentAccount.businessService.name")
	private Short businessServiceOrParentId;

	@SearchField(searchField = "teamSecurityGroup.id", searchFieldPath = "investmentAccount")
	private Short teamSecurityGroupId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getCalculationTypeId() {
		return this.calculationTypeId;
	}


	public void setCalculationTypeId(Short calculationTypeId) {
		this.calculationTypeId = calculationTypeId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public BigDecimal getCalculatedValue() {
		return this.calculatedValue;
	}


	public void setCalculatedValue(BigDecimal calculatedValue) {
		this.calculatedValue = calculatedValue;
	}


	public Integer getCalculatedValueCurrencyId() {
		return this.calculatedValueCurrencyId;
	}


	public void setCalculatedValueCurrencyId(Integer calculatedValueCurrencyId) {
		this.calculatedValueCurrencyId = calculatedValueCurrencyId;
	}


	public BigDecimal getFxRateToBase() {
		return this.fxRateToBase;
	}


	public void setFxRateToBase(BigDecimal fxRateToBase) {
		this.fxRateToBase = fxRateToBase;
	}


	public BigDecimal getCalculatedValueBase() {
		return this.calculatedValueBase;
	}


	public void setCalculatedValueBase(BigDecimal calculatedValueBase) {
		this.calculatedValueBase = calculatedValueBase;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Integer getInvestmentAccountAumCalculatorBeanId() {
		return this.investmentAccountAumCalculatorBeanId;
	}


	public void setInvestmentAccountAumCalculatorBeanId(Integer investmentAccountAumCalculatorBeanId) {
		this.investmentAccountAumCalculatorBeanId = investmentAccountAumCalculatorBeanId;
	}


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}
}
