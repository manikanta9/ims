package com.clifton.investment.account.assetclass.rebalance;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.assetclass.rebalance.search.InvestmentAccountRebalanceAssetClassHistorySearchForm;
import com.clifton.investment.account.assetclass.rebalance.search.InvestmentAccountRebalanceHistorySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountRebalanceServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class InvestmentAccountRebalanceServiceImpl implements InvestmentAccountRebalanceService {

	private AdvancedUpdatableDAO<InvestmentAccountRebalance, Criteria> investmentAccountRebalanceDAO;

	private AdvancedUpdatableDAO<InvestmentAccountRebalanceHistory, Criteria> investmentAccountRebalanceHistoryDAO;
	private AdvancedUpdatableDAO<InvestmentAccountRebalanceAssetClassHistory, Criteria> investmentAccountRebalanceAssetClassHistoryDAO;

	private CacheHandler<?, ?> cacheHandler;


	//////////////////////////////////////////////////////////////////////////
	////////////        Investment Account Rebalance Methods      //////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountRebalance getInvestmentAccountRebalance(int id) {
		return getInvestmentAccountRebalanceDAO().findByPrimaryKey(id);
	}


	/**
	 * NOTE: USE SPECIAL URL (NOT ENDING IN SAVE SO WE DON'T USE DEFAULT BINDING)
	 * Want to be able to pass ID and have the ability to create or update where necessary, not always expecting the object to be there.
	 */
	@Override
	public InvestmentAccountRebalance saveInvestmentAccountRebalance(InvestmentAccountRebalance bean) {
		ValidationUtils.assertNotNull(bean.getId(), "ID (Same as Account ID) should be passed to the save method.");
		// For some reason SystemBean is instantiated with id null - if so - clear it out
		if (bean.getMinimizeImbalancesCalculatorBean() != null && bean.getMinimizeImbalancesCalculatorBean().getId() == null) {
			bean.setMinimizeImbalancesCalculatorBean(null);
		}

		// If either Min/Max Rebalance Cash is set, then need to validate.
		if (bean.getMinAllowedCash() != null || bean.getMaxAllowedCash() != null) {
			// 1.  If one is set, then both must be populated.
			ValidationUtils.assertNotNull(bean.getMinAllowedCash(), "A Rebalance Cash Max value has been entered.  Please also enter a value for the rebalance minimum, or clear the maximum value.",
					"rebalanceMinAllowedCash");
			ValidationUtils.assertNotNull(bean.getMaxAllowedCash(), "A Rebalance Cash Min value has been entered.  Please also enter a value for the rebalance maximum, or clear the minimum value.",
					"rebalanceMaxAllowedCash");

			ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(bean.getMinAllowedCash(), bean.getMaxAllowedCash()), "Rebalance Cash Minimum must be less than Rebalance Cash Maximum.",
					"rebalanceMinAllowedCash");
		}
		else {
			// Otherwise - ensure automatic rebalancing option is turned off
			bean.setAutomatic(false);
		}

		InvestmentAccountRebalance original = getInvestmentAccountRebalance(bean.getId());
		if (original != null) {
			BeanUtils.copyPropertiesExceptAudit(bean, original);
			bean = getInvestmentAccountRebalanceDAO().save(original);
		}
		else {
			bean = getInvestmentAccountRebalanceDAO().save(bean);
			// After inserts clear the cache - these happen very infrequently - usually just when a new account is added
			// or new calculators are available for accounts.  It seems that after inserts the hibernate cache doesn't get updated with the rv right away
			// and subsequent updates are attempted as additional inserts (because natural key uses rv == null to check if new bean)
			// which fails PK - Because this happens so rarely clear the entire cache for now
			// which is still much more efficient then turning cache off for this table
			getCacheHandler().clear(getInvestmentAccountRebalanceDAO().getConfiguration().getCacheName());
		}
		return bean;
	}


	@Override
	public void deleteInvestmentAccountRebalance(int id) {
		getInvestmentAccountRebalanceDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////        Investment Account Rebalance History Methods      //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountRebalanceHistory getInvestmentAccountRebalanceHistory(int id) {
		InvestmentAccountRebalanceHistory bean = getInvestmentAccountRebalanceHistoryDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setAssetClassHistoryList(getInvestmentAccountRebalanceAssetClassHistoryListForHistory(id));
		}
		return bean;
	}


	/**
	 * Returns the InvestmentAccountRebalanceHistory record that is on or before given date.  If date is null, uses "today"
	 */
	@Override
	public InvestmentAccountRebalanceHistory getInvestmentAccountRebalanceHistoryRecent(int investmentAccountId, Date date) {
		InvestmentAccountRebalanceHistorySearchForm searchForm = new InvestmentAccountRebalanceHistorySearchForm();
		searchForm.setInvestmentAccountId(investmentAccountId);
		searchForm.addSearchRestriction(new SearchRestriction("rebalanceDate", ComparisonConditions.LESS_THAN, DateUtils.clearTime(DateUtils.addDays(date != null ? date : new Date(), 1))));
		searchForm.setOrderBy("rebalanceDate:desc#createDate:desc");
		searchForm.setLimit(1);

		InvestmentAccountRebalanceHistory bean = CollectionUtils.getFirstElement(getInvestmentAccountRebalanceHistoryList(searchForm));
		if (bean != null) {
			bean.setAssetClassHistoryList(getInvestmentAccountRebalanceAssetClassHistoryListForHistory(bean.getId()));
		}
		return bean;
	}


	@Override
	public List<InvestmentAccountRebalanceHistory> getInvestmentAccountRebalanceHistoryList(InvestmentAccountRebalanceHistorySearchForm searchForm) {
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("rebalanceDate:desc#createDate:desc");
		}
		return getInvestmentAccountRebalanceHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public InvestmentAccountRebalanceHistory saveInvestmentAccountRebalanceHistory(InvestmentAccountRebalanceHistory bean) {
		ValidationUtils.assertTrue(bean.isNewBean(), "Rebalance History Inserts are allowed only, not updates.");
		ValidationUtils.assertNotEmpty(bean.getAssetClassHistoryList(), "Asset Class Rebalance Values are required");

		for (InvestmentAccountRebalanceAssetClassHistory ah : bean.getAssetClassHistoryList()) {
			ah.setRebalanceHistory(bean);
		}
		bean.setBeforeRebalanceCashTotal(CoreMathUtils.sumProperty(bean.getAssetClassHistoryList(), InvestmentAccountRebalanceAssetClassHistory::getBeforeRebalanceCash));
		bean.setAfterRebalanceCashTotal(CoreMathUtils.sumProperty(bean.getAssetClassHistoryList(), InvestmentAccountRebalanceAssetClassHistory::getAfterRebalanceCash));

		List<InvestmentAccountRebalanceAssetClassHistory> assetClassHistoryList = bean.getAssetClassHistoryList();
		bean = getInvestmentAccountRebalanceHistoryDAO().save(bean);
		getInvestmentAccountRebalanceAssetClassHistoryDAO().saveList(assetClassHistoryList);
		bean.setAssetClassHistoryList(assetClassHistoryList);
		return bean;
	}


	/**
	 * NOTE: Special logic that you can only delete the last history record for an account
	 *
	 * @param id
	 */
	@Override
	@Transactional
	public void deleteInvestmentAccountRebalanceHistory(int id) {
		// Get Latest and see if Ids match
		InvestmentAccountRebalanceHistory bean = getInvestmentAccountRebalanceHistory(id);
		if (bean != null) {
			InvestmentAccountRebalanceHistory last = getInvestmentAccountRebalanceHistoryRecent(bean.getInvestmentAccount().getId(), null);
			if (last != null && !last.equals(bean)) {
				throw new ValidationException("Cannot delete rebalance history record because there has been subsequent rebalancing done.");
			}
			getInvestmentAccountRebalanceAssetClassHistoryDAO().deleteList(bean.getAssetClassHistoryList());
			getInvestmentAccountRebalanceHistoryDAO().delete(bean);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////    Investment Account Asset Class Rebalance History Methods   ///////
	////////////////////////////////////////////////////////////////////////////

	// NOTE: SAVES ARE DONE THROUGH THE ACCOUNT HISTORY - NO DIRECT ASSET CLASS HISTORY SAVES/DELETES


	/**
	 * Returns the InvestmentAccountRebalanceAssetClassHistory record that is on or before given date.  If date is null, uses "today"
	 */
	@Override
	public InvestmentAccountRebalanceAssetClassHistory getInvestmentAccountRebalanceAssetClassHistoryRecent(int investmentAccountAssetClassId, Date date) {
		InvestmentAccountRebalanceAssetClassHistorySearchForm searchForm = new InvestmentAccountRebalanceAssetClassHistorySearchForm();
		searchForm.setAccountAssetClassId(investmentAccountAssetClassId);
		searchForm.addSearchRestriction(new SearchRestriction("rebalanceDate", ComparisonConditions.LESS_THAN, DateUtils.clearTime(DateUtils.addDays(date != null ? date : new Date(), 1))));
		searchForm.setOrderBy("rebalanceDate:desc#historyCreateDate:desc");
		searchForm.setLimit(1);

		return CollectionUtils.getFirstElement(getInvestmentAccountRebalanceAssetClassHistoryList(searchForm));
	}


	private List<InvestmentAccountRebalanceAssetClassHistory> getInvestmentAccountRebalanceAssetClassHistoryListForHistory(int rebalanceHistoryId) {
		List<InvestmentAccountRebalanceAssetClassHistory> list = getInvestmentAccountRebalanceAssetClassHistoryDAO().findByField("rebalanceHistory.id", rebalanceHistoryId);
		// Sorting based on AccountAssetClass.Order
		return BeanUtils.sortWithFunction(list, rebalanceAssetClassHistory -> rebalanceAssetClassHistory.getAccountAssetClass().getOrder(), true);
	}


	@Override
	public List<InvestmentAccountRebalanceAssetClassHistory> getInvestmentAccountRebalanceAssetClassHistoryList(InvestmentAccountRebalanceAssetClassHistorySearchForm searchForm) {
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("rebalanceDate:desc#historyCreateDate:desc");
		}
		return getInvestmentAccountRebalanceAssetClassHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////          Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentAccountRebalance, Criteria> getInvestmentAccountRebalanceDAO() {
		return this.investmentAccountRebalanceDAO;
	}


	public void setInvestmentAccountRebalanceDAO(AdvancedUpdatableDAO<InvestmentAccountRebalance, Criteria> investmentAccountRebalanceDAO) {
		this.investmentAccountRebalanceDAO = investmentAccountRebalanceDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountRebalanceHistory, Criteria> getInvestmentAccountRebalanceHistoryDAO() {
		return this.investmentAccountRebalanceHistoryDAO;
	}


	public void setInvestmentAccountRebalanceHistoryDAO(AdvancedUpdatableDAO<InvestmentAccountRebalanceHistory, Criteria> investmentAccountRebalanceHistoryDAO) {
		this.investmentAccountRebalanceHistoryDAO = investmentAccountRebalanceHistoryDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountRebalanceAssetClassHistory, Criteria> getInvestmentAccountRebalanceAssetClassHistoryDAO() {
		return this.investmentAccountRebalanceAssetClassHistoryDAO;
	}


	public void setInvestmentAccountRebalanceAssetClassHistoryDAO(AdvancedUpdatableDAO<InvestmentAccountRebalanceAssetClassHistory, Criteria> investmentAccountRebalanceAssetClassHistoryDAO) {
		this.investmentAccountRebalanceAssetClassHistoryDAO = investmentAccountRebalanceAssetClassHistoryDAO;
	}


	public CacheHandler<?, ?> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<?, ?> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
