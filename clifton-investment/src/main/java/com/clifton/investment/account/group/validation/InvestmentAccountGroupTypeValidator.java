package com.clifton.investment.account.group.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.group.InvestmentAccountGroupType;
import org.springframework.stereotype.Component;


@Component
public class InvestmentAccountGroupTypeValidator extends SelfRegisteringDaoValidator<InvestmentAccountGroupType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(InvestmentAccountGroupType bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("Deleting System Defined Investment Account Group Types is not allowed.");
			}
			if (config.isInsert()) {
				throw new ValidationException("Adding new System Defined Investment Account Group Types is not allowed.");
			}
			if (config.isUpdate()) {
				throw new ValidationException("Updating System Defined Investment Account Group Types is not allowed.");
			}
		}
	}
}
