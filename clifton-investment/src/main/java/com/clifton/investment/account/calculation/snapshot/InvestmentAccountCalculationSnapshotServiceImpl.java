package com.clifton.investment.account.calculation.snapshot;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.calculation.snapshot.search.InvestmentAccountCalculationSnapshotDetailSearchForm;
import com.clifton.investment.account.calculation.snapshot.search.InvestmentAccountCalculationSnapshotSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
@Service
public class InvestmentAccountCalculationSnapshotServiceImpl implements InvestmentAccountCalculationSnapshotService {

	private AdvancedUpdatableDAO<InvestmentAccountCalculationSnapshot, Criteria> investmentAccountCalculationSnapshotDAO;
	private AdvancedUpdatableDAO<InvestmentAccountCalculationSnapshotDetail, Criteria> investmentAccountCalculationSnapshotDetailDAO;

	private DaoSingleKeyListCache<InvestmentAccountCalculationSnapshotDetail, Integer> investmentAccountCalculationSnapshotDetailListForSnapshotCache;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountCalculationSnapshot getInvestmentAccountCalculationSnapshot(int id) {
		InvestmentAccountCalculationSnapshot snapshot = getInvestmentAccountCalculationSnapshotDAO().findByPrimaryKey(id);
		if (snapshot != null) {
			snapshot.setDetailList(BeanUtils.sortWithFunction(getInvestmentAccountCalculationSnapshotDetailListForSnapshot(id), snapshotDetail -> (snapshotDetail.isChild() ? (snapshotDetail.getParent().getId() + "_" + snapshot.getId()) : snapshotDetail.getId()), true));
		}
		return snapshot;
	}


	@Override
	public List<InvestmentAccountCalculationSnapshot> getInvestmentAccountCalculationSnapshotList(InvestmentAccountCalculationSnapshotSearchForm searchForm, boolean populateDetails) {
		List<InvestmentAccountCalculationSnapshot> result = getInvestmentAccountCalculationSnapshotDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		if (populateDetails) {
			for (InvestmentAccountCalculationSnapshot snapshot : CollectionUtils.getIterable(result)) {
				snapshot.setDetailList(getInvestmentAccountCalculationSnapshotDetailListForSnapshot(snapshot.getId()));
			}
		}
		return result;
	}


	/**
	 * Saves the list of snapshots and the details for each snapshot
	 */
	@Override
	public void saveInvestmentAccountCalculationSnapshotList(Set<InvestmentAccountCalculationSnapshot> snapshotList, Set<InvestmentAccountCalculationSnapshot> existingSnapshotList) {
		for (InvestmentAccountCalculationSnapshot snapshot : CollectionUtils.getIterable(snapshotList)) {
			saveInvestmentAccountCalculationSnapshot(snapshot);
		}

		for (InvestmentAccountCalculationSnapshot existingSnapshot : CollectionUtils.getIterable(existingSnapshotList)) {
			if (!snapshotList.contains(existingSnapshot)) {
				deleteInvestmentAccountCalculationSnapshot(existingSnapshot);
			}
		}
	}


	@Transactional
	private void saveInvestmentAccountCalculationSnapshot(InvestmentAccountCalculationSnapshot snapshot) {
		List<InvestmentAccountCalculationSnapshotDetail> existingDetailList = null;
		if (!snapshot.isNewBean()) {
			existingDetailList = getInvestmentAccountCalculationSnapshotDetailListForSnapshot(snapshot.getId());
		}
		List<InvestmentAccountCalculationSnapshotDetail> detailList = snapshot.getDetailList();
		snapshot = getInvestmentAccountCalculationSnapshotDAO().save(snapshot);
		for (InvestmentAccountCalculationSnapshotDetail detail : CollectionUtils.getIterable(detailList)) {
			detail.setCalculationSnapshot(snapshot);
		}
		// Sorting is important here so all parents are saved first.  Otherwise if there are deletes/inserts the save can throw an unsaved transient instance error during the hierarchy validation
		detailList = BeanUtils.sortWithFunction(detailList, InvestmentAccountCalculationSnapshotDetail::getLevel, true);
		getInvestmentAccountCalculationSnapshotDetailDAO().saveList(detailList, existingDetailList);
	}


	@Transactional
	private void deleteInvestmentAccountCalculationSnapshot(InvestmentAccountCalculationSnapshot snapshot) {
		getInvestmentAccountCalculationSnapshotDetailDAO().deleteList(getInvestmentAccountCalculationSnapshotDetailListForSnapshot(snapshot.getId()));
		getInvestmentAccountCalculationSnapshotDAO().delete(snapshot.getId());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountCalculationSnapshotDetail getInvestmentAccountCalculationSnapshotDetail(int id) {
		return getInvestmentAccountCalculationSnapshotDetailDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountCalculationSnapshotDetail> getInvestmentAccountCalculationSnapshotDetailListForSnapshot(int snapshotId) {
		return getInvestmentAccountCalculationSnapshotDetailListForSnapshotCache().getBeanListForKeyValue(getInvestmentAccountCalculationSnapshotDetailDAO(), snapshotId);
	}


	@Override
	public List<InvestmentAccountCalculationSnapshotDetail> getInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshotDetailSearchForm searchForm) {
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("sortIdOrder:ASC");
		}
		return getInvestmentAccountCalculationSnapshotDetailDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////               Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentAccountCalculationSnapshot, Criteria> getInvestmentAccountCalculationSnapshotDAO() {
		return this.investmentAccountCalculationSnapshotDAO;
	}


	public void setInvestmentAccountCalculationSnapshotDAO(AdvancedUpdatableDAO<InvestmentAccountCalculationSnapshot, Criteria> investmentAccountCalculationSnapshotDAO) {
		this.investmentAccountCalculationSnapshotDAO = investmentAccountCalculationSnapshotDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountCalculationSnapshotDetail, Criteria> getInvestmentAccountCalculationSnapshotDetailDAO() {
		return this.investmentAccountCalculationSnapshotDetailDAO;
	}


	public void setInvestmentAccountCalculationSnapshotDetailDAO(AdvancedUpdatableDAO<InvestmentAccountCalculationSnapshotDetail, Criteria> investmentAccountCalculationSnapshotDetailDAO) {
		this.investmentAccountCalculationSnapshotDetailDAO = investmentAccountCalculationSnapshotDetailDAO;
	}


	public DaoSingleKeyListCache<InvestmentAccountCalculationSnapshotDetail, Integer> getInvestmentAccountCalculationSnapshotDetailListForSnapshotCache() {
		return this.investmentAccountCalculationSnapshotDetailListForSnapshotCache;
	}


	public void setInvestmentAccountCalculationSnapshotDetailListForSnapshotCache(DaoSingleKeyListCache<InvestmentAccountCalculationSnapshotDetail, Integer> investmentAccountCalculationSnapshotDetailListForSnapshotCache) {
		this.investmentAccountCalculationSnapshotDetailListForSnapshotCache = investmentAccountCalculationSnapshotDetailListForSnapshotCache;
	}
}
