package com.clifton.investment.account.assetclass.position;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentAccountAssetClassPositionAllocation</code> defines explicit contract splitting
 * across asset classes for a specified security.
 *
 * @author manderson
 */
public class InvestmentAccountAssetClassPositionAllocation extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentAccountAssetClass accountAssetClass;

	/**
	 * Optional replication selection - used for contract splitting within the same asset class
	 */
	private InvestmentReplication replication;

	private InvestmentSecurity security;

	/**
	 * Not required, but shouldn't be assigned if security isn't held on the date
	 * (Ignorable warning on Portfolio Runs is generated when this happens)
	 * Must be unique per asset class/security and date range.
	 */
	private Date startDate;
	private Date endDate;

	/**
	 * Order is optional to define which asset class to fill first
	 * if shouldn't use a smallest first and virtual contracts will be used.
	 * Generally will apply when splitting in more than 2 asset classes with short and long
	 */
	private Short allocationOrder;

	/**
	 * Total contracts that should be allocated to this asset class for this security
	 * Generally will = Actual, but in cases where splits are long/short virtual will also
	 * be used to fulfill the quantity specified
	 */
	private BigDecimal allocationQuantity;


	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////


	public String getDateLabel() {
		return DateUtils.fromDateRange(getStartDate(), getEndDate(), true, false);
	}


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder(16);
		if (getAccountAssetClass() != null) {
			sb.append(getAccountAssetClass().getAssetClass().getName());
		}
		if (getReplication() != null) {
			sb.append(" (").append(getReplication().getName()).append(")");
		}
		sb.append(" - ");
		if (getSecurity() != null) {
			sb.append(getSecurity().getLabel());
		}
		sb.append(" ");
		sb.append(getDateLabel());
		return sb.toString();
	}


	public String getLabelLong() {
		if (getAccountAssetClass() != null) {
			return getAccountAssetClass().getAccount().getLabel() + ": " + getLabel();
		}
		return getLabel();
	}


	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public void setAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public BigDecimal getAllocationQuantity() {
		return this.allocationQuantity;
	}


	public void setAllocationQuantity(BigDecimal allocationQuantity) {
		this.allocationQuantity = allocationQuantity;
	}


	public Short getAllocationOrder() {
		return this.allocationOrder;
	}


	public void setAllocationOrder(Short allocationOrder) {
		this.allocationOrder = allocationOrder;
	}


	public InvestmentReplication getReplication() {
		return this.replication;
	}


	public void setReplication(InvestmentReplication replication) {
		this.replication = replication;
	}
}
