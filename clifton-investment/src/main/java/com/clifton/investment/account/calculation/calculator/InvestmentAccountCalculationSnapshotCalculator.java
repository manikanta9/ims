package com.clifton.investment.account.calculation.calculator;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;


/**
 * The <code>InvestmentAccountCalculationSnapshotCalculator</code> is a generic interface that takes an account and date and returns the {@link com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot}
 * results (with details if applies)
 * <p>
 * It can be called by the {@link com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessor} for each account
 *
 * @author manderson
 */
public interface InvestmentAccountCalculationSnapshotCalculator {


	public InvestmentAccountCalculationSnapshot calculateInvestmentAccountCalculationSnapshot(InvestmentAccount investmentAccount, InvestmentAccountCalculationProcessorContext context);
}
