package com.clifton.investment.account.group.cache;


import com.clifton.core.cache.LazilyBuiltCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>InvestmentAccountGroupCacheImpl</code> contains a lazily built cache for
 * a mapping of account groups to a set of the accounts that fall within them.
 *
 * @author apopp
 */
@Component
public class InvestmentAccountGroupCacheImpl extends LazilyBuiltCache<Map<String, Set<Integer>>> implements InvestmentAccountGroupCache {

	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isInvestmentAccountInGroup(final String groupName, final int accountId) {
		return getGroupCache(groupName).contains(accountId);
	}


	@Override
	public List<InvestmentAccount> getInvestmentAccountListByGroup(final String groupName) {
		Set<Integer> accountIds = getGroupCache(groupName);
		if (accountIds != null) {
			return getInvestmentAccountService().getInvestmentAccountListByIds(new ArrayList<>(accountIds));
		}
		return null;
	}


	@Override
	public void clearGroupCache(final String groupName) {
		getOrBuildCache().remove(groupName);
	}


	/**
	 * Note: Method is public so pre-load cache job can build the cache for groups
	 */
	@Override
	public synchronized void buildGroupCache(final String groupName) {
		Set<Integer> groupCache = getOrBuildCache().get(groupName);

		if (groupCache == null) {
			Set<Integer> accountSet = Collections.synchronizedSet(new HashSet<>());

			InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
			searchForm.setGroupName(groupName);

			List<InvestmentAccountGroupAccount> accountsInGroupList = getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm);

			for (InvestmentAccountGroupAccount itemAccount : CollectionUtils.getIterable(accountsInGroupList)) {
				accountSet.add(itemAccount.getReferenceTwo().getId());
			}

			getOrBuildCache().put(groupName, accountSet);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Set<Integer> getGroupCache(final String groupName) {
		Set<Integer> groupCache = getOrBuildCache().get(groupName);

		if (groupCache == null) {
			buildGroupCache(groupName);
			return getOrBuildCache().get(groupName);
		}

		return groupCache;
	}


	@Override
	protected Map<String, Set<Integer>> buildCache() {
		return new ConcurrentHashMap<>();
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////            Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
