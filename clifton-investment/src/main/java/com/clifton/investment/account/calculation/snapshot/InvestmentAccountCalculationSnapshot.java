package com.clifton.investment.account.calculation.snapshot;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.calculation.InvestmentAccountCalculationType;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * The <code>InvestmentAccountCalculationSnapshot</code> is the result of the {@link InvestmentAccountCalculationType} processing
 * for a specific account and date.
 *
 * @author manderson
 */
public class InvestmentAccountCalculationSnapshot extends BaseSimpleEntity<Integer> {


	private InvestmentAccountCalculationType calculationType;

	private InvestmentAccount investmentAccount;

	private Date snapshotDate;

	/**
	 * The calculated value in the calculatedValueCurrency
	 */
	private BigDecimal calculatedValue;

	/**
	 * Calculated Value currency - for AUM this would be the Base CCY of the Account
	 * for Projected Revenue it would be the billing currency from the definition
	 */
	private InvestmentSecurity calculatedValueCurrency;

	/**
	 * FX Rate for the calculatedValue to be reported in the calculationType base currency
	 */
	private BigDecimal fxRateToBase;


	/**
	 * Database calculated column - calculatedValue * fxRateToBase
	 */
	private BigDecimal calculatedValueBase;


	private List<InvestmentAccountCalculationSnapshotDetail> detailList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationSnapshot() {
		super();
	}


	public InvestmentAccountCalculationSnapshot(InvestmentAccountCalculationType calculationType, InvestmentAccount investmentAccount, Date snapshotDate) {
		this();
		this.calculationType = calculationType;
		this.investmentAccount = investmentAccount;
		this.snapshotDate = snapshotDate;
	}


	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof InvestmentAccountCalculationSnapshot)) {
			return false;
		}
		InvestmentAccountCalculationSnapshot snapObject = (InvestmentAccountCalculationSnapshot) obj;
		if (CompareUtils.isEqual(snapObject.getInvestmentAccount(), this.investmentAccount)) {
			if (DateUtils.compare(snapObject.getSnapshotDate(), this.snapshotDate, false) == 0) {
				return true;
			}
		}
		return false;
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.investmentAccount, this.snapshotDate);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		StringBuilder label = new StringBuilder(50);
		if (getCalculationType() != null) {
			label.append(getCalculationType().getName());
			label.append(" - ");
		}
		if (getInvestmentAccount() != null) {
			label.append(getInvestmentAccount().getLabel());
			label.append(" - ");
		}
		if (getSnapshotDate() != null) {
			label.append(DateUtils.fromDateShort(getSnapshotDate()));
		}
		return label.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationType getCalculationType() {
		return this.calculationType;
	}


	public void setCalculationType(InvestmentAccountCalculationType calculationType) {
		this.calculationType = calculationType;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public BigDecimal getCalculatedValue() {
		return this.calculatedValue;
	}


	public void setCalculatedValue(BigDecimal calculatedValue) {
		this.calculatedValue = calculatedValue;
	}


	public InvestmentSecurity getCalculatedValueCurrency() {
		return this.calculatedValueCurrency;
	}


	public void setCalculatedValueCurrency(InvestmentSecurity calculatedValueCurrency) {
		this.calculatedValueCurrency = calculatedValueCurrency;
	}


	public BigDecimal getFxRateToBase() {
		return this.fxRateToBase;
	}


	public void setFxRateToBase(BigDecimal fxRateToBase) {
		this.fxRateToBase = fxRateToBase;
	}


	public BigDecimal getCalculatedValueBase() {
		return this.calculatedValueBase;
	}


	public void setCalculatedValueBase(BigDecimal calculatedValueBase) {
		this.calculatedValueBase = calculatedValueBase;
	}


	public List<InvestmentAccountCalculationSnapshotDetail> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<InvestmentAccountCalculationSnapshotDetail> detailList) {
		this.detailList = detailList;
	}
}
