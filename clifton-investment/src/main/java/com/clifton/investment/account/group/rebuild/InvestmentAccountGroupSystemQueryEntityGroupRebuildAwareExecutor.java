package com.clifton.investment.account.group.rebuild;

import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.query.bean.rebuild.SystemQueryEntityGroupRebuildAwareExecutor;


/**
 * <code>InvestmentAccountGroupSystemQueryEntityGroupRebuildAwareExecutor</code> is a {@link SystemBeanType}
 * that extends {@link SystemQueryEntityGroupRebuildAwareExecutor} but offers the same functionality. It also
 * implements {@link InvestmentAccountGroupEntityGroupRebuildAwareExecutor}. The reason for the new class is
 * because the SystemBean framework only allows a class to be referenced by one {@link SystemBeanType}.
 *
 * @author NickK
 */
public class InvestmentAccountGroupSystemQueryEntityGroupRebuildAwareExecutor extends SystemQueryEntityGroupRebuildAwareExecutor implements InvestmentAccountGroupEntityGroupRebuildAwareExecutor {

}
