package com.clifton.investment.account.securitytarget;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>InvestmentAccountSecurityTargetFreeze</code> represents a frozen quantity or notional amount of a {@link InvestmentAccountSecurityTarget}.
 * Frozen amounts are included in the target's utilization to prevent trading more than the targeted amount.
 * <p>
 * Frozen amounts can be created to freeze a held position to prevent it from being traded, or can be created to represent a frozen amount that should
 * be frozen until the expiration.
 * <p>
 * An example of using frozen shares is in the DeltaShift strategy. If a covered Option call position is closed more than 10 days before its expiration
 * date, the position must be accounted for in the portfolio until within the 10 day window. The closed position will result in a frozen quantity of the
 * underlying security to show utilization of the target until the 10 day window is reached.
 *
 * @author NickK
 */
public class InvestmentAccountSecurityTargetFreeze extends BaseEntity<Integer> {

	private InvestmentAccount clientInvestmentAccount;

	private InvestmentSecurity targetUnderlyingSecurity;

	private InvestmentAccountSecurityTargetFreezeType freezeType;

	/**
	 * An optional security with an underlying security matching that of the targeted underlying {@link InvestmentSecurity}.
	 * This field would be used to freeze a quantity for a specific security, such as when freezing a held position from being traded.
	 */
	private InvestmentSecurity causeInvestmentSecurity;
	/**
	 * The persisted quantity of the underlying {@link InvestmentSecurity} to freeze.
	 * The value is normalized to be be the quantity of {@link #getCauseInvestmentSecurity()} multiplied by the multiplier of the cause security.
	 *
	 * @see InvestmentAccountSecurityTarget#getTargetUnderlyingSecurity()
	 */
	private BigDecimal freezeUnderlyingQuantity;
	/**
	 * Non-persisted field that defines the quantity of the {@link #causeInvestmentSecurity} to freeze.
	 * The quantity value will be multiplied by the cause security's multiplier to calculate the {@link #freezeUnderlyingQuantity}
	 */
	@NonPersistentField
	private BigDecimal freezeCauseSecurityQuantity;

	private String freezeNote;

	private Date startDate;

	private Date endDate;

	/**
	 * Source link to the cause of the frozen quantity.
	 * Example: DeltaShift client position closed early (e.g. closed > 10 days before the option's expiration date)
	 */
	private SystemTable causeSystemTable;

	@SoftLinkField(tableBeanPropertyName = "causeSystemTable")
	private Integer causeFKFieldId;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetFreezeType getFreezeType() {
		return this.freezeType;
	}


	public void setFreezeType(InvestmentAccountSecurityTargetFreezeType freezeType) {
		this.freezeType = freezeType;
	}


	public InvestmentSecurity getCauseInvestmentSecurity() {
		return this.causeInvestmentSecurity;
	}


	public void setCauseInvestmentSecurity(InvestmentSecurity causeInvestmentSecurity) {
		this.causeInvestmentSecurity = causeInvestmentSecurity;
	}


	public BigDecimal getFreezeUnderlyingQuantity() {
		return this.freezeUnderlyingQuantity;
	}


	public void setFreezeUnderlyingQuantity(BigDecimal freezeUnderlyingQuantity) {
		this.freezeUnderlyingQuantity = freezeUnderlyingQuantity;
	}


	public BigDecimal getFreezeCauseSecurityQuantity() {
		return this.freezeCauseSecurityQuantity;
	}


	public void setFreezeCauseSecurityQuantity(BigDecimal freezeCauseSecurityQuantity) {
		this.freezeCauseSecurityQuantity = freezeCauseSecurityQuantity;
	}


	public String getFreezeNote() {
		return this.freezeNote;
	}


	public void setFreezeNote(String freezeNote) {
		this.freezeNote = freezeNote;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public SystemTable getCauseSystemTable() {
		return this.causeSystemTable;
	}


	public void setCauseSystemTable(SystemTable causeSystemTable) {
		this.causeSystemTable = causeSystemTable;
	}


	public Integer getCauseFKFieldId() {
		return this.causeFKFieldId;
	}


	public void setCauseFKFieldId(Integer causeFKFieldId) {
		this.causeFKFieldId = causeFKFieldId;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentSecurity getTargetUnderlyingSecurity() {
		return this.targetUnderlyingSecurity;
	}


	public void setTargetUnderlyingSecurity(InvestmentSecurity targetUnderlyingSecurity) {
		this.targetUnderlyingSecurity = targetUnderlyingSecurity;
	}
}
