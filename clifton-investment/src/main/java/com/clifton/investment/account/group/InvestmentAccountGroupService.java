package com.clifton.investment.account.group;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.account.group.search.InvestmentAccountGroupSearchForm;
import com.clifton.investment.account.group.search.InvestmentAccountGroupTypeSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>InvestmentAccountGroupService</code> interface defines methods for managing InvestmentAccountGroup objects.
 *
 * @author mwacker
 */
public interface InvestmentAccountGroupService {

	////////////////////////////////////////////////////////////////////////////
	///////           Investment Account Group Business Methods          ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroup getInvestmentAccountGroup(int id);


	public InvestmentAccountGroup getInvestmentAccountGroupByName(String name);


	public List<InvestmentAccountGroup> getInvestmentAccountGroupListByIds(List<Integer> ids);


	public List<InvestmentAccountGroup> getInvestmentAccountGroupList(InvestmentAccountGroupSearchForm searchForm);


	public InvestmentAccountGroup saveInvestmentAccountGroup(InvestmentAccountGroup bean);


	public void deleteInvestmentAccountGroup(int id);


	public Status rebuildInvestmentAccountGroup(int groupId);


	@SecureMethod(dtoClass = InvestmentAccountGroupAccount.class)
	public Status rebuildInvestmentAccountGroupSystemManaged();


	////////////////////////////////////////////////////////////////////////////
	////           Investment Account Group Account Business Methods        ////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroupAccount getInvestmentAccountGroupAccount(int id);


	public List<InvestmentAccountGroupAccount> getInvestmentAccountGroupAccountList(InvestmentAccountGroupAccountSearchForm searchForm);


	@RequestMapping("investmentAccountGroupAccountPrimaryByGroup")
	public InvestmentAccountGroupAccount getPrimaryInvestmentAccountGroupAccountByGroup(int groupId);


	public List<InvestmentAccount> getInvestmentAccountListByGroup(String groupName);


	@SecureMethod(dtoClass = InvestmentAccountGroupAccount.class)
	public boolean isInvestmentAccountInGroup(int accountId, String groupName);


	@SecureMethod(dtoClass = InvestmentAccountGroupAccount.class)
	public boolean isInvestmentAccountInGroupOfType(int accountId, String groupTypeName);


	@SecureMethod(dtoClass = InvestmentAccountGroup.class)
	public void linkInvestmentAccountGroupToAccount(int groupId, int accountId);


	@DoNotAddRequestMapping
	public void linkInvestmentAccountGroupToAccountList(InvestmentAccountGroup group, List<InvestmentAccount> accountList);


	public InvestmentAccountGroupAccount saveInvestmentAccountGroupAccount(InvestmentAccountGroupAccount bean);


	public void deleteInvestmentAccountGroupAccount(int groupId, int accountId);


	/**
	 * Deletes the provided {@link InvestmentAccountGroupAccount}. It assumes that the provided
	 * bean has been previously obtained so it does not have to be looked up.
	 */
	@DoNotAddRequestMapping
	public void deleteInvestmentAccountGroupAccount(InvestmentAccountGroupAccount bean);


	////////////////////////////////////////////////////////////////////////////
	////           Investment Account Group Type Business Methods           ////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroupType getInvestmentAccountGroupType(short id);


	public List<InvestmentAccountGroupType> getInvestmentAccountGroupTypeList(InvestmentAccountGroupTypeSearchForm searchForm);


	public InvestmentAccountGroupType saveInvestmentAccountGroupType(InvestmentAccountGroupType bean);


	public void deleteInvestmentAccountGroupType(short id);
}
