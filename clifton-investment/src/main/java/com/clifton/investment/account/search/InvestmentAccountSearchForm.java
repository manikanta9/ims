package com.clifton.investment.account.search;


import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.business.service.AccountingClosingMethods;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.system.schema.column.search.SystemColumnDatedValueAwareSearchForm;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentAccountSearchForm</code> class defines search configuration for InvestmentAccount objects.
 *
 * @author manderson
 */
public class InvestmentAccountSearchForm extends BaseWorkflowAwareSearchForm implements SystemColumnDatedValueAwareSearchForm {

	@SearchField(sortField = "number")
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Integer[] excludeIds;

	@SearchField(searchField = "number,number2,name,shortName")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String shortName;

	@SearchField
	private String number;

	@SearchField(searchField = "number", comparisonConditions = ComparisonConditions.EQUALS)
	private String numberEquals;

	@SearchField
	private String number2;

	@SearchField(searchField = "businessClient.id")
	private Integer clientId;

	@SearchField(searchField = "businessClient.company.id,businessClient.clientRelationship.company.id", leftJoin = true)
	private Integer clientOrClientRelationshipCompanyId;

	// Will include the client's parent's accounts (if a child), or the children client's accounts
	// ClientID = ? OR Client's Parent ID = ? OR (LOOKUP CLIENT'S PARENT ID AND IF NOT NULL - THAT VALUE = ClientID)
	private Integer clientIdOrRelatedClient;

	@SearchField(searchFieldPath = "businessClient", searchField = "name")
	private String clientName;

	@SearchField(searchFieldPath = "businessClient", searchField = "inceptionDate")
	private Date clientInceptionDate;

	@SearchField(searchFieldPath = "businessClient.category", searchField = "categoryType", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private BusinessClientCategoryTypes excludeClientCategoryType;

	@SearchField(searchFieldPath = "businessClient.category", searchField = "categoryType")
	private BusinessClientCategoryTypes[] clientCategoryTypes;

	@SearchField(searchFieldPath = "businessClient", searchField = "category.id")
	private Short[] clientCategoryIds;

	@SearchField(searchFieldPath = "businessClient", searchField = "clientRelationship.id", sortField = "clientRelationship.name")
	private Integer clientRelationshipId;

	@SearchField(searchFieldPath = "businessClient.clientRelationship", searchField = "inceptionDate")
	private Date clientRelationshipInceptionDate;

	@SearchField(searchFieldPath = "businessClient.clientRelationship", searchField = "relationshipCategory.id", sortField = "relationshipCategory.name")
	private Short clientRelationshipCategoryId;

	@SearchField(searchFieldPath = "businessClient.clientRelationship.relationshipCategory", searchField = "name")
	private String clientRelationshipCategoryName;

	@SearchField(searchFieldPath = "businessClient.clientRelationship.relationshipCategory", searchField = "retail", searchFieldCustomType = SearchFieldCustomTypes.COALESCE_FALSE, leftJoin = true)
	private Boolean clientRelationshipCategoryRetail;

	@SearchField(searchFieldPath = "businessClient", searchField = "taxable")
	private Boolean clientTaxable;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String accountType;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = ComparisonConditions.IN)
	private String[] accountTypes;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private String excludeAccountType;

	@SearchField(searchField = "type.id")
	private Short accountTypeId;

	@SearchField(searchField = "type.id", comparisonConditions = {ComparisonConditions.IN})
	private Short[] accountTypeIds;

	@SearchField(searchField = "businessContract.id")
	private Integer businessContractId;

	@SearchField(searchFieldPath = "type", searchField = "contractRequired")
	private Boolean contractRequired;

	@SearchField(searchFieldPath = "type", searchField = "executionWithIssuerRequired")
	private Boolean executionWithIssuerRequired;

	@SearchField(searchField = "businessContract.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean businessContractPopulated;

	@SearchField(searchFieldPath = "businessContract.contractType", searchField = "categoryName")
	private String contractCategory;

	@SearchField(searchFieldPath = "businessContract.contractType", searchField = "name")
	private String contractType;

	@SearchField
	private Boolean clientDirected;

	@SearchField
	private Boolean usedForSpeculation;

	@SearchField
	private BigDecimal initialMarginMultiplier;

	@SearchField(searchField = "companyPlatform.id", sortField = "companyPlatform.name")
	private Integer companyPlatformId;

	@SearchField
	private Boolean wrapAccount;


	@SearchField(searchField = "businessService.id")
	private Short businessServiceId;

	@SearchField(searchField = "businessService.id", comparisonConditions = {ComparisonConditions.IN})
	private Short[] businessServiceIds;

	@SearchField(searchField = "businessService.id", comparisonConditions = {ComparisonConditions.NOT_EQUALS})
	private Short excludeBusinessServiceId;

	@SearchField(searchField = "businessService.id", comparisonConditions = {ComparisonConditions.NOT_IN})
	private Short[] excludeBusinessServiceIds;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "businessService.id,businessService.parent.id,businessService.parent.parent.id,businessService.parent.parent.parent.id", leftJoin = true, sortField = "name")
	private Short businessServiceOrParentId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "businessService.id,businessService.parent.id,businessService.parent.parent.id,businessService.parent.parent.parent.id", leftJoin = true, sortField = "name")
	private Short[] businessServiceOrParentIds;

	@SearchField(searchField = "businessServiceList.businessService.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short businessServiceComponentId;


	@SearchField
	private AccountingClosingMethods accountingClosingMethodOverride;

	@SearchField(searchField = "aumCalculatorOverrideBean.id,businessService.aumCalculatorBean.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private Integer aumCalculatorBeanId;

	@SearchField
	private String clientAccountType;

	@SearchField
	private String discretionType;

	@SearchField(searchField = "discretionType", comparisonConditions = {ComparisonConditions.EQUALS})
	private String discretionTypeEqual;

	@SearchField(searchField = "discretionType", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullDiscretionType;

	@SearchField(searchField = "clientAccountChannel.id", sortField = "clientAccountChannel.text")
	private Integer clientAccountChannelId;

	@SearchField(searchField = "value,text", searchFieldPath = "clientAccountChannel", sortField = "text")
	private String clientAccountChannelName;

	@SearchField(searchField = "value,text", searchFieldPath = "externalPortfolioCode", sortField = "text")
	private String externalPortfolioCodeName;

	@SearchField(searchField = "value,text", searchFieldPath = "externalProductCode", sortField = "text")
	private String externalProductCodeName;

	@SearchField(searchField = "processingType", searchFieldPath = "serviceProcessingType")
	private ServiceProcessingTypes serviceProcessingType;

	@SearchField(searchField = "processingType", searchFieldPath = "serviceProcessingType", comparisonConditions = ComparisonConditions.IN)
	private ServiceProcessingTypes[] serviceProcessingTypes;

	@SearchField(searchField = "serviceProcessingType.id")
	private Short businessServiceProcessingTypeId;

	@SearchField(searchField = "serviceProcessingType.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] businessServiceProcessingTypeIds;

	@SearchField(searchField = "serviceProcessingType.name")
	private String businessServiceProcessingTypeName;

	@SearchField(searchField = "teamSecurityGroup.id")
	private Short teamSecurityGroupId;

	@SearchField(searchField = "baseCurrency.id")
	private Integer baseCurrencyId;

	@SearchField(searchField = "defaultExchangeRateSourceCompany.id", sortField = "defaultExchangeRateSourceCompany.name")
	private Integer defaultExchangeRateSourceCompanyId;

	@SearchField(searchField = "federalTaxNumber", searchFieldPath = "businessClient")
	private String federalTaxNumber;

	@SearchField(searchField = "stateTaxNumber", searchFieldPath = "businessClient")
	private String stateTaxNumber;

	@SearchField(searchField = "issuingCompany.id")
	private Integer issuingCompanyId;

	@SearchField(searchField = "issuingCompany.id", comparisonConditions = {ComparisonConditions.IN})
	private Integer[] issuingCompanyIds;

	@SearchField(searchFieldPath = "issuingCompany", searchField = "name")
	private String issuingCompanyName;

	@SearchField(searchFieldPath = "issuingCompany.type", searchField = "name")
	private String issuingCompanyTypeName;

	@SearchField
	private Date workflowStateEffectiveStartDate;

	@SearchField
	private Date inceptionDate;

	@SearchField(searchField = "inceptionDate", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean inceptionDatePopulated;

	@SearchField
	private Date performanceInceptionDate;

	@SearchField(searchField = "assetClassList.assetClass.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short assetClassId;

	////////////////////////////////////////////////////////////////////////////

	// Custom search filter - restrict accounts by relationships where this is the main account
	private Integer mainAccountId;

	// Custom search filter - restrict accounts by relationships where this is the related account
	private Integer relatedAccountId;

	// Custom search filter - lookup related accounts with the given main purpose
	private String mainPurpose;

	// Custom search filter - lookup related accounts with the given main purpose id
	private Short mainPurposeId;

	// Custom Search Filter - lookup related accounts with the given main purpose where the main account is in the account group (only used when mainPurposeId is populated)
	private Integer mainPurposeInvestmentAccountGroupId;

	// Custom search filter - lookup main accounts with the given related purpose
	private String relatedPurpose;

	// Custom search filter - lookup main accounts with one of the given related purposes
	private String[] relatedPurposes;

	// Custom search filter - lookup main accounts that have with all given related purposes
	private String[] relatedPurposesAllApplicable;

	// Custom search filter - validates relationship is active on given date
	private Date mainPurposeActiveOnDate;

	// Custom search filter - validates relationship is active on given date
	private Date relatedPurposeActiveOnDate;

	@SearchField(searchField = "relatedRelationshipList.referenceTwo.issuingCompany.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer relatedAccountIssuingCompanyId;

	@SearchField(searchField = "relatedRelationshipList.referenceTwo.issuingCompany.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer[] relatedAccountIssuingCompanyIds;

	// Custom search filters - restrict to account types that allow the following relationship mappings
	private Short mappingMainAccountTypeId;
	private Short mappingPurposeId;
	/**
	 * Used to get a list of a accounts that have a relationship to a client, either directly or through a parent
	 */
	private Integer issuingCompanyIdOrRelatedIssuingCompanyId;

	////////////////////////////////////////////////////////////////////////////

	// Custom Search Filter
	private Integer investmentAccountGroupId;

	// Custom Search Filter
	private Integer excludeInvestmentAccountGroupId;

	////////////////////////////////////////////////////////////////////////////

	// Custom Search Filter
	//This is used to include accounts in groups for collateral balance rebuild when the account is of a type
	// that is not mapped to by the CollateralTypes (e.g. an account group that has an OTC_ISDA and a Custodian account.
	// Only the OTC_ISDA account is found when searching by collateralType.holdingInvestmentAccountTypeID.
	private String investmentAccountGroupTypeName;

	////////////////////////////////////////////////////////////////////////////

	// Custom Search Filter
	// This field is used to filter on our account (Client Accounts).  A true value filters the results
	// to accounts that are of type 'Client' and a false value omits such accounts.  If the value is NULL,
	//
	private Boolean ourAccount;

	// Custom Search Filter
	// Field to filter DTC Participant accounts. Holding Accounts will have Issuing Company with DTC Number. Client Accounts will have a relationship to a Holding Account with an Issuing Company with DTC Number.
	private Boolean dtcParticipant;

	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	///////      System Column Dated Value Aware Search Form Fields      ///////
	////////////////////////////////////////////////////////////////////////////

	// Custom Search Fields: See {@link SystemColumnDatedValueAwareSearchFormUtils}
	private String systemColumnDatedValueColumnName;
	private String systemColumnDatedValue;
	private Boolean datedValueEquals;
	private Boolean datedValueActive;
	private Date datedValueActiveOnDate;


	@Override
	public String getSystemColumnDatedValueTableName() {
		return InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Integer[] getExcludeIds() {
		return this.excludeIds;
	}


	public void setExcludeIds(Integer[] excludeIds) {
		this.excludeIds = excludeIds;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getShortName() {
		return this.shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public String getNumber() {
		return this.number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public String getNumberEquals() {
		return this.numberEquals;
	}


	public void setNumberEquals(String numberEquals) {
		this.numberEquals = numberEquals;
	}


	public String getNumber2() {
		return this.number2;
	}


	public void setNumber2(String number2) {
		this.number2 = number2;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Integer getClientOrClientRelationshipCompanyId() {
		return this.clientOrClientRelationshipCompanyId;
	}


	public void setClientOrClientRelationshipCompanyId(Integer clientOrClientRelationshipCompanyId) {
		this.clientOrClientRelationshipCompanyId = clientOrClientRelationshipCompanyId;
	}


	public Integer getClientIdOrRelatedClient() {
		return this.clientIdOrRelatedClient;
	}


	public void setClientIdOrRelatedClient(Integer clientIdOrRelatedClient) {
		this.clientIdOrRelatedClient = clientIdOrRelatedClient;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public Date getClientInceptionDate() {
		return this.clientInceptionDate;
	}


	public void setClientInceptionDate(Date clientInceptionDate) {
		this.clientInceptionDate = clientInceptionDate;
	}


	public BusinessClientCategoryTypes getExcludeClientCategoryType() {
		return this.excludeClientCategoryType;
	}


	public void setExcludeClientCategoryType(BusinessClientCategoryTypes excludeClientCategoryType) {
		this.excludeClientCategoryType = excludeClientCategoryType;
	}


	public BusinessClientCategoryTypes[] getClientCategoryTypes() {
		return this.clientCategoryTypes;
	}


	public void setClientCategoryTypes(BusinessClientCategoryTypes[] clientCategoryTypes) {
		this.clientCategoryTypes = clientCategoryTypes;
	}


	public Short[] getClientCategoryIds() {
		return this.clientCategoryIds;
	}


	public void setClientCategoryIds(Short[] clientCategoryIds) {
		this.clientCategoryIds = clientCategoryIds;
	}


	public Integer getClientRelationshipId() {
		return this.clientRelationshipId;
	}


	public void setClientRelationshipId(Integer clientRelationshipId) {
		this.clientRelationshipId = clientRelationshipId;
	}


	public Date getClientRelationshipInceptionDate() {
		return this.clientRelationshipInceptionDate;
	}


	public void setClientRelationshipInceptionDate(Date clientRelationshipInceptionDate) {
		this.clientRelationshipInceptionDate = clientRelationshipInceptionDate;
	}


	public Short getClientRelationshipCategoryId() {
		return this.clientRelationshipCategoryId;
	}


	public void setClientRelationshipCategoryId(Short clientRelationshipCategoryId) {
		this.clientRelationshipCategoryId = clientRelationshipCategoryId;
	}


	public String getClientRelationshipCategoryName() {
		return this.clientRelationshipCategoryName;
	}


	public void setClientRelationshipCategoryName(String clientRelationshipCategoryName) {
		this.clientRelationshipCategoryName = clientRelationshipCategoryName;
	}


	public Boolean getClientRelationshipCategoryRetail() {
		return this.clientRelationshipCategoryRetail;
	}


	public void setClientRelationshipCategoryRetail(Boolean clientRelationshipCategoryRetail) {
		this.clientRelationshipCategoryRetail = clientRelationshipCategoryRetail;
	}


	public Boolean getClientTaxable() {
		return this.clientTaxable;
	}


	public void setClientTaxable(Boolean clientTaxable) {
		this.clientTaxable = clientTaxable;
	}


	public String getAccountType() {
		return this.accountType;
	}


	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	public String[] getAccountTypes() {
		return this.accountTypes;
	}


	public void setAccountTypes(String[] accountTypes) {
		this.accountTypes = accountTypes;
	}


	public String getExcludeAccountType() {
		return this.excludeAccountType;
	}


	public void setExcludeAccountType(String excludeAccountType) {
		this.excludeAccountType = excludeAccountType;
	}


	public Short getAccountTypeId() {
		return this.accountTypeId;
	}


	public void setAccountTypeId(Short accountTypeId) {
		this.accountTypeId = accountTypeId;
	}


	public Short[] getAccountTypeIds() {
		return this.accountTypeIds;
	}


	public void setAccountTypeIds(Short[] accountTypeIds) {
		this.accountTypeIds = accountTypeIds;
	}


	public Integer getBusinessContractId() {
		return this.businessContractId;
	}


	public void setBusinessContractId(Integer businessContractId) {
		this.businessContractId = businessContractId;
	}


	public Boolean getContractRequired() {
		return this.contractRequired;
	}


	public void setContractRequired(Boolean contractRequired) {
		this.contractRequired = contractRequired;
	}


	public Boolean getExecutionWithIssuerRequired() {
		return this.executionWithIssuerRequired;
	}


	public void setExecutionWithIssuerRequired(Boolean executionWithIssuerRequired) {
		this.executionWithIssuerRequired = executionWithIssuerRequired;
	}


	public Boolean getBusinessContractPopulated() {
		return this.businessContractPopulated;
	}


	public void setBusinessContractPopulated(Boolean businessContractPopulated) {
		this.businessContractPopulated = businessContractPopulated;
	}


	public String getContractCategory() {
		return this.contractCategory;
	}


	public void setContractCategory(String contractCategory) {
		this.contractCategory = contractCategory;
	}


	public String getContractType() {
		return this.contractType;
	}


	public void setContractType(String contractType) {
		this.contractType = contractType;
	}


	public Boolean getClientDirected() {
		return this.clientDirected;
	}


	public void setClientDirected(Boolean clientDirected) {
		this.clientDirected = clientDirected;
	}


	public Boolean getUsedForSpeculation() {
		return this.usedForSpeculation;
	}


	public void setUsedForSpeculation(Boolean usedForSpeculation) {
		this.usedForSpeculation = usedForSpeculation;
	}


	public BigDecimal getInitialMarginMultiplier() {
		return this.initialMarginMultiplier;
	}


	public void setInitialMarginMultiplier(BigDecimal initialMarginMultiplier) {
		this.initialMarginMultiplier = initialMarginMultiplier;
	}


	public Integer getCompanyPlatformId() {
		return this.companyPlatformId;
	}


	public void setCompanyPlatformId(Integer companyPlatformId) {
		this.companyPlatformId = companyPlatformId;
	}


	public Boolean getWrapAccount() {
		return this.wrapAccount;
	}


	public void setWrapAccount(Boolean wrapAccount) {
		this.wrapAccount = wrapAccount;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public Short[] getBusinessServiceIds() {
		return this.businessServiceIds;
	}


	public void setBusinessServiceIds(Short[] businessServiceIds) {
		this.businessServiceIds = businessServiceIds;
	}


	public Short getExcludeBusinessServiceId() {
		return this.excludeBusinessServiceId;
	}


	public void setExcludeBusinessServiceId(Short excludeBusinessServiceId) {
		this.excludeBusinessServiceId = excludeBusinessServiceId;
	}


	public Short[] getExcludeBusinessServiceIds() {
		return this.excludeBusinessServiceIds;
	}


	public void setExcludeBusinessServiceIds(Short[] excludeBusinessServiceIds) {
		this.excludeBusinessServiceIds = excludeBusinessServiceIds;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public Short[] getBusinessServiceOrParentIds() {
		return this.businessServiceOrParentIds;
	}


	public void setBusinessServiceOrParentIds(Short[] businessServiceOrParentIds) {
		this.businessServiceOrParentIds = businessServiceOrParentIds;
	}


	public Short getBusinessServiceComponentId() {
		return this.businessServiceComponentId;
	}


	public void setBusinessServiceComponentId(Short businessServiceComponentId) {
		this.businessServiceComponentId = businessServiceComponentId;
	}


	public AccountingClosingMethods getAccountingClosingMethodOverride() {
		return this.accountingClosingMethodOverride;
	}


	public void setAccountingClosingMethodOverride(AccountingClosingMethods accountingClosingMethodOverride) {
		this.accountingClosingMethodOverride = accountingClosingMethodOverride;
	}


	public Integer getAumCalculatorBeanId() {
		return this.aumCalculatorBeanId;
	}


	public void setAumCalculatorBeanId(Integer aumCalculatorBeanId) {
		this.aumCalculatorBeanId = aumCalculatorBeanId;
	}


	public String getClientAccountType() {
		return this.clientAccountType;
	}


	public void setClientAccountType(String clientAccountType) {
		this.clientAccountType = clientAccountType;
	}


	public String getDiscretionType() {
		return this.discretionType;
	}


	public void setDiscretionType(String discretionType) {
		this.discretionType = discretionType;
	}


	public String getDiscretionTypeEqual() {
		return this.discretionTypeEqual;
	}


	public void setDiscretionTypeEqual(String discretionTypeEqual) {
		this.discretionTypeEqual = discretionTypeEqual;
	}


	public Boolean getNullDiscretionType() {
		return this.nullDiscretionType;
	}


	public void setNullDiscretionType(Boolean nullDiscretionType) {
		this.nullDiscretionType = nullDiscretionType;
	}


	public Integer getClientAccountChannelId() {
		return this.clientAccountChannelId;
	}


	public void setClientAccountChannelId(Integer clientAccountChannelId) {
		this.clientAccountChannelId = clientAccountChannelId;
	}


	public String getClientAccountChannelName() {
		return this.clientAccountChannelName;
	}


	public void setClientAccountChannelName(String clientAccountChannelName) {
		this.clientAccountChannelName = clientAccountChannelName;
	}


	public String getExternalPortfolioCodeName() {
		return this.externalPortfolioCodeName;
	}


	public void setExternalPortfolioCodeName(String externalPortfolioCodeName) {
		this.externalPortfolioCodeName = externalPortfolioCodeName;
	}


	public String getExternalProductCodeName() {
		return this.externalProductCodeName;
	}


	public void setExternalProductCodeName(String externalProductCodeName) {
		this.externalProductCodeName = externalProductCodeName;
	}


	public ServiceProcessingTypes getServiceProcessingType() {
		return this.serviceProcessingType;
	}


	public void setServiceProcessingType(ServiceProcessingTypes serviceProcessingType) {
		this.serviceProcessingType = serviceProcessingType;
	}


	public ServiceProcessingTypes[] getServiceProcessingTypes() {
		return this.serviceProcessingTypes;
	}


	public void setServiceProcessingTypes(ServiceProcessingTypes[] serviceProcessingTypes) {
		this.serviceProcessingTypes = serviceProcessingTypes;
	}


	public Short getBusinessServiceProcessingTypeId() {
		return this.businessServiceProcessingTypeId;
	}


	public void setBusinessServiceProcessingTypeId(Short businessServiceProcessingTypeId) {
		this.businessServiceProcessingTypeId = businessServiceProcessingTypeId;
	}


	public Short[] getBusinessServiceProcessingTypeIds() {
		return this.businessServiceProcessingTypeIds;
	}


	public void setBusinessServiceProcessingTypeIds(Short[] businessServiceProcessingTypeIds) {
		this.businessServiceProcessingTypeIds = businessServiceProcessingTypeIds;
	}


	public String getBusinessServiceProcessingTypeName() {
		return this.businessServiceProcessingTypeName;
	}


	public void setBusinessServiceProcessingTypeName(String businessServiceProcessingTypeName) {
		this.businessServiceProcessingTypeName = businessServiceProcessingTypeName;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public Integer getDefaultExchangeRateSourceCompanyId() {
		return this.defaultExchangeRateSourceCompanyId;
	}


	public void setDefaultExchangeRateSourceCompanyId(Integer defaultExchangeRateSourceCompanyId) {
		this.defaultExchangeRateSourceCompanyId = defaultExchangeRateSourceCompanyId;
	}


	public String getFederalTaxNumber() {
		return this.federalTaxNumber;
	}


	public void setFederalTaxNumber(String federalTaxNumber) {
		this.federalTaxNumber = federalTaxNumber;
	}


	public String getStateTaxNumber() {
		return this.stateTaxNumber;
	}


	public void setStateTaxNumber(String stateTaxNumber) {
		this.stateTaxNumber = stateTaxNumber;
	}


	public Integer getIssuingCompanyId() {
		return this.issuingCompanyId;
	}


	public void setIssuingCompanyId(Integer issuingCompanyId) {
		this.issuingCompanyId = issuingCompanyId;
	}


	public Integer[] getIssuingCompanyIds() {
		return this.issuingCompanyIds;
	}


	public void setIssuingCompanyIds(Integer[] issuingCompanyIds) {
		this.issuingCompanyIds = issuingCompanyIds;
	}


	public String getIssuingCompanyName() {
		return this.issuingCompanyName;
	}


	public void setIssuingCompanyName(String issuingCompanyName) {
		this.issuingCompanyName = issuingCompanyName;
	}


	public String getIssuingCompanyTypeName() {
		return this.issuingCompanyTypeName;
	}


	public void setIssuingCompanyTypeName(String issuingCompanyTypeName) {
		this.issuingCompanyTypeName = issuingCompanyTypeName;
	}


	public Date getWorkflowStateEffectiveStartDate() {
		return this.workflowStateEffectiveStartDate;
	}


	public void setWorkflowStateEffectiveStartDate(Date workflowStateEffectiveStartDate) {
		this.workflowStateEffectiveStartDate = workflowStateEffectiveStartDate;
	}


	public Date getInceptionDate() {
		return this.inceptionDate;
	}


	public void setInceptionDate(Date inceptionDate) {
		this.inceptionDate = inceptionDate;
	}


	public Boolean getInceptionDatePopulated() {
		return this.inceptionDatePopulated;
	}


	public void setInceptionDatePopulated(Boolean inceptionDatePopulated) {
		this.inceptionDatePopulated = inceptionDatePopulated;
	}


	public Date getPerformanceInceptionDate() {
		return this.performanceInceptionDate;
	}


	public void setPerformanceInceptionDate(Date performanceInceptionDate) {
		this.performanceInceptionDate = performanceInceptionDate;
	}


	public Short getAssetClassId() {
		return this.assetClassId;
	}


	public void setAssetClassId(Short assetClassId) {
		this.assetClassId = assetClassId;
	}


	public Integer getMainAccountId() {
		return this.mainAccountId;
	}


	public void setMainAccountId(Integer mainAccountId) {
		this.mainAccountId = mainAccountId;
	}


	public Integer getRelatedAccountId() {
		return this.relatedAccountId;
	}


	public void setRelatedAccountId(Integer relatedAccountId) {
		this.relatedAccountId = relatedAccountId;
	}


	public String getMainPurpose() {
		return this.mainPurpose;
	}


	public void setMainPurpose(String mainPurpose) {
		this.mainPurpose = mainPurpose;
	}


	public Short getMainPurposeId() {
		return this.mainPurposeId;
	}


	public void setMainPurposeId(Short mainPurposeId) {
		this.mainPurposeId = mainPurposeId;
	}


	public Integer getMainPurposeInvestmentAccountGroupId() {
		return this.mainPurposeInvestmentAccountGroupId;
	}


	public void setMainPurposeInvestmentAccountGroupId(Integer mainPurposeInvestmentAccountGroupId) {
		this.mainPurposeInvestmentAccountGroupId = mainPurposeInvestmentAccountGroupId;
	}


	public String getRelatedPurpose() {
		return this.relatedPurpose;
	}


	public void setRelatedPurpose(String relatedPurpose) {
		this.relatedPurpose = relatedPurpose;
	}


	public String[] getRelatedPurposes() {
		return this.relatedPurposes;
	}


	public void setRelatedPurposes(String[] relatedPurposes) {
		this.relatedPurposes = relatedPurposes;
	}


	public String[] getRelatedPurposesAllApplicable() {
		return this.relatedPurposesAllApplicable;
	}


	public void setRelatedPurposesAllApplicable(String[] relatedPurposesAllApplicable) {
		this.relatedPurposesAllApplicable = relatedPurposesAllApplicable;
	}


	public Date getMainPurposeActiveOnDate() {
		return this.mainPurposeActiveOnDate;
	}


	public void setMainPurposeActiveOnDate(Date mainPurposeActiveOnDate) {
		this.mainPurposeActiveOnDate = mainPurposeActiveOnDate;
	}


	public Date getRelatedPurposeActiveOnDate() {
		return this.relatedPurposeActiveOnDate;
	}


	public void setRelatedPurposeActiveOnDate(Date relatedPurposeActiveOnDate) {
		this.relatedPurposeActiveOnDate = relatedPurposeActiveOnDate;
	}


	public Integer getRelatedAccountIssuingCompanyId() {
		return this.relatedAccountIssuingCompanyId;
	}


	public void setRelatedAccountIssuingCompanyId(Integer relatedAccountIssuingCompanyId) {
		this.relatedAccountIssuingCompanyId = relatedAccountIssuingCompanyId;
	}


	public Integer[] getRelatedAccountIssuingCompanyIds() {
		return this.relatedAccountIssuingCompanyIds;
	}


	public void setRelatedAccountIssuingCompanyIds(Integer[] relatedAccountIssuingCompanyIds) {
		this.relatedAccountIssuingCompanyIds = relatedAccountIssuingCompanyIds;
	}


	public Short getMappingMainAccountTypeId() {
		return this.mappingMainAccountTypeId;
	}


	public void setMappingMainAccountTypeId(Short mappingMainAccountTypeId) {
		this.mappingMainAccountTypeId = mappingMainAccountTypeId;
	}


	public Short getMappingPurposeId() {
		return this.mappingPurposeId;
	}


	public void setMappingPurposeId(Short mappingPurposeId) {
		this.mappingPurposeId = mappingPurposeId;
	}


	public Integer getIssuingCompanyIdOrRelatedIssuingCompanyId() {
		return this.issuingCompanyIdOrRelatedIssuingCompanyId;
	}


	public void setIssuingCompanyIdOrRelatedIssuingCompanyId(Integer issuingCompanyIdOrRelatedIssuingCompanyId) {
		this.issuingCompanyIdOrRelatedIssuingCompanyId = issuingCompanyIdOrRelatedIssuingCompanyId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Integer getExcludeInvestmentAccountGroupId() {
		return this.excludeInvestmentAccountGroupId;
	}


	public void setExcludeInvestmentAccountGroupId(Integer excludeInvestmentAccountGroupId) {
		this.excludeInvestmentAccountGroupId = excludeInvestmentAccountGroupId;
	}


	public String getInvestmentAccountGroupTypeName() {
		return this.investmentAccountGroupTypeName;
	}


	public void setInvestmentAccountGroupTypeName(String investmentAccountGroupTypeName) {
		this.investmentAccountGroupTypeName = investmentAccountGroupTypeName;
	}


	public Boolean getOurAccount() {
		return this.ourAccount;
	}


	public void setOurAccount(Boolean ourAccount) {
		this.ourAccount = ourAccount;
	}


	public Boolean getDtcParticipant() {
		return this.dtcParticipant;
	}


	public void setDtcParticipant(Boolean dtcParticipant) {
		this.dtcParticipant = dtcParticipant;
	}


	@Override
	public String getSystemColumnDatedValueColumnName() {
		return this.systemColumnDatedValueColumnName;
	}


	public void setSystemColumnDatedValueColumnName(String systemColumnDatedValueColumnName) {
		this.systemColumnDatedValueColumnName = systemColumnDatedValueColumnName;
	}


	@Override
	public String getSystemColumnDatedValue() {
		return this.systemColumnDatedValue;
	}


	public void setSystemColumnDatedValue(String systemColumnDatedValue) {
		this.systemColumnDatedValue = systemColumnDatedValue;
	}


	@Override
	public Boolean getDatedValueEquals() {
		return this.datedValueEquals;
	}


	public void setDatedValueEquals(Boolean datedValueEquals) {
		this.datedValueEquals = datedValueEquals;
	}


	@Override
	public Boolean getDatedValueActive() {
		return this.datedValueActive;
	}


	public void setDatedValueActive(Boolean datedValueActive) {
		this.datedValueActive = datedValueActive;
	}


	@Override
	public Date getDatedValueActiveOnDate() {
		return this.datedValueActiveOnDate;
	}


	public void setDatedValueActiveOnDate(Date datedValueActiveOnDate) {
		this.datedValueActiveOnDate = datedValueActiveOnDate;
	}
}
