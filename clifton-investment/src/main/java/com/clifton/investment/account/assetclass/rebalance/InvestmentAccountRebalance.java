package com.clifton.investment.account.assetclass.rebalance;


import com.clifton.core.beans.BaseEntityWithNaturalKey;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.system.bean.SystemBean;

import java.math.BigDecimal;


/**
 * The <code>InvestmentAccountRebalance</code> defines the account setup for {@link InvestmentAccount}
 * objects that utilize the rebalancing feature (currently used in PIOS runs)
 * <p/>
 * Similar to other areas (manager balances) the config and actual results are stored in investment project,
 * however the processing is done in product project.
 * <p/>
 * There is not a Primary Key that is generated automatically, but instead uses a Natural Key,
 * using the InvestmentAccountID as it's natural key.  This ensures 1:1 relationship between setup and the account
 * and also we don't have to have UX, FK, etc.
 *
 * @author manderson
 */
public class InvestmentAccountRebalance extends BaseEntityWithNaturalKey<Integer> {

	/**
	 * Used for Clifton Accounts during rebalancing.  Defines the threshold for rebalancing cash amounts.
	 * If sum of asset class rebalance cash amounts deviates from this range a mini-rebalance is triggered.
	 */
	private BigDecimal minAllowedCash;
	private BigDecimal maxAllowedCash;

	private RebalanceCalculationTypes calculationType;

	/**
	 * Can only be set when min and/or max cash is set and if true,
	 * then during PIOS runs the account will automatically be rebalanced
	 * if those threshold(s) are crossed.
	 */
	private boolean automatic;

	/**
	 * Effective Cash Exposure (Effective Cash - Overlay Exposure) Bands options used to
	 * determine if Trading or not.  If within the bands, all Overlay Targets = Overlay Exposure
	 */
	private BigDecimal minEffectiveCashExposure;
	private BigDecimal maxEffectiveCashExposure;

	private SystemBean minimizeImbalancesCalculatorBean;

	/**
	 * Applies adjustment to total cash available for minimizing imbalances
	 * i.e. apply a cash buffer, so reduce by 3mm
	 */
	private BigDecimal minimizeImbalancesAdjustmentAmount;

	/**
	 * In addition to a static adjustment, the system can automatically adjust overlay
	 * target based on the "Cash" (i.e. IsMainCash = true) asset class adjusted target.
	 */
	private boolean minimizeImbalancesAdjustForCashTarget;


	//////////////////////////////////////////////
	//////////////////////////////////////////////


	// Used only for UI field-set checkbox on whether to expand the section
	public boolean isRebalancingUsed() {
		if (getCalculationType() != null) {
			return true;
		}
		return getMinAllowedCash() != null || getMaxAllowedCash() != null;
	}


	// Used only for UI field-set checkbox on whether to expand the section
	public boolean isMinimizeImbalancesUsed() {
		if (getMinimizeImbalancesCalculatorBean() != null) {
			return true;
		}
		return getMinEffectiveCashExposure() != null || getMaxEffectiveCashExposure() != null;
	}


	//////////////////////////////////////////////
	//////////////////////////////////////////////


	public BigDecimal getMinAllowedCash() {
		return this.minAllowedCash;
	}


	public void setMinAllowedCash(BigDecimal minAllowedCash) {
		this.minAllowedCash = minAllowedCash;
	}


	public BigDecimal getMaxAllowedCash() {
		return this.maxAllowedCash;
	}


	public void setMaxAllowedCash(BigDecimal maxAllowedCash) {
		this.maxAllowedCash = maxAllowedCash;
	}


	public RebalanceCalculationTypes getCalculationType() {
		return this.calculationType;
	}


	public void setCalculationType(RebalanceCalculationTypes calculationType) {
		this.calculationType = calculationType;
	}


	public boolean isAutomatic() {
		return this.automatic;
	}


	public void setAutomatic(boolean automatic) {
		this.automatic = automatic;
	}


	public BigDecimal getMinEffectiveCashExposure() {
		return this.minEffectiveCashExposure;
	}


	public void setMinEffectiveCashExposure(BigDecimal minEffectiveCashExposure) {
		this.minEffectiveCashExposure = minEffectiveCashExposure;
	}


	public BigDecimal getMaxEffectiveCashExposure() {
		return this.maxEffectiveCashExposure;
	}


	public void setMaxEffectiveCashExposure(BigDecimal maxEffectiveCashExposure) {
		this.maxEffectiveCashExposure = maxEffectiveCashExposure;
	}


	public BigDecimal getMinimizeImbalancesAdjustmentAmount() {
		return this.minimizeImbalancesAdjustmentAmount;
	}


	public void setMinimizeImbalancesAdjustmentAmount(BigDecimal minimizeImbalancesAdjustmentAmount) {
		this.minimizeImbalancesAdjustmentAmount = minimizeImbalancesAdjustmentAmount;
	}


	public boolean isMinimizeImbalancesAdjustForCashTarget() {
		return this.minimizeImbalancesAdjustForCashTarget;
	}


	public void setMinimizeImbalancesAdjustForCashTarget(boolean minimizeImbalancesAdjustForCashTarget) {
		this.minimizeImbalancesAdjustForCashTarget = minimizeImbalancesAdjustForCashTarget;
	}


	public SystemBean getMinimizeImbalancesCalculatorBean() {
		return this.minimizeImbalancesCalculatorBean;
	}


	public void setMinimizeImbalancesCalculatorBean(SystemBean minimizeImbalancesCalculatorBean) {
		this.minimizeImbalancesCalculatorBean = minimizeImbalancesCalculatorBean;
	}
}
