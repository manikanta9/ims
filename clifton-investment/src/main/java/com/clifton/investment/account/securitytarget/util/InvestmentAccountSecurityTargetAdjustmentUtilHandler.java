package com.clifton.investment.account.securitytarget.util;

import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;

import java.math.BigDecimal;


/**
 * A utility class to adjust the {@link InvestmentAccountSecurityTarget} TargetUnderlyingQuantity or TargetUnderlyingNotional and save the
 * updated security target.
 *
 * @author NickK
 */
public interface InvestmentAccountSecurityTargetAdjustmentUtilHandler {


	///////////////////////////////////////////////////////////////////////////
	///////////         Target Adjustment Business Methods         ////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTarget adjustInvestmentAccountSecurityTargetQuantity(InvestmentAccountSecurityTarget securityTarget, BigDecimal quantity);


	public InvestmentAccountSecurityTarget adjustInvestmentAccountSecurityTargetNotionalFromQuantity(InvestmentAccountSecurityTarget securityTarget, BigDecimal quantity, BigDecimal averagePrice);


	public InvestmentAccountSecurityTarget adjustInvestmentAccountSecurityTargetNotional(InvestmentAccountSecurityTarget securityTarget, BigDecimal notional);


	///////////////////////////////////////////////////////////////////////////
	///////////         Target Setting Business Methods         ///////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTarget setInvestmentAccountSecurityTargetQuantity(InvestmentAccountSecurityTarget securityTarget, BigDecimal quantity);


	public InvestmentAccountSecurityTarget setInvestmentAccountSecurityTargetNotional(InvestmentAccountSecurityTarget securityTarget, BigDecimal notional);
}
