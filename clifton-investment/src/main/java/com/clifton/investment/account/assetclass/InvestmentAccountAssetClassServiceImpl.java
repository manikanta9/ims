package com.clifton.investment.account.assetclass;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass.ClientDirectedCashOptions;
import com.clifton.investment.account.assetclass.search.InvestmentAccountAssetClassBenchmarkSearchForm;
import com.clifton.investment.account.assetclass.search.InvestmentAccountAssetClassSearchForm;
import com.clifton.investment.account.targetexposure.InvestmentAccountTargetExposureHandler;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import com.clifton.investment.replication.history.InvestmentReplicationHistory;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildService;
import org.hibernate.Criteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountAssetClassServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class InvestmentAccountAssetClassServiceImpl implements InvestmentAccountAssetClassService {

	private AdvancedUpdatableDAO<InvestmentAccountAssetClass, Criteria> investmentAccountAssetClassDAO;
	private AdvancedUpdatableDAO<InvestmentAccountAssetClassAssignment, Criteria> investmentAccountAssetClassAssignmentDAO;
	private AdvancedUpdatableDAO<InvestmentAccountAssetClassBenchmark, Criteria> investmentAccountAssetClassBenchmarkDAO;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentReplicationService investmentReplicationService;
	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;
	private InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> investmentAccountTargetExposureHandler;

	////////////////////////////////////////////////////////////////////////////
	//////          Investment Account Asset Class Methods               ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountAssetClass getInvestmentAccountAssetClass(int id) {
		InvestmentAccountAssetClass bean = getInvestmentAccountAssetClassDAO().findByPrimaryKey(id);
		if (bean != null && (bean.isTargetExposureAdjustmentUsed() || bean.isCashAdjustmentUsed())) {
			// Looking up by parent will populate both adjustment types at once. Calculated getters can be used to get each list separately.
			List<InvestmentAccountAssetClassAssignment> assetClassAssignmentList = getInvestmentAccountAssetClassAssignmentListByParent(bean.getId());
			bean.setTargetExposureAssignmentCombinedList(assetClassAssignmentList);
		}
		return bean;
	}


	private InvestmentAccountAssetClass getInvestmentAccountAssetClassByAccountAndAssetClass(int accountId, short assetClassId) {
		return getInvestmentAccountAssetClassDAO().findOneByFields(new String[]{"account.id", "assetClass.id"}, new Object[]{accountId, assetClassId});
	}


	@Override
	public List<InvestmentAccountAssetClass> getInvestmentAccountAssetClassListByAccount(int accountId) {
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();
		searchForm.setAccountId(accountId);
		return getInvestmentAccountAssetClassList(searchForm);
	}


	@Override
	public List<InvestmentAccountAssetClass> getInvestmentAccountAssetClassList(final InvestmentAccountAssetClassSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getTargetExposureAdjustmentUsed() != null) {
					if (searchForm.getTargetExposureAdjustmentUsed()) {
						criteria.add(Restrictions.isNotNull("targetExposureAdjustment"));
					}
					else {
						criteria.add(Restrictions.isNull("targetExposureAdjustment"));
					}
				}

				if (searchForm.getCashAdjustmentUsed() != null) {
					if (searchForm.getCashAdjustmentUsed()) {
						criteria.add(Restrictions.isNotNull("cashAdjustment"));
					}
					else {
						criteria.add(Restrictions.isNull("cashAdjustment"));
					}
				}

				if (searchForm.getClientDirectedCashUsed() != null) {
					if (searchForm.getClientDirectedCashUsed()) {
						criteria.add(Restrictions.ne("clientDirectedCashOption", ClientDirectedCashOptions.NONE));
					}
					else {
						criteria.add(Restrictions.eq("clientDirectedCashOption", ClientDirectedCashOptions.NONE));
					}
				}
			}
		};
		return getInvestmentAccountAssetClassDAO().findBySearchCriteria(config);
	}


	// See: {@link InvestmentAccountAssetClassObserver} for additional validation/functionality outside of assignments
	@Override
	@Transactional
	public InvestmentAccountAssetClass saveInvestmentAccountAssetClass(InvestmentAccountAssetClass bean) {
		// If a new bean, check if inactive one already exists, if so update ID so new values are populated on existing bean.
		if (bean.isNewBean()) {
			InvestmentAccountAssetClass existing = getInvestmentAccountAssetClassByAccountAndAssetClass(bean.getAccount().getId(), bean.getAssetClass().getId());
			if (existing != null) {
				if (existing.isInactive()) {
					bean.setId(existing.getId());
				}
				else {
					throw new ValidationException("Account [" + bean.getAccount().getLabel() + "] is already tied to asset class [" + bean.getAssetClass().getName()
							+ "].  Asset Class definitions must be unique per account.");
				}
			}
		}

		if (!bean.isRollupAssetClass() && !bean.isExcludeFromExposure()) {
			ValidationUtils.assertNotNull(bean.getPerformanceOverlayTargetSource(), "Performance Overlay Target Source is required.", "performanceOverlayTargetSource");
		}

		// Exclude From Exposure or Exclude From Overlay Exposure
		validateInvestmentAccountAssetClassExcludeFromExposure(bean);

		// Target Exposure Adjustments & Assignments
		validateInvestmentAccountAssetClassAssignmentList(bean);

		// Client Directed Cash Options
		validateInvestmentAccountAssetClassClientDirected(bean);

		// get existing lists so that we know what to delete if necessary
		List<InvestmentAccountAssetClassAssignment> oldAssignments = null;
		if (!bean.isNewBean()) {
			oldAssignments = getInvestmentAccountAssetClassAssignmentListByParent(bean.getId());
		}

		// save the bean and its assignments
		List<InvestmentAccountAssetClassAssignment> targetExposureAssignmentList = bean.getTargetExposureAssignmentCombinedList();
		bean = getInvestmentAccountAssetClassDAO().save(bean);

		// Assignments - Delete if not a Target Exposure or Cash adjustment, otherwise save.
		if (!bean.isTargetExposureAdjustmentUsed() && !bean.isCashAdjustmentUsed() && !CollectionUtils.isEmpty(oldAssignments)) {
			getInvestmentAccountAssetClassAssignmentDAO().deleteList(oldAssignments);
		}
		else {
			getInvestmentAccountAssetClassAssignmentDAO().saveList(targetExposureAssignmentList, oldAssignments);
		}
		bean.setTargetExposureAssignmentCombinedList(targetExposureAssignmentList);
		return bean;
	}


	@Override
	@Transactional
	public void saveInvestmentAccountAssetClassList(List<InvestmentAccountAssetClass> beanList) {
		for (InvestmentAccountAssetClass accountAssetClass : CollectionUtils.getIterable(beanList)) {
			ValidationUtils.assertFalse(accountAssetClass.isNewBean(), "Inserts are not supported for the save of an Investment Account Asset Class list");
			// Make sure we don't lose this list when we save
			accountAssetClass.setTargetExposureAssignmentCombinedList(getInvestmentAccountAssetClassAssignmentListByParent(accountAssetClass.getId()));
			saveInvestmentAccountAssetClass(accountAssetClass);
		}
	}


	/**
	 * Validates/Sets properties for ExcludeFromExposure and ExcludeFromOverlayExposure options
	 */
	private void validateInvestmentAccountAssetClassExcludeFromExposure(InvestmentAccountAssetClass bean) {
		if (bean.isExcludeFromExposure()) {
			if (!bean.isNewBean()) {
				getInvestmentAccountTargetExposureHandler().validateTargetExposureAdjustmentAssignmentUnused(bean, "Asset Class", "excluded from exposure", getInvestmentAccountAssetClassAssignmentDAO());
			}

			// Make sure not a rollup
			if (bean.isRollupAssetClass()) {
				throw new ValidationException("This Asset Class cannot be excluded from exposure, because it is a rollup asset class");
			}

			// Make sure no parent (i.e. not used by a rollup)
			if (bean.getParent() != null) {
				throw new ValidationException("This Asset Class cannot be excluded from exposure, because it is a child of rollup asset class [" + bean.getParent().getAssetClass().getName() + "]");
			}

			// Clear/Reset Fields
			bean.setAssetClassPercentage(BigDecimal.ZERO);
			bean.setCashPercentage(BigDecimal.ZERO);
			bean.setBenchmarkSecurity(null);
			bean.setBenchmarkDurationSecurity(null);
			bean.setPrimaryReplication(null);
			bean.setSecondaryReplication(null);
			bean.setSecondaryReplicationAmount(null);
			bean.setRebalanceAllocationAbsoluteMax(null);
			bean.setRebalanceAllocationAbsoluteMin(null);
			bean.setRebalanceAllocationFixed(false);
			bean.setRebalanceAllocationMax(null);
			bean.setRebalanceAllocationMin(null);
			bean.setRebalanceBenchmarkSecurity(null);
			bean.setTargetExposureAdjustment(null);
			bean.setCashAdjustment(null);
			bean.setPerformanceOverlayTargetSource(null);
			bean.setClientDirectedCashOption(ClientDirectedCashOptions.NONE);
			bean.setClientDirectedCashAmount(null);
			bean.setClientDirectedCashDate(null);
		}
		else if (bean.isExcludeFromOverlayExposure()) {
			bean.setClientDirectedCashOption(ClientDirectedCashOptions.NONE);
			bean.setClientDirectedCashAmount(null);
			bean.setClientDirectedCashDate(null);
		}
	}


	/**
	 * Validates Target Exposure Adjustment Types & Assignments (Using or Used By)
	 */
	private void validateInvestmentAccountAssetClassAssignmentList(InvestmentAccountAssetClass bean) {
		if (bean.isTargetExposureAdjustmentUsed() || bean.isCashAdjustmentUsed()) {
			if (bean.isRollupAssetClass()) {
				throw new ValidationException("Target Exposure Adjustments are not applicable to Rollup Asset Classes.");
			}

			getInvestmentAccountTargetExposureHandler().validateTargetExposureAdjustmentTypeOptions(bean, "Asset Class", getInvestmentAccountAssetClassAssignmentDAO());
			if (getInvestmentAccountTargetExposureHandler().isTargetExposureAdjustmentAssignmentListUsed(bean.getTargetExposureAdjustment())) {
				for (InvestmentAccountAssetClassAssignment assign : CollectionUtils.getIterable(bean.getTargetExposureAssignmentCombinedList())) {
					if (assign.getReferenceTwo().isRollupAssetClass()) {
						throw new ValidationException("You cannot assign a Target Exposure Asset Class Target to a rollup asset class. [" + assign.getReferenceTwo().getAssetClass().getLabel()
								+ "] is a Rollup Asset Class.");
					}
					if (assign.getReferenceTwo().isExcludeFromExposure()) {
						throw new ValidationException("You cannot assign a Target Exposure Asset Class Target to a Asset Class. [" + assign.getReferenceTwo().getAssetClass().getLabel()
								+ "] because it is excluded from exposure.");
					}
				}
			}
		}
	}


	private void validateInvestmentAccountAssetClassClientDirected(InvestmentAccountAssetClass bean) {
		// Client Directed Cash - Amount and Date are required if not NONE
		if (bean.getClientDirectedCashOption() == null) {
			bean.setClientDirectedCashOption(ClientDirectedCashOptions.NONE);
		}
		if (ClientDirectedCashOptions.TARGET == bean.getClientDirectedCashOption() || ClientDirectedCashOptions.TARGET_PREVIOUS_DURATION == bean.getClientDirectedCashOption()) {
			ValidationUtils.assertNotNull(bean.getClientDirectedCashAmount(), "Client Directed Cash Type of Target requires setting the Overlay Target field value", "clientDirectedCashAmount");
			ValidationUtils.assertNotNull(bean.getClientDirectedCashDate(), "Client Directed Cash Type of Target requires setting the Cash Date field value", "clientDirectedCashDate");
		}
		else if (ClientDirectedCashOptions.CONSTANT == bean.getClientDirectedCashOption()) {
			ValidationUtils.assertNotNull(bean.getClientDirectedCashAmount(), "Client Directed Cash Type of Constant requires setting the Overlay Target Adjustment field value",
					"clientDirectedCashAmount");
			ValidationUtils.assertNotNull(bean.getClientDirectedCashDate(), "Client Directed Cash Type of Constant requires setting the Cash Date field value", "clientDirectedCashDate");
		}
		else {
			bean.setClientDirectedCashAmount(null);
			bean.setClientDirectedCashDate(null);
		}
	}


	@Override
	public void clearInvestmentAccountAssetClassSecondaryAmount() {
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();
		searchForm.setPopulatedSecondaryReplicationAmount(true);
		List<InvestmentAccountAssetClass> list = getInvestmentAccountAssetClassList(searchForm);
		for (InvestmentAccountAssetClass ac : CollectionUtils.getIterable(list)) {
			ac.setSecondaryReplicationAmount(null);
		}
		getInvestmentAccountAssetClassDAO().saveList(list);
	}


	@Override
	@Transactional
	public void deleteInvestmentAccountAssetClass(int id) {
		InvestmentAccountAssetClass bean = getInvestmentAccountAssetClass(id);

		bean.setInactive(true);
		bean.setAssetClassPercentage(BigDecimal.ZERO);
		bean.setCashPercentage(BigDecimal.ZERO);
		bean.setTargetExposureAdjustment(null);
		saveInvestmentAccountAssetClass(bean);

		if (bean.isRollupAssetClass()) {
			List<InvestmentAccountAssetClass> children = getInvestmentAccountAssetClassDAO().findByField("parent.id", id);
			for (InvestmentAccountAssetClass child : CollectionUtils.getIterable(children)) {
				child.setParent(null);
				// Only updating parent so don't call service method otherwise need to retrieve all lists and pass them when saving
				getInvestmentAccountAssetClassDAO().save(child);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////        Investment Account Asset Class Rollup Methods            //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void linkInvestmentAccountAssetClassRollup(int parentId, int childId) {
		InvestmentAccountAssetClass parent = getInvestmentAccountAssetClass(parentId);
		InvestmentAccountAssetClass child = getInvestmentAccountAssetClass(childId);

		// Make sure no parent (i.e. not used by a rollup)
		if (child.isExcludeFromExposure()) {
			throw new ValidationException("Asset Class [" + child.getAssetClass().getLabel() + "] cannot be a child of a rollup asset class because it is excluded from exposure.");
		}

		child.setParent(parent);

		saveInvestmentAccountAssetClass(child);
	}


	@Override
	public void deleteInvestmentAccountAssetClassRollup(int id) {
		InvestmentAccountAssetClass child = getInvestmentAccountAssetClass(id);
		child.setParent(null);
		saveInvestmentAccountAssetClass(child);
	}

	////////////////////////////////////////////////////////////////////////////
	/////       Investment Account Asset Class Assignment Methods          /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentAccountAssetClassAssignment> getInvestmentAccountAssetClassAssignmentListByParent(int parentId) {
		return getInvestmentAccountAssetClassAssignmentDAO().findByField("referenceOne.id", parentId);
	}

	////////////////////////////////////////////////////////////////////////////
	/////     Investment Account Asset Class Benchmark Business Methods    /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountAssetClassBenchmark getInvestmentAccountAssetClassBenchmark(int id) {
		return getInvestmentAccountAssetClassBenchmarkDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountAssetClassBenchmark> getInvestmentAccountAssetClassBenchmarkList(final InvestmentAccountAssetClassBenchmarkSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveOnDate() != null) {
					Date startDate = searchForm.getActiveOnDate();
					Date endDate = searchForm.getActiveOnDate();
					if (searchForm.isActiveMonthToDate()) {
						startDate = DateUtils.getFirstDayOfMonth(searchForm.getActiveOnDate());
					}
					// Range check - startDate < last day of month or endDate before
					// this_.StartDate is null or this_.StartDate<=MTD Date
					// and this_.EndDate is null or this_.EndDate>=First Day of Month
					LogicalExpression startDateFilter = Restrictions.or(Restrictions.isNull("startDate"), Restrictions.le("startDate", endDate));
					LogicalExpression endDateFilter = Restrictions.or(Restrictions.isNull("endDate"), Restrictions.ge("endDate", startDate));
					criteria.add(startDateFilter);
					criteria.add(endDateFilter);
				}
			}
		};
		return getInvestmentAccountAssetClassBenchmarkDAO().findBySearchCriteria(config);
	}


	@Override
	public List<InvestmentAccountAssetClassBenchmark> getInvestmentAccountAssetClassBenchmarkListActiveMonthToDate(int assetClassId, Date date) {
		InvestmentAccountAssetClassBenchmarkSearchForm searchForm = new InvestmentAccountAssetClassBenchmarkSearchForm();
		searchForm.setReferenceOneId(assetClassId);
		searchForm.setActiveOnDate(date);
		searchForm.setActiveMonthToDate(true);
		return getInvestmentAccountAssetClassBenchmarkList(searchForm);
	}


	@Override
	public InvestmentAccountAssetClassBenchmark saveInvestmentAccountAssetClassBenchmark(InvestmentAccountAssetClassBenchmark bean) {
		return getInvestmentAccountAssetClassBenchmarkDAO().save(bean);
	}


	@Override
	public void deleteInvestmentAccountAssetClassBenchmark(int id) {
		getInvestmentAccountAssetClassBenchmarkDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	/////        Investment Account Asset Class Replication Methods        /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentReplication> getInvestmentReplicationListForAccountAssetClass(int accountAssetClassId) {
		InvestmentAccountAssetClass ac = getInvestmentAccountAssetClass(accountAssetClassId);
		List<InvestmentReplication> repList = new ArrayList<>();
		addReplicationAndMatchingToList(ac.getPrimaryReplication(), repList);
		addReplicationAndMatchingToList(ac.getSecondaryReplication(), repList);
		return repList;
	}


	/**
	 * If replication is not null, will add the replication and all matching replications for that replication to the list - if already in the list, skips it as it's matching reps will have already been included
	 */
	private void addReplicationAndMatchingToList(InvestmentReplication replication, List<InvestmentReplication> repList) {
		if (replication != null && !repList.contains(replication)) {
			repList.add(replication);
			// Pull Replication with allocations populated
			replication = getInvestmentReplicationService().getInvestmentReplication(replication.getId());

			for (InvestmentReplicationAllocation alloc : CollectionUtils.getIterable(replication.getAllocationList())) {
				if (alloc.getMatchingReplication() != null) {
					addReplicationAndMatchingToList(alloc.getMatchingReplication(), repList);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////     Investment Account Asset Class Security Allowance Methods     ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public InvestmentAccountAssetClassSecurityAllowance getInvestmentAccountAssetClassSecurityAllowance(int clientAccountId, int securityId, Date balanceDate) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);

		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();
		searchForm.setAccountId(clientAccountId);
		searchForm.setUsesPrimaryReplication(true);
		searchForm.setRollupAssetClass(false);

		InvestmentAccountAssetClassSecurityAllowance securityAllowance = new InvestmentAccountAssetClassSecurityAllowance();

		if (balanceDate == null) {
			balanceDate = DateUtils.getPreviousWeekday(new Date());
		}
		for (InvestmentAccountAssetClass accountAssetClass : CollectionUtils.getIterable(getInvestmentAccountAssetClassList(searchForm))) {
			List<InvestmentReplication> replicationList = getInvestmentReplicationListForAccountAssetClass(accountAssetClass.getId());
			for (InvestmentReplication replication : CollectionUtils.getIterable(replicationList)) {
				boolean found = false;

				// TODO DO WE WANT TO BUILD IT IF MISSING?  NEEDED NOW FOR TESTS BUT ONCE WE HAVE SOME HISTORY BUILT UP MIGHT NOT NEED TO REBUILD
				InvestmentReplicationHistory replicationHistory = getInvestmentReplicationHistoryRebuildService().getOrBuildInvestmentReplicationHistory(replication.getId(), balanceDate);
				if (replicationHistory != null) {
					for (InvestmentReplicationAllocationHistory allocationHistory : CollectionUtils.getIterable(replicationHistory.getReplicationAllocationHistoryList())) {
						if (getInvestmentReplicationService().isInvestmentReplicationAllocationSecurityValid(allocationHistory.getReplicationAllocation(), balanceDate, security)) {
							found = true;
							break;
						}
					}
				}

				if (found) {
					securityAllowance.addInvestmentAccountAssetClassReplicationToMap(accountAssetClass, replication);
				}
			}
		}
		return securityAllowance;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                Getter & Setter Methods                    /////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentAccountAssetClass, Criteria> getInvestmentAccountAssetClassDAO() {
		return this.investmentAccountAssetClassDAO;
	}


	public void setInvestmentAccountAssetClassDAO(AdvancedUpdatableDAO<InvestmentAccountAssetClass, Criteria> investmentAccountAssetClassDAO) {
		this.investmentAccountAssetClassDAO = investmentAccountAssetClassDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountAssetClassAssignment, Criteria> getInvestmentAccountAssetClassAssignmentDAO() {
		return this.investmentAccountAssetClassAssignmentDAO;
	}


	public void setInvestmentAccountAssetClassAssignmentDAO(AdvancedUpdatableDAO<InvestmentAccountAssetClassAssignment, Criteria> investmentAccountAssetClassAssignmentDAO) {
		this.investmentAccountAssetClassAssignmentDAO = investmentAccountAssetClassAssignmentDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountAssetClassBenchmark, Criteria> getInvestmentAccountAssetClassBenchmarkDAO() {
		return this.investmentAccountAssetClassBenchmarkDAO;
	}


	public void setInvestmentAccountAssetClassBenchmarkDAO(AdvancedUpdatableDAO<InvestmentAccountAssetClassBenchmark, Criteria> investmentAccountAssetClassBenchmarkDAO) {
		this.investmentAccountAssetClassBenchmarkDAO = investmentAccountAssetClassBenchmarkDAO;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> getInvestmentAccountTargetExposureHandler() {
		return this.investmentAccountTargetExposureHandler;
	}


	public void setInvestmentAccountTargetExposureHandler(InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> investmentAccountTargetExposureHandler) {
		this.investmentAccountTargetExposureHandler = investmentAccountTargetExposureHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentReplicationHistoryRebuildService getInvestmentReplicationHistoryRebuildService() {
		return this.investmentReplicationHistoryRebuildService;
	}


	public void setInvestmentReplicationHistoryRebuildService(InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService) {
		this.investmentReplicationHistoryRebuildService = investmentReplicationHistoryRebuildService;
	}
}
