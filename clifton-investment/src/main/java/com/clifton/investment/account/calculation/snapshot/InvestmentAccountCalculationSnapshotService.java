package com.clifton.investment.account.calculation.snapshot;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.investment.account.calculation.snapshot.search.InvestmentAccountCalculationSnapshotDetailSearchForm;
import com.clifton.investment.account.calculation.snapshot.search.InvestmentAccountCalculationSnapshotSearchForm;

import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
public interface InvestmentAccountCalculationSnapshotService {

	/**
	 * Also populates the detail for the snapshot
	 */
	public InvestmentAccountCalculationSnapshot getInvestmentAccountCalculationSnapshot(int id);


	public List<InvestmentAccountCalculationSnapshot> getInvestmentAccountCalculationSnapshotList(InvestmentAccountCalculationSnapshotSearchForm searchForm, boolean populateDetails);


	/**
	 * Saves the list of snapshots and the details for each snapshot
	 * Pass existing list and new list in to properly perform inserts/updates/deletes
	 */
	@DoNotAddRequestMapping
	public void saveInvestmentAccountCalculationSnapshotList(Set<InvestmentAccountCalculationSnapshot> snapshotList, Set<InvestmentAccountCalculationSnapshot> existingSnapshotList);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationSnapshotDetail getInvestmentAccountCalculationSnapshotDetail(int id);


	public List<InvestmentAccountCalculationSnapshotDetail> getInvestmentAccountCalculationSnapshotDetailListForSnapshot(int snapshotId);


	public List<InvestmentAccountCalculationSnapshotDetail> getInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshotDetailSearchForm searchForm);
}
