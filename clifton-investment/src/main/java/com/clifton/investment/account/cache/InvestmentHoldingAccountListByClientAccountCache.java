package com.clifton.investment.account.cache;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;

import java.util.Date;
import java.util.List;


/**
 * A cache used to store a list of CLS compliant Holding Accounts for a given client InvestmentAccount,
 * relationshipPurpose, and the InvestmentAccountType of the holding account.
 *
 * @author davidi
 */
public interface InvestmentHoldingAccountListByClientAccountCache {

	List<InvestmentAccount> getHoldingAccountList(int clientInvestmentAccountId, String relationshipPurpose, Date relationshipPurposeActiveOnDate, InvestmentAccountType holdingAccountType);
}
