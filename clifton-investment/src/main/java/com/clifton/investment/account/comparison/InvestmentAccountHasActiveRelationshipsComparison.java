package com.clifton.investment.account.comparison;


import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountHasActiveRelationships</code> returns true if the account has any active relationships...
 * Looks for the account as the main and the related account in relationships active today.  This allows it to work for Client & Holding Accounts
 *
 * @author manderson
 */
public class InvestmentAccountHasActiveRelationshipsComparison implements Comparison<InvestmentAccount> {

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	/**
	 * If null or 0 then checks active as of today.  Otherwise will add or remove days from today to get the date to use.
	 * For example - When terminating accounts - will allow you to terminate accounts that have active relationships today, but not active relationships tomorrow
	 * i.e. relationship end date = today and you can terminate the account.
	 */
	private Integer addToDaysForActiveCheck;


	@Override
	public boolean evaluate(InvestmentAccount account, ComparisonContext context) {
		AssertUtils.assertNotNull(account, "Investment Account is required to find relationships for.");


		// Find all Active relationships today (or given date)...
		Date activeOnDate = (getAddToDaysForActiveCheck() == null ? new Date() : DateUtils.addDays(new Date(), getAddToDaysForActiveCheck()));
		List<InvestmentAccountRelationship> relatedAccountList = CollectionUtils.getFiltered(getInvestmentAccountRelationshipService().getRelatedInvestmentAccountList(account.getId(), false), investmentAccountRelationship -> DateUtils.isDateBetween(activeOnDate, investmentAccountRelationship.getStartDate(), investmentAccountRelationship.getEndDate(), false));
		List<InvestmentAccountRelationship> mainAccountList = CollectionUtils.getFiltered(getInvestmentAccountRelationshipService().getMainInvestmentAccountList(account.getId(), false), investmentAccountRelationship -> DateUtils.isDateBetween(activeOnDate, investmentAccountRelationship.getStartDate(), investmentAccountRelationship.getEndDate(), false));


		boolean result = !CollectionUtils.isEmpty(relatedAccountList) || !CollectionUtils.isEmpty(mainAccountList);
		StringBuilder message = new StringBuilder("(Account " + account.getNumber() + ") ");
		if (result) {
			message.append("has [").append(CollectionUtils.getSize(mainAccountList) + CollectionUtils.getSize(relatedAccountList)).append("] active account relationship(s).");
		}
		else {
			message.append("does not have any active account relationships.");
		}

		result = isReverse() ? !result : result;
		if (context != null) {
			if (result) {
				context.recordTrueMessage(message.toString());
			}
			else {
				context.recordFalseMessage(message.toString());
			}
		}
		return result;
	}


	protected boolean isReverse() {
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public Integer getAddToDaysForActiveCheck() {
		return this.addToDaysForActiveCheck;
	}


	public void setAddToDaysForActiveCheck(Integer addToDaysForActiveCheck) {
		this.addToDaysForActiveCheck = addToDaysForActiveCheck;
	}
}
