package com.clifton.investment.account.targetexposure.calculators;

/**
 * <code>BaseInvestmentAccountTargetExposureCalculator</code> is a base {@link InvestmentAccountTargetExposureCalculator}
 * that allows for customizable support for {@link InvestmentAccountTargetExposureCalculator#isDoNotRequireOffsettingAdjustments()}.
 * This class allows for the property to have a system bean property so a bean can define the use of a set of offsetting adjustments.
 * If no customization for the method is performed, the default behavior is to return true for the method.
 *
 * @author nickk
 */
public abstract class BaseInvestmentAccountTargetExposureCalculator implements InvestmentAccountTargetExposureCalculator {

	private boolean doNotRequireOffsettingAdjustments = true;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isDoNotRequireOffsettingAdjustments() {
		return this.doNotRequireOffsettingAdjustments;
	}


	public void setDoNotRequireOffsettingAdjustments(boolean doNotRequireOffsettingAdjustments) {
		this.doNotRequireOffsettingAdjustments = doNotRequireOffsettingAdjustments;
	}
}
