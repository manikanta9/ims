package com.clifton.investment.account.calculation.snapshot.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class InvestmentAccountCalculationSnapshotDetailListForSnapshotCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentAccountCalculationSnapshotDetail, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "calculationSnapshot.id";
	}


	@Override
	protected Integer getBeanKeyValue(InvestmentAccountCalculationSnapshotDetail bean) {
		if (bean.getCalculationSnapshot() != null) {
			return bean.getCalculationSnapshot().getId();
		}
		return null;
	}
}
