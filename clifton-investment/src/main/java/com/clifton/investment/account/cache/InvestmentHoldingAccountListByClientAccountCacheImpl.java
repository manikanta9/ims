package com.clifton.investment.account.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCustomKeyDaoListCache;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


@Component
public class InvestmentHoldingAccountListByClientAccountCacheImpl extends SelfRegisteringCustomKeyDaoListCache<String, InvestmentAccount> implements InvestmentHoldingAccountListByClientAccountCache {

	private static final int INVESTMENT_ACCOUNT_ID_INDEX = 0;
	private static final int RELATIONSHIP_PURPOSE_INDEX = 1;
	private static final int RELATIONSHIP_PURPOSE_ACTIVE_ON_DATE_INDEX = 2;
	private static final int ACCOUNT_TYPE_NAME_INDEX = 3;
	private static final int IS_CONTRACT_REQUIRED_INDEX = 4;
	private static final String KEY_DELIMITER = "::";

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentAccount> getHoldingAccountList(int clientInvestmentAccountId, String relationshipPurpose, Date relationshipPurposeActiveOnDate, InvestmentAccountType holdingAccountType) {
		final String key = getKey(clientInvestmentAccountId, relationshipPurpose, relationshipPurposeActiveOnDate, holdingAccountType != null ? holdingAccountType.getName() : null, holdingAccountType != null ? holdingAccountType.isContractRequired() : null);
		return computeIfAbsent(key, this::lookupHoldingAccounts);
	}


	@Override
	protected Class<InvestmentAccount> getDtoClass() {
		return InvestmentAccount.class;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getKey(int clientInvestmentAccountId, String relationshipPurpose, Date relationshipPurposeActiveOnDate, String holdingAccountTypeName, Boolean contractRequired) {
		return clientInvestmentAccountId + KEY_DELIMITER + relationshipPurpose + KEY_DELIMITER + DateUtils.fromDate(relationshipPurposeActiveOnDate, "MM/dd/yyyy") + KEY_DELIMITER + (holdingAccountTypeName != null ? holdingAccountTypeName : " ") + KEY_DELIMITER + (contractRequired != null ? contractRequired : " ");
	}


	private List<InvestmentAccount> lookupHoldingAccounts(String key) {
		String[] components = StringUtils.split(key, KEY_DELIMITER);

		int clientInvestmentAccountId = Integer.parseInt(components[INVESTMENT_ACCOUNT_ID_INDEX]);
		String relationshipPurpose = components[RELATIONSHIP_PURPOSE_INDEX];
		Date relationshipPurposeActiveOnDate = DateUtils.toDate(components[RELATIONSHIP_PURPOSE_ACTIVE_ON_DATE_INDEX], "MM/dd/yyyy");
		String accountTypeName = components[ACCOUNT_TYPE_NAME_INDEX].trim();
		Boolean isContractRequired = Boolean.parseBoolean(components[IS_CONTRACT_REQUIRED_INDEX]);

		InvestmentAccountSearchForm investmentAccountSearchForm = new InvestmentAccountSearchForm();
		if (!StringUtils.isEmpty(accountTypeName)) {
			investmentAccountSearchForm.setAccountType(accountTypeName);
		}
		investmentAccountSearchForm.setMainAccountId(clientInvestmentAccountId);
		investmentAccountSearchForm.setMainPurpose(relationshipPurpose);
		investmentAccountSearchForm.setMainPurposeActiveOnDate(relationshipPurposeActiveOnDate);
		if (!StringUtils.isEmpty(components[IS_CONTRACT_REQUIRED_INDEX])) {
			investmentAccountSearchForm.setContractRequired(isContractRequired);
		}
		investmentAccountSearchForm.setOurAccount(false);
		investmentAccountSearchForm.setWorkflowStatusNameEquals(WorkflowStatus.STATUS_ACTIVE);

		return getInvestmentAccountService().getInvestmentAccountList(investmentAccountSearchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
