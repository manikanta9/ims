package com.clifton.investment.account.calculation.snapshot;

import com.clifton.core.beans.hierarchy.HierarchicalSimpleEntity;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>InvestmentAccountCalculationSnapshotDetail</code> provides details of the calculated value in the {@link InvestmentAccountCalculationSnapshot}
 * When possible, this would show security level details which is useful for reporting.  It's also hierarchical in nature for cases where it's not just a sum of all
 * but sub-section sums.  i.e. Synthetic Exposure AUM calculation is the Sum of the Absolute Value of each asset class.  Therefore we cannot take each security's value on it's own
 *
 * @author manderson
 */
public class InvestmentAccountCalculationSnapshotDetail extends HierarchicalSimpleEntity<InvestmentAccountCalculationSnapshotDetail, Integer> {

	/**
	 * The snapshot this detail is for
	 */
	private InvestmentAccountCalculationSnapshot calculationSnapshot;

	/**
	 * If results can apply to a specific asset class this will be populated
	 */
	private InvestmentAssetClass investmentAssetClass;

	/**
	 * If applies to a specific security, this will be populated
	 */
	private InvestmentSecurity investmentSecurity;

	/**
	 * The quantity of the security position.  Useful for reporting to note if the account holds short positions
	 */
	private BigDecimal quantity;

	/**
	 * The preliminary calculated value - in the snapshot's currency
	 * This is not necessarily the calculated value.  For example - if calculation uses ABS of each position, this might show a negative value, but calculatedValue would show the ABS value
	 * Or, for cases like Options - If we need to take the bigger of the Puts vs. Calls - this value will be populated for all, but the smaller of the two will have a calculated value of zero
	 */
	private BigDecimal preliminaryCalculatedValue;

	/**
	 * The calculated value - in the snapshot's currency
	 * This would be the value that is actually attributed to the final amount on the account
	 */
	private BigDecimal calculatedValue;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationSnapshotDetail() {
		super();
	}


	public InvestmentAccountCalculationSnapshotDetail(InvestmentAccountCalculationSnapshot calculationSnapshot) {
		this();
		this.calculationSnapshot = calculationSnapshot;
	}

	////////////////////////////////////////////////////////////////////////////////


	public static InvestmentAccountCalculationSnapshotDetail ofAssetClass(InvestmentAccountCalculationSnapshot calculationSnapshot, InvestmentAssetClass investmentAssetClass) {
		return InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(calculationSnapshot, investmentAssetClass, null, null);
	}


	public static InvestmentAccountCalculationSnapshotDetail ofSecurity(InvestmentAccountCalculationSnapshot calculationSnapshot, InvestmentSecurity investmentSecurity, BigDecimal quantity) {
		return InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(calculationSnapshot, null, investmentSecurity, quantity);
	}


	public static InvestmentAccountCalculationSnapshotDetail ofAssetClassAndSecurity(InvestmentAccountCalculationSnapshot calculationSnapshot, InvestmentAssetClass investmentAssetClass, InvestmentSecurity investmentSecurity, BigDecimal quantity) {
		InvestmentAccountCalculationSnapshotDetail detail = new InvestmentAccountCalculationSnapshotDetail(calculationSnapshot);
		detail.setInvestmentAssetClass(investmentAssetClass);
		detail.setInvestmentSecurity(investmentSecurity);
		detail.setQuantity(quantity);
		return detail;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCalculatedValueBase() {
		if (getCalculationSnapshot() != null) {
			return MathUtils.multiply(getCalculatedValue(), getCalculationSnapshot().getFxRateToBase());
		}
		return null;
	}


	/**
	 * Needed for UI for previews because parent id is null since previews aren't saved
	 */
	public boolean isChild() {
		return getParent() != null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationSnapshot getCalculationSnapshot() {
		return this.calculationSnapshot;
	}


	public void setCalculationSnapshot(InvestmentAccountCalculationSnapshot calculationSnapshot) {
		this.calculationSnapshot = calculationSnapshot;
	}


	public InvestmentAssetClass getInvestmentAssetClass() {
		return this.investmentAssetClass;
	}


	public void setInvestmentAssetClass(InvestmentAssetClass investmentAssetClass) {
		this.investmentAssetClass = investmentAssetClass;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPreliminaryCalculatedValue() {
		return this.preliminaryCalculatedValue;
	}


	public void setPreliminaryCalculatedValue(BigDecimal preliminaryCalculatedValue) {
		this.preliminaryCalculatedValue = preliminaryCalculatedValue;
	}


	public BigDecimal getCalculatedValue() {
		return this.calculatedValue;
	}


	public void setCalculatedValue(BigDecimal calculatedValue) {
		this.calculatedValue = calculatedValue;
	}
}
