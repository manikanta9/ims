package com.clifton.investment.account.assetclass.rebalance;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountRebalanceHistory</code> ...
 *
 * @author manderson
 */
public class InvestmentAccountRebalanceHistory extends BaseEntity<Integer> {

	private String description;

	private InvestmentAccount investmentAccount;

	private Date rebalanceDate;

	private RebalanceCalculationTypes rebalanceCalculationType;

	private BigDecimal beforeRebalanceCashTotal;

	private BigDecimal afterRebalanceCashTotal;

	private List<InvestmentAccountRebalanceAssetClassHistory> assetClassHistoryList;


	////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////


	public String getLabel() {
		StringBuilder lbl = new StringBuilder(16);
		if (getInvestmentAccount() != null) {
			lbl.append(getInvestmentAccount().getNumber()).append(": ");
		}
		lbl.append(getRebalanceCalculationTypeLabel()).append(" Rebalance on ");
		if (getRebalanceDate() != null) {
			lbl.append(DateUtils.fromDateShort(getRebalanceDate()));
		}
		return lbl.toString();
	}


	public String getRebalanceCalculationTypeLabel() {
		if (getRebalanceCalculationType() != null) {
			return getRebalanceCalculationType().getLabel();
		}
		return null;
	}


	////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public Date getRebalanceDate() {
		return this.rebalanceDate;
	}


	public void setRebalanceDate(Date rebalanceDate) {
		this.rebalanceDate = rebalanceDate;
	}


	public RebalanceCalculationTypes getRebalanceCalculationType() {
		return this.rebalanceCalculationType;
	}


	public void setRebalanceCalculationType(RebalanceCalculationTypes rebalanceCalculationType) {
		this.rebalanceCalculationType = rebalanceCalculationType;
	}


	public List<InvestmentAccountRebalanceAssetClassHistory> getAssetClassHistoryList() {
		return this.assetClassHistoryList;
	}


	public void setAssetClassHistoryList(List<InvestmentAccountRebalanceAssetClassHistory> assetClassHistoryList) {
		this.assetClassHistoryList = assetClassHistoryList;
	}


	public BigDecimal getBeforeRebalanceCashTotal() {
		return this.beforeRebalanceCashTotal;
	}


	public void setBeforeRebalanceCashTotal(BigDecimal beforeRebalanceCashTotal) {
		this.beforeRebalanceCashTotal = beforeRebalanceCashTotal;
	}


	public BigDecimal getAfterRebalanceCashTotal() {
		return this.afterRebalanceCashTotal;
	}


	public void setAfterRebalanceCashTotal(BigDecimal afterRebalanceCashTotal) {
		this.afterRebalanceCashTotal = afterRebalanceCashTotal;
	}
}
