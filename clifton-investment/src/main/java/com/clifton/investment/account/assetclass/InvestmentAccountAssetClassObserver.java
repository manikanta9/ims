package com.clifton.investment.account.assetclass;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.targetexposure.InvestmentAccountTargetExposureHandler;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentAccountAssetClassObserver</code> will perform validation prior to saves,
 * as well as setting some specific fields.
 * <p/>
 * After save, will recalculate parent cash & target percentages when used by a rollup asset class & values have changed.
 *
 * @author manderson
 */
@Component
public class InvestmentAccountAssetClassObserver extends SelfRegisteringDaoObserver<InvestmentAccountAssetClass> {

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;
	private AdvancedReadOnlyDAO<InvestmentManagerAccountAllocation, Criteria> investmentManagerAccountAllocationDAO;
	private AdvancedReadOnlyDAO<InvestmentAccountAssetClassAssignment, Criteria> investmentAccountAssetClassAssignmentDAO;

	private InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> investmentAccountTargetExposureHandler;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<InvestmentAccountAssetClass> dao, DaoEventTypes event, InvestmentAccountAssetClass bean) {
		// In order to turn validation off it must be explicitly set to false.  Therefore, if it is not
		// explicitly false (most cases value will be null) then we need to  validate.
		if (!Boolean.FALSE.equals(getDaoEventContext().getBeanAttribute(bean, "validate"))) {
			InvestmentAccountAssetClass original = (event.isInsert() ? null : getOriginalBean(dao, bean));
			validateRollup(event, bean);
			validateAssetClass(event, bean, original);
			validateRebalancing(event, bean);
		}
	}


	/**
	 * If the InvestmentAccountAssetClass is not a RollupAssetClass and it has a parent and the cash and asset class percentages have been changed,
	 * modifies parent's values to reflect changes.
	 * <p/>
	 * Also, if Parent id is being set or removed, updates parent to reflect adding/removing the child.
	 */
	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<InvestmentAccountAssetClass> dao, DaoEventTypes event, InvestmentAccountAssetClass bean, Throwable e) {
		if (e == null) {
			InvestmentAccountAssetClass original = (event.isInsert() ? null : getOriginalBean(dao, bean));

			InvestmentAccountAssetClass originalParent = (original == null ? null : original.getParent());
			InvestmentAccountAssetClass newParent = bean.getParent();
			// Recalculate Values on the Old Parent and/or New Parent - Will only save if there is a change
			if (originalParent != null) {
				recalculateRollupPercentages((UpdatableDAO<InvestmentAccountAssetClass>) dao, originalParent);
			}
			if (newParent != null && !CompareUtils.isEqual(originalParent, newParent)) {
				recalculateRollupPercentages((UpdatableDAO<InvestmentAccountAssetClass>) dao, newParent);
			}
		}
	}


	private void recalculateRollupPercentages(UpdatableDAO<InvestmentAccountAssetClass> dao, InvestmentAccountAssetClass parent) {
		// Pull it again from the database to ensure adding to the correct amount
		parent = dao.findByPrimaryKey(parent.getId());
		List<InvestmentAccountAssetClass> children = dao.findByField("parent.id", parent.getId());

		// The database stores to 5 decimals, so if the calculated value is beyond that we don't want it to trigger as a change
		BigDecimal assetClassPercentage = MathUtils.round(CoreMathUtils.sumProperty(children, InvestmentAccountAssetClass::getAssetClassPercentage), 5);
		// Note: Fund Cash % field supports up to 15 decimals
		BigDecimal cashPercentage = MathUtils.round(CoreMathUtils.sumProperty(children, InvestmentAccountAssetClass::getCashPercentage), 15);
		BigDecimal rebalanceExposurePercentage = MathUtils.round(CoreMathUtils.sumProperty(children, InvestmentAccountAssetClass::getRebalanceExposureTarget), 5);

		boolean change = ((!MathUtils.isEqual(parent.getAssetClassPercentage(), assetClassPercentage)) || (!MathUtils.isEqual(parent.getCashPercentage(), cashPercentage)) || (!MathUtils.isEqual(parent.getRebalanceExposureTarget(), rebalanceExposurePercentage)));
		if (change) {
			parent.setAssetClassPercentage(assetClassPercentage);
			parent.setCashPercentage(cashPercentage);
			parent.setRebalanceExposureTarget(rebalanceExposurePercentage);

			getDaoEventContext().setBeanAttribute(parent, "validate", false);
			dao.save(parent);
			getDaoEventContext().removeBeanAttribute(parent, "validate");
		}
	}


	private void validateRollup(DaoEventTypes event, InvestmentAccountAssetClass bean) {
		if (event.isInsert() || event.isUpdate()) {
			if (bean.getParent() != null) {
				ValidationUtils.assertFalse(bean.equals(bean.getParent()),
						"An Account Asset Class cannot be a parent of itself. Please select a different child asset class or assign this asset class to a different parent.");
				ValidationUtils.assertTrue(bean.getParent().isRollupAssetClass(), "Account Asset Class [" + bean.getAssetClass().getName() + "] cannot be assigned to parent ["
						+ bean.getParent().getAssetClass().getName() + "].  Parent asset class is not flagged as a rollup asset class.");
			}
		}
	}


	/**
	 * Ensure all Rebalance Allocations are entered as positive numbers and absolute values are >= target counterparts
	 */
	private void validateRebalancing(DaoEventTypes event, InvestmentAccountAssetClass bean) {
		if (event.isInsert() || event.isUpdate()) {
			// If any of the 4 Rebalance Triggers is entered, then they all are required:
			if (bean.getRebalanceAllocationMin() != null || bean.getRebalanceAllocationAbsoluteMin() != null || bean.getRebalanceAllocationMax() != null
					|| bean.getRebalanceAllocationAbsoluteMax() != null) {

				ValidationUtils.assertNotNull(bean.getRebalanceAllocationMin(),
						"If any rebalancing triggers are entered, then they all must be populated.  Please enter a value for the Rebalance Allocation Minimum", "rebalanceAllocationMin");
				ValidationUtils.assertNotNull(bean.getRebalanceAllocationMax(),
						"If any rebalancing triggers are entered, then they all must be populated.  Please enter a value for the Rebalance Allocation Maximum", "rebalanceAllocationMax");

				ValidationUtils.assertNotNull(bean.getRebalanceAllocationAbsoluteMin(),
						"If any rebalancing triggers are entered, then they all must be populated.  Please enter a value for the Rebalance Allocation Absolute Minimum",
						"rebalanceAllocationAbsoluteMin");
				ValidationUtils.assertNotNull(bean.getRebalanceAllocationAbsoluteMax(),
						"If any rebalancing triggers are entered, then they all must be populated.  Please enter a value for the Rebalance Allocation Absolute Maximum",
						"rebalanceAllocationAbsoluteMax");

				if (MathUtils.isGreaterThan(0, bean.getRebalanceAllocationMin()) || MathUtils.isGreaterThan(0, bean.getRebalanceAllocationAbsoluteMin())
						|| MathUtils.isGreaterThan(0, bean.getRebalanceAllocationMax()) || MathUtils.isGreaterThan(0, bean.getRebalanceAllocationAbsoluteMax())) {
					throw new ValidationException("All rebalancing targets and absolutes must be entered as positive values.");
				}
				if (MathUtils.isGreaterThan(bean.getRebalanceAllocationMin(), bean.getRebalanceAllocationAbsoluteMin())) {
					throw new FieldValidationException("Absolute minimum [" + bean.getRebalanceAllocationAbsoluteMin().doubleValue() + "] must be greater than or equal to the target minimum ["
							+ bean.getRebalanceAllocationMin().doubleValue() + "]", "rebalanceAllocationAbsoluteMin");
				}
				if (MathUtils.isGreaterThan(bean.getRebalanceAllocationMax(), bean.getRebalanceAllocationAbsoluteMax())) {
					throw new FieldValidationException("Absolute maximum [" + bean.getRebalanceAllocationAbsoluteMax().doubleValue() + "] must be greater than or equal to the target maximum ["
							+ bean.getRebalanceAllocationMax().doubleValue() + "]", "rebalanceAllocationAbsoluteMax");
				}
				ValidationUtils.assertNotNull(bean.getRebalanceTriggerAction(), "Rebalance Trigger Action is required when rebalancing targets are entered.");
			}
			else {
				bean.setRebalanceAllocationMin(null);
				bean.setRebalanceAllocationMax(null);
				bean.setRebalanceAllocationAbsoluteMin(null);
				bean.setRebalanceAllocationAbsoluteMax(null);
				bean.setRebalanceTriggerAction(null);
			}
		}
	}


	private void validateAssetClass(DaoEventTypes event, InvestmentAccountAssetClass bean, InvestmentAccountAssetClass original) {
		boolean validateInactive = false;
		if (event.isUpdate() && bean.isInactive() && !original.isInactive()) {
			validateInactive = true;
		}
		// Cannot change/delete/de-activate an asset class that is used by managers.
		if (event.isDelete() || validateInactive || (event.isUpdate() && !bean.getAssetClass().equals(original.getAssetClass()))) {
			List<InvestmentManagerAccount> managerAccounts = getManagerAccountsUsed(original.getAccount().getId(), original.getAssetClass().getId());
			if (!CollectionUtils.isEmpty(managerAccounts)) {
				throw new ValidationException("Cannot remove or de-activate Asset Class [" + original.getAssetClass().getName()
						+ "] from this account because it is being used by the following manager accounts [" + BeanUtils.getPropertyValues(managerAccounts, "label", ",") + "].");
			}
		}

		if (validateInactive) {
			getInvestmentAccountTargetExposureHandler().validateTargetExposureAdjustmentAssignmentUnused(bean, "asset class", "de-activated", getInvestmentAccountAssetClassAssignmentDAO());
			if (bean.getParent() != null) {
				throw new ValidationException("Cannot de-activate Asset Class + [" + original.getAssetClass().getName() + "] from this account because it is tied to Rollup Asset Class ["
						+ bean.getParent().getAssetClass().getName() + "].");
			}
		}

		if (event.isInsert() || event.isUpdate()) {
			// Make sure, if Asset Class IsMainCash - it is the only one for the account
			if (bean.getAssetClass().isMainCash() && (event.isInsert() || (!original.getAssetClass().isMainCash()))) {
				InvestmentAccountAssetClass main = CollectionUtils.getOnlyElement(
						BeanUtils.filter(getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassListByAccount(bean.getAccount().getId()), accountAssetClass -> accountAssetClass.getAssetClass().isMainCash()));
				if (main != null) {
					ValidationUtils.assertTrue(main.equals(bean), "There can only be one main cash asset class selected per account.  Asset class [" + main.getAssetClass().getLabel()
							+ "] is already selected as the main cash asset class.", "assetClass.id");
				}
			}
		}
	}


	private List<InvestmentManagerAccount> getManagerAccountsUsed(final int accountId, final short assetClassId) {
		List<InvestmentManagerAccountAllocation> usedList = getInvestmentManagerAccountAllocationDAO().findBySearchCriteria((HibernateSearchConfigurer) criteria -> {
			criteria.createAlias("managerAccountAssignment", "maa");
			criteria.createAlias("maa.referenceTwo", "alloc");
			criteria.add(Restrictions.eq("alloc.id", accountId));
			criteria.add(Restrictions.eq("assetClass.id", assetClassId));
		});
		if (CollectionUtils.isEmpty(usedList)) {
			return null;
		}
		List<InvestmentManagerAccount> managerList = new ArrayList<>();
		for (InvestmentManagerAccountAllocation alloc : usedList) {
			if (!managerList.contains(alloc.getManagerAccountAssignment().getReferenceOne())) {
				managerList.add(alloc.getManagerAccountAssignment().getReferenceOne());
			}
		}
		return managerList;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public AdvancedReadOnlyDAO<InvestmentManagerAccountAllocation, Criteria> getInvestmentManagerAccountAllocationDAO() {
		return this.investmentManagerAccountAllocationDAO;
	}


	public void setInvestmentManagerAccountAllocationDAO(AdvancedReadOnlyDAO<InvestmentManagerAccountAllocation, Criteria> investmentManagerAccountAllocationDAO) {
		this.investmentManagerAccountAllocationDAO = investmentManagerAccountAllocationDAO;
	}


	public AdvancedReadOnlyDAO<InvestmentAccountAssetClassAssignment, Criteria> getInvestmentAccountAssetClassAssignmentDAO() {
		return this.investmentAccountAssetClassAssignmentDAO;
	}


	public void setInvestmentAccountAssetClassAssignmentDAO(AdvancedReadOnlyDAO<InvestmentAccountAssetClassAssignment, Criteria> investmentAccountAssetClassAssignmentDAO) {
		this.investmentAccountAssetClassAssignmentDAO = investmentAccountAssetClassAssignmentDAO;
	}


	public InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> getInvestmentAccountTargetExposureHandler() {
		return this.investmentAccountTargetExposureHandler;
	}


	public void setInvestmentAccountTargetExposureHandler(InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> investmentAccountTargetExposureHandler) {
		this.investmentAccountTargetExposureHandler = investmentAccountTargetExposureHandler;
	}
}
