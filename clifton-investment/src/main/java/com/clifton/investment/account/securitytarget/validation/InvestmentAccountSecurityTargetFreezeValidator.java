package com.clifton.investment.account.securitytarget.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetFreeze;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetFreezeType;
import org.springframework.stereotype.Component;


/**
 * <code>InvestmentAccountSecurityTargetFreezeValidator</code> is a validator for a saved {@link InvestmentAccountSecurityTargetFreeze}.
 *
 * @author NickK
 */
@Component
public class InvestmentAccountSecurityTargetFreezeValidator extends SelfRegisteringDaoValidator<InvestmentAccountSecurityTargetFreeze> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentAccountSecurityTargetFreeze frozenAllocation, DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertNotNull(frozenAllocation.getFreezeType(), "A security target freeze type is required.");
		InvestmentAccountSecurityTargetFreezeType type = frozenAllocation.getFreezeType();
		if (type.isCauseRequired()) {
			ValidationUtils.assertNotNull(frozenAllocation.getCauseSystemTable(), () -> "A security target freeze of type: " + type + " requires a cause table to be defined.");
			ValidationUtils.assertNotNull(frozenAllocation.getCauseFKFieldId(), () -> "A security target freeze of type: " + type + " requires a cause entity to be defined.");
		}
		if (type.isNoteRequired()) {
			ValidationUtils.assertNotNull(frozenAllocation.getFreezeNote(), () -> "A security target freeze of type: " + type + " requires a note to be defined.");
		}

		ValidationUtils.assertNotNull(frozenAllocation.getStartDate(), "Start Date is required.");
		ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(DateUtils.clearTime(frozenAllocation.getEndDate()), DateUtils.clearTime(frozenAllocation.getStartDate())), "End Date of client account security target freeze must be after or equal to Start Date.");
	}
}
