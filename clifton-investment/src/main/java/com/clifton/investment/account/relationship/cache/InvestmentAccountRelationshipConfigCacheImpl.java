package com.clifton.investment.account.relationship.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentAccountRelationshipConfigCacheImpl</code> caches
 * full Account Relationships for an account.  If the account has sub accounts, also includes
 * sub account relationships tied to the main account by the asset class from that association
 * <p>
 * All Configs for any account for the same client or related client are cleared when a relationship is added/updated/deleted
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentAccountRelationshipConfigCacheImpl extends SelfRegisteringSimpleDaoCache<InvestmentAccountRelationship, Integer, InvestmentAccountRelationshipConfig> implements InvestmentAccountRelationshipConfigCache {

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountRelationshipConfig getInvestmentAccountRelationshipConfig(Integer investmentAccountId) {
		return getCacheHandler().get(getCacheName(), investmentAccountId);
	}


	@Override
	public void setInvestmentAccountRelationshipConfig(Integer investmentAccountId, InvestmentAccountRelationshipConfig config) {
		getCacheHandler().put(getCacheName(), investmentAccountId, config);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentAccountRelationship> dao, @SuppressWarnings("unused") DaoEventTypes event, InvestmentAccountRelationship bean,
	                                Throwable e) {

		if (e == null) {
			// Note: Making some assumptions based on existing functionality
			// 1.  Main Account is a link field in UI, so can never change
			// 2.  Related Account selection is limited to the same client or related client
			// Therefore, in all cases, clear the cache for any account for that client or related client
			// We can't do direct relationships only because config cache contains nested relationships (i.e. main account also contains sub-account's relationships)
			if (bean != null && bean.getReferenceOne() != null) {
				InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
				searchForm.setClientIdOrRelatedClient(bean.getReferenceOne().getBusinessClient().getId());
				List<InvestmentAccount> allAccounts = getInvestmentAccountService().getInvestmentAccountList(searchForm);
				for (InvestmentAccount account : CollectionUtils.getIterable(allAccounts)) {
					getCacheHandler().remove(getCacheName(), account.getId());
				}
			}
		}
	}


	@Override
	public void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentAccountRelationship> dao, @SuppressWarnings("unused") DaoEventTypes event,
	                                 @SuppressWarnings("unused") InvestmentAccountRelationship bean) {
		// do nothing
	}


	//////////////////////////////////////////////////////////////////////////// 
	////////              Getter and Setter Methods                  /////////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
