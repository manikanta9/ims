package com.clifton.investment.account.relationship;

import com.clifton.core.util.validation.ValidationException;


/**
 * The MultipleRelatedAccountsFoundException class represent a {@link ValidationException} that should be thrown by
 * related investment account lookup for purpose logic when more than one related accounts are found.
 *
 * @author vgomelsky
 */
public class MultipleRelatedAccountsFoundException extends ValidationException {

	public MultipleRelatedAccountsFoundException(String message) {
		super(message);
	}
}
