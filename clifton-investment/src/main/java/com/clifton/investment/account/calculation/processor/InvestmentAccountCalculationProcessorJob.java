package com.clifton.investment.account.calculation.processor;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;

import java.util.Date;
import java.util.Map;


/**
 * The <code>InvestmentAccountCalculationProcessorJob</code> class processes account calculation snapshots for a specified calculation type
 * and date range
 *
 * @author manderson
 */
public class InvestmentAccountCalculationProcessorJob implements Task, ValidationAware, StatusHolderObjectAware<Status> {

	private InvestmentAccountCalculationProcessorService investmentAccountCalculationProcessorService;

	private Integer startWeekdaysBack;

	/**
	 * If blank will process ONLY on the start weekdays back date.
	 */
	private Integer endWeekdaysBack;

	/**
	 * If we should re-process existing snapshots
	 */
	private boolean reprocessExisting;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Required calculation type selection
	 */
	private Short calculationTypeId;

	private StatusHolderObject<Status> statusHolder;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getCalculationTypeId(), "Calculation Type is required.");
		ValidationUtils.assertNotNull(getStartWeekdaysBack(), "Start weekdays back is required.");
		ValidationUtils.assertTrue(getStartWeekdaysBack() > 0, "Start weekdays back must be greater than 0");
		if (getEndWeekdaysBack() != null) {
			ValidationUtils.assertTrue(getStartWeekdaysBack() > getEndWeekdaysBack(), "Start weekdays back must be greater than end weekdays back. Leave blank to just re-process for the one day.");
		}
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		InvestmentAccountCalculationProcessorCommand command = new InvestmentAccountCalculationProcessorCommand();
		command.setCalculationTypeId(getCalculationTypeId());
		command.setStatus(this.statusHolder.getStatus());
		command.setStartSnapshotDate(DateUtils.addWeekDays(new Date(), -getStartWeekdaysBack()));
		if (getEndWeekdaysBack() != null) {
			command.setEndSnapshotDate(DateUtils.addWeekDays(new Date(), -getEndWeekdaysBack()));
		}
		else {
			command.setEndSnapshotDate(command.getStartSnapshotDate());
		}
		command.setReprocessExisting(isReprocessExisting());
		command.setSynchronous(true);

		getInvestmentAccountCalculationProcessorService().processInvestmentAccountCalculationSnapshotList(command);
		return command.getStatus();
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////              Getter and Setter Methods                ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountCalculationProcessorService getInvestmentAccountCalculationProcessorService() {
		return this.investmentAccountCalculationProcessorService;
	}


	public void setInvestmentAccountCalculationProcessorService(InvestmentAccountCalculationProcessorService investmentAccountCalculationProcessorService) {
		this.investmentAccountCalculationProcessorService = investmentAccountCalculationProcessorService;
	}


	public Short getCalculationTypeId() {
		return this.calculationTypeId;
	}


	public void setCalculationTypeId(Short calculationTypeId) {
		this.calculationTypeId = calculationTypeId;
	}


	public Integer getStartWeekdaysBack() {
		return this.startWeekdaysBack;
	}


	public void setStartWeekdaysBack(Integer startWeekdaysBack) {
		this.startWeekdaysBack = startWeekdaysBack;
	}


	public Integer getEndWeekdaysBack() {
		return this.endWeekdaysBack;
	}


	public void setEndWeekdaysBack(Integer endWeekdaysBack) {
		this.endWeekdaysBack = endWeekdaysBack;
	}


	public boolean isReprocessExisting() {
		return this.reprocessExisting;
	}


	public void setReprocessExisting(boolean reprocessExisting) {
		this.reprocessExisting = reprocessExisting;
	}
}
