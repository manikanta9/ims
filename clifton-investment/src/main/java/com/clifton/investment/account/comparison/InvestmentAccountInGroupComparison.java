package com.clifton.investment.account.comparison;


import com.clifton.core.comparison.Comparison;


/**
 * The <code>InvestmentAccountInGroup</code> class is a {@link Comparison} that evaluates to true
 * if investment account on the bean (specified by the beanName)
 * is a member of the group(s) allowed by this comparison.
 * <p/>
 * Example: Billing Schedules can be defined once to apply an additional account fee for a set of accounts.
 * The system can then use this comparison to then "waive" or skip the additional account fee if
 * the billingSchedule."investmentAccount" is in
 * the group called "Waived Additional Account Fees"
 *
 * @author manderson
 */
public class InvestmentAccountInGroupComparison extends BaseInvestmentAccountInGroupComparison {

	@Override
	protected boolean isRequireInGroup() {
		return true;
	}


	@Override
	protected String getTrueMessage(String accountNumber, String groupList) {
		String messageWording = isMatchAnyGroup() ? "at least one group" : "all groups";
		return "(Account " + accountNumber + " is in " + messageWording + " in group list " + groupList + ".)";
	}


	@Override
	protected String getFalseMessage(String accountNumber, String groupList) {
		String messageWording = isMatchAnyGroup() ? "any groups" : "at least one group";
		return "(Account " + accountNumber + " is not in " + messageWording + " in group list " + groupList + ".)";
	}
}
