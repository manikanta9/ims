package com.clifton.investment.account.assetclass.position;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentAccountAssetClassPositionAllocationValidator</code> validates
 * on inserts/updates that the assetclass/security selection is unique for the date range
 * <p/>
 * Also validates: Security is active during date range
 * Not populated for asset class that has sub-account relationship for the same date range
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentAccountAssetClassPositionAllocationValidator extends SelfRegisteringDaoValidator<InvestmentAccountAssetClassPositionAllocation> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentAccountAssetClassPositionAllocation bean, @SuppressWarnings("unused") DaoEventTypes config, ReadOnlyDAO<InvestmentAccountAssetClassPositionAllocation> dao)
			throws ValidationException {
		// Validate Start/End Dates
		// Added msgPrefix so errors are clear when saving list of allocations from batch job updates
		String msgPrefix = "[" + bean.getLabelLong() + "]: ";
		if (bean.getStartDate() != null && bean.getEndDate() != null && DateUtils.compare(bean.getStartDate(), bean.getEndDate(), false) > 0) {
			throw new FieldValidationException(msgPrefix + "Start Date must be before End Date.");
		}

		List<InvestmentAccountAssetClassPositionAllocation> list = dao.findByFields(new String[]{"accountAssetClass.id", "security.id"}, new Object[]{bean.getAccountAssetClass().getId(),
				bean.getSecurity().getId()});
		for (InvestmentAccountAssetClassPositionAllocation b : CollectionUtils.getIterable(list)) {
			if (b.equals(bean)) {
				continue;
			}
			if (DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), b.getStartDate(), b.getEndDate())) {
				// If either doesn't have a replication selected, or the replication is the same:
				if (bean.getReplication() != null) {
					if (b.getReplication() == null) {
						throw new ValidationException(msgPrefix
								+ "Another Position Allocation for the same asset class and security is already used without a specified replication for this asset class on " + b.getDateLabel()
								+ ". Please enter a unique date range or select replications on both allocations.");
					}
					else if (bean.getReplication().equals(b.getReplication())) {
						throw new ValidationException(msgPrefix + "Another Position Allocation for the same asset class, replication, and security is already used for this asset class on "
								+ b.getDateLabel() + ". Please enter a unique date range.");
					}
				}
				else {
					if (b.getReplication() == null) {
						throw new ValidationException(msgPrefix + "Another Position Allocation for the same asset class and security is already used for this asset class on " + b.getDateLabel()
								+ ". Please enter a unique date range.");
					}
					throw new ValidationException(msgPrefix + "Another Position Allocation for the same asset class and security is already used for replication [" + b.getReplication().getName()
							+ "] for this asset class on " + b.getDateLabel() + ". Please enter a unique date range or select a replication that is different than " + b.getReplication().getName()
							+ ".");
				}
			}
		}
		validateAssetClassSelection(bean);
		validateSecuritySelection(bean);
	}


	@Override
	@SuppressWarnings("unused")
	public void validate(InvestmentAccountAssetClassPositionAllocation bean, DaoEventTypes config) throws ValidationException {
		// unused - uses dao method
	}


	private void validateAssetClassSelection(InvestmentAccountAssetClassPositionAllocation bean) {
		InvestmentAccountAssetClass iac = bean.getAccountAssetClass();
		ValidationUtils.assertNotNull(iac, "Asset class selection is required", "accountAssetClass.id");
		String name = iac.getAssetClass().getLabel();
		ValidationUtils.assertFalse(iac.isRollupAssetClass(), "Selected Asset Class [" + name + "] is an invalid selection because it is a rollup asset class.", "accountAssetClass.id");
		ValidationUtils.assertTrue(iac.isReplicationUsed(), "Selected Asset Class [" + name + "] is an invalid selection because it does not have any replications assigned to it.",
				"accountAssetClass.id");

		// Note - Removed so we can Support cases where an account can have a sub-account relationship across asset classes
		// If it's invalid assignment, will catch it during processing
	}


	private void validateSecuritySelection(InvestmentAccountAssetClassPositionAllocation bean) {
		ValidationUtils.assertNotNull(bean.getStartDate(), "Start Date is Required", "startDate");
		ValidationUtils.assertNotNull(bean.getSecurity(), "Investment Security is Required", "security.id");
		ValidationUtils.assertTrue(InvestmentUtils.isSecurityActiveOn(bean.getSecurity(), bean.getStartDate()), "Selected security [" + bean.getSecurity().getLabel()
				+ "] is not active on Start Date [" + DateUtils.fromDateShort(bean.getStartDate()) + "].", "security.id");
		if (bean.getEndDate() != null) {
			ValidationUtils.assertTrue(InvestmentUtils.isSecurityActiveOn(bean.getSecurity(), bean.getStartDate()), "Selected security [" + bean.getSecurity().getLabel()
					+ "] is not active on Start Date [" + DateUtils.fromDateShort(bean.getEndDate()) + "].", "security.id");
		}
	}
}
