package com.clifton.investment.account.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class InvestmentAccountTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "description,name")
	private String searchPattern;

	@SearchField
	private String description;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] names;

	@SearchField
	private String accountNumberLabel;

	@SearchField
	private String accountNumber2Label;

	@SearchField
	private String contractCategory;


	@SearchField
	private Boolean ourAccount;

	@SearchField
	private Boolean excludedAccount;

	@SearchField
	private Boolean contractRequired;

	@SearchField
	private Boolean aliasAllowed;

	@SearchField
	private Boolean otc;

	@SearchField
	private Boolean defaultExchangeRateSourceCompanySameAsIssuer;

	@SearchField
	private Boolean requirePositionClosingFromSameJournalType;

	@SearchField
	private Boolean executionWithIssuerRequired;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String[] getNames() {
		return this.names;
	}


	public void setNames(String[] names) {
		this.names = names;
	}


	public String getAccountNumberLabel() {
		return this.accountNumberLabel;
	}


	public void setAccountNumberLabel(String accountNumberLabel) {
		this.accountNumberLabel = accountNumberLabel;
	}


	public String getAccountNumber2Label() {
		return this.accountNumber2Label;
	}


	public void setAccountNumber2Label(String accountNumber2Label) {
		this.accountNumber2Label = accountNumber2Label;
	}


	public String getContractCategory() {
		return this.contractCategory;
	}


	public void setContractCategory(String contractCategory) {
		this.contractCategory = contractCategory;
	}


	public Boolean getOurAccount() {
		return this.ourAccount;
	}


	public void setOurAccount(Boolean ourAccount) {
		this.ourAccount = ourAccount;
	}


	public Boolean getExcludedAccount() {
		return this.excludedAccount;
	}


	public void setExcludedAccount(Boolean excludedAccount) {
		this.excludedAccount = excludedAccount;
	}


	public Boolean getContractRequired() {
		return this.contractRequired;
	}


	public void setContractRequired(Boolean contractRequired) {
		this.contractRequired = contractRequired;
	}


	public Boolean getAliasAllowed() {
		return this.aliasAllowed;
	}


	public void setAliasAllowed(Boolean aliasAllowed) {
		this.aliasAllowed = aliasAllowed;
	}


	public Boolean getOtc() {
		return this.otc;
	}


	public void setOtc(Boolean otc) {
		this.otc = otc;
	}


	public Boolean getDefaultExchangeRateSourceCompanySameAsIssuer() {
		return this.defaultExchangeRateSourceCompanySameAsIssuer;
	}


	public void setDefaultExchangeRateSourceCompanySameAsIssuer(Boolean defaultExchangeRateSourceCompanySameAsIssuer) {
		this.defaultExchangeRateSourceCompanySameAsIssuer = defaultExchangeRateSourceCompanySameAsIssuer;
	}


	public Boolean getRequirePositionClosingFromSameJournalType() {
		return this.requirePositionClosingFromSameJournalType;
	}


	public void setRequirePositionClosingFromSameJournalType(Boolean requirePositionClosingFromSameJournalType) {
		this.requirePositionClosingFromSameJournalType = requirePositionClosingFromSameJournalType;
	}


	public Boolean getExecutionWithIssuerRequired() {
		return this.executionWithIssuerRequired;
	}


	public void setExecutionWithIssuerRequired(Boolean executionWithIssuerRequired) {
		this.executionWithIssuerRequired = executionWithIssuerRequired;
	}
}
