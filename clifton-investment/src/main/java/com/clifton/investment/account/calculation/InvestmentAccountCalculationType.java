package com.clifton.investment.account.calculation;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>InvestmentAccountCalculationType</code> table represents various calculation types we use for accounts
 * Examples: AUM, Projected Revenue.
 * <p>
 * When processed results are saved in the {@link com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot} and {@link InvestmentAccountCalculationSnapshotDetail} tables
 *
 * @author manderson
 */
@CacheByName
public class InvestmentAccountCalculationType extends NamedEntity<Short> {

	/**
	 * The base currency of the calculations.  Usually just USD
	 * So we can report across all accounts in the same currency
	 */
	private InvestmentSecurity baseCurrency;


	/**
	 * The system bean of interface {@link com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessor}
	 * that handles processing all accounts that apply for a given date
	 */
	private SystemBean processorSystemBean;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabelWithCurrency() {
		if (getBaseCurrency() != null) {
			return getName() + " (" + getBaseCurrency().getSymbol() + ")";
		}
		return getName();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(InvestmentSecurity baseCurrency) {
		this.baseCurrency = baseCurrency;
	}


	public SystemBean getProcessorSystemBean() {
		return this.processorSystemBean;
	}


	public void setProcessorSystemBean(SystemBean processorSystemBean) {
		this.processorSystemBean = processorSystemBean;
	}
}
