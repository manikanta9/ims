package com.clifton.investment.account.assetclass.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class InvestmentAccountAssetClassBenchmarkSearchForm extends BaseAuditableEntitySearchForm {

	// InvestmentAccountAssetClassID
	@SearchField(searchField = "referenceOne.id")
	private Integer referenceOneId;

	// InvestmentSecurityID
	@SearchField(searchField = "referenceTwo.id")
	private Integer referenceTwoId;

	// Custom Search Fields
	private Date activeOnDate;
	// If true will return all benchmarks active on any date of the month up until the activeOnDate field
	private boolean activeMonthToDate;

	@SearchField
	private Date startDate;

	@SearchField
	private Date endDate;


	public Integer getReferenceOneId() {
		return this.referenceOneId;
	}


	public void setReferenceOneId(Integer referenceOneId) {
		this.referenceOneId = referenceOneId;
	}


	public Integer getReferenceTwoId() {
		return this.referenceTwoId;
	}


	public void setReferenceTwoId(Integer referenceTwoId) {
		this.referenceTwoId = referenceTwoId;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isActiveMonthToDate() {
		return this.activeMonthToDate;
	}


	public void setActiveMonthToDate(boolean activeMonthToDate) {
		this.activeMonthToDate = activeMonthToDate;
	}
}
