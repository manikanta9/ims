package com.clifton.investment.account.assetclass.rebalance.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;

import java.util.Date;


/**
 * The <code>InvestmentAccountRebalanceHistorySearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentAccountRebalanceHistorySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "investmentAccount.id", sortField = "investmentAccount.number")
	private Integer investmentAccountId;

	@SearchField
	private Date rebalanceDate;

	@SearchField
	private RebalanceCalculationTypes rebalanceCalculationType;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Date getRebalanceDate() {
		return this.rebalanceDate;
	}


	public void setRebalanceDate(Date rebalanceDate) {
		this.rebalanceDate = rebalanceDate;
	}


	public RebalanceCalculationTypes getRebalanceCalculationType() {
		return this.rebalanceCalculationType;
	}


	public void setRebalanceCalculationType(RebalanceCalculationTypes rebalanceCalculationType) {
		this.rebalanceCalculationType = rebalanceCalculationType;
	}
}
