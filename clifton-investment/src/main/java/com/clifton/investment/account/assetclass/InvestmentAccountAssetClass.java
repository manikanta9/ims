package com.clifton.investment.account.assetclass;


import com.clifton.core.beans.hierarchy.HierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustment;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentAware;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentAwareAssignment;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountAssetClass</code> class associates an asset class with an investment account
 * and defines additional attributes specific to the service provided by the investment account (PIOS, etc.)
 *
 * @author Mary Anderson
 */
public class InvestmentAccountAssetClass extends HierarchicalEntity<InvestmentAccountAssetClass, Integer> implements SystemColumnCustomValueAware, InvestmentTargetExposureAdjustmentAware<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> {

	public static final String TABLE_NAME = "InvestmentAccountAssetClass";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * The Clifton Account to which this asset class assignment is for;
	 */
	private InvestmentAccount account;

	/**
	 * An optional display name that can be used in user interface components and reports to replace the InvestmentAssetClass name value.
	 * This allows for changes in naming conventions without modification of the InvestmentAssetClass entity.
	 */
	private String displayName;

	/**
	 * The asset class assigned to the account
	 */
	private InvestmentAssetClass assetClass;

	private Short order;

	private boolean inactive;

	/**
	 * Will be copied to PIOS runs ProductOverlayAssetClass table to exclude
	 * from reports
	 */
	private boolean privateAssetClass;

	/**
	 * If true, is a rollup asset class.
	 * Account Asset Classes that are rollups can be assigned as parents.  The can also be children to other rollups.
	 * asset class and order fields as well as rebalancing section are editable
	 * target and fund cash percent fields are read only and summations of child asset classes: need to be updated on child change in the same transaction
	 */
	private boolean rollupAssetClass;

	/**
	 * If set to true, all other fields (replications, rebalancing, etc) are cleared, targets are set to 0
	 * The asset class can be allocated to by managers, but can't be used by other asset classes for TFA, etc.
	 * During PIOS runs, it will be skipped in asset class
	 * processing and will not show up in exposure.  Any $ allocated to it is ignored.
	 * Sample Client Account that uses this is GuideStone.
	 */
	private boolean excludeFromExposure;

	/**
	 * Temporary Solution for http://jira/browse/PRODUCT-89
	 * According to Andy, only Futures should be included in the overlay exposure, but
	 * until we can come up we a good rule for how this should work and since it's needed for GBR now
	 * adding this flag to always set overlay exposure for the asset class to 0 if this option is selected
	 */
	private boolean excludeFromOverlayExposure;

	/**
	 * Optional Replication Info
	 * NOT Used for IsRollupAssetClass
	 */
	private InvestmentReplication primaryReplication;
	private InvestmentReplication secondaryReplication;
	private BigDecimal secondaryReplicationAmount;

	/**
	 * Do not apply synthetic adjustment or duration adjustments for calculating contract values
	 */
	private boolean doNotAdjustContractValue;

	/**
	 * Used to flag asset classes as those that may duplicate the use of a contract.
	 * <p>
	 * Positions assigned to this replication are excluded from:
	 * Contract Splitting with other asset classes where this flag is not set
	 * Portfolio Run Contracts vs. Account Positions to avoid double counting
	 * Trade Entry is also disabled for this asset class and buys/sells are updated based on the entry in a non-disabled asset class
	 * <p>
	 * For example, Defensive Equity:
	 * matching replications in other asset classes contain the exposure for the options, however, the "Options" asset class
	 * would re-use those contracts to include the market value.  Since we trade on  the matching replication, the Options asset class
	 * gets this flag set so that we can duplicate the use of the same contracts.  This flag is also used to make trading on this asset class
	 * not allowed so that when trading on the matching replication, this one gets a copy of the trade for it's impact but we only have the one trade so as not to enter
	 * double trades.
	 */
	private boolean replicationPositionExcludedFromCount;

	/**
	 * Use Actual for calculating Matching Replication Targets
	 * If unchecked, uses Target
	 */
	private boolean useActualForMatchingReplication;

	private InvestmentSecurity benchmarkSecurity;

	/**
	 * Used to determine the benchmark duration for the asset class
	 */
	private InvestmentSecurity benchmarkDurationSecurity;

	/**
	 * By default, the benchmark security will use the Duration MarketDataField value for valuation.
	 * This property allows the field to be overridden on a per client account basis to be used by the benchmark security
	 * and any replication within this asset class. The replications' securities and benchmark should all use the same field.
	 */
	private String durationFieldNameOverride;

	/**
	 * Target Exposure
	 */
	private BigDecimal assetClassPercentage;

	/**
	 * Cash Allocation
	 */
	private BigDecimal cashPercentage;

	/**
	 * Target Exposure Adjustment : persisted as JSON
	 */
	private InvestmentTargetExposureAdjustment targetExposureAdjustment;
	/**
	 * Fund Cash Adjustment : persisted as JSON
	 */
	private InvestmentTargetExposureAdjustment cashAdjustment;

	/**
	 * Target Exposure and Cash Adjustment offsets are stored together. The assignments
	 * can be filtered using {@link InvestmentAccountAssetClassAssignment#isCashAllocation()};
	 */
	private List<InvestmentAccountAssetClassAssignment> targetExposureAssignmentCombinedList;


	/**
	 * All values are entered/stored as positive values.
	 * Min values are actually negative values, that we can convert when
	 * we need to use it.
	 * <p>
	 * If the rebalance allocation isn't fixed, then it's considered proportional.
	 * Proportional means the actual percentage entered is a percentage of the asset class adjusted target
	 */
	private boolean rebalanceAllocationFixed;
	private BigDecimal rebalanceAllocationMin;
	private BigDecimal rebalanceAllocationMax;
	private BigDecimal rebalanceAllocationAbsoluteMin;
	private BigDecimal rebalanceAllocationAbsoluteMax;
	private RebalanceActions rebalanceTriggerAction;
	/**
	 * See account 445000 as an example
	 * Defines rebalancing targets only for a sub-set of asset classes (investable asset classes that we do replicate).
	 * Percentage value is used as target exposure for PIOS exposure reports that only report against these asset classes
	 * During PIOS runs, if set for any asset class, sum across all asset classes must equal 100%
	 */
	private BigDecimal rebalanceExposureTarget;
	/**
	 * Optional: If set, calculates rebalance adjusted values based upon this benchmark's performance,
	 * else uses the asset class benchmark.
	 */
	private InvestmentSecurity rebalanceBenchmarkSecurity;
	private boolean skipRebalanceCashAdjustment;
	private ClientDirectedCashOptions clientDirectedCashOption = ClientDirectedCashOptions.NONE;
	private BigDecimal clientDirectedCashAmount;
	private Date clientDirectedCashDate;
	/**
	 * A List of custom column values for this security (field are assigned and vary by investment hierarchy)
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;
	private PerformanceOverlayTargetSources performanceOverlayTargetSource;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if primary Replication is defined
	 */
	public boolean isReplicationUsed() {
		return getPrimaryReplication() != null;
	}


	public boolean isRebalancingUsed() {
		return getRebalanceAllocationMin() != null;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return getAccount();
	}


	@Override
	public String getLabel() {
		return ObjectUtils.coalesce(getDisplayName(), getAssetClass().getName());
	}


	public boolean isClientDirectedCashUsed() {
		return ClientDirectedCashOptions.NONE != getClientDirectedCashOption();
	}


	@Override
	public boolean isTargetExposureAdjustmentUsed() {
		// Overridden since default interface methods are not supported by introspector
		return getTargetExposureAdjustment() != null && (InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(getTargetExposureAdjustment().getTargetExposureAdjustmentType()) || !StringUtils.isEmpty(getTargetExposureAdjustment().getJsonValue()));
	}


	@Override
	public BigDecimal getTargetPercent() {
		return getAssetClassPercentage();
	}


	@Override
	public List<InvestmentAccountAssetClassAssignment> getTargetAdjustmentAssignmentList() {
		// Overridden since default interface methods are not supported by introspector
		return CollectionUtils.getFiltered(getTargetExposureAssignmentCombinedList(), assignment -> !assignment.isCashAllocation());
	}


	@Override
	public boolean isCashAdjustmentUsed() {
		// Overridden since default interface methods are not supported by introspector
		return getCashAdjustment() != null && (InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(getCashAdjustment().getTargetExposureAdjustmentType()) || !StringUtils.isEmpty(getCashAdjustment().getJsonValue()));
	}


	@Override
	public BigDecimal getCashPercent() {
		return getCashPercentage();
	}


	@Override
	public List<InvestmentAccountAssetClassAssignment> getCashAdjustmentAssignmentList() {
		// Overridden since default interface methods are not supported by introspector
		return CollectionUtils.getFiltered(getTargetExposureAssignmentCombinedList(), InvestmentTargetExposureAdjustmentAwareAssignment::isCashAllocation);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getAccount() {
		return this.account;
	}


	public void setAccount(InvestmentAccount account) {
		this.account = account;
	}


	public String getDisplayName() {
		return this.displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public InvestmentAssetClass getAssetClass() {
		return this.assetClass;
	}


	public void setAssetClass(InvestmentAssetClass assetClass) {
		this.assetClass = assetClass;
	}


	public InvestmentReplication getPrimaryReplication() {
		return this.primaryReplication;
	}


	public void setPrimaryReplication(InvestmentReplication primaryReplication) {
		this.primaryReplication = primaryReplication;
	}


	public InvestmentReplication getSecondaryReplication() {
		return this.secondaryReplication;
	}


	public void setSecondaryReplication(InvestmentReplication secondaryReplication) {
		this.secondaryReplication = secondaryReplication;
	}


	public BigDecimal getSecondaryReplicationAmount() {
		return this.secondaryReplicationAmount;
	}


	public void setSecondaryReplicationAmount(BigDecimal secondaryReplicationAmount) {
		this.secondaryReplicationAmount = secondaryReplicationAmount;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public BigDecimal getAssetClassPercentage() {
		return this.assetClassPercentage;
	}


	public void setAssetClassPercentage(BigDecimal assetClassPercentage) {
		this.assetClassPercentage = assetClassPercentage;
	}


	public BigDecimal getCashPercentage() {
		return this.cashPercentage;
	}


	public void setCashPercentage(BigDecimal cashPercentage) {
		this.cashPercentage = cashPercentage;
	}


	@Override
	public InvestmentTargetExposureAdjustment getTargetExposureAdjustment() {
		return this.targetExposureAdjustment;
	}


	@Override
	public void setTargetExposureAdjustment(InvestmentTargetExposureAdjustment targetExposureAdjustment) {
		this.targetExposureAdjustment = targetExposureAdjustment;
	}


	@Override
	public InvestmentTargetExposureAdjustment getCashAdjustment() {
		return this.cashAdjustment;
	}


	@Override
	public void setCashAdjustment(InvestmentTargetExposureAdjustment cashAdjustment) {
		this.cashAdjustment = cashAdjustment;
	}


	@Override
	public List<InvestmentAccountAssetClassAssignment> getTargetExposureAssignmentCombinedList() {
		return this.targetExposureAssignmentCombinedList;
	}


	@Override
	public void setTargetExposureAssignmentCombinedList(List<InvestmentAccountAssetClassAssignment> targetExposureAssignmentCombinedList) {
		this.targetExposureAssignmentCombinedList = targetExposureAssignmentCombinedList;
	}


	public boolean isRebalanceAllocationFixed() {
		return this.rebalanceAllocationFixed;
	}


	public void setRebalanceAllocationFixed(boolean rebalanceAllocationFixed) {
		this.rebalanceAllocationFixed = rebalanceAllocationFixed;
	}


	public BigDecimal getRebalanceAllocationMin() {
		return this.rebalanceAllocationMin;
	}


	public void setRebalanceAllocationMin(BigDecimal rebalanceAllocationMin) {
		this.rebalanceAllocationMin = rebalanceAllocationMin;
	}


	public BigDecimal getRebalanceAllocationMax() {
		return this.rebalanceAllocationMax;
	}


	public void setRebalanceAllocationMax(BigDecimal rebalanceAllocationMax) {
		this.rebalanceAllocationMax = rebalanceAllocationMax;
	}


	public BigDecimal getRebalanceAllocationAbsoluteMin() {
		return this.rebalanceAllocationAbsoluteMin;
	}


	public void setRebalanceAllocationAbsoluteMin(BigDecimal rebalanceAllocationAbsoluteMin) {
		this.rebalanceAllocationAbsoluteMin = rebalanceAllocationAbsoluteMin;
	}


	public BigDecimal getRebalanceAllocationAbsoluteMax() {
		return this.rebalanceAllocationAbsoluteMax;
	}


	public void setRebalanceAllocationAbsoluteMax(BigDecimal rebalanceAllocationAbsoluteMax) {
		this.rebalanceAllocationAbsoluteMax = rebalanceAllocationAbsoluteMax;
	}


	public RebalanceActions getRebalanceTriggerAction() {
		return this.rebalanceTriggerAction;
	}


	public void setRebalanceTriggerAction(RebalanceActions rebalanceTriggerAction) {
		this.rebalanceTriggerAction = rebalanceTriggerAction;
	}


	public Short getOrder() {
		return this.order;
	}


	public void setOrder(Short order) {
		this.order = order;
	}


	public boolean isRollupAssetClass() {
		return this.rollupAssetClass;
	}


	public void setRollupAssetClass(boolean rollupAssetClass) {
		this.rollupAssetClass = rollupAssetClass;
	}


	public InvestmentSecurity getRebalanceBenchmarkSecurity() {
		return this.rebalanceBenchmarkSecurity;
	}


	public void setRebalanceBenchmarkSecurity(InvestmentSecurity rebalanceBenchmarkSecurity) {
		this.rebalanceBenchmarkSecurity = rebalanceBenchmarkSecurity;
	}


	@Override
	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}


	public InvestmentSecurity getBenchmarkDurationSecurity() {
		return this.benchmarkDurationSecurity;
	}


	public void setBenchmarkDurationSecurity(InvestmentSecurity benchmarkDurationSecurity) {
		this.benchmarkDurationSecurity = benchmarkDurationSecurity;
	}


	public String getDurationFieldNameOverride() {
		return this.durationFieldNameOverride;
	}


	public void setDurationFieldNameOverride(String durationFieldNameOverride) {
		this.durationFieldNameOverride = durationFieldNameOverride;
	}


	public boolean isExcludeFromExposure() {
		return this.excludeFromExposure;
	}


	public void setExcludeFromExposure(boolean excludeFromExposure) {
		this.excludeFromExposure = excludeFromExposure;
	}


	public BigDecimal getRebalanceExposureTarget() {
		return this.rebalanceExposureTarget;
	}


	public void setRebalanceExposureTarget(BigDecimal rebalanceExposureTarget) {
		this.rebalanceExposureTarget = rebalanceExposureTarget;
	}


	public boolean isSkipRebalanceCashAdjustment() {
		return this.skipRebalanceCashAdjustment;
	}


	public void setSkipRebalanceCashAdjustment(boolean skipRebalanceCashAdjustment) {
		this.skipRebalanceCashAdjustment = skipRebalanceCashAdjustment;
	}


	public boolean isDoNotAdjustContractValue() {
		return this.doNotAdjustContractValue;
	}


	public void setDoNotAdjustContractValue(boolean doNotAdjustContractValue) {
		this.doNotAdjustContractValue = doNotAdjustContractValue;
	}


	public boolean isUseActualForMatchingReplication() {
		return this.useActualForMatchingReplication;
	}


	public void setUseActualForMatchingReplication(boolean useActualForMatchingReplication) {
		this.useActualForMatchingReplication = useActualForMatchingReplication;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	public PerformanceOverlayTargetSources getPerformanceOverlayTargetSource() {
		return this.performanceOverlayTargetSource;
	}


	public void setPerformanceOverlayTargetSource(PerformanceOverlayTargetSources performanceOverlayTargetSource) {
		this.performanceOverlayTargetSource = performanceOverlayTargetSource;
	}


	public boolean isExcludeFromOverlayExposure() {
		return this.excludeFromOverlayExposure;
	}


	public void setExcludeFromOverlayExposure(boolean excludeFromOverlayExposure) {
		this.excludeFromOverlayExposure = excludeFromOverlayExposure;
	}


	public boolean isPrivateAssetClass() {
		return this.privateAssetClass;
	}


	public void setPrivateAssetClass(boolean privateAssetClass) {
		this.privateAssetClass = privateAssetClass;
	}


	public ClientDirectedCashOptions getClientDirectedCashOption() {
		return this.clientDirectedCashOption;
	}


	public void setClientDirectedCashOption(ClientDirectedCashOptions clientDirectedCashOption) {
		this.clientDirectedCashOption = clientDirectedCashOption;
	}


	public BigDecimal getClientDirectedCashAmount() {
		return this.clientDirectedCashAmount;
	}


	public void setClientDirectedCashAmount(BigDecimal clientDirectedCashAmount) {
		this.clientDirectedCashAmount = clientDirectedCashAmount;
	}


	public Date getClientDirectedCashDate() {
		return this.clientDirectedCashDate;
	}


	public void setClientDirectedCashDate(Date clientDirectedCashDate) {
		this.clientDirectedCashDate = clientDirectedCashDate;
	}


	public boolean isReplicationPositionExcludedFromCount() {
		return this.replicationPositionExcludedFromCount;
	}


	public void setReplicationPositionExcludedFromCount(boolean replicationPositionExcludedFromCount) {
		this.replicationPositionExcludedFromCount = replicationPositionExcludedFromCount;
	}


	public enum RebalanceActions {
		DO_NOTHING(false), CONTACT_CLIENT(true), REBALANCE(true);


		private final boolean generateWarning;


		RebalanceActions(boolean generateWarning) {
			this.generateWarning = generateWarning;
		}


		public boolean isGenerateWarning() {
			return this.generateWarning;
		}
	}


	/**
	 * Client Directed Cash Options
	 * Used to suggest trades (Target) only when Client tells us, (Uses TFA for dates after clientDirectedCashDate)
	 * Used to suggest trades (Target) only when Client tells us, but account for duration updates (Uses TFA for dates after clientDirectedCashDate, however uses Previous Duration in target exposure calc so can trade based on duration change only)
	 * or to add/remove exposure (Constant) when Client tells us
	 */
	public enum ClientDirectedCashOptions {
		NONE, TARGET(true, false), TARGET_PREVIOUS_DURATION(true, true), CONSTANT;


		private final boolean useTargetFollowsActualAfterDate;
		private final boolean usePreviousDurationForTargetFollowsActual;


		ClientDirectedCashOptions() {
			this(false, false);
		}


		ClientDirectedCashOptions(boolean useTargetFollowsActualAfterDate, boolean usePreviousDurationForTargetFollowsActual) {
			this.useTargetFollowsActualAfterDate = useTargetFollowsActualAfterDate;
			this.usePreviousDurationForTargetFollowsActual = usePreviousDurationForTargetFollowsActual;
		}


		public boolean isUseTargetFollowsActualAfterDate() {
			return this.useTargetFollowsActualAfterDate;
		}


		public boolean isUsePreviousDurationForTargetFollowsActual() {
			return this.usePreviousDurationForTargetFollowsActual;
		}
	}


	/**
	 * PerformanceSummaries - OverlayTargetSource
	 * Most cases, use Overlay Target field, but some cases
	 * 186100 Duke "Currency" accounts, use actual exposure for Overlay Target for performance summaries
	 */
	public enum PerformanceOverlayTargetSources {
		TARGET_EXPOSURE, ACTUAL_EXPOSURE, MARKET_VALUE_INCLUDE_CASH
	}
}
