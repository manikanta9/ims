package com.clifton.investment.account.targetexposure;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;


/**
 * @author manderson
 */
public interface InvestmentAccountTargetExposureHandler<T extends InvestmentTargetExposureAdjustmentAware<T, A>, A extends InvestmentTargetExposureAdjustmentAwareAssignment<T, A>> {

	/**
	 * Returns true if a {@link InvestmentTargetExposureAdjustmentAware} entity uses exposure adjustment assignments.
	 */
	public boolean isTargetExposureAdjustmentAssignmentListUsed(InvestmentTargetExposureAdjustment adjustment);


	/**
	 * Call from save method for TargetExposureAdjustmentAware (i.e. see {@link com.clifton.investment.account.assetclass.InvestmentAccountAssetClass}
	 * which validates Target Exposure Adjustment options and assignment list
	 */
	public void validateTargetExposureAdjustmentTypeOptions(T bean, String typeLabel, ReadOnlyDAO<A> assignmentDAO);


	/**
	 * Called from save method, dao update methods when necessary to verify first that the TargetExposureAdjustmentAware entity isn't used as a child
	 * reference in the assignment table (i.e. can't de-activate it if it's used, can't also set a target exposure adjustment if it's target is already being adjusted by something else)
	 */
	public void validateTargetExposureAdjustmentAssignmentUnused(T bean, String typeLabel, String actionDescription, ReadOnlyDAO<A> assignmentDAO);


	/**
	 * For the given {@link InvestmentTargetExposureAdjustmentAware} entity and run information, calculates the adjusted target value
	 */
	public BigDecimal calculateTargetExposureAdjustedAmount(T bean, InvestmentTargetExposureAdjustment adjustment, BigDecimal currentTargetAmount, BigDecimal currentActualAmount, Date date, BigDecimal totalPortfolioValue, boolean marketOnCloseAdjustmentsIncluded);


	/**
	 * Once we have the adjustment amount, checks the assignments from the {@TargetExposureAdjustmentAware} bean to properly allocate the differences
	 * based.  Proportional Map Override is optional (currently used for LDI only) where the proportional options are based off of the point's Liability DV01 values
	 * not each point's hedge target %.  For PIOS, this map is not populated, because proportional allocations use the asset class defined targets.
	 */
	public Map<T, BigDecimal> calculateTargetExposureAdjustmentMap(T bean, InvestmentTargetExposureAdjustment adjustment, BigDecimal totalAdjustment, Map<Serializable, BigDecimal> proportionalMapOverride);
}
