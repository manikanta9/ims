package com.clifton.investment.account.calculation.processor;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;

import java.util.Date;


/**
 * The <code>InvestmentAccountCalculationProcessorCommand</code> defines what should be processed (dates, calculation type)
 *
 * @author manderson
 */
public class InvestmentAccountCalculationProcessorCommand {

	/**
	 * The calculation type we are processing.  Used to find the processor bean - required
	 */
	private short calculationTypeId;

	private boolean synchronous;

	private boolean reprocessExisting;

	private Date startSnapshotDate;
	private Date endSnapshotDate;


	// If Previewing - Required
	private Integer investmentAccountId;

	// Ability to limit processing to accounts within a group
	private Integer investmentAccountGroupId;

	// allows to send "live" updates of rebuild status back to the caller (assume the caller sets this field)
	private Status status;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getRunId() {
		StringBuilder runId = new StringBuilder("");
		runId.append("CalcType_").append(getCalculationTypeId());
		runId.append("-").append(DateUtils.fromDateShort(getStartSnapshotDate()));
		runId.append("-").append(DateUtils.fromDateShort(getEndSnapshotDate()));
		if (getInvestmentAccountId() != null) {
			runId.append("-ACCT-").append(getInvestmentAccountId());
		}
		else if (getInvestmentAccountGroupId() != null) {
			runId.append("-GRP-").append(getInvestmentAccountGroupId());
		}
		return runId.toString();
	}


	public void setSnapshotDate(Date date) {
		setStartSnapshotDate(date);
		setEndSnapshotDate(date);
	}


	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	public short getCalculationTypeId() {
		return this.calculationTypeId;
	}


	public void setCalculationTypeId(short calculationTypeId) {
		this.calculationTypeId = calculationTypeId;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public boolean isReprocessExisting() {
		return this.reprocessExisting;
	}


	public void setReprocessExisting(boolean reprocessExisting) {
		this.reprocessExisting = reprocessExisting;
	}


	public Date getStartSnapshotDate() {
		return this.startSnapshotDate;
	}


	public void setStartSnapshotDate(Date startSnapshotDate) {
		this.startSnapshotDate = startSnapshotDate;
	}


	public Date getEndSnapshotDate() {
		return this.endSnapshotDate;
	}


	public void setEndSnapshotDate(Date endSnapshotDate) {
		this.endSnapshotDate = endSnapshotDate;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
