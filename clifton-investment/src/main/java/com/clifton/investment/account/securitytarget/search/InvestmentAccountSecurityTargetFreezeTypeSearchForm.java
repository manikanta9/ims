package com.clifton.investment.account.securitytarget.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * <code>InvestmentAccountSecurityTargetFreezeTypeSearchForm</code> represents the search configuration for
 * {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetFreezeType} entities.
 *
 * @author NickK
 */
public class InvestmentAccountSecurityTargetFreezeTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Boolean noteRequired;

	@SearchField
	private Boolean causeRequired;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getNoteRequired() {
		return this.noteRequired;
	}


	public void setNoteRequired(Boolean noteRequired) {
		this.noteRequired = noteRequired;
	}


	public Boolean getCauseRequired() {
		return this.causeRequired;
	}


	public void setCauseRequired(Boolean causeRequired) {
		this.causeRequired = causeRequired;
	}
}
