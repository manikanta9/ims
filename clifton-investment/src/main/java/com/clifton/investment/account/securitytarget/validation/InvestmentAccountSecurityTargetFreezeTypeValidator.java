package com.clifton.investment.account.securitytarget.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetFreezeType;
import org.springframework.stereotype.Component;


/**
 * <code>InvestmentAccountSecurityTargetFreezeTypeValidator</code> is a validator for a saved {@link InvestmentAccountSecurityTargetFreezeType}.
 *
 * @author NickK
 */
@Component
public class InvestmentAccountSecurityTargetFreezeTypeValidator extends SelfRegisteringDaoValidator<InvestmentAccountSecurityTargetFreezeType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentAccountSecurityTargetFreezeType bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("Deleting System Defined Investment Account Security Target Freeze Type is not allowed.");
			}
			if (config.isInsert()) {
				throw new ValidationException("Adding new System Defined Investment Account Security Target Freeze Type is not allowed.");
			}
		}
		if (config.isUpdate()) {
			InvestmentAccountSecurityTargetFreezeType original = getOriginalBean(bean);
			if (original.isSystemDefined() != bean.isSystemDefined()) {
				throw new FieldValidationException("You cannot edit the System Defined field for Investment Account Security Target Freeze Types", "systemDefined");
			}
			if (original.isSystemDefined()) {
				ValidationUtils.assertTrue(original.getName().equals(bean.getName()), "System Defined Investment Account Security Target Freeze Type Names cannot be changed.", "name");
			}
		}
	}
}
