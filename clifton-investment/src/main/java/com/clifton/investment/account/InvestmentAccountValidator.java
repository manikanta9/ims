package com.clifton.investment.account;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContractParty;
import com.clifton.business.contract.BusinessContractPartyRole;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.service.BusinessServiceLevelTypes;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipValidator;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>InvestmentAccountValidator</code> validates InvestmentAccount fields on insert/update
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentAccountValidator extends SelfRegisteringDaoValidator<InvestmentAccount> {

	private BusinessContractService businessContractService;
	private InvestmentAccountRelationshipValidator investmentAccountRelationshipValidator;

	private SqlHandler sqlHandler;

	/**
	 * Regex for characters that need to be removed from the account number.
	 */
	private static final String ACCOUNT_NUMBER_REGEX = "[-]";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentAccount account, DaoEventTypes config) throws ValidationException {
		if (account.getBusinessService() != null) {
			ValidationUtils.assertTrue(account.getType().isOurAccount(), "Business Service selection is only available for our/client accounts");
			ValidationUtils.assertTrue(account.getBusinessService().getServiceLevelType().isAssignableService(), "Selected service [" + account.getBusinessService().getNameExpanded() + "] is not assignable to accounts");
			for (InvestmentAccountBusinessService service : CollectionUtils.getIterable(account.getBusinessServiceList())) {
				service.setInvestmentAccount(account);
				ValidationUtils.assertTrue(service.getBusinessService().getServiceLevelType() == BusinessServiceLevelTypes.SERVICE_COMPONENT && service.getBusinessService().getParent() != null, "Selected service component [" + service.getBusinessService().getNameExpanded() + " is not applicable selection (either not a service component or missing parent service)");
				ValidationUtils.assertTrue(account.getBusinessService().equals(service.getBusinessService().getParent()), "Selected service component [" + service.getBusinessService().getNameExpanded() + "] is not applicable selection for service [" + account.getBusinessService().getNameExpanded() + "].");
				ValidationUtils.assertNotNull(service.getStartDate(), "Start Date is required for each service component.", "startDate");
			}
		}
		else {
			ValidationUtils.assertEmpty(account.getBusinessServiceList(), "Service Components can not be selected if the account doesn't have a service selected");
		}

		if (account.getType().isContractRequired() && account.getBusinessContract() == null) {
			throw new FieldValidationException("Business contract is required for accounts of type: " + account.getType().getName(), "businessContract");
		}
		if (account.getBusinessContract() != null) {
			BusinessCompany contractCompany = account.getBusinessContract().getCompany();
			BusinessCompany clientCompany = account.getBusinessClient().getCompany();
			// If not the same company and account client company has a parent and contract company doesn't equal the parent, throw exception...
			if (!clientCompany.equals(contractCompany) && (clientCompany.getParent() == null || !clientCompany.getParent().equals(contractCompany))) {
				throw new FieldValidationException("When business contract is specified, it must belong to the same Client's company as that of the Account's Client (or Parent).", "businessContract");
			}
			if (account.getType().getContractCategory() != null) {
				String contractCategory = account.getBusinessContract().getContractType().getCategoryName();
				if (!account.getType().getContractCategory().equals(contractCategory)) {
					throw new FieldValidationException("Selected contract type category '" + contractCategory + "' does not match selected account type contract category '"
							+ account.getType().getContractCategory() + "'", "businessContract");
				}
			}
			if (account.getType().getContractType() != null) {
				if (!account.getType().getContractType().equals(account.getBusinessContract().getContractType())) {
					throw new FieldValidationException("Selected contract type '" + account.getBusinessContract().getContractType().getName() + "' does not match selected account type contract type '"
							+ account.getType().getContractType().getName() + "'", "businessContract");
				}
			}
			BusinessContractPartyRole issuerRole = account.getType().getIssuerContractPartyRole();
			if (issuerRole != null) {
				List<BusinessContractParty> parties = getBusinessContractService().getBusinessContractPartyListByContract(account.getBusinessContract().getId());
				boolean found = false;
				for (BusinessContractParty party : CollectionUtils.getIterable(parties)) {
					if (party.getRole().equals(issuerRole) && account.getIssuingCompany().equals(party.getCompany())) {
						found = true;
						break;
					}
				}
				if (!found) {
					throw new ValidationException("Selected contract must have account issuer as its " + issuerRole.getName());
				}
			}
		}

		if (account.getType().isDefaultExchangeRateSourceCompanySameAsIssuer()) {
			ValidationUtils.assertNotNull(account.getIssuingCompany(), "Issuing Company is required for each Investment Account.", "issuingCompany");
			ValidationUtils.assertNotNull(account.getDefaultExchangeRateSourceCompany(), "FX Source must be specified and must be the same as account's issuer for investment accounts of selected type.", "defaultExchangeRateSourceCompany");
			ValidationUtils.assertTrue(account.getIssuingCompany().isSelfOrAncestor(account.getDefaultExchangeRateSourceCompany()), "FX Source must be the same as account's issuer (or any of its ancestors) for investment accounts of selected type.");
		}

		InvestmentAccount original = account.isNewBean() ? null : getOriginalBean(account);
		if ((original == null || !original.getNumber().equals(account.getNumber())) && (getSqlHandler() != null)) {
			String accountNumber = account.getNumber().replaceAll(ACCOUNT_NUMBER_REGEX, "").replaceAll(" ", "");
			String existingAccountNumber = getSqlHandler().queryForString(new SqlSelectCommand(
					"SELECT COALESCE((SELECT TOP 1 AccountNumber FROM InvestmentAccount WHERE ? = REPLACE(REPLACE(AccountNumber,'-',''),' ','') AND InvestmentAccountID <> ? AND IssuingCompanyID = ?), '')")
					.addStringParameterValue(accountNumber)
					.addIntegerParameterValue(account.isNewBean() ? -10 : account.getId())
					.addIntegerParameterValue(account.getIssuingCompany().getId())
			);

			ValidationUtils.assertTrue(StringUtils.isEmpty(existingAccountNumber), "An account with number [" + existingAccountNumber + "] already exists for [" + account.getIssuingCompany().getLabel()
					+ "].", "number");
		}

		// Cannot assign holding accounts to client's of type "Client Group"
		if (account.getBusinessClient().getCategory().isOurAccountsOnly()) {
			ValidationUtils.assertTrue(account.getType().isOurAccount(), "Clients under category [" + account.getBusinessClient().getCategory().getName() + "] can only have Clifton accounts.");
		}

		if (config.isUpdate() && original != null && !account.getType().equals(original.getType())) {
			// make sure that all existing account relationships are still valid for the new type
			ReadOnlyDAO<InvestmentAccountRelationship> dao = getInvestmentAccountRelationshipValidator().getDaoLocator().locate(InvestmentAccountRelationship.class);
			for (InvestmentAccountRelationship relationship : CollectionUtils.getIterable(dao.findByField("referenceOne.id", account.getId()))) {
				relationship.setReferenceOne(account);
				getInvestmentAccountRelationshipValidator().validate(relationship, config, dao);
			}
			for (InvestmentAccountRelationship relationship : CollectionUtils.getIterable(dao.findByField("referenceTwo.id", account.getId()))) {
				relationship.setReferenceTwo(account);
				getInvestmentAccountRelationshipValidator().validate(relationship, config, dao);
			}
		}

		// default initial margin multiplier to 1 if it's not set
		if (account.getInitialMarginMultiplier() == null) {
			account.setInitialMarginMultiplier(BigDecimal.ONE);
		}

		// If Our Account - Can select AUM Calculator Bean Override or Performance Bean Override
		// Validates the override is different from what is selected on the business service
		if (account.getType().isOurAccount()) {
			if (account.getBusinessService() != null) {
				if (account.getAumCalculatorOverrideBean() != null && CompareUtils.isEqual(account.getAumCalculatorOverrideBean(), account.getBusinessService().getAumCalculatorBean())) {
					throw new ValidationException("Selected AUM Calculator Override matches the calculator defined on the service.  Please clear the override.");
				}
			}
		}
		else {
			account.setAumCalculatorOverrideBean(null);
		}

		if (account.getCompanyPlatform() != null) {
			ValidationUtils.assertFalse(account.getIssuingCompany().getType().isOurCompany(), "Platform selections only apply to holding accounts");
			ValidationUtils.assertEquals(account.getIssuingCompany(), account.getCompanyPlatform().getBusinessCompany(), "The issuing company of the account does not match the company on the selected platform.  Only platforms for the same company as the issuer of the account are allowed.");
		}
		else {
			ValidationUtils.assertFalse(account.isWrapAccount(), "Wrap Account selections can only be applied if a platform is selected.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////          Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipValidator getInvestmentAccountRelationshipValidator() {
		return this.investmentAccountRelationshipValidator;
	}


	public void setInvestmentAccountRelationshipValidator(InvestmentAccountRelationshipValidator investmentAccountRelationshipValidator) {
		this.investmentAccountRelationshipValidator = investmentAccountRelationshipValidator;
	}


	public BusinessContractService getBusinessContractService() {
		return this.businessContractService;
	}


	public void setBusinessContractService(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
