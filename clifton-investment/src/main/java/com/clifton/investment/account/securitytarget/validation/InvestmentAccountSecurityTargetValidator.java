package com.clifton.investment.account.securitytarget.validation;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * <code>InvestmentAccountSecurityTargetValidator</code> is a validator for a saved {@link InvestmentAccountSecurityTarget}.
 * Each item is unique by client {@link com.clifton.investment.account.InvestmentAccount}, underlying {@link com.clifton.investment.instrument.InvestmentSecurity},
 * and start and end dates (start and end dates do not overlap an existing target).
 *
 * @author NickK
 */
@Component
public class InvestmentAccountSecurityTargetValidator extends SelfRegisteringDaoValidator<InvestmentAccountSecurityTarget> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentAccountSecurityTarget bean, DaoEventTypes config) throws ValidationException {
		// Nothing Here: Uses DAO validate method
	}


	@Override
	public void validate(InvestmentAccountSecurityTarget securityTarget, DaoEventTypes config, ReadOnlyDAO<InvestmentAccountSecurityTarget> dao) throws ValidationException {
		ValidationUtils.assertNotNull(securityTarget.getClientInvestmentAccount(), "Client Account is required.");
		ValidationUtils.assertNotNull(securityTarget.getTargetUnderlyingSecurity(), "Underlying Security is required.");
		ValidationUtils.assertMutuallyExclusive("Only Target Underlying Quantity, Target Underlying Notional, or Target Underlying Manager Account can and must be defined.", securityTarget.getTargetUnderlyingNotional(), securityTarget.getTargetUnderlyingQuantity(), securityTarget.getTargetUnderlyingManagerAccount());
		ValidationUtils.assertNotNull(securityTarget.getStartDate(), "Start Date is required.");
		ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(DateUtils.clearTime(securityTarget.getEndDate()), DateUtils.clearTime(securityTarget.getStartDate())), "End Date of client account security target must be after or equal to Start Date.");

		InvestmentAccountSecurityTargetSearchForm searchForm = new InvestmentAccountSecurityTargetSearchForm();
		searchForm.setClientInvestmentAccountId(securityTarget.getClientInvestmentAccount().getId());
		searchForm.setTargetUnderlyingSecurityId(securityTarget.getTargetUnderlyingSecurity().getId());
		List<InvestmentAccountSecurityTarget> existingTargetList = ((AdvancedReadOnlyDAO<InvestmentAccountSecurityTarget, Criteria>) dao).findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));

		for (InvestmentAccountSecurityTarget existingTarget : CollectionUtils.getIterable(existingTargetList)) {
			if (config.isInsert() || !securityTarget.equals(existingTarget)) {
				if (DateUtils.isOverlapInDates(existingTarget.getStartDate(), existingTarget.getEndDate(), securityTarget.getStartDate(), securityTarget.getEndDate())) {
					throw new ValidationException("Client Account Security Target record already exists for this Client Account and Underlying Security for overlapping date range " + DateUtils.fromDateRange(existingTarget.getStartDate(), existingTarget.getEndDate(), true, false));
				}
			}
		}
	}
}
