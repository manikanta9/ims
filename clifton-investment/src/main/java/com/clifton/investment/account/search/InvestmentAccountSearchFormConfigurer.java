package com.clifton.investment.account.search;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipMapping;
import com.clifton.system.schema.column.search.SystemColumnDatedValueAwareSearchFormUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * The <code>InvestmentAccountSearchFormConfigurer</code> class configures search criteria for the InvestmentAccountSearchForm search form.
 * <p>
 * NOTE: didn't use inline class because it's becoming too big
 *
 * @author vgomelsky
 */
public class InvestmentAccountSearchFormConfigurer extends WorkflowAwareSearchFormConfigurer {

	private final BusinessClientService businessClientService;
	private final BusinessCompanyService businessCompanyService;
	private final InvestmentAccountService investmentAccountService;


	public InvestmentAccountSearchFormConfigurer(InvestmentAccountSearchForm searchForm, BusinessClientService businessClientService, WorkflowDefinitionService workflowDefinitionService,
	                                             BusinessCompanyService businessCompanyService, InvestmentAccountService investmentAccountService) {
		super(searchForm, workflowDefinitionService);
		this.businessClientService = businessClientService;
		this.businessCompanyService = businessCompanyService;
		this.investmentAccountService = investmentAccountService;
	}


	private void addPurposeCriteria(String purpose, Short purposeId, Date purposeActiveOnDate, Integer accountId, Integer mainPurposeAccountGroupId, String reference, Criteria criteria) {
		DetachedCriteria mp = DetachedCriteria.forClass(InvestmentAccountRelationship.class, "mp");
		mp.setProjection(Projections.property("id"));
		mp.createAlias("purpose", "p");
		mp.add(Restrictions.eqProperty(reference + ".id", criteria.getAlias() + ".id"));
		if (purpose != null) {
			mp.add(Restrictions.eq("p.name", purpose));
		}
		if (purposeId != null) {
			mp.add(Restrictions.eq("p.id", purposeId));

			if (mainPurposeAccountGroupId != null && "referenceTwo".equals(reference)) {
				DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "link");
				sub.setProjection(Projections.property("id"));
				sub.createAlias("referenceOne", "group");
				sub.createAlias("referenceTwo", "acct");
				sub.add(Restrictions.eqProperty("acct.id", mp.getAlias() + ".referenceOne.id"));
				sub.add(Restrictions.eq("group.id", mainPurposeAccountGroupId));
				mp.add(Subqueries.exists(sub));
			}
		}

		if (purposeActiveOnDate != null) {
			mp.add(ActiveExpressionForDates.forActiveOnDate(true, purposeActiveOnDate));
		}
		if (accountId != null) {
			mp.add(Restrictions.eq(("referenceOne".equals(reference) ? "referenceTwo" : "referenceOne") + ".id", accountId));
		}

		criteria.add(Subqueries.exists(mp));
	}


	private void addPurposeInCriteria(String[] purposes, Date purposeActiveOnDate, Integer accountId, String reference, Criteria criteria) {
		DetachedCriteria mp = DetachedCriteria.forClass(InvestmentAccountRelationship.class, "mp");
		mp.setProjection(Projections.property("id"));
		mp.createAlias("purpose", "p");
		mp.add(Restrictions.eqProperty(reference + ".id", criteria.getAlias() + ".id"));
		if (purposes != null) {
			@SuppressWarnings("UnnecessaryLocalVariable") // Cast ensures that argument is handled as a varargs parameter
					Object[] varargsPurposes = purposes;
			mp.add(Restrictions.in("p.name", varargsPurposes));
		}
		if (purposeActiveOnDate != null) {
			mp.add(ActiveExpressionForDates.forActiveOnDate(true, purposeActiveOnDate));
		}
		if (accountId != null) {
			mp.add(Restrictions.eq(("referenceOne".equals(reference) ? "referenceTwo" : "referenceOne") + ".id", accountId));
		}
		criteria.add(Subqueries.exists(mp));
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		InvestmentAccountSearchForm searchForm = (InvestmentAccountSearchForm) getSortableSearchForm();

		if (searchForm.getInvestmentAccountGroupTypeName() != null) {
			ValidationUtils.assertNull(searchForm.getAccountTypeId(), "Unsupported search: Must use setAccountTypeIds instead of setAccountTypeId if using setInvestmentAccountGroupTypeName.");
			createInvestmentAccountGroupTypeNameCriteria(searchForm, criteria);
		}

		super.configureCriteria(criteria);
		SystemColumnDatedValueAwareSearchFormUtils.configureCriteriaForSystemColumnDatedValueAwareSearchForm(searchForm, criteria);

		if (searchForm.getInvestmentAccountGroupId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "link");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("referenceOne", "group");
			sub.createAlias("referenceTwo", "acct");
			sub.add(Restrictions.eqProperty("acct.id", criteria.getAlias() + ".id"));
			sub.add(Restrictions.eq("group.id", searchForm.getInvestmentAccountGroupId()));
			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getExcludeInvestmentAccountGroupId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "neLink");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("referenceOne", "neGroup");
			sub.createAlias("referenceTwo", "neAcct");
			sub.add(Restrictions.eqProperty("neAcct.id", criteria.getAlias() + ".id"));
			sub.add(Restrictions.eq("neGroup.id", searchForm.getExcludeInvestmentAccountGroupId()));
			criteria.add(Subqueries.notExists(sub));
		}

		// lookup related accounts with the given main purpose for a date, and restrict relationships by the main account
		if ((searchForm.getMainPurpose() != null) || (searchForm.getMainPurposeId() != null) || (searchForm.getMainPurposeActiveOnDate() != null) || (searchForm.getMainAccountId() != null)) {
			addPurposeCriteria(searchForm.getMainPurpose(), searchForm.getMainPurposeId(), searchForm.getMainPurposeActiveOnDate(), searchForm.getMainAccountId(),
					searchForm.getMainPurposeInvestmentAccountGroupId(), "referenceTwo", criteria);
		}

		// lookup main accounts with the given related purpose for a date, and restrict relationships by the related account
		if ((searchForm.getRelatedPurposesAllApplicable() != null) || (searchForm.getRelatedPurposes() != null) || (searchForm.getRelatedPurpose() != null) || (searchForm.getRelatedPurposeActiveOnDate() != null) || (searchForm.getRelatedAccountId() != null)) {
			Set<String> purposeNames = new HashSet<>();
			boolean purposeUsed = false;
			if (searchForm.getRelatedPurposesAllApplicable() != null) {
				// Add a criterion for each purpose to ensure the investment account applies to all purposes
				for (String purpose : searchForm.getRelatedPurposesAllApplicable()) {
					purposeNames.add(purpose);
					addPurposeCriteria(purpose, null, searchForm.getRelatedPurposeActiveOnDate(), searchForm.getRelatedAccountId(), null, "referenceOne", criteria);
					purposeUsed = true;
				}
			}
			else if (searchForm.getRelatedPurposes() != null) {
				ArrayUtils.getStream(searchForm.getRelatedPurposes()).forEach(purposeNames::add);
				addPurposeInCriteria(searchForm.getRelatedPurposes(), searchForm.getRelatedPurposeActiveOnDate(), searchForm.getRelatedAccountId(), "referenceOne", criteria);
				purposeUsed = true;
			}
			// Include Related Purpose if it was not already used in either of the previous.
			// e.g. want to search for Clients supporting ("Trading: Bonds" or "Trading: Bonds Collateral") and "Trading: Futures"
			if (searchForm.getRelatedPurpose() != null && !purposeNames.contains(searchForm.getRelatedPurpose())) {
				addPurposeCriteria(searchForm.getRelatedPurpose(), null, searchForm.getRelatedPurposeActiveOnDate(), searchForm.getRelatedAccountId(), null, "referenceOne", criteria);
				purposeUsed = true;
			}
			if (!purposeUsed) {
				addPurposeCriteria(null, null, searchForm.getRelatedPurposeActiveOnDate(), searchForm.getRelatedAccountId(), null, "referenceOne", criteria);
			}
		}

		if (searchForm.getMappingMainAccountTypeId() != null || searchForm.getMappingPurposeId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountRelationshipMapping.class, "m");
			sub.setProjection(Projections.property("id"));
			if (searchForm.getMappingMainAccountTypeId() != null) {
				sub.add(Restrictions.eq("mainAccountType.id", searchForm.getMappingMainAccountTypeId()));
			}
			if (searchForm.getMappingPurposeId() != null) {
				sub.add(Restrictions.eq("purpose.id", searchForm.getMappingPurposeId()));
			}
			sub.add(Restrictions.eqProperty("relatedAccountType.id", criteria.getAlias() + ".type.id"));
			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getClientIdOrRelatedClient() != null) {
			// Look up the Client to See if it is a Child or a Parent
			BusinessClient client = this.businessClientService.getBusinessClient(searchForm.getClientIdOrRelatedClient());
			if (client != null) {
				// Exact Client Match
				SimpleExpression clientRestriction = Restrictions.eq("businessClient.id", searchForm.getClientIdOrRelatedClient());

				// If Client has a parent, then include where client id = client's parent id
				if (client.getParent() != null) {
					criteria.add(Restrictions.or(clientRestriction, Restrictions.eq("businessClient.id", client.getParent().getId())));
				}
				// Otherwise if it is a parent client, include where client.parent.id = id
				else if (client.getCategory().isChildrenAllowed()) {
					String p1 = getPathAlias("businessClient.parent", criteria, JoinType.LEFT_OUTER_JOIN);
					criteria.add(Restrictions.or(clientRestriction, Restrictions.eq(p1 + ".id", client.getId())));
				}
				// Else - there are no related clients - just filter on the client id field
				else {
					criteria.add(clientRestriction);
				}
			}
		}

		if (searchForm.getIssuingCompanyIdOrRelatedIssuingCompanyId() != null) {
			String p1 = getPathAlias("issuingCompany", criteria, JoinType.LEFT_OUTER_JOIN);
			String p2 = getPathAlias("issuingCompany.parent", criteria, JoinType.LEFT_OUTER_JOIN);

			Integer companyId = searchForm.getIssuingCompanyIdOrRelatedIssuingCompanyId();
			BusinessCompany businessCompany = this.businessCompanyService.getBusinessCompany(companyId);
			Integer parentCompanyId = businessCompany.getParent() != null ? businessCompany.getParent().getId() : null;

			if (parentCompanyId != null) {
				Object[] companyAndParentCompanyIds = new Integer[]{companyId, parentCompanyId};
				criteria.add(Restrictions.or(
						Restrictions.in(p1 + ".id", companyAndParentCompanyIds),
						Restrictions.in(p2 + ".id", companyAndParentCompanyIds)
				));
			}
			else {
				criteria.add(Restrictions.or(Restrictions.eq(p1 + ".id", companyId), Restrictions.eq(p2 + ".id", companyId)));
			}
		}

		if (searchForm.getOurAccount() != null) {
			if (searchForm.getOurAccount()) {
				criteria.add(Restrictions.eq("type.id", this.investmentAccountService.getInvestmentAccountTypeByOurAccount().getId()));
			}
			else {
				criteria.add(Restrictions.ne("type.id", this.investmentAccountService.getInvestmentAccountTypeByOurAccount().getId()));
			}
		}

		if (searchForm.getDtcParticipant() != null) {
			DetachedCriteria relatedIsDtcMember = DetachedCriteria.forClass(InvestmentAccountRelationship.class, "relationship");
			relatedIsDtcMember.createAlias("relationship.referenceOne", "ca");
			relatedIsDtcMember.createAlias("relationship.referenceTwo", "ha");
			relatedIsDtcMember.createAlias("ha.issuingCompany", "haic");
			relatedIsDtcMember.setProjection(Projections.property("ha.id"));
			relatedIsDtcMember.add(Restrictions.and(Restrictions.isNotNull("haic.dtcNumber"), Restrictions.eqProperty("ca.id", criteria.getAlias() + ".id")));

			if (searchForm.getOurAccount() != null) {
				if (searchForm.getOurAccount()) {
					criteria.add(searchForm.getDtcParticipant() ? Subqueries.exists(relatedIsDtcMember) : Subqueries.notExists(relatedIsDtcMember));
				}
				else {
					criteria.add(searchForm.getDtcParticipant()
							? Restrictions.isNotNull(getPathAlias("issuingCompany", criteria) + ".dtcNumber")
							: Restrictions.isNull(getPathAlias("issuingCompany", criteria) + ".dtcNumber"));
				}
			}
			else {
				Criterion caCriterion = searchForm.getDtcParticipant() ? Subqueries.exists(relatedIsDtcMember) : Subqueries.notExists(relatedIsDtcMember);
				Criterion haCriterion = searchForm.getDtcParticipant() ? Restrictions.isNotNull(getPathAlias("issuingCompany", criteria) + ".dtcNumber")
						: Restrictions.isNull(getPathAlias("issuingCompany", criteria) + ".dtcNumber");
				criteria.add(Restrictions.or(caCriterion, haCriterion));
			}
		}
	}


	public void createInvestmentAccountGroupTypeNameCriteria(InvestmentAccountSearchForm searchForm, Criteria criteria) {
		//Find account groups that have at least one account of the specified account type.
		DetachedCriteria triPartyAccountGroups = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "accountGroupAccount");
		triPartyAccountGroups.createAlias("referenceOne", "accountGroup");
		triPartyAccountGroups.createAlias("referenceTwo", "account");
		triPartyAccountGroups.createAlias("account.type", "accountType");
		triPartyAccountGroups.setProjection(Projections.property("accountGroup.id"));
		Object[] accountTypeIds = searchForm.getAccountTypeIds();
		triPartyAccountGroups.add(Restrictions.in("accountType.id", accountTypeIds));

		//Find accounts inside the groups that were found containing at least one account of the specified account type.
		DetachedCriteria triPartyAccounts = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "accountGroupAccount");
		triPartyAccounts.createAlias("referenceOne", "accountGroup");
		triPartyAccounts.createAlias("referenceTwo", "account");
		triPartyAccounts.createAlias("accountGroup.type", "accountGroupType");
		triPartyAccounts.setProjection(Projections.property("account.id"));
		triPartyAccounts.add(
				Restrictions.and(
						Restrictions.eq("accountGroupType.name", searchForm.getInvestmentAccountGroupTypeName()),
						Subqueries.propertyIn("accountGroup.id", triPartyAccountGroups)
				)
		);

		if (searchForm.getId() != null) {
			//Merge the accounts of the specified type with those found in the account groups returned from the above criteria
			criteria.add(
					Restrictions.or(
							Restrictions.and(
									Restrictions.eq("id", searchForm.getId()),
									Restrictions.in("type.id", accountTypeIds),
									Restrictions.isNotNull("businessContract.id")
							),
							Subqueries.propertyIn(criteria.getAlias() + ".id", triPartyAccounts)
					)
			);
		}
		else {
			criteria.add(
					Restrictions.or(
							Restrictions.and(
									Restrictions.in("type.id", accountTypeIds),
									Restrictions.isNotNull("businessContract.id")
							),
							Subqueries.propertyIn(criteria.getAlias() + ".id", triPartyAccounts)
					)
			);
		}
		//Since the accounts found by the custom OR statement will not have a business contract populated; remove from the global criteria and set only
		//on the first restriction of the OR statement
		searchForm.removeSearchRestriction("id");
		searchForm.removeSearchRestriction("accountTypeIds");
		searchForm.removeSearchRestriction("businessContractPopulated");
	}
}
