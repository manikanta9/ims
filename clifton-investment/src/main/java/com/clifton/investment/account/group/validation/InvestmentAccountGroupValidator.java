package com.clifton.investment.account.group.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentAccountGroupValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentAccountGroupValidator extends SelfRegisteringDaoValidator<InvestmentAccountGroup> {

	private ReadOnlyDAO<InvestmentAccountGroupAccount> investmentAccountGroupAccountDAO;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	/////////         InvestmentAccountGroupValidator Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate(InvestmentAccountGroup bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("Deleting System Defined Investment Account Groups is not allowed.");
			}
			if (config.isInsert()) {
				throw new ValidationException("Adding new System Defined Investment Account Groups is not allowed.");
			}
		}
		if (config.isUpdate()) {
			InvestmentAccountGroup original = getOriginalBean(bean);
			if (original.isSystemDefined() != bean.isSystemDefined()) {
				throw new FieldValidationException("You cannot edit the System Defined field for Investment Account Groups", "systemDefined");
			}
			if (original.isSystemDefined()) {
				ValidationUtils.assertTrue(original.getName().equals(bean.getName()), "System Defined Investment Account Group Names cannot be changed.", "name");
			}
			InvestmentAccountType originalType = original.getAccountType();
			InvestmentAccountType newType = bean.getAccountType();
			// If new type is null, then selections are broader, so OK, otherwise need to validate
			// if any accounts already tied to group that don't match the selected type.
			if (newType != null && !CompareUtils.isEqual(originalType, newType)) {
				List<InvestmentAccountGroupAccount> list = getInvestmentAccountGroupAccountDAO().findByField("referenceOne.id", bean.getId());
				if (!CollectionUtils.isEmpty(list)) {
					list = BeanUtils.filter(list, investmentAccountGroupAccount -> !CompareUtils.isEqual(newType, investmentAccountGroupAccount.getReferenceTwo().getType()));
					if (!CollectionUtils.isEmpty(list)) {
						throw new FieldValidationException("Cannot change the type for this group to [" + newType.getName()
								+ "] because the following accounts that are already tied to this group do not match that type [" + StringUtils.collectionToCommaDelimitedString(list) + "]",
								"accountType.id");
					}
				}
			}
		}
		if (bean.isSystemManaged() && (config.isInsert() || config.isUpdate())) {
			SystemBean rebuildBean = bean.getRebuildSystemBean();
			if (rebuildBean != null) {
				((EntityGroupRebuildAwareExecutor) getSystemBeanService().getBeanInstance(rebuildBean)).validate(bean);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReadOnlyDAO<InvestmentAccountGroupAccount> getInvestmentAccountGroupAccountDAO() {
		return this.investmentAccountGroupAccountDAO;
	}


	public void setInvestmentAccountGroupAccountDAO(ReadOnlyDAO<InvestmentAccountGroupAccount> investmentAccountGroupAccountDAO) {
		this.investmentAccountGroupAccountDAO = investmentAccountGroupAccountDAO;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
