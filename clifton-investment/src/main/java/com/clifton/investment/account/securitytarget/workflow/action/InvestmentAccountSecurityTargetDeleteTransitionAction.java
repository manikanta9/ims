package com.clifton.investment.account.securitytarget.workflow.action;

import com.clifton.core.util.AssertUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.transition.WorkflowTransition;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * A InvestmentAccountSecurityTarget workflow transition action used to delete an existing security target and restore the previous
 * (historical) InvestmentAccountSecurityTarget into an active state if a matching historical target exists.
 *
 * @author davidi
 */
public class InvestmentAccountSecurityTargetDeleteTransitionAction extends BaseInvestmentAccountSecurityTargetWorkflowTargetTransitionAction {

	Boolean restorePreviousSecurityTarget;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Deletes the linked security target and restores the previous security target if configured to do so.
	 */
	@Override
	@Transactional
	public WorkflowTask processAction(WorkflowTask task, WorkflowTransition transition) {
		InvestmentAccountSecurityTarget currentActiveSecurityTarget = (InvestmentAccountSecurityTarget) getLinkedEntity(task);
		AssertUtils.assertNotNull(currentActiveSecurityTarget, "InvestmentAccountSecurityTarget (currentActiveSecurityTarget) must be defined (cannot be null).");

		getInvestmentAccountSecurityTargetService().deleteInvestmentAccountSecurityTarget(currentActiveSecurityTarget.getId());

		if (getRestorePreviousSecurityTarget()) {
			InvestmentAccountSecurityTarget previousSecurityTarget = getPreviousSecurityTarget(currentActiveSecurityTarget);
			if (previousSecurityTarget != null) {
				previousSecurityTarget.setEndDate(null);
				getInvestmentAccountSecurityTargetService().saveInvestmentAccountSecurityTarget(previousSecurityTarget);
			}
		}
		return task;
	}


	/**
	 * Looks up and returns the latest historical {@link InvestmentAccountSecurityTarget} entry for matching the provided active security target (e.g. same client account and underlying security).
	 * Will return null if no historical security target is found.
	 */
	private InvestmentAccountSecurityTarget getPreviousSecurityTarget(InvestmentAccountSecurityTarget currentActiveSecurityTarget) {
		InvestmentAccountSecurityTargetSearchForm searchForm = new InvestmentAccountSecurityTargetSearchForm();
		searchForm.setClientInvestmentAccountId(currentActiveSecurityTarget.getClientInvestmentAccount().getId());
		searchForm.setTargetUnderlyingSecurityId(currentActiveSecurityTarget.getTargetUnderlyingSecurity().getId());
		searchForm.setActive(false);

		searchForm.setOrderBy("endDate:desc");
		searchForm.setLimit(1);

		List<InvestmentAccountSecurityTarget> historicalSecurityTargetList = getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetList(searchForm);
		if (historicalSecurityTargetList.isEmpty()) {
			return null;
		}

		return historicalSecurityTargetList.get(0);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getRestorePreviousSecurityTarget() {
		return this.restorePreviousSecurityTarget;
	}


	public void setRestorePreviousSecurityTarget(Boolean restorePreviousSecurityTarget) {
		this.restorePreviousSecurityTarget = restorePreviousSecurityTarget;
	}
}
