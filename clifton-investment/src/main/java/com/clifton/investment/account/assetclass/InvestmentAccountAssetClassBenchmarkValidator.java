package com.clifton.investment.account.assetclass;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentAccountAssetClassBenchmarkValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentAccountAssetClassBenchmarkValidator extends SelfRegisteringDaoValidator<InvestmentAccountAssetClassBenchmark> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentAccountAssetClassBenchmark bean, @SuppressWarnings("unused") DaoEventTypes config, ReadOnlyDAO<InvestmentAccountAssetClassBenchmark> dao) throws ValidationException {
		// Validate Start/End Dates
		List<InvestmentAccountAssetClassBenchmark> list = dao.findByField("referenceOne.id", bean.getReferenceOne().getId());
		for (InvestmentAccountAssetClassBenchmark b : CollectionUtils.getIterable(list)) {
			if (b.equals(bean)) {
				continue;
			}
			if (DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), b.getStartDate(), b.getEndDate())) {
				throw new ValidationException("Another benchmark [" + b.getBenchmarkLabel() + "] is already used for this asset class by on " + b.getDateLabel()
						+ ". Please enter a unique date range.");
			}
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void validate(InvestmentAccountAssetClassBenchmark bean, DaoEventTypes config) throws ValidationException {
		// unused - uses dao method
	}
}
