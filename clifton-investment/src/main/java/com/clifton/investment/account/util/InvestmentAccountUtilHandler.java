package com.clifton.investment.account.util;

import com.clifton.investment.account.InvestmentAccount;


/**
 * <code>InvestmentAccountUtilHandler</code> provides common utility methods for {@link InvestmentAccount}s such as looking up
 * custom field values.
 *
 * @author NickK
 */
public interface InvestmentAccountUtilHandler {

	public static final String VALUE_AT_RISK_PERCENT_COLUMN_NAME = "Value At Risk Percent";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the provided holding investment account is a Directed Broker, one in which
	 * transactions must be executed with the same issuing company.
	 */
	public boolean isHoldingInvestmentAccountDirectedBroker(InvestmentAccount holdingAccount);


	/**
	 * Returns the custom column value for the specified holding {@link InvestmentAccount} and column name.
	 * <p>
	 * Uses {@link #getInvestmentAccountCustomColumnValue(InvestmentAccount, String, String)} with the
	 * {@link InvestmentAccount#HOLDING_ACCOUNT_COLUMN_GROUP_NAME} column group.
	 */
	public Object getHoldingInvestmentAccountCustomColumnValue(InvestmentAccount holdingAccount, String columnName);


	/**
	 * Returns the custom column value for the specified client {@link InvestmentAccount} and column name.
	 * <p>
	 * Uses {@link #getInvestmentAccountCustomColumnValue(InvestmentAccount, String, String)} with the
	 * {@link InvestmentAccount#CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME} column group.
	 */
	public Object getClientInvestmentAccountPortfolioSetupCustomColumnValue(InvestmentAccount clientAccount, String columnName);


	/**
	 * Returns the custom column value for the specified {@link InvestmentAccount}, column group name, and column name.
	 * The default value will be returned if there is not a specific value defined for the holding account.
	 */
	public Object getInvestmentAccountCustomColumnValue(InvestmentAccount investmentAccount, String columnGroupName, String columnName);
}
