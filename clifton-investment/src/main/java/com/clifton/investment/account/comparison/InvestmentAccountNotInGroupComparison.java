package com.clifton.investment.account.comparison;


import com.clifton.core.comparison.Comparison;


/**
 * The <code>InvestmentAccountNotInGroup</code> class is a {@link Comparison} that evaluates to true
 * if investment account on the bean (specified by the beanName)
 * is not a member of the group(s) allowed by this comparison.
 * <p/>
 *
 * @author lnaylor
 */
public class InvestmentAccountNotInGroupComparison extends BaseInvestmentAccountInGroupComparison {

	@Override
	protected boolean isRequireInGroup() {
		return false;
	}


	@Override
	protected String getTrueMessage(String accountNumber, String groupList) {
		String messageWording = isMatchAnyGroup() ? "at least one group" : "any groups";
		return "(Account " + accountNumber + " is not in " + messageWording + " in group list " + groupList + ".)";
	}


	@Override
	protected String getFalseMessage(String accountNumber, String groupList) {
		String messageWording = isMatchAnyGroup() ? "all groups" : "at least one group";
		return "(Account " + accountNumber + " is in " + messageWording + " in group list " + groupList + ".)";
	}
}
