package com.clifton.investment.account;


import com.clifton.investment.account.search.InvestmentAccountBusinessServiceSearchForm;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.account.search.InvestmentAccountServiceObjectiveSearchForm;
import com.clifton.investment.account.search.InvestmentAccountTypeSearchForm;

import java.util.List;


/**
 * The <code>InvestmentAccountService</code> ...
 *
 * @author mwacker
 */
public interface InvestmentAccountService {

	////////////////////////////////////////////////////////////////////////////
	//////        Investment Account Type Business Methods               ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountType getInvestmentAccountType(short id);


	public InvestmentAccountType getInvestmentAccountTypeByName(String name);


	public List<InvestmentAccountType> getInvestmentAccountTypeList(InvestmentAccountTypeSearchForm searchForm);


	/**
	 * Returns only one account because we expect only one Clifton Account Type
	 */
	public InvestmentAccountType getInvestmentAccountTypeByOurAccount();


	////////////////////////////////////////////////////////////////////////////
	/////////        Investment Account Business Methods               /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getInvestmentAccountSimple(int id);


	/**
	 * Returns InvestmentAccount object for the specified id populating with corresponding
	 * business service components if any.
	 *
	 * @param id
	 */
	public InvestmentAccount getInvestmentAccount(int id);


	public InvestmentAccount getInvestmentAccountByNumber(String accountNumber);


	public List<InvestmentAccount> getInvestmentAccountListByIds(List<Integer> ids);


	public List<InvestmentAccount> getInvestmentAccountList(InvestmentAccountSearchForm searchForm);


	public InvestmentAccount saveInvestmentAccount(InvestmentAccount account);


	public void deleteInvestmentAccount(int id);


	////////////////////////////////////////////////////////////////////////////
	//////     Investment Account Business Service Business Methods      ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountBusinessService getInvestmentAccountBusinessService(int id);


	public List<InvestmentAccountBusinessService> getInvestmentAccountBusinessServiceList(InvestmentAccountBusinessServiceSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	//////  Investment Account Service Type Objective Business Methods    //////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountServiceObjective getInvestmentAccountServiceObjective(int id);


	public List<InvestmentAccountServiceObjective> getInvestmentAccountServiceObjectiveList(InvestmentAccountServiceObjectiveSearchForm searchForm);


	public InvestmentAccountServiceObjective saveInvestmentAccountServiceObjective(InvestmentAccountServiceObjective bean);


	public void deleteInvestmentAccountServiceObjective(int id);
}
