package com.clifton.investment.account.relationship.cache;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import org.jetbrains.annotations.Contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>InvestmentAccountRelationshipConfig</code> contains meta data for all relationships for a specific account.
 *
 * @author Mary Anderson
 */
public class InvestmentAccountRelationshipConfig implements Serializable {

	/**
	 * All Client Accounts (Main Account & SubAccounts) for this Relationship Config (used when mainAccount is a client account)
	 * For other cases, would just contain one main holding account id
	 */
	private final Set<Integer> mainAccountIds = new LinkedHashSet<>();

	/**
	 * Meta Data for Each Relationship & SubAccount Relationship & Asset Class
	 */
	private final Map<Integer, InvestmentAccountRelationshipAssetClassConfig> accountRelationshipAssetClassConfigMap = new HashMap<>();

	/**
	 * Holds the full relationship list for a client account so we know if an account is valid
	 * and can filter on a Date for historically checking.  The config & maps above only hold currently active relationships
	 */
	private final List<InvestmentAccountRelationshipHolder> fullRelationshipList = new ArrayList<>();

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipConfig(Integer mainAccountId) {
		getMainAccountIds().add(mainAccountId);

		// Main Account Always Applies to All
		InvestmentAccountRelationshipAssetClassConfig config = this.getConfigPopulated(mainAccountId, true);
		config.addAccountAssetClassSecurityGroupMapping(null, null);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if there is an active relationship between this account and the given accountId on the given date
	 */
	public boolean isInvestmentAccountRelationshipValid(int accountId, Date date) {
		return isInvestmentAccountRelationshipValidImpl(accountId, date, this.fullRelationshipList);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// UTILITY METHODS - USED FOR QUICK COMMON LOOK UPS
	//////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if there is an active relationship of the specified purpose between this account and the given accountId on the given date
	 */
	public boolean isInvestmentAccountRelationshipForPurposeValid(int accountId, short purposeId, Date date) {
		List<InvestmentAccountRelationshipHolder> filteredList = BeanUtils.filter(this.fullRelationshipList, InvestmentAccountRelationshipHolder::getPurposeId, purposeId);
		return isInvestmentAccountRelationshipValidImpl(accountId, date, filteredList);
	}


	/**
	 * Returns true if there is an active relationship between this account and the given account on the given date
	 * in the given relationshipList (which can be previously filtered by a specific purpose if necessary)
	 */
	private boolean isInvestmentAccountRelationshipValidImpl(int accountId, Date date, List<InvestmentAccountRelationshipHolder> relationshipList) {
		for (InvestmentAccountRelationshipHolder rel : CollectionUtils.getIterable(relationshipList)) {
			if (rel.isActiveOnDate(date)) {
				if (getMainAccountIds().contains(rel.getMainAccountId())) {
					if (rel.getRelatedAccountId() == accountId) {
						return true;
					}
				}
				else if (rel.getMainAccountId() == accountId) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Returns a list of related account Ids for a given purpose and active date.
	 * If purpose is blank - will include all purposes.  If active date is null will not filter by active dates
	 */
	public List<Integer> getInvestmentAccountListForPurpose(short purposeId, Date activeOnDate) {
		return getInvestmentAccountListForRelationshipList(getInvestmentAccountRelationshipListForPurpose(null, purposeId, activeOnDate));
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// ACCOUNT METHODS
	//////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Return all Main accounts active for the account on the given date (i.e. Relationship.ReferenceTwo/RelatedAccountID = Account)
	 * <p/>
	 * To look for a direct relationship for a specific account (applies when an account has sub-accounts and this config holds multiple "mainAccounts")
	 * pass in specific accountId
	 *
	 * @param accountId - use null to return all for the accounts this config is set up for
	 */
	public List<Integer> getMainInvestmentAccountList(Integer accountId, Date activeOnDate) {
		return getInvestmentAccountListMainOrRelatedImpl(accountId, true, activeOnDate);
	}


	/**
	 * Return all Related accounts for a given account id (i.e. if the account has sub-accounts would only return those that are directly associated with the given account)
	 */
	public List<Integer> getRelatedInvestmentAccountList(Integer accountId, Date activeOnDate) {
		return getInvestmentAccountListMainOrRelatedImpl(accountId, false, activeOnDate);
	}


	private List<Integer> getInvestmentAccountListMainOrRelatedImpl(Integer accountId, boolean main, Date activeOnDate) {
		// First Filter on All Active Relationships
		List<InvestmentAccountRelationshipHolder> relationshipList = getInvestmentAccountRelationshipListForPurpose(null, null, activeOnDate);
		// Then If Specific Account - filter on that account
		if (accountId != null) {
			relationshipList = BeanUtils.filter(relationshipList, (main ? InvestmentAccountRelationshipHolder::getRelatedAccountId : InvestmentAccountRelationshipHolder::getMainAccountId), accountId);
		}
		// Otherwise return all that match this config's mainAccountIds
		else {
			relationshipList = BeanUtils.filter(relationshipList, relationshipHolder -> CollectionUtils.contains(getMainAccountIds(), (main ? relationshipHolder.getRelatedAccountId() : relationshipHolder.getMainAccountId())));
		}

		List<Integer> accountIdList = new ArrayList<>();
		for (InvestmentAccountRelationshipHolder rel : CollectionUtils.getIterable(relationshipList)) {
			addAccountToList(accountIdList, (main ? rel.getMainAccountId() : rel.getRelatedAccountId()));
		}
		return accountIdList;
	}


	/**
	 * Returns a list of related account ids from the specified relationship list..
	 */
	public List<Integer> getInvestmentAccountListForRelationshipList(List<InvestmentAccountRelationshipHolder> relationshipList) {
		List<Integer> accountList = new ArrayList<>();
		for (InvestmentAccountRelationshipHolder rel : CollectionUtils.getIterable(relationshipList)) {
			if (getMainAccountIds().contains(rel.getMainAccountId())) {
				// Could have multiple relationships for the same account - only add the account to the list once
				addAccountToList(accountList, rel.getRelatedAccountId());
			}
			else {
				// Could have multiple relationships for the same account - only add the account to the list once
				addAccountToList(accountList, rel.getMainAccountId());
			}
		}
		return accountList;
	}


	/**
	 * Helper method used to convert relationship to account lists - skips adding if it's already in the list
	 */
	private void addAccountToList(List<Integer> accountIdList, Integer accountId) {
		if (!CollectionUtils.contains(accountIdList, accountId)) {
			accountIdList.add(accountId);
		}
	}


	/**
	 * If Account ID is not null will return those relationships directly associated with the specified account
	 * Used for accounts with sub-accounts when we don't want to return the related accounts from the sub-accounts
	 */
	public List<InvestmentAccountRelationshipHolder> getInvestmentAccountRelationshipListForPurpose(Integer accountId, Short purposeId, Date activeOnDate) {
		List<InvestmentAccountRelationshipHolder> relList = new ArrayList<>();

		for (InvestmentAccountRelationshipHolder rel : CollectionUtils.getIterable(this.fullRelationshipList)) {
			if (accountId == null || MathUtils.isEqual(accountId, rel.getMainAccountId()) || MathUtils.isEqual(accountId, rel.getRelatedAccountId())) {
				if (purposeId == null || MathUtils.isEqual(purposeId, rel.getPurposeId())) {
					if (activeOnDate == null || rel.isActiveOnDate(activeOnDate)) {
						relList.add(rel);
					}
				}
			}
		}
		return relList;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// ACCOUNT RELATIONSHIP METHODS
	//////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used for adding Main Account Relationships.  Uses the asset class defined at the relationship level
	 */
	public void addInvestmentAccountRelationshipList(List<InvestmentAccountRelationship> relationshipList) {
		addInvestmentAccountRelationshipList(null, relationshipList);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used for adding SubAccount Relationship Relationships..  Uses the asset class defined for the subaccount relationship
	 * <p/>
	 * If iac is set, overrides relationship level asset classes with that passed in
	 */
	public void addInvestmentAccountRelationshipList(InvestmentAccountRelationship subAccountRelationship, List<InvestmentAccountRelationship> relationshipList) {
		if (subAccountRelationship != null) {
			this.fullRelationshipList.add(new InvestmentAccountRelationshipHolder(subAccountRelationship));
			InvestmentAccountAssetClass iac = subAccountRelationship.getAccountAssetClass();

			// Only Add Sub Account to Main Account Ids if the relationship is Active
			if (subAccountRelationship.getReferenceTwo().getType().isOurAccount() && subAccountRelationship.isActive()) {
				getMainAccountIds().add(subAccountRelationship.getReferenceTwo().getId());
				// Even if account is already checked once, can have multiple asset class mappings
				InvestmentAccountRelationshipAssetClassConfig config = getConfigPopulated(subAccountRelationship.getReferenceTwo().getId(), true);
				config.addAccountAssetClassSecurityGroupMapping(iac, subAccountRelationship.getSecurityGroup());
			}
		}

		for (InvestmentAccountRelationship relationship : CollectionUtils.getIterable(relationshipList)) {
			// Add relationship to the full list
			this.fullRelationshipList.add(new InvestmentAccountRelationshipHolder(relationship));
		}
	}


	/**
	 * Returns the specific configuration for a client-holding account relationship.
	 */
	public InvestmentAccountRelationshipAssetClassConfig getConfig(Integer clientAccountId) {
		return getConfigPopulated(clientAccountId, false);
	}


	/**
	 * Returns the specific configuration for a client-holding account relationship. If it doesn't exist, create a new one so can be used to add asset classes to
	 */
	@Contract("_, true -> !null")
	private InvestmentAccountRelationshipAssetClassConfig getConfigPopulated(Integer clientAccountId, boolean populateIfNull) {
		return getAccountRelationshipAssetClassConfigMap().compute(clientAccountId, (key, currentValue) -> {
			if (currentValue == null && populateIfNull) {
				return new InvestmentAccountRelationshipAssetClassConfig(clientAccountId);
			}
			return currentValue;
		});
	}


	public Map<Integer, InvestmentAccountRelationshipAssetClassConfig> getAccountRelationshipAssetClassConfigMap() {
		return this.accountRelationshipAssetClassConfigMap;
	}

	//////////////////////////////////////////////////////////////////////////////////////////


	public Collection<Integer> getMainAccountIds() {
		return this.mainAccountIds;
	}


	/**
	 * Contains the meta data for each Client Account and which asset class it applies to (or applies to all) for the MAIN account.
	 * The MAIN Account is always added as allowed for all, and then each sub-account
	 * is added with the asset class selection from the sub-account relationship(s)
	 */
	public static class InvestmentAccountRelationshipAssetClassConfig implements Serializable {

		private final Integer clientAccountId;
		private final List<Integer> accountAssetClassIdList = new ArrayList<>();
		private final List<Short> securityGroupIdList = new ArrayList<>();
		/**
		 * Map of Account Asset Class ID -> Security group Ids that apply
		 * and vice versa. Keys or Values of 0 mean they apply to all
		 */
		private final MultiValueMap<Short, Integer> securityGroupAccountAssetClassMap = new MultiValueHashMap<>(false);
		/**
		 * If there is a mapping for all asset classes and all security groups - additional mappings don't matter
		 */
		private boolean applyToAll;


		public InvestmentAccountRelationshipAssetClassConfig(Integer clientAccountId) {
			this.clientAccountId = clientAccountId;
		}


		public void addAccountAssetClassSecurityGroupMapping(InvestmentAccountAssetClass iac, InvestmentSecurityGroup group) {
			if (iac != null) {
				this.accountAssetClassIdList.add(iac.getId());
			}
			if (group != null) {
				this.securityGroupIdList.add(group.getId());
			}

			if (iac == null && group == null) {
				this.applyToAll = true;
			}

			// If the relationship was already entered as APPLY TO ALL then don't bother adding other details?
			if (!isApplyToAll()) {
				this.securityGroupAccountAssetClassMap.put(group == null ? (short) 0 : group.getId(), iac == null ? 0 : iac.getId());
			}
		}


		public boolean isApplyToAll() {
			return this.applyToAll;
		}


		public boolean isApplyToAllSecurityGroups() {
			return isApplyToAll() || CollectionUtils.isEmpty(this.securityGroupIdList);
		}


		public Integer getClientAccountId() {
			return this.clientAccountId;
		}


		public List<Integer> getAccountAssetClassIdList() {
			return this.accountAssetClassIdList;
		}


		public List<Short> getSecurityGroupIdList() {
			return this.securityGroupIdList;
		}


		public MultiValueMap<Short, Integer> getSecurityGroupAccountAssetClassMap() {
			return this.securityGroupAccountAssetClassMap;
		}
	}
}
