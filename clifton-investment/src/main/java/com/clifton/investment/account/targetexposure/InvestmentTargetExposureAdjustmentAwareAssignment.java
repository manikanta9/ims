package com.clifton.investment.account.targetexposure;

import com.clifton.core.beans.ManyToManyEntity;

import java.math.BigDecimal;


/**
 * The <code>TargetExposureAdjustmentAwareAssignment</code> assigns parent/child relationships for {@link InvestmentTargetExposureAdjustmentAware} objects
 * when a Target Exposure Adjustment option is used (i.e. defines where the put the target that was adjusted from the parent and how to apply it)
 *
 * @author Mary Anderson
 */
public class InvestmentTargetExposureAdjustmentAwareAssignment<T extends InvestmentTargetExposureAdjustmentAware<T, A>, A extends InvestmentTargetExposureAdjustmentAwareAssignment<T, A>> extends ManyToManyEntity<T, T, Integer> {

	/**
	 * Flag used to indicate if this assignment is applied to target (false) or cash (true).
	 */
	private boolean cashAllocation;
	private boolean proportionalAllocation;
	private BigDecimal allocationPercent;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isCashAllocation() {
		return this.cashAllocation;
	}


	public void setCashAllocation(boolean cashAllocation) {
		this.cashAllocation = cashAllocation;
	}


	public boolean isProportionalAllocation() {
		return this.proportionalAllocation;
	}


	public void setProportionalAllocation(boolean proportionalAllocation) {
		this.proportionalAllocation = proportionalAllocation;
	}


	public BigDecimal getAllocationPercent() {
		return this.allocationPercent;
	}


	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}
}
