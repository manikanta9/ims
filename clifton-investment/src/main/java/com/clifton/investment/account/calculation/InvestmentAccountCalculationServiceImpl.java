package com.clifton.investment.account.calculation;

import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class InvestmentAccountCalculationServiceImpl implements InvestmentAccountCalculationService {

	private UpdatableDAO<InvestmentAccountCalculationType> investmentAccountCalculationTypeDAO;

	private DaoNamedEntityCache<InvestmentAccountCalculationType> investmentAccountCalculationTypeCache;


	////////////////////////////////////////////////////////////////////////////////
	//////      Investment Account Calculation Type Business Methods         ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountCalculationType getInvestmentAccountCalculationType(short id) {
		return getInvestmentAccountCalculationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentAccountCalculationType getInvestmentAccountCalculationTypeByName(String name) {
		return getInvestmentAccountCalculationTypeCache().getBeanForKeyValueStrict(getInvestmentAccountCalculationTypeDAO(), name);
	}


	@Override
	public List<InvestmentAccountCalculationType> getInvestmentAccountCalculationTypeList() {
		// Note: Only 1 type now - not likely to grow very big at all so not using a search form
		return getInvestmentAccountCalculationTypeDAO().findAll();
	}


	@Override
	public InvestmentAccountCalculationType saveInvestmentAccountCalculationType(InvestmentAccountCalculationType bean) {
		return getInvestmentAccountCalculationTypeDAO().save(bean);
	}


	@Override
	public void deleteInvestmentAccountCalculationType(short id) {
		getInvestmentAccountCalculationTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////////


	public UpdatableDAO<InvestmentAccountCalculationType> getInvestmentAccountCalculationTypeDAO() {
		return this.investmentAccountCalculationTypeDAO;
	}


	public void setInvestmentAccountCalculationTypeDAO(UpdatableDAO<InvestmentAccountCalculationType> investmentAccountCalculationTypeDAO) {
		this.investmentAccountCalculationTypeDAO = investmentAccountCalculationTypeDAO;
	}


	public DaoNamedEntityCache<InvestmentAccountCalculationType> getInvestmentAccountCalculationTypeCache() {
		return this.investmentAccountCalculationTypeCache;
	}


	public void setInvestmentAccountCalculationTypeCache(DaoNamedEntityCache<InvestmentAccountCalculationType> investmentAccountCalculationTypeCache) {
		this.investmentAccountCalculationTypeCache = investmentAccountCalculationTypeCache;
	}
}
