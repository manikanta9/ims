package com.clifton.investment.account.relationship.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * The <code>InvestmentAccountRelationshipMappingSearchForm</code> class defines search configuration for InvestmentAccountRelationshipMapping objects.
 *
 * @author vgomelsky
 */
public class InvestmentAccountRelationshipMappingSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "mainAccountType.id")
	private Short mainAccountTypeId;

	@SearchField(searchField = "relatedAccountType.id")
	private Short relatedAccountTypeId;

	@SearchField(searchField = "purpose.id")
	private Short purposeId;

	@SearchField
	private Boolean onlyOneAllowedFromMain;
	@SearchField
	private Boolean onlyOneAllowedToRelated;

	@SearchField
	private Boolean assetClassAllowed;

	@SearchField
	private String note;

	@SearchField(searchField = "relationshipEntityModifyCondition.id", sortField = "relationshipEntityModifyCondition.name")
	private Integer relationshipEntityModifyConditionId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getMainAccountTypeId() {
		return this.mainAccountTypeId;
	}


	public void setMainAccountTypeId(Short mainAccountTypeId) {
		this.mainAccountTypeId = mainAccountTypeId;
	}


	public Short getRelatedAccountTypeId() {
		return this.relatedAccountTypeId;
	}


	public void setRelatedAccountTypeId(Short relatedAccountTypeId) {
		this.relatedAccountTypeId = relatedAccountTypeId;
	}


	public Short getPurposeId() {
		return this.purposeId;
	}


	public void setPurposeId(Short purposeId) {
		this.purposeId = purposeId;
	}


	public Boolean getOnlyOneAllowedFromMain() {
		return this.onlyOneAllowedFromMain;
	}


	public void setOnlyOneAllowedFromMain(Boolean onlyOneAllowedFromMain) {
		this.onlyOneAllowedFromMain = onlyOneAllowedFromMain;
	}


	public Boolean getOnlyOneAllowedToRelated() {
		return this.onlyOneAllowedToRelated;
	}


	public void setOnlyOneAllowedToRelated(Boolean onlyOneAllowedToRelated) {
		this.onlyOneAllowedToRelated = onlyOneAllowedToRelated;
	}


	public Boolean getAssetClassAllowed() {
		return this.assetClassAllowed;
	}


	public void setAssetClassAllowed(Boolean assetClassAllowed) {
		this.assetClassAllowed = assetClassAllowed;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Integer getRelationshipEntityModifyConditionId() {
		return this.relationshipEntityModifyConditionId;
	}


	public void setRelationshipEntityModifyConditionId(Integer relationshipEntityModifyConditionId) {
		this.relationshipEntityModifyConditionId = relationshipEntityModifyConditionId;
	}


	@Override
	public boolean isIncludeTime() {
		return false;
	}
}
