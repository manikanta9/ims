package com.clifton.investment.account.assetclass.upload;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.system.upload.SystemUploadHandler;
import com.clifton.system.upload.SystemUploadService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class InvestmentAccountAssetClassUploadServiceImpl implements InvestmentAccountAssetClassUploadService {

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	private SystemUploadHandler systemUploadHandler;
	private SystemUploadService systemUploadService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable downloadInvestmentAccountAssetClassUploadFileSample(InvestmentAccountAssetClassUploadCommand uploadCommand) {
		Map<String, Object> additionalFilterMap = new HashMap<>();
		if (uploadCommand.getInvestmentAccount() != null) {
			additionalFilterMap.put("account.id", uploadCommand.getInvestmentAccount().getId());
		}
		// Exclude inactive asset classes and rollups
		additionalFilterMap.put("inactive", false);
		additionalFilterMap.put("rollupAssetClass", false);

		// Pull Sample Data
		DataTable dataTable = getSystemUploadService().downloadSystemUploadFileSampleWithAdditionalFilters(uploadCommand, true, additionalFilterMap);

		// Customize the sample file:
		DataColumn[] columns = dataTable.getColumnList();
		if (columns != null && columns.length > 0) {
			for (DataColumn column : columns) {
				// Remove any column that we aren't going to support in the upload and also optionally exclude superfluous columns
				if (isColumnExcludedFromAccountAssetClassUpload(column.getColumnName(), uploadCommand.isDownloadAllColumns())) {
					column.setHidden(true);
				}
			}
		}
		return dataTable;
	}


	/**
	 * Used to remove some columns from the sample file.  If showAllColumns is true, it's still not all columns, but additional columns
	 * that can be helpful if the simple columns aren't enough.  i.e. setting benchmark and it's not unique by symbol, then we'll need the instrument, hierarchy columns included
	 */
	private boolean isColumnExcludedFromAccountAssetClassUpload(String columnName, boolean showAllColumns) {
		// For All cases: Exclude All parent fields, Inactive, and Rollup
		if (columnName.startsWith("Parent-") || StringUtils.isEqual(columnName, "Inactive") || StringUtils.isEqual(columnName, "RollupAssetClass")) {
			return true;
		}
		// Exclude all account fields, except number - should never need more than just the number
		if (columnName.startsWith("Account-")) {
			return !StringUtils.isEqual(columnName, "Account-Number");
		}

		// Optional field limitations
		if (showAllColumns) {
			return false;
		}
		// Exclude all asset class fields except name
		if (columnName.startsWith("AssetClass-")) {
			return !StringUtils.isEqual(columnName, "AssetClass-Name");
		}

		// Exclude all FK Security fields except symbol
		if (columnName.startsWith("BenchmarkSecurity-") || columnName.startsWith("BenchmarkDurationSecurity-") || columnName.startsWith("RebalanceBenchmarkSecurity-")) {
			return !columnName.endsWith("-Symbol");
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadInvestmentAccountAssetClassUploadFile(InvestmentAccountAssetClassUploadCommand uploadCommand) {
		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, true);
		List<InvestmentAccountAssetClass> accountAssetClassList = new ArrayList<>();

		for (IdentityObject bean : CollectionUtils.getIterable(beanList)) {
			InvestmentAccountAssetClass accountAssetClass = (InvestmentAccountAssetClass) bean;
			String label = accountAssetClass.getAccount().getNumber() + ": " + accountAssetClass.getLabel();
			ValidationUtils.assertFalse(accountAssetClass.isNewBean(), "New account asset classes cannot be added, you can only edit existing asset classes: " + label);
			ValidationUtils.assertFalse(accountAssetClass.isInactive(), "You cannot edit inactive asset class: " + label);
			ValidationUtils.assertFalse(accountAssetClass.isRollupAssetClass(), "You cannot edit rollup asset class: " + label);
			accountAssetClassList.add(accountAssetClass);
		}

		getInvestmentAccountAssetClassService().saveInvestmentAccountAssetClassList(accountAssetClassList);
		uploadCommand.getUploadResult().addUploadResultsUpdateOnly(uploadCommand.getTableName(), CollectionUtils.getSize(accountAssetClassList));
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}


	public SystemUploadService getSystemUploadService() {
		return this.systemUploadService;
	}


	public void setSystemUploadService(SystemUploadService systemUploadService) {
		this.systemUploadService = systemUploadService;
	}
}
