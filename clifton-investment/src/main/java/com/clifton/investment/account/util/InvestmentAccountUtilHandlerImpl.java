package com.clifton.investment.account.util;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.springframework.stereotype.Component;


/**
 * @author NickK
 */
@Component
public class InvestmentAccountUtilHandlerImpl implements InvestmentAccountUtilHandler {

	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isHoldingInvestmentAccountDirectedBroker(InvestmentAccount holdingAccount) {
		return (holdingAccount != null) && holdingAccount.getType().isExecutionWithIssuerRequired();
	}


	@Override
	public Object getHoldingInvestmentAccountCustomColumnValue(InvestmentAccount holdingAccount, String fieldName) {
		return getInvestmentAccountCustomColumnValue(holdingAccount, InvestmentAccount.HOLDING_ACCOUNT_COLUMN_GROUP_NAME, fieldName);
	}


	@Override
	public Object getClientInvestmentAccountPortfolioSetupCustomColumnValue(InvestmentAccount clientAccount, String fieldName) {
		return getInvestmentAccountCustomColumnValue(clientAccount, InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME, fieldName);
	}


	@Override
	public Object getInvestmentAccountCustomColumnValue(InvestmentAccount entity, String columnGroupName, String fieldName) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(entity, columnGroupName, fieldName, true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
