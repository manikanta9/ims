package com.clifton.investment.account.mapping;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.mapping.search.InvestmentAccountMappingPurposeSearchForm;
import com.clifton.investment.account.mapping.search.InvestmentAccountMappingSearchForm;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * This interface defines a service for mapping an InvestmentAccount entity to a value.  The value's meaning is determined
 * by the InvestmentAccountMappingPurpose value.
 *
 * @author davidi
 */
@Service
public interface InvestmentAccountMappingService {


	////////////////////////////////////////////////////////////////////////////
	//////      Investment Account Mapping Business Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountMapping getInvestmentAccountMapping(Integer id);


	public List<InvestmentAccountMapping> getInvestmentAccountMappingList(InvestmentAccountMappingSearchForm searchForm);


	@DoNotAddRequestMapping
	public InvestmentAccountMapping getInvestmentAccountMappingByAccountNumberAndPurpose(InvestmentAccount account, InvestmentAccountMappingPurpose mappingPurpose);


	@DoNotAddRequestMapping
	public List<InvestmentAccountMapping> getInvestmentAccountMappingListByMappedValueAndPurpose(String mappedValue, InvestmentAccountMappingPurpose mappingPurpose);


	public InvestmentAccountMapping saveInvestmentAccountMapping(InvestmentAccountMapping investmentAccountMapping);


	public void deleteInvestmentAccountMapping(int id);


	/**
	 * A method to facilitate lookup of mapped values by returning the mapped value directly, instead of the InvestmentAccountMapping
	 * instance.
	 *
	 * @return A string value representing the value for the specified account and mapping purpose.  Returns null if mapping is not found.
	 */
	@DoNotAddRequestMapping
	public String getInvestmentAccountMappedValue(InvestmentAccount account, InvestmentAccountMappingPurpose mappingPurpose);


	/**
	 * Performs a lookup of a mapped account given the mapped value.  This is support only for accounts
	 * with a 1:1 mapping.  If the search returns multiple InvestmentAccountMapping entries, a Runtime Exception is thrown.
	 *
	 * @param mappedValue
	 * @param mappingPurpose the mapping purpose to be used in the mapping lookup process
	 * @return the account number mapped to the value for the specified mapping purpose. If a reverse mapping is not found, returns null.
	 */
	@DoNotAddRequestMapping
	public String getInvestmentAccountReverseMapping(String mappedValue, InvestmentAccountMappingPurpose mappingPurpose);


	////////////////////////////////////////////////////////////////////////////
	//////      Investment Account Mapping Purpose Methods               ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountMappingPurpose getInvestmentAccountMappingPurpose(Short id);


	public List<InvestmentAccountMappingPurpose> getInvestmentAccountMappingPurposeList(InvestmentAccountMappingPurposeSearchForm searchForm);


	public InvestmentAccountMappingPurpose saveInvestmentAccountMappingPurpose(InvestmentAccountMappingPurpose investmentAccountMappingPurpose);


	public void deleteInvestmentAccountMappingPurpose(InvestmentAccountMappingPurpose investmentAccountMappingPurpose);
}
