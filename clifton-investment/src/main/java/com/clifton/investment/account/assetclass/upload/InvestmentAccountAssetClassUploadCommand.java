package com.clifton.investment.account.assetclass.upload;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.system.upload.SystemUploadCommand;


/**
 * The <code>InvestmentAccountAssetClassUploadCommand</code> is used to upload edits to existing account asset class information
 * <p>
 * This is most commonly used to update Target and Fund Cash %
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class InvestmentAccountAssetClassUploadCommand extends SystemUploadCommand {


	/**
	 * Can optionally select the account on screen instead of including it in the file
	 * This can also be used to filter the sample results to a specific account so users can easily generate a sample file to edit and re-upload
	 */
	private InvestmentAccount investmentAccount;

	/**
	 * When downloading a sample file, can optionally show additional columns that are usually not necessary
	 * only if there is additional information needed (i.e. can find one benchmark security by symbol)
	 */
	private boolean downloadAllColumns;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return InvestmentAccountAssetClass.TABLE_NAME;
	}


	/**
	 * We only support updating, not inserting
	 */
	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.UPDATE;
	}


	/**
	 * Only editing fields on the asset class, not able to create FK beans
	 */
	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		return false;
	}


	/**
	 * All or nothing (since % values is primary use and doesn't make sense if we can only upload some of them)
	 */
	@Override
	@ValueIgnoringGetter
	public boolean isPartialUploadAllowed() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isSimple() {
		return true;
	}


	@Override
	public void applyUploadSpecificProperties(IdentityObject bean, DataRow row) {
		// Copy properties from UploadCommand bean where null on each bean
		InvestmentAccountAssetClass accountAssetClass = (InvestmentAccountAssetClass) bean;
		if (accountAssetClass.getAccount() == null) {
			ValidationUtils.assertNotNull(getInvestmentAccount(), "There are rows in the file that are missing an account.  Please enter them OR select an account from the selection on screen.");
			if (getInvestmentAccount() != null) {
				accountAssetClass.setAccount(getInvestmentAccount());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public boolean isDownloadAllColumns() {
		return this.downloadAllColumns;
	}


	public void setDownloadAllColumns(boolean downloadAllColumns) {
		this.downloadAllColumns = downloadAllColumns;
	}
}
