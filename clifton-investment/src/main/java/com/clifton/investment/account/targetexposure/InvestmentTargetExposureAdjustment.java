package com.clifton.investment.account.targetexposure;

import com.clifton.core.json.custom.BaseCustomJsonObject;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.system.bean.SystemBean;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;


/**
 * <code>InvestmentTargetExposureAdjustment</code> is an object containing metadata for an adjustment to a portfolio target or cash percentage.
 *
 * @author nickk
 */
public class InvestmentTargetExposureAdjustment extends BaseCustomJsonObject {

	private InvestmentTargetExposureAdjustmentTypes targetExposureAdjustmentType;
	private BigDecimal targetExposureAmount;

	private Integer targetExposureManagerAccountId;
	@JsonIgnore // serialize ID not object
	private InvestmentManagerAccount targetExposureManagerAccount;

	private Integer dynamicTargetCalculatorBeanId;
	@JsonIgnore // serialize ID not object
	private SystemBean dynamicTargetCalculatorBean;

	private boolean cashAdjustment;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentTargetExposureAdjustment() {
		// No-arg constructor for web binding
		super();
	}


	public InvestmentTargetExposureAdjustment(String jsonValue) {
		super(jsonValue);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentTargetExposureAdjustmentTypes getTargetExposureAdjustmentType() {
		initializeObject();
		return this.targetExposureAdjustmentType;
	}


	public void setTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes targetExposureAdjustmentType) {
		updateModified(this.targetExposureAdjustmentType, targetExposureAdjustmentType);
		this.targetExposureAdjustmentType = targetExposureAdjustmentType;
	}


	public BigDecimal getTargetExposureAmount() {
		initializeObject();
		return this.targetExposureAmount;
	}


	public void setTargetExposureAmount(BigDecimal targetExposureAmount) {
		updateModified(this.targetExposureAmount, targetExposureAmount);
		this.targetExposureAmount = targetExposureAmount;
	}


	public Integer getTargetExposureManagerAccountId() {
		initializeObject();
		return this.targetExposureManagerAccountId;
	}


	public void setTargetExposureManagerAccountId(Integer targetExposureManagerAccountId) {
		this.targetExposureManagerAccountId = targetExposureManagerAccountId;
	}


	public InvestmentManagerAccount getTargetExposureManagerAccount() {
		initializeObject();
		return this.targetExposureManagerAccount;
	}


	public void setTargetExposureManagerAccount(InvestmentManagerAccount targetExposureManagerAccount) {
		updateModified(this.targetExposureManagerAccount, targetExposureManagerAccount);
		this.targetExposureManagerAccount = targetExposureManagerAccount;
	}


	public Integer getDynamicTargetCalculatorBeanId() {
		initializeObject();
		return this.dynamicTargetCalculatorBeanId;
	}


	public void setDynamicTargetCalculatorBeanId(Integer dynamicTargetCalculatorBeanId) {
		this.dynamicTargetCalculatorBeanId = dynamicTargetCalculatorBeanId;
	}


	public SystemBean getDynamicTargetCalculatorBean() {
		initializeObject();
		return this.dynamicTargetCalculatorBean;
	}


	public void setDynamicTargetCalculatorBean(SystemBean dynamicTargetCalculatorBean) {
		updateModified(this.dynamicTargetCalculatorBean, dynamicTargetCalculatorBean);
		this.dynamicTargetCalculatorBean = dynamicTargetCalculatorBean;
	}


	public boolean isCashAdjustment() {
		initializeObject();
		return this.cashAdjustment;
	}


	public void setCashAdjustment(boolean cashAdjustment) {
		updateModified(this.cashAdjustment, cashAdjustment);
		this.cashAdjustment = cashAdjustment;
	}
}
