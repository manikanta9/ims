package com.clifton.investment.account.targetexposure;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.List;


/**
 * <code>InvestmentTargetExposureAdjustmentAware</code> is an interface for a portfolio object that can contain a target adjustment, a fund cash adjustment, or both.
 *
 * @author manderson
 * @see InvestmentTargetExposureAdjustment
 */
public interface InvestmentTargetExposureAdjustmentAware<T extends InvestmentTargetExposureAdjustmentAware<T, A>, A extends InvestmentTargetExposureAdjustmentAwareAssignment<T, A>> extends IdentityObject, LabeledObject {

	/**
	 * Common properties or calculated getters that are needed for validation
	 */

	public InvestmentAccount getClientInvestmentAccount();


	public boolean isInactive();


	/**
	 * Returns true if target exposure adjustment is not null and the type != NONE
	 */
	default public boolean isTargetExposureAdjustmentUsed() {
		return getTargetExposureAdjustment() != null && (InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(getTargetExposureAdjustment().getTargetExposureAdjustmentType()) || !StringUtils.isEmpty(getTargetExposureAdjustment().getJsonValue()));
	}


	public InvestmentTargetExposureAdjustment getTargetExposureAdjustment();


	public void setTargetExposureAdjustment(InvestmentTargetExposureAdjustment targetExposureAdjustment);


	public BigDecimal getTargetPercent();


	/**
	 * Returns true if cash adjustment is not null and the type != NONE
	 */
	default public boolean isCashAdjustmentUsed() {
		return getCashAdjustment() != null && (InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(getCashAdjustment().getTargetExposureAdjustmentType()) || !StringUtils.isEmpty(getCashAdjustment().getJsonValue()));
	}


	default public InvestmentTargetExposureAdjustment getCashAdjustment() {
		// Not supported
		return null;
	}


	default public void setCashAdjustment(InvestmentTargetExposureAdjustment cashAdjustment) {
		// Not supported
	}


	default public BigDecimal getCashPercent() {
		// Cash adjustments are not supported
		return null;
	}


	public List<A> getTargetExposureAssignmentCombinedList();


	public void setTargetExposureAssignmentCombinedList(List<A> targetExposureAssignmentList);


	/**
	 * Calculated getters for target and cash adjustment allocations
	 */

	default public List<A> getTargetAdjustmentAssignmentList() {
		return CollectionUtils.getFiltered(getTargetExposureAssignmentCombinedList(), assignment -> !assignment.isCashAllocation());
	}


	default public void setTargetAdjustmentAssignmentList(List<A> targetAdjustmentAssignmentList) {
		setTargetExposureAssignmentCombinedList(CollectionUtils.combineCollections(getTargetExposureAssignmentCombinedList(), targetAdjustmentAssignmentList));
	}


	default public List<A> getCashAdjustmentAssignmentList() {
		return CollectionUtils.getFiltered(getTargetExposureAssignmentCombinedList(), InvestmentTargetExposureAdjustmentAwareAssignment::isCashAllocation);
	}


	default public void setCashAdjustmentAssignmentList(List<A> cashAdjustmentAssignmentList) {
		setTargetExposureAssignmentCombinedList(CollectionUtils.combineCollections(getTargetExposureAssignmentCombinedList(), cashAdjustmentAssignmentList));
	}
}
