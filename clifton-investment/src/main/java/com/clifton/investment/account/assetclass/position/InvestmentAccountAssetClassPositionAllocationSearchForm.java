package com.clifton.investment.account.assetclass.position;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


/**
 * The <code>InvestmentAccountAssetClassPositionAllocationSearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentAccountAssetClassPositionAllocationSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "accountAssetClass.id")
	private Integer accountAssetClassId;

	@SearchField(searchField = "replication.id")
	private Integer replicationId;

	@SearchField(searchFieldPath = "accountAssetClass", searchField = "account.id")
	private Integer accountId;

	@SearchField(searchField = "security.id")
	private Integer securityId;

	@SearchField(searchFieldPath = "accountAssetClass", searchField = "displayName,assetClass.name")
	private String assetClassName;

	@SearchField(searchFieldPath = "replication", searchField = "name")
	private String replicationName;

	@SearchField(searchFieldPath = "security", searchField = "symbol,name", sortField = "symbol")
	private String securityLabel;

	@SearchField
	private Short allocationOrder;

	@SearchField
	private BigDecimal allocationQuantity;


	public Integer getAccountAssetClassId() {
		return this.accountAssetClassId;
	}


	public void setAccountAssetClassId(Integer accountAssetClassId) {
		this.accountAssetClassId = accountAssetClassId;
	}


	public Integer getAccountId() {
		return this.accountId;
	}


	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Short getAllocationOrder() {
		return this.allocationOrder;
	}


	public void setAllocationOrder(Short allocationOrder) {
		this.allocationOrder = allocationOrder;
	}


	public BigDecimal getAllocationQuantity() {
		return this.allocationQuantity;
	}


	public void setAllocationQuantity(BigDecimal allocationQuantity) {
		this.allocationQuantity = allocationQuantity;
	}


	public String getAssetClassName() {
		return this.assetClassName;
	}


	public void setAssetClassName(String assetClassName) {
		this.assetClassName = assetClassName;
	}


	public String getSecurityLabel() {
		return this.securityLabel;
	}


	public void setSecurityLabel(String securityLabel) {
		this.securityLabel = securityLabel;
	}


	public Integer getReplicationId() {
		return this.replicationId;
	}


	public void setReplicationId(Integer replicationId) {
		this.replicationId = replicationId;
	}


	public String getReplicationName() {
		return this.replicationName;
	}


	public void setReplicationName(String replicationName) {
		this.replicationName = replicationName;
	}
}
