package com.clifton.investment.account.relationship;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfig;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfigCache;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipHolder;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipMappingSearchForm;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipPurposeSearchForm;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.jetbrains.annotations.Contract;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccountRelationshipServiceImpl</code> class provides basic implementation of the InvestmentAccountRelationshipService interface.
 *
 * @author vgomelsky
 */
@Service
public class InvestmentAccountRelationshipServiceImpl implements InvestmentAccountRelationshipService {

	private AdvancedUpdatableDAO<InvestmentAccountRelationship, Criteria> investmentAccountRelationshipDAO;
	private AdvancedUpdatableDAO<InvestmentAccountRelationshipMapping, Criteria> investmentAccountRelationshipMappingDAO;
	private AdvancedUpdatableDAO<InvestmentAccountRelationshipPurpose, Criteria> investmentAccountRelationshipPurposeDAO;

	private InvestmentAccountRelationshipConfigCache investmentAccountRelationshipConfigCache;
	private DaoNamedEntityCache<InvestmentAccountRelationshipPurpose> investmentAccountRelationshipPurposeCache;

	private InvestmentAccountService investmentAccountService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;


	////////////////////////////////////////////////////////////////////////////
	//////     Investment Account Relationship Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountRelationship getInvestmentAccountRelationship(int id) {
		return getInvestmentAccountRelationshipDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAccountRelationship> getInvestmentAccountRelationshipList(final InvestmentAccountRelationshipSearchForm searchForm) {
		return getInvestmentAccountRelationshipDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentAccountRelationship> getMainInvestmentAccountList(int relatedAccountId, boolean activeOnly) {
		List<InvestmentAccountRelationship> list = getInvestmentAccountRelationshipDAO().findByField("referenceTwo.id", relatedAccountId);
		if (activeOnly) {
			list = BeanUtils.filter(list, InvestmentAccountRelationship::isActive, true);
		}
		return list;
	}


	@Override
	public List<InvestmentAccountRelationship> getRelatedInvestmentAccountList(int mainAccountId, boolean activeOnly) {
		List<InvestmentAccountRelationship> list = getInvestmentAccountRelationshipDAO().findByField("referenceOne.id", mainAccountId);
		if (activeOnly) {
			list = BeanUtils.filter(list, InvestmentAccountRelationship::isActive, true);
		}
		return list;
	}


	@Override
	public boolean isInvestmentAccountRelationshipValid(int mainAccountId, int relatedAccountId, Date date) {
		InvestmentAccountRelationshipConfig config = getInvestmentAccountRelationshipConfig(mainAccountId);
		return config.isInvestmentAccountRelationshipValid(relatedAccountId, date);
	}


	@Override
	public boolean isInvestmentAccountRelationshipForPurposeValid(int mainAccountId, int relatedAccountId, String purpose, Date date) {
		InvestmentAccountRelationshipConfig config = getInvestmentAccountRelationshipConfig(mainAccountId);
		return config.isInvestmentAccountRelationshipForPurposeValid(relatedAccountId, getInvestmentAccountRelationshipPurposeIdByName(purpose, false), date);
	}


	@Override
	public InvestmentAccountRelationship saveInvestmentAccountRelationship(InvestmentAccountRelationship accountRelationship) {
		//See validator for save validation logic.
		return getInvestmentAccountRelationshipDAO().save(accountRelationship);
	}


	@Override
	public void deleteInvestmentAccountRelationship(int id) {
		getInvestmentAccountRelationshipDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////   Investment Account Relationship Purpose Business Methods   ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountRelationshipPurpose getInvestmentAccountRelationshipPurpose(short id) {
		return getInvestmentAccountRelationshipPurposeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentAccountRelationshipPurpose getInvestmentAccountRelationshipPurposeByName(String name) {
		return getInvestmentAccountRelationshipPurposeCache().getBeanForKeyValueStrict(getInvestmentAccountRelationshipPurposeDAO(), name);
	}


	@Contract("_, false -> !null")
	private Short getInvestmentAccountRelationshipPurposeIdByName(String name, boolean allowBlankToReturnNull) {
		if (!StringUtils.isEmpty(name)) {
			InvestmentAccountRelationshipPurpose purposeBean = getInvestmentAccountRelationshipPurposeByName(name);
			return purposeBean.getId();
		}
		else if (!allowBlankToReturnNull) {
			throw new ValidationException("Investment Account Relationship Purpose is Required.");
		}
		return null;
	}


	@Override
	public List<InvestmentAccountRelationshipPurpose> getInvestmentAccountRelationshipPurposeList(final InvestmentAccountRelationshipPurposeSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getMainAccountTypeId() != null || searchForm.getRelatedAccountTypeId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountRelationshipMapping.class, "m");
					sub.setProjection(Projections.property("id"));
					if (searchForm.getMainAccountTypeId() != null) {
						sub.add(Restrictions.eq("mainAccountType.id", searchForm.getMainAccountTypeId()));
					}
					if (searchForm.getRelatedAccountTypeId() != null) {
						sub.add(Restrictions.eq("relatedAccountType.id", searchForm.getRelatedAccountTypeId()));
					}
					sub.add(Restrictions.eqProperty("purpose.id", criteria.getAlias() + ".id"));
					criteria.add(Subqueries.exists(sub));
				}
			}
		};
		return getInvestmentAccountRelationshipPurposeDAO().findBySearchCriteria(searchConfigurer);
	}


	////////////////////////////////////////////////////////////////////////////
	///   Investment Account Relationship Purpose Mapping Business Methods   ///
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountRelationshipMapping getInvestmentAccountRelationshipMapping(int id) {
		return getInvestmentAccountRelationshipMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentAccountRelationshipMapping getInvestmentAccountRelationshipMappingActiveFor(InvestmentAccountRelationship investmentAccountRelationship) {
		InvestmentAccountRelationshipMappingSearchForm searchForm = new InvestmentAccountRelationshipMappingSearchForm();
		searchForm.setMainAccountTypeId(investmentAccountRelationship.getReferenceOne().getType().getId());
		searchForm.setRelatedAccountTypeId(investmentAccountRelationship.getReferenceTwo().getType().getId());
		searchForm.setPurposeId(investmentAccountRelationship.getPurpose().getId());
		List<InvestmentAccountRelationshipMapping> mappingList = getInvestmentAccountRelationshipMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		if (CollectionUtils.isEmpty(mappingList)) {
			return null;
		}
		for (InvestmentAccountRelationshipMapping relationshipMapping : CollectionUtils.getIterable(mappingList)) {
			// Have to check start date first // the null case works for us here
			if (relationshipMapping.isActiveOnDate(investmentAccountRelationship.getStartDate())) {
				//have to check for null end date here because isActiveOnDate does not work with null value
				if (investmentAccountRelationship.getEndDate() != null && relationshipMapping.isActiveOnDate(investmentAccountRelationship.getEndDate())) {
					return relationshipMapping;
				}
				else {
					// if the endDate is null the mapping date must be null
					if (relationshipMapping.getEndDate() == null) {
						return relationshipMapping;
					}
				}
			}
		}
		return null;
	}


	@Override
	public List<InvestmentAccountRelationshipMapping> getInvestmentAccountRelationshipMappingList(InvestmentAccountRelationshipMappingSearchForm searchForm) {
		return getInvestmentAccountRelationshipMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentAccountRelationshipMapping saveInvestmentAccountRelationshipMapping(InvestmentAccountRelationshipMapping bean) {
		return getInvestmentAccountRelationshipMappingDAO().save(bean);
	}


	@Override
	public void deleteInvestmentAccountRelationshipMapping(int id) {
		getInvestmentAccountRelationshipMappingDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////   Investment Account Relationship Config Business Methods   ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountRelationshipConfig getInvestmentAccountRelationshipConfig(int investmentAccountId) {
		InvestmentAccountRelationshipConfig config = getInvestmentAccountRelationshipConfigCache().getInvestmentAccountRelationshipConfig(investmentAccountId);
		if (config == null) {
			List<InvestmentAccountRelationship> fullRelationshipList = getRelatedInvestmentAccountList(investmentAccountId, false);
			List<InvestmentAccountRelationship> subAccountRelationshipList = BeanUtils.filter(fullRelationshipList, relationship -> relationship.getReferenceTwo().getType().isOurAccount());
			List<InvestmentAccountRelationship> holdingRelationshipList = BeanUtils.filter(fullRelationshipList, relationship -> !relationship.getReferenceTwo().getType().isOurAccount());
			List<InvestmentAccountRelationship> reverseRelationshipList = getMainInvestmentAccountList(investmentAccountId, false);

			config = new InvestmentAccountRelationshipConfig(investmentAccountId);
			config.addInvestmentAccountRelationshipList(holdingRelationshipList);
			config.addInvestmentAccountRelationshipList(reverseRelationshipList);

			// Sub Accounts, use the asset class defined at the sub account level
			for (InvestmentAccountRelationship subAccountRel : CollectionUtils.getIterable(subAccountRelationshipList)) {
				List<InvestmentAccountRelationship> list = getRelatedInvestmentAccountList(subAccountRel.getReferenceTwo().getId(), false);
				config.addInvestmentAccountRelationshipList(subAccountRel, list);
			}
			getInvestmentAccountRelationshipConfigCache().setInvestmentAccountRelationshipConfig(investmentAccountId, config);
		}
		return config;
	}


	@Override
	public List<InvestmentAccount> getInvestmentAccountRelatedListForPurpose(int investmentAccountId, Integer securityId, String purpose, Date activeOnDate) {
		// get all relationships active relationship for the purpose and limit them to those allowed for the specified security
		InvestmentAccountRelationshipConfig config = getInvestmentAccountRelationshipConfig(investmentAccountId);
		List<InvestmentAccountRelationshipHolder> relationshipList = config.getInvestmentAccountRelationshipListForPurpose(investmentAccountId,
				getInvestmentAccountRelationshipPurposeIdByName(purpose, true), activeOnDate);
		List<InvestmentAccountRelationshipHolder> filteredList = new ArrayList<>();
		if (securityId != null) {
			for (InvestmentAccountRelationshipHolder relationship : relationshipList) {
				if (relationship.getSecurityGroupId() != null) {
					if (getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(securityId, relationship.getSecurityGroupId())) {
						filteredList.add(relationship);
					}
				}
				else {
					filteredList.add(relationship);
				}
			}
		}
		else {
			filteredList = relationshipList;
		}
		// get accounts from relationships
		return getInvestmentAccountService().getInvestmentAccountListByIds(config.getInvestmentAccountListForRelationshipList(filteredList));
	}


	@Override
	public InvestmentAccount getInvestmentAccountRelatedForPurpose(int investmentAccountId, String purpose, Date activeOnDate) {
		InvestmentAccountRelationshipConfig config = getInvestmentAccountRelationshipConfig(investmentAccountId);
		List<Integer> accountList = config.getInvestmentAccountListForPurpose(getInvestmentAccountRelationshipPurposeIdByName(purpose, false), activeOnDate);
		if (CollectionUtils.getSize(accountList) == 1) {
			return getInvestmentAccountService().getInvestmentAccount(accountList.get(0));
		}
		if (CollectionUtils.getSize(accountList) > 1) {
			throw new ValidationException("Expected one active related account for purpose [" + purpose + "] and active on date [" + DateUtils.fromDateShort(activeOnDate) + "], however, found ["
					+ CollectionUtils.getSize(accountList) + ": " + BeanUtils.getPropertyValues(getInvestmentAccountService().getInvestmentAccountListByIds(accountList), "label", ",") + "]");
		}
		return null;
	}


	@Override
	public InvestmentAccount getInvestmentAccountRelatedForPurposeStrict(int clientAccountId, int holdingAccountId, int investmentSecurityId, String purpose, Date activeOnDate) {
		// first lookup holding account relationship: more important
		List<InvestmentAccount> holdingAccountList = getInvestmentAccountRelatedListForPurpose(holdingAccountId, investmentSecurityId, purpose, activeOnDate);
		int holdingCount = CollectionUtils.getSize(holdingAccountList);
		if (holdingCount == 1) {
			InvestmentAccount result = holdingAccountList.get(0);
			// keep going if it's the same client account
			if (!result.getId().equals(clientAccountId)) {
				return result;
			}
		}

		// zero or multiple holding found: try to limit to one by looking up client account relationships
		List<InvestmentAccount> clientAccountList = getInvestmentAccountRelatedListForPurpose(clientAccountId, investmentSecurityId, purpose, activeOnDate);
		int clientCount = CollectionUtils.getSize(clientAccountList);
		if (holdingCount == 0) {
			// no holding account relationships: rely on client account only
			if (clientCount == 0) {
				throw new NoRelatedAccountFoundException("Cannot find related account for purpose '" + purpose + "' on " + DateUtils.fromDateShort(activeOnDate) + " from Client Account "
						+ getAccountLabelForErrorMessage(clientAccountId) + " or Holding Account " + getAccountLabelForErrorMessage(holdingAccountId));
			}
			if (clientCount == 1) {
				return clientAccountList.get(0);
			}
			throw new MultipleRelatedAccountsFoundException("Found multiple client account relationships for purpose '" + purpose + "' on " + DateUtils.fromDateShort(activeOnDate) + " from Client Account "
					+ getAccountLabelForErrorMessage(clientAccountId) + " and no relationships from Holding Account " + getAccountLabelForErrorMessage(holdingAccountId) + ". Related Accounts: "
					+ BeanUtils.getPropertyValues(clientAccountList, "label", ","));
		}

		// more than 1 holding account: try to limit using client account
		if (clientCount == 0) {
			throw new MultipleRelatedAccountsFoundException("Found multiple holding account relationships for purpose '" + purpose + "' on " + DateUtils.fromDateShort(activeOnDate) + " from Holding Account "
					+ getAccountLabelForErrorMessage(holdingAccountId) + " and no relationships from Client Account " + getInvestmentAccountService().getInvestmentAccount(clientAccountId).getLabel()
					+ ". Related Accounts: " + BeanUtils.getPropertyValues(holdingAccountList, "label", ","));
		}

		// both relationships present: check if there's a single account in both (MUST BE ONLY ONE)
		InvestmentAccount result = null;
		for (InvestmentAccount hAccount : holdingAccountList) {
			for (InvestmentAccount cAccount : clientAccountList) {
				if (hAccount.equals(cAccount)) {
					if (result != null && !result.equals(hAccount)) {
						throw new MultipleRelatedAccountsFoundException("Found multiple client account relationships for purpose '" + purpose + "' on " + DateUtils.fromDateShort(activeOnDate) + " from Client Account "
								+ getAccountLabelForErrorMessage(clientAccountId) + " and from Holding Account " + getAccountLabelForErrorMessage(holdingAccountId)
								+ " and more than 1 account that is in both: " + result.getLabel() + " and " + hAccount.getLabel());
					}
					result = hAccount;
				}
			}
		}

		if (result == null) {
			// last chance to match: check if the only account related to client account is the holding account
			if (clientCount == 1 && clientAccountList.get(0).getId().equals(holdingAccountId)) {
				result = clientAccountList.get(0);
			}
			else {
				throw new MultipleRelatedAccountsFoundException("Found multiple holding account relationships for purpose '" + purpose + "' on " + DateUtils.fromDateShort(activeOnDate) + " from Holding Account "
						+ getAccountLabelForErrorMessage(holdingAccountId) + " and client relationships from Client Account [" + getAccountLabelForErrorMessage(clientAccountId)
						+ "]. But not a single common account.");
			}
		}

		return result;
	}


	private String getAccountLabelForErrorMessage(int accountId) {
		InvestmentAccount a = getInvestmentAccountService().getInvestmentAccount(accountId);
		if (a == null) {
			return "[ID = " + accountId + " UNKNOWN ACCOUNT]";
		}
		return "[" + a.getLabel() + "]";
	}


	@Override
	public List<InvestmentAccountRelationship> getInvestmentAccountRelationshipListForPurpose(int investmentAccountId, String purpose, Date activeOnDate, boolean includeSubAccountRelationships) {
		InvestmentAccountRelationshipConfig config = getInvestmentAccountRelationshipConfig(investmentAccountId);
		List<InvestmentAccountRelationshipHolder> relHolderList = config.getInvestmentAccountRelationshipListForPurpose(includeSubAccountRelationships ? null : investmentAccountId,
				getInvestmentAccountRelationshipPurposeIdByName(purpose, true), activeOnDate);
		if (!CollectionUtils.isEmpty(relHolderList)) {
			return getInvestmentAccountRelationshipDAO().findByPrimaryKeys(BeanUtils.getPropertyValues(relHolderList, "id", Integer.class));
		}
		return null;
	}


	@Override
	public List<InvestmentAccountRelationship> getInvestmentAccountRelationshipListForPurposeActive(int investmentAccountId, String purpose, boolean includeSubAccountRelationships) {
		return getInvestmentAccountRelationshipListForPurpose(investmentAccountId, purpose, new Date(), includeSubAccountRelationships);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////          Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentAccountRelationship, Criteria> getInvestmentAccountRelationshipDAO() {
		return this.investmentAccountRelationshipDAO;
	}


	public void setInvestmentAccountRelationshipDAO(AdvancedUpdatableDAO<InvestmentAccountRelationship, Criteria> investmentAccountRelationshipDAO) {
		this.investmentAccountRelationshipDAO = investmentAccountRelationshipDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAccountRelationshipPurpose, Criteria> getInvestmentAccountRelationshipPurposeDAO() {
		return this.investmentAccountRelationshipPurposeDAO;
	}


	public void setInvestmentAccountRelationshipPurposeDAO(AdvancedUpdatableDAO<InvestmentAccountRelationshipPurpose, Criteria> investmentAccountRelationshipPurposeDAO) {
		this.investmentAccountRelationshipPurposeDAO = investmentAccountRelationshipPurposeDAO;
	}


	public InvestmentAccountRelationshipConfigCache getInvestmentAccountRelationshipConfigCache() {
		return this.investmentAccountRelationshipConfigCache;
	}


	public void setInvestmentAccountRelationshipConfigCache(InvestmentAccountRelationshipConfigCache investmentAccountRelationshipConfigCache) {
		this.investmentAccountRelationshipConfigCache = investmentAccountRelationshipConfigCache;
	}


	public DaoNamedEntityCache<InvestmentAccountRelationshipPurpose> getInvestmentAccountRelationshipPurposeCache() {
		return this.investmentAccountRelationshipPurposeCache;
	}


	public void setInvestmentAccountRelationshipPurposeCache(DaoNamedEntityCache<InvestmentAccountRelationshipPurpose> investmentAccountRelationshipPurposeCache) {
		this.investmentAccountRelationshipPurposeCache = investmentAccountRelationshipPurposeCache;
	}


	public AdvancedUpdatableDAO<InvestmentAccountRelationshipMapping, Criteria> getInvestmentAccountRelationshipMappingDAO() {
		return this.investmentAccountRelationshipMappingDAO;
	}


	public void setInvestmentAccountRelationshipMappingDAO(AdvancedUpdatableDAO<InvestmentAccountRelationshipMapping, Criteria> investmentAccountRelationshipMappingDAO) {
		this.investmentAccountRelationshipMappingDAO = investmentAccountRelationshipMappingDAO;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
