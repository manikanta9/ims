package com.clifton.investment.account.mapping;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>InvestmentAccountMappingPurpose</code> class defines a purpose for
 * an InvestmentAccountMapping.
 *
 * @author davidi
 */
public class InvestmentAccountMappingPurpose extends NamedEntity<Short> {

	/**
	 * An identifier used for to label the purpose data entry field within UI components.
	 */
	private String fieldName;

	/**
	 * A flag that enforces control of 1:1 mapping of an account to its value.  If this flag is set to true,
	 * it indicates that one and only one account can be mapped to a given value. This facilitates reverse lookups
	 * by where a mapping entry is looked up by the mapped value, and should return only one mapping entry.
	 * If the flag is set to false, multiple accounts can be mapped to a given value.
	 */
	private boolean oneAccountValueMappingPerPurpose;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFieldName() {
		return this.fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public boolean isOneAccountValueMappingPerPurpose() {
		return this.oneAccountValueMappingPerPurpose;
	}


	public void setOneAccountValueMappingPerPurpose(boolean oneAccountValueMappingPerPurpose) {
		this.oneAccountValueMappingPerPurpose = oneAccountValueMappingPerPurpose;
	}
}
