package com.clifton.investment.account;


import com.clifton.business.service.BusinessServiceObjective;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.util.StringUtils;


/**
 * The <code>InvestmentAccountServiceObjective</code> assigns BusinessServiceObjectives to InvestmentAccounts
 * and allows to override the description for account specific objective text.
 *
 * @author Mary Anderson
 */
public class InvestmentAccountServiceObjective extends ManyToManyEntity<InvestmentAccount, BusinessServiceObjective, Integer> {

	/**
	 * Optional override to the BusinessServiceObjective Description field
	 */
	public String description;


	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public String getObjectiveDescription() {
		if (!StringUtils.isEmpty(getDescription())) {
			return getDescription();
		}
		else if (getReferenceTwo() != null) {
			return getReferenceTwo().getDescription();
		}
		return null;
	}


	public boolean isObjectiveDescriptionOverridden() {
		return !StringUtils.isEmpty(getDescription());
	}


	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
