package com.clifton.investment.account.group.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class InvestmentAccountGroupAccountSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer groupId;

	@SearchField(searchField = "name", searchFieldPath = "referenceOne")
	private String groupName;

	@SearchField(searchField = "referenceTwo.id")
	private Integer accountId;

	@SearchField(searchField = "type.id", searchFieldPath = "referenceOne")
	private Short groupTypeId;

	@SearchField(searchField = "name", searchFieldPath = "referenceOne.type")
	private String groupTypeName;

	@SearchField(searchField = "name", searchFieldPath = "referenceOne.type", comparisonConditions = ComparisonConditions.EQUALS)
	private String groupTypeNameEquals;

	@SearchField(searchField = "groupAlias", searchFieldPath = "referenceOne", comparisonConditions = ComparisonConditions.EQUALS)
	private String groupAlias;

	@SearchField
	private Boolean primary;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.businessClient")
	private String clientName;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.type")
	private String accountTypeName;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo")
	private String accountName;

	@SearchField(searchField = "number,name", searchFieldPath = "referenceTwo", sortField = "number")
	private String accountLabel;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.teamSecurityGroup")
	private String teamName;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.issuingCompany")
	private String issuingCompanyName;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "referenceTwo")
	private Integer issuingCompanyId;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.workflowState")
	private String workflowStateName;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.workflowStatus")
	private String workflowStatusName;

	@SearchField(searchField = "referenceTwo.baseCurrency.id")
	private Integer baseCurrencyId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchFieldPath = "referenceTwo.businessService", searchField = "id,parent.id,parent.parent.id,parent.parent.parent.id", leftJoin = true, sortField = "name")
	private Short businessServiceOrParentId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroupAccountSearchForm() {
		//
	}


	public InvestmentAccountGroupAccountSearchForm(int groupId) {
		this.groupId = groupId;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}


	public Integer getAccountId() {
		return this.accountId;
	}


	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}


	public Short getGroupTypeId() {
		return this.groupTypeId;
	}


	public void setGroupTypeId(Short groupTypeId) {
		this.groupTypeId = groupTypeId;
	}


	public String getGroupTypeName() {
		return this.groupTypeName;
	}


	public void setGroupTypeName(String groupTypeName) {
		this.groupTypeName = groupTypeName;
	}


	public Boolean getPrimary() {
		return this.primary;
	}


	public void setPrimary(Boolean primary) {
		this.primary = primary;
	}


	public String getGroupName() {
		return this.groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public String getAccountTypeName() {
		return this.accountTypeName;
	}


	public void setAccountTypeName(String accountTypeName) {
		this.accountTypeName = accountTypeName;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getTeamName() {
		return this.teamName;
	}


	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}


	public String getIssuingCompanyName() {
		return this.issuingCompanyName;
	}


	public void setIssuingCompanyName(String issuingCompanyName) {
		this.issuingCompanyName = issuingCompanyName;
	}


	public String getGroupAlias() {
		return this.groupAlias;
	}


	public void setGroupAlias(String groupAlias) {
		this.groupAlias = groupAlias;
	}


	public String getAccountLabel() {
		return this.accountLabel;
	}


	public void setAccountLabel(String accountLabel) {
		this.accountLabel = accountLabel;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	public String getWorkflowStatusName() {
		return this.workflowStatusName;
	}


	public void setWorkflowStatusName(String workflowStatusName) {
		this.workflowStatusName = workflowStatusName;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public Integer getIssuingCompanyId() {
		return this.issuingCompanyId;
	}


	public void setIssuingCompanyId(Integer issuingCompanyId) {
		this.issuingCompanyId = issuingCompanyId;
	}


	public String getGroupTypeNameEquals() {
		return this.groupTypeNameEquals;
	}


	public void setGroupTypeNameEquals(String groupTypeNameEquals) {
		this.groupTypeNameEquals = groupTypeNameEquals;
	}
}
