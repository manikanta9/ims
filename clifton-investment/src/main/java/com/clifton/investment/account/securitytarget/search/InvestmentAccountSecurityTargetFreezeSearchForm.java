package com.clifton.investment.account.securitytarget.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


/**
 * <code>InvestmentAccountSecurityTargetFreezeSearchForm</code> represents the search configuration for {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetFreeze} entities.
 *
 * @author NickK
 */
public class InvestmentAccountSecurityTargetFreezeSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "freezeType.id")
	private Short freezeTypeId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchField = "targetUnderlyingSecurity.id")
	private Integer targetUnderlyingSecurityId;

	@SearchField(searchField = "targetUnderlyingSecurity.id")
	private Integer[] targetUnderlyingSecurityIds;

	@SearchField(searchField = "causeInvestmentSecurity.id")
	private Integer causeInvestmentSecurityId;

	@SearchField
	private BigDecimal freezeUnderlyingQuantity;

	@SearchField(searchField = "causeSystemTable.id")
	private Short causeSystemTableId;

	@SearchField
	private Integer causeFKFieldId;

	@SearchField
	private String freezeNote;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Short getFreezeTypeId() {
		return this.freezeTypeId;
	}


	public void setFreezeTypeId(Short freezeTypeId) {
		this.freezeTypeId = freezeTypeId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public Integer getCauseInvestmentSecurityId() {
		return this.causeInvestmentSecurityId;
	}


	public void setCauseInvestmentSecurityId(Integer causeInvestmentSecurityId) {
		this.causeInvestmentSecurityId = causeInvestmentSecurityId;
	}


	public BigDecimal getFreezeUnderlyingQuantity() {
		return this.freezeUnderlyingQuantity;
	}


	public void setFreezeUnderlyingQuantity(BigDecimal freezeUnderlyingQuantity) {
		this.freezeUnderlyingQuantity = freezeUnderlyingQuantity;
	}


	public Short getCauseSystemTableId() {
		return this.causeSystemTableId;
	}


	public void setCauseSystemTableId(Short causeSystemTableId) {
		this.causeSystemTableId = causeSystemTableId;
	}


	public Integer getCauseFKFieldId() {
		return this.causeFKFieldId;
	}


	public void setCauseFKFieldId(Integer causeFKFieldId) {
		this.causeFKFieldId = causeFKFieldId;
	}


	public String getFreezeNote() {
		return this.freezeNote;
	}


	public void setFreezeNote(String freezeNote) {
		this.freezeNote = freezeNote;
	}


	public Integer getTargetUnderlyingSecurityId() {
		return this.targetUnderlyingSecurityId;
	}


	public void setTargetUnderlyingSecurityId(Integer targetUnderlyingSecurityId) {
		this.targetUnderlyingSecurityId = targetUnderlyingSecurityId;
	}


	public Integer[] getTargetUnderlyingSecurityIds() {
		return this.targetUnderlyingSecurityIds;
	}


	public void setTargetUnderlyingSecurityIds(Integer[] targetUnderlyingSecurityIds) {
		this.targetUnderlyingSecurityIds = targetUnderlyingSecurityIds;
	}
}
