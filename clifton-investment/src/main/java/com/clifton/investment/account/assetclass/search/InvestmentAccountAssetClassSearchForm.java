package com.clifton.investment.account.assetclass.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass.ClientDirectedCashOptions;

import java.math.BigDecimal;
import java.util.Date;


public class InvestmentAccountAssetClassSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "assetClass.name,assetClass.description,displayName")
	private String searchPattern;

	@SearchField(searchField = "displayName,assetClass.name")
	private String assetClassName;

	@SearchField(searchField = "displayName,assetClass.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String assetClassNameEquals;

	@SearchField
	private String displayName;

	@SearchField(searchField = "displayName", comparisonConditions = ComparisonConditions.EQUALS)
	private String displayNameEquals;

	@SearchField(searchField = "account.id")
	private Integer accountId;

	@SearchField(searchFieldPath = "account", searchField = "number,name", sortField = "number")
	private String investmentAccountLabel;

	@SearchField(searchFieldPath = "primaryReplication", searchField = "name")
	private String primaryReplication;

	@SearchField(searchFieldPath = "secondaryReplication", searchField = "name")
	private String secondaryReplication;

	private Boolean targetExposureAdjustmentUsed;

	private Boolean cashAdjustmentUsed;

	@SearchField
	private Boolean rollupAssetClass;

	@SearchField
	private Boolean excludeFromExposure;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "parent.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean emptyParentOnly;

	// Should never need to be set anywhere, when retrieving lists, always retrieve inactive ones.
	@SearchField
	private Boolean inactive = false;

	@SearchField(searchField = "primaryReplication.id,secondaryReplication.id")
	private Integer useReplicationId;

	@SearchField(searchField = "primaryReplication.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean usesPrimaryReplication;

	@SearchField(searchField = "secondaryReplicationAmount", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean populatedSecondaryReplicationAmount;

	private Boolean clientDirectedCashUsed;

	@SearchField
	private ClientDirectedCashOptions clientDirectedCashOption;

	@SearchField
	private BigDecimal clientDirectedCashAmount;

	@SearchField
	private Date clientDirectedCashDate;

	@SearchField
	private String durationFieldNameOverride;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getAssetClassName() {
		return this.assetClassName;
	}


	public void setAssetClassName(String assetClassName) {
		this.assetClassName = assetClassName;
	}


	public String getAssetClassNameEquals() {
		return this.assetClassNameEquals;
	}


	public void setAssetClassNameEquals(String assetClassNameEquals) {
		this.assetClassNameEquals = assetClassNameEquals;
	}


	public String getDisplayName() {
		return this.displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public String getDisplayNameEquals() {
		return this.displayNameEquals;
	}


	public void setDisplayNameEquals(String displayNameEquals) {
		this.displayNameEquals = displayNameEquals;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public String getPrimaryReplication() {
		return this.primaryReplication;
	}


	public void setPrimaryReplication(String primaryReplication) {
		this.primaryReplication = primaryReplication;
	}


	public String getSecondaryReplication() {
		return this.secondaryReplication;
	}


	public void setSecondaryReplication(String secondaryReplication) {
		this.secondaryReplication = secondaryReplication;
	}


	public Boolean getTargetExposureAdjustmentUsed() {
		return this.targetExposureAdjustmentUsed;
	}


	public void setTargetExposureAdjustmentUsed(Boolean targetExposureAdjustmentUsed) {
		this.targetExposureAdjustmentUsed = targetExposureAdjustmentUsed;
	}


	public Boolean getCashAdjustmentUsed() {
		return this.cashAdjustmentUsed;
	}


	public void setCashAdjustmentUsed(Boolean cashAdjustmentUsed) {
		this.cashAdjustmentUsed = cashAdjustmentUsed;
	}


	public Boolean getRollupAssetClass() {
		return this.rollupAssetClass;
	}


	public Boolean getExcludeFromExposure() {
		return this.excludeFromExposure;
	}


	public Boolean getEmptyParentOnly() {
		return this.emptyParentOnly;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public Boolean getUsesPrimaryReplication() {
		return this.usesPrimaryReplication;
	}


	public Boolean getPopulatedSecondaryReplicationAmount() {
		return this.populatedSecondaryReplicationAmount;
	}


	public Boolean getClientDirectedCashUsed() {
		return this.clientDirectedCashUsed;
	}


	public Integer getAccountId() {
		return this.accountId;
	}


	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}


	public void setRollupAssetClass(Boolean rollupAssetClass) {
		this.rollupAssetClass = rollupAssetClass;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public void setEmptyParentOnly(Boolean emptyParentOnly) {
		this.emptyParentOnly = emptyParentOnly;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public Integer getUseReplicationId() {
		return this.useReplicationId;
	}


	public void setUseReplicationId(Integer useReplicationId) {
		this.useReplicationId = useReplicationId;
	}


	public void setPopulatedSecondaryReplicationAmount(Boolean populatedSecondaryReplicationAmount) {
		this.populatedSecondaryReplicationAmount = populatedSecondaryReplicationAmount;
	}


	public void setExcludeFromExposure(Boolean excludeFromExposure) {
		this.excludeFromExposure = excludeFromExposure;
	}


	public void setClientDirectedCashUsed(Boolean clientDirectedCashUsed) {
		this.clientDirectedCashUsed = clientDirectedCashUsed;
	}


	public ClientDirectedCashOptions getClientDirectedCashOption() {
		return this.clientDirectedCashOption;
	}


	public void setClientDirectedCashOption(ClientDirectedCashOptions clientDirectedCashOption) {
		this.clientDirectedCashOption = clientDirectedCashOption;
	}


	public BigDecimal getClientDirectedCashAmount() {
		return this.clientDirectedCashAmount;
	}


	public void setClientDirectedCashAmount(BigDecimal clientDirectedCashAmount) {
		this.clientDirectedCashAmount = clientDirectedCashAmount;
	}


	public Date getClientDirectedCashDate() {
		return this.clientDirectedCashDate;
	}


	public void setClientDirectedCashDate(Date clientDirectedCashDate) {
		this.clientDirectedCashDate = clientDirectedCashDate;
	}


	public void setUsesPrimaryReplication(Boolean usesPrimaryReplication) {
		this.usesPrimaryReplication = usesPrimaryReplication;
	}


	public String getDurationFieldNameOverride() {
		return this.durationFieldNameOverride;
	}


	public void setDurationFieldNameOverride(String durationFieldNameOverride) {
		this.durationFieldNameOverride = durationFieldNameOverride;
	}
}
