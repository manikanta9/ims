package com.clifton.investment.account.securitytarget.workflow.action;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * Base Transition Action for Objects need to operate on an {@link InvestmentAccountSecurityTarget}
 *
 * @author davidi
 */
public abstract class BaseInvestmentAccountSecurityTargetWorkflowTargetTransitionAction implements WorkflowTransitionActionHandler<WorkflowTask> {

	private DaoLocator daoLocator;
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected IdentityObject getLinkedEntity(WorkflowTask task) {
		SystemTable linkedEntityTable = task.getDefinition().getLinkedEntityTable();
		ValidationUtils.assertNotNull(linkedEntityTable, "Linked Entity Table must be configured on the Workflow Task Definition to use this Transition Action");
		ReadOnlyDAO<IdentityObject> linkedEntityDao = getDaoLocator().locate(linkedEntityTable.getName());
		return linkedEntityDao.findByPrimaryKey(task.getLinkedEntityFkFieldId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}
}
