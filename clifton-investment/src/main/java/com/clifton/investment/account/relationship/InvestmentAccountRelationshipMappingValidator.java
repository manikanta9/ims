package com.clifton.investment.account.relationship;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentAccountRelationshipMappingValidator</code> performs validation on modify ensuring if any mapping is modified any corresponding
 * <code>InvestmentAccountRelationship</code> is not effected adversely.
 *
 * @author jonathanr
 */
@Component
public class InvestmentAccountRelationshipMappingValidator extends SelfRegisteringDaoValidator<InvestmentAccountRelationshipMapping> {

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	@SuppressWarnings("unused")
	public void validate(InvestmentAccountRelationshipMapping bean, DaoEventTypes config) throws ValidationException {
		// DO NOTHING, USE THE METHOD WITH THE DAO
	}


	@Override
	public void validate(InvestmentAccountRelationshipMapping relationshipMapping, DaoEventTypes config, ReadOnlyDAO<InvestmentAccountRelationshipMapping> dao) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			saveValidation(relationshipMapping, config, dao);
		}

		if (config.isDelete()) {
			deleteValidation(relationshipMapping, config, dao);
		}
	}


	public void saveValidation(InvestmentAccountRelationshipMapping relationshipMapping, DaoEventTypes config, ReadOnlyDAO<InvestmentAccountRelationshipMapping> dao) {
		if (relationshipMapping.getEndDate() != null) {
			ValidationUtils.assertTrue(DateUtils.isDateBefore(relationshipMapping.getStartDate(), relationshipMapping.getEndDate(), true), "Start date must be before the end date.");
		}
		List<InvestmentAccountRelationshipMapping> list = dao.findByFields(new String[]{"mainAccountType.id", "relatedAccountType.id", "purpose.id"}, new Object[]{relationshipMapping.getMainAccountType().getId(), relationshipMapping.getRelatedAccountType().getId(), relationshipMapping.getPurpose().getId()});
		for (InvestmentAccountRelationshipMapping existingRelationshipMapping : CollectionUtils.getIterable(list)) {
			if (!CompareUtils.isEqual(relationshipMapping, existingRelationshipMapping) && DateUtils.isOverlapInDates(relationshipMapping.getStartDate(), relationshipMapping.getEndDate(), existingRelationshipMapping.getStartDate(), existingRelationshipMapping.getEndDate())) {
				throw new ValidationException("There exists a relationship mapping for " + relationshipMapping.getMainAccountType().getName() + " - " + relationshipMapping.getRelatedAccountType().getName() + " for purpose " + relationshipMapping.getPurpose().getName() + " and overlapping date range.");
			}
		}
	}


	public void deleteValidation(InvestmentAccountRelationshipMapping relationshipMapping, DaoEventTypes config, ReadOnlyDAO<InvestmentAccountRelationshipMapping> dao) {
		InvestmentAccountRelationshipSearchForm searchForm = new InvestmentAccountRelationshipSearchForm();
		searchForm.setMainAccountTypeId(relationshipMapping.getMainAccountType().getId());
		searchForm.setRelatedAccountTypeId(relationshipMapping.getRelatedAccountType().getId());
		searchForm.setPurposeId(relationshipMapping.getPurpose().getId());
		List<InvestmentAccountRelationship> relationshipList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipList(searchForm);
		for (InvestmentAccountRelationship relationship : CollectionUtils.getIterable(relationshipList)) {
			if (relationshipMapping.isActiveOnDate(relationship.getStartDate()) || relationshipMapping.isActiveOnDate(relationship.getEndDate())) {
				throw new ValidationException("There exists a relationship for " + relationshipMapping.getMainAccountType().getName() + " - " + relationshipMapping.getRelatedAccountType().getName() + " for purpose " + relationshipMapping.getPurpose().getName() + " and overlapping date range.");
			}
		}
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}
}
