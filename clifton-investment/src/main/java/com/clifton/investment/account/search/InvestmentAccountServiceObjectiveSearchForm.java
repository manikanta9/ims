package com.clifton.investment.account.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class InvestmentAccountServiceObjectiveSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "referenceOne.id")
	private Integer investmentAccountId;

	@SearchField(searchFieldPath = "referenceOne", searchField = "number,name", sortField = "number")
	private String investmentAccountLabel;

	@SearchField(searchField = "referenceTwo.id")
	private Integer businessServiceObjectiveId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "businessService.id")
	private Short businessServiceId;

	@SearchField
	private String description;

	@SearchField(searchField = "description,referenceTwo.description", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String coalesceObjectiveDescription;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "name")
	private String objectiveName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public Integer getBusinessServiceObjectiveId() {
		return this.businessServiceObjectiveId;
	}


	public void setBusinessServiceObjectiveId(Integer businessServiceObjectiveId) {
		this.businessServiceObjectiveId = businessServiceObjectiveId;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCoalesceObjectiveDescription() {
		return this.coalesceObjectiveDescription;
	}


	public void setCoalesceObjectiveDescription(String coalesceObjectiveDescription) {
		this.coalesceObjectiveDescription = coalesceObjectiveDescription;
	}


	public String getObjectiveName() {
		return this.objectiveName;
	}


	public void setObjectiveName(String objectiveName) {
		this.objectiveName = objectiveName;
	}
}
