package com.clifton.investment.account;


import com.clifton.business.service.BusinessService;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>InvestmentAccountBusinessService</code> class associates a business service with an
 * investment account.  For example, PIOS account may have multiple sub-services associated with it:
 * cash securitization, rebalancing, etc.
 *
 * @author vgomelsky
 */
public class InvestmentAccountBusinessService extends BaseEntity<Integer> {

	private InvestmentAccount investmentAccount;
	private BusinessService businessService;

	private Date startDate;
	private Date endDate;


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public BusinessService getBusinessService() {
		return this.businessService;
	}


	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
