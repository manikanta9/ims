package com.clifton.investment.account.mapping;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;


/**
 * The <code>InvestmentAccountMapping</code> class represents a mapping between an investment
 * account and a field value.
 *
 * @author davidi
 */
public class InvestmentAccountMapping extends BaseEntity<Integer> {

	private InvestmentAccount investmentAccount;

	private InvestmentAccountMappingPurpose investmentAccountMappingPurpose;

	private String fieldValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		InvestmentAccount account = getInvestmentAccount();
		InvestmentAccountMappingPurpose purpose = getInvestmentAccountMappingPurpose();
		return account == null || purpose == null ? null : "Map " + account.getLabel() + " to " + getFieldValue() + " for " + purpose.getName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public InvestmentAccountMappingPurpose getInvestmentAccountMappingPurpose() {
		return this.investmentAccountMappingPurpose;
	}


	public void setInvestmentAccountMappingPurpose(InvestmentAccountMappingPurpose investmentAccountMappingPurpose) {
		this.investmentAccountMappingPurpose = investmentAccountMappingPurpose;
	}


	public String getFieldValue() {
		return this.fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
}
