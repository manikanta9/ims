package com.clifton.investment.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class InvestmentInstrumentRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof InvestmentInstrument;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		InvestmentInstrument instrument = (InvestmentInstrument) entity;

		if (instrument.getHierarchy() != null) {
			relatedEntityList.add(instrument.getHierarchy());
		}

		return relatedEntityList;
	}
}
