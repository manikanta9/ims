package com.clifton.investment.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.search.InvestmentReplicationAllocationSearchForm;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class InvestmentReplicationRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private InvestmentReplicationService investmentReplicationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof InvestmentReplication;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		InvestmentReplication replication = (InvestmentReplication) entity;

		InvestmentReplicationAllocationSearchForm searchForm = new InvestmentReplicationAllocationSearchForm();
		searchForm.setReplicationId(replication.getId());
		searchForm.setDynamicResult(false); // usually change daily and are not user defined
		List<InvestmentReplicationAllocation> allocations = getInvestmentReplicationService().getInvestmentReplicationAllocationList(searchForm);
		if (!CollectionUtils.isEmpty(allocations)) {
			relatedEntityList.addAll(allocations);
		}

		return relatedEntityList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////

	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}
}
