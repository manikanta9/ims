package com.clifton.investment.lifecycle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class InvestmentSecurityRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof InvestmentSecurity;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		InvestmentSecurity security = (InvestmentSecurity) entity;

		InvestmentInstrument instrument = security.getInstrument();
		if (instrument != null) {
			relatedEntityList.add(instrument);
		}

		// Notes - should be a more generic way to do this (check tables for note types?)
		SystemNoteSearchForm noteSearchForm = new SystemNoteSearchForm();
		noteSearchForm.setEntityTableName(InvestmentSecurity.TABLE_NAME);
		noteSearchForm.setFkFieldId(BeanUtils.getIdentityAsLong(security));
		noteSearchForm.setIncludeNotesAsLinkedEntity(true); // also include notes from Trades, etc.
		relatedEntityList.addAll(getSystemNoteService().getSystemNoteListForEntity(noteSearchForm));

		return relatedEntityList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}
}
