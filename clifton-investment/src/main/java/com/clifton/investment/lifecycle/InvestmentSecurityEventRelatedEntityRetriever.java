package com.clifton.investment.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventClientElection;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventClientElectionSearchForm;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class InvestmentSecurityEventRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private InvestmentSecurityEventService investmentSecurityEventService;
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof InvestmentSecurityEvent;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		InvestmentSecurityEvent event = (InvestmentSecurityEvent) entity;

		List<InvestmentSecurityEventDetail> detailList = getInvestmentSecurityEventService().getInvestmentSecurityEventDetailListByEvent(event.getId());
		if (!CollectionUtils.isEmpty(detailList)) {
			relatedEntityList.addAll(detailList);
		}

		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setSecurityEventId(event.getId());
		List<InvestmentSecurityEventPayout> payoutList = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventPayoutList(payoutSearchForm);
		if (!CollectionUtils.isEmpty(payoutList)) {
			relatedEntityList.addAll(payoutList);
		}

		InvestmentSecurityEventClientElectionSearchForm electionSearchForm = new InvestmentSecurityEventClientElectionSearchForm();
		electionSearchForm.setSecurityEventId(event.getId());
		List<InvestmentSecurityEventClientElection> electionList = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventClientElectionList(electionSearchForm);
		if (!CollectionUtils.isEmpty(electionList)) {
			relatedEntityList.addAll(electionList);
		}

		return relatedEntityList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public InvestmentSecurityEventPayoutService getInvestmentSecurityEventPayoutService() {
		return this.investmentSecurityEventPayoutService;
	}


	public void setInvestmentSecurityEventPayoutService(InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService) {
		this.investmentSecurityEventPayoutService = investmentSecurityEventPayoutService;
	}
}
