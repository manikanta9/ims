package com.clifton.investment.lifecycle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentSetupService;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class InvestmentEventRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private DocumentSetupService documentSetupService;
	private InvestmentCalendarService investmentCalendarService;
	private SystemNoteService systemNoteService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof InvestmentEvent;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		InvestmentEvent event = (InvestmentEvent) entity;
		// get fully populated event with manager adjustments
		event = getInvestmentCalendarService().getInvestmentEvent(event.getId());

		if (event.getRecurrenceSchedule() != null) {
			relatedEntityList.add(event.getRecurrenceSchedule());
		}
		else if (event.getSeriesDefinition() != null) {
			relatedEntityList.add(event.getSeriesDefinition());
			if (event.getSeriesDefinition().getRecurrenceSchedule() != null) {
				relatedEntityList.add((event.getSeriesDefinition().getRecurrenceSchedule()));
			}
		}

		if (!CollectionUtils.isEmpty(event.getManagerAdjustmentList())) {
			relatedEntityList.addAll(event.getManagerAdjustmentList());
		}

		List<DocumentFile> fileList = getDocumentSetupService().getDocumentFileListForEntity(InvestmentEvent.TABLE_NAME, BeanUtils.getIdentityAsLong(event));
		if (!CollectionUtils.isEmpty(fileList)) {
			relatedEntityList.addAll(fileList);
		}

		// Notes - should be a more generic way to do this (check tables for note types?)
		SystemNoteSearchForm noteSearchForm = new SystemNoteSearchForm();
		noteSearchForm.setEntityTableName(InvestmentEvent.TABLE_NAME);
		noteSearchForm.setFkFieldId(BeanUtils.getIdentityAsLong(event));
		// Not adding for linked entities - could be messy if we have notes on accounts
		relatedEntityList.addAll(getSystemNoteService().getSystemNoteListForEntity(noteSearchForm));

		return relatedEntityList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}
}
