package com.clifton.investment.lifecycle;

import com.clifton.business.client.BusinessClient;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class InvestmentAccountRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof InvestmentAccount;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		InvestmentAccount account = (InvestmentAccount) entity;

		BusinessClient client = account.getBusinessClient();
		if (client != null) {
			relatedEntityList.add(client);
			if (client.getClientRelationship() != null) {
				relatedEntityList.add(client.getClientRelationship());
			}
		}

		// Notes - should be a more generic way to do this (check tables for note types?)
		SystemNoteSearchForm noteSearchForm = new SystemNoteSearchForm();
		noteSearchForm.setEntityTableName(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME);
		noteSearchForm.setFkFieldId(BeanUtils.getIdentityAsLong(account));
		// Not adding for linked entities - could be messy if we have notes on accounts
		relatedEntityList.addAll(getSystemNoteService().getSystemNoteListForEntity(noteSearchForm));

		return relatedEntityList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}
}
