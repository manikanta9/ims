package com.clifton.investment.instrument.allocation.calculator;

import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentSecurityAllocationRebalanceCalculator</code> is an extension of the InvestmentSecurityAllocationCalculator, but
 * also defines methods for rebalancing.  Not all calculators use rebalancing, so this interface should be used if rebalancing is supported.
 *
 * @author manderson
 */
public interface InvestmentSecurityAllocationRebalanceCalculator extends InvestmentSecurityAllocationCalculator {


	/**
	 * Used for SHARE_PRICE only at this time (i.e. InvestmentSecurityAllocationTypes.isAutoRebalancingSupported() == true)
	 * Creates new set of allocations for the security using the base weights or shares on the base Date
	 * and starting new allocations on the selected startDate. Note: All previously active allocations will be
	 * first ended on startDate-1 and all allocations from the base date will be copied with a new start date and rebalanced based on selected weights or shares
	 *
	 * @param useBaseWeights - if true, uses base weights from selected base date, otherwise uses shares
	 * @param startingPrice  - if not set will use 100, used to calculate shares from weights
	 */
	public void rebalanceInvestmentSecurity(InvestmentSecurity security, short dataSourceId, Date baseDate, boolean useBaseWeights, Date startDate, BigDecimal startingPrice);


	/**
	 * Can be used to recalculate base weights or shares on given date.
	 * <p/>
	 * If date is blank, will recalculate on all rebalance dates for the security.
	 * If shares should be used to update weights, pass true - otherwise if weights should be used to update shares - pass false
	 *
	 * @param startingPrice - if not set will use 100, used only when calculating shares from weights (updateWeights = false)
	 */
	public void recalculateRebalance(InvestmentSecurity security, short dataSourceId, Date date, BigDecimal startingPrice, boolean updateWeights);
}
