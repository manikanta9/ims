package com.clifton.investment.instrument.event;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The InvestmentSecurityEventStatus class represents possible statuses for {@link InvestmentSecurityEvent} object.
 * An event is always in a particular status and that status may change over time: Incomplete => Approved.
 * <p>
 * Available statuses: Incomplete, In Conflict, Conditionally Approved, Approved, Cancelled, Deleted, Unsupported.
 *
 * @author vgomelsky
 */
@CacheByName
public class InvestmentSecurityEventStatus extends NamedEntity<Short> {

	public static final String STATUS_APPROVED = "Approved";
	public static final String STATUS_CONDITIONALLY_APPROVED = "Conditionally Approved";
	public static final String STATUS_INCOMPLETE = "Incomplete";
	public static final String STATUS_IN_CONFLICT = "In Conflict";
	public static final String STATUS_NOT_SUPPORTED = "Not Supported";
	public static final String STATUS_CANCELLED = "Cancelled";
	public static final String STATUS_DELETED = "Deleted";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If true, event journals can be generated for security events in this status: Approved, Conditionally Approved, Not Supported.
	 */
	private boolean eventJournalAllowed;

	/**
	 * If true, event journals will be generated automatically by the system: Approved.
	 * If false, event journals can be generated manually by a user after verifying all event fields.
	 */
	private boolean eventJournalAutomatic;


	/**
	 * Events in this status will be ignored by our system: Cancelled, Deleted.
	 * No event journals nor forecasts will be generated from events in this status.
	 */
	private boolean eventJournalNotApplicable;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isEventJournalAllowed() {
		return this.eventJournalAllowed;
	}


	public void setEventJournalAllowed(boolean eventJournalAllowed) {
		this.eventJournalAllowed = eventJournalAllowed;
	}


	public boolean isEventJournalAutomatic() {
		return this.eventJournalAutomatic;
	}


	public void setEventJournalAutomatic(boolean eventJournalAutomatic) {
		this.eventJournalAutomatic = eventJournalAutomatic;
	}


	public boolean isEventJournalNotApplicable() {
		return this.eventJournalNotApplicable;
	}


	public void setEventJournalNotApplicable(boolean eventJournalNotApplicable) {
		this.eventJournalNotApplicable = eventJournalNotApplicable;
	}
}
