package com.clifton.investment.instrument.search;


import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateOrderBySqlFormula;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureWeight;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.schema.column.value.SystemColumnValue;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.hibernate.type.DateType;

import java.util.Date;


/**
 * The <code>InvestmentSecuritySearchFormConfigurer</code> class defines search configuration for InvestmentSecurity objects.
 *
 * @author vgomelsky
 */
public class InvestmentSecuritySearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final InvestmentSecurityGroupService investmentSecurityGroupService;


	public InvestmentSecuritySearchFormConfigurer(SecuritySearchForm searchForm, InvestmentSecurityGroupService investmentSecurityGroupService) {
		super(searchForm);
		this.investmentSecurityGroupService = investmentSecurityGroupService;

		// Active is a custom search restriction that sets other fields, if set as a search restriction
		// remove it because there is no definition attached to it.
		if (searchForm.containsSearchRestriction("active") || searchForm.getActive() != null) {
			Boolean active = searchForm.getActive();
			// If not set explicitly on the form, it's in the restriction list
			if (active == null) {
				SearchRestriction restrict = searchForm.getSearchRestriction("active");
				active = Boolean.valueOf((String) restrict.getValue());
			}
			Date today = DateUtils.clearTime(new Date());
			if (active) {
				// If active, set active on date to today
				searchForm.setActiveOnDate(today);
			}
			else {
				// If not active, set not active date
				searchForm.setNotActiveOnDate(today);
			}
			searchForm.removeSearchRestriction("active");
		}

		if (searchForm.getNoMarketDataFieldOnDateId() != null) {
			searchForm.setNoMarketDataFieldOnDateType("Specified");
		}

		// If either is supplied, then both are required - This is validation configureCriteria will apply the actual filters
		if (!StringUtils.isEmpty(searchForm.getCustomColumnName()) || !StringUtils.isEmpty(searchForm.getCustomColumnValue())) {
			if (StringUtils.isEmpty(searchForm.getCustomColumnName())) {
				throw new ValidationException("Custom Column Name is required for Custom Column Value filter [" + searchForm.getCustomColumnValue() + "]");
			}
			if (StringUtils.isEmpty(searchForm.getCustomColumnValue())) {
				throw new ValidationException("Custom Column Value is required for Custom Column Name filter [" + searchForm.getCustomColumnName() + "]");
			}
		}
	}


	@Override
	public boolean configureOrderBy(Criteria criteria) {
		SecuritySearchForm searchForm = (SecuritySearchForm) getSortableSearchForm();

		// custom order by for search patters to move exact match to the top of the list
		int len = StringUtils.length(searchForm.getSearchPattern());
		if (len == 0) {
			len = StringUtils.length(searchForm.getSearchPatternUsingBeginsWith());
		}
		if (len > 0) {
			criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(CASE WHEN LEN(Symbol) = " + len + " THEN 0 WHEN LEN(Cusip) = " + len + " THEN 1 WHEN LEN(InvestmentSecurityName) = " + len
					+ " THEN 3 ELSE 4 END) ASC"));
		}
		if (!super.configureOrderBy(criteria)) {
			criteria.addOrder(Order.asc("symbol"));
			criteria.addOrder(Order.asc("name"));
		}
		return true;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		// also apply custom filters, if set
		SecuritySearchForm searchForm = (SecuritySearchForm) getSortableSearchForm();
		if (searchForm.getActiveOnDate() != null) {
			LogicalExpression startDateFilter = Restrictions.or(Restrictions.isNull("startDate"), Restrictions.le("startDate", searchForm.getActiveOnDate()));
			LogicalExpression endDateFilter = Restrictions.or(Restrictions.isNull("endDate"),
					Restrictions.sqlRestriction("COALESCE({alias}.EarlyTerminationDate, {alias}.EndDate) >= ?", searchForm.getActiveOnDate(), new DateType()));

			// NOTE: first notice date is set for futures only: cannot trade after it
			LogicalExpression firstNoticeDateFilter = Restrictions.or(Restrictions.isNull("firstNoticeDate"), Restrictions.ge("firstNoticeDate", searchForm.getActiveOnDate()));
			criteria.add(Restrictions.and(startDateFilter, Restrictions.and(endDateFilter, firstNoticeDateFilter)));

			if (!StringUtils.isEmpty(searchForm.getNoMarketDataFieldOnDateType())) {
				if ("Specified".equalsIgnoreCase(searchForm.getNoMarketDataFieldOnDateType())) {
					ValidationUtils.assertNotNull(searchForm.getNoMarketDataFieldOnDateId(), "Please specify the market data field to find missing data for");
					DetachedCriteria notExists = DetachedCriteria.forEntityName("com.clifton.marketdata.field.MarketDataValue", "mdv");
					notExists.setProjection(Projections.property("id"));
					notExists.add(Restrictions.eqProperty("investmentSecurity.id", criteria.getAlias() + ".id"));
					notExists.add(Restrictions.eq("measureDate", searchForm.getActiveOnDate()));
					notExists.add(Restrictions.eq("dataField.id", searchForm.getNoMarketDataFieldOnDateId()));
					criteria.add(Subqueries.notExists(notExists));
				}
				else if ("Latest".equalsIgnoreCase(searchForm.getNoMarketDataFieldOnDateType()) || "Closing".equalsIgnoreCase(searchForm.getNoMarketDataFieldOnDateType())) {
					criteria.add(Restrictions.sqlRestriction(" NOT EXISTS ( " + //
							" SELECT mdv.MarketDataValueID " + //
							" FROM MarketDataValue mdv " + //
							" INNER JOIN InvestmentSecurity s ON mdv.InvestmentSecurityID = s.InvestmentSecurityID " + //
							" INNER JOIN InvestmentInstrument inst ON s.InvestmentInstrumentID = inst.InvestmentInstrumentID " + //
							" INNER JOIN InvestmentInstrumentHierarchy instH ON inst.InvestmentInstrumentHierarchyID = instH.InvestmentInstrumentHierarchyID " + //
							// Instrument
							" LEFT JOIN MarketDataPriceFieldMapping im ON inst.InvestmentInstrumentID = im.InvestmentInstrumentID " + //
							// Hierarchy
							" LEFT JOIN MarketDataPriceFieldMapping hm ON inst.InvestmentInstrumentHierarchyID = hm.InvestmentInstrumentHierarchyID " + //
							// Sub Type 1 and Sub Type 2
							" LEFT JOIN MarketDataPriceFieldMapping st12m ON instH.InvestmentTypeSubTypeID = st12m.InvestmentTypeSubTypeID AND instH.InvestmentTypeSubType2ID = st12m.InvestmentTypeSubType2ID " + //
							// Sub Type 2
							" LEFT JOIN MarketDataPriceFieldMapping st2m ON instH.InvestmentTypeSubType2ID = st2m.InvestmentTypeSubType2ID AND st2m.InvestmentTypeSubTypeID IS NULL " + //
							// Sub Type 1
							" LEFT JOIN MarketDataPriceFieldMapping st1m ON instH.InvestmentTypeSubTypeID = st1m.InvestmentTypeSubTypeID AND st1m.InvestmentTypeSubType2ID IS NULL " + //
							// Type
							" LEFT JOIN MarketDataPriceFieldMapping tm ON instH.InvestmentTypeID = tm.InvestmentTypeID AND tm.InvestmentTypeSubTypeID IS NULL AND tm.InvestmentTypeSubType2ID IS NULL " + //
							" LEFT JOIN MarketDataPriceFieldMapping m ON m.MarketDataPriceFieldMappingID = COALESCE(im.MarketDataPriceFieldMappingID, hm.MarketDataPriceFieldMappingID, st12m.MarketDataPriceFieldMappingID, st2m.MarketDataPriceFieldMappingID, st1m.MarketDataPriceFieldMappingID, tm.MarketDataPriceFieldMappingID) " + //
							" INNER JOIN MarketDataField dp ON dp.DataFieldName = 'Last Trade Price' " + //
							" WHERE inst.InvestmentInstrumentID = {alias}.InvestmentInstrumentID  " + //
							" AND mdv.MarketDataFieldID = COALESCE(m." + searchForm.getNoMarketDataFieldOnDateType() + "PriceMarketDataFieldID, dp.MarketDataFieldID) " + //
							" AND (m." + searchForm.getNoMarketDataFieldOnDateType() + "PriceMarketDataSourceID IS NULL OR m." + searchForm.getNoMarketDataFieldOnDateType() + "PriceMarketDataSourceID = mdv.MarketDataSourceID) " + //
							" AND mdv.MeasureDate = ? )", searchForm.getActiveOnDate(), new DateType()));
					// If checking for prices - automatically exclude Currencies since prices don't apply to them
					criteria.add(Restrictions.eq("currency", false));
				}
				else {
					throw new ValidationException("Unmapped data field type to lookup [" + searchForm.getNoMarketDataFieldOnDateType());
				}
			}
		}
		else if (!StringUtils.isEmpty(searchForm.getNoMarketDataFieldOnDateType())) {
			throw new FieldValidationException("Filters 'noMarketDataFieldOnDateType' can only be used together with 'activeOnDate' filter.", "noMarketDataFieldOnDateType");
		}
		if (searchForm.getNotActiveOnDate() != null) {
			SimpleExpression startDateFilter = Restrictions.ge("startDate", searchForm.getNotActiveOnDate());
			SimpleExpression endDateFilter = Restrictions.le("endDate", searchForm.getNotActiveOnDate());
			criteria.add(Restrictions.or(startDateFilter, endDateFilter));
		}
		if (searchForm.getInvestmentSecurityGroupId() != null) {
			DetachedCriteria groupExists = DetachedCriteria.forClass(InvestmentSecurityGroupSecurity.class, "gs");
			groupExists.setProjection(Projections.property("id"));
			groupExists.add(Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".id"));
			groupExists.add(Restrictions.eq("referenceOne.id", searchForm.getInvestmentSecurityGroupId()));
			criteria.add(Subqueries.exists(groupExists));
		}
		if (searchForm.getExcludeInvestmentSecurityGroupId() != null) {
			addInvestmentSecurityGroupExclusion(criteria, searchForm.getExcludeInvestmentSecurityGroupId());
		}
		if (searchForm.getExcludeInvestmentSecurityGroupIds() != null) {
			for (Short excludeInvestmentSecurityId : searchForm.getExcludeInvestmentSecurityGroupIds()) {
				addInvestmentSecurityGroupExclusion(criteria, excludeInvestmentSecurityId);
			}
		}
		if (searchForm.getActivePositionsOnDate() != null) {
			DetachedCriteria exists = DetachedCriteria.forEntityName("com.clifton.accounting.gl.position.daily.AccountingPositionDaily", "apd");
			exists.setProjection(Projections.property("id"));
			exists.createAlias("accountingTransaction", "at");
			exists.add(Restrictions.eqProperty("at.investmentSecurity.id", criteria.getAlias() + ".id"));
			exists.add(Restrictions.eq("positionDate", searchForm.getActivePositionsOnDate()));
			criteria.add(Subqueries.exists(exists));
		}

		if (searchForm.getNoHolidayOnDate() != null) {
			// Use CalendarDayID for less joins
			int calendarDayId = DateUtils.getDaysSinceNaturalStart(searchForm.getNoHolidayOnDate());
			// Set it first so it does an inner join here, but we don't actually use it, because in order
			// for hibernate to generate sql correctly with alias, it needs actual paths, not alias + path
			getPathAlias("instrument", criteria);
			String ePath = getPathAlias("instrument.exchange", criteria, JoinType.LEFT_OUTER_JOIN);
			DetachedCriteria noHolidayExists = DetachedCriteria.forClass(CalendarHolidayDay.class, "chd");
			noHolidayExists.setProjection(Projections.property("id"));
			noHolidayExists.add(Restrictions.eqProperty("calendar.id", ePath + ".calendar.id"));
			noHolidayExists.add(Restrictions.eq("day.id", calendarDayId));
			criteria.add(Subqueries.notExists(noHolidayExists));
		}

		if (searchForm.getAfterFirstNoticeDateOrEndDate() != null) {
			LogicalExpression fndFilter = Restrictions.and(Restrictions.isNotNull("firstNoticeDate"), Restrictions.ge("firstNoticeDate", searchForm.getAfterFirstNoticeDateOrEndDate()));
			LogicalExpression edFilter = Restrictions.and(Restrictions.isNull("firstNoticeDate"), Restrictions.ge("endDate", searchForm.getAfterFirstNoticeDateOrEndDate()));
			criteria.add(Restrictions.or(fndFilter, edFilter));
		}

		// Already validated above that if one is set, then both are set
		if (!StringUtils.isEmpty(searchForm.getCustomColumnName())) {
			DetachedCriteria customValueExists = DetachedCriteria.forClass(SystemColumnValue.class, "cv");
			customValueExists.setProjection(Projections.property("id"));
			customValueExists.createAlias("column", "cc");
			customValueExists.createAlias("cc.columnGroup", "ccg");
			customValueExists.add(Restrictions.eq("cc.name", searchForm.getCustomColumnName()));
			customValueExists.add(Restrictions.eq("ccg.name", InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME));
			customValueExists.add(Restrictions.eq("value", searchForm.getCustomColumnValue()));
			customValueExists.add(Restrictions.eqProperty("fkFieldId", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.exists(customValueExists));
		}

		boolean filterInstrumentStructureTypes = (searchForm.getInstrumentStructureTypes() != null && searchForm.getInstrumentStructureTypes().length > 0);
		if (searchForm.getInstrumentStructureTypeExists() != null || filterInstrumentStructureTypes) {
			DetachedCriteria structuredDetached = DetachedCriteria.forClass(InvestmentSecurityStructureWeight.class, "sw");
			structuredDetached.setProjection(Projections.property("id"));
			structuredDetached.createAlias("instrumentWeight", "iw");
			structuredDetached.createAlias("iw.instrumentStructure", "st");
			structuredDetached.add(Restrictions.eqProperty("sw.security.id", criteria.getAlias() + ".id"));
			if (filterInstrumentStructureTypes) {
				if (searchForm.getInstrumentStructureTypes().length == 1) {
					structuredDetached.add(Restrictions.eq("st.instrumentStructureType", searchForm.getInstrumentStructureTypes()[0]));
				}
				else {
					structuredDetached.add(Restrictions.in("st.instrumentStructureType", (Object) searchForm.getInstrumentStructureTypes()));
				}
				criteria.add(Subqueries.exists(structuredDetached));
			}
			else {
				criteria.add(searchForm.getInstrumentStructureTypeExists() ? Subqueries.exists(structuredDetached) : Subqueries.notExists(structuredDetached));
			}
		}
		if (searchForm.getAccrualEventTypeIdOneAndTwo() != null && searchForm.getAccrualMethodOneAndTwo() != null) {

			String hierarchyAlias = getPathAlias("instrument.hierarchy", criteria);
			SimpleExpression hierarchyAccrualTypeOneCriterion = Restrictions.eq(hierarchyAlias + ".accrualSecurityEventType.id", searchForm.getAccrualEventTypeIdOneAndTwo());
			String instrumentAlias = getPathAlias("instrument", criteria);
			Disjunction firstAccrual = Restrictions.or(
					Restrictions.eq(criteria.getAlias() + ".accrualMethod", searchForm.getAccrualMethodOneAndTwo()),
					Restrictions.eq(instrumentAlias + ".accrualMethod", searchForm.getAccrualMethodOneAndTwo()),
					Restrictions.eq(hierarchyAlias + ".accrualMethod", searchForm.getAccrualMethodOneAndTwo())
			);
			LogicalExpression firstFilter = Restrictions.and(hierarchyAccrualTypeOneCriterion, firstAccrual);

			SimpleExpression hierarchyAccrualTypeTwoCriterion = Restrictions.eq(hierarchyAlias + ".accrualSecurityEventType2.id", searchForm.getAccrualEventTypeIdOneAndTwo());
			Disjunction secondAccrual = Restrictions.or(
					Restrictions.eq(criteria.getAlias() + ".accrualMethod2", searchForm.getAccrualMethodOneAndTwo()),
					Restrictions.eq(instrumentAlias + ".accrualMethod2", searchForm.getAccrualMethodOneAndTwo()),
					Restrictions.eq(hierarchyAlias + ".accrualMethod2", searchForm.getAccrualMethodOneAndTwo())
			);
			LogicalExpression secondFilter = Restrictions.and(hierarchyAccrualTypeTwoCriterion, secondAccrual);

			criteria.add(Restrictions.or(firstFilter, secondFilter));
		}
		else if (searchForm.getAccrualEventTypeIdOneAndTwo() != null || searchForm.getAccrualMethodOneAndTwo() != null) {
			throw new ValidationException("'AccrualEventTypeIdOneAndTwo' and 'AccrualMethodOneAndTwo' must be use together.");
		}
	}


	private void addInvestmentSecurityGroupExclusion(Criteria criteria, Short excludeInvestmentSecurityGroupId) {
		InvestmentSecurityGroup group = getInvestmentSecurityGroupService().getInvestmentSecurityGroup(excludeInvestmentSecurityGroupId);
		if (group != null) {
			DetachedCriteria ne = DetachedCriteria.forClass(InvestmentSecurityGroupSecurity.class, "gs");
			ne.setProjection(Projections.property("id"));

			if (group.isSecurityFromSameInstrumentAllowed()) {
				// Explicit tie from security to group is missing
				ne.add(Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".id"));
			}
			else {
				// Implicit tie from security's instrument to group is missing
				ne.createAlias("referenceTwo", "gss");
				ne.createAlias("gss.instrument", "gsi");
				ne.add(Restrictions.eqProperty("gsi.id", getPathAlias("instrument", criteria) + ".id"));
			}
			ne.add(Restrictions.eq("referenceOne.id", excludeInvestmentSecurityGroupId));
			criteria.add(Subqueries.notExists(ne));
		}
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}
}
