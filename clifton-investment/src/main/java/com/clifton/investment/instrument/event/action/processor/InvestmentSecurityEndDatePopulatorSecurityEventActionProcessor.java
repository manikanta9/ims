package com.clifton.investment.instrument.event.action.processor;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventAction;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionType;

import java.util.Date;


/**
 * <code>InvestmentSecurityEndDatePopulatorSecurityEventActionProcessor</code> is an action that populates the end date of the {@link InvestmentSecurity} of an {@link InvestmentSecurityEvent}.
 * An example where this is used in Mergers.
 *
 * @author nickk
 */
public class InvestmentSecurityEndDatePopulatorSecurityEventActionProcessor extends BaseInvestmentSecurityEventActionProcessor implements ValidationAware {

	private InvestmentInstrumentService investmentInstrumentService;

	/**
	 * Specifies whether Ex Date as opposed to Event Date should be used to lookup affected values
	 */
	private boolean exDateUsed;

	/**
	 * Bean property for one ore more end date properties of the security to populate from the event
	 */
	private String[] securityEndDatePropertyNames;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getSecurityEndDatePropertyNames(), "A property for the security end date is required");
		for (String endDateProperty : getSecurityEndDatePropertyNames()) {
			ValidationUtils.assertNotEmpty(endDateProperty, "A property for the security end date is cannot be of zero length");
			ValidationUtils.assertTrue(BeanUtils.isPropertyPresent(InvestmentSecurity.class, endDateProperty), "Property does not exist on security: " + endDateProperty);
			ValidationUtils.assertTrue(Date.class.isAssignableFrom(BeanUtils.getPropertyType(InvestmentSecurity.class, endDateProperty)), "Property of security is not a date: " + endDateProperty);
		}
	}


	@Override
	public boolean processActionImpl(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventActionType actionType) {
		updateEventSecurityEndDate(securityEvent, isExDateUsed() ? securityEvent.getExDate() : securityEvent.getEventDate());
		return true;
	}


	@Override
	public boolean rollbackActionImpl(InvestmentSecurityEventAction eventAction) {
		updateEventSecurityEndDate(eventAction.getSecurityEvent(), null);
		return true;
	}


	@Override
	public boolean isDataAvailableForProcessing(InvestmentSecurityEvent securityEvent) {
		boolean securityEndDateValueAfterEventDate = ArrayUtils.getStream(getSecurityEndDatePropertyNames())
				.map(endDateProperty -> (Date) BeanUtils.getPropertyValue(securityEvent.getSecurity(), endDateProperty))
				.anyMatch(endDateValue -> DateUtils.isDateAfter(endDateValue, isExDateUsed() ? securityEvent.getExDate() : securityEvent.getEventDate()));
		return securityEndDateValueAfterEventDate;
	}


	private InvestmentSecurity updateEventSecurityEndDate(InvestmentSecurityEvent securityEvent, Date endDate) {
		InvestmentSecurity securityToEnd = securityEvent.getSecurity();
		for (String endDateProperty : getSecurityEndDatePropertyNames()) {
			BeanUtils.setPropertyValue(securityToEnd, endDateProperty, endDate);
		}
		return getInvestmentInstrumentService().saveInvestmentSecurity(securityToEnd);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public boolean isExDateUsed() {
		return this.exDateUsed;
	}


	public void setExDateUsed(boolean exDateUsed) {
		this.exDateUsed = exDateUsed;
	}


	public String[] getSecurityEndDatePropertyNames() {
		return this.securityEndDatePropertyNames;
	}


	public void setSecurityEndDatePropertyNames(String[] securityEndDatePropertyNames) {
		this.securityEndDatePropertyNames = securityEndDatePropertyNames;
	}
}
