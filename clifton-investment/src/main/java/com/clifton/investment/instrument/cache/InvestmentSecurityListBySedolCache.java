package com.clifton.investment.instrument.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Component;


/**
 * The InvestmentSecurityListBySedolCache class caches the list of {@link InvestmentSecurity} objects by security SEDOL field.
 *
 * @author nickk
 */
@Component
public class InvestmentSecurityListBySedolCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentSecurity, String> {


	@Override
	protected String getBeanKeyProperty() {
		return "sedol";
	}


	@Override
	protected String getBeanKeyValue(InvestmentSecurity security) {
		return security.getSedol();
	}


	/**
	 * Returns the upper case string representation of the super implementation.
	 */
	@Override
	protected String getBeanKeySegmentForProperty(Object keyProperty) {
		return super.getBeanKeySegmentForProperty(keyProperty).toUpperCase();
	}
}
