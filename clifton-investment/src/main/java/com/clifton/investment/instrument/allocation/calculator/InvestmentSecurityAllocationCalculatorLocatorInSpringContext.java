package com.clifton.investment.instrument.allocation.calculator;


import com.clifton.core.util.AssertUtils;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class InvestmentSecurityAllocationCalculatorLocatorInSpringContext implements InvestmentSecurityAllocationCalculatorLocator, InitializingBean, ApplicationContextAware {

	private final Map<InvestmentSecurityAllocationTypes, InvestmentSecurityAllocationCalculator> calculatorMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		Map<String, InvestmentSecurityAllocationCalculator> beanMap = getApplicationContext().getBeansOfType(InvestmentSecurityAllocationCalculator.class);

		// need a map with InvestmentSecurityAllocationTypes as keys instead of bean names
		for (Map.Entry<String, InvestmentSecurityAllocationCalculator> stringInvestmentSecurityAllocationCalculatorEntry : beanMap.entrySet()) {
			InvestmentSecurityAllocationCalculator calculator = stringInvestmentSecurityAllocationCalculatorEntry.getValue();
			if (getCalculatorMap().containsKey(calculator.getInvestmentSecurityAllocationType())) {
				throw new RuntimeException("Cannot register '" + stringInvestmentSecurityAllocationCalculatorEntry.getKey() + "' as a calculator for investment security allocation type '" + calculator.getInvestmentSecurityAllocationType()
						+ "' because this allocation type already has a registered calculator.");
			}
			getCalculatorMap().put(calculator.getInvestmentSecurityAllocationType(), calculator);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityAllocationCalculator locate(InvestmentSecurityAllocationTypes allocationType) {
		AssertUtils.assertNotNull(allocationType, "Required allocation type cannot be null.");
		InvestmentSecurityAllocationCalculator result = getCalculatorMap().get(allocationType);
		AssertUtils.assertNotNull(result, "Cannot locate InvestmentSecurityAllocationCalculator for '%1s' allocation type.", allocationType);
		return result;
	}


	@Override
	public InvestmentSecurityAllocationRebalanceCalculator locateForRebalance(InvestmentSecurityAllocationTypes allocationType) {
		InvestmentSecurityAllocationCalculator calculator = locate(allocationType);
		AssertUtils.assertTrue(calculator instanceof InvestmentSecurityAllocationRebalanceCalculator, "InvestmentSecurityAllocationCalculator for '%1s' allocation type does not support rebalancing.", allocationType);
		return (InvestmentSecurityAllocationRebalanceCalculator) calculator;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<InvestmentSecurityAllocationTypes, InvestmentSecurityAllocationCalculator> getCalculatorMap() {
		return this.calculatorMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
