package com.clifton.investment.instrument.currency;


import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>InvestmentCurrencyService</code> interface defines currency specific methods.
 *
 * @author vgomelsky
 */
public interface InvestmentCurrencyService {

	/**
	 * Returns true if the convention is fromAmount * FX Rate = toAmount.
	 * Returns false if convention is fromAmount = toAmount / FX Rate.
	 */
	@RequestMapping("investmentCurrencyMultiplyConventionUsed")
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentCurrencyConvention.class)
	public boolean isInvestmentCurrencyMultiplyConvention(String fromSymbol, String toSymbol);


	/**
	 * Returns the number of business days that it takes to settle a trade for the specified currency pair.
	 * If null is returned, then use the default defined at instrument/hierarchy level.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentCurrencyConvention.class)
	public Integer getInvestmentCurrencyConventionDaysToSettle(String fromSymbol, String toSymbol);


	/**
	 * Returns the dominant security symbol of the two provided currency symbols.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentCurrencyConvention.class)
	public String getInvestmentCurrencyConventionDominantCurrency(String currencySymbolOne, String currencySymbolTwo);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCurrencyConvention getInvestmentCurrencyConvention(int id);


	public List<InvestmentCurrencyConvention> getInvestmentCurrencyConventionList(InvestmentCurrencyConventionSearchForm searchForm);


	public InvestmentCurrencyConvention saveInvestmentCurrencyConvention(InvestmentCurrencyConvention convention);


	public void deleteInvestmentCurrencyConvention(int id);
}
