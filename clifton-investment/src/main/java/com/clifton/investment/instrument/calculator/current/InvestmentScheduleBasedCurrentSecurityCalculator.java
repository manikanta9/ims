package com.clifton.investment.instrument.calculator.current;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;


/**
 * The <code>InvestmentScheduleBasedCurrentSecurityCalculator</code> is used to determine the current security
 * for an Instrument by calculating the symbol from the instrument + balance date + current security schedule + effective on schedule.
 *
 * @author michaelm
 */
public class InvestmentScheduleBasedCurrentSecurityCalculator extends BaseInvestmentCurrentSecurityCalculator {

	private static final int MIN_OCCURRENCE_OVERRIDE = 2;
	private static final int MAX_OCCURRENCE_OVERRIDE = 52;

	private CalendarBusinessDayService calendarBusinessDayService;
	private ScheduleApiService scheduleApiService;


	/**
	 * Schedule used to find specific security for instrument based on {@link InvestmentScheduleBasedCurrentSecurityCalculator#dateResultIsSecurityLastDeliveryDate}.
	 */
	private int currentSecurityScheduleId;

	/**
	 * Overrides which occurrence date from the Current Security Schedule is used to look up the current security. This does NOT affect the Effective On Schedule.
	 */
	private Integer occurrenceOverride;

	/**
	 * Date Result for the Current Security Schedule will be End Date unless this property is enabled.
	 */
	private boolean dateResultIsSecurityLastDeliveryDate;

	/**
	 * This property dictates how long the current security remains current. It defaults to Current Security Schedule and can be adjusted further by Effective on BusinessDay
	 * Adjustment, Business Day Calendar, and Use Calendar Days.
	 */
	private Integer effectiveOnScheduleId;

	/**
	 * Changes when the new current security becomes effective by adjusting business days following the Business Day Calendar.
	 */
	private Integer effectiveBusinessDayAdjustment;

	/**
	 * Holiday calendar to be used with Effective on Business Day Adjustment to support Trading adjusting backwards for country holidays when rolling forwards.
	 */
	private Short businessDayCalendarId;

	////////////////////////////////////////////////////////////////////////////
	////////////              Calculation Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void validateImpl(InvestmentInstrument instrument, InvestmentSecurityGroup securityGroup) {
		ValidationUtils.assertNotNull(instrument, "Instrument selection is required.");
		if (getOccurrenceOverride() != null) {
			ValidationUtils.assertTrue(getOccurrenceOverride() >= MIN_OCCURRENCE_OVERRIDE && getOccurrenceOverride() <= MAX_OCCURRENCE_OVERRIDE, String.format("Occurrence Override [%d] cannot be less than %d or greater than %d.", getOccurrenceOverride(), MIN_OCCURRENCE_OVERRIDE, MAX_OCCURRENCE_OVERRIDE));
		}
	}


	@Override
	public InvestmentSecurity calculateImpl(InvestmentInstrument instrument, @SuppressWarnings("unused") InvestmentSecurityGroup securityGroup, InvestmentSecurity underlyingSecurity, Date balanceDatePlusOneWeekday) {
		Date currentSecurityDateResult = calculateCurrentSecurityDateResult(balanceDatePlusOneWeekday);
		return findCurrentSecurity(instrument, securityGroup, underlyingSecurity, currentSecurityDateResult);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Date calculateCurrentSecurityDateResult(Date balanceDatePlusOneWeekday) {
		List<Date> futureOccurrenceDates = getFutureOccurrenceDates(balanceDatePlusOneWeekday);
		if (CollectionUtils.isEmpty(futureOccurrenceDates)) {
			throw new ValidationException(String.format("Expected at least one future occurrence using schedule [%s] on or after [%s] but no future occurrences were found.", getScheduleApiService().getSchedule(getCurrentSecurityScheduleId()).getName(), DateUtils.fromDate(balanceDatePlusOneWeekday)));
		}
		List<Date> lastTwoFutureOccurrenceDates = getLastTwoOccurrenceDates(futureOccurrenceDates);
		Date effectiveOnDate = DateUtils.clearTime(calculateEffectiveOnDate(CollectionUtils.getFirstElement(futureOccurrenceDates)));
		return determineEffectiveDateResult(balanceDatePlusOneWeekday, effectiveOnDate, lastTwoFutureOccurrenceDates.get(0), lastTwoFutureOccurrenceDates.get(1));
	}


	private List<Date> getFutureOccurrenceDates(Date balanceDatePlusOneWeekday) {
		int numOccurrences = ObjectUtils.coalesce(getOccurrenceOverride(), 1) + 1;
		return getScheduleApiService().getScheduleOccurrences(ScheduleOccurrenceCommand.forOccurrences(getCurrentSecurityScheduleId(), numOccurrences, balanceDatePlusOneWeekday));
	}


	private List<Date> getLastTwoOccurrenceDates(List<Date> futureOccurrenceDates) {
		ListIterator<Date> futureOccurrenceDatesListIterator = futureOccurrenceDates.listIterator(CollectionUtils.getSize(futureOccurrenceDates));
		Date lastOccurrenceDate = futureOccurrenceDatesListIterator.previous();
		Date penultimateOccurrenceDate = lastOccurrenceDate;
		if (futureOccurrenceDatesListIterator.hasPrevious()) {
			penultimateOccurrenceDate = futureOccurrenceDatesListIterator.previous();
		}
		return Arrays.asList(penultimateOccurrenceDate, lastOccurrenceDate);
	}


	private Date calculateEffectiveOnDate(Date firstFutureOccurrenceDate) {
		Date effectiveOnDate = firstFutureOccurrenceDate;
		if (getEffectiveOnScheduleId() != null) {
			Date calculatedEffectiveOnDate = getScheduleApiService().getScheduleOccurrence(ScheduleOccurrenceCommand.forOccurrences(getEffectiveOnScheduleId(), -1, firstFutureOccurrenceDate));
			effectiveOnDate = ObjectUtils.coalesce(calculatedEffectiveOnDate, effectiveOnDate);
		}
		return adjustEffectiveOnDate(effectiveOnDate);
	}


	private Date adjustEffectiveOnDate(Date effectiveOnDate) {
		if (getEffectiveBusinessDayAdjustment() != null) {
			if (getBusinessDayCalendarId() != null) {
				CalendarBusinessDayCommand businessDayCommand = CalendarBusinessDayCommand.forDate(effectiveOnDate, getBusinessDayCalendarId());
				effectiveOnDate = getCalendarBusinessDayService().getBusinessDayFrom(businessDayCommand, getEffectiveBusinessDayAdjustment());
			}
			else {
				effectiveOnDate = DateUtils.addWeekDays(effectiveOnDate, getEffectiveBusinessDayAdjustment());
			}
		}
		return effectiveOnDate;
	}


	private Date determineEffectiveDateResult(Date balanceDatePlusOneWeekday, Date effectiveOnDate, Date penultimateOccurrenceDate, Date lastOccurrenceDate) {
		if (DateUtils.isDateBefore(effectiveOnDate, balanceDatePlusOneWeekday, false)) {
			return lastOccurrenceDate;
		}
		return penultimateOccurrenceDate;
	}


	private InvestmentSecurity findCurrentSecurity(InvestmentInstrument instrument, InvestmentSecurityGroup securityGroup, InvestmentSecurity underlyingSecurity, Date currentSecurityDateResult) {
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		if (isDateResultIsSecurityLastDeliveryDate()) {
			securitySearchForm.setLastDeliveryDate(currentSecurityDateResult);
		}
		else {
			securitySearchForm.setEndDate(currentSecurityDateResult);
		}
		securitySearchForm.setInstrumentId(instrument.getId());
		ObjectUtils.doIfPresent(underlyingSecurity, s -> securitySearchForm.setUnderlyingSecurityId(s.getId()));
		ObjectUtils.doIfPresent(securityGroup, sg -> securitySearchForm.setSecurityGroupId(sg.getId()));
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);
		ValidationUtils.assertEquals(CollectionUtils.getSize(securityList), 1,
				String.format("Expected exactly 1 current security using Schedule Based Current Security Calculator but found %d for %s.",
						CollectionUtils.getSize(securityList), getUserFriendlySearchCriteria(instrument, securitySearchForm)));
		return CollectionUtils.getFirstElementStrict(securityList);
	}


	private String getUserFriendlySearchCriteria(InvestmentInstrument instrument, SecuritySearchForm securitySearchForm) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(instrument.getName());
		if (securitySearchForm.getEndDate() != null) {
			stringBuilder.append(" with End Date ").append(DateUtils.fromDateShort(securitySearchForm.getEndDate()));
		}
		if (securitySearchForm.getLastDeliveryDate() != null) {
			stringBuilder.append(" with Last Delivery Date ").append(DateUtils.fromDateShort(securitySearchForm.getLastDeliveryDate()));
		}
		return stringBuilder.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}


	public int getCurrentSecurityScheduleId() {
		return this.currentSecurityScheduleId;
	}


	public void setCurrentSecurityScheduleId(int currentSecurityScheduleId) {
		this.currentSecurityScheduleId = currentSecurityScheduleId;
	}


	public Integer getOccurrenceOverride() {
		return this.occurrenceOverride;
	}


	public void setOccurrenceOverride(Integer occurrenceOverride) {
		this.occurrenceOverride = occurrenceOverride;
	}


	public boolean isDateResultIsSecurityLastDeliveryDate() {
		return this.dateResultIsSecurityLastDeliveryDate;
	}


	public void setDateResultIsSecurityLastDeliveryDate(boolean dateResultIsSecurityLastDeliveryDate) {
		this.dateResultIsSecurityLastDeliveryDate = dateResultIsSecurityLastDeliveryDate;
	}


	public Integer getEffectiveOnScheduleId() {
		return this.effectiveOnScheduleId;
	}


	public void setEffectiveOnScheduleId(Integer effectiveOnScheduleId) {
		this.effectiveOnScheduleId = effectiveOnScheduleId;
	}


	public Integer getEffectiveBusinessDayAdjustment() {
		return this.effectiveBusinessDayAdjustment;
	}


	public void setEffectiveBusinessDayAdjustment(Integer effectiveBusinessDayAdjustment) {
		this.effectiveBusinessDayAdjustment = effectiveBusinessDayAdjustment;
	}


	public Short getBusinessDayCalendarId() {
		return this.businessDayCalendarId;
	}


	public void setBusinessDayCalendarId(Short businessDayCalendarId) {
		this.businessDayCalendarId = businessDayCalendarId;
	}
}
