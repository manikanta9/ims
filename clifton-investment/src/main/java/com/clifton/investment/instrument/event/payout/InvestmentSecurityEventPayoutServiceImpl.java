package com.clifton.investment.instrument.event.payout;

import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventClientElectionSearchForm;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutTypeAssignmentSearchForm;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutTypeSearchForm;
import com.clifton.investment.instrument.event.payout.validation.InvestmentSecurityEventPayoutValidator;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class InvestmentSecurityEventPayoutServiceImpl implements InvestmentSecurityEventPayoutService {

	private AdvancedUpdatableDAO<InvestmentSecurityEventPayout, Criteria> investmentSecurityEventPayoutDAO;
	private AdvancedReadOnlyDAO<InvestmentSecurityEventPayoutType, Criteria> investmentSecurityEventPayoutTypeDAO;
	private AdvancedUpdatableDAO<InvestmentSecurityEventPayoutTypeAssignment, Criteria> investmentSecurityEventPayoutTypeAssignmentDAO;
	private AdvancedUpdatableDAO<InvestmentSecurityEventClientElection, Criteria> investmentSecurityEventClientElectionDAO;

	private DaoNamedEntityCache<InvestmentSecurityEventPayoutType> investmentSecurityEventPayoutTypeCache;

	private InvestmentSecurityEventService investmentSecurityEventService;


	////////////////////////////////////////////////////////////////////////////
	//////////    Investment Security Event Payout Methods          ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventPayout getInvestmentSecurityEventPayout(int id) {
		return getInvestmentSecurityEventPayoutDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentSecurityEventPayout> getInvestmentSecurityEventPayoutList(InvestmentSecurityEventPayoutSearchForm searchForm) {
		return getInvestmentSecurityEventPayoutDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public InvestmentSecurityEventPayout saveInvestmentSecurityEventPayout(InvestmentSecurityEventPayout payout) {
		// validate based on meta-data first
		InvestmentSecurityEventPayoutType payoutType = getInvestmentSecurityEventPayoutType(payout.getPayoutType().getId());
		ValidationUtils.assertNotNull(payoutType, "Payout Type is required for Investment Security Event Payout.");
		if (payoutType.isBeforeSameAsAfter()) {
			ValidationUtils.assertTrue(MathUtils.isEqual(payout.getBeforeEventValue(), payout.getAfterEventValue()), "Before and After event values must be the same for payouts of type: " + payoutType.getLabel());
		}
		if (payoutType.isAdditionalSecurityCurrency() && payout.getPayoutSecurity() != null) {
			ValidationUtils.assertTrue(payout.getPayoutSecurity().isCurrency(), "Payout Security must be a Currency for payouts of type: " + payoutType.getLabel());
		}

		// make sure the payout type is allowed for the event type
		InvestmentSecurityEventPayoutTypeAssignmentSearchForm searchForm = new InvestmentSecurityEventPayoutTypeAssignmentSearchForm();
		searchForm.setEventTypeId(payout.getSecurityEvent().getType().getId());
		searchForm.setPayoutTypeId(payoutType.getId());
		if (CollectionUtils.isEmpty(getInvestmentSecurityEventPayoutTypeAssignmentList(searchForm))) {
			throw new ValidationException(String.format("Cannot create Security Event Payout because the specified Payout Type [%s] is not allowed for the specified Event Type [%s]",
					payoutType.getLabel(), payout.getSecurityEvent().getType().getLabel()));
		}

		return getInvestmentSecurityEventPayoutDAO().save(payout);
	}


	@Override
	public void deleteInvestmentSecurityEventPayout(int id, boolean ignoreValidation) {
		if (ignoreValidation) {
			// Ignore the deletion validation for being last payout for default election or client election. Payout use by journal details is still active.
			DaoUtils.executeWithSpecificObserversDisabled(() -> getInvestmentSecurityEventPayoutDAO().delete(id), InvestmentSecurityEventPayoutValidator.class);
		}
		else {
			getInvestmentSecurityEventPayoutDAO().delete(id);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////    Investment Security Event Payout Type Methods     ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventPayoutType getInvestmentSecurityEventPayoutType(short id) {
		return getInvestmentSecurityEventPayoutTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentSecurityEventPayoutType getInvestmentSecurityEventPayoutTypeByName(String name) {
		return getInvestmentSecurityEventPayoutTypeCache().getBeanForKeyValueStrict(getInvestmentSecurityEventPayoutTypeDAO(), name);
	}


	@Override
	public List<InvestmentSecurityEventPayoutType> getInvestmentSecurityEventPayoutTypeList(InvestmentSecurityEventPayoutTypeSearchForm searchForm) {
		return getInvestmentSecurityEventPayoutTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////   Investment Security Event Payout Type Assignment Methods  ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventPayoutTypeAssignment getInvestmentSecurityEventPayoutTypeAssignment(short id) {
		return getInvestmentSecurityEventPayoutTypeAssignmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentSecurityEventPayoutTypeAssignment> getInvestmentSecurityEventPayoutTypeAssignmentList(InvestmentSecurityEventPayoutTypeAssignmentSearchForm searchForm) {
		return getInvestmentSecurityEventPayoutTypeAssignmentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentSecurityEventPayoutTypeAssignment linkInvestmentSecurityEventPayoutTypeAssignment(short payoutTypeId, short eventTypeId) {
		InvestmentSecurityEventPayoutType payoutType = getInvestmentSecurityEventPayoutType(payoutTypeId);
		ValidationUtils.assertNotNull(payoutType, "Cannot find Investment Security Event Payout Type with id = " + payoutTypeId);
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventType(eventTypeId);
		ValidationUtils.assertNotNull(eventType, "Cannot find Investment Security Event Type with id = " + eventTypeId);

		InvestmentSecurityEventPayoutTypeAssignment link = new InvestmentSecurityEventPayoutTypeAssignment();
		link.setReferenceOne(payoutType);
		link.setReferenceTwo(eventType);
		return getInvestmentSecurityEventPayoutTypeAssignmentDAO().save(link);
	}


	@Override
	public void deleteInvestmentSecurityEventPayoutTypeAssignment(short id) {
		getInvestmentSecurityEventPayoutTypeAssignmentDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////   Investment Security Event Client Election Methods    ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventClientElection getInvestmentSecurityEventClientElection(int id) {
		return getInvestmentSecurityEventClientElectionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentSecurityEventClientElection> getInvestmentSecurityEventClientElectionList(InvestmentSecurityEventClientElectionSearchForm searchForm) {
		return getInvestmentSecurityEventClientElectionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentSecurityEventClientElection saveInvestmentSecurityEventClientElection(InvestmentSecurityEventClientElection clientElection) {
		return getInvestmentSecurityEventClientElectionDAO().save(clientElection);
	}


	@Override
	public void deleteInvestmentSecurityEventClientElection(int id) {
		getInvestmentSecurityEventClientElectionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentSecurityEventPayout, Criteria> getInvestmentSecurityEventPayoutDAO() {
		return this.investmentSecurityEventPayoutDAO;
	}


	public void setInvestmentSecurityEventPayoutDAO(AdvancedUpdatableDAO<InvestmentSecurityEventPayout, Criteria> investmentSecurityEventPayoutDAO) {
		this.investmentSecurityEventPayoutDAO = investmentSecurityEventPayoutDAO;
	}


	public AdvancedReadOnlyDAO<InvestmentSecurityEventPayoutType, Criteria> getInvestmentSecurityEventPayoutTypeDAO() {
		return this.investmentSecurityEventPayoutTypeDAO;
	}


	public void setInvestmentSecurityEventPayoutTypeDAO(AdvancedReadOnlyDAO<InvestmentSecurityEventPayoutType, Criteria> investmentSecurityEventPayoutTypeDAO) {
		this.investmentSecurityEventPayoutTypeDAO = investmentSecurityEventPayoutTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSecurityEventPayoutTypeAssignment, Criteria> getInvestmentSecurityEventPayoutTypeAssignmentDAO() {
		return this.investmentSecurityEventPayoutTypeAssignmentDAO;
	}


	public void setInvestmentSecurityEventPayoutTypeAssignmentDAO(AdvancedUpdatableDAO<InvestmentSecurityEventPayoutTypeAssignment, Criteria> investmentSecurityEventPayoutTypeAssignmentDAO) {
		this.investmentSecurityEventPayoutTypeAssignmentDAO = investmentSecurityEventPayoutTypeAssignmentDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSecurityEventClientElection, Criteria> getInvestmentSecurityEventClientElectionDAO() {
		return this.investmentSecurityEventClientElectionDAO;
	}


	public void setInvestmentSecurityEventClientElectionDAO(AdvancedUpdatableDAO<InvestmentSecurityEventClientElection, Criteria> investmentSecurityEventClientElectionDAO) {
		this.investmentSecurityEventClientElectionDAO = investmentSecurityEventClientElectionDAO;
	}


	public DaoNamedEntityCache<InvestmentSecurityEventPayoutType> getInvestmentSecurityEventPayoutTypeCache() {
		return this.investmentSecurityEventPayoutTypeCache;
	}


	public void setInvestmentSecurityEventPayoutTypeCache(DaoNamedEntityCache<InvestmentSecurityEventPayoutType> investmentSecurityEventPayoutTypeCache) {
		this.investmentSecurityEventPayoutTypeCache = investmentSecurityEventPayoutTypeCache;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}
}
