package com.clifton.investment.instrument.event.sequence.date;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.sequence.EventSequenceDefinition;

import java.util.Date;


public class EquityLegPaymentDateCalculator extends AbstractDateCalculator {

	@Override
	public InvestmentSecurityEvent calculateDates(Date declareDate, @SuppressWarnings("unused") Date nextDeclareDate, InvestmentSecurityEvent event, EventSequenceDefinition sequenceDefinition,
	                                              Schedule schedule) {
		if (ScheduleFrequencies.ONCE.name().equals(schedule.getFrequency())) {
			event.setDeclareDate(sequenceDefinition.getSecurity().getStartDate());
			event.setRecordDate(sequenceDefinition.getSecurity().getEndDate());
			event.setExDate(DateUtils.addDays(sequenceDefinition.getSecurity().getEndDate(), 1));
			event.setEventDate(sequenceDefinition.getSecurity().getLastDeliveryDate());
			return event;
		}

		Date recordDate = getScheduleApiService().getScheduleOccurrence(ScheduleOccurrenceCommand.forOccurrences(schedule, 1, DateUtils.addMinutes(declareDate, 1)));

		// check if there is a partial period
		if ((recordDate == null) && (DateUtils.compare(declareDate, sequenceDefinition.getSecurity().getEndDate(), false) < 0)) {
			// check if the using the security end date will put the event data past the final payment date
			Date evtDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.addDays(declareDate, 1), sequenceDefinition.getSettlementCalendar().getId()), sequenceDefinition.getSettlementCycle());
			if (DateUtils.compare(evtDate, sequenceDefinition.getSecurity().getLastDeliveryDate(), false) >= 0) {
				return null;
			}
			recordDate = sequenceDefinition.getSecurity().getEndDate();
		}
		else if (recordDate == null) {
			return null;
		}
		Date exDate = DateUtils.addDays(recordDate, 1);
		// used combined
		Date eventDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(recordDate, sequenceDefinition.getSettlementCalendar().getId()), sequenceDefinition.getSettlementCycle());

		event.setDeclareDate(DateUtils.compare(declareDate, sequenceDefinition.getSecurity().getStartDate(), false) < 0 ? sequenceDefinition.getSecurity().getStartDate() : declareDate);
		event.setRecordDate(recordDate);
		event.setExDate(exDate);
		event.setEventDate(eventDate);

		return event;
	}
}
