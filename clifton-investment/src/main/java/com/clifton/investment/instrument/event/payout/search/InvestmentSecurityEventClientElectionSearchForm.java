package com.clifton.investment.instrument.event.payout.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author vgomelsky
 */
public class InvestmentSecurityEventClientElectionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;


	@SearchField(searchField = "securityEvent.id")
	private Integer securityEventId;

	@SearchField(searchField = "securityEvent.type.id", sortField = "securityEvent.type.eventOrder")
	private Short typeId;

	@SearchField(searchField = "name", searchFieldPath = "securityEvent.type", comparisonConditions = ComparisonConditions.EQUALS)
	private String typeName;

	@SearchField(searchField = "status.id", searchFieldPath = "securityEvent")
	private Short statusId;

	@SearchField(searchField = "name", searchFieldPath = "securityEvent.status", comparisonConditions = ComparisonConditions.EQUALS)
	private String statusName;

	@SearchField(searchFieldPath = "securityEvent")
	private Long corporateActionIdentifier;

	@SearchField(searchField = "security.id", searchFieldPath = "securityEvent")
	private Integer securityId;

	@SearchField(searchFieldPath = "securityEvent")
	private Date declareDate;

	@SearchField(searchFieldPath = "securityEvent")
	private Date exDate;

	@SearchField(searchFieldPath = "securityEvent")
	private Date recordDate;

	@SearchField(searchFieldPath = "securityEvent")
	private Date eventDate;

	@SearchField(searchFieldPath = "securityEvent")
	private String eventDescription;


	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField
	private Short electionNumber;

	@SearchField
	private BigDecimal electionQuantity;

	@SearchField
	private BigDecimal electionValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getSecurityEventId() {
		return this.securityEventId;
	}


	public void setSecurityEventId(Integer securityEventId) {
		this.securityEventId = securityEventId;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Short getStatusId() {
		return this.statusId;
	}


	public void setStatusId(Short statusId) {
		this.statusId = statusId;
	}


	public String getStatusName() {
		return this.statusName;
	}


	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}


	public Long getCorporateActionIdentifier() {
		return this.corporateActionIdentifier;
	}


	public void setCorporateActionIdentifier(Long corporateActionIdentifier) {
		this.corporateActionIdentifier = corporateActionIdentifier;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Date getDeclareDate() {
		return this.declareDate;
	}


	public void setDeclareDate(Date declareDate) {
		this.declareDate = declareDate;
	}


	public Date getExDate() {
		return this.exDate;
	}


	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}


	public Date getRecordDate() {
		return this.recordDate;
	}


	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public String getEventDescription() {
		return this.eventDescription;
	}


	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Short getElectionNumber() {
		return this.electionNumber;
	}


	public void setElectionNumber(Short electionNumber) {
		this.electionNumber = electionNumber;
	}


	public BigDecimal getElectionQuantity() {
		return this.electionQuantity;
	}


	public void setElectionQuantity(BigDecimal electionQuantity) {
		this.electionQuantity = electionQuantity;
	}


	public BigDecimal getElectionValue() {
		return this.electionValue;
	}


	public void setElectionValue(BigDecimal electionValue) {
		this.electionValue = electionValue;
	}
}
