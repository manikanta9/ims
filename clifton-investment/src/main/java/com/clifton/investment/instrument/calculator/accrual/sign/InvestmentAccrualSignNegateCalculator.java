package com.clifton.investment.instrument.calculator.accrual.sign;


import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>InvestmentAccrualSignNegateCalculator</code> class is a InvestmentAccrualSignCalculator that
 * can either always NEGATE (Total Return Swaps always have negative accrual) or conditionally NEGATE
 * the sign of accrued interest (only negate for certain day count conventions.
 *
 * @author vgomelsky
 */
public class InvestmentAccrualSignNegateCalculator implements InvestmentAccrualSignCalculator {

	/**
	 * If this field is populated, negate the sign when couponFrequencyFieldName matches this field
	 */
	private String fieldNameToNegate;
	private boolean negateIfFieldMatches;
	private boolean negateIfFieldDoesNotMatch;


	@Override
	public boolean isAccrualAmountNegated(@SuppressWarnings("unused") InvestmentSecurity security, String couponFrequencyFieldName) {
		if (StringUtils.isEmpty(getFieldNameToNegate())) {
			return true;
		}
		if (getFieldNameToNegate().equals(couponFrequencyFieldName)) {
			return isNegateIfFieldMatches();
		}
		return isNegateIfFieldDoesNotMatch();
	}


	public String getFieldNameToNegate() {
		return this.fieldNameToNegate;
	}


	public void setFieldNameToNegate(String fieldNameToNegate) {
		this.fieldNameToNegate = fieldNameToNegate;
	}


	public boolean isNegateIfFieldMatches() {
		return this.negateIfFieldMatches;
	}


	public void setNegateIfFieldMatches(boolean negateIfFieldMatches) {
		this.negateIfFieldMatches = negateIfFieldMatches;
	}


	public boolean isNegateIfFieldDoesNotMatch() {
		return this.negateIfFieldDoesNotMatch;
	}


	public void setNegateIfFieldDoesNotMatch(boolean negateIfFieldDoesNotMatch) {
		this.negateIfFieldDoesNotMatch = negateIfFieldDoesNotMatch;
	}
}
