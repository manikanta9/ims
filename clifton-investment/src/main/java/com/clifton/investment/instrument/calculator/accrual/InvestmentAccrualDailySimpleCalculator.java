package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;


/**
 * The InvestmentAccrualDailySimpleCalculator class calculates the reference amount using the sum of daily interest on the initial notional and
 * Supports interest rate changes during a single payment period.  The calculation does not use compounding of interest.
 * <p>
 * Accrual Amount = Sum (Notional * AdjustedDailyRate) where AdjustedDailyRate = Ri / days_in_year).  Days in year is configured at in the AccrualConfig instance.
 * <p>
 * NOTE: used in conjunction with investments that use SOFR Average daily rates.
 *
 * @author davidi
 */
public class InvestmentAccrualDailySimpleCalculator extends InvestmentAccrualDailyRateCalculator {

	@Override
	protected BigDecimal calculateReferenceRateAmount(@SuppressWarnings("unused") InvestmentSecurityEvent paymentEvent, BigDecimal notional, AccrualDailyRate[] dailyRates, AccrualConfig accrualConfig) {
		BigDecimal accrual = BigDecimal.ZERO;
		int daysInYear = accrualConfig.getDayCountConvention().getDaysInYear();
		for (AccrualDailyRate accrualDailyRate : dailyRates) {
			BigDecimal dailyRate = accrualDailyRate.getRate();
			// normalizeDailyRate
			dailyRate = dailyRate.movePointLeft(2);
			BigDecimal adjustedDailyRate = MathUtils.divide(dailyRate, daysInYear);
			accrual = MathUtils.add(accrual, MathUtils.multiply(notional, adjustedDailyRate));
		}
		return accrual;
	}
}
