package com.clifton.investment.instrument.structure.upload;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructure;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author manderson
 */
public interface InvestmentInstrumentStructureUploadService {


	@SecureMethod(dtoClass = InvestmentInstrumentStructure.class)
	public DataTable downloadInvestmentInstrumentStructureWeightFile(Integer instrumentStructureId);


	/**
	 * Calls save of the instrument structure, but populates the weights from the given file instead of from the screen
	 */
	@RequestMapping("investmentInstrumentStructureFileUpload")
	@SecureMethod(dtoClass = InvestmentInstrumentStructure.class)
	public void uploadInvestmentInstrumentStructureFile(InvestmentInstrumentStructure bean, MultipartFile file);
}
