package com.clifton.investment.instrument.structure;

/**
 * Defines how the structure type is calculated.
 * NOTE: THE LABEL FIELDS ARE NOT ACTUALLY USED - WITH ONLY ONE USE CASE THE LABELS ARE DIRECTLY ENTERED IN investment-shared.js: Clifton.investment.instrument.structure.InvestmentSecurityStructuredIndexModelPortfolioWeightsGrid
 */
public enum InvestmentInstrumentStructureTypes {

	STRUCTURED_INDEX(false),
	MODEL_PORTFOLIO(true, "Unrealized Gain/Loss", false, "Realized Gain/Loss", false);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Determines whether or not values are saved.  Currently we only save for Model Portfolio because the calculation depends on previous days' calculations
	 */
	private final boolean saveDailyAllocationValues;

	/**
	 * The additional amount 1 label
	 */
	private final String additionalAmount1Label;

	/**
	 * Whether or not additional amount 1 is in Local or Base CCY
	 */
	private final boolean additionalAmount1Local;

	/**
	 * The additional amount 2 label
	 */
	private final String additionalAmount2Label;

	/**
	 * Whether or not the additional amount 2 is in Local or Base CCY
	 */
	private final boolean additionalAmount2Local;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	InvestmentInstrumentStructureTypes(boolean saveDailyAllocationValues) {
		this(saveDailyAllocationValues, null, false, null, false);
	}


	InvestmentInstrumentStructureTypes(boolean saveDailyAllocationValues, String additionalAmount1Label, boolean additionalAmount1Local, String additionalAmount2Label, boolean additionalAmount2Local) {
		this.saveDailyAllocationValues = saveDailyAllocationValues;
		this.additionalAmount1Label = additionalAmount1Label;
		this.additionalAmount1Local = additionalAmount1Local;
		this.additionalAmount2Label = additionalAmount2Label;
		this.additionalAmount2Local = additionalAmount2Local;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSaveDailyAllocationValues() {
		return this.saveDailyAllocationValues;
	}


	public String getAdditionalAmount1Label() {
		return this.additionalAmount1Label;
	}


	public boolean isAdditionalAmount1Local() {
		return this.additionalAmount1Local;
	}


	public String getAdditionalAmount2Label() {
		return this.additionalAmount2Label;
	}


	public boolean isAdditionalAmount2Local() {
		return this.additionalAmount2Local;
	}
}
