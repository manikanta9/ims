package com.clifton.investment.instrument.upload;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.exchange.InvestmentExchangeService;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationHandler;
import com.clifton.investment.instrument.event.retriever.InvestmentSecurityEventRetrieverService;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.investment.instrument.upload.allocation.InvestmentSecurityAllocationDataPopulator;
import com.clifton.investment.instrument.upload.allocation.InvestmentSecurityAllocationUploadCommand;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListService;
import com.clifton.system.upload.SystemUploadHandler;
import com.clifton.system.upload.SystemUploadService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>InvestmentInstrumentUploadServiceImpl</code> ...
 * The <code>InvestmentInstrumentUploadServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class InvestmentInstrumentUploadServiceImpl implements InvestmentInstrumentUploadService {

	private InvestmentExchangeService investmentExchangeService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler;
	private InvestmentSecurityEventRetrieverService investmentSecurityEventRetrieverService;

	private SystemListService<SystemList> systemListService;
	private SystemUploadHandler systemUploadHandler;
	private SystemUploadService systemUploadService;

	private InvestmentSecurityAllocationDataPopulator investmentSecurityAllocationDataPopulator;

	////////////////////////////////////////////////////////////////
	////////   Investment Security Allocation Uploads     //////////
	////////////////////////////////////////////////////////////////


	@Override
	public DataTable downloadInvestmentSecurityUploadFileSample(InvestmentSecurityUploadCommand uploadCommand, boolean allRows) {
		validateInvestmentSecurityUpload(uploadCommand);

		Map<String, Object> additionalFilterMap = new HashMap<>();
		if (uploadCommand.getParentHierarchy() != null) {
			additionalFilterMap.put("instrument.hierarchy.parent.id", uploadCommand.getParentHierarchy().getId());
			additionalFilterMap.put("instrument.hierarchy.oneSecurityPerInstrument", uploadCommand.isOneSecurityPerInstrument());
			if (uploadCommand.getTradingCurrency() != null) {
				additionalFilterMap.put("instrument.tradingCurrency.id", uploadCommand.getTradingCurrency().getId());
			}
		}
		// Pull Sample Data
		DataTable dataTable = getSystemUploadService().downloadSystemUploadFileSampleWithAdditionalFilters(uploadCommand, allRows, additionalFilterMap);

		// Customize the sample file:
		// Remove all Instrument Hierarchy Columns - Not needed because selected on screen - except last one if using parent hierarchy
		DataColumn[] columns = dataTable.getColumnList();
		if (columns != null && columns.length > 0) {
			for (DataColumn column : columns) {
				// Single Hierarchy: Skip Hierarchy in Sample - Included On Screen
				if (uploadCommand.getHierarchy() != null && column.getColumnName() != null && column.getColumnName().startsWith("Instrument-Hierarchy")) {
					column.setHidden(true);
				}
				// Parent Hierarchy: Skip Hierarchy Parents in Sample - Included On Screen
				if (uploadCommand.getParentHierarchy() != null && column.getColumnName() != null && column.getColumnName().startsWith("Instrument-Hierarchy-Parent")) {
					column.setHidden(true);
				}

				// One - to - One Securities
				if (uploadCommand.isOneSecurityPerInstrument()) {
					// Not necessary to include the Instrument Identifier Prefix
					if (StringUtils.isEqual(column.getColumnName(), "Instrument-IdentifierPrefix")) {
						column.setHidden(true);
					}
					// For Simplicity - Only need "Instrument-TradingCurrency-Symbol" column - Uploads would include all natural key fields - including identifier prefix & hierarchy
					if (column.getColumnName() != null && column.getColumnName().startsWith("Instrument-TradingCurrency") && !StringUtils.isEqual(column.getColumnName(), "Instrument-TradingCurrency-Symbol")) {
						column.setHidden(true);
					}
				}
				// Skip Currency (true/false) Column - Denoted by the Hierarchy.IsCurrency = true/false
				if (StringUtils.isEqual(column.getColumnName(), "Currency")) {
					column.setHidden(true);
				}
			}
		}
		return dataTable;
	}


	@Transactional
	@Override
	public void uploadInvestmentSecurityUploadFile(InvestmentSecurityUploadCommand uploadCommand) {
		validateInvestmentSecurityUpload(uploadCommand);

		// As securities are created - create a map of hierarchy id to the list of security ids
		// Will be used for auto generating events by hierarchy
		Map<InvestmentInstrumentHierarchy, List<Integer>> hierarchySecurityIdMap = new HashMap<>();

		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, true);

		for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
			InvestmentSecurity security = (InvestmentSecurity) obj;
			// If One-to-One - Set Hierarchy Now before saving - one to many were already set for instrument look ups
			if (uploadCommand.isOneSecurityPerInstrument()) {
				if (security.getInstrument().getTradingCurrency() != null && !StringUtils.isEmpty(security.getInstrument().getTradingCurrency().getSymbol())) {
					security.getInstrument().setTradingCurrency(getInvestmentInstrumentService().getInvestmentSecurityBySymbol(security.getInstrument().getTradingCurrency().getSymbol(), true));
				}
				if (security.getInstrument().getTradingCurrency() == null || security.getInstrument().getTradingCurrency().getId() == null) {
					ValidationUtils.assertNotNull(uploadCommand.getTradingCurrency(), "Trading Currency selection is required on screen because there are securities in the file that do not have Instrument-TradingCurrency-Symbol column populated.");
					security.getInstrument().setTradingCurrency(uploadCommand.getTradingCurrency());
				}
				if (security.getInstrument().getExchange() != null && security.getInstrument().getExchange().getId() == null && !StringUtils.isEmpty(security.getInstrument().getExchange().getName())) {
					security.getInstrument().setExchange(getInvestmentExchangeService().getInvestmentExchangeByName(security.getInstrument().getExchange().getName()));
				}
				if (security.getInstrument().getCompositeExchange() != null && security.getInstrument().getCompositeExchange().getId() == null && !StringUtils.isEmpty(security.getInstrument().getCompositeExchange().getName())) {
					security.getInstrument().setCompositeExchange(getInvestmentExchangeService().getInvestmentExchangeByName(security.getInstrument().getCompositeExchange().getName()));
				}
				if (security.getInstrument().getCountryOfRisk() != null && security.getInstrument().getCountryOfRisk().getId() == null && !StringUtils.isEmpty(security.getInstrument().getCountryOfRisk().getValue())) {
					security.getInstrument().setCountryOfRisk(getSystemListService().getSystemListItemByListAndValue(InvestmentInstrument.INSTRUMENT_COUNTRIES_SYSTEM_LIST_NAME, security.getInstrument().getCountryOfRisk().getValue()));
				}
				if (security.getInstrument().getCountryOfIncorporation() != null && security.getInstrument().getCountryOfIncorporation().getId() == null && !StringUtils.isEmpty(security.getInstrument().getCountryOfIncorporation().getValue())) {
					security.getInstrument().setCountryOfIncorporation(getSystemListService().getSystemListItemByListAndValue(InvestmentInstrument.INSTRUMENT_COUNTRIES_SYSTEM_LIST_NAME, security.getInstrument().getCountryOfIncorporation().getValue()));
				}
			}
			// Set currency same as hierarchy
			security.setCurrency(security.getInstrument().getHierarchy().isCurrency());
			getInvestmentInstrumentService().saveInvestmentSecurity(security);

			if (hierarchySecurityIdMap.containsKey(security.getInstrument().getHierarchy())) {
				hierarchySecurityIdMap.get(security.getInstrument().getHierarchy()).add(security.getId());
			}
			else {
				hierarchySecurityIdMap.put(security.getInstrument().getHierarchy(), CollectionUtils.createList(security.getId()));
			}
		}

		uploadCommand.getUploadResult().addUploadResults("InvestmentSecurity", CollectionUtils.getSize(beanList), false);

		if (uploadCommand.isAutoGenerateEvents()) {
			for (Map.Entry<InvestmentInstrumentHierarchy, List<Integer>> investmentInstrumentHierarchyListEntry : hierarchySecurityIdMap.entrySet()) {
				List<String> autoGenerateEventTypeList = uploadCommand.isAutoGenerateEvents() ? getInvestmentSecurityEventRetrieverService().getInvestmentSecurityEventTypeNamesWithRetrieverSupportedForHierarchy(
						investmentInstrumentHierarchyListEntry.getKey()) : null;
				String[] autoGenerateEventTypeNames = (autoGenerateEventTypeList == null ? null : autoGenerateEventTypeList.toArray(new String[0]));
				if (!CollectionUtils.isEmpty(autoGenerateEventTypeList)) {
					for (Integer securityId : investmentInstrumentHierarchyListEntry.getValue()) {
						getInvestmentSecurityEventRetrieverService().loadInvestmentSecurityEventHistory(securityId, autoGenerateEventTypeNames, true, false);
					}
					uploadCommand.getUploadResult().addUploadResults("InvestmentSecurityEvent - " + (investmentInstrumentHierarchyListEntry.getKey()).getNameExpanded(), "Auto-Generated all events for " + investmentInstrumentHierarchyListEntry.getValue().size() + " securities: " + StringUtils.collectionToCommaDelimitedString(autoGenerateEventTypeList));
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////
	////////   Investment Security Allocation Uploads     //////////
	////////////////////////////////////////////////////////////////


	@Override
	public DataTable downloadInvestmentSecurityAllocationUploadFileSample(InvestmentSecurityAllocationUploadCommand uploadCommand, boolean allRows) {
		validateInvestmentSecurityAllocationUpload(uploadCommand);

		Map<String, Object> additionalFilterMap = new HashMap<>();
		additionalFilterMap.put("parentInvestmentSecurity.id", uploadCommand.getParentInvestmentSecurity().getId());

		// Pull Generic Upload, with custom fields for selected hierarchy, and insertMissingFkBeans = false
		DataTable dataTable = getSystemUploadService().downloadSystemUploadFileSampleWithAdditionalFilters(uploadCommand, allRows, additionalFilterMap);

		// Customize the sample file:
		// Remove all Parent Security Columns - Not needed because selected on screen
		// Remove Start Date - Not needed because selected on screen
		// And Remove "Shares" if not supported by the allocation type
		DataColumn[] columns = dataTable.getColumnList();
		if (columns != null && columns.length > 0) {
			for (DataColumn column : columns) {
				// Skip Parent in Sample - Included On Screen
				if (column.getColumnName() != null && column.getColumnName().startsWith("ParentInvestmentSecurity")) {
					column.setHidden(true);
				}

				// And Remove "Shares" if not supported by the allocation type
				if (StringUtils.isEqual(column.getColumnName(), "AllocationShares") &&
						StringUtils.isEmpty(uploadCommand.getParentInvestmentSecurity().getInstrument().getHierarchy().getSecurityAllocationType().getSharesLabel())) {
					column.setHidden(true);
				}
				if (StringUtils.isEqual(column.getColumnName(), "StartDate")) {
					column.setHidden(true);
				}
			}
		}
		return dataTable;
	}


	@Override
	@Transactional
	public Status uploadInvestmentSecurityAllocationUploadFile(InvestmentSecurityAllocationUploadCommand uploadCommand) {
		Status status = new Status();
		try {
			validateInvestmentSecurityAllocationUpload(uploadCommand);
		}
		catch (ValidationException e) {
			if (TransactionSynchronizationManager.isActualTransactionActive()) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
			status.setMessage("Upload failed - failed to validate upload data.");
			status.addError(e.getMessage());
			return status;
		}
		InvestmentSecurity security = uploadCommand.getParentInvestmentSecurity();

		// Will be used to merge existing, with new and apply for changes only if selected
		List<InvestmentSecurityAllocation> existingList = null;

		if (uploadCommand.getStartDate() == null) {
			uploadCommand.setStartDate(security.getStartDate());
		}
		else if (uploadCommand.isApplyAsFullAllocationListOnStartDate()) {
			SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
			searchForm.setParentInvestmentSecurityId(security.getId());
			searchForm.setActiveOnDate(uploadCommand.getStartDate());
			existingList = getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationList(searchForm);
		}

		List<IdentityObject> beanList = null;

		try {
			beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, !uploadCommand.isSimple());
		}
		catch (Exception e) {
			if (TransactionSynchronizationManager.isActualTransactionActive()) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
			status.setMessage("Could not process, error in file conversion.");
			status.addError(e.getMessage());
			return status;
		}

		if (!uploadCommand.isError()) {
			// Only saving Securities
			List<InvestmentSecurityAllocation> saveList = new ArrayList<>();

			for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
				try {
					InvestmentSecurityAllocation allocation = (InvestmentSecurityAllocation) obj;
					if (allocation.getStartDate() != null && !DateUtils.isEqual(allocation.getStartDate(), uploadCommand.getStartDate())) {
						String message = "Note: Allocation " + allocation.getAllocationLabel() + " overrides Start Date to " + allocation.getStartDate();
						if (uploadCommand.isApplyAsFullAllocationListOnStartDate()) {
							status.addError(message);
						}
						else {
							status.addMessage(message);
						}
					}
					// Populate security
					getInvestmentSecurityAllocationDataPopulator().populateAllocation(allocation, uploadCommand, status);

					// Validate that it found child security or instrument
					if (allocation.getInvestmentSecurity() != null && !StringUtils.isEmpty(allocation.getInvestmentSecurity().getSymbol())) {
						ValidationUtils.assertFalse(allocation.getInvestmentSecurity().isNewBean(), "System was not able to find Security with Symbol [" + allocation.getInvestmentSecurity().getSymbol() + "].  Please double check your file.");
					}
					else if (allocation.getInvestmentInstrument() != null && !StringUtils.isEmpty(allocation.getInvestmentInstrument().getIdentifierPrefix())) {
						ValidationUtils.assertFalse(allocation.getInvestmentInstrument().isNewBean(), "System was not able to find Instrument with Identifier Prefix [" + allocation.getInvestmentInstrument() + "].  Please double check your file.");
					}

					// Set Parent Security
					allocation.setParentInvestmentSecurity(security);
					// Set Start Date (if not blank and not equal to security start)
					if (DateUtils.compare(security.getStartDate(), uploadCommand.getStartDate(), false) != 0 && allocation.getStartDate() == null) {
						allocation.setStartDate(uploadCommand.getStartDate());
					}
					saveList.add(allocation);
				}
				catch (Exception e) {
					if (TransactionSynchronizationManager.isActualTransactionActive()) {
						TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					}
					if (obj instanceof InvestmentSecurityAllocation) {
						status.addError(((InvestmentSecurityAllocation) obj).getAllocationLabel() + "\n" + e.getMessage());
					}
					else {
						status.addError(e.getMessage());
					}
				}
			}

			if (uploadCommand.isApplyAsFullAllocationListOnStartDate()) {
				BigDecimal totalWeight = CoreMathUtils.sumProperty(saveList, InvestmentSecurityAllocation::getAllocationWeight);
				// we allow total weight of zero, because that indicates that allocations were specified by shares instead of weight
				if (!MathUtils.isEqual(totalWeight, BigDecimal.ZERO) && !MathUtils.isEqual(totalWeight, new BigDecimal("100"))) {
					status.addError("Total weight of allocations did not sum to 100%. Total weight was " + totalWeight + "%");
					return generateStatusMessage(status, beanList);
				}
			}

			mergeAndSaveExistingWithNewAllocations(saveList, existingList, status, uploadCommand);

			if (uploadCommand.isRecalculateRebalanceOnStartDate()) {
				getInvestmentSecurityAllocationHandler().rebuildInvestmentSecurityAllocationRebalance(security, uploadCommand.getDataSourceId(), uploadCommand.getStartDate(), uploadCommand.getStartingPrice(), uploadCommand.isUpdateWeights());
				uploadCommand.getUploadResult().addUploadResults("InvestmentSecurityAllocationRebalance", "Rebalance on [" + DateUtils.fromDateShort(uploadCommand.getStartDate()) + "] recalculated.");
			}

			uploadCommand.getUploadResult().addUploadResults("InvestmentSecurityAllocation", CollectionUtils.getSize(saveList), true);
			return generateStatusMessage(status, beanList);
		}
		else {
			status.setMessage(uploadCommand.getUploadResult().toString());
		}

		return status;
	}


	private Status generateStatusMessage(Status status, List<IdentityObject> beanList) {
		StringBuilder message = new StringBuilder();
		if (status.getErrorCount() > 0) {
			message.append("Attempted to process ")
					.append(CollectionUtils.getSize(beanList))
					.append(" records. Failed on ")
					.append(status.getErrorCount())
					.append(" records.");
		}
		else {
			message.append("Successfully processed ")
					.append(CollectionUtils.getSize(beanList))
					.append(" records with no errors.");
		}
		status.setMessage(message.toString());
		return status;
	}


	private void mergeAndSaveExistingWithNewAllocations(List<InvestmentSecurityAllocation> newList, List<InvestmentSecurityAllocation> existingList, Status status, InvestmentSecurityAllocationUploadCommand command) {
		if (CollectionUtils.isEmpty(existingList)) {
			try {
				getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocationList(newList, null);
			}
			catch (Exception e) {
				if (TransactionSynchronizationManager.isActualTransactionActive()) {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				}
				status.addError(e.getMessage());
			}
		}
		else {
			Date startDate = command.getStartDate();
			Set<InvestmentSecurityAllocation> endedAllocations = existingList.stream().collect(Collectors.toSet());
			List<InvestmentSecurityAllocation> saveList = new ArrayList<>();
			for (InvestmentSecurityAllocation bean : newList) {
				try {
					boolean found = false;

					for (InvestmentSecurityAllocation existing : CollectionUtils.getIterable(existingList)) {

						if (isSameAllocation(bean, existing)) {
							if (CollectionUtils.contains(endedAllocations, existing)) {
								endedAllocations.remove(existing);
							}
							found = true;

							if (bean.getAllocationShares() != null) {
								if (MathUtils.isEqual(bean.getAllocationShares(), BigDecimal.ZERO)) {
									// Now It's Zero - Considered To Be Removed - Do Nothing
									// Set End Date On Existing & Add Note (If included in upload)
									existing.setEndDate(DateUtils.addDays(startDate, -1));
									if (!StringUtils.isEmpty(bean.getNote())) {
										existing.setNote(StringUtils.coalesce(true, existing.getNote()) + " " + bean.getNote());
									}
									saveList.add(existing);
								}

								else if (MathUtils.isEqual(bean.getAllocationShares(), existing.getAllocationShares())) {
									// No Change -  Keep Existing
									saveList.add(existing);
									break;
								}
								else {
									replaceAllocation(bean, existing, saveList, startDate);
								}
							}
							else if (bean.getAllocationWeight() != null) {
								if (MathUtils.isEqual(bean.getAllocationWeight(), BigDecimal.ZERO)) {
									// Now It's Zero - Considered To Be Removed - Do Nothing
									// Set End Date On Existing & Add Note (If included in upload)
									existing.setEndDate(DateUtils.addDays(startDate, -1));
									if (!StringUtils.isEmpty(bean.getNote())) {
										existing.setNote(StringUtils.coalesce(true, existing.getNote()) + " " + bean.getNote());
									}
									saveList.add(existing);
								}

								else if (MathUtils.isEqual(bean.getAllocationWeight(), existing.getAllocationWeight())) {
									// No Change -  Keep Existing
									saveList.add(existing);
									break;
								}
								else {
									replaceAllocation(bean, existing, saveList, startDate);
								}
							}
						}
					}
					if (!found) {
						// Add New Allocation
						saveList.add(bean);
					}
				}
				catch (Exception e) {
					if (TransactionSynchronizationManager.isActualTransactionActive()) {
						TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					}
					status.addError(bean.getAllocationLabel() + "\n" + e.getMessage());
				}
			}
			try {
				// Now, by the time we get here, the map will contain all existing allocations as keys. Any allocations that are ended will have null values
				endedAllocations.forEach(alloc -> endAllocation(alloc, saveList, command));

				getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocationList(saveList, existingList);
			}
			catch (Exception e) {
				if (TransactionSynchronizationManager.isActualTransactionActive()) {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				}
				status.addError(e.getMessage());
			}
		}
	}


	private void replaceAllocation(InvestmentSecurityAllocation bean, InvestmentSecurityAllocation existing, List<InvestmentSecurityAllocation> saveList, Date startDate) {
		// Change - End Existing - Start New
		// Need to copy existing otherwise the item in existingList is changed, and so the saveList functionality won't pick up the change
		InvestmentSecurityAllocation copyOfExisting = new InvestmentSecurityAllocation();
		BeanUtils.copyProperties(existing, copyOfExisting);
		copyOfExisting.setEndDate(DateUtils.addDays(startDate, -1));

		saveList.add(copyOfExisting);
		saveList.add(bean);
	}


	private void endAllocation(InvestmentSecurityAllocation existing, List<InvestmentSecurityAllocation> saveList, InvestmentSecurityAllocationUploadCommand command) {
		InvestmentSecurityAllocation copyOfExisting = new InvestmentSecurityAllocation();
		BeanUtils.copyProperties(existing, copyOfExisting);
		copyOfExisting.setEndDate(DateUtils.addDays(command.getStartDate(), -1));

		saveList.add(copyOfExisting);
	}


	private boolean isSameAllocation(InvestmentSecurityAllocation allocation1, InvestmentSecurityAllocation allocation2) {
		if (CompareUtils.isEqual(allocation1.getInvestmentSecurity(), allocation2.getInvestmentSecurity())) {
			if (CompareUtils.isEqual(allocation1.getInvestmentInstrument(), allocation2.getInvestmentInstrument())) {
				if (CompareUtils.isEqual(allocation1.getCurrentSecurityCalculatorBean(), allocation2.getCurrentSecurityCalculatorBean())) {
					return true;
				}
			}
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////////////////
	//  Helper Methods
	//////////////////////////////////////////////////////////////////////////////////////


	private void validateInvestmentSecurityUpload(InvestmentSecurityUploadCommand uploadCommand) {
		ValidationUtils.assertMutuallyExclusive("Either Single Hierarchy or Parent Hierarchy is required, but not both.", uploadCommand.getHierarchy(), uploadCommand.getParentHierarchy());

		if (uploadCommand.getHierarchy() != null) {
			if (!uploadCommand.getHierarchy().isAssignmentAllowed()) {
				throw new FieldValidationException("The hierarchy '" + uploadCommand.getHierarchy().getNameExpanded() + "' does not allow assignments. Cannot upload securities in this hierarchy.", "hierarchy.id");
			}
			uploadCommand.setLinkedValue(uploadCommand.getHierarchy().getId().toString());
		}
		// Because we are inserting instruments for 1:1 securities, we need to have Instrument-TradingCurrency-Symbol available as a column
		if (uploadCommand.isOneSecurityPerInstrument()) {
			uploadCommand.setAdditionalBeanPropertiesToInclude("instrument.tradingCurrency", "instrument.exchange", "instrument.compositeExchange", "instrument.countryOfRisk", "instrument.countryOfIncorporation");
			// Cannot instantiate an abstract class so must ignore this property
			uploadCommand.setBeanPropertiesToExclude(new String[]{"instrument.countryOfRisk.systemList", "instrument.countryOfIncorporation.systemList"});
		}
	}


	private void validateInvestmentSecurityAllocationUpload(InvestmentSecurityAllocationUploadCommand uploadCommand) {
		ValidationUtils.assertNotNull(uploadCommand.getParentInvestmentSecurity(), "Parent Security selection is required.");
		ValidationUtils.assertTrue(uploadCommand.getParentInvestmentSecurity().isAllocationOfSecurities(), "Security selected [" + uploadCommand.getParentInvestmentSecurity().getLabel() + "] is invalid because it is not an allocation of securities.");
	}

	//////////////////////////////////////////////////////////////////
	/////////            Getter & Setter Methods            //////////
	//////////////////////////////////////////////////////////////////


	public SystemUploadService getSystemUploadService() {
		return this.systemUploadService;
	}


	public void setSystemUploadService(SystemUploadService systemUploadService) {
		this.systemUploadService = systemUploadService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventRetrieverService getInvestmentSecurityEventRetrieverService() {
		return this.investmentSecurityEventRetrieverService;
	}


	public void setInvestmentSecurityEventRetrieverService(InvestmentSecurityEventRetrieverService investmentSecurityEventRetrieverService) {
		this.investmentSecurityEventRetrieverService = investmentSecurityEventRetrieverService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}


	public InvestmentSecurityAllocationHandler getInvestmentSecurityAllocationHandler() {
		return this.investmentSecurityAllocationHandler;
	}


	public void setInvestmentSecurityAllocationHandler(InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler) {
		this.investmentSecurityAllocationHandler = investmentSecurityAllocationHandler;
	}


	public InvestmentExchangeService getInvestmentExchangeService() {
		return this.investmentExchangeService;
	}


	public void setInvestmentExchangeService(InvestmentExchangeService investmentExchangeService) {
		this.investmentExchangeService = investmentExchangeService;
	}


	public SystemListService<SystemList> getSystemListService() {
		return this.systemListService;
	}


	public void setSystemListService(SystemListService<SystemList> systemListService) {
		this.systemListService = systemListService;
	}


	public InvestmentSecurityAllocationDataPopulator getInvestmentSecurityAllocationDataPopulator() {
		return this.investmentSecurityAllocationDataPopulator;
	}


	public void setInvestmentSecurityAllocationDataPopulator(InvestmentSecurityAllocationDataPopulator investmentSecurityAllocationDataPopulator) {
		this.investmentSecurityAllocationDataPopulator = investmentSecurityAllocationDataPopulator;
	}
}
