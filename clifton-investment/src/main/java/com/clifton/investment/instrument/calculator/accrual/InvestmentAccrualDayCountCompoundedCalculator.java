package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.DayCountCommand;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.sign.AccrualSignCalculators;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentAccrualDayCountCompoundedCalculator</code> class implements InvestmentAccrualCalculator for the AccrualCalculator.DAYCOUNT_COMPOUNDED type.
 * It is similar to DAYCOUNT calculator but support multiple interest rate changes during a single payment period. Accrued interest from the first
 * period is compounded during the second period (added to the notional), etc.  InvestmentSecurityEventDetail are used to specify rate changes.
 *
 * @author vgomelsky
 */
public class InvestmentAccrualDayCountCompoundedCalculator extends BaseInvestmentAccrualCalculator {

	private InvestmentSecurityEventService investmentSecurityEventService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal doCalculateAccruedInterest(InvestmentSecurityEvent paymentEvent, BigDecimal notional, BigDecimal notionalMultiplier, Date accrueUpToDate) {
		// lookup security configuration necessary to calculate accrual
		AccrualConfig accrualConfig = getAccrualConfig(paymentEvent, false);

		// adjust accrual start date if this is a partial first period
		Date accrualStartDate = paymentEvent.getAccrualStartDate();
		InvestmentSecurity security = paymentEvent.getSecurity();
		if (security.getStartDate() != null && DateUtils.compare(paymentEvent.getAccrualStartDate(), security.getStartDate(), false) < 0) {
			if (!security.getInstrument().getHierarchy().isFullPaymentForFirstPartialPeriod()) {
				accrualStartDate = security.getStartDate();
			}
		}

		// zero or negative days or accrual means 0 accrued interest (IRS with accrual start shifted by 1 day)
		if (DateUtils.compare(accrueUpToDate, accrualStartDate, true) <= 0) {
			return BigDecimal.ZERO;
		}

		// adjust notional if necessary (multiply by Index Ratio for inflation linked securities)
		if (notionalMultiplier == null) {
			notionalMultiplier = BigDecimal.ONE;
		}
		BigDecimal adjustedNotional = MathUtils.multiply(notional, notionalMultiplier); // should we round to 2 ???

		// Make use of caching the list by event, then filter results
		List<InvestmentSecurityEventDetail> fullDetailList = getInvestmentSecurityEventService().getInvestmentSecurityEventDetailListByEvent(paymentEvent.getId());
		fullDetailList = BeanUtils.sortWithFunction(fullDetailList, InvestmentSecurityEventDetail::getEffectiveDate, true);
		List<InvestmentSecurityEventDetail> detailList = new ArrayList<>();
		for (InvestmentSecurityEventDetail detail : CollectionUtils.getIterable(fullDetailList)) {
			// skip details where the rate hasn't been populated yet
			if (detail.getReferenceRate() != null && DateUtils.compare(detail.getEffectiveDate(), accrueUpToDate, false) <= 0) {
				detailList.add(detail);
			}
		}

		InvestmentSecurityEventDetail firstDetail = new InvestmentSecurityEventDetail();
		firstDetail.setEffectiveDate(accrualStartDate);
		firstDetail.setReferenceRate(paymentEvent.getAfterEventValue());
		detailList.add(0, firstDetail);

		BigDecimal result = BigDecimal.ZERO;
		int periodCount = detailList.size();
		for (int i = 0; i < periodCount; i++) {
			InvestmentSecurityEventDetail detail = detailList.get(i);
			Date upToDate = (i == (periodCount - 1)) ? accrueUpToDate : detailList.get(i + 1).getEffectiveDate();
			BigDecimal accrual = InvestmentCalculatorUtils.getNotionalCalculator(security).round(
					detail.getEffectiveRate()
							.movePointLeft(2)
							.multiply(adjustedNotional)
							.multiply(accrualConfig.calculateAccrualRate(
									DayCountCommand.forAccrualRange(detail.getEffectiveDate(), upToDate)
											.withAccrualPeriod(paymentEvent.getAccrualStartDate(), paymentEvent.getAccrualEndDate())
											.withFrequency(accrualConfig.getFrequency())
							)));
			result = MathUtils.add(result, accrual);
			// adjust notional to account for compounding
			adjustedNotional = MathUtils.add(adjustedNotional, accrual);
		}

		AccrualSignCalculators signCalculator = security.getInstrument().getHierarchy().getAccrualSignCalculator();
		if (signCalculator != null) {
			if (getInvestmentAccrualCalculatorFactory().getInvestmentAccrualSignCalculator(signCalculator).isAccrualAmountNegated(security, accrualConfig.getCouponFrequencyField())) {
				result = MathUtils.negate(result);
			}
		}

		// skip full payment events (as opposed to partial period accrual)
		Date dayAfterPeriodEnd = DateUtils.addDays(paymentEvent.getAccrualEndDate(), 1);
		if (!accrueUpToDate.equals(dayAfterPeriodEnd)) {
			if (DateUtils.compare(accrueUpToDate, paymentEvent.getExDate(), false) >= 0 && !security.getInstrument().getHierarchy().isAccrueAfterExDate()) {
				// British Linkers (Gilts) have the concept of Ex Date which is 8 days before period end date.
				// One is entitled to full dividend for a position at close of day before Ex Date.  From Ex Date to Accrual Period End Date, we're accruing negative interest.
				result = result.subtract(doCalculateAccruedInterest(paymentEvent, notional, notionalMultiplier, dayAfterPeriodEnd));
			}
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}
}
