package com.clifton.investment.instrument.structure;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.List;


/**
 * The <code>InvestmentInstrumentSecurityStructure</code> is a virtual dto used
 * to view/edit/save {@link InvestmentSecurityStructureWeight} list
 * for an {@link InvestmentInstrumentStructure} and {@link InvestmentSecurity}
 * <p/>
 * NOTE: Not a Real Table
 * Similar to an extended dto class where it is populated via hibernate query in xml
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class InvestmentInstrumentSecurityStructure extends BaseSimpleEntity<Integer> implements LabeledObject {

	private String uuid;

	private InvestmentInstrumentStructure structure;
	private InvestmentSecurity security;

	/**
	 * True if list of {@link InvestmentSecurityStructureWeight} has been set up for this
	 * structure and security
	 */
	private boolean populated;

	private List<InvestmentSecurityStructureWeight> securityStructureWeightList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isNewBean() {
		return false;
	}


	@Override
	public String getLabel() {
		return getStructure().getLabel() + " - " + getSecurity().getLabel();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentStructure getStructure() {
		return this.structure;
	}


	public void setStructure(InvestmentInstrumentStructure structure) {
		this.structure = structure;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public List<InvestmentSecurityStructureWeight> getSecurityStructureWeightList() {
		return this.securityStructureWeightList;
	}


	public void setSecurityStructureWeightList(List<InvestmentSecurityStructureWeight> securityStructureWeightList) {
		this.securityStructureWeightList = securityStructureWeightList;
	}


	public boolean isPopulated() {
		return this.populated;
	}


	public void setPopulated(boolean populated) {
		this.populated = populated;
	}


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
