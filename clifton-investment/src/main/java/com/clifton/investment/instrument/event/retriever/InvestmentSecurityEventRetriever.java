package com.clifton.investment.instrument.event.retriever;


import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.List;


/**
 * The <code>InvestmentSecurityEventRetriever</code> interface must be implemented by event type specific
 * event retrievers: Factor Change, Dividend Payment, etc.
 * <p/>
 * A specific event market data provider must be configured for each event type.
 * User InvestmentSecurityEventRetrieverLocator to locate a corresponding event retriever.
 *
 * @author vgomelsky
 */
public interface InvestmentSecurityEventRetriever {

	/**
	 * Retrieves latest event for the specified security.
	 */
	public InvestmentSecurityEvent getInvestmentSecurityEvent(InvestmentSecurity security);


	/**
	 * Retrieves full history of events for the specified security.
	 */
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventHistory(InvestmentSecurity security, boolean ignoreSeedEvents);


	/**
	 * Prepares the specified event to be saved in the system. Depending on event type, this
	 * logic can differ.  For example, Factor Change event may lookup and set previous factor.
	 * Event values may also be adjusted for rounding.
	 * <p/>
	 * Returns false if this event should be skipped (already exists, no change, etc.).
	 */
	public boolean prepareInvestmentSecurityEvent(InvestmentSecurityEvent event);
}
