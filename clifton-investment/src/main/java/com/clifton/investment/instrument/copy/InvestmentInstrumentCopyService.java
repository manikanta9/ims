package com.clifton.investment.instrument.copy;

import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * <code>InvestmentInstrumentCopyService</code> provides services for copying
 * investment data types from an existing one.
 *
 * @author NickK
 */
public interface InvestmentInstrumentCopyService {

	/**
	 * Customized copy from template method for Options that allows for overriding Instrument and
	 * custom columns (Strike, Expiration, Type, etc.) defined on the provided {@link InvestmentSecurityCopyCommand}.
	 * The newly created security will have updated:
	 * <br/>- Symbol, Name, Description
	 * <br/>- (Optional) Custom Security Columns (Example: Strike Price and Option Style)
	 * <br/>- (Optional) Instrument (if new Instrument is OTC, Counterparty and ISDA are required and defined)
	 * <p/>
	 * If {@link InvestmentSecurityCopyCommand#isReturnExisting()} is true, the new symbol generated will be used to
	 * determine if a security already exists and the found security will be returned. If false, a ValidationException
	 * will be thrown upon duplicate symbol.
	 */
	public InvestmentSecurity saveInvestmentSecurityOptionWithCommand(InvestmentSecurityCopyCommand command);


	/**
	 * Customized copy from template method for Forwards that requires a Settlement Date for the new Security and
	 * that allows for overriding start/end dates defined on the provided {@link InvestmentSecurityCopyCommand}.
	 * If {@link InvestmentSecurityCopyCommand#isReturnExisting()} is true, the new symbol generated will be used to
	 * determine if a security already exists and the found security will be returned. If false, a ValidationException
	 * will be thrown upon duplicate symbol.
	 */
	public InvestmentSecurity saveInvestmentSecurityForwardWithCommand(InvestmentSecurityCopyCommand command);


	/**
	 * Special security save method where the security was created from a template of another security in the same hierarchy
	 * Custom field values are often copied (sometimes changed in UI) so those on the bean have their ids set to null so they'll be inserted as new entities associated with the new security
	 * For one to one securities the instrument will also be cloned and clear the id value so a new one is inserted and the new security will reference it's own new instrument
	 * For many to one securities, the security will point to the same instrument as what it is copied from was
	 */
	public InvestmentSecurity saveInvestmentSecurityFromTemplate(InvestmentSecurity bean, Integer copyFromSecurityId);
}
