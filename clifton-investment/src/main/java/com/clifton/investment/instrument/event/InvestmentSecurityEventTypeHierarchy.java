package com.clifton.investment.instrument.event;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.report.definition.Report;


/**
 * The <code>InvestmentSecurityEventTypeHierarchy</code> class links valid event types to corresponding instrument hierarchies.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityEventTypeHierarchy extends ManyToManyEntity<InvestmentSecurityEventType, InvestmentInstrumentHierarchy, Integer> {

	/**
	 * When reconciling/viewing event journal details, if the report is selected
	 * this is the report generated.  i.e. Total Return Swaps: Equity Leg/Interest Leg events use TRS Reset Report
	 */
	private Report eventReport;

	/**
	 * Events with this flag set will be copied to all securities whose
	 * instruments underlying is the security that this event was created for.
	 */
	private boolean copiedFromUnderlying;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Report getEventReport() {
		return this.eventReport;
	}


	public void setEventReport(Report eventReport) {
		this.eventReport = eventReport;
	}


	public boolean isCopiedFromUnderlying() {
		return this.copiedFromUnderlying;
	}


	public void setCopiedFromUnderlying(boolean copiedFromUnderlying) {
		this.copiedFromUnderlying = copiedFromUnderlying;
	}
}
