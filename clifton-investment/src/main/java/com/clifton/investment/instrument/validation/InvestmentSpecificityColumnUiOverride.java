package com.clifton.investment.instrument.validation;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.specificity.BaseInvestmentSpecificityBean;

import java.util.Objects;
import java.util.function.Predicate;


/**
 * {@link BaseInvestmentSpecificityBean} implementation class for investment field UI overrides. This class provides fields for overriding standard field UI
 * configurations at the client-side.
 */
public class InvestmentSpecificityColumnUiOverride extends BaseInvestmentSpecificityBean implements ValidationAware {

	public static final int LABEL_OVERRIDE_MAXIMUM_LENGTH = 20;

	private String fieldLabelOverride;
	private Applicability fieldValueApplicability;


	public String getFieldLabelOverride() {
		return this.fieldLabelOverride;
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertFalse(StringUtils.length(getFieldLabelOverride()) > LABEL_OVERRIDE_MAXIMUM_LENGTH, "The label length cannot be greater than " + LABEL_OVERRIDE_MAXIMUM_LENGTH);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setFieldLabelOverride(String fieldLabelOverride) {
		this.fieldLabelOverride = fieldLabelOverride;
	}


	public Applicability getFieldValueApplicability() {
		return this.fieldValueApplicability;
	}


	public void setFieldValueApplicability(Applicability fieldValueApplicability) {
		this.fieldValueApplicability = fieldValueApplicability;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public enum Applicability {

		OPTIONAL(v -> true, "The field is not required to populate its value, but will be displayed in the UI."),
		REQUIRED(Objects::nonNull, "The field is required to populate its value and will be displayed in the UI."),
		NOT_APPLICABLE(v -> true, "The field is not applicable and will not be displayed in the UI.");

		private Predicate<Object> predicate;
		private String label;


		Applicability(Predicate<Object> predicate, String label) {
			this.predicate = predicate;
			this.label = label;
		}


		public Predicate<Object> getPredicate() {
			return this.predicate;
		}


		public String getLabel() {
			return this.label;
		}
	}
}
