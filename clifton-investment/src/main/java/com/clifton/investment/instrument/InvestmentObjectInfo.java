package com.clifton.investment.instrument;

import com.clifton.business.shared.Company;
import com.clifton.investment.account.InvestmentAccount;


/**
 * The InvestmentObjectInfo interface defines common attributes used by investment objects.
 *
 * @author vgomelsky
 */
public interface InvestmentObjectInfo {

	public InvestmentAccount getClientInvestmentAccount();


	public InvestmentAccount getHoldingInvestmentAccount();


	public InvestmentSecurity getInvestmentSecurity();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the {@link Company} that has the corresponding Data Source mapped to for Exchange Rate lookups.
	 * Supports FX Source overrides in the following order: COALESCE(Holding Account FX Source, Client Account FX Source, Holding Account Issuer).
	 */
	default public Company fxSourceCompany() {
		InvestmentAccount holdingAccount = getHoldingInvestmentAccount();
		if (holdingAccount != null) {
			if (holdingAccount.getDefaultExchangeRateSourceCompany() != null) {
				return holdingAccount.getDefaultExchangeRateSourceCompany().toCompany();
			}
		}
		InvestmentAccount clientAccount = getClientInvestmentAccount();
		if (clientAccount != null && clientAccount.getDefaultExchangeRateSourceCompany() != null) {
			return clientAccount.getDefaultExchangeRateSourceCompany().toCompany();
		}
		if (holdingAccount != null && holdingAccount.getIssuingCompany() != null) {
			return holdingAccount.getIssuingCompany().toCompany();
		}
		return null;
	}


	/**
	 * Returns true if the default Exchange Rate source was overridden on this object's Holding or Client Account.
	 */
	default public boolean fxSourceCompanyOverridden() {
		InvestmentAccount investmentAccount = getHoldingInvestmentAccount();
		if (investmentAccount != null && investmentAccount.getDefaultExchangeRateSourceCompany() != null) {
			return true;
		}
		investmentAccount = getClientInvestmentAccount();
		if (investmentAccount != null && investmentAccount.getDefaultExchangeRateSourceCompany() != null) {
			return true;
		}
		return false;
	}
}
