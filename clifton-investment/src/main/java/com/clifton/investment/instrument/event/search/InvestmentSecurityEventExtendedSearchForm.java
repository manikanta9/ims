package com.clifton.investment.instrument.event.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.investment.instrument.event.FractionalShares;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentSecurityEventExtendedSearchForm</code> class defines search configuration for {@link com.clifton.investment.instrument.event.InvestmentSecurityEventExtended} objects.
 *
 * @author nickk
 */
public class InvestmentSecurityEventExtendedSearchForm extends InvestmentSecurityEventSearchForm {

	@SearchField(searchField = "id")
	private Integer eventId;

	@SearchField(searchField = "payout.id")
	private Integer payoutId;

	@SearchField(searchField = "payoutType.id")
	private Short payoutTypeId;

	@SearchField(searchField = "typeCode", searchFieldPath = "payoutType", comparisonConditions = ComparisonConditions.EQUALS)
	private String payoutTypeCode;

	@SearchField
	private Short electionNumber;

	@SearchField
	private Short payoutNumber;

	@SearchField(searchField = "payoutSecurity.id")
	private Integer payoutSecurityId;

	@SearchField
	private BigDecimal payoutBeforeEventValue;

	@SearchField
	private BigDecimal payoutAfterEventValue;

	@SearchField
	private BigDecimal additionalPayoutValue;

	@SearchField
	private Date additionalPayoutDate;

	@SearchField
	private FractionalShares fractionalSharesMethod;

	@SearchField
	private String payoutDescription;

	@SearchField(searchField = "eventDate,additionalPayoutDate")
	private Date payoutDate;

	@SearchField(searchField = "dtcOnly", searchFieldPath = "payout")
	private Boolean dtcOnly;

	// Custom columns

	// Custom to look at payouts if they exist and/or events with no payouts. Only payouts have a flag for default. Events without payouts are default
	private Boolean defaultElection;

	// Custom to look at payouts if they exist and/or events with no payouts. Only payouts have a flag for deleted. Events without payouts are not deleted
	private Boolean deleted;

	private Boolean singlePayout;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getEventId() {
		return this.eventId;
	}


	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}


	public Integer getPayoutId() {
		return this.payoutId;
	}


	public void setPayoutId(Integer payoutId) {
		this.payoutId = payoutId;
	}


	public Short getPayoutTypeId() {
		return this.payoutTypeId;
	}


	public void setPayoutTypeId(Short payoutTypeId) {
		this.payoutTypeId = payoutTypeId;
	}


	public String getPayoutTypeCode() {
		return this.payoutTypeCode;
	}


	public void setPayoutTypeCode(String payoutTypeCode) {
		this.payoutTypeCode = payoutTypeCode;
	}


	public Short getElectionNumber() {
		return this.electionNumber;
	}


	public void setElectionNumber(Short electionNumber) {
		this.electionNumber = electionNumber;
	}


	public Short getPayoutNumber() {
		return this.payoutNumber;
	}


	public void setPayoutNumber(Short payoutNumber) {
		this.payoutNumber = payoutNumber;
	}


	public Integer getPayoutSecurityId() {
		return this.payoutSecurityId;
	}


	public void setPayoutSecurityId(Integer payoutSecurityId) {
		this.payoutSecurityId = payoutSecurityId;
	}


	public BigDecimal getPayoutBeforeEventValue() {
		return this.payoutBeforeEventValue;
	}


	public void setPayoutBeforeEventValue(BigDecimal payoutBeforeEventValue) {
		this.payoutBeforeEventValue = payoutBeforeEventValue;
	}


	public BigDecimal getPayoutAfterEventValue() {
		return this.payoutAfterEventValue;
	}


	public void setPayoutAfterEventValue(BigDecimal payoutAfterEventValue) {
		this.payoutAfterEventValue = payoutAfterEventValue;
	}


	public BigDecimal getAdditionalPayoutValue() {
		return this.additionalPayoutValue;
	}


	public void setAdditionalPayoutValue(BigDecimal additionalPayoutValue) {
		this.additionalPayoutValue = additionalPayoutValue;
	}


	public Date getAdditionalPayoutDate() {
		return this.additionalPayoutDate;
	}


	public void setAdditionalPayoutDate(Date additionalPayoutDate) {
		this.additionalPayoutDate = additionalPayoutDate;
	}


	public FractionalShares getFractionalSharesMethod() {
		return this.fractionalSharesMethod;
	}


	public void setFractionalSharesMethod(FractionalShares fractionalSharesMethod) {
		this.fractionalSharesMethod = fractionalSharesMethod;
	}


	public String getPayoutDescription() {
		return this.payoutDescription;
	}


	public void setPayoutDescription(String payoutDescription) {
		this.payoutDescription = payoutDescription;
	}


	public Date getPayoutDate() {
		return this.payoutDate;
	}


	public void setPayoutDate(Date payoutDate) {
		this.payoutDate = payoutDate;
	}


	public Boolean getDtcOnly() {
		return this.dtcOnly;
	}


	public void setDtcOnly(Boolean dtcOnly) {
		this.dtcOnly = dtcOnly;
	}


	public Boolean getDefaultElection() {
		return this.defaultElection;
	}


	public void setDefaultElection(Boolean defaultElection) {
		this.defaultElection = defaultElection;
	}


	public Boolean getDeleted() {
		return this.deleted;
	}


	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}


	public Boolean getSinglePayout() {
		return this.singlePayout;
	}


	public void setSinglePayout(Boolean singlePayout) {
		this.singlePayout = singlePayout;
	}
}
