package com.clifton.investment.instrument.event.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class InvestmentSecurityEventStatusSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean eventJournalAllowed;

	@SearchField
	private Boolean eventJournalAutomatic;

	@SearchField
	private Boolean eventJournalNotApplicable;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getEventJournalAllowed() {
		return this.eventJournalAllowed;
	}


	public void setEventJournalAllowed(Boolean eventJournalAllowed) {
		this.eventJournalAllowed = eventJournalAllowed;
	}


	public Boolean getEventJournalAutomatic() {
		return this.eventJournalAutomatic;
	}


	public void setEventJournalAutomatic(Boolean eventJournalAutomatic) {
		this.eventJournalAutomatic = eventJournalAutomatic;
	}


	public Boolean getEventJournalNotApplicable() {
		return this.eventJournalNotApplicable;
	}


	public void setEventJournalNotApplicable(Boolean eventJournalNotApplicable) {
		this.eventJournalNotApplicable = eventJournalNotApplicable;
	}
}
