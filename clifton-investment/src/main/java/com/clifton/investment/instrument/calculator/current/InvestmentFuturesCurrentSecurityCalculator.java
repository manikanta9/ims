package com.clifton.investment.instrument.calculator.current;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.CalendarMonth;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.instruction.delivery.InvestmentDeliveryMonthCodes;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;

import java.util.Date;


/**
 * The <code>InvestmentFuturesCurrentSecurityCalculator</code> can be used to determine the current security
 * for a Futures Instrument by calculating the symbol from the instrument + delivery month schedule +
 * <p>
 * Current Contract Symbol is built as Instrument Prefix + Month Code (Determined by deliveryMonthSchedule) + Last Digit of Year (Determined by monthMoveToNextYear)
 * If security with symbol doesn't exist throws an exception.
 *
 * @author manderson
 */
public class InvestmentFuturesCurrentSecurityCalculator extends BaseInvestmentCurrentSecurityCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;

	/**
	 * Each month in the year is assigned the delivery month of the current contract.
	 * So, if we are in January - lookup the januaryDelivery month.  If 3 = March = H
	 * Populated from {@link CalendarMonth} ID - which Uses our month #'s which represent numerical representation of the month, i.e. Jan = 1, Feb = 2
	 */
	private int januaryDelivery;
	private int februaryDelivery;
	private int marchDelivery;
	private int aprilDelivery;
	private int mayDelivery;
	private int juneDelivery;
	private int julyDelivery;
	private int augustDelivery;
	private int septemberDelivery;
	private int octoberDelivery;
	private int novemberDelivery;
	private int decemberDelivery;

	/**
	 * Once passing the given month (or on effective day of given month - see below), current contract year is moved to the following year.
	 * i.e. If in October of 2012, and selected option is September
	 * the current contract would use the month code (from deliveryMonthSchedule) + 3 since we should move to the following year
	 * <p>
	 * Populated from {@link CalendarMonth} ID - which Uses our month #'s which represent numerical representation of the month, i.e. Jan = 1, Feb = 2
	 */
	private int monthYearSwitches;

	/**
	 * Example: Year Switch July, Effective Day -3 calendar days
	 * If false, will switch the year once the date passes the monthYearSwitches (i.e. first day of following month) - Example Year switch is Aug 1st
	 * If true, will switch the year once the date passes the effective day of the give month. - Example Year switch is July 29th
	 */
	private boolean switchYearOnEffectiveDayOfMonth;

	/**
	 * Ability to have "current" year contracts really be for a future year.  Then switching for "monthYearSwitches" can go further in the future
	 * Example: In 2013 I hold 2015 contracts, and at the end of 2014 I switch to 2016 - In this case add to current year = 1
	 */
	private Integer addToCurrentYear;

	/**
	 * Defaults to the first business day of the month, but can
	 * be overridden to not switch the current security until we hit the xth business day of the month.
	 * i.e. = 5, February 4th, 2013 is the 2nd business day of the month.  So, on this day we'd still use the
	 * January month selection.  On February 7th (5th business day of the month) we'd start using the selection for February.
	 * <p>
	 * Use negative value to go from the end of the month - when adding/subtracting days we go from the last day of the previous month, and first day of the next month
	 */
	private Integer businessDayOfMonthEffective;

	/**
	 * Option to use calendar day logic instead of business day logic
	 */
	private boolean useCalendarDays;

	////////////////////////////////////////////////////////////////////////////
	////////////              Validation Methods                     ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void validateImpl(InvestmentInstrument instrument, InvestmentSecurityGroup securityGroup) {
		ValidationUtils.assertNull(securityGroup, "Security group selection is not allowed for the Futures Schedule calculator - doesn't apply.");
		ValidationUtils.assertNotNull(instrument, "Instrument selection is required.");
		ValidationUtils.assertTrue(InvestmentUtils.isInstrumentOfType(instrument, InvestmentType.FUTURES),
				"Futures Delivery Month Current Security Calculators can only be used for Futures type instrument selections");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////              Calculation Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurity calculateImpl(InvestmentInstrument instrument, @SuppressWarnings("unused") InvestmentSecurityGroup securityGroup, InvestmentSecurity underlyingSecurity, Date date) {
		String currentSymbol = calculateCurrentSecuritySymbol(instrument, date, false);
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(currentSymbol, false);
		if (security == null) {
			// Try again with 2 year format
			String currentSymbol2 = calculateCurrentSecuritySymbol(instrument, date, true);
			security = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(currentSymbol2, false);

			if (security == null) {
				throw new ValidationExceptionWithCause("InvestmentInstrument", instrument.getId(), "Missing current security for Investment Instrument [" + instrument.getLabel()
						+ "] using Futures schedule calculator.  Current Security Symbol Calculated As [" + currentSymbol + "] or [" + currentSymbol2 + "], but these securities do not exist.");
			}
		}
		return security;
	}


	protected String calculateCurrentSecuritySymbol(InvestmentInstrument instrument, Date activeOnDate, boolean useTwoYearFormat) {
		String prefix = instrument.getIdentifierPrefix();
		if (prefix.length() == 1) {
			prefix = prefix + " ";
		}
		return prefix + getCurrentContractDeliveryMonthCode(activeOnDate) + getCurrentContractDeliveryYear(activeOnDate, useTwoYearFormat);
	}


	private String getCurrentContractDeliveryMonthCode(Date date) {
		int month = DateUtils.getMonthOfYear(date);
		int effectiveDayOfMonth = getEffectiveDayOfMonth(date);

		// If before effective day of month move to previous month for lookup
		if (DateUtils.getDayOfMonth(date) < effectiveDayOfMonth) {
			month = (month == 1 ? 12 : month - 1);
		}
		int deliveryMonth;
		switch (month) {
			case 1:
				deliveryMonth = getJanuaryDelivery();
				break;
			case 2:
				deliveryMonth = getFebruaryDelivery();
				break;
			case 3:
				deliveryMonth = getMarchDelivery();
				break;
			case 4:
				deliveryMonth = getAprilDelivery();
				break;
			case 5:
				deliveryMonth = getMayDelivery();
				break;
			case 6:
				deliveryMonth = getJuneDelivery();
				break;
			case 7:
				deliveryMonth = getJulyDelivery();
				break;
			case 8:
				deliveryMonth = getAugustDelivery();
				break;
			case 9:
				deliveryMonth = getSeptemberDelivery();
				break;
			case 10:
				deliveryMonth = getOctoberDelivery();
				break;
			case 11:
				deliveryMonth = getNovemberDelivery();
				break;
			case 12:
				deliveryMonth = getDecemberDelivery();
				break;
			default:
				throw new ValidationException("Invalid Month Number for date [" + DateUtils.fromDateShort(date) + "]. Valid Months range from 1 to 12");
		}
		return InvestmentDeliveryMonthCodes.valueOfMonthNumber(deliveryMonth).name();
	}


	private String getCurrentContractDeliveryYear(Date date, boolean useTwoYearFormat) {
		int year = DateUtils.getYear(date);
		if (getAddToCurrentYear() != null) {
			year = year + getAddToCurrentYear();
		}
		if (isSwitchYearOnEffectiveDayOfMonth() && DateUtils.getMonthOfYear(date) == getMonthYearSwitches()) {
			// Check effective day of month
			int effectiveDayOfMonth = getEffectiveDayOfMonth(date);
			if (DateUtils.getDayOfMonth(date) >= effectiveDayOfMonth) {
				year = year + 1;
			}
		}
		else if (DateUtils.getMonthOfYear(date) > getMonthYearSwitches()) {
			year = year + 1;
		}
		return (Integer.toString(year)).substring(useTwoYearFormat ? 2 : 3);
	}


	private int getEffectiveDayOfMonth(Date date) {
		if (getBusinessDayOfMonthEffective() != null && getBusinessDayOfMonthEffective() != 1) {
			Date startDate = (getBusinessDayOfMonthEffective() > 0) ? DateUtils.getLastDayOfPreviousMonth(date) : DateUtils.addDays(DateUtils.getLastDayOfMonth(date), 1);
			if (isUseCalendarDays()) {
				return DateUtils.getDayOfMonth(DateUtils.addDays(startDate, getBusinessDayOfMonthEffective()));
			}
			else {
				return DateUtils.getDayOfMonth(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), getBusinessDayOfMonthEffective()));
			}
		}
		return 1; // Default to first day of month
	}

	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////


	public int getMonthYearSwitches() {
		return this.monthYearSwitches;
	}


	public void setMonthYearSwitches(int monthYearSwitches) {
		this.monthYearSwitches = monthYearSwitches;
	}


	public int getJanuaryDelivery() {
		return this.januaryDelivery;
	}


	public void setJanuaryDelivery(int januaryDelivery) {
		this.januaryDelivery = januaryDelivery;
	}


	public int getFebruaryDelivery() {
		return this.februaryDelivery;
	}


	public void setFebruaryDelivery(int februaryDelivery) {
		this.februaryDelivery = februaryDelivery;
	}


	public int getMarchDelivery() {
		return this.marchDelivery;
	}


	public void setMarchDelivery(int marchDelivery) {
		this.marchDelivery = marchDelivery;
	}


	public int getAprilDelivery() {
		return this.aprilDelivery;
	}


	public void setAprilDelivery(int aprilDelivery) {
		this.aprilDelivery = aprilDelivery;
	}


	public int getMayDelivery() {
		return this.mayDelivery;
	}


	public void setMayDelivery(int mayDelivery) {
		this.mayDelivery = mayDelivery;
	}


	public int getJuneDelivery() {
		return this.juneDelivery;
	}


	public void setJuneDelivery(int juneDelivery) {
		this.juneDelivery = juneDelivery;
	}


	public int getJulyDelivery() {
		return this.julyDelivery;
	}


	public void setJulyDelivery(int julyDelivery) {
		this.julyDelivery = julyDelivery;
	}


	public int getAugustDelivery() {
		return this.augustDelivery;
	}


	public void setAugustDelivery(int augustDelivery) {
		this.augustDelivery = augustDelivery;
	}


	public int getSeptemberDelivery() {
		return this.septemberDelivery;
	}


	public void setSeptemberDelivery(int septemberDelivery) {
		this.septemberDelivery = septemberDelivery;
	}


	public int getOctoberDelivery() {
		return this.octoberDelivery;
	}


	public void setOctoberDelivery(int octoberDelivery) {
		this.octoberDelivery = octoberDelivery;
	}


	public int getNovemberDelivery() {
		return this.novemberDelivery;
	}


	public void setNovemberDelivery(int novemberDelivery) {
		this.novemberDelivery = novemberDelivery;
	}


	public int getDecemberDelivery() {
		return this.decemberDelivery;
	}


	public void setDecemberDelivery(int decemberDelivery) {
		this.decemberDelivery = decemberDelivery;
	}


	public Integer getBusinessDayOfMonthEffective() {
		return this.businessDayOfMonthEffective;
	}


	public void setBusinessDayOfMonthEffective(Integer businessDayOfMonthEffective) {
		this.businessDayOfMonthEffective = businessDayOfMonthEffective;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public Integer getAddToCurrentYear() {
		return this.addToCurrentYear;
	}


	public void setAddToCurrentYear(Integer addToCurrentYear) {
		this.addToCurrentYear = addToCurrentYear;
	}


	public boolean isUseCalendarDays() {
		return this.useCalendarDays;
	}


	public void setUseCalendarDays(boolean useCalendarDays) {
		this.useCalendarDays = useCalendarDays;
	}


	public boolean isSwitchYearOnEffectiveDayOfMonth() {
		return this.switchYearOnEffectiveDayOfMonth;
	}


	public void setSwitchYearOnEffectiveDayOfMonth(boolean switchYearOnEffectiveDayOfMonth) {
		this.switchYearOnEffectiveDayOfMonth = switchYearOnEffectiveDayOfMonth;
	}
}
