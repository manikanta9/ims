package com.clifton.investment.instrument.allocation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.calculator.InvestmentSecurityAllocationCalculatorLocator;
import com.clifton.investment.instrument.allocation.calculator.InvestmentSecurityAllocationRebalanceCalculator;
import com.clifton.investment.instrument.allocation.dynamic.calculator.InvestmentSecurityAllocationDynamicCalculator;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Component
public class InvestmentSecurityAllocationHandlerImpl implements InvestmentSecurityAllocationHandler {


	private AdvancedUpdatableDAO<InvestmentSecurityAllocation, Criteria> investmentSecurityAllocationDAO;

	private InvestmentSecurityAllocationCalculatorLocator investmentSecurityAllocationCalculatorLocator;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	/////////       Investment Security Allocation Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityAllocation getInvestmentSecurityAllocation(int id) {
		return getInvestmentSecurityAllocationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentSecurityAllocation> getInvestmentSecurityAllocationListForParent(int parentSecurityId) {
		return getInvestmentSecurityAllocationDAO().findByField("parentInvestmentSecurity.id", parentSecurityId);
	}


	@Override
	public List<InvestmentSecurityAllocation> getInvestmentSecurityAllocationList(final SecurityAllocationSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// if searching for active on a date, check the parent security is active as well
				if (searchForm.getActiveOnDate() != null) {
					String alias = getPathAlias("parentInvestmentSecurity", criteria);
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(true, alias, "startDate", "endDate", searchForm.getActiveOnDate()));
				}
			}
		};
		return getInvestmentSecurityAllocationDAO().findBySearchCriteria(searchConfig);
	}


	/**
	 * See Validation in {@link com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationValidator} *
	 */
	@Override
	public InvestmentSecurityAllocation saveInvestmentSecurityAllocation(InvestmentSecurityAllocation bean) {
		return getInvestmentSecurityAllocationDAO().save(bean);
	}


	@Override
	@Transactional
	public InvestmentSecurityAllocation saveInvestmentSecurityAllocationFromExisting(Integer existingId, InvestmentSecurityAllocation bean) {
		if (bean.isNewBean() && existingId != null && bean.getStartDate() != null) {
			// End Existing
			InvestmentSecurityAllocation existing = getInvestmentSecurityAllocation(existingId);
			if (existing != null && existing.getEndDate() == null) {
				existing.setEndDate(DateUtils.addDays(bean.getStartDate(), -1));
				saveInvestmentSecurityAllocation(existing);
			}
		}
		return saveInvestmentSecurityAllocation(bean);
	}


	@Override
	public void saveInvestmentSecurityAllocationList(List<InvestmentSecurityAllocation> newList, List<InvestmentSecurityAllocation> oldList) {
		getInvestmentSecurityAllocationDAO().saveList(newList, oldList);
	}


	@Override
	public void deleteInvestmentSecurityAllocation(int id) {
		getInvestmentSecurityAllocationDAO().delete(id);
	}


	@Override
	public void deleteInvestmentSecurityAllocationList(List<InvestmentSecurityAllocation> list) {
		getInvestmentSecurityAllocationDAO().deleteList(list);
	}

	////////////////////////////////////////////////////////////////////////////
	//////       Investment Security Allocation Dynamic Methods          ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentSecurityAllocation> getInvestmentSecurityAllocationDynamicList(InvestmentSecurity security, List<InvestmentSecurityAllocation> allocationList, Date date) {
		validateParentSecurity(security);
		ValidationUtils.assertTrue(security.getInstrument().getHierarchy().isAllocationOfSecuritiesDynamic(), "Dynamically generating allocations and weights for a date is allowed only for hierarchies with a dynamic calculator bean defined.");
		InvestmentSecurityAllocationDynamicCalculator calculatorBean = (InvestmentSecurityAllocationDynamicCalculator) getSystemBeanService().getBeanInstance(security.getInstrument().getHierarchy().getDynamicAllocationCalculatorSystemBean());
		return calculatorBean.calculate(security, allocationList, date);
	}

	////////////////////////////////////////////////////////////////////////////
	//////       Investment Security Allocation Rebalance Methods         //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void deleteInvestmentSecurityAllocationRebalance(InvestmentSecurity security, Date rebalanceDate) {
		validateParentSecurity(security);
		// Can only delete last as long as not start date
		List<InvestmentSecurityAllocationRebalance> rebalanceList = getInvestmentSecurityAllocationRebalanceList(security, false);
		InvestmentSecurityAllocationRebalance last = CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(rebalanceList, InvestmentSecurityAllocationRebalance::getRebalanceDate, false));
		// If none
		if (last == null) {
			throw new ValidationException("There are no rebalance dates available to delete.");
		}
		// If not the last one
		if (DateUtils.compare(last.getRebalanceDate(), rebalanceDate, false) != 0) {
			throw new ValidationException("Rebalance entries can only be removed in order from newest to oldest. Selected Rebalance Date [" + DateUtils.fromDateShort(rebalanceDate)
					+ "] is before last rebalance on [" + DateUtils.fromDateShort(last.getRebalanceDate()) + "].");
		}
		// To delete a rebalance - delete all allocations that start on rebalance date, and change those that end on rebalance date - 1 to end date of null
		SecurityAllocationSearchForm deleteSearchForm = new SecurityAllocationSearchForm();
		deleteSearchForm.setParentInvestmentSecurityId(security.getId());
		// If it's the same as security start, start date on the allocation is likely null
		if (last.isSecurityStart()) {
			deleteSearchForm.setStartDateOrNull(rebalanceDate);
		}
		// Otherwise it needs to be exactly on that date
		else {
			deleteSearchForm.setStartDate(rebalanceDate);
		}
		List<InvestmentSecurityAllocation> deleteList = getInvestmentSecurityAllocationList(deleteSearchForm);
		getInvestmentSecurityAllocationDAO().deleteList(deleteList);

		// If NOT the last one, get the previous allocations and remove the end date
		if (!last.isSecurityStart()) {
			SecurityAllocationSearchForm updateSearchForm = new SecurityAllocationSearchForm();
			updateSearchForm.setParentInvestmentSecurityId(security.getId());
			updateSearchForm.setEndDate(DateUtils.addDays(rebalanceDate, -1));
			List<InvestmentSecurityAllocation> updateList = getInvestmentSecurityAllocationList(updateSearchForm);
			for (InvestmentSecurityAllocation allocation : CollectionUtils.getIterable(updateList)) {
				allocation.setEndDate(null);
			}
			getInvestmentSecurityAllocationDAO().saveList(updateList);
		}
	}


	@Override
	@Transactional
	public void processInvestmentSecurityAllocationRebalance(InvestmentSecurity security, Short dataSourceId, Date baseDate, boolean useBaseWeights, Date startDate, BigDecimal startingPrice) {
		validateParentSecurity(security);
		InvestmentSecurityAllocationTypes type = security.getInstrument().getHierarchy().getSecurityAllocationType();
		if (!type.isAutoRebalancingSupported()) {
			throw new ValidationException("Security [" + security.getLabel() + "] uses security allocation type [" + type.getLabel() + "] which does not support automatic rebalancing.");
		}
		InvestmentSecurityAllocationRebalanceCalculator calculator = getInvestmentSecurityAllocationCalculatorLocator().locateForRebalance(type);
		calculator.rebalanceInvestmentSecurity(security, dataSourceId, baseDate, useBaseWeights, startDate, startingPrice);
	}


	@Override
	@Transactional
	public void rebuildInvestmentSecurityAllocationRebalance(InvestmentSecurity security, Short dataSourceId, Date rebalanceDate, BigDecimal startingPrice, boolean updateWeights) {
		validateParentSecurity(security);
		InvestmentSecurityAllocationTypes type = security.getInstrument().getHierarchy().getSecurityAllocationType();
		if (!type.isAutoRebalancingSupported()) {
			throw new ValidationException("Security [" + security.getLabel() + "] uses security allocation type [" + type.getLabel() + "] which does not support automatic rebalancing.");
		}
		InvestmentSecurityAllocationRebalanceCalculator calculator = getInvestmentSecurityAllocationCalculatorLocator().locateForRebalance(type);
		calculator.recalculateRebalance(security, dataSourceId, rebalanceDate, startingPrice, updateWeights);
	}


	@Override
	public List<InvestmentSecurityAllocationRebalance> getInvestmentSecurityAllocationRebalanceList(InvestmentSecurity security, boolean populateDetails) {
		validateParentSecurity(security);
		List<InvestmentSecurityAllocation> allocationList = getInvestmentSecurityAllocationListForParent(security.getId());
		Map<String, InvestmentSecurityAllocationRebalance> dateMap = new HashMap<>();
		// Should never be null
		if (security.getStartDate() != null) {
			dateMap.put(DateUtils.fromDateShort(security.getStartDate()), new InvestmentSecurityAllocationRebalance(security.getStartDate(), true));
		}

		for (InvestmentSecurityAllocation alloc : CollectionUtils.getIterable(allocationList)) {
			if (alloc.getStartDate() != null) {
				addInvestmentSecurityAllocationRebalanceToList(dateMap, true, alloc.getStartDate(), (populateDetails ? alloc : null));
			}

			if (alloc.getEndDate() != null) {
				addInvestmentSecurityAllocationRebalanceToList(dateMap, false, DateUtils.addDays(alloc.getEndDate(), 1), (populateDetails ? alloc : null));
			}
		}
		List<InvestmentSecurityAllocationRebalance> dateList = new ArrayList<>(dateMap.values());
		return BeanUtils.sortWithFunction(dateList, InvestmentSecurityAllocationRebalance::getRebalanceDate, true);
	}


	/**
	 * @param allocation - if populated will include security information and notes
	 */
	private void addInvestmentSecurityAllocationRebalanceToList(Map<String, InvestmentSecurityAllocationRebalance> dateMap, boolean start, Date date, InvestmentSecurityAllocation allocation) {
		String dateVal = DateUtils.fromDateShort(date);
		InvestmentSecurityAllocationRebalance rebal = dateMap.get(dateVal);
		if (rebal == null) {
			rebal = new InvestmentSecurityAllocationRebalance(date);
		}
		rebal.addSecurityAllocation(start, allocation);
		dateMap.put(dateVal, rebal);
	}

	/////////////////////////////////////////////////////////////////////////////


	private void validateParentSecurity(InvestmentSecurity security) {
		ValidationUtils.assertNotNull(security, "Missing parent security.");
		if (!security.isAllocationOfSecurities()) {
			throw new ValidationException("Security [" + security.getLabel() + "] is not an allocated security.");
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	//////////               Getter & Setter Methods                   //////////
	/////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentSecurityAllocation, Criteria> getInvestmentSecurityAllocationDAO() {
		return this.investmentSecurityAllocationDAO;
	}


	public void setInvestmentSecurityAllocationDAO(AdvancedUpdatableDAO<InvestmentSecurityAllocation, Criteria> investmentSecurityAllocationDAO) {
		this.investmentSecurityAllocationDAO = investmentSecurityAllocationDAO;
	}


	public InvestmentSecurityAllocationCalculatorLocator getInvestmentSecurityAllocationCalculatorLocator() {
		return this.investmentSecurityAllocationCalculatorLocator;
	}


	public void setInvestmentSecurityAllocationCalculatorLocator(InvestmentSecurityAllocationCalculatorLocator investmentSecurityAllocationCalculatorLocator) {
		this.investmentSecurityAllocationCalculatorLocator = investmentSecurityAllocationCalculatorLocator;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
