package com.clifton.investment.instrument.event.action.processor;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventAction;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionService;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionType;
import com.clifton.investment.instrument.event.action.search.InvestmentSecurityEventActionSearchForm;
import com.clifton.security.impersonation.SecurityImpersonationHandler;
import com.clifton.security.user.SecurityUser;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * <code>BaseInvestmentSecurityEventActionProcessor</code> is an abstract {@link InvestmentSecurityEventActionProcessor} with common
 * logic for determining if an action should be performed and managing the persistence of performed actions.
 *
 * @author nickk
 */
public abstract class BaseInvestmentSecurityEventActionProcessor implements InvestmentSecurityEventActionProcessor {

	private InvestmentSecurityEventActionService investmentSecurityEventActionService;

	private SecurityImpersonationHandler securityImpersonationHandler;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public boolean processAction(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventActionType actionType) {
		if (isProcessActionAllowed(securityEvent, actionType)) {
			if (getSecurityImpersonationHandler().runAsSecurityUserNameAndReturn(SecurityUser.SYSTEM_USER, () -> processActionImpl(securityEvent, actionType))) {
				saveEventActionExecutedRecord(securityEvent, actionType);
				return true;
			}
		}
		return false;
	}


	@Override
	@Transactional
	public void rollbackAction(InvestmentSecurityEventAction eventAction) {
		if (getSecurityImpersonationHandler().runAsSecurityUserNameAndReturn(SecurityUser.SYSTEM_USER, () -> rollbackActionImpl(eventAction))) {
			getInvestmentSecurityEventActionService().deleteInvestmentSecurityEventAction(eventAction.getId());
		}
	}


	/**
	 * Preform the action for the {@link InvestmentSecurityEvent} and {@link InvestmentSecurityEventActionType}.
	 * Return true for successful processing, for which an {@link InvestmentSecurityEventAction} will be saved for the event and action type.
	 */
	protected abstract boolean processActionImpl(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventActionType actionType);


	/**
	 * Preform the action rollback for the provided {@link InvestmentSecurityEventAction}.
	 * Return true for successful rollback, for which the action will be deleted.
	 */
	protected abstract boolean rollbackActionImpl(InvestmentSecurityEventAction eventAction);


	/**
	 * Validates that the the execution of the action is after the provided events's event date and that an {@link InvestmentSecurityEventAction} does not already exist for the {@link InvestmentSecurityEvent} and {@link InvestmentSecurityEventActionType}.
	 */
	protected boolean isProcessActionAllowed(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventActionType actionType) {
		if (DateUtils.isDateBefore(new Date(), securityEvent.getEventDate(), false)) {
			throw new ValidationException("Cannot process '" + actionType.getName() + "' before Event Date: " + securityEvent);
		}

		// make sure it hasn't been processed yet
		InvestmentSecurityEventActionSearchForm actionSearchForm = new InvestmentSecurityEventActionSearchForm();
		actionSearchForm.setActionTypeId(actionType.getId());
		actionSearchForm.setSecurityEventId(securityEvent.getId());
		List<InvestmentSecurityEventAction> actionList = getInvestmentSecurityEventActionService().getInvestmentSecurityEventActionList(actionSearchForm);
		return CollectionUtils.isEmpty(actionList);
	}


	/**
	 * Saves an {@link InvestmentSecurityEventAction} for the provided {@link InvestmentSecurityEvent} and {@link InvestmentSecurityEventActionType} for the current day and time (<code>new Date()</code>).
	 */
	protected InvestmentSecurityEventAction saveEventActionExecutedRecord(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventActionType actionType) {
		InvestmentSecurityEventAction action = new InvestmentSecurityEventAction();
		action.setActionType(actionType);
		action.setSecurityEvent(securityEvent);
		action.setProcessedDate(new Date());
		return getInvestmentSecurityEventActionService().saveInvestmentSecurityEventAction(action);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventActionService getInvestmentSecurityEventActionService() {
		return this.investmentSecurityEventActionService;
	}


	public void setInvestmentSecurityEventActionService(InvestmentSecurityEventActionService investmentSecurityEventActionService) {
		this.investmentSecurityEventActionService = investmentSecurityEventActionService;
	}


	public SecurityImpersonationHandler getSecurityImpersonationHandler() {
		return this.securityImpersonationHandler;
	}


	public void setSecurityImpersonationHandler(SecurityImpersonationHandler securityImpersonationHandler) {
		this.securityImpersonationHandler = securityImpersonationHandler;
	}
}
