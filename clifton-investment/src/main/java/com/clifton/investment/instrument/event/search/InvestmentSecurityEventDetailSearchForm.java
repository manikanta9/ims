package com.clifton.investment.instrument.event.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class InvestmentSecurityEventDetailSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "event.id")
	private Integer eventId;

	@SearchField(searchFieldPath = "event", searchField = "security.id")
	private Integer investmentSecurityId;

	@SearchField
	private Date effectiveDate;

	@SearchField
	private BigDecimal referenceRate;

	@SearchField(searchField = "referenceRate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean referenceRatePopulated;

	@SearchField
	private BigDecimal spread;

	@SearchField(comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS, searchField = "effectiveDate")
	private Date effectiveDateBefore;

	@SearchField(comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS, searchField = "effectiveDate")
	private Date effectiveDateAfter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getEventId() {
		return this.eventId;
	}


	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Date getEffectiveDateBefore() {
		return this.effectiveDateBefore;
	}


	public void setEffectiveDateBefore(Date effectiveDateBefore) {
		this.effectiveDateBefore = effectiveDateBefore;
	}


	public Date getEffectiveDateAfter() {
		return this.effectiveDateAfter;
	}


	public void setEffectiveDateAfter(Date effectiveDateAfter) {
		this.effectiveDateAfter = effectiveDateAfter;
	}


	public BigDecimal getReferenceRate() {
		return this.referenceRate;
	}


	public void setReferenceRate(BigDecimal referenceRate) {
		this.referenceRate = referenceRate;
	}


	public BigDecimal getSpread() {
		return this.spread;
	}


	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}


	public Boolean getReferenceRatePopulated() {
		return this.referenceRatePopulated;
	}


	public void setReferenceRatePopulated(Boolean referenceRatePopulated) {
		this.referenceRatePopulated = referenceRatePopulated;
	}
}
