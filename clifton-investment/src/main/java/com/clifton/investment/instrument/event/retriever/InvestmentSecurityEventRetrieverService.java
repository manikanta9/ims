package com.clifton.investment.instrument.event.retriever;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventScheduleCopyCommand;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>InvestmentSecurityEventRetrieverService</code> interface defines methods responsible for retrieval
 * of InvestmentSecurityEvent objects from market data providers and saving them into our system.
 *
 * @author vgomelsky
 */
public interface InvestmentSecurityEventRetrieverService {

	/**
	 * Validates there is at least one security event retriever that applies to selected hierarchy
	 * (Note: Currently used to optionally auto-generate security events during upload process)
	 * <p>
	 * Returns a list of event type names that can be auto-generated for hierarchy
	 *
	 * @param hierarchy
	 * @throws ValidationException if the hierarchy has NO Event Types mapped to it, OR NO Retrievers for any of the event types.
	 */
	@DoNotAddRequestMapping
	public List<String> getInvestmentSecurityEventTypeNamesWithRetrieverSupportedForHierarchy(InvestmentInstrumentHierarchy hierarchy);


	/**
	 * Retrieves the latest security event of the specified type for the specified security
	 * and saves it into our system.
	 * <p>
	 * Skips duplicate events and returns the number of events created.
	 */
	@ModelAttribute
	@RequestMapping("investmentSecurityEventLoad")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_CREATE)
	public Integer loadInvestmentSecurityEvent(int securityId, String[] eventTypeNameList);


	/**
	 * Retrieves full history of events of the specified type for the specified security
	 * and saves them into our system.
	 * <p>
	 * Skips duplicate events and returns the number of events created.
	 *
	 * @param securityId
	 * @param eventTypeNameList
	 */
	@ModelAttribute
	@RequestMapping("investmentSecurityEventHistoryLoad")
	@SecureMethod(dtoClass = InvestmentSecurityEvent.class, permissions = SecurityPermission.PERMISSION_CREATE)
	public Integer loadInvestmentSecurityEventHistory(int securityId, String[] eventTypeNameList, boolean ignoreSeedEvents, boolean deleteUnbooked);


	/**
	 * Copies events from a source security to the destination security
	 */
	@ModelAttribute
	@SecureMethod(dtoClass = InvestmentSecurityEvent.class, permissions = SecurityPermission.PERMISSION_CREATE)
	public Integer copyInvestmentSecurityEventSchedule(InvestmentSecurityEventScheduleCopyCommand scheduleCopyCommand);
}
