package com.clifton.investment.instrument.event.payout.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventClientElection;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * <code>InvestmentSecurityEventClientElectionValidator</code> performs validation upon saving {@link InvestmentSecurityEventClientElection}s.
 *
 * @author NickK
 * @see <code>AccountingEventJournalSecurityEventClientElectionObserver</code> for further validation for booked journal details
 */
@Component
public class InvestmentSecurityEventClientElectionValidator extends SelfRegisteringDaoValidator<InvestmentSecurityEventClientElection> {

	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentSecurityEventClientElection clientElection, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setSecurityEventId(clientElection.getSecurityEvent().getId());
		payoutSearchForm.setElectionNumber(clientElection.getElectionNumber());
		List<InvestmentSecurityEventPayout> payoutList = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventPayoutList(payoutSearchForm);
		ValidationUtils.assertNotEmpty(payoutList, "The Security Event Client Election must refer to at least one Security Event Payout.");

		// if the security event is DTC Only, check that a holding account exists which has an issuer that is a DTC participant.
		for (InvestmentSecurityEventPayout payout : CollectionUtils.getIterable(payoutList)) {
			if (payout.isDtcOnly()) {
				ValidationUtils.assertTrue(holdingAccountWithDtcIssuerExists(clientElection.getClientInvestmentAccount(), payout.getSecurityEvent().getExDate()),
						"For payouts that are flagged as DTC Only, an active holding account with an issuer that is a DTC Participant is required.");
			}
		}
	}


	private boolean holdingAccountWithDtcIssuerExists(InvestmentAccount clientInvestmentAccount, Date activeOnDate) {
		// Search for at least one active relationship to a holding account that has an issuer with a DTC number
		InvestmentAccountRelationshipSearchForm relationshipSearchForm = new InvestmentAccountRelationshipSearchForm();
		relationshipSearchForm.setMainAccountId(clientInvestmentAccount.getId());
		relationshipSearchForm.setActiveOnDate(activeOnDate);
		List<InvestmentAccountRelationship> investmentAccountRelationshipList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipList(relationshipSearchForm);
		for (InvestmentAccountRelationship relationship : investmentAccountRelationshipList) {
			if (!StringUtils.isEmpty(relationship.getReferenceTwo().getIssuingCompany().getDtcNumber())
					&& relationship.getReferenceTwo().isActive()) {
				return true;
			}
		}
		return false;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventPayoutService getInvestmentSecurityEventPayoutService() {
		return this.investmentSecurityEventPayoutService;
	}


	public void setInvestmentSecurityEventPayoutService(InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService) {
		this.investmentSecurityEventPayoutService = investmentSecurityEventPayoutService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}
}
