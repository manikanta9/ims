package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentAccrualCalculator</code> interface methods for calculating Accrued Interest.
 * Each option in AccrualMethods should have its own implementation of InvestmentAccrualCalculator.
 *
 * @author vgomelsky
 */
public interface InvestmentAccrualCalculator {

	/**
	 * Calculates and returns Accrued Interest on the specified notional amount for the specified paymentEvent.
	 * The interest calculation uses industry standard "End Of Day" convention. The interest starts and end on End Of Day:
	 * the interest starts the day after paymentEvent start date and ends on accrueUpToDate (inclusive).
	 * <p/>
	 * Uses the interest rate of the period for calculation and rounds result using security's notional calculator rules.
	 *
	 * @param paymentEvent
	 * @param accrualBasis
	 * @param notionalMultiplier - this field is usually 1 (meaning no multiplier) or null which is the same.
	 *                           However, for inflation linked bonds (TIPS, etc.) it is equal to the index ratio and will multiply the quantity by this number
	 *                           unless instrument.hierarchy.notionalMultiplierForAccrualNotUsed = true (Zero Coupon Swaps) no need to multiply twice.
	 * @param accrueUpToDate
	 */
	public BigDecimal calculateAccruedInterest(InvestmentSecurityEvent paymentEvent, BigDecimal accrualBasis, BigDecimal notionalMultiplier, Date accrueUpToDate);
}
