package com.clifton.investment.instrument.calculator;


import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>DayCountConvention</code> enum defines day count conventions used to calculate bond coupon payments and accrued interest.
 * For example, THRITY_THREESIXTY, ACTUAL_THREESIXTY, ACTUAL_THREESIXTYFIVE, NOLEAP_THREESIXTYFIVE, ACTUAL_ACTUAL.
 */
public enum DayCountConventions {

	/**
	 * Uses US (30U/360) special handling for months with 31 days and for Feb.  The following rules are applied in the following order:
	 * <ul>
	 * <li>if D2 is the last day of February (28 in a non leap year; 29 in a leap year) and D1 is the last day of February, change D2 to 30 </li>
	 * <li>if D1 is the last day of February, change D1 to 30 </li>
	 * <li>if D2 is 31 and D1 is 30 or 31, change D2 to 30 </li>
	 * <li>if D1 is 31, change D1 to 30 </li>
	 * </ul>
	 */
	THRITY_THREESIXTY("30/360", false, 360) {
		@Override
		public BigDecimal calculateAccrualRate(DayCountCommand command) {
			int year1 = DateUtils.getYear(command.getAccrualStartDate());
			int year2 = DateUtils.getYear(command.getAccrueUpToDate());
			int month1 = DateUtils.getMonthOfYear(command.getAccrualStartDate());
			int month2 = DateUtils.getMonthOfYear(command.getAccrueUpToDate());
			int day1 = DateUtils.getDayOfMonth(command.getAccrualStartDate());
			int day2 = DateUtils.getDayOfMonth(command.getAccrueUpToDate());

			if (isLastDayOfFebruary(command.getAccrualStartDate()) && isLastDayOfFebruary(command.getAccrueUpToDate())) {
				day2 = 30;
			}
			if (isLastDayOfFebruary(command.getAccrualStartDate())) {
				day1 = 30;
			}
			if (day2 == 31 && (day1 == 30 || day1 == 31)) {
				day2 = 30;
			}
			if (day1 == 31) {
				day1 = 30;
			}
			return MathUtils.divide((getDaysInYear() * (year2 - year1) + 30 * (month2 - month1) + (day2 - day1)), getDaysInYear());
		}


		private boolean isLastDayOfFebruary(Date date) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			if (cal.get(Calendar.MONTH) == Calendar.FEBRUARY) {
				int maxDaysInFeb = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
				if (maxDaysInFeb == cal.get(Calendar.DAY_OF_MONTH)) {
					return true;
				}
			}
			return false;
		}
	},

	ACTUAL_THREESIXTY("Actual/360", false, 360),

	ACTUAL_THREESIXTYFIVE("Actual/365", false, 365),

	NOLEAP_THREESIXTYFIVE("NL/365", false, 365) {
		@Override
		public BigDecimal calculateAccrualRate(DayCountCommand command) {
			int year1 = DateUtils.getYear(command.getAccrualStartDate());
			int year2 = DateUtils.getYear(command.getAccrueUpToDate());

			int daysToExclude = 0;
			for (int y = year1; y <= year2; y++) {
				if (isLeapYear(y)) {
					Date lastDayOfFeb = getLastDayOfFebruary(y);
					if ((lastDayOfFeb.after(command.getAccrualStartDate()) || lastDayOfFeb.equals(command.getAccrualStartDate())) && (lastDayOfFeb.before(command.getAccrueUpToDate()) || lastDayOfFeb.equals(command.getAccrueUpToDate()))) {
						daysToExclude++;
					}
				}
			}

			int daysBetween = DateUtils.getDaysDifference(command.getAccrueUpToDate(), command.getAccrualStartDate());
			return MathUtils.divide(daysBetween - daysToExclude, getDaysInYear());
		}


		private Date getLastDayOfFebruary(int year) {
			Calendar cal = Calendar.getInstance();
			cal.set(year, Calendar.FEBRUARY, 0);

			int maxDaysInFeb = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			Calendar result = Calendar.getInstance();
			result.set(year, Calendar.FEBRUARY, maxDaysInFeb);
			return result.getTime();
		}


		private boolean isLeapYear(int year) {
			Calendar cal = Calendar.getInstance();
			cal.set(year, Calendar.FEBRUARY, 1);

			int maxDaysInFeb = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			return maxDaysInFeb == 29;
		}
	},

	ACTUAL_ACTUAL("Actual/Actual", false, null) {
		@Override
		public BigDecimal calculateAccrualRate(DayCountCommand command) {
			double daysInPeriod = DateUtils.getDaysDifference(command.getPeriodEndDate(), command.getPeriodStartDate());
			if (command.isOfFrequency(CouponFrequencies.ANNUAL)) {
				if (daysInPeriod < 365) {
					// the shortest year is 365 days: if shorter, then likely because of partial first period
					// NOTE: periodStart/EndDate(s) must always be the full period (if first period is partial then bond issue date will identify it)
					//       this code tries to recover from "incorrectly" defined first accrual period
					daysInPeriod = DateUtils.getDaysDifference(command.getPeriodEndDate(), DateUtils.addYears(command.getPeriodEndDate(), -1));
				}
				else if (daysInPeriod > 366 && daysInPeriod < 730) {
					// partial first period payment rolled into the second period: calculate weighted average of days in each year
					Date previousRegularEnd = DateUtils.addYears(command.getPeriodEndDate(), -1);
					daysInPeriod = DateUtils.getDaysDifference(previousRegularEnd, DateUtils.addYears(previousRegularEnd, -1));
					if (DateUtils.compare(command.getAccrueUpToDate(), previousRegularEnd, false) > 0) {
						// prorate first year + second year
						int nextYearDays = DateUtils.getDaysDifference(command.getPeriodEndDate(), previousRegularEnd);
						if (!MathUtils.isEqual(daysInPeriod, nextYearDays)) {
							// one leap and the other non-leap year
							double firstPeriodDays = DateUtils.getDaysDifference(previousRegularEnd, command.getPeriodStartDate());
							double nextPeriodDays = DateUtils.getDaysDifference(command.getAccrueUpToDate(), previousRegularEnd);
							daysInPeriod = daysInPeriod * firstPeriodDays / (firstPeriodDays + nextPeriodDays) + nextYearDays * nextPeriodDays / (firstPeriodDays + nextPeriodDays);
						}
					}
				}
			}
			if (MathUtils.isEqual(daysInPeriod, 0)) {
				return BigDecimal.ZERO;
			}

			int daysInAccrual = DateUtils.getDaysDifference(command.getAccrueUpToDate(), command.getAccrualStartDate());
			return MathUtils.divide(BigDecimal.valueOf(daysInAccrual), daysInPeriod * command.getFrequency());
		}
	},

	ACTUAL_ACTUAL_ISDA("Actual/Actual (ISDA)", false, null) {
		@Override
		public BigDecimal calculateAccrualRate(DayCountCommand command) {
			BigDecimal result = BigDecimal.ZERO;
			Map<Date, Date> periodDates = getStartAndEndPeriodDates(command.getAccrualStartDate(), command.getAccrueUpToDate());
			for (Map.Entry<Date, Date> entry : periodDates.entrySet()) {
				int daysInAccrual = DateUtils.getDaysDifference(entry.getValue(), entry.getKey());
				result = MathUtils.add(result, MathUtils.divide(BigDecimal.valueOf(daysInAccrual), DateUtils.getDaysInYear(entry.getKey())));
			}
			return result;
		}


		private Map<Date, Date> getStartAndEndPeriodDates(Date startDate, Date endDate) {
			Map<Date, Date> result = new HashMap<>();

			if (DateUtils.getYearsDifference(startDate, endDate) == 0) {
				result.put(startDate, endDate);
			}
			else {
				Date firstDayOfYear = DateUtils.addDays(DateUtils.getLastDayOfYear(startDate), 1);
				result.put(startDate, firstDayOfYear);
				result.putAll(getStartAndEndPeriodDates(firstDayOfYear, endDate));
			}
			return result;
		}
	},

	BUSINESS_252("BUS/252", true, 252) {
		@Override
		public BigDecimal calculateAccrualRate(DayCountCommand command) {
			int daysBetween = command.getCalendarBusinessDayService().getBusinessDaysBetween(command.getAccrualStartDate(), command.getAccrueUpToDate(), null, command.getCalendarId());

			return MathUtils.divide(daysBetween, getDaysInYear());
		}
	};

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns Accrual Rate for the specified arguments such that: Accrued Interest = Notional * Interest Rate * Accrual Rate.
	 * <b>DOES NOT INCLUDE ACCRUAL FOR THE accrualStartDate: uses industry standard End Of Day convention.</b>
	 * <p/>
	 * Period Start Date maybe before Accrual Start Date if first accrual period is partial (bond issued in the middle of accrual period)
	 * <p/>
	 * Each day count convention will use a different formula for this calculation.
	 */
	public BigDecimal calculateAccrualRate(DayCountCommand command) {
		int daysBetween = DateUtils.getDaysDifference(command.getAccrueUpToDate(), command.getAccrualStartDate());
		return MathUtils.divide(daysBetween, getDaysInYear());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private final String accrualString;
	private final boolean businessDaysDependent;
	/**
	 * The number of days in the accrual year: 360, 365, etc.  Use null for Actual.
	 */
	private final Integer daysInYear;


	DayCountConventions(String accrualString, boolean businessDaysDependent, Integer daysInYear) {
		this.accrualString = accrualString;
		this.businessDaysDependent = businessDaysDependent;
		this.daysInYear = daysInYear;
	}


	public String getAccrualString() {
		return this.accrualString;
	}


	/**
	 * DayCountConvention for the given accrualString key
	 */
	public static DayCountConventions getDayCountConventionByKey(String key) {
		for (DayCountConventions sector : DayCountConventions.values()) {
			if (sector.getAccrualString().equals(key)) {
				return sector;
			}
		}
		throw new IllegalArgumentException("Cannot find DayCountConvention for accrual type: " + key);
	}


	public boolean isBusinessDaysDependent() {
		return this.businessDaysDependent;
	}


	public int getDaysInYear() {
		if (this.daysInYear == null) {
			throw new ValidationException("'" + getAccrualString() + "'  Day Count Convention uses ACTUAL number of days in a year. It does not support getDaysInYear method.");
		}
		return this.daysInYear;
	}
}
