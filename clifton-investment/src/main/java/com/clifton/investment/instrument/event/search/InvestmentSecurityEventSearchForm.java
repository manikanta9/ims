package com.clifton.investment.instrument.event.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentSecurityEventSearchForm</code> class defines search configuration for {@link com.clifton.investment.instrument.event.InvestmentSecurityEvent} objects.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityEventSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Integer excludeId;

	@SearchField(searchField = "type.id", sortField = "type.eventOrder")
	private Short typeId;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = ComparisonConditions.EQUALS)
	private String typeName;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = ComparisonConditions.LIKE)
	private String typeNameSearch;

	@SearchField(searchField = "type.id", sortField = "type.eventOrder")
	private Short[] typeIds;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String[] typeNames;

	@SearchField(searchField = "status.id")
	private Short statusId;

	@SearchField(searchField = "name", searchFieldPath = "status", comparisonConditions = ComparisonConditions.EQUALS)
	private String statusName;

	@SearchField(searchField = "name", searchFieldPath = "status", comparisonConditions = ComparisonConditions.LIKE)
	private String statusNameSearch;

	@SearchField(searchField = "eventJournalNotApplicable", searchFieldPath = "status")
	private Boolean statusEventJournalNotApplicable;

	@SearchField(searchField = "eventJournalAllowed", searchFieldPath = "status")
	private Boolean statusEventJournalAllowed;

	@SearchField
	private Long corporateActionIdentifier;

	@SearchField(searchField = "corporateActionIdentifier")
	private Long[] corporateActionIdentifiers;

	@SearchField(searchField = "beforeSameAsAfter", searchFieldPath = "type")
	private Boolean beforeSameAsAfter;

	@SearchField(searchField = "security.id")
	private Integer securityId;

	@SearchField(searchField = "security.id")
	private Integer[] securityIds;

	// Custom filter to find events that reference this Security as a Payout or as an Additional Security
	private Integer indirectSecurityId;

	@SearchField(searchField = "instrument.id", searchFieldPath = "security")
	private Integer securityInstrumentId;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "security.instrument", sortField = "hierarchy.name")
	private Short securityInstrumentHierarchyId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "security.instrument.hierarchy", sortField = "investmentType.name")
	private Short securityInvestmentTypeId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "security.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "additionalSecurity.id")
	private Integer additionalSecurityId;

	@SearchField
	private BigDecimal beforeEventValue;

	@SearchField
	private BigDecimal afterEventValue;

	@SearchField
	private BigDecimal additionalEventValue;

	@SearchField
	private Date declareDate;

	@SearchField
	private Date exDate;

	@SearchField
	private Date recordDate;

	@SearchField
	private Date eventDate;

	@SearchField(searchField = "eventDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date beforeEventDate;

	@SearchField(searchField = "eventDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date afterEventDate;

	@SearchField
	private Date additionalDate;

	@SearchField
	private Date actualSettlementDate;

	// custom filter: (EventDate = @Date OR EventExDate = DATEADD(DAY, 1, @Date))
	private Date dayBeforeExOrEventDate;

	@SearchField(comparisonConditions = {ComparisonConditions.LIKE, ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL})
	private String eventDescription;

	@SearchField
	private Short bookingOrderOverride;

	@SearchField
	private Boolean voluntary;
	@SearchField
	private Boolean taxable;

	// Custom filter to find events where the date passed in falls within the date range of the event: inclusive on declare and excluding Ex
	private Date dateBetweenDeclareAndExDate;

	// Custom filter used by Securities that are Allocations of Securities to show all events for all securities within the custom allocation
	private Integer parentInvestmentSecurityId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getExcludeId() {
		return this.excludeId;
	}


	public void setExcludeId(Integer excludeId) {
		this.excludeId = excludeId;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Short[] getTypeIds() {
		return this.typeIds;
	}


	public void setTypeIds(Short[] typeIds) {
		this.typeIds = typeIds;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getTypeNameSearch() {
		return this.typeNameSearch;
	}


	public void setTypeNameSearch(String typeNameSearch) {
		this.typeNameSearch = typeNameSearch;
	}


	public String[] getTypeNames() {
		return this.typeNames;
	}


	public void setTypeNames(String[] typeNames) {
		this.typeNames = typeNames;
	}


	public Short getStatusId() {
		return this.statusId;
	}


	public void setStatusId(Short statusId) {
		this.statusId = statusId;
	}


	public String getStatusName() {
		return this.statusName;
	}


	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}


	public String getStatusNameSearch() {
		return this.statusNameSearch;
	}


	public void setStatusNameSearch(String statusNameSearch) {
		this.statusNameSearch = statusNameSearch;
	}


	public Long getCorporateActionIdentifier() {
		return this.corporateActionIdentifier;
	}


	public void setCorporateActionIdentifier(Long corporateActionIdentifier) {
		this.corporateActionIdentifier = corporateActionIdentifier;
	}


	public Long[] getCorporateActionIdentifiers() {
		return this.corporateActionIdentifiers;
	}


	public void setCorporateActionIdentifiers(Long[] corporateActionIdentifiers) {
		this.corporateActionIdentifiers = corporateActionIdentifiers;
	}


	public Boolean getBeforeSameAsAfter() {
		return this.beforeSameAsAfter;
	}


	public void setBeforeSameAsAfter(Boolean beforeSameAsAfter) {
		this.beforeSameAsAfter = beforeSameAsAfter;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer[] getSecurityIds() {
		return this.securityIds;
	}


	public void setSecurityIds(Integer[] securityIds) {
		this.securityIds = securityIds;
	}


	public Integer getIndirectSecurityId() {
		return this.indirectSecurityId;
	}


	public void setIndirectSecurityId(Integer indirectSecurityId) {
		this.indirectSecurityId = indirectSecurityId;
	}


	public Integer getSecurityInstrumentId() {
		return this.securityInstrumentId;
	}


	public void setSecurityInstrumentId(Integer securityInstrumentId) {
		this.securityInstrumentId = securityInstrumentId;
	}


	public Short getSecurityInstrumentHierarchyId() {
		return this.securityInstrumentHierarchyId;
	}


	public void setSecurityInstrumentHierarchyId(Short securityInstrumentHierarchyId) {
		this.securityInstrumentHierarchyId = securityInstrumentHierarchyId;
	}


	public Short getSecurityInvestmentTypeId() {
		return this.securityInvestmentTypeId;
	}


	public void setSecurityInvestmentTypeId(Short securityInvestmentTypeId) {
		this.securityInvestmentTypeId = securityInvestmentTypeId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getAdditionalSecurityId() {
		return this.additionalSecurityId;
	}


	public void setAdditionalSecurityId(Integer additionalSecurityId) {
		this.additionalSecurityId = additionalSecurityId;
	}


	public BigDecimal getBeforeEventValue() {
		return this.beforeEventValue;
	}


	public void setBeforeEventValue(BigDecimal beforeEventValue) {
		this.beforeEventValue = beforeEventValue;
	}


	public BigDecimal getAfterEventValue() {
		return this.afterEventValue;
	}


	public void setAfterEventValue(BigDecimal afterEventValue) {
		this.afterEventValue = afterEventValue;
	}


	public BigDecimal getAdditionalEventValue() {
		return this.additionalEventValue;
	}


	public void setAdditionalEventValue(BigDecimal additionalEventValue) {
		this.additionalEventValue = additionalEventValue;
	}


	public Date getDeclareDate() {
		return this.declareDate;
	}


	public void setDeclareDate(Date declareDate) {
		this.declareDate = declareDate;
	}


	public Date getExDate() {
		return this.exDate;
	}


	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}


	public Date getRecordDate() {
		return this.recordDate;
	}


	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Date getBeforeEventDate() {
		return this.beforeEventDate;
	}


	public void setBeforeEventDate(Date beforeEventDate) {
		this.beforeEventDate = beforeEventDate;
	}


	public Date getAfterEventDate() {
		return this.afterEventDate;
	}


	public void setAfterEventDate(Date afterEventDate) {
		this.afterEventDate = afterEventDate;
	}


	public Date getAdditionalDate() {
		return this.additionalDate;
	}


	public void setAdditionalDate(Date additionalDate) {
		this.additionalDate = additionalDate;
	}


	public Date getActualSettlementDate() {
		return this.actualSettlementDate;
	}


	public void setActualSettlementDate(Date actualSettlementDate) {
		this.actualSettlementDate = actualSettlementDate;
	}


	public Date getDayBeforeExOrEventDate() {
		return this.dayBeforeExOrEventDate;
	}


	public void setDayBeforeExOrEventDate(Date dayBeforeExOrEventDate) {
		this.dayBeforeExOrEventDate = dayBeforeExOrEventDate;
	}


	public String getEventDescription() {
		return this.eventDescription;
	}


	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}


	public Short getBookingOrderOverride() {
		return this.bookingOrderOverride;
	}


	public void setBookingOrderOverride(Short bookingOrderOverride) {
		this.bookingOrderOverride = bookingOrderOverride;
	}


	public Boolean getVoluntary() {
		return this.voluntary;
	}


	public void setVoluntary(Boolean voluntary) {
		this.voluntary = voluntary;
	}


	public Boolean getTaxable() {
		return this.taxable;
	}


	public void setTaxable(Boolean taxable) {
		this.taxable = taxable;
	}


	public Date getDateBetweenDeclareAndExDate() {
		return this.dateBetweenDeclareAndExDate;
	}


	public void setDateBetweenDeclareAndExDate(Date dateBetweenDeclareAndExDate) {
		this.dateBetweenDeclareAndExDate = dateBetweenDeclareAndExDate;
	}


	public Integer getParentInvestmentSecurityId() {
		return this.parentInvestmentSecurityId;
	}


	public void setParentInvestmentSecurityId(Integer parentInvestmentSecurityId) {
		this.parentInvestmentSecurityId = parentInvestmentSecurityId;
	}


	public Boolean getStatusEventJournalNotApplicable() {
		return this.statusEventJournalNotApplicable;
	}


	public void setStatusEventJournalNotApplicable(Boolean statusEventJournalNotApplicable) {
		this.statusEventJournalNotApplicable = statusEventJournalNotApplicable;
	}


	public Boolean getStatusEventJournalAllowed() {
		return this.statusEventJournalAllowed;
	}


	public void setStatusEventJournalAllowed(Boolean statusEventJournalAllowed) {
		this.statusEventJournalAllowed = statusEventJournalAllowed;
	}
}
