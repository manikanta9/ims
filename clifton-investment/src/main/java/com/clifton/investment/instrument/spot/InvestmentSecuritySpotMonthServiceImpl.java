package com.clifton.investment.instrument.spot;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.instrument.spot.calculators.InvestmentSecuritySpotMonthCalculator;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class InvestmentSecuritySpotMonthServiceImpl implements InvestmentSecuritySpotMonthService {

	private InvestmentInstrumentService investmentInstrumentService;

	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecuritySpotMonth getInvestmentSecuritySpotMonth(InvestmentSecurity security, boolean exceptionIfMissing) {
		InvestmentSecuritySpotMonthCalculator calculator = getInvestmentSecuritySpotMonthCalculatorForSecurity(security, exceptionIfMissing);
		if (calculator != null) {
			return calculator.calculateInvestmentSecuritySpotMonth(security);
		}
		return null;
	}


	@Override
	public List<InvestmentSecuritySpotMonth> getInvestmentSecuritySpotMonthList(SecuritySearchForm searchForm) {
		// Prevents accidentally trying to run for a large list of securities - limit to just one specific instrument
		ValidationUtils.assertNotNull(searchForm.getInstrumentId(), "Retrieving calculated spot month dates for a list of securities can only be performed for a specific instrument.");
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
		List<InvestmentSecuritySpotMonth> spotMonthList = new PagingArrayList<>();
		if (!CollectionUtils.isEmpty(securityList)) {
			InvestmentSecuritySpotMonthCalculator calculator = null;
			for (InvestmentSecurity security : securityList) {
				// They will all be the same, since all for the same instrument - get it for the first one and re-use the calculator
				if (calculator == null) {
					calculator = getInvestmentSecuritySpotMonthCalculatorForSecurity(security, false);
					// No Calculator, then no Spot Months Apply
					if (calculator == null) {
						return null;
					}
				}
				spotMonthList.add(calculator.calculateInvestmentSecuritySpotMonth(security));
			}
		}
		return spotMonthList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentSecuritySpotMonthCalculator getInvestmentSecuritySpotMonthCalculatorForSecurity(InvestmentSecurity security, boolean exceptionIfMissing) {
		if (security.getInstrument().getSpotMonthCalculatorBean() == null) {
			if (exceptionIfMissing) {
				throw new ValidationException("Cannot calculate spot month for security [" + security.getLabel() + "] because instrument [" + security.getInstrument().getLabel() + "] is missing a spot month calculator selection.");
			}
			return null;
		}
		return (InvestmentSecuritySpotMonthCalculator) getSystemBeanService().getBeanInstance(security.getInstrument().getSpotMonthCalculatorBean());
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
