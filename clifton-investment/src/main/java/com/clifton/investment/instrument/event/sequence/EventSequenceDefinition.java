package com.clifton.investment.instrument.event.sequence;


import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.setup.Calendar;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.sequence.date.InvestmentInstrumentEventSequenceDateCalculator;

import java.math.BigDecimal;
import java.util.Date;


public class EventSequenceDefinition {

	private InvestmentSecurityEventType type;
	private InvestmentSecurity security;

	/**
	 * Stock split: before = shares before split, after = shares after split
	 * Factor change: before = factor before change, after = factor after change
	 * Dividend: both values are the same
	 */
	private BigDecimal beforeEventValue;
	private BigDecimal afterEventValue;

	/**
	 * The start date of the sequence.  For Swaps this is the first declare date for the security.
	 */
	private Date sequenceStartDate;

	/**
	 * Indicates that sequence is being created using the first pre-existing event.
	 */
	private boolean firstSequenceEventExist;

	/**
	 * The sequence frequency used to create the event schedule.
	 */
	private CouponFrequencies sequenceFrequency;

	/**
	 * Calendar used for by the event sequence service to create a schedule to get the next event date.
	 * <p/>
	 * Populated by the usual get/set methods as well as get/set resetCalendar for swaps.
	 */
	private Calendar calendar;

	/**
	 * The business day convention for sequence.
	 */
	private BusinessDayConventions businessDayConvention;

	/**
	 * The date calculator used for the sequence.
	 */
	private InvestmentInstrumentEventSequenceDateCalculator dateCalculator;

	/**
	 * Indicated that the events should be generated from maturity backward.
	 */
	private boolean generateFromMaturityBackward = false;

	////////////////////////////////////////
	////////		Swap Fields		////////
	////////////////////////////////////////
	/**
	 * Calendar used for the payment dates.
	 */
	private Calendar settlementCalendar;

	/**
	 * Calendar used for the fixing dates.
	 */
	private Calendar fixingCalendar;

	/**
	 * The number of days to settle the swap reset payments.
	 */
	private Integer settlementCycle;

	/**
	 * The swap fixing cycle.
	 */
	private Integer fixingCycle;

	private EventSequenceResetAccrualConventions resetAccrualConvention = EventSequenceResetAccrualConventions.DEFAULT;

	private EventSequenceRecurrenceConventions valuationConvention = EventSequenceRecurrenceConventions.DEFAULT;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public Calendar getResetCalendar() {
		return getCalendar();
	}


	public void setResetCalendar(Calendar resetCalendar) {
		setCalendar(resetCalendar);
	}


	public InvestmentSecurityEventType getType() {
		return this.type;
	}


	public void setType(InvestmentSecurityEventType type) {
		this.type = type;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public BigDecimal getBeforeEventValue() {
		return this.beforeEventValue;
	}


	public void setBeforeEventValue(BigDecimal beforeEventValue) {
		this.beforeEventValue = beforeEventValue;
	}


	public BigDecimal getAfterEventValue() {
		return this.afterEventValue;
	}


	public void setAfterEventValue(BigDecimal afterEventValue) {
		this.afterEventValue = afterEventValue;
	}


	public Date getSequenceStartDate() {
		return this.sequenceStartDate;
	}


	public void setSequenceStartDate(Date sequenceStartDate) {
		this.sequenceStartDate = sequenceStartDate;
	}


	public CouponFrequencies getSequenceFrequency() {
		return this.sequenceFrequency;
	}


	public void setSequenceFrequency(CouponFrequencies sequenceFrequency) {
		this.sequenceFrequency = sequenceFrequency;
	}


	public Calendar getCalendar() {
		return this.calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public BusinessDayConventions getBusinessDayConvention() {
		return this.businessDayConvention;
	}


	public void setBusinessDayConvention(BusinessDayConventions businessDayConvention) {
		this.businessDayConvention = businessDayConvention;
	}


	public InvestmentInstrumentEventSequenceDateCalculator getDateCalculator() {
		return this.dateCalculator;
	}


	public void setDateCalculator(InvestmentInstrumentEventSequenceDateCalculator dateCalculator) {
		this.dateCalculator = dateCalculator;
	}


	public Calendar getSettlementCalendar() {
		return this.settlementCalendar;
	}


	public void setSettlementCalendar(Calendar settlementCalendar) {
		this.settlementCalendar = settlementCalendar;
	}


	public Calendar getFixingCalendar() {
		return this.fixingCalendar;
	}


	public void setFixingCalendar(Calendar fixingCalendar) {
		this.fixingCalendar = fixingCalendar;
	}


	public Integer getSettlementCycle() {
		return this.settlementCycle;
	}


	public void setSettlementCycle(Integer settlementCycle) {
		this.settlementCycle = settlementCycle;
	}


	public Integer getFixingCycle() {
		return this.fixingCycle;
	}


	public void setFixingCycle(Integer fixingCycle) {
		this.fixingCycle = fixingCycle;
	}


	public boolean isFirstSequenceEventExist() {
		return this.firstSequenceEventExist;
	}


	public void setFirstSequenceEventExist(boolean previousEventExist) {
		this.firstSequenceEventExist = previousEventExist;
	}


	public EventSequenceResetAccrualConventions getResetAccrualConvention() {
		return this.resetAccrualConvention;
	}


	public void setResetAccrualConvention(EventSequenceResetAccrualConventions resetAccrualConvention) {
		this.resetAccrualConvention = resetAccrualConvention;
	}


	public boolean isGenerateFromMaturityBackward() {
		return this.generateFromMaturityBackward;
	}


	public void setGenerateFromMaturityBackward(boolean generateFromMaturityBackward) {
		this.generateFromMaturityBackward = generateFromMaturityBackward;
	}


	public EventSequenceRecurrenceConventions getValuationConvention() {
		return this.valuationConvention;
	}


	public void setValuationConvention(EventSequenceRecurrenceConventions valuationConvention) {
		this.valuationConvention = valuationConvention;
	}
}
