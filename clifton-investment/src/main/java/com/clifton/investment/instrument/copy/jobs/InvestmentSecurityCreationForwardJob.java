package com.clifton.investment.instrument.copy.jobs;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarBusinessDayTypes;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.copy.InvestmentSecurityCopyCommand;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentType;

import java.util.Date;
import java.util.List;


/**
 * Bean that is used to check if a security exists for a given instrument on a given day.  If no security exists for the given instrument on the calculated day, a new security will be created,
 * using the most recent security for that instrument as a template.
 * <p>
 * A calendar list calculator can be provided, which will be used to calculate a list of calendars that the schedule will use to determine business days for security creation. This bean can also process multiple future occurrences, creating multiple securities for the same instrument.
 *
 * @author mitchellf
 */
public class InvestmentSecurityCreationForwardJob extends BaseInvestmentSecurityCreationJob {

	private CalendarSetupService calendarSetupService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private Integer fixingDateAdjustmentOverride;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		super.validate();

		StringBuilder errorMessage = new StringBuilder();

		for (Integer id : getInstrumentIds()) {
			InvestmentInstrument instrument = getInvestmentInstrumentService().getInvestmentInstrument(id);
			if (!InvestmentUtils.isInstrumentOfType(instrument, InvestmentType.FORWARDS)) {
				errorMessage.append("Instrument [")
						.append(instrument)
						.append("] is not of type FORWARDS");
			}
		}

		ValidationUtils.assertEquals(errorMessage.length(), 0, errorMessage.toString());
	}


	@Override
	protected InvestmentSecurity createSecurityForDate(InvestmentInstrument instrument, Date nextMaturityDate, InvestmentSecurity templateSecurity) {

		InvestmentSecurityCopyCommand copyCommand = new InvestmentSecurityCopyCommand();
		copyCommand.setSecurityToCopy(templateSecurity);
		copyCommand.setStartDate(templateSecurity.getStartDate());

		if (instrument != null && !instrument.isDeliverable()) {
			Integer fixingDateAdjustment = getFixingDateAdjustmentOverride() != null ? getFixingDateAdjustmentOverride() : calculateFixingDateAdjustment(templateSecurity, getCalendarBusinessDayService());
			Date fixingDate = getCalendarBusinessDayService().getBusinessDayFrom(nextMaturityDate, Math.abs(fixingDateAdjustment) * -1, CalendarBusinessDayTypes.BUSINESS_DAY, instrument.getSettlementCalendar());
			copyCommand.setEndDate(fixingDate);
		}
		copyCommand.setSettlementDate(nextMaturityDate);

		return getInvestmentInstrumentCopyService().saveInvestmentSecurityForwardWithCommand(copyCommand);
	}


	private static Integer calculateFixingDateAdjustment(InvestmentSecurity templateSecurity, CalendarBusinessDayService calendarBusinessDayService) {
		// by default (see MaturityDateSecurityCreationJob), the "endDate" on the security represents settlement (and therefore maturity) date.
		//   However, with non-deliverable forwards (only non-deliverable), firstDeliveryDate is used to represent settlement (maturity) date, and endDate instead represents
		//   fixing date.  If non-deliverable, the forward must be modified so that the provided endDate is used for firstDeliveryDate, and the new endDate is calculated by
		//   a fixingDateAdjustment.

		Short[] calendarIds = templateSecurity.getInstrument().getSettlementCalendar() != null
				? new Short[]{templateSecurity.getInstrument().getSettlementCalendar().getId()}
				: new Short[0];

		return calendarBusinessDayService.getBusinessDaysBetween(templateSecurity.getFirstDeliveryDate(), templateSecurity.getEndDate(), CalendarBusinessDayTypes.BUSINESS_DAY, calendarIds);
	}


	@Override
	protected boolean isSecurityPresentForDate(InvestmentInstrument instrument, Date nextMaturityDate) {
		SecuritySearchForm sf = new SecuritySearchForm();
		sf.setInstrumentId(instrument.getId());

		// if non-deliverable forward, the firstDeliveryDate field is used for maturity date
		if (!instrument.isDeliverable()) {
			sf.setFirstDeliveryDate(nextMaturityDate);
		}
		else {
			sf.setEndDate(nextMaturityDate);
		}

		List<InvestmentSecurity> securities = getInvestmentInstrumentService().getInvestmentSecurityList(sf);
		return !CollectionUtils.isEmpty(securities);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public Integer getFixingDateAdjustmentOverride() {
		return this.fixingDateAdjustmentOverride;
	}


	public void setFixingDateAdjustmentOverride(Integer fixingDateAdjustmentOverride) {
		this.fixingDateAdjustmentOverride = fixingDateAdjustmentOverride;
	}
}
