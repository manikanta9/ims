package com.clifton.investment.instrument.event.payout.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventClientElection;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventClientElectionSearchForm;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.investment.setup.InvestmentType;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * <code>InvestmentSecurityEventPayoutValidator</code> performs validation upon deleting {@link InvestmentSecurityEventPayout}.
 *
 * @author NickK
 * @see <code>AccountingEventJournalSecurityEventPayoutObserver</code> for further validation for booked journal details
 */
@Component
public class InvestmentSecurityEventPayoutValidator extends SelfRegisteringDaoValidator<InvestmentSecurityEventPayout> {

	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(InvestmentSecurityEventPayout eventPayout, DaoEventTypes config) {
		InvestmentSecurityEventPayout originalPayout = getOriginalBean(eventPayout);
		InvestmentSecurityEventPayoutSearchForm sf = new InvestmentSecurityEventPayoutSearchForm();
		sf.setSecurityEventId(eventPayout.getSecurityEvent().getId());
		sf.setDeleted(false);
		List<InvestmentSecurityEventPayout> payouts = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(sf);

		if (config.isDelete()) {
			if (!isInvestmentSecurityEventPayoutUpdateAllowedWithElectionUse(payouts, eventPayout, eventPayout.isDefaultElection())) {
				throw new UserIgnorableValidationException("The Security Event Payout cannot be deleted because it is used by at least one Security Event Client Election or is a default payout with no alternative payout.");
			}
		}
		else {
			validateInvestmentSecurityEventPayoutForUniqueness(eventPayout);
			validateInvestmentSecurityEventPayoutEventValue(eventPayout);
			if (InvestmentUtils.isSecurityOfType(eventPayout.getSecurityEvent().getSecurity(), InvestmentType.STOCKS)) {
				Date lastDeliveryDate = eventPayout.getSecurityEvent().getSecurity().getLastDeliveryDate();
				if (lastDeliveryDate != null) {
					ValidationUtils.assertFalse(DateUtils.isDateBefore(lastDeliveryDate, eventPayout.getAdditionalPayoutDate(), false), "Payout Date [" + DateUtils.fromDateShort(eventPayout.getAdditionalPayoutDate()) + "] for the Security Event Payout cannot be after the Last Payment Date [" + DateUtils.fromDateShort(lastDeliveryDate) + "] of the event security.");
				}
			}


			if (eventPayout.isDefaultElection() && payouts != null) {
				List<InvestmentSecurityEventPayout> defaultPayouts = payouts.stream().filter(InvestmentSecurityEventPayout::isDefaultElection).collect(Collectors.toList());
				if (!CollectionUtils.isEmpty(defaultPayouts)) {
					List<InvestmentSecurityEventPayout> filteredPayouts = defaultPayouts.stream().filter(p -> p.getElectionNumber() != eventPayout.getElectionNumber())
							.collect(Collectors.toList());
					StringBuilder message = new StringBuilder("The Security Event Payout cannot be ");
					message.append(originalPayout == null ? "created" : "updated");
					message.append(" because it is marked as a default election, and there are already other default elections defined for this event.");
					ValidationUtils.assertEmpty(filteredPayouts, message.toString());
				}
			}
			if (originalPayout != null) {
				List<String> differentProperties = CoreCompareUtils.getNoEqualProperties(originalPayout, eventPayout, false);
				if (!CollectionUtils.isEmpty(differentProperties)) {
					Set<String> differentPropertiesSet = new HashSet<>(differentProperties);
					if (Stream.of("securityEvent", "electionNumber", "deleted", "defaultElection").anyMatch(differentPropertiesSet::contains)) {
						ValidationUtils.assertTrue(isInvestmentSecurityEventPayoutUpdateAllowedWithElectionUse(payouts, originalPayout, eventPayout.isDefaultElection() || originalPayout.isDefaultElection()), "The Security Event Payout cannot be updated because it is used by at least one Security Event Client Election or is a default payout with no alternative payout.");
					}
				}
			}
		}
	}


	/**
	 * Validates if an election is being used by the provided {@link InvestmentSecurityEventPayout}. At least one payout must exist for each election's election number and security event.
	 * With multiple payouts applicable for an event, we can delete and/or update a payout as long as another payout will exist for the election.
	 */
	private boolean isInvestmentSecurityEventPayoutUpdateAllowedWithElectionUse(List<InvestmentSecurityEventPayout> eventPayoutList, InvestmentSecurityEventPayout eventPayout, boolean checkForDefault) {

		List<InvestmentSecurityEventPayout> remainingElectionPayoutList = CollectionUtils.getStream(eventPayoutList)
				.filter(payout -> eventPayout.getElectionNumber() == payout.getElectionNumber() && !eventPayout.equals(payout))
				.collect(Collectors.toList());

		if (checkForDefault) {
			List<InvestmentSecurityEventPayout> remainingDefaultElectionPayoutList = CollectionUtils.getStream(remainingElectionPayoutList)
					.filter(InvestmentSecurityEventPayout::isDefaultElection)
					.collect(Collectors.toList());
			if (CollectionUtils.isEmpty(remainingDefaultElectionPayoutList)) {
				return false;
			}
		}

		if (CollectionUtils.isEmpty(remainingElectionPayoutList)) {
			// Make sure no clients elected for this payout's election number
			InvestmentSecurityEventClientElectionSearchForm electionSearchForm = new InvestmentSecurityEventClientElectionSearchForm();
			electionSearchForm.setSecurityEventId(eventPayout.getSecurityEvent().getId());
			electionSearchForm.setElectionNumber(eventPayout.getElectionNumber());
			List<InvestmentSecurityEventClientElection> clientElectionList = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventClientElectionList(electionSearchForm);
			if (!CollectionUtils.isEmpty(clientElectionList)) {
				return false;
			}
		}

		return true;
	}


	private void validateInvestmentSecurityEventPayoutForUniqueness(InvestmentSecurityEventPayout eventPayout) {
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setSecurityEventId(eventPayout.getSecurityEvent().getId());
		payoutSearchForm.setPayoutNumber(eventPayout.getPayoutNumber());
		payoutSearchForm.setElectionNumber(eventPayout.getElectionNumber());
		payoutSearchForm.setDeleted(Boolean.FALSE);
		List<InvestmentSecurityEventPayout> payoutList = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventPayoutList(payoutSearchForm);
		int size = CollectionUtils.getSize(payoutList);
		if (size > 0) {
			boolean onlyPayoutIsProvidedPayout = !eventPayout.isNewBean() && size == 1 && payoutList.get(0).equals(eventPayout);
			ValidationUtils.assertTrue(onlyPayoutIsProvidedPayout, "Investment Security Event Payout already exists for Payout Number and Election Number for Security Event.");
		}
	}


	private void validateInvestmentSecurityEventPayoutEventValue(InvestmentSecurityEventPayout eventPayout) {
		InvestmentSecurityEventPayoutType payoutType = eventPayout.getPayoutType();
		if (payoutType.isBeforeSameAsAfter()) {
			eventPayout.setBeforeEventValue(eventPayout.getAfterEventValue());
		}

		// validate precision and min/max
		InvestmentSecurityEventType eventType = eventPayout.getSecurityEvent().getType();
		if (eventPayout.getBeforeEventValue() != null) {
			ValidationUtils.assertMaxDecimalPrecision(eventPayout.getBeforeEventValue(), eventType.getDecimalPrecision(), "beforeEventValue");
			if (eventType.getMaxValue() != null) {
				ValidationUtils.assertTrue(eventType.getMaxValue().compareTo(eventPayout.getBeforeEventValue()) >= 0, "Before value cannot be greater than max value = " + eventType.getMaxValue(), "beforeEventValue");
			}
			if (eventType.getMinValue() != null) {
				ValidationUtils.assertTrue(eventType.getMinValue().compareTo(eventPayout.getBeforeEventValue()) <= 0, "Before value cannot be less than min value = " + eventType.getMinValue(), "beforeEventValue");
			}
		}
		if (eventPayout.getAfterEventValue() != null) {
			ValidationUtils.assertMaxDecimalPrecision(eventPayout.getAfterEventValue(), eventType.getDecimalPrecision(), "afterEventValue");
			if (eventType.getMaxValue() != null) {
				ValidationUtils.assertTrue(eventType.getMaxValue().compareTo(eventPayout.getAfterEventValue()) >= 0, "After value cannot be greater than max value = " + eventType.getMaxValue(), "afterEventValue");
			}
			if (eventType.getMinValue() != null) {
				ValidationUtils.assertTrue(eventType.getMinValue().compareTo(eventPayout.getAfterEventValue()) <= 0, "After value cannot be less than min value = " + eventType.getMinValue(), "afterEventValue");
			}
		}

		ValidationUtils.assertFalse(eventType.isSinglePayoutOnly(), "Unable to add an Investment Security Event Payout to an Investment Security Event that is a Single Payout Only (payouts are not used)");
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventPayoutService getInvestmentSecurityEventPayoutService() {
		return this.investmentSecurityEventPayoutService;
	}


	public void setInvestmentSecurityEventPayoutService(InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService) {
		this.investmentSecurityEventPayoutService = investmentSecurityEventPayoutService;
	}
}
