package com.clifton.investment.instrument.calculator;


import com.clifton.calendar.schedule.ScheduleFrequencies;


/**
 * The <code>CouponFrequency</code> enum defines various types of coupon frequencies: how often payments are made.
 */
public enum CouponFrequencies {
	//ANNUAL(1), SEMIANNUAL(2), QUARTERLY(4), MONTHLY(12), WEEKLY(52);

	ANNUAL("Annual", 1, ScheduleFrequencies.YEARLY, 1), //
	SEMIANNUAL("SemiAnnual", 2, ScheduleFrequencies.MONTHLY, 6), //
	QUARTERLY("Quarterly", 4, ScheduleFrequencies.MONTHLY, 3), //
	MONTHLY("Monthly", 12, ScheduleFrequencies.MONTHLY, 1), //
	WEEKLY("Weekly", 52, ScheduleFrequencies.WEEKLY, 1), //
	NONE("N/A", 0, null, null), //
	BULLET("Bullet", 1, ScheduleFrequencies.ONCE, 0);

	private final int frequency;

	private String value;
	private Integer recurrence;
	private ScheduleFrequencies scheduleFrequency;


	/**
	 * Creates new CouponFrequency with the specified number of payment per year.
	 */
	CouponFrequencies(String value, int freq, ScheduleFrequencies frequency, Integer recurrence) {
		this.frequency = freq;
		this.value = value;
		this.scheduleFrequency = frequency;
		this.recurrence = recurrence;
	}


	/**
	 * Returns the number of coupon payments per year: annual = 1, quarterly = 4, etc.
	 */
	public int getFrequency() {
		return this.frequency;
	}


	public String getValue() {
		return this.value;
	}


	@Override
	public String toString() {
		return this.getValue();
	}


	public static CouponFrequencies getEnum(String value) {
		if (value != null) {
			for (CouponFrequencies v : values()) {
				if (value.equalsIgnoreCase(v.getValue())) {
					return v;
				}
			}
		}
		return null;
	}


	public Integer getRecurrence() {
		return this.recurrence;
	}


	public ScheduleFrequencies getScheduleFrequency() {
		return this.scheduleFrequency;
	}
}
