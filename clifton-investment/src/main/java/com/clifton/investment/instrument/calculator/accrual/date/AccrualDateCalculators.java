package com.clifton.investment.instrument.calculator.accrual.date;


/**
 * The <code>AccrualDateCalculator</code> enum defines available accrual calculators for the accrual date.
 * If a new calculation method is needed, the corresponding type should be added here.
 *
 * @author vgomelsky
 */
public enum AccrualDateCalculators {

	/**
	 * From Period Start To Transaction Date:
	 * Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Transaction/Valuation Date.
	 */
	DO_NOT_ADJUST(AccrualStartConventions.PERIOD_START, AccrualEndConventions.TRANSACTION_DATE, false, false),

	/**
	 * From Period Start To Transaction Date + 1:
	 * Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Transaction/Valuation Date + 1 day.
	 */
	ADD_ONE_DAY(AccrualStartConventions.PERIOD_START, AccrualEndConventions.TRANSACTION_DATE_PLUS_1, false, false),

	/**
	 * From Period Start To Transaction Date:
	 * Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Transaction/Valuation Date.
	 * Accrual is ZERO from the day before EX DATE until next period start (Cleared CDS to avoid double counting accrual and receivable).
	 */
	DO_NOT_ADJUST_NO_EX(AccrualStartConventions.PERIOD_START, AccrualEndConventions.TRANSACTION_DATE, false, true),

	/**
	 * From Period Start To Transaction Date (Settlement on Open):
	 * Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Transaction/Valuation Date.
	 * If Settlement Date of position opening is after Transaction/Valuation Date, accrue up to the Settlement Date of the open.
	 */
	SETTLEMENT_ON_OPEN(AccrualStartConventions.PERIOD_START, AccrualEndConventions.TRANSACTION_DATE, true, false),

	/**
	 * From Period Start To Default Settlement Date:
	 * Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Settlement/Valuation Date (uses instrument/hierarchy Days To Settle).
	 */
	DEFAULT_SETTLEMENT(AccrualStartConventions.PERIOD_START, AccrualEndConventions.SETTLEMENT_DATE, false, false),

	/**
	 * From Period Start To Settlement Cycle:
	 * Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Settlement/Valuation Date (uses security Settlement Cycle fields).
	 */
	SETTLEMENT_CYCLE(AccrualStartConventions.PERIOD_START, AccrualEndConventions.SETTLEMENT_CYCLE_DATE, false, false),

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * From Transaction Date + 1 To Transaction Date:
	 * Start Accrual the day after opening Transaction date and accrue up to and including Transaction/Valuation Date.
	 */
	DO_NOT_ADJUST_FROM_TRANSACTION(AccrualStartConventions.POSITION_OPEN_TRANSACTION, AccrualEndConventions.TRANSACTION_DATE, false, false),

	/**
	 * From Settlement Date + 1 To Settlement Cycle:
	 * Start Accrual the day after opening Settlement date and accrue up to and including Settlement/Valuation Date (uses Settlement Cycle Fields).
	 * <p>
	 * Use Swap Interest Leg specific accrual calculation: similar to default settlement only uses
	 * swap specific fields to determine "Settlement Cycle" and "Settlement Calendar".
	 * The same logic is also used for Interest Rate Swaps. Accrual is included for Settlement Date.
	 */
	SETTLEMENT_CYCLE_FROM_SETTLEMENT(AccrualStartConventions.POSITION_OPEN_SETTLEMENT, AccrualEndConventions.SETTLEMENT_CYCLE_DATE, false, false),

	/**
	 * From Transaction Date + 1  To Settlement Cycle:
	 * Start Accrual the day after opening Transaction date and accrue up to and including Settlement/Valuation Date (uses Settlement Cycle Fields).
	 * <p>
	 * Use Swap Interest Leg specific accrual calculation: similar to default settlement only uses
	 * swap specific fields to determine "Settlement Cycle" and "Settlement Calendar".
	 * The same logic is also used for Interest Rate Swaps. Accrual is included for Settlement Date.
	 */
	SETTLEMENT_CYCLE_FROM_TRANSACTION(AccrualStartConventions.POSITION_OPEN_TRANSACTION, AccrualEndConventions.SETTLEMENT_CYCLE_DATE, false, false);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	final AccrualStartConventions accrualStartConvention;
	final AccrualEndConventions accrualEndConvention;

	/**
	 * If Settlement Date of position opening is after Transaction/Valuation Date, accrue up to the Settlement Date of the open.
	 */
	final boolean accrueUpToOpeningSettlement;


	/**
	 * Cleared CDS with Monday payment have Ex Date on Saturday and 0 accrued interest on Friday, Saturday and Sunday.
	 * The reason for this is that one gets full receivable on Friday (the day before Ex) and no more accrual until next period.
	 */
	final boolean noAccrualFromDayBeforeEx;


	AccrualDateCalculators(AccrualStartConventions accrualStartConvention, AccrualEndConventions accrualEndConvention, boolean accrueUpToOpeningSettlement, boolean noAccrualFromDayBeforeEx) {
		this.accrualStartConvention = accrualStartConvention;
		this.accrualEndConvention = accrualEndConvention;
		this.accrueUpToOpeningSettlement = accrueUpToOpeningSettlement;
		this.noAccrualFromDayBeforeEx = noAccrualFromDayBeforeEx;
	}


	////////////////////////////////////////////////////////////////////////////


	public boolean isAccrueUpToOpeningSettlement() {
		return this.accrueUpToOpeningSettlement;
	}


	public boolean isNoAccrualFromDayBeforeEx() {
		return this.noAccrualFromDayBeforeEx;
	}


	public boolean isAccrualFromPeriodStart() {
		return AccrualStartConventions.PERIOD_START == this.accrualStartConvention;
	}


	public boolean isAccrualFromPositionOpenSettlement() {
		return AccrualStartConventions.POSITION_OPEN_SETTLEMENT == this.accrualStartConvention;
	}


	public boolean isAccrualFromPositionOpenTransaction() {
		return AccrualStartConventions.POSITION_OPEN_TRANSACTION == this.accrualStartConvention;
	}


	public boolean isAccrualEndDateNotAdjusted() {
		return AccrualEndConventions.TRANSACTION_DATE == this.accrualEndConvention;
	}

	////////////////////////////////////////////////////////////////////////////

	private enum AccrualStartConventions {
		PERIOD_START, POSITION_OPEN_SETTLEMENT, POSITION_OPEN_TRANSACTION
	}

	private enum AccrualEndConventions {
		SETTLEMENT_DATE, SETTLEMENT_CYCLE_DATE, TRANSACTION_DATE, TRANSACTION_DATE_PLUS_1
	}
}
