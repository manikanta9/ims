package com.clifton.investment.instrument;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.event.EventObject;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupRebuildCommand;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentSecurityObserver</code> adds a security maturity event on the end date of the security.
 * It also updates the security maturity event in case the end date of the security changes.
 * <p>
 * BEFORE DELETES WILL REMOVE SECURITY FROM ANY SECURITY GROUPS (THIS IS OK BECAUSE IF IT IS ACTUALLY USED OR HAS PRICES, IT WOULD FAIL ON THE FK)
 * <p>
 * AFTER INSERTS WILL SCHEDULE SECURITY GROUPS TO BE REBUILT - USES 30 SECOND DELAY AND
 * FOR BULK INSERTS RESCHEDULES THE RUNNER (IF PREVIOUSLY SCHEDULED) SO DOESN'T KEEP REBUILDING
 */
@Component
public class InvestmentSecurityObserver extends BaseDaoEventObserver<InvestmentSecurity> {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;

	private EventHandler eventHandler;
	private RunnerHandler runnerHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Delete all related {@link InvestmentSecurityGroupSecurity} and {@link InvestmentSecurityEvent} objects for this security on DELETE.
	 */
	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentSecurity> dao, DaoEventTypes event, InvestmentSecurity security) {
		if (event.isDelete()) {
			getInvestmentSecurityGroupService().deleteInvestmentSecurityGroupSecurityListForSecurity(security.getId());

			InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
			searchForm.setSecurityId(security.getId());
			for (InvestmentSecurityEvent securityEvent : CollectionUtils.getIterable(getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm))) {
				getInvestmentSecurityEventService().deleteInvestmentSecurityEvent(securityEvent.getId());
			}
		}
		else { // insert/update
			if (security.getFirstDeliveryDate() != null && security.getLastDeliveryDate() == null) {
				// if delivery dates are set, delivery is allowed only within the specified period
				// for example, currency forwards must deliver on the Settlement Date (if first date is set, set last to the same value)
				security.setLastDeliveryDate(security.getFirstDeliveryDate());
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<InvestmentSecurity> dao, DaoEventTypes event, InvestmentSecurity security, Throwable e) {
		// no need to validate if there was an exception
		if (e != null) {
			return;
		}

		// on insert of a new non-active security or update of an existing security, check if 1:1 instrument
		// requires property update for isInactive property.
		updateInstrumentIsInactiveProperty(security);

		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		InvestmentSecurityEventType maturityEventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.SECURITY_MATURITY);
		List<InvestmentSecurityEventType> typeList = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeListByHierarchy(hierarchy.getId());
		if (CollectionUtils.getSize(typeList) == 0 || !typeList.contains(maturityEventType)) {
			// don't add a maturity to a hierarchy that doesn't support it.
			return;
		}

		Date eventDate = security.getLastPositionDate();
		if (event.isInsert() && eventDate != null) {
			createNewMaturityEvent(security, maturityEventType, eventDate);
		}
		else if (event.isUpdate()) {
			InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
			searchForm.setSecurityId(security.getId());
			searchForm.setTypeId(maturityEventType.getId());
			InvestmentSecurityEvent maturityEvent = CollectionUtils.getOnlyElement(getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm));

			if (maturityEvent == null) {
				if (eventDate != null) {
					createNewMaturityEvent(security, maturityEventType, eventDate);
				}
			}
			else {
				if (eventDate == null) {
					// delete existing
					getInvestmentSecurityEventService().deleteInvestmentSecurityEvent(maturityEvent.getId());
				}
				else if (maturityEvent.getEventDate().compareTo(eventDate) != 0) { //only update if the endDate changes
					// update existing if the date changed
					int precision = maturityEvent.getType().getDecimalPrecision();
					maturityEvent.setAfterEventValue(MathUtils.round(maturityEvent.getAfterEventValue(), precision));
					maturityEvent.setBeforeEventValue(MathUtils.round(maturityEvent.getBeforeEventValue(), precision));
					maturityEvent.setEventDate(eventDate);
					maturityEvent.setDeclareDate(eventDate);
					maturityEvent.setExDate(DateUtils.addDays(eventDate, security.getInstrument().getHierarchy().isTradingOnEndDateAllowed() ? 1 : 0));
					maturityEvent.setRecordDate(eventDate);
					getInvestmentSecurityEventService().saveInvestmentSecurityEvent(maturityEvent);
				}
			}
		}
	}


	/**
	 * Schedules asynchronous rebuild of {@link InvestmentSecurityGroup} objects that might be impacted by this security addition.
	 */
	@Override
	protected void afterTransactionMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentSecurity> dao, DaoEventTypes event, @SuppressWarnings("unused") InvestmentSecurity bean, Throwable e) {
		if (e == null && event.isInsert()) {

			// notify listener that a new InvestmentSecurity has been saved
			getEventHandler().raiseEvent(EventObject.ofEventTarget(InvestmentSecurity.INVESTMENT_SECURITY_CREATED_TODAY_EVENT, bean));

			// asynchronous support
			String runId = "ALL";
			final Date scheduledDate = DateUtils.addSeconds(new Date(), 30);

			Runner runner = new AbstractStatusAwareRunner("INVESTMENT-SECURITY-GROUP", runId, scheduledDate) {

				@Override
				public void run() {
					getInvestmentSecurityGroupService().rebuildInvestmentSecurityGroupSystemManaged(InvestmentSecurityGroupRebuildCommand.forSystemManagedSecurityGroupsWithStatus(true, getStatus()));
				}
			};
			getRunnerHandler().rescheduleRunner(runner);
		}
		else if (e == null && event.isUpdate() && InvestmentUtils.isSecurityCreatedToday(bean)) {
			getEventHandler().raiseEvent(EventObject.ofEventTarget(InvestmentSecurity.NEW_INVESTMENT_SECURITY_UPDATED_TODAY_EVENT, bean));
		}
	}


	private void createNewMaturityEvent(InvestmentSecurity security, InvestmentSecurityEventType maturityEventType, Date eventDate) {
		InvestmentSecurityEvent maturityEvent = new InvestmentSecurityEvent();
		maturityEvent.setType(maturityEventType);
		maturityEvent.setStatus(maturityEvent.getType().getForceEventStatus());
		maturityEvent.setSecurity(security);
		maturityEvent.setBeforeAndAfterEventValue(BigDecimal.ZERO);
		maturityEvent.setEventDate(eventDate);
		maturityEvent.setDeclareDate(eventDate);
		maturityEvent.setExDate(DateUtils.addDays(eventDate, security.getInstrument().getHierarchy().isTradingOnEndDateAllowed() ? 1 : 0));
		maturityEvent.setRecordDate(eventDate);
		getInvestmentSecurityEventService().saveInvestmentSecurityEvent(maturityEvent);
	}


	/**
	 * Sets "inActive" property on a 1 to 1 InvestmentInstrument to true or false based on the value of the investment security's isActive() property.
	 */
	private void updateInstrumentIsInactiveProperty(InvestmentSecurity security) {
		if (security != null) {
			InvestmentInstrument instrument = security.getInstrument();
			if (instrument.getHierarchy().isOneSecurityPerInstrument() && security.isActive() == instrument.isInactive()) {
				instrument.setInactive(!security.isActive());
				// normally, for a 1:1 Instrument, we would save the security via the InvestmentInstrumentService.saveInvestmentSecurity(),
				// but doing so causes an endless feedback loop between InvestmentInstrumentService and this observer.
				getInvestmentInstrumentService().saveInvestmentInstrumentAllowOneToOne(instrument);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}

}
