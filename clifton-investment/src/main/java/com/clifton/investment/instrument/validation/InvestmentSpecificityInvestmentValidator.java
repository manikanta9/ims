package com.clifton.investment.instrument.validation;

import com.clifton.investment.specificity.BaseInvestmentSpecificityBean;


/**
 * {@link BaseInvestmentSpecificityBean} implementation class for custom investment entity validation conditions. This class provides fields for assigning
 * additional validation conditions for investment entities.
 */
public class InvestmentSpecificityInvestmentValidator extends BaseInvestmentSpecificityBean {

	private Integer validationConditionId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getValidationConditionId() {
		return this.validationConditionId;
	}


	public void setValidationConditionId(Integer validationConditionId) {
		this.validationConditionId = validationConditionId;
	}
}
