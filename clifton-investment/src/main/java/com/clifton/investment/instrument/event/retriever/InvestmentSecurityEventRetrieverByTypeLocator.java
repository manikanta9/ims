package com.clifton.investment.instrument.event.retriever;


import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>InvestmentSecurityEventRetrieverByTypeLocator</code> class is a Map based retriever by event type name.
 * It will usually be defined in Spring config with the retrieverMap property set for each supported event type.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityEventRetrieverByTypeLocator implements InvestmentSecurityEventRetrieverLocator {

	private Map<String, InvestmentSecurityEventRetriever> retrieverMap = new ConcurrentHashMap<>();


	@Override
	public InvestmentSecurityEventRetriever locate(InvestmentSecurityEventType eventType, InvestmentInstrumentHierarchy hierarchy) {
		InvestmentSecurityEventRetriever retriever = getRetrieverMap().get(eventType.getName() + ": " + hierarchy.getNameExpanded());
		if (retriever == null) {
			retriever = getRetrieverMap().get(eventType.getName());
		}
		return retriever;
	}


	public Map<String, InvestmentSecurityEventRetriever> getRetrieverMap() {
		return this.retrieverMap;
	}


	public void setRetrieverMap(Map<String, InvestmentSecurityEventRetriever> retrieverMap) {
		this.retrieverMap = retrieverMap;
	}
}
