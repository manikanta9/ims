package com.clifton.investment.instrument.event.action;

import com.clifton.core.beans.NamedEntity;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.action.processor.InvestmentSecurityEventActionProcessor;
import com.clifton.system.bean.SystemBean;


/**
 * The InvestmentSecurityEventActionType class defines what actions and in what order must be executed for a given security event type.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityEventActionType extends NamedEntity<Short> {

	private InvestmentSecurityEventType eventType;

	/**
	 * A {@link SystemBean} that implements {@link InvestmentSecurityEventActionProcessor} interface.
	 * It is responsible for performing or rolling back an action (historical adjustments to market data values, etc.).
	 */
	private SystemBean actionBean;

	/**
	 * Specifies whether this action must be processed before corresponding event journal(s) generation. If false, will process after.
	 */
	private boolean processBeforeEventJournal;
	/**
	 * If more than one action is defined for an event, use this field to define the order of execution.
	 */
	private short processingOrder;

	/**
	 * Indicates that this is a manual action type used where special processing is required.  Manual event actions are manually added to an InvestmentSecurityEvent and processed manually.
	 * Examples may be security event actions related to OTC Securities, or Price Baskets, where special processing actions need to be defined.
	 */
	private boolean manual;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventType getEventType() {
		return this.eventType;
	}


	public void setEventType(InvestmentSecurityEventType eventType) {
		this.eventType = eventType;
	}


	public SystemBean getActionBean() {
		return this.actionBean;
	}


	public void setActionBean(SystemBean actionBean) {
		this.actionBean = actionBean;
	}


	public boolean isProcessBeforeEventJournal() {
		return this.processBeforeEventJournal;
	}


	public void setProcessBeforeEventJournal(boolean processBeforeEventJournal) {
		this.processBeforeEventJournal = processBeforeEventJournal;
	}


	public short getProcessingOrder() {
		return this.processingOrder;
	}


	public void setProcessingOrder(short processingOrder) {
		this.processingOrder = processingOrder;
	}


	public boolean isManual() {
		return this.manual;
	}


	public void setManual(boolean manual) {
		this.manual = manual;
	}
}
