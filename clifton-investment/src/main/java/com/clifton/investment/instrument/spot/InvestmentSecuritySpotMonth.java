package com.clifton.investment.instrument.spot;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * The <code>InvestmentSecuritySpotMonth</code> is a virtual object that populates the spot month dates for a given security based on
 * the calculator on the instrument.
 *
 * @author manderson
 */
@NonPersistentObject
public class InvestmentSecuritySpotMonth {

	private final InvestmentSecurity investmentSecurity;

	private final Date startDate;

	private final Date endDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecuritySpotMonth(InvestmentSecurity investmentSecurity, Date startDate, Date endDate) {
		this.investmentSecurity = investmentSecurity;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSecurityInSpotMonth(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}
}
