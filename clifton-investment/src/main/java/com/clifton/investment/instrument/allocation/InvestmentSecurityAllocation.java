package com.clifton.investment.instrument.allocation;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.system.bean.SystemBean;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentSecurityAllocation</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentSecurityAllocation extends BaseEntity<Integer> {

	private InvestmentSecurity parentInvestmentSecurity;

	/**
	 * Can choose either security or instrument, if instrument is selected then a Current Security Calculator is required
	 */
	private InvestmentSecurity investmentSecurity;
	private InvestmentInstrument investmentInstrument;

	/**
	 * Used with instrument selection only to determine the current security for the instrument
	 */
	private SystemBean currentSecurityCalculatorBean;

	/**
	 * For Share Price Baskets this is the Base weight
	 */
	private BigDecimal allocationWeight;

	/**
	 * Used for Share Price Baskets only
	 */
	private BigDecimal allocationShares;

	/**
	 * Used to optionally specify an exchange for the security.  This is used for price lookups when pricing on a specific exchange is required.
	 */
	private InvestmentExchange overrideExchange;

	private Date startDate;
	private Date endDate;

	private String note;


	/////////////////////////////////////////////////////////


	/**
	 * Used by pricing calculator to determine percentage of this allocation that affects resulting price
	 * for weighted return calculations.
	 */
	public BigDecimal getAbsAllocationWeight() {
		return MathUtils.abs(getAllocationWeight());
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	public String getAllocationLabel() {
		if (getInvestmentSecurity() != null) {
			return "Security: " + getInvestmentSecurity().getLabel();
		}
		if (getInvestmentInstrument() != null) {
			return "Instrument: " + getInvestmentInstrument().getLabel();
		}
		return null;
	}


	public String getAllocationLabelWithDates() {
		return getAllocationLabel() + " " + DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true);
	}


	public String getAllocationLabelExpanded() {
		if (getCurrentSecurityCalculatorBean() != null) {
			return getAllocationLabel() + " [" + getCurrentSecurityCalculatorBean().getName() + "]";
		}
		return getAllocationLabel();
	}


	/**
	 * Convenience method to easily determine if there is an Override Exchange set on this object.
	 */
	public boolean isExchangeOverridden() {
		return getOverrideExchange() != null;
	}


	public InvestmentExchange getExchange() {
		if (getOverrideExchange() != null) {
			return getOverrideExchange();
		}
		if (getInvestmentSecurity() != null) {
			return getInvestmentSecurity().getInstrument().getExchange();
		}
		else if (getInvestmentInstrument() != null) {
			return getInvestmentInstrument().getExchange();
		}
		return null;
	}


	public InvestmentSecurity getTradingCurrency() {
		if (getInvestmentSecurity() != null) {
			return getInvestmentSecurity().getInstrument().getTradingCurrency();
		}
		else if (getInvestmentInstrument() != null) {
			return getInvestmentInstrument().getTradingCurrency();
		}
		return null;
	}


	public InvestmentInstrumentHierarchy getHierarchy() {
		if (getInvestmentSecurity() != null) {
			return getInvestmentSecurity().getInstrument().getHierarchy();
		}
		else if (getInvestmentInstrument() != null) {
			return getInvestmentInstrument().getHierarchy();
		}
		return null;
	}

	/////////////////////////////////////////////////////////


	public BigDecimal getAllocationWeight() {
		return this.allocationWeight;
	}


	public void setAllocationWeight(BigDecimal allocationWeight) {
		this.allocationWeight = allocationWeight;
	}


	public InvestmentSecurity getParentInvestmentSecurity() {
		return this.parentInvestmentSecurity;
	}


	public void setParentInvestmentSecurity(InvestmentSecurity parentInvestmentSecurity) {
		this.parentInvestmentSecurity = parentInvestmentSecurity;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public InvestmentInstrument getInvestmentInstrument() {
		return this.investmentInstrument;
	}


	public void setInvestmentInstrument(InvestmentInstrument investmentInstrument) {
		this.investmentInstrument = investmentInstrument;
	}


	public SystemBean getCurrentSecurityCalculatorBean() {
		return this.currentSecurityCalculatorBean;
	}


	public void setCurrentSecurityCalculatorBean(SystemBean currentSecurityCalculatorBean) {
		this.currentSecurityCalculatorBean = currentSecurityCalculatorBean;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public BigDecimal getAllocationShares() {
		return this.allocationShares;
	}


	public void setAllocationShares(BigDecimal allocationShares) {
		this.allocationShares = allocationShares;
	}


	public InvestmentExchange getOverrideExchange() {
		return this.overrideExchange;
	}


	public void setOverrideExchange(InvestmentExchange overrideExchange) {
		this.overrideExchange = overrideExchange;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}
}
