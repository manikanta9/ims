package com.clifton.investment.instrument.event.payout.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class InvestmentSecurityEventPayoutTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description,typeCode")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String description;

	@SearchField
	private String typeCode;

	@SearchField
	private Boolean beforeSameAsAfter;

	@SearchField
	private Boolean additionalSecurityCurrency;

	@SearchField
	private Boolean eventValueInEventCurrencyUnits;

	@SearchField
	private Boolean newSecurityCreated;

	@SearchField
	private Boolean newPositionCreated;

	@SearchField(searchField = "eventTypeList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short eventTypeId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getTypeCode() {
		return this.typeCode;
	}


	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}


	public Boolean getBeforeSameAsAfter() {
		return this.beforeSameAsAfter;
	}


	public void setBeforeSameAsAfter(Boolean beforeSameAsAfter) {
		this.beforeSameAsAfter = beforeSameAsAfter;
	}


	public Boolean getAdditionalSecurityCurrency() {
		return this.additionalSecurityCurrency;
	}


	public void setAdditionalSecurityCurrency(Boolean additionalSecurityCurrency) {
		this.additionalSecurityCurrency = additionalSecurityCurrency;
	}


	public Boolean getEventValueInEventCurrencyUnits() {
		return this.eventValueInEventCurrencyUnits;
	}


	public void setEventValueInEventCurrencyUnits(Boolean eventValueInEventCurrencyUnits) {
		this.eventValueInEventCurrencyUnits = eventValueInEventCurrencyUnits;
	}


	public Boolean getNewSecurityCreated() {
		return this.newSecurityCreated;
	}


	public void setNewSecurityCreated(Boolean newSecurityCreated) {
		this.newSecurityCreated = newSecurityCreated;
	}


	public Boolean getNewPositionCreated() {
		return this.newPositionCreated;
	}


	public void setNewPositionCreated(Boolean newPositionCreated) {
		this.newPositionCreated = newPositionCreated;
	}


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}
}
