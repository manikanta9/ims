package com.clifton.investment.instrument.event;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType.EventBeforeStartTypes;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentSecurityEventObserver</code> observes changes to events and also additions to securities
 * <p>
 * When a {@link InvestmentSecurity} is added in a hierarchy that supports same events for all securities in the instrument
 * copy all events to the security NOTE: Only Copy Events for Security Inserts, Not Updates
 * <p>
 * When a {@link InvestmentSecurityEvent} is added/updated/removed for a security that supports same events for all securities in the instrument
 * the same change is applied to the matching events for all securities for that instrument with the following exceptions:
 * If event is before security start will copy if event type allows events before start - if only one allowed and before start will copy only if after the one that is there.
 * Deletes will only be copied to all if not before start date
 *
 * @author manderson
 * @see <code>AccountingEventJournalSecurityEventObserver</code> for further validation for booked journal details
 */
@Component
public class InvestmentSecurityEventObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private static final String COPY_EVENT_CHANGES_KEY = "copyEvents";


	@SuppressWarnings("unchecked")
	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
		if (bean instanceof InvestmentSecurityEvent) {
			// Do Not Run Validation Again if Copying Events from another security for the same instrument
			if (!Boolean.FALSE.equals(getDaoEventContext().getBeanAttribute(bean, COPY_EVENT_CHANGES_KEY))) {
				validateSecurityEvent((InvestmentSecurityEvent) bean, event, (ReadOnlyDAO<InvestmentSecurityEvent>) dao);
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			if (event.isInsert()) {
				// need to update DaoEventContext keys because the bean now has id
				getDaoEventContext().updateFromNewTo(bean);
			}
			if (bean instanceof InvestmentSecurity) {
				// Only Copy Events for Security Inserts, Not Updates
				if (event.isInsert()) {
					InvestmentSecurity sec = (InvestmentSecurity) bean;
					if (isEventsSameForSecurity(sec)) {
						// NOTE: PULLS ALL EVENTS FOR ALL SECURITIES IN THAT INSTRUMENT
						// This ensures we can copy the latest "before" event if ONE allowed
						// and get all events for those the end last
						copyEventsToNewSecurity(sec);
					}
				}
			}
			else if (bean instanceof InvestmentSecurityEvent) {
				InvestmentSecurityEvent secEvent = (InvestmentSecurityEvent) bean;
				// In order to skip, must explicitly be set to false.  Allows when applying same change to other
				// security events, not to attempt to re-apply that change again
				if (!Boolean.FALSE.equals(getDaoEventContext().getBeanAttribute(bean, COPY_EVENT_CHANGES_KEY))) {
					copyEventChangeToAllSecurities(event, (event.isUpdate()) ? (InvestmentSecurityEvent) getOriginalBean(dao, bean) : null, secEvent);
				}
			}
			// Remove Copy Events DAO Attribute
			getDaoEventContext().removeBeanAttribute(bean, COPY_EVENT_CHANGES_KEY);
		}
	}


	private void validateSecurityEvent(InvestmentSecurityEvent securityEvent, DaoEventTypes config, ReadOnlyDAO<InvestmentSecurityEvent> dao) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			validateEventDate(securityEvent, dao);

			// 1. make sure event type is allowed for security's hierarchy
			InvestmentInstrumentHierarchy hierarchy = securityEvent.getSecurity().getInstrument().getHierarchy();
			InvestmentSecurityEventType eventType = securityEvent.getType();
			List<InvestmentSecurityEventType> typeList = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeListByHierarchy(hierarchy.getId());
			if (CollectionUtils.getSize(typeList) == 0 || !typeList.contains(eventType)) {
				throw new FieldValidationException("Security hierarchy '" + hierarchy.getNameExpanded() + "' does not allow selected event type '" + eventType.getName() + "'", "type");
			}

			// 2. validate/default event status
			if (eventType.getForceEventStatus() != null) {
				if (securityEvent.getStatus() == null) {
					securityEvent.setStatus(eventType.getForceEventStatus());
				}
				else {
					ValidationUtils.assertEquals(eventType.getForceEventStatus(), securityEvent.getStatus(), "Security event status cannot be different from event's type 'Force Event Status': " + securityEvent);
				}
			}
			ValidationUtils.assertNotNull(securityEvent.getStatus(), "Event Status is required.", "status");

			// 3. validate additional security
			if (eventType.isNewSecurityCreated()) {
				if (securityEvent.getStatus().isEventJournalAutomatic()) {
					ValidationUtils.assertNotNull(securityEvent.getAdditionalSecurity(), "Additional Security is required for event type '" + eventType.getName() + "'", "additionalSecurity");
				}
			}
			else if (eventType.isAdditionalSecurityAllowed()) {
				if (eventType.isAdditionalSecurityCurrency() && securityEvent.getAdditionalSecurity() != null) {
					ValidationUtils.assertTrue(securityEvent.getAdditionalSecurity().isCurrency(),
							"Additional security '" + securityEvent.getAdditionalSecurity().getLabel() + "' must be a currency for '" + eventType.getName() + "' event type.");
				}
			}
			else {
				ValidationUtils.assertNull(securityEvent.getAdditionalSecurity(), "Additional Security is not allowed for event type '" + eventType.getName() + "'", "additionalSecurity");
			}

			// 4. validate values
			if (eventType.isBeforeSameAsAfter()) {
				securityEvent.setBeforeEventValue(securityEvent.getAfterEventValue());
			}

			if (securityEvent.getStatus().isEventJournalAutomatic()) {
				securityEvent.validateRequiredFields();
			}

			// validate precision and min/max
			if (securityEvent.getBeforeEventValue() != null) {
				ValidationUtils.assertMaxDecimalPrecision(securityEvent.getBeforeEventValue(), eventType.getDecimalPrecision(), "beforeEventValue");
				if (eventType.getMaxValue() != null) {
					ValidationUtils.assertTrue(eventType.getMaxValue().compareTo(securityEvent.getBeforeEventValue()) >= 0, "Before value cannot be greater than max value = " + eventType.getMaxValue(), "beforeEventValue");
				}
				if (eventType.getMinValue() != null) {
					ValidationUtils.assertTrue(eventType.getMinValue().compareTo(securityEvent.getBeforeEventValue()) <= 0, "Before value cannot be less than min value = " + eventType.getMinValue(), "beforeEventValue");
				}
			}
			if (securityEvent.getAfterEventValue() != null) {
				ValidationUtils.assertMaxDecimalPrecision(securityEvent.getAfterEventValue(), eventType.getDecimalPrecision(), "afterEventValue");
				if (eventType.getMaxValue() != null) {
					ValidationUtils.assertTrue(eventType.getMaxValue().compareTo(securityEvent.getAfterEventValue()) >= 0, "After value cannot be greater than max value = " + eventType.getMaxValue(), "afterEventValue");
				}
				if (eventType.getMinValue() != null) {
					ValidationUtils.assertTrue(eventType.getMinValue().compareTo(securityEvent.getAfterEventValue()) <= 0, "After value cannot be less than min value = " + eventType.getMinValue(), "afterEventValue");
				}
			}

			// 5. validate global uniqueness of Corporate Action Identifier
			if (securityEvent.getCorporateActionIdentifier() != null) {
				List<InvestmentSecurityEvent> list = dao.findByField("corporateActionIdentifier", securityEvent.getCorporateActionIdentifier());
				if (CollectionUtils.getSize(list) > 1) {
					throw new FieldValidationException("Cannot have more than 1 event with the same Corporate Action Identifier = " + securityEvent.getCorporateActionIdentifier(), "corporateActionIdentifier");
				}
				else if (CollectionUtils.getSize(list) == 1) {
					InvestmentSecurityEvent existingEvent = list.get(0);
					if (!existingEvent.getId().equals(securityEvent.getId())) {
						throw new FieldValidationException("Cannot have more than 1 event with the same Corporate Action Identifier = " + securityEvent.getCorporateActionIdentifier(), "corporateActionIdentifier");
					}
				}
			}
		}
	}


	private void validateEventDate(InvestmentSecurityEvent securityEvent, ReadOnlyDAO<InvestmentSecurityEvent> dao) {
		// Validate Event Date Not After Security Maturity
		if (securityEvent.getEventDate() != null && securityEvent.getSecurity().getLastDeliveryOrEndDate() != null) {
			if (DateUtils.isDateBefore(securityEvent.getSecurity().getLastDeliveryOrEndDate(), securityEvent.getEventDate(), false)) {
				throw new FieldValidationException("Event Date [" + DateUtils.fromDateShort(securityEvent.getEventDate()) + "] is invalid because it is after Security [" + securityEvent.getSecurity().getSymbol()
						+ "] last delivery/maturity [" + DateUtils.fromDateShort(securityEvent.getSecurity().getLastDeliveryOrEndDate()) + "]", "eventDate");
			}
		}
		// Validate Actual Settlement is After or Equal to the Event Date
		if (securityEvent.getEventDate() != null && securityEvent.getSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
			if (DateUtils.isDateAfter(securityEvent.getEventDate(), securityEvent.getActualSettlementDate())) {
				throw new FieldValidationException("Actual Settlement Date [" + DateUtils.fromDateShort(securityEvent.getActualSettlementDate()) + "] is invalid because it is before Event Date [" + DateUtils.fromDateShort(securityEvent.getEventDate())
						+ "]", "actualSettlementDate");
			}
		}
		// Validate Ex Date Not After Security End Date
		if (InvestmentUtils.isSecurityOfType(securityEvent.getSecurity(), InvestmentType.STOCKS)) {
			if (securityEvent.getDayBeforeExDate() != null && securityEvent.getSecurity().getEndDate() != null) {
				if (DateUtils.isDateBefore(securityEvent.getSecurity().getEndDate(), securityEvent.getDayBeforeExDate(), false)) {
					throw new FieldValidationException("Ex Date [" + DateUtils.fromDateShort(securityEvent.getDayBeforeExDate()) + "] is invalid because it is after Security [" + securityEvent.getSecurity().getSymbol()
							+ "] End Date [" + DateUtils.fromDateShort(securityEvent.getSecurity().getEndDate()) + "]", "exDate");
				}
			}
		}

		// Validate Event Date Not Before Security Start (Unless Type Allows it)
		// Not Allowed
		InvestmentSecurityEventType eventType = securityEvent.getType();
		if (securityEvent.getEventDate() != null && securityEvent.getSecurity().getStartDate() != null) {
			if (DateUtils.compare(securityEvent.getEventDate(), securityEvent.getSecurity().getStartDate(), false) < 0) {
				if (EventBeforeStartTypes.NONE == eventType.getEventBeforeStartType()) {
					throw new FieldValidationException("Event Date [" + DateUtils.fromDateShort(securityEvent.getEventDate()) + "] is invalid because it is before Security [" + securityEvent.getSecurity().getSymbol()
							+ "] start date [" + DateUtils.fromDateShort(securityEvent.getSecurity().getStartDate()) + "] and event type [" + eventType.getName()
							+ "] does not allow events before the security start date.", "eventDate");
				}
				// Only One Allowed
				else if (EventBeforeStartTypes.ONE == eventType.getEventBeforeStartType()) {
					List<InvestmentSecurityEvent> existingList = dao.findByFields(new String[]{"type.id", "security.id"}, new Object[]{securityEvent.getType().getId(), securityEvent.getSecurity().getId()});
					for (InvestmentSecurityEvent ev : CollectionUtils.getIterable(existingList)) {
						if (ev.getEventDate() != null && DateUtils.compare(ev.getEventDate(), securityEvent.getSecurity().getStartDate(), false) < 0) {
							if (securityEvent.isNewBean() || !ev.equals(securityEvent)) {
								throw new FieldValidationException("There already exists a [" + eventType.getName() + "] event on [" + DateUtils.fromDateShort(ev.getEventDate())
										+ "] which is before the Security [" + securityEvent.getSecurity().getSymbol() + "] start date [" + DateUtils.fromDateShort(securityEvent.getSecurity().getStartDate())
										+ "]. Event type [" + eventType.getName() + "] allows only one event before the security start date.", "eventDate");
							}
						}
					}
				}
				// Many Allowed - No Additional Validation
			}
		}
	}


	private boolean isEventsSameForSecurity(InvestmentSecurity security) {
		if (security != null && security.getInstrument() != null && security.getInstrument().getHierarchy() != null) {
			return security.getInstrument().getHierarchy().isEventSameForInstrumentSecurities();
		}
		return false;
	}


	private List<InvestmentSecurityEvent> getInvestmentSecurityEventListForCopy(InvestmentSecurityEvent originalBean, InvestmentSecurityEvent bean) {
		InvestmentSecurityEvent event = (originalBean == null) ? bean : originalBean;
		List<InvestmentSecurityEvent> eventList = new ArrayList<>();
		if (isEventsSameForSecurity(event.getSecurity())) {
			eventList.addAll(getInvestmentSecurityEventService().getInvestmentSecurityEventListSameForInstrument(event));
		}

		eventList.addAll(getInvestmentSecurityEventService().getInvestmentSecurityEventListSameForInstrumentUnderlying(event));

		return eventList;
	}


	private List<InvestmentSecurity> getInvestmentSecurityListForEventCopy(InvestmentSecurityEvent securityEvent) {
		List<InvestmentSecurity> securityList = new ArrayList<>();
		if (isEventsSameForSecurity(securityEvent.getSecurity())) {
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setInstrumentId(securityEvent.getSecurity().getInstrument().getId());
			securityList.addAll(getInvestmentInstrumentService().getInvestmentSecurityList(searchForm));
		}

		//Find all securities whose instruments hierarchy supports an event of this type who has copy from underlying checked
		//  and whose instruments underlying is this security

		//1) Find all hierarchies who support an event of this type who has copy from underlying checked
		List<InvestmentSecurityEventTypeHierarchy> eventTypeHierarchyList = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeHierarchyListByEventType(securityEvent.getType().getId());
		eventTypeHierarchyList = BeanUtils.filter(eventTypeHierarchyList, InvestmentSecurityEventTypeHierarchy::isCopiedFromUnderlying);
		//2)For each hierarchy get a list of securities whose instruments underlying is the same as this events security
		for (InvestmentSecurityEventTypeHierarchy eventTypeHierarchy : CollectionUtils.getIterable(eventTypeHierarchyList)) {
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setUnderlyingInstrumentId(securityEvent.getSecurity().getInstrument().getId());
			searchForm.setHierarchyId(eventTypeHierarchy.getReferenceTwo().getId());
			securityList.addAll(getInvestmentInstrumentService().getInvestmentSecurityList(searchForm));
		}

		return securityList;
	}


	private void copyEventChangeToAllSecurities(DaoEventTypes daoEvent, InvestmentSecurityEvent originalSecurityEvent, InvestmentSecurityEvent securityEvent) {
		if (daoEvent.isDelete() || daoEvent.isUpdate()) {
			List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventListForCopy(originalSecurityEvent, securityEvent);
			if (!CollectionUtils.isEmpty(eventList)) {
				// Special Delete Handling

				// When Event Date is Before Security Start Date then just delete this event
				if (daoEvent.isDelete() && (securityEvent.getSecurity().getStartDate() != null && DateUtils.compare(securityEvent.getEventDate(), securityEvent.getSecurity().getStartDate(), false) < 0)) {
					// Do nothing else
					return;
				}

				// When Event Date is After Security End Date then only Delete on others if they also mature before event date
				boolean deleteAfterMaturity = false;
				if (daoEvent.isDelete() && (securityEvent.getSecurity().getEndDate() != null && DateUtils.compare(securityEvent.getEventDate(), securityEvent.getSecurity().getEndDate(), false) > 0)) {
					deleteAfterMaturity = true;
				}
				for (InvestmentSecurityEvent event : eventList) {
					//  Apply changes to Security if its not deleteAfterMaturity or if the event date is after the Security maturity date (EndDate)
					if (!deleteAfterMaturity || (event.getSecurity().getEndDate() != null && DateUtils.compare(securityEvent.getEventDate(), event.getSecurity().getEndDate(), false) > 0)) {
						applyEventChangeToSecurity((daoEvent.isUpdate() ? securityEvent : null), event, event.getSecurity());
					}
				}
			}
		}
		// Inserts
		else {
			List<InvestmentSecurity> securityList = getInvestmentSecurityListForEventCopy(securityEvent);
			if (CollectionUtils.isEmpty(securityList)) {
				return;
			}
			List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventListForCopy(originalSecurityEvent, securityEvent);
			for (InvestmentSecurity sec : CollectionUtils.getIterable(securityList)) {
				//Ensure we don't duplicate event on the same security and also check to make sure this event doesn't already exist on the security
				if (!sec.equals(securityEvent.getSecurity()) && CollectionUtils.isEmpty(BeanUtils.filter(eventList, InvestmentSecurityEvent::getSecurity, sec))) {
					applyEventChangeToSecurity(securityEvent, null, sec);
				}
			}
		}
	}


	private void copyEventsToNewSecurity(InvestmentSecurity toSecurity) {
		InvestmentSecurityEventSearchForm eventSearchForm = new InvestmentSecurityEventSearchForm();
		eventSearchForm.setSecurityInstrumentId(toSecurity.getInstrument().getId());
		eventSearchForm.setOrderBy("eventDate:desc");

		List<String> checkedEvents = new ArrayList<>();
		// Since we are sorting in Event Date desc order, if the event type allows only one event before security start
		// once we find the first one we can skip the older ones of that type
		List<Short> eventTypesBeforeStart = new ArrayList<>();

		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(eventSearchForm);
		if (!CollectionUtils.isEmpty(eventList)) {

			for (InvestmentSecurityEvent event : eventList) {
				if (checkedEvents.contains(getInstrumentEventUniqueKey(event))) {
					continue;
				}
				checkedEvents.add(getInstrumentEventUniqueKey(event));
				// If between start and end - Add it
				if (DateUtils.isDateBetween(event.getEventDate(), toSecurity.getStartDate(), toSecurity.getEndDate(), false)) {
					applyEventChangeToSecurity(event, null, toSecurity);
				}
				// Otherwise if before start and a before event is allowed and it's not in the ONE allowed check list
				else if (event.getType().isBeforeEventAllowed() && DateUtils.compare(event.getEventDate(), toSecurity.getStartDate(), false) < 0
						&& !eventTypesBeforeStart.contains(event.getType().getId())) {
					applyEventChangeToSecurity(event, null, toSecurity);
					if (EventBeforeStartTypes.ONE == event.getType().getEventBeforeStartType()) {
						eventTypesBeforeStart.add(event.getType().getId());
					}
				}
			}
		}
	}


	private String getInstrumentEventUniqueKey(InvestmentSecurityEvent event) {
		return event.getType().getId() + "_" + DateUtils.fromDateShort(event.getEventDate()) + "_" + event.getEventDescription();
	}


	/**
	 * Validates that the event date applies to the security, if so, copies the event, sets the new security and sets bean attribute in context to skip using this event to copy again
	 * Calls the Save/Delete method immediately so the context has the bean attribute for each bean (applies to inserts)
	 */
	@SuppressWarnings("unchecked")
	private void applyEventChangeToSecurity(InvestmentSecurityEvent copyEvent, InvestmentSecurityEvent existingEvent, InvestmentSecurity toSecurity) {
		// DELETE
		if (existingEvent != null && copyEvent == null) {
			getDaoEventContext().setBeanAttribute((T) existingEvent, COPY_EVENT_CHANGES_KEY, Boolean.FALSE);
			getInvestmentSecurityEventService().deleteInvestmentSecurityEvent(existingEvent.getId());
		}
		// INSERT OR UPDATE
		if (copyEvent != null) {
			boolean keepEvent = false;
			// As long as event date is within start/end date - ALWAYS COPY
			if (DateUtils.isDateBetween(copyEvent.getEventDate(), toSecurity.getStartDate(), toSecurity.getEndDate(), false)) {
				keepEvent = true;
			}
			// Otherwise if before start - verify if should copy it
			else if (copyEvent.getType().isBeforeEventAllowed() && DateUtils.compare(copyEvent.getEventDate(), toSecurity.getStartDate(), false) < 0) {
				// If it already exists - keep it
				if (existingEvent != null) {
					keepEvent = true;
				}
				else if (EventBeforeStartTypes.ONE == copyEvent.getType().getEventBeforeStartType()) {
					// See if there is already one before start date on the security
					InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
					searchForm.setSecurityId(toSecurity.getId());
					searchForm.setTypeId(copyEvent.getType().getId());
					searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, copyEvent.getEventDate()));
					searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.LESS_THAN, toSecurity.getStartDate()));
					if (CollectionUtils.isEmpty(getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm))) {
						keepEvent = true;
					}
				}
				else {
					// Many allowed - copy it
					keepEvent = true;
				}
			}
			// else No Longer applies - delete it

			if (keepEvent) {
				InvestmentSecurityEvent newEvent = BeanUtils.cloneBean(copyEvent, false, false);
				newEvent.setSecurity(toSecurity);
				if (existingEvent != null) {
					newEvent.setId(existingEvent.getId());
				}

				// Validation on save of the event - Need to round so that scale matches what is excepted
				// Not sure if there is a better way to handle this?  i.e. 5.0000000000 would fail for Interest Leg Payment which excepts a scale of 8
				newEvent.setBeforeEventValue(MathUtils.round(copyEvent.getBeforeEventValue(), copyEvent.getType().getDecimalPrecision()));
				newEvent.setAfterEventValue(MathUtils.round(copyEvent.getAfterEventValue(), copyEvent.getType().getDecimalPrecision()));

				getDaoEventContext().setBeanAttribute((T) newEvent, COPY_EVENT_CHANGES_KEY, Boolean.FALSE);
				getInvestmentSecurityEventService().saveInvestmentSecurityEvent(newEvent);
			}
			else if (existingEvent != null) {
				getDaoEventContext().setBeanAttribute((T) existingEvent, COPY_EVENT_CHANGES_KEY, Boolean.FALSE);
				getInvestmentSecurityEventService().deleteInvestmentSecurityEvent(existingEvent.getId());
			}
		}
	}


	////////////////////////////////////////////////////////
	//////////       Getter & Setter Methods       /////////
	////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}
}
