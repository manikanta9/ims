package com.clifton.investment.instrument.calculator.accrual.date;


import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * The <code>InvestmentAccrualDateCalculator</code> interface must be implemented by specific accrual date calculators.
 * See AccrualDateCalculator for available types.
 *
 * @author vgomelsky
 */
public interface InvestmentAccrualDateCalculator {

	/**
	 * Calculates and returns "Accrual" Date for the specified security and transaction date.
	 * <p/>
	 * To get accurate market value, it's best to use the Settlement Date as opposed to Transaction Date
	 * when calculating accrual: if position were closed today, accrual would still be accounted up to
	 * Settlement Date. Default days to settle applies in most instances so using this calculation with
	 * corresponding calendar is the best approach in most cases.
	 * <p/>
	 * Unfortunately, most banks do not use this approach: they use Transaction Date instead.
	 * In order to simplify reconciliation and reporting, we use transaction instead of settlement date
	 * for most bonds.  Total Return Swaps use more accurate Settlement date based approach.
	 *
	 * @param security
	 * @param transactionDate
	 */
	public Date calculateAccrualDate(InvestmentSecurity security, Date transactionDate);
}
