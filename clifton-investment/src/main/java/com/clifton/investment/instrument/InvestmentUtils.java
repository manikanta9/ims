package com.clifton.investment.instrument;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.function.Function;


/**
 * The <code>InvestmentUtils</code> class defines commonly used investment utility methods.
 *
 * @author vgomelsky
 */
public class InvestmentUtils {

	private static final int OCC_SYMBOL_STRIKE_DOLLAR_LENGTH = 5;
	private static final int OCC_SYMBOL_STRIKE_DECIMAL_LENGTH = 3;

	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the Name of the Investment Type of the specified security
	 */
	public static String getInvestmentTypeNameForSecurity(InvestmentSecurity security) {
		if (security != null && security.getInstrument() != null && security.getInstrument().getHierarchy() != null && security.getInstrument().getHierarchy().getInvestmentType() != null) {
			return security.getInstrument().getHierarchy().getInvestmentType().getName();
		}
		return null;
	}


	/**
	 * Returns true if the Investment Type of the specified security (instrument's hierarchy field" is the same as the argument type.
	 * <p>
	 * Return false if type field is not set of the type is different.
	 */
	public static boolean isSecurityOfType(InvestmentSecurity security, String investmentTypeName) {
		if (security.getInstrument() != null && security.getInstrument().getHierarchy() != null && security.getInstrument().getHierarchy().getInvestmentType() != null && investmentTypeName != null) {
			return investmentTypeName.equals(security.getInstrument().getHierarchy().getInvestmentType().getName());
		}
		return false;
	}


	/**
	 * Returns the Investment Type Sub Type of the specified security
	 */
	public static InvestmentTypeSubType getSecuritySubType(InvestmentSecurity security) {
		if (security.getInstrument() != null && security.getInstrument().getHierarchy() != null) {
			return security.getInstrument().getHierarchy().getInvestmentTypeSubType();
		}
		return null;
	}


	/**
	 * Returns true if the Investment Type Sub Type of the specified security matches by name
	 * <p>
	 * Return false if type Sub Type field is not set or the type Sub Type is different.
	 */
	public static boolean isSecurityOfTypeSubType(InvestmentSecurity security, String investmentTypeSubTypeName) {
		if (investmentTypeSubTypeName != null) {
			InvestmentTypeSubType subType = getSecuritySubType(security);
			if (subType != null) {
				return StringUtils.isEqual(investmentTypeSubTypeName, subType.getName());
			}
		}
		return false;
	}


	/**
	 * Returns true if the Investment Type Sub Type of the specified security matches by id
	 * <p>
	 * Return false if type Sub Type field is not set or the type Sub Type is different.
	 */
	public static boolean isSecurityOfTypeSubType(InvestmentSecurity security, short investmentTypeSubTypeId) {
		InvestmentTypeSubType subType = getSecuritySubType(security);
		if (subType != null) {
			return MathUtils.isEqual(investmentTypeSubTypeId, subType.getId());
		}
		return false;
	}


	/**
	 * Returns the Investment Type Sub Type 2 of the specified security
	 */
	public static InvestmentTypeSubType2 getSecuritySubType2(InvestmentSecurity security) {
		if (security.getInstrument() != null && security.getInstrument().getHierarchy() != null) {
			return security.getInstrument().getHierarchy().getInvestmentTypeSubType2();
		}
		return null;
	}


	/**
	 * Returns true if the Investment Type Sub Type2 of the specified security matches by name
	 * <p>
	 * Return false if type Sub Type2 field is not set or the type Sub Type2 is different.
	 */
	public static boolean isSecurityOfTypeSubType2(InvestmentSecurity security, String investmentTypeSubType2Name) {
		if (investmentTypeSubType2Name != null) {
			InvestmentTypeSubType2 subType2 = getSecuritySubType2(security);
			if (subType2 != null) {
				return StringUtils.isEqual(investmentTypeSubType2Name, subType2.getName());
			}
		}
		return false;
	}


	/**
	 * Returns true if the Investment Type Sub Type2 of the specified security matches by id
	 * <p>
	 * Return false if type Sub Type2 field is not set or the type Sub Type2 is different.
	 */
	public static boolean isSecurityOfTypeSubType2(InvestmentSecurity security, short investmentTypeSubType2Id) {
		InvestmentTypeSubType2 subType2 = getSecuritySubType2(security);
		if (subType2 != null) {
			return MathUtils.isEqual(investmentTypeSubType2Id, subType2.getId());
		}
		return false;
	}


	/**
	 * Returns true if the security is of investment type 'Options' otherwise returns false.
	 */
	public static boolean isOption(InvestmentSecurity security) {
		return isSecurityOfType(security, InvestmentType.OPTIONS);
	}


	/**
	 * Returns true if the security is a listed option otherwise returns false.
	 */
	public static boolean isListedOption(InvestmentSecurity security) {
		return isSecurityOfTypeSubType2(security, InvestmentTypeSubType2.OPTIONS_LISTED);
	}


	/**
	 * Returns true if the security is a FLEX option otherwise returns false.
	 */
	public static boolean isFlexOption(InvestmentSecurity security) {
		return isSecurityOfTypeSubType2(security, InvestmentTypeSubType2.OPTIONS_FLEX);
	}


	/**
	 * Returns true if the security is a Custom option otherwise returns false.
	 */
	public static boolean isCustomOption(InvestmentSecurity security) {
		return isSecurityOfTypeSubType2(security, InvestmentTypeSubType2.OPTIONS_CUSTOM);
	}


	/**
	 * Returns true if the security is a Look Alike option otherwise returns false.
	 */
	public static boolean isLookAlikeOption(InvestmentSecurity security) {
		return isSecurityOfTypeSubType2(security, InvestmentTypeSubType2.OPTIONS_LOOK_ALIKE);
	}


	/**
	 * Returns true if the Investment Type of the specified instrument (instrument's hierarchy field" is the same as the argument type.
	 * <p>
	 * Return false if type field is not set of the type is different.
	 */
	public static boolean isInstrumentOfType(InvestmentInstrument instrument, String investmentTypeName) {
		if (instrument != null && instrument.getHierarchy() != null && instrument.getHierarchy().getInvestmentType() != null && investmentTypeName != null) {
			return investmentTypeName.equals(instrument.getHierarchy().getInvestmentType().getName());
		}
		return false;
	}


	/**
	 * Returns true if the name of instrument's hierarchy (regular vs expanded name) matches the specified hierarchyName.
	 * <p>
	 * Return false otherwise.
	 */
	public static boolean isSecurityInHierarchy(InvestmentSecurity security, String hierarchyName) {
		if (security.getInstrument() != null && security.getInstrument().getHierarchy() != null && hierarchyName != null) {
			return hierarchyName.equals(security.getInstrument().getHierarchy().getName());
		}
		return false;
	}


	/**
	 * Returns true if the security is in a hierarchy flagged as OTC
	 */
	public static boolean isSecurityOTC(InvestmentSecurity security) {
		if (security.getInstrument() != null && security.getInstrument().getHierarchy() != null) {
			return security.getInstrument().getHierarchy().isOtc();
		}
		return false;
	}


	/**
	 * Returns true if the specified security is INTERNATIONAL from the perspective of the specified base currency.
	 * Returns false if it's DOMESTIC.
	 * <p>
	 * If Country of Risk is the set and the same, then domestic otherwise international.
	 * If Country of Risk is not set, then uses Currency Denomination for comparison.
	 */
	public static boolean isInternationalSecurity(InvestmentSecurity security, InvestmentSecurity baseCCY) {
		return isInternationalInstrument(security.getInstrument(), baseCCY);
	}


	/**
	 * Returns true if the specified instrument is INTERNATIONAL from the perspective of the specified base currency.
	 * Returns false if it's DOMESTIC.
	 * <p>
	 * If Country of Risk is the set and the same, then domestic otherwise international.
	 * If Country of Risk is not set, then uses Currency Denomination for comparison.
	 */
	public static boolean isInternationalInstrument(InvestmentInstrument instrument, InvestmentSecurity baseCCY) {
		// if "Country of Risk" is set, use it
		if (instrument.getCountryOfRisk() != null && baseCCY.getInstrument().getCountryOfRisk() != null) {
			return !instrument.getCountryOfRisk().equals(baseCCY.getInstrument().getCountryOfRisk());
		}

		// otherwise use Currency Denomination
		return !baseCCY.equals(instrument.getTradingCurrency());
	}


	/**
	 * Returns true when the specified investment's object security is the same as client account's base currency.
	 */
	public static boolean isSecurityEqualToClientAccountBaseCurrency(InvestmentObjectInfo investmentObject) {
		return getClientAccountBaseCurrency(investmentObject).equals(investmentObject.getInvestmentSecurity());
	}


	/**
	 * Returns true when the specified investment's object security's currency denomination is the same as client account's base currency.
	 */
	public static boolean isSecurityCurrencyDenominationEqualToClientAccountBaseCurrency(InvestmentObjectInfo investmentObject) {
		return getClientAccountBaseCurrency(investmentObject).equals(investmentObject.getInvestmentSecurity().getInstrument().getTradingCurrency());
	}


	/**
	 * Returns "Currency Denomination" (specified on security's instrument) for the specified object's security.
	 */
	public static InvestmentSecurity getSecurityCurrencyDenomination(InvestmentObjectInfo investmentObject) {
		return getSecurityCurrencyDenomination(investmentObject.getInvestmentSecurity());
	}


	/**
	 * Returns "Currency Denomination" (specified on security's instrument) for the specified security.
	 */
	public static InvestmentSecurity getSecurityCurrencyDenomination(InvestmentSecurity investmentSecurity) {
		return investmentSecurity.getInstrument().getTradingCurrency();
	}


	/**
	 * Returns "Currency Denomination" symbol (specified on security's instrument) for the specified object's security.
	 */
	public static String getSecurityCurrencyDenominationSymbol(InvestmentObjectInfo investmentObject) {
		return getSecurityCurrencyDenomination(investmentObject).getSymbol();
	}


	/**
	 * Returns "Base Currency" of the specified object's Client Account.
	 * Note that Client Account's base currency (as opposed to Holding Account's) is the default one that should be used in most places.
	 * For example, it is used in the General Ledger and for most client reporting and performance calculation.
	 * It is important to use this method instead of reading the base currency directly from investment account because this consolidates
	 * access to base currency and makes one think whether client or holding account's base currency should be used.
	 */
	public static InvestmentSecurity getClientAccountBaseCurrency(InvestmentObjectInfo investmentObject) {
		return getClientAccountBaseCurrency(investmentObject.getClientInvestmentAccount());
	}


	/**
	 * Returns "Base Currency" of the specified Client Account.
	 * Note that Client Account's base currency (as opposed to Holding Account's) is the default one that should be used in most places.
	 * For example, it is used in the General Ledger and for most client reporting and performance calculation.
	 * It is important to use this method instead of reading the base currency directly from investment account because this consolidates
	 * access to base currency and makes one think whether client or holding account's base currency should be used.
	 */
	public static InvestmentSecurity getClientAccountBaseCurrency(InvestmentAccount clientAccount) {
		return clientAccount.getBaseCurrency();
	}


	/**
	 * Returns "Base Currency" symbol of the specified object's Client Account.
	 * Note that Client Account's base currency (as opposed to Holding Account's) is the default one that should be used in most places.
	 * For example, it is used in the General Ledger and for most client reporting and performance calculation.
	 * It is important to use this method instead of reading the base currency directly from investment account because this consolidates
	 * access to base currency and makes one think whether client or holding account's base currency should be used.
	 */
	public static String getClientAccountBaseCurrencySymbol(InvestmentObjectInfo investmentObject) {
		return getClientAccountBaseCurrency(investmentObject).getSymbol();
	}


	/**
	 * Returns "Base Currency" of the specified object's Holding Account.
	 * Note that Client Account's base currency (as opposed to Holding Account's) is the default one that should be used in most places.
	 * For example, it is used in the General Ledger and for most client reporting and performance calculation.
	 * It is important to use this method instead of reading the base currency directly from investment account because this consolidates
	 * access to base currency and makes one think whether client or holding account's base currency should be used.
	 */
	public static InvestmentSecurity getHoldingAccountBaseCurrency(InvestmentObjectInfo investmentObject) {
		return investmentObject.getHoldingInvestmentAccount().getBaseCurrency();
	}


	/**
	 * Returns "Base Currency" of the specified object's Holding Account.
	 * Note that Client Account's base currency (as opposed to Holding Account's) is the default one that should be used in most places.
	 * For example, it is used in the General Ledger and for most client reporting and performance calculation.
	 * It is important to use this method instead of reading the base currency directly from investment account because this consolidates
	 * access to base currency and makes one think whether client or holding account's base currency should be used.
	 */
	public static String getHoldingAccountBaseCurrencySymbol(InvestmentObjectInfo investmentObject) {
		return getHoldingAccountBaseCurrency(investmentObject).getSymbol();
	}


	/**
	 * Returns "Settlement Currency" of the specified object.
	 * Note that Investment Security's settlement currency will either be the settlement currency (sometimes called the paying currency),
	 * if one was set, or if one was not set, it will default to the instrument's currency.
	 */
	public static InvestmentSecurity getSettlementCurrency(InvestmentSecurity investmentSecurity) {
		return ObjectUtils.coalesce(investmentSecurity.getSettlementCurrency(), investmentSecurity.getInstrument().getTradingCurrency());
	}


	/**
	 * Determines if the security was created today, and returns true if it was, false if not.
	 */
	public static boolean isSecurityCreatedToday(InvestmentSecurity security) {
		return security != null && DateUtils.compare(security.getCreateDate(), new Date(), false) == 0;
	}


	/**
	 * Returns true if the specified security is active on the specified date: BETWEEN StartDate AND COALESCE(LastDeliveryDate, EndDate)
	 * <p>
	 * If settlementDate is known = use signature with settlement date to properly check bonds that are OK before start date as long as settlement date is after start date
	 */
	public static boolean isSecurityActiveOn(InvestmentSecurity security, Date date) {
		return isSecurityActiveOn(security, date, null);
	}


	/**
	 * Returns true if the specified security is active on the specified date: BETWEEN StartDate AND COALESCE(LastDeliveryDate, EndDate)
	 * <p>
	 * Optional settlement date will check if date is before start date then as long as settlement date is after start date it is OK
	 * i.e. can buy a bond before its IssueDate as long as it settles on or after the issue date
	 */
	public static boolean isSecurityActiveOn(InvestmentSecurity security, Date date, Date settlementDate) {
		if (security.getStartDate() != null) {
			if (DateUtils.compare(date, security.getStartDate(), false) < 0) {
				// before start date - check settlement date (if populated) and if that is after start date then OK
				if (settlementDate == null || (DateUtils.compare(settlementDate, security.getStartDate(), false) < 0)) {
					return false;
				}
			}
		}
		if (security.getEarlyTerminationDate() != null) {
			// If early termination date before date - then false
			if (DateUtils.compare(security.getEarlyTerminationDate(), date, false) < 0) {
				return false;
			}
		}
		if (security.getEndDate() != null) {
			if (DateUtils.compare(security.getEndDate(), date, false) < 0) {
				if (security.getLastDeliveryDate() != null) {
					if (DateUtils.compare(security.getLastDeliveryDate(), date, false) >= 0) {
						// before or on last delivery date
						return true;
					}
				}
				// after end date and no delivery
				return false;
			}
		}
		return true;
	}


	/**
	 * Returns true if the specified security belongs to a hierarchy where securities close on maturity only (LME's, currency forwards, etc.)
	 */
	public static boolean isCloseOnMaturityOnly(InvestmentSecurity security) {
		return security.getInstrument().getHierarchy().isCloseOnMaturityOnly();
	}


	/**
	 * Returns true if the specified security belongs to a hierarchy where securities do not require a payment on open (Futures, etc.)
	 * Local and Base Debit/Credit are ZERO and only position Cost Basis is set.
	 */
	public static boolean isNoPaymentOnOpen(InvestmentSecurity security) {
		return security.getInstrument().getHierarchy().isNoPaymentOnOpen();
	}


	/**
	 * Returns true if the specified security belongs to an investment type flagged as IsPhysicalSecurity = true
	 */
	public static boolean isPhysicalSecurity(InvestmentSecurity security) {
		return security.getInstrument().getHierarchy().getInvestmentType().isPhysicalSecurity();
	}


	/**
	 * Returns true if the specified security supports accrual (bond with Cash Coupon Payment, Total Return Swap with Interest Leg)
	 */
	public static boolean isAccrualSupported(InvestmentSecurity security) {
		return security.getInstrument().getHierarchy().getAccrualSecurityEventType() != null;
	}


	/**
	 * Returns max decimal precision for the specified investment security quantity field using the following
	 * logic: COALESCE(Investment Sub Type 2, Investment Sub Type, Investment Type).
	 */
	public static Short getQuantityDecimalPrecision(InvestmentSecurity security) {
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		if (hierarchy.getInvestmentTypeSubType2() != null && hierarchy.getInvestmentTypeSubType2().getQuantityDecimalPrecision() != null) {
			return hierarchy.getInvestmentTypeSubType2().getQuantityDecimalPrecision();
		}
		if (hierarchy.getInvestmentTypeSubType() != null && hierarchy.getInvestmentTypeSubType().getQuantityDecimalPrecision() != null) {
			return hierarchy.getInvestmentTypeSubType().getQuantityDecimalPrecision();
		}
		return hierarchy.getInvestmentType().getQuantityDecimalPrecision();
	}


	/**
	 * Returns Quantity field name for the specified investment security quantity field using the following
	 * logic: COALESCE(Investment Sub Type 2, Investment Sub Type, Investment Type).
	 */
	public static String getQuantityFieldName(InvestmentSecurity security) {
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		if (hierarchy.getInvestmentTypeSubType2() != null && hierarchy.getInvestmentTypeSubType2().getQuantityName() != null) {
			return hierarchy.getInvestmentTypeSubType2().getQuantityName();
		}
		if (hierarchy.getInvestmentTypeSubType() != null && hierarchy.getInvestmentTypeSubType().getQuantityName() != null) {
			return hierarchy.getInvestmentTypeSubType().getQuantityName();
		}
		if (hierarchy.getInvestmentType() != null) {
			return hierarchy.getInvestmentType().getQuantityName();
		}
		return "Quantity";
	}


	/**
	 * Returns Unadjusted Quantity field name for the specified investment security quantity field using the following
	 * logic: COALESCE(Investment Sub Type 2, Investment Sub Type, Investment Type).
	 */
	public static String getUnadjustedQuantityFieldName(InvestmentSecurity security) {
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		if (hierarchy.getInvestmentTypeSubType2() != null && hierarchy.getInvestmentTypeSubType2().getUnadjustedQuantityName() != null) {
			return hierarchy.getInvestmentTypeSubType2().getUnadjustedQuantityName();
		}
		if (hierarchy.getInvestmentTypeSubType() != null && hierarchy.getInvestmentTypeSubType().getUnadjustedQuantityName() != null) {
			return hierarchy.getInvestmentTypeSubType().getUnadjustedQuantityName();
		}
		if (hierarchy.getInvestmentType() != null) {
			return hierarchy.getInvestmentType().getUnadjustedQuantityName();
		}
		return "Unadjusted Quantity";
	}


	/**
	 * Returns true if this security supports Factor Change Events.
	 *
	 * @see #getFactorChangeEventTypeName(InvestmentSecurity)
	 */
	public static boolean isFactorChangeSupported(InvestmentSecurity security) {
		return getFactorChangeEventTypeName(security) != null;
	}


	/**
	 * Returns the name of Factor Change InvestmentSecurityEventType for the specified security.
	 * For example, Credit Event for Credit Default Swaps and Factor Change for ABS and MBS Collateralized Fixed Income securities.
	 * <p>
	 * Return null if one is not defined (not supported)
	 */
	public static String getFactorChangeEventTypeName(InvestmentSecurity security) {
		InvestmentSecurityEventType eventType = security.getInstrument().getHierarchy().getFactorChangeEventType();
		return eventType == null ? null : eventType.getName();
	}


	/**
	 * Returns the name of Payment InvestmentSecurityEventType for the specified security.
	 * For example, Cash Coupon Payment for bonds that pay coupons.
	 * <p>
	 * Return null if one is not defined (not supported)
	 */
	public static String getAccrualEventTypeName(InvestmentSecurity security) {
		InvestmentSecurityEventType eventType = security.getInstrument().getHierarchy().getAccrualSecurityEventType();
		return (eventType == null) ? null : eventType.getName();
	}


	/**
	 * Returns the name of the second Payment InvestmentSecurityEventType for the specified security.
	 * For example, Cash Coupon Payment for bonds that pay coupons.
	 * In rare cases, there maybe 2 separate accrual events: specifies optional second event (IRS: fixed and floating legs).
	 * <p>
	 * Return null if one is not defined (not supported)
	 */
	public static String getAccrualEventTypeName2(InvestmentSecurity security) {
		InvestmentSecurityEventType eventType = security.getInstrument().getHierarchy().getAccrualSecurityEventType2();
		return (eventType == null) ? null : eventType.getName();
	}


	/**
	 * Returns true if the specified security event matches Accrual Event Type 2 for corresponding security.
	 * Returns false otherwise: matches Accrual Event Type.
	 */
	public static boolean isInvestmentSecurityEventOfAccrualType2(InvestmentSecurityEvent securityEvent) {
		String eventType2 = getAccrualEventTypeName2(securityEvent.getSecurity());
		return (eventType2 != null && eventType2.equals(securityEvent.getType().getName()));
	}


	/**
	 * Returns true if currency denomination for the specified security is equal to the specified currency symbol.
	 */
	public static boolean isSecurityDenominatedIn(InvestmentSecurity security, String currencySymbol) {
		if (security.getInstrument() != null && security.getInstrument().getTradingCurrency() != null && currencySymbol != null) {
			return currencySymbol.equals(security.getInstrument().getTradingCurrency().getSymbol());
		}
		return false;
	}


	/**
	 * Internally generated cusip are required for any OTC security or any Swap
	 */
	public static boolean isInternalCUSIPRequired(InvestmentSecurity security) {
		return (InvestmentUtils.isSecurityOTC(security) || InvestmentUtils.isSecurityOfType(security, InvestmentType.SWAPS));
	}


	/**
	 * Returns a valid CUSIP in the format reserved for internal use. Used to assign internal identifiers to OTC securities.
	 * Identifier starts with "99" followed by 0's then by investmentSecurityId and finally by check sum digit.
	 * Uses this format we can always generate a unique internal CUSIP for any security based on its id.
	 * <p>
	 * Issuer Identifiers Reserved for Internal Use: Issuer identifiers (990 to 999 and 99A to 99Z in each group of
	 * 1,000 numbers) have also been reserved for the user’s own purpose. This permits a user to assign an issuer
	 * identifier to any issuer which might be relevant to his holdings but which does not qualify for coverage under
	 * the CUSIP system. Other issuer identifiers (990000 to 999999 and 99000A to 99999Z) are also reserved for
	 * the user so that they may be assigned to non-security assets or for other internal operating purposes. Thus,
	 * with the addition to at least two numeric digits in the issue identifier field, a minimum of three million
	 * identifiers is available to the user for internal miscellaneous assets.
	 */
	public static String getInternalCUSIP(int investmentSecurityId) {
		String securityId = Integer.toString(investmentSecurityId);
		int securityLen = securityId.length();
		if (securityLen > 6) {
			// NOTE: if we ever get this, enhance the logic to start using letters (add 7th digit as a letter: 10 => A)???
			throw new IllegalArgumentException("Cannot generate Internal CUSIP for security with id longer than 6 characters: " + investmentSecurityId);
		}

		StringBuilder result = new StringBuilder(9);
		result.append("99");
		for (int i = 0; i < (6 - securityLen); i++) {
			result.append('0');
		}
		result.append(securityId);
		result.append(getCheckDigitForCUSIP(result.toString()));
		return result.toString();
	}


	public static char getCheckDigitForCUSIP(String cusip) {
		int checkSum = 0;
		int chMultiplier = 1;
		for (int i = 0; i < 8; i++) {
			char ch = cusip.charAt(i);
			int intValue = Character.isDigit(ch) ? (ch - '0') : ((ch - 'A') + 10);
			intValue *= chMultiplier;
			String stringValue = Integer.toString(intValue);
			for (int j = 0; j < stringValue.length(); j++) {
				checkSum += stringValue.charAt(j) - '0';
			}
			chMultiplier = (chMultiplier == 1) ? 2 : 1;
		}
		String checkSumString = Integer.toString(checkSum);
		int checkDigit = 10 - (checkSumString.charAt(checkSumString.length() - 1) - '0');
		if (checkDigit == 10) {
			checkDigit = 0;
		}
		return Integer.toString(checkDigit).charAt(0);
	}


	/**
	 * Returns the OCC strike price string representation of the provided strike price string.
	 * <p>
	 * The Options Clearing Corporation's Options Symbology Initiative defines the OCC symbol to use
	 * eight digits for the strike price (five for the explicit dollar (left of decimal point), and
	 * three for the decimal (right of the decimal point) (e.g. 56.5 = 00056500).
	 */
	public static String getOccStrikePriceString(String strikePriceString) {
		int decimalIndex = strikePriceString.indexOf('.');
		String explicitStrike;
		String decimal;
		if (decimalIndex > -1) {
			explicitStrike = strikePriceString.substring(0, decimalIndex);
			decimal = strikePriceString.substring(decimalIndex + 1);
		}
		else {
			explicitStrike = strikePriceString;
			decimal = StringUtils.EMPTY_STRING;
		}
		StringBuilder occStrikeStringBuilder = new StringBuilder(explicitStrike).append(decimal);
		int prefixingZeros = OCC_SYMBOL_STRIKE_DOLLAR_LENGTH - explicitStrike.length();
		ValidationUtils.assertFalse(prefixingZeros < 0, () -> "Strike price whole dollar string [" + explicitStrike + "] is larger than allowed. Only " + OCC_SYMBOL_STRIKE_DOLLAR_LENGTH + " digits are allowed.");
		for (int i = 0; i < prefixingZeros; i++) {
			occStrikeStringBuilder.insert(0, '0');
		}
		int suffixingZeros = OCC_SYMBOL_STRIKE_DECIMAL_LENGTH - decimal.length();
		ValidationUtils.assertFalse(suffixingZeros < 0, () -> "Strike price decimal string [" + decimal + "] is larger than allowed. Only " + OCC_SYMBOL_STRIKE_DECIMAL_LENGTH + " digits are allowed.");
		for (int i = 0; i < suffixingZeros; i++) {
			occStrikeStringBuilder.append('0');
		}
		return occStrikeStringBuilder.toString();
	}


	/**
	 * Used separately on hierarchies, instruments, and securities.  Each level can have additional validation specific to it's own rules, this method will validate the actual value entered is an acceptable value, i.e. greater than 0.
	 */
	public static void validatePriceMultiplierValue(BigDecimal priceMultiplier) {
		if (priceMultiplier != null) {
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(priceMultiplier, BigDecimal.ZERO), "Invalid Price Multiplier Entered [" + CoreMathUtils.formatNumberDecimal(priceMultiplier) + "]. Price Multiplier must be greater than zero.");
		}
	}


	/**
	 * Returns InvestmentExchange where the specified security is traded. Uses either instrument specific
	 * exchange or, if not defined, default hierarchy exchange.
	 * <p>
	 * Returns null if the exchange was not defined.
	 */
	public static InvestmentExchange getSecurityExchange(InvestmentSecurity security) {
		InvestmentExchange result = null;
		InvestmentInstrument instrument = security.getInstrument();
		if (instrument != null) {
			result = instrument.getExchange();
			if (result == null && instrument.getHierarchy() != null) {
				result = instrument.getHierarchy().getDefaultExchange();
			}
		}
		return result;
	}


	/**
	 * Returns the currency symbol for the given currency Ticker, i.e. $ for USD
	 * Returns null if currencyTicker is null
	 * returnTickerIfNoSymbol will return the passed in currencyTicker if there isn't a symbol associated with it, otherwise will return null.
	 */
	public static String getCurrencySymbol(String currencyTicker, boolean returnTickerIfNoSymbol) {
		if (currencyTicker == null) {
			return null;
		}
		switch (currencyTicker.toUpperCase()) {
			case "USD":
				return "$";
			case "GBP":
				return "£";
			case "EUR":
				return "€";
			case "JPY":
				return "¥";
			default:
				return (returnTickerIfNoSymbol ? currencyTicker : null);
		}
	}


	/**
	 * Returns the amount formatted with currency symbol or ticker. If a symbol exists will be Symbol + FormatNumber (Amount), else FormatNumber(Amount) + Ticker
	 * Example, for 1000 and USD, would return $ 1,000.00, for 1000 and CAD would return 1,000.00 CAD
	 * Can pass in the number format function to use, i.e. MathUtils::formatNumberDecimal, by default will use MathUtils::formatNumberMoney
	 *
	 * @param useSpaceAfterSymbol - If true and we have a currency symbol, will return with a space between the symbol and amount - i.e. $ 1,000 - if false, would be: $1,000
	 */
	public static String formatAmountWithCurrency(BigDecimal amount, String currencyTicker, boolean useSpaceAfterSymbol, Function<BigDecimal, String> numberFormatFunction) {
		AssertUtils.assertNotNull(amount, "Amount cannot be null");
		if (numberFormatFunction == null) {
			numberFormatFunction = CoreMathUtils::formatNumberMoney;
		}
		String currencySymbol = getCurrencySymbol(currencyTicker, false);
		if (!StringUtils.isEmpty(currencySymbol)) {
			return currencySymbol + (useSpaceAfterSymbol ? " " : "") + numberFormatFunction.apply(amount);
		}
		return numberFormatFunction.apply(amount) + " " + StringUtils.coalesce(true, currencyTicker);
	}


	public static Integer getFutureMonth(InvestmentSecurity security) {
		if (isSecurityOfType(security, InvestmentType.FUTURES) || isSecurityOfTypeSubType(security, InvestmentTypeSubType.OPTIONS_ON_FUTURES)) {
			String symbol = security.getSymbol();

			String monthCode;
			if (!StringUtils.isEmpty(security.getInstrument().getIdentifierPrefix())) {
				String prefix = security.getInstrument().getIdentifierPrefix().replace("OF-", "");
				String trimmedSymbol = symbol.substring(prefix.length()).trim();
				monthCode = trimmedSymbol.substring(0, 1);
			}
			else {
				int yearIndex = MathUtils.isNumber(symbol.substring(symbol.length() - 2)) ? 2 : 1;
				monthCode = symbol.substring(symbol.length() - (yearIndex + 1), symbol.length() - yearIndex);
			}

			int month = 0;
			switch (monthCode) {
				case "F":
					month = 1;
					break;
				case "G":
					month = 2;
					break;
				case "H":
					month = 3;
					break;
				case "J":
					month = 4;
					break;
				case "K":
					month = 5;
					break;
				case "M":
					month = 6;
					break;
				case "N":
					month = 7;
					break;
				case "Q":
					month = 8;
					break;
				case "U":
					month = 9;
					break;
				case "V":
					month = 10;
					break;
				case "X":
					month = 11;
					break;
				case "Z":
					month = 12;
					break;
			}
			return month;
		}
		return null;
	}


	/**
	 * Get the year from a future symbol.
	 * <p>
	 * NOTE:  This will support future of form ESH11 for 2011.
	 */
	public static Integer getFutureYear(InvestmentSecurity security) {
		if (isSecurityOfType(security, InvestmentType.FUTURES) || isSecurityOfTypeSubType(security, InvestmentTypeSubType.OPTIONS_ON_FUTURES)) {
			String symbol = security.getSymbol();
			int yearIndex = MathUtils.isNumber(symbol.substring(symbol.length() - 2)) ? 2 : 1;
			int yearCode = Integer.parseInt(symbol.substring(symbol.length() - yearIndex));

			int newYear;
			if (yearIndex == 2) {
				int currentYear = DateUtils.getYear(new Date());
				int currentYearCode = currentYear % 100;
				newYear = (currentYear - currentYearCode) + yearCode;
			}
			else {
				int currentYear = DateUtils.getYear(new Date());
				int currentYearCode = currentYear % 10;
				newYear = currentYear + (yearCode - currentYearCode);
				if (yearCode < currentYearCode) {
					newYear += 10;
				}
			}
			return newYear;
		}
		return null;
	}


	/**
	 * Returns the "ultimate" or top level underlying security.
	 * Note: This method will go at most 10 levels deep to prevent possible circular dependency
	 *
	 * @throws ValidationException if reaches 10 levels deep
	 */
	public static InvestmentSecurity getUltimateUnderlyingSecurity(InvestmentSecurity security) {
		InvestmentSecurity ultimateUnderlying = security.getUnderlyingSecurity();
		if (ultimateUnderlying != null) {
			// Use counter to prevent circular dependency - expect nothing to go beyond 10 levels deep for underlying
			int count = 0;
			while (ultimateUnderlying.getUnderlyingSecurity() != null) {
				ultimateUnderlying = ultimateUnderlying.getUnderlyingSecurity();
				count++;
				if (count == 10) {
					throw new ValidationException("Security [" + security.getLabel() + "]: Cannot determine ultimate underlying security.  There may be a circular dependency - Stopped after 10 levels deep.");
				}
			}
		}
		return ultimateUnderlying;
	}


	/**
	 * Given a list of key values from a map, returns the one that applies to the given security based on what is found
	 * from the following combinations - most specific first.
	 * <p>
	 * keyPrefix: [Type:SubType:SubType2]
	 * keyPrefix: [Type:SubType]
	 * keyPrefix: [Type]
	 * keyPrefix
	 * null
	 */
	public static String getSecurityOverrideKey(String keyPrefix, InvestmentSecurity security, Collection<String> keys) {
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		String key = null;
		if (hierarchy != null) {
			key = generateKeyForTypeOverrides(keys, keyPrefix, hierarchy);
		}
		// Last Check - Key Prefix Only
		if (key == null && keyPrefix != null && keys.contains(keyPrefix)) {
			key = keyPrefix;
		}
		return key;
	}


	/**
	 * Given a list of key values from a map, returns the one that applies to the given event payout on what is found
	 * from the following combinations - most specific first
	 * <p>EXAMPLE:
	 * Scrip Dividend: [Payout:Currency]: [Stocks:Common Stocks]
	 * Scrip Dividend: [Payout:Currency]: [Stocks]
	 * Scrip Dividend: [Payout:Currency]
	 * Scrip Dividend: [Stocks:Common Stocks]
	 * Scrip Dividend: [Stocks]
	 * Scrip Dividend
	 * null
	 */
	public static String getSecurityOverrideKey(String keyPrefix, InvestmentSecurityEventPayout eventPayout, Collection<String> keys) {
		// try payout specificity first
		String payoutKey = keyPrefix + ": [Payout:" + eventPayout.getPayoutType().getName() + "]";
		if (keys.contains(payoutKey)) {
			return payoutKey;
		}

		InvestmentSecurity security = eventPayout.getSecurityEvent().getSecurity();
		String key = generateKeyForTypeOverrides(keys, payoutKey, security.getInstrument().getHierarchy());
		if (key == null) {
			key = getSecurityOverrideKey(keyPrefix, security, keys);
		}
		return key;
	}


	/**
	 * Returns key that matches for that specific type/sub type/subtype 2 combination
	 * Returns null if no override found
	 */
	private static String generateKeyForTypeOverrides(Collection<String> keys, String keyPrefix, InvestmentInstrumentHierarchy hierarchy) {
		String keyTemplate = ((keyPrefix != null) ? keyPrefix + ": " : "") + "[SECURITY_OVERRIDE]";

		String investmentTypeName = hierarchy.getInvestmentType().getName();
		String investmentTypeSubTypeName = (hierarchy.getInvestmentTypeSubType() != null ? hierarchy.getInvestmentTypeSubType().getName() : null);
		String investmentTypeSubType2Name = (hierarchy.getInvestmentTypeSubType2() != null ? hierarchy.getInvestmentTypeSubType2().getName() : null);

		if (investmentTypeSubTypeName != null) {
			if (investmentTypeSubType2Name != null) {
				String key = keyTemplate.replace("SECURITY_OVERRIDE", StringUtils.join(new String[]{investmentTypeName, investmentTypeSubTypeName, investmentTypeSubType2Name}, ":"));
				if (keys.contains(key)) {
					return key;
				}
			}
			String key = keyTemplate.replace("SECURITY_OVERRIDE", StringUtils.join(new String[]{investmentTypeName, investmentTypeSubTypeName}, ":"));
			if (keys.contains(key)) {
				return key;
			}
		}
		String key = keyTemplate.replace("SECURITY_OVERRIDE", investmentTypeName);
		if (keys.contains(key)) {
			return key;
		}
		return null;
	}


	/**
	 * Returns the Clearing Broker company (holding investment account issuer) for the specified investment object.
	 *
	 * @param investmentObject the investment object to retrieve the clearing broker for
	 * @return the clearing broker of the transaction. If no holding investment account is specified, then <code>null</code> is returned.
	 */
	public static BusinessCompany getClearingBroker(InvestmentObjectInfo investmentObject) {
		InvestmentAccount account = investmentObject.getHoldingInvestmentAccount();
		return account == null ? null : account.getIssuingCompany();
	}


	/**
	 * Will return the Option symbol without the strike price assuming the price is at the end of symbol and it's an Option on a Future.
	 */
	public static String getOptionOnFutureSymbolWithoutStrikePrice(InvestmentSecurity security) {
		ValidationUtils.assertTrue(isSecurityOfType(security, InvestmentType.OPTIONS) && security.getUnderlyingSecurity() != null && isSecurityOfType(security.getUnderlyingSecurity(), InvestmentType.FUTURES), "Cannot remove strike price because [" + security.toString() + "] is not a valid Option of Futures.");
		return security.getSymbol().substring(0, security.getSymbol().lastIndexOf(" "));
	}


	/**
	 * Splits a listed option ticker and extracts the option symbol prefix.  For example EFA US 02/22/18 C122, would return symbol EFA.
	 */
	public static String getOptionSymbolPrefix(String optionSecuritySymbol) {
		if (StringUtils.isEmpty(optionSecuritySymbol)) {
			return null;
		}

		return optionSecuritySymbol.split(" ")[0];
	}
}
