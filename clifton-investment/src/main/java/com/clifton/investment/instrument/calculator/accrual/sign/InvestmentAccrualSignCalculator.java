package com.clifton.investment.instrument.calculator.accrual.sign;


import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>InvestmentAccrualSignCalculator</code> interface must be implemented by various accrual amount sign calculators.
 * For example, Total Return Swaps should always negate accrual amount.  Interest Rate Swaps calculate the sign depending
 * on Swap structure as well as which leg is being calculated (fixed or floating).
 *
 * @author vgomelsky
 */
public interface InvestmentAccrualSignCalculator {

	/**
	 * Returns true if the sign of calculated accrual amount should be negated for the specified security and coupon frequency field.
	 */
	public boolean isAccrualAmountNegated(InvestmentSecurity security, String couponFrequencyFieldName);
}
