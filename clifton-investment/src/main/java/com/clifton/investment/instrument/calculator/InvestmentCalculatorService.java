package com.clifton.investment.instrument.calculator;


import com.clifton.calendar.setup.Calendar;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentCalculatorService</code> interface defines methods that calculate various measures.
 *
 * @author vgomelsky
 */
public interface InvestmentCalculatorService {

	/**
	 * Returns today if the exchange is currently open. If it's currently closed, it will return the next TRADE business day from today using exchange's calendar.
	 * <p>
	 * Uses TRADE Business Days
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public Date getInvestmentSecurityTradeDate(int securityId);


	/**
	 * Returns settlement date based on the specified transaction date (trade date) for the specified security.
	 * Optional "settlementCurrencyId" parameter is required for spot currency trades that have settlement cycle
	 * defined per currency pair.
	 * <p>
	 * Uses SETTLEMENT Business Days
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public Date getInvestmentSecuritySettlementDate(int securityId, Integer settlementCurrencyId, Date transactionDate);


	/**
	 * Calculates and returns the notional for the specified security, price, and quantity.
	 * Uses security specific calculator definitions for rounding, etc.
	 *
	 * @param securityId
	 * @param price
	 * @param quantity
	 * @param notionalMultiplier - this field is usually 1 (meaning no multiplier) or null which is the same.
	 *                           However, for inflation linked bonds (TIPS, etc.) it is equal to the index ratio and will multiply the quantity by this number
	 *                           unless instrument.hierarchy.notionalMultiplierForPriceNotUsed = true (old British linkers) no need to multiply twice.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public BigDecimal getInvestmentSecurityNotional(int securityId, BigDecimal price, BigDecimal quantity, BigDecimal notionalMultiplier);


	/**
	 * Rounds the notional amount using the calculator from the provided security.
	 *
	 * @param roundingSecurityId - the id of the security that whose calculator will be used to round the notional value
	 * @param notionalAmount
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	@RequestMapping("investmentSecurityNotionalRoundingApply")
	public BigDecimal applyInvestmentSecurityNotionalRounding(int roundingSecurityId, BigDecimal notionalAmount);


	/**
	 * Retrieves the notional multiplier for the specified security (Index Ratio or TIPS, FV Notional Multiplier for ZCS).
	 * Returns 1 if no special multiplier is applicable for the specified security.
	 * Optionally can use Next Day for Lookup (false if not specified)
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public BigDecimal getInvestmentSecurityNotionalMultiplier(int securityId, Date date, Date settlementDate, Boolean useNextDayForLookup);


	@ResponseBody
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public BigDecimal getInvestmentSecurityDirtyPrice(int securityId, BigDecimal price, BigDecimal quantity, Date date);


	/**
	 * Calculates and returns Accrued Interest on the specified notional amount.
	 * The interest is calculated from start date of the interest period that the specified date fall into to the specified date.
	 * Uses the interest rate of the period for calculation and rounds result to 2 decimal places.
	 *
	 * @param securityId
	 * @param notionalMultiplier - this field is usually 1 (meaning no multiplier) or null which is the same.
	 *                           However, for inflation linked bonds (TIPS, etc.) it is equal to the index ratio and will multiply the quantity by this number
	 *                           unless instrument.hierarchy.notionalMultiplierForAccrualNotUsed = true (Zero Coupon Swaps) no need to multiply twice.
	 * @param notional
	 * @param date               include accrued interest up to and including this date
	 * @param accrueFromDate     optionally specify the date of accrual start if after start of current accrual period (TRS upsizing positions use position open date)
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public BigDecimal getInvestmentSecurityAccruedInterest(int securityId, BigDecimal notional, BigDecimal notionalMultiplier, Date date, Date accrueFromDate);


	/**
	 * Calculates and returns interest payment on the specified notional amount for a given time period and day count convention.
	 * The interest is calculated from start date of the interest period that the specified date fall into to the specified date.
	 * NOTE: interest is not included for the the date day!!! Bond buys/sells don't include accrued interest on the settlement date of the trade.
	 * Use the interest rate of the period for calculation and rounds result to 2 decimal places.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public BigDecimal getInvestmentInterestForPeriod(BigDecimal notional, BigDecimal rate, Date startDate, Date endDate, String dayCountConventionKey);


	/**
	 * Calculates and returns interest payment on the specified notional amount for a given start date, number of days and day count convention.
	 * The interest is calculated from start date of the interest period that the specified date fall into to the specified date.
	 * NOTE: interest is not included for the the date day!!! Bond buys/sells don't include accrued interest on the settlement date of the trade.
	 * Use the interest rate of the period for calculation and rounds result to 2 decimal places.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public BigDecimal getInvestmentInterestForDays(BigDecimal notional, BigDecimal rate, Date startDate, Integer days, String dayCountConventionKey);


	/**
	 * Multiplies two floating point values together and scales them according to the configured scale.  Used primarily from the UI since Javascript has normal floating point issues.
	 * Should not be used anywhere else, use MathUtils.multiply instead.
	 */
	@ResponseBody
	@SecureMethod(disableSecurity = true)
	public BigDecimal getInvestmentMultiplyAndRound(BigDecimal value1, BigDecimal value2, int scale);


	/**
	 * For a given security returns the settlement calendar (if field is selected on hierarchy and a settlement calendar is defined for the security
	 * else the exchange calendar
	 * <p>
	 * Used for determining business days for the security for price look ups
	 */
	public Calendar getInvestmentSecurityCalendar(int securityId);
}
