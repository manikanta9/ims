package com.clifton.investment.instrument.event.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.function.BiConsumer;


/**
 * <code>InvestmentSecurityEventSearchFormConfigurer</code> is a {@link HibernateSearchFormConfigurer} for a {@link InvestmentSecurityEventSearchForm}.
 *
 * @author nickk
 */
public class InvestmentSecurityEventSearchFormConfigurer<T extends InvestmentSecurityEventSearchForm> extends HibernateSearchFormConfigurer {

	private final InvestmentSecurityEventService investmentSecurityEventService;
	private final BiConsumer<InvestmentSecurityEventSearchFormConfigurer<T>, Criteria> extendedCriteriaConfigurer;


	public InvestmentSecurityEventSearchFormConfigurer(T securityEventSearchForm, InvestmentSecurityEventService investmentSecurityEventService) {
		this(securityEventSearchForm, investmentSecurityEventService, null);
	}


	public InvestmentSecurityEventSearchFormConfigurer(T securityEventSearchForm, InvestmentSecurityEventService investmentSecurityEventService, BiConsumer<InvestmentSecurityEventSearchFormConfigurer<T>, Criteria> extendedCriteriaConfigurer) {
		super(securityEventSearchForm, true);
		this.investmentSecurityEventService = investmentSecurityEventService;
		this.extendedCriteriaConfigurer = extendedCriteriaConfigurer;
		// must configure search form after the event service is set
		configureSearchForm();
	}


	public static InvestmentSecurityEventSearchFormConfigurer<InvestmentSecurityEventSearchForm> forEvent(InvestmentSecurityEvent event, InvestmentSecurityEventService investmentSecurityEventService, BiConsumer<InvestmentSecurityEventSearchFormConfigurer<InvestmentSecurityEventSearchForm>, Criteria> extendedCriteriaConfigurer) {
		InvestmentSecurityEventSearchForm securityEventSearchForm = new InvestmentSecurityEventSearchForm();
		securityEventSearchForm.setEventDate(event.getEventDate());
		securityEventSearchForm.setTypeId(event.getType().getId());

		if (event.getType().isDescriptionIncludedInNaturalKey()) {
			securityEventSearchForm.addSearchRestriction(new SearchRestriction("eventDescription", event.getEventDescription() == null ? ComparisonConditions.IS_NULL : ComparisonConditions.EQUALS, event.getEventDescription()));
		}
		return new InvestmentSecurityEventSearchFormConfigurer<>(securityEventSearchForm, investmentSecurityEventService, extendedCriteriaConfigurer);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected void configureSearchForm() {
		super.configureSearchForm();

		@SuppressWarnings("unchecked")
		T searchForm = (T) getSortableSearchForm();
		// avoid joins to speed up performance when possible
		if (!StringUtils.isEmpty(searchForm.getTypeName())) {
			searchForm.setTypeId(getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(searchForm.getTypeName()).getId());
			searchForm.setTypeName(null);
		}
		if (!StringUtils.isEmpty(searchForm.getStatusName())) {
			searchForm.setStatusId(getInvestmentSecurityEventService().getInvestmentSecurityEventStatusByName(searchForm.getStatusName()).getId());
			searchForm.setStatusName(null);
		}
		if (!ArrayUtils.isEmpty(searchForm.getTypeNames())) {
			Short[] typeIds = new Short[searchForm.getTypeNames().length];
			for (int i = 0; i < typeIds.length; i++) {
				typeIds[i] = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(searchForm.getTypeNames()[i]).getId();
			}
			searchForm.setTypeIds(typeIds);
			searchForm.setTypeNames(null);
		}
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		@SuppressWarnings("unchecked")
		T searchForm = (T) getSortableSearchForm();

		if (searchForm.getDateBetweenDeclareAndExDate() != null) {
			criteria.add(Restrictions.le("declareDate", searchForm.getDateBetweenDeclareAndExDate()));
			criteria.add(Restrictions.gt("exDate", searchForm.getDateBetweenDeclareAndExDate()));
		}
		if (searchForm.getParentInvestmentSecurityId() != null) {
			String securityAlias = getPathAlias("security", criteria);

			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentSecurityAllocation.class, "alloc");
			sub.setProjection(Projections.property("id"));

			LogicalExpression startDateFilter = Restrictions.or(Restrictions.isNull("alloc.startDate"), Restrictions.leProperty("alloc.startDate", criteria.getAlias() + ".exDate"));
			// For end date restriction, we need to look at one day before exDate for the allocation active range.
			String exDateSqlColumnAlias = criteria.getAlias() + "_.EventExDate";// _ is required suffix to alias.
			LogicalExpression endDateFilter = Restrictions.or(Restrictions.isNull("alloc.endDate"), Restrictions.sqlRestriction("{alias}.EndDate >= DATEADD(day, -1, " + exDateSqlColumnAlias + ")"));
			sub.add(Restrictions.and(startDateFilter, endDateFilter));

			sub.add(Restrictions.disjunction()
					.add(Restrictions.eqProperty("alloc.investmentSecurity.id", securityAlias + ".id"))
					.add(Restrictions.eqProperty("alloc.investmentInstrument.id", securityAlias + ".instrument.id")));

			sub.add(Restrictions.eq("alloc.parentInvestmentSecurity.id", searchForm.getParentInvestmentSecurityId()));

			criteria.add(Subqueries.exists(sub));
		}

		if (this.extendedCriteriaConfigurer != null) {
			this.extendedCriteriaConfigurer.accept(this, criteria);
		}
	}


	/**
	 * Publicly exposes {@link #getPathAlias(String, Criteria)} for use in extended configurer.
	 *
	 * @see #getPathAlias(String, Criteria)
	 */
	public String getConfigurerPathAlias(String path, Criteria criteria) {
		return super.getPathAlias(path, criteria);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}
}
