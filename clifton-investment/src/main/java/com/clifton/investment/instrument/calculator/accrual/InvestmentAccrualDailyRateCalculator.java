package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.DayCountCommand;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.sign.AccrualSignCalculators;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;


/**
 * The <code>InvestmentAccrualDailyRateCalculator</code> encapsulates functionality for accruals that have rates
 * that can vary from day to day.  Because the spread and rate calculations may differ, it's subclasses must override two methods:
 * one that calculates the reference rate, and one that calculates the spread for the accrual.
 *
 * @author rbrooks
 */
public abstract class InvestmentAccrualDailyRateCalculator extends BaseInvestmentAccrualCalculator {

	private InvestmentCalculator investmentCalculator;
	private InvestmentSecurityEventService investmentSecurityEventService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal doCalculateAccruedInterest(InvestmentSecurityEvent paymentEvent, BigDecimal notional, BigDecimal notionalMultiplier, Date accrueUpToDate) {
		// lookup security configuration necessary to calculate accrual
		AccrualConfig accrualConfig = getAccrualConfig(paymentEvent, false);

		Date periodStartDate = paymentEvent.getAccrualStartDate();
		Date periodEndDate = paymentEvent.getAccrualEndDate();
		Date accrualDateLimit = DateUtils.addDays(periodEndDate, 1); // allow accrual one day past end date, so that end date may be included in accrual

		ValidationUtils.assertTrue(
				DateUtils.isDateBetween(accrueUpToDate, periodStartDate, accrualDateLimit, false),
				"\"accrueUpToDate\" [" + DateUtils.fromDate(accrueUpToDate, DateUtils.DATE_FORMAT_INPUT) + "] is invalid for payment event with accrual start date of ["
						+ DateUtils.fromDate(periodStartDate, DateUtils.DATE_FORMAT_INPUT) + "], and accrual end date of [" + DateUtils.fromDate(periodEndDate, DateUtils.DATE_FORMAT_INPUT) + "] "
						+ "(accruals are allowed to one day past the period end date).");

		// ONCE VARIABLE RATE SPREADS ARE SUPPORTED, LOOK UP SPREAD ON InvestmentSecurityEventDetail
		InvestmentSecurity security = paymentEvent.getSecurity();
		BigDecimal spread = (BigDecimal) getSecurityFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_SPREAD);

		// Make use of caching the list by event, then filter results
		List<InvestmentSecurityEventDetail> fullDetailList = getInvestmentSecurityEventService().getInvestmentSecurityEventDetailListByEvent(paymentEvent.getId());
		fullDetailList = BeanUtils.sortWithFunction(fullDetailList, InvestmentSecurityEventDetail::getEffectiveDate, true);
		List<InvestmentSecurityEventDetail> detailList = new ArrayList<>();
		for (InvestmentSecurityEventDetail detail : CollectionUtils.getIterable(fullDetailList)) {
			// skip details where the rate hasn't been populated yet
			if (detail.getReferenceRate() != null && DateUtils.compare(detail.getEffectiveDate(), accrueUpToDate, false) <= 0) {
				ValidationUtils.assertTrue(MathUtils.isEqual(spread, detail.getSpread()), "The spread for Investment Security Event [" + paymentEvent.getLabel()
						+ "] differs from one or more of its Event Details (Event spread = " + spread + ", Event Detail spread = " + detail.getSpread()
						+ ").  Variable rate spreads are not supported at this time.");
				detailList.add(detail);
			}
		}

		// normalize spread and calculate reference rate (getAfterEventValue() = spread + referenceRate)
		BigDecimal normalizedSpread = MathUtils.divide(spread, 100);
		BigDecimal referenceRate = MathUtils.subtract(paymentEvent.getAfterEventValue(), normalizedSpread);

		if (notionalMultiplier != null) {
			notional = MathUtils.multiply(notional, notionalMultiplier); // should we round to 2 ???
		}

		// PROCESS REFERENCE RATE ACCRUAL
		AccrualDailyRate[] dailyRates = getAccrualPeriodDailyRates(security, referenceRate, periodStartDate, accrueUpToDate, accrualConfig, detailList);
		BigDecimal rateResult = calculateReferenceRateAmount(paymentEvent, notional, dailyRates, accrualConfig);
		rateResult = InvestmentCalculatorUtils.getNotionalCalculator(security).round(rateResult);

		// PROCESS SPREAD ACCRUAL
		BigDecimal spreadResult = BigDecimal.ZERO;
		if (!MathUtils.isNullOrZero(normalizedSpread)) {
			spreadResult = normalizedSpread.movePointLeft(2)
					.multiply(notional)
					.multiply(accrualConfig.calculateAccrualRate(
							DayCountCommand.forAccrualRange(paymentEvent.getAccrualStartDate(), accrueUpToDate)
									.withAccrualPeriod(paymentEvent.getAccrualStartDate(), paymentEvent.getAccrualEndDate())
									.withFrequency(accrualConfig.getFrequency()))
					);
			spreadResult = InvestmentCalculatorUtils.getNotionalCalculator(security).round(spreadResult);
		}

		// CALCULATE FINAL RESULT
		BigDecimal finalResult = rateResult.add(spreadResult);

		AccrualSignCalculators signCalculator = security.getInstrument().getHierarchy().getAccrualSignCalculator();
		if (signCalculator != null) {
			if (getInvestmentAccrualCalculatorFactory().getInvestmentAccrualSignCalculator(signCalculator).isAccrualAmountNegated(security, accrualConfig.getCouponFrequencyField())) {
				finalResult = MathUtils.negate(finalResult);
			}
		}
		return finalResult;
	}


	private int getDaysBetween(Date startDate, Date endDate, AccrualConfig accrualConfig) {
		if (accrualConfig.getDayCountConvention().isBusinessDaysDependent()) {
			return getCalendarBusinessDayService().getBusinessDaysBetween(startDate, endDate, null, accrualConfig.getCalendarId());
		}
		return DateUtils.getDaysDifference(endDate, startDate);
	}


	/**
	 * Event details are populated on events from Fixing Date of a security when it exists, which may result in details
	 * prior to accrual start date but still applicable. The event's accrual period (accrual start/end dates) is defined
	 * according to effective dates in IMS (e.g., T+1). In processing effective date for accrual calculator, we need to
	 * adjust the effective date according to the fixing cycle to pick up the correct daily rate. Event details should be
	 * added with an effective date according to fixing dates so events will exist prior to accrual start date.
	 */
	private AccrualDailyRate[] getAccrualPeriodDailyRates(InvestmentSecurity eventSecurity, BigDecimal eventReferenceRate, Date accrualPeriodStartDate, Date accrualPeriodUpToDate, AccrualConfig accrualConfig, List<InvestmentSecurityEventDetail> eventDetailList) {
		int daysInAccrual = getDaysBetween(accrualPeriodStartDate, accrualPeriodUpToDate, accrualConfig);
		AccrualDailyRate[] dailyRates = new AccrualDailyRate[daysInAccrual];

		Iterator<InvestmentSecurityEventDetail> detailIterable = CollectionUtils.getIterable(eventDetailList).iterator();
		Supplier<InvestmentSecurityEventDetail> nextDetailSupplier = () -> detailIterable.hasNext() ? detailIterable.next() : null;
		InvestmentSecurityEventDetail nextDetail = nextDetailSupplier.get();

		int index = 0;
		// start at the period start date and process each day in the period for accrual rates (per fixing date) according to event detail rates
		UnaryOperator<Date> accrualDateFixingDateConverter = getFixingCalendarPreviousBusinessDateConverter(eventSecurity);
		Date accrualDate = accrualPeriodStartDate;
		BigDecimal previousRate = eventReferenceRate;
		while (index < daysInAccrual && DateUtils.isDateBeforeOrEqual(accrualDate, accrualPeriodUpToDate, false)) {
			Date accrualFixingDate = accrualDateFixingDateConverter.apply(accrualDate);
			int accrualFixingCompareResult = nextDetail == null ? -1 : DateUtils.compare(accrualFixingDate, nextDetail.getEffectiveDate(), false);

			if (accrualFixingCompareResult > -1) {
				// the current detail is effective before or after the accrual fixing date, so advance it
				previousRate = nextDetail.getReferenceRate();
				nextDetail = nextDetailSupplier.get();
			}

			if (accrualFixingCompareResult < 1) {
				// Calculate each accrual day uniquely with range of 1 day. The days between will return 1 or 0 for non-business day when necessary.
				Date nextAccrualDate = DateUtils.addDays(accrualDate, 1);
				int daysInSubPeriod = getDaysBetween(accrualDate, nextAccrualDate, accrualConfig);
				for (int j = 0; j < daysInSubPeriod; j++) {
					dailyRates[index] = AccrualDailyRate.forRateOnDate(previousRate, accrualFixingDate);
					index++;
				}
				accrualDate = nextAccrualDate;
			}
		}

		return dailyRates;
	}


	/**
	 * Returns a date converter that uses the fixing calendar of the provided security to determine the previous
	 * business day for a provided accrual period date. The previous business day is the effective date for a daily rate.
	 */
	private UnaryOperator<Date> getFixingCalendarPreviousBusinessDateConverter(InvestmentSecurity eventSecurity) {
		BiFunction<Date, Integer, Date> calendarConverter = getSecurityFixingCalendarDateConverter(eventSecurity);
		return effectiveDate -> calendarConverter.apply(effectiveDate, -1);
	}


	/**
	 * Returns a converter to convert a provided date by a number of days using the applicable security business day calculator.
	 */
	private BiFunction<Date, Integer, Date> getSecurityFixingCalendarDateConverter(InvestmentSecurity eventSecurity) {
		Short calendarId = getFixingCalendarId(eventSecurity);
		if (calendarId == null) {
			return (effectiveDate, days) -> effectiveDate;
		}
		return (effectiveDate, days) -> getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(effectiveDate, calendarId), days);
	}


	/**
	 * Returns the fixing calendar for the provided security. If no fixing calendar is defined, null is returned.
	 */
	private Short getFixingCalendarId(InvestmentSecurity eventSecurity) {
		Number fixingCalendarId = (Number) getSecurityCustomColumn(eventSecurity, InvestmentSecurity.CUSTOM_FIELD_FIXING_CALENDAR);
		return fixingCalendarId != null ? fixingCalendarId.shortValue() : null;
	}


	private Object getSecurityCustomColumn(InvestmentSecurity eventSecurity, String customField) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(eventSecurity, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, customField, false);
	}


	/**
	 * Calculates the accrual for the reference rate of the swap's interest leg.
	 *
	 * @param notional   The initial notional amount
	 * @param dailyRates An array of rates for each day of the accrual
	 * @return The accrual for the reference rate
	 */
	protected abstract BigDecimal calculateReferenceRateAmount(InvestmentSecurityEvent paymentEvent, BigDecimal notional, AccrualDailyRate[] dailyRates, AccrualConfig accrualConfig);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public static final class AccrualDailyRate {

		private final Date rateDate;
		private final BigDecimal rate;


		private AccrualDailyRate(BigDecimal rate, Date rateDate) {
			this.rate = rate;
			this.rateDate = rateDate;
		}


		public static AccrualDailyRate forRateOnDate(BigDecimal rate, Date rateDate) {
			return new AccrualDailyRate(rate, rateDate);
		}


		public BigDecimal getRate() {
			return this.rate;
		}


		public Date getRateDate() {
			return this.rateDate;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}
}
