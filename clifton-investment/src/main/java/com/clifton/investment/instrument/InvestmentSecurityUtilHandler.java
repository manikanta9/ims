package com.clifton.investment.instrument;

import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;


/**
 * Provides standard security utility methods.
 *
 * @author mwacker
 */
public interface InvestmentSecurityUtilHandler {

	// The following CFI Constants were derived from online calculator at:  https://sec.report/CFI/
	public final static String CFI_FUTURES = "FXXXXX";
	public final static String CFI_EQUITIES = "EXXXXX";
	public final static String CFI_FUNDS = "EUXXXX";


	/**
	 * If transaction security has Cash Location Purpose defined on the hierarchy, lookups up the corresponding
	 * related holding account using COALESCE(Cash Location Purpose, Cash Location Purpose 2) and returns it if 1 found.
	 * If Cash Location Purpose is not defined, returns the holding account from the specified transaction info.
	 * If the purpose is defined but an active related holding account action on Transaction Date cannot be found
	 * or more than 1 account is found, throws an exception.
	 */
	public InvestmentAccount getCashLocationHoldingAccount(InvestmentTransactionInfo transactionInfo);


	/**
	 * Return the CFI Code of the option.
	 * <p>
	 * <a href='http://www.onixs.biz/fix-dictionary/4.4/app_6_d.html'>http://www.onixs.biz/fix-dictionary/4.4/app_6_d.html</a>
	 */
	public String getOptionCFICode(InvestmentSecurity option);


	/**
	 * Return the CFI Code of the Security.
	 *
	 * @return CFI Code as a string, null if mapping is not implemented.
	 */
	public String getCFICode(InvestmentSecurity security);


	/**
	 * Retrieves the specified custom column value for the given security using the {@link InvestmentSecurity#SECURITY_CUSTOM_FIELDS_GROUP_NAME standard security custom column
	 * group}.
	 *
	 * @param security   the security whose custom column value to retrieve
	 * @param columnName the custom column name
	 * @return the value of the custom column
	 */
	public Object getCustomColumnValue(InvestmentSecurity security, String columnName);


	/**
	 * Get the ISITC Code using the Specificity cache and the investment instrument.
	 */
	public ISITCCodes getISITCCodeStrict(InvestmentInstrument investmentInstrument);


	/**
	 * Get the Interest Rate 'Coupon Amount (%)' for the trade's security.
	 */
	public BigDecimal getInterestRate(InvestmentSecurity security);


	/**
	 * The coupon type describes how the bond's coupon amount is calculated at each coupon payment date.
	 */
	public String getCouponType(InvestmentSecurity security);


	/**
	 * For the provided security, determine the place of settlement using the specificity cache.
	 */
	public String getPlaceOfSettlementStrict(InvestmentSecurity investmentSecurity);


	/**
	 * For the provided security, returns the BTIC prefix value from the InvestmentSecurity's investment instrument.
	 * If no BTIC prefix is defined for the instrument, returns null.
	 */
	public String getBticPrefix(InvestmentSecurity security);


	/**
	 * Checks the hierarchy of the security to determine if it is a security that can be used in Continuous Linked Settlement (CLS) trades.
	 * Returns true if the security is a member of the "CLS Currency" InvestmentGroup otherwise returns false;
	 */
	public boolean isClsCurrency(InvestmentSecurity security);
}
