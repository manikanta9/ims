package com.clifton.investment.instrument.event.action;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.investment.instrument.event.action.search.InvestmentSecurityEventActionSearchForm;
import com.clifton.investment.instrument.event.action.search.InvestmentSecurityEventActionTypeSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class InvestmentSecurityEventActionServiceImpl implements InvestmentSecurityEventActionService {

	private AdvancedUpdatableDAO<InvestmentSecurityEventAction, Criteria> investmentSecurityEventActionDAO;
	private AdvancedUpdatableDAO<InvestmentSecurityEventActionType, Criteria> investmentSecurityEventActionTypeDAO;


	////////////////////////////////////////////////////////////////////////////
	///////       Investment Security Event Action Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventAction getInvestmentSecurityEventAction(int id) {
		return getInvestmentSecurityEventActionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentSecurityEventAction> getInvestmentSecurityEventActionList(InvestmentSecurityEventActionSearchForm searchForm) {
		return getInvestmentSecurityEventActionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentSecurityEventAction saveInvestmentSecurityEventAction(InvestmentSecurityEventAction eventAction) {
		return getInvestmentSecurityEventActionDAO().save(eventAction);
	}


	@Override
	public void deleteInvestmentSecurityEventAction(int id) {
		getInvestmentSecurityEventActionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////       Investment Security Event Action Type Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventActionType getInvestmentSecurityEventActionType(short id) {
		return getInvestmentSecurityEventActionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentSecurityEventActionType> getInvestmentSecurityEventActionTypeList(InvestmentSecurityEventActionTypeSearchForm searchForm) {
		return getInvestmentSecurityEventActionTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentSecurityEventActionType saveInvestmentSecurityEventActionType(InvestmentSecurityEventActionType actionType) {
		return getInvestmentSecurityEventActionTypeDAO().save(actionType);
	}


	@Override
	public void deleteInvestmentSecurityEventActionType(short id) {
		getInvestmentSecurityEventActionTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentSecurityEventAction, Criteria> getInvestmentSecurityEventActionDAO() {
		return this.investmentSecurityEventActionDAO;
	}


	public void setInvestmentSecurityEventActionDAO(AdvancedUpdatableDAO<InvestmentSecurityEventAction, Criteria> investmentSecurityEventActionDAO) {
		this.investmentSecurityEventActionDAO = investmentSecurityEventActionDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSecurityEventActionType, Criteria> getInvestmentSecurityEventActionTypeDAO() {
		return this.investmentSecurityEventActionTypeDAO;
	}


	public void setInvestmentSecurityEventActionTypeDAO(AdvancedUpdatableDAO<InvestmentSecurityEventActionType, Criteria> investmentSecurityEventActionTypeDAO) {
		this.investmentSecurityEventActionTypeDAO = investmentSecurityEventActionTypeDAO;
	}
}
