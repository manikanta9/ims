package com.clifton.investment.instrument.event;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>InvestmentSecurityEventDetailValidator</code> validates {@link InvestmentSecurityEventDetail} objects prior to inserting/updates
 * <p/>
 * Validates:
 * Event Type Details are supported for the security
 * Accrual Event Details - allowed only for those that do not have a Constant Rate
 * Detail Effective Date is between event accrual start/end dates
 *
 * @author manderson
 */
@Component
public class InvestmentSecurityEventDetailValidator extends SelfRegisteringDaoValidator<InvestmentSecurityEventDetail> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentSecurityEventDetail bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		InvestmentSecurityEvent event = bean.getEvent();
		String eventTypeName = event.getType().getName();
		if (eventTypeName.equals(InvestmentUtils.getAccrualEventTypeName(event.getSecurity()))) {
			AccrualMethods accrualMethod = InvestmentCalculatorUtils.getAccrualMethod(event.getSecurity());
			if (accrualMethod == null || accrualMethod.isConstantRate()) {
				throw new ValidationException("Security Event Details are only allowed for securities with non 'Constant Rate' accrual methods.");
			}
		}
		else if (eventTypeName.equals(InvestmentUtils.getAccrualEventTypeName2(event.getSecurity()))) {
			AccrualMethods accrualMethod = InvestmentCalculatorUtils.getAccrualMethod2(event.getSecurity());
			if (accrualMethod == null || accrualMethod.isConstantRate()) {
				throw new ValidationException("Security Event Details are only allowed for securities with non 'Constant Rate' accrual methods (2).");
			}
		}
		else {
			throw new ValidationException(eventTypeName + " event type is not allowed for " + event.getSecurity().getLabel());
		}

		Date accrualStartDate = event.getAccrualStartDate();
		if (event.getAdditionalDate() != null && DateUtils.isDateBefore(event.getAdditionalDate(), accrualStartDate, false)) {
			// Use Fixing Date (additional date) as the accrual start if it exists and is before the accrual start date
			accrualStartDate = event.getAdditionalDate();
		}
		ValidationUtils.assertTrue(DateUtils.isDateBetween(bean.getEffectiveDate(), accrualStartDate, event.getAccrualEndDate(), false),
				"Effective Date must be between accrual start and end dates for corresponding event.", "effectiveDate");
	}
}
