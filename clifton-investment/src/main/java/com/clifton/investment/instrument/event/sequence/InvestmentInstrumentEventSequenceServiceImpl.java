package com.clifton.investment.instrument.event.sequence;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.CalendarScheduleUtils;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class InvestmentInstrumentEventSequenceServiceImpl implements InvestmentInstrumentEventSequenceService {

	private CalendarBusinessDayService calendarBusinessDayService;
	private ScheduleApiService scheduleApiService;


	@Override
	public InvestmentSecurityEvent getSecurityEventNext(EventSequenceDefinition sequenceDefinition, Date date) {
		Schedule schedule = getCalendarSchedule(sequenceDefinition, date);
		Date eventDate = getScheduleApiService().getScheduleOccurrence(ScheduleOccurrenceCommand.forOccurrences(schedule, 1, date));
		if (eventDate != null) {
			Date nextEventDate = getScheduleApiService().getScheduleOccurrence(ScheduleOccurrenceCommand.forOccurrences(schedule, 1, DateUtils.addMinutes(date, 1)));
			return createEvent(eventDate, nextEventDate, sequenceDefinition, schedule);
		}
		return null;
	}


	@Override
	public List<InvestmentSecurityEvent> getSecurityEventList(EventSequenceDefinition sequenceDefinition, Date startDate, Date endDate) {
		List<InvestmentSecurityEvent> result = new ArrayList<>();

		Schedule schedule = getCalendarSchedule(sequenceDefinition, startDate);

		List<Date> dateList = getScheduleApiService().getScheduleOccurrences(ScheduleOccurrenceCommand.forOccurrencesBetween(schedule, startDate, endDate));
		if (sequenceDefinition.isGenerateFromMaturityBackward()) {
			dateList.sort((o1, o2) -> -1 * CompareUtils.compare(o1, o2));
		}
		for (int i = 0; i < dateList.size(); i++) {
			Date date = dateList.get(i);
			Date nextDate = i + 1 < dateList.size() ? dateList.get(i + 1) : null;
			InvestmentSecurityEvent event = createEvent(date, nextDate, sequenceDefinition, schedule);
			if (event == null) {
				break;
			}
			result.add(event);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private Schedule getCalendarSchedule(EventSequenceDefinition sequenceDefinition, Date startDate) {
		if (startDate == null) {
			startDate = sequenceDefinition.getSecurity().getStartDate();
		}

		Date sequenceEndDate;
		if (sequenceDefinition.getSequenceFrequency().getScheduleFrequency() == ScheduleFrequencies.ONCE || sequenceDefinition.isGenerateFromMaturityBackward()) {
			sequenceEndDate = sequenceDefinition.getSecurity().getEndDate();
		}
		else {
			sequenceEndDate = sequenceDefinition.getSecurity().getLastDeliveryDate() != null ? sequenceDefinition.getSecurity().getLastDeliveryDate() : sequenceDefinition.getSecurity().getEndDate();
		}

		Schedule schedule = new Schedule();
		schedule.setCalendar(sequenceDefinition.getCalendar().toCalendar());
		schedule.setBusinessDayConvention(sequenceDefinition.getBusinessDayConvention().name());

		schedule.setStartDate(startDate);
		schedule.setEndDate(sequenceEndDate);
		schedule.setStartTime(new Time(0));
		schedule.setFrequency(sequenceDefinition.getSequenceFrequency().getScheduleFrequency().name());
		schedule.setRecurrence(sequenceDefinition.getSequenceFrequency().getRecurrence());
		schedule.setScheduleType(getScheduleApiService().getScheduleTypeByName("Standard Schedules"));
		updateScheduleWithFrequency(schedule, sequenceDefinition);
		return schedule;
	}


	private InvestmentSecurityEvent createEvent(Date date, Date nextDate, EventSequenceDefinition sequenceDefinition, Schedule schedule) {
		InvestmentSecurityEvent result = new InvestmentSecurityEvent();

		result = sequenceDefinition.getDateCalculator().calculateDates(date, nextDate, result, sequenceDefinition, schedule);

		if (result != null) {
			result.setBeforeEventValue(sequenceDefinition.getBeforeEventValue());
			result.setAfterEventValue(sequenceDefinition.getAfterEventValue());
			result.setSecurity(sequenceDefinition.getSecurity());
			result.setType(sequenceDefinition.getType());
			result.setStatus(result.getType().getForceEventStatus());
		}
		return result;
	}


	private void updateScheduleWithFrequency(Schedule schedule, EventSequenceDefinition sequenceDefinition) {
		Date firstDeclareDate = sequenceDefinition.getSequenceStartDate();
		switch (sequenceDefinition.getSequenceFrequency().getScheduleFrequency()) {
			case WEEKLY:
				CalendarScheduleUtils.updateWeekDay(schedule, DateUtils.getDayOfWeek(firstDeclareDate));
				break;
			case MONTHLY:
				// check if the schedule is using the last business day of the month
				Date nextBusinessDay = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(firstDeclareDate, schedule.getCalendar().getId()));
				if (sequenceDefinition.isGenerateFromMaturityBackward()) {
					schedule.setDayOfMonth(DateUtils.getDayOfMonth(sequenceDefinition.getSecurity().getEndDate()));
				}
				else if ((sequenceDefinition.getValuationConvention() == EventSequenceRecurrenceConventions.MONTH_END_RECURRENCE) ||
						(DateUtils.getMonthOfYear(firstDeclareDate) != DateUtils.getMonthOfYear(nextBusinessDay))) {
					schedule.setDayOfMonth(1);
					schedule.setCountFromLastDayOfMonth(true);
				}
				else {
					schedule.setDayOfMonth(DateUtils.getDayOfMonth(firstDeclareDate));
				}
				break;
			case YEARLY:
				if (sequenceDefinition.isGenerateFromMaturityBackward()) {
					schedule.setDayOfMonth(DateUtils.getDayOfMonth(sequenceDefinition.getSecurity().getEndDate()));
				}
				else {
					schedule.setDayOfMonth(DateUtils.getDayOfMonth(sequenceDefinition.getSequenceStartDate()));
				}
				CalendarScheduleUtils.updateMonth(schedule, DateUtils.getMonthOfYear(firstDeclareDate));
				break;
			case ONCE:
				schedule.setStartDate(schedule.getEndDate());
				schedule.setEndDate(null);
				break;
			default:
				break;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}
}
