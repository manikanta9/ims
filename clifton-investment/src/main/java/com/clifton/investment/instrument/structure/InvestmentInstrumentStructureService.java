package com.clifton.investment.instrument.structure;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.structure.search.InvestmentSecurityStructureAllocationSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentInstrumentStructureService</code> ...
 *
 * @author manderson
 */
public interface InvestmentInstrumentStructureService {

	////////////////////////////////////////////////////////////////////////////////
	//////////      Investment Instrument Structure Business Methods     ///////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates the list of {@link InvestmentInstrumentStructureWeight} which is required to be populated for every instrument structure
	 */
	public InvestmentInstrumentStructure getInvestmentInstrumentStructure(int id);


	public List<InvestmentInstrumentStructure> getInvestmentInstrumentStructureListByInstrument(int instrumentId);


	public InvestmentInstrumentStructure saveInvestmentInstrumentStructure(InvestmentInstrumentStructure bean);


	/**
	 * Uses the selected structure with id as a model and ends the existing on the day before the startDate
	 * New one copies the structure and instrument weight list using start & end dates
	 * If copying security structures, will also make copies of the security structures
	 * <p/>
	 * Most cases will want to copy the entire structure and then go back and make the few updates for the changes that are needed.
	 */
	public void copyInvestmentInstrumentStructure(int id, Date newStartDate, Date newEndDate, boolean copySecurityStructures);


	public void deleteInvestmentInstrumentStructure(int id);


	////////////////////////////////////////////////////////////////////////////////
	////////    Investment Instrument Security Structure Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = InvestmentInstrumentStructure.class)
	public List<InvestmentInstrumentSecurityStructure> getInvestmentInstrumentSecurityStructureListByStructure(int structureId);


	@SecureMethod(dtoClass = InvestmentInstrumentStructure.class)
	public List<InvestmentInstrumentSecurityStructure> getInvestmentInstrumentSecurityStructureListBySecurity(int securityId);


	@SecureMethod(dtoClass = InvestmentInstrumentStructure.class)
	public InvestmentInstrumentSecurityStructure getInvestmentInstrumentSecurityStructure(int structureId, int securityId);


	@SecureMethod(dtoClass = InvestmentInstrumentStructure.class)
	public void saveInvestmentInstrumentSecurityStructure(InvestmentInstrumentSecurityStructure bean);


	@SecureMethod(dtoClass = InvestmentInstrumentStructure.class)
	public void deleteInvestmentInstrumentSecurityStructure(int structureId, int securityId);


	////////////////////////////////////////////////////////////////////////////////
	////////    Investment Security Structure Allocation Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////////


	public void rebuildInvestmentSecurityStructureAllocation(Integer structureId, int securityId, Date startDate, Date endDate, boolean reprocessExisting);


	/**
	 * Returns a list of current securities and weights for a given Security (Structured Index) and Date
	 * Based on the Structure Type, it pulls dynamic values, else saved values.  If saved values are used, but don't exist, will save them
	 * <p/>
	 * If structureId is null - pulls the active one
	 *
	 * @param calculateCurrentForNextBusinessDay - Used during replication processing to use prices for the balance date, but the current security calculator will use the next business day
	 *                                           NOTE: This option will likely be removed because now the replication can re-set the current security after the allocations are calculated for the balance date
	 */
	public List<InvestmentSecurityStructureAllocation> getInvestmentSecurityStructureAllocationListForSecurityAndDate(Integer structureId, int securityId, Date date,
	                                                                                                                  boolean calculateCurrentForNextBusinessDay);


	/**
	 * Uses saved values only to return history
	 */
	public List<InvestmentSecurityStructureAllocation> getInvestmentSecurityStructureAllocationList(InvestmentSecurityStructureAllocationSearchForm searchForm);
}
