package com.clifton.investment.instrument.event.cache;

import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentSecurityEventPreviousForAccrualEndDateCacheImpl</code> class caches InvestmentSecurityEvent id's for accrual end date lookup.
 * <p>
 * Caching is done by securityId so that we can clear only security specific caches on changes.
 *
 * @author vgomelsky
 */
@Component
public class InvestmentSecurityEventPreviousForAccrualEndDateCacheImpl extends InvestmentSecurityEventForAccrualEndDateCacheImpl {

	// class name will be used for cache name and the rest is the same
}
