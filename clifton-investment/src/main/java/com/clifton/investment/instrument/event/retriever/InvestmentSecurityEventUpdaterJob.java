package com.clifton.investment.instrument.event.retriever;


import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentSecurityEventUpdaterJob</code> class inserts latest event of the specified type if one doesn't already exist.
 *
 * @author akorver
 */
public class InvestmentSecurityEventUpdaterJob implements Task, StatusHolderObjectAware<Status> {

	/**
	 * Investment group to run update over
	 */
	private Short investmentGroupId;

	/**
	 * Security group to exclude from this update run
	 */
	private Short excludeInvestmentSecurityGroupId;

	/**
	 * Security event update type used, EX. Coupon, Factor Change, Dividend, Stock Split
	 */
	private Short investmentSecurityEventTypeId;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventRetrieverService investmentSecurityEventRetrieverService;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;


	private StatusHolderObject<Status> statusHolder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		int skippedExisting = 0, externalCalls = 0, eventsAdded = 0, errors = 0;

		InvestmentSecurityEventType investmentSecurityEventType = getInvestmentSecurityEventService().getInvestmentSecurityEventType(getInvestmentSecurityEventTypeId());
		ValidationUtils.assertNotNull(investmentSecurityEventType, "Cannot find investment security event type for id = " + getInvestmentSecurityEventTypeId());

		String eventTypeName = investmentSecurityEventType.getName();

		// get active securities under hierarchies that allow the specified security event
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInvestmentGroupId(getInvestmentGroupId());
		searchForm.setExcludeInvestmentSecurityGroupId(getExcludeInvestmentSecurityGroupId());
		searchForm.setEventTypeName(eventTypeName);
		searchForm.setInstrumentIsInactive(false);
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);

		Date today = new Date();
		Status status = this.statusHolder.getStatus();
		int currentSecurity = 0;
		for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
			// check if event already exists for current period
			InvestmentSecurityEventSearchForm eventSearchForm = new InvestmentSecurityEventSearchForm();
			eventSearchForm.setDateBetweenDeclareAndExDate(today);
			eventSearchForm.setTypeName(eventTypeName);
			eventSearchForm.setSecurityId(security.getId());

			try {
				currentSecurity++;
				status.setMessage("Securities: " + currentSecurity + " of " + securityList.size() + "; Skipped Existing: " + skippedExisting + "; External Calls: " + externalCalls + "; Events Added: " + eventsAdded + "; Errors: " + errors);

				InvestmentSecurityEvent existingEvent = CollectionUtils.getFirstElement(getInvestmentSecurityEventService().getInvestmentSecurityEventList(eventSearchForm));
				// if there is no event with a date range where today would fall within that range then go fetch the event from Bloomberg.
				// i.e. if today is 4/6/2011 and there is an event with range 4/1/11 - 5/1/11 then we do not need to go to Bloomberg.  If there is no such event, we need to get it.
				// Exclude Dividends: could have multiple declared dividends per period
				if (existingEvent == null || InvestmentSecurityEventType.CASH_DIVIDEND_PAYMENT.equals(eventTypeName)) {
					String[] typeNames = {eventTypeName};
					eventsAdded += getInvestmentSecurityEventRetrieverService().loadInvestmentSecurityEvent(security.getId(), typeNames);
					externalCalls++;
				}
				else {
					skippedExisting++;
				}
			}
			catch (Exception e) {
				errors++;
				status.addError(security.getLabel() + ": " + ExceptionUtils.getDetailedMessage(e));

				// log non-validation exceptions so that more research can be done
				//if (!CoreExceptionUtils.isValidationException(ex)) { // TODO: populate method always throws ValidationException: change this???
				LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "InvestmentSecurityEventUpdaterJob: " + ExceptionUtils.getNormalizedMessage(CoreExceptionUtils.getLoggableOrOriginalException(e))));
				//}
			}
		}

		StringBuilder result = new StringBuilder();
		result.append("Securities: ").append(CollectionUtils.getSize(securityList));
		result.append("; Skipped Existing: ").append(skippedExisting);
		result.append("; External Calls: ").append(externalCalls);
		result.append("; Events Added: ").append(eventsAdded);
		result.append("; Errors: ").append(errors);
		status.setMessage(result.toString());

		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public InvestmentSecurityEventRetrieverService getInvestmentSecurityEventRetrieverService() {
		return this.investmentSecurityEventRetrieverService;
	}


	public void setInvestmentSecurityEventRetrieverService(InvestmentSecurityEventRetrieverService investmentSecurityEventRetrieverService) {
		this.investmentSecurityEventRetrieverService = investmentSecurityEventRetrieverService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public Short getExcludeInvestmentSecurityGroupId() {
		return this.excludeInvestmentSecurityGroupId;
	}


	public void setExcludeInvestmentSecurityGroupId(Short excludeInvestmentSecurityGroupId) {
		this.excludeInvestmentSecurityGroupId = excludeInvestmentSecurityGroupId;
	}


	public Short getInvestmentSecurityEventTypeId() {
		return this.investmentSecurityEventTypeId;
	}


	public void setInvestmentSecurityEventTypeId(Short investmentSecurityEventTypeId) {
		this.investmentSecurityEventTypeId = investmentSecurityEventTypeId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}
}
