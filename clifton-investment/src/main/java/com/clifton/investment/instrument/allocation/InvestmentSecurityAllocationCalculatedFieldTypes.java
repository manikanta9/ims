package com.clifton.investment.instrument.allocation;


/**
 * The <code>InvestmentSecurityAllocationCalculatedFieldTypes</code> define the field type that
 * the allocated security calculator builds.  i.e. Prices, Month End Returns
 *
 * @author manderson
 */
public enum InvestmentSecurityAllocationCalculatedFieldTypes {

	PRICE(true, false), //  System Calculates Prices based on allocations
	MONTHLY_RETURN(true, true), // System Calculates Monthly Return based on allocations
	NONE(false, false); // System doesn't calculate any data - just holds the list of allocations for the security - Prices or other data should be imported from another source or uploaded manually


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	InvestmentSecurityAllocationCalculatedFieldTypes(boolean systemCalculated, boolean monthEndOnly) {
		this.systemCalculated = systemCalculated;
		this.monthEndOnly = monthEndOnly;
	}

	////////////////////////////////////////////////////////////////////////////////

	private final boolean systemCalculated;
	private final boolean monthEndOnly;

	////////////////////////////////////////////////////////////////////////////////


	public boolean isMonthEndOnly() {
		return this.monthEndOnly;
	}


	public boolean isSystemCalculated() {
		return this.systemCalculated;
	}
}
