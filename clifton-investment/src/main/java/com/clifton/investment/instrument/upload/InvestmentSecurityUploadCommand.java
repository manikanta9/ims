package com.clifton.investment.instrument.upload;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.system.upload.SystemUploadCommand;


/**
 * The <code>InvestmentSecurityUploadCommand</code> extends the System Upload
 * however has hierarchy specific custom fields available for the security that will be applied
 * to the securities during the insert process.
 *
 * @author Mary Anderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class InvestmentSecurityUploadCommand extends SystemUploadCommand {


	/**
	 * If uploading for a specific hierarchy - In this case hierarchy is NOT specified in the file
	 */
	private InvestmentInstrumentHierarchy hierarchy;

	/**
	 * When uploading across hierarchies - all hierarchies must reference this as their parent
	 * Required if a single hierarchy is not selected - In this case only the child hierarchy is specified in the file
	 */
	private InvestmentInstrumentHierarchy parentHierarchy;

	/**
	 * Used on screen to filter hierarchy selection, OR when parent hierarchy is used, used
	 * to limit what child hierarchies we are looking for for sample files.
	 * Also, if true, then trading CCY can be entered on screen, and applied to instruments during inserts
	 * and will be used for and 1:1 security that doesn't have trading currency specified for the row in the file
	 */
	private boolean oneSecurityPerInstrument;


	/**
	 * Used for 1:1 Securities only - field on the instrument that we need to save the security
	 */
	private InvestmentSecurity tradingCurrency;

	/**
	 * If populated - first check event types for Hierarchy to make sure there is at least one event
	 * type that can be auto-generated.  Otherwise returns exception to user.
	 * <p/>
	 * For each event type for the hierarchy that can be auto-generated, will also auto-generate all events after all securities are uploaded
	 */
	private boolean autoGenerateEvents;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return "InvestmentSecurity";
	}


	@Override
	@ValueIgnoringGetter
	public String getColumnGroupName() {
		return InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isSimple() {
		return true;
	}


	// Overrides - Investment Security Upload is all or nothing, do not insert fk beans, and


	@Override
	@ValueIgnoringGetter
	public boolean isPartialUploadAllowed() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		// Because of how 1:1 securities are saved, we can't support updates with this upload
		// Updates can easily be supported through the generic upload
		return FileUploadExistingBeanActions.SKIP;
	}


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyUploadSpecificProperties(IdentityObject bean, DataRow row) {
		// Set hierarchy if defined
		if (getHierarchy() != null) {
			BeanUtils.setPropertyValue(bean, "instrument.hierarchy", getHierarchy());
		}
		// Else set Parent Hierarchy - so we can easily look up hierarchy based on name & parent
		else if (getParentHierarchy() != null) {
			BeanUtils.setPropertyValue(bean, "instrument.hierarchy.parent", getParentHierarchy());
		}
		// Set this so we have it - otherwise looking up by hierarchy only returns all instruments in that hierarchy
		// And we skip those that exist anyway
		if (isOneSecurityPerInstrument()) {
			BeanUtils.setPropertyValue(bean, "instrument.identifierPrefix", BeanUtils.getPropertyValue(bean, "symbol"));
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentHierarchy getHierarchy() {
		return this.hierarchy;
	}


	public void setHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		this.hierarchy = hierarchy;
	}


	public InvestmentInstrumentHierarchy getParentHierarchy() {
		return this.parentHierarchy;
	}


	public void setParentHierarchy(InvestmentInstrumentHierarchy parentHierarchy) {
		this.parentHierarchy = parentHierarchy;
	}


	public boolean isOneSecurityPerInstrument() {
		return this.oneSecurityPerInstrument;
	}


	public void setOneSecurityPerInstrument(boolean oneSecurityPerInstrument) {
		this.oneSecurityPerInstrument = oneSecurityPerInstrument;
	}


	public InvestmentSecurity getTradingCurrency() {
		return this.tradingCurrency;
	}


	public void setTradingCurrency(InvestmentSecurity tradingCurrency) {
		this.tradingCurrency = tradingCurrency;
	}


	public boolean isAutoGenerateEvents() {
		return this.autoGenerateEvents;
	}


	public void setAutoGenerateEvents(boolean autoGenerateEvents) {
		this.autoGenerateEvents = autoGenerateEvents;
	}
}
