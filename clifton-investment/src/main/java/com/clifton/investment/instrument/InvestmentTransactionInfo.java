package com.clifton.investment.instrument;

import java.util.Date;


/**
 * The InvestmentTransactionInfo interface defines an investment transaction with Transaction Date, Settlement Date,
 * and other fields from {@link InvestmentObjectInfo}.
 *
 * @author vgomelsky
 */
public interface InvestmentTransactionInfo extends InvestmentObjectInfo {

	public Date getTransactionDate();


	public Date getSettlementDate();
}
