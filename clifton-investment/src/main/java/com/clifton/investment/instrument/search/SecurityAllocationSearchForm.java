package com.clifton.investment.instrument.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


/**
 * The <code>SecurityAllocationSearchForm</code> ...
 *
 * @author manderson
 */
public class SecurityAllocationSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(required = true, searchField = "parentInvestmentSecurity.id")
	private Integer parentInvestmentSecurityId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "symbol,name", sortField = "symbol", leftJoin = true)
	private String investmentSecurityLabel;

	@SearchField(searchField = "investmentInstrument.id")
	private Integer investmentInstrumentId;

	@SearchField(searchFieldPath = "investmentInstrument", searchField = "identifierPrefix,name", sortField = "name", leftJoin = true)
	private String investmentInstrumentLabel;

	@SearchField(searchField = "currentSecurityCalculatorBean.id")
	private Integer currentSecurityCalculatorBeanId;

	@SearchField(searchFieldPath = "currentSecurityCalculatorBean", searchField = "name")
	private String currentSecurityCalculatorBeanName;

	@SearchField
	private BigDecimal allocationWeight;

	@SearchField
	private String note;

	@SearchField(searchField = "overrideExchange.id")
	private Short overrideExchangeId;

	@SearchField(searchFieldPath = "overrideExchange", searchField = "name")
	private String overrideExchangeName;

	@SearchField(searchFieldPath = "overrideExchange", searchField = "exchangeCode")
	private String overrideExchangeCode;

	@SearchField(searchField = "overrideExchange", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean exchangeOverridden;


	/**
	 * Not a search field. Can be used to request specific security naming convention.
	 * For example, set to "Bloomberg" to request Bloomberg symbol naming convention: "AAPL UW Equity"
	 */
	private String securityConventionDataSource;

	private boolean disableNoActiveAllocationsValidationError;

	/**
	 * Not a search field. This optional value is used to override the default data source used for FX Rate lookups,
	 * is passed to the MarketDataInvestmentSecurityAllocationServiceImpl::getMarketDataInvestmentSecurityAllocationList.
	 */
	private Short fxRateMarketDataSourceId;


	////////////////////////////////////////////////////////////////////////////


	public Integer getParentInvestmentSecurityId() {
		return this.parentInvestmentSecurityId;
	}


	public void setParentInvestmentSecurityId(Integer parentInvestmentSecurityId) {
		this.parentInvestmentSecurityId = parentInvestmentSecurityId;
	}


	public String getInvestmentSecurityLabel() {
		return this.investmentSecurityLabel;
	}


	public void setInvestmentSecurityLabel(String investmentSecurityLabel) {
		this.investmentSecurityLabel = investmentSecurityLabel;
	}


	public String getInvestmentInstrumentLabel() {
		return this.investmentInstrumentLabel;
	}


	public void setInvestmentInstrumentLabel(String investmentInstrumentLabel) {
		this.investmentInstrumentLabel = investmentInstrumentLabel;
	}


	public String getCurrentSecurityCalculatorBeanName() {
		return this.currentSecurityCalculatorBeanName;
	}


	public void setCurrentSecurityCalculatorBeanName(String currentSecurityCalculatorBeanName) {
		this.currentSecurityCalculatorBeanName = currentSecurityCalculatorBeanName;
	}


	public BigDecimal getAllocationWeight() {
		return this.allocationWeight;
	}


	public void setAllocationWeight(BigDecimal allocationWeight) {
		this.allocationWeight = allocationWeight;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Integer getCurrentSecurityCalculatorBeanId() {
		return this.currentSecurityCalculatorBeanId;
	}


	public void setCurrentSecurityCalculatorBeanId(Integer currentSecurityCalculatorBeanId) {
		this.currentSecurityCalculatorBeanId = currentSecurityCalculatorBeanId;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getSecurityConventionDataSource() {
		return this.securityConventionDataSource;
	}


	public void setSecurityConventionDataSource(String securityConventionDataSource) {
		this.securityConventionDataSource = securityConventionDataSource;
	}


	public Short getOverrideExchangeId() {
		return this.overrideExchangeId;
	}


	public void setOverrideExchangeId(Short overrideExchangeId) {
		this.overrideExchangeId = overrideExchangeId;
	}


	public String getOverrideExchangeName() {
		return this.overrideExchangeName;
	}


	public void setOverrideExchangeName(String overrideExchangeName) {
		this.overrideExchangeName = overrideExchangeName;
	}


	public String getOverrideExchangeCode() {
		return this.overrideExchangeCode;
	}


	public void setOverrideExchangeCode(String overrideExchangeCode) {
		this.overrideExchangeCode = overrideExchangeCode;
	}


	public Boolean getExchangeOverridden() {
		return this.exchangeOverridden;
	}


	public void setExchangeOverridden(Boolean exchangeOverridden) {
		this.exchangeOverridden = exchangeOverridden;
	}

	// TODO - ADD OTHER FIELDS FOR SORTING???


	public boolean isDisableNoActiveAllocationsValidationError() {
		return this.disableNoActiveAllocationsValidationError;
	}


	public void setDisableNoActiveAllocationsValidationError(boolean disableNoActiveAllocationsValidationError) {
		this.disableNoActiveAllocationsValidationError = disableNoActiveAllocationsValidationError;
	}


	public Short getFxRateMarketDataSourceId() {
		return this.fxRateMarketDataSourceId;
	}


	public void setFxRateMarketDataSourceId(Short fxRateMarketDataSourceId) {
		this.fxRateMarketDataSourceId = fxRateMarketDataSourceId;
	}
}
