package com.clifton.investment.instrument.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionType;
import org.springframework.stereotype.Component;


/**
 * The DAO validator for {@link InvestmentSecurity} entities.
 */
@Component
public class InvestmentSecurityEventActionTypeValidator extends SelfRegisteringDaoValidator<InvestmentSecurityEventActionType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(InvestmentSecurityEventActionType actionType, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			ValidationUtils.assertTrue(actionType.isManual(), "Action Type: \"" + actionType.getLabel() + "\" is not a manual action type, and therefore cannot be deleted.");
		}
		else {
			validateFields(actionType);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks that all required fields are populated on the action type before allowing it to be saved.
	 */
	private void validateFields(InvestmentSecurityEventActionType actionType) {
		ValidationUtils.assertNotNull(actionType.getName(), "The action type requires a name.");
		ValidationUtils.assertNotNull(actionType.getDescription(), "The action type requires a description.");
		ValidationUtils.assertNotNull(actionType.getActionBean(), "The action type must have an Action Bean defined.");
	}
}
