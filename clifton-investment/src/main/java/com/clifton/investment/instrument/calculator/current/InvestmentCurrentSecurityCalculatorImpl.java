package com.clifton.investment.instrument.calculator.current;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentCurrentSecurityCalculatorImpl</code> provides various properties and generic support
 * for current security calculations.
 *
 * @author manderson
 */
public class InvestmentCurrentSecurityCalculatorImpl extends BaseInvestmentCurrentSecurityCalculator {


	/**
	 * Determines whether or not to first check for a matching security from the current security group
	 */
	private boolean checkCurrentSecurityGroup = false;

	/**
	 * If want to use a different group other than the "Current Securities" default group
	 */
	private Short useSecurityGroupAsCurrentSecurities = null;

	/**
	 * If should only check against the group if we have positions for the "current" one
	 * If no positions, or not existsInGroupOnly - pulls active matching securities
	 */
	private boolean existsInGroupOnly = false;


	/**
	 * Allows filtering to not the one ending first, but at least x days away
	 */
	private Integer minDaysToMaturity = null;

	/**
	 * Allows filtering to one that isn't too far out
	 */
	private Integer maxDaysToMaturity = null;

	///////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurity calculateImpl(InvestmentInstrument instrument, InvestmentSecurityGroup securityGroup, InvestmentSecurity underlyingSecurity, Date date) {
		// Keep this list in case nothing matching from the "current" group
		List<InvestmentSecurity> list = getActiveSecuritiesForAllocation(instrument, securityGroup, underlyingSecurity, date);
		ValidationUtils.assertNotEmpty(list, "There are NO active securities available.");

		if (isCheckCurrentSecurityGroup()) {
			List<InvestmentSecurity> filteredList;
			if (getUseSecurityGroupAsCurrentSecurities() != null) {
				filteredList = CollectionUtils.getIntersection(list, getInvestmentSecurityGroupService().getInvestmentSecurityListForSecurityGroup(getUseSecurityGroupAsCurrentSecurities(), null, null));
			}
			else {
				filteredList = CollectionUtils.getIntersection(list, getInvestmentSecurityGroupService().getInvestmentSecurityCurrentList(null, null));
			}

			// Set filtered list as what to use if exists in group only = true or there is at least one security in it
			if (isExistsInGroupOnly()) {
				if (!CollectionUtils.isEmpty(filteredList)) {
					list = filteredList;
				}
			}
		}

		// Return the first one sorted by end date asc
		return CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(list, InvestmentSecurity::getEndDate, true));
	}

	///////////////////////////////////////////////////////////////////


	private List<InvestmentSecurity> getActiveSecuritiesForAllocation(InvestmentInstrument instrument, InvestmentSecurityGroup securityGroup, InvestmentSecurity underlyingSecurity, Date date) {
		Integer instrumentId = instrument != null ? instrument.getId() : null;
		Integer underlyingSecurityId = underlyingSecurity != null ? underlyingSecurity.getId() : null;
		Short groupId = securityGroup != null ? securityGroup.getId() : null;

		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInstrumentId(instrumentId);
		searchForm.setUnderlyingSecurityId(underlyingSecurityId);
		searchForm.setSecurityGroupId(groupId);
		searchForm.setActiveOnDate(date);
		if (getMinDaysToMaturity() != null) {
			searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.GREATER_THAN, DateUtils.addDays(date, getMinDaysToMaturity())));
		}

		if (getMaxDaysToMaturity() != null) {
			searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.LESS_THAN, DateUtils.addDays(date, getMaxDaysToMaturity())));
		}

		return getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
	}

	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	public boolean isCheckCurrentSecurityGroup() {
		return this.checkCurrentSecurityGroup;
	}


	public void setCheckCurrentSecurityGroup(boolean checkCurrentSecurityGroup) {
		this.checkCurrentSecurityGroup = checkCurrentSecurityGroup;
	}


	public boolean isExistsInGroupOnly() {
		return this.existsInGroupOnly;
	}


	public void setExistsInGroupOnly(boolean existsInGroupOnly) {
		this.existsInGroupOnly = existsInGroupOnly;
	}


	public Short getUseSecurityGroupAsCurrentSecurities() {
		return this.useSecurityGroupAsCurrentSecurities;
	}


	public void setUseSecurityGroupAsCurrentSecurities(Short useSecurityGroupAsCurrentSecurities) {
		this.useSecurityGroupAsCurrentSecurities = useSecurityGroupAsCurrentSecurities;
	}


	public Integer getMinDaysToMaturity() {
		return this.minDaysToMaturity;
	}


	public void setMinDaysToMaturity(Integer minDaysToMaturity) {
		this.minDaysToMaturity = minDaysToMaturity;
	}


	public Integer getMaxDaysToMaturity() {
		return this.maxDaysToMaturity;
	}


	public void setMaxDaysToMaturity(Integer maxDaysToMaturity) {
		this.maxDaysToMaturity = maxDaysToMaturity;
	}
}
