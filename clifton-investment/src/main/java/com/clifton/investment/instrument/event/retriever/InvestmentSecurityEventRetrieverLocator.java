package com.clifton.investment.instrument.event.retriever;


import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;


/**
 * The <code>InvestmentSecurityEventRetrieverLocator</code> interface allows lookup
 * of event type specific InvestmentSecurityEventRetriever(s).
 *
 * @author vgomelsky
 */
public interface InvestmentSecurityEventRetrieverLocator {

	/**
	 * Returns event retriever for the specified event type.
	 */
	public InvestmentSecurityEventRetriever locate(InvestmentSecurityEventType eventType, InvestmentInstrumentHierarchy hierarchy);
}
