package com.clifton.investment.instrument.calculator.current;


import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureWeight;
import com.clifton.investment.replication.InvestmentReplicationAllocation;

import java.util.Date;


/**
 * The <code>InvestmentCurrentSecurityCalculator</code> defines interface with two types of
 * calculation methods -
 * <p/>
 * One supports returns the current security for an instrument
 * Used by {@link InvestmentSecurityStructureWeight}
 * The second supports a more detailed configuration for replication allocations
 * which could also use a security group and rely on the client's positions already held, etc.
 * Used by {@link InvestmentReplicationAllocation}
 * <p/>
 * Many times both can use the same calculators, but validation methods are available to verify setup applies to the calculator
 * i.e. Futures Schedule calculator for replication allocations wouldn't validate if a security group was selected.
 *
 * @author manderson
 */
public interface InvestmentCurrentSecurityCalculator {

	////////////////////////////////////////////////////////////////////////////
	////////////              Validation Methods                     ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates that the calculator is an applicable selection for the {@link InvestmentSecurityStructureWeight}
	 *
	 * @param securityStructureWeight
	 */
	public void validateInvestmentSecurityStructureWeight(InvestmentSecurityStructureWeight securityStructureWeight);


	/**
	 * Validates that the calculator is an applicable selection for the {@link InvestmentReplicationAllocation}
	 *
	 * @param replicationAllocation
	 */
	public void validateInvestmentReplicationAllocation(InvestmentReplicationAllocation replicationAllocation);


	/**
	 * Validates that the calculator is an applicable selection for the {@link InvestmentSecurityAllocation}
	 *
	 * @param securityAllocation
	 */
	public void validateInvestmentSecurityAllocation(InvestmentSecurityAllocation securityAllocation);


	////////////////////////////////////////////////////////////////////////////
	////////////              Calculation Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates and sets the current security for the passed {@link InvestmentSecurityStructureAllocation} object
	 * Passes the securityStructureAllocation so can copy additional information when then used to calculate replications
	 *
	 * @param securityStructureAllocation
	 */
	public void calculateCurrentSecurity(InvestmentSecurityStructureAllocation securityStructureAllocation, Date date);


	public InvestmentSecurity calculateCurrentSecurity(InvestmentReplicationAllocation replicationAllocation, Date date);


	/**
	 * Calculates and returns the current security for the passed {@link InvestmentSecurityAllocation} object
	 * For this case currently no other information necessary to populate so doesn't set anything else and just returns the current security
	 *
	 * @param securityAllocation
	 */
	public InvestmentSecurity calculateCurrentSecurity(InvestmentSecurityAllocation securityAllocation, Date date);
}
