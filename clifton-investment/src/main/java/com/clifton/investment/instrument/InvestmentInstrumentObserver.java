package com.clifton.investment.instrument;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.investment.setup.group.InvestmentGroupService;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>InvestmentInstrumentObserver</code> will delete investment group ties to instruments
 * prior to deleting the instrument.
 * <p/>
 * On Inserts will schedule rebuilding Investment Groups that are system defined.
 *
 * @author manderson
 */
@Component
public class InvestmentInstrumentObserver extends SelfRegisteringDaoObserver<InvestmentInstrument> {

	private InvestmentGroupService investmentGroupService;
	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<InvestmentInstrument> dao, DaoEventTypes event, InvestmentInstrument bean) {
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
		if (event.isDelete()) {
			getInvestmentGroupService().deleteInvestmentGroupMatrixListByInstrument(bean.getId());
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<InvestmentInstrument> dao, DaoEventTypes event, InvestmentInstrument bean, Throwable e) {
		if (e == null && isRebuildInvestmentGroup(event, bean, dao)) {
			// asynchronous support
			String runId = "SYSTEM_DEFINED";
			final Date scheduledDate = DateUtils.addSeconds(new Date(), 30);

			Runner runner = new AbstractStatusAwareRunner("INVESTMENT-GROUP", runId, scheduledDate) {

				@Override
				public void run() {
					Status result = getInvestmentGroupService().rebuildInvestmentGroupItemInstrumentList();
					getStatus().mergeStatus(result);
				}
			};
			getRunnerHandler().rescheduleRunner(runner);
		}
	}


	/**
	 * REBUILD IF INSERT OR UPDATE AND HIERARCHY FIELD CHANGED
	 */
	private boolean isRebuildInvestmentGroup(DaoEventTypes event, InvestmentInstrument bean, ReadOnlyDAO<InvestmentInstrument> dao) {
		if (event.isInsert()) {
			return true;
		}
		if (event.isUpdate()) {
			InvestmentInstrument originalBean = getOriginalBean(dao, bean);
			if (originalBean != null && !originalBean.getHierarchy().equals(bean.getHierarchy())) {
				return true;
			}
		}
		return false;
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
