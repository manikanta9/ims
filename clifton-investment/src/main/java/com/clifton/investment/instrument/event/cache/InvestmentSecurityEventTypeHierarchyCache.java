package com.clifton.investment.instrument.event.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.investment.instrument.event.InvestmentSecurityEventTypeHierarchy;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentSecurityEventTypeHierarchyCache</code> caches the InvestmentSecurityEventTypeHierarchy
 * object for key EventTypeID_HierarchyID
 *
 * @author manderson
 */
@Component
public class InvestmentSecurityEventTypeHierarchyCache extends SelfRegisteringCompositeKeyDaoCache<InvestmentSecurityEventTypeHierarchy, Short, Short> {


	@Override
	protected String getBeanKey1Property() {
		return "referenceOne.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "referenceTwo.id";
	}


	@Override
	protected Short getBeanKey1Value(InvestmentSecurityEventTypeHierarchy bean) {
		if (bean.getReferenceOne() != null) {
			return bean.getReferenceOne().getId();
		}
		return null;
	}


	@Override
	protected Short getBeanKey2Value(InvestmentSecurityEventTypeHierarchy bean) {
		if (bean.getReferenceTwo() != null) {
			return bean.getReferenceTwo().getId();
		}
		return null;
	}
}
