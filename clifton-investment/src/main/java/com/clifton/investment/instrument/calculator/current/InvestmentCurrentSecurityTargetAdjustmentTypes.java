package com.clifton.investment.instrument.calculator.current;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentCurrentSecurityTargetAdjustmentTypes</code> defines the various ways the target for the current security can be adjusted based on other
 * contract positions.
 *
 * @author manderson
 */
public enum InvestmentCurrentSecurityTargetAdjustmentTypes {

	// When non-current security is held, those securities will always follow the "Target Follows Actual" approach. The current security target will be adjusted to get the difference, regardless if we need to open/close positions or are over/underweight.
	DIFFERENCE_ALL("Difference - All") {
		@Override
		public Map<InvestmentSecurity, BigDecimal> calculateTargetAmountMap(BigDecimal totalTarget, InvestmentSecurity currentSecurity, Map<InvestmentSecurity, BigDecimal> actualExposureAmountMap) {
			Map<InvestmentSecurity, BigDecimal> targetMap = new HashMap<>();
			BigDecimal remainingTarget = totalTarget;
			for (Map.Entry<InvestmentSecurity, BigDecimal> investmentSecurityBigDecimalEntry : actualExposureAmountMap.entrySet()) {
				if (!(investmentSecurityBigDecimalEntry.getKey()).equals(currentSecurity)) {
					BigDecimal actualExposure = investmentSecurityBigDecimalEntry.getValue();
					targetMap.put(investmentSecurityBigDecimalEntry.getKey(), actualExposure);
					remainingTarget = MathUtils.subtract(remainingTarget, actualExposure);
				}
			}
			targetMap.put(currentSecurity, remainingTarget);
			return targetMap;
		}
	},


	// When non-current security is held, and we are currently overweight and need to close positions, those non-current securities will get the full target resulting in difference to close positions. The "current" security would have a target of 0. When non-current security is held, and we are currently underweight and need to open positions, those non-current securities will get TFA and the current security target will be adjusted to get the difference and open new positions in the current security.
	DIFFERENCE_OPENING("Difference - Opening") {
		@Override
		public Map<InvestmentSecurity, BigDecimal> calculateTargetAmountMap(BigDecimal totalTarget, InvestmentSecurity currentSecurity, Map<InvestmentSecurity, BigDecimal> actualExposureAmountMap) {
			BigDecimal actualExposureSum = CoreMathUtils.sum(actualExposureAmountMap.values());
			// If we are opening positions, use the default DIFFERENCE_ALL calculation as the current will get the remainder
			if (isTargetOpeningPositions(totalTarget, actualExposureSum) && !isTargetChangingDirection(totalTarget, actualExposureSum)) {
				return DIFFERENCE_ALL.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureAmountMap);
			}
			// If we are changing direction, then use the ALL calculation as the non-currents should be closed out and the current security gets everything
			if (isTargetChangingDirection(totalTarget, actualExposureSum)) {
				return ALL.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureAmountMap);
			}
			// Otherwise, need to close out target for non-current securities - sorted by end date asc
			List<InvestmentSecurity> securityList = CollectionUtils.toArrayList(actualExposureAmountMap.keySet());
			securityList = BeanUtils.sortWithFunction(securityList, InvestmentSecurity::getEndDate, true);

			InvestmentSecurity adjustingSecurity = null;
			for (InvestmentSecurity security : securityList) {
				if (!security.equals(currentSecurity)) {
					adjustingSecurity = security;
					break;
				}
			}

			// Now, can use the Difference All calculation, while passing the adjusting Security as the "current" so that is the one that is adjusted
			return DIFFERENCE_ALL.calculateTargetAmountMap(totalTarget, adjustingSecurity, actualExposureAmountMap);
		}
	},


	// When non-current security is held, its target will always be set to 0 to instruct that position to be closed. The "current" security will always get 100% of the target.
	ALL("All") {
		@Override
		public Map<InvestmentSecurity, BigDecimal> calculateTargetAmountMap(BigDecimal totalTarget, InvestmentSecurity currentSecurity, Map<InvestmentSecurity, BigDecimal> actualExposureAmountMap) {
			Map<InvestmentSecurity, BigDecimal> targetMap = new HashMap<>();
			for (InvestmentSecurity security : actualExposureAmountMap.keySet()) {
				if (security.equals(currentSecurity)) {
					targetMap.put(security, totalTarget);
				}
				else {
					targetMap.put(security, BigDecimal.ZERO);
				}
			}
			return targetMap;
		}
	};


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private final String label;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	InvestmentCurrentSecurityTargetAdjustmentTypes(String label) {
		this.label = label;
	}


	/**
	 * Returns Map of Securities to New Target Amounts for the given Overall Target, Current Security, and Map of Securities to Actual Exposure amounts
	 */
	public abstract Map<InvestmentSecurity, BigDecimal> calculateTargetAmountMap(BigDecimal totalTarget, InvestmentSecurity currentSecurity, Map<InvestmentSecurity, BigDecimal> actualExposureAmountMap);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Going from Long to Short or Short to Long
	 */
	protected boolean isTargetChangingDirection(BigDecimal target, BigDecimal actual) {
		// If neither is zero and target and actual are different signs (Short to Long or Long to Short)
		return (!MathUtils.isNullOrZero(target) && !MathUtils.isNullOrZero(actual) && MathUtils.isGreaterThan(target, BigDecimal.ZERO) != MathUtils.isGreaterThan(actual, BigDecimal.ZERO));
	}


	protected boolean isTargetOpeningPositions(BigDecimal target, BigDecimal actual) {
		// If target quantity is 0
		if (MathUtils.isEqual(target, 0)) {
			return false;
		}
		// If actual is 0
		if (MathUtils.isEqual(actual, 0)) {
			return true;
		}
		// If changing direction, then it's an opening
		if (isTargetChangingDirection(target, actual)) {
			return true;
		}
		// If actual is short and target is less than original or long
		if (MathUtils.isLessThan(actual, 0)) {
			if (MathUtils.isGreaterThan(target, 0)) {
				return true;
			}
			return (MathUtils.isLessThan(target, actual));
		}
		// Otherwise long - so if target is greater than actual, or less than 0
		if (MathUtils.isLessThan(target, 0)) {
			return true;
		}
		return MathUtils.isGreaterThan(target, actual);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}
}
