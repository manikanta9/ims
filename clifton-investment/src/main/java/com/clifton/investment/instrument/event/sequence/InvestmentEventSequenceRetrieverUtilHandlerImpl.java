package com.clifton.investment.instrument.event.sequence;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * @author mwacker
 */
@Component
public class InvestmentEventSequenceRetrieverUtilHandlerImpl implements InvestmentEventSequenceRetrieverUtilHandler {

	private CalendarBusinessDayService calendarBusinessDayService;
	private ScheduleApiService scheduleApiService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventSearchForm getInvestmentSecurityEventDuplicateSearchForm(InvestmentSecurityEvent event) {
		// check if this is a duplicate event
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(event.getSecurity().getId());
		searchForm.setTypeId(event.getType().getId());
		// if it's the last event, use the event date in case there is a partial period
		if (DateUtils.compare(event.getEventDate(), event.getSecurity().getLastDeliveryDate(), false) == 0) {
			searchForm.setEventDate(event.getEventDate());
		}
		else {
			searchForm.setAfterEventDate(DateUtils.getFirstDayOfMonth(event.getEventDate()));
			searchForm.setBeforeEventDate(DateUtils.getLastDayOfMonth(event.getEventDate()));
		}
		if (!event.getType().isOnePerEventDate()) {
			searchForm.setAfterEventValue(event.getAfterEventValue());
			searchForm.setBeforeEventValue(event.getBeforeEventValue());
		}
		return searchForm;
	}


	@Override
	public Date getFixingDate(Calendar fixingCalendar, BusinessDayConventions fixingBusinessDayConvention, int fixingCycle, Date eventDate) {
		Schedule schedule = new Schedule();
		schedule.setCalendar(fixingCalendar.toCalendar());
		schedule.setBusinessDayConvention(fixingBusinessDayConvention.name());

		// start the schedule before the provided event date to ensure that we can handle all BusinessDayConventions
		Date scheduleStartDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(eventDate, fixingCalendar.getId()), -Math.abs(fixingCycle) * 2);
		schedule.setStartDate(scheduleStartDate);
		schedule.setStartTime(new Time(0));
		schedule.setFrequency(ScheduleFrequencies.DAILY.name());
		schedule.setRecurrence(Math.abs(fixingCycle));
		schedule.setScheduleType(getScheduleApiService().getScheduleTypeByName("Standard Schedules"));

		// the date at which to calculate the fixing date from
		Date lookupDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(eventDate, fixingCalendar.getId()), fixingCycle);
		return getScheduleApiService().getScheduleOccurrence(ScheduleOccurrenceCommand.forOccurrences(schedule, 1, lookupDate));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}
}
