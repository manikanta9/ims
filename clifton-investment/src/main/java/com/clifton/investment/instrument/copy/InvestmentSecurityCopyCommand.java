package com.clifton.investment.instrument.copy;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.ExpiryTimeValues;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>InvestmentSecurityCopyCommand</code> is used to create a new {@link InvestmentSecurity}
 * using an existing security as a template. Security custom columns can be provided to override
 * values on the new security.
 *
 * @author NickK
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class InvestmentSecurityCopyCommand {

	private InvestmentSecurity securityToCopy;
	private InvestmentInstrument instrument;

	/**
	 * Optional reference security that can be defined when creating a security from a template.
	 * For example, when creating FLEX options, the reference security can be set upon creation.
	 */
	private InvestmentSecurity referenceSecurity;

	private Date expirationDate;

	// Options
	private BigDecimal strikePrice;
	private ExpiryTimeValues settlementTime;
	private InvestmentSecurityOptionTypes optionType;
	private OptionStyleTypes optionStyle;

	// Forwards
	private Date startDate;
	private Date endDate;
	private Date settlementDate;

	/////////////////////////////       OTC       /////////////////////////////
	private InvestmentAccount clientAccount;
	private BusinessCompany counterparty;
	private BusinessContract isda;

	// Flag to return the existing security if it already exists
	private boolean returnExisting;
	/**
	 * Flag to tell the system to raise an event to automatically apply market data values
	 * looked up from available field mappings.
	 */
	private boolean applyMarketDataToNewSecurity;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getSecurityToCopy() {
		return this.securityToCopy;
	}


	public void setSecurityToCopy(InvestmentSecurity securityToCopy) {
		this.securityToCopy = securityToCopy;
	}


	public InvestmentInstrument getInstrument() {
		return this.instrument;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public InvestmentSecurity getReferenceSecurity() {
		return this.referenceSecurity;
	}


	public void setReferenceSecurity(InvestmentSecurity referenceSecurity) {
		this.referenceSecurity = referenceSecurity;
	}


	public Date getExpirationDate() {
		return this.expirationDate;
	}


	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}


	public BigDecimal getStrikePrice() {
		return this.strikePrice;
	}


	public void setStrikePrice(BigDecimal strikePrice) {
		this.strikePrice = strikePrice;
	}


	public ExpiryTimeValues getSettlementTime() {
		return this.settlementTime;
	}


	public void setSettlementTime(ExpiryTimeValues settlementTime) {
		this.settlementTime = settlementTime;
	}


	public InvestmentSecurityOptionTypes getOptionType() {
		return this.optionType;
	}


	public void setOptionType(InvestmentSecurityOptionTypes optionType) {
		this.optionType = optionType;
	}


	public OptionStyleTypes getOptionStyle() {
		return this.optionStyle;
	}


	public void setOptionStyle(OptionStyleTypes optionStyle) {
		this.optionStyle = optionStyle;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public BusinessCompany getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(BusinessCompany counterparty) {
		this.counterparty = counterparty;
	}


	public BusinessContract getIsda() {
		return this.isda;
	}


	public void setIsda(BusinessContract isda) {
		this.isda = isda;
	}


	public boolean isReturnExisting() {
		return this.returnExisting;
	}


	public void setReturnExisting(boolean returnExisting) {
		this.returnExisting = returnExisting;
	}


	public boolean isApplyMarketDataToNewSecurity() {
		return this.applyMarketDataToNewSecurity;
	}


	public void setApplyMarketDataToNewSecurity(boolean applyMarketDataToNewSecurity) {
		this.applyMarketDataToNewSecurity = applyMarketDataToNewSecurity;
	}
}
