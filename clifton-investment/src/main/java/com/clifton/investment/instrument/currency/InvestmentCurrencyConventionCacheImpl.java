package com.clifton.investment.instrument.currency;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>InvestmentCurrencyConventionCacheImpl</code> class cashes all currency conventions and provides fast
 * look ups for conventions between currencies.
 *
 * @author vgomelsky
 */
@Component
public class InvestmentCurrencyConventionCacheImpl extends SelfRegisteringSimpleDaoCache<InvestmentCurrencyConvention, String, Map<String, CurrencyConventionHolder>> implements InvestmentCurrencyConventionCache {

	public static final String MULTIPLIER_CONVENTION_KEY = "MULTIPLIER_CONVENTION_KEY";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CurrencyConventionHolder getInvestmentCurrencyConvention(String fromSymbol, String toSymbol) {
		Map<String, CurrencyConventionHolder> conventionMap = getCacheHandler().get(getCacheName(), MULTIPLIER_CONVENTION_KEY);
		if (conventionMap != null) {
			return conventionMap.get(getCacheKey(fromSymbol, toSymbol));
		}
		return null;
	}


	@Override
	public void updateInvestmentCurrencyConventionList(List<InvestmentCurrencyConvention> conventionList) {
		Map<String, CurrencyConventionHolder> multiplierConventionMap = new ConcurrentHashMap<>();
		for (InvestmentCurrencyConvention convention : CollectionUtils.getIterable(conventionList)) {
			multiplierConventionMap.put(getCacheKey(convention.getFromCurrency().getSymbol(), convention.getToCurrency().getSymbol()), new CurrencyConventionHolder(convention.isMultiplyFromByFX(),
					convention.getDaysToSettle(), convention.getDominantCurrency().getSymbol()));
			multiplierConventionMap.put(getCacheKey(convention.getToCurrency().getSymbol(), convention.getFromCurrency().getSymbol()), new CurrencyConventionHolder(!convention.isMultiplyFromByFX(),
					convention.getDaysToSettle(), convention.getDominantCurrency().getSymbol()));
		}
		getCacheHandler().put(getCacheName(), MULTIPLIER_CONVENTION_KEY, multiplierConventionMap);
	}


	private String getCacheKey(String fromSymbol, String toSymbol) {
		return (fromSymbol + toSymbol);
	}
}
