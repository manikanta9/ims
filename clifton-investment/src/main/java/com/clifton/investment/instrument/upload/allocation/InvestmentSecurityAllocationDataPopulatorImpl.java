package com.clifton.investment.instrument.upload.allocation;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.exchange.InvestmentExchangeSearchForm;
import com.clifton.investment.exchange.InvestmentExchangeService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * @author mitchellf
 */
@Component
public class InvestmentSecurityAllocationDataPopulatorImpl implements InvestmentSecurityAllocationDataPopulator {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentExchangeService investmentExchangeService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void populateAllocation(InvestmentSecurityAllocation allocation, InvestmentSecurityAllocationUploadCommand command, Status status) {
		// populate Investment security
		Date startDate = allocation.getStartDate() != null ? allocation.getStartDate() : command.getStartDate();
		populateInvestmentSecurity(allocation, startDate, status);

		// populate overridden InvestmentExchange
		InvestmentExchange baseExchange = allocation.getOverrideExchange();
		if (baseExchange != null) {
			InvestmentExchange overrideExchange;
			InvestmentExchangeSearchForm sf = new InvestmentExchangeSearchForm();
			// we can safely assume that either name and/or symbol will be populated, because if the exchange is defined, at least one of those must be populated.
			if (baseExchange.getName() != null && baseExchange.getExchangeCode() == null) {
				overrideExchange = getInvestmentExchangeService().getInvestmentExchangeByName(baseExchange.getName());
			}
			else {
				// it's ok if one of these is null - setting a field to null on the search form is the same as not setting it
				sf.setName(baseExchange.getName());
				sf.setExchangeCode(baseExchange.getExchangeCode());

				List<InvestmentExchange> exchangeList = this.getInvestmentExchangeService().getInvestmentExchangeList(sf);
				ValidationUtils.assertEquals(CollectionUtils.getSize(exchangeList), 1, "Could not find individual Investment Exchange with name " + baseExchange.getName() + " and exchange code " + baseExchange.getExchangeCode());
				overrideExchange = CollectionUtils.getOnlyElement(exchangeList);
			}
			allocation.setOverrideExchange(overrideExchange);
		}
	}


	private void populateInvestmentSecurity(InvestmentSecurityAllocation allocation, Date startDate, Status status) {
		InvestmentSecurity baseSecurity = allocation.getInvestmentSecurity();
		StringBuilder identifiers = new StringBuilder();
		List<InvestmentSecurity> foundSecurities = null;
		if (baseSecurity != null && baseSecurity.getId() == null) {
			String sedol = baseSecurity.getSedol();
			if (!StringUtils.isEmpty(sedol)) {
				foundSecurities = getActiveSecurityListForSecurityService(startDate, () -> getInvestmentInstrumentService().getInvestmentSecurityListBySedol(sedol));
				if (setInvestmentSecurityAndValidate(foundSecurities, allocation, baseSecurity, status)) {
					return;
				}
				identifiers.append("Sedol: ")
						.append(sedol)
						.append("\n");
			}
			String symbol = baseSecurity.getSymbol();
			if (symbol != null) {
				List<InvestmentSecurity> securities = getActiveSecurityListForSecurityService(startDate, () -> getInvestmentInstrumentService().getInvestmentSecurityListBySymbol(symbol));
				if (setInvestmentSecurityAndValidate(securities, allocation, baseSecurity, status)) {
					return;
				}
				identifiers.append("Symbol: ")
						.append(symbol)
						.append("\n");
			}
			String cusip = baseSecurity.getCusip();
			if (cusip != null) {
				List<InvestmentSecurity> securities = getActiveSecurityListForSecurityService(startDate, () -> getInvestmentInstrumentService().getInvestmentSecurityListByCusip(cusip));
				if (setInvestmentSecurityAndValidate(securities, allocation, baseSecurity, status)) {
					return;
				}
				identifiers.append("Cusip: ")
						.append(cusip)
						.append("\n");
			}
			String isin = baseSecurity.getIsin();
			if (isin != null) {
				List<InvestmentSecurity> securities = getActiveSecurityListForSecurityService(startDate, () -> getInvestmentInstrumentService().getInvestmentSecurityListByIsin(isin));
				if (setInvestmentSecurityAndValidate(securities, allocation, baseSecurity, status)) {
					return;
				}
				identifiers.append("Isin: ")
						.append(isin)
						.append("\n");
			}
			int size = CollectionUtils.getSize(foundSecurities);
			if (size != 1) {
				StringBuilder message = new StringBuilder();
				message.append("Could not create allocation for ")
						.append(allocation.getAllocationLabel())
						.append(". Could not determine Investment Security to use for allocation. ")
						.append(size > 1 ? "Multiple" : "No")
						.append(" securities were found for identifier(s):\n")
						.append(identifiers.toString());
				throw new ValidationException(message.toString());
			}
			allocation.setInvestmentSecurity(CollectionUtils.getOnlyElement(foundSecurities));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private boolean setInvestmentSecurityAndValidate(List<InvestmentSecurity> securities, InvestmentSecurityAllocation allocation, InvestmentSecurity baseSecurity, Status status) {
		if (CollectionUtils.getSize(securities) == 1) {
			InvestmentSecurity foundSecurity = CollectionUtils.getOnlyElement(securities);
			allocation.setInvestmentSecurity(foundSecurity);
			validateSecurityIdentifiers(baseSecurity, allocation.getInvestmentSecurity(), status);
			return true;
		}
		return false;
	}


	private void validateSecurityIdentifiers(InvestmentSecurity baseSecurity, InvestmentSecurity foundSecurity, Status status) {
		StringBuilder warningMessage = new StringBuilder();
		final String startMessage = "For security " + foundSecurity.getName() + ": ";
		warningMessage.append(startMessage);
		if (!StringUtils.isEqual(baseSecurity.getSymbol(), foundSecurity.getSymbol())) {
			warningMessage.append("Symbol do not match, expected: " + foundSecurity.getSymbol() + " actual: " + baseSecurity.getSymbol() + ".").append(StringUtils.NEW_LINE);
		}
		if (!StringUtils.isEqual(baseSecurity.getSedol(), foundSecurity.getSedol())) {
			warningMessage.append("SEDOL do not match, expected: " + foundSecurity.getSedol() + " actual: " + baseSecurity.getSedol() + ".").append(StringUtils.NEW_LINE);
		}
		if (!StringUtils.isEqual(baseSecurity.getIsin(), foundSecurity.getIsin())) {
			warningMessage.append("ISIN do not match, expected: " + foundSecurity.getIsin() + " actual: " + baseSecurity.getIsin() + ".").append(StringUtils.NEW_LINE);
		}
		if (!StringUtils.isEqual(baseSecurity.getCusip(), foundSecurity.getCusip())) {
			warningMessage.append("CUSIP do not match, expected: " + foundSecurity.getCusip() + " actual: " + baseSecurity.getCusip() + ".").append(StringUtils.NEW_LINE);
		}
		if (!StringUtils.isEqual(warningMessage.toString(), startMessage)) {
			status.addWarning(warningMessage.toString());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Filters a list of securities to remove inactive securities.
	 */
	private List<InvestmentSecurity> getActiveSecurityListForSecurityService(Date activeOnDate, Supplier<List<InvestmentSecurity>> securityListSupplier) {
		return CollectionUtils.getStream(securityListSupplier.get())
				.filter(security -> security.isActiveOn(activeOnDate))
				.collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentExchangeService getInvestmentExchangeService() {
		return this.investmentExchangeService;
	}


	public void setInvestmentExchangeService(InvestmentExchangeService investmentExchangeService) {
		this.investmentExchangeService = investmentExchangeService;
	}
}
