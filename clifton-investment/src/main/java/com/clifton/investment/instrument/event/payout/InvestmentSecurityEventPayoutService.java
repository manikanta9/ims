package com.clifton.investment.instrument.event.payout;

import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventClientElectionSearchForm;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutTypeAssignmentSearchForm;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutTypeSearchForm;

import java.util.List;


/**
 * @author vgomelsky
 */
public interface InvestmentSecurityEventPayoutService {

	////////////////////////////////////////////////////////////////////////////
	//////////    Investment Security Event Payout Methods          //////////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventPayout getInvestmentSecurityEventPayout(int id);


	public List<InvestmentSecurityEventPayout> getInvestmentSecurityEventPayoutList(InvestmentSecurityEventPayoutSearchForm searchForm);


	public InvestmentSecurityEventPayout saveInvestmentSecurityEventPayout(InvestmentSecurityEventPayout payout);


	@UserIgnorableValidation
	public void deleteInvestmentSecurityEventPayout(int id, boolean ignoreValidation);


	////////////////////////////////////////////////////////////////////////////
	//////////    Investment Security Event Payout Type Methods     ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventPayoutType getInvestmentSecurityEventPayoutType(short id);


	public InvestmentSecurityEventPayoutType getInvestmentSecurityEventPayoutTypeByName(String name);


	public List<InvestmentSecurityEventPayoutType> getInvestmentSecurityEventPayoutTypeList(InvestmentSecurityEventPayoutTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////   Investment Security Event Payout Type Assignment Methods  ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventPayoutTypeAssignment getInvestmentSecurityEventPayoutTypeAssignment(short id);


	public List<InvestmentSecurityEventPayoutTypeAssignment> getInvestmentSecurityEventPayoutTypeAssignmentList(InvestmentSecurityEventPayoutTypeAssignmentSearchForm searchForm);


	public InvestmentSecurityEventPayoutTypeAssignment linkInvestmentSecurityEventPayoutTypeAssignment(short payoutTypeId, short eventTypeId);


	public void deleteInvestmentSecurityEventPayoutTypeAssignment(short id);


	////////////////////////////////////////////////////////////////////////////
	////////   Investment Security Event Client Election Methods    ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventClientElection getInvestmentSecurityEventClientElection(int id);


	public List<InvestmentSecurityEventClientElection> getInvestmentSecurityEventClientElectionList(InvestmentSecurityEventClientElectionSearchForm searchForm);


	public InvestmentSecurityEventClientElection saveInvestmentSecurityEventClientElection(InvestmentSecurityEventClientElection clientElection);


	public void deleteInvestmentSecurityEventClientElection(int id);
}
