package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.sign.AccrualSignCalculators;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The InvestmentAccrualLatestDetailCalculator class calculates accrual based on the latest event detail Effective Rate.
 * It multiples that rate by the corresponding accrual basis (notional or quantity).  The logic ignores event values and dates.
 *
 * @author vgomelsky
 */
public class InvestmentAccrualLatestDetailCalculator extends BaseInvestmentAccrualCalculator {

	private InvestmentSecurityEventService investmentSecurityEventService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal doCalculateAccruedInterest(InvestmentSecurityEvent paymentEvent, BigDecimal accrualBasis, BigDecimal notionalMultiplier, Date accrueUpToDate) {
		BigDecimal result;

		// Make use of caching the list by event, then filter results
		List<InvestmentSecurityEventDetail> detailList = getInvestmentSecurityEventService().getInvestmentSecurityEventDetailListByEvent(paymentEvent.getId());
		detailList = BeanUtils.filter(detailList, d -> DateUtils.isDateBeforeOrEqual(d.getEffectiveDate(), accrueUpToDate, false));
		detailList = BeanUtils.sortWithFunction(detailList, InvestmentSecurityEventDetail::getEffectiveDate, false);

		BigDecimal effectiveRate = getEffectiveRate(CollectionUtils.getFirstElement(detailList), paymentEvent);
		if (accrualBasis == null) {
			LogUtils.error(getClass(), String.format("Expected a non-null accrual basis for security [%s] accrual calculation.", paymentEvent.getSecurity().getLabel()));
			accrualBasis = BigDecimal.ZERO;
		}
		InvestmentSecurity security = paymentEvent.getSecurity();
		result = MathUtils.multiply(accrualBasis, effectiveRate);
		result = InvestmentCalculatorUtils.getNotionalCalculator(security).round(result);

		AccrualSignCalculators signCalculator = security.getInstrument().getHierarchy().getAccrualSignCalculator();
		if (signCalculator != null) {
			// lookup security configuration necessary to calculate accrual
			AccrualConfig accrualConfig = getAccrualConfig(paymentEvent, true);
			if (getInvestmentAccrualCalculatorFactory().getInvestmentAccrualSignCalculator(signCalculator).isAccrualAmountNegated(security, accrualConfig.getCouponFrequencyField())) {
				result = MathUtils.negate(result);
			}
		}

		return result;
	}


	private BigDecimal getEffectiveRate(InvestmentSecurityEventDetail detail, InvestmentSecurityEvent paymentEvent) {
		if (detail != null) {
			return detail.getEffectiveRate() != null ? detail.getEffectiveRate() : BigDecimal.ZERO;
		}
		if (paymentEvent != null) {
			return paymentEvent.getAfterEventValue() != null ? paymentEvent.getAfterEventValue() : BigDecimal.ZERO;
		}
		return BigDecimal.ZERO;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}
}
