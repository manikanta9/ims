package com.clifton.investment.instrument.structure.upload;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructure;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureService;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureWeight;
import com.clifton.system.upload.SystemUploadCommand;
import com.clifton.system.upload.SystemUploadHandler;
import com.clifton.system.upload.SystemUploadService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
@Service
public class InvestmentInstrumentStructureUploadServiceImpl implements InvestmentInstrumentStructureUploadService {

	private InvestmentInstrumentStructureService investmentInstrumentStructureService;

	private SystemUploadHandler systemUploadHandler;

	private SystemUploadService systemUploadService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable downloadInvestmentInstrumentStructureWeightFile(Integer instrumentStructureId) {
		SystemUploadCommand uploadCommand = new SystemUploadCommand();
		uploadCommand.setTableName("InvestmentInstrumentStructureWeight");
		uploadCommand.setSimple(true);
		Map<String, Object> additionalFilterMap = new HashMap<>();
		if (instrumentStructureId != null) {
			additionalFilterMap.put("instrumentStructure.id", instrumentStructureId);
		}
		// Include all rows if downloading sample for an existing structure
		return getSystemUploadService().downloadSystemUploadFileSampleWithAdditionalFilters(uploadCommand, (instrumentStructureId != null), additionalFilterMap);
	}


	@Override
	public void uploadInvestmentInstrumentStructureFile(InvestmentInstrumentStructure bean, MultipartFile file) {
		List<IdentityObject> instrumentWeightList = convertFileToInvestmentInstrumentStructureWeightList(file);
		ValidationUtils.assertNotEmpty(instrumentWeightList, "No weights supplied in the upload.");
		InvestmentInstrumentStructure existingBean = (bean.isNewBean() ? null : getInvestmentInstrumentStructureService().getInvestmentInstrumentStructure(bean.getId()));
		List<InvestmentInstrumentStructureWeight> existingWeightList = (existingBean == null ? null : existingBean.getInstrumentStructureWeightList());
		List<InvestmentInstrumentStructureWeight> saveList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(existingWeightList)) {
			// Merge Lists
			Set<Integer> weightIdsUsed = new HashSet<>(); // Will match by instrument, not likely, but it is possible to have multiple
			for (IdentityObject weightObj : instrumentWeightList) {
				InvestmentInstrumentStructureWeight weight = (InvestmentInstrumentStructureWeight) weightObj;
				for (InvestmentInstrumentStructureWeight existingWeight : existingWeightList) {
					if (weightIdsUsed.contains(existingWeight.getId())) {
						continue;
					}
					if (existingWeight.getInstrument().equals(weight.getInstrument())) {
						weightIdsUsed.add(existingWeight.getId());
						weight.setId(existingWeight.getId());
						weight.setRv(existingWeight.getRv());
						break;
					}
				}
				saveList.add(weight);
			}
		}
		else {
			for (IdentityObject weightObj : instrumentWeightList) {
				InvestmentInstrumentStructureWeight weight = (InvestmentInstrumentStructureWeight) weightObj;
				saveList.add(weight);
			}
		}
		bean.setInstrumentStructureWeightList(saveList);
		getInvestmentInstrumentStructureService().saveInvestmentInstrumentStructure(bean);
	}


	private List<IdentityObject> convertFileToInvestmentInstrumentStructureWeightList(MultipartFile weightFile) {
		SystemUploadCommand uploadCommand = new SystemUploadCommand();
		uploadCommand.setTableName("InvestmentInstrumentStructureWeight");
		uploadCommand.setSimple(true);
		uploadCommand.setFile(weightFile);
		// Consider All to Be Inserts, will manually merge if necessary
		uploadCommand.setExistingBeans(FileUploadExistingBeanActions.INSERT);
		return getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, true);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentStructureService getInvestmentInstrumentStructureService() {
		return this.investmentInstrumentStructureService;
	}


	public void setInvestmentInstrumentStructureService(InvestmentInstrumentStructureService investmentInstrumentStructureService) {
		this.investmentInstrumentStructureService = investmentInstrumentStructureService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}


	public SystemUploadService getSystemUploadService() {
		return this.systemUploadService;
	}


	public void setSystemUploadService(SystemUploadService systemUploadService) {
		this.systemUploadService = systemUploadService;
	}
}
