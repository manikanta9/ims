package com.clifton.investment.instrument.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.CustomJsonStringAwareSearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.instrument.InvestmentInstrument;

import java.math.BigDecimal;


/**
 * The <code>InvestmentInstrumentSearchForm</code> contains the search fields
 * that can be used when searching for {@link InvestmentInstrument}s.
 * <p>
 * The search pattern set is searched against the instrument's name or identifier prefix field.
 *
 * @author manderson
 */
public class InstrumentSearchForm extends BaseAuditableEntitySearchForm implements CustomJsonStringAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "identifierPrefix,name", sortField = "name")
	private String searchPattern;

	@SearchField(searchField = "hierarchy.id")
	private Short hierarchyId;

	@SearchField(searchField = "hierarchy.id")
	private Short[] hierarchyIds;

	@SearchField(searchField = "name", searchFieldPath = "hierarchy")
	private String hierarchyName;

	@SearchField(searchField = "name", searchFieldPath = "hierarchy", comparisonConditions = ComparisonConditions.EQUALS)
	private String hierarchyNameEquals;

	@SearchField(searchField = "tradingCurrency.id")
	private Integer tradingCurrencyId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "underlyingInstrument")
	private Integer underlyingTradingCurrencyId;

	@SearchField(searchField = "underlyingInstrument.id")
	private Integer underlyingInstrumentId;

	@SearchField(searchFieldPath = "underlyingInstrument", searchField = "hierarchy.id")
	private Short underlyingHierarchyId;

	// NOTE: Used in hierarchy validation when underlying hierarchy is set to verify there are no violations
	// Does an inner join so automatically skips instruments where underlying instrument is not populated
	@SearchField(searchFieldPath = "underlyingInstrument", searchField = "hierarchy.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short underlyingHierarchyIdNotEqual;


	@SearchField(searchField = "bigInstrument.id")
	private Integer bigInstrumentId;


	@SearchField(searchField = "bigInstrument.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean bigInstrumentPopulated;


	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean investmentAccountPopulated;

	@SearchField(searchField = "exchange.id")
	private Short exchangeId;

	@SearchField(searchField = "compositeExchange.id")
	private Short compositeExchangeId;

	@SearchField(searchField = "settlementCalendar.id")
	private Short settlementCalendarId;

	@SearchField(searchField = "countryOfRisk.id")
	private Integer countryOfRiskId;

	@SearchField(searchField = "countryOfIncorporation.id")
	private Integer countryOfIncorporationId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "hierarchy")
	private Short investmentTypeId;

	@SearchField(searchField = "name", searchFieldPath = "hierarchy.investmentType")
	private String investmentType;

	@SearchField(searchField = "name", searchFieldPath = "hierarchy.investmentTypeSubType")
	private String investmentTypeSubType;

	@SearchField(searchField = "investmentTypeSubType.id", searchFieldPath = "hierarchy")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "name", searchFieldPath = "hierarchy.investmentTypeSubType2")
	private String investmentTypeSubType2;

	@SearchField(searchField = "investmentTypeSubType2.id", searchFieldPath = "hierarchy")
	private Short investmentTypeSubType2Id;

	@SearchField
	private String identifierPrefix;

	@SearchField(searchField = "identifierPrefix", comparisonConditions = ComparisonConditions.EQUALS)
	private String identifierPrefixEquals;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String groupName;

	@SearchField
	private BigDecimal priceMultiplier;

	@SearchField
	private BigDecimal exposureMultiplier;

	@SearchField
	private Integer daysToSettle;

	@SearchField
	private BigDecimal hedgerInitialMarginPerUnit;
	@SearchField
	private BigDecimal hedgerSecondaryMarginPerUnit;
	@SearchField
	private BigDecimal speculatorInitialMarginPerUnit;
	@SearchField
	private BigDecimal speculatorSecondaryMarginPerUnit;

	@SearchField
	private Boolean fairValueAdjustmentUsed;

	@SearchField
	private Boolean deliverable;

	@SearchField
	private Boolean discountNote;

	@SearchField(searchFieldPath = "hierarchy")
	private Boolean tradingDisallowed;

	@SearchField(searchField = "groupItemList.referenceOne.group.name", comparisonConditions = ComparisonConditions.EXISTS_LIKE)
	private String investmentGroupName;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	// Custom Search Filter
	private Short missingInvestmentSecurityGroupId;

	@SearchField
	private Boolean inactive;

	@SearchField(searchFieldPath = "hierarchy")
	private Boolean oneSecurityPerInstrument;

	@SearchField(searchField = "spotMonthCalculatorBean.id")
	private Integer spotMonthCalculatorBeanId;

	@SearchField(searchFieldPath = "hierarchy")
	private Boolean otc;

	@SearchField(searchFieldPath = "hierarchy")
	private Boolean referenceSecurityAllowed;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getTradingCurrencyId() {
		return this.tradingCurrencyId;
	}


	public void setTradingCurrencyId(Integer tradingCurrencyId) {
		this.tradingCurrencyId = tradingCurrencyId;
	}


	public Integer getUnderlyingTradingCurrencyId() {
		return this.underlyingTradingCurrencyId;
	}


	public void setUnderlyingTradingCurrencyId(Integer underlyingTradingCurrencyId) {
		this.underlyingTradingCurrencyId = underlyingTradingCurrencyId;
	}


	public Short getHierarchyId() {
		return this.hierarchyId;
	}


	public void setHierarchyId(Short hierarchyId) {
		this.hierarchyId = hierarchyId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getTradingDisallowed() {
		return this.tradingDisallowed;
	}


	public void setTradingDisallowed(Boolean tradingDisallowed) {
		this.tradingDisallowed = tradingDisallowed;
	}


	public String getIdentifierPrefix() {
		return this.identifierPrefix;
	}


	public void setIdentifierPrefix(String identifierPrefix) {
		this.identifierPrefix = identifierPrefix;
	}


	public Integer getUnderlyingInstrumentId() {
		return this.underlyingInstrumentId;
	}


	public void setUnderlyingInstrumentId(Integer underlyingInstrumentId) {
		this.underlyingInstrumentId = underlyingInstrumentId;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getExchangeId() {
		return this.exchangeId;
	}


	public void setExchangeId(Short exchangeId) {
		this.exchangeId = exchangeId;
	}


	public Short getCompositeExchangeId() {
		return this.compositeExchangeId;
	}


	public void setCompositeExchangeId(Short compositeExchangeId) {
		this.compositeExchangeId = compositeExchangeId;
	}


	public Short getSettlementCalendarId() {
		return this.settlementCalendarId;
	}


	public void setSettlementCalendarId(Short settlementCalendarId) {
		this.settlementCalendarId = settlementCalendarId;
	}


	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	public void setPriceMultiplier(BigDecimal priceMultiplier) {
		this.priceMultiplier = priceMultiplier;
	}


	public Boolean getDeliverable() {
		return this.deliverable;
	}


	public void setDeliverable(Boolean deliverable) {
		this.deliverable = deliverable;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Short getMissingInvestmentSecurityGroupId() {
		return this.missingInvestmentSecurityGroupId;
	}


	public void setMissingInvestmentSecurityGroupId(Short missingInvestmentSecurityGroupId) {
		this.missingInvestmentSecurityGroupId = missingInvestmentSecurityGroupId;
	}


	public String getHierarchyName() {
		return this.hierarchyName;
	}


	public void setHierarchyName(String hierarchyName) {
		this.hierarchyName = hierarchyName;
	}


	public String getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(String investmentType) {
		this.investmentType = investmentType;
	}


	public String getInvestmentGroupName() {
		return this.investmentGroupName;
	}


	public void setInvestmentGroupName(String investmentGroupName) {
		this.investmentGroupName = investmentGroupName;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public Integer getBigInstrumentId() {
		return this.bigInstrumentId;
	}


	public void setBigInstrumentId(Integer bigInstrumentId) {
		this.bigInstrumentId = bigInstrumentId;
	}


	public Boolean getBigInstrumentPopulated() {
		return this.bigInstrumentPopulated;
	}


	public void setBigInstrumentPopulated(Boolean bigInstrumentPopulated) {
		this.bigInstrumentPopulated = bigInstrumentPopulated;
	}


	public BigDecimal getExposureMultiplier() {
		return this.exposureMultiplier;
	}


	public void setExposureMultiplier(BigDecimal exposureMultiplier) {
		this.exposureMultiplier = exposureMultiplier;
	}


	public BigDecimal getHedgerInitialMarginPerUnit() {
		return this.hedgerInitialMarginPerUnit;
	}


	public void setHedgerInitialMarginPerUnit(BigDecimal hedgerInitialMarginPerUnit) {
		this.hedgerInitialMarginPerUnit = hedgerInitialMarginPerUnit;
	}


	public BigDecimal getHedgerSecondaryMarginPerUnit() {
		return this.hedgerSecondaryMarginPerUnit;
	}


	public void setHedgerSecondaryMarginPerUnit(BigDecimal hedgerSecondaryMarginPerUnit) {
		this.hedgerSecondaryMarginPerUnit = hedgerSecondaryMarginPerUnit;
	}


	public BigDecimal getSpeculatorInitialMarginPerUnit() {
		return this.speculatorInitialMarginPerUnit;
	}


	public void setSpeculatorInitialMarginPerUnit(BigDecimal speculatorInitialMarginPerUnit) {
		this.speculatorInitialMarginPerUnit = speculatorInitialMarginPerUnit;
	}


	public BigDecimal getSpeculatorSecondaryMarginPerUnit() {
		return this.speculatorSecondaryMarginPerUnit;
	}


	public void setSpeculatorSecondaryMarginPerUnit(BigDecimal speculatorSecondaryMarginPerUnit) {
		this.speculatorSecondaryMarginPerUnit = speculatorSecondaryMarginPerUnit;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	public Boolean getInvestmentAccountPopulated() {
		return this.investmentAccountPopulated;
	}


	public void setInvestmentAccountPopulated(Boolean investmentAccountPopulated) {
		this.investmentAccountPopulated = investmentAccountPopulated;
	}


	public Boolean getOneSecurityPerInstrument() {
		return this.oneSecurityPerInstrument;
	}


	public void setOneSecurityPerInstrument(Boolean oneSecurityPerInstrument) {
		this.oneSecurityPerInstrument = oneSecurityPerInstrument;
	}


	public Short getUnderlyingHierarchyId() {
		return this.underlyingHierarchyId;
	}


	public void setUnderlyingHierarchyId(Short underlyingHierarchyId) {
		this.underlyingHierarchyId = underlyingHierarchyId;
	}


	public Short getUnderlyingHierarchyIdNotEqual() {
		return this.underlyingHierarchyIdNotEqual;
	}


	public void setUnderlyingHierarchyIdNotEqual(Short underlyingHierarchyIdNotEqual) {
		this.underlyingHierarchyIdNotEqual = underlyingHierarchyIdNotEqual;
	}


	public String getGroupName() {
		return this.groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public Boolean getFairValueAdjustmentUsed() {
		return this.fairValueAdjustmentUsed;
	}


	public void setFairValueAdjustmentUsed(Boolean fairValueAdjustmentUsed) {
		this.fairValueAdjustmentUsed = fairValueAdjustmentUsed;
	}


	public Boolean getDiscountNote() {
		return this.discountNote;
	}


	public void setDiscountNote(Boolean discountNote) {
		this.discountNote = discountNote;
	}


	public Short[] getHierarchyIds() {
		return this.hierarchyIds;
	}


	public void setHierarchyIds(Short[] hierarchyIds) {
		this.hierarchyIds = hierarchyIds;
	}


	public String getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(String investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public Integer getCountryOfRiskId() {
		return this.countryOfRiskId;
	}


	public void setCountryOfRiskId(Integer countryOfRiskId) {
		this.countryOfRiskId = countryOfRiskId;
	}


	public Integer getCountryOfIncorporationId() {
		return this.countryOfIncorporationId;
	}


	public void setCountryOfIncorporationId(Integer countryOfIncorporationId) {
		this.countryOfIncorporationId = countryOfIncorporationId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public String getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(String investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Integer getSpotMonthCalculatorBeanId() {
		return this.spotMonthCalculatorBeanId;
	}


	public void setSpotMonthCalculatorBeanId(Integer spotMonthCalculatorBeanId) {
		this.spotMonthCalculatorBeanId = spotMonthCalculatorBeanId;
	}


	public String getIdentifierPrefixEquals() {
		return this.identifierPrefixEquals;
	}


	public void setIdentifierPrefixEquals(String identifierPrefixEquals) {
		this.identifierPrefixEquals = identifierPrefixEquals;
	}


	public String getHierarchyNameEquals() {
		return this.hierarchyNameEquals;
	}


	public void setHierarchyNameEquals(String hierarchyNameEquals) {
		this.hierarchyNameEquals = hierarchyNameEquals;
	}


	public Boolean getOtc() {
		return this.otc;
	}


	public void setOtc(Boolean otc) {
		this.otc = otc;
	}


	public Boolean getReferenceSecurityAllowed() {
		return this.referenceSecurityAllowed;
	}


	public void setReferenceSecurityAllowed(Boolean referenceSecurityAllowed) {
		this.referenceSecurityAllowed = referenceSecurityAllowed;
	}
}
