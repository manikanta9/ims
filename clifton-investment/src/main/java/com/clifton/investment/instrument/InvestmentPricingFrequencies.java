package com.clifton.investment.instrument;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>InvestmentPricingFrequencies</code> is used to define how often prices should exist for a security.  Most cases, the default is daily, but in some cases (Bonds, Benchmarks) we may only be able to get prices Monthly or Quarterly.
 * This can be used for notifications (for missing prices), and custom security allocation price rebuilds to ensure underlying data is valid before using "flexible" price look up to generate the allocated price.
 *
 * @author manderson
 */
public enum InvestmentPricingFrequencies {

	DAILY {
		@Override
		public Date getMinimumPriceDateForDate(Date date, short calendarId, CalendarBusinessDayService calendarBusinessDayService) {
			// Daily Prices Require Prices on that Day
			return date;
		}
	},

	MONTHLY(true) {
		@Override
		public Date getMinimumPriceDateForDate(Date date, short calendarId, CalendarBusinessDayService calendarBusinessDayService) {
			Date nextBusinessDay = calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarId));
			Date lastDayOfMonth = DateUtils.getLastDayOfMonth(date);
			// If Last Day of the Month is Between Date and Next Business Day - then require price on last day of the month
			if (DateUtils.compare(date, lastDayOfMonth, false) <= 0 && DateUtils.compare(lastDayOfMonth, nextBusinessDay, false) < 0) {
				return lastDayOfMonth;
			}
			// Otherwise return last day of previous month
			return DateUtils.getLastDayOfPreviousMonth(date);
		}
	},

	QUARTERLY(true) {
		@Override
		public Date getMinimumPriceDateForDate(Date date, short calendarId, CalendarBusinessDayService calendarBusinessDayService) {
			Date nextBusinessDay = calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarId));
			Date lastDayOfQuarter = DateUtils.getLastDayOfQuarter(date);
			// If Last Day of the Quarter is Between Date and Next Business Day - then require price on last day of quarter
			if (DateUtils.compare(date, lastDayOfQuarter, false) <= 0 && DateUtils.compare(lastDayOfQuarter, nextBusinessDay, false) < 0) {
				return lastDayOfQuarter;
			}
			// Otherwise return last day of previous quarter
			return DateUtils.getLastDayOfPreviousQuarter(date);
		}
	},

	// Will be used for current date forward means that pricing should be daily, but historically we may only have monthly pricing
	DAILY_FLEXIBLE(false) {
		@Override
		public Date getMinimumPriceDateForDate(Date date, short calendarId, CalendarBusinessDayService calendarBusinessDayService) {
			// If on or after previous weekday - use Daily
			if (DateUtils.compare(date, DateUtils.getPreviousWeekday(new Date()), false) >= 0) {
				return DAILY.getMinimumPriceDateForDate(date, calendarId, calendarBusinessDayService);
			}
			return MONTHLY.getMinimumPriceDateForDate(date, calendarId, calendarBusinessDayService);
		}
	};

	////////////////////////////////////////////////////////////////////////////////


	InvestmentPricingFrequencies() {
		this(false);
	}


	InvestmentPricingFrequencies(boolean positionPriceUsedWhenSecurityPriceMissing) {
		this.positionPriceUsedWhenSecurityPriceMissing = positionPriceUsedWhenSecurityPriceMissing;
	}


	////////////////////////////////////////////////////////////////////////////////

	/**
	 * For some securities (Bonds as an example) we get pricing one a month from another source (not Bloomberg)
	 * If one of these securities is created/traded before that monthly price batch job is ran, then prices are missing
	 * For these securities, instead we will use the position price (i.e. trade price) until newer prices are loaded
	 */
	private final boolean positionPriceUsedWhenSecurityPriceMissing;

	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the required minimum date for the given date that the price can be used.
	 * Note: Does not account for any business day logic
	 * <p>
	 * For example, daily prices would just return the date, Monthly would return the previous month end (unless it is the month end), etc.
	 */
	public abstract Date getMinimumPriceDateForDate(Date date, short calendarId, CalendarBusinessDayService calendarBusinessDayService);


	////////////////////////////////////////////////////////////////////////////////


	public boolean isPositionPriceUsedWhenSecurityPriceMissing() {
		return this.positionPriceUsedWhenSecurityPriceMissing;
	}
}
