package com.clifton.investment.instrument.calculator.accrual;

/**
 * The <code>AccrualHolder</code> holds Accrual objects that are built up during different stages of accrual calculation.
 *
 * @author michaelm
 */
public class AccrualHolder {

	private AccrualDates accrualDates;
	private AccrualBasis accrualBasis;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccrualHolder(AccrualBasis accrualBasis, AccrualDates accrualDates) {
		this.accrualBasis = accrualBasis;
		this.accrualDates = accrualDates;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccrualDates getAccrualDates() {
		return this.accrualDates;
	}


	public void setAccrualDates(AccrualDates accrualDates) {
		this.accrualDates = accrualDates;
	}


	public AccrualBasis getAccrualBasis() {
		return this.accrualBasis;
	}


	public void setAccrualBasis(AccrualBasis accrualBasis) {
		this.accrualBasis = accrualBasis;
	}
}
