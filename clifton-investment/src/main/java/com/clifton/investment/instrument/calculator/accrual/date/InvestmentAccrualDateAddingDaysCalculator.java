package com.clifton.investment.instrument.calculator.accrual.date;


import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * The <code>InvestmentAccrualDateAddingDaysCalculator</code> class is InvestmentAccrualDateCalculator
 * that can add 0 (no adjustment) or more days ot transactionDate.
 * <p/>
 * Event though this will usually result in less accurate market value, it maybe necessary to use
 * it in order to match custodians that use this method.
 *
 * @author vgomelsky
 */
public class InvestmentAccrualDateAddingDaysCalculator implements InvestmentAccrualDateCalculator {

	private int daysToAdd = 0;


	@Override
	public Date calculateAccrualDate(@SuppressWarnings("unused") InvestmentSecurity security, Date transactionDate) {
		return getDaysToAdd() == 0 ? transactionDate : DateUtils.addDays(transactionDate, getDaysToAdd());
	}


	public int getDaysToAdd() {
		return this.daysToAdd;
	}


	public void setDaysToAdd(int daysToAdd) {
		this.daysToAdd = daysToAdd;
	}
}
