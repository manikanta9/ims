package com.clifton.investment.instrument.spot.calculators;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.spot.InvestmentSecuritySpotMonth;

import java.util.Date;


/**
 * The <code>InvestmentSecuritySpotMonthDateFieldCalculator</code> calculator is a basic spot month calculator for securities
 * that can be used to calculate the spot month based on adding/removing days to fields defined on the security.
 *
 * @author manderson
 */
public class InvestmentSecuritySpotMonthDateFieldCalculator implements InvestmentSecuritySpotMonthCalculator, ValidationAware {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentCalculator investmentCalculator;

	////////////////////////////////////////////////////////////////////////////////


	/**
	 * The field to use to calculate the start of the spot month from
	 */
	private String startDateBeanPropertyName;

	/**
	 * Can be used for:
	 * 1. First Day of Month
	 * 2. First Weekday of Month (Selected weekday is required)
	 */
	private boolean startFromFirstDayOfMonth;

	/**
	 * Can be used for:
	 * 1. Last Day of Month
	 * 2. Last Weekday of Month (Selected weekday is required)
	 */
	private boolean startFromLastDayOfMonth;


	/**
	 * Can be used for:
	 * 3. Previous Friday from Start Date Field (Selected weekday is required)
	 */
	private boolean startFromPreviousDay;

	/**
	 * If startDateAdjustmentType requires a weekday selection
	 */
	private Short startDateWeekdayId;

	/**
	 * Calculate start date from startDateBeanPropertyName +/- these days
	 * Negative value means go back from the date, positive means go forward
	 * Applied AFTER startDateAdjustmentType is first processed.
	 */
	private Integer startBusinessDays;


	/**
	 * The field to use to calculate the end of the spot month
	 */
	private String endDateBeanPropertyName;

	/**
	 * Calculate end date from endDateBeanPropertyName +/- these days
	 * Negative value means go back from the date, positive means go forward
	 */
	private Integer endBusinessDays;


	/**
	 * Can be used for:
	 * Last Day of Month
	 */
	private boolean endFromLastDayOfMonth;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getStartDateBeanPropertyName(), "Start Date Bean Property Name is Required");
		Class<?> clazz = BeanUtils.getPropertyType(InvestmentSecurity.class, getStartDateBeanPropertyName());
		ValidationUtils.assertTrue(Date.class.equals(clazz), "Start Date Bean Property Must be a Date Field.");

		ValidationUtils.assertNotNull(getEndDateBeanPropertyName(), "End Date Bean Property Name is Required");
		clazz = BeanUtils.getPropertyType(InvestmentSecurity.class, getEndDateBeanPropertyName());
		ValidationUtils.assertTrue(Date.class.equals(clazz), "End Date Bean Property Must be a Date Field.");

		// If Any Are Set
		if (isStartFromFirstDayOfMonth() || isStartFromPreviousDay() || isStartFromLastDayOfMonth()) {
			String mutuallyExclusiveMessage = "Can only select one of start from 1. First Day of Month, 2. Previous Day, 3. Last Day of Month.";
			// Then only one can be set
			if (isStartFromFirstDayOfMonth()) {
				ValidationUtils.assertFalse(isStartFromPreviousDay() || isStartFromLastDayOfMonth(), mutuallyExclusiveMessage);
			}
			else if (isStartFromPreviousDay()) {
				ValidationUtils.assertFalse(isStartFromLastDayOfMonth(), mutuallyExclusiveMessage);
			}
		}
	}


	@Override
	public InvestmentSecuritySpotMonth calculateInvestmentSecuritySpotMonth(InvestmentSecurity security) {
		Date startDate = calculateFromDate(security, true);
		Date endDate = calculateFromDate(security, false);
		return new InvestmentSecuritySpotMonth(security, startDate, endDate);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////////


	private Date calculateFromDate(InvestmentSecurity security, boolean start) {
		String datePropertyName = start ? getStartDateBeanPropertyName() : getEndDateBeanPropertyName();
		Date date = (Date) BeanUtils.getPropertyValue(security, datePropertyName, false);
		ValidationUtils.assertNotNull(date, "Investment Security [" + security.getLabel() + "] is missing a date for property [" + datePropertyName + "].  Cannot calculate spot month.");
		if (start) {
			if (isStartFromFirstDayOfMonth()) {
				date = DateUtils.getFirstDayOfMonth(date);
				// Example: First Friday
				date = getWeekdayDate(date, getStartDateWeekdayId(), true);
			}
			else if (isStartFromLastDayOfMonth()) {
				date = DateUtils.getLastDayOfMonth(date);
				// Example: Last Friday
				date = getWeekdayDate(date, getStartDateWeekdayId(), false);
			}
			else if (isStartFromPreviousDay()) {
				// Go back one day so we don't include current date
				date = DateUtils.addDays(date, -1);
				// Example: Previous Friday
				date = getWeekdayDate(date, getStartDateWeekdayId(), false);
			}
		}
		else if (isEndFromLastDayOfMonth()) {
			date = DateUtils.getLastDayOfMonth(date);
		}
		Integer dayCount = start ? getStartBusinessDays() : getEndBusinessDays();
		if (dayCount != null && dayCount != 0) {
			date = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, getInvestmentCalculator().getInvestmentSecurityCalendar(security).getId()), dayCount);
		}
		return date;
	}


	/**
	 * Returns the "next" or "previous" date for the weekday selected
	 * i.e. get Next Friday, or get Previous Friday
	 */
	private Date getWeekdayDate(Date date, Short weekdayId, boolean next) {
		if (weekdayId != null) {
			while (DateUtils.getDayOfWeek(date) != weekdayId) {
				date = DateUtils.addDays(date, next ? 1 : -1);
			}
		}
		return date;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public String getStartDateBeanPropertyName() {
		return this.startDateBeanPropertyName;
	}


	public void setStartDateBeanPropertyName(String startDateBeanPropertyName) {
		this.startDateBeanPropertyName = startDateBeanPropertyName;
	}


	public Integer getStartBusinessDays() {
		return this.startBusinessDays;
	}


	public void setStartBusinessDays(Integer startBusinessDays) {
		this.startBusinessDays = startBusinessDays;
	}


	public boolean isStartFromFirstDayOfMonth() {
		return this.startFromFirstDayOfMonth;
	}


	public void setStartFromFirstDayOfMonth(boolean startFromFirstDayOfMonth) {
		this.startFromFirstDayOfMonth = startFromFirstDayOfMonth;
	}


	public boolean isStartFromPreviousDay() {
		return this.startFromPreviousDay;
	}


	public void setStartFromPreviousDay(boolean startFromPreviousDay) {
		this.startFromPreviousDay = startFromPreviousDay;
	}


	public Short getStartDateWeekdayId() {
		return this.startDateWeekdayId;
	}


	public void setStartDateWeekdayId(Short startDateWeekdayId) {
		this.startDateWeekdayId = startDateWeekdayId;
	}


	public String getEndDateBeanPropertyName() {
		return this.endDateBeanPropertyName;
	}


	public void setEndDateBeanPropertyName(String endDateBeanPropertyName) {
		this.endDateBeanPropertyName = endDateBeanPropertyName;
	}


	public Integer getEndBusinessDays() {
		return this.endBusinessDays;
	}


	public void setEndBusinessDays(Integer endBusinessDays) {
		this.endBusinessDays = endBusinessDays;
	}


	public boolean isStartFromLastDayOfMonth() {
		return this.startFromLastDayOfMonth;
	}


	public void setStartFromLastDayOfMonth(boolean startFromLastDayOfMonth) {
		this.startFromLastDayOfMonth = startFromLastDayOfMonth;
	}


	public boolean isEndFromLastDayOfMonth() {
		return this.endFromLastDayOfMonth;
	}


	public void setEndFromLastDayOfMonth(boolean endFromLastDayOfMonth) {
		this.endFromLastDayOfMonth = endFromLastDayOfMonth;
	}
}
