package com.clifton.investment.instrument.event.sequence.date;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.sequence.EventSequenceDefinition;
import com.clifton.investment.instrument.event.sequence.EventSequenceResetAccrualConventions;

import java.util.Date;


/**
 * The <code>InterestLegPaymentFactory</code> update the event dates for "Interest Leg Payment" event type.
 * <p/>
 * The input declareDate is instance date calculated from Initial Valuation Date.  The accrualStartDate will be the declareDate + (settlementCycle + 1) business days.
 * <p/>
 * The accrualEndDate is calculated by getting the next declareDate and adding settlementCycle business day.  The eventDate is set equal to accrualEndDate, and the
 * exDate is set to the first day of the month calculated with the accrualEndDate.
 * <p/>
 * The additionalDate or (fixingDate) is calculated by adjusting the declareDate by the fixingCycle.
 * <p/>
 * Example (for input declareDate = '6/29/12'):
 * accrualStartDate: 07/03/2012
 * accrualEndDate: 08/01/2012
 * exDate: 08/01/2012
 * accrualEndDate: 08/01/2012
 * additionalDate: 06/28/2012
 *
 * @author mwacker
 */
public class InterestLegPaymentDateCalculator extends AbstractDateCalculator {


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEvent calculateDates(Date declareDate, Date nextDeclareDate, InvestmentSecurityEvent event, EventSequenceDefinition sequenceDefinition, Schedule schedule) {
		if (ScheduleFrequencies.ONCE.name().equals(schedule.getFrequency())) {
			event.setAccrualStartDate(sequenceDefinition.getSecurity().getStartDate());
			if (sequenceDefinition.getResetAccrualConvention() == EventSequenceResetAccrualConventions.INTEREST_ACCRUES_ON_VALUATION_DATE) {
				event.setAccrualEndDate(sequenceDefinition.getSecurity().getEndDate());
			}
			else {
				event.setAccrualEndDate(sequenceDefinition.getSecurity().getLastDeliveryDate());
			}
			event.setExDate(DateUtils.addDays(sequenceDefinition.getSecurity().getEndDate(), 1));
			event.setPaymentDate(sequenceDefinition.getSecurity().getLastDeliveryDate());
			if (sequenceDefinition.getFixingCycle() != null) {
				// fixing calendar
				event.setAdditionalDate(getAdditionalDate(sequenceDefinition, sequenceDefinition.getSecurity().getStartDate()));
			}
		}
		else {
			// use payment calendar for accrual dates
			nextDeclareDate = getScheduleApiService().getScheduleOccurrence(ScheduleOccurrenceCommand.forOccurrences(schedule, 1, DateUtils.addMinutes(declareDate, 1)));
			declareDate = DateUtils.compare(declareDate, sequenceDefinition.getSecurity().getStartDate(), false) < 0 ? sequenceDefinition.getSecurity().getStartDate() : declareDate;
			Date accrualStartDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(declareDate, sequenceDefinition.getSettlementCalendar().getId()), sequenceDefinition.getSettlementCycle());

			boolean partialPeriod = false;
			// check if there is a partial period
			if ((nextDeclareDate == null) && (DateUtils.compare(declareDate, sequenceDefinition.getSecurity().getEndDate(), false) < 0)) {
				// check if the using the security end date will put the event data past the final payment date
				Date evtDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.addDays(declareDate, 1), sequenceDefinition.getSettlementCalendar().getId()), sequenceDefinition.getSettlementCycle());
				if (DateUtils.compare(evtDate, sequenceDefinition.getSecurity().getLastDeliveryDate(), false) >= 0) {
					return null;
				}
				nextDeclareDate = sequenceDefinition.getSecurity().getEndDate();
				partialPeriod = true;
			}
			else if (nextDeclareDate == null) {
				return null;
			}
			// use payment calendar for accrual dates
			Date accrualEndDate;
			Date exDate;
			Date paymentDate;

			switch (sequenceDefinition.getResetAccrualConvention()) {
				case INTEREST_ACCRUES_ON_MONTH_END: {
					ValidationUtils.assertTrue(ScheduleFrequencies.MONTHLY.name().equals(schedule.getFrequency()), "[" + sequenceDefinition.getResetAccrualConvention() + "] does not support ["
							+ sequenceDefinition.getSequenceFrequency() + "] Reset Frequency.");
					Date aclStartDate = getLastBusinessDayOfPreviousMonth(accrualStartDate);
					accrualEndDate = DateUtils.getLastDayOfMonth(accrualStartDate);
					if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(accrualEndDate))) {
						accrualEndDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(accrualEndDate), -1);
					}
					accrualStartDate = aclStartDate;

					exDate = DateUtils.addDays(accrualEndDate, 1);
					paymentDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(accrualEndDate, sequenceDefinition.getSettlementCalendar().getId()), sequenceDefinition.getSettlementCycle());
					break;
				}
				case INTEREST_ACCRUES_ON_VALUATION_DATE: {
					accrualEndDate = nextDeclareDate;
					accrualStartDate = declareDate;

					exDate = DateUtils.addDays(accrualEndDate, 1);
					paymentDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(accrualEndDate, sequenceDefinition.getSettlementCalendar().getId()), sequenceDefinition.getSettlementCycle());
					break;
				}
				default: {
					accrualEndDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(nextDeclareDate, sequenceDefinition.getSettlementCalendar().getId()), sequenceDefinition.getSettlementCycle());
					paymentDate = accrualEndDate;

					if (partialPeriod) {
						exDate = DateUtils.addDays(nextDeclareDate, 1);
					}
					else {
						exDate = DateUtils.getFirstDayOfMonth(accrualEndDate);
					}
					break;
				}
			}

			event.setAccrualStartDate(accrualStartDate);
			event.setAccrualEndDate(accrualEndDate);
			event.setExDate(exDate);
			event.setPaymentDate(paymentDate);
			if (sequenceDefinition.getFixingCycle() != null) {
				// fixing calendar
				event.setAdditionalDate(getAdditionalDate(sequenceDefinition, accrualStartDate));
			}
		}
		return event;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculate the additional date for the Floating leg event.
	 */
	private Date getAdditionalDate(EventSequenceDefinition sequenceDefinition, Date startDate) {
		if (MathUtils.isNullOrZero(sequenceDefinition.getFixingCycle())) {
			return getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(startDate, sequenceDefinition.getFixingCalendar().getId()), sequenceDefinition.getFixingCycle());
		}
		Calendar fixingCalendar = sequenceDefinition.getFixingCalendar();
		Integer fixingCycle = sequenceDefinition.getFixingCycle();
		BusinessDayConventions businessDayConventions = sequenceDefinition.getBusinessDayConvention();


		return getInvestmentEventSequenceRetrieverUtilHandler().getFixingDate(fixingCalendar, businessDayConventions
				, fixingCycle, startDate);
	}


	private Date getLastBusinessDayOfPreviousMonth(Date date) {
		Date lastDayOfMonth = DateUtils.getLastDayOfMonth(DateUtils.addMonths(date, -1));
		if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(lastDayOfMonth))) {
			return getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(lastDayOfMonth), -1);
		}
		return lastDayOfMonth;
	}
}
