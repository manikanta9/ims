package com.clifton.investment.instrument.upload.allocation;

import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;


/**
 * Used to populate data on {@link com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation}s that cannot be looked up from the database. Currently only functionality is populating the security
 *
 * @author mitchellf
 */
public interface InvestmentSecurityAllocationDataPopulator {

	public void populateAllocation(InvestmentSecurityAllocation allocation, InvestmentSecurityAllocationUploadCommand command, Status status);
}
