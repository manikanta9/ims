package com.clifton.investment.instrument.event.sequence;


import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.retriever.BaseInvestmentSecurityEventRetriever;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.event.sequence.date.InvestmentInstrumentEventSequenceDateCalculator;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class EventSequenceRetriever extends BaseInvestmentSecurityEventRetriever {

	private InvestmentInstrumentEventSequenceService investmentInstrumentEventSequenceService;
	private SystemColumnValueHandler systemColumnValueHandler;
	private CalendarSetupService calendarSetupService;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private String securityEventTypeName;
	private String firstSecurityEventTypeName;
	private InvestmentInstrumentEventSequenceDateCalculator dateCalculator;
	private InvestmentEventSequenceRetrieverUtilHandler investmentEventSequenceRetrieverUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String eventBusinessDayConventionCustomFieldName = "Business Day Convention";
	private String eventCalendarCustomFieldName;
	private String eventSettlementCalendarCustomFieldName;
	private String eventFixingCalendarCustomFieldName;

	private String eventSequenceStartDateCustomFieldName;
	private String eventFrequencyCustomFieldName;

	private String eventSettlementCycleCustomFieldName;
	private String eventFixingCycleCustomFieldName;

	private String eventResetAccrualConventionCustomFieldName;
	private String eventRecurrenceConventionCustomFieldName;

	/**
	 * The custom field name that contains the value to put in the beforeEventValue field.
	 */
	private String eventBeforeEventValueFieldName;
	/**
	 * The custom field name that contains the value to put in the afterEventValue field.
	 */
	private String eventAfterEventValueFieldName;

	/**
	 * Generate events starting at maturity backward.
	 */
	private boolean generateFromMaturityBackward = false;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEvent(InvestmentSecurity security) {
		EventSequenceDefinition sequenceDefinition = getSequenceDefinition(security, false);
		return getInvestmentInstrumentEventSequenceService().getSecurityEventNext(sequenceDefinition, DateUtils.clearTime(new Date()));
	}


	@Override
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventHistory(InvestmentSecurity security, boolean ignoreSeedEvents) {
		EventSequenceDefinition sequenceDefinition = getSequenceDefinition(security, ignoreSeedEvents);
		Date endDate = DateUtils.compare(security.getLastDeliveryDate(), security.getEndDate(), false) < 0 ? security.getEndDate() : security.getLastDeliveryDate();
		return getInvestmentInstrumentEventSequenceService().getSecurityEventList(sequenceDefinition, sequenceDefinition.getSequenceStartDate(), endDate);
	}


	@Override
	public boolean prepareInvestmentSecurityEvent(InvestmentSecurityEvent event) {
		if (event == null) {
			return false; // skip
		}

		// check if this is a duplicate event
		InvestmentSecurityEventSearchForm searchForm = getInvestmentEventSequenceRetrieverUtilHandler().getInvestmentSecurityEventDuplicateSearchForm(event);
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		return CollectionUtils.isEmpty(eventList);
	}


	private EventSequenceDefinition getSequenceDefinition(InvestmentSecurity security, boolean ignoreSeedEvents) {
		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setSecurity(security);
		sequence.setGenerateFromMaturityBackward(isGenerateFromMaturityBackward());

		sequence.setDateCalculator(getDateCalculator());
		updateSequenceCalendars(sequence, security);

		sequence.setBusinessDayConvention(BusinessDayConventions.valueOf((String) getSystemColumnValueHandler().getSystemColumnValueForEntity(security,
				InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, getEventBusinessDayConventionCustomFieldName(), false)));


		String frequency = getField(security, getEventFrequencyCustomFieldName());
		sequence.setSequenceFrequency(CouponFrequencies.getEnum(frequency));


		sequence.setSettlementCycle(getField(security, getEventSettlementCycleCustomFieldName()));
		sequence.setFixingCycle(getField(security, getEventFixingCycleCustomFieldName()));

		updateResetAccrualConvention(sequence, security);
		updateValuationConvention(sequence, security);
		updateEventValues(sequence, security);
		updateSequenceDatesAndFirstEvent(sequence, security, ignoreSeedEvents);
		updateEventType(sequence);

		return sequence;
	}


	private void updateResetAccrualConvention(EventSequenceDefinition sequence, InvestmentSecurity security) {
		EventSequenceResetAccrualConventions resetAccrualConvention = EventSequenceResetAccrualConventions.DEFAULT;
		String resetAccrualConventionValue = getField(security, getEventResetAccrualConventionCustomFieldName());
		if (resetAccrualConventionValue != null) {
			resetAccrualConvention = EventSequenceResetAccrualConventions.valueOf(resetAccrualConventionValue);
		}
		sequence.setResetAccrualConvention(resetAccrualConvention);
	}


	private void updateValuationConvention(EventSequenceDefinition sequence, InvestmentSecurity security) {
		EventSequenceRecurrenceConventions valuationConvention = EventSequenceRecurrenceConventions.DEFAULT;
		String valuationConventionValue = getField(security, getEventRecurrenceConventionCustomFieldName());
		if (valuationConventionValue != null) {
			valuationConvention = EventSequenceRecurrenceConventions.valueOf(valuationConventionValue);
		}
		sequence.setValuationConvention(valuationConvention);
	}


	private void updateEventValues(EventSequenceDefinition sequence, InvestmentSecurity security) {
		BigDecimal beforeEventValue = getField(security, getEventBeforeEventValueFieldName());
		sequence.setBeforeEventValue(beforeEventValue == null ? BigDecimal.ZERO : beforeEventValue);


		sequence.setAfterEventValue(BigDecimal.ZERO);
		if (!StringUtils.isEmpty(getEventAfterEventValueFieldName())) {
			// use the before value if they point to the same property
			if (getEventBeforeEventValueFieldName().equals(getEventAfterEventValueFieldName())) {
				sequence.setAfterEventValue(beforeEventValue);
			}
			else {
				BigDecimal value = getField(security, getEventBeforeEventValueFieldName());
				if (value != null) {
					sequence.setAfterEventValue(value);
				}
			}
		}
	}


	private void updateEventType(EventSequenceDefinition sequence) {
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(getSecurityEventTypeName());
		sequence.setType(eventType);
	}


	private void updateSequenceCalendars(EventSequenceDefinition sequence, InvestmentSecurity security) {
		sequence.setCalendar(getCalendarByName(security, getEventCalendarCustomFieldName()));
		sequence.setSettlementCalendar(getCalendarByName(security, getEventSettlementCalendarCustomFieldName()));
		sequence.setFixingCalendar(getCalendarByName(security, getEventFixingCalendarCustomFieldName()));
	}


	private void updateSequenceDatesAndFirstEvent(EventSequenceDefinition sequence, InvestmentSecurity security, boolean ignoreSeedEvents) {
		InvestmentSecurityEvent firstEventForRecordDate = null;
		InvestmentSecurityEvent firstEventOfCurrentType = null;

		if (!ignoreSeedEvents) {
			firstEventForRecordDate = getFirstEvent(security, StringUtils.isEmpty(getFirstSecurityEventTypeName()) ? getSecurityEventTypeName() : getFirstSecurityEventTypeName());
			if (!getSecurityEventTypeName().equals(getFirstSecurityEventTypeName())) {
				firstEventOfCurrentType = getFirstEvent(security, getSecurityEventTypeName());
			}
			else {
				firstEventOfCurrentType = firstEventForRecordDate;
			}
		}

		sequence.setSequenceStartDate(firstEventForRecordDate != null && firstEventOfCurrentType != null ? firstEventForRecordDate.getRecordDate() : getSequenceStartDate(sequence, security));
		sequence.setFirstSequenceEventExist(firstEventOfCurrentType != null);
		sequence.getSecurity().setEndDate(security.getEndDate());
	}


	private Date getSequenceStartDate(EventSequenceDefinition sequence, InvestmentSecurity security) {
		Date result;
		if (!StringUtils.isEmpty(getEventSequenceStartDateCustomFieldName()) && getField(security, getEventSequenceStartDateCustomFieldName()) != null) {
			result = getField(security, getEventSequenceStartDateCustomFieldName());
		}
		else {
			result = security.getStartDate();
		}

		switch (sequence.getValuationConvention()) {
			case MONTH_END_RECURRENCE:
				Date lastBusinessDateOfThisMonth = getCalendarDateGenerationHandler().generateDate(DateGenerationOptions.LAST_BUSINESS_DAY_OF_CURRENT_MONTH, result, sequence.getCalendar());
				if (DateUtils.compare(result, lastBusinessDateOfThisMonth, false) != 0) {
					result = getCalendarDateGenerationHandler().generateDate(DateGenerationOptions.LAST_BUSINESS_DAY_OF_PREVIOUS_MONTH, result, sequence.getCalendar());
				}
				break;
			default:
				break;
		}

		return result;
	}


	private Calendar getCalendarByName(InvestmentSecurity security, String name) {
		if (!StringUtils.isEmpty(name)) {
			Integer calendarId = getField(security, name);
			ValidationUtils.assertNotNull(calendarId, name + " is required to build an event sequence.");
			return getCalendarSetupService().getCalendar(calendarId.shortValue());
		}
		return null;
	}


	private InvestmentSecurityEvent getFirstEvent(InvestmentSecurity security, String eventTypeName) {
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(eventTypeName);

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeId(eventType.getId());
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		return CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(eventList, InvestmentSecurityEvent::getDeclareDate, true));
	}


	@SuppressWarnings("unchecked")
	private <T> T getField(InvestmentSecurity security, String name) {
		if (!StringUtils.isEmpty(name)) {
			Object value = getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, name, false);
			if (value != null) {
				return (T) value;
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentEventSequenceService getInvestmentInstrumentEventSequenceService() {
		return this.investmentInstrumentEventSequenceService;
	}


	public void setInvestmentInstrumentEventSequenceService(InvestmentInstrumentEventSequenceService investmentInstrumentEventSequenceService) {
		this.investmentInstrumentEventSequenceService = investmentInstrumentEventSequenceService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public String getSecurityEventTypeName() {
		return this.securityEventTypeName;
	}


	public void setSecurityEventTypeName(String securityEventTypeName) {
		this.securityEventTypeName = securityEventTypeName;
	}


	public InvestmentInstrumentEventSequenceDateCalculator getDateCalculator() {
		return this.dateCalculator;
	}


	public void setDateCalculator(InvestmentInstrumentEventSequenceDateCalculator dateCalculator) {
		this.dateCalculator = dateCalculator;
	}


	public String getEventCalendarCustomFieldName() {
		return this.eventCalendarCustomFieldName;
	}


	public void setEventCalendarCustomFieldName(String eventCalendarCustomFieldName) {
		this.eventCalendarCustomFieldName = eventCalendarCustomFieldName;
	}


	public String getEventSettlementCalendarCustomFieldName() {
		return this.eventSettlementCalendarCustomFieldName;
	}


	public void setEventSettlementCalendarCustomFieldName(String eventSettlementCalendarCustomFieldName) {
		this.eventSettlementCalendarCustomFieldName = eventSettlementCalendarCustomFieldName;
	}


	public String getEventFixingCalendarCustomFieldName() {
		return this.eventFixingCalendarCustomFieldName;
	}


	public void setEventFixingCalendarCustomFieldName(String eventFixingCalendarCustomFieldName) {
		this.eventFixingCalendarCustomFieldName = eventFixingCalendarCustomFieldName;
	}


	public String getEventSequenceStartDateCustomFieldName() {
		return this.eventSequenceStartDateCustomFieldName;
	}


	public void setEventSequenceStartDateCustomFieldName(String eventSequenceStartDateCustomFieldName) {
		this.eventSequenceStartDateCustomFieldName = eventSequenceStartDateCustomFieldName;
	}


	public String getEventFrequencyCustomFieldName() {
		return this.eventFrequencyCustomFieldName;
	}


	public void setEventFrequencyCustomFieldName(String eventFrequencyCustomFieldName) {
		this.eventFrequencyCustomFieldName = eventFrequencyCustomFieldName;
	}


	public String getEventSettlementCycleCustomFieldName() {
		return this.eventSettlementCycleCustomFieldName;
	}


	public void setEventSettlementCycleCustomFieldName(String eventSettlementCycleCustomFieldName) {
		this.eventSettlementCycleCustomFieldName = eventSettlementCycleCustomFieldName;
	}


	public String getEventFixingCycleCustomFieldName() {
		return this.eventFixingCycleCustomFieldName;
	}


	public void setEventFixingCycleCustomFieldName(String eventFixingCycleCustomFieldName) {
		this.eventFixingCycleCustomFieldName = eventFixingCycleCustomFieldName;
	}


	public String getEventBusinessDayConventionCustomFieldName() {
		return this.eventBusinessDayConventionCustomFieldName;
	}


	public void setEventBusinessDayConventionCustomFieldName(String eventBusinessDayConventionCustomFieldName) {
		this.eventBusinessDayConventionCustomFieldName = eventBusinessDayConventionCustomFieldName;
	}


	public String getFirstSecurityEventTypeName() {
		return this.firstSecurityEventTypeName;
	}


	public void setFirstSecurityEventTypeName(String firstSecurityEventTypeName) {
		this.firstSecurityEventTypeName = firstSecurityEventTypeName;
	}


	public String getEventResetAccrualConventionCustomFieldName() {
		return this.eventResetAccrualConventionCustomFieldName;
	}


	public void setEventResetAccrualConventionCustomFieldName(String eventResetAccrualConventionCustomFieldName) {
		this.eventResetAccrualConventionCustomFieldName = eventResetAccrualConventionCustomFieldName;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public boolean isGenerateFromMaturityBackward() {
		return this.generateFromMaturityBackward;
	}


	public void setGenerateFromMaturityBackward(boolean generateFromMaturityBackward) {
		this.generateFromMaturityBackward = generateFromMaturityBackward;
	}


	public String getEventBeforeEventValueFieldName() {
		return this.eventBeforeEventValueFieldName;
	}


	public void setEventBeforeEventValueFieldName(String eventBeforeEventValueFieldName) {
		this.eventBeforeEventValueFieldName = eventBeforeEventValueFieldName;
	}


	public String getEventAfterEventValueFieldName() {
		return this.eventAfterEventValueFieldName;
	}


	public void setEventAfterEventValueFieldName(String eventAfterEventValueFieldName) {
		this.eventAfterEventValueFieldName = eventAfterEventValueFieldName;
	}


	public String getEventRecurrenceConventionCustomFieldName() {
		return this.eventRecurrenceConventionCustomFieldName;
	}


	public void setEventRecurrenceConventionCustomFieldName(String eventRecurrenceConventionCustomFieldName) {
		this.eventRecurrenceConventionCustomFieldName = eventRecurrenceConventionCustomFieldName;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public InvestmentEventSequenceRetrieverUtilHandler getInvestmentEventSequenceRetrieverUtilHandler() {
		return this.investmentEventSequenceRetrieverUtilHandler;
	}


	public void setInvestmentEventSequenceRetrieverUtilHandler(InvestmentEventSequenceRetrieverUtilHandler investmentEventSequenceRetrieverUtilHandler) {
		this.investmentEventSequenceRetrieverUtilHandler = investmentEventSequenceRetrieverUtilHandler;
	}
}
