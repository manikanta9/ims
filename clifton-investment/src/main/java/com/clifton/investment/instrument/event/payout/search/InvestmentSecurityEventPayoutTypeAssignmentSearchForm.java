package com.clifton.investment.instrument.event.payout.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class InvestmentSecurityEventPayoutTypeAssignmentSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField(searchField = "referenceOne.id")
	private Short payoutTypeId;

	@SearchField(searchField = "referenceOne.id")
	private Short[] payoutTypeIds;

	@SearchField(searchField = "name", searchFieldPath = "referenceOne")
	private String payoutTypeName;

	@SearchField(searchField = "referenceTwo.id")
	private Short eventTypeId;

	@SearchField(searchField = "referenceTwo.id")
	private Short[] eventTypeIds;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo")
	private String eventTypeName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public Short getPayoutTypeId() {
		return this.payoutTypeId;
	}


	public void setPayoutTypeId(Short payoutTypeId) {
		this.payoutTypeId = payoutTypeId;
	}


	public Short[] getPayoutTypeIds() {
		return this.payoutTypeIds;
	}


	public void setPayoutTypeIds(Short[] payoutTypeIds) {
		this.payoutTypeIds = payoutTypeIds;
	}


	public String getPayoutTypeName() {
		return this.payoutTypeName;
	}


	public void setPayoutTypeName(String payoutTypeName) {
		this.payoutTypeName = payoutTypeName;
	}


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}


	public Short[] getEventTypeIds() {
		return this.eventTypeIds;
	}


	public void setEventTypeIds(Short[] eventTypeIds) {
		this.eventTypeIds = eventTypeIds;
	}


	public String getEventTypeName() {
		return this.eventTypeName;
	}


	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}
}
