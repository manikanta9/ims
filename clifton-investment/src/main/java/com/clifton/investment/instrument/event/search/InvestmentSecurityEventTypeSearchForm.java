package com.clifton.investment.instrument.event.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;

import java.math.BigDecimal;


/**
 * @author vgomelsky
 */
public class InvestmentSecurityEventTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean beforeSameAsAfter;

	@SearchField
	private Boolean valuePercent;

	@SearchField
	private Boolean additionalSecurityAllowed;

	@SearchField
	private Boolean additionalSecurityCurrency;

	@SearchField
	private Boolean eventValueInEventCurrencyUnits;

	@SearchField
	private Boolean newSecurityCreated;

	@SearchField
	private Boolean newPositionCreated;

	@SearchField
	private Boolean paymentDelayAllowed;

	@SearchField
	private Integer decimalPrecision;

	@SearchField
	private BigDecimal minValue;
	@SearchField
	private BigDecimal maxValue;

	@SearchField
	private Integer eventOrder;

	@SearchField
	private Boolean onePerEventDate;

	@SearchField
	private Boolean actionAllowed;

	@SearchField
	private Boolean withoutPayout;

	@SearchField
	private Boolean singlePayoutOnly;


	@SearchField
	private InvestmentSecurityEventType.EventBeforeStartTypes eventBeforeStartType;

	@SearchField
	private Boolean descriptionIncludedInNaturalKey;

	@SearchField(searchField = "name", searchFieldPath = "forceEventStatus")
	private String forceEventStatus;

	@SearchField(searchField = "hierarchyList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentInstrumentHierarchyId;

	@SearchField(searchField = "name")
	private String[] typeNames;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getBeforeSameAsAfter() {
		return this.beforeSameAsAfter;
	}


	public void setBeforeSameAsAfter(Boolean beforeSameAsAfter) {
		this.beforeSameAsAfter = beforeSameAsAfter;
	}


	public Boolean getValuePercent() {
		return this.valuePercent;
	}


	public void setValuePercent(Boolean valuePercent) {
		this.valuePercent = valuePercent;
	}


	public Boolean getAdditionalSecurityAllowed() {
		return this.additionalSecurityAllowed;
	}


	public void setAdditionalSecurityAllowed(Boolean additionalSecurityAllowed) {
		this.additionalSecurityAllowed = additionalSecurityAllowed;
	}


	public Boolean getAdditionalSecurityCurrency() {
		return this.additionalSecurityCurrency;
	}


	public void setAdditionalSecurityCurrency(Boolean additionalSecurityCurrency) {
		this.additionalSecurityCurrency = additionalSecurityCurrency;
	}


	public Boolean getEventValueInEventCurrencyUnits() {
		return this.eventValueInEventCurrencyUnits;
	}


	public void setEventValueInEventCurrencyUnits(Boolean eventValueInEventCurrencyUnits) {
		this.eventValueInEventCurrencyUnits = eventValueInEventCurrencyUnits;
	}


	public Boolean getNewSecurityCreated() {
		return this.newSecurityCreated;
	}


	public void setNewSecurityCreated(Boolean newSecurityCreated) {
		this.newSecurityCreated = newSecurityCreated;
	}


	public Boolean getNewPositionCreated() {
		return this.newPositionCreated;
	}


	public void setNewPositionCreated(Boolean newPositionCreated) {
		this.newPositionCreated = newPositionCreated;
	}


	public Boolean getPaymentDelayAllowed() {
		return this.paymentDelayAllowed;
	}


	public void setPaymentDelayAllowed(Boolean paymentDelayAllowed) {
		this.paymentDelayAllowed = paymentDelayAllowed;
	}


	public Integer getDecimalPrecision() {
		return this.decimalPrecision;
	}


	public void setDecimalPrecision(Integer decimalPrecision) {
		this.decimalPrecision = decimalPrecision;
	}


	public BigDecimal getMinValue() {
		return this.minValue;
	}


	public void setMinValue(BigDecimal minValue) {
		this.minValue = minValue;
	}


	public BigDecimal getMaxValue() {
		return this.maxValue;
	}


	public void setMaxValue(BigDecimal maxValue) {
		this.maxValue = maxValue;
	}


	public Integer getEventOrder() {
		return this.eventOrder;
	}


	public void setEventOrder(Integer eventOrder) {
		this.eventOrder = eventOrder;
	}


	public Boolean getOnePerEventDate() {
		return this.onePerEventDate;
	}


	public void setOnePerEventDate(Boolean onePerEventDate) {
		this.onePerEventDate = onePerEventDate;
	}


	public Boolean getActionAllowed() {
		return this.actionAllowed;
	}


	public void setActionAllowed(Boolean actionAllowed) {
		this.actionAllowed = actionAllowed;
	}


	public Boolean getWithoutPayout() {
		return this.withoutPayout;
	}


	public void setWithoutPayout(Boolean withoutPayout) {
		this.withoutPayout = withoutPayout;
	}


	public Boolean getSinglePayoutOnly() {
		return this.singlePayoutOnly;
	}


	public void setSinglePayoutOnly(Boolean singlePayoutOnly) {
		this.singlePayoutOnly = singlePayoutOnly;
	}


	public InvestmentSecurityEventType.EventBeforeStartTypes getEventBeforeStartType() {
		return this.eventBeforeStartType;
	}


	public void setEventBeforeStartType(InvestmentSecurityEventType.EventBeforeStartTypes eventBeforeStartType) {
		this.eventBeforeStartType = eventBeforeStartType;
	}


	public Boolean getDescriptionIncludedInNaturalKey() {
		return this.descriptionIncludedInNaturalKey;
	}


	public void setDescriptionIncludedInNaturalKey(Boolean descriptionIncludedInNaturalKey) {
		this.descriptionIncludedInNaturalKey = descriptionIncludedInNaturalKey;
	}


	public String getForceEventStatus() {
		return this.forceEventStatus;
	}


	public void setForceEventStatus(String forceEventStatus) {
		this.forceEventStatus = forceEventStatus;
	}


	public Short getInvestmentInstrumentHierarchyId() {
		return this.investmentInstrumentHierarchyId;
	}


	public void setInvestmentInstrumentHierarchyId(Short investmentInstrumentHierarchyId) {
		this.investmentInstrumentHierarchyId = investmentInstrumentHierarchyId;
	}


	public String[] getTypeNames() {
		return this.typeNames;
	}


	public void setTypeNames(String[] typeNames) {
		this.typeNames = typeNames;
	}
}
