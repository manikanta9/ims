package com.clifton.investment.instrument.validation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityUtilHandler;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The DAO validator for {@link InvestmentInstrument} entities.
 */
@Component
public class InvestmentInstrumentValidator extends SelfRegisteringDaoValidator<InvestmentInstrument> {

	private InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler;
	private SystemConditionService systemConditionService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentInstrument instrument, DaoEventTypes config) throws ValidationException {
		validateInstrumentForSpecificityConditions(instrument);

		if (instrument.getAccrualMethod() != null) {
			ValidationUtils.assertTrue(instrument.getHierarchy().getAccrualSecurityEventType() != null, "Accrual Method should not be set on an Instrument that does not have Accruals", "accrualMethod");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates the given instrument using the conditions applied via existing investment specificity entries.
	 *
	 * @param instrument the instrument to validate
	 */
	private void validateInstrumentForSpecificityConditions(InvestmentInstrument instrument) {
		// Get validators for the instrument using specificity
		List<InvestmentSpecificityEntry> instrumentValidatorHolders = getInvestmentSpecificityUtilHandler()
				.getInvestmentSpecificityEntryListForInstrument(InvestmentSpecificityDefinition.DEFINITION_INSTRUMENT_VALIDATOR, instrument);

		// Run all validators
		for (InvestmentSpecificityEntry validatorEntry : instrumentValidatorHolders) {
			InvestmentSpecificityInvestmentValidator validatorEntryBean = getInvestmentSpecificityUtilHandler().generatePopulatedSpecificityBean(validatorEntry);
			SystemCondition condition = getSystemConditionService().getSystemCondition(validatorEntryBean.getValidationConditionId());
			EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(condition, instrument);
			if (!result.isResult()) {
				InvestmentSpecificityField field = validatorEntry.getField();
				String beanPropertyName = (field.getSystemColumn() instanceof SystemColumnStandard)
						? ((SystemColumnStandard) field.getSystemColumn()).getBeanPropertyName()
						: null;
				throw new FieldValidationException(String.format("%s validation failed on field [%s] for condition [%s]: %s", InvestmentSpecificityDefinition.DEFINITION_INSTRUMENT_VALIDATOR, field.getLabel(), condition.getName(), result.getMessage()),
						beanPropertyName);
			}
		}

		// Check Applicability
		List<InvestmentSpecificityEntry> overrideEntryList = getInvestmentSpecificityUtilHandler()
				.getInvestmentSpecificityEntryListForInstrument(InvestmentSpecificityDefinition.DEFINITION_INSTRUMENT_UI_OVERRIDE, instrument);
		for (InvestmentSpecificityEntry overrideEntry : overrideEntryList) {
			InvestmentSpecificityColumnUiOverride overrideEntryBean = getInvestmentSpecificityUtilHandler().generatePopulatedSpecificityBean(overrideEntry);
			SystemColumnStandard systemColumn = (SystemColumnStandard) overrideEntry.getField().getSystemColumn();
			String beanPropertyName = systemColumn.getBeanPropertyName();
			Object propertyValue = BeanUtils.getPropertyValue(instrument, beanPropertyName);
			if (!overrideEntryBean.getFieldValueApplicability().getPredicate().test(propertyValue)) {
				throw new FieldValidationException(String.format("%s validation failed on field [%s] for applicability [%s]: %s"
						, InvestmentSpecificityDefinition.DEFINITION_INSTRUMENT_UI_OVERRIDE
						, overrideEntry.getField().getLabel()
						, overrideEntryBean.getFieldValueApplicability().toString()
						, overrideEntryBean.getFieldValueApplicability().getLabel()),
						beanPropertyName);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityUtilHandler getInvestmentSpecificityUtilHandler() {
		return this.investmentSpecificityUtilHandler;
	}


	public void setInvestmentSpecificityUtilHandler(InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler) {
		this.investmentSpecificityUtilHandler = investmentSpecificityUtilHandler;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
