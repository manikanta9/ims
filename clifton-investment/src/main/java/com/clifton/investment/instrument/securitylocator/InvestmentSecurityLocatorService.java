package com.clifton.investment.instrument.securitylocator;


import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>InvestmentSecurityLocatorService</code> interface allows "smart" security lookup which will
 * run through all registered data providers and will try to use their conventions to find the specified
 * security if direct match cannot be found.
 * <p/>
 * For example, Bloomberg locator can map the following 2 symbols to AAPL: "AAPL UW Equity", "AAPL Equity"
 *
 * @author vgomelsky
 */
public interface InvestmentSecurityLocatorService {

	/**
	 * Returns InvestmentSecurity for the specified symbol.  Queries all registered data providers
	 * in priority order in order to find security.
	 * <p/>
	 * For example, Bloomberg locator can map the following 3 symbols to AAPL: "AAPL UW Equity", "AAPL Equity", "AAPL UW"
	 *
	 * @param symbol
	 */
	public InvestmentSecurity getInvestmentSecurityByFullSymbol(String symbol);
}
