package com.clifton.investment.instrument.event.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentSecurityEventDetailListCache</code> caches the list of InvestmentSecurityEventDetail rows
 * for an InvestmentSecurityEvent
 *
 * @author manderson
 */
@Component
public class InvestmentSecurityEventDetailListCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentSecurityEventDetail, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "event.id";
	}


	@Override
	protected Integer getBeanKeyValue(InvestmentSecurityEventDetail bean) {
		if (bean.getEvent() != null) {
			return bean.getEvent().getId();
		}
		return null;
	}
}
