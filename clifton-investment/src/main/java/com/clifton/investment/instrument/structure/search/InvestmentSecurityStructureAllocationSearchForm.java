package com.clifton.investment.instrument.structure.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public class InvestmentSecurityStructureAllocationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "securityStructureWeight.id")
	private Integer securityStructureWeightId;

	@SearchField
	private Date measureDate;

	@SearchField(searchFieldPath = "securityStructureWeight", searchField = "security.id")
	private Integer securityId;

	@SearchField(searchFieldPath = "securityStructureWeight.instrumentWeight", searchField = "instrumentStructure.id")
	private Integer structureId;

	@SearchField(searchFieldPath = "securityStructureWeight.instrumentWeight", searchField = "order")
	private Integer order;

	@SearchField(searchFieldPath = "currentSecurity.instrument", searchField = "identifierPrefix,name", sortField = "name")
	private String currentSecurityInstrumentLabel;

	@SearchField(searchField = "currentSecurity.instrument.id")
	private Integer currentSecurityInstrumentId;

	@SearchField(searchField = "currentSecurity.instrument.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] currentSecurityInstrumentIds;

	@SearchField(searchFieldPath = "currentSecurity", searchField = "symbol,name", sortField = "symbol")
	private String currentSecurityLabel;

	@SearchField(searchField = "currentSecurity.id")
	private Integer currentSecurityId;

	@SearchField(searchField = "currentSecurity.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] currentSecurityIds;

	@SearchField(searchFieldPath = "securityStructureWeight", searchField = "weightOverride,instrumentWeight.weight", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal coalesceWeight;

	@SearchField
	private BigDecimal allocationWeight;

	@SearchField
	private BigDecimal allocationValueLocal;

	@SearchField
	private BigDecimal fxRateToBase;

	@SearchField
	private BigDecimal additionalAmount1;

	@SearchField
	private BigDecimal additionalAmount2;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getSecurityStructureWeightId() {
		return this.securityStructureWeightId;
	}


	public void setSecurityStructureWeightId(Integer securityStructureWeightId) {
		this.securityStructureWeightId = securityStructureWeightId;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer getStructureId() {
		return this.structureId;
	}


	public void setStructureId(Integer structureId) {
		this.structureId = structureId;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public String getCurrentSecurityInstrumentLabel() {
		return this.currentSecurityInstrumentLabel;
	}


	public void setCurrentSecurityInstrumentLabel(String currentSecurityInstrumentLabel) {
		this.currentSecurityInstrumentLabel = currentSecurityInstrumentLabel;
	}


	public Integer getCurrentSecurityInstrumentId() {
		return this.currentSecurityInstrumentId;
	}


	public void setCurrentSecurityInstrumentId(Integer currentSecurityInstrumentId) {
		this.currentSecurityInstrumentId = currentSecurityInstrumentId;
	}


	public Integer[] getCurrentSecurityInstrumentIds() {
		return this.currentSecurityInstrumentIds;
	}


	public void setCurrentSecurityInstrumentIds(Integer[] currentSecurityInstrumentIds) {
		this.currentSecurityInstrumentIds = currentSecurityInstrumentIds;
	}


	public String getCurrentSecurityLabel() {
		return this.currentSecurityLabel;
	}


	public void setCurrentSecurityLabel(String currentSecurityLabel) {
		this.currentSecurityLabel = currentSecurityLabel;
	}


	public Integer getCurrentSecurityId() {
		return this.currentSecurityId;
	}


	public void setCurrentSecurityId(Integer currentSecurityId) {
		this.currentSecurityId = currentSecurityId;
	}


	public Integer[] getCurrentSecurityIds() {
		return this.currentSecurityIds;
	}


	public void setCurrentSecurityIds(Integer[] currentSecurityIds) {
		this.currentSecurityIds = currentSecurityIds;
	}


	public BigDecimal getCoalesceWeight() {
		return this.coalesceWeight;
	}


	public void setCoalesceWeight(BigDecimal coalesceWeight) {
		this.coalesceWeight = coalesceWeight;
	}


	public BigDecimal getAllocationWeight() {
		return this.allocationWeight;
	}


	public void setAllocationWeight(BigDecimal allocationWeight) {
		this.allocationWeight = allocationWeight;
	}


	public BigDecimal getAllocationValueLocal() {
		return this.allocationValueLocal;
	}


	public void setAllocationValueLocal(BigDecimal allocationValueLocal) {
		this.allocationValueLocal = allocationValueLocal;
	}


	public BigDecimal getFxRateToBase() {
		return this.fxRateToBase;
	}


	public void setFxRateToBase(BigDecimal fxRateToBase) {
		this.fxRateToBase = fxRateToBase;
	}


	public BigDecimal getAdditionalAmount1() {
		return this.additionalAmount1;
	}


	public void setAdditionalAmount1(BigDecimal additionalAmount1) {
		this.additionalAmount1 = additionalAmount1;
	}


	public BigDecimal getAdditionalAmount2() {
		return this.additionalAmount2;
	}


	public void setAdditionalAmount2(BigDecimal additionalAmount2) {
		this.additionalAmount2 = additionalAmount2;
	}
}
