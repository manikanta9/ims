package com.clifton.investment.instrument.calculator;


import com.clifton.calendar.setup.Calendar;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.calculator.accrual.AccrualDatesCommand;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentCalculator</code> class defines utility methods that can be used to perform investment security
 * related calculations.
 *
 * @author vgomelsky
 */
public interface InvestmentCalculator {

	/**
	 * Returns trade date based on the specified transactionDate (with time) (uses current time if null) for the specified security.
	 * Depends on the exchange the security is traded in and it's operating hours and business days
	 * <p>
	 * Uses TRADE business days - If it's a partial day trade holiday, the exchanges partial day hours (if supplied) will be used
	 */
	public Date calculateTradeDate(InvestmentSecurity security, Date transactionDate);


	/**
	 * Returns settlement date based on the specified transaction date (trade date) for the specified security.
	 * Optional "settlementCurrency" parameter is required for spot currency trades that have settlement cycle
	 * defined per currency pair.
	 * <p>
	 * Uses SETTLEMENT business days
	 */
	public Date calculateSettlementDate(InvestmentSecurity security, InvestmentSecurity settlementCurrency, Date transactionDate);


	/**
	 * For Accrue Up To Dates:
	 * Calculates and returns "Accrual" Date (accrual is included on this date) for the specified security and dates.
	 * Uses security's accrualDateCalculator to perform calculation and returns the specified transactionDate if the calculator is not defined.
	 * <p>
	 * To get accurate market value, it's best to use the Settlement Date as opposed to Transaction Date when calculating accrual:
	 * if position were closed today, accrual would still be accounted up to Settlement Date. Default days to settle applies in most instances
	 * so using this calculation with corresponding calendar is the best approach in most cases.
	 * <p>
	 * Unfortunately, most banks do not use this approach: they use Transaction Date instead.
	 * In order to simplify reconciliation and reporting, we use transaction instead of settlement date for most bonds.
	 * Total Return Swaps use more accurate Settlement date based approach.
	 * <p>
	 * For Accrue From Dates:
	 * Returns accrual start date for the specified transaction (accrual is not included on this date: end of day industry convention).
	 * Most of the time it is accrual period start date and null is returned in this case.
	 * However, some securities (Total Return Swap upsizing) do not have a payment on position open and therefore calculate accrual only for the portion
	 * of the period that position was open.  Returns corresponding Transaction or Settlement Date in those cases based on accrualDateCalculator defined for this security.
	 * NOTE: because a security may have up to 2 accruals with different conventions, return both accrual dates.
	 */
	public AccrualDates calculateAccrualDates(AccrualDatesCommand command);


	/**
	 * For a given security returns the settlement calendar (if field is selected on hierarchy and a settlement calendar is defined for the security
	 * else the settlement calendar of the instrument
	 * else the exchange calendar
	 * else the default calendar
	 */
	public Calendar getInvestmentSecurityCalendar(InvestmentSecurity security);


	/**
	 * Returns the number of days to settle for the specified security.
	 * Optional "settlementCurrency" parameter is required for spot currency trades that have settlement cycle
	 * defined per currency pair.
	 */
	public Integer getInvestmentSecurityDaysToSettle(InvestmentSecurity security, InvestmentSecurity settlementCurrency);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal calculateNotional(InvestmentSecurity security, BigDecimal price, BigDecimal quantity, Date notionalMultiplierLookupDate);


	/**
	 * Calculates and returns accounting notional for the specified security, price, and quantity.
	 * Uses security specific calculator definitions for rounding, etc.
	 *
	 * @param security
	 * @param price
	 * @param quantity
	 * @param notionalMultiplier - this field is usually 1 (meaning no multiplier) or null which is the same.
	 *                           However, for inflation linked bonds (TIPS, etc.) it is equal to the index ratio and will multiply the quantity by this number
	 *                           unless instrument.hierarchy.notionalMultiplierForPriceNotUsed = true (old British linkers) no need to multiply twice.
	 */
	public BigDecimal calculateNotional(InvestmentSecurity security, BigDecimal price, BigDecimal quantity, BigDecimal notionalMultiplier);


	/**
	 * Calculates and returns accounting notional for the specified security, price, and quantity.
	 * Uses security specific calculator definitions for rounding, etc.
	 *
	 * @param security
	 * @param price
	 * @param quantity
	 * @param notionalMultiplier - this field is usually 1 (meaning no multiplier) or null which is the same.
	 *                           However, for inflation linked bonds (TIPS, etc.) it is equal to the index ratio and will multiply the quantity by this number
	 *                           unless instrument.hierarchy.notionalMultiplierForPriceNotUsed = true (old British linkers) no need to multiply twice.
	 * @param roundingCalculator - the calculator used to round the resulting notional value
	 */
	public BigDecimal calculateNotional(InvestmentSecurity security, BigDecimal price, BigDecimal quantity, BigDecimal notionalMultiplier, InvestmentNotionalCalculatorTypes roundingCalculator);


	/**
	 * Calculates and returns quantity for the specified security, price, and accounting notional.
	 * Uses security specific calculator definitions for rounding, etc.
	 *
	 * @param security
	 * @param price
	 * @param notional
	 * @param notionalMultiplier - this field is usually 1 (meaning no multiplier) or null which is the same.
	 *                           However, for inflation linked bonds (TIPS, etc.) it is equal to the index ratio and will multiply the quantity by this number
	 *                           unless instrument.hierarchy.notionalMultiplierForPriceNotUsed = true (old British linkers) no need to multiply twice.
	 */
	public BigDecimal calculateQuantityFromNotional(InvestmentSecurity security, BigDecimal price, BigDecimal notional, BigDecimal notionalMultiplier);


	/**
	 * Calculates and returns quantity for the specified security, price, and accounting notional.
	 * Uses security specific calculator definitions for rounding, etc.
	 *
	 * @param security
	 * @param price
	 * @param notional
	 * @param notionalMultiplier - this field is usually 1 (meaning no multiplier) or null which is the same.
	 *                           However, for inflation linked bonds (TIPS, etc.) it is equal to the index ratio and will multiply the quantity by this number
	 *                           unless instrument.hierarchy.notionalMultiplierForPriceNotUsed = true (old British linkers) no need to multiply twice.
	 * @param roundingCalculator - the calculator used to round the resulting notional value
	 */
	public BigDecimal calculateQuantityFromNotional(InvestmentSecurity security, BigDecimal price, BigDecimal notional, BigDecimal notionalMultiplier, InvestmentNotionalCalculatorTypes roundingCalculator);


	/**
	 * Calculates and returns exposure for the specified security, price, and quantity.
	 * Uses security specific calculator definitions for rounding, etc.
	 * NOTE: exposure is very similar to accounting notional except that it accounts for "Exposure Multiplier"
	 * which is 1 more most securities (Euro Dollar and Euro Yen have Exposure Multiplier = 4)
	 */
	public BigDecimal calculateExposure(InvestmentSecurity security, BigDecimal price, BigDecimal quantity);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates and returns Accrued Interest on the specified notional amount.
	 * The interest is calculated from start date of the interest period that the specified date fall into to the specified date.
	 * Uses the interest rate of the period for calculation and rounds result using security's notional calculator rules.
	 * Adds Accrued Interest calculated for first and second accrual events and returns the sum.
	 *
	 * @param security
	 * @param accrualBasis
	 * @param notionalMultiplier - this field is usually 1 (meaning no multiplier) or null which is the same.
	 *                           However, for inflation linked bonds (TIPS, etc.) it is equal to the index ratio and will multiply the quantity by this number
	 *                           unless instrument.hierarchy.notionalMultiplierForAccrualNotUsed = true (Zero Coupon Swaps) no need to multiply twice.
	 * @param accrualDates       Include accrued interest up to and including this "Up To" date(s).
	 *                           Optionally start interest accrual on this date as opposed to corresponding accrual period start (TRS upsized mid period).
	 */
	public BigDecimal calculateAccruedInterest(InvestmentSecurity security, AccrualBasis accrualBasis, BigDecimal notionalMultiplier, AccrualDates accrualDates);


	/**
	 * Use this method to separate accrual accruals for first and second events: only applicable to securities with 2 accrual legs: IRS.
	 */
	public BigDecimal calculateAccruedInterestForEvent1(InvestmentSecurity security, BigDecimal notional, BigDecimal notionalMultiplier, Date accrueUpToDate, Date accrueFromDate);


	/**
	 * Use this method to separate accrual accruals for first and second events: only applicable to securities with 2 accrual legs: IRS.
	 */
	public BigDecimal calculateAccruedInterestForEvent2(InvestmentSecurity security, BigDecimal notional, BigDecimal notionalMultiplier, Date accrueUpToDate, Date accrueFromDate);


	/**
	 * Calculates the amount of a single interest (coupon) payment for the specified notional.
	 * Looks up the coupon on the specified date and adjusts it for security's coupon payment frequency.
	 * <p>
	 * Returns the payment rounded using security's notional calculator rules.
	 *
	 * @param couponPaymentEvent
	 * @param accrualBasis
	 * @param notionalMultiplier - this field is usually 1 (meaning no multiplier) or null which is the same.
	 *                           However, for inflation linked bonds (TIPS, etc.) it is equal to the index ratio and will multiply the quantity by this number
	 *                           unless instrument.hierarchy.notionalMultiplierForAccrualNotUsed = true (Zero Coupon Swaps) no need to multiply twice.
	 * @param accrueFromDate     optionally start payment accrual on this date as opposed to corresponding accrual period start (TRS upsized mid period)
	 */
	public BigDecimal calculateInterestPayment(InvestmentSecurityEvent couponPaymentEvent, BigDecimal accrualBasis, BigDecimal notionalMultiplier, Date accrueFromDate);


	/**
	 * Calculates the amount of a single interest payment for the specified notional amount over a time period with a specified day count convention.
	 * NOTE: interest calculation includes interest on both start and end dates.
	 * <p>
	 * Returns the payment rounded to 2 decimal places.
	 *
	 * @param notional
	 * @param periodStartDate
	 * @param periodEndDate
	 * @param accrualStartDate   optional: usually same as periodStartDate. However, may be after period start for partial first month periods.
	 * @param dayCountConvention
	 * @param frequency
	 */
	public BigDecimal calculateInterestPayment(BigDecimal notional, BigDecimal annualRate, Date periodStartDate, Date periodEndDate, Date accrualStartDate, DayCountConventions dayCountConvention, CouponFrequencies frequency);


	/**
	 * Calculates the dirty price of the given security using the quantity, clean price, notional multiplier and date
	 * dirty price calculation includes accrued interest so needs coupon
	 * <p>
	 * Calculation uses Notional Calculator to determine how to calculate because how notional is calculated can also affect how Dirty Price is calculated (CDS and IRS are different)
	 * We also need to pass in a quantity, because for CDS and IRS the direction of the position held could affect the dirty price.
	 * Note - If quantity is zero, assumes a long position and will use 1,000,000,000 as value (i.e. use a sizable face) to calculate
	 * <p>
	 * Default Calculation: Dirty Price = Notional Multiplier * Clean Price * (1 + Accrued Interest / Notional)
	 * Discount Notional Calc = (Accrual/abs(QTY)/Price Multiplier/Notional Multiplier) + Clean Price
	 */
	public BigDecimal calculateDirtyPrice(InvestmentSecurity security, BigDecimal quantity, BigDecimal cleanPrice, Date date);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the notional multiplier for the specified security on the specified date.  Returns null if security does not support additional notional multiplier.
	 */
	public BigDecimal getNotionalMultiplier(InvestmentSecurity security, Date date);


	/**
	 * Returns the notional multiplier for the specified security on the specified date.  Returns null if security does not support additional notional multiplier.
	 * Settlement Date is used to make sure that the lookup does not happen before a position opening settles: uses the settlementDate when the date is before it.
	 */
	public BigDecimal getNotionalMultiplier(InvestmentSecurity security, Date date, Date settlementDate, boolean useNextDayForLookup);
}
