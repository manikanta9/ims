package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.calculator.DayCountConventions;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The BaseInvestmentAccrualCalculator abstract class contains common dependencies and API used by all accrual calculators.
 * <p>
 * Uses the following logic to retrieve the calendar used with business days aware accrual conventions:
 * COALESCE("Accrual Calendar" custom security field, "Settlement Calendar" custom security field, "Settlement Calendar" standard instrument field)
 *
 * @author vgomelsky
 */
public abstract class BaseInvestmentAccrualCalculator implements InvestmentAccrualCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentAccrualCalculatorFactory investmentAccrualCalculatorFactory;
	private SystemColumnValueHandler systemColumnValueHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final BigDecimal calculateAccruedInterest(InvestmentSecurityEvent paymentEvent, BigDecimal notional, BigDecimal notionalMultiplier, Date accrueUpToDate) {
		BigDecimal result = doCalculateAccruedInterest(paymentEvent, notional, notionalMultiplier, accrueUpToDate);
		// need to round result according to event's security local currency convention
		return InvestmentCalculatorUtils.roundLocalAmount(result, paymentEvent.getSecurity());
	}


	/**
	 * Actual implementation that returns result that has not been rounded to security's local currency.
	 */
	protected abstract BigDecimal doCalculateAccruedInterest(InvestmentSecurityEvent paymentEvent, BigDecimal notional, BigDecimal notionalMultiplier, Date accrueUpToDate);


	protected AccrualConfig getAccrualConfig(InvestmentSecurityEvent paymentEvent, boolean doNotNeedDaycountOrFrequency) {
		InvestmentSecurity security = paymentEvent.getSecurity();
		String paymentEventType = InvestmentUtils.getAccrualEventTypeName(security);
		ValidationUtils.assertNotNull(paymentEventType, "Cannot calculate Accrued Interest for security that does not have Accrual Event Type defined: " + paymentEvent);
		String couponFrequencyField;
		String dayCountConventionField;
		if (StringUtils.isEqual(paymentEventType, paymentEvent.getType().getName())) {
			// first accrual event
			couponFrequencyField = InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY;
			dayCountConventionField = InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION;
		}
		else {
			paymentEventType = InvestmentUtils.getAccrualEventTypeName2(security);
			ValidationUtils.assertNotNull(paymentEventType, "Cannot calculate Accrued Interest for security that does not have Accrual Event Type 2 defined (Accrual Event Type does not match): " + paymentEvent);
			if (StringUtils.isEqual(paymentEventType, paymentEvent.getType().getName())) {
				// second accrual event
				couponFrequencyField = InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY2;
				dayCountConventionField = InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION2;
			}
			else {
				throw new ValidationException("Security Accrual Event Type and Accrual Event Type 2 do not match event: " + paymentEvent);
			}
		}

		String couponFrequency = (String) getSecurityFieldValue(security, couponFrequencyField);
		CouponFrequencies frequency = null;
		if (!doNotNeedDaycountOrFrequency) {
			ValidationUtils.assertNotNull(couponFrequency, "'" + couponFrequencyField + "' is not defined for: " + security.getLabel());
			frequency = CouponFrequencies.getEnum(couponFrequency);
			ValidationUtils.assertFalse(frequency == CouponFrequencies.NONE, "Coupon frequency [" + frequency.getValue() + "] does not support interest calculations.");
		}

		DayCountConventions dayCountConvention = null;
		String dayCountConventionValue = (String) getSecurityFieldValue(security, dayCountConventionField);
		if (dayCountConventionValue != null) {
			dayCountConvention = DayCountConventions.getDayCountConventionByKey(dayCountConventionValue);
		}
		else if (!doNotNeedDaycountOrFrequency) {
			ValidationUtils.fail("'" + dayCountConventionField + "' is not defined for: " + security.getLabel());
		}

		AccrualConfig accrualConfig = AccrualConfig.of(frequency, dayCountConvention, couponFrequencyField);
		if (dayCountConvention != null && dayCountConvention.isBusinessDaysDependent()) {
			// need the calendar and business day service
			Number calendarId = (Number) getSecurityFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_ACCRUAL_CALENDAR);
			if (calendarId == null) {
				calendarId = (Number) getSecurityFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR);
				if (calendarId == null) {
					// if not defined as custom field, try to get the standard field from the instrument
					Calendar settlementCalendar = security.getInstrument().getSettlementCalendar();
					if (settlementCalendar != null) {
						calendarId = settlementCalendar.getId();
					}
				}
			}
			accrualConfig.withBusinessDayCalendar(getCalendarBusinessDayService(), calendarId == null ? null : calendarId.shortValue());
		}

		return accrualConfig;
	}


	protected Object getSecurityFieldValue(InvestmentSecurity security, String fieldName) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, fieldName, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentAccrualCalculatorFactory getInvestmentAccrualCalculatorFactory() {
		return this.investmentAccrualCalculatorFactory;
	}


	public void setInvestmentAccrualCalculatorFactory(InvestmentAccrualCalculatorFactory investmentAccrualCalculatorFactory) {
		this.investmentAccrualCalculatorFactory = investmentAccrualCalculatorFactory;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
