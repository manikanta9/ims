package com.clifton.investment.instrument.event;

import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The FractionalShares enum defines options for how fractional shares should be treated for a specific security event.
 * <p>
 * If not specified, defaults to {@link #DEFAULT}.
 *
 * @author vgomelsky
 */
public enum FractionalShares {

	CASH_IN_LIEU("Cash in Lieu") {
		@Override
		public BigDecimal round(BigDecimal quantity) {
			return quantity;
		}
	},

	ROUND_UP("Round Up") {
		@Override
		public BigDecimal round(BigDecimal quantity) {
			return MathUtils.round(quantity, 0, BigDecimal.ROUND_UP);
		}
	},

	ROUND_HALF_UP("Round Half Up") {
		@Override
		public BigDecimal round(BigDecimal quantity) {
			return MathUtils.round(quantity, 0, BigDecimal.ROUND_HALF_UP);
		}
	},

	ROUND_DOWN("Round Down") {
		@Override
		public BigDecimal round(BigDecimal quantity) {
			return MathUtils.round(quantity, 0, BigDecimal.ROUND_DOWN);
		}
	};

	public static final FractionalShares DEFAULT = CASH_IN_LIEU;

	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////


	FractionalShares(String name) {
		this.name = name;
	}


	private String name;


	public String getName() {
		return this.name;
	}


	/**
	 * Returns the {@code FractionalShares} with the provided name, or {@link #DEFAULT} if the name does not match an applicable value.
	 */
	public static FractionalShares forName(String name) {
		FractionalShares result = forNameStrict(name);
		return result == null ? DEFAULT : result;
	}


	/**
	 * Returns the {@code FractionalShares} with the provided name, or null if the name does not match an applicable value.
	 */
	public static FractionalShares forNameStrict(String name) {
		if (ROUND_DOWN.equalsIgnoreCase(name)) {
			return ROUND_DOWN;
		}
		if (ROUND_UP.equalsIgnoreCase(name)) {
			return ROUND_UP;
		}
		if (ROUND_HALF_UP.equalsIgnoreCase(name)) {
			return ROUND_HALF_UP;
		}
		if (CASH_IN_LIEU.equalsIgnoreCase(name)) {
			return CASH_IN_LIEU;
		}

		return null;
	}


	public boolean equalsIgnoreCase(String name) {
		if (name == null) {
			return false;
		}

		return getName().equalsIgnoreCase(name);
	}


	public abstract BigDecimal round(BigDecimal quantity);
}
