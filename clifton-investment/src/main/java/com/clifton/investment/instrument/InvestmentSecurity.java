package com.clifton.investment.instrument;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.shared.Security;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentSecurity</code> class represents individual instances of {@link InvestmentInstrument} objects.
 * Securities can be valued, traded and form positions.
 * <p>
 * Example:
 * - each future instrument will generally have 4 security instances per year.
 * - each stock option instrument will have a security instance for different exercise prices, expiration dates, and type (put/call).
 * - each stock instrument will have only one security instance.
 *
 * @author vgomelsky
 */
public class InvestmentSecurity extends NamedEntityWithoutLabel<Integer> implements SystemColumnCustomValueAware {

	public static final String TABLE_NAME = "InvestmentSecurity";

	/**
	 * Security event name for a newly created security. This event can trigger automatic market data value imports.
	 */
	public final static String INVESTMENT_SECURITY_CREATED_TODAY_EVENT = "InvestmentSecurityCreatedTodayEvent";
	/**
	 * Security event name for an update for a newly created security (same day).
	 * This event can trigger automatic market data value imports such as with new FLEX options with a reference security defined.
	 */
	public final static String NEW_INVESTMENT_SECURITY_UPDATED_TODAY_EVENT = "InvestmentSecurityUpdatedTodayEvent";
	/**
	 * Security event name for automatically updating a security by applying market data values based on field mappings.
	 */
	public static final String INVESTMENT_SECURITY_APPLY_MARKET_DATA_EVENT = "InvestmentSecurityApplyMarketDataEvent";

	/**
	 * Securities in different investment hierarchies may have hierarchy specific custom columns
	 * which are part of the column group with this name.
	 */
	public static final String SECURITY_CUSTOM_FIELDS_GROUP_NAME = "Security Custom Fields";

	/**
	 * Securities in different investment hierarchies may have hierarchy specific dated custom columns
	 * which are part of the column group with this name.
	 */
	public static final String SECURITY_DATED_CUSTOM_FIELDS_GROUP_NAME = "Dated Security Fields";

	/**
	 * Custom field that defines day count convention used to calculate accrued interest and payment for bonds.
	 * See DayCountConvention enum for available options.
	 */
	public static final String CUSTOM_FIELD_DAY_COUNT_CONVENTION = "Day Count";

	/**
	 * Custom field that defines day count convention used to calculate accrued interest and payment for bonds.
	 * See DayCountConvention enum for available options.
	 * Used when more than one accrual is used by a security: Interest Rate Swaps (Fixed and Floating legs).
	 */
	public static final String CUSTOM_FIELD_DAY_COUNT_CONVENTION2 = "Day Count 2";

	/**
	 * Custom field that defines how often payments are made: annual, quarterly, monthly, etc.
	 * See CouponFrequency enum for available options.
	 */
	public static final String CUSTOM_FIELD_COUPON_FREQUENCY = "Coupon Frequency";

	/**
	 * The coupon type describes how the bond's coupon amount is calculated at each coupon payment date.
	 * None, Zero, Fixed, Fixed OID, Floating, Variable, Adjustable, Step-up, Auction
	 */
	public static final String CUSTOM_FIELD_COUPON_TYPE = "Coupon Type";

	/**
	 * Custom field that defines how often payments are made: annual, quarterly, monthly, etc.
	 * See CouponFrequency enum for available options.
	 */
	public static final String CUSTOM_FIELD_PAYMENT_FREQUENCY = "Payment Frequency";

	/**
	 * Custom field that defines how often payments are made: annual, quarterly, monthly, etc.
	 * See CouponFrequency enum for available options.
	 * Used when more than one accrual is used by a security: Interest Rate Swaps (Fixed and Floating legs).
	 */
	public static final String CUSTOM_FIELD_COUPON_FREQUENCY2 = "Coupon Frequency 2";


	/**
	 * Custom field that defines the interest rate percentage.
	 */
	public static final String CUSTOM_FIELD_COUPON_AMOUNT_PERCENT = "Coupon Amount (%)";

	/**
	 * Custom field that defines the structure of a security (Interest Rate Swap): PAY_FLOAT or PAY_FIXED
	 */
	public static final String CUSTOM_FIELD_STRUCTURE = "Structure";

	/**
	 * Custom field that defines the number of entities within the underlying index when it was created (CDS).
	 */
	public static final String CUSTOM_FIELD_ORIGINAL_MEMBERS = "Original Members";
	/**
	 * The starting point for the tranche size that defines the amount of subordination a tranche enjoys. For example, enter 10 for 10%.
	 */
	public static final String CUSTOM_FIELD_ATTACHMENT_POINT = "Attachment Point";
	/**
	 * The ending point for the tranche size that defines the amount of subordination a tranche enjoys. For example, enter 15 for 15%.
	 */
	public static final String CUSTOM_FIELD_DETACHMENT_POINT = "Detachment Point";


	/**
	 * For inflation linked securities (TIPS, Foreign Linkers), identifies "security" that has "Value" market
	 * data field value that is the Index Ratio for this security.  For inflation linked securities, coupon
	 * rate usually stays the same but the Face used to calculate the coupon is adjusted by the index ratio:
	 * Inflation Adjusted Face = Original Face * Index Ratio
	 */
	public static final String CUSTOM_FIELD_INDEX_RATIO = "Index Ratio";


	/**
	 * The Inflation Index custom field stores an Investment Security ID value used to identify the security which has associated MarketDataValue entries representing
	 * inflation index values.
	 */
	public static final String CUSTOM_FIELD_INFLATION_INDEX = "Inflation Index";

	/**
	 * An optional custom field that defines the Base Inflation Value used for calculating an Index Ratio.
	 */
	public static final String CUSTOM_FIELD_BASE_INFLATION_VALUE = "Base Inflation Value";

	/**
	 * Number of inflation reporting periods, denoted in months, between the effective period of the inflation rate and the period in which the associated determination date occurs.
	 * For Australia and New Zealand where inflation is reported on a quarterly basis and number is denoted in quarters.
	 */
	public static final String CUSTOM_FIELD_INFLATION_REPORTING_PERIODS = "Inflation Reporting Periods";

	/**
	 * Ratio between the future value and the present value notional for the swap (zero coupon) at the point of trading.
	 */
	public static final String CUSTOM_FIELD_FV_NOTIONAL_MULTIPLIER = "FV Notional Multiplier";


	/**
	 * Custom field that defines how equity leg is reset for total return swaps:
	 * KEEP_QUANTITY, KEEP_NOTIONAL, etc.  See ResetType enum for available options.
	 */
	public static final String CUSTOM_FIELD_RESET_TYPE = "Reset Type";

	/**
	 * Specifies the number of business days it takes to settle trades for this security (IRS, TRS, etc.)
	 */
	public static final String CUSTOM_FIELD_SETTLEMENT_CYCLE = "Settlement Cycle";
	/**
	 * Specifies the calendar to use when calculating settlement date (IRS, TRS, etc.)
	 */
	public static final String CUSTOM_FIELD_SETTLEMENT_CALENDAR = "Settlement Calendar";
	/**
	 * Specifies the calendar to use when calculating accruals that use day count convention that requires business days.
	 * For example, Brazilian Zero Coupon Swaps that use BUS/252.
	 */
	public static final String CUSTOM_FIELD_ACCRUAL_CALENDAR = "Accrual Calendar";

	/**
	 * Custom field that defines the spread for the interest leg of a Swap
	 */
	public static final String CUSTOM_FIELD_SPREAD = "Spread";

	/**
	 * Calendar used to determine business days for fixing date calculation.
	 */
	public static final String CUSTOM_FIELD_FIXING_CALENDAR = "Fixing Calendar";

	/**
	 * Number of business days using Fixing Calendar from accrual period end date to interest Reset Date.
	 */
	public static final String CUSTOM_FIELD_FIXING_CYCLE = "Fixing Cycle";

	/**
	 * Used by Variance Swaps: Equity notional expressed in "Vega Units" in which one unit is equal to a "Variance Unit" multiplied for 2 times the volatility reference price.
	 */
	public static final String CUSTOM_FIELD_VEGA_NOTIONAL_AMOUNT = "Vega Notional Amount";

	/**
	 * Used for Dynamic Security Allocations that reference a replication in order to dynamically generate weights for its allocations.
	 */
	public static final String CUSTOM_FIELD_INVESTMENT_REPLICATION = "Investment Replication";
	public static final String CUSTOM_FIELD_INVESTMENT_REPLICATION_2 = "Investment Replication 2";
	public static final String CUSTOM_FIELD_ALLOCATED_SECURITY_CALCULATION_START_DATE = "Allocation Calculation Start Date";

	/**
	 * Used for Weighted Return Rebalanced Monthly to indicate which business day of the month to rebalance
	 */
	public static final String CUSTOM_FIELD_REBALANCE_BUSINESS_DAY = "Rebalance Business Day";

	/**
	 * Business day convention to use when calculating Fixed Leg payment dates.
	 */
	public static final String CUSTOM_FIELD_BUSINESS_DAY_CONVENTION = "Business Day Convention";

	/**
	 * Business day convention to use when calculating Floating Leg payment dates.
	 */
	public static final String CUSTOM_FIELD_FLOATING_BUSINESS_DAY_CONVENTION = "Floating Business Day Convention";

	/**
	 * The number of years to maturity for the benchmark index. Usually 1, 2, 3, 4, 5, 7 or 10.
	 */
	public static final String CUSTOM_FIELD_TENOR = "Tenor";

	public static final String CUSTOM_FIELD_SYNTHETIC_BENCHMARK = "Synthetic Benchmark";

	/**
	 * Independent Amount % used for collateral calculations.
	 */
	public static final String INDEPENDENT_AMOUNT_PERCENT_CUSTOM_COLUMN = "Independent Amount %";

	/**
	 * Specify if the market data represents an annual rate, semi-annual, etc.
	 */
	public static final String PERIODICITY = "Periodicity";

	/**
	 * Specify the minimum months the nearest rate within market data will represent (typically 6 or 12 months).
	 */
	public static final String MIN_RATE_MONTHS = "Min Rate Months";

	/**
	 * Specify the maximum months the farthest rate within market data will represent (typically 600 months).
	 */
	public static final String MAX_RATE_MONTHS = "Max Rate Months";

	/**
	 * Specify the number of months the rate fields within market data will increase (i.e. 1.5 Year Rate, 2 Year Rate, etc. would have 6 months as its value).
	 */
	public static final String INTERVAL_RATE_MONTHS = "Interval Rate Months";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * The Instrument for this Security
	 */
	private InvestmentInstrument instrument;

	/**
	 * Optional business contract that this security was created under (ISDA for OTC securities).
	 */
	private BusinessContract businessContract;
	/**
	 * Counterparty company for the selected business contract this security applies to for Swaps.
	 * Or an Issuer when applicable (bonds, stocks, etc.)
	 */
	private BusinessCompany businessCompany;

	/**
	 * Investment instrument has "underlyingInstrument" field which most of the time points to underlying index.
	 * However, there are cases when underlying instrument is a one-to-many security (Options On Futures).
	 * Therefore it's important to specify the underlying security vs just the instrument.
	 * If underlying is one-to-one security then this field should be set automatically by corresponding service.
	 */
	private InvestmentSecurity underlyingSecurity;

	/**
	 * If instrument has a BigInstrument reference, the BigSecurity is the
	 * matching security from the BigInstrument for this security.
	 */
	private InvestmentSecurity bigSecurity;

	/**
	 * Optional InvestmentSecurity reference that can be used for obtaining data for this security.
	 * <p>
	 * An example usage is for pricing FLEX Options. A Listed Option is used as a reference to pull
	 * volatility values that can be used for theoretical pricing functions such as Black-Scholes.
	 */
	private InvestmentSecurity referenceSecurity;

	/**
	 * In rare cases for OTC securities (TRS, etc.), it maybe possible to settle all cash flows in a currency other
	 * than the currency denomination of security. This field should be populated in those cases and will be used to default
	 * Settlement Currency for corresponding trades and events.
	 */
	private InvestmentSecurity settlementCurrency;

	/**
	 * A short abbreviation used to uniquely identify publicly traded shares of a particular stock on a particular stock market.
	 * Also known as stock symbol, ticker symbol, option symbol, etc.
	 */
	private String symbol;
	/**
	 * 9-character alphanumeric security identifier distributed by Committee on Uniform Security Identification Procedures
	 * for the purposes of facilitating clearing and settlement of trades.
	 */
	private String cusip;
	/**
	 * An International Securities Identification Number (ISIN) uniquely identifies a security. Its structure is defined in ISO 6166.
	 * Securities for which ISINs are issued include bonds, commercial paper, stocks and warrants. The ISIN code is a 12-character
	 * alpha-numerical code that does not contain information characterizing financial instruments but serves for uniform identification
	 * of a security at trading and settlement.
	 */
	private String isin;
	/**
	 * SEDOL stands for Stock Exchange Daily Official List, a list of security identifiers used in the United Kingdom and Ireland for
	 * clearing purposes. The numbers are assigned by the London Stock Exchange, on request by the security issuer. SEDOL serves as the
	 * National Securities Identifying Number for all securities issued in the United Kingdom and are therefore part of the security's ISIN as well.
	 * The SEDOL Master File (SMF) provides reference data on millions of global multi-asset securities each uniquely identified at the
	 * market level using a universal SEDOL code.
	 */
	private String sedol;
	/**
	 * Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character
	 * unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued
	 * per trading venue.
	 */
	private String figi;
	/**
	 * OCC Symbol is a unique code used to identify Options on a futures exchange.  Options Clearing Corporation's (OCC) Options Symbology Initiative (OSI)
	 * mandated an industry-wide change to this methodology in 2010. We use it to reconcile and confirm trades with external parties.
	 */
	private String occSymbol;

	/**
	 * Flags if this security is a Currency
	 */
	private boolean currency;

	/**
	 * Some securities will need to override the instrument level price multiplier
	 * EXAMPLE: FNH6  UK security where each security for the instrument differs based on number of days in the month
	 * This field is allowed if the hierarchy option is set for: securityPriceMultiplierOverrideAllowed
	 */
	private BigDecimal priceMultiplierOverride;

	/**
	 * The date of First Trade. Trading security before this date is not allowed.
	 */
	private Date startDate;
	/**
	 * The date of Last Trade. Trading security after this date is not allowed.
	 */
	private Date endDate;
	/**
	 * For securities that allow early termination (Swaps, bonds that are fully called, etc.),
	 * specifies the date of early termination. If set, this field will override endDate for
	 * determining whether a security is active. Note, this field is necessary because we can't
	 * just update the endDate.
	 */
	private Date earlyTerminationDate;

	// specific to futures when instrument.deliverable == true (could also be used by other securities: closing of a position is allowed during delivery period)
	// NOTE: FIRST NOTICE DATE ALSO USED AS VALUATION DATE FOR OPTIONS - USED FOR OPTIONS ON FUTURES FOR EXAMPLE WHEN CALCULATING CURRENT UNDERLYING SECURITY (FUTURE) FOR THE OPTION
	private Date firstNoticeDate; // for non-deliverable securities (Cash Settled) may indicate Valuation Date if one exists: can get a future from option on future on this date and immediately cash settle it
	private Date firstDeliveryDate;
	private Date lastDeliveryDate;

	/**
	 * Illiquid securities cannot be easily sold or exchanged for cash without a substantial loss in value.
	 * This indicator can be used by compliance rules: 40 Act Fund 15% Illiquid Securities Test
	 */
	private boolean illiquid;

	// properties for Option securities
	private InvestmentSecurityOptionTypes optionType;

	private OptionStyleTypes optionStyle;

	private ExpiryTimeValues settlementExpiryTime;

	private BigDecimal optionStrikePrice;

	/**
	 * Defines accrual method that should be used when calculating accrual for this securities.
	 * Most of the time it's DAYCOUNT. However, some swaps may use custom calculations.
	 * If not set, instrument accrual method or hierarchy accrual method is used. Can only be set when accrualSecurityEventType is defined.
	 */
	private AccrualMethods accrualMethod;

	private AccrualMethods accrualMethod2;

	/**
	 * A List of custom column values for this security (field are assigned and vary by investment hierarchy)
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;


	/**
	 * This field is not persisted in the database but used to request that when a one-to-one security is updated,
	 * to use underlying security from security vs underlying instrument from security's instrument as the true source for the underlying.
	 */
	@NonPersistentField
	private boolean underlyingSecurityFromSecurityUsed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * For convenience with testing
	 */
	public InvestmentSecurity(String symbol, int id) {
		this(symbol);
		this.setId(id);
	}


	public InvestmentSecurity(String symbol) {
		this();
		this.symbol = symbol;
	}


	public InvestmentSecurity() {
		super();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Security toSecurity() {
		Security result = new Security();
		if (getId() != null) {
			result.setId(getId());
		}
		result.setSymbol(getSymbol());
		result.setCusip(getCusip());
		result.setIsin(getIsin());
		result.setSedol(getSedol());
		result.setOccSymbol(getOccSymbol());
		result.setName(getName());
		result.setEndDate(getEndDate());
		InvestmentInstrument securityInstrument = getInstrument();
		if (securityInstrument != null) {
			if (securityInstrument.getId() != null) {
				result.setInstrumentId(securityInstrument.getId());
			}
			if (securityInstrument.getTradingCurrency() != null) {
				result.setCurrencyDenominationSymbol(securityInstrument.getTradingCurrency().getSymbol());
			}
			if (securityInstrument.getExchange() != null) {
				result.setPrimaryExchangeCode(securityInstrument.getExchange().getExchangeCode());
			}
			if (securityInstrument.getCompositeExchange() != null) {
				result.setCompositeExchangeCode(securityInstrument.getCompositeExchange().getExchangeCode());
			}
			InvestmentInstrumentHierarchy hierarchy = securityInstrument.getHierarchy();
			if (hierarchy != null) {
				if (hierarchy.getInvestmentType() != null) {
					result.setSecurityType(hierarchy.getInvestmentType().getName());
				}
				if (hierarchy.getInvestmentTypeSubType() != null) {
					result.setSecurityTypeSubType(hierarchy.getInvestmentTypeSubType().getName());
				}
				if (hierarchy.getInvestmentTypeSubType2() != null) {
					result.setSecurityTypeSubType2(hierarchy.getInvestmentTypeSubType2().getName());
				}
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String result = getName();
		if (this.symbol != null) {
			if (result == null) {
				result = this.symbol;
			}
			else if (!result.equalsIgnoreCase(this.symbol)) {
				result = this.symbol + " (" + result + ')';
			}
		}

		if (getEndDate() != null && !isActive()) {
			result += " [INACTIVE]";
		}

		return result;
	}


	/**
	 * Returns label for security with labels for UX
	 * Hierarchy - Instrument Identifier Prefix - Symbol
	 */
	public String getUniqueLabel() {
		StringBuilder result = new StringBuilder();
		if (getInstrument() != null) {
			if (getInstrument().getHierarchy() != null) {
				result.append(getInstrument().getHierarchy().getNameExpanded());
				result.append(": ");
			}
			result.append(getInstrument().getIdentifierPrefix());
			result.append(": ");
		}
		result.append(getSymbol());
		return result.toString();
	}


	public String getLabelWithBigSecurity() {
		if (getBigSecurity() == null) {
			return getLabel();
		}
		return getBigSecurity().getLabel() + " (Big Security for: " + getLabel() + ")";
	}


	/**
	 * Returns true if the specified exchangeCode matches the Primary exchange code for this security.
	 */
	public boolean belongsToPrimaryExchange(String exchangeCode) {
		InvestmentInstrument securityInstrument = getInstrument();
		if (securityInstrument == null) {
			return false;
		}
		InvestmentExchange primaryExchange = securityInstrument.getExchange();
		return (primaryExchange != null && exchangeCode.equals(primaryExchange.getExchangeCode()));
	}


	/**
	 * Returns true if the specified exchangeCode matches the Primary or Composite exchange code for this security.
	 */
	public boolean belongsToPrimaryOrCompositeExchange(String exchangeCode) {
		InvestmentInstrument securityInstrument = getInstrument();
		if (securityInstrument == null) {
			return false;
		}
		InvestmentExchange primaryExchange = securityInstrument.getExchange();
		InvestmentExchange compositeExchange = securityInstrument.getCompositeExchange();
		return (primaryExchange != null && exchangeCode.equals(primaryExchange.getExchangeCode())) || (compositeExchange != null && exchangeCode.equals(compositeExchange.getExchangeCode()));
	}


	public boolean isActiveOn(Date date) {
		return DateUtils.isDateBetween(date, this.startDate, ObjectUtils.coalesce(this.earlyTerminationDate, this.endDate), false);
	}


	public boolean isActive() {
		return isActiveOn(new Date());
	}


	/**
	 * Returns the last date when positions for this security are allowed.
	 * <p>
	 * If Early Termination Date is set, then this date is used (overrides other dates).
	 * <p>
	 * If both endDate and firstDeliveryDate are defined and firstDeliveryDate is after the endDate,
	 * then returns firstDeliveryDate. Otherwise returns endDate.
	 * <p>
	 * Example, LME positions are closed on firstDeliveryDate (which is also same as lastDeliveryDate).
	 */
	public Date getLastPositionDate() {
		Date lastDate = getEarlyTerminationDate();
		if (lastDate == null) {
			lastDate = getEndDate();
			if (lastDate != null) {
				if (getFirstDeliveryDate() != null) {
					if (lastDate.before(getFirstDeliveryDate())) {
						lastDate = getFirstDeliveryDate();
					}
				}
			}
		}
		return lastDate;
	}


	/**
	 * Last Delivery Date is not required, so if not populated use the end date
	 * Used for Compliance Checks that use "spot" month which breaks out limits by their delivery month
	 */
	public Date getLastDeliveryOrEndDate() {
		if (getLastDeliveryDate() == null) {
			return ObjectUtils.coalesce(getEarlyTerminationDate(), getEndDate());
		}
		return getLastDeliveryDate();
	}


	public Date getFirstNoticeDateOrEndDate() {
		if (getFirstNoticeDate() == null) {
			return ObjectUtils.coalesce(getEarlyTerminationDate(), getEndDate());
		}
		return getFirstNoticeDate();
	}


	public boolean isAllocationOfSecurities() {
		if (getInstrument() != null && getInstrument().getHierarchy() != null) {
			return getInstrument().getHierarchy().isAllocationOfSecurities();
		}
		return false;
	}


	public boolean isAllocationOfSecuritiesSystemCalculated() {
		if (getInstrument() != null && getInstrument().getHierarchy() != null && getInstrument().getHierarchy().isAllocationOfSecurities()) {
			return getInstrument().getHierarchy().getSecurityAllocationType().getCalculatedFieldType().isSystemCalculated();
		}
		return false;
	}


	/**
	 * Returns the price multiplier for this security - If no override present will return the price multiplier on the instrument
	 */
	public BigDecimal getPriceMultiplier() {
		if (getPriceMultiplierOverride() != null) {
			return getPriceMultiplierOverride();
		}
		if (getInstrument() != null) {
			return getInstrument().getPriceMultiplier();
		}
		return null;
	}


	/**
	 * Returns true if the security is a PUT option.  Used to facilitate identification of the Option's type.
	 */
	public boolean isPutOption() {
		return getOptionType() == InvestmentSecurityOptionTypes.PUT;
	}


	/**
	 * Returns true if the security is a CALL option.  Used to facilitate identification of the Option's type.
	 */
	public boolean isCallOption() {
		return getOptionType() == InvestmentSecurityOptionTypes.CALL;
	}
	////////////////////////////////////////////////////////////


	public InvestmentInstrument getInstrument() {
		return this.instrument;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isCurrency() {
		return this.currency;
	}


	public void setCurrency(boolean currency) {
		this.currency = currency;
	}


	public Date getFirstNoticeDate() {
		return this.firstNoticeDate;
	}


	public void setFirstNoticeDate(Date firstNoticeDate) {
		this.firstNoticeDate = firstNoticeDate;
	}


	public Date getFirstDeliveryDate() {
		return this.firstDeliveryDate;
	}


	public void setFirstDeliveryDate(Date firstDeliveryDate) {
		this.firstDeliveryDate = firstDeliveryDate;
	}


	public Date getLastDeliveryDate() {
		return this.lastDeliveryDate;
	}


	public void setLastDeliveryDate(Date lastDeliveryDate) {
		this.lastDeliveryDate = lastDeliveryDate;
	}


	public BusinessContract getBusinessContract() {
		return this.businessContract;
	}


	public void setBusinessContract(BusinessContract businessContract) {
		this.businessContract = businessContract;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	public InvestmentSecurity getBigSecurity() {
		return this.bigSecurity;
	}


	public void setBigSecurity(InvestmentSecurity bigSecurity) {
		this.bigSecurity = bigSecurity;
	}


	public InvestmentSecurity getReferenceSecurity() {
		return this.referenceSecurity;
	}


	public void setReferenceSecurity(InvestmentSecurity referenceSecurity) {
		this.referenceSecurity = referenceSecurity;
	}


	public InvestmentSecurity getSettlementCurrency() {
		return this.settlementCurrency;
	}


	public void setSettlementCurrency(InvestmentSecurity settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}


	public BusinessCompany getBusinessCompany() {
		return this.businessCompany;
	}


	public void setBusinessCompany(BusinessCompany businessCompany) {
		this.businessCompany = businessCompany;
	}


	public String getSedol() {
		return this.sedol;
	}


	public void setSedol(String sedol) {
		this.sedol = sedol;
	}


	public String getFigi() {
		return this.figi;
	}


	public void setFigi(String figi) {
		this.figi = figi;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public String getOccSymbol() {
		return this.occSymbol;
	}


	public void setOccSymbol(String occSymbol) {
		this.occSymbol = occSymbol;
	}


	public boolean isIlliquid() {
		return this.illiquid;
	}


	public void setIlliquid(boolean illiquid) {
		this.illiquid = illiquid;
	}


	public InvestmentSecurityOptionTypes getOptionType() {
		return this.optionType;
	}


	public void setOptionType(InvestmentSecurityOptionTypes optionType) {
		this.optionType = optionType;
	}


	public OptionStyleTypes getOptionStyle() {
		return this.optionStyle;
	}


	public void setOptionStyle(OptionStyleTypes optionStyle) {
		this.optionStyle = optionStyle;
	}


	public ExpiryTimeValues getSettlementExpiryTime() {
		return this.settlementExpiryTime;
	}


	public void setSettlementExpiryTime(ExpiryTimeValues settlementExpiryTime) {
		this.settlementExpiryTime = settlementExpiryTime;
	}


	public BigDecimal getOptionStrikePrice() {
		return this.optionStrikePrice;
	}


	public void setOptionStrikePrice(BigDecimal optionStrikePrice) {
		this.optionStrikePrice = optionStrikePrice;
	}


	public InvestmentSecurity getUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public void setUnderlyingSecurity(InvestmentSecurity underlyingSecurity) {
		this.underlyingSecurity = underlyingSecurity;
	}


	public Date getEarlyTerminationDate() {
		return this.earlyTerminationDate;
	}


	public void setEarlyTerminationDate(Date earlyTerminationDate) {
		this.earlyTerminationDate = earlyTerminationDate;
	}


	public boolean isUnderlyingSecurityFromSecurityUsed() {
		return this.underlyingSecurityFromSecurityUsed;
	}


	public void setUnderlyingSecurityFromSecurityUsed(boolean underlyingSecurityFromSecurityUsed) {
		this.underlyingSecurityFromSecurityUsed = underlyingSecurityFromSecurityUsed;
	}


	/**
	 * Should ONLY be used to get the override value.  Use getPriceMultiplier method on the security to get the coalesce(override value, instrument price multiplier)
	 */
	public BigDecimal getPriceMultiplierOverride() {
		return this.priceMultiplierOverride;
	}


	public void setPriceMultiplierOverride(BigDecimal priceMultiplierOverride) {
		this.priceMultiplierOverride = priceMultiplierOverride;
	}


	public AccrualMethods getAccrualMethod() {
		return this.accrualMethod;
	}


	public void setAccrualMethod(AccrualMethods accrualMethod) {
		this.accrualMethod = accrualMethod;
	}


	public AccrualMethods getAccrualMethod2() {
		return this.accrualMethod2;
	}


	public void setAccrualMethod2(AccrualMethods accrualMethod2) {
		this.accrualMethod2 = accrualMethod2;
	}
}
