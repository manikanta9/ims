package com.clifton.investment.instrument.allocation.dynamic.calculator;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public interface InvestmentSecurityAllocationDynamicCalculator {

	public static final String DYNAMIC_NOTE_ERROR_PREFIX = "ERROR: ";


	/**
	 * Returns a list of calculated {@link InvestmentSecurityAllocation} objects
	 * based on existing allocations/calculation method for the security
	 *
	 * @param security
	 * @param allocationList - Entered allocations
	 * @param measureDate    - Market Data Value Measure Date
	 */
	public List<InvestmentSecurityAllocation> calculate(InvestmentSecurity security, List<InvestmentSecurityAllocation> allocationList, Date measureDate);
}
