package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;


/**
 * The <code>InvestmentAccrualOISCompoundCalculator</code> calculates the reference rate and spread for
 * an accrual that needs day by day granularity.  It is based on the Overnight Index Swap accrual calculation.
 *
 * @author rbrooks
 */
public class InvestmentAccrualOISCompoundCalculator extends InvestmentAccrualDailyRateCalculator {

	@Override
	protected BigDecimal calculateReferenceRateAmount(@SuppressWarnings("unused") InvestmentSecurityEvent paymentEvent, BigDecimal notional, AccrualDailyRate[] dailyRates, AccrualConfig accrualConfig) {
		// Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(1 + Ri/360) - 1)
		//  Where Ri is Reference Rate on day i and d is the total number of days in current period
		BigDecimal result = BigDecimal.ONE;
		for (AccrualDailyRate accrualDailyRate : dailyRates) {
			BigDecimal dailyRate = accrualDailyRate.getRate();
			//normalizeDailyRate
			dailyRate = dailyRate.movePointLeft(2);

			result = MathUtils.multiply(result, MathUtils.add(BigDecimal.ONE, MathUtils.divide(dailyRate, 360)));
		}
		result = MathUtils.subtract(result, BigDecimal.ONE);

		return MathUtils.multiply(notional, result);
	}
}
