package com.clifton.investment.instrument.structure;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityCalculator;
import com.clifton.investment.instrument.structure.calculators.InvestmentStructureCurrentWeightCalculator;
import com.clifton.investment.instrument.structure.search.InvestmentSecurityStructureAllocationSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class InvestmentInstrumentStructureServiceImpl implements InvestmentInstrumentStructureService {

	private AdvancedUpdatableDAO<InvestmentInstrumentStructure, Criteria> investmentInstrumentStructureDAO;
	private AdvancedUpdatableDAO<InvestmentInstrumentStructureWeight, Criteria> investmentInstrumentStructureWeightDAO;
	private AdvancedUpdatableDAO<InvestmentSecurityStructureWeight, Criteria> investmentSecurityStructureWeightDAO;
	private AdvancedUpdatableDAO<InvestmentSecurityStructureAllocation, Criteria> investmentSecurityStructureAllocationDAO;

	private AdvancedReadOnlyDAO<InvestmentInstrumentSecurityStructure, Criteria> investmentInstrumentSecurityStructureDAO;

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentInstrumentService investmentInstrumentService;
	private SystemBeanService systemBeanService;


	///////////////////////////////////////////////////////////////
	/////  Investment Instrument Structure Business Methods  //////
	///////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstrumentStructure getInvestmentInstrumentStructure(int id) {
		// avoid extra DAO lookup
		List<InvestmentInstrumentStructureWeight> instrumentWeightList = getInvestmentInstrumentStructureWeightListByStructure(id);
		InvestmentInstrumentStructure bean = CollectionUtils.isEmpty(instrumentWeightList) ? null : instrumentWeightList.get(0).getInstrumentStructure();
		if (bean == null) {
			// no instrument weights - get the structure
			bean = getInvestmentInstrumentStructureDAO().findByPrimaryKey(id);
		}
		else {
			bean.setInstrumentStructureWeightList(instrumentWeightList);
		}
		return bean;
	}


	@Override
	public List<InvestmentInstrumentStructure> getInvestmentInstrumentStructureListByInstrument(int instrumentId) {
		return getInvestmentInstrumentStructureDAO().findByField("instrument.id", instrumentId);
	}


	/**
	 * If need different search fields - may be good to change to use a search form, but can't think of cases where we'd need
	 * that at this time.
	 *
	 * @param instrumentId
	 * @param activeOnDate
	 */
	private InvestmentInstrumentStructure getInvestmentInstrumentStructureByInstrument(final int instrumentId, final Date activeOnDate) {
		ValidationUtils.assertNotNull(activeOnDate, "Active On Date is required to find the applicable structure for an instrument.", "activeOnDate");
		HibernateSearchConfigurer searchConfig = criteria -> {
			criteria.add(Restrictions.eq("instrument.id", instrumentId));
			criteria.add(ActiveExpressionForDates.forActiveOnDate(true, activeOnDate));
		};
		return CollectionUtils.getOnlyElement(getInvestmentInstrumentStructureDAO().findBySearchCriteria(searchConfig));
	}


	/**
	 * Extra Validation for the structure can be found in {@link InvestmentInstrumentStructureValidator} which handles
	 * verifying overlapping dates
	 */
	@Override
	@Transactional
	public InvestmentInstrumentStructure saveInvestmentInstrumentStructure(InvestmentInstrumentStructure bean) {
		ValidationUtils.assertNotNull(bean.getCurrentWeightCalculatorBean(), "Current Weight Calculator is required");

		List<InvestmentInstrumentStructureWeight> list = bean.getInstrumentStructureWeightList();
		ValidationUtils.assertNotEmpty(list, "At least one instrument weight is required.");

		List<InvestmentInstrumentStructureWeight> originalList = null;
		if (!bean.isNewBean()) {
			originalList = getInvestmentInstrumentStructureWeightListByStructure(bean.getId());
		}

		InvestmentStructureCurrentWeightCalculator weightCalculator = (InvestmentStructureCurrentWeightCalculator) getSystemBeanService().getBeanInstance(bean.getCurrentWeightCalculatorBean());

		for (InvestmentInstrumentStructureWeight insWeight : CollectionUtils.getIterable(list)) {
			insWeight.setInstrumentStructure(bean);
			weightCalculator.validateInvestmentInstrumentStructureWeight(insWeight);
		}

		bean = getInvestmentInstrumentStructureDAO().save(bean);
		getInvestmentInstrumentStructureWeightDAO().saveList(list, originalList);
		bean.setInstrumentStructureWeightList(list);
		return bean;
	}


	/**
	 * Uses the selected structure with id as a model and ends the existing on the day before the startDate
	 * New one copies the structure and instrument weight list using start & end dates
	 * If copying security structures, will also make copies of the security structures
	 * <p>
	 * Most cases will want to copy the entire structure and then go back and make the few updates for the changes that are needed.
	 */
	@Override
	@Transactional
	public void copyInvestmentInstrumentStructure(int id, Date newStartDate, Date newEndDate, boolean copySecurityStructures) {
		// Use DAO Get so Doesn't Populate Instrument Weight List - Don't want it yet
		InvestmentInstrumentStructure originalStructure = getInvestmentInstrumentStructureDAO().findByPrimaryKey(id);
		boolean updateExisting = false;
		if (newStartDate != null && (originalStructure.getEndDate() == null || DateUtils.compare(newStartDate, originalStructure.getEndDate(), false) < 0)) {
			originalStructure.setEndDate(DateUtils.addDays(newStartDate, -1));
			updateExisting = true;
		}

		// Create a New Structure Bean
		InvestmentInstrumentStructure newStructure = BeanUtils.cloneBean(originalStructure, false, false);
		newStructure.setStartDate(newStartDate);
		newStructure.setEndDate(newEndDate);

		// Do First Date Validation here before attempting to copy (Real Date Validation across all is done in the DAO Validator)
		if (DateUtils.isOverlapInDates(originalStructure.getStartDate(), originalStructure.getEndDate(), newStartDate, newEndDate)) {
			throw new ValidationException("Start/End Dates entered are invalid as they overlap with the Structure you are copying from " + originalStructure.getLabel() + ".");
		}

		List<InvestmentInstrumentStructureWeight> insWeightList = getInvestmentInstrumentStructureWeightListByStructure(originalStructure.getId());

		List<InvestmentInstrumentStructureWeight> newInsWeightList = new ArrayList<>();
		List<InvestmentSecurityStructureWeight> newSecWeightList = (copySecurityStructures ? new ArrayList<>() : null);
		for (InvestmentInstrumentStructureWeight iw : CollectionUtils.getIterable(insWeightList)) {
			copyInvestmentInstrumentStructureWeight(newStructure, newInsWeightList, newSecWeightList, iw);
		}

		// Use DAO Methods since we don't need to duplicate the validation that's in the save methods here
		if (updateExisting) {
			// just updating the end date field
			getInvestmentInstrumentStructureDAO().save(originalStructure);
		}

		getInvestmentInstrumentStructureDAO().save(newStructure);
		getInvestmentInstrumentStructureWeightDAO().saveList(newInsWeightList);
		if (copySecurityStructures) {
			getInvestmentSecurityStructureWeightDAO().saveList(newSecWeightList);
		}
	}


	/**
	 * Is there a better way to handle the copy?  Likely used only once a year for each
	 */
	private void copyInvestmentInstrumentStructureWeight(InvestmentInstrumentStructure newStructure, List<InvestmentInstrumentStructureWeight> newInsWeightList,
	                                                     List<InvestmentSecurityStructureWeight> newSecWeightList, InvestmentInstrumentStructureWeight iw) {

		InvestmentInstrumentStructureWeight newIW = BeanUtils.cloneBean(iw, false, false);
		newIW.setInstrumentStructure(newStructure);
		newInsWeightList.add(newIW);

		// also copy security weight list - only if list isn't null - if null then user opted not to copy
		if (newSecWeightList != null) {
			List<InvestmentSecurityStructureWeight> secWeightList = getInvestmentSecurityStructureWeightDAO().findByField("instrumentWeight.id", iw.getId());
			if (!CollectionUtils.isEmpty(secWeightList)) {
				for (InvestmentSecurityStructureWeight sw : secWeightList) {
					InvestmentSecurityStructureWeight newSW = BeanUtils.cloneBean(sw, false, false);
					newSW.setInstrumentWeight(newIW);
					newSecWeightList.add(newSW);
				}
			}
		}
	}


	@Override
	@Transactional
	public void deleteInvestmentInstrumentStructure(int id) {
		// ALLOW DELETING ONLY IF NO SECURITY STRUCTURE WEIGHTS USING IT
		// Will get FK error, so no additional validation added.
		getInvestmentInstrumentStructureWeightDAO().deleteList(getInvestmentInstrumentStructureWeightListByStructure(id));
		getInvestmentInstrumentStructureDAO().delete(id);
	}


	///////////////////////////////////////////////////////////////
	/// Investment Instrument Structure Weight Business Methods ///
	///////////////////////////////////////////////////////////////


	private List<InvestmentInstrumentStructureWeight> getInvestmentInstrumentStructureWeightListByStructure(int instrumentStructureId) {
		return getInvestmentInstrumentStructureWeightDAO().findByField("instrumentStructure.id", instrumentStructureId);
	}


	///////////////////////////////////////////////////////////////
	// Investment Instrument Security Structure Business Methods //
	///////////////////////////////////////////////////////////////


	private InvestmentInstrumentSecurityStructure getInvestmentInstrumentSecurityStructureImpl(int structureId, int securityId) {
		InvestmentInstrumentSecurityStructure bean = CollectionUtils.getOnlyElement(getInvestmentInstrumentSecurityStructureListImpl(structureId, securityId));
		if (bean != null && bean.isPopulated()) {
			bean.setSecurityStructureWeightList(getInvestmentSecurityStructureWeightListByStructure(bean.getStructure().getId(), bean.getSecurity().getId()));
		}
		return bean;
	}


	private List<InvestmentInstrumentSecurityStructure> getInvestmentInstrumentSecurityStructureListImpl(final Integer structureId, final Integer securityId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			if (structureId != null) {
				criteria.add(Restrictions.eq("structure.id", structureId));
			}
			if (securityId != null) {
				criteria.add(Restrictions.eq("security.id", securityId));
			}
		};
		return getInvestmentInstrumentSecurityStructureDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<InvestmentInstrumentSecurityStructure> getInvestmentInstrumentSecurityStructureListByStructure(int structureId) {
		return getInvestmentInstrumentSecurityStructureListImpl(structureId, null);
	}


	@Override
	public List<InvestmentInstrumentSecurityStructure> getInvestmentInstrumentSecurityStructureListBySecurity(int securityId) {
		return getInvestmentInstrumentSecurityStructureListImpl(null, securityId);
	}


	@Override
	public InvestmentInstrumentSecurityStructure getInvestmentInstrumentSecurityStructure(int structureId, int securityId) {
		InvestmentInstrumentSecurityStructure bean = getInvestmentInstrumentSecurityStructureImpl(structureId, securityId);
		// Not sure if this is ever possible - should always return valid links even if not populated yet
		if (bean == null) {
			InvestmentInstrumentStructure structure = getInvestmentInstrumentStructure(structureId);
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
			bean = new InvestmentInstrumentSecurityStructure();
			bean.setSecurity(security);
			bean.setStructure(structure);
		}

		List<InvestmentSecurityStructureWeight> securityWeightList = bean.getSecurityStructureWeightList();
		List<InvestmentInstrumentStructureWeight> instrumentWeightList = getInvestmentInstrumentStructureWeightListByStructure(structureId);
		if (CollectionUtils.getSize(instrumentWeightList) != CollectionUtils.getSize(securityWeightList)) {
			// Find Difference and Add Missing
			List<InvestmentSecurityStructureWeight> newSecurityWeightList = new ArrayList<>();
			for (InvestmentInstrumentStructureWeight insWeight : CollectionUtils.getIterable(instrumentWeightList)) {
				boolean found = false;
				for (InvestmentSecurityStructureWeight secWeight : CollectionUtils.getIterable(securityWeightList)) {
					if (insWeight.equals(secWeight.getInstrumentWeight())) {
						found = true;
						newSecurityWeightList.add(secWeight);
					}
				}
				if (!found) {
					InvestmentSecurityStructureWeight newSw = new InvestmentSecurityStructureWeight();
					newSw.setInstrumentWeight(insWeight);
					newSecurityWeightList.add(newSw);
				}
			}
			bean.setSecurityStructureWeightList(newSecurityWeightList);
		}
		else {
			bean.setSecurityStructureWeightList(securityWeightList);
		}
		return bean;
	}


	@Override
	public void saveInvestmentInstrumentSecurityStructure(InvestmentInstrumentSecurityStructure bean) {
		List<InvestmentSecurityStructureWeight> existingList = getInvestmentSecurityStructureWeightListByStructure(bean.getStructure().getId(), bean.getSecurity().getId());
		List<InvestmentSecurityStructureWeight> newList = bean.getSecurityStructureWeightList();
		for (InvestmentSecurityStructureWeight sw : CollectionUtils.getIterable(newList)) {
			// No Need to have current security calculator if weight override = 0 which means excluded
			if (MathUtils.isEqual(sw.getWeightOverride(), BigDecimal.ZERO)) {
				sw.setCurrentSecurityCalculatorBean(null);
			}
			else {
				ValidationUtils.assertNotNull(sw.getCurrentSecurityCalculatorBean(), "Current Security Calculators must be selected for all weights unless the weight override is 0 (excluded).");
			}
			sw.setSecurity(bean.getSecurity());
		}
		getInvestmentSecurityStructureWeightDAO().saveList(newList, existingList);
	}


	@Override
	public void deleteInvestmentInstrumentSecurityStructure(int structureId, int securityId) {
		InvestmentInstrumentSecurityStructure bean = getInvestmentInstrumentSecurityStructureImpl(structureId, securityId);
		if (bean.isPopulated()) {
			getInvestmentSecurityStructureWeightDAO().deleteList(bean.getSecurityStructureWeightList());
		}
		// Otherwise nothing to delete
	}


	///////////////////////////////////////////////////////////////
	//// Investment Security Structure Weight Business Methods ////
	///////////////////////////////////////////////////////////////


	private List<InvestmentSecurityStructureWeight> getInvestmentSecurityStructureWeightListByStructure(final int instrumentStructureId, final int securityId) {
		HibernateSearchConfigurer searchConfig = criteria -> {
			criteria.createAlias("instrumentWeight", "iw");
			criteria.add(Restrictions.eq("iw.instrumentStructure.id", instrumentStructureId));
			criteria.add(Restrictions.eq("security.id", securityId));
		};
		return getInvestmentSecurityStructureWeightDAO().findBySearchCriteria(searchConfig);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////    Investment Security Structure Allocation Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void rebuildInvestmentSecurityStructureAllocation(Integer structureId, int securityId, Date startDate, Date endDate, boolean reprocessExisting) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		ValidationUtils.assertNotNull(security.getStartDate(), "Security Start Date is required in order to rebuild structured index allocations.");

		Short calendarId = null;
		if (security.getInstrument().getExchange() != null && security.getInstrument().getExchange().getCalendar() != null) {
			calendarId = security.getInstrument().getExchange().getCalendar().getId();
		}
		boolean startBusinessDay = (calendarId != null) ? getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(security.getStartDate(), calendarId)) : getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(security.getStartDate()));
		ValidationUtils.assertTrue(startBusinessDay, "Security start date is not a business day.  Please enter a start date for a valid business day");

		if (startDate == null || DateUtils.isDateBefore(startDate, security.getStartDate(), false)) {
			startDate = security.getStartDate();
		}
		if (endDate == null) {
			endDate = ObjectUtils.coalesce(security.getEndDate(), DateUtils.getPreviousWeekday(new Date()));
		}

		// Delete all existing data prior to start date
		deleteInvestmentSecurityStructureAllocationList(null, securityId, null, DateUtils.addDays(security.getStartDate(), -1));
		// Delete all existing data after end date
		if (security.getEndDate() != null) {
			deleteInvestmentSecurityStructureAllocationList(null, securityId, DateUtils.addDays(security.getEndDate(), 1), null);
		}

		Date date = startDate;

		try {
			while (DateUtils.isDateBeforeOrEqual(date, endDate, false)) {
				boolean rebuild = (calendarId != null) ? getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarId)) : getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(date));
				if (rebuild) {
					getInvestmentSecurityStructureAllocationListForSecurityAndDateImpl(structureId, securityId, date, false, reprocessExisting);
				}
				// If not a business day, make sure if there is saved data it's removed (i.e. calendar switch)
				else {
					deleteInvestmentSecurityStructureAllocationList(structureId, securityId, date, date);
				}
				date = DateUtils.addWeekDays(date, 1);
			}
		}
		catch (Exception e) {
			throw new ValidationException("Unable to calculate structured index allocations for security " + security.getLabel() + " on " + DateUtils.fromDateShort(date) + ": " + ExceptionUtils.getOriginalMessage(e), e);
		}
	}


	@Override
	public List<InvestmentSecurityStructureAllocation> getInvestmentSecurityStructureAllocationListForSecurityAndDate(Integer structureId, int securityId, Date date,
	                                                                                                                  boolean calculateCurrentForNextBusinessDay) {
		return getInvestmentSecurityStructureAllocationListForSecurityAndDateImpl(structureId, securityId, date, calculateCurrentForNextBusinessDay, false);
	}


	@Transactional
	protected List<InvestmentSecurityStructureAllocation> getInvestmentSecurityStructureAllocationListForSecurityAndDateImpl(Integer structureId, int securityId, Date date,
	                                                                                                                         boolean calculateCurrentForNextBusinessDay, boolean reprocessExisting) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		ValidationUtils.assertNotNull(security, "Structured Index Security is required to build weighted allocation list", "security");
		ValidationUtils.assertNotNull(date, "Date is required to build weighted allocation list for a structured index.");

		Date currentDate = date;
		if (calculateCurrentForNextBusinessDay) {
			currentDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(date), 1);
		}

		// If not explicit - Pull the Active Structure to Use
		InvestmentInstrumentStructure structure = null;
		if (structureId != null) {
			structure = getInvestmentInstrumentStructure(structureId);
		}
		if (structure == null) {
			structure = getInvestmentInstrumentStructureByInstrument(security.getInstrument().getId(), date);
			ValidationUtils.assertNotNull(structure, "There is not an active structure for Instrument [" + security.getInstrument().getName() + "] on [" + DateUtils.fromDateShort(date));
		}

		// If this is a type that saves it's values and we aren't re-processing existing, see if we have the values
		List<InvestmentSecurityStructureAllocation> existingList = null;
		if (structure.getInstrumentStructureType().isSaveDailyAllocationValues()) {
			// Pull existing list by Security only, not structure since there can only be one active structure per date if the structures change we want to use the newly active only
			existingList = getInvestmentSecurityStructureAllocationSavedListForSecurityStructureAndDate(null, securityId, date, date);
			if (!CollectionUtils.isEmpty(existingList) && !reprocessExisting) {
				return existingList;
			}
			ValidationUtils.assertFalse(calculateCurrentForNextBusinessDay, "Cannot use next day's current security calculation for structures that save daily values.");
		}

		List<InvestmentSecurityStructureWeight> securityWeightList = getInvestmentSecurityStructureWeightListByStructure(structure.getId(), security.getId());
		ValidationUtils.assertNotEmpty(securityWeightList, "Security Structure is not set up for [" + security.getLabel() + "] and instrument structure [" + structure.getLabel() + "].");

		List<InvestmentSecurityStructureAllocation> securityAllocationList = new ArrayList<>();
		Map<Integer, InvestmentCurrentSecurityCalculator> beanMap = new HashMap<>();
		for (InvestmentSecurityStructureWeight secWeight : securityWeightList) {
			// If weight isn't zero, then include it
			if (!MathUtils.isEqual(BigDecimal.ZERO, secWeight.getCoalesceWeight())) {
				InvestmentSecurityStructureAllocation alloc = new InvestmentSecurityStructureAllocation(secWeight, date);
				InvestmentCurrentSecurityCalculator currentCalc;
				if (beanMap.containsKey(secWeight.getCurrentSecurityCalculatorBean().getId())) {
					currentCalc = beanMap.get(secWeight.getCurrentSecurityCalculatorBean().getId());
				}
				else {
					currentCalc = (InvestmentCurrentSecurityCalculator) getSystemBeanService().getBeanInstance(secWeight.getCurrentSecurityCalculatorBean());
					beanMap.put(secWeight.getCurrentSecurityCalculatorBean().getId(), currentCalc);
				}
				currentCalc.calculateCurrentSecurity(alloc, currentDate);
				if (alloc.getCurrentSecurity() == null) {
					throw new ValidationException("Unable to calculate current security for instrument [" + alloc.getSecurityStructureWeight().getInstrumentWeight().getInstrument().getLabel() + "] on ["
							+ DateUtils.fromDateShort(date) + "] using calculator [" + secWeight.getCurrentSecurityCalculatorBean().getName());
				}
				securityAllocationList.add(alloc);
			}
		}
		InvestmentStructureCurrentWeightCalculator weightCalc = (InvestmentStructureCurrentWeightCalculator) getSystemBeanService().getBeanInstance(structure.getCurrentWeightCalculatorBean());
		weightCalc.calculateCurrentWeights(structure, securityAllocationList, date);

		// If we save daily allocation values - and we are re-processing existing merge old and new lists and save changes
		if (structure.getInstrumentStructureType().isSaveDailyAllocationValues()) {
			if (!CollectionUtils.isEmpty(existingList)) {
				for (InvestmentSecurityStructureAllocation allocation : securityAllocationList) {
					for (InvestmentSecurityStructureAllocation existingAllocation : existingList) {
						if (existingAllocation.getSecurityStructureWeight().equals(allocation.getSecurityStructureWeight())) {
							allocation.setId(existingAllocation.getId());
							allocation.setRv(existingAllocation.getRv());
							break;
						}
					}
				}
			}
			getInvestmentSecurityStructureAllocationDAO().saveList(securityAllocationList, existingList);
		}

		return BeanUtils.sortWithFunction(securityAllocationList, securityStructureAllocation -> securityStructureAllocation.getSecurityStructureWeight().getInstrumentWeight().getOrder(), true);
	}


	@Override
	public List<InvestmentSecurityStructureAllocation> getInvestmentSecurityStructureAllocationList(InvestmentSecurityStructureAllocationSearchForm searchForm) {
		return getInvestmentSecurityStructureAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private List<InvestmentSecurityStructureAllocation> getInvestmentSecurityStructureAllocationSavedListForSecurityStructureAndDate(Integer structureId, int securityId, Date startDate, Date endDate) {
		InvestmentSecurityStructureAllocationSearchForm searchForm = new InvestmentSecurityStructureAllocationSearchForm();
		searchForm.setSecurityId(securityId);
		searchForm.setStructureId(structureId);
		if (startDate != null && DateUtils.isEqualWithoutTime(startDate, endDate)) {
			searchForm.setMeasureDate(startDate);
		}
		else if (startDate != null) {
			searchForm.addSearchRestriction(new SearchRestriction("measureDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		}
		else if (endDate != null) {
			searchForm.addSearchRestriction(new SearchRestriction("measureDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		}

		return getInvestmentSecurityStructureAllocationList(searchForm);
	}


	private void deleteInvestmentSecurityStructureAllocationList(Integer structureId, int securityId, Date startDate, Date endDate) {
		getInvestmentSecurityStructureAllocationDAO().deleteList(getInvestmentSecurityStructureAllocationSavedListForSecurityStructureAndDate(structureId, securityId, startDate, endDate));
	}


	///////////////////////////////////////////////////////////////
	///////            Getters and Setters Methods          ///////
	///////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstrumentStructure, Criteria> getInvestmentInstrumentStructureDAO() {
		return this.investmentInstrumentStructureDAO;
	}


	public void setInvestmentInstrumentStructureDAO(AdvancedUpdatableDAO<InvestmentInstrumentStructure, Criteria> investmentInstrumentStructureDAO) {
		this.investmentInstrumentStructureDAO = investmentInstrumentStructureDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInstrumentStructureWeight, Criteria> getInvestmentInstrumentStructureWeightDAO() {
		return this.investmentInstrumentStructureWeightDAO;
	}


	public void setInvestmentInstrumentStructureWeightDAO(AdvancedUpdatableDAO<InvestmentInstrumentStructureWeight, Criteria> investmentInstrumentStructureWeightDAO) {
		this.investmentInstrumentStructureWeightDAO = investmentInstrumentStructureWeightDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSecurityStructureWeight, Criteria> getInvestmentSecurityStructureWeightDAO() {
		return this.investmentSecurityStructureWeightDAO;
	}


	public void setInvestmentSecurityStructureWeightDAO(AdvancedUpdatableDAO<InvestmentSecurityStructureWeight, Criteria> investmentSecurityStructureWeightDAO) {
		this.investmentSecurityStructureWeightDAO = investmentSecurityStructureWeightDAO;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public AdvancedReadOnlyDAO<InvestmentInstrumentSecurityStructure, Criteria> getInvestmentInstrumentSecurityStructureDAO() {
		return this.investmentInstrumentSecurityStructureDAO;
	}


	public void setInvestmentInstrumentSecurityStructureDAO(AdvancedReadOnlyDAO<InvestmentInstrumentSecurityStructure, Criteria> investmentInstrumentSecurityStructureDAO) {
		this.investmentInstrumentSecurityStructureDAO = investmentInstrumentSecurityStructureDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSecurityStructureAllocation, Criteria> getInvestmentSecurityStructureAllocationDAO() {
		return this.investmentSecurityStructureAllocationDAO;
	}


	public void setInvestmentSecurityStructureAllocationDAO(AdvancedUpdatableDAO<InvestmentSecurityStructureAllocation, Criteria> investmentSecurityStructureAllocationDAO) {
		this.investmentSecurityStructureAllocationDAO = investmentSecurityStructureAllocationDAO;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
