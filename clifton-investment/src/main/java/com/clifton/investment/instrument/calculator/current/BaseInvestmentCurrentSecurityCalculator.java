package com.clifton.investment.instrument.calculator.current;


import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureWeight;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;

import java.util.Date;


/**
 * The <code>BaseInvestmentCurrentSecurityCalculator</code> ...
 *
 * @author manderson
 */
public abstract class BaseInvestmentCurrentSecurityCalculator implements InvestmentCurrentSecurityCalculator {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private InvestmentReplicationService investmentReplicationService;

	////////////////////////////////////////////////////////////////////////////
	////////////              Validation Methods                     ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final void validateInvestmentSecurityStructureWeight(InvestmentSecurityStructureWeight securityStructureWeight) {
		ValidationUtils.assertNotNull(securityStructureWeight, "Investment Security Structure Weight is required.");
		validateImpl(securityStructureWeight.getInstrumentWeight().getInstrument(), null);
	}


	@Override
	public final void validateInvestmentReplicationAllocation(InvestmentReplicationAllocation replicationAllocation) {
		getInvestmentReplicationService().validateInvestmentReplicationAllocationSecuritySpecificity(replicationAllocation);
		validateImpl(replicationAllocation.getReplicationInstrument(), replicationAllocation.getReplicationSecurityGroup());
	}


	@Override
	public final void validateInvestmentSecurityAllocation(InvestmentSecurityAllocation securityAllocation) {
		ValidationUtils.assertNotNull(securityAllocation, "Security Allocation is required.");
		validateImpl(securityAllocation.getInvestmentInstrument(), null);
	}


	@SuppressWarnings("unused")
	protected void validateImpl(InvestmentInstrument instrument, InvestmentSecurityGroup securityGroup) {
		// No Additional Default Validation
	}

	////////////////////////////////////////////////////////////////////////////
	////////////              Calculation Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final void calculateCurrentSecurity(InvestmentSecurityStructureAllocation securityStructureAllocation, Date date) {
		validateInvestmentSecurityStructureWeight(securityStructureAllocation.getSecurityStructureWeight());
		securityStructureAllocation.setCurrentSecurity(calculateCurrentSecurityImpl(securityStructureAllocation.getSecurityStructureWeight().getInstrumentWeight().getInstrument(), null, null, date, securityStructureAllocation.getInstrumentAssignment().getLabel()));
	}


	@Override
	public InvestmentSecurity calculateCurrentSecurity(InvestmentReplicationAllocation replicationAllocation, Date date) {
		validateInvestmentReplicationAllocation(replicationAllocation);
		return calculateCurrentSecurityImpl(replicationAllocation.getReplicationInstrument(), replicationAllocation.getReplicationSecurityGroup(), replicationAllocation.getReplicationSecurity(), date, replicationAllocation.getAllocationLabel());
	}


	@Override
	public InvestmentSecurity calculateCurrentSecurity(InvestmentSecurityAllocation securityAllocation, Date date) {
		return calculateCurrentSecurityImpl(securityAllocation.getInvestmentInstrument(), null, null, date, securityAllocation.getAllocationLabel());
	}


	private InvestmentSecurity calculateCurrentSecurityImpl(InvestmentInstrument instrument, InvestmentSecurityGroup securityGroup, InvestmentSecurity underlyingSecurity, Date date, String messagePrefix) {
		try {
			return calculateImpl(instrument, securityGroup, underlyingSecurity, date);
		}
		catch (Throwable e) {
			LogUtils.errorOrInfo(LogCommand.ofThrowable(getClass(), e));
			throw new ValidationException("Cannot calculate current security for allocation " + messagePrefix + " on " + DateUtils.fromDateShort(date) + ": " + ExceptionUtils.getOriginalMessage(e));
		}
	}


	protected abstract InvestmentSecurity calculateImpl(InvestmentInstrument instrument, InvestmentSecurityGroup securityGroup, InvestmentSecurity underlyingSecurity, Date date);

	////////////////////////////////////////////////////////////////////////////
	//////////            Getter and Setter Methods                    /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}
}
