package com.clifton.investment.instrument.event.sequence.date;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.investment.instrument.event.sequence.InvestmentEventSequenceRetrieverUtilHandler;


public abstract class AbstractDateCalculator implements InvestmentInstrumentEventSequenceDateCalculator {

	private ScheduleApiService scheduleApiService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentEventSequenceRetrieverUtilHandler investmentEventSequenceRetrieverUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentEventSequenceRetrieverUtilHandler getInvestmentEventSequenceRetrieverUtilHandler() {
		return this.investmentEventSequenceRetrieverUtilHandler;
	}


	public void setInvestmentEventSequenceRetrieverUtilHandler(InvestmentEventSequenceRetrieverUtilHandler investmentEventSequenceRetrieverUtilHandler) {
		this.investmentEventSequenceRetrieverUtilHandler = investmentEventSequenceRetrieverUtilHandler;
	}
}
