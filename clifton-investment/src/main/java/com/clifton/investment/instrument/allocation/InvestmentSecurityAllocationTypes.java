package com.clifton.investment.instrument.allocation;


/**
 * The <code>InvestmentSecurityAllocationTypes</code> defines the types of security allocations supported.
 * <p>
 * These types generally correspond with the price market data calculator used.
 *
 * @author manderson
 */
public enum InvestmentSecurityAllocationTypes {
	/**
	 * Price on start date is 100.
	 * Following the start date the price is calculated as the weighted return from the date's price to the start date price.
	 * Example: 50% of SPTR.
	 * Day 1: Price is 100,
	 * Day 2: Price is 100.25 (50% of SPTR increase of 0.5% since start date),
	 * Day 3: Price is 101.25 (50% of SPTR increase of 2.5% since start date).
	 * For currencies, the return is determined by the percent change of the Goldman Sachs exchange rate to USD.
	 */
	WEIGHTED_RETURN("Weighted Return", "Return Weight", InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, true), //


	/**
	 * Price on start date is 100.
	 * Following the start date the price is calculated as the weighted return from the date's price to the previous price.
	 * Example: 50% of SPTR.
	 * Day 1: Price is 100,
	 * Day 2: Price is 100.25 (50% of SPTR increase of 0.5% since previous day),
	 * Day 3: Price is 101.2527  (50% of SPTR increase of 2.0% since previous day).
	 * For currencies, the return is determined by the percent change of the Goldman Sachs exchange rate to USD.
	 */
	WEIGHTED_RETURN_ADJUST_DAILY("Weighted Return (Rebalanced Daily)", "Return Weight", InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, true), //

	/**
	 * Price on start date is 100
	 * Following the start date the price is calculated as the weighted return from the date's price to the previous month end price
	 */
	WEIGHTED_RETURN_ADJUST_MONTHLY("Weighted Return (Rebalanced Monthly)", "Return Weight", InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, true), //

	/**
	 * The price is calculated as the sum of the product of (# of Shares, Price, Goldman Sachs FX Rate to base currency)  divided by the price multiplier.
	 * Example: USD Basket (Price Multiplier of 92.8) of AAPL UW Equity (25 Shares USD) and AH NA Equity (50 Shares EUR).
	 * Day 1: Price is ((25 * 100 (price of AAPL)) + (50 * 100 (price of AH NA) * 1.356 (FX Rate from EUR to USD))) / 92.8 = 100,
	 * Day 2: Price is ((25 * 101 (price of AAPL)) + (50 * 100 (price of AH NA) * 1.356 (FX Rate from EUR to USD))) / 92.8 = 100.27,
	 * Day 3: Price is 100.46 (AAPL price decreases to 99, AH NA price increases to 101, FX rate remains the same.)
	 * <p>
	 * Base weight is calculated from the # of shares and prices on the security start date.  Or can be entered and # of shares can be calculated from it
	 */
	SHARE_PRICE("Share Price", "Base Weight", "Shares", InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, true, false), //

	/**
	 * The monthly return on the last day of the month is the weighted average of it's allocation's monthly returns
	 * from that month.  If the allocation has monthly return field populated, it uses that otherwise will calculate
	 * the monthly return based on price changes.
	 * Example. Security X = 50% of Security Y and 50% of Security Z
	 * Security Y had a monthly return of 1% and Security Z had a monthly return of 5%
	 * Security X Monthly Return = .5*1 + .5*5 = 0.5 + 2.5 = 3% return
	 */
	MONTHLY_RETURN_WEIGHTED_AVERAGE("Monthly Return (Weighted Average)", "Weight", InvestmentSecurityAllocationCalculatedFieldTypes.MONTHLY_RETURN, false), //


	/**
	 * The system doesn't calculate any market data for these.  The security itself contains an allocation of other securities
	 * and their weights.  Used for cases where we don't have market data for the underlying allocations, but just store
	 * the list of members in the basket.
	 */
	WEIGHTED_BASKET_NO_PRICE("Weighted Basket (Not Priced)", "Weight", InvestmentSecurityAllocationCalculatedFieldTypes.NONE, false);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private final String label;
	private final String weightLabel;
	private final String sharesLabel;

	private final InvestmentSecurityAllocationCalculatedFieldTypes calculatedFieldType;

	private final boolean autoRebalancingSupported;
	/**
	 * For some calculation types (Rebalanced Daily or From Start) Start Date has meaning and we need it to be a business day in order to properly calculate
	 */
	private final boolean startDateBusinessDayRequired;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	InvestmentSecurityAllocationTypes(String label, String weightLabel, InvestmentSecurityAllocationCalculatedFieldTypes calculatedFieldType, boolean startDateBusinessDayRequired) {
		this(label, weightLabel, null, calculatedFieldType, false, startDateBusinessDayRequired);
	}


	InvestmentSecurityAllocationTypes(String label, String weightLabel, String sharesLabel, InvestmentSecurityAllocationCalculatedFieldTypes calculatedFieldType,
	                                  boolean autoRebalancingSupported, boolean startDateBusinessDayRequired) {
		this.label = label;
		this.weightLabel = weightLabel;
		this.sharesLabel = sharesLabel;
		this.calculatedFieldType = calculatedFieldType;
		this.autoRebalancingSupported = autoRebalancingSupported;
		this.startDateBusinessDayRequired = startDateBusinessDayRequired;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}


	public String getWeightLabel() {
		return this.weightLabel;
	}


	public String getSharesLabel() {
		return this.sharesLabel;
	}


	public boolean isAutoRebalancingSupported() {
		return this.autoRebalancingSupported;
	}


	public InvestmentSecurityAllocationCalculatedFieldTypes getCalculatedFieldType() {
		return this.calculatedFieldType;
	}


	public boolean isStartDateBusinessDayRequired() {
		return this.startDateBusinessDayRequired;
	}
}
