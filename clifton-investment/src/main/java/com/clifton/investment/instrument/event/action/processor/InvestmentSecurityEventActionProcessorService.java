package com.clifton.investment.instrument.event.action.processor;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventAction;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author vgomelsky
 */
public interface InvestmentSecurityEventActionProcessorService {

	@ModelAttribute("result")
	@SecureMethod(dtoClass = InvestmentSecurityEventAction.class)
	public Status processInvestmentSecurityEventActions(int securityEventId, Boolean processBeforeEventJournal);


	@ModelAttribute("result")
	@RequestMapping("investmentSecurityEventActionsRollback")
	@SecureMethod(dtoClass = InvestmentSecurityEventAction.class)
	public Status rollbackInvestmentSecurityEventActions(int securityEventId, Boolean processBeforeEventJournal);


	/**
	 * Returns true if the specified security event has at least one action that has not been processed.
	 * Note, if an action was not processed but there is no impact (data updates) of that action, the method will return false.
	 */
	@SecureMethod(dtoClass = InvestmentSecurityEventAction.class)
	public boolean isInvestmentSecurityEventActionPendingProcessing(int securityEventId, Boolean beforeEventJournal);
}
