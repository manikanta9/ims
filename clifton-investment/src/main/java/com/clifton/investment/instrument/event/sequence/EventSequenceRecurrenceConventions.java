package com.clifton.investment.instrument.event.sequence;

/**
 * @author mwacker
 */
public enum EventSequenceRecurrenceConventions {
	DEFAULT,
	/**
	 * Indicates that the sequence recurrence is always on month end.
	 */
	MONTH_END_RECURRENCE
}
