package com.clifton.investment.instrument.event.action.processor;

import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventAction;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionType;


/**
 * The InvestmentSecurityEventActionProcessor interface must be implemented by specific security event actions: for example post-split price adjustments.
 *
 * @author vgomelsky
 */
public interface InvestmentSecurityEventActionProcessor {


	/**
	 * Process the action for the specified event.
	 *
	 * @return returns true, if the action was successfully processed and false, if there was nothing to process.
	 */
	public boolean processAction(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventActionType actionType);


	/**
	 * Reverse the specified action.
	 */
	public void rollbackAction(InvestmentSecurityEventAction eventAction);


	/**
	 * Returns true if there is data that would be affected by this processor.
	 * Usually checked after verifying that the action has not been processed yet.
	 */
	public boolean isDataAvailableForProcessing(InvestmentSecurityEvent securityEvent);
}
