package com.clifton.investment.instrument;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.SecurityIdentifierUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationHandler;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.InvestmentSecuritySearchFormConfigurer;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.instrument.securitylocator.InvestmentSecurityLocatorService;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy.InstrumentDeliverableTypes;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.audit.auditor.SystemAuditDaoUtils;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class InvestmentInstrumentServiceImpl implements InvestmentInstrumentService {

	private AdvancedUpdatableDAO<InvestmentInstrument, Criteria> investmentInstrumentDAO;
	private AdvancedUpdatableDAO<InvestmentSecurity, Criteria> investmentSecurityDAO;

	private InvestmentGroupService investmentGroupService;

	private InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler;
	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private InvestmentSecurityLocatorService investmentSecurityLocatorService;
	private InvestmentSetupService investmentSetupService;

	private SystemColumnService systemColumnService;
	private SystemColumnValueHandler systemColumnValueHandler;


	private DaoSingleKeyListCache<InvestmentSecurity, String> investmentSecurityListBySymbolCache;
	private DaoSingleKeyListCache<InvestmentSecurity, String> investmentSecurityListBySedolCache;
	private DaoSingleKeyListCache<InvestmentSecurity, String> investmentSecurityListByCusipCache;
	private DaoSingleKeyListCache<InvestmentSecurity, String> investmentSecurityListByIsinCache;

	////////////////////////////////////////////////////////////////////////////
	//////////          Investment Instrument Methods               ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInstrument getInvestmentInstrument(int id) {
		return this.investmentInstrumentDAO.findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInstrument> getInvestmentInstrumentList(final InstrumentSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getMissingInvestmentSecurityGroupId() != null) {
					DetachedCriteria ne = DetachedCriteria.forClass(InvestmentSecurityGroupSecurity.class, "gs");
					ne.setProjection(Projections.property("id"));
					ne.createAlias("referenceTwo", "gss");
					ne.createAlias("gss.instrument", "gsi");
					ne.add(Restrictions.eqProperty("gsi.id", criteria.getAlias() + ".id"));
					ne.add(Restrictions.eq("referenceOne.id", searchForm.getMissingInvestmentSecurityGroupId()));
					criteria.add(Subqueries.notExists(ne));
				}
			}
		};
		return getInvestmentInstrumentDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<InvestmentInstrument> getInvestmentInstrumentList(SearchConfigurer<Criteria> searchConfigurer) {
		return getInvestmentInstrumentDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public InvestmentInstrument saveInvestmentInstrument(InvestmentInstrument bean) {
		return saveInvestmentInstrument(bean, false);
	}


	/**
	 * Used in rare cases where a 1 to 1 Instrument must be saved outside of the saveInvestmentSecurity() method.
	 */
	@Override
	public InvestmentInstrument saveInvestmentInstrumentAllowOneToOne(InvestmentInstrument bean) {
		return saveInvestmentInstrument(bean, true);
	}


	@Override
	//This method bypasses validationException for oneSecurityPerInstrument during a json migration
	//The json migration walks the object, and so will attempt to save the instrument prior to the
	//security, the validation prevents it, so instead we call this method which will skip one to
	//one securities and they will be saved when the json crawls back up to the security and saves.
	public void saveInvestmentInstrumentIgnoreOneToOne(InvestmentInstrument bean) {
		InvestmentInstrumentHierarchy hierarchy = getInvestmentSetupService().getInvestmentInstrumentHierarchy(bean.getHierarchy().getId());
		if (!hierarchy.isOneSecurityPerInstrument()) {
			saveInvestmentInstrument(bean, false);
		}
	}


	private InvestmentInstrument saveInvestmentInstrument(InvestmentInstrument bean, boolean allowOneToOne) {
		if (!allowOneToOne) {
			// validate against hierarchy rules first
			InvestmentInstrumentHierarchy hierarchy = getInvestmentSetupService().getInvestmentInstrumentHierarchy(bean.getHierarchy().getId());
			if (hierarchy.isOneSecurityPerInstrument()) {
				throw new ValidationException("Cannot save instrument directly when there is one security per instrument. Save security instead.");
			}
			validateInvestmentInstrument(bean, hierarchy);
		}

		return getInvestmentInstrumentDAO().save(bean);
	}


	private void validateInvestmentInstrument(InvestmentInstrument instrument, InvestmentInstrumentHierarchy hierarchy) {
		if (!hierarchy.isAssignmentAllowed()) {
			throw new FieldValidationException("The hierarchy '" + hierarchy.getName() + "' does not allow instrument assignments.", "hierarchy.id");
		}
		ValidationUtils.assertNotNull(instrument.getTradingCurrency(), "Currency denomination field is required.", "tradingCurrency.id");

		if (hierarchy.getAccrualSecurityEventType() == null) {
			ValidationUtils.assertNull(instrument.getAccrualDateCalculator(), "Accrual Calculator is not allowed for instruments in hierarchy that do not support accruals.", "accrualDateCalculator.id");
		}

		if (instrument.getExchange() != null) {
			ValidationUtils.assertFalse(instrument.getExchange().isCompositeExchange(), "Composite Exchange cannot be used for Primary Exchange field: " + instrument.getExchange());
		}
		if (instrument.getCompositeExchange() != null) {
			ValidationUtils.assertTrue(instrument.getCompositeExchange().isCompositeExchange(), "Composite Exchange must be used for Composite Exchange field: " + instrument.getCompositeExchange());
		}

		if (instrument.getHierarchy().getPriceMultiplier() != null && !instrument.getHierarchy().isSecurityPriceMultiplierOverrideAllowed()) {
			ValidationUtils.assertTrue(MathUtils.isEqual(instrument.getHierarchy().getPriceMultiplier(), instrument.getPriceMultiplier()),
					"Instrument cannot have a Price Multiplier that is different from this defined on Hierarchy", "priceMultiplier");
		}

		if (instrument.getUnderlyingInstrument() != null && instrument.getHierarchy().getUnderlyingHierarchy() != null) {
			ValidationUtils.assertTrue(instrument.getUnderlyingInstrument().getHierarchy().equals(instrument.getHierarchy().getUnderlyingHierarchy()), "Underlying Instrument selection [" + instrument.getUnderlyingInstrument().getLabel()
					+ "] is invalid because hierarchy [" + instrument.getHierarchy().getLabelExpanded() + "] allows underlying instruments from ["
					+ instrument.getHierarchy().getUnderlyingHierarchy().getLabelExpanded() + "] hierarchy only.");
		}

		if (instrument.getUnderlyingInstrument() != null && instrument.getHierarchy().getUnderlyingInvestmentGroup() != null) {
			ValidationUtils.assertTrue(getInvestmentGroupService().isInvestmentInstrumentInGroup(instrument.getHierarchy().getUnderlyingInvestmentGroup().getName(), instrument.getUnderlyingInstrument().getId()), "Underlying Instrument selection [" + instrument.getUnderlyingInstrument().getLabel()
					+ "] is invalid because hierarchy [" + instrument.getHierarchy().getLabelExpanded() + "] allows underlying instrument from [" + instrument.getHierarchy().getUnderlyingInvestmentGroup().getName() + "] instrument group only.");
		}

		if (InstrumentDeliverableTypes.ALWAYS == hierarchy.getInstrumentDeliverableType()) {
			ValidationUtils.assertTrue(instrument.isDeliverable(), "All instruments in Hierarchy [" + hierarchy.getLabelExpanded() + "] must be deliverable.");
		}
		else if (InstrumentDeliverableTypes.NEVER == hierarchy.getInstrumentDeliverableType()) {
			ValidationUtils.assertFalse(instrument.isDeliverable(), "No instruments in Hierarchy [" + hierarchy.getLabelExpanded() + "] can be deliverable.");
		}

		if (instrument.getSpotMonthCalculatorBean() != null) {
			ValidationUtils.assertFalse(hierarchy.isOneSecurityPerInstrument(), "Spot Month Calculators can only be selected for instruments with one-to-many securities");
		}

		// We should reject setting custom column json values if the reference column name does not exist
		CustomJsonString customColumns = instrument.getCustomColumns();

		if (customColumns != null && !StringUtils.isEmpty(customColumns.getJsonValue())) {

			Set<String> allowedColumnNames = CollectionUtils.getStream(getSystemColumnService().getSystemColumnCustomListForGroup(InvestmentInstrument.INSTRUMENT_CUSTOM_FIELDS_GROUP_NAME))
					.map(SystemColumnCustom::getName)
					.collect(Collectors.toSet());

			for (String colName : CustomJsonStringUtils.getCustomJsonStringAsMap(customColumns).keySet()) {
				ValidationUtils.assertTrue(CollectionUtils.contains(allowedColumnNames, colName), "A custom value cannot be assigned for custom JSON column that does not exist.  Non-existent column: [" + colName + "].");
			}
		}

		InvestmentUtils.validatePriceMultiplierValue(instrument.getPriceMultiplier());
	}


	/**
	 * See {@link InvestmentInstrumentObserver} for deleting additional related entities
	 */
	@Override
	@Transactional
	public void deleteInvestmentInstrument(int id) {
		// also delete the security for one-to-one instruments
		InvestmentInstrument instrument = getInvestmentInstrument(id);
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		if (hierarchy.isOneSecurityPerInstrument()) {
			InvestmentSecurity security = getInvestmentSecurityByInstrument(id);
			if (security != null) {
				getInvestmentSecurityDAO().delete(security);
			}
		}

		getInvestmentInstrumentDAO().delete(instrument);
	}


	@Override
	public void moveInvestmentInstrument(int id, short newInstrumentHierarchyId) {
		moveInvestmentInstrumentImpl(id, newInstrumentHierarchyId, false);
	}


	@Override
	public void moveInvestmentInstrumentBypassValidation(int id, short newInstrumentHierarchyId) {
		moveInvestmentInstrumentImpl(id, newInstrumentHierarchyId, true);
	}


	private void moveInvestmentInstrumentImpl(int instrumentId, short newInstrumentHierarchyId, boolean bypassCustomColumnValidation) {
		InvestmentInstrument instrument = getInvestmentInstrument(instrumentId);
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		InvestmentInstrumentHierarchy newHierarchy = getInvestmentSetupService().getInvestmentInstrumentHierarchy(newInstrumentHierarchyId);

		// Validate Hierarchy Options are the Same so Instrument Can Be Moved
		try {
			getInvestmentSetupService().validateInvestmentInstrumentHierarchyMove(instrumentId, hierarchy, newHierarchy, bypassCustomColumnValidation);
		}
		catch (UserIgnorableValidationException e) {
			throw new UserIgnorableValidationException(getClass(), "moveInvestmentInstrumentBypassValidation", e.getMessage());
		}

		// Move The Instrument and All Custom Fields for that Instrument and it's Securities
		doMoveInvestmentInstrumentImpl(instrument, hierarchy, newHierarchy, bypassCustomColumnValidation);
	}


	@Transactional
	protected void doMoveInvestmentInstrumentImpl(InvestmentInstrument instrument, InvestmentInstrumentHierarchy fromHierarchy, InvestmentInstrumentHierarchy toHierarchy, boolean bypassCustomColumnValidation) {
		instrument.setHierarchy(toHierarchy);

		// Move Instrument Custom Columns (If Any)
		getSystemColumnValueHandler().moveSystemColumnValueListForEntityList(InvestmentInstrument.INSTRUMENT_CUSTOM_FIELDS_GROUP_NAME, CollectionUtils.createList(instrument.getId()), fromHierarchy.getId() + "", toHierarchy.getId() + "", bypassCustomColumnValidation);

		// One:One Hierarchy Move
		if (fromHierarchy.isOneSecurityPerInstrument()) {
			InvestmentSecurity security = getInvestmentSecurityByInstrument(instrument.getId());

			// Move Security Custom Columns
			getSystemColumnValueHandler().moveSystemColumnValueListForEntityList(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, CollectionUtils.createList(security.getId()), fromHierarchy.getId() + "", toHierarchy.getId() + "", bypassCustomColumnValidation);

			// Update Instrument on the Security
			security.setInstrument(instrument);

			// Save Security (Instrument Save is Through Security for 1:1)
			saveInvestmentSecurity(security);
		}
		// One:Many Hierarchy Move
		else {
			// Move Security Custom Columns
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setInstrumentId(instrument.getId());
			List<InvestmentSecurity> securityList = getInvestmentSecurityList(searchForm);
			getSystemColumnValueHandler().moveSystemColumnValueListForEntityList(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, BeanUtils.getBeanIdentityList(securityList), fromHierarchy.getId() + "", toHierarchy.getId() + "", bypassCustomColumnValidation);

			// Save Instrument
			saveInvestmentInstrument(instrument);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////////          Investment Security Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurity getInvestmentSecurity(int id) {
		return getInvestmentSecurityDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentSecurity getInvestmentSecurityBySymbolOnly(String symbol) {
		return getInvestmentSecurityBySymbol(symbol, null);
	}


	@Override
	public InvestmentSecurity getInvestmentSecurityBySymbol(String symbol, Boolean currency) {
		ValidationUtils.assertNotNull(symbol, "Security 'symbol' cannot be null. It is required in order to look up security.");
		InvestmentSecurity result = null;
		List<InvestmentSecurity> resultList = getInvestmentSecurityListBySymbol(symbol);
		if (!CollectionUtils.isEmpty(resultList)) {
			if (currency == null) {
				if (resultList.size() > 1) {
					throw new ValidationException("Found more than one Security for symbol: '" + symbol + "'. Results: " + CollectionUtils.toString(resultList, 5));
				}
				result = resultList.get(0);
			}
			else {
				if (resultList.size() > 1) {
					resultList = BeanUtils.filter(resultList, InvestmentSecurity::isCurrency, currency);
					if (resultList.size() > 1) {
						throw new ValidationException("Found more than one " + (currency ? "Currency" : "Non-Currency Security") + " for symbol: '" + symbol + "'. Results: " + CollectionUtils.toString(resultList, 5));
					}
				}
				if (resultList.size() == 1) {
					result = resultList.get(0);
					if (result.isCurrency() != currency) {
						result = null;
					}
				}
			}
		}

		if (result == null) {
			// check if the symbol has exchange code in it: if yes, try to match on clean symbol and exchange code
			// For example, "BEZ LN" will be matched to "BEZ" security on "London Stock Exchange" with code "LN"
			result = getInvestmentSecurityLocatorService().getInvestmentSecurityByFullSymbol(symbol);
		}

		return result;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurityByInstrument(int instrumentId) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInstrumentId(instrumentId);
		InvestmentSecurity security = CollectionUtils.getOnlyElement(getInvestmentSecurityList(searchForm));
		if (security != null && !security.getInstrument().getHierarchy().isOneSecurityPerInstrument()) {
			throw new IllegalArgumentException("Investment instrument " + security.getInstrument().getLabel() + " must have a one-to-one security relationship");
		}
		return security;
	}


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityListByIds(List<Integer> securityIds) {
		if (CollectionUtils.isEmpty(securityIds)) {
			return null;
		}
		return getInvestmentSecurityDAO().findByPrimaryKeys(securityIds.toArray(new Integer[0]));
	}


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityListBySymbol(String symbol) {
		return getInvestmentSecurityListBySymbolCache().getBeanListForKeyValue(getInvestmentSecurityDAO(), symbol);
	}


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityListByCusip(String cusip) {
		return getInvestmentSecurityListByCusipCache().getBeanListForKeyValue(getInvestmentSecurityDAO(), cusip);
	}


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityListBySedol(String sedol) {
		return getInvestmentSecurityListBySedolCache().getBeanListForKeyValue(getInvestmentSecurityDAO(), sedol);
	}


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityListByIsin(String isin) {
		return getInvestmentSecurityListByIsinCache().getBeanListForKeyValue(getInvestmentSecurityDAO(), isin);
	}


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityList(final SecuritySearchForm searchForm) {
		return getInvestmentSecurityDAO().findBySearchCriteria(new InvestmentSecuritySearchFormConfigurer(searchForm, getInvestmentSecurityGroupService()));
	}


	@Override
	@Transactional
	public InvestmentSecurity saveInvestmentSecurity(InvestmentSecurity security) {
		InvestmentInstrument instrument = security.getInstrument();

		//Perform majority of the validation first
		InvestmentInstrumentHierarchy hierarchy = validateAndUpdateSecurityBasedOnHierarchyRules(security, instrument);
		validateAndUpdateSecurityIdentifiers(security);
		validateAndUpdateUnderlyingSecurityAndInstrument(security, instrument, hierarchy);

		// If a new security and missing column group name, make sure no required custom fields
		// Useful for uploads to ensure users select the column group
		if (security.isNewBean() && StringUtils.isEmpty(security.getColumnGroupName())) {
			List<SystemColumnCustom> customColumnList = getSystemColumnService().getSystemColumnCustomListForGroupAndLinkedValue(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, hierarchy.getId().toString(), true);
			customColumnList = BeanUtils.filter(customColumnList, SystemColumnCustom::isRequired, true);
			if (!CollectionUtils.isEmpty(customColumnList)) {
				throw new ValidationException("Cannot create new security " + security.getLabel() + ", because missing required custom fields: " + StringUtils.collectionToCommaDelimitedString(customColumnList, SystemColumnCustom::getLabel));
			}
		}

		// When creating currencies, the instrument uses the currency as the trading currency (currency denomination).
		// Setting the trading currency to the currency being created will produce a circular, transient dependency in Hibernate and is sensitive to flushes.
		// Thus, we use the following flag to help conditionally update the instrument's trading currency and security within the transaction of this save action.
		boolean updateInstrumentTradingCurrency = false;

		// for one-to-one securities: need to save the instrument first
		if (hierarchy.isOneSecurityPerInstrument()) {
			// set instrument properties
			instrument.setName(security.getName());
			instrument.setDescription(security.getDescription());
			instrument.setIdentifierPrefix(security.getSymbol());
			if (hierarchy.isCurrency()) {
				instrument.setPriceMultiplier(BigDecimal.ONE);
				instrument.setExposureMultiplier(BigDecimal.ONE);
				instrument.setTradingCurrency(security);
				updateInstrumentTradingCurrency = security.isNewBean();
			}
			if (hierarchy.isTradingDisallowed()) {
				// Set it only if blank - need it for SHARE_PRICE Custom Allocations
				if (instrument.getPriceMultiplier() == null) {
					instrument.setPriceMultiplier(BigDecimal.ONE);
				}
				instrument.setExposureMultiplier(BigDecimal.ONE);
			}

			validateInvestmentInstrument(instrument, hierarchy);

			if (updateInstrumentTradingCurrency) {
				instrument.setTradingCurrency(null);
			}

			instrument = getInvestmentInstrumentDAO().save(instrument);
			security.setInstrument(instrument);
		}
		else if (!hierarchy.isTradingDisallowed()) {
			// tradable one-to-many securities
			ValidationUtils.assertNotNull(security.getEndDate(), "Security End Date is required for tradable one-to-many securities.", "endDate");
			if (instrument.isDeliverable()) {
				if (InvestmentUtils.isSecurityOfType(security, InvestmentType.FORWARDS)) {
					// deliverable forwards have "Settlement Date" which must be the same for End Date, First Delivery Date and Last Delivery Date
					security.setFirstDeliveryDate(security.getEndDate());
					security.setLastDeliveryDate(security.getEndDate());
				}
				ValidationUtils.assertNotNull(security.getFirstDeliveryDate(), "First Delivery Date is required for deliverable securities.", "firstDeliveryDate");
				ValidationUtils.assertNotNull(security.getLastDeliveryDate(), "Last Delivery Date is required for deliverable securities.", "lastDeliveryDate");
			}
		}
		if (!updateInstrumentTradingCurrency) {
			ValidationUtils.assertNotNull(instrument.getTradingCurrency(), "Currency denomination field is required.", "tradingCurrency.id");
		}

		if (hierarchy.isCurrency()) {
			ValidationUtils.assertNull(security.getBusinessCompany(), "Business Company field is not allowed on physical currencies.");
			security.setCurrency(true);
		}
		else if (security.isCurrency()) {
			throw new FieldValidationException("Cannot mark as currency a security that doesn't belong to currency hierarchy", "currency");
		}

		// validate Big Security (if active)
		if (instrument.getBigInstrument() != null && !instrument.getBigInstrument().isInactive()) {
			ValidationUtils.assertNotNull(security.getBigSecurity(), "Big Security field is required for " + security.getSymbol(), "bigSecurity.id");
			if (!instrument.getBigInstrument().equals(security.getBigSecurity().getInstrument())) {
				throw new FieldValidationException("Big Security for " + security.getSymbol() + " must belong to the following big instrument: "
						+ security.getInstrument().getBigInstrument().getLabelLong(), "bigSecurity.id");
			}
			// NOTE: should we also validate that Valuation Date is the same for cash settled futures and non-OTC options?
		}
		else {
			security.setBigSecurity(null);
		}

		// validate Reference Security
		if (security.getReferenceSecurity() != null) {
			if (hierarchy.isReferenceSecurityAllowed()) {
				if (hierarchy.isReferenceSecurityFromSameInstrument() && !security.getReferenceSecurity().getInstrument().equals(security.getInstrument())) {
					throw new FieldValidationException("Reference Security must belong to the same Instrument as the security: " + security, "referenceSecurity");
				}
				if (security.getReferenceSecurity().equals(security)) {
					throw new FieldValidationException("Reference Security cannot reference itself: " + security, "referenceSecurity");
				}
			}
			else {
				throw new FieldValidationException("Reference Security is not allowed for this hierarchy. Security: " + security, "referenceSecurity");
			}
		}

		if (security.getSettlementCurrency() != null) {
			ValidationUtils.assertTrue(hierarchy.isDifferentSettlementCurrencyAllowed(), "Settlement Currency is not allowed for security's hierarchy: " + security, "settlementCurrency");
		}

		if (security.getPriceMultiplierOverride() != null) {
			// FYI: Screen should not display the field if hierarchy doesn't allow it
			ValidationUtils.assertTrue(hierarchy.isSecurityPriceMultiplierOverrideAllowed(), "Cannot enter a price multiplier override for this security, because the hierarchy does not allow overrides.", "priceMultiplierOverride");
			InvestmentUtils.validatePriceMultiplierValue(security.getPriceMultiplierOverride());
		}

		// save security and its inseparable dependencies
		security = getInvestmentSecurityDAO().save(security);

		// auto-generate internal CUSIP for OTC securities and Cleared Swaps
		if (InvestmentUtils.isInternalCUSIPRequired(security)) {
			String cusip = InvestmentUtils.getInternalCUSIP(security.getId());
			if (!StringUtils.isEqual(cusip, security.getCusip())) {
				if (!StringUtils.isEmpty(security.getCusip())) {
					throw new FieldValidationException("Internally generated CUSIP for OTC security must be: " + cusip, "cusip");
				}
				security.setCusip(cusip);
				security = getInvestmentSecurityDAO().save(security);
			}
		}

		if (updateInstrumentTradingCurrency) {
			// Setting the trading currency was delayed until the currency was saved to avoid circular, transient dependency
			InvestmentInstrument toUpdate = instrument;
			toUpdate.setTradingCurrency(security);
			instrument = SystemAuditDaoUtils.executeWithAuditingDisabled(new String[]{"tradingCurrency"}, () -> getInvestmentInstrumentDAO().save(toUpdate));
			security.setInstrument(instrument);
		}

		return security;
	}


	private InvestmentInstrumentHierarchy validateAndUpdateSecurityBasedOnHierarchyRules(InvestmentSecurity security, InvestmentInstrument instrument) {
		if (instrument == null || instrument.getHierarchy() == null || instrument.getHierarchy().isNewBean()) {
			throw new FieldValidationException("Instrument hierarchy attribute is required", "instrument.hierarchy");
		}
		InvestmentInstrumentHierarchy hierarchy = getInvestmentSetupService().getInvestmentInstrumentHierarchy(instrument.getHierarchy().getId());
		ValidationUtils.assertNotNull(hierarchy, "Cannot find investment hierarchy for id = " + instrument.getHierarchy().getId());
		if (!hierarchy.isAssignmentAllowed()) {
			throw new FieldValidationException("The hierarchy '" + hierarchy.getName() + "' does not allow instrument assignments.", "hierarchy.id");
		}

		// Allocated Securities that use the system to calculate market data require a start date
		if (hierarchy.isAllocationOfSecurities() && hierarchy.getSecurityAllocationType().getCalculatedFieldType().isSystemCalculated()) {
			ValidationUtils.assertNotNull(security.getStartDate(), "Start Date is required for securities that are an allocation of securities.", "startDate");
		}

		if (hierarchy.getBusinessCompanyType() != null && security.getBusinessCompany() != null) {
			ValidationUtils.assertEquals(hierarchy.getBusinessCompanyType(), security.getBusinessCompany().getType(), "Securities in this hierarchy require a company of type: "
					+ hierarchy.getBusinessCompanyType().getName());
		}
		if (hierarchy.isIlliquid() && !security.isIlliquid()) {
			throw new FieldValidationException("The hierarchy '" + hierarchy.getName() + "' does not allow liquid securities.", "illiquid");
		}

		if (instrument.isNewBean()) {
			// set default price multiplier for new instruments if one is defined on hierarchy
			if (instrument.getHierarchy().getPriceMultiplier() != null && MathUtils.isEqual(instrument.getPriceMultiplier(), 1)) {
				instrument.setPriceMultiplier(instrument.getHierarchy().getPriceMultiplier());
			}
		}
		if (instrument.getHierarchy().getPriceMultiplier() != null && !instrument.getHierarchy().isSecurityPriceMultiplierOverrideAllowed()) {
			ValidationUtils.assertTrue(MathUtils.isEqual(instrument.getHierarchy().getPriceMultiplier(), instrument.getPriceMultiplier()),
					"Instrument cannot have a Price Multiplier that is different from this defined on Hierarchy", "priceMultiplier");
		}

		if (security.getEarlyTerminationDate() != null) {
			if (security.getEndDate() == null) {
				throw new FieldValidationException("Early Termination Date cannot be specified when End Date is not defined.", "earlyTerminationDate");
			}
			if (security.getEarlyTerminationDate().after(security.getEndDate())) {
				throw new FieldValidationException("Early Termination Date cannot be after End Date of security.", "earlyTerminationDate");
			}
		}

		if (hierarchy.isOtc()) {
			if (!InvestmentUtils.isSecurityOfType(security, InvestmentType.FORWARDS)) {
				// require Client, Counterparty and ISDA for OTC securities (except for Forwards)
				ValidationUtils.assertNotNull(security.getBusinessCompany(), "Counterparty is required for OTC securities (except for Forwards).", "businessCompany");
				ValidationUtils.assertNotNull(security.getBusinessContract(), "Contract is required for OTC securities (except for Forwards).", "businessContract");
			}
		}

		return hierarchy;
	}


	/**
	 * Throws FieldValidationException if security Symbol, CUSIP, ISIN, OCC Symbol or other identifier fields are invalid.
	 * Converts symbol to upper case and sets CUSIP, ISIN fields if that's the value stored in the symbol.
	 */
	private void validateAndUpdateSecurityIdentifiers(InvestmentSecurity security) {
		// always capitalize CUSIP and Symbol
		if (!StringUtils.isEmpty(security.getSymbol())) {
			security.setSymbol(security.getSymbol().toUpperCase());
		}
		if (!StringUtils.isEmpty(security.getCusip())) {
			security.setCusip(security.getCusip().toUpperCase());
			SecurityIdentifierUtils.isValidCUSIP(security.getCusip(), true);
		}
		else if (!InvestmentUtils.isInternalCUSIPRequired(security) && SecurityIdentifierUtils.isValidCUSIP(security.getSymbol(), false)) {
			// if CUSIP was not populated but Symbol is a CUSIP, then also set CUSIP = Symbol
			security.setCusip(security.getSymbol());
		}
		if (!StringUtils.isEmpty(security.getIsin())) {
			security.setIsin(security.getIsin().toUpperCase());
			SecurityIdentifierUtils.isValidISIN(security.getIsin(), true);
		}
		else if (SecurityIdentifierUtils.isValidISIN(security.getSymbol(), false)) {
			// if ISIN was not populated but Symbol is an ISIN, then also set ISIN = Symbol
			security.setIsin(security.getSymbol());
		}
		SecurityIdentifierUtils.isValidOccSymbol(security.getOccSymbol(), true);

		// ensure that name is not equal to symbol, when CUSIP is populated
		if (!StringUtils.isEmpty(security.getCusip())) {
			boolean nameEqualsSymbol = (StringUtils.compare(security.getName(), security.getSymbol()) == 0);
			boolean nameEqualsCusip = (StringUtils.compare(security.getName(), security.getCusip()) == 0);

			if (nameEqualsSymbol) {
				if (nameEqualsCusip) {
					// if name is same as symbol and cusip, indicate name field as wrong
					throw new FieldValidationException("\"Name\" field must differ from \"Symbol\" field for: " + security.getSymbol(), "name");
				}
				// else do not indicate a specific field
				throw new ValidationException("\"Symbol\" and \"Name\" fields must be different for: " + security.getLabel());
			}
		}
	}


	private void validateAndUpdateUnderlyingSecurityAndInstrument(InvestmentSecurity security, InvestmentInstrument instrument, InvestmentInstrumentHierarchy hierarchy) {
		// validate underlying
		InvestmentInstrument underlyingInstrument = instrument.getUnderlyingInstrument();
		InvestmentSecurity underlyingSecurity = security.getUnderlyingSecurity();
		if (underlyingInstrument == null) {
			if (hierarchy.isOneSecurityPerInstrument() && underlyingSecurity != null && underlyingSecurity.getInstrument().getHierarchy().isOneSecurityPerInstrument()) {
				if (hierarchy.isUnderlyingAllowed()) {
					// 1-1 securities such as Depository Receipts (e.g. ADRs and GDRs) can have an underlying security that is another 1-1 security. If the underlying security is set, reflect it on instrument.
					underlyingInstrument = underlyingSecurity.getInstrument();
					instrument.setUnderlyingInstrument(underlyingInstrument);
				}
				else {
					underlyingSecurity = null;
					security.setUnderlyingSecurity(underlyingSecurity);
				}
			}
			ValidationUtils.assertTrue((underlyingInstrument == null) == (underlyingSecurity == null), "Underlying security cannot be specified for a security for an instrument that doesn't have underlying.", "underlyingSecurity");
		}
		else {
			// If underlying is 1:1 - automatically set the security
			if (underlyingInstrument.getHierarchy().isOneSecurityPerInstrument()) {
				if (security.isUnderlyingSecurityFromSecurityUsed()) {
					// Security underlying should dictate the instrument's underlying
					underlyingInstrument = (underlyingSecurity == null) ? null : underlyingSecurity.getInstrument();
					instrument.setUnderlyingInstrument(underlyingInstrument);
				}
				else {
					underlyingSecurity = getInvestmentSecurityByInstrument(underlyingInstrument.getId());
					security.setUnderlyingSecurity(underlyingSecurity);
				}
			}

			// for 1 to 1 security: use instrument's underlying (security underlying is optional)
			else if (hierarchy.isOneSecurityPerInstrument()) {
				// If Underlying Is Not 1:1 - only validate it as required if this security is tradable
				// For some synthetics, we need to allow the instrument selection only, not the security level.  Example, SYN S&P MID CAP is a synthetic benchmark that applies to the S&P 400 Mini Future
				// Because the benchmark is 1:1 and the future is 1:many we can't say which future security specifically applies, however for what it is used for (matching underlying to the future instrument selected in a replication), we only case about the instrument level.
				// clear underlying security
				underlyingSecurity = null;
				if (!hierarchy.isTradingDisallowed()) {
					ValidationUtils.assertTrue(underlyingInstrument.getHierarchy().isOneSecurityPerInstrument(), "Underlying security is required because the instrument has underlying defined.", "underlyingSecurity");
				}
				security.setUnderlyingSecurity(underlyingSecurity);
			}

			if (underlyingSecurity != null) {
				if (!underlyingInstrument.equals(underlyingSecurity.getInstrument())) {
					throw new FieldValidationException("Underlying security for '" + security.getSymbol() + "' must belong to '" + underlyingInstrument.getLabel() + "' investment instrument.", "underlyingSecurity");
				}
				if (security.getEndDate() != null && underlyingSecurity.getEndDate() != null && security.getEndDate().after(underlyingSecurity.getEndDate())) {
					throw new FieldValidationException("Underlying security must have End Date on or after for this security.", "underlyingSecurity");
				}
			}
		}
	}


	@Override
	@Transactional
	public void deleteInvestmentSecurity(int id) {
		InvestmentSecurity security = getInvestmentSecurity(id);
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();

		// Also delete the allocations
		if (security.isAllocationOfSecurities()) {
			getInvestmentSecurityAllocationHandler().deleteInvestmentSecurityAllocationList(getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationListForParent(id));
		}

		getInvestmentSecurityDAO().delete(id);

		// also delete the instrument for one-to-one securities
		if (hierarchy.isOneSecurityPerInstrument()) {
			getInvestmentInstrumentDAO().delete(security.getInstrument());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstrument, Criteria> getInvestmentInstrumentDAO() {
		return this.investmentInstrumentDAO;
	}


	public void setInvestmentInstrumentDAO(AdvancedUpdatableDAO<InvestmentInstrument, Criteria> investmentInstrumentDAO) {
		this.investmentInstrumentDAO = investmentInstrumentDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSecurity, Criteria> getInvestmentSecurityDAO() {
		return this.investmentSecurityDAO;
	}


	public void setInvestmentSecurityDAO(AdvancedUpdatableDAO<InvestmentSecurity, Criteria> investmentSecurityDAO) {
		this.investmentSecurityDAO = investmentSecurityDAO;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public InvestmentSecurityAllocationHandler getInvestmentSecurityAllocationHandler() {
		return this.investmentSecurityAllocationHandler;
	}


	public void setInvestmentSecurityAllocationHandler(InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler) {
		this.investmentSecurityAllocationHandler = investmentSecurityAllocationHandler;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public InvestmentSecurityLocatorService getInvestmentSecurityLocatorService() {
		return this.investmentSecurityLocatorService;
	}


	public void setInvestmentSecurityLocatorService(InvestmentSecurityLocatorService investmentSecurityLocatorService) {
		this.investmentSecurityLocatorService = investmentSecurityLocatorService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public DaoSingleKeyListCache<InvestmentSecurity, String> getInvestmentSecurityListBySymbolCache() {
		return this.investmentSecurityListBySymbolCache;
	}


	public void setInvestmentSecurityListBySymbolCache(DaoSingleKeyListCache<InvestmentSecurity, String> investmentSecurityListBySymbolCache) {
		this.investmentSecurityListBySymbolCache = investmentSecurityListBySymbolCache;
	}


	public DaoSingleKeyListCache<InvestmentSecurity, String> getInvestmentSecurityListBySedolCache() {
		return this.investmentSecurityListBySedolCache;
	}


	public void setInvestmentSecurityListBySedolCache(DaoSingleKeyListCache<InvestmentSecurity, String> investmentSecurityListBySedolCache) {
		this.investmentSecurityListBySedolCache = investmentSecurityListBySedolCache;
	}


	public DaoSingleKeyListCache<InvestmentSecurity, String> getInvestmentSecurityListByCusipCache() {
		return this.investmentSecurityListByCusipCache;
	}


	public void setInvestmentSecurityListByCusipCache(DaoSingleKeyListCache<InvestmentSecurity, String> investmentSecurityListByCusipCache) {
		this.investmentSecurityListByCusipCache = investmentSecurityListByCusipCache;
	}


	public DaoSingleKeyListCache<InvestmentSecurity, String> getInvestmentSecurityListByIsinCache() {
		return this.investmentSecurityListByIsinCache;
	}


	public void setInvestmentSecurityListByIsinCache(DaoSingleKeyListCache<InvestmentSecurity, String> investmentSecurityListByIsinCache) {
		this.investmentSecurityListByIsinCache = investmentSecurityListByIsinCache;
	}
}
