package com.clifton.investment.instrument.event.sequence;


import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.Date;
import java.util.List;


public interface InvestmentInstrumentEventSequenceService {

	public InvestmentSecurityEvent getSecurityEventNext(EventSequenceDefinition sequenceDefinition, Date date);


	public List<InvestmentSecurityEvent> getSecurityEventList(EventSequenceDefinition sequenceDefinition, Date startDate, Date endDate);
}
