package com.clifton.investment.instrument.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Component;


/**
 * The InvestmentSecurityListByIsinCache class caches the list of {@link InvestmentSecurity} objects by security ISIN field.
 *
 * @author nickk
 */
@Component
public class InvestmentSecurityListByIsinCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentSecurity, String> {


	@Override
	protected String getBeanKeyProperty() {
		return "isin";
	}


	@Override
	protected String getBeanKeyValue(InvestmentSecurity security) {
		return security.getIsin();
	}


	/**
	 * Returns the upper case string representation of the super implementation.
	 */
	@Override
	protected String getBeanKeySegmentForProperty(Object keyProperty) {
		return super.getBeanKeySegmentForProperty(keyProperty).toUpperCase();
	}
}
