package com.clifton.investment.instrument.event;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>InvestmentSecurityEventExtended</code> is an extension of {@link InvestmentSecurityEvent} that includes payout related data.
 * This object will represent a unique payout for an event, or just an event when the event has no payout.
 *
 * @author nickk
 */
@NonPersistentObject
public class InvestmentSecurityEventExtended extends InvestmentSecurityEvent {

	private String uuid;

	/**
	 * The following fields are from event payout. All primitives are Objects to allow nulls
	 */
	private InvestmentSecurityEventPayout payout;

	private InvestmentSecurityEventPayoutType payoutType;

	private Short electionNumber;
	private Short payoutNumber;

	private InvestmentSecurity payoutSecurity;
	private BigDecimal payoutBeforeEventValue;
	private BigDecimal payoutAfterEventValue;
	private BigDecimal additionalPayoutValue;
	private BigDecimal additionalPayoutValue2;
	private BigDecimal prorationRate;
	private Date additionalPayoutDate;
	private FractionalShares fractionalSharesMethod;
	private String payoutDescription;

	private Boolean defaultElection;
	private Boolean deleted;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getEventLabel() {
		return super.getLabel();
	}


	@Override
	public String getLabel() {
		if (!isPayoutPresent()) {
			return getEventLabel();
		}
		return getPayout().getLabel();
	}


	@Override
	public BigDecimal getBeforeEventValue() {
		return ObjectUtils.coalesce(getPayoutBeforeEventValue(), super.getBeforeEventValue());
	}


	@Override
	public BigDecimal getAfterEventValue() {
		return ObjectUtils.coalesce(getPayoutAfterEventValue(), super.getAfterEventValue());
	}


	public String getDescription() {
		return ObjectUtils.coalesce(getPayoutDescription(), super.getEventDescription());
	}


	public boolean isPayoutPresent() {
		return getPayout() != null;
	}


	public Date getPayoutDate() {
		return ObjectUtils.coalesce(getAdditionalPayoutDate(), getEventDate());
	}


	public Integer getEventId() {
		return getId();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public InvestmentSecurityEventPayout getPayout() {
		return this.payout;
	}


	public void setPayout(InvestmentSecurityEventPayout payout) {
		this.payout = payout;
	}


	public InvestmentSecurityEventPayoutType getPayoutType() {
		return this.payoutType;
	}


	public void setPayoutType(InvestmentSecurityEventPayoutType payoutType) {
		this.payoutType = payoutType;
	}


	public Short getElectionNumber() {
		return this.electionNumber;
	}


	public void setElectionNumber(Short electionNumber) {
		this.electionNumber = electionNumber;
	}


	public Short getPayoutNumber() {
		return this.payoutNumber;
	}


	public void setPayoutNumber(Short payoutNumber) {
		this.payoutNumber = payoutNumber;
	}


	public InvestmentSecurity getPayoutSecurity() {
		return this.payoutSecurity;
	}


	public void setPayoutSecurity(InvestmentSecurity payoutSecurity) {
		this.payoutSecurity = payoutSecurity;
	}


	public BigDecimal getPayoutBeforeEventValue() {
		return this.payoutBeforeEventValue;
	}


	public void setPayoutBeforeEventValue(BigDecimal payoutBeforeEventValue) {
		this.payoutBeforeEventValue = payoutBeforeEventValue;
	}


	public BigDecimal getPayoutAfterEventValue() {
		return this.payoutAfterEventValue;
	}


	public void setPayoutAfterEventValue(BigDecimal payoutAfterEventValue) {
		this.payoutAfterEventValue = payoutAfterEventValue;
	}


	public BigDecimal getAdditionalPayoutValue() {
		return this.additionalPayoutValue;
	}


	public void setAdditionalPayoutValue(BigDecimal additionalPayoutValue) {
		this.additionalPayoutValue = additionalPayoutValue;
	}


	public BigDecimal getAdditionalPayoutValue2() {
		return this.additionalPayoutValue2;
	}


	public void setAdditionalPayoutValue2(BigDecimal additionalPayoutValue2) {
		this.additionalPayoutValue2 = additionalPayoutValue2;
	}


	public BigDecimal getProrationRate() {
		return this.prorationRate;
	}


	public void setProrationRate(BigDecimal prorationRate) {
		this.prorationRate = prorationRate;
	}


	public Date getAdditionalPayoutDate() {
		return this.additionalPayoutDate;
	}


	public void setAdditionalPayoutDate(Date additionalPayoutDate) {
		this.additionalPayoutDate = additionalPayoutDate;
	}


	public FractionalShares getFractionalSharesMethod() {
		return this.fractionalSharesMethod;
	}


	public void setFractionalSharesMethod(FractionalShares fractionalSharesMethod) {
		this.fractionalSharesMethod = fractionalSharesMethod;
	}


	public String getPayoutDescription() {
		return this.payoutDescription;
	}


	public void setPayoutDescription(String payoutDescription) {
		this.payoutDescription = payoutDescription;
	}


	public Boolean getDefaultElection() {
		return this.defaultElection;
	}


	public void setDefaultElection(Boolean defaultElection) {
		this.defaultElection = defaultElection;
	}


	public Boolean getDeleted() {
		return this.deleted;
	}


	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
}
