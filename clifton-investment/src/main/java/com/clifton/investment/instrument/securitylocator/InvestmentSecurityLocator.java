package com.clifton.investment.instrument.securitylocator;


import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>InvestmentSecurityLocator</code> interface should be implemented by various
 * data provider InvestmentSecurity locators.  For example, Bloomberg, etc.
 * <p/>
 * All locators defined in application context that implement this interface, will be
 * automatically registered and will be applied in the specified PriorityOrder.
 *
 * @author vgomelsky
 */
public interface InvestmentSecurityLocator {

	/**
	 * Defines priority order of this security locator.  When multiple locators are
	 * registered, locator with smallest order will be executed first.
	 * If a locator finds InvestmentSecurity, then this security will be returned and
	 * no additional locators will be executed.
	 */
	public int getPriorityOrder();


	/**
	 * Returns InvestmentSecurity for the specified symbol. The symbol may follow
	 * data provider specific syntax.  For example, "AAPL UW Equity".
	 * <p/>
	 * Returns null if no security can be found.
	 *
	 * @param symbol
	 */
	public InvestmentSecurity getInvestmentSecurity(String symbol);
}
