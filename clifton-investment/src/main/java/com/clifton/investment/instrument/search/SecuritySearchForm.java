package com.clifton.investment.instrument.search;


import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.investment.instrument.ExpiryTimeValues;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>SecuritySearchForm</code> class defines search configuration for InvestmentSecurity objects.
 *
 * @author manderson
 */
public class SecuritySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "symbol,name,cusip,isin,sedol,occSymbol")
	private String searchPattern;

	@SearchField(searchField = "symbol,name,cusip,isin,sedol,occSymbol", comparisonConditions = {ComparisonConditions.BEGINS_WITH})
	private String searchPatternUsingBeginsWith;

	@SearchField
	private String symbol;

	@SearchField(searchField = "symbol", comparisonConditions = {ComparisonConditions.EQUALS})
	private String symbolExact;

	@SearchField
	private String name;

	@SearchField
	private String cusip;

	@SearchField(searchField = "cusip", comparisonConditions = {ComparisonConditions.EQUALS})
	private String cusipEquals;

	@SearchField
	private String isin;

	@SearchField(searchField = "isin", comparisonConditions = {ComparisonConditions.EQUALS})
	private String isinEquals;

	@SearchField
	private String sedol;

	@SearchField(searchField = "sedol", comparisonConditions = {ComparisonConditions.EQUALS})
	private String sedolEquals;

	@SearchField
	private String figi;

	@SearchField
	private String occSymbol;

	@SearchField(searchField = "instrument.id")
	private Integer instrumentId;

	@SearchField(searchField = "instrument.inactive")
	private Boolean instrumentIsInactive;

	@SearchField(searchField = "investmentAccount.id", searchFieldPath = "instrument")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.id", searchFieldPath = "instrument")
	private Integer[] investmentAccountIds;

	@SearchField(searchField = "businessClient.id", searchFieldPath = "instrument.investmentAccount")
	private Integer businessClientId;

	@SearchField(searchFieldPath = "instrument.investmentAccount.businessClient.category", searchField = "categoryType")
	private BusinessClientCategoryTypes businessClientCategoryType;

	@SearchField(searchField = "investmentAccount.id", searchFieldPath = "instrument", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean investmentAccountPopulated;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "instrument")
	private Short hierarchyId;

	@SearchField(searchField = "name", searchFieldPath = "instrument.hierarchy")
	private String hierarchyName;

	@SearchField(searchField = "otc", searchFieldPath = "instrument.hierarchy")
	private Boolean otc;

	@SearchField(searchField = "name", searchFieldPath = "instrument.hierarchy.investmentType")
	private String investmentType;

	@SearchField(searchField = "name", searchFieldPath = "instrument.hierarchy.investmentTypeSubType")
	private String investmentTypeSubType;

	@SearchField(searchField = "name", searchFieldPath = "instrument.hierarchy.investmentTypeSubType2")
	private String investmentTypeSubType2;

	@SearchField(searchField = "eventTypeList.id", searchFieldPath = "instrument.hierarchy", comparisonConditions = ComparisonConditions.EXISTS)
	private Short eventTypeId;

	@SearchField(searchField = "eventTypeList.name", searchFieldPath = "instrument.hierarchy", comparisonConditions = ComparisonConditions.EXISTS)
	private String eventTypeName;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short securityGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short[] securityGroupIds;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "instrument")
	private Integer tradingCurrencyId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "instrument")
	private Integer currencyDenominationId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "instrument", comparisonConditions = {ComparisonConditions.IN})
	private Integer[] currencyDenominationIds;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "instrument.underlyingInstrument")
	private Integer underlyingCurrencyDenominationId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "instrument.underlyingInstrument", comparisonConditions = {ComparisonConditions.IN})
	private Integer[] underlyingCurrencyDenominationIds;

	@SearchField(searchField = "oneSecurityPerInstrument", searchFieldPath = "instrument.underlyingInstrument.hierarchy")
	private Boolean underlyingIsOneSecurityPerInstrument;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "instrument.underlyingInstrument.hierarchy")
	private Short underlyingInvestmentTypeId;

	@SearchField(searchField = "underlyingInstrument.id", searchFieldPath = "instrument")
	private Integer underlyingInstrumentId;

	@SearchField(searchField = "exchange.id", searchFieldPath = "instrument")
	private Short exchangeId;

	@SearchField(searchField = "exchangeCode", searchFieldPath = "instrument.exchange", comparisonConditions = {ComparisonConditions.EQUALS})
	private String exchangeCode;

	@SearchField(searchField = "compositeExchange.id", searchFieldPath = "instrument")
	private Short compositeExchangeId;

	@SearchField(searchField = "exchangeCode", searchFieldPath = "instrument.compositeExchange")
	private String compositeExchangeCode;

	@SearchField(searchField = "exchange.exchangeCode,compositeExchange.exchangeCode", searchFieldPath = "instrument", searchFieldCustomType = SearchFieldCustomTypes.OR)
	private String primaryOrCompositeExchange;

	@SearchField(searchField = "settlementCalendar.id", searchFieldPath = "instrument")
	private Short settlementCalendarId;

	@SearchField(searchField = "countryOfRisk.id", searchFieldPath = "instrument")
	private Integer countryOfRiskId;

	@SearchField(searchField = "countryOfIncorporation.id", searchFieldPath = "instrument")
	private Integer countryOfIncorporationId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "instrument.hierarchy")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentTypeSubType.id", searchFieldPath = "instrument.hierarchy")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "investmentTypeSubType2.id", searchFieldPath = "instrument.hierarchy")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "accrualSecurityEventType.id", searchFieldPath = "instrument.hierarchy")
	private Short instrumentHierarchyAccrualEventTypeId;

	@SearchField(searchField = "accrualMethod", searchFieldPath = "instrument.hierarchy")
	private AccrualMethods instrumentHierarchyAccrualMethod;

	@SearchField(searchField = "businessContract.id")
	private Integer businessContractId;

	@SearchField(searchField = "businessCompany.id")
	private Integer businessCompanyId;

	@SearchField(searchField = "businessCompany.id")
	private Integer[] businessCompanyIds;

	@SearchField(searchField = "underlyingSecurity.id")
	private Integer underlyingSecurityId;

	@SearchField
	private InvestmentSecurityOptionTypes optionType;

	@SearchField
	private OptionStyleTypes optionStyle;

	@SearchField
	private ExpiryTimeValues settlementExpiryTime;

	@SearchField
	private BigDecimal optionStrikePrice;

	@SearchField(searchField = "bigSecurity.id")
	private Integer bigSecurityId;

	@SearchField(searchField = "bigSecurity.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] bigSecurityIdList;

	@SearchField(searchField = "referenceSecurity.id")
	private Integer referenceSecurityId;

	@SearchField(searchField = "settlementCurrency.id")
	private Integer settlementCurrencyId;

	@SearchField(comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL, ComparisonConditions.IS_NOT_NULL, ComparisonConditions.LESS_THAN, ComparisonConditions.GREATER_THAN})
	private Date startDate;

	@SearchField(comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL, ComparisonConditions.IS_NOT_NULL, ComparisonConditions.LESS_THAN, ComparisonConditions.GREATER_THAN,
			ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.GREATER_THAN_OR_IS_NULL})
	private Date endDate;

	@SearchField(comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL, ComparisonConditions.LESS_THAN, ComparisonConditions.GREATER_THAN,
			ComparisonConditions.LESS_THAN_OR_EQUALS})
	private Date earlyTerminationDate;

	@SearchField(comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL, ComparisonConditions.LESS_THAN, ComparisonConditions.GREATER_THAN,
			ComparisonConditions.LESS_THAN_OR_EQUALS})
	private Date lastDeliveryDate;

	// Custom Search Field >= coalesce(firstNotice,endDate)
	private Date afterFirstNoticeDateOrEndDate;

	// CUSTOM FILTER: If active == true, sets activeOnDate to today, if active == false, sets endDate to yesterday
	private Boolean active;

	// CUSTOM FILTER: (startDate IS NULL OR startDate >= activeOnDate) AND (endDate IS NULL OR COALESCE(earlyTerminationDate, endDate) <= activeOnDate)
	private Date activeOnDate;

	// CUSTOM FILTER = IF SET - EXCLUDES SECURITIES TIED TO A CALENDAR THAT HAVE GIVEN DATE SET AS A HOLIDAY
	private Date noHolidayOnDate;

	// CUSTOM FILTER: (startDate IS NOT NULL AND startDate > activeOnDate) OR (endDate IS NOT NULL AND endDate < activeOnDate)
	private Date notActiveOnDate;

	// CUSTOM FILTER - CAN BE CLOSING, LAST, OR SPECIFIED - If Specified looks for missing data for field selected by noMarketDataFieldOnDateId
	private String noMarketDataFieldOnDateType;

	// MUTUALLY EXCLUSIVE WITH noClosingPriceMarketDataOnDate & noLatestPriceMarketDataOnDate  CUSTOM FILTER: can only be used in conjunction with 'activeOnDate' filter; restricts to securities that do not have a value for the specified data field on activeOnDate
	private Short noMarketDataFieldOnDateId;

	/*
	 * CUSTOM FILTER: Returns securities where we have active positions on specified date (Uses AccountingPositionDaily).
	 * AccountingPositionDaily is built daily to contain the daily position snapshots up to the previous week day.
	 * Thus, if looking for active positions, use a week day date before or up to the previous week day of the current day.
	 */
	private Date activePositionsOnDate;

	@SearchField(searchFieldPath = "instrument.hierarchy", searchField = "securityAllocationType", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean allocationOfSecurities;

	@SearchField(searchFieldPath = "instrument.hierarchy", searchField = "securityAllocationType")
	private InvestmentSecurityAllocationTypes securityAllocationType;

	// Custom Search Field - where exists security structure weight for instrument structure of these types
	private InvestmentInstrumentStructureTypes[] instrumentStructureTypes;

	// Custom Search Field - where security structure weight exists for instrument
	private Boolean instrumentStructureTypeExists;

	/**
	 * If true, will return only securities flagged as currency
	 * false - returns only securities flagged as not currency
	 * null - doesn't filter
	 */
	@SearchField
	private Boolean currency;

	@SearchField
	private Boolean illiquid;

	@SearchField(searchFieldPath = "instrument.hierarchy")
	private Boolean tradingDisallowed;

	@SearchField(searchFieldPath = "instrument.hierarchy")
	private Boolean oneSecurityPerInstrument;

	@SearchField(searchFieldPath = "instrument.hierarchy")
	private Boolean referenceSecurityAllowed;

	@SearchField(searchField = "instrumentScreen", searchFieldPath = "instrument.hierarchy")
	private String instrumentScreen;

	@SearchField(searchField = "priceMultiplierOverride,instrument.priceMultiplier", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal priceMultiplier;

	@SearchField(searchField = "priceMultiplierOverride", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean priceMultiplierOverridePopulated;

	@SearchField(searchField = "firstDeliveryDate")
	private Date firstDeliveryDate;

	// Custom Search Filter (Only returns those securities specifically tied to a group)
	private Short investmentSecurityGroupId;

	// Custom Search Filter (Only returns those securities not tied to the group, or if only one per instrument, no ties to the security's instrument)
	private Short excludeInvestmentSecurityGroupId;

	// Custom Search Filter (Only returns those securities not tied to the group, or if only one per instrument, no ties to the security's instrument)
	private Short[] excludeInvestmentSecurityGroupIds;

	// Custom Search Filter - used with customColumnValue
	// Can be used to find a security with a specific value for a custom column - i.e. Filter by all Put options
	private String customColumnName;

	// Custom Search Filter - used with customColumnName
	private String customColumnValue;

	// Custom Search Filter - to be used with accrualMethodOneAndTwo
	private Short accrualEventTypeIdOneAndTwo;

	// Custom Search Filter - to be used with accrualEventTypeIdOneAndTwo
	private AccrualMethods accrualMethodOneAndTwo;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getSearchPatternUsingBeginsWith() {
		return this.searchPatternUsingBeginsWith;
	}


	public void setSearchPatternUsingBeginsWith(String searchPatternUsingBeginsWith) {
		this.searchPatternUsingBeginsWith = searchPatternUsingBeginsWith;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getSymbolExact() {
		return this.symbolExact;
	}


	public void setSymbolExact(String symbolExact) {
		this.symbolExact = symbolExact;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public String getCusipEquals() {
		return this.cusipEquals;
	}


	public void setCusipEquals(String cusipEquals) {
		this.cusipEquals = cusipEquals;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public String getIsinEquals() {
		return this.isinEquals;
	}


	public void setIsinEquals(String isinEquals) {
		this.isinEquals = isinEquals;
	}


	public String getSedol() {
		return this.sedol;
	}


	public void setSedol(String sedol) {
		this.sedol = sedol;
	}


	public String getSedolEquals() {
		return this.sedolEquals;
	}


	public void setSedolEquals(String sedolEquals) {
		this.sedolEquals = sedolEquals;
	}


	public String getFigi() {
		return this.figi;
	}


	public void setFigi(String figi) {
		this.figi = figi;
	}


	public String getOccSymbol() {
		return this.occSymbol;
	}


	public void setOccSymbol(String occSymbol) {
		this.occSymbol = occSymbol;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public Boolean getInstrumentIsInactive() {
		return this.instrumentIsInactive;
	}


	public void setInstrumentIsInactive(Boolean instrumentIsInactive) {
		this.instrumentIsInactive = instrumentIsInactive;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Integer[] getInvestmentAccountIds() {
		return this.investmentAccountIds;
	}


	public void setInvestmentAccountIds(Integer[] investmentAccountIds) {
		this.investmentAccountIds = investmentAccountIds;
	}


	public Integer getBusinessClientId() {
		return this.businessClientId;
	}


	public void setBusinessClientId(Integer businessClientId) {
		this.businessClientId = businessClientId;
	}


	public BusinessClientCategoryTypes getBusinessClientCategoryType() {
		return this.businessClientCategoryType;
	}


	public void setBusinessClientCategoryType(BusinessClientCategoryTypes businessClientCategoryType) {
		this.businessClientCategoryType = businessClientCategoryType;
	}


	public Boolean getInvestmentAccountPopulated() {
		return this.investmentAccountPopulated;
	}


	public void setInvestmentAccountPopulated(Boolean investmentAccountPopulated) {
		this.investmentAccountPopulated = investmentAccountPopulated;
	}


	public Short getHierarchyId() {
		return this.hierarchyId;
	}


	public void setHierarchyId(Short hierarchyId) {
		this.hierarchyId = hierarchyId;
	}


	public String getHierarchyName() {
		return this.hierarchyName;
	}


	public void setHierarchyName(String hierarchyName) {
		this.hierarchyName = hierarchyName;
	}


	public Boolean getOtc() {
		return this.otc;
	}


	public void setOtc(Boolean otc) {
		this.otc = otc;
	}


	public String getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(String investmentType) {
		this.investmentType = investmentType;
	}


	public String getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(String investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public String getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(String investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}


	public String getEventTypeName() {
		return this.eventTypeName;
	}


	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public void setSecurityGroupId(Short securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public Short[] getSecurityGroupIds() {
		return this.securityGroupIds;
	}


	public void setSecurityGroupIds(Short[] securityGroupIds) {
		this.securityGroupIds = securityGroupIds;
	}


	public Integer getTradingCurrencyId() {
		return this.tradingCurrencyId;
	}


	public void setTradingCurrencyId(Integer tradingCurrencyId) {
		this.tradingCurrencyId = tradingCurrencyId;
	}


	public Integer getCurrencyDenominationId() {
		return this.currencyDenominationId;
	}


	public void setCurrencyDenominationId(Integer currencyDenominationId) {
		this.currencyDenominationId = currencyDenominationId;
	}


	public Integer[] getCurrencyDenominationIds() {
		return this.currencyDenominationIds;
	}


	public void setCurrencyDenominationIds(Integer[] currencyDenominationIds) {
		this.currencyDenominationIds = currencyDenominationIds;
	}


	public Integer getUnderlyingCurrencyDenominationId() {
		return this.underlyingCurrencyDenominationId;
	}


	public void setUnderlyingCurrencyDenominationId(Integer underlyingCurrencyDenominationId) {
		this.underlyingCurrencyDenominationId = underlyingCurrencyDenominationId;
	}


	public Integer[] getUnderlyingCurrencyDenominationIds() {
		return this.underlyingCurrencyDenominationIds;
	}


	public void setUnderlyingCurrencyDenominationIds(Integer[] underlyingCurrencyDenominationIds) {
		this.underlyingCurrencyDenominationIds = underlyingCurrencyDenominationIds;
	}


	public Boolean getUnderlyingIsOneSecurityPerInstrument() {
		return this.underlyingIsOneSecurityPerInstrument;
	}


	public void setUnderlyingIsOneSecurityPerInstrument(Boolean underlyingIsOneSecurityPerInstrument) {
		this.underlyingIsOneSecurityPerInstrument = underlyingIsOneSecurityPerInstrument;
	}


	public Short getUnderlyingInvestmentTypeId() {
		return this.underlyingInvestmentTypeId;
	}


	public void setUnderlyingInvestmentTypeId(Short underlyingInvestmentTypeId) {
		this.underlyingInvestmentTypeId = underlyingInvestmentTypeId;
	}


	public Integer getUnderlyingInstrumentId() {
		return this.underlyingInstrumentId;
	}


	public void setUnderlyingInstrumentId(Integer underlyingInstrumentId) {
		this.underlyingInstrumentId = underlyingInstrumentId;
	}


	public Short getExchangeId() {
		return this.exchangeId;
	}


	public void setExchangeId(Short exchangeId) {
		this.exchangeId = exchangeId;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}


	public Short getCompositeExchangeId() {
		return this.compositeExchangeId;
	}


	public void setCompositeExchangeId(Short compositeExchangeId) {
		this.compositeExchangeId = compositeExchangeId;
	}


	public String getCompositeExchangeCode() {
		return this.compositeExchangeCode;
	}


	public void setCompositeExchangeCode(String compositeExchangeCode) {
		this.compositeExchangeCode = compositeExchangeCode;
	}


	public String getPrimaryOrCompositeExchange() {
		return this.primaryOrCompositeExchange;
	}


	public void setPrimaryOrCompositeExchange(String primaryOrCompositeExchange) {
		this.primaryOrCompositeExchange = primaryOrCompositeExchange;
	}


	public Short getSettlementCalendarId() {
		return this.settlementCalendarId;
	}


	public void setSettlementCalendarId(Short settlementCalendarId) {
		this.settlementCalendarId = settlementCalendarId;
	}


	public Integer getCountryOfRiskId() {
		return this.countryOfRiskId;
	}


	public void setCountryOfRiskId(Integer countryOfRiskId) {
		this.countryOfRiskId = countryOfRiskId;
	}


	public Integer getCountryOfIncorporationId() {
		return this.countryOfIncorporationId;
	}


	public void setCountryOfIncorporationId(Integer countryOfIncorporationId) {
		this.countryOfIncorporationId = countryOfIncorporationId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public AccrualMethods getInstrumentHierarchyAccrualMethod() {
		return this.instrumentHierarchyAccrualMethod;
	}


	public void setInstrumentHierarchyAccrualMethod(AccrualMethods instrumentHierarchyAccrualMethod) {
		this.instrumentHierarchyAccrualMethod = instrumentHierarchyAccrualMethod;
	}


	public Short getInstrumentHierarchyAccrualEventTypeId() {
		return this.instrumentHierarchyAccrualEventTypeId;
	}


	public void setInstrumentHierarchyAccrualEventTypeId(Short instrumentHierarchyAccrualEventTypeId) {
		this.instrumentHierarchyAccrualEventTypeId = instrumentHierarchyAccrualEventTypeId;
	}


	public Integer getBusinessContractId() {
		return this.businessContractId;
	}


	public void setBusinessContractId(Integer businessContractId) {
		this.businessContractId = businessContractId;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}


	public Integer[] getBusinessCompanyIds() {
		return this.businessCompanyIds;
	}


	public void setBusinessCompanyIds(Integer[] businessCompanyIds) {
		this.businessCompanyIds = businessCompanyIds;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public InvestmentSecurityOptionTypes getOptionType() {
		return this.optionType;
	}


	public void setOptionType(InvestmentSecurityOptionTypes optionType) {
		this.optionType = optionType;
	}


	public OptionStyleTypes getOptionStyle() {
		return this.optionStyle;
	}


	public void setOptionStyle(OptionStyleTypes optionStyle) {
		this.optionStyle = optionStyle;
	}


	public ExpiryTimeValues getSettlementExpiryTime() {
		return this.settlementExpiryTime;
	}


	public void setSettlementExpiryTime(ExpiryTimeValues settlementExpiryTime) {
		this.settlementExpiryTime = settlementExpiryTime;
	}


	public BigDecimal getOptionStrikePrice() {
		return this.optionStrikePrice;
	}


	public void setOptionStrikePrice(BigDecimal optionStrikePrice) {
		this.optionStrikePrice = optionStrikePrice;
	}


	public Integer getBigSecurityId() {
		return this.bigSecurityId;
	}


	public void setBigSecurityId(Integer bigSecurityId) {
		this.bigSecurityId = bigSecurityId;
	}


	public Integer[] getBigSecurityIdList() {
		return this.bigSecurityIdList;
	}


	public void setBigSecurityIdList(Integer[] bigSecurityIdList) {
		this.bigSecurityIdList = bigSecurityIdList;
	}


	public Integer getReferenceSecurityId() {
		return this.referenceSecurityId;
	}


	public void setReferenceSecurityId(Integer referenceSecurityId) {
		this.referenceSecurityId = referenceSecurityId;
	}


	public Integer getSettlementCurrencyId() {
		return this.settlementCurrencyId;
	}


	public void setSettlementCurrencyId(Integer settlementCurrencyId) {
		this.settlementCurrencyId = settlementCurrencyId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getEarlyTerminationDate() {
		return this.earlyTerminationDate;
	}


	public void setEarlyTerminationDate(Date earlyTerminationDate) {
		this.earlyTerminationDate = earlyTerminationDate;
	}


	public Date getLastDeliveryDate() {
		return this.lastDeliveryDate;
	}


	public void setLastDeliveryDate(Date lastDeliveryDate) {
		this.lastDeliveryDate = lastDeliveryDate;
	}


	public Date getAfterFirstNoticeDateOrEndDate() {
		return this.afterFirstNoticeDateOrEndDate;
	}


	public void setAfterFirstNoticeDateOrEndDate(Date afterFirstNoticeDateOrEndDate) {
		this.afterFirstNoticeDateOrEndDate = afterFirstNoticeDateOrEndDate;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public Date getNoHolidayOnDate() {
		return this.noHolidayOnDate;
	}


	public void setNoHolidayOnDate(Date noHolidayOnDate) {
		this.noHolidayOnDate = noHolidayOnDate;
	}


	public Date getNotActiveOnDate() {
		return this.notActiveOnDate;
	}


	public void setNotActiveOnDate(Date notActiveOnDate) {
		this.notActiveOnDate = notActiveOnDate;
	}


	public String getNoMarketDataFieldOnDateType() {
		return this.noMarketDataFieldOnDateType;
	}


	public void setNoMarketDataFieldOnDateType(String noMarketDataFieldOnDateType) {
		this.noMarketDataFieldOnDateType = noMarketDataFieldOnDateType;
	}


	public Short getNoMarketDataFieldOnDateId() {
		return this.noMarketDataFieldOnDateId;
	}


	public void setNoMarketDataFieldOnDateId(Short noMarketDataFieldOnDateId) {
		this.noMarketDataFieldOnDateId = noMarketDataFieldOnDateId;
	}


	public Date getActivePositionsOnDate() {
		return this.activePositionsOnDate;
	}


	public void setActivePositionsOnDate(Date activePositionsOnDate) {
		this.activePositionsOnDate = activePositionsOnDate;
	}


	public Boolean getAllocationOfSecurities() {
		return this.allocationOfSecurities;
	}


	public void setAllocationOfSecurities(Boolean allocationOfSecurities) {
		this.allocationOfSecurities = allocationOfSecurities;
	}


	public InvestmentSecurityAllocationTypes getSecurityAllocationType() {
		return this.securityAllocationType;
	}


	public void setSecurityAllocationType(InvestmentSecurityAllocationTypes securityAllocationType) {
		this.securityAllocationType = securityAllocationType;
	}


	public InvestmentInstrumentStructureTypes[] getInstrumentStructureTypes() {
		return this.instrumentStructureTypes;
	}


	public void setInstrumentStructureTypes(InvestmentInstrumentStructureTypes[] instrumentStructureTypes) {
		this.instrumentStructureTypes = instrumentStructureTypes;
	}


	public Boolean getInstrumentStructureTypeExists() {
		return this.instrumentStructureTypeExists;
	}


	public void setInstrumentStructureTypeExists(Boolean instrumentStructureTypeExists) {
		this.instrumentStructureTypeExists = instrumentStructureTypeExists;
	}


	public Boolean getCurrency() {
		return this.currency;
	}


	public void setCurrency(Boolean currency) {
		this.currency = currency;
	}


	public Boolean getIlliquid() {
		return this.illiquid;
	}


	public void setIlliquid(Boolean illiquid) {
		this.illiquid = illiquid;
	}


	public Boolean getTradingDisallowed() {
		return this.tradingDisallowed;
	}


	public void setTradingDisallowed(Boolean tradingDisallowed) {
		this.tradingDisallowed = tradingDisallowed;
	}


	public Boolean getOneSecurityPerInstrument() {
		return this.oneSecurityPerInstrument;
	}


	public void setOneSecurityPerInstrument(Boolean oneSecurityPerInstrument) {
		this.oneSecurityPerInstrument = oneSecurityPerInstrument;
	}


	public Boolean getReferenceSecurityAllowed() {
		return this.referenceSecurityAllowed;
	}


	public void setReferenceSecurityAllowed(Boolean referenceSecurityAllowed) {
		this.referenceSecurityAllowed = referenceSecurityAllowed;
	}


	public String getInstrumentScreen() {
		return this.instrumentScreen;
	}


	public void setInstrumentScreen(String instrumentScreen) {
		this.instrumentScreen = instrumentScreen;
	}


	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	public void setPriceMultiplier(BigDecimal priceMultiplier) {
		this.priceMultiplier = priceMultiplier;
	}


	public Boolean getPriceMultiplierOverridePopulated() {
		return this.priceMultiplierOverridePopulated;
	}


	public void setPriceMultiplierOverridePopulated(Boolean priceMultiplierOverridePopulated) {
		this.priceMultiplierOverridePopulated = priceMultiplierOverridePopulated;
	}


	public Date getFirstDeliveryDate() {
		return this.firstDeliveryDate;
	}


	public void setFirstDeliveryDate(Date firstDeliveryDate) {
		this.firstDeliveryDate = firstDeliveryDate;
	}


	public Short getInvestmentSecurityGroupId() {
		return this.investmentSecurityGroupId;
	}


	public void setInvestmentSecurityGroupId(Short investmentSecurityGroupId) {
		this.investmentSecurityGroupId = investmentSecurityGroupId;
	}


	public Short getExcludeInvestmentSecurityGroupId() {
		return this.excludeInvestmentSecurityGroupId;
	}


	public void setExcludeInvestmentSecurityGroupId(Short excludeInvestmentSecurityGroupId) {
		this.excludeInvestmentSecurityGroupId = excludeInvestmentSecurityGroupId;
	}


	public Short[] getExcludeInvestmentSecurityGroupIds() {
		return this.excludeInvestmentSecurityGroupIds;
	}


	public void setExcludeInvestmentSecurityGroupIds(Short[] excludeInvestmentSecurityGroupIds) {
		this.excludeInvestmentSecurityGroupIds = excludeInvestmentSecurityGroupIds;
	}


	public String getCustomColumnName() {
		return this.customColumnName;
	}


	public void setCustomColumnName(String customColumnName) {
		this.customColumnName = customColumnName;
	}


	public String getCustomColumnValue() {
		return this.customColumnValue;
	}


	public void setCustomColumnValue(String customColumnValue) {
		this.customColumnValue = customColumnValue;
	}


	public Short getAccrualEventTypeIdOneAndTwo() {
		return this.accrualEventTypeIdOneAndTwo;
	}


	public void setAccrualEventTypeIdOneAndTwo(Short accrualEventTypeIdOneAndTwo) {
		this.accrualEventTypeIdOneAndTwo = accrualEventTypeIdOneAndTwo;
	}


	public AccrualMethods getAccrualMethodOneAndTwo() {
		return this.accrualMethodOneAndTwo;
	}


	public void setAccrualMethodOneAndTwo(AccrualMethods accrualMethodOneAndTwo) {
		this.accrualMethodOneAndTwo = accrualMethodOneAndTwo;
	}
}
