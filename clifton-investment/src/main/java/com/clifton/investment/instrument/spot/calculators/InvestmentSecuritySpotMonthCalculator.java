package com.clifton.investment.instrument.spot.calculators;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.spot.InvestmentSecuritySpotMonth;


/**
 * The <code>InvestmentSecuritySpotMonthCalculator</code> is used to calculate the spot month for a security
 * <p>
 * Spot Month is a time period until last delivery or expiration date of the the contract.  Used for Exchange Limits as we enter the Spot month
 * our limits are generally reduced so we start trading out of that contract and into a new one.
 *
 * @author manderson
 */
public interface InvestmentSecuritySpotMonthCalculator {


	public InvestmentSecuritySpotMonth calculateInvestmentSecuritySpotMonth(InvestmentSecurity security);
}
