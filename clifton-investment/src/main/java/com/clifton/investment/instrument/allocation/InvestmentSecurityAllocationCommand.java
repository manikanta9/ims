package com.clifton.investment.instrument.allocation;

import com.clifton.core.beans.BeanListCommand;

import java.util.Date;


/**
 * This command is used for updating/ending {@link InvestmentSecurityAllocation}s. Specify a list of allocations to process and an optional end date. Used by {@link InvestmentSecurityAllocationService} for submitting multiple allocations.
 *
 * @author mitchellf
 */
public class InvestmentSecurityAllocationCommand extends BeanListCommand<InvestmentSecurityAllocation> {

	private Date endDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
