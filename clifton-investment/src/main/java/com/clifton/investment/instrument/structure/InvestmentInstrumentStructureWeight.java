package com.clifton.investment.instrument.structure;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instrument.InvestmentInstrument;

import java.math.BigDecimal;


/**
 * The <code>InvestmentInstrumentStructureWeight</code> defines the details of a specific
 * structure.  Is assigned Instruments, can also be designated as a specific sector, and a weight multiplier.
 *
 * @author manderson
 */
public class InvestmentInstrumentStructureWeight extends BaseEntity<Integer> {

	/**
	 * Structure this weight is defined for
	 */
	private InvestmentInstrumentStructure instrumentStructure;

	/**
	 * Instrument included in the structure
	 * i.e. Corn, Natural Gas, etc for Commodity Futures
	 */
	private InvestmentInstrument instrument;

	/**
	 * Optional order field for sorting
	 */
	private Integer order;

	/**
	 * Optional sector for the selected instrument, i.e. Natural Gas = Energy
	 * Selectable options pull from SystemList=Investment Instrument Sectors
	 */
	private String sector;

	/**
	 * Multiplier that is applied to the instrument's weight calculation.
	 */
	private BigDecimal weight;


	public InvestmentInstrumentStructure getInstrumentStructure() {
		return this.instrumentStructure;
	}


	public void setInstrumentStructure(InvestmentInstrumentStructure instrumentStructure) {
		this.instrumentStructure = instrumentStructure;
	}


	public InvestmentInstrument getInstrument() {
		return this.instrument;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public String getSector() {
		return this.sector;
	}


	public void setSector(String sector) {
		this.sector = sector;
	}


	public BigDecimal getWeight() {
		return this.weight;
	}


	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}
}
