package com.clifton.investment.instrument.event.retriever;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventScheduleCopyCommand;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * The <code>InvestmentSecurityEventRetrieverServiceImpl</code> class provides basic implementation of InvestmentSecurityEventRetrieverService interface.
 *
 * @author vgomelsky
 */
@Service
public class InvestmentSecurityEventRetrieverServiceImpl implements InvestmentSecurityEventRetrieverService {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventRetrieverLocator investmentSecurityEventRetrieverLocator;
	private InvestmentSecurityEventService investmentSecurityEventService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getInvestmentSecurityEventTypeNamesWithRetrieverSupportedForHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		List<InvestmentSecurityEventType> eventTypeList = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeListByHierarchy(hierarchy.getId());
		if (CollectionUtils.isEmpty(eventTypeList)) {
			throw new ValidationException("Unable to Auto-Generate Events for Hierarchy [" + hierarchy.getLabelExpanded() + "].  There are no event types associated with this hierarchy.");
		}
		List<String> eventTypeNameList = new ArrayList<>();
		for (InvestmentSecurityEventType eventType : eventTypeList) {
			if (!eventTypeNameList.contains(eventType.getName())) {
				InvestmentSecurityEventRetriever retriever = getInvestmentSecurityEventRetrieverLocator().locate(eventType, hierarchy);
				if (retriever != null) {
					eventTypeNameList.add(eventType.getName());
				}
			}
		}

		if (CollectionUtils.isEmpty(eventTypeNameList)) {
			throw new ValidationException("Unable to Auto-Generate Events for Hierarchy [" + hierarchy.getLabelExpanded()
					+ "].  None of the event types associated with this hierarchy support auto-generation.");
		}
		return eventTypeNameList;
	}


	private InvestmentSecurityEventRetriever getInvestmentSecurityEventRetriever(String eventTypeName, InvestmentSecurity security) {
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(eventTypeName);
		ValidationUtils.assertNotNull(eventType, "Cannot find investment security event type for name = " + eventTypeName);
		InvestmentSecurityEventRetriever retriever = getInvestmentSecurityEventRetrieverLocator().locate(eventType, security.getInstrument().getHierarchy());
		ValidationUtils.assertNotNull(retriever, "Cannot find investment security event retriever for event type = " + eventTypeName);
		return retriever;
	}


	@Override
	public Integer loadInvestmentSecurityEvent(int securityId, String[] eventTypeNameList) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		ValidationUtils.assertNotNull(security, "Cannot find investment security for id = " + securityId);

		int result = 0;
		for (String eventTypeName : CollectionUtils.getIterable(Arrays.asList(eventTypeNameList))) {
			result += doLoadInvestmentSecurityEvent(security, eventTypeName);
		}
		return result;
	}


	private Integer doLoadInvestmentSecurityEvent(InvestmentSecurity security, String eventTypeName) {
		// 1. get event retriever for security and event type
		InvestmentSecurityEventRetriever retriever = getInvestmentSecurityEventRetriever(eventTypeName, security);

		// 2. get event
		InvestmentSecurityEvent event = retriever.getInvestmentSecurityEvent(security);

		// 3. prepare event: set additional properties (previous factor, date, etc.)
		if (!retriever.prepareInvestmentSecurityEvent(event)) {
			// duplicate event: skip
			return 0;
		}

		// 4. set the settlement currency
		if (event.getSecurity().getSettlementCurrency() != null) {
			event.setAdditionalSecurity(event.getSecurity().getSettlementCurrency());
		}

		// 5. save new event
		getInvestmentSecurityEventService().saveInvestmentSecurityEvent(event);

		return 1;
	}


	@Override
	@Transactional
	public Integer loadInvestmentSecurityEventHistory(int securityId, String[] eventTypeNameList, boolean ignoreSeedEvents, boolean deleteUnbooked) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		ValidationUtils.assertNotNull(security, "Cannot find investment security for id = " + securityId);

		int result = 0;
		for (String eventTypeName : CollectionUtils.getIterable(Arrays.asList(eventTypeNameList))) {
			if (deleteUnbooked) {
				deleteUnbookedEvents(security, eventTypeName);
			}
			result += doLoadInvestmentSecurityEventHistory(security, eventTypeName, ignoreSeedEvents);
		}
		return result;
	}


	@Override
	@Transactional
	public Integer copyInvestmentSecurityEventSchedule(InvestmentSecurityEventScheduleCopyCommand scheduleCopyCommand) {
		InvestmentSecurity destinationSecurity = getInvestmentInstrumentService().getInvestmentSecurity(scheduleCopyCommand.getDestinationSecurity().getId());

		ValidationUtils.assertNotNull(destinationSecurity, "The intended destination security for the schedule copy is not available.");
		if (scheduleCopyCommand.getSourceSecurity() != null) {
			return copyScheduleFromSourceToDestinationSecurity(scheduleCopyCommand.getSourceSecurity(), destinationSecurity);
		}

		ValidationUtils.assertNotNull(scheduleCopyCommand.getSourceInstrument(), "No Investment Instrument or Investment Security was selected, please select one and try again.");
		ValidationUtils.assertTrue(scheduleCopyCommand.getSourceInstrument().getHierarchy().isOneSecurityPerInstrument(),
				"The selected Investment Instrument is not a 1 to 1 mapping to a security, please select an Investment Security or a different Investment Instrument.");

		InvestmentSecurity sourceSecurity = getInvestmentInstrumentService().getInvestmentSecurityByInstrument(scheduleCopyCommand.getSourceInstrument().getId());
		ValidationUtils.assertNotNull(sourceSecurity, "The selected instrument did not have an associated security, please make a different selection.");
		return copyScheduleFromSourceToDestinationSecurity(sourceSecurity, destinationSecurity);
	}

	private Integer copyScheduleFromSourceToDestinationSecurity(InvestmentSecurity sourceSecurity, InvestmentSecurity destinationSecurity) throws ValidationException {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(sourceSecurity.getId());
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		List<InvestmentSecurityEvent> resultList = new ArrayList<>();
		if (!eventList.isEmpty()) {
			for (InvestmentSecurityEvent securityEvent : CollectionUtils.getIterable(eventList)) {
				InvestmentSecurityEvent clonedEvent = BeanUtils.cloneBean(securityEvent, false, false);
				clonedEvent.setSecurity(destinationSecurity);
				resultList.add(getInvestmentSecurityEventService().saveInvestmentSecurityEvent(clonedEvent));
			}
		}
		ValidationUtils.assertEquals(resultList.size(), eventList.size(), String.format("Schedule was not fully copied, expected: [%s] result: [%s].", eventList.size(), resultList.size()));
		return resultList.size();
	}


	private void deleteUnbookedEvents(InvestmentSecurity security, String eventTypeName) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setTypeName(eventTypeName);
		searchForm.setSecurityId(security.getId());
		searchForm.addSearchRestriction(new SearchRestriction("booked", null, true));

		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);

		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {
			getInvestmentSecurityEventService().deleteInvestmentSecurityEvent(event.getId());
		}
	}


	private Integer doLoadInvestmentSecurityEventHistory(InvestmentSecurity security, String eventTypeName, boolean ignoreSeedEvents) {
		// 1. get event retriever for security and event type
		InvestmentSecurityEventRetriever retriever = getInvestmentSecurityEventRetriever(eventTypeName, security);

		// 2. get events
		List<InvestmentSecurityEvent> eventList = retriever.getInvestmentSecurityEventHistory(security, ignoreSeedEvents);

		// 3. prepare events: set additional properties (previous factor, date, etc.)
		List<InvestmentSecurityEvent> newEventList = new ArrayList<>();
		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {
			if (retriever.prepareInvestmentSecurityEvent(event)) {
				newEventList.add(event);
			}
		}

		// 4. save new event
		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(newEventList)) {
			// set the settlement currency
			if (event.getSecurity().getSettlementCurrency() != null) {
				event.setAdditionalSecurity(event.getSecurity().getSettlementCurrency());
			}

			getInvestmentSecurityEventService().saveInvestmentSecurityEvent(event);
		}

		return newEventList.size();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventRetrieverLocator getInvestmentSecurityEventRetrieverLocator() {
		return this.investmentSecurityEventRetrieverLocator;
	}


	public void setInvestmentSecurityEventRetrieverLocator(InvestmentSecurityEventRetrieverLocator investmentSecurityEventRetrieverLocator) {
		this.investmentSecurityEventRetrieverLocator = investmentSecurityEventRetrieverLocator;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
