package com.clifton.investment.instrument.event.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>InvestmentSecurityEventForAccrualEndDateCacheImpl</code> class caches InvestmentSecurityEvent id's for accrual end date lookup.
 * <p>
 * Caching is done by securityId so that we can clear only security specific caches on changes.
 *
 * @author vgomelsky
 */
@Component
public class InvestmentSecurityEventForAccrualEndDateCacheImpl extends SelfRegisteringSimpleDaoCache<InvestmentSecurityEvent, Integer, Map<String, Optional<Integer>>> implements InvestmentSecurityEventForDateCache {


	@Override
	public Optional<Integer> getInvestmentSecurityEvent(int securityId, String typeName, Date lookupDate) {
		// intentional use of null: null means the lookup hasn't happened vs empty that means no data was returned by the lookup
		@SuppressWarnings("OptionalAssignedToNull")
		Optional<Integer> result = null;
		Map<String, Optional<Integer>> securityMap = getCacheHandler().get(getCacheName(), securityId);
		if (securityMap != null) {
			result = securityMap.get(getBeanKey(typeName, lookupDate));
		}
		return result;
	}


	@Override
	public void setInvestmentSecurityEvent(int securityId, String typeName, Date lookupDate, InvestmentSecurityEvent securityEvent) {
		Map<String, Optional<Integer>> securityMap = getCacheHandler().get(getCacheName(), securityId);
		if (securityMap == null) {
			securityMap = new ConcurrentHashMap<>();
			getCacheHandler().put(getCacheName(), securityId, securityMap);
		}
		securityMap.put(getBeanKey(typeName, lookupDate), securityEvent == null ? Optional.empty() : Optional.of(securityEvent.getId()));
	}


	private String getBeanKey(String typeName, Date lookupDate) {
		StringBuilder key = new StringBuilder(32);
		key.append(typeName);
		key.append(lookupDate.getTime());
		return key.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////                 Observer Methods                    //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<InvestmentSecurityEvent> dao, DaoEventTypes event, InvestmentSecurityEvent securityEvent, Throwable e) {
		if (e == null && securityEvent != null) {
			InvestmentSecurity security = securityEvent.getSecurity();
			if (security != null && security.getId() != null) {
				getCacheHandler().remove(getCacheName(), securityEvent.getSecurity().getId());
			}
		}
	}
}
