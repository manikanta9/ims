package com.clifton.investment.instrument.calculator.accrual;


/**
 * The <code>AccrualMethods</code> enum defines available accrual calculation methods. Event thought most investment instruments
 * use the same formula based on Payment Frequency and Day Count convention, some Swaps use fairly unique and more complex calculations.
 *
 * @author vgomelsky
 */
public enum AccrualMethods {

	/**
	 * Default accrual convention based on "Coupon Frequency" and "Day Count" custom fields. It's used by most instruments including all bonds and most swaps.
	 */
	DAYCOUNT(true, true),

	/**
	 * Similar to DAYCOUNT but support multiple interest rate changes during a single payment period. Accrued interest from the first period
	 * is compounded during the second period (added to the notional), etc.  InvestmentSecurityEventDetail are used to specify rate changes.
	 */
	DAYCOUNT_COMPOUNDED(false, true),

	/**
	 * Daily compounding interest is calculated on the initial notional and also on the accumulated interest from prior interest rates.
	 * Supports interest rate changes during a single payment period.
	 * <p>
	 * Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(POWER(1 + Ri, 1/DayCountDenominator)) - 1)
	 * <p>
	 * NOTE: Uses [Coupon Frequency] and [Day Count] custom fields for Accrual Event Type and [Coupon Frequency 2] and [Day Count 2] custom fields for Accrual Event Type2.
	 */
	DAILY_COMPOUNDING(false, false),

	/**
	 * Accrual amount is calculated on the sum of daily Ri * Notional. This is a non-compounding, simple interest calculation.
	 * Supports interest rate changes during a single payment period.
	 * <p>
	 * Accrual Amount = Sum (Notional * AdjustedDailyRate) where AdjustedDailyRate = Ri / DayCountDenominator  (DayCountDenominator is the configured number of days per year to use).
	 * <p>
	 * NOTE: Uses [Coupon Frequency] and [Day Count] custom fields for Accrual Event Type and [Coupon Frequency 2] and [Day Count 2] custom fields for Accrual Event Type2.
	 */
	DAILY_NON_COMPOUNDING(false, false),

	/**
	 * Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(1 + Ri/360) - 1)
	 * Where Ri is Reference Rate on day i and d is the total number of days in current period
	 * <p/>
	 * OIS == Overnight Index Swap; Reference Rate could be daily FED FUND rate.
	 */
	DAILY_OIS_COMPOUND(false, false),

	/**
	 * Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(POWER(1 - Ri*91/360, -1/91)) - 1)
	 * Where Ri is Reference rate for PREVIOUS day
	 * <p/>
	 * This calculator is used for USD T-BILL Auction High Rate (91 because it's a 3 months T-bill?)
	 */
	DAILY_HIGH_RATE(false, false),

	/**
	 * Accrual for period is calculated as an aggregation of daily accruals calculated using the compounded average between dates
	 * utilizing a compounded interest rate index with daily rates.
	 *
	 * Daily average rate between X and Y = (IndexRateY/IndexRateX - 1) * (DayCountDenominator/Dc);
	 * X = start date of period, Y = end date of period, Dc = calendar days in period
	 *
	 * Daily accrual uses the average rate for the day in the period = Notional * DailyAverageRate * (DayCount/DayCountDenominator)
	 */
	DAILY_COMPOUNDED_AVERAGE(false, false),

	/**
	 * Accrual Amount = Notional * (LATEST EVENT DETAIL EFFECTIVE RATE)
	 * Looks up accrual detail with Effective Date closest to Accrue Up To Date and multiplies detail's Effective Rate by the Notional (or Quantity).
	 */
	LATEST_DETAIL_RATE(false, false);

	// UK_OLD hierarchy could have a calculator here too???

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	AccrualMethods(boolean constantRate, boolean sameCalculatorForSpread) {
		this.constantRate = constantRate;
		this.sameCalculatorForSpread = sameCalculatorForSpread;
	}


	/**
	 * Specifies whether the same rate applies to the period (TRUE) or if there could be multiple sub-periods with different rates/spreads.
	 */
	private final boolean constantRate;

	/**
	 * For securities that have effective accrual rate based on Spread + Reference Rate (most Total Return Swaps), specifies whether the
	 * same calculation is used for the spread as for the reference rate. If not, default DAYCOUNT calculator will be used for the spread
	 * while this calculator is used for the Reference Rate.
	 */
	private final boolean sameCalculatorForSpread;


	public boolean isConstantRate() {
		return this.constantRate;
	}


	public boolean isSameCalculatorForSpread() {
		return this.sameCalculatorForSpread;
	}
}
