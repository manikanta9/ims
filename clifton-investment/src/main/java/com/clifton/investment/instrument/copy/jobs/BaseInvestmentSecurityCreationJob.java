package com.clifton.investment.instrument.copy.jobs;

import com.clifton.calendar.calculator.CalendarCalculationResult;
import com.clifton.calendar.calculator.CalendarListCalculator;
import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.copy.InvestmentInstrumentCopyService;

import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author mitchellf
 */
public abstract class BaseInvestmentSecurityCreationJob implements Task, ValidationAware {


	public static final String EXISTING_SECURITIES = "existingSecurities";
	public static final String NEW_SECURITIES = "newSecurities";
	public static final String NO_TEMPLATE = "noTemplate";

	private List<Integer> instrumentIds;
	private Integer maturityScheduleId;
	private Integer calendarCalculatorId;
	private Integer evaluationDayAdjustment;
	private Date evaluationDateOverride;
	private Integer numberOfOccurrences;
	private List<String> fieldOverrides;

	private ScheduleApiService scheduleApiService;
	private InvestmentInstrumentService investmentInstrumentService;
	private SystemBeanService systemBeanService;
	private InvestmentInstrumentCopyService investmentInstrumentCopyService;


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getInstrumentIds(), "An instrument ID must be provided.");
		ValidationUtils.assertNotNull(getMaturityScheduleId(), "A schedule to find next maturity date must be provided.");

		StringBuilder errorMessage = new StringBuilder();

		for (Integer id : getInstrumentIds()) {
			InvestmentInstrument instrument = getInvestmentInstrumentService().getInvestmentInstrument(id);
			if (instrument.getHierarchy().isOneSecurityPerInstrument()) {
				errorMessage.append("Error: Instrument: [")
						.append(instrument)
						.append("] is a one-to-one instrument, and can not be used.");
			}
		}

		ValidationUtils.assertEquals(errorMessage.length(), 0, errorMessage.toString());

		if (!CollectionUtils.isEmpty(getFieldOverrides())) {
			getFieldOverrides().forEach(str -> {

				String[] pair = StringUtils.split(str, ",");

				if (pair.length != 2 || pair[0] == null || pair[1] == null) {
					throw new ValidationException("Improperly specified Security field overrides.  Overrides should be a :: delimited list of key,value pairs, eg. key1,val1::key2,val2::key3,val3");
				}
				ValidationUtils.assertTrue(BeanUtils.isPropertyPresent(InvestmentSecurity.class, pair[0]), "Property: [" + pair[0] + "] is not valid for InvestmentSecurities.");
			});
		}
	}


	@Override
	public Status run(Map<String, Object> context) {

		Status result = new Status();

		result.setStatusMap(new HashMap<>());
		result.getStatusMap().put(EXISTING_SECURITIES, 0);
		result.getStatusMap().put(NEW_SECURITIES, 0);
		result.getStatusMap().put(NO_TEMPLATE, 0);

		Date evalDate = getEvaluationDateOverride() != null ? getEvaluationDateOverride() : new Date();
		if (getEvaluationDayAdjustment() != null) {
			evalDate = DateUtils.addDays(evalDate, getEvaluationDayAdjustment());
		}

		Schedule maturitySchedule = getScheduleApiService().getSchedule(getMaturityScheduleId());

		List<Short> calendarIdsToUse = getCalendarOverrides(maturitySchedule, evalDate);

		ScheduleOccurrenceCommand command = ScheduleOccurrenceCommand.forOccurrences(maturitySchedule.getId(), (getNumberOfOccurrences() == null ? 1 : getNumberOfOccurrences()), evalDate);

		if (!CollectionUtils.isEmpty(calendarIdsToUse)) {
			command.setCalendarIdList(calendarIdsToUse);
		}

		List<Date> nextMaturityDates = getScheduleApiService().getScheduleOccurrences(command);

		if (!CollectionUtils.isEmpty(nextMaturityDates)) {
			for (Integer id : getInstrumentIds()) {
				InvestmentInstrument instrument = getInvestmentInstrumentService().getInvestmentInstrument(id);
				processInstrument(instrument, nextMaturityDates, result, calendarIdsToUse);
			}
		}

		result.setMessage("Processed " + getInstrumentIds().size() + " instruments and " + (getNumberOfOccurrences() != null ? getNumberOfOccurrences() : 1) + " maturity dates/securities per instrument."
				+ "\nFound " + result.getStatusMap().get(EXISTING_SECURITIES) + " existing securities for the specified maturity date(s)."
				+ "\nCreated " + result.getStatusMap().get(NEW_SECURITIES) + " new securities from templates for the calculated maturity date(s)."
				+ "\nThere were " + result.getStatusMap().get(NO_TEMPLATE) + " instruments where no existing security could be found to use as a template.");

		return result;
	}


	@Transactional
	protected void processInstrument(InvestmentInstrument instrument, List<Date> nextMaturityDates, Status result, List<Short> calendarOverrides) {

		InvestmentSecurity templateSecurity = lookupCurrentSecurityForInstrument(instrument);

		for (Date nextMaturityDate : nextMaturityDates) {
			processForDate(instrument, nextMaturityDate, templateSecurity, result);
		}
	}


	private InvestmentSecurity lookupCurrentSecurityForInstrument(InvestmentInstrument instrument) {
		// get existing security for that instrument, get most recent by end date
		SecuritySearchForm sfByInstrument = new SecuritySearchForm();
		sfByInstrument.setInstrumentId(instrument.getId());
		sfByInstrument.setOrderBy("endDate:desc");
		sfByInstrument.setLimit(1);
		return CollectionUtils.getOnlyElement(getInvestmentInstrumentService().getInvestmentSecurityList(sfByInstrument));
	}


	protected void processForDate(InvestmentInstrument instrument, Date nextMaturityDate, InvestmentSecurity templateSecurity, Status status) {
		if (templateSecurity == null) {
			status.addWarning("\nCould not find any securities to use as a template for instrument: [" + instrument.getLabel() + "].");
			status.getStatusMap().put(NO_TEMPLATE, (Integer) status.getStatusMap().get(NO_TEMPLATE) + 1);
			return;
		}

		if (isSecurityPresentForDate(instrument, nextMaturityDate)) {
			status.getStatusMap().put(EXISTING_SECURITIES, (Integer) status.getStatusMap().get(EXISTING_SECURITIES) + 1);
		}
		else {
			try {
				InvestmentSecurity newSecurity = createSecurityForDate(instrument, nextMaturityDate, templateSecurity); // might want a try/catch around this for status tracking
				if (newSecurity != null) {
					applyOverridesToNewSecurity(newSecurity);
					status.getStatusMap().put(NEW_SECURITIES, (Integer) status.getStatusMap().get(NEW_SECURITIES) + 1);
				}
				else {
					status.addError("Did not create security for [" + instrument + "] on " + nextMaturityDate + ". ");
				}
			}
			catch (Exception e) {
				status.addError("Found unexpected Exception when trying to create security for [" + instrument + "] on " + nextMaturityDate + ".  Exception was: " + e.getClass() + " : " + e.getMessage());
			}
		}
	}


	protected abstract boolean isSecurityPresentForDate(InvestmentInstrument instrument, Date nextMaturityDate);


	protected abstract InvestmentSecurity createSecurityForDate(InvestmentInstrument instrument, Date nextMaturityDate, InvestmentSecurity templateSecurity);


	protected InvestmentSecurity applyOverridesToNewSecurity(InvestmentSecurity newSecurity) {
		boolean saveNeeded = false;

		if (!CollectionUtils.isEmpty(getFieldOverrides())) {
			for (String str : CollectionUtils.getIterable(getFieldOverrides())) {
				String[] pair = StringUtils.split(str, ",");

				String key = pair[0];
				String val = pair[1];

				if (!CompareUtils.isEqual(val, String.valueOf(BeanUtils.getPropertyValue(newSecurity, key)))) {
					BeanUtils.setPropertyValue(newSecurity, key, val);
					saveNeeded = true;
				}
			}
		}

		return saveNeeded ? getInvestmentInstrumentService().saveInvestmentSecurity(newSecurity) : newSecurity;
	}


	protected List<Short> getCalendarOverrides(Schedule maturitySchedule, Date evalDate) {
		if (maturitySchedule.getBusinessDayConvention() != null && getCalendarCalculatorId() != null) {
			CalendarListCalculator calendarListCalculator = (CalendarListCalculator) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(getCalendarCalculatorId()));
			CalendarCalculationResult calendarCalculationResult = calendarListCalculator.calculate(evalDate);
			if (!CollectionUtils.isEmpty(calendarCalculationResult.getCalendarList())) {
				List<Calendar> calendars = calendarCalculationResult.getCalendarList();
				return calendars.stream()
						.map(Calendar::getId)
						.collect(Collectors.toList());
			}
		}
		return Collections.emptyList();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<Integer> getInstrumentIds() {
		return this.instrumentIds;
	}


	public void setInstrumentIds(List<Integer> instrumentIds) {
		this.instrumentIds = instrumentIds;
	}


	public Integer getMaturityScheduleId() {
		return this.maturityScheduleId;
	}


	public void setMaturityScheduleId(Integer maturityScheduleId) {
		this.maturityScheduleId = maturityScheduleId;
	}


	public Integer getCalendarCalculatorId() {
		return this.calendarCalculatorId;
	}


	public void setCalendarCalculatorId(Integer calendarCalculatorId) {
		this.calendarCalculatorId = calendarCalculatorId;
	}


	public Integer getEvaluationDayAdjustment() {
		return this.evaluationDayAdjustment;
	}


	public void setEvaluationDayAdjustment(Integer evaluationDayAdjustment) {
		this.evaluationDayAdjustment = evaluationDayAdjustment;
	}


	public Date getEvaluationDateOverride() {
		return this.evaluationDateOverride;
	}


	public void setEvaluationDateOverride(Date evaluationDateOverride) {
		this.evaluationDateOverride = evaluationDateOverride;
	}


	public Integer getNumberOfOccurrences() {
		return this.numberOfOccurrences;
	}


	public void setNumberOfOccurrences(Integer numberOfOccurrences) {
		this.numberOfOccurrences = numberOfOccurrences;
	}


	public List<String> getFieldOverrides() {
		return this.fieldOverrides;
	}


	public void setFieldOverrides(List<String> fieldOverrides) {
		this.fieldOverrides = fieldOverrides;
	}


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentInstrumentCopyService getInvestmentInstrumentCopyService() {
		return this.investmentInstrumentCopyService;
	}


	public void setInvestmentInstrumentCopyService(InvestmentInstrumentCopyService investmentInstrumentCopyService) {
		this.investmentInstrumentCopyService = investmentInstrumentCopyService;
	}
}
