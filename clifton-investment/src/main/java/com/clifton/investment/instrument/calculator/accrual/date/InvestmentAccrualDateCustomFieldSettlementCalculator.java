package com.clifton.investment.instrument.calculator.accrual.date;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.util.Date;


/**
 * The <code>InvestmentAccrualDateCustomFieldSettlementCalculator</code> class is InvestmentAccrualDateCalculator that
 * calculates the settlement date for the specified transaction date based on calendar and days to settle
 * custom fields of security.
 *
 * @author vgomelsky
 */
public class InvestmentAccrualDateCustomFieldSettlementCalculator implements InvestmentAccrualDateCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;
	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private String daysToSettleFieldName;
	private String calendarFieldName;
	/**
	 * Throw a corresponding ValidationException if one of the fields doesn't have a value set.
	 * If False, then return transactionDate.
	 */
	private boolean errorIfFieldValueMissing;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date calculateAccrualDate(InvestmentSecurity security, Date transactionDate) {
		String daysToSettleField = getDaysToSettleFieldName();
		ValidationUtils.assertNotNull(daysToSettleField, "Required property is not defined: daysToSettleFieldName");
		Integer daysToSettle = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, daysToSettleField, false);
		if (daysToSettle == null) {
			if (isErrorIfFieldValueMissing()) {
				throw new ValidationException(daysToSettleField + " property value is not defined for " + security.getLabel());
			}
			return transactionDate;
		}

		String calendarField = getCalendarFieldName();
		if (calendarField == null) {
			if (isErrorIfFieldValueMissing()) {
				throw new ValidationException("'Calendar Field Name' is not defined for " + security.getLabel());
			}
			return transactionDate;
		}

		Integer calendarId = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, calendarField, false);
		if (calendarId == null) {
			if (isErrorIfFieldValueMissing()) {
				throw new ValidationException(calendarField + " property value is not defined for " + security.getLabel());
			}
			return transactionDate;
		}
		return getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(transactionDate, calendarId.shortValue()), daysToSettle);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public String getDaysToSettleFieldName() {
		return this.daysToSettleFieldName;
	}


	public void setDaysToSettleFieldName(String daysToSettleFieldName) {
		this.daysToSettleFieldName = daysToSettleFieldName;
	}


	public String getCalendarFieldName() {
		return this.calendarFieldName;
	}


	public void setCalendarFieldName(String calendarFieldName) {
		this.calendarFieldName = calendarFieldName;
	}


	public boolean isErrorIfFieldValueMissing() {
		return this.errorIfFieldValueMissing;
	}


	public void setErrorIfFieldValueMissing(boolean errorIfFieldValueMissing) {
		this.errorIfFieldValueMissing = errorIfFieldValueMissing;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
