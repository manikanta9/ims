package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * There could be up to 2 accruals for a security and each leg may use a different accrual basis: notional for interest leg of TRS and quantity for dividend leg.
 *
 * @author vgomelsky
 */
public class AccrualBasis {

	/**
	 * Accrual Basis for Event 1.
	 */
	private BigDecimal accrualBasis1;
	/**
	 * Accrual Basis for Event 2.
	 */
	private BigDecimal accrualBasis2;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "{Accrual1 = " + this.accrualBasis1 + "; Accrual2 = " + this.accrualBasis2 + "}";
	}


	public static AccrualBasis of(BigDecimal accrualBasis) {
		return AccrualBasis.of(accrualBasis, accrualBasis);
	}


	public static AccrualBasis of(BigDecimal accrualBasis1, BigDecimal accrualBasis2) {
		AccrualBasis result = new AccrualBasis();
		result.accrualBasis1 = accrualBasis1;
		result.accrualBasis2 = accrualBasis2;
		return result;
	}


	public static AccrualBasis forHierarchy(InvestmentInstrumentHierarchy hierarchy, BigDecimal notional, BigDecimal quantity) {
		AccrualBasis result = new AccrualBasis();

		if (hierarchy.getAccrualSecurityEventType() != null) {
			result.accrualBasis1 = hierarchy.isAccrualBasedOnNotional() ? notional : quantity;
		}
		if (hierarchy.getAccrualSecurityEventType2() != null) {
			result.accrualBasis2 = hierarchy.isAccrual2BasedOnNotional() ? notional : quantity;
		}

		return result;
	}


	public static AccrualBasis forSecurity(InvestmentSecurity security, BigDecimal notional, BigDecimal quantity) {
		return AccrualBasis.forHierarchy(security.getInstrument().getHierarchy(), notional, quantity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getAccrualBasis1() {
		return this.accrualBasis1;
	}


	public BigDecimal getAccrualBasis2() {
		return this.accrualBasis2;
	}


	public boolean isAccrualBasisNotZero() {
		return !MathUtils.isNullOrZero(getAccrualBasis1()) || !MathUtils.isNullOrZero(getAccrualBasis2());
	}
}
