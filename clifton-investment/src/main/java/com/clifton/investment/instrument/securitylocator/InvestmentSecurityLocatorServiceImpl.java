package com.clifton.investment.instrument.securitylocator;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InvestmentSecurityLocatorServiceImpl implements InvestmentSecurityLocatorService {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityLocatorFactory investmentSecurityLocatorFactory;


	@Override
	public InvestmentSecurity getInvestmentSecurityByFullSymbol(String symbol) {
		// check if the symbol has exchange code in it: if yes, try to match on clean symbol and exchange code
		// For example, "BEZ LN" will be matched to "BEZ" security on "London Stock Exchange" with code "LN"
		int index = symbol.lastIndexOf(' ');
		if (index != -1) {
			String cleanSymbol = symbol.substring(0, index);
			String exchangeCode = symbol.substring(index + 1);

			List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityListBySymbol(cleanSymbol);
			securityList = BeanUtils.filter(securityList, security -> security.belongsToPrimaryOrCompositeExchange(exchangeCode));
			int listSize = CollectionUtils.getSize(securityList);
			if (listSize == 1) {
				return securityList.get(0);
			}
			else if (listSize > 1) {
				// if there are multiple securities that use the exchange as primary or composite, then try only primary
				securityList = BeanUtils.filter(securityList, security -> security.belongsToPrimaryExchange(exchangeCode));
				if (CollectionUtils.getSize(securityList) == 1) {
					return securityList.get(0);
				}
			}
		}

		// still not found: try registered locators
		// For example, Bloomberg locator can map the following 2 symbols to AAPL: "AAPL UW Equity", "AAPL Equity"
		for (InvestmentSecurityLocator locator : CollectionUtils.getIterable(getInvestmentSecurityLocatorFactory().getInvestmentSecurityLocators())) {
			InvestmentSecurity security = locator.getInvestmentSecurity(symbol);
			if (security != null) {
				return security;
			}
		}

		return null;
	}

	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityLocatorFactory getInvestmentSecurityLocatorFactory() {
		return this.investmentSecurityLocatorFactory;
	}


	public void setInvestmentSecurityLocatorFactory(InvestmentSecurityLocatorFactory investmentSecurityLocatorFactory) {
		this.investmentSecurityLocatorFactory = investmentSecurityLocatorFactory;
	}
}
