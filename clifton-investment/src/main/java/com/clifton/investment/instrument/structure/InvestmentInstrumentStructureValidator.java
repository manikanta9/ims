package com.clifton.investment.instrument.structure;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentInstrumentStructureValidator</code> validates structures for an instrument cannot overlap
 *
 * @author manderson
 */
@Component
public class InvestmentInstrumentStructureValidator extends SelfRegisteringDaoValidator<InvestmentInstrumentStructure> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(InvestmentInstrumentStructure bean, DaoEventTypes config) throws ValidationException {
		// uses dao method
	}


	@Override
	public void validate(InvestmentInstrumentStructure bean, DaoEventTypes config, ReadOnlyDAO<InvestmentInstrumentStructure> dao) throws ValidationException {
		InvestmentInstrumentStructure originalBean = null;
		if (config.isUpdate()) {
			originalBean = getOriginalBean(bean);
		}
		if (originalBean == null || DateUtils.compare(bean.getStartDate(), originalBean.getStartDate(), false) != 0 || DateUtils.compare(bean.getEndDate(), originalBean.getEndDate(), false) != 0) {
			List<InvestmentInstrumentStructure> instrumentStructureList = dao.findByField("instrument.id", bean.getInstrument().getId());
			for (InvestmentInstrumentStructure existing : CollectionUtils.getIterable(instrumentStructureList)) {
				if (originalBean == null || !MathUtils.isEqual(originalBean.getId(), existing.getId())) {
					if (DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), existing.getStartDate(), existing.getEndDate())) {
						throw new ValidationException("Instrument Structure dates cannot overlap with another structure for the same instrument: " + existing.getLabel()
								+ ". Please enter a unique date range.");
					}
				}
			}
		}
	}
}
