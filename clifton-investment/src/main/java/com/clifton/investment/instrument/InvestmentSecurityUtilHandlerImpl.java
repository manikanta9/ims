package com.clifton.investment.instrument;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.NoRelatedAccountFoundException;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityEntryProperty;
import com.clifton.investment.specificity.InvestmentSpecificityService;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntrySearchForm;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author mwacker
 */
@Component
public class InvestmentSecurityUtilHandlerImpl implements InvestmentSecurityUtilHandler {

	private BusinessCompanyService businessCompanyService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentSpecificityService investmentSpecificityService;
	private SystemColumnValueHandler systemColumnValueHandler;
	private InvestmentSecurityGroupService investmentSecurityGroupService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getCashLocationHoldingAccount(InvestmentTransactionInfo transactionInfo) {

		InvestmentAccount result = transactionInfo.getHoldingInvestmentAccount();
		InvestmentInstrumentHierarchy hierarchy = transactionInfo.getInvestmentSecurity().getInstrument().getHierarchy();
		InvestmentAccountRelationshipPurpose cashLocationPurpose = hierarchy.getCashLocationPurpose();
		if (cashLocationPurpose != null) {
			ValidationUtils.assertNotNull(transactionInfo.getHoldingInvestmentAccount(), "Holding Investment Account is required in order to lookup Cash Location Purpose account.", "holdingInvestmentAccount.id");
			ValidationUtils.assertNotNull(transactionInfo.getTransactionDate(), "Transaction Date is required in order to lookup the active Cash Location Purpose account.", "transactionDate");
			try {
				result = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurposeStrict(transactionInfo.getClientInvestmentAccount().getId(),
						transactionInfo.getHoldingInvestmentAccount().getId(), transactionInfo.getInvestmentSecurity().getId(), cashLocationPurpose.getName(), transactionInfo.getTransactionDate());
			}
			catch (NoRelatedAccountFoundException e) {
				if (hierarchy.getCashLocationPurpose2() == null) {
					throw e;
				}
				else {
					// try to lookup related account using the second purpose
					result = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurposeStrict(transactionInfo.getClientInvestmentAccount().getId(),
							transactionInfo.getHoldingInvestmentAccount().getId(), transactionInfo.getInvestmentSecurity().getId(), hierarchy.getCashLocationPurpose2().getName(), transactionInfo.getTransactionDate());
				}
			}

			if (result.getType().isOurAccount()) {
				throw new ValidationException("'" + cashLocationPurpose.getName() + "' account " + result.getLabel() + " for " + transactionInfo.getClientInvestmentAccount().getLabel() + " must be a HOLDING account.");
			}
		}
		return result;
	}


	@Override
	public ISITCCodes getISITCCodeStrict(InvestmentInstrument investmentInstrument) {
		if (Objects.nonNull(investmentInstrument)) {
			InvestmentSpecificityEntrySearchForm searchForm = new InvestmentSpecificityEntrySearchForm();
			searchForm.setAppliesToInstrumentId(investmentInstrument.getId());
			searchForm.setDefinition(InvestmentSpecificityService.SPECIFICITY_GENERIC_MAPPING_DEFINITION);
			searchForm.setField(InvestmentSpecificityService.SPECIFICITY_ISITC_CODE_FIELD);
			List<InvestmentSpecificityEntry> investmentSpecificityEntryList = getInvestmentSpecificityService().getInvestmentSpecificityEntryList(searchForm);

			List<ISITCCodes> codeList = CollectionUtils.getStream(investmentSpecificityEntryList)
					.flatMap(e -> CollectionUtils.getStream(getInvestmentSpecificityService().getInvestmentSpecificityEntryPropertiesForEntry(e)))
					.filter(p -> Objects.equals(InvestmentSpecificityService.SPECIFICITY_FIELD_VALUE, p.getType().getName()))
					.map(InvestmentSpecificityEntryProperty::getValue)
					.map(ISITCCodes::valueOf)
					.distinct()
					.collect(Collectors.toList());
			ValidationUtils.assertFalse(codeList.size() > 1, () -> String.format("The Investment Instrument hierarchy [%s] matches more than one ISITC Code [%s]",
					buildHierarchyString(investmentInstrument.getHierarchy()), codeList.toString()));
			return codeList.isEmpty() ? ISITCCodes.NONE : CollectionUtils.getFirstElement(codeList);
		}
		return ISITCCodes.NONE;
	}


	@Override
	public String getOptionCFICode(InvestmentSecurity option) {
		ValidationUtils.assertTrue(option != null && option.getOptionType() != null && InvestmentUtils.isSecurityOfType(option, InvestmentType.OPTIONS),
				"Cannot build an Option CFI Code for security [" + (option != null ? option.getLabel() : "") + "].");

		StringBuilder code = new StringBuilder();
		code.append("O"); // O for options
		code.append(option.getOptionType().toString().substring(0, 1).toUpperCase()); // C (CALL) or P (PUT)
		code.append(option.getOptionStyle() != null ? option.getOptionStyle().toString().substring(0, 1).toUpperCase() : "X"); // Scheme A (American), E (European) and X (Unknown)

		// Could to be based on the underlying asset type
		code.append("X");  // Underlying Asset, always use 'X' for Unknown

		// add the Delivery code
		if (option.getInstrument().getHierarchy().getInstrumentDeliverableType() != null) {
			switch (option.getInstrument().getHierarchy().getInstrumentDeliverableType()) {
				case ALWAYS:
					code.append("P"); // Physical
					break;
				case NEVER:
					code.append("C"); // Cash
					break;
				case BOTH:
					code.append("X");
					break;
			}
		}
		else {
			code.append("X");
		}

		if (InvestmentUtils.isSecurityOfTypeSubType2(option, InvestmentTypeSubType2.OPTIONS_LISTED)) {
			code.append("S");  // for standard terms
		}
		else if (InvestmentUtils.isSecurityOfTypeSubType2(option, InvestmentTypeSubType2.OPTIONS_FLEX)) {
			code.append("N");  // for non-standard terms
		}
		else {
			code.append("X");  // for unknown terms
		}

		return code.toString();
	}


	/**
	 * Returns the CFI code for the given security.  This is currently only used with SocGen broker that requires the CFI codes be sent in allocation messages.
	 * This supports the basic instruments we will be trading with SocGen. Due to limited development time, a full mapping is not possible at this time.
	 * TODO: build this out with additional CFI codes if needed.
	 *
	 * @param security
	 * @return A string representing the CFI code.  If mapping for the investment not is not currently supported, will return null.
	 */
	@Override
	public String getCFICode(InvestmentSecurity security) {
		ValidationUtils.assertNotNull(security, "The security passed to getCFICode() must be defined (not null)");
		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.OPTIONS)) {
			return getOptionCFICode(security);
		}

		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.FUTURES)) {
			return CFI_FUTURES;
		}

		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.STOCKS)) {
			return CFI_EQUITIES;
		}

		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.FUNDS)) {
			return CFI_FUNDS;
		}

		return null;
	}


	@Override
	public String getPlaceOfSettlementStrict(InvestmentSecurity investmentSecurity) {
		AssertUtils.assertNotNull(investmentSecurity, "Investment Security is required.");

		InvestmentSpecificityEntrySearchForm searchForm = new InvestmentSpecificityEntrySearchForm();
		searchForm.setAppliesToSecurityId(investmentSecurity.getId());
		searchForm.setDefinition(InvestmentSpecificityService.SPECIFICITY_INSTRUMENT_TO_COMPANY_DEFINITION);
		searchForm.setField(InvestmentSpecificityService.SPECIFICITY_SETTLEMENT_COMPANY_FIELD);
		List<InvestmentSpecificityEntry> specificityEntries = getInvestmentSpecificityService().getInvestmentSpecificityEntryList(searchForm);

		List<String> bicList = CollectionUtils.asNonNullList(specificityEntries).stream()
				.flatMap(e -> CollectionUtils.buildStream(e.getPropertyList()))
				.filter(p -> InvestmentSpecificityService.SPECIFICITY_BUSINESS_COMPANY_PROPERTY_TYPE.equals(p.getType().getName()))
				.map(InvestmentSpecificityEntryProperty::getValue)
				.map(StringUtils::trim)
				.filter(s -> !StringUtils.isEmpty(s))
				.filter(MathUtils::isNumber)
				.map(Integer::parseInt)
				.map(i -> getBusinessCompanyService().getBusinessCompany(i))
				.map(BusinessCompany::getBusinessIdentifierCode)
				.filter(i -> !StringUtils.isEmpty(i))
				.distinct()
				.collect(Collectors.toList());
		ValidationUtils.assertFalse(bicList.size() > 1, () -> String.format("The Investment Instrument hierarchy [%s] matches more than one Place of Settlement BIC [%s]",
				buildHierarchyString(investmentSecurity.getInstrument().getHierarchy()), bicList.toString()));
		return bicList.stream().findFirst().orElse(null);
	}


	@Override
	public BigDecimal getInterestRate(InvestmentSecurity security) {
		return Optional.ofNullable(getCustomColumnValue(security, InvestmentSecurity.CUSTOM_FIELD_COUPON_AMOUNT_PERCENT)).map(BigDecimal.class::cast).orElse(BigDecimal.ZERO);
	}


	@Override
	public String getCouponType(InvestmentSecurity security) {
		return Optional.ofNullable(getCustomColumnValue(security, InvestmentSecurity.CUSTOM_FIELD_COUPON_TYPE)).map(String.class::cast).orElse(null);
	}


	@Override
	public Object getCustomColumnValue(InvestmentSecurity security, String columnName) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, columnName, true);
	}


	@Override
	public String getBticPrefix(InvestmentSecurity security) {
		Object bticPrefix = getSystemColumnValueHandler().getSystemColumnValueForEntity(security.getInstrument(), InvestmentInstrument.INSTRUMENT_CUSTOM_FIELDS_GROUP_NAME, InvestmentInstrument.INSTRUMENT_BTIC_PREFIX_CUSTOM_FIELD, false);
		return bticPrefix == null ? null : bticPrefix.toString();
	}


	@Override
	public boolean isClsCurrency(InvestmentSecurity security) {
		if (security != null) {
			return getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(security.getId(), InvestmentSecurityGroup.CLS_SETTLEMENT_CURRENCY_SECURITY_GROUP);
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String buildHierarchyString(InvestmentInstrumentHierarchy hierarchy) {
		return String.join(", ",
				Optional.ofNullable(hierarchy).map(InvestmentInstrumentHierarchy::getInvestmentType).map(InvestmentType::toString).orElse(""),
				Optional.ofNullable(hierarchy).map(InvestmentInstrumentHierarchy::getInvestmentTypeSubType).map(InvestmentTypeSubType::toString).orElse(""),
				Optional.ofNullable(hierarchy).map(InvestmentInstrumentHierarchy::getInvestmentTypeSubType2).map(InvestmentTypeSubType2::toString).orElse("")
		);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentSpecificityService getInvestmentSpecificityService() {
		return this.investmentSpecificityService;
	}


	public void setInvestmentSpecificityService(InvestmentSpecificityService investmentSpecificityService) {
		this.investmentSpecificityService = investmentSpecificityService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}
}
