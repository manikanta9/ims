package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentTransactionInfo;

import java.util.Date;


/**
 * The AccrualDatesCommand class holds all required inputs necessary to calculated accrual start/end dates for both accrual events.
 * It also provides simple static constructors to create these objects.
 *
 * @author vgomelsky
 */
public class AccrualDatesCommand {

	/**
	 * Usually defaults to Valuation Date: convention used by most banks (as opposed to position Settlement Date calculated from Valuation Date)
	 */
	private Date accrueUpToDate;

	private Date positionOpenTransactionDate;
	/**
	 * Optional Settlement Date for position opening: cannot start accruing before this date unless DO_NOT_ADJUST AccrualDateCalculator is used.
	 */
	private Date positionOpenSettlementDate;

	private InvestmentSecurity security;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccrualDatesCommand of(InvestmentTransactionInfo openingLot, Date accrueUpToDate) {
		AccrualDatesCommand command = of(openingLot.getInvestmentSecurity(), openingLot.getTransactionDate(), openingLot.getSettlementDate(), accrueUpToDate);
		return command;
	}


	public static AccrualDatesCommand of(InvestmentSecurity security, Date transactionDate, Date settlementDate, Date accrueUpToDate) {
		AccrualDatesCommand command = of(security, accrueUpToDate);
		command.positionOpenTransactionDate = transactionDate;
		command.positionOpenSettlementDate = settlementDate;
		return command;
	}


	public static AccrualDatesCommand of(InvestmentSecurity security, Date accrueUpToDate) {
		AccrualDatesCommand command = new AccrualDatesCommand();
		command.security = security;
		command.accrueUpToDate = accrueUpToDate;
		return command;
	}


	private AccrualDatesCommand() {
		// use static constructors instead
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getAccrueUpToDate() {
		return this.accrueUpToDate;
	}


	public Date getPositionOpenTransactionDate() {
		return this.positionOpenTransactionDate;
	}


	public Date getPositionOpenSettlementDate() {
		return this.positionOpenSettlementDate;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}
}
