package com.clifton.investment.instrument.event.sequence;

import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.setup.Calendar;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;

import java.util.Date;


/**
 * @author mwacker
 */
public interface InvestmentEventSequenceRetrieverUtilHandler {

	/**
	 * Returns a search form that can be used to find duplicate security events before creating a new event.
	 */
	public InvestmentSecurityEventSearchForm getInvestmentSecurityEventDuplicateSearchForm(InvestmentSecurityEvent event);


	/**
	 * Create a CalendarSchedule and use it to calculate the the Fixing Date for the given eventDate.
	 */
	public Date getFixingDate(Calendar fixingCalendar, BusinessDayConventions fixingBusinessDayConvention, int fixingCycle, Date eventDate);
}
