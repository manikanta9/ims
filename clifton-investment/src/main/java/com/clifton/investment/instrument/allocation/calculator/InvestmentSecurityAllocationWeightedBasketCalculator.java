package com.clifton.investment.instrument.allocation.calculator;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentSecurityAllocationWeightedBasketCalculator</code> is used to validate that allocation weights are entered
 * Since there are no system calculations for market data, there is no market data implementation of this calculator
 *
 * @author manderson
 */
@Component
public class InvestmentSecurityAllocationWeightedBasketCalculator implements InvestmentSecurityAllocationCalculator {


	@Override
	public InvestmentSecurityAllocationTypes getInvestmentSecurityAllocationType() {
		return InvestmentSecurityAllocationTypes.WEIGHTED_BASKET_NO_PRICE;
	}


	@Override
	public void validateInvestmentSecurityAllocation(InvestmentSecurityAllocation bean) {
		ValidationUtils.assertNotNull(bean.getAllocationWeight(), getInvestmentSecurityAllocationType().getWeightLabel() + " is required for each allocation.", "allocationWeight");
	}
}
