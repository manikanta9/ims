package com.clifton.investment.instrument.event.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.instrument.event.InvestmentSecurityEventTypeHierarchy;
import org.springframework.stereotype.Component;


/**
 * The InvestmentSecurityEventTypeHierarchyListByHierarchyCache class caches lists of {@link InvestmentSecurityEventTypeHierarchy} objects
 * for each {@link com.clifton.investment.setup.InvestmentInstrumentHierarchy}.
 *
 * @author vgomelsky
 */
@Component
public class InvestmentSecurityEventTypeHierarchyListByHierarchyCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentSecurityEventTypeHierarchy, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "referenceTwo.id";
	}


	@Override
	protected Short getBeanKeyValue(InvestmentSecurityEventTypeHierarchy bean) {
		if (bean.getReferenceTwo() != null) {
			return bean.getReferenceTwo().getId();
		}
		return null;
	}
}
