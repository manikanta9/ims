package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * There could be up to 2 accruals for a security and each leg may use a different accrue up to or from date logic:
 * Settlement Date for Interest Leg and Transaction Date for Dividend Leg.
 * <p>
 * We follow industry convention and include accrual for the Up To Date.
 *
 * @author vgomelsky
 */
public class AccrualDates {

	/**
	 * Accrue From Date for Event 1.
	 */
	private Date accrueFromDate1;
	/**
	 * Accrue From Date for Event 2.
	 */
	private Date accrueFromDate2;

	/**
	 * Accrue Up To Date for Event 1.
	 */
	private Date accrueUpToDate1;
	/**
	 * Accrue Up To Date for Event 2.
	 */
	private Date accrueUpToDate2;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccrualDates of(Date accrueFromDate1, Date accrueFromDate2, Date accrueUpToDate1, Date accrueUpToDate2) {
		AccrualDates result = new AccrualDates();
		result.accrueFromDate1 = accrueFromDate1;
		result.accrueFromDate2 = accrueFromDate2;
		result.accrueUpToDate1 = accrueUpToDate1;
		result.accrueUpToDate2 = accrueUpToDate2;
		return result;
	}


	/**
	 * Specifies only Up To Dates for both events. Sets both From dates to null (period start).
	 */
	public static AccrualDates ofUpToDates(Date accrueUpToDate1, Date accrueUpToDate2) {
		return of(null, null, accrueUpToDate1, accrueUpToDate2);
	}


	/**
	 * Specifies single Up To Date that should be used for both events. Sets both From dates to null (period start).
	 */
	public static AccrualDates ofUpToDate(Date accrueUpToDate) {
		return ofUpToDates(accrueUpToDate, accrueUpToDate);
	}


	/**
	 * Specifies From and Up To dates.  The dates are the same for both events.
	 */
	public static AccrualDates ofSame1and2(Date accrueFromDate, Date accrueToDate) {
		return of(accrueFromDate, accrueFromDate, accrueToDate, accrueToDate);
	}


	private AccrualDates() {
		// use static constructors instead
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "{from1 = " + DateUtils.fromDate(this.accrueFromDate1) + "; from2 = " + DateUtils.fromDate(this.accrueFromDate2) + ", to1 = " + DateUtils.fromDate(this.accrueUpToDate1) + "; to2 = " + DateUtils.fromDate(this.accrueUpToDate2) + "}";
	}


	public Date getAccrueFromDate1() {
		return this.accrueFromDate1;
	}


	public Date getAccrueFromDate2() {
		return this.accrueFromDate2;
	}


	public Date getAccrueUpToDate1() {
		return this.accrueUpToDate1;
	}


	public Date getAccrueUpToDate2() {
		return this.accrueUpToDate2;
	}
}
