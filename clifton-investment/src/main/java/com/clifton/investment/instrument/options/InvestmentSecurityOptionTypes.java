package com.clifton.investment.instrument.options;

/**
 * An entity that represents an InvestmentSecurity Option's type.
 *
 * @author davidi
 */
public enum InvestmentSecurityOptionTypes {
	CALL,
	PUT
}
