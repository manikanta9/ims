package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.DayCountCommand;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.sign.AccrualSignCalculators;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;


/**
 * The <code>InvestmentAccrualDayCountCalculator</code> class implements InvestmentAccrualCalculator for the AccrualCalculator.DAYCOUNT type.
 *
 * @author vgomelsky
 */
public class InvestmentAccrualDayCountCalculator extends BaseInvestmentAccrualCalculator {


	@Override
	public BigDecimal doCalculateAccruedInterest(InvestmentSecurityEvent paymentEvent, BigDecimal notional, BigDecimal notionalMultiplier, Date accrueUpToDate) {
		// lookup security configuration necessary to calculate accrual
		AccrualConfig accrualConfig = getAccrualConfig(paymentEvent, false);

		// adjust accrual start date if this is a partial first period
		InvestmentSecurity security = paymentEvent.getSecurity();
		Date accrualStartDate = paymentEvent.getAccrualStartDate();
		if (security.getStartDate() != null && DateUtils.compare(paymentEvent.getAccrualStartDate(), security.getStartDate(), false) < 0) {
			if (!security.getInstrument().getHierarchy().isFullPaymentForFirstPartialPeriod()) {
				// excludes accrual on start date
				accrualStartDate = security.getStartDate();
			}
		}
		// zero or negative days of accrual means 0 accrued interest (IRS with accrual start shifted by 1 day)
		if (DateUtils.compare(accrueUpToDate, accrualStartDate, true) <= 0) {
			return BigDecimal.ZERO;
		}

		// adjust notional if necessary (multiply by Index Ratio for inflation linked securities)
		if (notionalMultiplier == null) {
			notionalMultiplier = BigDecimal.ONE;
		}
		BigDecimal adjustedNotional = MathUtils.multiply(notional, notionalMultiplier); // should we round to 2 ???

		BigDecimal result;
		DayCountCommand dayCountCommand = DayCountCommand.forAccrualRange(accrualStartDate, accrueUpToDate).withAccrualPeriod(paymentEvent.getAccrualStartDate(), paymentEvent.getAccrualEndDate()).withFrequency(accrualConfig.getFrequency());
		if (InvestmentUtils.isSecurityInHierarchy(security, "UK-Old")) {
			// custom calculation with 4 decimal rounding down for old British linkers
			// NOTE: should we refactor this into meta data (another precision field on hierarchy)???
			BigDecimal multiplier = paymentEvent.getAfterEventValue().multiply(notionalMultiplier).divide(new BigDecimal(accrualConfig.getFrequency()), 4, RoundingMode.DOWN);
			BigDecimal accrualRate = accrualConfig.calculateAccrualRate(dayCountCommand);
			result = InvestmentCalculatorUtils.getNotionalCalculator(security).round(
					multiplier.movePointLeft(2)
							.multiply(accrualRate)
							.multiply(new BigDecimal(accrualConfig.getFrequency()))
							.multiply(notional));
		}
		else {
			BigDecimal accrualRate = accrualConfig.calculateAccrualRate(dayCountCommand);
			result = InvestmentCalculatorUtils.getNotionalCalculator(security).round(
					paymentEvent.getAfterEventValue()
							.movePointLeft(2)
							.multiply(adjustedNotional)
							.multiply(accrualRate));
		}

		AccrualSignCalculators signCalculator = security.getInstrument().getHierarchy().getAccrualSignCalculator();
		if (signCalculator != null) {
			if (getInvestmentAccrualCalculatorFactory().getInvestmentAccrualSignCalculator(signCalculator).isAccrualAmountNegated(security, accrualConfig.getCouponFrequencyField())) {
				result = MathUtils.negate(result);
			}
		}

		// skip full payment events (as opposed to partial period accrual)
		if (!accrueUpToDate.equals(paymentEvent.getAccrualEndDate())) {
			if (DateUtils.compare(accrueUpToDate, paymentEvent.getExDate(), false) >= 0 && !security.getInstrument().getHierarchy().isAccrueAfterExDate()) {
				// British Linkers (Gilts) have the concept of Ex Date which is 8 days before period end date.
				// One is entitled to full dividend for a position at close of day before Ex Date.  From Ex Date to Accrual Period End Date, we're accruing negative interest.
				result = result.subtract(doCalculateAccruedInterest(paymentEvent, notional, notionalMultiplier, paymentEvent.getAccrualEndDate()));
			}
		}

		return result;
	}
}
