package com.clifton.investment.instrument.event.retriever;


import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;

import java.util.List;


/**
 * The <code>BaseInvestmentSecurityEventRetriever</code> class should be extended by most InvestmentSecurityEventRetriever
 * implementation and provides basic helper/common methods.
 *
 * @author vgomelsky
 */
public abstract class BaseInvestmentSecurityEventRetriever implements InvestmentSecurityEventRetriever {

	private InvestmentSecurityEventService investmentSecurityEventService;
	private InvestmentCalculator investmentCalculator;


	/**
	 * Strict implementation: before and after values must match in order to be excluded as a duplicate.
	 * This is in addition to security, eventType and eventDate.
	 */
	@Override
	public boolean prepareInvestmentSecurityEvent(InvestmentSecurityEvent event) {
		if (event == null) {
			return false; // skip
		}
		// check if this is a duplicate event
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(event.getSecurity().getId());
		searchForm.setTypeId(event.getType().getId());
		searchForm.setEventDate(event.getEventDate());
		if (!event.getType().isOnePerEventDate()) {
			searchForm.setAfterEventValue(event.getAfterEventValue());
			searchForm.setBeforeEventValue(event.getBeforeEventValue());
		}
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		if (!CollectionUtils.isEmpty(eventList)) {
			return false; // skip
		}

		return true; // good
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
