package com.clifton.investment.instrument.currency;

import java.util.List;


/**
 * The <code>InvestmentCurrencyConventionCache</code> class caches all currency conventions and provides fast
 * look ups for conventions between currencies.
 *
 * @author vgomelsky
 */
public interface InvestmentCurrencyConventionCache {

	public CurrencyConventionHolder getInvestmentCurrencyConvention(String fromSymbol, String toSymbol);


	public void updateInvestmentCurrencyConventionList(List<InvestmentCurrencyConvention> conventionList);
}
