package com.clifton.investment.instrument.structure.jobs;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureService;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureTypes;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>InvestmentSecurityStructureAllocationRebuildJob</code> rebuilds structured index security allocations for given date range
 * for those types (i.e. Model Portfolio) that save the values.
 *
 * @author manderson
 */
public class InvestmentSecurityStructureAllocationRebuildJob implements Task {

	private InvestmentInstrumentService investmentInstrumentService;

	private InvestmentInstrumentStructureService investmentInstrumentStructureService;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Use weekdays because each security could use a different calendar
	 */
	private int weekdaysBackFromToday;

	private boolean includeTodayInRebuild;

	/**
	 * Allow to either skip or rebuild days where there already exists saved values
	 */
	private boolean reprocessExisting;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Date endDate = DateUtils.getEndOfDay(new Date());
		Date startDate = DateUtils.clearTime(DateUtils.addWeekDays(endDate, getWeekdaysBackFromToday() * -1));
		if (!isIncludeTodayInRebuild()) {
			endDate = DateUtils.addWeekDays(endDate, -1);
		}

		SecuritySearchForm searchForm = new SecuritySearchForm();
		Set<InvestmentInstrumentStructureTypes> structureTypes = new HashSet<>();
		for (InvestmentInstrumentStructureTypes structureType : InvestmentInstrumentStructureTypes.values()) {
			if (structureType.isSaveDailyAllocationValues()) {
				structureTypes.add(structureType);
			}
		}
		if (CollectionUtils.isEmpty(structureTypes)) {
			return Status.ofMessage("There are no instrument structure types that save daily values.  Nothing to calculate", false);
		}
		searchForm.setInstrumentStructureTypes(structureTypes.toArray(new InvestmentInstrumentStructureTypes[structureTypes.size()]));
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
		if (CollectionUtils.isEmpty(securityList)) {
			return Status.ofMessage("There are no securities use structured indices that save daily values.  Nothing to calculate", false);
		}

		int successCount = 0;
		Status status = Status.ofEmptyMessage();
		for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
			if (DateUtils.isOverlapInDates(security.getStartDate(), security.getEndDate(), startDate, endDate)) {
				try {
					getInvestmentInstrumentStructureService().rebuildInvestmentSecurityStructureAllocation(null, security.getId(), startDate, endDate, isReprocessExisting());
					successCount++;
				}
				catch (Throwable e) {
					// Called method returns friendly message
					LogUtils.errorOrInfo(getClass(), e.getMessage(), e);
					status.addError(e.getMessage());
				}
			}
			else {
				status.addSkipped(security.getLabel());
			}
		}

		if (successCount > 0) {
			status.setMessage("Successfully processed " + successCount + " security's structured index allocations.");
		}
		else {
			status.setMessage((status.getErrorCount() == 0) ? "No active securities/structures to process.  Nothing to calculate." : "No security structure allocations processed.");
			status.setActionPerformed(false);
		}
		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentInstrumentStructureService getInvestmentInstrumentStructureService() {
		return this.investmentInstrumentStructureService;
	}


	public void setInvestmentInstrumentStructureService(InvestmentInstrumentStructureService investmentInstrumentStructureService) {
		this.investmentInstrumentStructureService = investmentInstrumentStructureService;
	}


	public int getWeekdaysBackFromToday() {
		return this.weekdaysBackFromToday;
	}


	public void setWeekdaysBackFromToday(int weekdaysBackFromToday) {
		this.weekdaysBackFromToday = weekdaysBackFromToday;
	}


	public boolean isIncludeTodayInRebuild() {
		return this.includeTodayInRebuild;
	}


	public void setIncludeTodayInRebuild(boolean includeTodayInRebuild) {
		this.includeTodayInRebuild = includeTodayInRebuild;
	}


	public boolean isReprocessExisting() {
		return this.reprocessExisting;
	}


	public void setReprocessExisting(boolean reprocessExisting) {
		this.reprocessExisting = reprocessExisting;
	}
}
