package com.clifton.investment.instrument.event;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.cache.InvestmentSecurityEventForDateCache;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventDetailSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventExtendedSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchFormConfigurer;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventStatusSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventTypeSearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.type.IntegerType;
import org.hibernate.type.Type;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * The <code>InvestmentSecurityEventServiceImpl</code> class provides basic implementation of InvestmentSecurityEventService interface.
 *
 * @author vgomelsky
 */
@Service
public class InvestmentSecurityEventServiceImpl implements InvestmentSecurityEventService {

	private AdvancedUpdatableDAO<InvestmentSecurityEvent, Criteria> investmentSecurityEventDAO;
	private AdvancedReadOnlyDAO<InvestmentSecurityEventExtended, Criteria> investmentSecurityEventExtendedDAO;
	private AdvancedUpdatableDAO<InvestmentSecurityEventDetail, Criteria> investmentSecurityEventDetailDAO;
	private AdvancedReadOnlyDAO<InvestmentSecurityEventStatus, Criteria> investmentSecurityEventStatusDAO;
	private AdvancedReadOnlyDAO<InvestmentSecurityEventType, Criteria> investmentSecurityEventTypeDAO;
	private AdvancedUpdatableDAO<InvestmentSecurityEventTypeHierarchy, Criteria> investmentSecurityEventTypeHierarchyDAO;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSetupService investmentSetupService;

	private DaoNamedEntityCache<InvestmentSecurityEventStatus> investmentSecurityEventStatusCache;
	private DaoNamedEntityCache<InvestmentSecurityEventType> investmentSecurityEventTypeCache;
	private DaoCompositeKeyCache<InvestmentSecurityEventTypeHierarchy, Short, Short> investmentSecurityEventTypeHierarchyCache;
	private InvestmentSecurityEventForDateCache investmentSecurityEventForAccrualEndDateCache;
	private InvestmentSecurityEventForDateCache investmentSecurityEventPreviousForAccrualEndDateCache;
	private InvestmentSecurityEventForDateCache investmentSecurityEventWithLatestExDateCache;
	private DaoSingleKeyListCache<InvestmentSecurityEventDetail, Integer> investmentSecurityEventDetailListCache;
	private DaoSingleKeyListCache<InvestmentSecurityEventTypeHierarchy, Short> investmentSecurityEventTypeHierarchyListByHierarchyCache;


	////////////////////////////////////////////////////////////////////////////
	//////////       Investment Security Event Methods              ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEvent(int id) {
		return getInvestmentSecurityEventDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEventForAccrualEndDate(int securityId, String typeName, Date accrualEndDate) {
		// lookup in cache first
		Optional<Integer> eventId = getInvestmentSecurityEventForAccrualEndDateCache().getInvestmentSecurityEvent(securityId, typeName, accrualEndDate);
		InvestmentSecurityEvent result = null;

		//noinspection OptionalAssignedToNull
		if (eventId == null) {
			final short eventTypeId = getInvestmentSecurityEventTypeByName(typeName).getId(); // cached lookup to avoid extra join

			// find event of the specified type with first Record Date on or AFTER the specified date
			List<InvestmentSecurityEvent> list = getInvestmentSecurityEventDAO().findBySearchCriteria(new HibernateSearchConfigurer() {

				@Override
				public void configureCriteria(Criteria criteria) {
					criteria.add(Restrictions.eq("security.id", securityId)).add(Restrictions.eq("type.id", eventTypeId));
					criteria.add(Restrictions.gt("recordDate", accrualEndDate)); // recordDate === accrualEndDate
					criteria.add(Restrictions.le("declareDate", accrualEndDate)); // declareDate === accrualStartDate
					criteria.addOrder(Order.asc("recordDate"));
				}


				@Override
				public int getLimit() {
					return 1;
				}
			});
			result = CollectionUtils.getFirstElement(list);
			if (result != null && result.getRecordDate() != null) {
				// Pre-Cache all other relevant accrual end dates for this event (Billing using dirty price looks this up for each position - each date in the quarter)
				Date date = DateUtils.getNextWeekday(accrualEndDate);
				// While date is before accrual end (still always the next one) - set it in the cache
				while (DateUtils.compare(date, result.getRecordDate(), false) < 0) {
					getInvestmentSecurityEventForAccrualEndDateCache().setInvestmentSecurityEvent(securityId, typeName, date, result);
					date = DateUtils.getNextWeekday(date);
				}
			}
			getInvestmentSecurityEventForAccrualEndDateCache().setInvestmentSecurityEvent(securityId, typeName, accrualEndDate, result);
		}
		else if (eventId.isPresent()) {
			result = getInvestmentSecurityEvent(eventId.get());
		}

		return result;
	}


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEventPreviousForAccrualEndDate(int securityId, String typeName, Date accrualEndDate) {
		// lookup in cache first
		Optional<Integer> eventId = getInvestmentSecurityEventPreviousForAccrualEndDateCache().getInvestmentSecurityEvent(securityId, typeName, accrualEndDate);
		InvestmentSecurityEvent result = null;

		//noinspection OptionalAssignedToNull
		if (eventId == null) {
			final short eventTypeId = getInvestmentSecurityEventTypeByName(typeName).getId(); // cached lookup to avoid extra join

			// find FIRST event of the specified type with Record Date BEFORE the specified date
			List<InvestmentSecurityEvent> list = getInvestmentSecurityEventDAO().findBySearchCriteria(new HibernateSearchConfigurer() {

				@Override
				public void configureCriteria(Criteria criteria) {
					criteria.add(Restrictions.eq("security.id", securityId)).add(Restrictions.eq("type.id", eventTypeId)).add(Restrictions.lt("recordDate", accrualEndDate)); // recordDate === accrualEndDate
					criteria.addOrder(Order.desc("recordDate"));
				}


				@Override
				public int getLimit() {
					return 1;
				}
			});
			result = CollectionUtils.getFirstElement(list);

			if (result != null && result.getRecordDate() != null) {
				// Pre-Cache all other relevant accrual end dates for this event (Billing using dirty price looks this up for each position - each date in the quarter)
				Date date = DateUtils.getNextWeekday(accrualEndDate);
				// While date is before accrual end (still always the next one) - set it in the cache
				while (DateUtils.compare(date, result.getRecordDate(), false) < 0) {
					getInvestmentSecurityEventPreviousForAccrualEndDateCache().setInvestmentSecurityEvent(securityId, typeName, date, result);
					date = DateUtils.getNextWeekday(date);
				}
			}
			getInvestmentSecurityEventPreviousForAccrualEndDateCache().setInvestmentSecurityEvent(securityId, typeName, accrualEndDate, result);
		}
		else if (eventId.isPresent()) {
			result = getInvestmentSecurityEvent(eventId.get());
		}

		return result;
	}


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEventWithLatestExDate(int securityId, String typeName, Date valuationDate) {
		// lookup in cache first
		Optional<Integer> eventId = getInvestmentSecurityEventWithLatestExDateCache().getInvestmentSecurityEvent(securityId, typeName, valuationDate);
		InvestmentSecurityEvent result = null;

		// noinspection OptionalAssignedToNull
		if (eventId == null) {
			final short eventTypeId = getInvestmentSecurityEventTypeByName(typeName).getId(); // cached lookup to avoid extra join

			List<InvestmentSecurityEvent> list = getInvestmentSecurityEventDAO().findBySearchCriteria(new HibernateSearchConfigurer() {

				@Override
				public void configureCriteria(Criteria criteria) {
					// optimization that avoids unnecessary SQL key look-ups by using fully covered index to get single result first
					DetachedCriteria subQuery = DetachedCriteria.forClass(InvestmentSecurityEvent.class);
					subQuery.setProjection(Projections.sqlProjection("TOP 1 InvestmentSecurityEventID", new String[]{"InvestmentSecurityEventID"}, new Type[]{new IntegerType()}));
					subQuery.add(Restrictions.eq("security.id", securityId));
					subQuery.add(Restrictions.eq("type.id", eventTypeId));
					subQuery.add(Restrictions.le("exDate", valuationDate));
					subQuery.addOrder(Order.desc("exDate"));
					criteria.add(Subqueries.propertyEq("id", subQuery));
				}


				@Override
				public boolean configureOrderBy(Criteria criteria) {
					return true;
				}
			});
			result = CollectionUtils.getFirstElement(list);
			getInvestmentSecurityEventWithLatestExDateCache().setInvestmentSecurityEvent(securityId, typeName, valuationDate, result);
		}
		else if (eventId.isPresent()) {
			result = getInvestmentSecurityEvent(eventId.get());
		}

		return result;
	}


	@Override
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventListSameForInstrument(final InvestmentSecurityEvent event) {
		InvestmentSecurityEventSearchFormConfigurer<InvestmentSecurityEventSearchForm> searchConfig = InvestmentSecurityEventSearchFormConfigurer.forEvent(event, this,
				(configurer, criteria) -> criteria.createAlias("security", "sec").add(Restrictions.eq("sec.instrument.id", event.getSecurity().getInstrument().getId())));
		return getInvestmentSecurityEventDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventListSameForInstrumentUnderlying(final InvestmentSecurityEvent event) {
		InvestmentSecurityEventSearchFormConfigurer<InvestmentSecurityEventSearchForm> searchConfig = InvestmentSecurityEventSearchFormConfigurer.forEvent(event, this,
				(configurer, criteria) -> {
					//All events whose events securities instruments underlying is this events security
					criteria.createAlias("security", "sec").createAlias("sec.instrument", "instrumentSec").add(Restrictions.eq("instrumentSec.underlyingInstrument.id", event.getSecurity().getInstrument().getId()));
					//Additionally all events whose securities instruments hierarchy has an event type that is the same as this events type and
					//  copy from underlying is checked
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentSecurityEventTypeHierarchy.class, "eventTypeHierarchy");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.eq("eventTypeHierarchy.referenceOne.id", event.getType().getId()));
					sub.add(Restrictions.eq("eventTypeHierarchy.copiedFromUnderlying", true));
					criteria.add(Subqueries.exists(sub));
				});
		return getInvestmentSecurityEventDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventList(final InvestmentSecurityEventSearchForm searchForm) {
		return getInvestmentSecurityEventDAO().findBySearchCriteria(new InvestmentSecurityEventSearchFormConfigurer<>(searchForm, this,
				(configurer, criteria) -> {
					if (searchForm.getDayBeforeExOrEventDate() != null) {
						// custom filter: (EventDate = @Date OR EventExDate = DATEADD(DAY, 1, @Date))
						criteria.add(Restrictions.disjunction()
								.add(Restrictions.eq("eventDate", searchForm.getDayBeforeExOrEventDate()))
								.add(Restrictions.eq("exDate", DateUtils.addDays(searchForm.getDayBeforeExOrEventDate(), 1))));
					}
					if (searchForm.getIndirectSecurityId() != null) {
						// custom filter: all events with Event Security ID != @SecurityId AND (Payout Security = @SecurityId OR Additional Security ID = @SecurityId)
						criteria.add(
								Restrictions.conjunction(
										Restrictions.ne(criteria.getAlias() + ".security.id", searchForm.getIndirectSecurityId()),
										Restrictions.disjunction(
												Subqueries.exists(getIndirectPayoutDetachedCriteria(searchForm, criteria)),
												Restrictions.eq(criteria.getAlias() + ".additionalSecurity.id", searchForm.getIndirectSecurityId())
										)
								));
					}
				}));
	}


	@Override
	public List<InvestmentSecurityEventExtended> getInvestmentSecurityEventExtendedList(final InvestmentSecurityEventExtendedSearchForm searchForm) {
		return getInvestmentSecurityEventExtendedDAO().findBySearchCriteria(new InvestmentSecurityEventSearchFormConfigurer<>(searchForm, this,
				(configurer, criteria) -> {
					if (searchForm.getDayBeforeExOrEventDate() != null) {
						// custom filter: (EventDate = @Date OR AdditionalPayoutDate = @Date OR EventExDate = DATEADD(DAY, 1, @Date))
						criteria.add(Restrictions.disjunction()
								.add(Restrictions.eq("eventDate", searchForm.getDayBeforeExOrEventDate()))
								.add(Restrictions.eq("additionalPayoutDate", searchForm.getDayBeforeExOrEventDate()))
								.add(Restrictions.eq("exDate", DateUtils.addDays(searchForm.getDayBeforeExOrEventDate(), 1))));
					}
					if (searchForm.getDefaultElection() != null) {
						if (searchForm.getDefaultElection()) {
							criteria.add(Restrictions.or(
									Restrictions.eq("defaultElection", searchForm.getDefaultElection()),
									Restrictions.isNull("payout.id")));
						}
						else {
							criteria.add(Restrictions.eq("defaultElection", searchForm.getDefaultElection()));
						}
					}
					if (searchForm.getDeleted() != null) {
						InvestmentSecurityEventStatus deletedStatus = getInvestmentSecurityEventStatusByName(InvestmentSecurityEventStatus.STATUS_DELETED);
						if (searchForm.getDeleted()) {
							criteria.add(Restrictions.or(
									Restrictions.eq("deleted", searchForm.getDeleted()),
									Restrictions.eq("status.id", deletedStatus.getId())));
						}
						else {
							criteria.add(Restrictions.and(
									Restrictions.or(
											Restrictions.eq("deleted", searchForm.getDeleted()),
											Restrictions.isNull("payout.id")
									),
									Restrictions.ne("status.id", deletedStatus.getId())));
						}
					}
					if (searchForm.getSinglePayout() != null) {
						DetachedCriteria subquery = DetachedCriteria.forClass(InvestmentSecurityEventPayout.class, "payout");
						subquery.setProjection(Projections.count("id"));
						subquery.add(Restrictions.eqProperty("securityEvent.id", criteria.getAlias() + ".id"));
						if (searchForm.getSinglePayout()) {
							// find all events with 1 or fewer payouts
							criteria.add(Subqueries.ge(1L, subquery));
						}
						else {
							// find all events with 2 or more payouts
							criteria.add(Subqueries.le(2L, subquery));
						}
					}
				}));
	}


	/**
	 * @see InvestmentSecurityEventObserver
	 */
	@Override
	public InvestmentSecurityEvent saveInvestmentSecurityEvent(InvestmentSecurityEvent bean) {
		return getInvestmentSecurityEventDAO().save(bean);
	}


	/**
	 * @see InvestmentSecurityEventObserver
	 */
	@Override
	public void saveInvestmentSecurityEvents(List<InvestmentSecurityEvent> list) {
		getInvestmentSecurityEventDAO().saveList(list);
	}


	/**
	 * @see InvestmentSecurityEventObserver
	 */
	@Override
	public void saveInvestmentSecurityEventList(List<InvestmentSecurityEvent> newList, List<InvestmentSecurityEvent> originalList) {
		getInvestmentSecurityEventDAO().saveList(newList, originalList);
	}


	/**
	 * @see InvestmentSecurityEventObserver
	 */
	@Override
	@Transactional
	public void deleteInvestmentSecurityEvent(int id) {
		List<InvestmentSecurityEventDetail> detailList = getInvestmentSecurityEventDetailListByEvent(id);
		getInvestmentSecurityEventDetailDAO().deleteList(detailList);
		getInvestmentSecurityEventDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////       Investment Security Event Methods              ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventDetail getInvestmentSecurityEventDetail(int id) {
		return getInvestmentSecurityEventDetailDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentSecurityEventDetail getInvestmentSecurityEventDetailByEventAndEffectiveDate(int securityEventId, Date effectiveDate) {
		return getInvestmentSecurityEventDetailDAO().findOneByFields(new String[]{"event.id", "effectiveDate"}, new Object[]{securityEventId, effectiveDate});
	}


	@Override
	public List<InvestmentSecurityEventDetail> getInvestmentSecurityEventDetailList(InvestmentSecurityEventDetailSearchForm searchForm) {
		return getInvestmentSecurityEventDetailDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentSecurityEventDetail> getInvestmentSecurityEventDetailListByEvent(int securityEventId) {
		return getInvestmentSecurityEventDetailListCache().getBeanListForKeyValue(getInvestmentSecurityEventDetailDAO(), securityEventId);
	}


	/**
	 * @see InvestmentSecurityEventDetailValidator
	 */
	@Override
	public InvestmentSecurityEventDetail saveInvestmentSecurityEventDetail(InvestmentSecurityEventDetail eventDetail) {
		return getInvestmentSecurityEventDetailDAO().save(eventDetail);
	}


	@Override
	public void saveInvestmentSecurityEventDetailList(List<InvestmentSecurityEventDetail> beanList) {
		getInvestmentSecurityEventDetailDAO().saveList(beanList);
	}


	@Override
	public void deleteInvestmentSecurityEventDetail(int id) {
		getInvestmentSecurityEventDetailDAO().delete(id);
	}


	@Override
	public void deleteInvestmentSecurityEventDetailList(List<InvestmentSecurityEventDetail> detailList) {
		getInvestmentSecurityEventDetailDAO().deleteList(detailList);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////     Investment Security Event Status Methods         ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventStatus getInvestmentSecurityEventStatus(short id) {
		return getInvestmentSecurityEventStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentSecurityEventStatus getInvestmentSecurityEventStatusByName(String name) {
		return getInvestmentSecurityEventStatusCache().getBeanForKeyValueStrict(getInvestmentSecurityEventStatusDAO(), name);
	}


	@Override
	public List<InvestmentSecurityEventStatus> getInvestmentSecurityEventStatusList(InvestmentSecurityEventStatusSearchForm searchForm) {
		return getInvestmentSecurityEventStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	//////////     Investment Security Event Type Methods           ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventType getInvestmentSecurityEventType(short id) {
		return getInvestmentSecurityEventTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentSecurityEventType getInvestmentSecurityEventTypeByName(String name) {
		return getInvestmentSecurityEventTypeCache().getBeanForKeyValueStrict(getInvestmentSecurityEventTypeDAO(), name);
	}


	@Override
	public boolean isInvestmentSecurityEventTypeAllowedForSecurity(final int securityId, final String eventTypeName) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		ValidationUtils.assertNotNull(security, "Cannot find investment security with id = " + securityId);

		return isInvestmentSecurityEventTypeAllowedForHierarchy(security.getInstrument().getHierarchy().getId(), eventTypeName);
	}


	@Override
	public boolean isInvestmentSecurityEventTypeAllowedForHierarchy(final short hierarchyId, final String eventTypeName) {
		List<InvestmentSecurityEventType> typeList = getInvestmentSecurityEventTypeListByHierarchy(hierarchyId);
		for (InvestmentSecurityEventType eventType : CollectionUtils.getIterable(typeList)) {
			if (eventType.getName().equalsIgnoreCase(eventTypeName)) {
				return true;
			}
		}

		return false;
	}


	@Override
	public List<InvestmentSecurityEventType> getInvestmentSecurityEventTypeList(InvestmentSecurityEventTypeSearchForm searchForm) {
		return getInvestmentSecurityEventTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentSecurityEventType> getInvestmentSecurityEventTypeListByHierarchy(final short hierarchyId) {
		List<InvestmentSecurityEventType> result = null;
		List<InvestmentSecurityEventTypeHierarchy> mappingList = getInvestmentSecurityEventTypeHierarchyListByHierarchyCache().getBeanListForKeyValue(getInvestmentSecurityEventTypeHierarchyDAO(), hierarchyId);
		if (!CollectionUtils.isEmpty(mappingList)) {
			result = mappingList.stream().map(InvestmentSecurityEventTypeHierarchy::getReferenceOne).collect(Collectors.toList());
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	///////      Investment Security Event Type Hierarchy Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEventTypeHierarchy getInvestmentSecurityEventTypeHierarchy(int id) {
		return getInvestmentSecurityEventTypeHierarchyDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentSecurityEventTypeHierarchy getInvestmentSecurityEventTypeHierarchyByEventTypeAndHierarchy(short eventTypeId, short instrumentHierarchyId) {
		return getInvestmentSecurityEventTypeHierarchyCache().getBeanForKeyValues(getInvestmentSecurityEventTypeHierarchyDAO(), eventTypeId, instrumentHierarchyId);
	}


	@Override
	public List<InvestmentSecurityEventTypeHierarchy> getInvestmentSecurityEventTypeHierarchyListByHierarchy(short hierarchyId) {
		return getInvestmentSecurityEventTypeHierarchyDAO().findByField("referenceTwo.id", hierarchyId);
	}


	@Override
	public List<InvestmentSecurityEventTypeHierarchy> getInvestmentSecurityEventTypeHierarchyListByEventType(short eventTypeId) {
		return getInvestmentSecurityEventTypeHierarchyDAO().findByField("referenceOne.id", eventTypeId);
	}


	@Override
	public InvestmentSecurityEventTypeHierarchy linkInvestmentSecurityEventTypeHierarchy(short eventTypeId, short instrumentHierarchyId) {
		InvestmentInstrumentHierarchy hierarchy = getInvestmentSetupService().getInvestmentInstrumentHierarchy(instrumentHierarchyId);
		ValidationUtils.assertTrue(hierarchy.isAssignmentAllowed(), "Cannot assign Event Type to Hierarchy that does not allow security assignments.", "assignmentAllowed");
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventTypeDAO().findByPrimaryKey(eventTypeId);

		InvestmentSecurityEventTypeHierarchy link = new InvestmentSecurityEventTypeHierarchy();
		link.setReferenceOne(eventType);
		link.setReferenceTwo(hierarchy);
		getInvestmentSecurityEventTypeHierarchyDAO().save(link);
		return link;
	}


	@Override
	public InvestmentSecurityEventTypeHierarchy saveInvestmentSecurityEventTypeHierarchy(InvestmentSecurityEventTypeHierarchy bean) {
		return getInvestmentSecurityEventTypeHierarchyDAO().save(bean);
	}


	@Override
	public void deleteInvestmentSecurityEventTypeHierarchy(int id) {
		getInvestmentSecurityEventTypeHierarchyDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DetachedCriteria getIndirectPayoutDetachedCriteria(InvestmentSecurityEventSearchForm searchForm, Criteria criteria) {
		DetachedCriteria subquery = DetachedCriteria.forClass(InvestmentSecurityEventPayout.class, "payout");
		subquery.setProjection(Projections.property("id"));
		subquery.add(Restrictions.conjunction(
				Restrictions.eqProperty("securityEvent.id", criteria.getAlias() + ".id"),
				Restrictions.eq("payoutSecurity.id", searchForm.getIndirectSecurityId()))
		);
		return subquery;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentSecurityEvent, Criteria> getInvestmentSecurityEventDAO() {
		return this.investmentSecurityEventDAO;
	}


	public void setInvestmentSecurityEventDAO(AdvancedUpdatableDAO<InvestmentSecurityEvent, Criteria> investmentSecurityEventDAO) {
		this.investmentSecurityEventDAO = investmentSecurityEventDAO;
	}


	public AdvancedReadOnlyDAO<InvestmentSecurityEventExtended, Criteria> getInvestmentSecurityEventExtendedDAO() {
		return this.investmentSecurityEventExtendedDAO;
	}


	public void setInvestmentSecurityEventExtendedDAO(AdvancedReadOnlyDAO<InvestmentSecurityEventExtended, Criteria> investmentSecurityEventExtendedDAO) {
		this.investmentSecurityEventExtendedDAO = investmentSecurityEventExtendedDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSecurityEventDetail, Criteria> getInvestmentSecurityEventDetailDAO() {
		return this.investmentSecurityEventDetailDAO;
	}


	public void setInvestmentSecurityEventDetailDAO(AdvancedUpdatableDAO<InvestmentSecurityEventDetail, Criteria> investmentSecurityEventDetailDAO) {
		this.investmentSecurityEventDetailDAO = investmentSecurityEventDetailDAO;
	}


	public AdvancedReadOnlyDAO<InvestmentSecurityEventStatus, Criteria> getInvestmentSecurityEventStatusDAO() {
		return this.investmentSecurityEventStatusDAO;
	}


	public void setInvestmentSecurityEventStatusDAO(AdvancedReadOnlyDAO<InvestmentSecurityEventStatus, Criteria> investmentSecurityEventStatusDAO) {
		this.investmentSecurityEventStatusDAO = investmentSecurityEventStatusDAO;
	}


	public AdvancedReadOnlyDAO<InvestmentSecurityEventType, Criteria> getInvestmentSecurityEventTypeDAO() {
		return this.investmentSecurityEventTypeDAO;
	}


	public void setInvestmentSecurityEventTypeDAO(AdvancedReadOnlyDAO<InvestmentSecurityEventType, Criteria> investmentSecurityEventTypeDAO) {
		this.investmentSecurityEventTypeDAO = investmentSecurityEventTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSecurityEventTypeHierarchy, Criteria> getInvestmentSecurityEventTypeHierarchyDAO() {
		return this.investmentSecurityEventTypeHierarchyDAO;
	}


	public void setInvestmentSecurityEventTypeHierarchyDAO(AdvancedUpdatableDAO<InvestmentSecurityEventTypeHierarchy, Criteria> investmentSecurityEventTypeHierarchyDAO) {
		this.investmentSecurityEventTypeHierarchyDAO = investmentSecurityEventTypeHierarchyDAO;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public DaoNamedEntityCache<InvestmentSecurityEventStatus> getInvestmentSecurityEventStatusCache() {
		return this.investmentSecurityEventStatusCache;
	}


	public void setInvestmentSecurityEventStatusCache(DaoNamedEntityCache<InvestmentSecurityEventStatus> investmentSecurityEventStatusCache) {
		this.investmentSecurityEventStatusCache = investmentSecurityEventStatusCache;
	}


	public DaoNamedEntityCache<InvestmentSecurityEventType> getInvestmentSecurityEventTypeCache() {
		return this.investmentSecurityEventTypeCache;
	}


	public void setInvestmentSecurityEventTypeCache(DaoNamedEntityCache<InvestmentSecurityEventType> investmentSecurityEventTypeCache) {
		this.investmentSecurityEventTypeCache = investmentSecurityEventTypeCache;
	}


	public DaoCompositeKeyCache<InvestmentSecurityEventTypeHierarchy, Short, Short> getInvestmentSecurityEventTypeHierarchyCache() {
		return this.investmentSecurityEventTypeHierarchyCache;
	}


	public void setInvestmentSecurityEventTypeHierarchyCache(DaoCompositeKeyCache<InvestmentSecurityEventTypeHierarchy, Short, Short> investmentSecurityEventTypeHierarchyCache) {
		this.investmentSecurityEventTypeHierarchyCache = investmentSecurityEventTypeHierarchyCache;
	}


	public InvestmentSecurityEventForDateCache getInvestmentSecurityEventForAccrualEndDateCache() {
		return this.investmentSecurityEventForAccrualEndDateCache;
	}


	public void setInvestmentSecurityEventForAccrualEndDateCache(InvestmentSecurityEventForDateCache investmentSecurityEventForAccrualEndDateCache) {
		this.investmentSecurityEventForAccrualEndDateCache = investmentSecurityEventForAccrualEndDateCache;
	}


	public InvestmentSecurityEventForDateCache getInvestmentSecurityEventPreviousForAccrualEndDateCache() {
		return this.investmentSecurityEventPreviousForAccrualEndDateCache;
	}


	public void setInvestmentSecurityEventPreviousForAccrualEndDateCache(InvestmentSecurityEventForDateCache investmentSecurityEventPreviousForAccrualEndDateCache) {
		this.investmentSecurityEventPreviousForAccrualEndDateCache = investmentSecurityEventPreviousForAccrualEndDateCache;
	}


	public InvestmentSecurityEventForDateCache getInvestmentSecurityEventWithLatestExDateCache() {
		return this.investmentSecurityEventWithLatestExDateCache;
	}


	public void setInvestmentSecurityEventWithLatestExDateCache(InvestmentSecurityEventForDateCache investmentSecurityEventWithLatestExDateCache) {
		this.investmentSecurityEventWithLatestExDateCache = investmentSecurityEventWithLatestExDateCache;
	}


	public DaoSingleKeyListCache<InvestmentSecurityEventDetail, Integer> getInvestmentSecurityEventDetailListCache() {
		return this.investmentSecurityEventDetailListCache;
	}


	public void setInvestmentSecurityEventDetailListCache(DaoSingleKeyListCache<InvestmentSecurityEventDetail, Integer> investmentSecurityEventDetailListCache) {
		this.investmentSecurityEventDetailListCache = investmentSecurityEventDetailListCache;
	}


	public DaoSingleKeyListCache<InvestmentSecurityEventTypeHierarchy, Short> getInvestmentSecurityEventTypeHierarchyListByHierarchyCache() {
		return this.investmentSecurityEventTypeHierarchyListByHierarchyCache;
	}


	public void setInvestmentSecurityEventTypeHierarchyListByHierarchyCache(DaoSingleKeyListCache<InvestmentSecurityEventTypeHierarchy, Short> investmentSecurityEventTypeHierarchyListByHierarchyCache) {
		this.investmentSecurityEventTypeHierarchyListByHierarchyCache = investmentSecurityEventTypeHierarchyListByHierarchyCache;
	}
}
