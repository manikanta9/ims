package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.instrument.calculator.accrual.date.InvestmentAccrualDateCalculator;
import com.clifton.investment.instrument.calculator.accrual.sign.AccrualSignCalculators;
import com.clifton.investment.instrument.calculator.accrual.sign.InvestmentAccrualSignCalculator;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>InvestmentAccrualCalculatorFactory</code> class returns InvestmentAccrualCalculator or InvestmentAccrualDateCalculator
 * implementations for the specified AccrualCalculator and AccrualDateCalculator types.
 * <p/>
 * Note: the factory must be defined in Spring context and all calculators must be set.
 *
 * @author vgomelsky
 */
public class InvestmentAccrualCalculatorFactory {

	private Map<AccrualMethods, InvestmentAccrualCalculator> calculatorMap = new ConcurrentHashMap<>();
	private Map<AccrualDateCalculators, InvestmentAccrualDateCalculator> dateCalculatorMap = new ConcurrentHashMap<>();
	private Map<AccrualSignCalculators, InvestmentAccrualSignCalculator> signCalculatorMap = new ConcurrentHashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccrualCalculator getInvestmentAccrualCalculator(AccrualMethods accrualMethod) {
		InvestmentAccrualCalculator result = getCalculatorMap().get(accrualMethod);
		if (result == null) {
			throw new IllegalArgumentException("Cannot find InvestmentAccrualCalculator for " + accrualMethod);
		}
		return result;
	}


	public InvestmentAccrualDateCalculator getInvestmentAccrualDateCalculator(AccrualDateCalculators calculator) {
		InvestmentAccrualDateCalculator result = getDateCalculatorMap().get(calculator);
		if (result == null) {
			throw new IllegalArgumentException("Cannot find InvestmentAccrualDateCalculator for " + calculator);
		}
		return result;
	}


	public InvestmentAccrualSignCalculator getInvestmentAccrualSignCalculator(AccrualSignCalculators calculator) {
		InvestmentAccrualSignCalculator result = getSignCalculatorMap().get(calculator);
		if (result == null) {
			throw new IllegalArgumentException("Cannot find InvestmentAccrualSignCalculator for " + calculator);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<AccrualMethods, InvestmentAccrualCalculator> getCalculatorMap() {
		return this.calculatorMap;
	}


	public void setCalculatorMap(Map<AccrualMethods, InvestmentAccrualCalculator> calculatorMap) {
		this.calculatorMap = calculatorMap;
	}


	public Map<AccrualDateCalculators, InvestmentAccrualDateCalculator> getDateCalculatorMap() {
		return this.dateCalculatorMap;
	}


	public void setDateCalculatorMap(Map<AccrualDateCalculators, InvestmentAccrualDateCalculator> dateCalculatorMap) {
		this.dateCalculatorMap = dateCalculatorMap;
	}


	public Map<AccrualSignCalculators, InvestmentAccrualSignCalculator> getSignCalculatorMap() {
		return this.signCalculatorMap;
	}


	public void setSignCalculatorMap(Map<AccrualSignCalculators, InvestmentAccrualSignCalculator> signCalculatorMap) {
		this.signCalculatorMap = signCalculatorMap;
	}
}
