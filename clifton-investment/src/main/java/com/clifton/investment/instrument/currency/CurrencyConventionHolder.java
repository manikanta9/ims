package com.clifton.investment.instrument.currency;


import java.io.Serializable;


/**
 * The <code>CurrencyConventionHolder</code> class is used to cache currency convention details.
 *
 * @author vgomelsky
 */
public class CurrencyConventionHolder implements Serializable {

	private final boolean multiplyFromByFX;
	private final Integer daysToSettle;
	private final String dominantCurrencySymbol;


	public CurrencyConventionHolder(boolean multiplyFromByFX, Integer daysToSettle, String dominantCurrencySymbol) {
		this.multiplyFromByFX = multiplyFromByFX;
		this.daysToSettle = daysToSettle;
		this.dominantCurrencySymbol = dominantCurrencySymbol;
	}


	public boolean isMultiplyFromByFX() {
		return this.multiplyFromByFX;
	}


	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	public String getDominantCurrencySymbol() {
		return this.dominantCurrencySymbol;
	}
}
