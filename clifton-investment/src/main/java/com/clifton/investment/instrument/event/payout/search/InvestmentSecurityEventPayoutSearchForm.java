package com.clifton.investment.instrument.event.payout.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.instrument.event.FractionalShares;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author vgomelsky
 */
public class InvestmentSecurityEventPayoutSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "securityEvent.id")
	private Integer securityEventId;

	@SearchField(searchField = "securityEvent.id")
	private Integer[] securityEventIds;

	@SearchField(searchField = "type.id", searchFieldPath = "securityEvent")
	private Short eventTypeId;

	@SearchField(searchField = "status.id", searchFieldPath = "securityEvent")
	private Short eventStatusId;

	@SearchField(searchField = "security.id", searchFieldPath = "securityEvent")
	private Integer eventSecurityId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "securityEvent.security.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchFieldPath = "securityEvent")
	private Long corporateActionIdentifier;

	@SearchField(searchFieldPath = "securityEvent")
	private Date eventDate;


	@SearchField(searchField = "payoutType.id")
	private Short payoutTypeId;

	@SearchField(searchField = "typeCode", searchFieldPath = "payoutType", comparisonConditions = ComparisonConditions.EQUALS)
	private String payoutTypeCode;

	@SearchField
	private Short electionNumber;

	@SearchField
	private Short payoutNumber;

	@SearchField(searchField = "payoutSecurity.id")
	private Integer payoutSecurityId;

	@SearchField
	private BigDecimal beforeEventValue;

	@SearchField
	private BigDecimal afterEventValue;

	@SearchField
	private BigDecimal additionalPayoutValue;

	@SearchField
	private BigDecimal additionalPayoutValue2;

	@SearchField
	private BigDecimal prorationRate;

	@SearchField
	private Date additionalPayoutDate;

	@SearchField
	private FractionalShares fractionalSharesMethod;

	@SearchField
	private String description;

	@SearchField
	private Boolean defaultElection;

	@SearchField
	private Boolean deleted;

	@SearchField
	private Boolean dtcOnly;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getSecurityEventId() {
		return this.securityEventId;
	}


	public void setSecurityEventId(Integer securityEventId) {
		this.securityEventId = securityEventId;
	}


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}


	public Short getEventStatusId() {
		return this.eventStatusId;
	}


	public void setEventStatusId(Short eventStatusId) {
		this.eventStatusId = eventStatusId;
	}


	public Integer getEventSecurityId() {
		return this.eventSecurityId;
	}


	public void setEventSecurityId(Integer eventSecurityId) {
		this.eventSecurityId = eventSecurityId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Long getCorporateActionIdentifier() {
		return this.corporateActionIdentifier;
	}


	public void setCorporateActionIdentifier(Long corporateActionIdentifier) {
		this.corporateActionIdentifier = corporateActionIdentifier;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Short getPayoutTypeId() {
		return this.payoutTypeId;
	}


	public void setPayoutTypeId(Short payoutTypeId) {
		this.payoutTypeId = payoutTypeId;
	}


	public String getPayoutTypeCode() {
		return this.payoutTypeCode;
	}


	public void setPayoutTypeCode(String payoutTypeCode) {
		this.payoutTypeCode = payoutTypeCode;
	}


	public Integer[] getSecurityEventIds() {
		return this.securityEventIds;
	}


	public void setSecurityEventIds(Integer[] securityEventIds) {
		this.securityEventIds = securityEventIds;
	}


	public Short getElectionNumber() {
		return this.electionNumber;
	}


	public void setElectionNumber(Short electionNumber) {
		this.electionNumber = electionNumber;
	}


	public Short getPayoutNumber() {
		return this.payoutNumber;
	}


	public void setPayoutNumber(Short payoutNumber) {
		this.payoutNumber = payoutNumber;
	}


	public Integer getPayoutSecurityId() {
		return this.payoutSecurityId;
	}


	public void setPayoutSecurityId(Integer payoutSecurityId) {
		this.payoutSecurityId = payoutSecurityId;
	}


	public BigDecimal getBeforeEventValue() {
		return this.beforeEventValue;
	}


	public void setBeforeEventValue(BigDecimal beforeEventValue) {
		this.beforeEventValue = beforeEventValue;
	}


	public BigDecimal getAfterEventValue() {
		return this.afterEventValue;
	}


	public void setAfterEventValue(BigDecimal afterEventValue) {
		this.afterEventValue = afterEventValue;
	}


	public BigDecimal getAdditionalPayoutValue() {
		return this.additionalPayoutValue;
	}


	public void setAdditionalPayoutValue(BigDecimal additionalPayoutValue) {
		this.additionalPayoutValue = additionalPayoutValue;
	}


	public BigDecimal getAdditionalPayoutValue2() {
		return this.additionalPayoutValue2;
	}


	public void setAdditionalPayoutValue2(BigDecimal additionalPayoutValue2) {
		this.additionalPayoutValue2 = additionalPayoutValue2;
	}


	public BigDecimal getProrationRate() {
		return this.prorationRate;
	}


	public void setProrationRate(BigDecimal prorationRate) {
		this.prorationRate = prorationRate;
	}


	public Date getAdditionalPayoutDate() {
		return this.additionalPayoutDate;
	}


	public void setAdditionalPayoutDate(Date additionalPayoutDate) {
		this.additionalPayoutDate = additionalPayoutDate;
	}


	public FractionalShares getFractionalSharesMethod() {
		return this.fractionalSharesMethod;
	}


	public void setFractionalSharesMethod(FractionalShares fractionalSharesMethod) {
		this.fractionalSharesMethod = fractionalSharesMethod;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getDefaultElection() {
		return this.defaultElection;
	}


	public void setDefaultElection(Boolean defaultElection) {
		this.defaultElection = defaultElection;
	}


	public Boolean getDeleted() {
		return this.deleted;
	}


	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}


	public Boolean getDtcOnly() {
		return this.dtcOnly;
	}


	public void setDtcOnly(Boolean dtcOnly) {
		this.dtcOnly = dtcOnly;
	}
}
