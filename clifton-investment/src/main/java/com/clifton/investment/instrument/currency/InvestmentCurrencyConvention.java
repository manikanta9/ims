package com.clifton.investment.instrument.currency;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>InvestmentCurrencyConvention</code> class defines conventions between different currencies.
 * <p/>
 * For example, FX Rate between USD and GBP is quoted as 1.5 meaning 1 USD = 1 GBP / 1.5 [multiplyFromByFX = false]
 * But FX rate between USD and JPY is quoted as 80 meaning 1 USD = 1 JPY * 80 [multiplyFromByFX = true]
 * <p/>
 * The conventions have been used for years when quoting exchange rates. Even though our system always
 * store FX Rate on transactions as Local * FX = Base, some screens and reports may apply these conventions.
 *
 * @author vgomelsky
 */
public class InvestmentCurrencyConvention extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentSecurity fromCurrency;
	private InvestmentSecurity toCurrency;
	private boolean multiplyFromByFX;
	/**
	 * Specifies the number of days it take to settle a physical currency trade for this currency pair.
	 * If defined, will override instrument/hierarchy specific settings for spot currency trades.
	 * For example, USD to CAD is T+1 while USD to other currencies is T+2.
	 */
	private Integer daysToSettle;


	@Override
	public String getLabel() {
		if (this.fromCurrency != null && this.toCurrency != null) {
			return (this.fromCurrency.getSymbol() + " to " + this.toCurrency.getSymbol());
		}
		return null;
	}


	public InvestmentSecurity getFromCurrency() {
		return this.fromCurrency;
	}


	public void setFromCurrency(InvestmentSecurity fromCurrency) {
		this.fromCurrency = fromCurrency;
	}


	public InvestmentSecurity getToCurrency() {
		return this.toCurrency;
	}


	public void setToCurrency(InvestmentSecurity toCurrency) {
		this.toCurrency = toCurrency;
	}


	public boolean isMultiplyFromByFX() {
		return this.multiplyFromByFX;
	}


	public void setMultiplyFromByFX(boolean multiplyFromByFX) {
		this.multiplyFromByFX = multiplyFromByFX;
	}


	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	public InvestmentSecurity getDominantCurrency() {
		return isMultiplyFromByFX() ? getFromCurrency() : getToCurrency();
	}
}
