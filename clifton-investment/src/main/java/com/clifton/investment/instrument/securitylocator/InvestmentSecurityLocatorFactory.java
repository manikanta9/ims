package com.clifton.investment.instrument.securitylocator;


import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;


/**
 * The <code>InvestmentSecurityLocatorFactory</code> class retrieves all InvestmentSecurityLocator beans registered
 * in application context and sorts them according to priority order. The factory allows retrieval of this ordered Collection.
 *
 * @author vgomelsky
 */
@Component
public class InvestmentSecurityLocatorFactory implements InitializingBean, ApplicationContextAware {

	/**
	 * Ordered list of security locators found in application context.
	 */
	private final Collection<InvestmentSecurityLocator> locatorList = new ConcurrentSkipListSet<>((o1, o2) -> (o1.getPriorityOrder() - o2.getPriorityOrder()));

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		// get all beans registered in the context
		Map<String, InvestmentSecurityLocator> beanMap = getApplicationContext().getBeansOfType(InvestmentSecurityLocator.class);
		for (InvestmentSecurityLocator securityLocator : beanMap.values()) {
			this.locatorList.add(securityLocator);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns ordered Collection of registered InvestmentSecurityLocator objects.
	 */
	public Collection<InvestmentSecurityLocator> getInvestmentSecurityLocators() {
		return this.locatorList;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
