package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;


/**
 * The <code>InvestmentAccrualHighRateCalculator</code> calculates the reference rate and spread
 * for an accrual that needs day by day granularity.  It is based on the USD T-Bill Auction High Rate calculation.
 *
 * @author rbrooks
 */
public class InvestmentAccrualHighRateCalculator extends InvestmentAccrualDailyRateCalculator {

	@Override
	protected BigDecimal calculateReferenceRateAmount(@SuppressWarnings("unused") InvestmentSecurityEvent paymentEvent, BigDecimal notional, AccrualDailyRate[] dailyRates, AccrualConfig accrualConfig) {
		// Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(POWER(1 - Ri*91/360, -1/91)) - 1)
		// Where Ri is Reference rate for PREVIOUS day
		BigDecimal result = BigDecimal.ONE;
		for (AccrualDailyRate accrualDailyRate : dailyRates) {
			BigDecimal dailyRate = accrualDailyRate.getRate();
			//normalizeDailyRate
			dailyRate = dailyRate.movePointLeft(2);

			BigDecimal temp = MathUtils.subtract(BigDecimal.ONE, MathUtils.multiply(dailyRate, MathUtils.divide(BigDecimal.valueOf(91), 360)));
			BigDecimal power = BigDecimal.valueOf(Math.pow(temp.doubleValue(), -1.0 / 91));
			result = MathUtils.multiply(result, power);
		}
		return MathUtils.multiply(notional, MathUtils.subtract(result, 1));
	}
}
