package com.clifton.investment.instrument.currency;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class InvestmentCurrencyConventionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "fromCurrency.id")
	private Integer fromCurrencyId;

	@SearchField(searchField = "toCurrency.id")
	private Integer toCurrencyId;

	@SearchField(searchField = "fromCurrency.id,toCurrency.id")
	private Integer fromOrToCurrencyId;

	@SearchField
	private Boolean multiplyFromByFX;

	@SearchField
	private Integer daysToSettle;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getFromCurrencyId() {
		return this.fromCurrencyId;
	}


	public void setFromCurrencyId(Integer fromCurrencyId) {
		this.fromCurrencyId = fromCurrencyId;
	}


	public Integer getToCurrencyId() {
		return this.toCurrencyId;
	}


	public void setToCurrencyId(Integer toCurrencyId) {
		this.toCurrencyId = toCurrencyId;
	}


	public Integer getFromOrToCurrencyId() {
		return this.fromOrToCurrencyId;
	}


	public void setFromOrToCurrencyId(Integer fromOrToCurrencyId) {
		this.fromOrToCurrencyId = fromOrToCurrencyId;
	}


	public Boolean getMultiplyFromByFX() {
		return this.multiplyFromByFX;
	}


	public void setMultiplyFromByFX(Boolean multiplyFromByFX) {
		this.multiplyFromByFX = multiplyFromByFX;
	}


	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}
}
