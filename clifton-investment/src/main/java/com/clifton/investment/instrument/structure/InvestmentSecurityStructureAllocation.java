package com.clifton.investment.instrument.structure;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentSecurityStructureAllocation</code> contains the current security and calculated weight
 * for a specific {@link InvestmentSecurityStructureWeight} and date as well as any other necessary fields that the calculator needs to save.
 * Note - not all structure types save allocation values and not all use all fields and their labels are defined by the types.
 *
 * @author manderson
 */
public class InvestmentSecurityStructureAllocation extends BaseEntity<Integer> {

	private InvestmentSecurityStructureWeight securityStructureWeight;

	private InvestmentSecurity currentSecurity;

	private Date measureDate;

	private BigDecimal allocationWeight;

	private BigDecimal allocationValueLocal;

	private BigDecimal fxRateToBase;

	private BigDecimal additionalAmount1;

	private BigDecimal additionalAmount2;


	/**
	 * Calculation comments for how the values are built
	 */
	@NonPersistentField
	private String comments;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityStructureAllocation() {

	}


	public InvestmentSecurityStructureAllocation(InvestmentSecurityStructureWeight securityStructureWeight, Date measureDate) {
		this.securityStructureWeight = securityStructureWeight;
		this.measureDate = measureDate;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrument getInstrumentAssignment() {
		if (getSecurityStructureWeight() != null && getSecurityStructureWeight().getInstrumentWeight() != null) {
			return getSecurityStructureWeight().getInstrumentWeight().getInstrument();
		}
		return null;
	}


	public String getSector() {
		if (getSecurityStructureWeight() != null && getSecurityStructureWeight().getInstrumentWeight() != null && !StringUtils.isEmpty(getSecurityStructureWeight().getInstrumentWeight().getSector())) {
			return getSecurityStructureWeight().getInstrumentWeight().getSector();
		}
		return "None";
	}


	public void appendComment(String comment) {
		if (getComments() == null) {
			setComments(comment);
		}
		else {
			setComments(getComments() + ", " + comment);
		}
	}


	public BigDecimal getAllocationValueBase() {
		return MathUtils.multiply(getAllocationValueLocal(), getFxRateToBase());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityStructureWeight getSecurityStructureWeight() {
		return this.securityStructureWeight;
	}


	public void setSecurityStructureWeight(InvestmentSecurityStructureWeight securityStructureWeight) {
		this.securityStructureWeight = securityStructureWeight;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public BigDecimal getAllocationWeight() {
		return this.allocationWeight;
	}


	public void setAllocationWeight(BigDecimal allocationWeight) {
		this.allocationWeight = allocationWeight;
	}


	public BigDecimal getAllocationValueLocal() {
		return this.allocationValueLocal;
	}


	public void setAllocationValueLocal(BigDecimal allocationValueLocal) {
		this.allocationValueLocal = allocationValueLocal;
	}


	public BigDecimal getFxRateToBase() {
		return this.fxRateToBase;
	}


	public void setFxRateToBase(BigDecimal fxRateToBase) {
		this.fxRateToBase = fxRateToBase;
	}


	public BigDecimal getAdditionalAmount1() {
		return this.additionalAmount1;
	}


	public void setAdditionalAmount1(BigDecimal additionalAmount1) {
		this.additionalAmount1 = additionalAmount1;
	}


	public BigDecimal getAdditionalAmount2() {
		return this.additionalAmount2;
	}


	public void setAdditionalAmount2(BigDecimal additionalAmount2) {
		this.additionalAmount2 = additionalAmount2;
	}


	public String getComments() {
		return this.comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}


	public InvestmentSecurity getCurrentSecurity() {
		return this.currentSecurity;
	}


	public void setCurrentSecurity(InvestmentSecurity currentSecurity) {
		this.currentSecurity = currentSecurity;
	}
}
