package com.clifton.investment.instrument;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import org.hibernate.Criteria;

import java.util.List;


/**
 * The <code>InvestmentInstrumentService</code> defines the methods for working with {@link InvestmentInstrument} and {@link InvestmentSecurity} Objects.
 *
 * @author manderson
 */
public interface InvestmentInstrumentService {

	////////////////////////////////////////////////////////////////////////////
	//////////          Investment Instrument Methods               ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrument getInvestmentInstrument(int id);


	public List<InvestmentInstrument> getInvestmentInstrumentList(InstrumentSearchForm searchForm);


	/**
	 * Returns a list of {@link InvestmentInstrument} items based upon passed in search configurer
	 */
	public List<InvestmentInstrument> getInvestmentInstrumentList(SearchConfigurer<Criteria> searchConfigurer);


	public InvestmentInstrument saveInvestmentInstrument(InvestmentInstrument bean);


	@DoNotAddRequestMapping
	public InvestmentInstrument saveInvestmentInstrumentAllowOneToOne(InvestmentInstrument bean);


	@DoNotAddRequestMapping
	public void saveInvestmentInstrumentIgnoreOneToOne(InvestmentInstrument bean);


	public void deleteInvestmentInstrument(int id);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void moveInvestmentInstrument(int id, short newInstrumentHierarchyId);


	/**
	 * Moves instrument to new hierarchy and allows bypassing validation that the old and new hierarchies require the same custom fields
	 */
	@SecureMethod(permissions = SecurityPermission.PERMISSION_FULL_CONTROL, dtoClass = InvestmentInstrument.class)
	public void moveInvestmentInstrumentBypassValidation(int id, short newInstrumentHierarchyId);


	////////////////////////////////////////////////////////////////////////////
	///////////          Investment Security Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getInvestmentSecurity(int id);


	/**
	 * This method works only for securities that have one-to-one relationship with instruments.
	 * It will throw an exception if an invalid id is passed.
	 */
	public InvestmentSecurity getInvestmentSecurityByInstrument(int instrumentId);


	/**
	 * Returns InvestmentSecurity with the specified symbol. Throws an exception if more than one is found.
	 * Optionally one can restrict the lookup to only currencies, only non-currencies or both.
	 * This is necessary because there are a few stocks/ETF's that share their symbol with currencies: CAD, AUD, NOK, ...
	 * <p/>
	 * Also supports Exchange Code in the symbol. For example, "BEZ LN" will be matched to "BEZ" security on "London Stock Exchange" with code "LN".
	 * If exact match is not found on specified symbol, check if the symbol has exchange code in it.
	 * If yes, try to match on clean symbol and exchange code.
	 * <p/>
	 * NOTE: use the following method instead if you know whether you're looking up a currency or not:
	 * public InvestmentSecurity getInvestmentSecurityBySymbol(String symbol, Boolean currency)
	 */
	public InvestmentSecurity getInvestmentSecurityBySymbolOnly(String symbol);


	/**
	 * Returns InvestmentSecurity with the specified symbol. Throws an exception if more than one is found.
	 * Optionally one can restrict the lookup to only currencies, only non-currencies or both.
	 * This is necessary because there are a few stocks/ETF's that share their symbol with currencies: CAD, AUD, NOK, ...
	 * <p/>
	 * Also supports Exchange Code in the symbol. For example, "BEZ LN" will be matched to "BEZ" security on "London Stock Exchange" with code "LN".
	 * If exact match is not found on specified symbol, check if the symbol has exchange code in it.
	 * If yes, try to match on clean symbol and exchange code.
	 * <p/>
	 * NOTE: it's very important to pass currency = true when looking up a currency.
	 */
	public InvestmentSecurity getInvestmentSecurityBySymbol(String symbol, Boolean currency);


	public List<InvestmentSecurity> getInvestmentSecurityListByIds(List<Integer> securityIds);


	/**
	 * Uses caching to speed up the lookup and avoid DB access.  Most of the time only one {@link InvestmentSecurity} will be returned.
	 */
	public List<InvestmentSecurity> getInvestmentSecurityListBySymbol(String symbol);


	/**
	 * Returns InvestmentSecurity with the specified CUSIP.
	 * Uses caching to speed up the lookup and avoid DB access.  Most of the time only one {@link InvestmentSecurity} will be returned.
	 */
	public List<InvestmentSecurity> getInvestmentSecurityListByCusip(String cusip);


	/**
	 * Returns InvestmentSecurity with the specified CUSIP.
	 * Uses caching to speed up the lookup and avoid DB access.  Most of the time only one {@link InvestmentSecurity} will be returned.
	 */
	public List<InvestmentSecurity> getInvestmentSecurityListBySedol(String sedol);


	/**
	 * Returns InvestmentSecurity with the specified CUSIP.
	 * Uses caching to speed up the lookup and avoid DB access.  Most of the time only one {@link InvestmentSecurity} will be returned.
	 */
	public List<InvestmentSecurity> getInvestmentSecurityListByIsin(String isin);


	public List<InvestmentSecurity> getInvestmentSecurityList(SecuritySearchForm searchForm);


	public InvestmentSecurity saveInvestmentSecurity(InvestmentSecurity security);


	public void deleteInvestmentSecurity(int id);
}
