package com.clifton.investment.instrument.event.action;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.Date;


/**
 * The InvestmentSecurityEventAction class keeps track of whether an action of a given type was executed for a given security event.
 * It is important to keep track of this because most actions are not re-runnable (historical price adjustments after a stock split)
 * and may need to be reversed.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityEventAction extends BaseSimpleEntity<Integer> {

	private InvestmentSecurityEvent securityEvent;
	private InvestmentSecurityEventActionType actionType;

	private Date processedDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEvent getSecurityEvent() {
		return this.securityEvent;
	}


	public void setSecurityEvent(InvestmentSecurityEvent securityEvent) {
		this.securityEvent = securityEvent;
	}


	public InvestmentSecurityEventActionType getActionType() {
		return this.actionType;
	}


	public void setActionType(InvestmentSecurityEventActionType actionType) {
		this.actionType = actionType;
	}


	public Date getProcessedDate() {
		return this.processedDate;
	}


	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}
}
