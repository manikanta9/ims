package com.clifton.investment.instrument.allocation.dynamic.calculator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.search.InvestmentReplicationAllocationSearchForm;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.column.value.SystemColumnValueService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * InvestmentSecurityAllocationForReplicationWeightsDynamicCalculator uses user input allocations
 * and selected replication (custom field on the benchmark) to dynamically generate weights for a
 * specific day.
 * <p>
 * Example: Allocated Synthetic Benchmark Syn Russell 1000 2 Contract contains 2 allocations
 * 1. SPTR-SYN (Underlying of S&P 500 Mini)
 * 2. SYN S&P MID CAP (Underlying of S&P 400 Mini)
 * The linked Replication: DE Russell 1000 2 Contract contains 2 allocations
 * 1. S&P 500 Mini
 * 2. S&P 400 Mini
 * <p>
 * Weights will be updated on the replication, this calculator then gets the weight active on a specific date
 * and uses them for the 2 synthetic allocations in order to calculate it's market data values.
 * <p>
 * Additional options are available to select a second replication, and condition on which replication allocations to include. This is used for CCY Synthetics, which follow the above logic
 * and then a second replication is used to adjust the return weight by the % of non-USD denominated allocations.
 *
 * @author manderson
 */
public class InvestmentSecurityAllocationForReplicationWeightsDynamicCalculator implements InvestmentSecurityAllocationDynamicCalculator {

	private InvestmentReplicationService investmentReplicationService;

	private SystemColumnValueService systemColumnValueService;
	private SystemConditionService systemConditionService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	/**
	 * Historically we've sometimes used month start or month end weights depending on the type of calculation
	 * Going forward we plan to always use month start, but as an option, can use either
	 */
	private boolean useMonthEndWeights;

	/**
	 * If a second replication is supplied to adjust return weights by, this condition is used to determine which weights to include
	 * For example, CCY Synthetics adjust return weights by non-USD denominated securities in the second replication
	 */
	private Integer adjustmentAllocationConditionId;


	@Override
	public List<InvestmentSecurityAllocation> calculate(InvestmentSecurity security, List<InvestmentSecurityAllocation> allocationList, Date measureDate) {
		ValidationUtils.assertNotEmpty(allocationList, "At least one allocation is required to be entered in order to map weights from the replication.");
		Integer replicationId = (Integer) getSystemColumnValueService().getSystemColumnCustomValue(security.getId(), InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_INVESTMENT_REPLICATION);
		ValidationUtils.assertNotNull(replicationId, "Replication selection for security [" + security.getLabel() + "] is required in order to dynamically generate security allocation weights.");

		List<InvestmentReplicationAllocation> replicationAllocationList = getReplicationAllocationList(replicationId, measureDate);

		List<InvestmentSecurityAllocation> newAllocationList = new ArrayList<>();
		BigDecimal total = BigDecimal.ZERO;
		for (InvestmentSecurityAllocation allocation : allocationList) {
			InvestmentSecurityAllocation newAllocation = BeanUtils.cloneBean(allocation, false, false);
			// Clear Note and Weight so we can set with calculation results
			newAllocation.setId(allocation.getId()); // Set ID for Drill Down Support in UI
			newAllocation.setAllocationWeight(BigDecimal.ZERO);
			newAllocation.setNote("");
			boolean found = false;
			for (InvestmentReplicationAllocation replicationAllocation : replicationAllocationList) {
				if (isSecurityAllocationMatchReplicationAllocation(allocation, replicationAllocation)) {
					// Not the First One - add a comma
					if (found) {
						newAllocation.setNote(newAllocation.getNote() + ", ");
					}
					else {
						newAllocation.setNote("Matched to: ");
					}
					found = true;
					newAllocation.setAllocationWeight(MathUtils.add(newAllocation.getAllocationWeight(), MathUtils.getPercentageOf(allocation.getAllocationWeight(), replicationAllocation.getAllocationWeight(), true)));
					newAllocation.setNote(newAllocation.getNote() + replicationAllocation.getAllocationLabel() + ": " + CoreMathUtils.formatNumberDecimal(replicationAllocation.getAllocationWeight()));
					total = MathUtils.add(total, replicationAllocation.getAllocationWeight());
				}
			}
			if (!found) {
				newAllocation.setNote(DYNAMIC_NOTE_ERROR_PREFIX + "Could not find a matching replication allocation.");
			}
			newAllocationList.add(newAllocation);
		}

		Integer adjustmentReplicationId = (Integer) getSystemColumnValueService().getSystemColumnCustomValue(security.getId(), InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_INVESTMENT_REPLICATION_2);
		if (adjustmentReplicationId != null) {
			replicationAllocationList = getReplicationAllocationList(adjustmentReplicationId, measureDate);
			SystemCondition condition = (getAdjustmentAllocationConditionId() == null ? null : getSystemConditionService().getSystemCondition(getAdjustmentAllocationConditionId()));
			BigDecimal totalWeight = BigDecimal.ZERO;
			for (InvestmentReplicationAllocation allocation : replicationAllocationList) {
				boolean add = (condition == null || getSystemConditionEvaluationHandler().isConditionTrue(condition, allocation));
				if (add) {
					totalWeight = MathUtils.add(totalWeight, allocation.getAllocationWeight());
				}
			}

			for (InvestmentSecurityAllocation newAllocation : newAllocationList) {
				newAllocation.setNote(newAllocation.getNote() + "<br>Original Weight: " + CoreMathUtils.formatNumberDecimal(newAllocation.getAllocationWeight()) + ", Adjusted by: " + CoreMathUtils.formatNumberMoney(totalWeight) + "%");
				newAllocation.setAllocationWeight(MathUtils.getPercentageOf(totalWeight, newAllocation.getAllocationWeight(), true));
			}
		}

		return newAllocationList;
	}


	private List<InvestmentReplicationAllocation> getReplicationAllocationList(Integer replicationId, Date measureDate) {
		InvestmentReplication replication = getInvestmentReplicationService().getInvestmentReplication(replicationId);
		ValidationUtils.assertFalse(replication.isDynamic(), "Dynamic replications are not currently supported");
		Date replicationActiveOnDate = getReplicationAllocationsActiveOnDate(measureDate);
		InvestmentReplicationAllocationSearchForm searchForm = new InvestmentReplicationAllocationSearchForm();
		searchForm.setReplicationId(replicationId);
		searchForm.setActiveOnDate(replicationActiveOnDate);

		// Get the List of Replication Allocations Active On the given Date
		// Note - We use this method because if the replication is dynamic as well, it will automatically calculate weights/securities
		List<InvestmentReplicationAllocation> replicationAllocationList = getInvestmentReplicationService().getInvestmentReplicationAllocationList(searchForm);
		ValidationUtils.assertNotEmpty(replicationAllocationList, "Replication [" + replication.getName() + "] does not have any active allocations on [" + DateUtils.fromDateShort(replicationActiveOnDate) + "].");
		return replicationAllocationList;
	}


	private Date getReplicationAllocationsActiveOnDate(Date measureDate) {
		if (isUseMonthEndWeights()) {
			return DateUtils.getEndOfDay(DateUtils.getLastDayOfMonth(measureDate));
		}
		return DateUtils.getEndOfDay(DateUtils.getFirstDayOfMonth(measureDate));
	}


	private boolean isSecurityAllocationMatchReplicationAllocation(InvestmentSecurityAllocation allocation, InvestmentReplicationAllocation replicationAllocation) {
		ValidationUtils.assertNull(replicationAllocation.getReplicationSecurityGroup(), "Cannot use replications that have security groups used in allocations");
		InvestmentInstrument allocationInstrument = allocation.getInvestmentSecurity() != null ? allocation.getInvestmentSecurity().getInstrument() : allocation.getInvestmentInstrument();
		InvestmentInstrument replicationAllocationInstrument = replicationAllocation.getReplicationSecurity() != null ? replicationAllocation.getReplicationSecurity().getInstrument() : replicationAllocation.getReplicationInstrument();

		// Same - Consider a Match
		if (allocationInstrument.equals(replicationAllocationInstrument)) {
			return true;
		}
		// Underlying of security allocation = replication Security's instrument
		if (allocationInstrument.getUnderlyingInstrument() != null) {
			if (allocationInstrument.getUnderlyingInstrument().equals(replicationAllocationInstrument)) {
				return true;
			}
			// Underlying of Security Allocation = Replication Security's Underlying
			if (replicationAllocationInstrument.getUnderlyingInstrument() != null) {
				if (allocationInstrument.getUnderlyingInstrument().equals(replicationAllocationInstrument.getUnderlyingInstrument())) {
					return true;
				}
			}
		}
		return false;
	}


	///////////////////////////////////////////////////////////////
	//////////        Getter & Setter Methods          ////////////
	///////////////////////////////////////////////////////////////


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public boolean isUseMonthEndWeights() {
		return this.useMonthEndWeights;
	}


	public void setUseMonthEndWeights(boolean useMonthEndWeights) {
		this.useMonthEndWeights = useMonthEndWeights;
	}


	public Integer getAdjustmentAllocationConditionId() {
		return this.adjustmentAllocationConditionId;
	}


	public void setAdjustmentAllocationConditionId(Integer adjustmentAllocationConditionId) {
		this.adjustmentAllocationConditionId = adjustmentAllocationConditionId;
	}
}
