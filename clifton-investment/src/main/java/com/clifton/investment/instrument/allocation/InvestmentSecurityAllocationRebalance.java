package com.clifton.investment.instrument.allocation;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>InvestmentSecurityAllocationRebalance</code> ...
 *
 * @author manderson
 */
@NonPersistentObject
public class InvestmentSecurityAllocationRebalance {

	private final Date rebalanceDate;

	private boolean securityStart;

	private final Map<String, String> startSecurityRebalanceInformationMap = new HashMap<>();
	private final Map<String, String> endSecurityRebalanceInformationMap = new HashMap<>();
	private final Map<String, String> rebalSecurityRebalanceInformationMap = new HashMap<>();


	public InvestmentSecurityAllocationRebalance(Date rebalanceDate) {
		this.rebalanceDate = rebalanceDate;
	}


	public InvestmentSecurityAllocationRebalance(Date rebalanceDate, boolean securityStart) {
		this.rebalanceDate = rebalanceDate;
		this.securityStart = securityStart;
	}


	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof InvestmentSecurityAllocationRebalance)) {
			return false;
		}
		InvestmentSecurityAllocationRebalance rebalObj = (InvestmentSecurityAllocationRebalance) obj;
		return DateUtils.compare(rebalObj.getRebalanceDate(), this.rebalanceDate, false) == 0;
	}


	@Override
	public int hashCode() {
		return this.rebalanceDate.hashCode();
	}


	/////////////////////////////////////////////////////////////


	/**
	 * If ended - shows end note
	 * If started - shows start note
	 * If Rebalanced - shows start note only
	 *
	 * @param start
	 * @param allocation
	 */
	public void addSecurityAllocation(boolean start, InvestmentSecurityAllocation allocation) {
		if (allocation != null) {
			String symbol = (allocation.getInvestmentSecurity() != null ? allocation.getInvestmentSecurity().getSymbol() : allocation.getInvestmentInstrument().getIdentifierPrefix());

			if (start) {
				if (this.endSecurityRebalanceInformationMap.containsKey(symbol)) {
					this.rebalSecurityRebalanceInformationMap.put(symbol, allocation.getNote());
					this.endSecurityRebalanceInformationMap.remove(symbol);
				}
				else {
					this.startSecurityRebalanceInformationMap.put(symbol, allocation.getNote());
				}
			}
			else {
				if (this.startSecurityRebalanceInformationMap.containsKey(symbol)) {
					this.rebalSecurityRebalanceInformationMap.put(symbol, this.startSecurityRebalanceInformationMap.remove(symbol));
				}
				else {
					this.endSecurityRebalanceInformationMap.put(symbol, allocation.getNote());
				}
			}
		}
	}


	public String getDescription() {
		StringBuilder sb = new StringBuilder(16);
		if (isSecurityStart()) {
			sb.append("Security Start Date");
		}

		appendMapToDescription(sb, "Ended Allocations: ", this.endSecurityRebalanceInformationMap);
		appendMapToDescription(sb, "Started Allocations: ", this.startSecurityRebalanceInformationMap);
		appendMapToDescription(sb, "Rebalanced Allocations: ", this.rebalSecurityRebalanceInformationMap);
		return sb.toString();
	}


	private void appendMapToDescription(StringBuilder sb, String prefix, Map<String, String> allocationMap) {
		if (!CollectionUtils.isEmpty(allocationMap)) {
			sb.append("<b>").append(prefix).append("</b>");
			boolean first = true;
			for (Map.Entry<String, String> stringStringEntry : allocationMap.entrySet()) {
				if (!first) {
					sb.append(", ");
				}
				first = false;
				sb.append(stringStringEntry.getKey()).append(StringUtils.isEmpty(stringStringEntry.getValue()) ? "" : " (" + stringStringEntry.getValue() + ")");
			}
			sb.append("<br/>");
		}
	}


	/////////////////////////////////////////////////////////////


	public Date getRebalanceDate() {
		return this.rebalanceDate;
	}


	public boolean isSecurityStart() {
		return this.securityStart;
	}
}
