package com.clifton.investment.instrument.upload.allocation;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.upload.SystemUploadCommand;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentSecurityAllocationUploadCommand</code> extends the System Upload
 * however has simple version that allows selecting the parent security and if rebalancing
 * is supported - recalculating the rebalance
 *
 * @author Mary Anderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class InvestmentSecurityAllocationUploadCommand extends SystemUploadCommand {

	/**
	 * Security that is allocationOfSecurities = true - replaces parent columns in the file
	 */
	private InvestmentSecurity parentInvestmentSecurity;

	/**
	 * Sets the allocations to start on the given date (unless same as security start date, then will leave blank to follow security start date)
	 */
	private Date startDate;

	/**
	 * Start/End Dates in the file will be ignored.
	 * <p/>
	 * Can only be selected if start date is not null.
	 * If true, will retrieve all allocations active on the start date
	 * and attempt to match each allocation to the upload file.  Those
	 * missing from the file (or entered with weight/shares = 0) will be ended on start date -1.
	 * Those that match with no change will not be changes.  Those that match with a change will
	 * be ended on start date - 1 and new allocation will be started on start date.
	 */
	private boolean applyAsFullAllocationListOnStartDate;

	/**
	 * If true, will recalculate shares/weights for selected custom allocation on start date
	 */
	private boolean recalculateRebalanceOnStartDate;

	private Short dataSourceId;

	private BigDecimal startingPrice;

	private boolean updateWeights;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityAllocationUploadCommand() {
		super();
		setAdditionalBeanPropertiesToInclude("investmentSecurity.sedol", "investmentSecurity.cusip", "investmentSecurity.figi", "investmentSecurity.isin", "overrideExchange.exchangeCode");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return "InvestmentSecurityAllocation";
	}


	@Override
	@ValueIgnoringGetter
	public boolean isSimple() {
		return true;
	}

	// Overrides - Investment Security Allocation Upload is all or nothing, do not insert fk beans, and


	@Override
	@ValueIgnoringGetter
	public boolean isPartialUploadAllowed() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getParentInvestmentSecurity() {
		return this.parentInvestmentSecurity;
	}


	public void setParentInvestmentSecurity(InvestmentSecurity parentInvestmentSecurity) {
		this.parentInvestmentSecurity = parentInvestmentSecurity;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public boolean isRecalculateRebalanceOnStartDate() {
		return this.recalculateRebalanceOnStartDate;
	}


	public void setRecalculateRebalanceOnStartDate(boolean recalculateRebalanceOnStartDate) {
		this.recalculateRebalanceOnStartDate = recalculateRebalanceOnStartDate;
	}


	public BigDecimal getStartingPrice() {
		return this.startingPrice;
	}


	public void setStartingPrice(BigDecimal startingPrice) {
		this.startingPrice = startingPrice;
	}


	public boolean isUpdateWeights() {
		return this.updateWeights;
	}


	public void setUpdateWeights(boolean updateWeights) {
		this.updateWeights = updateWeights;
	}


	public boolean isApplyAsFullAllocationListOnStartDate() {
		return this.applyAsFullAllocationListOnStartDate;
	}


	public void setApplyAsFullAllocationListOnStartDate(boolean applyAsFullAllocationListOnStartDate) {
		this.applyAsFullAllocationListOnStartDate = applyAsFullAllocationListOnStartDate;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}
}
