package com.clifton.investment.instrument.currency;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>InvestmentCurrencyServiceImpl</code> class provides basic implementation of InvestmentCurrencyService interface.
 *
 * @author vgomelsky
 */
@Service
public class InvestmentCurrencyServiceImpl implements InvestmentCurrencyService {

	private AdvancedUpdatableDAO<InvestmentCurrencyConvention, Criteria> investmentCurrencyConventionDAO;

	private InvestmentCurrencyConventionCache investmentCurrencyConventionCache;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isInvestmentCurrencyMultiplyConvention(String fromSymbol, String toSymbol) {
		return getCurrencyConventionHolder(fromSymbol, toSymbol).isMultiplyFromByFX();
	}


	@Override
	public Integer getInvestmentCurrencyConventionDaysToSettle(String fromSymbol, String toSymbol) {
		return getCurrencyConventionHolder(fromSymbol, toSymbol).getDaysToSettle();
	}


	@Override
	public String getInvestmentCurrencyConventionDominantCurrency(String currencySymbolOne, String currencySymbolTwo) {
		return getCurrencyConventionHolder(currencySymbolOne, currencySymbolTwo).getDominantCurrencySymbol();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentCurrencyConvention getInvestmentCurrencyConvention(int id) {
		return getInvestmentCurrencyConventionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentCurrencyConvention> getInvestmentCurrencyConventionList(InvestmentCurrencyConventionSearchForm searchForm) {
		return getInvestmentCurrencyConventionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentCurrencyConvention saveInvestmentCurrencyConvention(InvestmentCurrencyConvention convention) {
		// make sure there is no reverse convention that contradicts the one being saved
		List<InvestmentCurrencyConvention> reverseList = getInvestmentCurrencyConventionDAO().findByFields(new String[]{"fromCurrency.id", "toCurrency.id"},
				new Object[]{convention.getToCurrency().getId(), convention.getFromCurrency().getId()});
		InvestmentCurrencyConvention reverse = CollectionUtils.getOnlyElement(reverseList);
		if (reverse != null) {
			if (reverse.isMultiplyFromByFX() == convention.isMultiplyFromByFX()) {
				throw new ValidationException("When currency convention is defined twice between to currencies (from-to and to-from), the multipliers cannot contradict each other.");
			}
		}

		return getInvestmentCurrencyConventionDAO().save(convention);
	}


	@Override
	public void deleteInvestmentCurrencyConvention(int id) {
		getInvestmentCurrencyConventionDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////                 Helper Methods                   //////////////
	////////////////////////////////////////////////////////////////////////////


	private CurrencyConventionHolder getCurrencyConventionHolder(String fromSymbol, String toSymbol) {
		CurrencyConventionHolder convention = getInvestmentCurrencyConventionCache().getInvestmentCurrencyConvention(fromSymbol, toSymbol);
		if (convention == null) {
			getInvestmentCurrencyConventionCache().updateInvestmentCurrencyConventionList(getInvestmentCurrencyConventionDAO().findAll());
			convention = getInvestmentCurrencyConventionCache().getInvestmentCurrencyConvention(fromSymbol, toSymbol);
			ValidationUtils.assertNotNull(convention, "Currency convention between '" + fromSymbol + "' and '" + toSymbol + "' is not defined.");
		}
		return convention;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentCurrencyConvention, Criteria> getInvestmentCurrencyConventionDAO() {
		return this.investmentCurrencyConventionDAO;
	}


	public void setInvestmentCurrencyConventionDAO(AdvancedUpdatableDAO<InvestmentCurrencyConvention, Criteria> investmentCurrencyConventionDAO) {
		this.investmentCurrencyConventionDAO = investmentCurrencyConventionDAO;
	}


	public InvestmentCurrencyConventionCache getInvestmentCurrencyConventionCache() {
		return this.investmentCurrencyConventionCache;
	}


	public void setInvestmentCurrencyConventionCache(InvestmentCurrencyConventionCache investmentCurrencyConventionCache) {
		this.investmentCurrencyConventionCache = investmentCurrencyConventionCache;
	}
}
