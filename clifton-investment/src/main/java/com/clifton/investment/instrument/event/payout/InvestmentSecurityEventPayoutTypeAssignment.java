package com.clifton.investment.instrument.event.payout;

import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;


/**
 * The InvestmentSecurityEventPayoutTypeAssignment class specifies what Payout Types are allow for each Event Type.
 * This information is used by corresponding validation logic to prevent invalid event type selection.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityEventPayoutTypeAssignment extends ManyToManyEntity<InvestmentSecurityEventPayoutType, InvestmentSecurityEventType, Short> {

	// not other attributes for now
}
