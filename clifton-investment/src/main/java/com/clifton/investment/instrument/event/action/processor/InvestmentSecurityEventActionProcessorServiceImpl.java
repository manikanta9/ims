package com.clifton.investment.instrument.event.action.processor;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventAction;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionService;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionType;
import com.clifton.investment.instrument.event.action.search.InvestmentSecurityEventActionSearchForm;
import com.clifton.investment.instrument.event.action.search.InvestmentSecurityEventActionTypeSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class InvestmentSecurityEventActionProcessorServiceImpl implements InvestmentSecurityEventActionProcessorService {

	private InvestmentSecurityEventService investmentSecurityEventService;
	private InvestmentSecurityEventActionService investmentSecurityEventActionService;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status processInvestmentSecurityEventActions(int securityEventId, Boolean processBeforeEventJournal) {
		InvestmentSecurityEvent securityEvent = getInvestmentSecurityEventService().getInvestmentSecurityEvent(securityEventId);
		ValidationUtils.assertNotNull(securityEvent, "Cannot find Investment Security Event with id = " + securityEventId);

		List<InvestmentSecurityEventActionType> actionTypeList = getActionTypeList(securityEvent, processBeforeEventJournal);
		if (CollectionUtils.isEmpty(actionTypeList)) {
			return Status.ofMessage("No Action Types defined for: " + securityEvent);
		}

		// remove action types that have been processed
		Status status = Status.ofMessage("Evaluating " + actionTypeList.size() + " event actions.");
		List<InvestmentSecurityEventAction> actionList = getActionList(securityEventId, processBeforeEventJournal);
		for (InvestmentSecurityEventAction action : CollectionUtils.getIterable(actionList)) {
			if (actionTypeList.remove(action.getActionType())) {
				status.addSkipped(action.getActionType().getName() + " was already processed on " + DateUtils.fromDate(action.getProcessedDate()));
			}
		}

		// process remaining actions
		actionTypeList = BeanUtils.sortWithFunction(actionTypeList, InvestmentSecurityEventActionType::getProcessingOrder, true);
		for (InvestmentSecurityEventActionType actionType : actionTypeList) {
			InvestmentSecurityEventActionProcessor processor = (InvestmentSecurityEventActionProcessor) getSystemBeanService().getBeanInstance(actionType.getActionBean());
			if (processor.processAction(securityEvent, actionType)) {
				status.addMessage(actionType.getName() + " was successfully processed");
				status.setActionPerformed(true);
			}
			else {
				status.addSkipped(actionType.getName() + " has no data to be processed");
			}
		}

		return status;
	}


	@Override
	public Status rollbackInvestmentSecurityEventActions(int securityEventId, Boolean processBeforeEventJournal) {
		InvestmentSecurityEvent securityEvent = getInvestmentSecurityEventService().getInvestmentSecurityEvent(securityEventId);
		ValidationUtils.assertNotNull(securityEvent, "Cannot find Investment Security Event with id = " + securityEventId);

		List<InvestmentSecurityEventAction> actionList = getActionList(securityEventId, processBeforeEventJournal);
		if (CollectionUtils.isEmpty(actionList)) {
			return Status.ofMessage("No Action(s) found for: " + securityEvent);
		}

		Status status = Status.ofMessage("Rolling back " + actionList.size() + " event actions.");
		for (InvestmentSecurityEventAction action : CollectionUtils.getIterable(actionList)) {
			InvestmentSecurityEventActionProcessor processor = (InvestmentSecurityEventActionProcessor) getSystemBeanService().getBeanInstance(action.getActionType().getActionBean());
			processor.rollbackAction(action);
			status.addMessage(action.getActionType().getName() + " was successfully rolled back.");
			status.setActionPerformed(true);
		}

		return status;
	}


	@Override
	public boolean isInvestmentSecurityEventActionPendingProcessing(int securityEventId, Boolean beforeEventJournal) {
		InvestmentSecurityEvent securityEvent = getInvestmentSecurityEventService().getInvestmentSecurityEvent(securityEventId);
		ValidationUtils.assertNotNull(securityEvent, "Cannot find Investment Security Event with id = " + securityEventId);

		List<InvestmentSecurityEventActionType> actionTypeList = getActionTypeList(securityEvent, beforeEventJournal);
		if (!CollectionUtils.isEmpty(actionTypeList)) {
			// remove action types that have been processed
			List<InvestmentSecurityEventAction> actionList = getActionList(securityEventId, beforeEventJournal);
			for (InvestmentSecurityEventAction action : CollectionUtils.getIterable(actionList)) {
				actionTypeList.remove(action.getActionType());
			}
			// remove action types that would have no impact: nothing to be processed
			for (InvestmentSecurityEventActionType actionType : actionTypeList) {
				InvestmentSecurityEventActionProcessor processor = (InvestmentSecurityEventActionProcessor) getSystemBeanService().getBeanInstance(actionType.getActionBean());
				if (!processor.isDataAvailableForProcessing(securityEvent)) {
					actionTypeList.remove(actionType);
				}
			}
		}

		return !CollectionUtils.isEmpty(actionTypeList);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<InvestmentSecurityEventActionType> getActionTypeList(InvestmentSecurityEvent securityEvent, Boolean processBeforeEventJournal) {
		if (!securityEvent.getType().isActionAllowed()) {
			return null;
		}

		InvestmentSecurityEventActionTypeSearchForm searchForm = new InvestmentSecurityEventActionTypeSearchForm();
		searchForm.setEventTypeId(securityEvent.getType().getId());
		searchForm.setProcessBeforeEventJournal(processBeforeEventJournal);
		List<InvestmentSecurityEventActionType> typeList = getInvestmentSecurityEventActionService().getInvestmentSecurityEventActionTypeList(searchForm);
		return BeanUtils.sortWithFunction(typeList, InvestmentSecurityEventActionType::getProcessingOrder, true);
	}


	private List<InvestmentSecurityEventAction> getActionList(int securityEventId, Boolean processBeforeEventJournal) {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setSecurityEventId(securityEventId);
		searchForm.setProcessBeforeEventJournal(processBeforeEventJournal);
		return getInvestmentSecurityEventActionService().getInvestmentSecurityEventActionList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public InvestmentSecurityEventActionService getInvestmentSecurityEventActionService() {
		return this.investmentSecurityEventActionService;
	}


	public void setInvestmentSecurityEventActionService(InvestmentSecurityEventActionService investmentSecurityEventActionService) {
		this.investmentSecurityEventActionService = investmentSecurityEventActionService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
