package com.clifton.investment.instrument.allocation.calculator;


import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;


public interface InvestmentSecurityAllocationCalculatorLocator {

	/**
	 * Returns InvestmentSecurityAllocationCalculator for the specified allocationType
	 */
	public InvestmentSecurityAllocationCalculator locate(InvestmentSecurityAllocationTypes allocationType);


	/**
	 * Returns InvestmentSecurityAllocationRebalanceCalculator for the specified allocationType
	 */
	public InvestmentSecurityAllocationRebalanceCalculator locateForRebalance(InvestmentSecurityAllocationTypes allocationType);
}
