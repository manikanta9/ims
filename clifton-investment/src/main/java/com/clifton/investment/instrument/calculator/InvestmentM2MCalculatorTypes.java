package com.clifton.investment.instrument.calculator;


/**
 * The <code>InvestmentM2MCalculatorTypes</code> enum defines different types of Mark to Market calculations.
 * <p>
 * Different types of securities may have different rules for calculating daily mark to market transfer amount.
 * For example, futures generally have the mark equal to the change in open trade equity.
 * LME's have full mark on maturity, etc.
 *
 * @author vgomelsky
 */
public enum InvestmentM2MCalculatorTypes {

	/**
	 * Change in open trade equity from yesterday. On opening day, use opening price instead of previous settlement price.
	 * On closing day use closing price instead of today's settlement price.
	 * This type is used by most futures.
	 */
	DAILY_OTE_CHANGE,

	/**
	 * Mark to Market = BASE NPV + Realized - Commission - Prior BASE NPV
	 * <p>
	 * NPV (Net Present Value) == Market Value
	 * Used by Cleared Swaps for M2M calculations
	 */
	DAILY_NPV_CHANGE,

	/**
	 * Mark to Market = BASE NPV + Realized - Commission - Prior BASE NPV + Realized Adjustment
	 * <p>
	 * NPV (Net Present Value) == Market Value
	 * Used by Cleared CDS on ICE for M2M calculations. Realized gain/loss from Coupons and Credit Events is included one day earlier: Transaction Date vs Settlement Date
	 */
	DAILY_NPV_CHANGE_REALIZED_ON_TRANSACTION,

	/**
	 * Cash flows: payment for cost + commission on open (0 for LME); payment of principal + commission on close.
	 * This type is used by by LME's and options on index futures.
	 */
	COST_AND_REALIZED,

	/**
	 * Total Gain/Loss for today: realized + unrealized + accrual. Also known as NPV Change.
	 */
	DAILY_GAIN_LOSS,

	/**
	 * Do not apply mark to market (zero). Can be used to exclude a security from the mark.
	 */
	NONE
}
