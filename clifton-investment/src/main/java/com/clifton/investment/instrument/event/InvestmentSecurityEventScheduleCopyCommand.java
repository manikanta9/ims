package com.clifton.investment.instrument.event;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>InvestmentSecurityEventScheduleCopyCommand</code> defines the options for copying the schedule of the source instrument or security to the destination security
 *
 * @author JasonS
 */
@NonPersistentObject
public class InvestmentSecurityEventScheduleCopyCommand {

	private InvestmentSecurity destinationSecurity;

	/**
	 * Can be used if it is 1-to-1 with a security, if not an error will be thrown
	 */
	private InvestmentInstrument sourceInstrument;

	/**
	 * This field should be used when the instrument selected doesn't have a 1-to-1 relationship with a security
	 */
	private InvestmentSecurity sourceSecurity;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getDestinationSecurity() {
		return this.destinationSecurity;
	}


	public void setDestinationSecurity(InvestmentSecurity destinationSecurity) {
		this.destinationSecurity = destinationSecurity;
	}


	public InvestmentInstrument getSourceInstrument() {
		return this.sourceInstrument;
	}


	public void setSourceInstrument(InvestmentInstrument sourceInstrument) {
		this.sourceInstrument = sourceInstrument;
	}


	public InvestmentSecurity getSourceSecurity() {
		return this.sourceSecurity;
	}


	public void setSourceSecurity(InvestmentSecurity sourceSecurity) {
		this.sourceSecurity = sourceSecurity;
	}
}
