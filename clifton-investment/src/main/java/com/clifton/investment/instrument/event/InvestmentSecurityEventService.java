package com.clifton.investment.instrument.event;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventDetailSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventExtendedSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventStatusSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventTypeSearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentSecurityEventService</code> interface defines methods for working with investment security events.
 *
 * @author vgomelsky
 */
public interface InvestmentSecurityEventService {

	////////////////////////////////////////////////////////////////////////////
	//////////       Investment Security Event Methods              ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEvent getInvestmentSecurityEvent(int id);


	/**
	 * Returns the first InvestmentSecurityEvent of the specified type that that falls into the specified accrual period.
	 * First event with RecordDate > accrualEndDate AND DeclareDate <= accrualEndDate.
	 * <p>
	 * NOTE: DeclareDate == StartDate; RecordDate == EndDate (End Of Day convention for both dates)
	 * <p>
	 * Use this method to get current event for bond Factor Change, Cash Coupon Payment events.
	 */
	public InvestmentSecurityEvent getInvestmentSecurityEventForAccrualEndDate(int securityId, String typeName, Date accrualEndDate);


	/**
	 * Returns the first InvestmentSecurityEvent of the specified type that falls into the period right before the specified accrual period.
	 * <p>
	 * Use this method to get current event for bond Factor Change, Cash Coupon Payment events.
	 */
	public InvestmentSecurityEvent getInvestmentSecurityEventPreviousForAccrualEndDate(int securityId, String typeName, Date accrualEndDate);


	/**
	 * Returns the latest InvestmentSecurityEvent of the specified type for the specified security such that Ex Date of the event
	 * is before or on the specified valuationDate. Can be used to lookup effective Credit Event for a CDS trade or Factor Change for ABS.
	 */
	public InvestmentSecurityEvent getInvestmentSecurityEventWithLatestExDate(int securityId, String typeName, Date valuationDate);


	/**
	 * Returns list of matching events for same eventDate, Event Type, and Security.Instrument.id
	 */
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventListSameForInstrument(InvestmentSecurityEvent event);


	/**
	 * Returns list of matching events for same eventDate, Event Type, and Security.Instrument.UnderlyingInstrument.id
	 */
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventListSameForInstrumentUnderlying(InvestmentSecurityEvent event);


	public List<InvestmentSecurityEvent> getInvestmentSecurityEventList(InvestmentSecurityEventSearchForm searchForm);


	@SecureMethod(dtoClass = InvestmentSecurityEvent.class)
	public List<InvestmentSecurityEventExtended> getInvestmentSecurityEventExtendedList(InvestmentSecurityEventExtendedSearchForm searchForm);


	public InvestmentSecurityEvent saveInvestmentSecurityEvent(InvestmentSecurityEvent bean);


	@SecureMethod(dtoClass = InvestmentSecurityEvent.class)
	public void saveInvestmentSecurityEvents(List<InvestmentSecurityEvent> list);


	public void saveInvestmentSecurityEventList(List<InvestmentSecurityEvent> newList, List<InvestmentSecurityEvent> originalList);


	/**
	 * Deletes InvestmentSecurityEvent with the specified id and corresponding event details.
	 */
	public void deleteInvestmentSecurityEvent(int id);


	////////////////////////////////////////////////////////////////////////////
	//////////      Investment Security Event Detail Methods        ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventDetail getInvestmentSecurityEventDetail(int id);


	public InvestmentSecurityEventDetail getInvestmentSecurityEventDetailByEventAndEffectiveDate(int securityEventId, Date effectiveDate);


	public List<InvestmentSecurityEventDetail> getInvestmentSecurityEventDetailList(InvestmentSecurityEventDetailSearchForm searchForm);


	public List<InvestmentSecurityEventDetail> getInvestmentSecurityEventDetailListByEvent(int securityEventId);


	public InvestmentSecurityEventDetail saveInvestmentSecurityEventDetail(InvestmentSecurityEventDetail eventDetail);


	public void saveInvestmentSecurityEventDetailList(List<InvestmentSecurityEventDetail> beanList);


	public void deleteInvestmentSecurityEventDetail(int id);


	public void deleteInvestmentSecurityEventDetailList(List<InvestmentSecurityEventDetail> detailList);


	////////////////////////////////////////////////////////////////////////////
	//////////     Investment Security Event Status Methods         ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventStatus getInvestmentSecurityEventStatus(short id);


	public InvestmentSecurityEventStatus getInvestmentSecurityEventStatusByName(String name);


	public List<InvestmentSecurityEventStatus> getInvestmentSecurityEventStatusList(InvestmentSecurityEventStatusSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	//////////     Investment Security Event Type Methods           ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventType getInvestmentSecurityEventType(short id);


	public InvestmentSecurityEventType getInvestmentSecurityEventTypeByName(String name);


	@ModelAttribute("result")
	@SecureMethod(dtoClass = InvestmentSecurityEventType.class)
	public boolean isInvestmentSecurityEventTypeAllowedForHierarchy(short hierarchyId, String eventTypeName);


	@ModelAttribute("result")
	@SecureMethod(dtoClass = InvestmentSecurityEventType.class)
	public boolean isInvestmentSecurityEventTypeAllowedForSecurity(int securityId, String eventTypeName);


	public List<InvestmentSecurityEventType> getInvestmentSecurityEventTypeList(InvestmentSecurityEventTypeSearchForm searchForm);


	public List<InvestmentSecurityEventType> getInvestmentSecurityEventTypeListByHierarchy(short hierarchyId);


	////////////////////////////////////////////////////////////////////////////
	///////      Investment Security Event Type Hierarchy Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventTypeHierarchy getInvestmentSecurityEventTypeHierarchy(int id);


	public InvestmentSecurityEventTypeHierarchy getInvestmentSecurityEventTypeHierarchyByEventTypeAndHierarchy(short eventTypeId, short instrumentHierarchyId);


	public List<InvestmentSecurityEventTypeHierarchy> getInvestmentSecurityEventTypeHierarchyListByHierarchy(short hierarchyId);


	public List<InvestmentSecurityEventTypeHierarchy> getInvestmentSecurityEventTypeHierarchyListByEventType(short eventTypeId);


	public InvestmentSecurityEventTypeHierarchy linkInvestmentSecurityEventTypeHierarchy(short eventTypeId, short instrumentHierarchyId);


	public InvestmentSecurityEventTypeHierarchy saveInvestmentSecurityEventTypeHierarchy(InvestmentSecurityEventTypeHierarchy bean);


	public void deleteInvestmentSecurityEventTypeHierarchy(int id);
}
