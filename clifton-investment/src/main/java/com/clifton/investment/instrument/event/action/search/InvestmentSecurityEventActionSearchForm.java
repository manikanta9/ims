package com.clifton.investment.instrument.event.action.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * @author vgomelsky
 */
public class InvestmentSecurityEventActionSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "securityEvent.security.name,actionType.name,securityEvent.type.name,securityEvent.status.name")
	private String searchPattern;

	@SearchField(searchField = "securityEvent.id")
	private Integer securityEventId;

	@SearchField(searchField = "security.id", searchFieldPath = "securityEvent")
	private Integer investmentSecurityId;

	@SearchField(searchField = "name", searchFieldPath = "securityEvent.security")
	private String investmentSecurityName;

	@SearchField(searchField = "actionType.id")
	private Short actionTypeId;

	@SearchField(searchField = "name", searchFieldPath = "actionType")
	private String actionTypeName;

	@SearchField(searchFieldPath = "actionType")
	private Short processingOrder;

	@SearchField(searchFieldPath = "actionType")
	private Boolean processBeforeEventJournal;

	@SearchField(searchFieldPath = "securityEvent")
	private Date eventDate;

	@SearchField(dateFieldIncludesTime = true)
	private Date processedDate;

	@SearchField(searchField = "type.id", searchFieldPath = "securityEvent")
	private Short eventTypeId;

	@SearchField(searchField = "status.id", searchFieldPath = "securityEvent")
	private Short eventStatusId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getSecurityEventId() {
		return this.securityEventId;
	}


	public void setSecurityEventId(Integer securityEventId) {
		this.securityEventId = securityEventId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public String getInvestmentSecurityName() {
		return this.investmentSecurityName;
	}


	public void setInvestmentSecurityName(String investmentSecurityName) {
		this.investmentSecurityName = investmentSecurityName;
	}


	public Short getActionTypeId() {
		return this.actionTypeId;
	}


	public void setActionTypeId(Short actionTypeId) {
		this.actionTypeId = actionTypeId;
	}


	public String getActionTypeName() {
		return this.actionTypeName;
	}


	public void setActionTypeName(String actionTypeName) {
		this.actionTypeName = actionTypeName;
	}


	public Short getProcessingOrder() {
		return this.processingOrder;
	}


	public void setProcessingOrder(Short processingOrder) {
		this.processingOrder = processingOrder;
	}


	public Boolean getProcessBeforeEventJournal() {
		return this.processBeforeEventJournal;
	}


	public void setProcessBeforeEventJournal(Boolean processBeforeEventJournal) {
		this.processBeforeEventJournal = processBeforeEventJournal;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Date getProcessedDate() {
		return this.processedDate;
	}


	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}


	public Short getEventStatusId() {
		return this.eventStatusId;
	}


	public void setEventStatusId(Short eventStatusId) {
		this.eventStatusId = eventStatusId;
	}
}

