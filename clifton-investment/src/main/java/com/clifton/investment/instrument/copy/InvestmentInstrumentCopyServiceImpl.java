package com.clifton.investment.instrument.copy;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractPartyRole;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.contract.BusinessContractTypes;
import com.clifton.business.contract.search.BusinessContractSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.event.EventObject;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.delivery.InvestmentDeliveryMonthCodes;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationHandler;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * <code>InvestmentInstrumentCopyServiceImpl</code> provides services for copying
 * investment data types from an existing one.
 *
 * @author NickK
 */
@Service
public class InvestmentInstrumentCopyServiceImpl implements InvestmentInstrumentCopyService {

	/**
	 * Regex to get strike from Option symbols.
	 * Example symbols include: "IWM FLEX 05/24/19 P137.00", "RXM9P 165.50", "PFE US 06/21/19 C42", "SPX OTC 12/08/14 C1953.37", "EEM 03/18/16 C48 (PGDE-BANA)", "3690 03302022 260.00P"
	 * <br/>- match C, P, Put[s], or Call[s] with 0-2 spaces before strike (prefix)
	 * <br/>- match C, P, Put[s], or Call[s] with 0-2 spaces after strike (suffix)
	 * <br/>- match the put/call and strike combination from the end of string accounting for some optional words at the end [space, text, -, (, )]
	 */
	private static final Pattern OPTION_SYMBOL_STRIKE_PATTERN = Pattern.compile("[\\w\\s\\d](((?:[CP]|Put[s]?|Call[s]?)\\s{0,2})(\\d{1,5}(?:\\.\\d{0,3})?)|(\\b\\d{1,5}(?:\\.\\d{0,3})?)(\\s{0,2}(?:[CP]|Call[s]?|Put[s]?)))(?:\\b[-\\w()\\s]+)?$");
	/**
	 * Regex for Option symbol date. Examples: "IWM FLEX 05/24/19 P137.00", "3690 03302022 260.00P", "3690 - December 2021 - 300 Puts", "3690 - Dec 2021 - 300 Puts"
	 * <br/>\b - word boundary (dates between prefix and suffix)
	 * <br/>- mm/dd/yyyy or m/d/yy
	 * <br/>- mmddyyyy
	 * <br/>- Month yyyy or Month abbreviation with year
	 */
	private static final Pattern OPTION_SYMBOL_DATE_PATTERN = Pattern.compile(".+\\b((\\d{1,2}/\\d{1,2}/(?:\\d{4}|\\d{2}))|(\\d{2}\\d{2}\\d{4})|((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun[e]?|Jul[y]?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(?:Nov|Dec)(?:ember)?)\\s\\d{4}))\\b.+");

	private BusinessContractService businessContractService;

	private EventHandler eventHandler;

	private InvestmentInstrumentService investmentInstrumentService;

	private InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler;

	private SystemColumnValueHandler systemColumnValueHandler;
	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public InvestmentSecurity saveInvestmentSecurityOptionWithCommand(InvestmentSecurityCopyCommand command) {
		if (command.getStrikePrice() == null) {
			throw new ValidationException("Strike Price is required to create a copy of an Option with a different strike price.");
		}
		InvestmentSecurity copyFromSecurity = command.getSecurityToCopy();
		ValidationUtils.assertTrue(InvestmentUtils.isSecurityOfType(copyFromSecurity, InvestmentType.OPTIONS), "Cannot use Option copy feature with new strike price. Copy from security ["
				+ copyFromSecurity.getLabel() + "] is not an option.");

		if (command.getInstrument() != null && !command.getInstrument().equals(copyFromSecurity.getInstrument())) {
			// The copying of a security can be complicated if we change InvestmentInstruments. If the instruments are different, make sure the
			// provided instrument is for creating an OTC Option from a Listed Option of valid combination of InvestmentType/SubType/SubType2
			// until there is a use case to open it up beyond this.
			InvestmentInstrumentHierarchy templateHierarchy = command.getSecurityToCopy().getInstrument().getHierarchy();
			InvestmentInstrumentHierarchy newHierarchy = command.getInstrument().getHierarchy();
			boolean sameInvestmentType = templateHierarchy.getInvestmentType().equals(newHierarchy.getInvestmentType());
			boolean sameInvestmentTypeSubType = CompareUtils.isEqual(templateHierarchy.getInvestmentTypeSubType(), newHierarchy.getInvestmentTypeSubType());
			ValidationUtils.assertTrue(newHierarchy.isOtc() && sameInvestmentType && sameInvestmentTypeSubType,
					() -> new StringBuilder("Can only create a new security from a template when the new security and template use the same investment instrument ")
							.append("or the new security's instrument is based on an OTC hierarchy with the same investment type and investment subtype2 as ")
							.append("the templates instrument's hierarchy.\nNew Instrument's Hierarchy [otc: ").append(newHierarchy.isOtc())
							.append(", investmentType: ").append(newHierarchy.getInvestmentType())
							.append(", investmentTypeSubType: ").append(newHierarchy.getInvestmentTypeSubType())
							.append(", investmentTypeSubType2: ").append(newHierarchy.getInvestmentTypeSubType2()).append("]\n")
							.append("Templates Instrument's Hierarchy [otc: ").append(templateHierarchy.isOtc())
							.append(", investmentType: ").append(templateHierarchy.getInvestmentType())
							.append(", investmentTypeSubType: ").append(templateHierarchy.getInvestmentTypeSubType())
							.append(", investmentTypeSubType2: ").append(templateHierarchy.getInvestmentTypeSubType2()).append("]").toString());
		}

		copyFromSecurity.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		InvestmentSecurity newSecurity = BeanUtils.cloneBean(copyFromSecurity, false, false);
		newSecurity.setCusip(null); // Used for OTC Securities and will auto generate unique internal cusip
		newSecurity.setReferenceSecurity(command.getReferenceSecurity()); // set the reference security if available
		newSecurity.setDescription(null); // clear description when copying from template
		newSecurity.setOptionType(ObjectUtils.coalesce(command.getOptionType(), command.getSecurityToCopy().getOptionType()));
		newSecurity.setOptionStrikePrice(command.getStrikePrice());
		newSecurity.setOptionStyle(ObjectUtils.coalesce(command.getOptionStyle(), command.getSecurityToCopy().getOptionStyle()));
		newSecurity.setSettlementExpiryTime(ObjectUtils.coalesce(command.getSettlementTime(), command.getSecurityToCopy().getSettlementExpiryTime()));
		newSecurity.setFigi(null);  // Clear the copied FIGI value before saving

		populateOptionBigSecurity(newSecurity, command);
		applyOptionInstrumentUpdate(newSecurity, command);
		populateCopiedSystemColumnValues(newSecurity, command);
		applyOptionExpirationDateUpdate(newSecurity, command);

		return getExistingSecurityOrCreateNewSecurity(newSecurity, command);
	}


	@Override
	@Transactional
	public InvestmentSecurity saveInvestmentSecurityForwardWithCommand(InvestmentSecurityCopyCommand command) {
		ValidationUtils.assertNotNull(command.getSettlementDate(), "Settlement Date is required to create a copy of a Forward.");
		ValidationUtils.assertNotNull(command.getStartDate(), "First Trade Date is required to create a copy of a Forward.");
		InvestmentSecurity copyFromSecurity = command.getSecurityToCopy();

		if (copyFromSecurity == null) {
			ValidationUtils.assertNotNull(command.getInstrument(), "At least one of CopyFromSecurity or InvestmentInstrument must be provided");
			ValidationUtils.assertNotNull(command.getInstrument().getId(), "Cannot lookup security to copy from if InvestmentInstrument does not have ID populated.");
			copyFromSecurity = lookupCurrentSecurityForInstrument(command.getInstrument());
			ValidationUtils.assertNotNull(copyFromSecurity, "Could not find any security to copy from for Instrument [" + command.getInstrument().getLabel() + "]");
			command.setSecurityToCopy(copyFromSecurity);
		}

		InvestmentTypeSubType subType = InvestmentUtils.getSecuritySubType(copyFromSecurity);
		if (subType != null && subType.getName().equals(InvestmentTypeSubType.NON_DELIVERABLE_FORWARDS)) {
			ValidationUtils.assertNotNull(command.getEndDate(), "Fixing Date is required to create a copy of a Non Deliverable Forward.");
		}
		ValidationUtils.assertTrue(InvestmentUtils.isSecurityOfType(copyFromSecurity, InvestmentType.FORWARDS), "Cannot use Forward copy feature. Copy from security ["
				+ copyFromSecurity.getLabel() + "] is not a Forward.");

		InvestmentSecurity newSecurity = BeanUtils.cloneBean(copyFromSecurity, false, false);
		newSecurity.setCusip(null); // Used for OTC Securities and will auto generate unique internal cusip
		newSecurity.setStartDate(command.getStartDate());
		newSecurity.setEndDate(ObjectUtils.coalesce(command.getEndDate(), command.getSettlementDate()));
		newSecurity.setFirstNoticeDate(newSecurity.getEndDate()); // set to null for deliverable and the fixing date for non-deliverable
		newSecurity.setLastDeliveryDate(command.getSettlementDate());
		newSecurity.setFigi(null); // Clear the copied FIGI value before saving

		// Fixing date processing
		if (!newSecurity.getInstrument().isDeliverable()) {
			newSecurity.setFirstDeliveryDate(newSecurity.getEndDate());
		}

		newSecurity.setSymbol(newSecurity.getInstrument().getIdentifierPrefix().split(" ")[0].replace("-", "/") + DateUtils.fromDateISOSimple(newSecurity.getLastDeliveryDate()));
		newSecurity.setName(newSecurity.getInstrument().getName() + " " + DateUtils.fromDateShort(newSecurity.getLastDeliveryDate()));
		newSecurity.setDescription(newSecurity.getName());

		return getExistingSecurityOrCreateNewSecurity(newSecurity, command);
	}


	private InvestmentSecurity lookupCurrentSecurityForInstrument(InvestmentInstrument instrument) {
		// get existing security for that instrument, get most recent by end date
		SecuritySearchForm sfByInstrument = new SecuritySearchForm();
		sfByInstrument.setInstrumentId(instrument.getId());
		sfByInstrument.setOrderBy("endDate:desc");
		sfByInstrument.setLimit(1);
		return CollectionUtils.getOnlyElement(getInvestmentInstrumentService().getInvestmentSecurityList(sfByInstrument));
	}


	@Override
	@Transactional
	public InvestmentSecurity saveInvestmentSecurityFromTemplate(InvestmentSecurity bean, Integer copyFromSecurityId) {
		// Currently used by Swaps - but can be used for any security that is created in UI from
		// a template.  Properly checks if instrument/security is 1:1 and if so, will also create a new instrument
		// for the security.
		List<InvestmentSecurityAllocation> newAllocationList = null;

		if (copyFromSecurityId != null) {
			InvestmentSecurity originalSecurity = getInvestmentInstrumentService().getInvestmentSecurity(copyFromSecurityId);
			if (originalSecurity.isAllocationOfSecurities()) {
				List<InvestmentSecurityAllocation> allocationList = getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationListForParent(copyFromSecurityId);
				newAllocationList = new ArrayList<>();
				for (InvestmentSecurityAllocation alloc : CollectionUtils.getIterable(allocationList)) {
					InvestmentSecurityAllocation newAlloc = BeanUtils.cloneBean(alloc, false, false);
					newAlloc.setId(null);
					newAlloc.setParentInvestmentSecurity(bean);
					newAllocationList.add(newAlloc);
				}
			}
			if (CompareUtils.isEqual(bean.getFigi(), originalSecurity.getFigi())) {
				// Clear the copied FIGI value before saving
				bean.setFigi(null);
			}
		}

		if (!CollectionUtils.isEmpty(bean.getColumnValueList())) {
			// First - Change Custom Column Value Ids to Null so they are saved as new beans
			for (SystemColumnValue val : CollectionUtils.getIterable(bean.getColumnValueList())) {
				val.setId(null);
			}
		}

		// If 1:1 Instrument:Security - Create a copy of the instrument and set it on the bean
		if (bean.getInstrument().getHierarchy().isOneSecurityPerInstrument()) {
			InvestmentInstrument newInstrument = BeanUtils.cloneBean(bean.getInstrument(), false, false);
			newInstrument.setId(null);
			bean.setInstrument(newInstrument);
			// Not necessary to explicitly save new instrument as one to one securities automatically save the instrument during the security save
		}
		// Save Security & Custom Fields
		bean = getInvestmentInstrumentService().saveInvestmentSecurity(bean);
		// If Allocations - Save Them
		if (!CollectionUtils.isEmpty(newAllocationList)) {
			getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocationList(newAllocationList, null);
		}
		return bean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If the Option security template to copy has a big security that is also and option and the new Option security
	 * is to use the same instrument as the template, create the new corresponding big security and set its value on
	 * the new Option security.
	 */
	private void populateOptionBigSecurity(InvestmentSecurity newOptionSecurity, InvestmentSecurityCopyCommand command) {
		// If Big Security is also an Option - make a copy of that as well with new Strike Price
		InvestmentSecurity bigSecurity = command.getSecurityToCopy().getBigSecurity();
		boolean newHasSameInstrument = command.getInstrument() == null || command.getInstrument().equals(command.getSecurityToCopy().getInstrument());
		if (bigSecurity != null && InvestmentUtils.isSecurityOfType(bigSecurity, InvestmentType.OPTIONS) && newHasSameInstrument) {
			InvestmentSecurityCopyCommand bigCommand = new InvestmentSecurityCopyCommand();
			BeanUtils.copyProperties(command, bigCommand);
			bigCommand.setSecurityToCopy(bigSecurity);
			bigCommand.setInstrument(bigSecurity.getInstrument());
			// return existing to avoid failing on big security instead of new security
			bigCommand.setReturnExisting(true);
			bigSecurity = saveInvestmentSecurityOptionWithCommand(bigCommand);
		}
		newOptionSecurity.setBigSecurity(bigSecurity);
	}


	/**
	 * Updates the provided new security's Instrument if it is different from the template security's Instrument. If the new Instrument is OTC,
	 * the new security's name, symbol, counterparty, and ISDA will be populated.
	 * <p>
	 * Returns true if the new security's Instrument is updated and different than the template's Instrument so custom columns values can be updated
	 * accordingly.
	 */
	private boolean applyOptionInstrumentUpdate(InvestmentSecurity newOptionSecurity, InvestmentSecurityCopyCommand command) {
		if (command.getInstrument() != null && !command.getInstrument().equals(command.getSecurityToCopy().getInstrument())) {
			newOptionSecurity.setInstrument(command.getInstrument());
			if (newOptionSecurity.getInstrument().getHierarchy().isOtc() && !command.getSecurityToCopy().getInstrument().getHierarchy().isOtc()) {
				ValidationUtils.assertNotNull(command.getCounterparty(), "Counterparty is required to create a new OTC Option.");
				ValidationUtils.assertNotNull(command.getClientAccount(), "Client Account is required to create a new OTC Option.");
				// update the name to include OTC.
				StringBuilder otcSuffix = new StringBuilder(" (");
				BusinessClient businessClient = command.getClientAccount().getBusinessClient();
				if (businessClient != null && businessClient.getCompany() != null && !StringUtils.isEmpty(businessClient.getCompany().getAbbreviation())) {
					otcSuffix.append(command.getClientAccount().getBusinessClient().getCompany().getAbbreviation()).append('-');
				}
				otcSuffix.append(command.getCounterparty().getAbbreviation()).append(')');
				// Update Symbol to reflect OCT Suffix (e.g. "EEM US 12/15/17 P31.5" -> "EEM 12/15/17 P31.5 (PGDE-BANA)") - 'US' is removed to shorten symbol for reports
				newOptionSecurity.setSymbol(new StringBuilder(newOptionSecurity.getSymbol().replace(" US", "")).append(otcSuffix).toString());
				// Update Name to reflect OCT Suffix (e.g. "EEM US 12/15/17 P31.5" -> "EEM OTC 12/15/17 P31.5 (PGDE-BANA)") - 'US' is removed to shorten symbol for reports
				newOptionSecurity.setName(new StringBuilder(newOptionSecurity.getName().replace(" US", "")).insert(newOptionSecurity.getName().indexOf(" "), " OTC").append(otcSuffix).toString());
				newOptionSecurity.setBusinessCompany(command.getCounterparty());

				newOptionSecurity.setOptionType(ObjectUtils.coalesce(command.getOptionType(), command.getSecurityToCopy().getOptionType()));
				newOptionSecurity.setOptionStrikePrice(command.getStrikePrice());
				newOptionSecurity.setOptionStyle(ObjectUtils.coalesce(command.getOptionStyle(), command.getSecurityToCopy().getOptionStyle()));
				newOptionSecurity.setSettlementExpiryTime(ObjectUtils.coalesce(command.getSettlementTime(), command.getSecurityToCopy().getSettlementExpiryTime()));

				BusinessContract isda = command.getIsda();
				if (isda == null) {
					BusinessContractSearchForm businessContractSearchForm = new BusinessContractSearchForm();
					businessContractSearchForm.setCompanyId(command.getClientAccount().getBusinessClient().getCompany().getId());
					businessContractSearchForm.setPartyCompanyId(command.getCounterparty().getId());
					businessContractSearchForm.setContractTypeName(BusinessContractTypes.ISDA.getName());
					businessContractSearchForm.setActive(true);
					businessContractSearchForm.setPartyRoleId(getBusinessContractService().getBusinessContractPartyRoleByName(BusinessContractPartyRole.PARTY_ROLE_COUNTERPARTY_NAME).getId());
					List<BusinessContract> contracts = getBusinessContractService().getBusinessContractList(businessContractSearchForm);
					int size = CollectionUtils.getSize(contracts);
					ValidationUtils.assertFalse(size == 0, () -> "There is not an ISDA contract available for Counterparty ["
							+ command.getCounterparty() + "] and Client Account [" + command.getClientAccount() + "].");
					ValidationUtils.assertFalse(size > 1, () -> "There are more than one ISDA contracts available for Counterparty ["
							+ command.getCounterparty() + "] and Client Account [" + command.getClientAccount() + "].");
					isda = contracts.get(0);
				}
				newOptionSecurity.setBusinessContract(isda);

				if (newOptionSecurity.getInstrument().getHierarchy().isIlliquid() && !newOptionSecurity.isIlliquid()) {
					newOptionSecurity.setIlliquid(!newOptionSecurity.isIlliquid());
				}
			}
			return true;
		}
		return false;
	}


	/**
	 * Copies the {@link SystemColumnValue}s of the security to copy and populates them on the new security. If the
	 * new security's instrument differs from the template, the {@link SystemColumnCustom} reference to each value
	 * will be updated as well.
	 */
	private void populateCopiedSystemColumnValues(InvestmentSecurity newSecurity, InvestmentSecurityCopyCommand command) {
		String strikePriceString = command.getStrikePrice().stripTrailingZeros().toPlainString();
		List<SystemColumnValue> valueList = getSystemColumnValueHandler().getSystemColumnValueListForEntity(command.getSecurityToCopy());
		boolean newSecurityInstrumentDiffersFromTemplate = !newSecurity.getInstrument().equals(command.getSecurityToCopy().getInstrument());

		List<SystemColumnCustom> newCustomColumnList = newSecurityInstrumentDiffersFromTemplate ? getSystemColumnService().getSystemColumnCustomListForEntity(newSecurity) : Collections.emptyList();
		List<SystemColumnValue> newValueList = new ArrayList<>();
		for (SystemColumnValue cv : CollectionUtils.getIterable(valueList)) {
			SystemColumnValue ncv = BeanUtils.cloneBean(cv, false, false);
			if (newSecurityInstrumentDiffersFromTemplate) {
				// Update the custom column reference
				Iterator<SystemColumnCustom> newCustomColumnIterator = newCustomColumnList.iterator();
				while (newCustomColumnIterator.hasNext()) {
					SystemColumnCustom newCustomColumn = newCustomColumnIterator.next();
					if (newCustomColumn.getName().equals(ncv.getColumn().getName())) {
						ncv.setColumn(newCustomColumn);
						// remove the matched column so fewer columns are iterated in the next pass
						newCustomColumnIterator.remove();
						break;
					}
				}
			}
			newValueList.add(ncv);
		}
		newSecurity.setColumnValueList(newValueList);

		if (command.getStrikePrice() != null) {
			Matcher matcher = OPTION_SYMBOL_STRIKE_PATTERN.matcher(newSecurity.getSymbol());
			ValidationUtils.assertTrue(matcher.find(), "Unable to resolve Strike Price from existing symbol: " + newSecurity.getSymbol());

			String originalStrikePriceString = StringUtils.isEmpty(matcher.group(3)) ? matcher.group(4) : matcher.group(3); // e.g. 122.50, 2850, 165.50
			if (strikePriceString.contains(".") || originalStrikePriceString.contains(".")) {
				// Decimal digits can be up to three digits
				strikePriceString = CoreMathUtils.formatNumber(command.getStrikePrice(), InvestmentUtils.isListedOption(newSecurity) ? "#0.###" : "#0.00#");
			}
			updateSecurityPropertyStrikePrice(newSecurity, command.getStrikePrice(), strikePriceString, "symbol");
			updateSecurityPropertyStrikePrice(newSecurity, command.getStrikePrice(), strikePriceString, "name");
			if (!StringUtils.isEmpty(newSecurity.getOccSymbol())) {
				String newOccStrikeString = InvestmentUtils.getOccStrikePriceString(strikePriceString);
				newSecurity.setOccSymbol(StringUtils.replace(newSecurity.getOccSymbol(), InvestmentUtils.getOccStrikePriceString(originalStrikePriceString), newOccStrikeString));
				char existingOptionTypeFirstChar = newSecurity.getOccSymbol().charAt(12);
				char newOptionTypeFirstChar = newSecurity.getOptionType().name().charAt(0);
				if (existingOptionTypeFirstChar != newOptionTypeFirstChar) {
					newSecurity.setOccSymbol(StringUtils.replace(newSecurity.getOccSymbol(), existingOptionTypeFirstChar + newOccStrikeString, newOptionTypeFirstChar + newOccStrikeString));
				}
			}
		}
		else {
			throw new ValidationException("Copy From Security [" + command.getSecurityToCopy().getLabel() + "]: no strike price specified for new option security.");
		}

		newSecurity.setColumnValueList(newValueList);
	}


	private void updateSecurityPropertyStrikePrice(InvestmentSecurity newSecurity, BigDecimal strikePrice, String strikePriceString, String propertyName) {
		String symbol = (String) BeanUtils.getPropertyValue(newSecurity, propertyName);
		Matcher matcher = OPTION_SYMBOL_STRIKE_PATTERN.matcher(symbol);
		ValidationUtils.assertTrue(matcher.find(), "Unable to resolve Strike Price from existing " + propertyName + " value: " + newSecurity.getName());

		String currentSymbolStrikeString = matcher.group(1); // e.g. P122.50, C2850, P 165.50, 300 Puts
		String currentPutCallPrefix = StringUtils.isEmpty(matcher.group(2)) ? "" : matcher.group(2); // e.g. P, C, Put[s], Call[s] (prefix)
		String originalStrikePriceString = StringUtils.isEmpty(matcher.group(3)) ? matcher.group(4) : matcher.group(3); // e.g. 122.50, 2850, 165.50
		String currentPutCallSuffix = StringUtils.isEmpty(matcher.group(5)) ? "" : matcher.group(5); // e.g. P, C, Put[s], Call[s] (suffix)

		String replacementStrikePriceString = strikePriceString;
		if (replacementStrikePriceString.contains(".") || originalStrikePriceString.contains(".")) {
			// Decimal digits can be up to three digits
			replacementStrikePriceString = CoreMathUtils.formatNumber(strikePrice, InvestmentUtils.isListedOption(newSecurity) ? "#0.###" : "#0.00#");
		}

		String newSymbolStrikeString = getOptionTypeValueForExistingProperty(newSecurity, currentPutCallPrefix) + replacementStrikePriceString + getOptionTypeValueForExistingProperty(newSecurity, currentPutCallSuffix);

		BeanUtils.setPropertyValue(newSecurity, propertyName, StringUtils.replace(symbol, currentSymbolStrikeString, newSymbolStrikeString));
	}


	private String getOptionTypeValueForExistingProperty(InvestmentSecurity newSecurity, String existingValue) {
		if (StringUtils.isEmpty(existingValue)) {
			return existingValue;
		}

		String newTypeString = newSecurity.getOptionType().name();
		char newTypeValueFirstChar = newTypeString.charAt(0);
		// trim leading and trailing spaces that may be on the existing value.
		String trimmedExistingValue = StringUtils.trim(existingValue);
		char existingTypeValueFirstChar = trimmedExistingValue.charAt(0);

		if (newTypeValueFirstChar == existingTypeValueFirstChar) {
			return existingValue;
		}

		StringBuilder newValue = new StringBuilder(Character.toString(newTypeValueFirstChar));
		for (int i = 1; i < trimmedExistingValue.length(); i++) {
			if (i < newTypeString.length()) {
				newValue.append(Character.toLowerCase(newTypeString.charAt(i)));
			}
			else {
				// append s for Puts or Calls
				newValue.append('s');
				break;
			}
		}
		int trimmedExistingLength = trimmedExistingValue.length();
		int existingLength = existingValue.length();
		if (trimmedExistingLength != existingLength) {
			// add spaces that may be in the regex group
			if (existingValue.charAt(0) == existingTypeValueFirstChar) {
				// add spaces to end
				newValue.append(existingValue.substring(trimmedExistingLength));
			}
			else {
				// add spaces to beginning
				newValue.insert(0, existingValue.substring(0, existingLength - trimmedExistingLength));
			}
		}
		return newValue.toString();
	}


	private void applyOptionExpirationDateUpdate(InvestmentSecurity newSecurity, InvestmentSecurityCopyCommand command) {
		if (command.getExpirationDate() != null) {
			int year = DateUtils.getYear(command.getExpirationDate());
			int month = DateUtils.getMonthOfYear(command.getExpirationDate());
			int day = DateUtils.getDayOfMonth(command.getExpirationDate());

			Matcher matcher = OPTION_SYMBOL_DATE_PATTERN.matcher(newSecurity.getSymbol());
			if (matcher.matches()) {
				updateSecurityPropertyDate(command, year, month, day, newSecurity, "symbol");
				updateSecurityPropertyDate(command, year, month, day, newSecurity, "name");
				if (!StringUtils.isEmpty(newSecurity.getDescription())) {
					updateSecurityPropertyDate(command, year, month, day, newSecurity, "description");
				}
			}
			else {
				InvestmentDeliveryMonthCodes monthCode = InvestmentDeliveryMonthCodes.valueOfMonthNumber(month);
				String securityMonthExpirationCode = monthCode.name() + (Integer.toString(year).substring(3));
				if (newSecurity.getSymbol().matches(securityMonthExpirationCode)) {
					String securityMonthExpirationLongCode = monthCode.getMonthName().substring(0, 2) + (Integer.toString(year).substring(2));
					newSecurity.setSymbol(newSecurity.getSymbol().replaceFirst("", securityMonthExpirationCode));
					newSecurity.setName(newSecurity.getName().replaceFirst("", securityMonthExpirationLongCode));
					if (!StringUtils.isEmpty(newSecurity.getDescription())) {
						newSecurity.setDescription(newSecurity.getDescription().replaceFirst("", securityMonthExpirationLongCode));
					}
				}
			}

			if (!StringUtils.isEmpty(newSecurity.getOccSymbol())) {
				newSecurity.setOccSymbol(newSecurity.getOccSymbol().replaceFirst("\\d{6}([CP])", Integer.toString(year).substring(2) + ((month < 10 ? "0" : "") + month) + ((day < 10 ? "0" : "") + day) + "$1"));
			}

			newSecurity.setStartDate(new Date());
			newSecurity.setFirstDeliveryDate(command.getExpirationDate());
			newSecurity.setEndDate(command.getExpirationDate());
			newSecurity.setLastDeliveryDate(command.getExpirationDate());
		}
	}


	private void updateSecurityPropertyDate(InvestmentSecurityCopyCommand command, int year, int month, int day, InvestmentSecurity newSecurity, String propertyName) {
		String valueToReplace = (String) BeanUtils.getPropertyValue(newSecurity, propertyName);
		Matcher matcher = OPTION_SYMBOL_DATE_PATTERN.matcher(valueToReplace);
		ValidationUtils.assertTrue(matcher.find(), "Unable to find date in existing " + propertyName + " value: " + valueToReplace);

		String currentDateString = matcher.group(1);
		// Assume match for mm/dd/yy
		String replacementDateString = ((month < 10 ? "0" : "") + month) + "/" + ((day < 10 ? "0" : "") + day) + "/" + Integer.toString(year).substring(2);
		if (!StringUtils.isEmpty(matcher.group(3))) {
			// match for mmddyyyy
			replacementDateString = ((month < 10 ? "0" : "") + month) + ((day < 10 ? "0" : "") + day) + (year);
		}
		else if (!StringUtils.isEmpty(matcher.group(4))) {
			// match for month name with year
			replacementDateString = DateUtils.fromDate(command.getExpirationDate(), "MMMM yyyy");
		}

		BeanUtils.setPropertyValue(newSecurity, propertyName, StringUtils.replace(valueToReplace, currentDateString, replacementDateString));
	}


	private InvestmentSecurity getExistingSecurityOrCreateNewSecurity(InvestmentSecurity newSecurity, InvestmentSecurityCopyCommand command) {
		// Check if security already exists (by symbol)
		InvestmentSecurity existingSecurity = getInvestmentInstrumentService().getInvestmentSecurityBySymbolOnly(newSecurity.getSymbol());
		if (existingSecurity != null) {
			if (command.isReturnExisting()) {
				return existingSecurity;
			}
			throw new ValidationException("Security with Symbol [" + newSecurity.getSymbol() + "] already exists.");
		}

		newSecurity = saveInvestmentSecurityFromTemplate(newSecurity, command.getSecurityToCopy().getId());

		if (command.isApplyMarketDataToNewSecurity()) {
			getEventHandler().raiseEvent(EventObject.ofEventTarget(InvestmentSecurity.INVESTMENT_SECURITY_APPLY_MARKET_DATA_EVENT, newSecurity));
		}

		return newSecurity;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContractService getBusinessContractService() {
		return this.businessContractService;
	}


	public void setBusinessContractService(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityAllocationHandler getInvestmentSecurityAllocationHandler() {
		return this.investmentSecurityAllocationHandler;
	}


	public void setInvestmentSecurityAllocationHandler(InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler) {
		this.investmentSecurityAllocationHandler = investmentSecurityAllocationHandler;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}
}
