package com.clifton.investment.instrument.calculator;

import com.clifton.calendar.holiday.CalendarBusinessDayService;

import java.util.Date;


/**
 * The DayCountCommand class is a command object passed to {@link DayCountConventions} and used to calculated accrued interest.
 * <p/>
 * <b>DOES NOT INCLUDE ACCRUAL FOR THE accrualStartDate: uses industry standard End Of Day convention.</b>
 * <p/>
 * Period Start Date maybe before Accrual Start Date if first accrual period is partial (bond issued in the middle of accrual period).
 *
 * @author vgomelsky
 */
public class DayCountCommand {

	/**
	 * <b>DOES NOT INCLUDE ACCRUAL FOR THE accrualStartDate: uses industry standard End Of Day convention.</b>
	 */
	private Date accrualStartDate;
	private Date accrueUpToDate;

	/**
	 * Period Start Date maybe before Accrual Start Date if first accrual period is partial (bond issued in the middle of accrual period).
	 */
	private Date periodStartDate;
	private Date periodEndDate;

	/**
	 * See {@link CouponFrequencies#frequency}: ANNUAL, SEMIANNUAL, QUARTERLY, etc.
	 */
	private Integer frequency;

	/**
	 * For Day Count Conventions that rely on actual business days, these fields must be initialized.
	 */
	private CalendarBusinessDayService calendarBusinessDayService;
	private Short calendarId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DayCountCommand() {
		// use static initializers to construct
	}


	public static DayCountCommand forAccrualRange(Date accrualStartDate, Date accrueUpToDate) {
		DayCountCommand command = new DayCountCommand();
		command.accrualStartDate = accrualStartDate;
		command.accrueUpToDate = accrueUpToDate;
		return command;
	}


	public DayCountCommand withAccrualPeriod(Date periodStartDate, Date periodEndDate) {
		this.periodStartDate = periodStartDate;
		this.periodEndDate = periodEndDate;
		return this;
	}


	public DayCountCommand withFrequency(CouponFrequencies couponFrequency) {
		this.frequency = (couponFrequency == null) ? null : couponFrequency.getFrequency();
		return this;
	}


	public DayCountCommand withFrequency(int frequency) {
		this.frequency = frequency;
		return this;
	}


	public DayCountCommand withBusinessDayCalendar(CalendarBusinessDayService calendarBusinessDayService, Short calendarId) {
		this.calendarBusinessDayService = calendarBusinessDayService;
		this.calendarId = calendarId;
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getAccrualStartDate() {
		return this.accrualStartDate;
	}


	public Date getAccrueUpToDate() {
		return this.accrueUpToDate;
	}


	public Date getPeriodStartDate() {
		return this.periodStartDate;
	}


	public Date getPeriodEndDate() {
		return this.periodEndDate;
	}


	public int getFrequency() {
		if (this.frequency == null) {
			throw new IllegalStateException("Cannot calculate accrual rate because 'frequency' is not defined for a Day Count Convention that requires frequency.");
		}
		if (this.frequency == 0) {
			throw new IllegalStateException("Cannot calculate accrual rate when frequency is [0].");
		}
		return this.frequency;
	}


	public boolean isOfFrequency(CouponFrequencies couponFrequency) {
		return getFrequency() == couponFrequency.getFrequency();
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		if (this.calendarBusinessDayService == null) {
			throw new IllegalStateException("Cannot calculate accrual rate because 'CalendarBusinessDayService' is not defined for a day Count Convention that requires business day calculations");
		}
		return this.calendarBusinessDayService;
	}


	public Short getCalendarId() {
		if (this.calendarId == null) {
			throw new IllegalStateException("Cannot calculate accrual rate because 'calendarId' is not defined for a day Count Convention that requires business day calculations");
		}
		return this.calendarId;
	}
}
