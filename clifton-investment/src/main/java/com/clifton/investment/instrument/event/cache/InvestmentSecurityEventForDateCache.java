package com.clifton.investment.instrument.event.cache;

import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.Date;
import java.util.Optional;


/**
 * The <code>InvestmentSecurityEventForDateCache</code> interface defines methods caching InvestmentSecurityEvent id's for various date lookups.
 * <p>
 * Caching is done by securityId so that we can clear only security specific caches on changes.
 *
 * @author vgomelsky
 */
public interface InvestmentSecurityEventForDateCache {

	/**
	 * Returns null if not present in cache.  Otherwise returns Optional which could be empty (present in cache but no data) or populated with corresponding id.
	 */
	public Optional<Integer> getInvestmentSecurityEvent(int securityId, String typeName, Date lookupDate);


	public void setInvestmentSecurityEvent(int securityId, String typeName, Date lookupDate, InvestmentSecurityEvent event);
}
