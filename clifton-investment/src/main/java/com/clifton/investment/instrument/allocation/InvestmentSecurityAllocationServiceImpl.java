package com.clifton.investment.instrument.allocation;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * InvestmentSecurityAllocationService class contains methods called from UI for working with allocations.
 * Actual implementation is held within the InvestmentSecurityAllocationHandler which contains the DAO for real look ups.
 * This service can take a security id, lookup the security and then pass that to the handler to prevent circular dependencies.
 *
 * @author manderson
 */
@Service
public class InvestmentSecurityAllocationServiceImpl implements InvestmentSecurityAllocationService {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler;

	////////////////////////////////////////////////////////////////////////////
	/////////       Investment Security Allocation Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityAllocation getInvestmentSecurityAllocation(int id) {
		return getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocation(id);
	}


	@Override
	public List<InvestmentSecurityAllocation> getInvestmentSecurityAllocationList(final SecurityAllocationSearchForm searchForm) {
		return getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationList(searchForm);
	}


	@Override
	public InvestmentSecurityAllocation saveInvestmentSecurityAllocation(InvestmentSecurityAllocation bean) {
		return getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocation(bean);
	}


	@Override
	public InvestmentSecurityAllocation saveInvestmentSecurityAllocationFromExisting(Integer existingId, InvestmentSecurityAllocation bean) {
		return getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocationFromExisting(existingId, bean);
	}


	@Override
	public void saveInvestmentSecurityAllocationList(List<InvestmentSecurityAllocation> newList, List<InvestmentSecurityAllocation> oldList) {
		getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocationList(newList, oldList);
	}


	@Override
	public void deleteInvestmentSecurityAllocation(int id) {
		getInvestmentSecurityAllocationHandler().deleteInvestmentSecurityAllocation(id);
	}


	@Override
	@Transactional
	public void endInvestmentSecurityAllocation(InvestmentSecurityAllocationCommand command) {
		for (InvestmentSecurityAllocation allocation : command.getBeanList()) {
			Date endDate = command.getEndDate() == null ? new Date() : command.getEndDate();
			ValidationUtils.assertTrue(allocation.isActiveOnDate(endDate), "Cannot end inactive allocation. Allocation is active from " + allocation.getStartDate() + " to " + allocation.getEndDate());
			allocation.setEndDate(endDate);
			saveInvestmentSecurityAllocation(allocation);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//////       Investment Security Allocation Dynamic Methods          ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Allowed for dynamic security allocations only.  Used to preview security allocations and weights for a given date
	 */
	@Override
	public List<InvestmentSecurityAllocation> getInvestmentSecurityAllocationDynamicList(int securityId, Date date) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(securityId);
		searchForm.setActiveOnDate(date);
		return getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationDynamicList(security, getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationList(searchForm), date);
	}

	////////////////////////////////////////////////////////////////////////////
	//////       Investment Security Allocation Rebalance Methods         //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void deleteInvestmentSecurityAllocationRebalance(int securityId, Date rebalanceDate) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		getInvestmentSecurityAllocationHandler().deleteInvestmentSecurityAllocationRebalance(security, rebalanceDate);
	}


	@Override
	public void processInvestmentSecurityAllocationRebalance(int securityId, Short dataSourceId, Date baseDate, boolean useBaseWeights, Date startDate, BigDecimal startingPrice) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		getInvestmentSecurityAllocationHandler().processInvestmentSecurityAllocationRebalance(security, dataSourceId, baseDate, useBaseWeights, startDate, startingPrice);
	}


	@Override
	@Transactional
	public void rebuildInvestmentSecurityAllocationRebalance(int securityId, Short dataSourceId, Date rebalanceDate, BigDecimal startingPrice, boolean updateWeights) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		getInvestmentSecurityAllocationHandler().rebuildInvestmentSecurityAllocationRebalance(security, dataSourceId, rebalanceDate, startingPrice, updateWeights);
	}


	@Override
	public List<InvestmentSecurityAllocationRebalance> getInvestmentSecurityAllocationRebalanceList(int parentInvestmentSecurityId, boolean populateDetails) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(parentInvestmentSecurityId);
		return getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationRebalanceList(security, populateDetails);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityAllocationHandler getInvestmentSecurityAllocationHandler() {
		return this.investmentSecurityAllocationHandler;
	}


	public void setInvestmentSecurityAllocationHandler(InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler) {
		this.investmentSecurityAllocationHandler = investmentSecurityAllocationHandler;
	}
}
