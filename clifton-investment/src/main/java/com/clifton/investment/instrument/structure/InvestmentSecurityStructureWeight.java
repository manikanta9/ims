package com.clifton.investment.instrument.structure;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityTargetAdjustmentTypes;
import com.clifton.system.bean.SystemBean;

import java.math.BigDecimal;


/**
 * The <code>InvestmentSecurityStructureWeight</code> defines the specific structure, weight override (optional),
 * and current security calculator for a specific version (f2, f3) of the structured index.
 * <p/>
 * For each security for a structured instrument, there must be an assignment below that applies specifically to that version
 *
 * @author manderson
 */
public class InvestmentSecurityStructureWeight extends BaseEntity<Integer> {

	private InvestmentInstrumentStructureWeight instrumentWeight;

	/**
	 * A security from the instrument defined by the
	 * {@link InvestmentInstrumentStructureWeight}
	 */
	private InvestmentSecurity security;

	/**
	 * Optionally can be set to override the instrument weight multiplier
	 * or set to 0 to exclude a specific instrument.
	 */
	private BigDecimal weightOverride;

	/**
	 * Current Security Calculator bean that defines how to get the current security
	 * for the instrument.
	 */
	private SystemBean currentSecurityCalculatorBean;

	/**
	 * Required when current security calculator bean is defined.  Determines how the portfolio adjusts the target for the current security when non-current securities are held
	 * (i.e. usually when rolling) - give non current TFA and current difference, give current difference only if suggested to open new positions
	 */
	private InvestmentCurrentSecurityTargetAdjustmentTypes currentSecurityTargetAdjustmentType;

	/**
	 * This flag determines whether or not a current security is always included during portfolio run processing, even if it results in a 0 allocation (0 target, 0 exposure). If FALSE, the current security will not be included if it results in a 0 allocation.
	 * Can be applied for Security specific replication allocations, however only really comes into play when the target is 0
	 */
	private boolean alwaysIncludeCurrentSecurity;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCoalesceWeight() {
		if (getWeightOverride() != null) {
			return getWeightOverride();
		}
		return getInstrumentWeight().getWeight();
	}


	public String getCurrentSecurityTargetAdjustmentTypeLabel() {
		if (getCurrentSecurityTargetAdjustmentType() != null) {
			return getCurrentSecurityTargetAdjustmentType().getLabel();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentStructureWeight getInstrumentWeight() {
		return this.instrumentWeight;
	}


	public void setInstrumentWeight(InvestmentInstrumentStructureWeight instrumentWeight) {
		this.instrumentWeight = instrumentWeight;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public BigDecimal getWeightOverride() {
		return this.weightOverride;
	}


	public void setWeightOverride(BigDecimal weightOverride) {
		this.weightOverride = weightOverride;
	}


	public SystemBean getCurrentSecurityCalculatorBean() {
		return this.currentSecurityCalculatorBean;
	}


	public void setCurrentSecurityCalculatorBean(SystemBean currentSecurityCalculatorBean) {
		this.currentSecurityCalculatorBean = currentSecurityCalculatorBean;
	}


	public InvestmentCurrentSecurityTargetAdjustmentTypes getCurrentSecurityTargetAdjustmentType() {
		return this.currentSecurityTargetAdjustmentType;
	}


	public void setCurrentSecurityTargetAdjustmentType(InvestmentCurrentSecurityTargetAdjustmentTypes currentSecurityTargetAdjustmentType) {
		this.currentSecurityTargetAdjustmentType = currentSecurityTargetAdjustmentType;
	}


	public boolean isAlwaysIncludeCurrentSecurity() {
		return this.alwaysIncludeCurrentSecurity;
	}


	public void setAlwaysIncludeCurrentSecurity(boolean alwaysIncludeCurrentSecurity) {
		this.alwaysIncludeCurrentSecurity = alwaysIncludeCurrentSecurity;
	}
}
