package com.clifton.investment.instrument.validation;

import com.clifton.investment.specificity.BaseInvestmentSpecificityBean;


/**
 * {@link InvestmentSpecificityInstrumentGenericMapping} implementation class for custom investment instrument entity value mappings.
 * This class provides fields for assigning a single value based on the selected Specificity.
 */
public class InvestmentSpecificityInstrumentGenericMapping extends BaseInvestmentSpecificityBean {

	private String systemColumnValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSystemColumnValue() {
		return this.systemColumnValue;
	}


	public void setSystemColumnValue(String systemColumnValue) {
		this.systemColumnValue = systemColumnValue;
	}
}
