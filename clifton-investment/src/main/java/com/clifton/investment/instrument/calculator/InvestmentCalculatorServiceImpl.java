package com.clifton.investment.instrument.calculator;


import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentCalculatorServiceImpl</code> class provides basic implementation of InvestmentCalculatorService interface.
 *
 * @author vgomelsky
 */
@Service
public class InvestmentCalculatorServiceImpl implements InvestmentCalculatorService {

	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getInvestmentSecurityTradeDate(int securityId) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		return getInvestmentCalculator().calculateTradeDate(security, null);
	}


	@Override
	public Date getInvestmentSecuritySettlementDate(int securityId, Integer settlementCurrencyId, Date transactionDate) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		InvestmentSecurity settlementSecurity = (settlementCurrencyId == null) ? null : getInvestmentInstrumentService().getInvestmentSecurity(settlementCurrencyId);
		return getInvestmentCalculator().calculateSettlementDate(security, settlementSecurity, transactionDate);
	}


	@Override
	public BigDecimal getInvestmentSecurityAccruedInterest(int securityId, BigDecimal notional, BigDecimal notionalMultiplier, Date date, Date accrueFromDate) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		return getInvestmentCalculator().calculateAccruedInterest(security, AccrualBasis.of(notional), notionalMultiplier, AccrualDates.ofSame1and2(accrueFromDate, date));
	}


	@Override
	public BigDecimal getInvestmentInterestForPeriod(BigDecimal notional, BigDecimal rate, Date startDate, Date endDate, String dayCountConventionKey) {
		// TODO: use actual day count convention object, spent to much time trying to make it work in the javascript
		DayCountConventions dayCountConvention = DayCountConventions.getDayCountConventionByKey(dayCountConventionKey);
		return getInvestmentCalculator().calculateInterestPayment(notional, rate, startDate, endDate, startDate, dayCountConvention, CouponFrequencies.ANNUAL);
	}


	@Override
	public BigDecimal getInvestmentInterestForDays(BigDecimal notional, BigDecimal rate, Date startDate, Integer days, String dayCountConventionKey) {
		Date endDate = DateUtils.addDays(startDate, days);
		DayCountConventions dayCountConvention = DayCountConventions.getDayCountConventionByKey(dayCountConventionKey);
		return getInvestmentCalculator().calculateInterestPayment(notional, rate, startDate, endDate, startDate, dayCountConvention, CouponFrequencies.ANNUAL);
	}


	@Override
	public BigDecimal getInvestmentSecurityNotional(int securityId, BigDecimal price, BigDecimal quantity, BigDecimal notionalMultiplier) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		return getInvestmentCalculator().calculateNotional(security, price, quantity, notionalMultiplier);
	}


	@Override
	public BigDecimal getInvestmentSecurityNotionalMultiplier(int securityId, Date date, Date settlementDate, Boolean useNextDayForLookup) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		BigDecimal result = getInvestmentCalculator().getNotionalMultiplier(security, date, settlementDate, ObjectUtils.coalesce(useNextDayForLookup, false));
		return (result == null) ? BigDecimal.ONE : result;
	}


	@Override
	public BigDecimal applyInvestmentSecurityNotionalRounding(int roundingSecurityId, BigDecimal notionalAmount) {
		InvestmentSecurity roundingSecurity = getInvestmentInstrumentService().getInvestmentSecurity(roundingSecurityId);
		InvestmentNotionalCalculatorTypes roundingCalculator = InvestmentCalculatorUtils.getNotionalCalculator(roundingSecurity);
		return roundingCalculator.round(notionalAmount);
	}


	@Override
	public BigDecimal getInvestmentSecurityDirtyPrice(int securityId, BigDecimal price, BigDecimal quantity, Date date) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		return getInvestmentCalculator().calculateDirtyPrice(security, price, quantity, date);
	}


	@Override
	public BigDecimal getInvestmentMultiplyAndRound(BigDecimal value1, BigDecimal value2, int scale) {
		return MathUtils.multiply(value1, value2, scale);
	}


	@Override
	public Calendar getInvestmentSecurityCalendar(int securityId) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		return getInvestmentCalculator().getInvestmentSecurityCalendar(security);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
