package com.clifton.investment.instrument.validation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityUtilHandler;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The DAO validator for {@link InvestmentSecurity} entities.
 */
@Component
public class InvestmentSecurityValidator extends SelfRegisteringDaoValidator<InvestmentSecurity> {

	private InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler;
	private SystemConditionService systemConditionService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private static final int FIGI_LENGTH = 12;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentSecurity security, DaoEventTypes config) throws ValidationException {
		validateSecurityForSpecificityConditions(security);

		if (security.getFigi() != null) {
			validateFigiField(security);
		}

		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.STOCKS)) {
			validateExistingEventsAndPayouts(security);
		}

		if (security.getAccrualMethod() != null) {
			ValidationUtils.assertTrue(InvestmentUtils.isAccrualSupported(security), "Accrual Method should not be set on a Security that does not have Accruals", "accrualMethod");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates the given security using the conditions applied via existing investment specificity entries.
	 *
	 * @param security the security to validate
	 */
	private void validateSecurityForSpecificityConditions(InvestmentSecurity security) {
		// Get validators for the security using specificity
		List<InvestmentSpecificityEntry> securityValidatorHolders = getInvestmentSpecificityUtilHandler()
				.getInvestmentSpecificityEntryListForSecurity(InvestmentSpecificityDefinition.DEFINITION_SECURITY_VALIDATOR, security);

		// Run all validators
		for (InvestmentSpecificityEntry validatorEntry : securityValidatorHolders) {
			InvestmentSpecificityInvestmentValidator validatorEntryBean = getInvestmentSpecificityUtilHandler().generatePopulatedSpecificityBean(validatorEntry);
			SystemCondition condition = getSystemConditionService().getSystemCondition(validatorEntryBean.getValidationConditionId());
			EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(condition, security);
			if (!result.isResult()) {
				InvestmentSpecificityField field = validatorEntry.getField();
				String beanPropertyName = (field.getSystemColumn() instanceof SystemColumnStandard)
						? ((SystemColumnStandard) field.getSystemColumn()).getBeanPropertyName()
						: null;
				throw new FieldValidationException(String.format("%s validation failed on field [%s] for condition [%s]: %s", InvestmentSpecificityDefinition.DEFINITION_SECURITY_VALIDATOR, field.getLabel(), condition.getName(), result.getMessage()),
						beanPropertyName);
			}
		}

		// Check Applicability
		List<InvestmentSpecificityEntry> overrideEntryList = getInvestmentSpecificityUtilHandler()
				.getInvestmentSpecificityEntryListForSecurity(InvestmentSpecificityDefinition.DEFINITION_SECURITY_UI_OVERRIDE, security);
		for (InvestmentSpecificityEntry overrideEntry : overrideEntryList) {
			InvestmentSpecificityColumnUiOverride overrideEntryBean = getInvestmentSpecificityUtilHandler().generatePopulatedSpecificityBean(overrideEntry);
			SystemColumnStandard systemColumn = (SystemColumnStandard) overrideEntry.getField().getSystemColumn();
			String beanPropertyName = systemColumn.getBeanPropertyName();
			Object propertyValue = BeanUtils.getPropertyValue(security, beanPropertyName);
			if (!overrideEntryBean.getFieldValueApplicability().getPredicate().test(propertyValue)) {
				throw new FieldValidationException(String.format("%s validation failed on field [%s] for applicability [%s]: %s"
						, InvestmentSpecificityDefinition.DEFINITION_SECURITY_UI_OVERRIDE
						, overrideEntry.getField().getLabel()
						, overrideEntryBean.getFieldValueApplicability().toString()
						, overrideEntryBean.getFieldValueApplicability().getLabel()),
						beanPropertyName);
			}
		}
	}


	/**
	 * Validates that existing {@link InvestmentSecurityEvent}s and {@link InvestmentSecurityEventPayout}s are not invalidated if the updates to the given security are persisted.
	 *
	 * @param security the security to validate
	 */
	private void validateExistingEventsAndPayouts(InvestmentSecurity security) {
		if (security.isNewBean()) {
			return;
		}
		validateExistingEvents(security);
		validateExistingPayouts(security);
	}


	private void validateExistingEvents(InvestmentSecurity security) {
		InvestmentSecurityEventSearchForm securityEventSearchForm = new InvestmentSecurityEventSearchForm();
		securityEventSearchForm.setSecurityId(security.getId());
		List<InvestmentSecurityEvent> investmentSecurityEventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(securityEventSearchForm);
		List<String> endDateViolations = new ArrayList<>();
		List<String> lastDeliveryDateViolations = new ArrayList<>();
		for (InvestmentSecurityEvent event : investmentSecurityEventList) {
			if (security.getEndDate() != null && DateUtils.isDateBefore(security.getEndDate(), event.getDayBeforeExDate(), false)) {
				endDateViolations.add(event.getLabel() + " with Ex Date [" + DateUtils.fromDateShort(event.getDayBeforeExDate()) + "]");
			}
			if (security.getLastDeliveryDate() != null) {
				if (DateUtils.isDateBefore(security.getLastDeliveryDate(), event.getEventDate(), false)) {
					lastDeliveryDateViolations.add(event.getLabel());
				}
			}
		}
		ValidationUtils.assertEmpty(endDateViolations,
				StringUtils.joinErrorListOfViolations("Cannot save " + security.getLabel() + " due to " + endDateViolations.size() + " existing event(s) with Ex Date after the security's End Date [" + DateUtils.fromDateShort(security.getEndDate()) + "]:", endDateViolations));
		ValidationUtils.assertEmpty(lastDeliveryDateViolations,
				StringUtils.joinErrorListOfViolations("Cannot save " + security.getLabel() + " due to " + lastDeliveryDateViolations.size() + " existing event(s) with Event Date after the security's Last Delivery Date [" + DateUtils.fromDateShort(security.getLastDeliveryDate()) + "]:", lastDeliveryDateViolations));
	}


	private void validateExistingPayouts(InvestmentSecurity security) {
		List<String> lastDeliveryDatePayoutViolations = new ArrayList<>();
		InvestmentSecurityEventPayoutSearchForm securityEventPayoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		securityEventPayoutSearchForm.setEventSecurityId(security.getId());
		List<InvestmentSecurityEventPayout> payoutList = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventPayoutList(securityEventPayoutSearchForm);
		for (InvestmentSecurityEventPayout payout : payoutList) {
			if (security.getLastDeliveryDate() != null) {
				if (DateUtils.isDateBefore(security.getLastDeliveryDate(), payout.getAdditionalPayoutDate(), false)) {
					lastDeliveryDatePayoutViolations.add(payout.getLabel() + " with Additional Date [" + DateUtils.fromDateShort(payout.getAdditionalPayoutDate()) + "]");
				}
			}
		}
		ValidationUtils.assertEmpty(lastDeliveryDatePayoutViolations,
				StringUtils.joinErrorListOfViolations("Cannot save " + security.getLabel() + " due to " + lastDeliveryDatePayoutViolations.size() + " existing payout(s) with Additional Date after the security's Last Delivery Date [" + DateUtils.fromDateShort(security.getLastDeliveryDate()) + "]:", lastDeliveryDatePayoutViolations));
	}


	private void validateFigiField(InvestmentSecurity security) {
		ValidationUtils.assertFalse(security.getInstrument().getHierarchy().isOtc(), "The FIGI field is not allowed for OTC securities.", "figi");
		ValidationUtils.assertTrue(security.getFigi().length() == FIGI_LENGTH, "The FIGI field must be exactly " + FIGI_LENGTH + " characters long.", "figi");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityUtilHandler getInvestmentSpecificityUtilHandler() {
		return this.investmentSpecificityUtilHandler;
	}


	public void setInvestmentSpecificityUtilHandler(InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler) {
		this.investmentSpecificityUtilHandler = investmentSpecificityUtilHandler;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public InvestmentSecurityEventPayoutService getInvestmentSecurityEventPayoutService() {
		return this.investmentSecurityEventPayoutService;
	}


	public void setInvestmentSecurityEventPayoutService(InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService) {
		this.investmentSecurityEventPayoutService = investmentSecurityEventPayoutService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}
}
