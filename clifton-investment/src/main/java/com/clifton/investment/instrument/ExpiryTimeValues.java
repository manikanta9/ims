package com.clifton.investment.instrument;

/**
 * An enum to represent an Option's expiry time.
 *
 * @author davidi
 */
public enum ExpiryTimeValues {
	AM,
	PM
}
