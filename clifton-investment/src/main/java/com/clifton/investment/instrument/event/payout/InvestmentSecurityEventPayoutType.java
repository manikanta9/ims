package com.clifton.investment.instrument.event.payout;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The InvestmentSecurityEventPayoutType class represents a type of {@link InvestmentSecurityEventPayout}.
 * The following types are available:
 * CY = Currency
 * SN = Security (New)
 * SE = Security (Existing)
 * SS = Security (Stock Split)
 * SD = Security (Stock Dividend)
 *
 * @author vgomelsky
 */
@CacheByName
public class InvestmentSecurityEventPayoutType extends NamedEntityWithoutLabel<Short> {

	public static final String CURRENCY = "Currency";
	public static final String CURRENCY_FRANKING_CREDIT = "Currency (with Franking Credit)";
	public static final String SECURITY_NEW = "Security (New)";
	public static final String SECURITY_EXISTING = "Security (Existing)";
	public static final String SECURITY_STOCK_SPLIT = "Security (Stock Split)";
	public static final String SECURITY_STOCK_DIVIDEND = "Security (Stock Dividend)";
	public static final String SECURITY_FRANKING_CREDIT = "Security (with Franking Credit)";

	/**
	 * Short code that uniquely identifies this payout type. It maybe referenced by other parts of the system and should never change.
	 */
	private String typeCode;

	/**
	 * Specifies whether values before and after event are the same: dividend, coupon payments.
	 * They're different for stock splits, factor changes, etc.
	 */
	private boolean beforeSameAsAfter;

	/**
	 * Specifies whether additionalSecurity field on InvestmentSecurityEvent is a currency.
	 * For example, it can be used to specify Dividend Currency for a Dividend Payment event.
	 */
	private boolean additionalSecurityCurrency;
	/**
	 * When Additional Security is Currency, specifies if the Before and After Value are in the Event currency units or security currency units.
	 * For example, "Cash Dividend Payment" event type will have this field set to true, meaning the dividend per share amount (Before/After Value)
	 * is in Additional Security units (Event currency) and the exchange rate is from Event currency to Client Account base currency.
	 */
	private boolean eventValueInEventCurrencyUnits;
	/**
	 * Some events create a new security: stock spin offs or symbol changes
	 */
	private boolean newSecurityCreated;

	/**
	 * Specifies whether a new position is created and an existing position is closed: Stock Split.
	 * Bond factor change will have this as false as it does not create a new position but rather closes a portion of existing position.
	 */
	private boolean newPositionCreated;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getTypeCode() + ": " + getName();
	}


	public boolean isWithFrankingCredit() {
		return getName().equals(CURRENCY_FRANKING_CREDIT) || getName().equals(SECURITY_FRANKING_CREDIT);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTypeCode() {
		return this.typeCode;
	}


	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}


	public boolean isBeforeSameAsAfter() {
		return this.beforeSameAsAfter;
	}


	public void setBeforeSameAsAfter(boolean beforeSameAsAfter) {
		this.beforeSameAsAfter = beforeSameAsAfter;
	}


	public boolean isAdditionalSecurityCurrency() {
		return this.additionalSecurityCurrency;
	}


	public void setAdditionalSecurityCurrency(boolean additionalSecurityCurrency) {
		this.additionalSecurityCurrency = additionalSecurityCurrency;
	}


	public boolean isEventValueInEventCurrencyUnits() {
		return this.eventValueInEventCurrencyUnits;
	}


	public void setEventValueInEventCurrencyUnits(boolean eventValueInEventCurrencyUnits) {
		this.eventValueInEventCurrencyUnits = eventValueInEventCurrencyUnits;
	}


	public boolean isNewSecurityCreated() {
		return this.newSecurityCreated;
	}


	public void setNewSecurityCreated(boolean newSecurityCreated) {
		this.newSecurityCreated = newSecurityCreated;
	}


	public boolean isNewPositionCreated() {
		return this.newPositionCreated;
	}


	public void setNewPositionCreated(boolean newPositionCreated) {
		this.newPositionCreated = newPositionCreated;
	}
}
