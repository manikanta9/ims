package com.clifton.investment.instrument.structure.calculators;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructure;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureWeight;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BaseInvestmentStructureCurrentWeightCalculator</code> ...
 *
 * @author manderson
 */
public abstract class BaseInvestmentStructureCurrentWeightCalculator implements InvestmentStructureCurrentWeightCalculator {

	public static final int DEFAULT_WEIGHT_PRECISION = 2;

	/**
	 * Allows users to override if they want more precision in the weight calculations
	 */
	private Integer weightPrecision;


	@SuppressWarnings("unused")
	@Override
	public void validateInvestmentInstrumentStructureWeight(InvestmentInstrumentStructureWeight instrumentWeight) {
		// No Default Validation Necessary
	}


	@Override
	public final void calculateCurrentWeights(InvestmentInstrumentStructure structure, List<InvestmentSecurityStructureAllocation> securityAllocationList, Date date) {
		BigDecimal additionalMultiplier = structure.getAdditionalMultiplier();
		if (additionalMultiplier == null) {
			additionalMultiplier = BigDecimal.ONE;
		}
		calculateCurrentWeightsImpl(structure, securityAllocationList, date, additionalMultiplier);
		// Re-Allocation Current Weights as a Percentage and Sum to 100
		reapplyCurrentWeightAsAPercentage(securityAllocationList);
	}


	public abstract void calculateCurrentWeightsImpl(InvestmentInstrumentStructure structure, List<InvestmentSecurityStructureAllocation> securityAllocationList, Date date,
	                                                 BigDecimal additionalMultiplier);


	protected void reapplyCurrentWeightAsAPercentage(List<InvestmentSecurityStructureAllocation> securityAllocationList) {
		BigDecimal total = CoreMathUtils.sumProperty(securityAllocationList, InvestmentSecurityStructureAllocation::getAllocationWeight);
		for (InvestmentSecurityStructureAllocation allocation : CollectionUtils.getIterable(securityAllocationList)) {
			allocation.setAllocationWeight(MathUtils.round(CoreMathUtils.getPercentValue(allocation.getAllocationWeight(), total, true), getCoalesceWeightPrecision()));
		}
		CoreMathUtils.applySumPropertyDifference(securityAllocationList, InvestmentSecurityStructureAllocation::getAllocationWeight, InvestmentSecurityStructureAllocation::setAllocationWeight, MathUtils.BIG_DECIMAL_ONE_HUNDRED, true, a -> true);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Integer getCoalesceWeightPrecision() {
		if (getWeightPrecision() != null) {
			return getWeightPrecision();
		}
		return DEFAULT_WEIGHT_PRECISION;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getWeightPrecision() {
		return this.weightPrecision;
	}


	public void setWeightPrecision(Integer weightPrecision) {
		this.weightPrecision = weightPrecision;
	}
}
