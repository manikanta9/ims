package com.clifton.investment.instrument.allocation;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public interface InvestmentSecurityAllocationService {

	////////////////////////////////////////////////////////////////////////////
	/////////       Investment Security Allocation Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityAllocation getInvestmentSecurityAllocation(int id);


	public List<InvestmentSecurityAllocation> getInvestmentSecurityAllocationList(SecurityAllocationSearchForm searchForm);


	public InvestmentSecurityAllocation saveInvestmentSecurityAllocation(InvestmentSecurityAllocation bean);


	/**
	 * If new bean with a start date, and existingId is not null and it has a null end date
	 * will also update existing end date to start date - 1
	 * Otherwise, just performs as a regular save
	 */
	public InvestmentSecurityAllocation saveInvestmentSecurityAllocationFromExisting(Integer existingId, InvestmentSecurityAllocation bean);


	public void saveInvestmentSecurityAllocationList(List<InvestmentSecurityAllocation> newList, List<InvestmentSecurityAllocation> oldList);


	public void deleteInvestmentSecurityAllocation(int id);


	@RequestMapping("investmentSecurityAllocationEnd")
	public void endInvestmentSecurityAllocation(InvestmentSecurityAllocationCommand command);

	////////////////////////////////////////////////////////////////////////////
	//////       Investment Security Allocation Dynamic Methods          ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Allowed for dynamic security allocations only.  Used to preview security allocations and weights for a given date
	 */
	public List<InvestmentSecurityAllocation> getInvestmentSecurityAllocationDynamicList(int securityId, Date date);

	////////////////////////////////////////////////////////////////////////////
	//////       Investment Security Allocation Rebalance Methods         //////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Delete a rebalance
	 * Ability to "undo" the last rebalance for an allocated security
	 * - checks that rebalance date is the last one and it wasn't the security start date
	 * - Delete method will delete all allocations with a start date on the rebalance date
	 * and all allocations that end on rebalance date - 1 will be updated with a null end date so
	 * they are still active.
	 */
	@SecureMethod(dtoClass = InvestmentSecurityAllocation.class, permissions = SecurityPermission.PERMISSION_WRITE)
	public void deleteInvestmentSecurityAllocationRebalance(int securityId, Date rebalanceDate);


	/**
	 * Returns for a Security all of the dates a "rebalance" has occurred
	 * <p/>
	 * A rebalance is the Security Start Date + (Any Allocation EndDate + 1) + (Any Allocation Start Date)
	 *
	 * @param populateDetails - Used for Usability/Display only to show information on which securities were updated and any notes entered on the allocation
	 */
	@SecureMethod(dtoClass = InvestmentSecurityAllocation.class)
	public List<InvestmentSecurityAllocationRebalance> getInvestmentSecurityAllocationRebalanceList(int parentInvestmentSecurityId, boolean populateDetails);


	/**
	 * Used for SHARE_PRICE only at this time (i.e. InvestmentSecurityAllocationTypes.isAutoRebalancingSupported() == true)
	 * Creates new set of allocations for the security using the base weights or shares on the base Date
	 * and starting new allocations on the selected startDate. Note: All previously active allocations will be
	 * first ended on startDate-1 and all allocations from the base date will be copied with a new start date and rebalanced based on selected weights or shares
	 *
	 * @param useBaseWeights - if true, uses base weights from selected base date, otherwise uses shares
	 * @param startingPrice  - if not set will use 100, used to calculate shares from weights
	 */
	@SecureMethod(dtoClass = InvestmentSecurityAllocation.class)
	public void processInvestmentSecurityAllocationRebalance(int securityId, Short dataSourceId, Date baseDate, boolean useBaseWeights, Date startDate, BigDecimal startingPrice);


	/**
	 * Allowed for securities that are allocations of securities that use an {@link com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes}
	 * that support automatic re-balancing
	 *
	 * @param rebalanceDate - Leave blank to recalculate all existing rebalances
	 * @param startingPrice - Defaults to 100 used only if updateWeights = false
	 * @param updateWeights - if allocationWeight or allocationShares should be updated
	 */
	@SecureMethod(dtoClass = InvestmentSecurityAllocation.class)
	public void rebuildInvestmentSecurityAllocationRebalance(int securityId, Short dataSourceId, Date rebalanceDate, BigDecimal startingPrice, boolean updateWeights);
}
