package com.clifton.investment.instrument.event.action.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class InvestmentSecurityEventActionTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String description;

	@SearchField(searchField = "eventType.id")
	private Short eventTypeId;

	@SearchField(searchField = "actionBean.id")
	private Integer actionBeanId;

	@SearchField
	private Boolean processBeforeEventJournal;

	@SearchField
	private Short processingOrder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}


	public Integer getActionBeanId() {
		return this.actionBeanId;
	}


	public void setActionBeanId(Integer actionBeanId) {
		this.actionBeanId = actionBeanId;
	}


	public Boolean getProcessBeforeEventJournal() {
		return this.processBeforeEventJournal;
	}


	public void setProcessBeforeEventJournal(Boolean processBeforeEventJournal) {
		this.processBeforeEventJournal = processBeforeEventJournal;
	}


	public Short getProcessingOrder() {
		return this.processingOrder;
	}


	public void setProcessingOrder(Short processingOrder) {
		this.processingOrder = processingOrder;
	}
}
