package com.clifton.investment.instrument.calculator.accrual.sign;


import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;


/**
 * The <code>InvestmentAccrualSignStructureCalculator</code> class provides Interest Rate Swap specific implementation of the InvestmentAccrualSignCalculator interface.
 *
 * @author vgomelsky
 */
public class InvestmentAccrualSignStructureCalculator implements InvestmentAccrualSignCalculator {

	private SystemColumnValueHandler systemColumnValueHandler;

	private String structureField = InvestmentSecurity.CUSTOM_FIELD_STRUCTURE;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isAccrualAmountNegated(InvestmentSecurity security, String couponFrequencyFieldName) {
		String structure = (String) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, getStructureField(), false);
		if (!"PAY_FLOAT".equalsIgnoreCase(structure) && !"PAY_FIXED".equalsIgnoreCase(structure)) {
			throw new FieldValidationException("Invalid Security Structure field value: " + structure, getStructureField());
		}

		boolean negate = ("PAY_FLOAT".equalsIgnoreCase(structure)); // as opposed to "PAY_FIXED"
		if (InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY.equalsIgnoreCase(couponFrequencyFieldName)) {
			// first set of fields ("Coupon Frequency" vs "Coupon Frequency 2")
			negate = !negate;
		}

		return negate;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getStructureField() {
		return this.structureField;
	}


	public void setStructureField(String structureField) {
		this.structureField = structureField;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
