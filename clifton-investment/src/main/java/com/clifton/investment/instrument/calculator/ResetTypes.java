package com.clifton.investment.instrument.calculator;


/**
 * The <code>ResetType</code> enum defines the types of resets for Total Return Swaps.
 *
 * @author vgomelsky
 */
public enum ResetTypes {

	/**
	 * Close existing position and open new position with the same number of units (quantity) but
	 * new notional based on the new price. This is most common ResetType for Total Return Swaps.
	 */
	KEEP_QUANTITY,
	/**
	 * Close existing position and open new position with the same notional (cost basis) but new
	 * units (quantity) based on the price.
	 */
	KEEP_NOTIONAL
}
