package com.clifton.investment.instrument.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Component;


/**
 * The InvestmentSecurityListBySymbolCache class caches the list of {@link InvestmentSecurity} objects by security symbol field.
 * This is very common use case that will reduce the number of database hits.  Most of the time, security symbols are globally unique.
 * Out of 100K securities, we have 50 cases when the same symbol is used exactly twice for different securities.
 *
 * @author vgomelsky
 */
@Component
public class InvestmentSecurityListBySymbolCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentSecurity, String> {


	@Override
	protected String getBeanKeyProperty() {
		return "symbol";
	}


	@Override
	protected String getBeanKeyValue(InvestmentSecurity security) {
		return security.getSymbol();
	}


	/**
	 * Returns the upper case string representation of the super implementation.
	 */
	@Override
	protected String getBeanKeySegmentForProperty(Object keyProperty) {
		return super.getBeanKeySegmentForProperty(keyProperty).toUpperCase();
	}
}
