package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.calculator.DayCountCommand;
import com.clifton.investment.instrument.calculator.DayCountConventions;

import java.math.BigDecimal;


/**
 * The AccrualConfig class holds parameters necessary to calculate accrued interest.
 *
 * @author vgomelsky
 */
public class AccrualConfig {

	private CouponFrequencies frequency;

	private DayCountConventions dayCountConvention;

	private String couponFrequencyField;

	/**
	 * For Day Count Conventions that rely on actual business days, these fields must be initialized.
	 */
	private CalendarBusinessDayService calendarBusinessDayService;
	private Short calendarId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccrualConfig() {
	}


	public static AccrualConfig of(CouponFrequencies frequency, DayCountConventions dayCountConvention, String couponFrequencyField) {
		AccrualConfig config = new AccrualConfig();

		config.frequency = frequency;
		config.dayCountConvention = dayCountConvention;
		config.couponFrequencyField = couponFrequencyField;

		return config;
	}


	public AccrualConfig withBusinessDayCalendar(CalendarBusinessDayService calendarBusinessDayService, Short calendarId) {
		this.calendarBusinessDayService = calendarBusinessDayService;
		this.calendarId = calendarId;
		return this;
	}


	public BigDecimal calculateAccrualRate(DayCountCommand command) {
		if (getDayCountConvention().isBusinessDaysDependent()) {
			command.withBusinessDayCalendar(this.calendarBusinessDayService, this.calendarId);
		}
		return getDayCountConvention().calculateAccrualRate(command);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getFrequency() {
		return this.frequency.getFrequency();
	}


	public DayCountConventions getDayCountConvention() {
		return this.dayCountConvention;
	}


	public String getCouponFrequencyField() {
		return this.couponFrequencyField;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public Short getCalendarId() {
		return this.calendarId;
	}
}
