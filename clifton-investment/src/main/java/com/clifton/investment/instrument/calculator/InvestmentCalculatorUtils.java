package com.clifton.investment.instrument.calculator;


import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;

import java.math.BigDecimal;


/**
 * The <code>InvestmentCalculatorUtils</code> class provides utility methods that perform various calculations.
 * <p>
 * NOTE: it's best to use InvestmentCalculator instead; this class was created for places where dependency injection
 * is not feasible (DTO's).
 *
 * @author vgomelsky
 */
public class InvestmentCalculatorUtils {

	/**
	 * Returns AccrualMethods entry for the specified security.
	 * Returns default AccrualMethods.DAYCOUNT if one is not defined.
	 */
	public static AccrualMethods getAccrualMethod(InvestmentSecurity security) {
		return ObjectUtils.coalesce(
				security.getAccrualMethod(),
				security.getInstrument().getAccrualMethod(),
				security.getInstrument().getHierarchy().getAccrualMethod(),
				AccrualMethods.DAYCOUNT
		);
	}


	/**
	 * Returns AccrualMethods entry for the second event type of the specified security.
	 * Returns default AccrualMethods.DAYCOUNT if one is not defined.
	 * <p>
	 * In rare cases, there maybe 2 separate accrual events: specifies optional second event (IRS: fixed and floating legs).
	 */
	public static AccrualMethods getAccrualMethod2(InvestmentSecurity security) {
		return ObjectUtils.coalesce(
				security.getAccrualMethod2(),
				security.getInstrument().getAccrualMethod2(),
				security.getInstrument().getHierarchy().getAccrualMethod2(),
				AccrualMethods.DAYCOUNT
		);
	}


	/**
	 * Returns AccrualCalculator that should be used in accrual calculations for the specified security.
	 * Returns null if one is not set.
	 */
	public static AccrualDateCalculators getAccrualDateCalculator(InvestmentSecurity security) {
		return ObjectUtils.coalesce(
				security.getInstrument().getAccrualDateCalculator(),
				security.getInstrument().getHierarchy().getAccrualDateCalculator()
		);
	}


	/**
	 * Returns AccrualCalculator that should be used in accrual calculations for Accrual Event Type 2 of the specified security.
	 * Returns null if one is not set.
	 */
	public static AccrualDateCalculators getAccrualDateCalculator2(InvestmentSecurity security) {
		return ObjectUtils.coalesce(
				security.getInstrument().getAccrualDateCalculator2(),
				security.getInstrument().getHierarchy().getAccrualDateCalculator2()
		);	}


	/**
	 * Returns the InvestmentNotionalCalculatorTypes for the specified security.
	 * COALESCE(instrument calculator, hierarchy calculator, instrument currency denomination calculator, default calculator)
	 * <p>
	 * Note: Currency denomination calculator is used to default rounding for some currencies.
	 * For example, JPY rounds to 0 decimal places because there are no cents in Japan.
	 */
	public static InvestmentNotionalCalculatorTypes getNotionalCalculator(InvestmentSecurity security) {
		InvestmentNotionalCalculatorTypes calculator = InvestmentNotionalCalculatorTypes.getDefaultCalculatorType();
		if (security != null) {
			InvestmentInstrument instrument = security.getInstrument();
			if (instrument != null) {
				if (instrument.getCostCalculator() != null) {
					calculator = instrument.getCostCalculator();
				}
				else if (instrument.getHierarchy() != null && instrument.getHierarchy().getCostCalculator() != null) {
					calculator = instrument.getHierarchy().getCostCalculator();
				}
				else if (instrument.getTradingCurrency() != null && instrument.getTradingCurrency().getInstrument().getCostCalculator() != null) {
					// For example, all JPY denominated securities default to 0 decimals rounding (no pennies in Japan)
					calculator = instrument.getTradingCurrency().getInstrument().getCostCalculator();
				}
			}
		}
		return calculator;
	}


	/**
	 * Calculates and returns contract value for the specified security for the specified price.
	 * Contract value is adjusted for instrument's multiplier and rounding (if necessary).
	 * <p>
	 * Some securities (Australian Bond Futures) may use fairly complex calculation to get contract value.
	 * Optionally applies exposure multiplier adjustment (annualized equivalent of quarterly rates, etc.)
	 */
	public static BigDecimal calculateContractValue(InvestmentSecurity security, BigDecimal price, boolean applyExposureMultiplier) {
		if (price == null) {
			return BigDecimal.ZERO;
		}

		InvestmentNotionalCalculatorTypes calculator = getNotionalCalculator(security);
		BigDecimal contractValue = calculator.calculateContractValue(price, security.getPriceMultiplier());
		if (applyExposureMultiplier) {
			contractValue = MathUtils.multiply(contractValue, security.getInstrument().getExposureMultiplier());
		}

		return contractValue;
	}


	/**
	 * Returns Initial Margin Requirement for the security of this transaction.
	 * Based on whether the holding account is used for hedging or speculation, different margin requirement will be used.
	 * Also, if the holding account is setup to have initial margin multiplier that is not 1, will adjust margin requirements accordingly.
	 */
	public static BigDecimal calculateRequiredCollateral(InvestmentSecurity security, InvestmentAccount holdingAccount, BigDecimal quantity, BigDecimal fxRate, boolean throwExceptionForMissingMargin) {
		BigDecimal instrumentMargin = BigDecimal.ZERO;

		if (!MathUtils.isNullOrZero(quantity)) {
			boolean speculation = holdingAccount.isUsedForSpeculation();

			// Only applies to positions with Actual Contracts != 0 AND Futures
			if (!MathUtils.isNullOrZero(quantity) && InvestmentUtils.isSecurityOfType(security, InvestmentType.FUTURES)) {

				// If Used for Speculation - Can only use Speculation
				if (speculation) {
					instrumentMargin = security.getInstrument().getSpeculatorInitialMarginPerUnit();
				}
				// Otherwise if using hedger value, and not defined, can use speculator
				else {
					instrumentMargin = security.getInstrument().getHedgerInitialMarginPerUnit();
					// Zero value is allowed - just not null
					if (instrumentMargin == null) {
						instrumentMargin = security.getInstrument().getSpeculatorInitialMarginPerUnit();
					}
				}
				// Zero value is allowed - just not null
				if (instrumentMargin == null) {
					if (throwExceptionForMissingMargin) {
						throw new ValidationException("Unable to determine initial margin for Security [" + security.getLabel() + "].  Instrument "
								+ (speculation ? " Speculator " : " Speculator and Hedger ") + "initial margin value is not defined.");
					}
				}
				// If instrumentMargin is zero, return zero since result multiplies by this value
				else if (MathUtils.isEqual(instrumentMargin, BigDecimal.ZERO)) {
					return BigDecimal.ZERO;
				}
				else {
					// Take into account additional margin requirement multiplier
					instrumentMargin = MathUtils.multiply(instrumentMargin, holdingAccount.getInitialMarginMultiplier());
					// Take Into Account Foreign Values
					instrumentMargin = MathUtils.multiply(instrumentMargin, fxRate);
					instrumentMargin = MathUtils.abs(MathUtils.multiply(instrumentMargin, quantity));
				}
			}
		}
		return instrumentMargin;
	}


	/**
	 * Returns the exchange rate from the specified forward price applying corresponding cross-currency convention.
	 */
	public static BigDecimal calculateExchangeRateFromForwardPrice(InvestmentSecurity currencyForward, BigDecimal price) {
		return getNotionalCalculator(currencyForward).isDivideByPrice() ? MathUtils.divide(BigDecimal.ONE, price, DataTypes.EXCHANGE_RATE.getPrecision()) : price;
	}


	/**
	 * Will determine the notional calculator for the base currency of the client account, and return the calculated base amount (localAmount * fx_rate) rounded to the
	 * correct decimal place.
	 */
	public static BigDecimal calculateBaseAmount(BigDecimal localAmount, BigDecimal fxRateToBase, InvestmentAccount clientAccount) {
		ValidationUtils.assertNotNull(clientAccount, "Client Account is required in order to determine Base Currency and round accordingly.");
		return calculateBaseAmount(localAmount, fxRateToBase, clientAccount.getBaseCurrency());
	}


	/**
	 * Will determine the notional calculator for the base currency, and return the calculated base amount (localAmount * fx_rate) rounded to the
	 * correct decimal place.
	 */
	public static BigDecimal calculateBaseAmount(BigDecimal localAmount, BigDecimal fxRateToBase, InvestmentSecurity baseCurrency) {
		InvestmentNotionalCalculatorTypes notionalCalculator = getNotionalCalculator(baseCurrency);
		return notionalCalculator.round(MathUtils.multiply(localAmount, fxRateToBase));
	}


	/**
	 * Will determine the notional calculator for the local currency, and return the calculated local amount (baseAmount / fx_rate) rounded to the
	 * correct decimal place.
	 */
	public static BigDecimal calculateLocalAmount(BigDecimal baseAmount, BigDecimal fxRateToBase, InvestmentSecurity localCurrency) {
		InvestmentNotionalCalculatorTypes notionalCalculator = getNotionalCalculator(localCurrency);
		return notionalCalculator.round(MathUtils.divide(baseAmount, fxRateToBase));
	}


	/**
	 * Rounds the specified base amount using hte rounding method of the base currency denomination for the given client account. This returns a properly rounded {@link
	 * BigDecimal#ZERO 0} if the specified <code>baseAmount</code> is <code>null</code>.
	 */
	public static BigDecimal roundBaseAmount(BigDecimal baseAmount, InvestmentAccount clientAccount) {
		return InvestmentCalculatorUtils.getNotionalCalculator(clientAccount.getBaseCurrency()).round(baseAmount == null ? BigDecimal.ZERO : baseAmount);
	}


	/**
	 * Rounds the specified local amount using rounding method of currency denomination of the specified security.
	 * Returns properly rounded 0 if the specified localAmount is null.
	 */
	public static BigDecimal roundLocalAmount(BigDecimal localAmount, InvestmentSecurity security) {
		return InvestmentCalculatorUtils.getNotionalCalculator(security.getInstrument().getTradingCurrency()).round(localAmount == null ? BigDecimal.ZERO : localAmount);
	}
}
