package com.clifton.investment.instrument.event.payout;

import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;


/**
 * The InvestmentSecurityEventClientElection class represents a specific election of payout(s) that each client made
 * for each corporate action (event) with multiple elections.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityEventClientElection extends BaseEntity<Integer> {

	private InvestmentSecurityEvent securityEvent;
	private InvestmentAccount clientInvestmentAccount;
	/**
	 * Auto-generated election number by corporate actions data provider such as Markit that is used to group
	 * multiple payment(s) for the same election.  See {@link InvestmentSecurityEventPayout#electionNumber}.
	 */
	private short electionNumber;
	/**
	 * The quantity will usually equal to the quantity of the total position held.
	 * However, it's possible to apply one election to a portion of one's position while a different election to the rest of that position.
	 * Null quantity, means total quantity held by the client (most common election).
	 */
	private BigDecimal electionQuantity;

	/**
	 * Election Value will be used to allow clients to declare a bid on Dutch Auctions for consideration by the Event Offeror.
	 */
	private BigDecimal electionValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEvent getSecurityEvent() {
		return this.securityEvent;
	}


	public void setSecurityEvent(InvestmentSecurityEvent securityEvent) {
		this.securityEvent = securityEvent;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public short getElectionNumber() {
		return this.electionNumber;
	}


	public void setElectionNumber(short electionNumber) {
		this.electionNumber = electionNumber;
	}


	public BigDecimal getElectionQuantity() {
		return this.electionQuantity;
	}


	public void setElectionQuantity(BigDecimal electionQuantity) {
		this.electionQuantity = electionQuantity;
	}


	public BigDecimal getElectionValue() {
		return this.electionValue;
	}


	public void setElectionValue(BigDecimal electionValue) {
		this.electionValue = electionValue;
	}
}
