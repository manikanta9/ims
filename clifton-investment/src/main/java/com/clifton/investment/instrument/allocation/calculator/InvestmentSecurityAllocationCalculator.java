package com.clifton.investment.instrument.allocation.calculator;


import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;


/**
 * The <code>InvestmentSecurityAllocationCalculator</code> interface defines the methods used for
 * allocated securities.
 * <p/>
 * NOTE: All current allocation types currently depend on Market Data, so calculators are implemented there
 *
 * @author manderson
 */
public interface InvestmentSecurityAllocationCalculator {

	public InvestmentSecurityAllocationTypes getInvestmentSecurityAllocationType();


	/////////////////////////////////////////////////


	/**
	 * Called when saving an allocation.  Different calculators require different fields
	 */
	public void validateInvestmentSecurityAllocation(InvestmentSecurityAllocation bean);
}
