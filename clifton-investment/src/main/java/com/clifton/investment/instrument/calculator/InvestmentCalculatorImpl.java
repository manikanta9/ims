package com.clifton.investment.instrument.calculator;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarBusinessDayTypes;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.calendar.setup.CalendarTimeZone;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.calculator.accrual.AccrualDatesCommand;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.calculator.accrual.InvestmentAccrualCalculator;
import com.clifton.investment.instrument.calculator.accrual.InvestmentAccrualCalculatorFactory;
import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.instrument.calculator.accrual.date.InvestmentAccrualDateCalculator;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;


/**
 * The <code>InvestmentCalculatorImpl</code> class provides basic implementation of investment calculator.
 *
 * @author vgomelsky
 */
@Component
public class InvestmentCalculatorImpl implements InvestmentCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;

	private InvestmentAccrualCalculatorFactory investmentAccrualCalculatorFactory;
	private InvestmentCurrencyService investmentCurrencyService;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private SystemBeanService systemBeanService;
	private SystemColumnService systemColumnService;
	private SystemColumnValueHandler systemColumnValueHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Note: Changes to this method should also be reviewed/applied to OrderSharedUtilHandlerImpl.calculateTradeDate
	 * Applies the same logic, however uses different security objects/structure
	 */
	@Override
	public Date calculateTradeDate(InvestmentSecurity security, Date transactionDate) {
		if (transactionDate == null) {
			transactionDate = new Date();
		}
		Date tradeDate = transactionDate;
		// No exchange - Use today
		InvestmentExchange exchange = InvestmentUtils.getSecurityExchange(security);
		if (exchange != null) {
			Calendar calendar = exchange.getCalendar();
			if (calendar != null) {
				CalendarTimeZone timeZone = exchange.getTimeZone();
				if (timeZone != null) {
					// Find out what day it is on the exchange
					tradeDate = DateUtils.getDateInTimeZone(tradeDate, timeZone.getName());
				}
				// Is "today" a business day?
				if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forTrade(tradeDate, calendar.getId()))) {
					// If not, return next business day
					tradeDate = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forTrade(tradeDate, calendar.getId()));
				}
				else {
					// Otherwise check against time
					Time closingTime = exchange.getCloseTime();

					// If it is a business day - is it a PARTIAL business day?
					// Only call this if the exchange has different hours for partial or full day
					if (exchange.isPartialDayHoursDefinedAndDifferent() && getCalendarBusinessDayService().isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(tradeDate, calendar.getId()))) {
						closingTime = exchange.getCloseTimePartialDay();
					}

					// Otherwise check against time
					if (closingTime != null) {
						int currentTime = DateUtils.getMillisecondsFromMidnight(tradeDate);
						// If after closing time - move to next business day
						if (closingTime.getMilliseconds() < currentTime) {
							tradeDate = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(tradeDate, calendar.getId()));
						}
					}
				}
			}
		}
		return DateUtils.clearTime(tradeDate);
	}


	/**
	 * Note: Changes to this method should also be reviewed/applied to OrderSharedUtilHandlerImpl.calculateSettlementDate
	 * Applies the same logic, however uses different security objects/structure
	 */
	@Override
	public Date calculateSettlementDate(InvestmentSecurity security, InvestmentSecurity settlementCurrency, Date transactionDate) {
		ValidationUtils.assertNotNull(transactionDate, "Transaction date is required from which settlement date is calculated", "tradeDate");
		Integer daysToSettle = getInvestmentSecurityDaysToSettle(security, settlementCurrency);

		Calendar calendar = getInvestmentSecurityCalendar(security);
		Calendar settlementCalendar = (settlementCurrency != null) ? getInvestmentSecurityCalendar(settlementCurrency) : null;
		Calendar[] calendars = ArrayUtils.getStream(calendar, settlementCalendar).filter(Objects::nonNull).toArray(Calendar[]::new);

		// If days to settle = 0 and date is NOT a business day - need to move forward an extra day
		// This happens if a security can be traded today, but couldn't settle today
		if (daysToSettle == 0) {
			for (Calendar cal : calendars) {
				if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forSettlement(transactionDate, cal.getId()))) {
					daysToSettle++;
					break;
				}
			}
		}

		Date result;
		if (calendars.length == 1) {
			result = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forSettlement(transactionDate, calendar.getId()), daysToSettle);
		}
		else {
			result = getCalendarBusinessDayService().getBusinessDayFrom(transactionDate, daysToSettle, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY, calendars);
		}

		// cannot settle a trade before Issue Date for a bond
		if (security.getStartDate() != null && DateUtils.compare(security.getStartDate(), result, false) > 0) {
			result = security.getStartDate();
		}
		return result;
	}


	@Override
	public AccrualDates calculateAccrualDates(AccrualDatesCommand command) {
		InvestmentSecurity security = command.getSecurity();
		Date positionOpenTransactionDate = command.getPositionOpenTransactionDate();
		Date positionOpenSettlementDate = command.getPositionOpenSettlementDate();
		Date accrueUpToDate = command.getAccrueUpToDate();

		AccrualDateCalculators dateCalculator1 = InvestmentCalculatorUtils.getAccrualDateCalculator(security);
		AccrualDateCalculators dateCalculator2 = InvestmentCalculatorUtils.getAccrualDateCalculator2(security);

		Date accrueFromDate1 = calculateAccrueFromDate(positionOpenTransactionDate, positionOpenSettlementDate, dateCalculator1);
		Date accrueFromDate2 = calculateAccrueFromDate(positionOpenTransactionDate, positionOpenSettlementDate, dateCalculator2);

		// use transaction date if no calculator is defined
		Date accrueUpToDate1 = calculateAccrueUpToDate(accrueUpToDate, positionOpenSettlementDate, security, dateCalculator1);
		Date accrueUpToDate2 = calculateAccrueUpToDate(accrueUpToDate, positionOpenSettlementDate, security, dateCalculator2);

		return AccrualDates.of(accrueFromDate1, accrueFromDate2, accrueUpToDate1, accrueUpToDate2);
	}


	private Date calculateAccrueFromDate(Date positionOpenTransactionDate, Date positionOpenSettlementDate, AccrualDateCalculators dateCalculator) {
		if (dateCalculator != null) {
			if (dateCalculator.isAccrualFromPositionOpenSettlement()) {
				// End Of Day Convention: no accrual on the opening date
				return positionOpenSettlementDate;
			}
			else if (dateCalculator.isAccrualFromPositionOpenTransaction()) {
				// End Of Day Convention: no accrual on the opening date
				return positionOpenTransactionDate;
			}
		}
		return null;
	}


	private Date calculateAccrueUpToDate(Date accrueUpToDate, Date positionOpenSettlementDate, InvestmentSecurity security, AccrualDateCalculators dateCalculator) {
		Date result = accrueUpToDate;
		if (dateCalculator != null) {
			InvestmentAccrualDateCalculator calculator = getInvestmentAccrualCalculatorFactory().getInvestmentAccrualDateCalculator(dateCalculator);
			result = calculator.calculateAccrualDate(security, result);

			// cannot be before settlement date of opening position (sometimes bonds may take more days to settle than usually)
			if (positionOpenSettlementDate != null && dateCalculator.isAccrueUpToOpeningSettlement() && DateUtils.compare(positionOpenSettlementDate, result, false) > 0) {
				result = positionOpenSettlementDate;
			}
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculateNotional(InvestmentSecurity security, BigDecimal price, BigDecimal quantity, Date notionalMultiplierLookupDate) {
		return calculateNotional(security, price, quantity, getNotionalMultiplier(security, notionalMultiplierLookupDate));
	}


	@Override
	public BigDecimal calculateNotional(InvestmentSecurity security, BigDecimal price, BigDecimal quantity, BigDecimal notionalMultiplier) {
		return doCalculateNotional(security, price, quantity, notionalMultiplier, false, null);
	}


	@Override
	public BigDecimal calculateNotional(InvestmentSecurity security, BigDecimal price, BigDecimal quantity, BigDecimal notionalMultiplier, InvestmentNotionalCalculatorTypes
			roundingCalculator) {
		return doCalculateNotional(security, price, quantity, notionalMultiplier, false, roundingCalculator);
	}


	@Override
	public BigDecimal calculateQuantityFromNotional(InvestmentSecurity security, BigDecimal price, BigDecimal notional, BigDecimal notionalMultiplier) {
		return doCalculateNotional(security, price, notional, notionalMultiplier, true, null);
	}


	@Override
	public BigDecimal calculateQuantityFromNotional(InvestmentSecurity security, BigDecimal price, BigDecimal notional, BigDecimal
			notionalMultiplier, InvestmentNotionalCalculatorTypes roundingCalculator) {
		return doCalculateNotional(security, price, notional, notionalMultiplier, true, roundingCalculator);
	}


	private BigDecimal doCalculateNotional(InvestmentSecurity security, BigDecimal price, BigDecimal quantity, BigDecimal notionalMultiplier,
	                                       boolean reverse, InvestmentNotionalCalculatorTypes roundingCalculator) {
		if (security.isCurrency()) {
			return quantity;
		}
		if (quantity == null || price == null) {
			return BigDecimal.ZERO;
		}

		// get the decimal precision for quantity
		Short quantityScale = InvestmentUtils.getQuantityDecimalPrecision(security);

		// index ratio adjustment for TIPS? Skip if price is already index ratio adjusted (old style British linkers)
		if (notionalMultiplier != null && !security.getInstrument().getHierarchy().isNotionalMultiplierForPriceNotUsed()) {
			quantity = reverse ? MathUtils.divide(quantity, notionalMultiplier) : quantity.multiply(notionalMultiplier);
		}

		InvestmentNotionalCalculatorTypes calculator = InvestmentCalculatorUtils.getNotionalCalculator(security);
		if (calculator.isDivideByPrice() && MathUtils.isNullOrZero(price)) {
			throw new ValidationException(security + " security cannot have 0 Price because it uses division by price in its Notional Calculator.");
		}
		roundingCalculator = roundingCalculator != null ? roundingCalculator : calculator;
		return reverse ? calculator.calculateQuantityFromNotional(price, security.getPriceMultiplier(), quantity, quantityScale)
				: roundingCalculator.round(calculator.calculateNotionalWithoutRounding(price, security.getPriceMultiplier(), quantity));
	}


	@Override
	public BigDecimal calculateExposure(InvestmentSecurity security, BigDecimal price, BigDecimal quantity) {
		return calculateNotional(security, price, quantity, security.getInstrument().getExposureMultiplier());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculateAccruedInterest(InvestmentSecurity security, AccrualBasis accrualBasis, BigDecimal notionalMultiplier, AccrualDates accrualDates) {
		BigDecimal accrual1 = doCalculateAccruedInterestForEvent1(security, accrualBasis.getAccrualBasis1(), notionalMultiplier, accrualDates.getAccrueUpToDate1(), accrualDates.getAccrueFromDate1());
		if (accrual1 == null) {
			// null for first accrual means second accrual is not applicable
			return InvestmentCalculatorUtils.roundLocalAmount(BigDecimal.ZERO, security);
		}
		BigDecimal accrual2 = calculateAccruedInterestForEvent2(security, accrualBasis.getAccrualBasis2(), notionalMultiplier, accrualDates.getAccrueUpToDate2(), accrualDates.getAccrueFromDate2());
		return MathUtils.add(accrual1, accrual2);
	}


	@Override
	public BigDecimal calculateAccruedInterestForEvent1(InvestmentSecurity security, BigDecimal notional, BigDecimal notionalMultiplier, Date accrueUpToDate, Date
			accrueFromDate) {
		BigDecimal result = doCalculateAccruedInterestForEvent1(security, notional, notionalMultiplier, accrueUpToDate, accrueFromDate);
		return (result == null) ? InvestmentCalculatorUtils.roundLocalAmount(BigDecimal.ZERO, security) : result;
	}


	/**
	 * Returns null if there is no accrual and one shouldn't attempt calculating it for the second event
	 */
	private BigDecimal doCalculateAccruedInterestForEvent1(InvestmentSecurity security, BigDecimal accrualBasis, BigDecimal notionalMultiplier, Date accrueUpToDate, Date
			accrueFromDate) {
		String paymentEventType = InvestmentUtils.getAccrualEventTypeName(security);
		if (paymentEventType == null) {
			// security doesn't support accruals
			return null;
		}

		// find interest period for the specified date: End Of Day convention: last day of accrual period means next period
		AccrualDateCalculators dateCalculator = InvestmentCalculatorUtils.getAccrualDateCalculator(security);
		InvestmentSecurityEvent paymentEvent = getPaymentEvent(security, paymentEventType, accrueUpToDate);

		if (paymentEvent == null) {
			return null;
		}
		if (paymentEvent.getAccrualStartDate().compareTo(paymentEvent.getPaymentDate()) >= 0) {
			throw new ValidationException("Coupon period start date " + paymentEvent.getAccrualStartDate() + " cannot be after or on the payment date " + paymentEvent.getPaymentDate()
					+ " for coupon of " + paymentEvent.getAfterEventValue() + " for security " + security.getLabel());
		}

		if (dateCalculator != null && dateCalculator.isNoAccrualFromDayBeforeEx() && !accrueUpToDate.before(DateUtils.addDays(paymentEvent.getExDate(), -1))) {
			// no accrued interest from the day before Ex Date: already have receivable for full payment amount
			return null;
		}

		if (notionalMultiplier != null && security.getInstrument().getHierarchy().isNotionalMultiplierForAccrualNotUsed()) {
			notionalMultiplier = null;
		}

		// calculated accrued interest
		AccrualMethods accrualMethod = InvestmentCalculatorUtils.getAccrualMethod(paymentEvent.getSecurity());
		InvestmentAccrualCalculator calculator = getInvestmentAccrualCalculatorFactory().getInvestmentAccrualCalculator(accrualMethod);
		BigDecimal result = calculator.calculateAccruedInterest(paymentEvent, accrualBasis, notionalMultiplier, accrueUpToDate);

		// some securities only include accrual from position open instead of full period:
		// Accrual = (Period Start to Accrue up to Date) - (Period Start to Accrue from Date)
		if (accrueFromDate != null && accrueFromDate.after(paymentEvent.getAccrualStartDate())) {
			if (dateCalculator != null && !dateCalculator.isAccrualFromPeriodStart()) {
				BigDecimal accrualToExclude = calculator.calculateAccruedInterest(paymentEvent, accrualBasis, notionalMultiplier, accrueFromDate);
				result = MathUtils.subtract(result, accrualToExclude);
			}
		}

		return result;
	}


	private InvestmentSecurityEvent getPaymentEvent(InvestmentSecurity security, String paymentEventType, Date accrueUpToDate) {
		Date eventLookupDate = accrueUpToDate;
		AccrualDateCalculators dateCalculator = InvestmentCalculatorUtils.getAccrualDateCalculator(security);
		if (dateCalculator != null && !dateCalculator.isAccrualEndDateNotAdjusted()) {
			// TRS: adjust the end date for event lookup
			eventLookupDate = DateUtils.addDays(eventLookupDate, -1);
		}
		return getInvestmentSecurityEventService().getInvestmentSecurityEventForAccrualEndDate(security.getId(), paymentEventType, eventLookupDate);
	}


	@Override
	public BigDecimal calculateAccruedInterestForEvent2(InvestmentSecurity security, BigDecimal accrualBasis, BigDecimal notionalMultiplier, Date accrueUpToDate, Date
			accrueFromDate) {
		BigDecimal result = InvestmentCalculatorUtils.roundLocalAmount(BigDecimal.ZERO, security);

		// check if there's second accrual event (Fixed and Floating Legs of IRS)
		String paymentEventType2 = InvestmentUtils.getAccrualEventTypeName2(security);
		if (paymentEventType2 != null) {
			InvestmentSecurityEvent paymentEvent = getPaymentEvent(security, paymentEventType2, accrueUpToDate);
			if (paymentEvent != null) {
				if (paymentEvent.getAccrualStartDate().compareTo(paymentEvent.getPaymentDate()) >= 0) {
					throw new ValidationException("Coupon period start date " + paymentEvent.getAccrualStartDate() + " cannot be after or on the payment date " + paymentEvent.getPaymentDate()
							+ " for coupon of " + paymentEvent.getAfterEventValue() + " for security " + security.getLabel());
				}

				if (notionalMultiplier != null && security.getInstrument().getHierarchy().isNotionalMultiplierForAccrualNotUsed()) {
					notionalMultiplier = null;
				}

				AccrualMethods accrualMethod = InvestmentCalculatorUtils.getAccrualMethod2(paymentEvent.getSecurity());
				InvestmentAccrualCalculator calculator = getInvestmentAccrualCalculatorFactory().getInvestmentAccrualCalculator(accrualMethod);
				result = calculator.calculateAccruedInterest(paymentEvent, accrualBasis, notionalMultiplier, accrueUpToDate);

				// some securities only include accrual from position open instead of full period:
				// Accrual = (Period Start to Accrue up to Date) - (Period Start to Accrue from Date)
				if (accrueFromDate != null && accrueFromDate.after(paymentEvent.getAccrualStartDate())) {
					AccrualDateCalculators dateCalculator = InvestmentCalculatorUtils.getAccrualDateCalculator2(paymentEvent.getSecurity());
					if (dateCalculator != null && !dateCalculator.isAccrualFromPeriodStart()) {
						BigDecimal accrualToExclude = calculator.calculateAccruedInterest(paymentEvent, accrualBasis, notionalMultiplier, accrueFromDate);
						result = MathUtils.subtract(result, accrualToExclude);
					}
				}
				LogUtils.info(getClass(), security.getSymbol() + " on " + DateUtils.fromDateShort(accrueUpToDate) + ": " + paymentEventType2 + " = " + result);
			}
		}
		return result;
	}


	@Override
	public BigDecimal calculateInterestPayment(InvestmentSecurityEvent paymentEvent, BigDecimal accrualBasis, BigDecimal notionalMultiplier, Date accrueFromDate) {
		if (paymentEvent == null) {
			return BigDecimal.ZERO;
		}

		// match event type to corresponding accrual method: one or two
		AccrualMethods accrualMethod = InvestmentCalculatorUtils.getAccrualMethod(paymentEvent.getSecurity());
		String accrualEventType = InvestmentUtils.getAccrualEventTypeName(paymentEvent.getSecurity());
		String paymentEventType = paymentEvent.getType().getName();
		if (!paymentEventType.equals(accrualEventType)) {
			accrualEventType = InvestmentUtils.getAccrualEventTypeName2(paymentEvent.getSecurity());
			if (!paymentEventType.equals(accrualEventType)) {
				throw new ValidationException("Interest Payment event type '" + paymentEventType + "' is not allowed for corresponding security: " + paymentEvent);
			}
			accrualMethod = InvestmentCalculatorUtils.getAccrualMethod2(paymentEvent.getSecurity());
		}

		if (notionalMultiplier != null && paymentEvent.getSecurity().getInstrument().getHierarchy().isNotionalMultiplierForAccrualNotUsed()) {
			notionalMultiplier = null;
		}

		InvestmentAccrualCalculator calculator = getInvestmentAccrualCalculatorFactory().getInvestmentAccrualCalculator(accrualMethod);
		BigDecimal result = calculator.calculateAccruedInterest(paymentEvent, accrualBasis, notionalMultiplier, paymentEvent.getAccrualEndDate());

		if (accrueFromDate != null && accrueFromDate.after(paymentEvent.getAccrualStartDate())) {
			// some securities only include accrual from position open instead of full accrual period
			BigDecimal accrualToExclude = calculator.calculateAccruedInterest(paymentEvent, accrualBasis, notionalMultiplier, accrueFromDate);
			result = MathUtils.subtract(result, accrualToExclude);
		}

		return result;
	}


	@Override
	public BigDecimal calculateInterestPayment(BigDecimal notional, BigDecimal annualRate, Date periodStartDate, Date periodEndDate, Date accrualStartDate, DayCountConventions dayCountConvention, CouponFrequencies frequency) {
		ValidationUtils.assertNotNull(notional, "Notional is required in order to calculate interest payment", "notional");
		ValidationUtils.assertNotNull(annualRate, "Annual Rate is required in order to calculate interest payment", "annualRate");
		ValidationUtils.assertNotNull(dayCountConvention, "Day Count Convention is required in order to calculate interest payment", "dayCountConvention");
		ValidationUtils.assertNotNull(frequency, "Coupon Frequency is required in order to calculate interest payment", "frequency");

		if (accrualStartDate == null) {
			accrualStartDate = periodStartDate;
		}

		return annualRate.movePointLeft(2)
				.multiply(notional)
				.multiply(dayCountConvention.calculateAccrualRate(DayCountCommand.forAccrualRange(accrualStartDate, periodEndDate).withAccrualPeriod(periodStartDate, periodEndDate).withFrequency(frequency)))
				.setScale(2, BigDecimal.ROUND_HALF_UP);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculateDirtyPrice(InvestmentSecurity security, BigDecimal quantity, BigDecimal cleanPrice, Date date) {
		BigDecimal notionalMultiplier = getNotionalMultiplier(security, date);
		// If Notional Multiplier IS NULL default it to 1
		if (notionalMultiplier == null) {
			notionalMultiplier = BigDecimal.ONE;
		}

		// dirty price calculation includes accrued interest so needs coupon: can use any sizable face
		// however, it does not include RECEIVABLES
		// Dirty Price calculations are specific to the notional calculator - so actual implementation is there
		if (MathUtils.isNullOrZero(quantity)) {
			quantity = BigDecimal.valueOf(1_000_000_000);
		}
		BigDecimal accruedInterest = calculateAccruedInterest(security, AccrualBasis.of(quantity), notionalMultiplier, AccrualDates.ofUpToDate(date));
		BigDecimal notional = calculateNotional(security, cleanPrice, quantity, notionalMultiplier);

		// Price is already index Ratio adjusted, so set it to one
		if (security.getInstrument().getHierarchy().isNotionalMultiplierForPriceNotUsed()) {
			notionalMultiplier = BigDecimal.ONE;
		}
		return InvestmentCalculatorUtils.getNotionalCalculator(security).calculateDirtyPrice(cleanPrice, quantity, notional, accruedInterest, security.getPriceMultiplier(), notionalMultiplier);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Calendar getInvestmentSecurityCalendar(InvestmentSecurity security) {
		InvestmentInstrument instrument = security.getInstrument();
		if (instrument.getHierarchy().getSettlementCalendarColumn() != null) {
			SystemColumn column = getSystemColumnService().getSystemColumn(instrument.getHierarchy().getSettlementCalendarColumn().getId());
			Integer calendarId = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, column.getName(), false);
			if (calendarId != null) {
				return getCalendarSetupService().getCalendar(calendarId.shortValue());
			}
		}
		if (instrument.getSettlementCalendar() != null) {
			return instrument.getSettlementCalendar();
		}

		InvestmentExchange exchange = InvestmentUtils.getSecurityExchange(security);
		if (exchange != null && exchange.getCalendar() != null) {
			return exchange.getCalendar();
		}

		return getCalendarSetupService().getDefaultCalendar();
	}


	@Override
	public Integer getInvestmentSecurityDaysToSettle(InvestmentSecurity security, InvestmentSecurity settlementCurrency) {
		Integer daysToSettle = null;

		// for currencies, settlement cycle is defined per currency pair
		if (security.isCurrency() && settlementCurrency != null) {
			daysToSettle = getInvestmentCurrencyService().getInvestmentCurrencyConventionDaysToSettle(security.getSymbol(), settlementCurrency.getSymbol());
		}

		InvestmentInstrument instrument = security.getInstrument();
		if (daysToSettle == null) {
			daysToSettle = instrument.getDaysToSettle();
			if (daysToSettle == null) {
				InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
				daysToSettle = hierarchy.getDaysToSettle();
				if (daysToSettle == null) {
					daysToSettle = hierarchy.getInvestmentType().getDaysToSettle();
				}
			}
		}

		// some securities (IRS, TRS) have security specific settlement
		if (InvestmentUtils.isInstrumentOfType(instrument, InvestmentType.SWAPS)) {
			Integer daysToSettleCustom = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CYCLE, false);
			if (daysToSettleCustom != null) {
				daysToSettle = daysToSettleCustom;
			}
		}
		return daysToSettle;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getNotionalMultiplier(InvestmentSecurity security, Date date) {
		BigDecimal notionalMultiplier = null;
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		if (hierarchy.isNotionalMultiplierUsed()) {
			NotionalMultiplierRetriever notionalMultiplierRetriever = (NotionalMultiplierRetriever) getSystemBeanService().getBeanInstance(hierarchy.getNotionalMultiplierRetriever());
			notionalMultiplier = notionalMultiplierRetriever.retrieveNotionalMultiplier(security, () -> date, true);
		}
		return notionalMultiplier;
	}


	@Override
	public BigDecimal getNotionalMultiplier(InvestmentSecurity security, Date date, Date settlementDate, boolean useNextDayForLookup) {
		Date lookupDate = date;
		ValidationUtils.assertNotNull(settlementDate, "Settlement Date is required.");
		if (DateUtils.compare(settlementDate, lookupDate, false) > 0) {
			// cannot be before settlement date of opening position (sometimes bonds may take more days to settle than usually)
			lookupDate = settlementDate;
		}
		if (useNextDayForLookup) {
			lookupDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(lookupDate), 1);
		}
		return getNotionalMultiplier(security, lookupDate);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentAccrualCalculatorFactory getInvestmentAccrualCalculatorFactory() {
		return this.investmentAccrualCalculatorFactory;
	}


	public void setInvestmentAccrualCalculatorFactory(InvestmentAccrualCalculatorFactory investmentAccrualCalculatorFactory) {
		this.investmentAccrualCalculatorFactory = investmentAccrualCalculatorFactory;
	}


	public InvestmentCurrencyService getInvestmentCurrencyService() {
		return this.investmentCurrencyService;
	}


	public void setInvestmentCurrencyService(InvestmentCurrencyService investmentCurrencyService) {
		this.investmentCurrencyService = investmentCurrencyService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
