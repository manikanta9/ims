package com.clifton.investment.instrument.spot;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;

import java.util.List;


/**
 * The <code>InvestmentSecuritySpotMonthService</code> contains helpful methods for calculating the spot month for securities
 * These are not real objects, but calculated based on the calculator defined on the security's instrument
 *
 * @author manderson
 */
public interface InvestmentSecuritySpotMonthService {


	@DoNotAddRequestMapping
	public InvestmentSecuritySpotMonth getInvestmentSecuritySpotMonth(InvestmentSecurity security, boolean exceptionIfMissing);


	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public List<InvestmentSecuritySpotMonth> getInvestmentSecuritySpotMonthList(SecuritySearchForm searchForm);
}
