package com.clifton.investment.instrument.allocation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.allocation.calculator.InvestmentSecurityAllocationCalculator;
import com.clifton.investment.instrument.allocation.calculator.InvestmentSecurityAllocationCalculatorLocator;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityCalculator;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentSecurityAllocationValidator</code> ...
 *
 * @author manderson
 */
@Component
public class InvestmentSecurityAllocationValidator extends SelfRegisteringDaoValidator<InvestmentSecurityAllocation> {

	private InvestmentSecurityAllocationCalculatorLocator investmentSecurityAllocationCalculatorLocator;

	private InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler;

	private SystemBeanService systemBeanService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentSecurityAllocation bean, DaoEventTypes config) throws ValidationException {

		// Validate Hierarchy for Parent allows Allocations Of Securities
		InvestmentInstrumentHierarchy hierarchy = bean.getParentInvestmentSecurity().getInstrument().getHierarchy();
		ValidationUtils.assertTrue(hierarchy.isAllocationOfSecurities(), "The hierarchy '" + hierarchy.getNameExpanded() + "' for security '" + bean.getParentInvestmentSecurity().getLabel()
				+ "' does not allow securities to be allocations of other securities.");

		InvestmentSecurityAllocationTypes type = hierarchy.getSecurityAllocationType();

		// Apply Type Specific validation
		InvestmentSecurityAllocationCalculator calculator = getInvestmentSecurityAllocationCalculatorLocator().locate(type);
		calculator.validateInvestmentSecurityAllocation(bean);

		// Date Validation
		if (bean.getStartDate() != null) {
			ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");
		}

		// Security or Instrument
		ValidationUtils.assertMutuallyExclusive("Either an instrument or security can be selected for each allocation, but not both", bean.getInvestmentInstrument(), bean.getInvestmentSecurity());
		// If Security Selected - No Current Security Calculator Needed
		if (bean.getInvestmentSecurity() != null) {
			bean.setCurrentSecurityCalculatorBean(null);
		}
		// Else - Required & Validate Calculator Selection
		else {
			// First ensure that the instrument selection is one-to-many - if one to one - makes sense to select at the security level so we don't unnecessarily use current security calculator
			ValidationUtils.assertFalse(bean.getInvestmentInstrument().getHierarchy().isOneSecurityPerInstrument(), "Instrument [" + bean.getInvestmentInstrument().getLabel()
					+ "] is a one-to-one instrument.  Please use security selection for one-to-one instruments.");
			ValidationUtils.assertNotNull(bean.getCurrentSecurityCalculatorBean(), "Allocations that use an instrument selection must also select the current security calculator");
			InvestmentCurrentSecurityCalculator currentCalc = (InvestmentCurrentSecurityCalculator) getSystemBeanService().getBeanInstance(bean.getCurrentSecurityCalculatorBean());
			currentCalc.validateInvestmentSecurityAllocation(bean);
		}

		// UX BY SECURITY OR (INSTRUMENT & CURRENT SECURITY CALCULATOR) BY DATE RANGE FOR A PARENT SECURITY
		InvestmentSecurityAllocation originalBean;
		boolean checkUx = config.isInsert();
		if (config.isUpdate()) {
			originalBean = getOriginalBean(bean);
			// Anything besides allocation weight changed - need to verify ux
			List<String> changedValues = CoreCompareUtils.getNoEqualProperties(originalBean, bean, false, "allocationWeight");
			checkUx = !CollectionUtils.isEmpty(changedValues);
		}

		if (checkUx) {
			SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
			searchForm.setParentInvestmentSecurityId(bean.getParentInvestmentSecurity().getId());
			if (bean.getInvestmentSecurity() != null) {
				searchForm.setInvestmentSecurityId(bean.getInvestmentSecurity().getId());
			}
			else {
				searchForm.setInvestmentInstrumentId(bean.getInvestmentInstrument().getId());
				searchForm.setCurrentSecurityCalculatorBeanId(bean.getCurrentSecurityCalculatorBean().getId());
			}
			List<InvestmentSecurityAllocation> existingList = getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationList(searchForm);
			for (InvestmentSecurityAllocation existing : CollectionUtils.getIterable(existingList)) {
				// Check ID value because when re-balancing, we'll save as MarketDataInvestmentSecurityAllocation which extends this so technically not equal
				if (config.isInsert() || !MathUtils.isEqual(existing.getId(), bean.getId())) {
					ValidationUtils.assertFalse(DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), existing.getStartDate(), existing.getEndDate()), "There exists an allocation for ["
							+ bean.getAllocationLabelExpanded()
							+ "] with an overlapping date range. Selected 1. Security or 2. Instrument & Current Security Calculator must be unique per date range.");
				}
			}
		}
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentSecurityAllocationHandler getInvestmentSecurityAllocationHandler() {
		return this.investmentSecurityAllocationHandler;
	}


	public void setInvestmentSecurityAllocationHandler(InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler) {
		this.investmentSecurityAllocationHandler = investmentSecurityAllocationHandler;
	}


	public InvestmentSecurityAllocationCalculatorLocator getInvestmentSecurityAllocationCalculatorLocator() {
		return this.investmentSecurityAllocationCalculatorLocator;
	}


	public void setInvestmentSecurityAllocationCalculatorLocator(InvestmentSecurityAllocationCalculatorLocator investmentSecurityAllocationCalculatorLocator) {
		this.investmentSecurityAllocationCalculatorLocator = investmentSecurityAllocationCalculatorLocator;
	}
}
