package com.clifton.investment.instrument.calculator.accrual.date;


import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;

import java.util.Date;


/**
 * The <code>InvestmentAccrualDateDefaultSettlementCalculator</code> class is InvestmentAccrualDateCalculator
 * that uses default settlement date for accrual calculations.  Most trades use default settlement
 * cycle convention: this will result in most accurate market value calculation.
 * <p/>
 * If non-standard settlement cycle is used for a particular trade, then accrual value maybe a little
 * different for a day or two.
 *
 * @author vgomelsky
 */
public class InvestmentAccrualDateDefaultSettlementCalculator implements InvestmentAccrualDateCalculator {

	private InvestmentCalculator investmentCalculator;


	@Override
	public Date calculateAccrualDate(InvestmentSecurity security, Date transactionDate) {
		return getInvestmentCalculator().calculateSettlementDate(security, null, transactionDate);
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
