package com.clifton.investment.instrument.upload;


import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.upload.allocation.InvestmentSecurityAllocationUploadCommand;


/**
 * The <code>InvestmentInstrumentUploadService</code> has methods for uploading securities into the system
 *
 * @author Mary Anderson
 */
public interface InvestmentInstrumentUploadService {

	////////////////////////////////////////////////////////////////
	///////////      Investment Security Uploads        ////////////
	////////////////////////////////////////////////////////////////


	/**
	 * Returns a sample DataTable for the upload for securities based on the properties
	 * selected on the upload bean.
	 * If allRows is true, exports ALL data into the excel file, otherwise
	 * just a subset (currently set to 10) rows of data.
	 */
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public DataTable downloadInvestmentSecurityUploadFileSample(InvestmentSecurityUploadCommand uploadCommand, boolean allRows);


	/**
	 * Uploads {@link InvestmentSecurity} into the system
	 * <p/>
	 * Performs proper default setting for fields, and validation
	 */
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public void uploadInvestmentSecurityUploadFile(InvestmentSecurityUploadCommand uploadCommand);


	////////////////////////////////////////////////////////////////
	////////   Investment Security Allocation Uploads     //////////
	////////////////////////////////////////////////////////////////


	/**
	 * Returns a sample DataTable for the upload for securities allocations for given parent security
	 * If allRows is true, exports ALL data into the excel file, otherwise
	 * just a subset (currently set to 10) rows of data.
	 */
	@SecureMethod(dtoClass = InvestmentSecurityAllocation.class)
	public DataTable downloadInvestmentSecurityAllocationUploadFileSample(InvestmentSecurityAllocationUploadCommand uploadCommand, boolean allRows);


	/**
	 * Uploads {@link InvestmentSecurityAllocation} into the system and also supports rebalancing (if security allocation type supports it)
	 * <p/>
	 * Performs proper default setting for fields, and validation
	 */
	@SecureMethod(dtoClass = InvestmentSecurityAllocation.class)
	public Status uploadInvestmentSecurityAllocationUploadFile(InvestmentSecurityAllocationUploadCommand uploadCommand);
}
