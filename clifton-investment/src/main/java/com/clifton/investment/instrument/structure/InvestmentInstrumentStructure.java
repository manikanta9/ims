package com.clifton.investment.instrument.structure;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.structure.calculators.InvestmentStructureCurrentWeightCalculator;
import com.clifton.system.bean.SystemBean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentInstrumentStructure</code> defines the structure an instrument
 * (representative of an index) for a specified period of time.
 * <p/>
 * Example: DJ-UBS Commodities Index has different versions, but they all use the same
 * structure.  Each version (F2, F3) is a security for the instrument, uses this structure
 * and applies any version specific information (weight overrides, current security calculator)
 *
 * @author manderson
 */
public class InvestmentInstrumentStructure extends BaseEntity<Integer> implements LabeledObject {


	private InvestmentInstrumentStructureTypes instrumentStructureType;


	private InvestmentInstrument instrument;

	/**
	 * System Bean that implements {@link InvestmentStructureCurrentWeightCalculator}
	 * that for a given security under the instrument, the structure is passed to calculate
	 * weights for a given date.
	 */
	private SystemBean currentWeightCalculatorBean;

	/**
	 * Structure multiplier that can be applied to the weight calculation.
	 * Example: GSCI Commodity Indices use a GSCI Normalizing Constant.  Normally calculation would
	 * divide by this normalizing constant, so value for additional multiplier = 1/GSCI Normalizing Constant
	 */
	private BigDecimal additionalMultiplier;

	private Date startDate;

	private Date endDate;

	private List<InvestmentInstrumentStructureWeight> instrumentStructureWeightList;


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder lbl = new StringBuilder(16);
		if (getInstrument() != null) {
			lbl.append(getInstrument().getName()).append(": ");
		}
		lbl.append(DateUtils.fromDateRange(getStartDate(), getEndDate(), true, false));
		return lbl.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentStructureTypes getInstrumentStructureType() {
		return this.instrumentStructureType;
	}


	public void setInstrumentStructureType(InvestmentInstrumentStructureTypes instrumentStructureType) {
		this.instrumentStructureType = instrumentStructureType;
	}


	public InvestmentInstrument getInstrument() {
		return this.instrument;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public SystemBean getCurrentWeightCalculatorBean() {
		return this.currentWeightCalculatorBean;
	}


	public void setCurrentWeightCalculatorBean(SystemBean currentWeightCalculatorBean) {
		this.currentWeightCalculatorBean = currentWeightCalculatorBean;
	}


	public BigDecimal getAdditionalMultiplier() {
		return this.additionalMultiplier;
	}


	public void setAdditionalMultiplier(BigDecimal additionalMultiplier) {
		this.additionalMultiplier = additionalMultiplier;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public List<InvestmentInstrumentStructureWeight> getInstrumentStructureWeightList() {
		return this.instrumentStructureWeightList;
	}


	public void setInstrumentStructureWeightList(List<InvestmentInstrumentStructureWeight> instrumentStructureWeightList) {
		this.instrumentStructureWeightList = instrumentStructureWeightList;
	}
}
