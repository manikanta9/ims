package com.clifton.investment.instrument.calculator.accrual.sign;


/**
 * The <code>AccrualSignCalculators</code> enum defines available InvestmentAccrualSignCalculator implementations.
 *
 * @author vgomelsky
 */
public enum AccrualSignCalculators {

	/**
	 * Always negate the sign of accrued interest (Total Return Swaps)
	 */
	NEGATE,

	/**
	 * Negate accrued interest for all calculations except for the accrued interest on the opening trade.
	 */
	NEGATE_EXCEPT_ON_OPENING_TRADE,

	/**
	 * Negate accrued interest for event that uses CUSTOM_FIELD_COUPON_FREQUENCY field and leave it unchanged for other fields.
	 * Can be used to negate that sign of AccrualMethods.DAYCOUNT.
	 */
	NEGATE_COUPON_FREQUENCY,

	/**
	 * Negate accrued interest for event that DO NOT uses CUSTOM_FIELD_COUPON_FREQUENCY field and leave it unchanged for other fields.
	 * Can be used to negate that sign of NON AccrualMethods.DAYCOUNT.
	 */
	NEGATE_NON_COUPON_FREQUENCY,

	/**
	 * Interest Rate Swap specific implementation that is based on the value of InvestmentSecurity.CUSTOM_FIELD_STRUCTURE.
	 */
	IRS_STRUCTURE
}
