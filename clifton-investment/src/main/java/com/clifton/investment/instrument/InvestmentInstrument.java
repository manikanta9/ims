package com.clifton.investment.instrument;


import com.clifton.calendar.setup.Calendar;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.calculator.InvestmentM2MCalculatorTypes;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;

import java.math.BigDecimal;


/**
 * The <code>InvestmentInstrument</code> class represents groupings of {@link InvestmentSecurity} objects.
 * <p>
 * Example:
 * - each future instrument will generally have 4 security instances per year.
 * - each stock option instrument will have a security instance for different exercise prices, expiration dates, and type (put/call).
 * - each stock instrument will have only one security instance.
 *
 * @author vgomelsky
 */
public class InvestmentInstrument extends NamedEntityWithoutLabel<Integer> implements SystemColumnCustomJsonAware {

	/**
	 * Instruments in different investment hierarchies may have hierarchy specific custom columns
	 * which are part of the column group with this name.
	 * Global custom fields (rarely populated values) can also be used.
	 */
	public static final String INSTRUMENT_CUSTOM_FIELDS_GROUP_NAME = "Instrument Custom Fields";

	public static final String INSTRUMENT_ROLL_DATED_CUSTOM_FIELDS_GROUP_NAME = "Roll Spread";
	// Currently used for calculating CCY Futures Monthly Returns. During months we roll, this V.W.A.P. factor in included in the return calculation to account for the change in security
	public static final String INSTRUMENT_ROLL_VOLUME_WEIGHTED_AVERAGE_PRICE_CUSTOM_FIELD = "Volume Weighted Average Price";

	public static final String INSTRUMENT_BTIC_PREFIX_CUSTOM_FIELD = "BTIC Prefix";

	public static final String INSTRUMENT_COUNTRIES_SYSTEM_LIST_NAME = "Countries";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private InvestmentInstrumentHierarchy hierarchy;

	/**
	 * Primary Exchange: The most important stock exchange in a given country. Common characteristics of a primary exchange include a long history,
	 * primary listings of the country's top companies, listings of many important foreign corporations, large total market capitalization and a
	 * large trade value. A country may have other important stock exchanges in addition to its primary exchange.
	 */
	private InvestmentExchange exchange;

	/**
	 * Composite Exchange is not a real exchange. It is used to calculate average or standardized prices for a security across all exchanges where it trades.
	 * Brokers and Custodians usually use composite (as opposed to primary) exchange pricing.
	 */
	private InvestmentExchange compositeExchange;

	/**
	 * Defines the Settlement Calendar to be used when calculating Settlement Date this security.
	 * Can be used to override 'exchange' calendar for non exchange traded securities or for exchange
	 * trade securities from a different exchange. If set, this calendar will also be used for pricing.
	 */
	private Calendar settlementCalendar;
	private SystemListItem countryOfRisk;
	private SystemListItem countryOfIncorporation;

	/**
	 * The currency denomination for securities of this instrument.  "currencyDenomination" would be a better field name.
	 */
	private InvestmentSecurity tradingCurrency;

	private InvestmentInstrument underlyingInstrument;

	/**
	 * Optional "big" instrument that this "mini" instrument is linked to.
	 * Example: S&P 500 Future is ES and corresponding "mini" is SP
	 * Example: S&P 400 Future is EA and corresponding "mini" is MD
	 * <p>
	 * The price of the big instrument (when available) is used in mark to market calculations (proportional to multipliers).
	 * Also, big and mini positions can be summed together to get total holdings that can be checked against exchange limits.
	 */
	private InvestmentInstrument bigInstrument;

	/**
	 * Internally managed funds will have a security (bought by our clients) as well as corresponding investment
	 * account that implements the fund: Defensive Equity, Commodity LP, etc.
	 * <p>
	 * Second use case: LDI uses Client Account specific benchmarks for creating KRD values for liabilities.
	 */
	private InvestmentAccount investmentAccount;

	private String identifierPrefix;

	/**
	 * Optional group name can be used in reporting in order to combine multiple instruments together under this name.
	 * COALESCE(groupName, name) will usually be used in order to do this.
	 * For example, one might want to combine MFS and MES contracts in reporting under "MSCI EAFE Mini".
	 */
	private String groupName;

	/**
	 * Default Notional Calculator: Notional = priceMultiplier * price * quantity
	 */
	private BigDecimal priceMultiplier = BigDecimal.ONE;

	/**
	 * Exposure Multiplier (used in PIOS) used for a few futures (Euro dollar and Euro Yen = 4)
	 * where the exposure is different than the notional value and needs to be accounted for by this multiplier
	 * Example:
	 * Euro Dollar has contract size of $1,000,000 (multiplied by 4 to get annualized equivalent of quarterly interest rate: 12 / 3 = 4).
	 * Exposure = Price * Quantity * Multiplier * 4 where 4 is the exposure multiplier
	 */
	private BigDecimal exposureMultiplier = BigDecimal.ONE;

	/**
	 * Specifies the number of days it may take to settle a security for this Instrument.
	 * This value overrides the value specified at Investment Type and Investment Hierarchy.
	 * Some Securities can override this value via custom fields.
	 */
	private Integer daysToSettle;

	/**
	 * The type of calculator to use to calculate costs for securities under this instrument. Overrides hierarchy calculator.
	 */
	private InvestmentNotionalCalculatorTypes costCalculator;

	/**
	 * The type of calculator to use to calculate mark to market. Overrides hierarchy calculator.
	 */
	private InvestmentM2MCalculatorTypes m2mCalculator;

	/**
	 * Defines accrual method that should be used when calculating accrual for securities of this instrument.
	 * Most of the time it's DAYCOUNT. However, some swaps may use custom calculations.
	 * If not set, hierarchy accrual method is used. Can only be set when accrualSecurityEventType is defined.
	 */
	private AccrualMethods accrualMethod;
	/**
	 * Defines accrual date calculator to be used for this instrument's securities. Overrides hierarchy calculator.
	 * If not set, hierarchy accrual calculator is used. Can only be set when accrualSecurityEventType is defined.
	 */
	private AccrualDateCalculators accrualDateCalculator;

	private AccrualMethods accrualMethod2;

	private AccrualDateCalculators accrualDateCalculator2;

	// fields specific to futures?
	private boolean deliverable;
	private boolean discountNote;
	// Fair value adjustment to futures value. Used for Equity Index Futures to calculate mispricing per contract and displayed/used on Trade Creation screen for PMs to include the mispricing information into their trades.
	// Mispricing calculation: (Future Price - (Index Price + Fair Value Adjustment)) * FX Rate * Future Price Multiplier * # of Contracts
	private boolean fairValueAdjustmentUsed;

	/**
	 * Initial margin is the amount per contract required to open a position.
	 * Secondary margin (maintenance margin) is usually about 75% of initial margin.
	 * If margin balance falls below maintenance margin requirement, margin call is made and position holder is required to get margin back to Initial margin level.
	 */
	private BigDecimal hedgerInitialMarginPerUnit;
	private BigDecimal hedgerSecondaryMarginPerUnit;
	private BigDecimal speculatorInitialMarginPerUnit;
	private BigDecimal speculatorSecondaryMarginPerUnit;

	/**
	 * Indicates whether this instrument is inactive. For 1 to 1 securities this field is automatically updated and goes beyond security maturity date.
	 * For example, ABS and CDS become inactive after factor goes to 0.  IRS become inactive after full early termination.
	 */
	private boolean inactive;

	/**
	 * Used for 1:Many Instruments (Primarily Futures/Options on Futures) to determine when the security's spot month
	 * begins/end.
	 * Spot Month is a time period until last delivery or expiration date of the the contract.  Used for Exchange Limits as we enter the Spot month
	 * our limits are generally reduced so we start trading out of that contract and into a new one.
	 */
	private SystemBean spotMonthCalculatorBean;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * A List of custom column values for this security (field are assigned and vary by investment hierarchy)
	 */
	@NonPersistentField
	private String columnGroupName;

	/**
	 * JSON String value representing the custom column values
	 */
	private CustomJsonString customColumns;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getHierarchy() == null) {
			return getName();
		}
		StringBuilder result = new StringBuilder();
		result.append(getName());
		result.append(" (");
		result.append(getHierarchy().getLabelExpanded());
		result.append(')');
		return result.toString();
	}


	public String getLabelShort() {
		StringBuilder result = new StringBuilder();
		result.append(getIdentifierPrefix());
		result.append(": ");
		result.append(getName());
		return result.toString();
	}


	public String getLabelLong() {
		if (getHierarchy() == null) {
			return getLabelShort();
		}
		return getLabelShort() + " (" + getHierarchy().getLabelExpanded() + ")";
	}


	public InvestmentExchange getExchange() {
		return this.exchange;
	}


	public void setExchange(InvestmentExchange exchange) {
		this.exchange = exchange;
	}


	public InvestmentExchange getCompositeExchange() {
		return this.compositeExchange;
	}


	public void setCompositeExchange(InvestmentExchange compositeExchange) {
		this.compositeExchange = compositeExchange;
	}


	public InvestmentSecurity getTradingCurrency() {
		return this.tradingCurrency;
	}


	public void setTradingCurrency(InvestmentSecurity tradingCurrency) {
		this.tradingCurrency = tradingCurrency;
	}


	public InvestmentInstrumentHierarchy getHierarchy() {
		return this.hierarchy;
	}


	public void setHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		this.hierarchy = hierarchy;
	}


	public InvestmentInstrument getUnderlyingInstrument() {
		return this.underlyingInstrument;
	}


	public void setUnderlyingInstrument(InvestmentInstrument underlyingInstrument) {
		this.underlyingInstrument = underlyingInstrument;
	}


	public String getIdentifierPrefix() {
		return this.identifierPrefix;
	}


	public void setIdentifierPrefix(String identifierPrefix) {
		this.identifierPrefix = identifierPrefix;
	}


	/**
	 * Should not be used directly (unless explicitly needing instrument price multiplier only)
	 * Use security.getPriceMultiplier instead of this method which will return the coalesce of the security override value with this value.
	 */
	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	public void setPriceMultiplier(BigDecimal priceMultiplier) {
		this.priceMultiplier = priceMultiplier;
	}


	public boolean isDeliverable() {
		return this.deliverable;
	}


	public void setDeliverable(boolean deliverable) {
		this.deliverable = deliverable;
	}


	public boolean isDiscountNote() {
		return this.discountNote;
	}


	public void setDiscountNote(boolean discountNote) {
		this.discountNote = discountNote;
	}


	public BigDecimal getHedgerInitialMarginPerUnit() {
		return this.hedgerInitialMarginPerUnit;
	}


	public void setHedgerInitialMarginPerUnit(BigDecimal hedgerInitialMarginPerUnit) {
		this.hedgerInitialMarginPerUnit = hedgerInitialMarginPerUnit;
	}


	public BigDecimal getHedgerSecondaryMarginPerUnit() {
		return this.hedgerSecondaryMarginPerUnit;
	}


	public void setHedgerSecondaryMarginPerUnit(BigDecimal hedgerSecondaryMarginPerUnit) {
		this.hedgerSecondaryMarginPerUnit = hedgerSecondaryMarginPerUnit;
	}


	public BigDecimal getSpeculatorInitialMarginPerUnit() {
		return this.speculatorInitialMarginPerUnit;
	}


	public void setSpeculatorInitialMarginPerUnit(BigDecimal speculatorInitialMarginPerUnit) {
		this.speculatorInitialMarginPerUnit = speculatorInitialMarginPerUnit;
	}


	public BigDecimal getSpeculatorSecondaryMarginPerUnit() {
		return this.speculatorSecondaryMarginPerUnit;
	}


	public void setSpeculatorSecondaryMarginPerUnit(BigDecimal speculatorSecondaryMarginPerUnit) {
		this.speculatorSecondaryMarginPerUnit = speculatorSecondaryMarginPerUnit;
	}


	public InvestmentInstrument getBigInstrument() {
		return this.bigInstrument;
	}


	public void setBigInstrument(InvestmentInstrument bigInstrument) {
		this.bigInstrument = bigInstrument;
	}


	public InvestmentNotionalCalculatorTypes getCostCalculator() {
		return this.costCalculator;
	}


	public void setCostCalculator(InvestmentNotionalCalculatorTypes costCalculator) {
		this.costCalculator = costCalculator;
	}


	public boolean isFairValueAdjustmentUsed() {
		return this.fairValueAdjustmentUsed;
	}


	public void setFairValueAdjustmentUsed(boolean fairValueAdjustmentUsed) {
		this.fairValueAdjustmentUsed = fairValueAdjustmentUsed;
	}


	public BigDecimal getExposureMultiplier() {
		return this.exposureMultiplier;
	}


	public void setExposureMultiplier(BigDecimal exposureMultiplier) {
		this.exposureMultiplier = exposureMultiplier;
	}


	public InvestmentM2MCalculatorTypes getM2mCalculator() {
		return this.m2mCalculator;
	}


	public void setM2mCalculator(InvestmentM2MCalculatorTypes m2mCalculator) {
		this.m2mCalculator = m2mCalculator;
	}


	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	public AccrualDateCalculators getAccrualDateCalculator() {
		return this.accrualDateCalculator;
	}


	public void setAccrualDateCalculator(AccrualDateCalculators accrualDateCalculator) {
		this.accrualDateCalculator = accrualDateCalculator;
	}


	public String getGroupName() {
		return this.groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public AccrualMethods getAccrualMethod() {
		return this.accrualMethod;
	}


	public void setAccrualMethod(AccrualMethods accrualMethod) {
		this.accrualMethod = accrualMethod;
	}


	public AccrualMethods getAccrualMethod2() {
		return this.accrualMethod2;
	}


	public void setAccrualMethod2(AccrualMethods accrualMethod2) {
		this.accrualMethod2 = accrualMethod2;
	}


	public AccrualDateCalculators getAccrualDateCalculator2() {
		return this.accrualDateCalculator2;
	}


	public void setAccrualDateCalculator2(AccrualDateCalculators accrualDateCalculator2) {
		this.accrualDateCalculator2 = accrualDateCalculator2;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	public CustomJsonString getCustomColumns() {
		return this.customColumns;
	}


	public void setCustomColumns(CustomJsonString customColumns) {
		this.customColumns = customColumns;
	}


	public SystemListItem getCountryOfRisk() {
		return this.countryOfRisk;
	}


	public void setCountryOfRisk(SystemListItem countryOfRisk) {
		this.countryOfRisk = countryOfRisk;
	}


	public SystemListItem getCountryOfIncorporation() {
		return this.countryOfIncorporation;
	}


	public void setCountryOfIncorporation(SystemListItem countryOfIncorporation) {
		this.countryOfIncorporation = countryOfIncorporation;
	}


	public Calendar getSettlementCalendar() {
		return this.settlementCalendar;
	}


	public void setSettlementCalendar(Calendar settlementCalendar) {
		this.settlementCalendar = settlementCalendar;
	}


	public SystemBean getSpotMonthCalculatorBean() {
		return this.spotMonthCalculatorBean;
	}


	public void setSpotMonthCalculatorBean(SystemBean spotMonthCalculatorBean) {
		this.spotMonthCalculatorBean = spotMonthCalculatorBean;
	}
}
